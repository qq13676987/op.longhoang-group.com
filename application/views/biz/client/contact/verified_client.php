<script>
    $(function () {
        $('#tt').edatagrid({
            url: '/biz_client_contact/get_verified_client_data/',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            }
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
        });

    });
    function email_for(value, row, index){
        if(value.length < 3) return value;
        if(row.email_verify == 1) return value + "<br><span style='color: blue;'>&nbsp;已验证</span>";
        if(row.email_verify == 0) return value + `<br><span style='color: green;'>&nbsp;未验证</span>`;
    }
    function telephone_for(value, row, index) {
        var str = value;
        if(row.telephone_verify == 1){
            str += "<br><span style='color: blue;'>&nbsp;已验证</span>"
        }else if(value != '' && row.telephone_verify == 0){
            str += `<br><span style='color: green;'>&nbsp;未验证</span>`;
        }
        return str;
    }
    function position_for(value, row, index) {
        var str = value;
        return str+"<br><span style='color: green;'>&nbsp;"+row.job_content+"</span>";
    }
    function menu_for(value, row, index) {
        var str = value;
        return str.replaceAll(',','<br>');
    }
    function company_name_for(value, row, index) {
        var str = value;
        return str+"<br><span style='color: green;'>&nbsp;"+row.company_name_en+"</span>";
    }
    function taxpayer_name_for(value, row, index) {
        var str = value;
        str+="<br><span>"+row.taxpayer_id+"</span>"
        str+="<br><span>"+row.taxpayer_address+"</span>"
        str+="<br><span>"+row.taxpayer_telephone+"</span>"
        str+="<br><span>"+row.invoice_email+"</span>"
        return str;
    }
</script>
<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th field="create_by_name" width="100">创建人</th>
        <th field="update_time" width="150" sortable="true">更新时间</th>
        <th field="name" width="100" >验证联系人</th>
        <th field="telephone" width="120" formatter="telephone_for">验证电话</th>
        <th field="email" width="150" formatter="email_for">验证邮箱</th>
        <th field="position" width="120" formatter="position_for" >公司职位</th>
        <th field="menu" width="100" formatter="menu_for">前台权限</th>
        <th field="client_name" width="190" >客户简称</th>
        <th field="company_name" width="290" formatter="company_name_for">客户全称</th>
        <th field="taxpayer_name" width="360" formatter="taxpayer_name_for">开票信息</th>
        <th field="verified_name" width="100">验证发送人</th>
    </tr>
    </thead>
</table>
<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="open_chaxun()"><?= lang('query');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#tt').datagrid('reload','/biz_client_contact/get_verified_client_data2')"><?= lang('已发送未验证');?></a>
            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>
<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                <?= lang('query');?> 1
            </td>
            <td align="right">
                <select name="f1"   id="f1"  class="easyui-combobox"  required="true" style="width:150px;" data-options=" onSelect:function(v){
                change_person(v,1)

        }">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$rs[1]'>$rs[0]</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1"  id="s1"  class="easyui-combobox"  required="true" style="width:100px;" >
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value="<>">!=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;<span class="v1">
                    <input id="v1" class="easyui-textbox "  style="width:120px;">
                </span>



            </td>
        </tr>
        <tr>
            <td>
                <?= lang('query');?> 2
            </td>
            <td align="right">
                <select name="f2"  id="f2"  class="easyui-combobox"  required="true" style="width:150px;" data-options=" onSelect:function(v){
                change_person(v,2)

        }">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$rs[1]'>$rs[0]</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2"  id="s2"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value="<>">!=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <span class="v2">
                    <input id="v2" class="easyui-textbox"  style="width:120px;">
                </span>

            </td>
        </tr>

        <tr>
            <td>
                <?= lang('query');?> 3
            </td>
            <td align="right">
                <select name="f3"  id="f3"  class="easyui-combobox"  required="true" style="width:150px;"data-options=" onSelect:function(v){
                change_person(v,3)

        }">
                    <?php
                    foreach ($f as $rs){
                        echo "<option value='$rs[1]'>$rs[0]</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s3"  id="s3"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value="<>">!=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <span class="v3">
                    <input id="v3" class="easyui-textbox"  style="width:120px;">
                </span>

            </td>
        </tr>

    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;"><?= lang('search');?></a>
</div>
<script>
    let v1status = '';
    let v2status = '';
    let v3status = '';
    function open_chaxun(){
        let v1 = $('#f1').combobox('getValue')
        let v2 = $('#f2').combobox('getValue')
        let v3 = $('#f3').combobox('getValue')
        change_person({value:v1},1)
        change_person({value:v2},2)
        change_person({value:v3},3)
        $('#chaxun').window('open')
    }
    function change_person(v,index){
        let ele = 'v'+index
        if(v.value == 'update_time'){
            $('.'+ele).html(`<input id='${ele}' class='easyui-datebox'  style='width:120px;'>`)
            $('#'+ele).datebox()
            if(index == 1){
                v1status = '1'
            }else if(index == 2){
                v2status = '1'
            }else if(index == 3){
                v3status = '1'
            }
        }else if(v.value == 'create_by'){
            $('.'+ele).html(`<input id='${ele}' class='easyui-combobox'  style='width:120px;'>`)
            $('#'+ele).combobox({
                valueField: 'id',
                textField: 'otherName',
                url: '/bsc_user/get_data_role',
            })
            if(index == 1){
                v1status = '2'
            }else if(index == 2){
                v2status = '2'
            }else if(index == 3){
                v3status = '2'
            }
        }else{
            $('.'+ele).html(`<input id='${ele}' class='easyui-textbox'  style='width:120px;'>`)
            $('#'+ele).textbox()
            if(index == 1){
                v1status = '3'
            }else if(index == 2){
                v2status = '3'
            }else if(index == 3){
                v3status = '3'
            }
        }
    }
    function doSearch(){
        let v1 = '';
        let v2 = '';
        let v3 = '';
        if(v1status == '1'){
            v1 = $('#v1').datebox('getValue')
        }else if(v1status == '2'){
            v1 = $('#v1').combobox('getValue')
        }else if(v1status == '3'){
            v1 = $('#v1').textbox('getValue')
        }
        if(v2status == '1'){
            v2 = $('#v2').datebox('getValue')
        }else if(v2status == '2'){
            v2 = $('#v2').combobox('getValue')
        }else if(v2status == '3'){
            v2 = $('#v2').textbox('getValue')
        }
        if(v3status == '1'){
            v3 = $('#v3').datebox('getValue')
        }else if(v3status == '2'){
            v3 = $('#v3').combobox('getValue')
        }else if(v3status == '3'){
            v3 = $('#v3').textbox('getValue')
        }
        $('#tt').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: v1 ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: v2 ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: v3 ,
        });
        $('#chaxun').window('close');
    }
</script>