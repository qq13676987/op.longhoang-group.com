<script>
    $(function () {
        //异步写入数据
        $('#save_data').click(function () {
            var field = {};
            $.each($('#form').serializeArray(), function (index, item) {
                field[item.name] = item.value;
            });
            var send_status = $('#send_status').val();
            if(send_status === '0'){
                $.messager.alert("<?= lang('提示');?>", '<?= lang('请点击测试连接成功再试');?>');
                return false;
            }
            if(send_status === '-1'){
                $.messager.alert("<?= lang('提示');?>", '<?= lang('测试连接未成功,无法提交');?>');
                return false;
            }
            var url = '/mail_sender/add_data/';

            if(field.id !== '') url = '/mail_sender/update_data/';

            $.ajax({
                url: url,
                type: 'POST',
                data: field,
                dataType: 'json',
                success: function (data) {
                    if (data.code == 1){
                        $('#send_status').val(0);
                        $.messager.alert("<?= lang('提示');?>", data.msg);
                        location.reload();
                    }else{
                        $.messager.alert("<?= lang('提示');?>", '<?= lang('保存失败：');?>'+data.msg, 'error');
                        location.reload();
                    }
                },
                error: function (xhr, status, error) {
                    $.messager.alert("<?= lang('提示');?>", xhr.responseText); //xhr.responseText
                    location.reload();
                }
            });
        });

        //表头工具栏“send_email”事件
        $('#testConnect').click(function () {
            var smtp_host = $('#smtp_host').textbox('getValue');
            var smtp_username = $('#smtp_username').textbox('getValue');
            var smtp_password = $('#smtp_password').textbox('getValue');
            var smtp_port = $('#smtp_port').textbox('getValue');
            var ssl_enable = $('.ssl_enable:checked').val();
            ajaxLoading();
            $.ajax({
                url: '/mail_sender/testConnect',
                type: 'post',
                dataType: 'json',
                data:{
                    smtp_host:smtp_host,
                    smtp_username:smtp_username,
                    smtp_password:smtp_password,
                    smtp_port:smtp_port,
                    ssl_enable:ssl_enable,
                },
                success: function (data) {
                    ajaxLoadEnd();
                    $.messager.alert("<?= lang('提示');?>", data.msg);
                    if (data.code == 0){
                        $('#send_status').val(1);
                        $('#save_data').trigger('click');
                    }else{
                        $('#send_status').val(-1);
                    }
                },
                error: function (xhr, status, error) {
                    ajaxLoadEnd();
                    $.messager.alert("<?= lang('提示');?>", xhr.responseText); //xhr.responseText
                }
            });
        });

        $('#form').form({
            onChange: function (target) {
                if($('#send_status').val() !== '0'){
                    $.messager.alert('<?= lang('提示');?>', '<?= lang('值已修改,需要重新测试连接');?>');
                    $('#send_status').val(0);
                }

            },
        });
    });
</script>
<title><?= lang('邮箱配置');?></title>
<body style="margin:10px;">
<div>
    <input type="hidden" name="send_status" id="send_status" value="0">
    <form id="form" method="post">
        <input type="hidden" name="id" id="id" value="<?= $id;?>">
        <table>
            <tr style="height: 40px;">
                <td><?= lang('通道');?>:</td>
                <td>
                    <input id="sender_name" name="sender_name" class="easyui-textbox" readonly value="<?= $sender_name;?>" data-options="required:true," style="width: 240px;">
                </td>
            </tr>
            <tr style="height: 40px;">
                <td><?= lang('邮箱账号');?>:</td>
                <td>
                    <input id="smtp_username" name="smtp_username" class="easyui-textbox" readonly value="<?= $smtp_username;?>" data-options="required:true," style="width: 240px;">
                </td>
            </tr>
            <tr style="height: 40px;">
                <td><?= lang('邮箱密码');?>:</td>
                <td>
                    <input id="smtp_password" name="smtp_password" class="easyui-textbox" value="<?= $smtp_password;?>" data-options="required:true," style="width: 240px;">
                    <span style="color:red;font-size:14px;margin-left:10px;">
<!--                    --><?//= lang('请仔细阅读页面底部的配置说明。');?>
                    </span>
                </td>
            </tr>
            <tr style="height: 40px;">
                <td><?= lang('通道地址');?>:</td>
                <td>
                    <input id="smtp_host" name="smtp_host" class="easyui-textbox" value="<?= $smtp_host;?>" data-options="required:true," style="width: 240px;">
                </td>
            </tr>
            <tr style="height: 40px;">
                <td><?= lang('发送端口');?>:</td>
                <td>
                    <input id="smtp_port" name="smtp_port" class="easyui-textbox" value="<?= $smtp_port;?>" data-options="required:true," style="width: 240px;">
                </td>
            </tr>
            <tr style="height: 40px;">
                <td><?= lang('使用SSL');?>:</td>
                <td>
                    <input type="radio" class="ssl_enable" name="ssl_enable" <?= $ssl_enable === '1' ? 'checked' : '';?> value="1" > <?= lang('是');?></input>
                    <input type="radio" class="ssl_enable" <?= $ssl_enable === '0' ? 'checked' : '';?> name="ssl_enable" value="0" > <?= lang('否');?></input>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <a href="javascript:void(0);" class="easyui-linkbutton" id="save_data" style="display: none;"><?= lang('保存');?></a>
                    <a href="javascript:void(0);" class="easyui-linkbutton" id="testConnect"><?= lang('测试并保存');?></a>

                </td>
            </tr>
        </table>
    </form>

    <br><br><br>
<!--    请仔细阅读步骤：<br><br>-->
<!--    第1步： 打开 https://exmail.qq.com/ 用上面你填的账号和密码登录试试看。 如果登录不了，请找HR重置密码。 <span style="color:red;"> 麻烦仔细看清楚，用上面的账号和密码登录打网页版的邮箱，如果登录不了，一切都是浮云！</span>-->
<!--    <br><br>-->
<!--    第2步：  参考这个网址：<a href='https://open.work.weixin.qq.com/help2/pc/19902?person_id=1' target="_blank"> https://open.work.weixin.qq.com/help2/pc/19902?person_id=1 </a>-->
<!--    获取客户端专用密码。 然后填入上面的密码框保存。-->
    <br><br>   <br><br>   <br><br>
    <br><br>
    <br>

    <br><br><br>
    <br><br><br>
</div>
</body>