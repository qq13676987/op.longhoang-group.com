<?php


class biz_shipment_ebooking_model extends CI_Model
{
    protected $table = 'biz_shipment_ebooking';
    
    public $field_edit = array('id', 'sales_id', 'sales_group','client_contact', 'client_email', 'client_telephone', 'customer_service_id', 'customer_service_group','client_code', 'client_code2','client_company', 'operator_id', 'operator_group', 'shipper_ref', 'hbl_type', 'trans_origin' ,'trans_origin_name', 'trans_discharge' ,'trans_discharge_name', 'trans_destination_inner', 'trans_destination', 'trans_destination_name', 'trans_destination_terminal', 'trans_term', 'INCO_term', 'requirements', 'biz_type', 'trans_mode', 'trans_tool','box_info', 'goods_type', 'service_options', 'trans_carrier', 'booking_ETD', 'quotation_id', 'free_svr', 'agent_code','cus_no', 'carrier_ref', 'INCO_address');
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_one($f = "", $v = "")
    {
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_where_one($where='', $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where, null, false);
        $this->db->order_by($sort, $order, false);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function total($where = "")
    {
        if ($where != "") $this->db->where($where, null, false);
        $this->db->from($this->table);
        return $this->db->count_all_results('');
    }
    
    public function join_get($where = "", $sort = "id", $order = "desc")
    {
        $this->db->join('biz_client', 'biz_client.client_code = biz_shipment_ebooking.client_code', 'LEFT');
        return $this->get($where, $sort, $order);
    }

    public function join_total($where = "")
    {
        $this->db->join('biz_client', 'biz_client.client_code = biz_shipment_ebooking.client_code', 'LEFT');
        return $this->total($where);
    }

    public function save($data = array())
    {
        $data["created_time"] = date('Y-m-d H:i:s', time());
        $data["created_by"] = $this->session->userdata('id');
        $data["updated_time"] = date('Y-m-d H:i:s', time());
        $data["updated_by"] = $this->session->userdata('id');
        
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id = '', $data = array())
    {
        $data["updated_time"] = date('Y-m-d H:i:s', time());
        $data["updated_by"] = $this->session->userdata('id');
        
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id = '')
    {
        // delete container
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
}