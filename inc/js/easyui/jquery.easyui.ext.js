/**
 * 该文件用于单独重写esayui 的一些方法使用
 * 汪庭彬
 */
$.fn.datebox.defaults.formatter = function(date) {
    //为加密格式时直接返回
    if(date === '***') return date;
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    return y + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);
};
$.fn.datetimebox.defaults.formatter = function(date){
    //为加密格式时直接返回
    if(date === '***') return date;
    var h=date.getHours();
    var M=date.getMinutes();
    var s=date.getSeconds();
    function _ae0(_ae1){
        return (_ae1<10?"0":"")+_ae1;
    };
    var _ae2=$(this).datetimebox("spinner").timespinner("options").separator;
    var r=$.fn.datebox.defaults.formatter(date)+" "+_ae0(h)+_ae2+_ae0(M);
    if($(this).datetimebox("options").showSeconds){
        r+=_ae2+_ae0(s);
    }
    return r;
};
//2022-05-25 由于金晶想改为数字的,所以这里加入纯数字兼容格式
$.fn.datebox.defaults.parser = function(s) {
    if(!s) return new Date();

    //如果是纯数字,走另一套规则
    var csz = /^[0-9]*$/;
    var ss = [];
    if (csz.test(s)) {
        ss.push(s.substr(0,4));
        ss.push(s.substr(4,2));
        ss.push(s.substr(6,2));
    }else{
        //为加密格式时直接返回
        if(s === '***') return  s;
        ss = s.split('-');
    }

    var y=parseInt(ss[0],10);
    var m=parseInt(ss[1],10);
    var d=parseInt(ss[2],10);
    if(!isNaN(y)&&!isNaN(m)&&!isNaN(d)) return new Date(y, m - 1, d);
    else return new Date();
};
//给datebox的 textbox加一个 绑定的脱离焦点自动选择事件
let old_blur = $.fn.datebox.defaults.events.blur;
$.fn.datebox.defaults.events.blur = function(e){
    old_blur(e);
    var opt_data;
    if($(e.target).parent().prev()[0] == undefined) opt_data = {};
    else opt_data = $.data($(e.target).parent().prev()[0]);
    if(opt_data.datebox !== undefined){
        //如果数值少于8位数,不触发
        var auto_enter = false;
        
        if($($(e.target).parent().prev()[0]).datebox('getValue').length == 8) auto_enter = true;
        
        if(auto_enter) _aa9($(e.target).parent().prev()[0]);
        
        function _aa9(_aaa){
            var _aab=$.data(_aaa,"datebox");
            var opts=_aab.options;
            var _aac=_aab.calendar.calendar("options").current;
            if(_aac){
                _aa8(_aaa,opts.formatter.call(_aaa,_aac));
                $(_aaa).combo("hidePanel");
            }
        };
        function _aa8(_aad,_aae,_aaf){
            var _ab0=$.data(_aad,"datebox");
            var opts=_ab0.options;
            var _ab1=_ab0.calendar;
            _ab1.calendar("moveTo",opts.parser.call(_aad,_aae));
            if(_aaf){
                $(_aad).combo("setValue",_aae);
            }else{
                if(_aae){
                    _aae=opts.formatter.call(_aad,_ab1.calendar("options").current);
                }
                $(_aad).combo("setText",_aae).combo("setValue",_aae);
            }
        }
    }
};

//2023-10-11 我以前那个方法不见了, 这里新加一个 生成一个便携式表单的弹窗
$.messager.diy = function(_17,msg, fn, form_items = ""){
    //参数1标题, 参数二 显示的文案, 参数3 回调函数
    //form_items 表示表单元素
    var _18=typeof _17=="object"?_17:{title:_17,msg:msg,fn:fn,form_items:form_items};
    if(_18.form_items === '') _18.form_items = "<input name='text' class=\"messager-input\" type=\"text\"/>";
    _18=$.extend({},$.messager.defaults,
        {content:
                "<div class=\"messager-icon messager-question\"></div>"+
                "<div>"+_18.msg+"</div>"+"<br/>"+"<div style=\"clear:both;\"/>"+
                "<div>" +
                "<form class='messager-form'>" +
                _18.form_items +
                "</form>" +
                "</div>"
        },_18);
    function _6(dlg,_10){
        dlg.dialog("close");
        dlg.dialog("options").fn(_10);
    }
    if(!_18.buttons){
        _18.buttons=[
            {text:_18.ok,onClick:function(){
                var _6_f_d = dlg.find(".messager-form").serializeArray();

                var _d_d = {};
                $.each(_6_f_d, function (i, it) {
                    //判断如果name存在,且为string类型
                    if (_d_d.hasOwnProperty(it.name) === true) {
                        if (typeof _d_d[it.name] == 'string') {
                            _d_d[it.name] = [_d_d[it.name]];
                            _d_d[it.name].push(it.value);
                        } else {
                            _d_d[it.name].push(it.value);
                        }
                    } else {
                        _d_d[it.name] = it.value;
                    }
                });

                _6(dlg, _d_d);
            }},
            {text:_18.cancel,onClick:function(){
                _6(dlg);
            }}
        ];
    }
    function _d(_e){
        function _1(){
            $(document).unbind(".messager").bind("keydown.messager",function(e){
                if(e.keyCode==27){
                    $("body").children("div.messager-window").children("div.messager-body").each(function(){
                        $(this).dialog("close");
                    });
                }else{
                    if(e.keyCode==9){
                        var _2=$("body").children("div.messager-window");
                        if(!_2.length){
                            return;
                        }
                        var _3=_2.find(".messager-input,.messager-button .l-btn");
                        for(var i=0;i<_3.length;i++){
                            if($(_3[i]).is(":focus")){
                                $(_3[i>=_3.length-1?0:i+1]).focus();
                                return false;
                            }
                        }
                    }else{
                        if(e.keyCode==13){
                            var _4=$(e.target).closest("input.messager-input");
                            if(_4.length){
                                var _5=_4.closest(".messager-body");
                                _6(_5,_4.val());
                            }
                        }
                    }
                }
            });
        };
        _1();
        var _f=$("<div class=\"messager-body\"></div>").appendTo("body");
        _f.dialog($.extend({},_e,{noheader:(_e.title?false:true),onClose:function(){
                function _7(){
                    $(document).unbind(".messager");
                };
                _7();
                if(_e.onClose){
                    _e.onClose.call(this);
                }
                setTimeout(function(){
                    _f.dialog("destroy");
                },100);
            }}));
        var win=_f.dialog("dialog").addClass("messager-window");
        win.find(".dialog-button").addClass("messager-button").find("a:first").focus();
        return _f;
    };
    var dlg=_d(_18);
    $.parser.parse(dlg.find('.messager-form'));

    return dlg;
};