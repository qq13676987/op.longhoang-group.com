<style>
    .isf_div{
        text-align: center;
        margin: auto;
        width: 1200px;
    }
    table{
        border-collapse: collapse;
        border-spacing: 0;
        width: 1200px;
    }
    /*tr td:nth-child(2){*/
    /*    width: 350px;*/
    /*}*/
    td{
        border: 1px solid black;
    }
    .yellow{
        background-color: yellow;
    }
    .blue{
        background-color: rgb(184,204,228);
    }
</style>
<body>
    <div class="isf_div" id="tb">
        <h2>预配舱单（船代）</h2>
        <?php foreach($shipments as $k => $shipment){ ?>
        <table border='0' cellpadding='4' cellspacing='1'>
            <tr>
                <td class="yellow">提单号</td>
                <td>
                    <?= $shipment['billofladingno'];?>
                </td>
                <td class="yellow">货物类型</td>
                <td colspan="2">
                    <?= $shipment['cargoid'];?>
                </td>
                <td class="yellow">收货地</td>
                <td>
                    
                </td>
                <td colspan="6">
                    
                </td>
            </tr>
            <tr>
                <td class="yellow">运编号</td>
                <td>
                    <?= $shipment['billofladingno'];?>
                </td>
                <td class="yellow">运输条款</td>
                <td colspan="2">
                    <?= $shipment['shippingitem'];?>
                </td>
                <td class="yellow">装货港</td>
                <td>
                    <?= $shipment['co_portofloading'];?>
                </td>
                <td colspan="6">
                    <?= $shipment['portofloading'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">船司</td>
                <td>
                    <?= $shipment['carrier'];?>
                </td>
                <td class="blue">约号</td>
                <td colspan="2">
                    
                </td>
                <td class="yellow">卸货港</td>
                <td>
                    <?= $shipment['co_portofdischarge'];?>
                </td>
                <td colspan="6">
                    <?= $shipment['portofdischarge'];?>
                </td>
            </tr>
            <tr>
                <td class="blue">船舶呼号</td>
                <td>
                    
                </td>
                <td class="yellow">提单类型</td>
                <td colspan="2">
                    <?= $shipment['billofladingtype'];?>
                </td>
                <td class="blue">中转港</td>
                <td>
                    
                </td>
                <td colspan="6">
                    
                </td>
            </tr>
            <tr>
                <td class="yellow">船名</td>
                <td>
                    <?= $shipment['oceanvessel'];?>
                </td>
                <td class="yellow">签发地</td>
                <td>
                    <?= $shipment['placeofissuecode'];?>
                </td>
                <td>
                    <?= $shipment['placeofissue'];?>
                </td>
                <td class="yellow">交货地</td>
                <td>
                    <?= $shipment['co_placeofdelivery'];?>
                </td>
                <td colspan="6">
                    <?= $shipment['placeofdelivery'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">航次</td>
                <td>
                    <?= $shipment['voyno'];?>
                </td>
                <td class="yellow">提单份数</td>
                <td colspan="2">
                    <?= $shipment['numberofcopys'];?>
                </td>
                <td class="blue">目的地</td>
                <td>
                    
                </td>
                <td colspan="6">
                    
                </td>
            </tr>
            <tr>
                <td class="blue">订舱代理</td>
                <td>
                    
                </td>
                <td class="yellow">付款方式</td>
                <td colspan="2">
                    <?= $shipment['paymenttermcode'];?>
                </td>
                <td class="blue">付款地点</td>
                <td>
                    
                </td>
                <td colspan="6">
                    
                </td>
            </tr>
            <tr>
                <td class="yellow">船代</td>
                <td>
                    <?= $shipment['shipagent'];?>
                </td>
            </tr>
            <tr>
                <td colspan="14"> </td>
            </tr>
            <tr>
                <td rowspan="6" class="blue">发货人</td>
                <td class="blue">代码</td>
                <td colspan="12">
                    <?= $shipment['shippercode'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">名称</td>
                <td colspan="12">
                    <?= $shipment['shippername'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">地址</td>
                <td colspan="12">
                    <?= $shipment['shipperaddress'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">国家代码</td>
                <td colspan="12">
                    <?= $shipment['shippercountry'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">电话</td>
                <td colspan="12">
                    <?= $shipment['shippertele'];?>
                </td>
            </tr>
            <tr>
                <td class="blue">AEO企业编码</td>
                <td colspan="12">
                    <?= $shipment['shipperAEO'];?>
                </td>
            </tr>
            <tr>
                <td rowspan="8" class="blue">收货人</td>
                <td class="blue">代码</td>
                <td colspan="12">
                    <?= $shipment['consigneecode'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">名称</td>
                <td colspan="12">
                    <?= $shipment['consigneename'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">地址</td>
                <td colspan="12">
                    <?= $shipment['consigneeaddress'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">国家代码</td>
                <td colspan="12">
                    <?= $shipment['consigneecountry'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">电话</td>
                <td colspan="12">
                    <?= $shipment['consigneetele'];?>
                </td>
            </tr>
            <tr>
                <td class="blue">AEO企业编码</td>
                <td colspan="12">
                    <?= $shipment['consigneeAEO'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">具体联系人</td>
                <td colspan="12">
                    <?= $shipment['consigneeperson'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">联系人电话</td>
                <td colspan="12">
                    <?= $shipment['consigneepersontele'];?>
                </td>
            </tr>
            <tr>
                <td rowspan="6" class="blue">通知人</td>
                <td class="blue">代码</td>
                <td colspan="12">
                    <?= $shipment['notifycode'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">名称</td>
                <td colspan="12">
                    <?= $shipment['notifyname'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">地址</td>
                <td colspan="12">
                    <?= $shipment['notifyaddress'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">国家代码</td>
                <td colspan="12">
                    <?= $shipment['notifycountry'];?>
                </td>
            </tr>
            <tr>
                <td class="yellow">电话</td>
                <td colspan="12">
                    <?= $shipment['notifytele'];?>
                </td>
            </tr>
            <tr>
                <td class="blue ">AEO企业编码</td>
                <td colspan="12">
                    <?= $shipment['notifyAEO'];?>
                </td>
            </tr>
            <tr>
                <td colspan="14"> </td>
            </tr>
            <tr>
                <td colspan="14" style="text-align:center;" class="blue">明细品名及数据</td>
            </tr>
            <tr>
                <td class="yellow">箱号</td>
                <td class="yellow">封号</td>
                <td class="yellow">箱型</td>
                <td class="yellow">货主箱标记</td>
                <td class="yellow">英文品名</td>
                <td class="yellow">Hscode</td>
                <td class="yellow">件数</td>
                <td class="yellow">包装单位</td>
                <td class="yellow">毛重(KGS)</td>
                <td class="yellow">体积(CBM)</td>
                <td class="yellow">唛头</td>
                <td class="yellow">UN Code(危)</td>
                <td class="yellow">类别(危)</td>
            </tr>
            <?php foreach($shipment['containers'] as $container){ ?>
            <tr>
                <td><?= $container['containerno'];?></td>
                <td><?= $container['sealno'];?></td>
                <td><?= $container['containertype'];?></td>
                <td></td>
                <td><?= $container['description'];?></td>
                <td><?= $container['hscode'];?></td>
                <td><?= $container['containernoofpkgs'];?></td>
                <td><?= $container['containerpackaging'];?></td>
                <td><?= $container['containergrossweight'];?></td>
                <td><?= $container['containercbm'];?></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php } ?>
        </table>
        <?php } ?>
    </div>
</body>

