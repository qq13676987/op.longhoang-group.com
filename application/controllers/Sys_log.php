<?php


class Sys_log extends Menu_Controller
{
    protected $menu_name = '';//菜单权限对应的名称
    protected $method_check = array(//需要设置权限的方法
        'index',
        'add_data',
        'update_data',
        'delete_data',
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sys_log_model');
        $this->model = $this->sys_log_model;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "insert_time") continue;
            array_push($this->field_edit, $v);
        }
    }


    public $field_edit = array();

    public $field_all = array(
        array('id', 'id', '60', '1', 'sortable:true'),
        array('table_name', 'table_name', '130', '3', ''),
        array('key', 'order number', '100', '4', ''),
        array('action', 'action', '60', '5', ''),
        array('value', 'value', '800', '6', "formatter:value_for"),
        array('user_id', 'user_id', '100', '6', ''),
        array('insert_time', 'insert_time', '150', '8', 'sortable:true'),
    );

    public function index(){
        $data = array();
        $table_name = getValue('table_name', '');
        $key_no = getValue('key_no', '');
        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('sys_log_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }
        // page account
        $rs = $this->sys_config_model->get_one('sys_log_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }
        $data['table_name'] = $table_name;
        $data['key_no'] = $key_no;
        $data['child_table_name'] = $this->sys_log_model->get_table_name($table_name, $key_no);
        $this->load->view('head');
        $this->load->view('sys/log/index_view', $data);
    }

    public function get_data($table_name = '', $key_no = ''){
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $master_table_name = isset($_POST['master_table_name']) ? strval($_POST['master_table_name']) : '';
        $master_key = isset($_POST['master_key']) ? (int)$_POST['master_key'] : 0;
        setLang($table_name);

        //-------where -------------------------------
        $where = array();

        $where[] = "`table_name` ='$table_name'";
        if($key_no != 'all')$where[] = "`key` = '$key_no'";
        if($master_table_name != '' && $master_key != ''){
            $where[] = "master_table_name = '$master_table_name' and master_key = '$master_key'";
        }
        
        $where = join(' and ', $where);
        //-----------------------------------------------------------------
        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $this->db->select('*, (select name from bsc_user where id = user_id) as user_name');
        $rs = $this->model->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            $row['user_id'] = $row['user_name'];
            $row['value_json'] = $row['value'];
            $row['value'] = json_decode($row['value'], true);
            //将key替换
            $value = array();
            foreach ($row['value'] as $k => $r){
                // if(!isset($_COOKIE['class_name'])){
                //     setcookie('class_name',$row['table_name'],time() + 1000, '/');
                // }else{
                //     if($_COOKIE['class_name'] != $row['table_name']){
                //         setcookie('class_name',$row['table_name'],time() + 1000, '/');
                //     }
                // }
                $value[lang($k)] = $r;
            }
            $row['value'] = $value;
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }
    
    /**
     * 恢复数据
     * @param int $id
     */
    public function rollback_log($id = 0){
        $log = $this->model->get_by_id($id);
        if(empty($log)){
            echo json_encode(array('code' => 1, 'msg' => '日志不存在'));
            return;
        }
        $model_name = $log['table_name'] . '_model';
        $this->load->model($model_name);
        //获取主键名称
        $this->load->model('m_model');
        $sql = "SELECT k.column_name FROM information_schema.table_constraints t JOIN information_schema.key_column_usage k " .
            "USING (constraint_name,table_schema,table_name) WHERE t.constraint_type='PRIMARY KEY' " .
            "AND t.table_schema='op_china' AND t.table_name='{$log['table_name']}'";//op_china
        $rs = $this->m_model->sqlquery($sql);
        $row = $rs->row_array();
        $data = $this->$model_name->get_one($row['column_name'], $log['key']);


        $json_data = json_decode($log['value'], true);
        if(isset($json_data[$row['column_name']]))unset($json_data[$row['column_name']]);
        if(empty($json_data)){
            echo json_encode(array('code' => 2, 'msg' => '无可恢复数据'));
            return;
        }

        if(!empty($data)){//如果数据存在,可执行update insert 回滚
            $actions = array('update', 'insert');
            if(!in_array($log['action'], $actions)){
                echo json_encode(array('code' => 3, 'msg' => '该数据暂不支持恢复'));
                return;
            }
            $this->$model_name->update($data['id'], $json_data);
            $a = lastquery();
            // record the log
            $log_data = array();
            $log_data["table_name"] = $log['table_name'];
            $log_data["key"] = $log['key'];
            $log_data["action"] = "update_rollback";
            $log_data["value"] = json_encode($json_data);
            log_rcd($log_data);
        }else{
            $actions = array('insert', 'delete');
            if(!in_array($log['action'], $actions)){
                echo json_encode(array('code' => 3, 'msg' => '该数据暂不支持恢复'));
                return;
            }
            $json_data[$row['column_name']] = $log['key'];
            if($model_name == 'biz_quotation_model'){
                $this->$model_name->insert($json_data);
            }else{
                $this->$model_name->save($json_data);
            }
            $a = lastquery();

            // record the log
            $log_data = array();
            $log_data["table_name"] = $log['table_name'];
            $log_data["key"] = $log['key'];
            $log_data["action"] = "insert_rollback";
            $log_data["value"] = json_encode($json_data);
            log_rcd($log_data);
        }
        echo json_encode(array('code' => 0, 'msg' => '回滚成功', $a));
    }
    
    public function bind_shipment_log(){
        $data = array();
        
        $consol_id = getValue('id', '');
        $this->db->select('`key`, insert_time');
        $shipment_bind_log = $this->model->get("table_name = 'biz_shipment' and master_table_name = 'biz_consol' and master_key = '$consol_id' and (action = 'bind_update' or action = 'apply_update')", 'id', 'desc');
        
        $shipment_bind_log_date = array_column($shipment_bind_log, 'insert_time', 'key');
        $shipment_ids = array_keys($shipment_bind_log_date);
        if(empty($shipment_ids)){
            echo '暂无';
            return;
        }
        
        //绑定日期，job no ,销售，操作，客服
        Model('m_model');
        $sql = "select id,job_no,client_company,(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'operator') as operator_id,(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'sales') as sales_id,(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'customer_service') as customer_service_id from biz_shipment where biz_shipment.id in (" . join(',', $shipment_ids) . ") ";
        $rs = $this->m_model->query_array($sql);
        $data['rows'] = array();
        
        //提前获取所有相关人员名称
        $user_ids = filter_unique_array(array_merge(array_column($rs, 'operator_id'), array_column($rs, 'sales_id'), array_column($rs, 'customer_service_id')));
        if(empty($user_ids)) $user_ids[] = 0;
        
        Model('bsc_user_model');
        $this->db->select('id, name');
        $users = array_column($this->bsc_user_model->get("id in (" . join(',', $user_ids) . ")"), null, 'id');
        
        foreach ($rs as $row){
            $row['insert_time'] = $shipment_bind_log_date[$row['id']];
            $row['operator_name'] = isset($users[$row['operator_id']]) ? $users[$row['operator_id']]['name'] : '';
            $row['sales_name'] = isset($users[$row['sales_id']]) ? $users[$row['sales_id']]['name'] : '';
            $row['customer_service_name'] = isset($users[$row['customer_service_id']]) ? $users[$row['customer_service_id']]['name'] : '';
            
            $data['rows'][] = $row;
        }
        
        $this->load->view('head');
        $this->load->view('/sys/log/consol/bind_shipment_log', $data);
    }
    public function bill_split_log($split_id=-1){
        $this->load->model("m_model");
        $sql = "select * from sys_log where id <(
SELECT id FROM `op_china`.`sys_log` WHERE `key` = '$split_id' AND `table_name` = 'biz_bill' AND `action` = 'split_update' LIMIT 0, 1000
) and `key` = '$split_id' AND `table_name` = 'biz_bill' order by id desc limit 1";
        $row = $this->m_model->query_one($sql);
        $value = $row["value"];
        $value = json_decode($value,true);
        if(!empty($value["amount"])) $original_amount = $value["amount"];
        echo "original amount: $original_amount <br>";
        $sql = "select * from biz_bill where split_id = $split_id";
        $rows = $this->m_model->query_array($sql);
        foreach($rows as $k=>$row){
            //print_r($row);
            $k++;
            $amount = $row["amount"];
            echo "split amount $k: $amount <br>";
        }
        
    }
}
