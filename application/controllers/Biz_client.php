<?php

class Biz_client extends Menu_Controller
{

    public function __construct()
    {
        parent::__construct();
        Model('biz_client_model');
        Model('bsc_user_model');
        $this->field_all = $this->biz_client_model->field_all;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }

    public $field_edit = array();

    public function index($role = "")
    {
        // exit("revising..");
        $data = array();

        $data['client_level'] = getValue('level', '');

        $data["role"] = $role;
        $data['hasDelete'] = false;

        if (isset($_GET['import'])) {
            $data['import'] = 'import';
        } else {
            $data['import'] = '';
        }
        $role = $this->db->where('catalog','role')->get('bsc_dict')->result_array();
        $data['client'] = $data['supplier'] = [];
        foreach ($role as $v){
            if($v['ext1'] == 1) array_push($data['client'],$v['value']);
            if($v['ext1'] == 2) array_push($data['supplier'],$v['value']);
        }
        $this->load->view('head');
        if ($data["import"] == "") {
            $this->load->view('biz/client/index_view', $data);
        } else {
            $table_name = "biz_client_index";
            $this->load->model('sys_config_model');
            $rs = $this->sys_config_model->get_one($table_name.'_table');
            if (!empty($rs['config_name'])) {
                $data["f"] = json_decode($rs['config_text']);
            } else {
                $this->load->model('sys_config_title_model');
                $data["f"] = $this->sys_config_title_model->get_one($table_name);
            }


            // page account
            $rs = $this->sys_config_model->get_one('biz_client_table_row_num');
            if (!empty($rs['config_name'])) {
                $data["n"] = $rs['config_text'];
            } else {
                $data["n"] = "30";
            }

            $userId = $this->session->userdata('id');
            $userBsc = $this->bsc_user_model->get_by_id($userId);

            $this->load->model('bsc_user_role_model');

            $userRole = $this->bsc_user_role_model->get_user_role(array('delete_text'));
            $userBsc = array_merge($userBsc, $userRole);

            $userBscDelete = explode(",", (isset($userBsc['delete_text']) ? $userBsc['delete_text'] : ''));

            $data["hasDelete"] = in_array("client", $userBscDelete);

            foreach ($data['f'] as $k => $v) {
                if ($v[0] == "role") unset($data['f'][$k]);
            }
            //循环保留需要的
            // foreach ($data['f'] as $k => $v){
            // if($v[0]!='client_code' && $v[0]!='client_name' && $v[0]!='company_name'&&$v[0]!='sales_ids'&&$v[0]!='customer_service_ids'&&$v[0]!='sailing_area'&&$v[0]!='company_address'){
            //     unset($data['f'][$k]);
            // }
            // }
            $this->load->view('biz/client/index_crm_import', $data);
        }
    }
    
    /**
     * 新增基础库的信息
     */
    public function add_basic(){
        $data = array();
        
        $this->load->view('head');
        $this->load->view('biz/client/add_basic_view', $data);
    }
    
    /**
     * 请求聚翰的接口生成对用的basic库数据
     */
    public function add_basic_data(){
        $url = "http://china.leagueshipping.com/other/add_basic_data";
        $company_name_en = postValue('company_name_en');
        //如果获取到的用户名在系统里,已存在,那么就不生成
        $old_row = Model('biz_client_model')->get_one('company_search_en', match_chinese($company_name_en));
        if(!empty($old_row)){
            return jsonEcho(array('code' => 1, 'msg' => lang('该公司已存在')));
        }
        
        $post_data = array(
            'company_name_en' => $company_name_en
        );
        echo curl_post_body($url, array(), http_build_query($post_data));
    }

    public function add($id = 0)
    {
        $userId = get_session('id');
        $field = $this->field_edit;
        $data = array();    
        foreach ($field as $item) {
            $data[$item] = "";
        }
        //2023-08-31 新加入了 默认带上code 和 客户名称参数
        $data['client_code'] = $client_code = postValue('client_code', '');
        $data['company_name_en']= $company_name_en = postValue('company_name_en', '');
        
        $data['id'] = '';
        if ($id != 0) {
            $data = $this->biz_client_model->get_one('id', $id);
            //加一个client权限相关的数据显示
            $client_duty = Model('biz_client_duty_model')->get_client_data_one($data['client_code']);
            $data = array_merge($data, $client_duty);
        }
        $data['client_level'] = 1;
        $data['temp_client_code'] = "A" . $userId . date('YmdHis');
        //判断只是销售
        $data['is_sales'] = false;
        if (is_admin()) $data['is_sales'] = true;
        $this->load->view('head');
        $this->load->view('biz/client/add_view', $data);
    }
    
    public function add_data($company_type = "")
    {
        $field = $this->field_edit;
        $data = array();
        
        //  20220913 删除必填项： , 'bank_name_cny', 'bank_account_cny', 'bank_name_usd', 'bank_account_usd', 'taxpayer_name', 'taxpayer_id', 'taxpayer_address', 'taxpayer_telephone'
        $required_field = array('client_code', 'country_code', 'client_name', 'company_name', 'company_name_en', 'company_address', 'deliver_info', 'role');
        
        foreach ($field as $item) {
             if(isset($_POST[$item])){
                $temp = $_POST[$item];
                //数组不处理

                if(is_string($temp)) $temp = ts_replace(trim($temp));//如果是字符串,进行字符替换和去除两边空格
                
                //如果是必填字段,那么不能为空
                if(in_array($item, $required_field) && empty($temp)) return jsonEcho(array('code' => 1, 'msg' => lang($item) . " 不能为空"));
                //判断日期字段，如果小于2000年就置空
                $data[$item] = $temp;
            }
            // $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            // $data[$item] = is_string($temp) ? ts_replace(trim($temp)) : $temp;
        }
        $data['company_type'] = $company_type;
        if(isset($data['sales_id']) && $data['sales_id'] == ''){
            $data['sales_id'] = 20001;
        }
        
        $temp_client_code = postValue('temp_client_code', '');
        
        $result = array('code' => 1, 'msg' => '参数错误');
        
        $old_row = $this->biz_client_model->get_one('company_name_en', $data['company_name_en']);
        if(!empty($old_row)){
            $result['msg'] = lang('该公司已存在');
            echo json_encode($result);
            return;
        }
        $old_row = $this->biz_client_model->get_one('client_code', $data['client_code']);
        if(!empty($old_row)){
            $result['msg'] = lang('该公司代码已存在');
            echo json_encode($result);
            return;
        }
        $company_name = isset($data['company_name']) ? $data['company_name'] : $old_row['company_name'];
        if($check_company_role = check_company_role($company_name, $data['role'])){
             $result['msg'] = $check_company_role;
            echo json_encode($result);
            return;
        }
        
        //根据全称和国家代码生成对应的code
        // $this->load->library('Pinyin');
        // $pingying = new Pinyin();

        // //获取新的code
        // //国家代码规则
        // $company_name = trim(match_chinese($data['company_name']));
        // $data['company_search'] = $company_name;
        // $data['company_search_en'] = match_chinese($data['company_name_en']);
        // $prefix = '';
        // if($data['country_code'] !== 'CN'){
        //     $prefix .= $data['country_code'];
        // }

        // if(preg_match("/[\x7f-\xff]/", $company_name)){
        //     //1、CN直接 前6个字首字母 + 编号
        //     $prefix .= substr($pingying->get(' '. $company_name, true, true), 0 , 6 - strlen($prefix));
        // }else{
        //     //2、CN以外, 根据空格切割 国家代码 + 字母缩写 + 编号
        //     //2.1、如果数组长度<=1 取前4个字母
        //     $company_name = explode(' ', $data['company_name']);
        //     //2.2、如果数组长度>1 取每个单词的首字母
        //     foreach ($company_name as $val){
        //         if(strlen($prefix) == 6) break;
        //         $prefix .= substr(strtoupper(match_chinese($val)), 0, 1);
        //     }
        // }
        // var_dump($prefix);
        // return;
        // $user = $this->biz_client_model->get_last_code($prefix);
        // if(empty($user)){
        //     $client_code = $prefix . '01';
        // }else{
        //     $num = str_pad((int)substr($user['client_code'], -2, 2) + 1, 2, '0', STR_PAD_LEFT);
        //     $client_code = $prefix . $num;
        // }
        // $data['client_code'] = $client_code;

        $data['company_search'] = match_chinese($data['company_name']);
        $data['company_search_en'] = match_chinese($data['company_name_en']);
        
        $data['finance_payment'] = 'after work';
        //contact数据单独处理,将ID作为client_code
        $contact_datas = isset($_POST['contact']) ? $_POST['contact'] : array();
        $add_contact_datas = array();
        if(!empty($contact_datas)){
            Model('biz_client_contact_model');
            foreach ($contact_datas as $contact_data){
                //此处执行新增contact操作
                $contact_name = isset($contact_data['name']) ? trim($contact_data['name']) : '';
                $contact_email = isset($contact_data['email']) ? trim($contact_data['email']) : '';
                // $contact_telephone = isset($contact_data['telephone']) ? trim($contact_data['telephone']) : '';
                if(isset($contact_data['position']) && is_array($contact_data['position'])) $contact_data['position'] = join(',', $contact_data['position']);

                if($contact_name == '') continue;
                // //查询手机号是否存在,存在不给新增
                // $this->db->select('id');
                // $telephone_row = $this->biz_client_contact_model->get_where_one("telephone = '{$contact_telephone}'");
                // if(!empty($telephone_row)){
                //     $result['msg'] = '手机号' . $contact_telephone . '已存在,添加失败';
                //     echo json_encode($result);
                //     return;
                // }
                //判断邮箱是否有效
                // if(isset($contact_data['email']) && !check_mail_valid($contact_email)){
                //     $result['msg'] = '邮箱' . $contact_email . '不是有效邮箱,添加失败,若确认邮箱有效可手动确认有效后，再保存';
                //     echo json_encode($result);
                //     return;
                // }
                $add_contact_datas[] = $contact_data;
            }
        }
        // if(empty($add_contact_datas)){
        //     $result['msg'] = '未填写有效联系人, 请重新填写联系人信息';
        //     echo json_encode($result);
        //     return;
        // }
        //判断是否添加了关联人员
        $role_arr = filter_unique_array(explode(',', $data['role']));
        $client_role_config = Model("biz_client_duty_model")->client_role_config;
        foreach ($role_arr as $raval){
            //检测当前规则下,必填项是否有设置人员
            if(!isset($client_role_config[$raval])) continue;
            $client_duty = array_column(Model("biz_client_duty_model")->get("client_code = '{$temp_client_code}' and client_role = '{$raval}'"), null, 'user_role');
            $this_role_config = $client_role_config[$raval];
            foreach ($this_role_config[2] as $urval){
                //如果没有配置该角色的,那么返回提示
                if(!isset($client_duty[$urval])) return jsonEcho(array('code' => 2, 'msg' => lang('未配置当前客户的{role}角色的{user_role}人员', array('role' => $raval, 'user_role' => $urval)), 'data' => array('role' => $raval)));
            }
        }
        
        //2022-09-20 存在供应商角色时,引进人必填
        //假如角色是 booking_agent,truck,carrier,warehouse,declaration，则read_group设置为all
        $rd_all_arr = array("booking_agent", "truck", "carrier", "warehouse", "declaration", "other");
        $is_supplier = false;
        foreach ($rd_all_arr as $r) {
            if (in_array($r, $role_arr)) {
                $is_supplier = true;
                // if(empty($_POST['supplier_admin_ids'])) return jsonEcho(array('code' => 1, 'msg' => "引进人必填!!!!!(引进人是 角色 上面的框)"));
            }
        }

        $id = $this->biz_client_model->save($data);
        
        if($id > 0){
            //2022.9.13 xu add
            // $is_supplier && $this->add_client_duty($temp_client_code, (int)$_POST['supplier_admin_ids']);
            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_client";
            $log_data["key"] = $id;
            $log_data["action"] = "insert";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
            
             //先把当前这个审核的权限人员移动过去--start
            Model('biz_client_duty_model')->client_code_update($temp_client_code, $data['client_code']);
            //先把当前这个审核的权限人员移动过去--end
            
            foreach($add_contact_datas as $add_contact_data){
                $add_contact_data['client_code'] = $data['client_code'];
                $this_contact_id = $this->biz_client_contact_model->save($add_contact_data);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_client_contact";
                $log_data["key"] = $this_contact_id;
                $log_data["action"] = "insert";
                $log_data["value"] = json_encode($add_contact_data);
                log_rcd($log_data);
            }
            
           
            $data['id'] = $id;
            $result['code'] = 0;
            $result['msg'] = '保存成功';
            $result['data']['id'] = $id;
        }else{
            $result['msg'] = '保存失败';
        }
        echo json_encode($result);
    }
    
    /**
     * @title 供应商引进人
     * @param string $client_code
     * @param int $user_id
     * @return bool
     */
    private function add_client_duty($client_code = '', $user_id = 0){
        if (!$client_code || !$user_id) return false;
        $user = $this->db->select('id, group')->where('id', $user_id)->get('bsc_user')->row_array();
        if (empty($user)) return false;

        $data = [
            'user_id'    => $user['id'],
            'user_group'    => $user['group'],
            'client_code'    => $client_code,
            'client_role'    => '-1',
            'user_role'    => 'supplier_admin'
        ];

        $where = array(
            'client_code'       => $client_code,
            'user_role'         => 'supplier_admin',
            'client_role'       => '-1',
        );

        $count = $this->db->select('count(*) as count')->where($where)->get('biz_client_duty')->row_array()['count'];
        if ($count > 0){
            $query = $this->db->where($where)->update('biz_client_duty', $data);
        }else{
            $query = $this->db->insert('biz_client_duty', $data);
        }
    }

    public function edit($id = 0)
    {
        //如果输入CODE什么的这里直接重新跳转
        if (!is_numeric($id) && strlen($id) > 3) {
            $client = $this->biz_client_model->get_one('client_code', $id);
            if (!empty($client)) $id = $client["id"];
            echo "<script>location.href='/biz_client/edit/$id';</script>";
        }
        $field = $this->field_edit;
        $client = $this->biz_client_model->get_one('id', $id);
        if (empty($client)) {
            echo lang('客户不存在');
            return;
        }
        $data = $client;

        //这里将数据重新处理下
        foreach ($field as $item) {
            if (!isset($data[$item])) $data[$item] = "";
            if (isset($data[$item]) && ($data[$item] == '0000-00-00' || $data[$item] == '0000-00-00 00:00:00')) $data[$item] = null;
        }

        //加一个client权限相关的数据显示
        $client_duty = Model('biz_client_duty_model')->get_client_data_one($data['client_code']);
        $data = array_merge($data, $client_duty);

        if (!empty($data)) client_read_role($data);
        $userId = get_session('id');
        $userBsc = $this->bsc_user_model->get_by_id($userId);

        //获取当前的查看权限和修改权限
        $userRole = Model('bsc_user_role_model')->get_user_role(array('read_text', 'edit_text'));
        $userBsc = array_merge($userBsc, $userRole);

        //页面传假值
        $userBsc['edit_text'] = isset($_POST['edit_text']) ? $_POST['edit_text'] : $userBsc['edit_text'];
        $userBsc['read_text'] = isset($_POST['read_text']) ? $_POST['read_text'] : $userBsc['read_text'];

        $data1 = array();
        $g_read_table = false; //全表读
        $g_edit_table = false; //全表可写
        foreach ($data as $key => $value) {
            $data1[$key] = '***';
        }

        $userBscRead = explode(",", (isset($userBsc['read_text']) ? $userBsc['read_text'] : ''));
        foreach ($userBscRead as $row) {
            if (strpos($row, 'client.') !== false) {
                $arr_temp = explode(".", $row);
                $key = $arr_temp[1];
                $data1[$key] = empty($data[$key]) ? '' : $data[$key];
            } else if ($row == 'client') {
                // 全表
                $g_read_table = true;
                break;
            } else if (is_admin()) {
                // 全表
                $g_read_table = true;
                break;
            }
        }
        if ($g_read_table) {
            $data1 = $data;
        }
        $data1["id"] = $id;
        $data1["client_level"] = $data['client_level'];
        foreach ($client_duty as $key => $val){
            $data1[$key] = $val;
        }

        $userBscEdit1 = explode(",", (isset($userBsc['edit_text']) ? $userBsc['edit_text'] : ''));
        $userBscEdit = array();
        foreach ($userBscEdit1 as $row) {
            if (strpos($row, 'client.') !== false) {
                $arr_temp = explode(".", $row);
                array_push($userBscEdit, $arr_temp[1]);
            } else if ($row == 'client' || $this->session->userdata('level') == '9999') {
                // 全表
                $g_edit_table = true;
                break;
            }
        }
        if ($g_edit_table) {
            $userBscEdit = array_keys($data);
        }

        $data1['r_e'] = false;
        $data1['G_userBscEdit'] = $userBscEdit;
        if (isset($_POST['edit_text']) || isset($_POST['read_text'])) $data1['r_e'] = true;
        //控制是否可以看到read_user和read_user_group
        $data1['read_config'] = in_array($userId, explode(',', $data1['sales_ids'])) || $this->session->userdata('level') == '9999' ? true : false;
        //判断是不是管理员，如果管理员显示管理人的修改
        $data1['is_sales'] = false;
        $supplier_admin = Model('biz_client_duty_model')->get_where_one("client_code = '{$data['client_code']}' and user_role = 'supplier_admin'");
        if(empty($supplier_admin)) $supplier_admin['user_id'] = 0;
        if (is_admin() || $supplier_admin['user_id'] == get_session('id')) $data1['is_sales'] = true;
        //2022-06-28 新增tabs
        $data1['tab_title'] = isset($_GET['tab_title']) ? $_GET['tab_title'] : '';

        //2022-08-22 显示最后合作日期
        $temp_row = Model('m_model')->query_one("select booking_ETD from biz_shipment where biz_shipment.client_code = '{$data['client_code']}' order by booking_ETD desc");
        $data1['last_deal_time'] = !empty($temp_row) ? $temp_row['booking_ETD'] : '暂未合作';

        $code=$data1['client_code'];
        $nowTime = date('Y-m-d',time());
        $sql="select contract_start,contract_expiry,id,party_b from biz_client_contract_list where client_code = '$code' and contract_expiry > '{$nowTime}';";
        $query=$this->db->query($sql);
        $rows = $query->result_array();
        $data1['contract']=$rows;

        if(!empty($rows)){
            $data1['contract_start']= $rows[0]['contract_start'];
            $data1['contract_expiry']= $rows[0]['contract_expiry'];
        }else{
            $data1['contract_start']= '';
            $data1['contract_expiry']= '';
        }
        $where = array(
            'client_code'       => $data1['client_code'],
            'user_role'         => 'supplier_admin',
            'client_role'       => '-1',
        );
        $user = $this->db->select('*')->where($where)->get('biz_client_duty')->row_array();
        $userid = isset($user['user_id']) ? (int)$user['user_id'] : '';
        $this->load->vars(array('user_id'=>$userid));

        $data1['parent_company'] = "";

        $this->load->view('head');
        $this->load->view('biz/client/edit_view', $data1);
    }
    
    public function update_data($id = 0)
    {
        if ($id == 0) $id = $_REQUEST["id"];
        $old_row = $this->biz_client_model->get_by_id($id);

        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            if (isset($_POST[$item])) {
                $temp = $_POST[$item];
                if (array_key_exists($item, $old_row) && $old_row[$item] != $temp) $data[$item] = trim($temp);
                //地址不特殊处理
                if (in_array($item, array('company_address'))) continue;
                if (isset($data[$item]) && is_string($data[$item])) $data[$item] = ts_replace($data[$item]);
            }

        }
		if(!empty($_POST['finance_belong_company_code']) && !empty($_POST['dn_accounts'])){
			$this->load->model('bsc_overseas_account_model');
			$finance_belong_company_code = str_replace(',',"','",$_POST['finance_belong_company_code']);
			$overseas_account = $this->bsc_overseas_account_model->get("company_code in ('$finance_belong_company_code')");
			$dn_accounts = array_filter(explode(',',$_POST['dn_accounts']));
			if(count(array_intersect(array_column($overseas_account,'id'),$dn_accounts)) != count($dn_accounts)){
				echo '<meta charset="utf-8">';
				echo "<script>alert('" . lang('归属公司和银行账号不匹配')."')</script>";
				redirect('biz_client/edit/'.$id, 'refresh');
				return;
			}
		}
        if (isset($data['company_name'])) $data['company_search'] = match_chinese($data['company_name']);
        if (isset($data['company_name_en'])) $data['company_search_en'] = match_chinese($data['company_name_en']);

        //2021-12-1 判断当前客户角色是否与联系人相关的角色相等,不相等时,不能为正式客户
        $this->load->model('biz_client_contact_model');
        $this_role = isset($data['role']) ? $data['role'] : $old_row['role'];
        $this_client_level = isset($data['client_level']) ? $data['client_level'] : $old_row['client_level'];
        $client_contact_role = array_column($this->biz_client_contact_model->get_option($old_row['client_code']), 'role');
        $unique_contact_role = filter_unique_array(explode(',', join(',', $client_contact_role)));
        $this_role_arr = filter_unique_array(explode(',', $this_role));
        
        $client_code = $old_row['client_code'];
        $client_role_config = Model("biz_client_duty_model")->client_role_config;
        foreach ($this_role_arr as $raval){
            //检测当前规则下,必填项是否有设置人员
            if(!isset($client_role_config[$raval])) continue;
            $client_duty = array_column(Model("biz_client_duty_model")->get("client_code = '{$client_code}' and client_role = '{$raval}'"), null, 'user_role');
            $this_role_config = $client_role_config[$raval];
            foreach ($this_role_config[2] as $urval){
                //因为这里默认有sales,所以直接跳过
                if($urval == 'sales') continue;
                //如果没有配置该角色的,那么返回提示
                if(!isset($client_duty[$urval]))
                {
                    echo '<meta charset="utf-8">';
                    echo "<script>alert('" . lang('未配置当前客户的{role}角色的{user_role}人员', array('role' => $raval, 'user_role' => $urval)) . "');</script>";
                    redirect('biz_client/edit/'.$id, 'refresh');
                    return;
                }
                //return jsonEcho(array('code' => 2, 'msg' => "未配置当前客户的{$raval}角色的{$urval}人员", 'data' => array('role' => $raval)));
            }
        }
        //这里将差集获取到,然后提示应该添加这些角色的联系人
        $company_name = isset($data['company_name']) ? $data['company_name'] : $old_row['company_name'];
        if ($check_company_role = check_company_role($company_name, $this_role)) {
            echo '<meta charset="utf-8">';
//            echo "<script>alert('{$check_company_role}');history.go(-1);</script>";
            echo "<script>alert('{$check_company_role}');</script>";
            redirect('biz_client/edit/'.$id, 'refresh');
            return;
        }

//         $diff_role = array_diff($this_role_arr, $unique_contact_role);
//         //2021-12-06 修改为只判断当前client的role数大于contact
//         if ($this_client_level === '1' && sizeof($diff_role) > 0) {
//             echo '<meta charset="utf-8">';
//             echo "<script>alert('检测到当前客户的联系人信息只存在" . join(',', $unique_contact_role) . "联系人, 请添加角色" . join(',', $diff_role) . "等的相关联系人后再试');</script>";
//             redirect('biz_client/edit/'.$id, 'refresh');
// //            echo "<script>alert('请添加角色" . join(',', $diff_role) . "等的相关联系人后再试');history.go(-1);</script>";
//             return;
//         }

        //新增客户联系人-start
        if (isset($data['contact_name'], $data['contact_telephone'])) {
            $this->db->where('client_code', $old_row['client_code']);
            $client_contact = $this->biz_client_contact_model->get_one('name', $data['contact_name']);
            if (empty($client_contact) && $data['contact_telephone'] != '') {
                $contact = array();
                $contact['name'] = $_POST['contact_name'];
                $contact['live_chat'] = $_POST['contact_live_chat'];
                $contact['telephone'] = $_POST['contact_telephone'];
                $contact['telephone2'] = $_POST['contact_telephone2'];
                $contact['email'] = $_POST['contact_email'];
                $contact['client_code'] = $_POST['client_code'];
                $contact['position'] = $_POST['contact_position'];
                $contact_id = $this->biz_client_contact_model->save($contact);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_client_contact_list";
                $log_data["key"] = $contact_id;
                $log_data["action"] = "insert";
                $log_data["value"] = json_encode($contact);
                log_rcd($log_data);
            }

        }
        
        //新增客户联系人-end

        //2021-05-31 新增停用日期功能，到达时间自动改为潜在
        if (isset($data['stop_date'])) {
            if (empty($data['stop_date'])) $data['stop_date'] = '0000-00-00 00:00:00';
            // die_queue(6, 'biz_client', $id);
            // $result_data = array();
            // if($data['stop_date'] != '0000-00-00 00:00:00'){//停止任务
            //     save_queue(6, 'biz_client', $id, $result_data, $data['stop_date']);
            // }
        }
        client_role_duty($old_row['client_code'], $this_role);
        $id = $this->biz_client_model->update($id, $data);
        if (isset($data["read_user_group"])) {
            $read_user_group = $data["read_user_group"];
        } else {
            $read_user_group = $old_row["read_user_group"];
        }
        //2022.9.13 xu add
        // $this->add_client_duty($old_row['client_code'], (int)$_POST['supplier_admin_ids']);

        $data['id'] = $id;
        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_client";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);


        echo '<meta charset="utf-8">';
        echo "<script>alert('保存成功');try{window.opener.$('#tt').edatagrid('reload');}catch (e) {} window.location.href='/biz_client/edit/$id'; </script>";
    }

    //新表头配置版本的获取数据
    public function get_data_v2(){
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';


        //-------这个查询条件想修改成通用的 -------------------------------
        $f_role = isset($_REQUEST['f_role']) ? $_REQUEST['f_role'] : '';
        $quit_sales = isset($_REQUEST['quit_sales']) ? $_REQUEST['quit_sales'] : '';
        $whzts = postValue('whzts', '');//未合作天数
        $is_cooperate = postValue('is_cooperate', ''); //已合作
        $crm_user_type = postValue('crm_user_type', ''); //已合作
        $crm_user = postValue('crm_user', ''); //已合作
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $client_level = getValue('level', '');
        $import = getValue('import', '');

        $where = array();
        $join_more = array();

        //新增查询条件 2022-05-26
        $client_name = isset($_GET['client_name']) ? $_GET['client_name'] : '';

        //加入权限的获取 加在这里是因为下面的异常需要在这里添加额外条件
        $role_where = $this->biz_client_model->read_role_version2(false);

        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_client', 'biz_client_index');//获取相关的配置信息
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }

                if ($f == 'biz_client.company_name') {
                    $f = 'biz_client.company_search';
                    $v = match_chinese($v);
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];


                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') $v = '';
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) $title_data['sql_join'][$title_data['base_data'][$title_f]['sql_join']] = $this_join;

                    $where[] = search_like($f, $s, $v);
                }
            }
        }

        if ($client_level != "") $where[] = "biz_client.client_level = '$client_level'";
        if ($f_role != '') {
            if (!is_array($f_role)) $f_role = array($f_role);
            $where_or = array();
            foreach ($f_role as $i_role) {
                $where_or[] = "FIND_IN_SET('{$i_role}', biz_client.role)";
            }
            $where[] = "(" . join(" or ", $where_or) . ")";
        }
        if ($quit_sales != '') {
            if (!is_array($quit_sales)) $quit_sales = array($quit_sales);
            $quit_sales = filter_unique_array($quit_sales);
            if (!empty($quit_sales)) $join_more[] = "biz_client_duty.user_role = 'sales' and biz_client_duty.user_id in ('" . join('\',\'', $quit_sales) . "')";
        }

        //2022-06-01 新增未合作天数查询
        if ($whzts !== '') $where[] = "(select biz_shipment.booking_ETD from biz_shipment where biz_shipment.client_code = biz_client.client_code order by booking_ETD desc limit 1) <= '" . date('Y-m-d', strtotime("-{$whzts} day")) . "'";
        if ($is_cooperate === 'true') $where[] = "(select biz_shipment.booking_ETD from biz_shipment where biz_shipment.client_code = biz_client.client_code order by booking_ETD desc limit 1) is not null";

        // 原本查sales_ids ,改为查权限表sales,且user_id等于当前用户权限
        if ($import != "" && !is_admin()) {
            $join_more[] = "biz_client_duty.user_role = 'sales' and biz_client_duty.user_id in (" . join(",", get_session("user_range")) . ")";
            $where[] = "biz_client.is_client=1 and biz_client.is_supplier=0";
        }

        //加入权限条件
        !empty($role_where) && $join_more[] = '(' . join(' or ', $role_where) . ')';

        //根据当前的相关duty 查询获取where语句
        $where[] = $this->biz_client_model->get_read_role_where($join_more);

        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;

        $this->db->limit($rows, $offset);
        //这里已经加了索引,查询速度快了
//        $this->db->select('biz_client.*,(select booking_ETD from biz_shipment where biz_shipment.client_code = biz_client.client_code order by booking_ETD desc limit 1) as last_deal_time', false);
//        获取需要的字段--start
        $selects = array(
            'biz_client.id as id',
            'biz_client.role as role',
        );
        $must_field = array('id', 'role');
        foreach ($title_data['select_field'] as $key => $val){
            if($key == 'biz_client.client_level_remark_id') isset($title_data['base_select_field']['biz_client.client_level_remark']) && $selects[] = $title_data['base_select_field']['biz_client.client_level_remark'];
            if(!in_array($val, $selects)) $selects[] = $val;
        }
        //获取需要的字段--end
        $this->db->select(join(',', $selects), false);
        //这里拼接join数据
        foreach ($title_data['sql_join'] as $j){
            $this->db->join($j[0], $j[1], $j[2]);
        }//将order字段替换为 对应的sql字段
        $sort = $title_data['sql_field']["biz_client.{$sort}"];
        $this->db->is_reset_select(false);//不重置查询
        $rs = $this->biz_client_model->no_role_get($where, $sort, $order);
        $result[] = lastquery();

        $this->db->clear_limit();//清理下limit
        $result["total"] = $this->db->count_all_results('');

//        $client_duty = Model('biz_client_duty_model')->get_client_data(array_column($rs, 'client_code'));
//        foreach ($rs as $key => $row){
//            isset($client_duty[$row['client_code']]) && $rs[$key] = array_merge($rs[$key], $client_duty[$row['client_code']]);
//        }
//
//        //这里将所有用户ID相关的东西整理到一起统一查询
//        $user_ids_temp = array_merge(array_column($rs, 'created_by'), array_column($rs, 'updated_by'), array_column($rs, 'sales_ids'), array_column($rs, 'customer_service_ids'));
//        $user_ids = filter_unique_array(explode(',', join(',', $user_ids_temp)));
//        $users = array();
//        if (!empty($user_ids)) {
//            Model('bsc_user_model');
//            $this->db->select('id, name');
//            $users = array_column($this->bsc_user_model->get("id in (" . join(',', $user_ids) . ")"), null, 'id');
//        }

        $rows = array();
        $userRole = Model('bsc_user_role_model')->get_data_role();
        foreach ($rs as $row) {
//            if (isset($users[$row['created_by']])) $row['created_by'] = $users[$row['created_by']]['name'];
//            if (isset($users[$row['updated_by']])) $row['updated_by'] = $users[$row['updated_by']]['name'];
//            //批量替换销售和客服的
//            $row['sales_ids'] = join(',', array_map(function ($r) use ($users) {
//                if (isset($users[$r])) return $users[$r]['name'];
//                else return $r;
//            }, explode(',', $row['sales_ids'])));
//
//
//            $row['customer_service_ids'] = join(',', array_map(function ($r) use ($users) {
//                if (isset($users[$r])) return $users[$r]['name'];
//                else return $r;
//            }, explode(',', $row['customer_service_ids'])));
//            if($row["client_level"]==-1){
//                //2022-08-08 黑名单备注 保存
//                if($row['client_level_remark_id']=='1'){
//                    $row['remark']='业务冲突';
//                }else if($row['client_level_remark_id']=='2'){
//                    $row['remark']='运费有未付：'.$row["client_level_remark"];
//                } else if($row['client_level_remark_id']=='3'){
//                    $row['remark']='备注：'.$row["client_level_remark"];
//                }else{
//                    $row['remark']=$row["client_level_remark"];
//                }
//            }
            $row = data_role($row, $must_field, explode(',', $userRole['read_text']), 'biz_client', 'biz_client_index');
            array_push($rows, $row);
        }
        $result["code"] = 0;
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    /**
     * @param string $role
     * @param int $type
     * 2022-03-30 加入分页功能
     */
    public function get_option($role = "", $type = 0)
    {
		//特殊补丁， 当 role = carrier的时候，改为从字典表取值
		if($role=='carrier'){
			$rs = $this->db->query("SELECT `value` as client_code, `name` as client_name FROM `bsc_dict` where catalog = 'carrier' order by orderby asc")->result_array();
			array_unshift($rs,['client_code'=>' ','client_name'=>'-','company_name'=>'-','company_name_en'=>'-']);
			echo json_encode($rs);
			return;
		}
        $q = isset($_POST['q']) ? (str_replace("'","\'",$_POST['q'])) : '';
        $q_field = isset($_GET['q_field']) ? check_param($_GET['q_field']) : 'company_name';
        $client_code = isset($_POST['client_code']) ? check_param($_POST['client_code']) : '';
        $company_name = isset($_POST['company_name']) ? ($_POST['company_name']) : '';
        $company_name_or = isset($_POST['company_name_or']) ? ($_POST['company_name_or']) : '';
        $clientIds = isset($_GET['clientIds']) ? $_GET['clientIds'] : '';
        $other_role = isset($_GET['other_role']) ? explode(',', check_param($_GET['other_role'])) : array();
        $bill_type = getValue('bill_type', '');
        $limit = getValue('limit', '3000');
        $rows = array();
        $where = array();
        $select = array();

        $other_role[] = $role;

        //拼接当前需要查询的角色--start
        $role_or_where = array();
        if ($role !== '0'){//role为0代表全查
            foreach ($other_role as $client_role) {
                // $role_or_where[] = "FIND_IN_SET('{$client_role}', biz_client.role)"; //这条是用client进行查询的,可能会导致client角色也能看到全开
                $role_or_where[] = "biz_client_duty.client_role = '{$client_role}'";//这条是从duty表进行查询,权限单独配置
            }
            // $where[] = "(" . join(' or ', $role_or_where) . ")";
        }
        //拼接当前需要查询的角色--end

        //数据中加入强行拼接在后面的客户--start
        $client_level_where = array();
        if ($clientIds != '') {
            $client_level_where[] = "(biz_client.id in ('" . join("','", explode(',', $clientIds)) . "') or client_level = 1)";
        } else {
            $client_level_where[] = 'biz_client.client_level = 1';
        }
        if ($type == 1) $client_level_where[] = 'biz_client.client_level = 0'; //费用输入允许潜在的抬头
        $where[] = "(" . join(' or ', $client_level_where) . ")";
        //数据中加入强行拼接在后面的客户--end

        if ($q != '') $where[] = "biz_client.{$q_field} like '%$q%'";
        if ($client_code != '') $where[] = "biz_client.client_code like '%$client_code%'";
        if ($company_name != '') $where[] = "biz_client.company_name like '%$company_name%'";
        if ($company_name_or != '') $where[] = "(biz_client.company_name like '%{$company_name_or}%' or biz_client.company_name_en like '%{$company_name_or}%')";
        if ($bill_type == 'sell') {
            //如果是应收，不能出现纯供应商抬头
            $client_roles = array('client', 'factory', 'logistics_client', 'oversea_client');

            $role_or_where = array();
            foreach ($client_roles as $client_role) {
                $role_or_where[] = "FIND_IN_SET('{$client_role}', biz_client.role)";
            }
            // $where[] = "(" . join(' or ', $role_or_where) . ")";
        }
        //这里新增一个只能显示申请正式的
        if (!in_array('booking_agent', $other_role)) {
            $where[] = "(biz_client.client_code not in (select distinct client_code from biz_client_crm where apply_status>0) or biz_client.is_supplier = 1)";
        }

        if ($type == 1) {//费用输入专用
            $select = array('biz_client.id','biz_client.client_code','biz_client.company_name','biz_client.company_name_en');
            $limit = '';
        }
        //如果有q_field,那么限制显示前50条
        if(isset($_GET['q_field'])){
            $limit = 50;
        }

        //加入权限
        $read_role_where = array();
        //加入权限
        $role_where = $this->biz_client_model->read_role_version2();
        if(empty($role_or_where)) $role_or_where[] = "1 = 1";
        if($role_where != '') $read_role_where[] = "(" . join(' or ', $role_or_where) . ")" . " and " . $role_where;
        else $read_role_where[] = "(" . join(' or ', $role_or_where) . ")";
        $where[] = $this->biz_client_model->get_read_role_where($read_role_where);

        $where_str = join(' and ', $where);

        if ($limit != '') $this->db->limit($limit);
        if(empty($select)) $select = array('biz_client.*', "(select email_verify from biz_client_contact_list where biz_client_contact_list.client_code = biz_client.client_code and biz_client_contact_list.name = biz_client.contact_name and biz_client_contact_list.telephone = biz_client.contact_telephone limit 1) as email_verify");
        $this->db->select(join(',', $select), false);
        $rs = Model('biz_client_model')->no_role_get($where_str);

        $field_edit = $this->field_edit;

        //2022-06-30 新增去除-
        $not = isset($_GET['not']) ? $_GET['not'] : '';
        if ($not != 'not') {
            $null_array = array();
            foreach ($field_edit as $val) {
                //2021-08-25 因为微信小熊爱吃软糖询问 承运人选错后，无法清空，所以这里从空值改为了空格，允许选择空的
                $null_array[$val] = ' ';
            }
            $null_array['client_code'] = ' ';
            $null_array['client_name'] = '-';
            $null_array['company_name'] = '-';
            $null_array['company_name_en'] = '-';
            $rows[] = $null_array;
        }

        //2022-09-01 添加tag的显示,但是需要开启参数才行
        $is_tag = getValue('is_tag', 'false');
        $tag_arr = array();
        if($is_tag === 'true'){
            $rs_ids = filter_unique_array(array_column($rs, 'id'));
            $this->db->select("group_concat(biz_tag.tag_name) as tag_names,biz_tag_relation.id_no");
            $this->db->join("biz_tag", "biz_tag.id = biz_tag_relation.tag_id");
            $this->db->group_by("biz_tag_relation.id_no");
            $tag_arr = array_column(Model('biz_tag_relation_model')->get("biz_tag_relation.id_type = 'biz_client' and biz_tag_relation.id_no in ('" . join("','", $rs_ids) . "')", 'biz_tag_relation.id'), null, 'id_no');
        }

        foreach ($rs as $row) {
            //判断是否过期
            if (in_array('client', $other_role)) { //$client_role == 'client'
                //先转化role
                $rs_role = explode(',', $row['role']);
                //判断是否代理权限
                $row['stop_bool'] = true;
                //in_array('booking_agent',$rs_role)||
                if (in_array('oversea_agent', $rs_role)) {
                    if ($row['stop_date'] != '0000-00-00 00:00:00') {
                        $time = time();
                        $stop_time = strtotime($row['stop_date']);
                        //当前小于过期时间，就通行true,反之不过
                        if ($time < $stop_time) {
                            $row['stop_bool'] = true;
                        } else {
                            $row['stop_bool'] = false;
                        }

                    }
                }
            }
            $row['email_verify'] = 1;
            $row['tag'] = isset($tag_arr[$row['id']]) ? $tag_arr[$row['id']]['tag_names'] : '';
            array_push($rows, $row);
        }

        echo json_encode($rows);
    }

    public function get_client()
    {
        $q = isset($_POST['q']) ? ($_POST['q']) : '';
        $q_field = isset($_GET['q_field']) ? check_param($_GET['q_field']) : 'company_name';
        $client_code = postValue('client_code', '');
        $company_name = postValue('company_name', '');

        $where = array();
        if ($q != '') $where[] = "$q_field like '%$q%'";
        if ($client_code != '') $where[] = "client_code like '%$client_code%'";
        if ($company_name != '') $where[] = "company_name like '%$company_name%'";
        $where = join(' AND ', $where);
        if (empty($where)) return jsonEcho(array());

        $this->db->limit(10);
        $rs = $this->biz_client_model->no_role_get($where);
        $data = array();

        $finance_sign_company_codes = filter_unique_array(explode(',', join(',', array_column($rs, 'finance_sign_company_code'))));

        $this->db->select('company_code, company_name, company_title');
        $finance_sign_company_datas = array_column(Model('biz_sub_company_model')->get("company_code in ('" . join('\',\'', $finance_sign_company_codes) . "')"), null, 'company_code');
        foreach ($rs as $row) {
            //这里填充 finance_sign_company_data
            $row['finance_sign_company_data'] = array_map(function ($r) use ($finance_sign_company_datas) {
                return array('company_code' => $r, 'company_name' => isset($finance_sign_company_datas[$r]) ? $finance_sign_company_datas[$r]['company_name'] : $r,'company_title' => isset($finance_sign_company_datas[$r]) ? $finance_sign_company_datas[$r]['company_title'] : $r);
            }, explode(',', $row['finance_sign_company_code']));

            $data[] = $row;
        }

        return jsonEcho($data);
    }

    /**
     * 获取当前账单相关的抬头
     * 2021-05-21 新加入将bill里已存在的账单客户默认填入
     * 2021-12-31 http://wenda.leagueshipping.com/?/question/688 默认数据里新增当前客户的相关受票单位
     * 2022-04-07 这里加入限制,如果是获取的cost的话,限制只能获取非纯粹供应商抬头的
     * @param string $id_type
     * @param string $id_no
     */
    public function get_bill_client($id_type = '', $id_no = '')
    {
        $type = getValue('type', '');
        $client_codes = array();

        $client_where = array();
        if ($id_type == 'consol') {
            $row = Model('biz_consol_model')->get_where_one("id = '{$id_no}'");
            $client_codes[] = $row['agent_code'];
            $client_codes[] = $row['creditor'];
            $client_codes[] = $row['trans_carrier'];
            $client_codes[] = $row['warehouse_code'];

            $this->db->select('id,client_code');
            $bills = Model('biz_bill_model')->not_role_get("id_type = 'consol' and id_no = '{$id_no}'");
            foreach ($bills as $bill) {
                if (!in_array($bill['client_code'], $client_codes)) $client_codes[] = $bill['client_code'];
            }
        } else if ($id_type == 'shipment') {
            $row = Model('biz_shipment_model')->get_where_one("id = '{$id_no}'");

            $client_codes[] = $row['client_code'];
            $client_codes[] = $row['client_code2'];
            $client_codes[] = $row['trans_carrier'];
            $client_codes[] = $row['warehouse_code'];
            $client_codes[] = $row['agent_code'];

            //2021-12-31 根据当前shipment的 client_code 查询关联,
            // $this->db->select('relation_client_code');
            // $rs = Model('biz_client_second_relation_model')->get_client_second_relation($row['client_code']);

            // $client_codes = array_merge($client_codes, array_column($rs, 'relation_client_code'));

            $consol = Model('biz_consol_model')->get_one('id', $row['consol_id']);

            if (!empty($consol)) {
                $client_codes[] = $consol['agent_code'];
                $client_codes[] = $consol['creditor'];
                $client_codes[] = $consol['trans_carrier'];
                $client_codes[] = $consol['warehouse_code'];
            }


            $truck = Model('biz_shipment_truck_model')->get("shipment_id = $id_no");

            $client_codes = array_merge($client_codes, array_column($truck, 'truck_client_code'));

            $this->db->select('id,client_code');
            $bills = Model('biz_bill_model')->not_role_get("id_type = 'shipment' and id_no = $id_no");
            foreach ($bills as $bill) {
                if (!in_array($bill['client_code'], $client_codes)) $client_codes[] = $bill['client_code'];
            }
        }else if($id_type == 'kml'){
            //kml的sell是固定聚翰和精心
            //cost固定kml
            if($type == 'sell'){
                $client_codes[] = "SHJHCW02";//聚翰
                $client_codes[] = "SHJXGY01";//精心
            }else{
                $client_codes[] = "JXHY01";//KML
            }
        }
        $client_codes_str = join("','", filter_unique_array($client_codes));

        $client_where[] = "biz_client.client_code in ('$client_codes_str')";
        //2022-04-07 这里加入限制,如果是获取的cost的话,限制只能获取非纯粹供应商抬头的
        if ($type == 'sell' && $id_type != 'kml') {
            //下面是相关的供应商类
            $client_roles = array('client', 'factory', 'logistics_client', 'oversea_client');

            $role_or_where = array();
            foreach ($client_roles as $client_role) {
                $role_or_where[] = "FIND_IN_SET('{$client_role}', biz_client.role)";
            }
            $client_where[] = "(" . join(' or ', $role_or_where) . ")";
        }

        $client_where_str = join(' and ', $client_where);

        $clients = $this->biz_client_model->no_role_get($client_where_str);

        echo json_encode($clients);
    }

    public function get_agent()
    {
        $company_name = isset($_POST['company_name']) ? $_POST['company_name'] : '';
        $country_code = isset($_POST['country_code']) ? $_POST['country_code'] : '';
        $sales_ids = isset($_GET['sales_ids']) ? $_GET['sales_ids'] : '00000';
        $userId = $this->session->userdata('id');
        $rows = array();
        $where = array();
        $where[] = 'client_level = 1';
        if ($company_name !== '') $where[] = "company_search like '%" . match_chinese($company_name) . "%'";
        if ($country_code !== '') $where[] = "country_code like '%$country_code%'";
        //"role like '$val,%' or role like '%,$val,%' or role like '%,$val' or role = '$val'";
        //A + 该销售下的B

        // (sales_ids like '%,$sales_ids,%' or sales_ids like '%,$sales_ids' or sales_ids like '$sales_ids,%' or sales_ids = '$sales_ids')
        //2022-09-23 这里由于原本的duty的改版,也改为了新版的查询
        $sales_where = array();
        $sales_ids_arr = explode(',', $sales_ids);
        $this->db->select("client_code");
        $this->db->group_by('client_code');
        $oversea_clients = array_column(Model('biz_client_duty_model')->get("client_role = 'oversea_client' and user_id in ('" . join('\',\'', $sales_ids_arr) . "')"), 'client_code');
        // if (!empty($sales_ids_arr)) {
        //     foreach ($sales_ids_arr as $val) {
        //         $sales_where[] = "sales_ids like '%,$val,%' or sales_ids like '%,$val' or sales_ids like '$val,%' or sales_ids = '$val'";
        //     }
        // } else {
        //     $sales_where[] = 'sales_ids = \'00000\'';
        // }
        $sales_where = '( ' . join(' or ', $sales_where) . ' )';
        $where[] = "(( role like 'oversea_agent,%' or role like '%,oversea_agent,%' or role like '%,oversea_agent' or role = 'oversea_agent' )" .
            " or ( " .
            " (role like 'oversea_client,%' or role like '%,oversea_client,%' or role like '%,oversea_client' or role = 'oversea_client') " .
            " and client_code in ('" . join('\',\'', $oversea_clients) . "')" .
            // "and " . $sales_where
            "))";

        $where = join(' and ', $where);
        $rs = $this->biz_client_model->no_role_get($where);

        $client_duty = Model('biz_client_duty_model')->get_client_data(array_column($rs, 'client_code'));
        foreach ($rs as $key => $row){
            isset($client_duty[$row['client_code']]) && $rs[$key] = array_merge($rs[$key], $client_duty[$row['client_code']]);
        }
        // echo lastquery();exit();

        foreach ($rs as $row) {
            //先转化role
            $rs_role = explode(',', $row['role']);
            //判断是否代理权限
            $row['stop_bool'] = true;
            if (in_array('oversea_agent', $rs_role)) {
                if ($row['stop_date'] != '0000-00-00 00:00:00') {
                    $time = time();
                    $stop_time = strtotime($row['stop_date']);
                    //当前小于过期时间，就通行true,反之不过
                    if ($time < $stop_time) {
                        $row['stop_bool'] = true;
                    } else {
                        $row['stop_bool'] = false;
                    }

                }
            }
            array_push($rows, $row);
        }
        //2022-07-01 新增
        // $new_rows = array();
        // $trans_carrier = isset($_POST['trans_carrier']) ? $_POST['trans_carrier'] : '';
        // $trans_destination = isset($_POST['trans_destination']) ? $_POST['trans_destination'] : '';
        // if (empty($trans_carrier) && empty($trans_destination)) { 
        //     $new_rows = $rows;
        // } else {
        //     foreach ($rows as $k => $v){
        //         $carrier_agent=$this->db->where('carrier_code',$trans_carrier)->where('oversea_agent_code',$v['client_code'])->get('biz_carrier_agent')->row_array();
        //         if(!empty($carrier_agent)){ 
        //             $destination=explode(',',$carrier_agent['destination_inner_use']);
        //             if(in_array($trans_destination,$destination)){
        //                 $new_rows[] = $v;
        //             }
        //         }
        //     } 
        // }

        // echo json_encode($new_rows);
        echo json_encode($rows);
    }

    /**
     * 2022-07-07 consol的获取agent逻辑更改,只根据 biz_carrier_agent库来获取
     */
    public function get_consol_agent(){
        $trans_carrier = postValue('trans_carrier', '');
        $trans_destination = postValue('trans_destination', '');
        $company_name = postValue('company_name', '');
        $country_code = postValue('country_code', '');

        $where = array();

        $where[] = "biz_carrier_agent.carrier_code = '{$trans_carrier}'";
        $where[] = "FIND_IN_SET('{$trans_destination}', biz_carrier_agent.destination_inner_use)";
        if ($company_name !== '') $where[] = "biz_client.company_search like '%" . match_chinese($company_name) . "%'";
        if ($country_code !== '') $where[] = "biz_client.country_code like '%$country_code%'";

        $where_str = join(' and ', $where);

        $rs = $this->db->select('biz_client.*')->join('biz_client', "biz_client.client_code = biz_carrier_agent.oversea_agent_code", 'inner')->where($where_str, null, false)->group_by('biz_carrier_agent.oversea_agent_code')->get('biz_carrier_agent')->result_array();
        $rows = array();
        foreach ($rs as $row) {
            //先转化role
            $rs_role = explode(',', $row['role']);
            //判断是否代理权限
            $row['stop_bool'] = true;
            if (in_array('oversea_agent', $rs_role)) {
                if ($row['stop_date'] != '0000-00-00 00:00:00') {
                    $time = time();
                    $stop_time = strtotime($row['stop_date']);
                    //当前小于过期时间，就通行true,反之不过
                    if ($time < $stop_time) {
                        $row['stop_bool'] = true;
                    } else {
                        $row['stop_bool'] = false;
                    }

                }
            }
            array_push($rows, $row);
        }

        echo json_encode($rows);
    }

    public function table_tree($type = 'read')
    {
        $data = array();
        $this->field_edit = array();
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            if (isset($row[4]) && !empty($row[4])) {
                $this->field_edit[$row[4]][] = $v;
            } else {
                array_push($this->field_edit, $v);
            }
        }
        $temp = array();
        foreach ($this->field_edit as $key => $val) {
            if (is_array($val)) {
                $cd_temp = array();
                foreach ($val as $v) {
                    array_push($cd_temp, array("id" => "1$type." . $v, "text" => lang($v)));
                }
                array_push($temp, array('id' => "1$type." . $key, 'text' => lang($key), 'children' => $cd_temp, 'not_add' => 1));
            } else {
                array_push($temp, array("id" => "1$type." . $val, "text" => lang($val)));
            }
        }
        array_push($data, array("id" => "1$type", "text" => lang("$type"), "children" => $temp));
        echo json_encode($data);
    }

    /**
     * 配置client里的销售与其他角色关系的页面
     */
    public function sales_relation()
    {
        $client_code = getValue('client_code', '');
        $rs = Model('biz_client_duty_model')->get_client_data_one($client_code);
        $rs['client_code'] = $client_code;
        $data = $rs;

        //销售单独传过去作为表头
        $data['sales_ids'] = explode(',', $rs['sales_ids']);

        //其他人员合并到一个二维数组里
        $data['rows'] = array();

        //将所有用户ID集合起来,一起查询user表
        $user_ids = filter_unique_array(array_merge(explode(',', $rs['operator_ids']), explode(',', $rs['customer_service_ids']), explode(',', $rs['sales_ids'])
            , explode(',', $rs['marketing_ids']), explode(',', $rs['oversea_cus_ids']), explode(',', $rs['booking_ids'])
            , explode(',', $rs['document_ids'])));
        Model('bsc_user_model');
        if (empty($user_ids)) $user_ids[] = 0;
        $this->db->select('id,name');
        $users = array_column($this->bsc_user_model->get("id in (" . join(',', $user_ids) . ")"), 'name', 'id');

        //查询当前客户相关的销售关联,
        $biz_client_sales_op_model = Model('m_model');
        $biz_client_sales_op_model::$table = 'biz_client_sales_op';


        $role = array('operator', 'customer_service', 'marketing', 'oversea_cus', 'booking', 'document');
        foreach ($role as $val) {
            //取出指定角色人员,去重去空
            $this_role_ids_arr = filter_unique_array(explode(',', $data[$val . '_ids']));
            foreach ($this_role_ids_arr as $this_role_id) {
                $this_user_name = isset($users[$this_role_id]) ? $users[$this_role_id] : '';
                //判断sales_op里是否存在,
                $sales = array();
                foreach ($data['sales_ids'] as $sales_id) {
                    $checked = '';
                    $this->db->select('id');
                    $this_sales_op = $biz_client_sales_op_model->get_one_where("sales_id = $sales_id and client_code = '{$rs['client_code']}' and role = '$val' and user_id = $this_role_id");
                    if (!empty($this_sales_op)) $checked = 'checked';
                    $sales[] = array('id' => $sales_id, 'checked' => $checked);
                }


                $data['rows'][] = array('id' => $this_role_id, 'name' => $this_user_name, 'role' => $val, 'sales' => $sales);
            }
        }

        $this->load->view('head');
        $this->load->view('/biz/client/sales_relation', $data);
    }
    
     /**
     * 保存client里销售的关联关系
     */
    public function save_sales_relation_data()
    {
        //这里如果销售取消了在client里的关联,数据咋办,不处理?
        $sales_id = (int)postValue('sales_id', 0);
        $client_code = postValue('client_code', '');
        $role = postValue('role', '');
        $user_id = (int)postValue('user_id', 0);
        $checked = (bool)postValue('checked', false);

        $result = array('code' => 1, 'msg' => lang('参数错误'));

        if (empty($sales_id) || empty($client_code) || empty($role) || empty($user_id)) {
            return jsonEcho($result);
        }

        //查询当前对应的数据
        $biz_client_sales_op_model = Model('m_model');
        $biz_client_sales_op_model::$table = 'biz_client_sales_op';

        $sales_op = $biz_client_sales_op_model->get_one_where("sales_id = $sales_id and client_code = '$client_code' and role = '$role' and user_id = $user_id");

        $data = array();
        $data['sales_id'] = $sales_id;
        $data['client_code'] = $client_code;
        $data['role'] = $role;
        $data['user_id'] = $user_id;
        if ($checked) {
            //选中
            //无数据新增
            if (empty($sales_op)) $biz_client_sales_op_model->save($data);
            //如果有数据,不处理
        } else {
            //取消选中
            //有数据删除
            if (!empty($sales_op)) $biz_client_sales_op_model->mdelete($sales_op['id']);
            //无数据不处理
        }
        $result[] = $sales_op;
        $result[] = $checked;
        $result['code'] = 0;
        $result['msg'] = lang('保存成功');
        return jsonEcho($result);
    }
    
        public function client_sales()
    {
        $data = array();

        $where = array();


        $company_name = isset($_GET['company_name']) ? $_GET['company_name'] : '';
        if (!empty($company_name)) {
            $company_name_arr = explode(' ', match_chinese($company_name));
            foreach ($company_name_arr as $val) {
                $where[] = "company_search like '%" . $val . "%'";
            }
        } else {
            $where[] = "id = 0";
        }

        $where = join(' and ', $where);
        $this->db->select('client_code,company_name, sales_ids');
        $total = $this->biz_client_model->no_role_total($where);
        Model('m_model');
        if ($total == 0) {
            $rows = array(array('company_name' => '不存在匹配值', 'sales_ids_names' => '', 'not_traded_days' => '', 'tag' => ''));
        } else {
            $rs = $this->db->where($where)->order_by('id', 'desc')->limit(10, 0)->get('biz_client')->result_array();

            $rs_ids = filter_unique_array(array_column($rs, 'id'));
            $this->db->select("group_concat(biz_tag.tag_name) as tag_names,biz_tag_relation.id_no");
            $this->db->join("biz_tag", "biz_tag.id = biz_tag_relation.tag_id");
            $this->db->group_by("biz_tag_relation.id_no");
            //显示操作类标签
            $tag_arr = array_column(Model('biz_tag_relation_model')->get("biz_tag.tag_class= '操作类' and biz_tag_relation.id_type = 'biz_client' and biz_tag_relation.id_no in ('" . join("','", $rs_ids) . "')", 'biz_tag_relation.id'), null, 'id_no');

            $rows = array();
            foreach ($rs as $row) {
                $row['sales_ids_names'] = '';

                $sql = "select id,booking_ETD from biz_shipment where client_code = '{$row['client_code']}' and status = 'normal' order by booking_ETD desc";
                $last_shipment = $this->m_model->query_one($sql);
                //新增未合作天数 查询最后一笔normal的单子
                if (empty($last_shipment)) {
                    $row['not_traded_days'] = '未进行过交易';
                } else {
                    $booking_ETD = $last_shipment['booking_ETD'];
                    $diff_day = round((time() - strtotime($booking_ETD)) / (3600 * 24));//相差天数
                    if ($diff_day <= 10) {
                        $row['not_traded_days'] = '10天内有交易';
                    } else {
                        $row['not_traded_days'] = $diff_day . '天前有交易';
                    }
                }

                //从duty表中取出当前客户的所有销售人员，拼接成字符串输出
                $this->db->select('bsc_user.name')->where(array('biz_client_duty.client_code' => $row['client_code'], 'biz_client_duty.user_role' => 'sales'));
                $this->db->join('bsc_user', 'bsc_user.id = biz_client_duty.user_id', 'LEFT');
                $users = $this->db->get('biz_client_duty')->result_array();
                if($users) $row['sales_ids_names'] = join(',', array_column($users, 'name'));

                $row['tag'] = isset($tag_arr[$row['id']]) ? $tag_arr[$row['id']]['tag_names'] : '';
                $rows[] = $row;
            }
        }

        $data['rows'] = $rows;
        $data['company_name'] = $company_name;
        $this->load->vars('total', $total);
        $this->load->view("/biz/client/client_sales_view", $data);
    }

    /**
     * 查询是否存在
     */
    public function search()
    {
        $company_name = isset($_GET['company_name']) ? strtoupper(match_chinese($_GET['company_name'])) : '';
        $id = isset($_GET['id']) ? (int)$_GET['id'] : 0;
        if ($company_name === '') {
            echo json_encode(array('code' => 1, 'msg' => '未找到'));
            return;
        }

        $this->db->select('company_name, company_search, client_level, client_code');//company_search
        $clients = $this->biz_client_model->no_role_get('id != ' . $id);

        //将固定字符串全部去除--start 去除 “有限公司”、“公司”、   COMPANY、 COLTD
        $this->replace_strs = array('有限', '公司', '责任', '国际', '物流', '船务', '代理', '服务', 'COMPANY', 'COLTD', '上海', '宁波', '常州', '安徽', '杭州', '江苏', '苏州', '绍兴', '广东', '青岛');
        foreach ($this->replace_strs as $replace_str) {
            $company_name = str_replace($replace_str, '', $company_name);
        }
        //将固定字符串全部去除--end
        $this->company_name = $company_name;

        //过滤相似度低于60%的
        $this->similarity = 0;
        array_walk($clients, function (&$row) {
            $row['company_search'] = strtoupper($row['company_search']);
            foreach ($this->replace_strs as $replace_str) {
                $row['company_search'] = str_replace($replace_str, '', $row['company_search']);
            }

            similar_text($this->company_name, $row['company_search'], $percent);
            $row['percent'] = round($percent, 2);
        });
        $filter_clients = array_filter($clients, function ($row) {
            //比较相似度
            if ($row['percent'] >= $this->similarity) {
                return true;
            }
        });
        if (!$filter_clients) {
            echo json_encode(array('code' => 1, 'msg' => '未找到'));
            return;
        }
        //根据相似度排序
        $filter_clients_percents = array_column($filter_clients, 'percent');
        array_multisort($filter_clients_percents, SORT_DESC, $filter_clients);

        echo json_encode(array('code' => 0, 'msg' => '查找成功', 'data' => $filter_clients[0]));
    }
}
