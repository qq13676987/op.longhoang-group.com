<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<style>
    table {
        width: 100%;
        border-collapse: collapse;
    }

    table caption {
        font-size: 2em;
        font-weight: bold;
        margin: 1em 0;
    }

    th, td {
        border: 1px solid #808695;
        text-align: center;
        padding: 10px 0;
    }

    table thead tr {
        background-color: #009688;
        color: #fff;
        font-size: 13px;
        font-weight: bold;
    }

    table tbody tr:nth-child(odd) {
        background-color: #eee;
    }

    table tbody tr:hover {
        background-color: #F0FFFF       ;
    }

    table tbody tr td:first-child {
        color: #009688;
        font-weight: bold;
        font-size: 13px;
    }

    table tfoot tr td {
        text-align: right;
        padding-right: 20px;
    }

    div::-webkit-scrollbar {
        /*滚动条整体样式*/
        width: 5px; /*高宽分别对应横竖滚动条的尺寸*/
        height: 1px;
    }

    div::-webkit-scrollbar-thumb {
        /*滚动条里面小方块*/

        box-shadow: inset 0 0 5px rgba(0, 0, 0, 0.2);
        background: #95B8E7;
    }

    div::-webkit-scrollbar-track {
        /*滚动条里面轨道*/
        box-shadow: inset 0 0 5px rgba(0, 0, 0, 0.2);
        background: #ededed;
    }
</style>
<body>
<h1>目标设置</h1>
<div style="padding-top: 10px;padding-bottom: 10px;line-height:30px;height:30px">
    <sapn style="font-size: 1rem;color:#79a1d7;font-weight: bold;vertical-align: middle;">请选择月份</sapn>
    <input name="date" id="date" autocomplete="off" value="<?=$date?>" onclick="javascript:WdatePicker({startDate:'%y-%M',dateFmt:'yyyy-MM'});"  style="width:150px;border: 1px solid #95B8E7;line-height: 20px;height:20px;border-radius: 5px;outline: none;margin:0px 10px">
    <button onclick="selected()" style="height:20px;line-height: 16px;vertical-align: middle;">确认</button>
</div>
<div style="height:670px;overflow:scroll;padding:1px;border:1px solid #79a1d7;width:98%;">
    <table >
        <thead>
        <tr>
            <th width="20%" rowspan="2">销售部门</th>
            <th rowspan="2" style="background: #FF5722;width:10%">目标月份</th>
            <th colspan="3" style="background: #5FB878">月目标</th>
<!--            <th colspan="3" style="background: #ff9900">年目标</th>-->
            <th width="10%" rowspan="2">操作</th>
        </tr>
        <tr>
            <th style="background: #5FB878;width:10%">crm客户数</th>
            <th style="background: #5FB878;width:10%">新增合作客户数</th>
            <th style="background: #5FB878;width:10%">订单量</th>
           <!-- <th style="background: #ff9900;width:10%">crm客户数</th>
            <th style="background: #ff9900;width:10%">新增合作客户数</th>
            <th style="background: #ff9900;width:10%">订单量</th>-->
        </tr>
        </thead>
        <tbody>
        <?php if (empty($arr)): ?>
            <tr>
                <td colspan="9">请先选择月份暂无数据</td>
            </tr>
        <?php else: ?>
            <?php foreach ($arr as $k => $v): ?>
                <tr>
                    <td>
                       <?= $v['target_name_cn'] ?>
                    </td>
                    <td style="color:#FF5722;font-weight: bold;font-size: 13px;">
                        <?= $v['start_date'] ?>
                    </td>
                    <td>
                        <input id="num1_month_<?=$k?>" type="number" value="<?= $v['crm_num_month'] ?>" style="border:none;outline:none;background:none;color:#5FB878;font-weight: bold;font-size: 15px;text-align: center;width:95%" min="1">
                    </td>
                    <td>
                        <input id="num2_month_<?=$k?>" type="number" value="<?= $v['client_num_month'] ?>" style="border:none;outline:none;background:none;color:#5FB878;font-weight: bold;font-size: 15px;text-align: center;width:95%" min="1">
                    </td>
                    <td>
                        <input id="num3_month_<?=$k?>" type="number" value="<?= $v['shipment_num_month'] ?>" style="border:none;outline:none;background:none;color:#5FB878;font-weight: bold;font-size: 15px;text-align: center;width:95%" min="1">
                    </td>
                    <!--<td style="color:#ff9900;font-weight: bold">
                        <input id="num1_year_<?/*=$k*/?>" type="number" value="<?/*= $v['crm_num_year'] */?>" style="border:none;outline:none;background:none;color:#5FB878;font-weight: bold;font-size: 15px;text-align: center;width:95%" min="1">
                    </td>
                    <td style="color:#ff9900;font-weight: bold">
                        <input id="num2_year_<?/*=$k*/?>" type="number" value="<?/*= $v['client_num_year'] */?>" style="border:none;outline:none;background:none;color:#5FB878;font-weight: bold;font-size: 15px;text-align: center;width:95%" min="1">
                    </td>
                    <td style="color:#ff9900;font-weight: bold">
                        <input id="num3_year_<?/*=$k*/?>" type="number" value="<?/*= $v['shipment_num_year'] */?>" style="border:none;outline:none;background:none;color:#5FB878;font-weight: bold;font-size: 15px;text-align: center;width:95%" min="1">
                    </td>-->
                    <td>
                        <?php if($v['button']):?>
                            <a style="color: #1E9FFF;font-size: 13px;cursor:pointer" onclick="save('<?=$k?>','<?= $v['target_name'] ?>','<?= $v['start_date'] ?>')">保存设置</a>
                        <?php else:?>
                            <a style="color: #1E9FFF;font-size: 13px;cursor:pointer">暂无设置权限</a>
                        <?php endif;?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
</div>

</body>
<script>
    function selected() {
        var date = $('#date').val();
        if (date == '') {
            alert('请选择月份');
            return;
        }
        window.location.href = '/biz_crm/crm_set_num_user?group_code=<?= $group_code ?>&date=' + date;
    }

    function save(num,target_name,start_date){

        //获取设置数据
        var crm_num_month =$('#num1_month_'+num).val();
        var client_num_month =$('#num2_month_'+num).val();
        var shipment_num_month =$('#num3_month_'+num).val();

        /*var crm_num_year =$('#num1_year_'+num).val();
        var client_num_year =$('#num2_year_'+num).val();
        var shipment_num_year =$('#num3_year_'+num).val();*/

        $.ajax({
            type: 'POST',
            url: '/biz_crm/crm_set_num_save/2',
            data: {
                crm_num_month:crm_num_month,
                client_num_month:client_num_month,
                shipment_num_month:shipment_num_month,
                /*crm_num_year:crm_num_year,
                client_num_year:client_num_year,
                shipment_num_year:shipment_num_year,*/
                target_name:target_name,
                start_date:start_date,
            },
            dataType: 'json',
            success: function (res) {
                alert(res.msg);
                if (res.code == 0) {
                    window.location.reload();
                }
            },
            error: function (res) {
                alert('系统错误请联系管理员');
            },
        });
    }
</script>