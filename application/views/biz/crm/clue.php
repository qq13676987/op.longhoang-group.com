<!doctype html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="/inc/css/normal.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/icon.css?v=3">
<link href="/inc/third/layui/css/layui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.easyui.min.js?v=11"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.edatagrid.js?v=5"></script>
<script type="text/javascript" src="/inc/js/easyui/datagrid-detailview.js"></script>
<script type="text/javascript" src="/inc/js/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/inc/js/other.js?v=1"></script>
<script type="text/javascript" src="/inc/js/set_search.js?v=5"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.easyui.ext.js?v=53"></script>
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<title><?= lang('线索');?></title>
<style type="text/css">
    .f {
        width: 115px;
    }

    .s {
        width: 100px;
    }

    .v {
        width: 200px;
    }

    .del_tr {
        width: 23.8px;
    }

    .this_v {
        display: inline;
    }

    .input {
        width: 200px;
    }

    .select {
        width: 210px;
    }

    .textarea {
        width: 200px;
        height: 60px;
    }

    .query_f {
        padding-left: 15px;
        color: #1E9FFF;
    }

    .query_s {
        padding-left: 5px;
        color: #5FB878;
    }

    .query_v {
        padding-left: 5px;
        color: #FF5722;
    }

    /**
    * 修改datagrid表格样式
    **/
    .datagrid-body td {height: 40px !important;}
    .datagrid-header, .datagrid-htable {height: 40px !important; background: #eceff0; border-bottom: none;}
    .datagrid-header-row td{border-width: 0px 1px 1px 0px; border-style: solid; border-color: #ddd;}
    .panel-body-noheader {border-top:0px !important;}
    .tabs {padding-left:0px;}
    .tabs-inner {height: 25px !important;}
    .tabs li.tabs-selected a.tabs-inner {border-bottom: 1px solid #F4F4F4;}
    .tabs-header {background: none;}
    .tabs li a.tabs-inner{padding: 0px 20px;}
    .tabs li.tabs-selected a.tabs-inner{background: #F4F4F4}
    .datagrid-row-over {background: #eceeeb;}
    .datagrid-header td.datagrid-header-over {background:#e5e7e8;color: #000000;cursor: default;}
    .layui-layer-lan .layui-layer-title {background: #325763 !important;}
    .datagrid .panel-body {border:none;}
    #cx table tr td {height: 30px; min-width: 50px;}
    #cx table tr td button {width: 23px;}
</style>
<!--这里是查询框相关的代码-->
<script>
    var query_option = {};//这个是存储已加载的数据使用的
    var table_name = 'biz_crm_clue',//需要加载的表名称
        view_name = 'biz_crm_clue_view';//视图名称
    var add_tr_str = "";//这个是用于存储 点击+号新增查询框的值, load_query_box会根据接口进行加载
    var url = '/biz_crm/clue';
    var ids_str = '';

    $(function () {
        load_query_box();
        $('#cx').on('keydown', 'input', function (e) {
            if (e.keyCode == 13) {
                doSearch();
            }
        });

        //加入黑名单
        $('#add_black_list').click(function () {
            var ids = {};
            var rows = $('#tt').datagrid('getSelections');
            if (rows.length < 1){
                layer.alert('请选择黑名单对象！',{icon:5});
                return;
            }

            $.each(rows, function (index, item) {
                ids[index] = item.id;
            })

            layer.prompt({title: '请输入拉黑原因！', formType: 2, btn:['确定拉黑', '取消']}, function(black_list_cause, index){
                var loading = layer.load(1);
                $.ajax({
                    url: "/biz_crm/add_black_list",
                    type: "POST",
                    data: {ids: ids, black_list_cause:black_list_cause},
                    dataType: "json",
                    success: function (data) {
                        layer.close(loading);
                        layer.close(index);
                        if (data.code == 1) {
                            $('#tt').datagrid('reload');
                            layer.msg('Succes.', {icon:1})
                        } else {
                            layer.alert(data.msg, {icon: 5});
                        }
                    },
                    error: function (data) {
                        layer.close(loading);
                        layer.msg('请求失败，错误代码：' + data.status);
                    }
                });
            });
        });
    });
    /**
     * 加载查询框
     */
    function load_query_box(){
        ajaxLoading();
        $.ajax({
            type:'GET',
            url: '/sys_config_title/get_user_query_box?view_name=' + view_name + '&table_name=' + table_name,
            dataType:'json',
            success:function (res) {
                ajaxLoadEnd();
                if(res.code == 0){
                    //加载查询框
                    $('#cx table').append(res.data.box_html);
                    //需要调用下加载器才行
                    $.parser.parse($('#cx table tbody .query_box'));

                    add_tr_str = res.data.add_tr_str;
                    query_option = res.data.query_option;
                    var table_columns = [[
                        //{field:'ck', checkbox:true},
                    ]];//表的表头

                    //填充表头信息
                    $.each(res.data.table_columns, function (i, it) {
                        //读取列配置,然后这里进行合并
                        var column_config;
                        try {
                            column_config = JSON.parse(it.column_config,function(k,v){
                                if(v && typeof v === 'string') {
                                    return v.indexOf('function') > -1 || v.indexOf('FUNCTION') > -1 ? new Function(`return ${v}`)() : v;
                                }
                                return v;
                            });
                        }catch (e) {
                            column_config = {};
                        }
                        var this_column_config = {field:it.table_field, title:it.title, width:it.width, sortable: it.sortable, formatter: get_function(it.table_field + '_for'), styler: get_function(it.table_field + '_styler')};
                        //合并一下
                        $.extend(this_column_config, column_config);

                        table_columns[0].push(this_column_config);
                    });


                    //渲染表头
                    $('#tt').datagrid({
                        columns:table_columns,
                        width: 'auto',
                        height: $(window).height(),
                        url: url,
                        pageSize:20,
                        onDblClickRow: function (index, row) {
                            parent.layer.open({
                                type: 2,
                                title: '<?= lang('查看线索');?>',
                                area: ['1300px','600px'],
                                shade: 0.5,
                                offset:'100px',
                                content: '/biz_crm/clue_detail/?company_search='+row.company_search+'&clue_pure_text='+row.company_search_en+'&id='+row.id,
                                success: function(layero, index) {
                                    $('#tt').datagrid('reload');
                                    //找到当前弹出层的iframe元素
                                    var iframe = $(layero).find('iframe');
                                    var childPageHeight = iframe[0].contentDocument.body.offsetHeight;
                                    if (childPageHeight > 600){
                                        iframe.css('height', 600);
                                    }else{
                                        //对加载后的iframe进行宽高度自适应
                                        layer.iframeAuto(index);
                                    }
                                }
                            });
                        }
                    });
                    $(window).resize(function () {
                        $('#tt').datagrid('resize');
                    });

                    //为了避免初始加载时, 重复加载的问题,这里进行了初始加载ajax数据
                    var aj = [];
                    $.each(res.data.load_url, function (i,it) {
                        aj.push(get_ajax_data(it, i));
                    });
                    //加载完毕触发下面的下拉框渲染
                    //$_GET 在other.js里封装
                    var this_url_get = {};
                    $.each($_GET, function (i, it) {
                        this_url_get[i] = it;
                    });
                    var auto_click = this_url_get.auto_click;

                    $.when.apply($,aj).done(function () {
                        //这里进行字段对应输入框的变动
                        $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                            var ec_value = $(ec_item).combobox('getValue');
                            var v_inp = $('.v:eq(' + ec_index + ')');
                            var s_val = $('.s:eq(' + ec_index + ')').combobox('getValue');
                            var v_val = v_inp.textbox('getValue');
                            //如果自动查看,初始值为空
                            if(auto_click === '1') v_val = '';

                            $(ec_item).combobox('clear').combobox('select', ec_value);
                            //这里比对get数组里的值
                            $.each(this_url_get, function (trg_index, trg_item) {
                                //切割后,第一个是字段,第二个是符号
                                var trg_index_arr = trg_index.split('-');
                                //没有第二个参数时,默认用等于
                                if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';
                                //用一个删一个
                                //如果当前的选择框的值,等于get的字段值
                                if(ec_value === trg_index_arr[0] && s_val === trg_index_arr[1]){
                                    v_val = trg_item;//将v改为当前get的
                                    delete this_url_get[trg_index];
                                }
                            });
                            //没找到就正常处理
                            $('.v:eq(' + ec_index + ')').textbox('setValue', v_val);
                        });
                        //判断是否有自动查询参数
                        var no_query_get = ['auto_click'];//这里存不需要塞入查询的一些特殊变量
                        $.each(no_query_get, function (i,it) {
                            delete this_url_get[it];
                        });
                        //完全没找到的剩余值,在这里新增一个框进行选择
                        $.each(this_url_get, function (trg_index, trg_item) {
                            add_tr();//新增一个查询框
                            var trg_index_arr = trg_index.split('-');
                            //没有第二个参数时,默认用等于
                            if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';

                            $('#cx table tbody tr:last-child .f').combobox('setValue', trg_index_arr[0]);
                            $('#cx table tbody tr:last-child .s').combobox('setValue', trg_index_arr[1]);
                            $('#cx table tbody tr:last-child .v').textbox('setValue', trg_item);
                        });

                        //这里自动执行查询,显示明细
                        if(auto_click === '1'){
                            doSearch();
                        }
                    });
                }else{
                    $.messager.alert("<?= lang('提示');?>", res.msg);
                }
            },error:function (e) {
                ajaxLoadEnd();
                $.messager.alert("<?= lang('提示');?>", e.responseText);
            }
        });
    }

    /**
     * 新增一个查询框
     */
    function add_tr() {
        $('#cx table tbody').append(add_tr_str);
        $.parser.parse($('#cx table tbody tr:last-child'));
        var last_f = $('#cx table tbody tr:last-child .f');
        var last_f_v = last_f.combobox('getValue');
        last_f.combobox('clear').combobox('select', last_f_v);
        return false;
    }

    /**
     * 删除一个查询
     */
    function del_tr(e) {
        //删除按钮
        var index = $(e).parents('tr').index();
        $(e).parents('tr').remove();
        return false;
    }

    function setValue(jq, val) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combobox !== undefined){
            return $(jq).combobox('setValue',val);
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('setValue',val);
        }else if(data.datebox !== undefined){
            return $(jq).datebox('setValue',val);
        }else if(data.textbox !== undefined){
            return $(jq).textbox('setValue',val);
        }
    }

    function getValue(jq) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if (data.combobox !== undefined) {
            return $(jq).combobox('getValue');
        } else if (data.datetimebox !== undefined) {
            return $(jq).datetimebox('getValue');
        } else if (data.datebox !== undefined) {
            return $(jq).datebox('getValue');
        } else if (data.textbox !== undefined) {
            return $(jq).textbox('getValue');
        }
    }

    //模板相关--start
    function clue_status_for(value, row, index) {
        var str = '<div style="text-align: center">';
        switch(value) {
            case '1':
                str += '<span style="color: #ff6000;">分析中<i style="font-size: 12px;" class="layui-icon layui-icon-loading layui-anim layui-anim-rotate layui-anim-loop"></i>   </span>';
                break;
            // case '1':
            //     str += '<a href="javascript:void(0);" style="color: #077320;" onclick="gaining(' + row.id + ')"><?= lang('可提取');?></a>';
            //     break;
            case '2':
                str += '<a href="javascript:void(0);" style="color: #077320;" onclick="gaining(' + row.id + ')"><?= lang('可提取');?></a>';
                break;
            default:
                str += '<span style="color: #9b9b9b;"><?= lang('不可提取');?></span>';
        }
        str += '</div>';
        return str;
    }

    function clue_analysis_result_for(value, row, index) {
        if (value == ''){
            return '<span style="color: #9b9b9b;"><?= lang('暂无分析结果');?></span>';
        }else{
            return '<span style="color: #077320;">'+value+'</span>';
        }
    }

    function browse_log_for(value, row, index) {
        if (value == 0){
            return '<span style="color: #9b9b9b;"><?= lang('暂无访问记录');?></span>';
        }else{
            return '<span style="color: #1e7ed6; cursor: pointer" onclick="browse_log('+row.id+')">'+value+'</span>';
        }
    }
    //模板相关--end

    var ids = []
    //selectAll和这个函数差不多,改的时候记得一起改
    function doSearch(url = '') {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if (json.hasOwnProperty(item.name) === true) {
                if (typeof json[item.name] == 'string') {
                    json[item.name] = [json[item.name]];
                    json[item.name].push(item.value);
                } else {
                    json[item.name].push(item.value);
                }
            } else {
                json[item.name] = item.value;
            }
        });

        //清空显示的查询条件
        $('#query').html('');

        var cx_data2 = cx_data
        $.each(cx_data2, function (index, item) {
            if (item.value == 'client.company_name') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('客户全称');?>'};
            }
            if (item.value == 'client.client_name') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('客户简称');?>'};
            }
            if (item.value == 'client.country_code') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('国家代码');?>'};
            }
            if (item.value == 'client.city') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('市');?>'};
            }
            if (item.value == 'client.company_address') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('客户地址');?>'};
            }
            if (item.value == 'crm.export_sailing') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('出口航线');?>'};
            }
            if (item.value == 'crm.trans_mode') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('运输方式');?>'};
            }
            if (item.value == 'crm.product_type') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('品名大类');?>'};
            }
            if (item.value == 'crm.product_details') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('品名大类描述');?>'};
            }
            if (item.value == 'crm.client_source') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('客户来源');?>'};
            }
            if (item.value == 'crm.created_by') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('创建人');?>'};
            }
            if (item.value == 'crm.close_plan_date') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('结束时间');?>'};
            }
            if (item.value == 'crm.created_time') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('创建时间');?>'};
            }
            if (item.name == 'field[v][]') {
                if (item.value == '') {
                    cx_data2[index] = {name: 'field[v][]', value: '<?= lang('无查询条件');?>'};
                }
            }
        });
        $.each(cx_data2, function (index, item) {
            if (item.name == 'f_role') {
                if (item.value != '') {
                    $("#query").append('<span class="query_f">' + item.value + '</span>')
                }
            }
            //判断如果name存在,且为string类型
            if (item.name == 'field[name][]') {
                $("#query").append('<span class="query_f">' + item.value + '</span>')
            }
            if (item.name == 'field[s][]') {
                $("#query").append('<span class="query_s">' + item.value + '</span>')
            }
            if (item.name == 'field[v][]') {
                $("#query").append('<span class="query_v">' + item.value + '</span>')
            }

        });

        var opt = $('#tt').datagrid('options');
        if (url == '') {
            opt.url = '/biz_crm/clue';
        } else {
            opt.url = url;
        }

        $('#tt').datagrid('load', json).datagrid('clearSelections');
        set_config();
        $('#select_ids').val('');
        $('#chaxun').window('close');
    }

    function resetSearch(){
        $.each($('.v'), function(i,it){
            setValue($(it), '');
        });
    }

    function doSearch_all() {
        var url = '/biz_crm/clue';
        doSearch(url);
    }

    /**
     * 删除客户
     */
    function del() {
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length>0){
            var ids = [];
            $.each(rows, function (i, it) {
                ids.push(it.id);
            });

            $.messager.confirm('<?= lang('提示');?>', '<?= lang('确认删除选中客户?');?>', function(r){
                if(r){
                    $.getJSON("/biz_crm/del_customer?ids="+ids,function(data){
                        if(data.code==200){
                            $('#tt').datagrid('load');
                            layer.alert(data.msg, {icon:1});
                        }
                        else {
                            layer.alert('<?= lang('请选择删除项!');?>', {icon:2})
                        }

                    })
                }
            })
        }
        else {
            layer.alert('<?= lang('请选择删除项!');?>', {icon:2});
            return
        }
    }

    function gaining(crm_id) {
        layer.open({
            type: 2,
            title: '<?= lang('我要提取');?>',
            shade:0.5,
            area: ['1270px', '700px'],
            content: '/biz_crm/gaining/?clue_id='+crm_id
        });
    }
</script>
<body>
<table id="tt" style="width:1100px;" rownumbers="false" pagination="true" idField="id" pagesize="30" toolbar="#tb" singleSelect="true" nowrap="true"></table>
<div id="tb" style="padding:5px;">
    <div style="display:flex;display:-webkit-flex;">
        <div style="flex: 1;">
            <?php
            //只有田诗雯可以删除和导入
            if(is_admin()):
                ?>
                <!--<a href="javascript:void(0);" id="add_black_list" class="easyui-linkbutton" iconCls="icon-add" plain="true"><?= lang('加入黑名单');?></a>-->
            <?php endif; ?>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('查询');?></a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="config()"><?= lang('表头设置');?></a>            |
            <div id="query" style="display:inline-block"></div>
            <div style="display:inline-block; margin-left: 50px; color:red;"><?= lang('线索总数：{total}条', array('total' => $total));?></div>
            <div style="display:inline-block; margin-left: 20px; color:red;"><?= lang('分析中：{num}%', array('num' => $load_proportion));?></div>
            <div style="display:inline-block; margin-left: 20px; color:red;"><?= lang('可提取：{num}%', array('num' => $gain_proportion));?></div>
            <div style="display:inline-block; margin-left: 20px; color:red;"><?= lang('不可提取：{num}%', array('num' => $not_gain));?></div>
        </div>
    </div>
</div>

<div id="title_tabs" style="hidden" value=""></div>
<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <form id="cx">
        <table></table>
        <br><br>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="doSearch_all();"><?= lang('查询');?></button>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="resetSearch();"><?= lang('重置');?></button>
        <a href="javascript:void;" class="easyui-tooltip" data-options="
            content: $('<div></div>'),
            onShow: function(){
                $(this).tooltip('arrow').css('left', 20);
                $(this).tooltip('tip').css('left', $(this).offset().left);
            },
            onUpdate: function(cc){
                cc.panel({
                    width: 500,
                    height: 'auto',
                    border: false,
                    href: '/bsc_help_content/help/query_condition'
                });
            }
        ">help</a>
    </form>
</div>
<script type="text/javascript"src="/inc/third/layui/layui.js"></script>
<script>
    layui.use(['form','laydate'], function(){
        var form = layui.form;
        var laydate = layui.laydate;
        //绑定日期控件
        laydate.render({
            elem: '#end_date'
        });

        //空格转换
        form.on('select', function(data){
            //layui的渲染方法可以做到无缝转换，但是不能解决右键时的BUG
            form.render();
            //解决右键BUG就用下面的代码强制转换为空格
            $('body').click(function () {
                $('.layui-form-select .layui-select-title>.layui-input').each(function () {
                    var _this = this;
                    setTimeout(function () {
                        var val = $(_this).val();
                        var new_val = val.replace(/\&nbsp\;/g, ' ')
                        if (new_val !== val) {
                            $(_this).val(new_val)
                        }
                    }, 100)
                })
            })
        });

        //提交表单
        form.on('submit(*)', function(data){
            var loading = layer.load(1);
            var sale_id = $('#sale_id').val();
            var end_date = $('#end_date').val();
            data.field.ids = ids_str;
            $.ajax({
                type: 'post',
                url: '/biz_crm/batch_deliver_client',
                data: {ids: ids_str, sale_id: sale_id, end_date: end_date},
                success: function (data) {
                    layer.close(loading);
                    if (data.code == 1) {
                        layer.close(batch_deliver_window);
                        //刷新父窗口（CRM客户）easyUI的datagrid控件
                        parent.$('#tt').datagrid('reload');
                        layer.alert('<?= lang('转交成功');?>', {icon: 1, closeBtn:0}, function (index) {
                            //刷新easyUI的datagrid控件
                            $('#tt').datagrid('reload').datagrid('clearSelections');
                            //清空表单
                            form.val("myform", { "sale_id": "","end_date": ""});
                            layer.close(index);
                        });
                    } else {
                        layer.alert(data.msg, {icon: 5});
                    }
                },
                error: function (data) {
                    layer.close(loading);
                    layer.alert('<?= lang('请求失败，错误代码：');?>' + data.status, {icon: 5});
                },
                dataType: "json"
            });
            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        });
    });

    function browse_log(id_no) {
        layer.open({
            type: 2,
            title: '<?= lang('访问记录');?>',
            area: ['550px', '600px'],
            shade: 0.5,
            skin: 'layui-layer-lan',
            shadeClose: true,
            content: '/Sys_browse_log/get_detail/?id_type=clue1&id_no='+id_no,
        });
    }

    //批量转交
    function batch_deliver() {
        var tt = $('#tt');
        var rows = tt.datagrid('getSelections');
        if (rows.length > 0) {
            var ids = [];
            $.each(rows, function (i, it) {
                ids.push(it.id);
            });
            ids_str = ids.join(',');
            batch_deliver_window = layer.open({
                id: 'to-sales',
                type: 1,
                title: '<?= lang('批量转交销售');?>',
                area: '330',
                offset: '20%',
                shade: 0.5,
                content: $('#batch_deliver_div'),
                zIndex: 9999,
                btn: ['<?= lang('保存');?>', '<?= lang('取消');?>'],
                success: function (layero, index) {
                    $('#to-sales').css('overflow', 'unset');
                },
                yes: function (e, layero) {
                    $('#batch_deliver_btn').click();
                }
            });
        } else {
            layer.msg("<?= lang('请选择任意一行后再试');?>", {icon:2});
        }
    }
</script>
</body>
<input type="hidden" id="select_ids">
