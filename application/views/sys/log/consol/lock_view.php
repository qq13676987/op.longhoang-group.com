<script type="text/javascript" src="/inc/js/easyui/datagrid-export.js?v=1"></script>
<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<style type="text/css">
</style>
<script type="text/javascript">
    var data = {};
    function doSearch(){
    }

    var more_searchs = false;
    function more_search(){
        var ed = $('#cx tr:nth-child(-n+9)');
        if(more_searchs){
            ed.css('display', 'none');
        }else{
            ed.css('display', 'table-row');
        }
        more_searchs = !more_searchs;
    }

    function getData(){
        var rows = [];
        <?php foreach($rows as $row){ ?>
        rows.push({
            id: '<?= $row['id'];?>',
            job_no: '<?= $row['job_no'];?>',
            user_id_name: '<?= $row['user_id_name'];?>',
            op_time: '<?= $row['op_time'];?>',
            op_value: '<?= $row['op_value'];?>',
        });
        <?php } ?>
        return rows;
    }

    function pagerFilter(data){
        if (typeof data.length == 'number' && typeof data.splice == 'function'){	// is array
            data = {
                total: data.length,
                rows: data
            }
        }
        var dg = $(this);
        var opts = dg.datagrid('options');
        var pager = dg.datagrid('getPager');
        pager.pagination({
            onSelectPage:function(pageNum, pageSize){
                opts.pageNumber = pageNum;
                opts.pageSize = pageSize;
                pager.pagination('refresh',{
                    pageNumber:pageNum,
                    pageSize:pageSize
                });
                dg.datagrid('loadData',data);
            }
        });
        if (!data.originalRows){
            data.originalRows = (data.rows);
        }
        var start = (opts.pageNumber-1)*parseInt(opts.pageSize);
        var end = start + parseInt(opts.pageSize);
        data.rows = (data.originalRows.slice(start, end));

        return data;
    }

    var setting = {
    };
    var datebox = [];

    var datetimebox = [];
    $(function(){
        $('#chaxun').window('open');

        var selectIndex = -1;
        $('#cx').on('keydown', 'input', function (e) {
            if(e.keyCode == 13){
                doSearch();
            }
        });

        $('#tt').datagrid({
            width:'auto',
            height: $(window).height() - 26,
            rowStyler:function(index,row){
                switch(row.status){
                    case 'cancel':
                        return 'color:red;';
                        break;
                    case 'normal':
                        return 'color:blue;';
                        break;
                }
            },
            onDblClickRow: function(rowIndex) {
                $(this).datagrid('selectRow', rowIndex);
                var row = $('#tt').datagrid('getSelected');
                if (row){
                    url = '/biz_shipment/edit/'+row.id;
                    window.open(url);
                }
            },
            loadFilter:pagerFilter,
            data:getData(),
        });
        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
        //新增查询列
        var a1 = ajax_get('/bsc_dict/get_option/bill_status', 'status');//状态
        $.when(a1).done(function () {
        });
    });

    function ajax_get(url,name) {
        return $.ajax({
            type:'POST',
            url:url,
            dataType:'json',
            success:function (res) {
                data[name] = res;
            },
            error:function () {
                // $.messager.alert('获取数据错误');
            }
        });
    }
</script>

<table id="tt" class="easyui-datagrid" rownumbers="false" idField="id" toolbar="#tb" singleSelect="false" nowrap="false">
    <thead>
    <tr>
        <th data-options="field: 'job_no'">JOB NO</th>
        <th data-options="field: 'user_id_name'">操作人</th>
        <th data-options="field: 'op_time'">操作时间</th>
        <th data-options="field: 'op_value'">锁等级</th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td colspan="8">
                <form action="" method="GET">

                    <!--<button type="submit">submit</button>-->
                </form>
            </td>
        </tr>
        <tr>
            <td>
            </td>
        </tr>
    </table>

</div>