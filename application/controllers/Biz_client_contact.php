<?php

class biz_client_contact extends Menu_Controller
{
    protected $menu_name = 'client_contact';//菜单权限对应的名称
    protected $method_check = array(//需要设置权限的方法
        'index',
        'add_data',
        'update_data',
        'delete_data',
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_client_contact_model');
        $this->model = $this->biz_client_contact_model;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }


    public $field_edit = array();

    public $field_all = array(
        array('id', 'ID', '80', '1', 'sortable="true"'),
//        array('client_code','client_code','100','2',''),
        array('role', 'role', '120', '7', 'editor="{type:\'combobox\',
                        options:{ 
                            editable:false,
                            required:true,
                            multiple:true,
                            valueField:\'value\',
                            textField:\'value\',
                            url:\'/bsc_dict/get_option/role\'
                        }}"'),
        ///biz_client_contact/get_role_option/clientcodereplacement
        // url:\'/bsc_dict/get_option/role\'
        array('name', 'name', '100', '3', 'editor="{type:\'validatebox\',options:{required:true}}"'),
        array('telephone', 'telephone', '120', '4', "editor=\"{type:'textbox', options:{validType:'Num'}}\""),
        array('telephone2', 'telephone2', '100', '4', 'editor="text"'),
        array('email', 'email', '100', '5', "editor=\"{type:'validatebox',options:{validType:'email'}}\""),
        array('live_chat', 'live_chat', '100', '6', 'sortable="true" editor="text"'),
        array('position', 'position', '100', '7', 'editor="text"'),
        array('remark', 'remark', '100', '7', 'editor="text"'),
        array('sailing', 'sailing', '500', '7', 'editor="{type:\'combobox\',
                            options:{
                                multiple:true,
                                valueField:\'value\',
                                textField:\'value\',
                                url:\'/bsc_dict/get_option/export_sailing\'
                            }}"'),
        array('password', 'password', '100', '3', 'editor="text"'),
        array('create_time', 'create_time', '150', '8', ''),
//        array('create_by','create_by','150','9',''),
        array('update_time', 'update_time', '150', '10', ''),
//        array('update_by','update_by','150','11',''),
    );

    public function index($client_code = 0)
    {
        $data = array();

        Model('biz_client_model');
        $data["client_code"] = $client_code;
        $this->db->select('id,client_code,apply_type');
        $client = $this->biz_client_model->get_one('client_code', $client_code);

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_client_contact_list_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
            $data["f"] = array_replace_str($data["f"], array("clientcodereplacement" => $client_code));
            if (!empty($client) && $client['apply_type'] == 1) $data['f'] = array_filter(array_map(function ($r) {
                if (in_array($r[0], array('id', 'role', 'name', 'telephone', 'sailing', 'email', 'position', 'live_chat', 'create_time', 'update_time'))) return $r;
            }, $data['f']));
        }

        // page account
        $rs = $this->sys_config_model->get_one('biz_client_contact_list_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }

        $this->load->view('head');
        $this->load->view('biz/client/contact/index_view', $data);
    }

    /**
     * @title 添加联系人
     */
    public function add(){
        $return = ['msg'=>'', 'code'=>0];
        $_post = $this->input->post();
        $client_code = (string)$this->input->get('client_code');

        //查询客户信息
        Model('biz_client_model');
        $this->db->select('id,role, client_code');
        $client = $this->biz_client_model->get_one('client_code', $client_code);

        //如果是提交表单则处理表单，不渲染视图
        if (is_ajax() && isset($_post['is_submit']))
        {
            //判断户客是否存在
            if(empty($client)){
                $return['msg'] = '参数"client_code"无效！';
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            //校验提交内容
            $_post['client_code'] = $client_code;
            $validate = $this->validate($_post);
            if ($validate !== true){
                $return['msg'] = $validate;
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            //允许存入的字段
            $data = $this->create_date($_post);
            //写入数据
            if ($this->db->insert('biz_client_contact_list', $data)) {
                $id = $this->db->query('select MAX(id) as id from biz_client_contact_list')->row_array()['id'];
                $return['code'] = 1;
            } else {
                $return['msg'] = "添加联系人失败， error type: mysql！";
            }

            // 记录日志的例子
            $this->write_log($id, 'insert', $data);
            //返回操作结果
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        //客户可选角色
        if (!empty($client) && !empty($client['role'])){
            $client['role'] = explode(',', $client['role']);
        }

        //渲染视图
        $this->load->vars('client', $client);
        $this->load->view('head');
        $this->load->view('/biz/client/contact/add_view');
    }

    /**
     * @title 编辑系人
     */
    public function edit(){
        $id = (int)$this->input->get('id');
        $_post = $this->input->post();
        $client = [];

        //表单默认值
        $row = $this->db->where('id', $id)->get('biz_client_contact_list')->row_array();
        if ($row) {
            //查询客户信息
            Model('biz_client_model');
            $this->db->select('id,role, client_code');
            $client = $this->biz_client_model->get_one('client_code', $row['client_code']);
            $row['menu'] = array_filter(explode(',', $row['menu']));
            $row['sailing'] = array_filter(explode(',', $row['sailing']));
        }

        //如果是提交表单则处理表单
        if (is_ajax() && isset($_post['is_submit']))
        {
            //判断户客是否存在
            if(empty($client)){
                $return['msg'] = '参数"client_code"无效！';
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            //校验提交内容
            $_post['client_code'] = $client['client_code'];
            $validate = $this->validate($_post, 'edit');
            if ($validate !== true){
                $return['msg'] = $validate;
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            //允许写入的字段
            $data = $this->create_date($_post);
            //2023-10-18 如果邮箱已验证,那么 不能修改或删除
            if(!empty($data['email']) &&$row['email_verify'] === '1'){
                $data['isError'] = true; // 该参数是页面判断失败的参数,必备
                $data['msg'] = lang('邮箱已验证, 不能修改');
                return jsonEcho($data);
            }
            //修改数据
            if ($this->db->where('id', $id)->update('biz_client_contact_list', $data)) {
                $return['code'] = 1;
            } else {
                $return['msg'] = "修改联系人失败， error type: mysql！";
            }

            // 记录日志的例子
            $this->write_log($id, 'insert', $data);
            //返回操作结果
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        //可选角色
        if (!empty($client) && !empty($client['role'])){
            $client['role'] = explode(',', $client['role']);
        }

        //渲染视图
        $this->load->vars('client', $client);
        $this->load->view('head');
        $this->load->view('/biz/client/contact/edit_view', ['data'=>$row]);
    }

    /**
     * @title 复制系人
     */
    public function copy(){
        $id = (int)$this->input->get('id');

        //表单默认值
        $row = $this->db->where('id', $id)->get('biz_client_contact_list')->row_array();
        if ($row) {
            unset($row['role']);
            //查询客户信息
            Model('biz_client_model');
            $this->db->select('id,role, client_code');
            $client = $this->biz_client_model->get_one('client_code', $row['client_code']);
        }

        //可选角色
        if (!empty($client) && !empty($client['role'])){
            $client['role'] = explode(',', $client['role']);
        }

        //渲染视图
        $this->load->vars('client', $client);
        $this->load->view('head');
        $this->load->view('/biz/client/contact/copy_view', ['data'=>$row]);
    }

    public function get_data($client_code = "")
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = "";
        $where .= 'client_code = CONCAT("' . $client_code . '")';
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($v1 != "") $where .= " and $f1 $s1 '$v1'";
        if ($v2 != "") $where .= " and $f2 $s2 '$v2'";
        if ($v3 != "") $where .= " and $f3 $s3 '$v3'";
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $this->db->select("biz_client_contact_list.*", false);
        $rs = $this->model->get($where, $sort, $order);

        Model('biz_client_model');
        $this->db->select('id, contact_name,contact_telephone,role');
        $client = $this->biz_client_model->get_one('client_code', $client_code);
        $rows = array();
        foreach ($rs as $row) {
            $row['is_default'] = false;

            //2022-08-18 由于 apply_status 的去除, 这里功能先暂时屏蔽,后续需要再看
            if (!empty($client)) {

                if ($row['name'] == $client['contact_name'] && $row['telephone'] == $client['contact_telephone']) $row['is_default'] = true;
                //如果为false表示显示***
                if (!$this->model->role($client_code, $row['role']) && get_session('id') != $row['create_by']) {
                    $row = array_map(function ($v) {
                        return '***';
                    }, $row);
                }
            }
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data($client_code = "")
    {
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($temp != "")
                $data[$item] = $temp;
        }
        $data['client_code'] = $client_code;
        // $telephone_row = $this->model->get_where_one("telephone = '{$data['telephone']}'");
        // if(!empty($telephone_row)){
        //     $data['msg'] = '手机号已存在，原客户:' . $telephone_row['client_code'];
        //     $data['isError'] = true;
        //     echo json_encode($data);
        //     return;
        // }
        if (isset($data['email']) && !check_mail_valid($data['email'])) {
            $data['msg'] = '邮箱验证未通过,该邮箱不满足邮箱格式 或 不是有效邮箱,是否自动验证通过？';
            $data['isError'] = true;
            $data['code'] = '0';
            echo json_encode($data);
            return;
        }
        $id = $this->model->save($data);
        $data['id'] = $id;

        echo json_encode($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_client_contact_list";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function update_data()
    {
        $id = $_REQUEST["id"];
        $old_row = $this->model->get_one('id', $id);

        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            if (isset($_POST[$item])) {
                $temp = $_POST[$item];
                if ($old_row[$item] != $temp) $data[$item] = $temp;
            }

        }
        if (empty($old_row)) {
            echo json_encode($data);
            return;
        }

        if(isset($data["role"]) && strlen($data["role"])<3){
            $data['isError'] = true; // 该参数是页面判断失败的参数,必备
            $data['msg'] = "角色没选择,新增失败";
            return jsonEcho($data);
        }
        if (!empty($data['email']) && !check_mail_valid($data['email'])) {
            $data['msg'] = '邮箱验证未通过,该邮箱不满足邮箱格式 或 不是有效邮箱,是否自动验证通过？';
            $data['isError'] = true;
            $data['code'] = '0';
            echo json_encode($data);
            return;
        }
        
        //更新联系人信息到client里
        Model('biz_client_model');
        $this->db->select('id, contact_name, contact_live_chat, contact_telephone, contact_telephone2, contact_email, contact_position,sales_ids');
        $client = $this->biz_client_model->get_one('client_code', $old_row['client_code']);
        if (empty($client)) {
            echo json_encode($data);
            return;
        }
        $updata_client_data = array();

        if ($client['contact_name'] == $old_row['name'] && $client['contact_telephone'] == $old_row['telephone']) {
            if (isset($data['name']) && $client['contact_name'] != $data['name']) $updata_client_data['contact_name'] = $data['name'];
            if (isset($data['live_chat']) && $client['contact_live_chat'] != $data['live_chat']) $updata_client_data['contact_live_chat'] = $data['live_chat'];
            if (isset($data['telephone']) && $client['contact_telephone'] != $data['telephone']) $updata_client_data['contact_telephone'] = $data['telephone'];
            if (isset($data['telephone2']) && $client['contact_telephone2'] != $data['telephone2']) $updata_client_data['contact_telephone2'] = $data['telephone2'];
            if (isset($data['email']) && $client['contact_email'] != $data['email']) $updata_client_data['contact_email'] = $data['email'];
            if (isset($data['position']) && $client['contact_position'] != $data['position']) $updata_client_data['contact_position'] = $data['position'];
            if (!empty($updata_client_data)) {
                $this->biz_client_model->update($client['id'], $updata_client_data);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_client";
                $log_data["key"] = $client['id'];
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($updata_client_data);
                log_rcd($log_data);
            }
        }
        $id = $this->model->update($id, $data);
        $data['id'] = $id;
        echo json_encode($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_client_contact_list";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->model->get_one('id', $id);
        //2023-10-18 如果邮箱已验证,那么 不能修改或删除
        if(!is_admin() && $old_row['email_verify'] === '1'){
            $data['isError'] = true; // 该参数是页面判断失败的参数,必备
            $data['success'] = false; 
            $data['msg'] = lang('邮箱已验证, 请联系管理员删除');
            return jsonEcho($data);
        }
        
        $this->model->mdelete($id);
        echo json_encode(array('success' => true));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_client_contact_list";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
    }

    public function get_option($client_code = "")
    {

        $data = array();
        $empty = array('client_code' => $client_code, 'name' => ' ', 'password' => ' ', 'telephone' => ' ', 'telephone2' => ' ', 'email' => ' ', 'live_chat' => ' ', 'position' => ' ', 'role' => ' ', 'remark' => ' ');
        $data[] = $empty;

        $rs = array();
        if (trim($client_code) != '') $rs = $this->model->get_option($client_code);
        foreach ($rs as $row) {
            $data[] = $row;
        }

        echo json_encode($data);
    }

    public function get_option_by_role($client_code = "")
    {

        $data = array();
        $empty = array('client_code' => $client_code, 'name' => ' ', 'password' => ' ', 'telephone' => ' ', 'telephone2' => ' ', 'email' => ' ', 'live_chat' => ' ', 'position' => ' ', 'role' => ' ', 'remark' => ' ');
        $data[] = $empty;
        
        $name = trim(postValue('name', ''));
        $email = trim(postValue('email', ''));
        $telephone = trim(postValue('telephone', ''));

        $where = array();
        
        
        $roles = $_GET["roles"];
        $roles = explode(",",$roles);
        foreach($roles as $role){
            $where[] = "role = '$role'";
        }
        $where = implode(" or ",$where);
        $where = "client_code = '$client_code' and ($where)";
        
        if(!empty($name)) $where .= " and name like '%{$name}%'";
        if(!empty($email)) $where .= " and email like '%{$email}%'";
        if(!empty($telephone)) $where .= " and telephone like '%{$telephone}%'";

        $rs = array();
        if (trim($client_code) != '') $rs = $this->model->get($where);
        foreach ($rs as $row) {
            $data[] = $row;
        }

        echo json_encode($data);
    }

    public function get_role_option($client_code = "")
    {
        $this->load->model("m_model");
        $sql = "select role from biz_client where client_code ='$client_code'";
        if (is_numeric($client_code)) $sql = "select role from biz_client_apply where id ='$client_code'";
        $row = $this->m_model->query_one($sql);
        $roles = explode(",", $row["role"]);
        $data = array();
        foreach ($roles as $role) array_push($data, array("value" => $role));
        echo json_encode($data);
    }

    /**
     * CRM联系人页面使用的角色选择数据
     */
    public function get_role_option_crm()
    {
        $crm_id = getValue('crm_id', '');
        $this->load->model("m_model");
        $sql = "select role from biz_client_crm where id ='$crm_id'";
        $row = $this->m_model->query_one($sql);
        $roles = explode(",", $row["role"]);
        $data = array();
        foreach ($roles as $role) array_push($data, array("value" => $role));
        echo json_encode($data);
    }

    public function client_user_menu_option()
    {
        echo '[{"value":"Price_T2"},{"value":"Tracking"},{"value":"ebooking"},{"value":"SI"},{"value":"bill"},{"value":"wx_push"}]';
    }

    /**
     * 设置该联系人为该客户的默认联系人
     */
    public function set_client_contact()
    {
        $client_code = postValue('client_code', '');
        $id = postValue('id', '');

        $result = array('code' => 1, 'msg' => '参数错误');

        $contact = $this->model->get_one('id', $id);
        if (empty($contact)) {
            echo json_encode($result);
            return;
        }

        Model('biz_client_model');
        $this->db->select('id, contact_name, contact_live_chat, contact_telephone, contact_telephone2, contact_email, contact_position,sales_ids');
        $client = $this->biz_client_model->get_one('client_code', $client_code);
        if (empty($client)) {
            echo json_encode($result);
            return;
        }
        $userId = get_session('id');

        //不是销售无权修改
        if (!in_array($userId, explode(',', $client['sales_ids'])) && !is_admin()) {
            $result['msg'] = '无权设置';
            echo json_encode($result);
            return;
        }

        $data = array();

        if ($client['contact_name'] != $contact['name']) $data['contact_name'] = $contact['name'];
        if ($client['contact_live_chat'] != $contact['live_chat']) $data['contact_live_chat'] = $contact['live_chat'];
        if ($client['contact_telephone'] != $contact['telephone']) $data['contact_telephone'] = $contact['telephone'];
        if ($client['contact_telephone2'] != $contact['telephone2']) $data['contact_telephone2'] = $contact['telephone2'];
        if ($client['contact_email'] != $contact['email']) $data['contact_email'] = $contact['email'];
        if ($client['contact_position'] != $contact['position']) $data['contact_position'] = $contact['position'];
        if (!empty($data)) {
            $this->biz_client_model->update($client['id'], $data);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_client";
            $log_data["key"] = $client['id'];
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        }

        $result['code'] = 0;
        $result['msg'] = '设置成功';
        echo json_encode($result);
    }

    public function get_role($client_code = '')
    {
        $this->db->select('role');
        $rs = $this->model->get("client_code = '$client_code'");
        //将角色获取出来去重去空重新组合
        $roles = filter_unique_array(explode(',', join(',', array_column($rs, 'role'))));
        $datas = array();
        foreach ($roles as $role) {
            $data = array();
            $data['text'] = $role;
            $data['value'] = $role;

            $datas[] = $data;
        }
        echo json_encode($datas);
    }

    /**
     * 获取用户数据
     */
    public function get_user_data()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $fields = postValue('field', array());

        //-------where -------------------------------
        $where = array();

        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($f1 == 'company_name' ) {
            $c_n='biz_client';
        } else {
            $c_n='biz_client_contact_list';
        }
        if ($v1 != "") $where[] = "$c_n.$f1 $s1 '%$v1%'";
        if ($f2 == 'company_name' ) {
            $c_n='biz_client';
        } else {
            $c_n='biz_client_contact_list';
        }
        if ($v2 != "") $where[] = "$c_n.$f2 $s2 '%$v2%'";
        if ($v3 != "") $where[] = "biz_client_contact_list.$f3 $s3 '%$v3%'";


        //加入client权限
        Model('biz_client_model');
        $role_where = $this->biz_client_model->read_role();
        if ($role_where != '') $where[] = $role_where;

        //默认只显示设置了密码的
        $where[] = "password != ''";
        //只查询CRM_id = 空的
        $where[] = "crm_id = 0";

        $where_str = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $this->db->join('biz_client', 'biz_client.client_code = biz_client_contact_list.client_code', 'LEFT');
        $result["total"] = $this->model->total($where_str);
        $this->db->limit($rows, $offset);
        $this->db->select("biz_client_contact_list.id, biz_client_contact_list.role,biz_client.client_code,biz_client_contact_list.name,biz_client_contact_list.telephone,biz_client_contact_list.email,biz_client_contact_list.password,biz_client_contact_list.menu" .
            ",biz_client.company_name" .
            ",(select max(login_time) from biz_client_contact_list_login where biz_client_contact_list.email = biz_client_contact_list_login.email) as last_login_time" .
            ",(select count(1) from biz_client_contact_list_login where biz_client_contact_list.email = biz_client_contact_list_login.email) as thirty_days_login_count" .
            ",(select ok from crm_promote_mail_invalid where crm_promote_mail_invalid.invalid_email = biz_client_contact_list.email) as ok", false);
        $this->db->join('biz_client', 'biz_client.client_code = biz_client_contact_list.client_code', 'right');
        $rs = $this->model->get($where_str, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            $row['is_default'] = false;
            if (!empty($client) && $client['apply_status'] == 0) {

                if ($row['name'] == $client['contact_name'] && $row['telephone'] == $client['contact_telephone']) $row['is_default'] = true;
                //如果为false表示显示***
                if (!$this->model->role($row['client_code'], $row['role']) && get_session('id') != $row['create_by']) {
                    $row = array_map(function ($v) {
                        return '***';
                    }, $row);
                }
            }
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    /**
     * @title 过滤post提交
     * @param array $post
     * @return array
     */
    private function create_date($post = []){
        $field = ['role', 'name', 'address', 'telephone', 'telephone2', 'email', 'live_chat', 'position', 'menu', 'password', 'sailing', 'remark', 'client_code', 'crm_id','user_range','job_content'];
        $data = [];
        foreach ($field as $val){
            $data[$val] = isset($post[$val])?$post[$val]:'';
        }
        return $data;
    }

    /**
     * @title 校验post提交
     * @param array $data
     * @return bool|string
     */
    private function validate($data = [], $event='insert'){
        $return = ['msg'=>'', 'code'=>0];
        $validate_field = [
            'role'=>'角色',
            'name'=>'姓名',
            //'telephone'=>'手机号码',
            'email'=>'邮箱',
            //'sailing'=>'负责航线',
        ];

        //是否为重复添加相同的联系人
        $where = [
            'client_code' => $data['client_code'],
            'role' => $data['role'],
            'name' => $data['name'],
        ];
        //编辑时查询条件需要去除当前编辑对象
        if ($event == 'edit') {
            $where['id != '] = (int)$this->input->get('id');
        }

        $count = $this->db->select('count(*) as count')->where($where)->get('biz_client_contact_list')->row_array()['count'];
        //echo $this->db->last_query();
        if ($count > 0) {
            return "相同的联系人和角色已存在！";
        }

        $this->db->select("id,country_code");
        $client = Model('biz_client_model')->get_where_one("client_code = '{$data['client_code']}'");

        $this->load->library('form_validation');
        foreach ($validate_field as $key => $val)
        {
            if (!isset($data[$key]) || $data[$key] == ''){
                $msg = "{$val}必须填写！";
                return $msg;
            }

            switch ($key){
                case 'telephone':
                    //2022-09-26 国内才限制格式
                    if ($client['country_code'] == 'CN' && !preg_match('/^\d{11}$/', $data['telephone'])){
                        $msg = "手机号码格式错误！";
                        return $msg;
                    }
                    break;
                case 'email':
                    if ($this->form_validation->valid_email($data['email']) === false){
                        $msg = "邮箱格式错误！";
                        return $msg;
                    }
                    break;
                default:
            }
        }
        return true;
    }

    /***
     * @title 写入操作日志
     * @param int $id
     * @param string $event
     * @param array $data
     */
    private function write_log($id=0, $event='insert', $data=[]){
        $log_data = array();
        $log_data["table_name"] = "biz_client_contact_list"; //表名
        $log_data["key"] = $id; //主键
        $log_data["action"] = $event; //操作
        $log_data["value"] = json_encode($data); //值
        log_rcd($log_data);
    }

    /**
     * crm的联系人数据
     */
    public function index_crm()
    {
        $crm_id = (int)getValue('crm_id', 0);
        $data = array();
        if ($crm_id == 0) return $this->load->view('/errors/html/error_layui', array('msg'=>lang('参数错误！'), 'icon'=>'layui-icon-404'));
        $data["crm_id"] = $crm_id;
        $this->load->view('head');
        $this->load->view('biz/crm_contact/index_view', $data);
    }
    
    
    public function add_crm(){
        $return = ['msg'=>'', 'code'=>0];
        $_post = $this->input->post();
        $crm_id = (int)getValue('crm_id', 0);
        $data = $this->db->where('id', $crm_id)->get('biz_client_crm')->row_array();

        //如果是提交表单则处理表单，不渲染视图
        if (is_ajax() && isset($_post['is_submit']))
        {
            $_post['client_code'] = '';
            $_post['crm_id'] = $crm_id;
            //校验提交内容
            $validate = $this->validate($_post);
            if ($validate !== true){
                $return['msg'] = $validate;
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }
            //邮箱重复验证
            $contact = $this->db->where('email',$_post['email'])->get('biz_client_contact_list')->row_array();
            if(!empty($contact)){
                if ($contact['client_code'] != ''){
                    $field = "company_name, (select user_id from biz_client_duty where client_code=biz_client.client_code and user_role = 'sales' limit 1) as sales_id";
                    $client = $this->db->select($field)->where('client_code', $contact['client_code'])->get('biz_client')->row_array();
                }else{
                    $client = $this->db->select('company_name, sales_id')->where('id', $contact['crm_id'])->get('biz_client_crm')->row_array();
                }

                if($contact['name'] != $_post['name']){
                    $return['msg'] = "该邮箱已被占用,联系人是：“{$contact['name']}”，客户名称是：“{$client['company_name']}” ，销售是：".getUserName($client['sales_id']);
                    $return['code'] = 2;
                    return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
                }
            }
            //允许存入的字段
            $data = $this->create_date($_post);
            $data['pre_alert_flag'] = isset($_POST['pre_alert_flag'])?$_POST['pre_alert_flag']:0;
            $data['user_range' ] = '';
            $data['parent_company_id' ] = 0;
            $data['create_by'] = get_session('id');
            $data['update_by'] = get_session('id');
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['update_time'] = date('Y-m-d H:i:s');
            //写入数据
            if ($this->db->insert('biz_client_contact_list', $data)) {
                $id = $this->db->insert_id();
                //给crm刷新一下跟进日期
                Model('biz_client_crm_model');
                $this->biz_client_crm_model->update($crm_id, array('last_add_contact_time' => $data['create_time'], 'updated_time' => $data['create_time']));

                $return['code'] = 1;
            } else {
                $return['msg'] = "添加联系人失败， error type: mysql！";
            }

            // 记录日志的例子
            $this->write_log($id, 'insert', $data);
            //返回操作结果
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        if ($crm_id == 0 || empty($data)) return $this->load->view('/errors/html/error_layui', array('msg'=>'参数错误！', 'icon'=>'layui-icon-404'));
        $this->load->vars('data', $data);
        $this->load->view('head');
        $this->load->view('biz/crm_contact/add_view');
    }

    /**
     * @title 编辑系人
     */
    public function edit_crm(){
        $id = (int)$this->input->get('id');
        $_post = $this->input->post();

        //表单默认值
        $row = $this->db->where('id', $id)->get('biz_client_contact_list')->row_array();
        if ($row) {
            //查询客户信息
            $this->db->select('id,role, export_sailing');
            $crm = $this->db->where('id', $row['crm_id'])->get('biz_client_crm')->row_array();
            $row['role'] = $crm['role'];
            $row['sailing'] = array_filter(explode(',', $crm['export_sailing']));
        }else{
            $this->load->view('/errors/html/error_layui', array('msg'=>'参数无效！', 'icon'=>'layui-icon-404'));
        }

        //如果是提交表单则处理表单
        if (is_ajax() && isset($_post['is_submit']))
        {
            //校验提交内容
            $_post['client_code'] = '';
            $_post['crm_id'] = $row['crm_id'];
            $validate = $this->validate($_post, 'edit');
            if ($validate !== true){
                $return['msg'] = $validate;
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }
            //邮箱重复验证
            $contact = $this->db->where('email',$_post['email'])->where('id != '.$id)->get('biz_client_contact_list')->row_array();
            if(!empty($contact) && $row['email'] != $_post['email']){
                if ($contact['client_code'] != ''){
                    $field = "company_name, (select user_id from biz_client_duty where client_code=biz_client.client_code and user_role = 'sales' limit 1) as sales_id";
                    $client = $this->db->select($field)->where('client_code', $contact['client_code'])->get('biz_client')->row_array();
                }else{
                    $client = $this->db->select('company_name, sales_id')->where('id', $contact['crm_id'])->get('biz_client_crm')->row_array();
                }

                if($contact['name'] != $_post['name']){
                    $return['msg'] = "该邮箱已被占用,联系人是：“{$contact['name']}”，客户名称是：“{$client['company_name']}” ，销售是：".getUserName($client['sales_id']);
                    $return['code'] = 2;
                    return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
                }
            }

            //允许写入的字段
            $data = $this->create_date($_post);
            $data['pre_alert_flag'] = isset($_POST['pre_alert_flag'])?$_POST['pre_alert_flag']:0;
            //修改数据
            if ($this->db->where('id', $id)->update('biz_client_contact_list', $data)) {
                $return['code'] = 1;
            } else {
                $return['msg'] = "修改联系人失败， error type: mysql！";
            }

            // 记录日志的例子
            $this->write_log($id, 'insert', $data);
            //返回操作结果
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        //可选角色
        if (!empty($client) && !empty($client['role'])){
            $client['role'] = explode(',', $client['role']);
        }

        //渲染视图
        $this->load->vars('pre_alert_flag', $row['pre_alert_flag']);
        $this->load->view('head');
        $this->load->view('/biz/crm_contact/edit_view', ['data'=>$row]);
    }

    /**
     * 获取CRM联系人的数据
     */
    public function get_data_crm()
    {
        $client_code = getValue('client_code', '');
        $crm_id = (int)getValue('crm_id', 0);
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        if (empty($crm_id)) return jsonEcho(array('rows' => array(), 'total' => 0));// 如果crm_id为空,那么直接断掉,防止过多信息透露

        //-------where -------------------------------
        $where = array();
        $where[] = "crm_id = '{$crm_id}'";
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($v1 != "") $where[] = "$f1 $s1 '$v1'";
        if ($v2 != "") $where[] = "$f2 $s2 '$v2'";
        if ($v3 != "") $where[] = "$f3 $s3 '$v3'";
        //-----------------------------------------------------------------
        //自己看自己的,如果有组权限,那么可以看组员的
        $userId = get_session('id');
        $userRange = get_session('user_range');
        $userGroupRange = explode(',', get_session('group_range'));
//        $where[] = "(create_by = $userId or (select `group` from bsc_user where bsc_user.id = biz_client_contact_list.create_by) in ('" . join('\',\'', $userGroupRange) . "'))";
        $where[] = "create_by in ('$userId', '" . join('\',\'', $userRange) . "')";
        $where_str = join(' and ', $where);

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where_str);
        $this->db->limit($rows, $offset);
        $this->db->select("biz_client_contact_list.*,(select ok from crm_promote_mail_invalid where crm_promote_mail_invalid.invalid_email = biz_client_contact_list.email) as ok", false);
        $rs = $this->model->get($where_str, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            if ($row['email'] == ''){
                $apply_params = json_decode($row['apply_params'], true);
                if (!empty($apply_params)){
                    $row['email'] = "申请邮箱：{$apply_params['apply_email']}<br>{$apply_params['remark']}";
                }
            }
            $row['approve_user_name'] = getUserName($row['approve_user']);
            $row['create_by_name'] = getUserField($row['create_by'],'name');
            $row['update_by_name'] = getUserField($row['update_by'],'name');
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    //删除CRM联系人
    public function del_crm_contact(){
        $id = intval($_REQUEST['id']);
        $old_row = $this->model->get_one('id', $id);

        if (!empty($old_row)) {
            //如果删除的是CRM联系人需要是管理员或创建人
            if(get_session('id') != $old_row['create_by'] && !is_admin()){
                return jsonEcho(array('isError'=>true,'msg' => '无删除权限！'));
            }

            $this->model->mdelete($id);
            echo json_encode(array('success' => true));

            //save the operation log
            $log_data = array();
            $log_data["table_name"] = "biz_client_contact_list";
            $log_data["key"] = $id;
            $log_data["action"] = "delete";
            $log_data["value"] = json_encode($old_row);
            $this->log_rcd($log_data);
        }else{
            return jsonEcho(array('isError'=>true,'msg' => '联系人不存在！'));
        }
    }
    
    
    //新增获取前台权限
    public function get_menu()
    {
        $data = array(array('value' => '订单追踪'), array('value' => 'ebooking'), array('value' => '运价查询'), array('value' => '账号维护'), array('value' => '现舱供应'), array('value' => '任务模块'));
        echo json_encode($data);
    }
    
    public function verified_client(){
        if(!menu_role('contact_verified_client')){
            die(lang('无权限'));
        }
        $data['f'] = array(
            array('创建人', 'create_by'),
            array('更新时间', 'update_time'),
            array('验证联系人', 'name'),
            array('验证电话', 'telephone'),
            array('验证邮箱', 'email'),
            array('客户简称', '(select biz_client.client_name from biz_client where biz_client.client_code = biz_client_contact_list.client_code)'),
            array('客户全称', '(select biz_client.company_name from biz_client where biz_client.client_code = biz_client_contact_list.client_code)'),
            array('英文全称', '(select biz_client.company_name_en from biz_client where biz_client.client_code = biz_client_contact_list.client_code)'),
        );
        $this->load->view('head');
        $this->load->view('biz/client/contact/verified_client',$data);
    }
    
        public function get_verified_client_data()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "update_time";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = array();
        $where[] = "(telephone_verify = 1 or email_verify = 1) and crm_id=0 and client_code != ''";
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($v1 != "") $where[] = search_like($f1, $s1, $v1);
        if ($v2 != "") $where[] = search_like($f2, $s2, $v2);
        if ($v3 != "") $where[] = search_like($f3, $s3, $v3);
        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where,false);
        $this->db->limit($rows, $offset);
        $this->db->select("biz_client_contact_list.*" .
            ",(select bsc_user.name from bsc_user where bsc_user.id = biz_client_contact_list.create_by) as create_by_name" .
            ",(select biz_client.client_name from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as client_name" .
            ",(select biz_client.company_name from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as company_name" .
            ",(select biz_client.company_name_en from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as company_name_en" .
            ",(select biz_client.taxpayer_name from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as taxpayer_name" .
            ",(select biz_client.taxpayer_id from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as taxpayer_id" .
            ",(select biz_client.taxpayer_address from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as taxpayer_address" .
            ",(select biz_client.taxpayer_telephone from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as taxpayer_telephone" .
            ",(select biz_client.invoice_email from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as invoice_email" .
            ",IF(telephone_verify=1,(select name from bsc_user where id=(select created_by from mail_box where id_type='biz_client_contact_list' and id_no = biz_client_contact_list.id and mail_click = 1 and template_id = -1 limit 1)),'') as verified_telephone_name" .
            ",IF(email_verify=1,(select name from bsc_user where id=(select created_by from mail_box where id_type='biz_client_contact_list' and id_no = biz_client_contact_list.id and mail_click = 1 and template_id = 48 limit 1)),'') as verified_email_name" .
            "", false);
        $rs = $this->model->get($where, $sort, $order);
        $result[] = lastquery();
        $rows = array();
        foreach ($rs as $row) {
            $row['verified_name']='';
            if($row['telephone_verify'] == 1){
                $row['verified_name'] = $row['verified_telephone_name'];
            }
            if($row['email_verify'] == 1){
                $row['verified_name'] = $row['verified_email_name'];
            }
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function get_verified_client_data2()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "update_time";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = array();
        $where[] = "biz_client_contact_list.crm_id=0 and biz_client_contact_list.client_code != ''";
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($v1 != "") $where[] = search_like($f1, $s1, $v1);
        if ($v2 != "") $where[] = search_like($f2, $s2, $v2);
        if ($v3 != "") $where[] = search_like($f3, $s3, $v3);
        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $this->db->join('mail_box',"mail_box.template_id in (-1,48) and mail_box.mail_click = 0 and mail_box.id_type = 'biz_client_contact_list' and mail_box.id_no = biz_client_contact_list.id",'right');
        $this->db->where("IF(mail_box.template_id=-1,biz_client_contact_list.telephone_verify = 0,biz_client_contact_list.email_verify = 0)");
        $result["total"] = $this->model->total($where,false);
        $this->db->limit($rows, $offset);
        $this->db->select("biz_client_contact_list.*" .
            ",(select bsc_user.name from bsc_user where bsc_user.id = biz_client_contact_list.create_by) as create_by_name" .
            ",(select biz_client.client_name from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as client_name" .
            ",(select biz_client.company_name from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as company_name" .
            ",(select biz_client.company_name_en from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as company_name_en" .
            ",(select biz_client.taxpayer_name from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as taxpayer_name" .
            ",(select biz_client.taxpayer_id from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as taxpayer_id" .
            ",(select biz_client.taxpayer_address from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as taxpayer_address" .
            ",(select biz_client.taxpayer_telephone from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as taxpayer_telephone" .
            ",(select biz_client.invoice_email from biz_client where biz_client.client_code = biz_client_contact_list.client_code) as invoice_email" .
            ",(select name from bsc_user where id = mail_box.created_by) as verified_name" .
            "", false);
        $this->db->join('mail_box',"mail_box.template_id in (-1,48) and mail_box.mail_click = 0 and mail_box.id_type = 'biz_client_contact_list' and mail_box.id_no = biz_client_contact_list.id",'right');
        $this->db->where("IF(mail_box.template_id=-1,biz_client_contact_list.telephone_verify = 0,biz_client_contact_list.email_verify = 0)");
        $rs = $this->model->get($where, $sort, $order);
        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }
}