<script type="text/javascript">
    var is_submit = false;
    function reset_data(){
        var json = {'is_reset':'1'};
        // GET方式示例，其它方式修改相关参数即可
        var form = $('<form></form>').attr('action','').attr('method','POST');

        $.each(json, function (index, item) {
            if(typeof item === 'string'){
                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
            }else{
                $.each(item, function(index2, item2){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                })
            }
        });

        form.appendTo('body').submit().remove();
    }

    /**
     * 保存数据
     */
    function save_data(){
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $('#fm').form('submit', {
            url: '/bsc_message/set_message_data/consol/<?= $consol_id;?>/ACI/EFREIGHT',
            onSubmit: function(param){
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                    is_submit = false;
                    $('.ShippingParty').css('display', 'table-row');
                }

                return validate;
            },
            success:function(res_json){
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误请联系管理员');
                    return;
                }
                ajaxLoadEnd();
                is_submit = false;
                if(res.code == 0){
                    create_edi();
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.href = "";
                    });
                }else{
                    $.messager.alert('Tips', res.msg);
                }
            }
        });
    }

    function create_edi(action = 'insert'){
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $.ajax({
            type: 'GET',
            url: '/bsc_message/create_message/<?= $consol_id;?>/EFREIGHT/ACI/' + action,
            success: function (res_json) {
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误请联系管理员');
                    return;
                }
                ajaxLoadEnd();
                is_submit = false;
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.reload();
                    });
                }else{
                    $.messager.alert('Tips', res.msg);
                }
            },
            error: function () {
                ajaxLoadEnd();
                is_submit = false;
                $.messager.alert('Tips', '发生错误');
            }
        });
    }

    function send_edi(action, receiver, id) {
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $.ajax({
            type: 'GET',
            url: '/bsc_message/send_message/' + id + '/' + action + '/' + receiver,
            success: function (res_json) {
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误请联系管理员');
                    return;
                }
                ajaxLoadEnd();
                is_submit = false;
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.reload();
                    });
                }else{
                    $.messager.alert('Tips', res.msg);
                }
            },
            error: function () {
                ajaxLoadEnd();
                is_submit = true;
                $.messager.alert('Tips', 'error');
            }
        });
    }

    function read_message(id) {
        window.open('/bsc_message/file_read/' + id);
    }

    function ajax_get(url,name) {
        return $.ajax({
            type:'POST',
            url:url,
            dataType:'json',
            success:function (res) {
                ajax_data[name] = res;
            },
            error:function () {
                // $.messager.alert('获取数据错误');
            }
        });
    }

    function create_tool(div_id, id){
        var html = '<div id="' + div_id + '_div">' +
            '<form id="' + div_id + '_form" method="post">' +
            '<div style="padding-left: 5px;display: inline-block;">' +
            '<label><?= lang('company_name');?>:</label><input name="company_name" class="easyui-textbox keydown_search" onkeydown="keydown_search(event, ' + id + ')" style="width:96px;" data-options="prompt:\'text\'"/>' +
            '</div>' +
            '<form>' +
            '<div style="padding-left: 1px; display: inline-block;">' +
            '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" onclick="query_report(\'' + div_id + '\', \'' + id + '\')"><?= lang('search');?></a>' +
            '</div>' +
            '</div>';
        $('body').append(html);
        $.parser.parse('#' + div_id + '_div');
        return div_id;
    }

    var ajax_data = {};
    $(function () {
        var a1 = ajax_get('/city/get_country','country_code');
        $.when(a1).done(function () {
            $('.country').combobox('loadData', ajax_data.country_code);
        });
        var a2 = ajax_get('/biz_port/get_option?type=<?= $consol['trans_mode'];?>&carrier=<?= $consol['trans_carrier'];?>','port');
        $.when(a2).done(function () {
            $('.port').combobox('loadData', ajax_data.port);
        });

        var company_config = {
            panelWidth: '600px',
            panelHeight: '300px',
            idField: 'id',              //ID字段
            textField: 'id',    //显示的字段
            // multiple:true,
            fitColumns : true,
            striped: true,
            editable: false,
            pagination: false,           //是否分页
            // pageList : [30,50,100],
            // pageSize : 50,
            rownumbers: true,           //序号
            collapsible: true,         //是否可折叠的
            method: 'post',
            columns:[[
                {field:'company_name',title:'<?= lang('country_code');?>',width:200},
                {field:'city',title:'<?= lang('city');?>',width:100},
            ]],
            emptyMsg : '未找到相应数据!',
            onSelect : function(index, row){
                //判断下面是否有对应ID的，没有生成一套新的
                var id = row.id;
                //首先得找到这个div
                var id_input = $('#PartyCode_' + id);
                if(id_input.length > 0){
                    // 找到了进行赋值
                    $('#PartyName_' + id).val(row.company_name);
                    $('#Address_' + id).val(row.company_address);
                    $('#Country_' + id).combobox('select', row.country);
                    $('#City_' + id).textbox('setValue', row.city);
                    $('#PostalCode_' + id).textbox('setValue', row.postalcode);
                    $('#Region_' + id).combobox('select', row.region);
                    $('#SubDivisionCode_' + id).combobox('select', row.region_code);
                    $('#ContactName_' + id).combobox('select', row.company_contact);
                    $('#ContactMail_' + id).combobox('select', row.company_email);
                    $('#Phone_' + id).combobox('select', row.company_telephone);
                }else{
                    //没找到的是放最下面呢，还是放在
                    var this_key = $('.ShippingParty').length;
                    var ShippingParty = $("<fieldset/>").addClass('ShippingParty');
                    var str = "<legend><h4>ShippingParty " + (this_key + 1) + "</h4></legend>\n" +
                        "                        <table>\n" +
                        "                            <tr>\n" +
                        "                                <td>PartyCode</td>\n" +
                        "                                <td>\n" +
                        "                                    <input class=\"easyui-textbox input\" required readonly name=\"ShippingParties[" + this_key + "][PartyCode]\" id=\"PartyCode_" + id + "\" value=\"" + id + "\">\n" +
                        "                                </td>\n" +
                        "                                <td>PartyType</td>\n" +
                        "                                <td>\n" +
                        "                                    <input class=\"easyui-textbox input\" required readonly name=\"ShippingParties[" + this_key + "][PartyType]\" id=\"PartyType_" + id + "\" value=\"CU\">\n" +
                        "                                </td>\n" +
                        "                                <td>PartyName</td>\n" +
                        "                                <td>\n" +
                        "                                    <textarea class=\"input_textarea\" name=\"ShippingParties[" + this_key + "][PartyName]\" id=\"PartyName_" + id + "\" required>" + row.company_name + "</textarea>\n" +
                        "                                </td>\n" +
                        "                            </tr>\n" +
                        "                            <tr>\n" +
                        "                                <td><h4>PartyLocation</h4></td>\n" +
                        "                            </tr>\n" +
                        "                            <tr>\n" +
                        "                                <td>Address</td>\n" +
                        "                                <td>\n" +
                        "                                    <textarea class=\"input_textarea\" name=\"ShippingParties[" + this_key + "][PartyLocation][Address]\" id=\"Address_" + id + "\" required>" + row.company_address + "</textarea>\n" +
                        "                                </td>\n" +
                        "                                <td>Country</td>\n" +
                        "                                <td>\n" +
                        "                                    <select class=\"easyui-combobox country input\" required name=\"ShippingParties[" + this_key + "][PartyLocation][Country][code]\" id=\"Countrycode_" + id + "\" data-options=\"\n" +
                        "                                        valueField:'cityname_en',\n" +
                        "                                        textField:'namecode_en',\n" +
                        "                                        value:'" + row.country + "',\n" +
                        "                                        onSelect: function(rec){\n" +
                        "                                            if(rec == undefined)return;\n" +
                        "                                            $('#Region_" + id + "').combobox('reload', '/city/get_province?country=' + rec.cityname).combobox('clear');\n" +
                        "                                            $('#CountryValue_" + id + "').val(rec.cityname_en);\n" +
                        "                                        },\n" +
                        "                                    \">\n" +
                        "                                    </select>\n" +
                        "                                    <input type=\"hidden\" name=\"ShippingParties[" + this_key + "][PartyLocation][Country][Value]\" id=\"CountryValue_" + id + "\" value=\"" + row.country_name + "\">\n" +
                        "                                </td>\n" +
                        "                                <td><span>SubDivisionCode</span></td>\n" +
                        "                                <td>\n" +
                        "                                    <select class=\"easyui-combobox input\" id=\"Region_" + id + "\"  name=\"ShippingParties[" + this_key + "][PartyLocation][Region]\" data-options=\"\n" +
                        "                                        valueField:'cityname_en',\n" +
                        "                                        textField:'cityname_en',\n" +
                        "                                        value: '" + row.region + "',\n" +
                        "                                        url:'/city/get_province/0/code?country=" + row.country + "'," +
                        "                                        onLoadSuccess: function(){\n" +
                        "                                            var val = $(this).combobox('getValue');\n" +
                        "                                            $(this).combobox('clear').combobox('select', val);\n" +
                        "                                        },\n" +
                        "                                        onSelect:function(rec){\n" +
                        "                                            if(rec == undefined) return;" +
                        "                                            $('#SubDivisionCode_" + id + "').val(rec.code);\n" +
                        "                                        },\n" +
                        "                                    \">\n" +
                        "                                    </select>\n" +
                        "                                    <input type=\"hidden\" name=\"ShippingParties[" + this_key + "][PartyLocation][SubDivisionCode]\" id=\"SubDivisionCode_" + id + "\" value=\"" + row.region_code + "\">\n" +
                        "                                </td>\n" +
                        "                                <td>City</td>\n" +
                        "                                <td>\n" +
                        "                                    <input required class=\"easyui-textbox input\" name=\"ShippingParties[" + this_key + "][PartyLocation][City]\" id=\"City_" + id + "\" value=\"" + row.city + "\">\n" +
                        "                                </td>\n" +
                        "                                <td>PostalCode</td>\n" +
                        "                                <td>\n" +
                        "                                    <input class=\"easyui-textbox input\" required name=\"ShippingParties[" + this_key + "][PartyLocation][PostalCode]\" id=\"PostalCode_" + id + "\" value=\"" + row.postalcode + "\">\n" +
                        "                                </td>\n" +
                        "                            </tr>\n" +
                        "                            <tr>\n" +
                        "                                <td><h4>ContactPersons</h4></td>\n" +
                        "                            </tr>\n" +
                        "                            <tr>\n" +
                        "                                <td><span class=\"head5\">Person</span></td>\n" +
                        "                            </tr>\n" +
                        "                            <tr>\n" +
                        "                                <td>ContactName</td>\n" +
                        "                                <td>\n" +
                        "                                    <input class=\"easyui-textbox input\" id=\"ContactName_" + id + "\" name=\"ShippingParties[" + this_key + "][ContactPersons][ContactName]\" value=\"" + row.company_contact + "\">\n" +
                        "                                </td>\n" +
                        "                                <td>ContactMail</td>\n" +
                        "                                <td>\n" +
                        "                                    <input class=\"easyui-textbox input\" id=\"ContactMail_" + id + "\" name=\"ShippingParties[" + this_key + "][ContactPersons][ContactMail]\" value=\"" + row.company_email + "\">\n" +
                        "                                </td>\n" +
                        "                                <td>Phone</td>\n" +
                        "                                <td>\n" +
                        "                                    <input class=\"easyui-textbox input\" id=\"Phone_" + id + "\" name=\"ShippingParties[" + this_key + "][ContactPersons][Phone]\" value=\"" + row.company_telephone + "\">\n" +
                        "                                </td>\n" +
                        "                            </tr>\n" +
                        "                        </table>"
                    ShippingParty.append(str);

                    var td = $('<td/>');
                    td.append(ShippingParty);
                    var tr = $('<tr/>').append(td);

                    $('.ShippingParties_content>tbody').append(tr);
                    $.parser.parse(tr);
                }
            },
            onLoadSuccess : function(data) {
                var opts = $(this).combogrid('grid').datagrid('options');
                var vc = $(this).combogrid('grid').datagrid('getPanel').children('div.datagrid-view');
                vc.children('div.datagrid-empty').remove();
                if (!$(this).combogrid('grid').datagrid('getRows').length) {
                    var d = $('<div class="datagrid-empty"></div>').html(opts.emptyMsg || 'no records').appendTo(vc);
                    d.css({
                        position : 'absolute',
                        left : 0,
                        top : 50,
                        width : '100%',
                        fontSize : '14px',
                        textAlign : 'center'
                    });
                }
            },
        };

        $.each($('.House'), function (i, it) {
            var this_inp = $('#ShipperRefCode' + i);
            this_inp.combogrid('reset');
            company_config.company_type = 'shipper';
            company_config.url = '/biz_company/get_options/shipper/' + this_inp.attr('client_code');
            company_config.value = this_inp.val();
            company_config.toolbar = '#' + create_tool('ShipperRefCode' + i, 'ShipperRefCode' + i) + '_div';
            this_inp.combogrid(company_config);

            var this_inp = $('#ConsigneeRefCode' + i);
            this_inp.combogrid('reset');
            company_config.company_type = 'consignee';
            company_config.url = '/biz_company/get_options/consignee/' + this_inp.attr('client_code');
            company_config.value = this_inp.val();
            company_config.toolbar = '#' + create_tool('ConsigneeRefCode' + i, 'ConsigneeRefCode' + i) + '_div';
            this_inp.combogrid(company_config);

            var this_inp = $('#NotifyRefCode' + i);
            this_inp.combogrid('reset');
            company_config.company_type = 'notify';
            company_config.url = '/biz_company/get_options/notify/' + this_inp.attr('client_code');
            company_config.value = this_inp.val();
            company_config.toolbar = '#' + create_tool('NotifyRefCode' + i, 'NotifyRefCode' + i) + '_div';
            this_inp.combogrid(company_config);
        });
    });

    function query_report(div_id, load_id){
        var where = {};
        var form_data = $('#' + div_id + '_form').serializeArray();
        $.each(form_data, function (index, item) {
            if(where.hasOwnProperty(item.name) === true){
                if(typeof where[item.name] == 'string'){
                    where[item.name] =  where[item.name].split(',');
                    where[item.name].push(item.value);
                }else{
                    where[item.name].push(item.value);
                }
            }else{
                where[item.name] = item.value;
            }
        });
        // where['field'] = $('#to_search_field').combo('getValue');
        $('#' + load_id).combogrid('grid').datagrid('load',where);
    }
    //text添加输入值改变
    function keydown_search(e, click_id){
        if(e.keyCode == 13){
            $('#' + click_id).trigger('click');
        }
    }

    function edit_company(id) {
        var val = $('#' + id).combogrid('getValue');
        if(val == ''){
            $.messager.confirm('Tips', '请先选中任意客户信息');
            return;
        }
        window.open('/biz_company/edit/' + val);
    }

    function add_company(id) {
        var client_code = $('#' + id).attr('client_code');
        var options = $('#' + id).combogrid('options');
        window.open('/biz_company/add/' + options.company_type + '/' + client_code);
    }
</script>
<style type="text/css">
    .head5{
        font-weight: bold;
    }
    .b_table{
        width: 100%;
        text-align: left;
    }
    .input{
        width: 200px;
    }
    .input_textarea{
        width: 200px;
        height: 50px;
    }
    .a_table{

    }
    input:read-only,select:read-only,textarea:read-only
    {
        background-color: #d6d6d6;
        cursor: default;
    }
    .ShippingPartyChecked{
        display: table-row;
    }
</style>
<body>
<button onclick="reset_data()" title="初始化数据">初始化</button>
<button onclick="save_data()" title="保存">保存数据</button>
<?php if(empty($message)){ ?>
    <button onclick="create_edi('<?= $action;?>')" title="生成报文">生成报文</button>
<?php }else{?>
    <button onclick="create_edi('<?= $action;?>')" title="生成报文">生成报文</button>
    <br />
    发送情况:
    <?php if($message['status'] == 0){?>
        未发送
    <?php }else if($message['status'] == 1){
        echo $message['send_time'];?>
        发送成功
    <?php }else if($message['status'] == 2){?>
        发送修改成功
    <?php }?>
    <br />
    <button class="read_message" onclick="read_message(<?= $message['id'];?>)">查看报文</button>
    <button class="send" onclick="send_edi('API', '<?= $message['receiver'];?>', <?= $message['id'];?>)">发送报文</button>
<?php }?>
<br/>
<span style="color:red;font-size:20px;">该页面除第一次生成时，后续进入该页面，都是读取上一次的，如果某些信息修改后需要重新获取，请点击初始化</span>
<form id="fm" method="post">
    <h2>Master</h2>
    <input type="hidden" name="Master[FilerCode]" value="<?= $edi_data['Master']['FilerCode'];?>">
    <table class="a_table">
        <tr>
            <td>OPType</td>
            <td>
                <select class="easyui-combobox input" required name="Master[OPType]" data-options="
                    value:'<?= $edi_data['Master']['OPType'];?>',
                ">
                    <option value="OE">海运出口</option>
                    <option value="OI">海运进口</option>
                </select>
            </td>
            <td>FilingType</td>
            <td>
                <select class="easyui-combobox input" id="filing_type" required name="Master[FilingType]" editable="false" data-options="
                    value:'<?= $edi_data['Master']['FilingType'];?>',
                ">
                    <option value="S">In-Transit / TandE Transportation and Exportation</option>
                    <option value="F">Freight Remain on Board</option>
                </select>
            </td>
            <td>MasterBillNo</td>
            <td>
                <input class="easyui-textbox input" required name="Master[MasterBillNo]" value="<?= $edi_data['Master']['MasterBillNo'];?>">
            </td>
        </tr>
        <tr>
            <td>OriginAgentCode</td>
            <td>
                <input class="easyui-textbox input" name="Master[OriginAgentCode]" required readonly value="<?= $edi_data['Master']['OriginAgentCode'];?>">
            </td>
            <td>DestinationAgentCode</td>
            <td>
                <input class="easyui-textbox input" name="Master[DestinationAgentCode]" required readonly value="<?= $edi_data['Master']['DestinationAgentCode'];?>">
            </td>
            <td>CarrierName</td>
            <td>
                <input class="easyui-textbox input" name="Master[CarrierName][code]" required readonly value="<?= $edi_data['Master']['CarrierName']['code'];?>">
                <input type="hidden" name="Master[CarrierName][Value]" value="<?= $edi_data['Master']['CarrierName']['Value'];?>">
            </td>
            <td>ConsignmentType</td>
            <td>
                <input class="easyui-textbox input" name="Master[ConsignmentType]" id="ConsignmentType" required readonly value="<?= $edi_data['Master']['ConsignmentType'];?>">
            </td>
            <td>MasterRefNo</td>
            <td>
                <input class="easyui-textbox input" name="Master[MasterRefNo]" required readonly value="<?= $edi_data['Master']['MasterRefNo'];?>">
            </td>
        </tr>
    </table>
    <table class="a_table">
        <tr>
            <td colspan="10"><h3>RouteInformation</h3></td>
        </tr>
        <tr>
            <td>ArrivalVessel</td>
            <td>IMO</td>
            <td>
                <input class="easyui-textbox input" id="IMO" name="Master[RouteInformation][ArrivalVessel][IMO]" required value="<?= $edi_data['Master']['RouteInformation']['ArrivalVessel']['IMO'];?>">
            </td>
            <td>voyage</td>
            <td>
                <input class="easyui-textbox input" name="Master[RouteInformation][ArrivalVessel][voyage]" required value="<?= $edi_data['Master']['RouteInformation']['ArrivalVessel']['voyage'];?>">
            </td>
            <td>vessel</td>
            <td>
                <select class="easyui-combogrid input vessel" name="Master[RouteInformation][ArrivalVessel][Value]" data-options="
                    required:true,
                	idField:'value',
					textField:'value',
					panelWidth:450,
					panelHeight:345,
					pagination:true,
					mode:'remote',
					url:'/bsc_dict/get_option_limit/vessel',
                    columns:[[
                        {field:'name',title:'name',width:200},
                        {field:'ext1',title:'ext1',width:120},
                    ]],
                    value:'<?= $edi_data['Master']['RouteInformation']['ArrivalVessel']['Value'];?>',
                    onSelect: function(index, row){
					    if(row != undefined){
                            $('#IMO').textbox('setValue', row.ext1);
					    }
					},
                ">
                </select>
            </td>
        </tr>
        <tr>
            <td>LoadingPort</td>
            <td>code</td>
            <td>
                <select class="easyui-combobox input port" id="LoadingPortcode" name="Master[RouteInformation][LoadingPort][code]" required data-options="
                	valueField:'port_code',
					textField:'port_code',
                    value:'<?= $edi_data['Master']['RouteInformation']['LoadingPort']['code'];?>',
                    onSelect: function(rec){
					    if(rec != undefined){
                            $('#LoadingPortValue').combobox('setValue', rec.port_name);
					    }
					},
                    onHidePanel: function() {
						var valueField = $(this).combobox('options').valueField;
						var val = $(this).combobox('getValue');
						var allData = $(this).combobox('getData');
						var result = true;
						for (var i = 0; i < allData.length; i++) {
							if (val == allData[i][valueField]) {
								result = false;
							}
						}
						if (result) {
							$(this).combobox('clear');
						}
					},
                ">
                </select>
            </td>
            <td>date</td>
            <td>
                <input class="easyui-datebox input" name="Master[RouteInformation][LoadingPort][date]" required value="<?= $edi_data['Master']['RouteInformation']['LoadingPort']['date'];?>">
            </td>
            <td>name</td>
            <td>
                <select class="easyui-combobox input port" id="LoadingPortValue" name="Master[RouteInformation][LoadingPort][Value]" required data-options="
                	valueField:'port_name',
					textField:'port_name',
                    value:'<?= $edi_data['Master']['RouteInformation']['LoadingPort']['Value'];?>',
                    onSelect: function(rec){
					    if(rec != undefined){
                            $('#LoadingPortcode').combobox('setValue', rec.port_code);
					    }
					},
                    onHidePanel: function() {
						var valueField = $(this).combobox('options').valueField;
						var val = $(this).combobox('getValue');
						var allData = $(this).combobox('getData');
						var result = true;
						for (var i = 0; i < allData.length; i++) {
							if (val == allData[i][valueField]) {
								result = false;
							}
						}
						if (result) {
							$(this).combobox('clear');
						}
					},
                ">
                </select>
            </td>
        </tr>
        <tr>
            <td>DischargePort</td>
            <td>code</td>
            <td>
                <select class="easyui-combobox input port" id="DischargePortcode" name="Master[RouteInformation][DischargePort][code]" required data-options="
                	valueField:'port_code',
					textField:'port_code',
                    value:'<?= $edi_data['Master']['RouteInformation']['DischargePort']['code'];?>',
                    onSelect: function(rec){
					    if(rec != undefined){
                            $('#DischargePortValue').combobox('setValue', rec.port_name);
					    }
					},
                    onHidePanel: function() {
						var valueField = $(this).combobox('options').valueField;
						var val = $(this).combobox('getValue');
						var allData = $(this).combobox('getData');
						var result = true;
						for (var i = 0; i < allData.length; i++) {
							if (val == allData[i][valueField]) {
								result = false;
							}
						}
						if (result) {
							$(this).combobox('clear');
						}
					},
                ">
                </select>
            </td>
            <td>date</td>
            <td>
                <input class="easyui-datebox input" name="Master[RouteInformation][DischargePort][date]" required value="<?= $edi_data['Master']['RouteInformation']['DischargePort']['date'];?>">
            </td>
            <td>name</td>
            <td>
                <select class="easyui-combobox input port" id="DischargePortValue" name="Master[RouteInformation][DischargePort][Value]" required data-options="
                	valueField:'port_name',
					textField:'port_name',
                    value:'<?= $edi_data['Master']['RouteInformation']['DischargePort']['Value'];?>',
                    onSelect: function(rec){
					    if(rec != undefined){
                            $('#DischargePortcode').combobox('setValue', rec.port_code);
					    }
					},
                    onHidePanel: function() {
						var valueField = $(this).combobox('options').valueField;
						var val = $(this).combobox('getValue');
						var allData = $(this).combobox('getData');
						var result = true;
						for (var i = 0; i < allData.length; i++) {
							if (val == allData[i][valueField]) {
								result = false;
							}
						}
						if (result) {
							$(this).combobox('clear');
						}
					},
                ">
                </select>
            </td>
        </tr>
        <tr>
            <td>DestinationPort</td>
            <td>code</td>
            <td>
                <select class="easyui-combobox input port" id="DestinationPortcode" name="Master[RouteInformation][DestinationPort][code]" data-options="
                	valueField:'port_code',
					textField:'port_code',
                    value:'<?= isset($edi_data['Master']['RouteInformation']['DestinationPort']['code']) ? $edi_data['Master']['RouteInformation']['DestinationPort']['code'] : '';?>',
                    onSelect: function(rec){
					    if(rec != undefined){
                            $('#DestinationPortValue').combobox('setValue', rec.port_name);
					    }
					},
                    onHidePanel: function() {
						var valueField = $(this).combobox('options').valueField;
						var val = $(this).combobox('getValue');
						var allData = $(this).combobox('getData');
						var result = true;
						for (var i = 0; i < allData.length; i++) {
							if (val == allData[i][valueField]) {
								result = false;
							}
						}
						if (result) {
							$(this).combobox('clear');
						}
					},
                ">
                </select>
            </td>
            <td>date</td>
            <td>
                <input class="easyui-datebox input" name="Master[RouteInformation][DestinationPort][date]" value="<?= isset($edi_data['Master']['RouteInformation']['DestinationPort']['date']) ? $edi_data['Master']['RouteInformation']['DestinationPort']['date'] : '';?>">
            </td>
            <td>name</td>
            <td>
                <select class="easyui-combobox input port" id="DestinationPortValue" name="Master[RouteInformation][DestinationPort][Value]" data-options="
                	valueField:'port_name',
					textField:'port_name',
                    value:'<?= isset($edi_data['Master']['RouteInformation']['DestinationPort']['Value']) ? $edi_data['Master']['RouteInformation']['DestinationPort']['Value'] : '';?>',
                    onSelect: function(rec){
					    if(rec != undefined){
                            $('#DestinationPortcode').combobox('setValue', rec.port_code);
					    }
					},
                    onHidePanel: function() {
						var valueField = $(this).combobox('options').valueField;
						var val = $(this).combobox('getValue');
						var allData = $(this).combobox('getData');
						var result = true;
						for (var i = 0; i < allData.length; i++) {
							if (val == allData[i][valueField]) {
								result = false;
							}
						}
						if (result) {
							$(this).combobox('clear');
						}
					},
                ">
                </select>
            </td>
        </tr>
    </table>
    <table class="a_table">
        <tr>
            <td><h3>MasterContainers</h3></td>
        </tr>

        <?php foreach ($edi_data['Master']['MasterContainers'] as $key => $container){ ?>
            <tr>
                <td><h4>Container <?= $key+1;?></h4></td>
            </tr>
            <tr>
                <td>ContainerNo</td>
                <td>
                    <input class="easyui-textbox input" required readonly name="Master[MasterContainers][<?= $key;?>][ContainerNo]" value="<?= $container['ContainerNo'];?>">
                </td>
                <td>ContainerType</td>
                <td>
                    <input class="easyui-textbox input" required readonly name="Master[MasterContainers][<?= $key;?>][ContainerType]" value="<?= $container['ContainerType'];?>">
                </td>
                <td>SealNo</td>
                <td>
                    <input class="easyui-textbox input" required readonly name="Master[MasterContainers][<?= $key;?>][SealNo]" value="<?= $container['SealNo'];?>">
                    <input type="hidden" name="Master[MasterContainers][<?= $key;?>][ServiceCode]" value="<?= $container['ServiceCode'];?>">
                    <input type="hidden" name="Master[MasterContainers][<?= $key;?>][ShipperOwned]" value="<?= $container['ShipperOwned'];?>">
                </td>
            </tr>
        <?php } ?>
    </table>
    <table class="b_table">
        <tr>
            <td><h3>Houses</h3></td>
        </tr>
        <?php foreach ($edi_data['Master']['Houses'] as $key => $House){ ?>
            <tr class="House">
                <td>
                    <fieldset>
                        <legend><h4>House <?= $key+1;?></h4></legend>
                        <table>
                            <tr>
                                <td>HouseNo</td>
                                <td>
                                    <input class="easyui-textbox input" required readonly name="Master[Houses][<?= $key;?>][HouseNo]" value="<?= $House['HouseNo'];?>">
                                    <input type="hidden" name="Master[Houses][<?= $key;?>][OriginalHouseNo]" value="<?= $House['OriginalHouseNo'];?>">
                                </td>
                                <td>
                                    ShipperRefCode
                                    <a href="javascript:void(0);" onclick="edit_company('ShipperRefCode<?= $key;?>')">E</a>/<a href="javascript:void(0);" onclick="add_company('ShipperRefCode<?= $key;?>')">A</a>
                                </td>
                                <td>
                                    <input class="easyui-combogrid input" id="ShipperRefCode<?= $key;?>" name="Master[Houses][<?= $key;?>][ShipperRefCode]" client_code="<?= $shipments[$House['HouseNo']]['client_code'];?>" value="<?= $House['ShipperRefCode'];?>">
                                </td>
                                <td>
                                    ConsigneeRefCode
                                    <a href="javascript:void(0);" onclick="edit_company('ConsigneeRefCode<?= $key;?>')">E</a>/<a href="javascript:void(0);" onclick="add_company('ConsigneeRefCode<?= $key;?>')">A</a>
                                </td>
                                <td>
                                    <input class="easyui-combogrid input" id="ConsigneeRefCode<?= $key;?>" name="Master[Houses][<?= $key;?>][ConsigneeRefCode]" client_code="<?= $shipments[$House['HouseNo']]['client_code'];?>" value="<?= $House['ConsigneeRefCode'];?>">
                                </td>
                                <td>
                                    NotifyRefCode
                                    <a href="javascript:void(0);" onclick="edit_company('NotifyRefCode<?= $key;?>')">E</a>/<a href="javascript:void(0);" onclick="add_company('NotifyRefCode<?= $key;?>')">A</a>
                                </td>
                                <td>
                                    <input class="easyui-combogrid input" id="NotifyRefCode<?= $key;?>" name="Master[Houses][<?= $key;?>][NotifyRefCode]" client_code="<?= $shipments[$House['HouseNo']]['client_code'];?>" value="<?= $House['NotifyRefCode'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>PlaceReceipt[code]</td>
                                <td>
                                    <select class="easyui-combobox input port" id="PlaceReceiptcode<?= $key;?>" name="Master[Houses][<?= $key;?>][PlaceReceipt][code]" required data-options="
                                	valueField:'port_code',
                					textField:'port_code',
                                    value:'<?= $House['PlaceReceipt']['code'];?>',
                                    onSelect: function(rec){
                					    if(rec != undefined){
                                            $('#PlaceReceiptValue<?= $key;?>').combobox('setValue', rec.port_name);
                					    }
                					},
                                    onHidePanel: function() {
                						var valueField = $(this).combobox('options').valueField;
                						var val = $(this).combobox('getValue');
                						var allData = $(this).combobox('getData');
                						var result = true;
                						for (var i = 0; i < allData.length; i++) {
                							if (val == allData[i][valueField]) {
                								result = false;
                							}
                						}
                						if (result) {
                							$(this).combobox('clear');
                						}
                					},
                                ">
                                    </select>
                                </td>
                                <td>PlaceReceipt[date]</td>
                                <td>
                                    <input class="easyui-datebox input" required name="Master[Houses][<?= $key;?>][PlaceReceipt][date]" value="<?= $House['PlaceReceipt']['date'];?>">
                                </td>
                                <td>PlaceReceipt[name]</td>
                                <td>
                                    <select class="easyui-combobox input port" id="PlaceReceiptValue<?= $key;?>" name="Master[Houses][<?= $key;?>][PlaceReceipt][Value]" required data-options="
                                	valueField:'port_name',
                					textField:'port_name',
                                    value:'<?= $House['PlaceReceipt']['Value'];?>',
                                    onSelect: function(rec){
                					    if(rec != undefined){
                                            $('#PlaceReceiptcode<?= $key;?>').combobox('setValue', rec.port_code);
                					    }
                					},
                                    onHidePanel: function() {
                						var valueField = $(this).combobox('options').valueField;
                						var val = $(this).combobox('getValue');
                						var allData = $(this).combobox('getData');
                						var result = true;
                						for (var i = 0; i < allData.length; i++) {
                							if (val == allData[i][valueField]) {
                								result = false;
                							}
                						}
                						if (result) {
                							$(this).combobox('clear');
                						}
                					},
                                ">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><h4>HouseContainers</h4></td>
                            </tr>
                            <?php if(isset($House['HouseContainers'])) foreach ($House['HouseContainers'] as $hc_k => $HouseContainer){ ?>
                                <tr>
                                    <td><h5>Container <?= $hc_k+1;?></h5></td>
                                </tr>
                                <tr>
                                    <td>ContainerRefNo</td>
                                    <td>
                                        <input class="easyui-textbox input" required name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][ContainerRefNo]" value="<?= $HouseContainer['ContainerRefNo'];?>">
                                    </td>
                                    <td>Package</td>
                                    <td>
                                        <input class="easyui-textbox input" required name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][Package]" value="<?= $HouseContainer['Package'];?>">
                                    </td>
                                    <td>Unit</td>
                                    <td>
                                        <select class="easyui-combobox input" required name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][Unit]" data-options="
                                            required: true,
                                            valueField:'name',
                                            textField:'name',
                                            value:'<?= $HouseContainer['Unit'];?>',
                                            url: '/bsc_dict/get_option/packing_unit',
                                            onHidePanel: function() {
                                                var valueField = $(this).combobox('options').valueField;
                                                var val = $(this).combobox('getValue');
                                                var allData = $(this).combobox('getData');
                                                var result = true;
                                                for (var i = 0; i < allData.length; i++) {
                                                    if (val == allData[i][valueField]) {
                                                        result = false;
                                                    }
                                                }
                                                if (result) {
                                                    $(this).combobox('clear');
                                                }
                                            },
                                            onSelect: function (data) {
                                                var good_outers = $('#good_outers').val();
                                                var all = $('#good_outers_unit').combobox('getData');
                                                var name1 = data.name;
                                                var name2 = data.name;
                                                $.each(all,function(index,item){
                                                       if(data.value == item.value && data.name != item.name){
                                                            name2 = item.name;
                                                       }
                                                })
                                                var len1 = name1.length;
                                                var len2 = name2.length;
                                                if(good_outers > 1){
                                                    if(len2>len1){
                                                        $('#good_outers_unit').combobox('setValue', name2);
                                                    }
                                                }else{
                                                    if(len2<len1){
                                                        $('#good_outers_unit').combobox('setValue', name2);
                                                    }
                                                }
                                            }
                                        ">
                                        </select>
                                    </td>
                                    <td>GrossWeight</td>
                                    <td>
                                        <input class="easyui-textbox input" required name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][GrossWeight]" value="<?= $HouseContainer['GrossWeight'];?>">
                                    </td>
                                    <td>CBM</td>
                                    <td>
                                        <input class="easyui-textbox input" required name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][CBM]" value="<?= $HouseContainer['CBM'];?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td>CountryOrigin[code]</td>
                                    <td>
                                        <input class="easyui-textbox input" readonly required name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][CountryOrigin][code]" value="<?= $HouseContainer['CountryOrigin']['code'];?>">
                                    </td>
                                    <td>CountryOrigin[Value]</td>
                                    <td>
                                        <input class="easyui-textbox input" readonly required name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][CountryOrigin][Value]" value="<?= $HouseContainer['CountryOrigin']['Value'];?>">
                                    </td>
                                    <td>Marking</td>
                                    <td>
                                        <textarea class="input_textarea" name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][Marking]"><?= $HouseContainer['Marking'];?></textarea>
                                    </td>
                                    <td>Description</td>
                                    <td>
                                        <textarea class="input_textarea" name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][Description]"><?= $HouseContainer['Description'];?></textarea>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                        <?php
                        $ShippingParties = $edi_data['ShippingParties'];
                        foreach ($edi_data['ShippingParties'] as $key => $ShippingParty){
                            $shippingCode = array($House['ConsigneeRefCode'], $House['ShipperRefCode'], $House['NotifyRefCode']);
                            unset($ShippingParties[$key]);
                            if(!in_array($ShippingParty['PartyCode'], $shippingCode)) continue;
                            $ShippingParty_name = 'ShippingParty';
                            if($House['ConsigneeRefCode'] == $ShippingParty['PartyCode']) $ShippingParty_name = 'Consignee';
                            if($House['ShipperRefCode'] == $ShippingParty['PartyCode']) $ShippingParty_name = 'Shipper';
                            if($House['NotifyRefCode'] == $ShippingParty['PartyCode']) $ShippingParty_name = 'Notify';
                            ?>
                            <fieldset class="ShippingParty">
                                <legend><h4><?= $ShippingParty_name;?></h4></legend>
                                <table>
                                    <tr>
                                        <td>PartyCode</td>
                                        <td>
                                            <input class="easyui-textbox input" required readonly name="ShippingParties[<?= $key;?>][PartyCode]" id="PartyCode_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['PartyCode'];?>">
                                        </td>
                                        <td>PartyType</td>
                                        <td>
                                            <input class="easyui-textbox input" required readonly name="ShippingParties[<?= $key;?>][PartyType]" id="PartyType_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['PartyType'];?>">
                                        </td>
                                        <td>PartyName</td>
                                        <td>
                                            <textarea class="input_textarea" name="ShippingParties[<?= $key;?>][PartyName]" id="PartyName_<?= $ShippingParty['PartyCode'];?>" required><?= $ShippingParty['PartyName'];?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><h4>PartyLocation</h4></td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td>
                                            <textarea class="input_textarea" name="ShippingParties[<?= $key;?>][PartyLocation][Address]" id="Address_<?= $ShippingParty['PartyCode'];?>" required><?= $ShippingParty['PartyLocation']['Address'];?></textarea>
                                        </td>
                                        <td>Country</td>
                                        <td>
                                            <select class="easyui-combobox country input" required name="ShippingParties[<?= $key;?>][PartyLocation][Country][code]" id="CountryCode_<?= $ShippingParty['PartyCode'];?>" data-options="
                                            valueField:'code',
                                            textField:'namecode_en',
                                            value:'<?= $ShippingParty['PartyLocation']['Country']['code'];?>',
                                            onSelect: function(rec){
                                                if(rec == undefined)return;
                                                $('#Region_<?= $ShippingParty['PartyCode'];?>').combobox('reload', '/city/get_province?country=' + rec.cityname).combobox('clear');
                                                $('#CountryValue_<?= $ShippingParty['PartyCode'];?>').val(rec.namecode_en);
                                            },
                                        ">
                                            </select>
                                            <input type="hidden" name="ShippingParties[<?= $key;?>][PartyLocation][Country][Value]" id="CountryValue_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['PartyLocation']['Country']['Value'];?>">
                                        </td>
                                        <td><span>SubDivisionCode</span></td>
                                        <td>
                                            <select class="easyui-combobox input" id="Region_<?= $ShippingParty['PartyCode'];?>"  name="ShippingParties[<?= $key;?>][PartyLocation][Region]" data-options="
                                                valueField:'cityname_en',
                                                textField:'cityname_en',
                                                value: '<?= $ShippingParty['PartyLocation']['Region'];?>',
                                                <?php if(!empty($ShippingParty['PartyLocation']['Country']['code'])) echo 'url:\'/city/get_province/0/code?country=' . $ShippingParty['PartyLocation']['Country']['code'] . '\',';?>
                                                onLoadSuccess: function(){
                                                    var val = $(this).combobox('getValue');
                                                    $(this).combobox('clear').combobox('select', val);
                                                },
                                                onSelect:function(rec){
                                                    if(rec == undefined) return;
                                                    $('#SubDivisionCode_<?= $ShippingParty['PartyCode'];?>').val(rec.code);
                                                },
                                            ">
                                            </select>
                                            <input type="hidden" name="ShippingParties[<?= $key;?>][PartyLocation][SubDivisionCode]" id="SubDivisionCode_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['PartyLocation']['SubDivisionCode'];?>">
                                        </td>
                                        <td>City</td>
                                        <td>
                                            <input required class="easyui-textbox input" name="ShippingParties[<?= $key;?>][PartyLocation][City]" id="City_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['PartyLocation']['City'];?>">
                                        </td>

                                        <td>PostalCode</td>
                                        <td>
                                            <input class="easyui-textbox input" required name="ShippingParties[<?= $key;?>][PartyLocation][PostalCode]" id="PostalCode_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['PartyLocation']['PostalCode'];?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><h4>ContactPersons</h4></td>
                                    </tr>
                                    <tr>
                                        <td><span class="head5">Person</span></td>
                                    </tr>
                                    <tr>
                                        <td>ContactName</td>
                                        <td>
                                            <input class="easyui-textbox input" name="ShippingParties[<?= $key;?>][ContactPersons][ContactName]" id="ContactName_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['ContactPersons']['ContactName'];?>">
                                        </td>
                                        <td>ContactMail</td>
                                        <td>
                                            <input class="easyui-textbox input" name="ShippingParties[<?= $key;?>][ContactPersons][ContactMail]" id="ContactMail_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['ContactPersons']['ContactMail'];?>">
                                        </td>
                                        <td>Phone</td>
                                        <td>
                                            <input class="easyui-textbox input" name="ShippingParties[<?= $key;?>][ContactPersons][Phone]" id="Phone_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['ContactPersons']['Phone'];?>">
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        <?php } ?>
                    </fieldset>
                </td>
            </tr>
        <?php } ?>
    </table>
    <table class="b_table ShippingParties_content">
        <tr>
            <td><h3>ShippingParties</h3></td>
        </tr>
        <?php
        foreach ($ShippingParties as $key => $ShippingParty){
            ?>
            <tr>
                <td>
                    <fieldset class="ShippingParty">
                        <legend><h4>ShippingParty <?= $key + 1;?></h4></legend>
                        <table>
                            <tr>
                                <td>PartyCode</td>
                                <td>
                                    <input class="easyui-textbox input" required readonly name="ShippingParties[<?= $key;?>][PartyCode]" id="PartyCode_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['PartyCode'];?>">
                                </td>
                                <td>PartyType</td>
                                <td>
                                    <input class="easyui-textbox input" required readonly name="ShippingParties[<?= $key;?>][PartyType]" id="PartyType_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['PartyType'];?>">
                                </td>
                                <td>PartyName</td>
                                <td>
                                    <textarea class="input_textarea" name="ShippingParties[<?= $key;?>][PartyName]" id="PartyName_<?= $ShippingParty['PartyCode'];?>" required><?= $ShippingParty['PartyName'];?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td><h4>PartyLocation</h4></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>
                                    <textarea class="input_textarea" name="ShippingParties[<?= $key;?>][PartyLocation][Address]" id="Address_<?= $ShippingParty['PartyCode'];?>" required><?= $ShippingParty['PartyLocation']['Address'];?></textarea>
                                </td>
                                <td>Country</td>
                                <td>
                                    <select class="easyui-combobox country input" required name="ShippingParties[<?= $key;?>][PartyLocation][Country][code]" id="Countrycode_<?= $ShippingParty['PartyCode'];?>" data-options="
                                        valueField:'cityname_en',
                                        textField:'namecode_en',
                                        value:'<?= $ShippingParty['PartyLocation']['Country']['code'];?>',
                                        onSelect: function(rec){
                                            if(rec == undefined)return;
                                            $('#Region_<?= $ShippingParty['PartyCode'];?>').combobox('reload', '/city/get_province?country=' + rec.cityname).combobox('clear');
                                            $('#CountryValue_<?= $ShippingParty['PartyCode'];?>').val(rec.cityname_en);
                                        },
                                    ">
                                    </select>
                                    <input type="hidden" name="ShippingParties[<?= $key;?>][PartyLocation][Country][Value]" id="CountryValue_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['PartyLocation']['Country']['Value'];?>">
                                </td>
                                <td><span>SubDivisionCode</span></td>
                                <td>
                                    <select class="easyui-combobox input" id="Region_<?= $ShippingParty['PartyCode'];?>"  name="ShippingParties[<?= $key;?>][PartyLocation][Region]" data-options="
                                        valueField:'cityname_en',
                                        textField:'cityname_en',
                                        value: '<?= $ShippingParty['PartyLocation']['Region'];?>',
                                        <?php if(!empty($ShippingParty['PartyLocation']['Country']['code'])) echo 'url:\'/city/get_province/0/code?country=' . $ShippingParty['PartyLocation']['Country']['code'] . '\',';?>
                                        onLoadSuccess: function(){
                                            var val = $(this).combobox('getValue');
                                            $(this).combobox('clear').combobox('select', val);
                                        },
                                        onSelect:function(rec){
                                            if(rec == undefined) return;
                                            $('#SubDivisionCode_<?= $ShippingParty['PartyCode'];?>').val(rec.code);
                                        },
                                    ">
                                    </select>
                                    <input type="hidden" name="ShippingParties[<?= $key;?>][PartyLocation][SubDivisionCode]" id="SubDivisionCode_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['PartyLocation']['SubDivisionCode'];?>">
                                </td>
                                <td>City</td>
                                <td>
                                    <input required class="easyui-textbox input" name="ShippingParties[<?= $key;?>][PartyLocation][City]" id="City_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['PartyLocation']['City'];?>">
                                </td>

                                <td>PostalCode</td>
                                <td>
                                    <input class="easyui-textbox input" required name="ShippingParties[<?= $key;?>][PartyLocation][PostalCode]" id="PostalCode_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['PartyLocation']['PostalCode'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td><h4>ContactPersons</h4></td>
                            </tr>
                            <tr>
                                <td><span class="head5">Person</span></td>
                            </tr>
                            <tr>
                                <td>ContactName</td>
                                <td>
                                    <input class="easyui-textbox input" name="ShippingParties[<?= $key;?>][ContactPersons][ContactName]" id="ContactName_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['ContactPersons']['ContactName'];?>">
                                </td>
                                <td>ContactMail</td>
                                <td>
                                    <input class="easyui-textbox input" name="ShippingParties[<?= $key;?>][ContactPersons][ContactMail]" id="ContactMail_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['ContactPersons']['ContactMail'];?>">
                                </td>
                                <td>Phone</td>
                                <td>
                                    <input class="easyui-textbox input" name="ShippingParties[<?= $key;?>][ContactPersons][Phone]" id="Phone_<?= $ShippingParty['PartyCode'];?>" value="<?= $ShippingParty['ContactPersons']['Phone'];?>">
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        <?php } ?>

    </table>
</form>
</body>