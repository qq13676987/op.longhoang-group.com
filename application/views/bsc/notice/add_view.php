<style type="text/css">
    .fs{
        width: 500px;
    }
    .fs_input{
        width: 255px;
    }
</style>
<script type="text/javascript">
    $('#fs_table').form({
        url: '/bsc_notice/add_data',
        onSubmit: function(){
            $('#notice_user_group').val($('#notice_user_group1').combotree('getValues'));
            return true;
        },
        success:function(res_json){
            var res;
            try{
                res = $.parseJSON(res_json);
            }catch (e) {

            }
            if(res === undefined){
                alert('发生错误,请联系管理员');
                $('.icon-save').attr('onclick', "$('#f').form('submit');");
            }else{
                alert(res.msg);
                $('#hs_code_tip').html('');
                $('.icon-save').attr('onclick', "$('#f').form('submit');");
                if(res.code == 0){
                    try{opener.$('#tt').edatagrid('reload');opener.$('#add_notice').window('close');}catch (e) {}
                    $('#tt').edatagrid('reload');
                    $('#add_notice').window('close');
                }
            }
        }
    });
    // $('#notice_href').textbox('textbox').focus(function () {
    //     $('#notice_href_help').tooltip('show');
    // }).blur(function () {
    //     $('#notice_href_help').tooltip('hide');
    // });
</script>
<fieldset class="fs">
    <legend>发送通知</legend>
    <form id="fs_table" method="POST">
        <table class="fs_table">
            <tr>
                <td><?= lang('start');?></td>
                <td>
                    <input class="easyui-datetimebox fs_input" name="start">
                </td>
            </tr>
            <tr>
                <td><?= lang('end');?></td>
                <td>
                    <input class="easyui-datetimebox fs_input" name="end">
                </td>
            </tr>
            <tr>
                <td><?= lang('txt');?></td>
                <td>
                    <textarea name="txt" class="fs_input" style="height:80px;"></textarea>
                </td>
            </tr>
            <tr>
                <td><?= lang('href');?></td>
                <td>
                    <input class="easyui-textbox fs_input" id="notice_href" name="href">
                    <a href="javascript:void(0);" id="notice_href_help" class="easyui-tooltip" data-options="
                    content: $('<div></div>'),
                    onShow: function(){
                        $(this).tooltip('arrow').css('left', 20);
                        $(this).tooltip('tip').css('left', $(this).offset().left);
                    },
                    onUpdate: function(cc){
                        cc.panel({
                            width: 500,
                            height: 'auto',
                            border: false,
                            href: '/bsc_help_content/help/notice_href'
                        });
                    }
                "><?= lang('help');?></a>
                </td>
            </tr>
            <tr>
                <td><?= lang('notice_user');?></td>
                <td>
                    <select name="notice_user" id="notice_user" class="easyui-combotree fs_input" data-options="
                    editable:true,
                    multiple:true,
                    valueField:'id',
                    textField:'text',
                    cascadeCheck: false,
                    url:'/bsc_user/get_user_tree/',
                    onBeforeCheck: function(node,checked){
                        var values = $('#notice_user').combotree('getValues');
                        if(checked && node.children != undefined){
                            $.each(node.children, function(index, item){
                                if(item.children == undefined && $.inArray(item.id, values) == -1){
                                    values.push(item.id);
                                }else{
                                    $.each(item.children, function(i, it){
                                        if(it.children == undefined && $.inArray(it.id, values) == -1)values.push(it.id);

                                    })
                                }
                            });
                            $('#notice_user').combotree('setValues', values);
                            return false;
                        }
                    },
                ">

                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('notice_user_group');?></td>
                <td>
                    <select class="easyui-combotree fs_input" id="notice_user_group1" editable="true" data-options="
                    multiple:true,
                    valueField:'id',
                    textField:'text',
                    url:'/bsc_user/get_user_tree1',
                    
                ">
                    </select>
                    <input type="hidden" name="notice_user_group" id="notice_user_group">
                </td>
            </tr>
            <tr>
                <td><?= lang('status');?></td>
                <td>
                    <input type="radio" name="status" value="0" checked><?= lang('normal');?>
                    <input type="radio" name="status" value="1"><?= lang('disable');?>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right;">
                    <button type="button" onclick="javascript:$('#fs_table').form('submit');" style="margin-right: 30px;"><?= lang('submit');?></button>
                </td>
            </tr>
        </table>
    </form>
</fieldset>