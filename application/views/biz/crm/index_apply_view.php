<style type="text/css">
    .f{
        width:115px;
    }
    .s{
        width:100px;
    }
    .v{
        width:120px;
    }

</style>

<script type="text/javascript">
    function doSearch() {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            json[item.name] = item.value;
        });
        $('#tt').datagrid('load', json);
        $('#chaxun').window('close');
    }

    function reset(){
        $('#cx').form('reset');
    }

    $(function () {
        var selectIndex = -1;
        $('#tt').edatagrid({
            url: '/biz_crm/get_index_apply_data',
            // destroyUrl: '/biz_client_apply/delete_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            }
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
            onDblClickRow: function (rowIndex) {
                $(this).datagrid('selectRow', rowIndex);
                var row = $('#tt').datagrid('getSelected');
                if (row) {
                    url = '/biz_crm/edit/' + row.id;
                    window.open(url);
                }
            },
            onClickRow: function (index, data) {
                if (index == selectIndex) {
                    $(this).datagrid('unselectRow', index);
                    selectIndex = -1;
                } else {
                    selectIndex = index;
                }
            }

        });
        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
        //新增查询列
        $('#cx table').on('click' , '.add_tr', function () {
            var index = $('.f').length;
            var str = '<tr><td><?= lang('query');?></td><td align="right"><select name="field[f][' + index + ']" class="f easyui-combobox"  required="true" data-options="' +
                'onChange:function(newVal, oldVal){\n' +
                '   localStorage.setItem(\'search_f\' + $(this).parents(\'tr\').index() + \'_\' + path, newVal);\n' +
                '}' +
                '">' +
                "<?php foreach($f as $rs){
                    $item = $rs[0];
                    echo "<option value='$item'>" . lang($item) . "</option>";
                }?>" +
                '</select> &nbsp; <select name="field[s][' + index + ']" class="s easyui-combobox"  required="true"  data-options="\n' +
                'onChange:function(newVal, oldVal){\n' +
                '   localStorage.setItem(\'search_s\' + $(this).parents(\'tr\').index() + \'_\' + path, newVal);\n' +
                '}\n' +
                '">' +
                '<option value="like">like</option> <option value="=">=</option> <option value=">=">>=</option> <option value="<="><=</option> <option value="!=">!=</option> </select> &nbsp; <input name="field[v][' + index + ']" class="v easyui-textbox"> <button class="add_tr">+</button> </td> </tr>';
            $('#cx table tbody').append(str);
            $.parser.parse($('#cx table tbody tr:last-child'));
            var this_sf_f = localStorage.getItem('search_f' + $('.f:last').parents('tr').index() + '_' + path);
            var this_sf_s = localStorage.getItem('search_s' + $('.f:last').parents('tr').index() + '_' + path);
            if(this_sf_f != null)$('.f:last').combobox('setValue', this_sf_f);
            if(this_sf_s != null)$('.s:last').combobox('setValue', this_sf_s);
            return false;
        });
        if(localStorage.getItem('search_f' + $('.f:last').parents('tr').index() + '_' + path) != null)$('.f:last').combobox('setValue', localStorage.getItem('search_f' + $('.f:last').parents('tr').index() + '_' + path));
        if(localStorage.getItem('search_s' + $('.f:last').parents('tr').index() + '_' + path) != null)$('.s:last').combobox('setValue', localStorage.getItem('search_s' + $('.f:last').parents('tr').index() + '_' + path));

    });

    function add() {
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            window.open("/biz_client_apply/add");
            // window.open("/biz_client/add/" + row.id);
        } else {
            window.open("/biz_client_apply/add");
        }
    }
    
    function client_apply(){
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            window.open("/biz_client_apply/add");
            // window.open("/biz_client/add/" + row.id);
        } else {
            window.open("/biz_client_apply/add");
        }
    }
    

    function del() {
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            //if(row.step!="未收款"){
            //	alert("不可以删除");
            //}else{
            $('#tt').edatagrid('destroyRow');
            //}

        } else {
            alert("No Item Selected");
        }
    }
    function approved(){


        var row = $('#tt').datagrid('getSelected');
        if (row){
            $.messager.confirm('Tips', '是否确认通过申请', function (r) {
                if(r){
                    $.post("/biz_crm/approved_client", {id:row.id}, function(data){
                        if(data.code == 0){
                            $('#tt').datagrid('reload');
                        }else{
                            $.messager.alert('Tips', data.msg);
                        }
                    },"Json");
                }
            });
        } else {
            alert("No Item Selected");
        }
    }
</script>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false" >
    <thead>
    <tr>
        <?php
        if (!empty($f)) {
            foreach ($f as $rs) {

                $field = $rs[0];
                $disp = $rs[1];
                $width = $rs[2];
                $sortable = empty($rs[4]) ? 'false' : 'true';

                if ($width == "0") continue;
                if ($field == "id" || $field == "job_no") {
                    echo "<th field=\"$field\" width=\"$width\" sortable=\"true\" align=\"center\" >" . lang($disp) . "</th>";
                    continue;
                }
                if ($field == "apply_type") {
                    echo "<div  style=\"display:none;visibility:hidden\" >" . lang($disp) . "</div>";
                    continue;
                }
                echo "<th field=\"$field\" width=\"$width\" align=\"center\" >" . lang($disp) . "</th>";
            }
        }
        ?>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" plain="true" onclick="approved();"><?= lang('通过申请'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true"
                   onclick="window.open('/sys_config_title/index/biz_crm_index_apply')"><?= lang('表头设置'); ?></a>
            </td>
        </tr>
    </table>
</div>




