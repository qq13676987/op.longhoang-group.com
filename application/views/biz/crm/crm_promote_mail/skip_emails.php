<script type="text/javascript">
    $(function () {
        $('#tt').edatagrid({
            url: '/crm_promote_mail/get_skip_data',
            saveUrl: '/crm_promote_mail/add_skip_data',
            updateUrl: '/crm_promote_mail/update_skip_data',
            destroyUrl: '/crm_promote_mail/delete_skip_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            }
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height()
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });

    });

</script>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th data-options="field:'id',width:'70',sortable:true"><?= lang('ID');?></th>
        <th data-options="field:'email_domain',width:'180',sortable:true,editor:'text'"><?= lang('email_domain');?></th>
		<th field="created_by_name" width="80">created_by</th>
		<th field="created_time" width="140">created_time</th>
		<th field="updated_by_name" width="80">updated_by</th>
		<th field="updated_time" width="140">updated_time</th>

	</tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#tt').edatagrid('addRow',0)"><?= lang('add');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="$('#tt').edatagrid('destroyRow')"><?= lang('delete');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#tt').edatagrid('saveRow')"><?= lang('save');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#tt').edatagrid('cancelRow')"><?= lang('cancel');?></a>
            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

