<script type="text/javascript">
    function doSearch(){
        $('#tt').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val() ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val() ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val()
        });
        $('#chaxun').window('close');
    }

    var is_submit = false;

    function submit_start() {
        is_submit = true;
        ajaxLoading();
    }

    function submit_end() {
        is_submit = false;
        ajaxLoadEnd();
    }

    function ts_tool_search(fm_id, load_id) {
        var where = {};
        var form_data = $('#' + fm_id).serializeArray();
        $.each(form_data, function (index, item) {
            if(item.value == "") return true;
            if(where.hasOwnProperty(item.name) === true){
                if(typeof where[item.name] == 'string'){
                    where[item.name] =  where[item.name].split(',');
                    where[item.name].push(item.value);
                }else{
                    where[item.name].push(item.value);
                }
            }else{
                where[item.name] = item.value;
            }
        });
        if(where.length == 0){
            $.messager.alert('Tips', '请填写需要搜索的值');
            return;
        }
        $('#' + load_id).combogrid('grid').datagrid('load',where);
    }

    /**
     * 新增
     */
    function add(){
        $('#add_div').window('open');
    }

    /**
     * 新增保存事件
     */
    function add_save(){
        if(is_submit){
            return;
        }
        if($('#add_truck_supplier').combogrid('getValue') != ''){
            var g = $('#add_truck_supplier').combogrid('grid'); // 获取数据表格对象
            var r = g.datagrid('getSelected'); // 获取选择的行
            $('#add_truck_client_code').val(r.client_code);
        }

        //验证表单项
        var param = $('#add_fm').serializeArray();
        if (_validate(param) == 0) return;

        $.messager.confirm('确认对话框', '是否确认提交？', function(r) {
            if (r) {
                submit_start();
                var this_window = $('#add_div');
                var form = $('#add_fm');
                this_window.window('close');
                form.form('submit', {
                    url: '/biz_shipment_truck/add_data/' + $('#shipment_id').val(),
                    onSubmit: function (param) {
                        var validate = form.form('validate');
                        param.client_code = $('#client_code').val();
                        if (!validate) {
                            submit_end();
                            this_window.window('open');
                        }
                        return validate;
                    },
                    success: function (res_json) {
                        var res;
                        try {
                            res = $.parseJSON(res_json);
                        } catch (e) {
                        }
                        if (res == undefined) {
                            submit_end();
                            this_window.window('open');
                            $.messager.alert('Tips', res_json);
                            return;
                        }
                        this_window.window('close');
                        submit_end();
                        if (res.id > 0) {
                            $.messager.alert('<?= lang('Tips')?>', '新增成功');
                            $('#tt').datagrid('reload').datagrid('clearSelections');
                        } else {
                            $.messager.alert('<?= lang('Tips')?>', '新增失败', 'info', function(){
                                this_window.window('open');
                            });
                        }

                    }
                });
            }
        });
    }

    /**
     * 编辑
     */
    function edit(row){
        if(row.id > 0){
            $('#edit_div').window('open');
            $('#edit_fm').form('load', row);
        }
    }

    /**
     * 新增保存事件
     */
    function edit_save(){
        if(is_submit){
            return;
        }

        //验证表单项
        var param = $('#edit_fm').serializeArray();
        if (_validate(param) == 0) return;

        if($('#edit_truck_supplier').combogrid('getValue') != ''){
            var g = $('#edit_truck_supplier').combogrid('grid'); // 获取数据表格对象
            var r = g.datagrid('getSelected'); // 获取选择的行
            $('#edit_truck_client_code').val(r.client_code);
        }

        $.messager.confirm('确认对话框', '是否确认提交？', function(r) {
            if (r) {
                submit_start();
                var this_window = $('#edit_div');
                var form = $('#edit_fm');
                this_window.window('close');
                form.form('submit', {
                    url: '/biz_shipment_truck/update_data',
                    onSubmit: function (param) {
                        var validate = form.form('validate');
                        if (!validate) {
                            submit_end();
                            this_window.window('open');
                        }
                        return validate;
                    },
                    success: function (res_json) {
                        var res;
                        try {
                            res = $.parseJSON(res_json);
                        } catch (e) {
                        }
                        if (res == undefined) {
                            submit_end();
                            this_window.window('open');
                            $.messager.alert('Tips', res_json);
                            return;
                        }
                        this_window.window('close');
                        submit_end();
                        if (res.id > 0) {
                            $.messager.alert('<?= lang('Tips')?>', '修改成功');
                            $('#tt').datagrid('reload').datagrid('clearSelections');
                        } else {
                            $.messager.alert('<?= lang('Tips')?>', '修改失败', 'info', function(){
                                this_window.window('open');
                            });
                        }

                    }
                });
            }
        });
    }

    //异步校验表单
    function _validate(formData) {
        var result = 0;
        $.ajax({
            type: 'post',
            url: '/biz_shipment_truck/validate',
            data: formData,
            success: function (data) {
                if (data.code == 0){
                    $.messager.alert('系统消息', data.msg);
                }
                result = data.code;
            },
            dataType: "json",
            async: false
        });

        return result;
    }

    $(function () {
        $('#tt').edatagrid({
            url: '/biz_shipment_truck/get_data/<?php echo $shipment_id;?>',
            destroyUrl: '/biz_shipment_truck/delete_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            }
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
            <?php if($lock_lv > 0){ ?>
            onDblClickCell: function () {
            },
            <?php }else{ ?>
            onDblClickRow: function (index, row) {
                edit(row);
            },
            <?php } ?>
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
        
        $('#zxtz').click(function () {
            var url = $(this).prop('href');
            if(url.indexOf('/bsc_template/set_parameter') != -1){
                $('#window').window({
                    title:'window',
                    width:500,
                    height:200,
                    href:url,
                    modal:true
                });
                return false;
            }else{
                return true;
            }

        });
    });

</script>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <?php if (!empty($f)) {
            foreach ($f as $rs) {

                $field = $rs[0];
                $disp = $rs[1];
                $width = $rs[2];
                $attr = $rs[4];

                if ($width == "0") continue;

                echo "<th data-options=\"field:'$field',width:$width\">" . lang($disp) . "</th>";
            }
        }
        ?>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <?php if($lock_lv < 1){?>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()"><?= lang('add');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#tt').edatagrid('destroyRow')"><?= lang('delete');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>
                <?php }?>
            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                <?= lang('query');?> 1
            </td>
            <td align="right">
                <select name="f1"  id="f1"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1"  id="s1"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v1" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('query');?> 2
            </td>
            <td align="right">
                <select name="f2"  id="f2"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2"  id="s2"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v2" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('query');?> 3
            </td>
            <td align="right">
                <select name="f3"  id="f3"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s3"  id="s3"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v3" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;"><?= lang('search');?></a>
</div>
<div id="window">

</div>
<div id="add_div" class="easyui-window"  title="<?= lang('add');?>" closed="true" style="width:400px;height:380px;padding:5px;">
    <form id="add_fm" method="post">
        <table>
            <tr>
                <td><?= lang('truck_date');?></td>
                <td>
                    <input class="easyui-datetimebox" name="truck_date"  style="width: 255px;">
                </td>
            </tr>
            <tr>
                <td><?= lang('truck_supplier');?></td>
                <td>
                    <input type="hidden" name="truck_client_code"  id="add_truck_client_code" value="">
                    <select class="easyui-combogrid" name="truck_supplier" id="add_truck_supplier" style="width: 255px" data-options="
                        required:true,
                        editable:false,
                        panelWidth:650,
                        panelHeight:450,
                        idField:'company_name',
                        textField:'company_name',
                        url:'/biz_client/get_option/truck',
                        columns:[[
                            {field:'company_name',title:'<?= lang('company_name');?>',width:330},
                            {field:'remark',title:'<?= lang('remark');?>',width:300},
                        ]],
                        toolbar:'#add_ts_tool',
                        rowStyler: function(index,row){
                            if (row.remark == '优先'){
                                return 'background-color:yellow;';
                            }
                        },
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('truck_company');?></td>
                <td>
                    <select class="easyui-combobox" name="truck_company" id="add_truck_company" style="width: 255px" data-options="
                        required:true,
                        valueField:'truck_company',
                        textField:'truck_company',
                        url:'/biz_truck_supplier/get_option/<?= $client_code;?>',
                        onSelect: function(res){
                            $('#add_truck_address').textbox('setValue', res.truck_address);
                            $('#add_truck_contact').textbox('setValue', res.truck_contact);
                            $('#add_truck_telephone').textbox('setValue', res.truck_telephone);
                            $('#add_truck_remark').textbox('setValue', res.truck_remark);
                            $('#add_GP20PRICE').numberbox('setValue', res.GP20PRICE);
                            $('#add_GP40PRICE').numberbox('setValue', res.GP40PRICE);
                        },
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('truck_address');?></td>
                <td>
                    <input class="easyui-textbox" name="truck_address" id="add_truck_address" style="width: 255px">
                </td>
            </tr>
            <tr>
                <td><?= lang('truck_contact');?></td>
                <td>
                    <input class="easyui-textbox" name="truck_contact" id="add_truck_contact" style="width: 255px">
                </td>
            </tr>
            <tr>
                <td><?= lang('truck_telephone');?></td>
                <td>
                    <input class="easyui-textbox" name="truck_telephone" id="add_truck_telephone" style="width: 255px">
                </td>
            </tr>
            <tr>
                <td><?= lang('truck_remark');?></td>
                <td>
                    <input class="easyui-textbox" name="truck_remark" id="add_truck_remark" style="width: 255px">
                </td>
            </tr>
            <tr>
                <td><?= lang('GP20');?></td>
                <td>
                    <input class="easyui-numberbox" name="GP20" id="add_GP20" data-options="min:0" style="width: 255px">
                </td>
            </tr>
            <tr>
                <td><?= lang('GP20PRICE');?></td>
                <td>
                    <input class="easyui-numberbox" name="GP20PRICE" id="add_GP20PRICE" style="width: 255px" data-options="min:0,precision:2">
                </td>
            </tr>
            <tr>
                <td><?= lang('GP40');?></td>
                <td>
                    <input class="easyui-numberbox" name="GP40" id="add_GP40" style="width: 255px" data-options="min:0">
                </td>
            </tr>
            <tr>
                <td><?= lang('GP40PRICE');?></td>
                <td>
                    <input class="easyui-numberbox" name="GP40PRICE" id="add_GP40PRICE" style="width: 255px" data-options="min:0,precision:2">
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <button type="button" onclick="add_save()" style="width: 100px"><?= lang('submit');?></button>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="edit_div" class="easyui-window"  title="<?= lang('edit');?>" closed="true" style="width:400px;height:380px;padding:5px;">
    <form id="edit_fm" method="post">
        <input type="hidden" name="id">
        <table>
            <tr>
                <td><?= lang('truck_date');?></td>
                <td>
                    <input class="easyui-datetimebox" name="truck_date" style="width: 255px;">
                </td>
            </tr>
            <tr>
                <td><?= lang('truck_supplier');?></td>
                <td>
                    <input type="hidden" name="truck_client_code"  id="edit_truck_client_code" value="">
                    <select class="easyui-combogrid" name="truck_supplier" id="edit_truck_supplier" style="width: 255px" data-options="
                        editable:false,
                        panelWidth:650,
                        panelHeight:450,
                        idField:'company_name',
                        textField:'company_name',
                        url:'/biz_client/get_option/truck',
                        columns:[[
                            {field:'company_name',title:'<?= lang('company_name');?>',width:330},
                            {field:'remark',title:'<?= lang('remark');?>',width:300},
                        ]],
                        toolbar:'#edit_ts_tool',
                        rowStyler: function(index,row){
                            if (row.remark == '优先'){
                                return 'background-color:yellow;';
                            }
                        },
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('truck_company');?></td>
                <td>
                    <select class="easyui-combobox" name="truck_company" id="edit_truck_company" style="width: 255px" data-options="
                        required:true,
                        valueField:'truck_company',
                        textField:'truck_company',
                        url:'/biz_truck_supplier/get_option/<?= $client_code;?>',
                        onSelect: function(res){
                            $('#edit_truck_address').textbox('setValue', res.truck_address);
                            $('#edit_truck_contact').textbox('setValue', res.truck_contact);
                            $('#edit_truck_telephone').textbox('setValue', res.truck_telephone);
                            $('#edit_truck_remark').textbox('setValue', res.truck_remark);
                            $('#edit_GP20PRICE').numberbox('setValue', res.GP20PRICE);
                            $('#edit_GP40PRICE').numberbox('setValue', res.GP40PRICE);
                        },
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('truck_address');?></td>
                <td>
                    <input class="easyui-textbox" name="truck_address" id="edit_truck_address" style="width: 255px">
                </td>
            </tr>
            <tr>
                <td><?= lang('truck_contact');?></td>
                <td>
                    <input class="easyui-textbox" name="truck_contact" id="edit_truck_contact" style="width: 255px">
                </td>
            </tr>
            <tr>
                <td><?= lang('truck_telephone');?></td>
                <td>
                    <input class="easyui-textbox" name="truck_telephone" id="edit_truck_telephone" style="width: 255px">
                </td>
            </tr>
            <tr>
                <td><?= lang('truck_remark');?></td>
                <td>
                    <input class="easyui-textbox" name="truck_remark" id="edit_truck_remark" style="width: 255px">
                </td>
            </tr>
            <tr>
                <td><?= lang('GP20');?></td>
                <td>
                    <input class="easyui-numberbox" name="GP20" id="edit_GP20" data-options="min:0" style="width: 255px">
                </td>
            </tr>
            <tr>
                <td><?= lang('GP20PRICE');?></td>
                <td>
                    <input class="easyui-numberbox" name="GP20PRICE" id="edit_GP20PRICE" style="width: 255px" data-options="min:0,precision:2">
                </td>
            </tr>
            <tr>
                <td><?= lang('GP40');?></td>
                <td>
                    <input class="easyui-numberbox" name="GP40" id="edit_GP40" style="width: 255px" data-options="min:0">
                </td>
            </tr>
            <tr>
                <td><?= lang('GP40PRICE');?></td>
                <td>
                    <input class="easyui-numberbox" name="GP40PRICE" id="edit_GP40PRICE" style="width: 255px" data-options="min:0,precision:2">
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <button type="button" onclick="edit_save()" style="width: 100px"><?= lang('submit');?></button>
                </td>
            </tr>
        </table>
    </form>
</div>
<!--工具栏-->
<div id="add_ts_tool">
    <form id="add_ts_tool_fm" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('company_name');?>:</label><input class="easyui-textbox" name="company_name" style="width:96px;"/>
        </div>
        <div style="padding-left: 5px;display: inline-block;">
            <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-search" onclick="ts_tool_search('add_ts_tool_fm', 'add_truck_supplier')"><?= lang('search');?></a>
        </div>
    </form>
</div>

<!--工具栏-->
<div id="edit_ts_tool">
    <form id="edit_ts_tool_fm" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('company_name');?>:</label><input class="easyui-textbox" name="company_name" style="width:96px;"/>
        </div>
        <div style="padding-left: 5px;display: inline-block;">
            <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-search" onclick="ts_tool_search('edit_ts_tool_fm', 'edit_truck_supplier')"><?= lang('search');?></a>
        </div>
    </form>
</div>

<input type="hidden" id="client_code" value="<?= $client_code;?>">
<input type="hidden" id="shipment_id" value="<?= $shipment_id;?>">