<link rel="stylesheet" href="/inc/third/layui/css/layui.css" media="all">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="/inc/css/normal.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/default/easyui.css">
<script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/inc/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/inc/js/md5.js"></script>

<body>
<style>
    .layui-input, .layui-textarea{
        display:inline-block;
    }
    .layui-input, .layui-select, .layui-textarea{
        line-height: 25px;
        height: 25px;
        border: 1px solid #95B8E7;
        border-radius: 5px 5px 5px 5px;
    }

</style>

<form class="layui-form" action="" id="form" lay-filter="form">
    <div class="layui-row" style="background-color: #393D49" style="margin:30px;">

        <div style="margin:0px 10px;">
            <div class="layui-row layui-col-space15" style="margin-top:0px;">
                <div class="layui-col-sm6 layui-col-md3">
                    <div class="layui-card">
                        <div class="layui-card-header">
                            <?= lang('开始推广');?>
                        </div>
                        <div class="layui-card-body layuiadmin-card-list" style="height:55px;">
                            <p style="font-size:20px;margin-top:15px;">
                                <a href="/biz_crm/customer_apply/import" style="color:blue;" target="_blank">
                                    <?= lang('从CRM导入邮箱群发');?> </a>
                                <!--<a href="/crm_promote_mail/search_send" style="color:blue;" target="_blank">
                                    从CRM导入邮箱群发 </a>-->
                            </p>
                        </div>
                    </div>
                </div>

                <div class="layui-col-sm6 layui-col-md3">
                    <div class="layui-card">
                        <div class="layui-card-header">
                            <?= lang('开始推广');?>
                        </div>
                        <div class="layui-card-body layuiadmin-card-list" style="height:55px;">
                            <p style="font-size:20px;margin-top:15px;">
                                <a href="/crm_promote_mail/consignee_group_list" style="color:blue;" target="_blank">
                                    <?= lang('手工创建邮箱群发');?> </a>
                            </p>
                        </div>
                    </div>
                </div>
                <!--<div class="layui-col-sm6 layui-col-md3">-->
                <!--    <div class="layui-card">-->
                <!--        <div class="layui-card-header">-->
                <!--            开始推广-->
                <!--        </div>-->
                <!--        <div class="layui-card-body layuiadmin-card-list" style="height:55px;">-->
                <!--            <p style="font-size:20px;margin-top:15px;">-->
                <!--                <a href="/biz_client/index?import=import" style="color:blue;" target="_blank">-->
                <!--                    从Client导入邮箱群发 </a>-->
                <!--                                <a href="/crm_promote_mail/search_send_client" style="color:blue;" target="_blank">-->
                <!--                                    从Client导入邮箱群发 </a>-->
                <!--            </p>-->
                <!--        </div>-->
                <!--    </div>-->
                <!--</div>-->
                <div class="layui-col-sm6 layui-col-md3">
                    <div class="layui-card">
                        <div class="layui-card-header">
                            <?= lang('今日发送总量');?>
                        </div>
                        <div class="layui-card-body layuiadmin-card-list" style="height:55px;">
                            <p style="font-size:30px;margin-top:15px;">
                                <a href="javascript:void(0);" onclick="detail('/biz_crm/Crmsend','<?=$crm_date; ?>');" style="color:blue;"><?php echo $total_send; ?></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="layui-col-sm12 layui-col-md12">
                <div class="layui-card">
                    <div class="layui-card-header" style="padding:10px 15px; background: #f3f3f6;">
                        <div class="layui-inline" style="width: 570px;">
                            <label class="layui-form-label" style="line-height: 25px"><b><?= lang('选择部门');?></b></label>
                            <div class="layui-input-inline" id="group" style="width: 350px;">
                            </div>
                        </div>
                    </div>
                    <!--图表-->
                    <div class="layui-card-body" id="_Highcharts_box" style="height: 300px;"></div>

                    <!--报表-->
                    <div class="layui-card-body" id="send_email_table"></div>
                </div>
            </div>
        </div>

</form>
<script type="text/html" id="send_email_tpl">
    <table class="layui-table">
        <thead>
        <tr>
            <th lay-data="{field:'1',   fixed: true}"><?= lang('日期');?></th>
            <th lay-data="{field:'3' }"><?= lang('发送总量');?></th>
            <th lay-data="{field:'4' }"><?= lang('发送成功');?></th>
            <th lay-data="{field:'4' }"><?= lang('发送失败');?></th>
        </tr>
        </thead>
        <tbody>
        {{#  layui.each(d.date, function(index, item){ }}
        <tr>
            <td>{{ item }}</td>
            <td>{{ d._total[index] }}</td>
            <td>{{ d._succ[index] }}</td>
            <td>{{ d._fail[index] }}</td>
        </tr>
        {{#  }); }}
        </tbody>
    </table>
</script>
</body>
<script src="/inc/third/layui/layui.js"></script>
<script type="text/javascript" src="/inc/third/layui/xm-select.js"></script>
<script type="text/javascript" src="/inc/js/hcharts/highcharts.js"></script>
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<script>
    var laytpl;
    var chart;
    var group;
    var selected_group;
    var group_data = <?=json_encode($user_role);?>;
    layui.use(['laytpl'], function () {
        laytpl = layui.laytpl;
    });

    $(function () {
        group = xmSelect.render({
            el: '#group',
            name: 'group',
            model: { label: { type: 'text' } },
            radio: true,
            clickClose: true,
            tree: {
                show: true,
                strict: false,
                expandedKeys: true,
                clickExpand: true,
                //点击节点是否选中
                clickCheck: true,
            },
            height: '500px',
            data:group_data,
            on: function(data){
                var type = data.change[0].type;
                var value = data.change[0].value;
                get_stat_result(value, type);
            },
        });

        //默认选中
        $('.xm-option').each(function (index, item) {
            var value = $(item).attr('value');
            if ($(item).is('.disabled') == false) {
                get_stat_result(value, group_data[index].type);
                selected_group = value;
                group.setValue([value]);
                if (value == 'HEAD') {
                    $('#type').val('HEAD');
                } else if (typeof value == 'string') {
                    $('#type').val('group');
                } else {
                    $('#type').val('user');
                }
                return false;
            }
        });
    });

    function get_stat_result(value, type) {
        if (selected_group == ''){
            layer.msg('<?= lang('请选择部门！');?>');
        }else{
            var loading = layer.load(1);
            $.ajax({
                type: 'POST',
                url: '/biz_stat_client/get_send_email',
                data: {group:value, type:type},
                dataType: 'json',
                success: function (res) {
                    layer.close(loading);
                    laytpl($('#send_email_tpl').html()).render(res.data, function (html) {
                        $('#send_email_table').html(html);
                    });
                    Highcharts.chart('_Highcharts_box',{
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: '<?= lang('15天内邮件发送报表');?>'
                        },
                        xAxis: {
                            categories: res.data.date,
                            crosshair: true
                        },

                        colors: ['#1282d0', '#20991f', '#c42525'],
                        yAxis: {
                            min: 0,
                            title: {
                                text: '<?= lang('发送数量');?>'
                            }
                        },
                        tooltip: {
                            // head + 每个 point + footer 拼接成完整的 table
                            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                                '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                            footerFormat: '</table>',
                            shared: true,
                            useHTML: true
                        },
                        plotOptions: {
                            column: {
                                borderWidth: 0
                            }
                        },
                        series: [{
                            name: '<?= lang('发送数量');?>',
                            data: res.data._total
                        },{
                            name: '<?= lang('成功数量');?>',
                            data: res.data._succ
                        },{
                            name: '<?= lang('失败数量');?>',
                            data: res.data._fail
                        }]
                    });
                },
                error:function (res) {
                    layer.close(loading);
                    layer.alert('<?= lang('请求失败！');?>', {icon: 5});
                }
            })
        }
    }
</script>
