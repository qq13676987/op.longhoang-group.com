<script type="text/javascript">
    function doSearch(){
        var json = {};
        var cx_data = $('#myform').serializeArray();
        $.each(cx_data, function (index, item) {
            if (item.value != '') {
                //判断如果name存在,且为string类型
                if (json.hasOwnProperty(item.name) === true) {
                    if (typeof json[item.name] == 'string') {
                        json[item.name] = json[item.name].split(',');
                        json[item.name].push(item.value);
                    } else {
                        json[item.name].push(item.value);
                    }
                } else {
                    json[item.name] = item.value;
                }
            }
        });

        //查询条件是否完整
        if (json.hasOwnProperty('field[f][]') === true){
            $('#tt').datagrid('load', json);
            $('#chaxun').window('close');
        }
    }

    function import_excel(){
        $('#import').window('open');
    }
    $(function () {

        $('#tt').edatagrid({
            url: '/biz_charge/get_data',
            saveUrl: '/biz_charge/add_data',
            updateUrl: '/biz_charge/update_data',
            destroyUrl: '/biz_charge/delete_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            },
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height()
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });

    });

</script>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <?php if (!empty($f)) {
            foreach ($f as $rs) {

                $field = $rs[0];
                $disp = $rs[1];
                $width = $rs[2];
                $attr = isset($rs[4]) ? $rs[4] : '';

                if ($width == "0") continue;

                echo "<th field=\"$field\" width=\"$width\" $attr>" . lang($disp) . "</th>";
            }
        }
        ?>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#tt').edatagrid('addRow',0)"><?= lang('add');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#tt').edatagrid('destroyRow')"><?= lang('delete');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#tt').edatagrid('saveRow')"><?= lang('save');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#tt').edatagrid('cancelRow')"><?= lang('cancel');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>

            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <form name="myform" id="myform" onsubmit="return false;" >
    <table>
        <?php
        for ($i=0; $i<=2; $i++){
        ?>
        <tr>
            <td>
                <?= lang('query');?> <?=$i+1;?>
            </td>
            <td align="right">
                <select name="field[f][]" class="easyui-combobox"  required="true" style="width:150px;">
                    <option value=''></option>
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>                &nbsp;

                <select name="field[s][]" class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input name="field[v][]" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <?php }?>
    </table>
    </form>
    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="doSearch()" style="width:200px;height:30px;"><?= lang('search');?></a>
</div>

<div id="import" class="easyui-dialog" style="background-color: #F4F4F4;border:1px solid #95B8E7;margin-top:10px;"
     data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons',title:'<?= lang('Import');?>'">
    <table>
        <tr>
            <td>
                <input class="easyui-filebox" name="file" id="file_upload"
                       data-options="" style="width:400px">
            </td>
        </tr>
    </table>
    <div id="dlg-buttons">
        <a class="easyui-linkbutton" id="file_save" iconCls="icon-ok" style="width:90px"><?= lang('save');?></a>
    </div>
    <div id="div_files"></div>
</div>