<?php
class Biz_bill_currency extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_bill_currency_model');
        $this->model = $this->biz_bill_currency_model;
        foreach($this->field_all as $row){
            $v = $row[0];
            if($v=="id" || $v=="create_by" || $v=="create_time" || $v=="update_by" || $v=="update_time") continue;
            array_push($this->field_edit,$v);
        }
    }

    public $field_edit=array();

    public $field_all =  array(
        array('id','ID','80','1'),
        array('currency_from','currency_from','100','3'),
        array('currency_to','currency_to','100','3'),
        array('sell_value','sell_value','100','4'),
        array('cost_value','cost_value','100','4'),
        array('invoice_value','invoice_value','100','4'),
        array('start_time','start_time','150','8'),
        array('create_time','create_time','150','8'),
        array('update_time','update_time','150','10'),
    );

    public function get_currency($type){
        $rows = array();
        $rs = Model('bsc_dict_model')->get_option("currency");
        
        array_push($rows, array("name"=>"","namevalue"=>"","value"=>""));
        foreach ($rs as $row){
            array_push($rows, $row);
        }
        echo json_encode($rows);
    }

    public function index(){
        $data = array();

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_bill_currency_table');
        if (!empty($rs['config_name'])){
            $data["f"] = json_decode($rs['config_text']);
        }else{
            $data["f"] = $this->field_all;
        }
        // page account
        $this->load->view('head');
        $this->load->view('biz/bill/currency/index_view',$data);
    }

    public function get_data(){
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = '';
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if($v1!="") $where .= $s1 != 'like' ? "$f1 $s1 '$v1'" : "$f1 $s1 '%$v1%'";
        if($v2!="" && $where != '') $where .= $s2 != 'like' ? " and $f2 $s2 '$v2'" : " and $f2 $s2 '%$v2%'";
        if($v3!="" && $where != '') $where .= $s3 != 'like' ? " and $f3 $s3 '$v3'" : " and $f3 $s3 '%$v3%'";
        //-----------------------------------------------------------------
        $offset = ($page-1)*$rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $rs = $this->model->get($where,$sort,$order);

        $rows = array();
        foreach ($rs as $row){
            $row['created_by'] = getUserName($row['created_by']);
            $row['updated_by'] = getUserName($row['updated_by']);
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data($client_id=""){
        $field = $this->field_edit;
        $data= array();
        foreach ($field as $item){
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if($temp!="")
                $data[$item] = $temp;
        }
        $id = $this->model->save($data);
        $data['id']= $id;

        echo json_encode( $data );

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_bill_currency";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function update_data(){
        $id = $_REQUEST["id"];
        $old_row = $this->model->get_one('id', $id);

        $field = $this->field_edit;

        $data= array();
        foreach ($field as $item){
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if($old_row[$item]!=$temp)
                $data[$item] = $temp;
        }
        $id = $this->model->update($id,$data);
        $data['id']= $id;
        echo json_encode( $data );

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_bill_currency";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function delete_data(){

        $id = intval($_REQUEST['id']);
        $old_row = $this->model->get_one('id', $id);
        $this->model->mdelete($id);
        echo json_encode(array('success'=>true));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_bill_currency";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
    }
}