<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?= lang('导入CRM客户');?></title>
    <meta name="keywords" content="">
    <meta name="viewport" content="width=1180">
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css"/>
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <style>
        .search_box {
            border: 1px solid #ddd;
            float: left;
            margin: 10px 0px 0px;
            padding: 15px;
            background-color: #edf2f3;
            width: 99%;
        }
        .selected-td{ background: #ff4c42 !important; }
        .layui-table td, .layui-table th {
            vertical-align: top;
        }
    </style>
</head>
<body>
<?php if ($company_name):?>
    <?= lang('线索分析');?>：<span class="layui-badge layui-bg-blue"><?= $company_name;?></span>
<?php endif;?>
<div class="layui-tab layui-tab-card">
    <ul class="layui-tab-title">
        <li class="layui-this"><?= lang('分析往来单位');?></li>
    </ul>
    <div class="layui-tab-content" style="height: auto;">
        <div class="layui-tab-item layui-show">
            <table class="layui-table" width="600px" style="">
                <colgroup>
                    <col width="">
                    <col width="">
                    <col width="">
                    <col width="">
                </colgroup>
                <thead>
                <tr>
                    <th><?= lang('客户全称'); ?></th>
                    <th><?= lang('当前销售'); ?></th>
                    <th><?= lang('shipment未合作天数'); ?></th>
                    <th><?= lang('标签'); ?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($client):
                    ?>
                    <tr>
                        <td>
                            <?= $client['company_name']; ?>
                            <?php
                            if ($client['client_level'] == -1) {
                                echo "<br><font color='red'>" . lang('【黑名单】') . "</font><font style='color: #fd810b'>{$client['client_level_remark']}</font>";
                            }
                            ?>
                        </td>
                        <td><?= $client['sales_ids_names']; ?></td>
                        <td><?= $client['not_traded_days']; ?></td>
                        <td><?= $client['tag']; ?></td>
                    </tr>
                <?php
                else:
                    ?>
                    <tr bgcolor="#FFFFFF">
                        <td colspan="4" style="color: ; height: 35px; line-height: 35px; text-align: center">
                            <?= lang('往来单位中未找到与“{company_name}”相关的结果！', array('company_name', "<font color=\"red\">{$company_name}</font>"));?>
                        </td>
                    </tr>
                <?php
                endif;
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div style="width: 90%; height: 20px; line-height: 20px; margin-top: 40px; color: #d35400; font-size: 12px;"><?= lang('Tips：在CRM中有以下人员正在跟进');?></div>
<div class="layui-tab layui-tab-card" >
    <ul class="layui-tab-title">
        <li class="layui-this"><?= lang('分析CRM');?></li>
    </ul>
    <div class="layui-tab-content" style="height: auto;">
        <div class="layui-tab-item layui-show">
            <!--其他跟进销售-->
            <table class="layui-table">
                <colgroup>
                    <col width="">
                    <col width="">
                    <col>
                </colgroup>
                <thead>
                <tr>
                    <th><?= lang('销售姓名');?></th>
                    <th><?= lang('销售Email');?></th>
                    <th style="text-align: center"><?= lang('当前开发进度');?></th>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($crm)):
                    foreach ($crm as $item): ?>
                        <tr>
                            <td><?= $item['sales_name']; ?></td>
                            <td><?= $item['sales_email']; ?></td>
                            <td>
                                <div class="layui-progress" lay-showPercent="true" style="margin-top: 15px;">
                                    <div class="layui-progress-bar layui-bg-green" lay-percent="<?= $item['score']; ?>"></div>
                                </div>
                            </td>
                        </tr>
                    <?php
                    endforeach;
                else: ?>
                    <tr>
                        <td colspan="3" style="text-align: center; color: red;"><?= lang('暂无其他销售人员跟进！');?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>

<script type="text/javascript" src="/inc/third/layui/layui.js"></script>
<script type="text/javascript">
    $(function () {
        layui.use(['element', 'form'], function () {
            form = layui.form;
        });
    })
</script>