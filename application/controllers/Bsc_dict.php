<?php

class bsc_dict extends Menu_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param string $catalog
     */
    public function get_option($catalog = "")
    {
        $q = isset($_POST['q']) ? check_param($_POST['q']) : 0;
        $q_field = isset($_GET['q_field']) ? check_param($_GET['q_field']) : 'name';
        $ext1 = isset($_GET['ext1']) ? check_param($_GET['ext1']) : '';
        $rows = array();
        $this->load->model('bsc_dict_model');
        $where = array();
        // if ($ext1 !== '') $this->db->where('ext1', $ext1);
         if ($ext1 !== '') $where[] = "ext1 = '{$ext1}'";
        if ($q != '') {
            if ($catalog == 'hs_code') {
                // $this->db->where("$q_field like '$q%'");
                $where[] = "$q_field like '$q%'";
            } else {
                // $this->db->where("$q_field like '%$q%'");
                $where[] = "$q_field like '%$q%'";
            }
        }
        $biz_table = isset($_GET['biz_table'])?$_GET['biz_table']:'';
        if(!empty($biz_table)) $where[] = "FIND_IN_SET('{$biz_table}', ext1)";
        $where = join(' and ', $where);
        $rs = $this->bsc_dict_model->get_option($catalog, $where);
        
        if(!in_array($catalog, array('export_sailing'))){
            array_push($rows, array("name" => "", "namevalue" => "", "value" => " ", 'valuename' => '', 'lang' => ''));
        }
        
        foreach ($rs as $row) {
            if ($catalog == 'port_code') {
                $row["namevalue"] = $row["value"] . " | " . $row["name"];
            } else {
                $row["namevalue"] = $row["name"] . "(" . $row["value"] . ")";
            }
            $row['valuename'] = $row['value'] . ' | ' . $row['name'];
            $row['lang'] = lang($row['name']);
            array_push($rows, $row);
        }
        echo json_encode($rows);
    }
    public function get_branch_office()
    {
		$op = $this->load->database('china',true);
        $rows = $op->select("bsc_dict.*,(select company_name_en from biz_client where client_code = bsc_dict.value) as company_name")->where('catalog','branch_office')->get('bsc_dict')->result_array();
        echo json_encode($rows);
    }

	public function get_client_account($client_code='')
	{
		$this->load->model('bsc_overseas_account_model');
		//2023-03-01 由于新加坡离岸,这里改为5个中文
		if($client_code == ''){
			$rs = $this->bsc_overseas_account_model->get("`status` = 1");
		}else{
			$code = getValue('code','');
			if($code != ''){
				$finance_belong_company_code = join(',',array_filter(explode(',',$code)));
			}else{
				$finance_belong_company_code = $this->db->where('client_code',$client_code)->get('biz_client')->row_array()['finance_belong_company_code'];
			}

			if($finance_belong_company_code != ''){
				$finance_belong_company_code = str_replace(',',"','",$finance_belong_company_code);
				$rs = $this->bsc_overseas_account_model->get("company_code in ('$finance_belong_company_code') and `status` = 1");
			}else{
				$rs = [];
			}
		}
		$rows = array();
		foreach ($rs as $row) {
			$row['title_cn'] = $row['title_cn'].' '.$row['currency'];
			$rows[] = $row;
		}
		echo json_encode($rows);
	}

	public function get_client_account1($client_code='')
	{
		$this->load->model('bsc_overseas_account_model');
		$client = $this->db->where('client_code',$client_code)->get('biz_client')->row_array();
		if(empty($client)) return jsonEcho([]);
		if(empty($client['dn_accounts'])) $client['dn_accounts'] = "9999999999";
		$ids = join(',',array_filter(explode(',',$client['dn_accounts'])));
		$where = "id in ($ids) and `status` = 1";
		$rs = $this->bsc_overseas_account_model->get($where);
		$rows = array();
		foreach ($rs as $row) {
			$row['title_cn_currency'] = $row['title_cn'].' '.$row['currency'];
			$rows[] = $row;
		}
		echo json_encode($rows);
	}

    public function get_biz_type()
    {
        $rows = array();
        $this->load->model('bsc_dict_model');
        $biz_types = $this->bsc_dict_model->get_option('biz_type');
        $trans_modes = $this->bsc_dict_model->get_option('trans_mode');
        array_push($rows, array("biz_type" => "", "trans_mode" => "", 'name' => ''));
        foreach ($biz_types as $biz_type) {
            foreach ($trans_modes as $trans_mode) {
                array_push($rows, array("biz_type" => $biz_type['value'], "trans_mode" => $trans_mode['value'], "name" => $biz_type['name'] . ' ' . $trans_mode['name']));
            }
        }
        return jsonEcho($rows);
    }
    
    /**
     * 往来单位的api配置页面
     */
    public function client_api_config($client_code = ""){
        $api_config_arr = array(
            'ebooking_url' => array('name' => 'ebooking接收url', 'catalog' => "{$client_code}_api_ebooking_url"),
        );

        //这里配置的都是
        $rs = array_column(Model('bsc_dict_model')->no_role_get("catalog like '{$client_code}_api_%'"), null, 'catalog');
        $rows = array();
        foreach ($api_config_arr as $api_config){
            if(isset($rs[$api_config['catalog']])){
                $this_row = $rs[$api_config['catalog']];
            }else{
                $this_row = $api_config;
                $field = array('id', 'catalog', 'name', 'value', 'ext1');
                foreach ($field as $val){
                    if(!isset($this_row[$val])) $this_row[$val] = "";
                }
            }
            $rows[] = $this_row;
        }
        $data['rows'] = $rows;

        $this->load->view('head');
        $this->load->view('/bsc/dict/client_api_config_view', $data);
    }
    
    public function update_by_client()
    {
        $this->load->model('bsc_dict_model');
        $id = $_REQUEST['id'];
        $filed = array("name", "value", "orderby", "catalog", 'ext1', 'ext2', 'ext3', 'ext4', 'ext5');
        $data = array();
        foreach ($filed as $item) {
            if (isset($_REQUEST[$item])) {
                $data[$item] = $_REQUEST[$item];
            }
        }
        $this->db->where('ext1', $data['ext1']);
        $old_row = $this->bsc_dict_model->get_one('name', $data['name'], $data['catalog']);
        if (!empty($old_row)) $id = $old_row['id'];
        if ($id == '') {
            $id = $this->bsc_dict_model->save($data);
            $action = 'insert';
        } else {
            $this->bsc_dict_model->update($id, $data);
            $action = 'update';
        }
        $data["id"] = $id;
        $log_data = array();
        $log_data["table_name"] = "bsc_dict";
        $log_data["key"] = $id;
        $log_data["action"] = $action;
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        echo json_encode($data);
    }

    public function get_hbl_type()
    {
        // $this->load->model('bsc_user_model');
        // $user = $this->bsc_user_model->get_one('id', $this->session->userdata('id'));
        // $rows = array(
        // array("name" => "MBL", "value" => "MBL"),
        // array("name" => $user['company'], "value" => $user['company']),
        // );
        Model('bsc_dict_model');
        $rs = $this->bsc_dict_model->get_option('hbl_type_oversea');
        $rows = array();
        // $rows[] = array("name" => "MBL", "value" => "MBL");
        foreach ($rs as $row) {
            $rows[] = $row;
        }
        echo json_encode($rows);
    }

    public function index()
    {
        if (!is_admin()) {
            // exit('不是管理员');
        }
        $data["catalog"] = getValue('catalog', '');
        $this->load->view('head');
        $this->load->view('bsc/dict/index_view', $data);
    }

    public function get($catalog = "")
    {
        $result = array();
        $this->load->model('bsc_dict_model');
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $name = isset($_POST['name']) ? ($_POST['name']) : '';
        //$catalog = isset($_POST['catalog']) ? ($_POST['catalog']) : '';
        $offset = ($page - 1) * $rows;
        $result["total"] = $this->bsc_dict_model->total($name, $catalog);
        $this->db->limit($rows, $offset);
        $rs = $this->bsc_dict_model->get($name, $catalog, $sort, $order);
        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add($catalog = "")
    {
        return jsonEcho(array('isError' => true, 'msg' => lang('请到 国内 系统维护')));
        $_REQUEST["catalog"] = $catalog;
        $filed = array("name", "value", "orderby", "catalog", 'ext1','ext2','ext3','ext4','ext5',);
        $this->load->model('bsc_dict_model');
        $data = array();
        foreach ($filed as $item) {
            $temp = isset($_REQUEST[$item]) ? trim(ts_replace($_REQUEST[$item])) : '';
            $data[$item] = $temp;
        }
        if ($catalog == "vessel") {
            $data["name"] = strtoupper($data["name"]);
            $data["value"] = strtoupper($data["value"]);
            $check = check_banjiao($data["name"]);
            if (!empty($check[0])) {
                $data['isError'] = true;
                $data['msg'] = '非法字符';
                echo json_encode($data);
                return;
            }
            $check = check_banjiao($data["value"]);
            if (!empty($check[0])) {
                $data['isError'] = true;
                $data['msg'] = '非法字符';
                echo json_encode($data);
                return;
            }
            //船名 新增一个不能重复添加
            $this->db->select('id');
            $row = $this->bsc_dict_model->get_one_eq('name', $data['name'], 'vessel');
            if (!empty($row)) {
                $data['isError'] = true;
                $data['q'] = lastquery();
                $data['msg'] = '该船名已存在';
                echo json_encode($data);
                return;
            }
        }
        $id = $this->bsc_dict_model->save($data);
        $data["id"] = $id;
        $log_data = array();
        $log_data["table_name"] = "bsc_dict";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        echo json_encode($data);
    }

    public function update($catalog = "")
    {
        return jsonEcho(array('isError' => true, 'msg' => lang('请到 国内 系统维护')));
        $_REQUEST["catalog"] = $catalog;
        $id = $_REQUEST['id'];
        $filed = array("name", "value", "orderby", "catalog", 'ext1','ext2','ext3','ext4','ext5',);
        $data = array();
        foreach ($filed as $item) {
            if (isset($_REQUEST[$item])) {
                $data[$item] = trim(ts_replace($_REQUEST[$item]));
            }
        }
        if ($catalog == "vessel") {
            $data["name"] = strtoupper($data["name"]);
            $data["value"] = strtoupper($data["value"]);
            $check = check_banjiao($data["name"]);
            if (!empty($check[0])) {
                $data['isError'] = true;
                $data['msg'] = '非法字符';
                echo json_encode($data);
                return;
            }
            $check = check_banjiao($data["value"]);
            if (!empty($check[0])) {
                $data['isError'] = true;
                $data['msg'] = '非法字符';
                echo json_encode($data);
                return;
            }
        }
        $this->load->model('bsc_dict_model');
        $id = $this->bsc_dict_model->update($id, $data);
        $data["id"] = $id;
        $log_data = array();
        $log_data["table_name"] = "bsc_dict";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        echo json_encode($data);
    }

    public function mdelete()
    {
        $id = intval($_REQUEST['id']);
        $this->load->model('bsc_dict_model');
        $this->bsc_dict_model->mdelete($id);
        echo json_encode(array('success' => true));
    }
    
    public function get_option_limit($catalog = "")
    {
        $page = isset($_POST['page']) ? $_POST['page'] : '';
        $rows = isset($_POST['rows']) ? $_POST['rows'] : '';
        $post_where = isset($_POST['where']) ? $_POST['where'] : '';
        $q = isset($_POST['q']) ? trim($_POST['q']) : '';
        $where = array();        //where----start
        if (!empty($q)) $where[] = "name like '%$q%'";
        if (isset($post_where['text']) && !empty($post_where['text'])) $where[] = "name like '%" . strtoupper($post_where['text']) . "%' or value like '%" . strtoupper($post_where['text']) . "%'";
        $where = join(' and ', $where);
        //where----end
        $offset = ($page - 1) * $rows;
        $total = Model('bsc_dict_model')->total_where($catalog, $where);
        $this->db->limit($rows, $offset);
        $rs = Model('bsc_dict_model')->get_option_where($catalog, $where);
        $data['total'] = $total;
        $data['rows'] = $rs;
        $data['Q'] = $q;
        echo json_encode($data);
    }
}
