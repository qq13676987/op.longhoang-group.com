    <script type="text/javascript">
        function doSearch(){
            $('#tt').datagrid('load',{
                name: $('#name').val()
            });
        }
        $(function(){
            $('#tt').edatagrid({
                url: '/bsc_dict/get_ext1/<?php echo $catalog;?>/<?= $ext1;?>',
            });
			$('#tt').datagrid({
				width:'auto',
				height: $(window).height() 
            }); 
			
			$(window).resize(function () {
				$('#tt').datagrid('resize');
			});
			
			$('#name').keydown(function(e){
			    if(e.keyCode==13){
			        doSearch();
			    }
			});
        });
            
    </script>
    
<table id="tt" style="width:1100px;height:450px"
            rownumbers="false" pagination="true"  idField="id"
            pagesize="30" toolbar="#tb"  singleSelect="true" nowrap="false">
        <thead>
            <tr>
                <th field="id" width="60" sortable="true">ID</th>      
                <th field="catalog" width="150" align="center" sortable="true">catalog</th>           
                <th field="name" width="200" align="center"  editor="{type:'validatebox',options:{required:true}}">name</th>
				<th field="value" width="200" editor="{type:'validatebox',options:{required:true}}">value</th>
				<th field="orderby" width="150" align="left" sortable="true" editor="{type:'validatebox',options:{required:true}}">orderby</th>
				<th field="ext1" width="150" align="left" sortable="true" editor="{type:'validatebox',}">ext1</th>
            </tr>
        </thead>
</table>

   <div id="tb" style="padding:3px;">
        <table><tr>
        <td>
            
        </td>
        <td>
        <span style="font-size:12px;">name</span>
        <input id="name" style="border:1px solid #ccc">
        </td><td>
        <a href="#" class="easyui-linkbutton" plain="true" onclick="doSearch()"><?= lang('search');?></a>
        <?php if($catalog == 'hs_code'){?>
        <span style="color:red;">根据输入的前6位查询</span>
        <?php }?>
        </td>
        </tr></table>
         
    </div>
