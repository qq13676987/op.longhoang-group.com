<link type="text/css" rel="stylesheet" href="/inc/third/layui/css/layui.css">
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<title><?= lang('下单客户申请');?></title>
<script type="text/javascript">
    $(function () {
        //第三步保存数据
        $('#save_data').click(function () {
            var field = {};
            field.contact_info = {};
            $.each($('#myform').serializeArray(), function (index, item) {
                //判断如果name存在,且为string类型
                if (field.hasOwnProperty(item.name) === true) {
                    if (typeof field[item.name] == 'string') {
                        field[item.name] = [field[item.name]];
                        field[item.name].push(item.value);
                    } else {
                        field[item.name].push(item.value);
                    }
                } else {
                    field[item.name] = item.value;
                }
            });
            
            if($('#company_name_en').textbox('getValue') == ''){
                return layer.alert("<?= lang('{field}必填', array('field' => lang('英文全称')));?>", {icon: 5});
            }

            var load = layer.load(1);
            $.ajax({
                url: '/biz_client/add_basic_data',
                type: 'POST',
                data: field,
                dataType: 'json',
                success: function (res) {
                    layer.close(load);
                    if (res.code == 0) {
                        //如果成功了, 带着 code 进入下一个页面
                        layer.alert("<?= lang('生成成功');?>", {icon: 1, closeBtn: 0}, function () {
                            //2023-08-31 用post 跳转, 不然可能会被改参数
                            post_jump({client_code:res.data.client_code,company_name_en:res.data.company_name_en}, '/biz_client/add');
                        });
                    } else {
                        layer.alert(res.msg, {icon: 5});
                    }
                },
                error: function (xhr, status, error) {
                    layer.close(load);
                    layer.alert(xhr.responseText, {icon: 5});
                }
            });
        });
    });
</script>
<style type="text/css">
    html,body{
        height: 100%;
        margin: 0;
        padding: 0;
    }
    input:disabled, select:disabled, textarea:disabled {
        background-color: #d6d6d6;
        cursor: default;
    }
    #save_tools {width: 100%; text-align: center; padding: 20px 0px; border-top:1px solid #588c98; position: fixed; z-index: 999; bottom: 0px; background-color:#fff;}
    .datagrid-toolbar {padding: 5px 2px;}
    #step_1 {height: 100%; margin-top: 50px;}
    #step_1 table tr {height: 50px;}
    #step_2 {height: 100%; margin-top: 50px; margin: 50px auto 0px;}
    #step_2 table tr {height: 50px;}
    #step_3 {height: 100%; margin-top: 50px; margin: 50px auto 0px;}
    #step_3 table tr {height: 50px;}
    .finance_info tr {height: 40px !important;}
    #editor_role {overflow: unset;}
</style>
<body>
<div class="layui-tab layui-tab-briefs" style="margin: 0px; height: 50px; width: 100%; position: absolute; z-index: 999">
    <ul class="layui-tab-title" style="background: #385359; border:none; overflow: hidden; height: 48px;">
        <li class="layui-this" style="background:#fff; margin: 8px 0px 0px 30px;"><?= lang('正式下单客户申请');?></li>
    </ul>
</div>
<div class="layui-container" style="width:1350px; height: 100%; min-height: 950px; overflow:hidden;">
    <form id="myform" name="myform" method="post" style="height: 100%">
        <input type="hidden" name="company_type" value="1">
        <!--基础信息-->
        <div id="step_1" style="display: ">
            <div style="padding: 10px 20px 20px; background: #f6f8f5; margin-bottom: 50px height: 950px; width: 520px; float: left; border-right:1px solid #eee;">
                <table class="form_table" style="" border="0" cellspacing="0">
                    <tr style="height: 50px;">
                        <td colspan="2" valign="top" style="text-align: center; font-size: 20px;"><?= lang('客户基本信息');?></td>
                    </tr>
                    <tr>
                        <td><?= lang('英文全称');?> </td>
                        <td>
                            <input class="easyui-textbox" required name="company_name_en" id="company_name_en"
                                   style="width:400px; height: 40px;" value=""
                                   data-options="
                                    onChange: function(newValue, oldValue){
                                        $('#company_name_en').textbox('setValue', newValue.replace(/[^\w|\d|' '|'.']/ig,''));
                                    }
                               "/>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="clear: both"></div>
        </div>
    </form>
    <div style="clear: both"></div>
</div>
<!--底部按钮-->
<div id="save_tools">
    <div id="step_1_button_box">
        <button class="layui-btn" id="save_data" style=" width: 200px; "><?= lang('提交申请');?></button>
    </div>
</div>
<script type="text/javascript" src="/inc/third/layui/layui.js"></script>
<script>
    var laytpl,element,layer,form;       //全局化layui组件
    layui.use(['layer', 'form','laytpl', 'element'], function(){
        form = layui.form;
        laytpl = layui.laytpl;
        element = layui.element;
        layer = layui.layer;
    });
</script>
</body>
</html