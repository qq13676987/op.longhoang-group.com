<script type="text/javascript">
    function isf_message(action){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/bsc_message/create_message/<?= $shipment['id'];?>/AMS/ISF/' + action,
            onBeforeLoad:function(){
                ajaxLoading();
            },
            success: function (res) {
                ajaxLoadEnd();
                $('.reload_message_isf').attr('disabled', false);
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        window.open('/bsc_message/file_read/' + res.data.id);
                        location.reload();
                    });
                }else{
                    $.messager.alert('Tips', res.msg, 'info');
                }
            },
            error: function () {
                ajaxLoadEnd();
                $('.reload_message_isf').attr('disabled', false);
                $.messager.alert('Tips', 'error');
            }
        });
    }
    $(function () {
        $('.reload_message_isf').click(function () {
            // // $(this).attr('disabled', true);
            var action = $(this).attr('action');
            $.messager.confirm('确认对话框', '是否填写额外参数后提交？', function(r){
                if (r){
                    window.open('/bsc_message/message_form/shipment/<?= $shipment['id'];?>/ISF/EFREIGHT');
                }else{
                    isf_message(action);
                }
            });

        });

        $('.send').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');
            var receiver = $(this).attr('receiver');
            var id = $(this).attr('mid');
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/bsc_message/send_message/' + id + '/' + action + '/' + receiver,
                onBeforeLoad:function(){
                    ajaxLoading();
                },
                success: function (res) {
                    ajaxLoadEnd();
                    $('.send').attr('disabled', false);
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg, 'info', function () {
                            location.reload();
                        });
                    }else{
                        $.messager.alert('Tips', res.msg, 'info');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $('.send').attr('disabled', false);
                    $.messager.alert('Tips', 'error');
                }

            });
        });

        //查看报文
        $('.read_message').click(function () {
            var id = $(this).attr('mid');
            window.open('/bsc_message/file_read/' + id);
        });
    });
</script>
<fieldset>
    <legend>ISF发送</legend>
    <?php if(empty($isf)){ ?>
        <button class="reload_message_isf" action="insert">点击生成报文</button>
    <?php }else{?>
        <?php if($isf['receiver'] == 'EFREIGHT' && $isf['type'] == 'ISF'){?>
            <button class="reload_message_isf" action="insert">重新生成报文</button>
            <br />
            发送情况:
            <?php if($isf['status'] == 0){?>
                未发送
            <?php }else if($isf['status'] == 1){
               echo $isf['send_time'];?>
                发送成功
            <?php }else if($isf['status'] == 2){?>
                发送修改成功
            <?php }?>
            <br />
            <button class="read_message" id="isf_read" mid="<?= $isf['id'];?>">查看报文</button>
            <button class="send" action="API" mid="<?= $isf['id'];?>" receiver="<?= $isf['receiver'];?>">发送报文</button>
        <?php }?>
    <?php }?>
</fieldset>
