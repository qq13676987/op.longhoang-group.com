<?php


class mail extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->model = Model('biz_market_mail_model');
    }

    public function index(){
        $data = array();
        
        $inbox_count = $this->get_mail_count(0);
        $inbox['market_remind'] = isset($inbox_count['市场提醒']['count']) ? $inbox_count['市场提醒']['count'] : 0;
        $inbox['consol_cancel'] = isset($inbox_count['订舱退关']['count']) ? $inbox_count['订舱退关']['count'] : 0;
        $inbox['closing'] = isset($inbox_count['预配更新']['count']) ? $inbox_count['预配更新']['count'] : 0;
        $inbox['finance_remind'] = isset($inbox_count['财务提醒']['count']) ? $inbox_count['财务提醒']['count'] : 0;
        $inbox['client_follow_up'] = isset($inbox_count['客户跟踪']['count']) ? $inbox_count['客户跟踪']['count'] : 0;
        $inbox['quotation_update'] = isset($inbox_count['运价更新']['count']) ? $inbox_count['运价更新']['count'] : 0;
        $inbox['operator_remind'] = isset($inbox_count['操作提醒']['count']) ? $inbox_count['操作提醒']['count'] : 0;
        $inbox['error_alert'] = isset($inbox_count['纠错警报']['count']) ? $inbox_count['纠错警报']['count'] : 0;
        $inbox['document_remind'] = isset($inbox_count['截单提醒']['count']) ? $inbox_count['截单提醒']['count'] : 0;
        $data['inbox_count'] = array_sum(array_column($inbox_count, 'count'));
        $data['inbox'] = $inbox;
        
        $outbox_count = $this->get_mail_count(1);
        $outbox['market_remind'] = isset($outbox_count['市场提醒']['count']) ? $outbox_count['市场提醒']['count'] : 0;
        $outbox['consol_cancel'] = isset($outbox_count['订舱退关']['count']) ? $outbox_count['订舱退关']['count'] : 0;
        $outbox['closing'] = isset($outbox_count['预配更新']['count']) ? $outbox_count['预配更新']['count'] : 0;
        $outbox['finance_remind'] = isset($outbox_count['财务提醒']['count']) ? $outbox_count['财务提醒']['count'] : 0;
        $data['outbox_count'] = array_sum(array_column($outbox_count, 'count'));
        $data['outbox'] = $outbox;
        $this->load->view('mail/index_view', $data);
    }
    
    public function get_count(){
        $inbox_count = $this->get_mail_count(0);
        $data['inbox_market_remind'] = isset($inbox_count['市场提醒']['count']) ? $inbox_count['市场提醒']['count'] : 0;
        $data['inbox_consol_cancel'] = isset($inbox_count['订舱退关']['count']) ? $inbox_count['订舱退关']['count'] : 0;
        $data['inbox_closing'] = isset($inbox_count['预配更新']['count']) ? $inbox_count['预配更新']['count'] : 0;
        $data['inbox_finance_remind'] = isset($inbox_count['财务提醒']['count']) ? $inbox_count['财务提醒']['count'] : 0;
        $data['inbox_client_follow_up'] = isset($inbox_count['客户跟踪']['count']) ? $inbox_count['客户跟踪']['count'] : 0;
        $data['inbox_quotation_update'] = isset($inbox_count['运价更新']['count']) ? $inbox_count['运价更新']['count'] : 0;
        $data['inbox_operator_remind'] = isset($inbox_count['操作提醒']['count']) ? $inbox_count['操作提醒']['count'] : 0;
        $data['inbox_error_alert'] = isset($inbox_count['纠错警报']['count']) ? $inbox_count['纠错警报']['count'] : 0;
        $data['inbox_document_remind'] = isset($inbox_count['截单提醒']['count']) ? $inbox_count['截单提醒']['count'] : 0;
        $data['inbox_count'] = array_sum(array_column($inbox_count, 'count'));
        
        echo json_encode(array('code' => 0, 'msg' => 'success', 'data' => $data));
    }

    public function datagrid(){
        $category = isset($_GET['category']) ? $_GET['category'] : '市场提醒';
        $isSender = isset($_GET['isSender']) ? (int)$_GET['isSender'] : 0;
        $data = array();
        
        $data['is_read'] = isset($_GET['is_read']) ? $_GET['is_read'] : 0;
        $data['category'] = $category;
        $this->load->view('mail/head');
        if($category == '市场提醒'){
            if($isSender){
                $this->load->view('mail/outbox/market_remind', $data);
            }else{
                $this->load->view('mail/inbox/details', $data);
            }
        }else if($category == '预配更新'){
            if($isSender){
                $this->load->view('mail/outbox/closing', $data);
            }else{
                $this->load->view('mail/inbox/details', $data);
            }
        }else if($category == '订舱退关'){
            if($isSender){
                $this->load->view('mail/outbox/consol_cancel', $data);
            }else{
                $this->load->view('mail/inbox/details', $data);
            }
        }else if($category == '财务提醒'){
            if($isSender){
                $this->load->view('mail/outbox/finance_remind', $data);
            }else{
                $this->load->view('mail/inbox/details', $data);
            }
        }else if($category == '客户跟踪'){
            if($isSender){
                $this->load->view('mail/outbox/client_follow_up', $data);
            }else{
                $this->load->view('mail/inbox/details', $data);
            }
        }else if($category == '运价更新'){
            if($isSender){
                $this->load->view('mail/outbox/quotation_update', $data);
            }else{
                $this->load->view('mail/inbox/details', $data);
            }
        }else if($category == '操作提醒'){
            if($isSender){
                $this->load->view('mail/outbox/operator_remind', $data);
            }else{
                $this->load->view('mail/inbox/details', $data);
            }
        }else if($category == '纠错警报'){
            if($isSender){
                $this->load->view('mail/outbox/error_alert', $data);
            }else{
                $this->load->view('mail/inbox/details', $data);
            }
        }else if($category == '截单提醒'){
            if($isSender){
                $this->load->view('mail/outbox/document_remind', $data);
            }else{
                $this->load->view('mail/inbox/details', $data);
            }
        }
        else{
            echo '暂无';
        }
        
    }
    
    public function details(){
        $data = array();
        $data['id'] = isset($_GET['id']) ? (int)$_GET['id'] : 0;
        $data['isSender'] = isset($_GET['isSender']) ? (int)$_GET['isSender'] : 0;
        $this->load->view('mail/head');
        $this->load->view('mail/outbox/details', $data);
    }
    
    private function get_mail_count($isSender = 0, $category = ''){
        Model('m_model');
        $userId = get_session('id');
        if($userId == '') $userId = 0;
        $where = array();
        if(getValue('admin') != 'true'){
            if($isSender) $where[] = "created_by = $userId";
            else $where[] = "recipient_by = $userId";
            if($category !== '') $where[] = "category = '{$category}'";
        }
        // $where[] = "end_time >= '" . date('Y-m-d H:i:s') . "'";
        $where[] = "is_delete = 0";
        $where[] = "is_read = 0";
        $where = join(' AND ', $where);
        $sql = "select count(1) as count,category from (SELECT * FROM (`biz_market_mail`) WHERE $where GROUP BY `created_by`, `plan_send_time`, `subject`, `content`) a group by category";
        $rs = $this->m_model->query_array($sql);
        return array_column($rs, null,'category');
    }
    
    /**
     * 一键已读
     */
    public function batch_read(){
        $ids = isset($_POST['mail_ids']) ? $_POST['mail_ids'] : '';
        //只能已读收件人是自己的
        $ids_array = array_filter(array_unique(explode(',', $ids)));
        $result = array('code' => 1, 'msg' => '参数错误');
        if(empty($ids_array)){
            $result['msg'] = '参数不能为空';
            echo json_encode($result);
            return;
        }
        $this->db->select('id');
        $userId = get_session('id');
        $old_rows = $this->model->get("id in (" . join(',', $ids_array) . ") and recipient_by = $userId and is_read = 0");
        
        $update_datas = array();
        foreach ($old_rows as $old_row){
            $update_data = array();
            $update_data['id'] = $old_row['id'];
            $update_data['is_read'] = 1;
            
            $update_datas[] = $update_data;
        }
        
        foreach ($update_datas as $update_data){
            $this_id = $update_data['id'];
            unset($update_data['id']);
            
            $this->model->update($this_id, $update_data);
            
            if(empty($update_data)) continue;
            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_market_mail";
            $log_data["key"] = $this_id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($update_data);
            log_rcd($log_data);
        }
        
        $result['code'] = 0;
        $result['msg'] = 'success';
        echo json_encode($result);
    }

    /**
     * 获取shipment或consol相关的邮箱
     * @return bool
     */
    public function get_email(){
        $id_type = getValue('id_type', '');
        $id_no = getValue('id_no', '');
        $rows = array();
        if($id_type == 'shipment'){
            $this->db->select("client_email,client_code,client_code2");
            $shipment = Model("biz_shipment_model")->get_where_one_all("id = $id_no");
//            $rows[] = array('name' => '委托方邮箱', 'email' => $shipment['client_email']);
            $yiji = $this->db->where("client_code = '{$shipment['client_code']}' and role in ('logistics_client','factory') and email != ''")->get('biz_client_contact_list')->result_array();
            if(!empty($yiji)){
                if(count($yiji) > 1){
                    foreach ($yiji as $k=>$v){
                        $num = $k+1;
                        $rows[] = array('name' => '1级委托方邮箱'.$num, 'email' => $v['email']);
                    }
                }else{
                    $rows[] = array('name' => '1级委托方邮箱', 'email' => $yiji[0]['email']);
                }
            }
            if(!empty($shipment['client_code2'])){
                $erji = $this->db->where("client_code = '{$shipment['client_code2']}' and role in ('logistics_client','factory') and email != ''")->get('biz_client_contact_list')->result_array();
                if(!empty($erji)){
                    if(count($erji) > 1){
                        foreach ($erji as $k=>$v){
                            $num = $k+1;
                            $rows[] = array('name' => '2级委托方邮箱'.$num, 'email' => $v['email']);
                        }
                    }else{
                        $rows[] = array('name' => '2级委托方邮箱', 'email' => $erji[0]['email']);
                    }
                }
            }
            $biz_bill_option_company = $this->db->distinct('client_code')->where(['id_type'=>'shipment','billing_type'=>'sell','id_no'=>$id_no,'role'=>'fob工厂'])->get('biz_bill_option_company')->result_array();
            if(!empty($biz_bill_option_company)){
                $client_codes = array_column($biz_bill_option_company,'client_code');
                foreach ($client_codes as $k=>$client_code){
                    $biz_client_contact_list = $this->db->where("client_code = '{$client_code}' and role in ('logistics_client','factory') and email != ''")->get('biz_client_contact_list')->result_array();
                    if(!empty($biz_client_contact_list)){
                        $num1 = $k+2+1;
                        if(count($biz_client_contact_list) > 1){
                            foreach ($biz_client_contact_list as $kk=>$vv){
                                $num = $kk+1;
                                $rows[] = array('name' => $num1.'级委托方邮箱'.$num, 'email' => $vv['email']);
                            }
                        }else{
                            $rows[] = array('name' => $num1.'级委托方邮箱', 'email' => $biz_client_contact_list[0]['email']);
                        }
                    }
                }
            }
            $dutys = Model('biz_duty_model')->get_new("id_type = 'biz_shipment' and id_no = '{$id_no}'");
            foreach ($dutys as $duty){
                $rows[] = array('name' => $duty['user_role'], 'email' => getUserField($duty['user_id'],'email'));
            }
        }
        return jsonEcho($rows);
    }

    /**
     * 获取内部员工的邮箱
     */
    public function inner_email(){
        $email = trim(postValue('email', ''));

        $where = array();
        if(!empty($email)) $where[] = "email like '%$email%' or name like '%$email%'";

        $where_str = join(' and ', $where);

        $this->db->limit(30);
        $rs = Model('bsc_user_model')->get($where_str);

        $rows = array();
        foreach ($rs as $row){
            $rows[] = array('name' => $row['name'], 'email' => $row['email']);
        }

        return jsonEcho($rows);
    }

    /**
     * 获取客户的邮箱 客户邮箱要根据当前的用户权限来,且只能向后匹配
     */
    public function client_email(){
        $email = trim(postValue('email', ''));
        if(empty($email)) return jsonEcho(array(array('name' => '<span style="color:red;">请输入搜索客户邮箱, 只支持向后匹配</span>', 'email' => '')));

        $where = array();

        //加入权限的获取 加在这里是因为下面的异常需要在这里添加额外条件
        $role_where = Model('biz_client_model')->read_role_version2();
        //加入权限条件
        if($role_where != '') $where[] = $role_where;
        $where[] = "email != ''";
        if(!empty($email)) $where[] = "email like '{$email}%' or name like '%$email%'";

        $where_str = join(' and ', $where);

        $this->db->limit(30);
        $this->db->select("DISTINCT biz_client_contact_list.name,biz_client_contact_list.email", false);
        $this->db->join("biz_client_duty", "biz_client_contact_list.client_code = biz_client_duty.client_code", "left");
        $rs = Model('biz_client_contact_model')->get($where_str, 'biz_client_contact_list.id');

        $rows = array();

        foreach ($rs as $row){
            $rows[] = array('name' => $row['name'], 'email' => $row['email']);
        }

        return jsonEcho($rows);
    }
}