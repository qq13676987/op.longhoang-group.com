<script type="text/javascript">

    $(function () {
        $('#tt').edatagrid({
            url: '/sys_config_title/get_data/<?php echo $table_name;?>',
            saveUrl: '/sys_config_title/add_data/<?php echo $table_name;?>',
            updateUrl: '/sys_config_title/update_data/<?php echo $table_name;?>',
            destroyUrl: '/sys_config_title/delete_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            }
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height()
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });

</script>
<body>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th field="id" width="60" sortable="true">ID</th>
        <th field="table_name" width="200" align="center" sortable="true">table_name</th>
        <th field="table_field" width="200" align="center" editor="{type:'validatebox',options:{required:true}}">table_field</th>
        <th field="field_name" width="200" editor="{type:'validatebox',options:{required:true}}">field_name</th>
        <th field="width" width="200" editor="{type:'numberbox',options:{required:true}}">width</th>
        <th field="order_by" width="150" align="left" sortable="true" editor="{type:'numberbox',options:{required:true}}">order_by
        </th>
        <th field="sortable" width="150" align="left" sortable="true" editor="{type:'numberbox',options:{required:true}}">sortable</th>
    </tr>
    </thead>
</table>
</body>
<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
                   onclick="javascript:$('#tt').edatagrid('addRow',0)"><?= lang('add'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
                   onclick="javascript:$('#tt').edatagrid('destroyRow')"><?= lang('delete'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true"
                   onclick="javascript:$('#tt').edatagrid('saveRow')"><?= lang('save'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true"
                   onclick="javascript:$('#tt').edatagrid('cancelRow')"><?= lang('cancel'); ?></a>
            </td>
        </tr>
    </table>

</div>
