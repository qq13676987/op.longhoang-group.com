<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>Client-Edit</title>

<script type="text/javascript">
    var is_update = false;

    window.onbeforeunload = function (e) {
        // 兼容IE8和Firefox 4之前的版本
        //检测是否有字段变动
        e = e || window.event;
        var dialogText = '您有未保存的内容,是否确认退出';

        if (is_update) {
            if (e) {
                e.returnValue = dialogText;
            }
            // Chrome, Safari, Firefox 4+, Opera 12+ , IE 9+
            return dialogText;
        }
    };

    //角色类型是否重复
    function check_client(val){
        var tmp_val=val;
        if (val == 'factory') tmp_val='factoryclient';
        var selected = $('#role1').combobox('getValues');
        selected.pop(); //删除刚刚选择的角色
        //把已选择的角色拼接成字符串
        var selected_str = selected.join(',');
        selected_str = selected_str.replace('factory', 'factoryclient');

        //已选择的角色是否已包含了client类型
        var selected_client = selected_str.search(/client/);
        //刚刚选中的角色是否包含client
        var now_select_client = tmp_val.search(/client/);
        if(selected_client >= 0 && now_select_client >= 0){
            alert('只能选择一个“client”类型的角色！')
            $('#role1').combobox('unselect', val);
        }
    }

    $(function () {
        $('#company_name').textbox('setValue', '<?= es_encode($company_name);?>');
        $('#company_name_en').textbox('setValue', '<?= es_encode($company_name_en);?>');
        $('#client_name').textbox('setValue', '<?= es_encode($client_name);?>');
        $('#contact_name').combobox('setValue', '<?= es_encode($contact_name);?>');
        // $('#bank_name_usd').combobox('setValue', '<?= es_encode($bank_name_usd);?>');
        // $('#bank_name_cny').combobox('setValue', '<?= es_encode($bank_name_cny);?>');
        role_select();
        $('#f').form({
            onChange: function (target) {
                if ($(target).attr('id') !== 'finance_describe') {

                    is_update = true;
                }
            }
        });
        $('#tb').tabs({

            border: false,
            onSelect: function (title) {
                if (title == "Contact list") {
                    if (document.getElementById("contact_list").src == "") {
                        document.getElementById("contact_list").src = "/biz_client_contact/index/<?php echo $client_code;?>";
                    }
                }
                if (title == "Contract list") {
                    if (document.getElementById("contract_list").src == "") {
                        document.getElementById("contract_list").src = "/biz_client_contract/index/<?php echo $client_code;?>";
                    }
                }
                if (title == "Account list") {
                    if (document.getElementById("account_list").src == "") {
                        document.getElementById("account_list").src = "/biz_client_account/index/<?php echo $client_code;?>";
                    }
                }
                if (title == "Upload files") {
                    if (document.getElementById("upload_files_sub").src == "") {
                        document.getElementById("upload_files_sub").src = "/bsc_upload/upload_files/biz_client/<?php echo $id;?>";
                    }
                }
                if (title == "Auto rate client") {
                    if (document.getElementById("auto_rate_client").src == "") {
                        document.getElementById("auto_rate_client").src = "/biz_auto_rate/client_index/client/<?= $client_code;?>";
                    }
                }
                if (title == "Auto rate booking agent") {
                    if (document.getElementById("auto_rate_booking_agent").src == "") {
                        document.getElementById("auto_rate_booking_agent").src = "/biz_auto_rate/client_index/booking_agent/<?= $client_code;?>";
                    }
                }

                if (title == "Charge") {
                    if (document.getElementById("charge").src == "") {
                        document.getElementById("charge").src = "/biz_client_charge/index/<?= $client_code;?>";
                    }
                }


                if (title == "B/L company") {
                    if (document.getElementById("B/L_company").src == "") {
                        document.getElementById("B/L_company").src = "/biz_company/index/consignee/<?= $client_code;?>";
                    }
                }
                if (title == 'Background') {
                    if (document.getElementById("background").src == "") {
                        document.getElementById("background").src = "/biz_client_background/index/<?= $client_code;?>";
                    }
                }
                if (title == 'Second Client') {
                    if (document.getElementById("second_client").src == "") {
                        document.getElementById("second_client").src = "/biz_client_second_relation/index?client_code=<?= $client_code;?>";
                    }
                }
                if (title == 'oversea_agent') {
                    if (document.getElementById("oversea_agent").src == "") {
                        document.getElementById("oversea_agent").src = "/biz_carrier_agent/index/<?= $client_code;?>";
                    }
                }
                if (title == 'Port Code') {
                    if (document.getElementById("port_code").src == "") {
                        document.getElementById("port_code").src = "/biz_port/index/<?= $client_code;?>";
                    }
                }
                if (title == 'S/C No') {
                    if (document.getElementById("sc_no").src == "") {
                        document.getElementById("sc_no").src = "/biz_client_sc_no/index?client_code=<?= $client_code;?>";
                    }
                }
                if (title == 'user_commision_rate') {
                    if (document.getElementById("user_commision_rate").src == "") {
                        document.getElementById("user_commision_rate").src = "/bsc_user_client_commision_rate/index/<?= $client_code;?>";
                    }
                }
                if (title == 'Dict') {
                    if (document.getElementById("dict").src == "") {
                        document.getElementById("dict").src = "/bsc_dict/client_dict/<?= $client_code;?>";
                    }
                }
                if (title == 'Log') {
                    if (document.getElementById("log").src == "") {
                        document.getElementById("log").src = "/sys_log/index?table_name=biz_client&key_no=<?= $id?>";
                    }
                }
            }

        });
        $('#client_code').textbox('textbox').keyup(function () {
            var val = $(this).val().toUpperCase();
            $(this).val(val.replace(/[^\a-zA-Z0-9]/g, ''));
        });
        $('.finance_od_basis').click(function () {
            // var val = $(this).val();
            // if(val == 'monthly'){
            //     $('#finance_payment_month').textbox('enable');
            //     $('#finance_payment_day_th').textbox('enable');
            //     $('#finance_payment_days').textbox('disable');
            // }else if(val == 'daily'){
            //     $('#finance_payment_month').textbox('disable');
            //     $('#finance_payment_day_th').textbox('disable');
            //     $('#finance_payment_days').textbox('enable');
            // }
            get_finance_describe();
        });

        $('.finance_payment').click(function () {
            get_finance_describe();
        });

        //初始值
        var finance_payment = $('#finance_payment').val();
        if (finance_payment != null && finance_payment != '') {
            $(".finance_payment[value='" + finance_payment + "']").attr("checked", true);
        }

        var finance_od_basis = $('#finance_od_basis').val();
        if (finance_od_basis != null && finance_od_basis != '') {
            $(".finance_od_basis[value='" + finance_od_basis + "']").attr("checked", true);
        }
        $('.finance_od_basis:checked').trigger('click');
        $('.tags').on('click', '.tag-remove', function () {
            var tag_val = $('#tag').val();
            var tag_val_arr = tag_val.split(',');
            var this_val = $(this).siblings('.tag-span').text();
            tag_val_arr.splice($.inArray(this_val, tag_val_arr), 1);
            $('#tag').val(tag_val_arr.join(','));
            $(this).parent('.tag').remove();
        });

        //2022-06-28 获取选中tab tb
        <?php if(!empty($tab_title)):?>
        $("#tb").tabs('select', '<?=$tab_title;?>');
        <?php endif;?>

    })

    function role_select() {
        if ($('#role1').length > 0) {
            $('#role').val($('#role1').combobox('getValues'));
        }
    }

    function checkvalue() {
        var vv = $('#client_code').textbox('getValue');
        if (vv == "") {
            alert("client_code required！");
            return false;
        }
        var vv = $('#client_name').textbox('getValue');
        if (vv == "") {
            alert("client_name required！");
            return false;
        }
		var vv = $('#finance_belong_company_code').combobox('getValues');
		if (vv.length == 0) {
			alert("<?=lang('归属公司必填');?>");
			return false;
		}
        //2022-08-08 新增黑名单备注
        //已修复黑名单备注不存在时,报错的问题
        if($.data($('#client_level_remark_id')).combobox !== undefined){
            var client_level_remark_id = $('#client_level_remark_id').combobox('getValue');
            if(client_level_remark_id=='2'||client_level_remark_id=='3'){
                var vv = $('#client_level_remark').textbox('getValue');
                if (vv == "") {
                    alert("黑名单详情不能为空");
                    return false;
                }
            }
        }

        // $('#party_b').val($('#party_b').combobox('setValues', ['001','002']);
        $('#finance_sign_company_code').val($('#finance_sign_company_code_select').combobox('getValues').join(','));
        
        // var vv = $('#sales_id').combobox('getValue');
        // if (vv == "") {
        //     alert("sales required！");
        //     return false;
        // }
        // $('#sailing_area').combobox('setValue',$('#sailing_area').combobox('getValues').join(','));
        $('#dn_accounts').val($('#dn_accounts1').combobox('getValues').join(','));
		$('#finance_belong_company_code_val').val($('#finance_belong_company_code').combobox('getValues').join(','));
		let v1 = $('#finance_sign_company_code_select').combobox('getValues')
		let v2 = $('#finance_belong_company_code').combobox('getValues')
		if(v1.length > 0){
			if(v1.filter(item => v2.includes(item)).length != v1.length){
				alert("<?=lang('归属公司必须包含签约子公司');?>");
				return;
			}
		}
		ajaxLoading();
        is_update = false;
        document.f.submit();
    }

    //获取综合描述
    function get_finance_describe() {
        var finance_payment = $('.finance_payment:checked').val();
        var finance_od_basis = $('.finance_od_basis:checked').val();

        var lang = '';
        if (finance_od_basis == 'monthly') {
            <?php if(in_array("finance_payment_month", $G_userBscEdit)){ ?>
            $('#finance_payment_month').textbox('enable');
            <?php } ?>
            <?php if(in_array("finance_payment_day_th", $G_userBscEdit)){ ?>
            $('#finance_payment_day_th').textbox('enable');
            <?php } ?>
            $('#finance_payment_days').textbox('disable');

            var finance_payment_month = $('#finance_payment_month').numberbox('getValue');
            var finance_payment_day_th = $('#finance_payment_day_th').numberbox('getValue');

            if (finance_payment == 'after work') {
                lang = '<?= lang('Payment on the (day)th day of (month) months after work');?>';
            } else if (finance_payment == 'after invoice') {
                lang = '<?= lang('Payment on the (day)th day of (month) months after invoice');?>';
            }

            lang = lang.replace('(month)', finance_payment_month).replace('(day)', finance_payment_day_th);

        } else if (finance_od_basis == 'daily') {
            $('#finance_payment_month').textbox('disable');
            $('#finance_payment_day_th').textbox('disable');
            <?php if(in_array("finance_payment_days", $G_userBscEdit)){ ?>
            $('#finance_payment_days').textbox('enable');
            <?php } ?>


            var finance_payment_days = $('#finance_payment_days').numberbox('getValue');

            if (finance_payment == 'after work') {
                lang = '<?= lang('Payment (day) days after work');?>';
            } else if (finance_payment == 'after invoice') {
                lang = '<?= lang('Payment (day) days after invoice');?>';
            }
            lang = lang.replace('(day)', finance_payment_days);
        }
        $('#finance_describe').textbox('setValue', lang);
    }

    function reload_select(type = 'combobox') {
        if (type == 'combobox') {
            $('#contact_name').combobox('reload');
            $('#bank_name_cny').combobox('reload');
            $('#bank_name_usd').combobox('reload');
        }
    }

    function select_all(id, name) {
        var ed = $('#' + id);
        var data = ed.combobox('getData');
        var this_val = ed.combobox('getValues');
        var val = [];
        $.each(data, function (index, item) {
            val.push(item[name]);
        });
        val = val.join(',');
        this_val = this_val.join(',');
        if (val !== this_val) {
            ed.combobox('setValues', val);
        } else {
            ed.combobox('setValues', '');
        }
    }

    function reset_over_credit(client_code) {
        $.ajax({
            type: 'POST',
            url: '/other/client_credit',
            data: {client_code: client_code},
            dataType: 'json',
            success: function (res) {
                $.messager.alert('Tips', res.msg);
            },
            error: function () {
                $.messager.alert('Tips', 'error');
            }
        });
    }

    function overdue_info(client_code) {
        window.open('/biz_bill/overdue_info/' + client_code);
    }

    //查找相似度
    function search_similar() {
        var company_name = $('#company_name').textbox('getValue');
        if (company_name === '') {
            $.messager.alert('Tips', '先填写全称');
            return;
        }
        ajaxLoading();
        is_search = true;
        $.ajax({
            type: 'GET',
            url: '/biz_client/search?company_name=' + company_name + '&id=<?= $id;?>',
            success: function (res_json) {
                ajaxLoadEnd();
                var res;
                try {
                    res = $.parseJSON(res_json);
                } catch (e) {

                }
                if (res == undefined) {
                    $.messager.alert('Tips', '发生错误,请联系管理员');
                } else {
                    if (res.code == 0) {
                        $.messager.alert('Tips', res.msg + '<br />最接近的为' + res.data.company_name + ',相似度为' + res.data.percent + '%');
                    } else {
                        $.messager.alert('Tips', res.msg);
                    }

                }
            },
        });
    }

    function re_select(id) {
        var inp = $('#' + id);
        var val = inp.combobox('getValue');
        inp.combobox('reload').combobox('clear').combobox('select', val);
    }
</script>
<style>
    input:disabled, select:disabled, textarea:disabled {
        background-color: #d6d6d6;
        cursor: default;
    }

    input:read-only, select:read-only, textarea:read-only {
        background-color: #F0F0F0;
        cursor: default;
    }

    .tag {
        position: relative;
        display: inline-block;
        background-color: #00BFFF;
        height: 18px;
        line-height: 18px;
        margin-right: 2px;
    }

    .tag-span {
        font-size: 12px;
        text-align: center;
        color: #fff;
        border-radius: 2px;
        padding-left: 6px;
        padding-right: 16px;
    }

    .tag-remove {
        background: url('/inc/image/tagbox_icons.png') no-repeat -16px center;
        position: absolute;
        display: block;
        width: 16px;
        height: 16px;
        right: 2px;
        top: 50%;
        margin-top: -8px;
        opacity: 0.6;
        filter: alpha(opacity=60);
    }
</style>
<body style="margin:10px;">
<table>
    <tr>
        <td>
            <h2> Edit </h2></td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td><?php echo $client_code; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $company_name; ?></td>
        <td></td>
    </tr>
</table>

<div id="tb" class="easyui-tabs" style="width:100%;height:850px">
    <div title="Basic info" style="padding:1px">
        <form id="f" name="f" method="post" action="/biz_client/update_data/<?php echo $id; ?>">
            <div class="easyui-layout" style="width:100%;height:780px;">
                <div data-options="region:'west',tools:'#tt'" title="Basic" style="width:490px;">
                    <table width="450" border="0" cellspacing="0">
                        <tr>
                            <td><?= lang('id'); ?></td>
                            <td>
                                <input class="easyui-textbox" name="id"
                                       id="id" readonly="true" style="width:255px;"
                                       value="<?php echo $id; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('client_code'); ?></td>
                            <td>
                                <input class="easyui-textbox" name="country_code"
                                       id="country_code"
                                       readonly="true" <?php echo in_array("country_code", $G_userBscEdit) ? '' : 'disabled'; ?>
                                       style="width:80px;"
                                       value="<?php echo $country_code; ?>"/>
                                <input class="easyui-textbox" name="client_code"
                                       id="client_code"
                                       readonly="true" <?php echo in_array("client_code", $G_userBscEdit) ? '' : 'disabled'; ?>
                                       data-options="required:true" style="width:175px;"
                                       value="<?php echo $client_code; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('company_name'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="company_name"
                                       id="company_name" <?php if ($country_code == 'CN' && !is_admin()) echo 'readonly'; ?> <?php echo in_array("company_name", $G_userBscEdit) ? '' : 'disabled'; ?>
                                       style="width:255px;"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('company_name_en'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="company_name_en"
                                       id="company_name_en" <?php echo in_array("company_name_en", $G_userBscEdit) ? '' : 'disabled'; ?>
                                       style="width:255px;"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('client_name'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="client_name"
                                       id="client_name" <?php echo in_array("client_name", $G_userBscEdit) ? '' : 'disabled'; ?>
                                       style="width:190px;"/>  
                                <?php if(is_admin()){ ?>
                                <a id="api_config_window_click" href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'icon-search',onClick:api_config_window_click" style=" <?= $own_company_flag == 1 ? '' : 'display:none;';?>"><?= lang('配置');?></a>
                                <?php }?>
                                <script>
                                    /**
                                     * API配置按钮的点击事件
                                     */
                                    function api_config_window_click(){
                                        var client_code = $('#client_code').val();
                                        var api_config_window = $('#api_config_window');
                                        if(api_config_window.length == 0){
                                            api_config_window = $("<div />").attr('id', 'api_config_window');
                                        }
                                        api_config_window.window({
                                            width:600,
                                            height:400,
                                            title:'<?= lang('API接口配置');?>',
                                            href: '/bsc_dict/client_api_config/' + client_code,
                                        });
                                    }

                                    $(function () {
                                        $("#own_company_flag").click(function () {
                                            var this_checked = $(this).prop('checked');
                                            //如果选中
                                            if(this_checked){
                                                $('#api_config_window').css('display', 'inline-block');
                                                //显示配置按钮,点开进入接口url配置网址
                                            }else{
                                                $('#api_config_window').css('display', 'none');
                                            }
                                        });
                                    });
                                </script>
                            </td>
                        </tr>
                        <!--<tr>
                            <td><?= lang('client area'); ?></td>
                            <td>
                                <select class="easyui-combobox" name="province"
                                        id="province" <?php echo in_array("province", $G_userBscEdit) ? '' : 'disabled'; ?>
                                        data-options="
                                valueField:'cityname',
                                textField:'cityname',
                                url:'/city/get_province',
                                value: '<?= $province; ?>',
                                onSelect: function(rec){
                                    if(rec != undefined){
                                        $('#city').combobox('reload', '/city/get_city/' + rec.id);
                                    }
                                },
                                onLoadSuccess:function(){
                                    var this_data = $(this).combobox('getData');
                                    var this_val = $(this).combobox('getValue');
                                    var rec = this_data.filter(el => el.cityname === this_val);
                                    if(rec.length > 0)rec = rec[0];
                                    $('#city').combobox('reload', '/city/get_city/' + rec.id);
                                }
                                " style="width: 80px;"></select>

                                <select class="easyui-combobox" name="city"
                                        id="city" <?php echo in_array("city", $G_userBscEdit) ? '' : 'disabled'; ?>
                                        style="width: 80px;" data-options="
                                    valueField:'cityname',
                                    textField:'cityname',
                                    value: '<?= $city; ?>',
                                    onSelect: function(rec){
                                        if(rec != undefined){
                                            $('#area').combobox('reload', '/city/get_area/' + rec.id);
                                        }
                                    },
                                    onLoadSuccess:function(){
                                        var this_data = $(this).combobox('getData');
                                        var this_val = $(this).combobox('getValue');
                                        var rec = this_data.filter(el => el['cityname'] === this_val);
                                        if(rec.length > 0)rec = rec[0];
                                        $('#area').combobox('reload', '/city/get_area/' + rec.id);
                                    }
                                "></select>

                                <select class="easyui-combobox" name="area"
                                        id="area" <?php echo in_array("area", $G_userBscEdit) ? '' : 'disabled'; ?>
                                        style="width: 80px;" data-options="
                                    valueField:'cityname',
                                    textField:'cityname',
                                    value: '<?= $area; ?>',
                                "></select>
                            </td>
                        </tr>-->
                        <tr>
                            <td><?= lang('company_address'); ?></td>
                            <td>
                                <textarea name="company_address"
                                          id="company_address" <?php echo in_array("company_address", $G_userBscEdit) ? '' : 'disabled'; ?> style="width:250px;"><?php echo $company_address; ?></textarea>
                            </td>
                        </tr>

                        <!--<tr>-->
                        <!--    <td><?= lang('sailing_area'); ?></td>-->
                        <!--    <td>-->
                        <!--        <select class="easyui-combobox" <?php echo in_array("sailing_area", $G_userBscEdit) ? '' : 'disabled'; ?> name="sailing_area" id="sailing_area" style="width:255px;" data-options="-->
                        <!--            valueField:'sailing_area',-->
                        <!--             multiple:true,-->
                        <!--            textField:'sailing',-->
                        <!--            url:'/biz_port/get_sailing',-->
                        <!--            value:'<?php echo $sailing_area; ?>',-->
                        <!--        ">-->
                        <!--        </select>-->
                        <!--    </td>-->
                        <!--</tr>-->
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>------------------------</td>
                            <td>
                                ------------------------------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('deliver_info'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="deliver_info"
                                       id="deliver_info" <?php echo in_array("deliver_info", $G_userBscEdit) ? '' : 'disabled'; ?>
                                       style="width:255px;" value="<?php echo $deliver_info; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('remark'); ?> </td>
                            <td>
                                <textarea name="remark"
                                          id="remark" <?php echo in_array("remark", $G_userBscEdit) ? '' : 'disabled'; ?> style="width:250px;"><?php echo $remark; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>------------------------</td>
                            <td>
                                ------------------------------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('sales'); ?></td>
                            <td>
                                <input class="easyui-textbox" disabled style="width: 190px"
                                       value="<?php foreach (explode(',', $sales_ids) as $r) {
                                           echo getUserName($r) . ',';
                                       } ?>">
                                <span style="color: red">&nbsp;<?php if($is_open == 1) echo '公海状态';?></span>
                            </td>
                        </tr>
                        <!--<tr>
                            <td>引进人<?php show_tip("用于审批延期申请");?></td>
                            <td>
                                <select class="easyui-combogrid default_partner" name="supplier_admin_ids"
                                        editable="true" id="supplier_admin_ids1" ids="supplier_admin_ids" style="width: 255px"></select>
                            </td>
                        </tr>-->

                        <?php if ($read_config) { ?>
                            <input type="hidden" <?php echo in_array("read_user_group", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   name="read_user_group" id="read_user_group" value="<?= $read_user_group; ?>">
                        <?php } ?>
                        <?php if ($read_config || (in_array(get_session('id'), array(20043, 20047, 20147, 20005, 20288, 20013)) && in_array('oversea_agent', explode(',', $role)))) { ?>
                            <tr>
                                <td><?= lang('level'); ?></td>
                                <td>
                                    <input type="radio" <?php echo in_array("client_level", $G_userBscEdit) ? '' : 'disabled'; ?>
                                           name="client_level" id="client_level1" onclick="black(this)"
                                           value="1" <?php if ($client_level == 1) echo 'checked'; ?>><?= lang('formal'); ?>
                                    <input type="radio" <?php echo in_array("client_level", $G_userBscEdit) ? '' : 'disabled'; ?>
                                           name="client_level" id="client_level0" onclick="black(this)"
                                           value="0" <?php if ($client_level == 0) echo 'checked'; ?>><?= lang('potential'); ?>
                                    <input type="radio" <?php echo in_array("client_level", $G_userBscEdit) ? '' : 'disabled'; ?>
                                           name="client_level" id="client_level-1" onclick="black(this)"
                                           value="-1" <?php if ($client_level == -1) echo 'checked'; ?>><?= lang('黑名单'); ?>
                                </td>
                            </tr>

                            <tr id="blacklist" <?php if ($client_level != -1) echo 'hidden'; ?> >
                                <td>黑名单备注</td>
                                <td>
                                    <select <?php echo in_array("client_level_remark_id", $G_userBscEdit) ? '' : 'disabled'; ?>
                                            id="client_level_remark_id" class="easyui-combobox"
                                            name="client_level_remark_id" style="width:255px;"
                                            data-options="value:'<?= $client_level_remark_id; ?>',onSelect:function(data){
                            black2(data);
                                    }">
                                        <option value="0"></option>
                                        <option value="1">业务冲突</option>
                                        <option value="2">运费有未付</option>
                                        <option value="3">备注</option>
                                    </select>
                                </td>
                            </tr>
                            <tr id="blacklist2" <?php if ($client_level_remark_id == 0 || $client_level_remark_id == 1) echo 'hidden'; ?> >
                                <td id="blacktitle">
                                    <?php if($client_level_remark_id==2):?>
                                        (几月未付)详情
                                    <?php elseif ($client_level_remark_id==3): ?>
                                        (备注)详情
                                    <?php endif;?>
                                </td>
                                <td>
                                    <input <?php echo in_array("client_level_remark", $G_userBscEdit) ? '' : 'disabled'; ?>
                                            id="client_level_remark" name="client_level_remark" style="width:255px;"
                                            class="easyui-textbox" data-options="value:'<?= $client_level_remark; ?>',required:true">
                                </td>
                            </tr>
                            <script>
                                function black(e) {
                                    var val = $(e).val();
                                    if (val == '-1') {
                                        $('input:radio[name="client_level"]:checked').val(val)
                                        $('#blacklist').show();
                                    } else {
                                        $('input:radio[name="client_level"]:checked').val(val)
                                        $('#blacklist').hide();
                                        $('#blacklist2').hide();
                                        $('#client_level_remark_id').combobox('setValue', '0');
                                        $('#client_level_remark').textbox('setValue', '');

                                    }
                                }

                                function black2(data) {
                                    console.log(data.value);
                                    if (data.value == '2') {
                                        $('#blacklist2').show();
                                        $('#client_level_remark').textbox('setValue', '');
                                        $('#blacktitle').html('(几月未付)详情');
                                    } else if (data.value == '3') {
                                        $('#blacklist2').show();
                                        $('#client_level_remark').textbox('setValue', '');
                                        $('#blacktitle').html('(备注)详情');
                                    } else {
                                        $('#blacklist2').hide();
                                        $('#client_level_remark').textbox('setValue', '');
                                    }
                                }

                            </script>

                        <?php } else { ?>
                        <tr>
                            <td><?= lang('level'); ?></td>
                            <td>
                                <input type="radio" disabled
                                       name="client_level" id="client_level1" 
                                       value="1" <?php if ($client_level == 1) echo 'checked'; ?>><?= lang('formal'); ?>
                                <input type="radio" disabled
                                       name="client_level" id="client_level0" 
                                       value="0" <?php if ($client_level == 0) echo 'checked'; ?>><?= lang('potential'); ?>
                                <input type="radio" disabled
                                       name="client_level" id="client_level-1" 
                                       value="-1" <?php if ($client_level == -1) echo 'checked'; ?>><?= lang('黑名单'); ?>
                            </td>
                        </tr>
                        <input type="hidden" <?php echo in_array("client_level", $G_userBscEdit) ? '' : 'disabled'; ?>
                               name="client_level" value="<?= $client_level; ?>" >
                        <?php } ?>

                </td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td><?= lang('role'); ?> 
                    <a href="javascript:void;" class="easyui-tooltip" data-options="
                                content: '<div>下拉框内容是根据contact来的,<br/>' +
                                '1、下拉框里没有您需要的角色?在contact里添加对应角色的联系人后刷新页面.' +
                                '</div>',
                            "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                            </td>
                    <td>
                        <select id="role1" class="easyui-combobox role1"
                                name="role1" <?php echo in_array("role", $G_userBscEdit) && $read_config ? '' : 'disabled'; ?>
                                style="width:255px;" data-options="required:true,
                                    multiple:true,
                                    editable:false,
    								valueField:'value',
    								textField:'value',
    								url:'/bsc_dict/get_option/role',
    								value:'<?php echo $role; ?>',
    								onChange: function(newVal, oldVal){
    								    role_select();
                                        load_role_tag();
    								},
    								onSelect:function(res){
    								    check_client(res.value);
    								}
								">
                        </select>
                        <!-- 这是之前的根据联系获取角色 /biz_client_contact/get_role/<?= $client_code; ?> -->
                        <!--此处版本又回到了以前的,不过这里角色验证改到后台了,如果后台角色与联系人验证不通过,且当前为正式客户,那么不给保存-->
                        <input type="hidden" id="role"
                               name="role" <?php echo in_array("role", $G_userBscEdit) ? '' : 'disabled'; ?>
                               value="<?php echo $role; ?>">
                        <a href="javascript:void;" class="easyui-tooltip" data-options="
                                    content: $('<div></div>'),
                                    onShow: function(){
                                        $(this).tooltip('arrow').css('left', 20);
                                        $(this).tooltip('tip').css('left', $(this).offset().left);
                                    },
                                    onUpdate: function(cc){
                                        cc.panel({
                                            width: 500,
                                            height: 'auto',
                                            border: false,
                                            href: '/bsc_help_content/help/role'
                                        });
                                    }
                                "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <!--<span style="color:red;">点蓝色设置人员</span>-->
                    </td>
                    <td>
                        <div class="role_tag_box" style="width: 285px;">
                        </div>
                    </td>
                </tr>
                <?php
                    $default_partner = array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'finance');
                    foreach ($default_partner as $k){
                        $vk = $k . "_ids";
                        $v = isset($$vk) ? $$vk : '';
                        if(empty($v)) continue;
                        ?>
                        <tr>
                            <td><?= lang($k); ?></td>
                            <td>
                                
                                <?php foreach (explode(',', $v) as $r) {
                                           echo getUserName($r) . ', ';
                                       } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
                <br><br> <br><br> <br><br>
                <div id="tt">
                    <a href="javascript:void(0)" class="icon-save" onclick="<?php if(!$r_e) echo 'checkvalue()';?>"
                       title="save"
                       style="margin-right:15px;"></a>
                </div>
            </div>
            <div data-options="region:'center',title:'Finance'">
                <table width="700" border="0" cellspacing="0">
                    <!--<tr>
                            <td><? /*= lang('finance_payment'); */ ?> </td>
                            <td>
                                <input class="easyui-textbox" name="finance_payment"
                                       id="finance_payment" <?php /*echo in_array("finance_payment", $G_userBscEdit) ? '' : 'disabled'; */ ?>
                                       style="width:255px;" value="<?php /*echo $finance_payment; */ ?>"/>
                            </td>
                        </tr>-->
                    <!--<tr>
                            <td><? /*= lang('contract_no'); */ ?> </td>
                            <td>
                                <input class="easyui-textbox" name="contract_no"
                                       id="contract_no" <?php /*echo in_array("contract_no", $G_userBscEdit) ? '' : 'disabled'; */ ?>
                                       style="width:255px;" value="<?php /*echo $contract_no; */ ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><? /*= lang('contract_expiry'); */ ?> </td>
                            <td>
                                <input class="easyui-datebox" name="contract_expiry"
                                       id="contract_expiry" <?php /*echo in_array("contract_expiry", $G_userBscEdit) ? '' : 'disabled'; */ ?>
                                       style="width:255px;" value="<?php /*echo $contract_expiry; */ ?>"/>
                            </td>
                        </tr>-->
                    <!--<tr>
                            <td><? /*= lang('credit_amount'); */ ?> </td>
                            <td>
                                <input class="easyui-textbox" name="credit_amount"
                                       id="credit_amount" <?php /*echo in_array("credit_amount", $G_userBscEdit) ? '' : 'disabled'; */ ?>
                                       style="width:255px;" value="<?php /*echo $credit_amount; */ ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><? /*= lang('finance_status'); */ ?> </td>
                            <td>
                                <input class="easyui-textbox" name="finance_status"
                                       id="finance_status" <?php /*echo in_array("finance_status", $G_userBscEdit) ? '' : 'disabled'; */ ?>
                                       style="width:255px;" value="<?php /*echo $finance_status; */ ?>"/>
                            </td>
                        </tr>-->
                    <tr>
                        <td><?= lang('finance_payment'); ?> </td>
                        <td>
                            <input type="hidden" <?php echo in_array("finance_payment", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   id="finance_payment" value="<?= $finance_payment; ?>">
                            <input type="radio" <?php echo in_array("finance_payment", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   class="finance_payment" name="finance_payment" value="after work"><?= lang('开航后');?>
                            <!--<input type="radio" <?php echo in_array("finance_payment", $G_userBscEdit) ? '' : 'disabled'; ?> class="finance_payment" name="finance_payment" value="after invoice" ><?= lang('after invoice'); ?>-->
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('finance_od_basis'); ?> </td>
                        <td>
                            <input type="hidden" <?php echo in_array("finance_od_basis", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   id="finance_od_basis" value="<?= $finance_od_basis; ?>">
                            <input type="radio" <?php echo in_array("finance_od_basis", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   class="finance_od_basis" name="finance_od_basis"
                                   value="monthly"><?= lang('monthly'); ?>
                            <!--<input type="radio" <?php echo in_array("finance_od_basis", $G_userBscEdit) ? '' : 'disabled'; ?> class="finance_od_basis" name="finance_od_basis" value="daily" ><?= lang('daily'); ?>-->
                            <input type="radio" <?php echo in_array("finance_od_basis", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   class="finance_od_basis" name="finance_od_basis" value="daily"><?= lang('按天付款买单');?>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('finance_payment_month_day'); ?> </td>
                        <td>
                            <input <?php echo in_array("finance_payment_month", $G_userBscEdit) ? '' : 'disabled'; ?>
                                    class="easyui-numberbox" name="finance_payment_month" id="finance_payment_month"
                                    style="width: 95px;" data-options="
                                    min:1,
                                    value: '<?= $finance_payment_month; ?>',
                                    onChange:function(newValue,oldValue){
                                        get_finance_describe();
                                    }
                                ">
                            <?= lang('month'); ?>
                            <input <?php echo in_array("finance_payment_day_th", $G_userBscEdit) ? '' : 'disabled'; ?>
                                    class="easyui-numberbox" name="finance_payment_day_th" id="finance_payment_day_th"
                                    style="width: 95px;" data-options="
                                    min:1,
                                    max:31,
                                    value: '<?= $finance_payment_day_th; ?>',
                                    onChange:function(newValue,oldValue){
                                        get_finance_describe();
                                    }
                                ">
                            <?= lang('th'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('credit_amount'); ?></td>
                        <td>
                            <input class="easyui-numberbox"
                                   name="credit_amount" <?php echo in_array("credit_amount", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   id="credit_amount" style="width: 95px;" value="<?= $credit_amount; ?>"
                                   data-options="min:0,precision:2">
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('finance_payment_day'); ?> </td>
                        <td>
                            <input class="easyui-numberbox" <?php echo in_array("finance_payment_days", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   name="finance_payment_days" id="finance_payment_days" style="width: 95px;"
                                   data-options="
                                    min:0,
                                    value: '<?= $finance_payment_days; ?>',
                                    onChange:function(newValue,oldValue){
                                        get_finance_describe();
                                    }
                                ">
                            <?= lang('days'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('finance_free_overdue_date'); ?></td>
                        <td>
                            <input class="easyui-datebox" <?php echo in_array("finance_free_overdue_date", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   name="finance_free_overdue_date" id="finance_free_overdue_date" style="width:255px"
                                   value="<?= $finance_free_overdue_date; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('finance_describe'); ?> </td>
                        <td>
                            <input class="easyui-textbox" disabled id="finance_describe"
                                   style="width:255px;"/>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('finance_remark'); ?> </td>
                        <td>
                            <input class="easyui-textbox" name="finance_remark"
                                   id="finance_remark" <?php echo in_array("finance_remark", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   style="width:255px;" value="<?php echo $finance_remark; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('签约子公司'); ?> </td>
                        <td>
                            <select class="easyui-combobox" disabled <?php echo in_array("finance_sign_company_code", $G_userBscEdit) ? '' : 'disabled'; ?>
                                    id="finance_sign_company_code_select" style="width: 255px"
                                    data-options="
                                        multiple:true,
                                        valueField:'company_code',
                                        textField:'company_name',
                                        url:'/biz_sub_company/get_option',
                                        value: '<?= $finance_sign_company_code; ?>',
                                    ">

                            </select>
                            <input type="hidden" name="finance_sign_company_code" id="finance_sign_company_code">
                        </td>
                    </tr>

                    
                     <?php foreach ($contract as $item=>$v):?>
                     <tr>
                        <td><?= $v['party_b']?><?= lang('合同有效期');?></td>
                        <td>
                           <input class="easyui-textbox" 
                                       name="contract_start" id="contract_start" value="<?php echo isset($v['contract_start']) ? $v['contract_start'] : ''; ?>" disabled style="width:115px;">
                            <?= lang('-'); ?>
                            <input class="easyui-textbox"
                                       name="contract_expiry" id="contract_expiry" value="<?php echo isset ($v['contract_expiry']) ? $v['contract_expiry'] : ''; ?>" disabled style="width:115px;">
                        </td>
                    </tr>
                    <?php endforeach; ?>
                    
                    
                    <!--<tr>
                        <td>----------------------------------</td>
                        <td>
                            --------------------------------------------------------
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('bank_name_usd'); ?> </td>
                        <td>
                            <select class="easyui-combobox" name="bank_name_usd"
                                    id="bank_name_usd" <?php echo in_array("bank_name_usd", $G_userBscEdit) ? '' : 'disabled'; ?>
                                    data-options="
                                        valueField:'bank_name',
                                        textField:'bank_name',
                                        url:'/biz_client_account/get_option/<?= $client_code; ?>/usd',
                                        onSelect: function(rec){
                                            $('#bank_account_usd').textbox('setValue',rec.bank_account);
                                            $('#bank_address_usd').textbox('setValue',rec.bank_address);
                                            $('#bank_swift_code_usd').textbox('setValue',rec.bank_swift_code);
                                        },
                                        "
                                    style="width:255px;"></select>
                            <a href="javascript:re_select('bank_name_usd');" class="easyui-tooltip" title="请先到account list新增账号，再点击R来选择"><?= lang('R'); ?></a>
                        </td>
                    </tr>
                    <tr>

                        <td><?= lang('bank_account_usd'); ?> </td>
                        <td>
                            <input class="easyui-textbox" readonly name="bank_account_usd"
                                   id="bank_account_usd" <?php echo in_array("bank_account_usd", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   style="width:255px;" value="<?php echo $bank_account_usd; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('bank_address_usd'); ?> </td>
                        <td>
                            <input class="easyui-textbox" readonly name="bank_address_usd"
                                   id="bank_address_usd" <?php echo in_array("bank_address_usd", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   style="width:255px;" value="<?php echo $bank_address_usd; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('bank_swift_code_usd'); ?> </td>
                        <td>
                            <input class="easyui-textbox" readonly name="bank_swift_code_usd"
                                   id="bank_swift_code_usd" <?php echo in_array("bank_swift_code_usd", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   style="width:255px;" value="<?php echo $bank_swift_code_usd; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>----------------------------------</td>
                        <td>
                            --------------------------------------------------------
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('bank_name_cny'); ?> </td>
                        <td>
                            <select class="easyui-combobox" name="bank_name_cny"
                                    id="bank_name_cny" <?php echo in_array("bank_name_cny", $G_userBscEdit) ? '' : 'disabled'; ?>
                                    data-options="
                                        required:true,
                                        valueField:'bank_name',
                                        textField:'bank_name',
                                        url:'/biz_client_account/get_option/<?= $client_code; ?>/cny',
                                        onSelect: function(rec){
                                            $('#bank_account_cny').textbox('setValue',rec.bank_account);
                                            $('#bank_address_cny').textbox('setValue',rec.bank_address);
                                            $('#bank_swift_code_cny').textbox('setValue',rec.bank_swift_code);
                                        },
                                        "
                                    style="width:255px;"></select>
                            <a href="javascript:re_select('bank_name_cny');"><?= lang('R'); ?></a>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('bank_account_cny'); ?> </td>
                        <td>
                            <input class="easyui-textbox" readonly name="bank_account_cny"
                                   id="bank_account_cny" <?php echo in_array("bank_account_cny", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   style="width:255px;" value="<?php echo $bank_account_cny; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('bank_address_cny'); ?> </td>
                        <td>
                            <input class="easyui-textbox" readonly name="bank_address_cny"
                                   id="bank_address_cny" <?php echo in_array("bank_address_cny", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   style="width:255px;" value="<?php echo $bank_address_cny; ?>"/>
                        </td>
                    </tr>-->
                    <!--tr>
                            <td><?= lang('bank_swift_code_cny'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_swift_code_cny"
                                       id="bank_swift_code_cny" <?php echo in_array("bank_swift_code_cny", $G_userBscEdit) ? '' : 'disabled'; ?>
                                       style="width:255px;" value="<?php echo $bank_swift_code_cny; ?>"/>
                            </td>
                        </tr-->
                    <tr>
                        <td>----------------------------------</td>
                        <td>
                            --------------------------------------------------------
                        </td>
                    </tr>
                    <script>
                        function get_taxpayer() {
                            company_name = $('#company_name').textbox('getValue');
                            $.ajax({
                                type: 'POST',
                                url: '/api/bw_search_company?company_name=' + company_name,
                                data: '',
                                dataType: 'json',
                                success: function (res) {
                                    ddd = res.response.result[0];
                                    $('#taxpayer_name').textbox('setValue', ddd.name);
                                    $('#taxpayer_id').textbox('setValue', ddd.taxId);
                                    $('#taxpayer_address').textbox('setValue', ddd.location);
                                    $('#taxpayer_telephone').textbox('setValue', ddd.fixedPhone);

                                },
                                error: function () {
                                    $.messager.alert('Tips', 'error');
                                }
                            });
                        }
                    </script>
                    <tr>
                        <td><?= lang('taxpayer_name'); ?></td>
                        <td>
                            <input class="easyui-textbox" name="taxpayer_name"
                                   id="taxpayer_name" <?php echo in_array("taxpayer_name", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   style="width:255px;" value="<?php echo $taxpayer_name; ?>"/>
                        </td>
                    </tr>
                    <tr>

                        <td><?= lang('taxpayer_id'); ?> </td>
                        <td>
                            <input class="easyui-textbox" name="taxpayer_id"
                                   id="taxpayer_id" <?php echo in_array("taxpayer_id", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   style="width:255px;" value="<?php echo $taxpayer_id; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('taxpayer_address'); ?> </td>
                        <td>
                            <input class="easyui-textbox" name="taxpayer_address"
                                   id="taxpayer_address" <?php echo in_array("taxpayer_address", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   style="width:255px;" value="<?php echo $taxpayer_address; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('taxpayer_telephone'); ?> </td>
                        <td>
                            <input class="easyui-textbox" name="taxpayer_telephone"
                                   id="taxpayer_telephone" <?php echo in_array("taxpayer_telephone", $G_userBscEdit) ? '' : 'disabled'; ?>
                                   style="width:255px;" value="<?php echo $taxpayer_telephone; ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>----------------------------------</td>
                        <td>
                            --------------------------------------------------------
                        </td>
                    </tr>
					<tr>
						<td><?=lang('signed company')?></td>
						<td><select class="easyui-combobox"
									<?php echo in_array("finance_belong_company_code", $G_userBscEdit) ? '' : 'disabled'; ?>
									id="finance_belong_company_code"
									style="width:255px;"
									data-options="
                                        multiple:true,
                                        required:true,
                                        valueField:'company_code',
                                        textField:'company_name',
                                        url:'/biz_sub_company/get_option',
                                        value: '<?= $finance_belong_company_code; ?>',
                                        onSelect(rec){
                                            $('#dn_accounts1').combobox('reload','/bsc_dict/get_client_account/<?=$client_code?>/?code='+$(this).combobox('getValues').join(','))
                                        }
                                    ">

							</select>
							<input type="hidden" value="<?= $finance_belong_company_code; ?>" id="finance_belong_company_code_val" name="finance_belong_company_code">
						</td>
					</tr>
					<tr>
						<td><?= lang('signed_accounts'); ?></td>
						<td>
							<select class="easyui-combobox" <?php echo in_array("dn_accounts", $G_userBscEdit) ? '' : 'disabled'; ?>
									id="dn_accounts1" style="width:255px;" data-options="
                                    valueField : 'id',
                                    textField : 'title_cn',
                                    url:'/bsc_dict/get_client_account/<?=$client_code?>',
                                    value: '<?= $dn_accounts;  ?>',
                                    multiple:true,
                                ">
							</select>
							<input type="hidden" <?php echo in_array("dn_accounts", $G_userBscEdit) ? '' : 'disabled'; ?>
								   id="dn_accounts" name="dn_accounts">
						</td>
					</tr>

					<tr>
						<td>----------------------------------</td>
						<td>
							--------------------------------------------------------
						</td>
					</tr>
					<tr>
						<td><?= lang('customer dn_title'); ?> </td>
						<td>
							<input class="easyui-textbox" name="dn_title"
								   id="dn_title" <?php echo in_array("dn_title", $G_userBscEdit) ? '' : 'disabled'; ?>
								   style="width:255px;" value="<?= $dn_title; ?>" data-options="
                                onChange(newVal, oldVal){
                                    var newVal=newVal.toUpperCase();
                                    $('#dn_title').textbox('setValue', newVal);
                                }"/>

						</td>
					</tr>
                    <tr>
                        <td><?= lang('DN地址'); ?> </td>
                        <td>
                            <textarea name="dn_address" id="dn_address" onkeyup="dn_address_keyup(this)" <?php echo in_array("dn_address", $G_userBscEdit) ? '' : 'disabled'; ?> style="width:250px;"><?= $dn_address;?></textarea>
                            <script>
                                function dn_address_keyup(e) {
                                    e.value=e.value.replace(/[^\w\r\n '=[\]~!@#$%^&*()_\-+<>?:;"{},.\/]+/g,'')
                                }
                            </script>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            
                            
                        </td>
                    </tr>
                    <!--  <tr>
                            <td><? /*= lang('website_url'); */ ?> </td>
                            <td>
                                <input class="easyui-textbox" name="website_url"
                                       id="website_url" <?php /*echo in_array("website_url", $G_userBscEdit) ? '' : 'disabled'; */ ?>
                                       style="width:255px;" value="<?php /*echo $website_url; */ ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><? /*= lang('source_from'); */ ?> </td>
                            <td>
                                <input class="easyui-textbox" name="source_from"
                                       id="source_from" <?php /*echo in_array("source_from", $G_userBscEdit) ? '' : 'disabled'; */ ?>
                                       style="width:255px;" value="<?php /*echo $source_from; */ ?>"/>
                            </td>
                        </tr>-->
                </table>
                <input type="hidden" <?php echo in_array("tag", $G_userBscEdit) ? '' : 'disabled'; ?> name="tag"
                       id="tag" value="<?= $tag; ?>">
                <br>

            </div>
            
            <!--<div data-options="region:'east',title:'Tags'" style="width: 375px; height: 210px;">-->
            <!--</div>-->
    </div>
    </form>
    <div id="tags_tab" class="easyui-accordion" data-options="multiple:true" style="width:300px;position: absolute;right: 38px;top: 10px;z-index: 999;">
        <style type="text/css">
            .topic-tag{
                display:inline-block
            }
            .topic-tag .text {
                display: inline-block;
                height: 16px;
                line-height: 16px;
                padding: 2px 5px;
                background-color: #99cfff;
                font-size: 12px;
                color: #fff;
                border-radius: 4px;
                cursor: pointer;
            }
            .topic-tag {
                margin: 0 5px 2px 0;
            }
            .topic-tag .text:hover, .topic-tag .close:hover{
                background-color: #339dff;
            }
            .this_tag .topic-tag .text, .role_tag .topic-tag .text{
                border-radius: 4px 0 0 4px;
                float: left;
            }
            .this_tag .topic-tag .close, .role_tag .topic-tag .close{
                float: left;
            }
            .topic-tag-apply .text{
                background-color: #cccccc;
            }
            .topic-tag .close {
                width: 20px;
                height: 20px;
                background-color: #66b7ff;
                text-align: center;
                line-height: 20px;
                color: #fff;
                font-size: 10px;
                opacity: 1;
                filter: alpha(opacity=100);
                border-radius: 0 4px 4px 0;
                display: inline-block;
                text-shadow: 0 1px 0 #fff;
                cursor: pointer;
            }
            .add_tags_body_input{
                display: inline-block;
                vertical-align: middle;
                width: 200px !important;
                margin: 0 5px 10px 0;
                padding: 6px;
                resize: none;
                box-shadow: none;
                height: 22px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
                background-color: #fff;
                background-image: none;
                border: 1px solid #ccc;
                border-radius: 4px;
                transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                float: left;
            }
            .add_tags_body_type{
                display: inline-block;
                vertical-align: middle;
                width: 100px !important;
                margin: 0 5px 10px 0;
                padding: 6px;
                resize: none;
                box-shadow: none;
                height: 36px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
                background-image: none;
                border: 1px solid #ccc;
                border-radius: 4px;
                transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                float: left;
            }
            .add_tags_body_input_btn:hover{
                background-color: rgb(22,155,213);
            }
            .add_tags_body_input_btn{
                margin: 0 10px 10px 0;
                border: none !important;
                background-color: rgb(94,180,214);
                color: #fff;
                min-width: 76px;
                height: 34px;
                padding: 0 10px;
                line-height: 34px;
                font-size: 14px;
                display: inline-block;
                font-weight: 400;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                background-image: none;
                border-radius: 4px;
                box-sizing: border-box;
            }
            .add_tags_body h3{
                margin-top: 20px;
                color: #333;
                font-size: 100%;
                line-height: 1.7;
                margin-bottom: 10px;
            }
            .add_tags_body{
                padding:10px;
            }
        </style>
        <script type="text/javascript">
            //添加标签
            function add_tags() {
                $('#add_tags_div').window('open');
            }
            //申请标签
            function apply_tag() {
                var tag_name = $('#tag_name').val().trim();
                if(tag_name == ''){
                    $.messager.alert('<?= lang('tips');?>', 'Label can be empty');
                    return false;
                }
                var tag_class = $('#tag_class').val();
                if(tag_class == ''){
                    $.messager.alert('<?= lang('tips');?>', 'please select Label Type');
                    return false;
                }
                var post_data = {};
                post_data.tag_name = tag_name;
                post_data.tag_class = tag_class;
                post_data.id_type = 'biz_client';
                post_data.id_no = $('#id').textbox('getValue');
                submit_add_tag(post_data, function (res) {
                    $.messager.alert('<?= lang('提示');?>', res.msg);
                    if(res.code == 0){
                        $('#tag_name').val('');
                        var pass = res.data.is_pass;
                        add_tag_html(post_data.tag_name, tag_class, 'system_tag', pass);
                        add_tag_html(post_data.tag_name, tag_class, 'this_tag', pass);
                    }else{

                    }
                });
            }

            //点击添加标签
            function click_add_tag(e) {
                var tag_name = $(e).text();
                var tag_class = $(e).parents('.topic-bar').attr('tag_class');
                var post_data = {};
                post_data.tag_name = tag_name;
                post_data.tag_class = tag_class;
                post_data.id_type = 'biz_client';
                post_data.id_no = $('#id').textbox('getValue');
                submit_add_tag(post_data, function (res) {
                    if(res.code == 0){
                        var pass = res.data.is_pass;
                        add_tag_html(post_data.tag_name, tag_class, 'this_tag', pass);
                    }else{
                        $.messager.alert('<?= lang('提示');?>', res.msg);
                    }
                });
            }

            /**
             * 添加一个tag标签
             * @param tag_name 标签名
             * @param tag_class 标签类
             * @param tag_type 标签类型, 目前有this_tag 右上角的,system_tag 系统推荐标签
             * @param pass 是否通过,没通过的是灰色
             */
            function add_tag_html(tag_name, tag_class, tag_type, pass = 0) {
                var span_class = "topic-tag";
                if(pass == 0) span_class += ' topic-tag-apply';

                if(tag_type === 'this_tag'){
                    //如果不存在,生成一个
                    if($(".this_tag[tag_class='" + tag_class + "']").length == 0){
                        var topic_bar_html = "<div class=\"topic-bar clearfix this_tag\" tag_class=\"" + tag_class + "\" style=\"margin: 0;\"><h3>" + tag_class + "</h3></div>";
                        $('.this_tag_box').append(topic_bar_html);
                    }

                    var tag_html = "<span class=\"" + span_class + "\">\n" +
                        "                    <a class=\"text\">" + tag_name + "</a>\n" +
                        "<a class=\"close\" onclick=\"click_del_tag(this)\">X</a>" +
                        "                </span>";
                    $(".this_tag[tag_class='" + tag_class + "']").append(tag_html);
                }else if(tag_type === 'system_tag'){
                    //如果不存在,生成一个
                    if($(".system_tag[tag_class='" + tag_class + "']").length == 0){
                        var topic_bar_html = "<div class=\"topic-bar clearfix system_tag\" tag_class=\"" + tag_class + "\" style=\"margin: 0;\"><h3>" + tag_class + "</h3></div>";
                        $('.system_tag_box').append(topic_bar_html);
                    }

                    var tag_html = "<span class=\"" + span_class + "\">\n" +
                        "                    <a class=\"text\" onclick=\"click_add_tag(this)\">" + tag_name + "</a>\n" +
                        "                </span>";
                    $(".system_tag[tag_class='" + tag_class + "']").append(tag_html);
                }else if(tag_type === 'role_tag'){
                    //如果不存在,生成一个
                    if($(".role_tag[tag_class='" + tag_class + "']").length == 0){
                        var topic_bar_html = "<div class=\"topic-bar clearfix role_tag\" tag_class=\"" + tag_class + "\" style=\"margin: 0;\"></div>";
                        $('.role_tag_box').append(topic_bar_html);
                    }

                    var tag_html = "<span class=\"" + span_class + "\">\n" +
                        "                    <a class=\"text\" onclick=\"click_role_open('" + tag_name + "')\" role=\"" + tag_name + "\">" + tag_name + "</a>\n" +
                        "<a class=\"close\" onclick=\"click_del_role(this)\" role=\"" + tag_name + "\">X</a>" +
                        "                </span>";
                    $(".role_tag[tag_class='" + tag_class + "']").append(tag_html);
                }
            }

            //提交方法
            function submit_add_tag(post_data, fn) {
                ajaxLoading();
                $.ajax({
                    type: 'POST',
                    url: '/biz_tag/add_tag',
                    data: post_data,
                    success: function (res_json) {
                        ajaxLoadEnd();
                        try {
                            var res = $.parseJSON(res_json);
                            fn(res);
                        }catch (e) {
                            $.messager.alert('<?= lang('提示');?>', res_json);
                        }
                    },
                    error:function (e) {
                        ajaxLoadEnd();
                        $.messager.alert('<?= lang('提示');?>', e.responseText);
                    }
                });
            }

            //点击删除当前tag
            function click_del_tag(e) {
                var tag_name = $(e).prev().text();
                var tag_class = $(e).parents('.topic-bar').attr('tag_class');
                var post_data = {};
                post_data.tag_name = tag_name;
                post_data.tag_class = tag_class;
                post_data.id_type = 'biz_client';
                post_data.id_no = $('#id').textbox('getValue');
                ajaxLoading();
                $.ajax({
                    type: 'POST',
                    url: '/biz_tag/del_this_tag',
                    data: post_data,
                    success: function (res_json) {
                        ajaxLoadEnd();
                        try {
                            var res = $.parseJSON(res_json);
                            var topic_tag = $(e).parents('.topic-bar').children('.topic-tag');
                            if(topic_tag.length === 1) $(e).parents('.topic-bar').remove();
                            else $(e).parents('.topic-tag').remove();
                            //判断当前tag下是否还有其他标签, 如果一个都没了,tag_class整个删除
                        }catch (e) {
                            $.messager.alert('<?= lang('提示');?>', res_json);
                        }
                    },
                    error:function (e) {
                        ajaxLoadEnd();
                        $.messager.alert('<?= lang('提示');?>', e.responseText);
                    }
                });
            }

            //删除role角色
            function click_del_role(e) {
                //去除标签,且去除role里的值即可
                var role = $(e).attr('role');

                $('#role1').combobox('unselect', role);
                $(e).parents('.topic-tag').remove();
            }

            //打开client 角色维护权限窗口
            function click_role_open(role) {
                // var role = $(e).attr('role');
                var client_code = $('#client_code').textbox('getValue');
                $('#window_iframe').css({width: 600,height:500,}).attr('src', '/biz_client_duty/index?client_code=' + client_code + '&client_role=' + role);
                $('#window').window({
                    title: role + ' <?= lang('角色权限');?>',
                    width:'615',
                    height:'539',
                    top:200,
                    modal:true
                });
            }

            //加载当前tag
            function load_tag() {
                var id_no = $('#id').textbox('getValue');
                //分为2部分,1是当前的tag,2是公共模板的tag
                $.ajax({
                    type: 'GET',
                    url: '/biz_tag/get_tag?id_type=biz_client&id_no=' + id_no,
                    success: function (res_json) {
                        ajaxLoadEnd();
                        try {
                            var res = $.parseJSON(res_json);
                            if(res.code == 0){
                                //系统推荐的
                                $.each(res.data.system_tag, function (i, it) {
                                    $.each(it.data, function (i2, it2) {
                                        add_tag_html(it2.tag_name, it.tag_class, 'system_tag', it2.pass);
                                    });
                                });

                                //当前的
                                $.each(res.data.this_tag, function (i, it) {
                                    $.each(it.data, function (i2, it2) {
                                        add_tag_html(it2.tag_name, it.tag_class, 'this_tag', it2.pass);
                                    });
                                })
                            }else{
                                $.messager.alert('<?= lang('提示');?>', res_json);
                            }
                        }catch (e) {
                            $.messager.alert('<?= lang('提示');?>', res_json);
                        }
                    },
                    error:function (e) {
                        ajaxLoadEnd();
                        $.messager.alert('<?= lang('提示');?>', e.responseText);
                    }
                });
            }

            function load_role_tag(){
                 <?php if(!$read_config){ ?>
                 return false;
                 <?php } ?>
                
                if($('#role').val() == '') return false;
                var role = $('#role').val().split(',');
                $('.role_tag_box').empty();
                $.each(role, function (i, it) {
                    add_tag_html(it, '', 'role_tag', 1);
                });
                return true;
            }

            $(function () {
                load_tag();
                load_role_tag();
            });
        </script>
        <div title="Tags" style="padding:10px;" >
            <a href="javascript:void(0)" onclick="add_tags()">add Tags</a>
            <br>-------------------------------------
            <br><br>
            <div class="this_tag_box">
            </div>
        </div>
    </div>
    <!--添加标签-->
    <div id="add_tags_div" class="easyui-window" style="width:450px;" data-options="title:'window',modal:true,closed:true,height:'auto',top:200">
        <div class="add_tags_body">
            <h3>Add Tag</h3>
            <div class="add_tags_body_input_div" style="display: block;">
                <input type="text" class="add_tags_body_input" id="tag_name" autocomplete="off" placeholder="Add new...">
                <select class="add_tags_body_type" id="tag_class" style="background-color: #fff;">
                    <option value="">Select</option>
                    <option value="operator">operator</option>
                    <option value="Finance">Finance</option>
                    <option value="Other">Other</option>
                </select>
                <a href="javascript:void(0);" onclick="apply_tag()" class="add_tags_body_input_btn">Apply</a>
            </div>
            <div class="system_tag_box">

            </div>
        </div>
    </div>
</div>
    <div title="Contact list" style="padding:1px">
        <iframe scrolling="auto" frameborder="0" id="contact_list" style="width:100%;height:750px;"></iframe>
    </div>
    <div title="Account list" style="padding:1px">
        <iframe scrolling="auto" frameborder="0" id="account_list" style="width:100%;height:750px;"></iframe>
    </div>
    <div title="Upload files" style="padding:1px">
        <iframe scrolling="auto" frameborder="0" id="upload_files_sub" style="width:100%;height:750px;"></iframe>
    </div>
    <?php if(is_admin()){ ?>
     <div title="Log" style="padding:1px">
        <iframe scrolling="auto" frameborder="0" id="log" style="width:100%;height:750px;"></iframe>
    </div>
    <?php } ?>
</div>


</body>
<div id="window" style="display:none;">
    <iframe id="window_iframe" name="window_iframe" frameborder="0"></iframe>
</div>
<script>
    $(function () {
        // //实现表格下拉框的分页和筛选--start
        // var html = '<div id="supplier_admin_Tb">';
        // html += '<form id="supplier_admin_form" style="padding:5px">';
        // html += '<div style="padding-left: 5px;display: inline-block;"><label>姓名 : </label><input name="name" class="easyui-textbox user_search"style="width:96px"data-options="prompt:\'text\'"/></div>';
        // html += '<div style="padding-left: 5px;display: inline-block;"><label>部门 : </label><input name="group" class="easyui-textbox user_search"style="width:96px"data-options="prompt:\'text\'"/></div>';

        // html += '<div style="padding-left: 10px; display: inline-block;"><form>';
        // html += '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="supplier_admin_QueryReport">查询</a>';
        // html += '</div>';
        // html += '</div>';
        // $('body').append(html);
        // $.parser.parse('#supplier_admin_Tb');

        // $('#supplier_admin_ids1').combogrid({
        //     value: '<?=$user_id;?>',
        //     panelWidth: '600px',
        //     panelHeight: '400px',
        //     width: 255,
        //     multiple: false,
        //     idField: 'id',              //ID字段
        //     textField: 'name',    //显示的字段
        //     fitColumns: true,
        //     striped: true,
        //     editable: false,
        //     pagination: true,           //是否分页
        //     pageList: [30, 100, 500],
        //     pageSize: 500,
        //     toolbar: '#supplier_admin_Tb',
        //     rownumbers: true,           //序号
        //     collapsible: true,         //是否可折叠的
        //     method: 'post',
        //     columns: [[
        //         {field: 'name', title: '姓名', width: 120},
        //         {field: 'group_name', title: '部门', width: 120},
        //         {field: 'group', title: '部门代码', width: 120},

        //     ]],
        //     emptyMsg: '未找到相应数据!',
        //     onLoadSuccess: function (data) {

        //     },
        //     url: '/bsc_user/get_user_tree2/?more_ids=',
        // });
        // //点击搜索
        // $('#supplier_admin_QueryReport').click(function () {
        //     var where = {};
        //     var form_data = $('#supplier_admin_form').serializeArray();
        //     $.each(form_data, function (index, item) {
        //         if (where.hasOwnProperty(item.name) === true) {
        //             if (typeof where[item.name] == 'string') {
        //                 where[item.name] = where[item.name].split(',');
        //                 where[item.name].push(item.value);
        //             } else {
        //                 where[item.name].push(item.value);
        //             }
        //         } else {
        //             where[item.name] = item.value;
        //         }
        //     });
        //     // where['field'] = $('#to_search_field').combo('getValue');

        //     $('#supplier_admin_ids1').combogrid('grid').datagrid('load', where);
        // });
        // //text添加输入值改变
        // $('.user_search').textbox('textbox').keydown(function (e) {
        //     if (e.keyCode == 13) {
        //         $('#supplier_admin_QueryReport').trigger('click');
        //     }
        // });

    })
</script>
