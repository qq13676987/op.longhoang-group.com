<?php

class biz_branch_consol extends Menu_Controller
{
	protected $admin_field = array('operator', 'customer_service', 'marketing', 'oversea_cus', 'booking', 'document');//, 'finance');

	public function __construct()
	{
		parent::__construct();
		$this->load->model('biz_consol_model');
		$this->load->model('biz_shipment_model');
		$this->load->model('bsc_user_model');
		$this->load->model('bsc_user_role_model');
		$this->load->model('m_model');
		$this->load->helper('cz_consol');

		$this->field_all = $this->biz_consol_model->field_all;

		foreach ($this->field_all as $row) {
			$v = $row[0];
			if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
			$this->lock_filed[$row[4]][] = $v;
			array_push($this->field_edit, $v);
		}
		foreach ($this->admin_field as $val) {
			$this->lock_filed[1][] = $val;
		}
	}

	public $lock_filed = array();   //锁字段，锁哪个格子

	public $field_edit = array();
	public function other_system(){
		$system_type = strtolower(get_system_type());
		$dict = $this->db->where("catalog = 'branch_office' and name != '$system_type'")->get('bsc_dict')->result_array();
		$client_code = $this->db->where("catalog = 'branch_office' and name = '$system_type'")->get('bsc_dict')->row_array();
		$this->load->view('head');
		$this->load->view('biz/consol/other_system/tabs',['tabs'=>$dict,'client_code'=>$client_code['value']]);
	}
	public function index($client_code='',$system='')
	{
		$data = array();
		$data['client_code'] = $client_code;
		$data['system'] = $system;
		$this->load->view('head');
		$this->load->view('biz/consol/other_system/index', $data);
	}
	public function get_consol_pod_view_data($client_code='',$system='')
	{
		$this->db = $this->load->database(strtolower($system),true);
		$result = array();
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : "booking_ETD";
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

		$where = $this->get_data_where_v2();
		//限制只有在 海外客服 是当前用户的情况下，才看到POD的单子，比较纯粹
//		$this->load->model('bsc_user_role_model');
//		$where[] = $this->bsc_user_role_model->get_field_where('biz_consol',array('oversea_cus'));
		//这里是新版的查询代码
		$title_data = get_user_title_sql_data('biz_consol', 'biz_consol_index');//获取相关的配置信息
		if(!empty($fields)){
			foreach ($fields['f'] as $key => $field){
				$f = trim($fields['f'][$key]);
				$s = $fields['s'][$key];
				$v = trim($fields['v'][$key]);
				if($f == '') continue;
				//TODO 如果是datetime字段,=默认>=且<=
				$date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
				//datebox的用等于时代表搜索当天的
				if(in_array($f, $date_field) && $s == '='){
					if(!isset($title_data['sql_field'][$f])) continue;
					$f = $title_data['sql_field'][$f];
					if($v != ''){
						$where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
					}
					continue;
				}
				//datebox的用等于时代表搜索小于等于当天最晚的时间
				if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
					if(!isset($title_data['sql_field'][$f])) continue;
					$f = $title_data['sql_field'][$f];
					if($v != ''){
						$where[] = "{$f} $s '$v 23:59:59'";
					}
					continue;
				}

				//把f转化为对应的sql字段
				//不是查询字段直接排除
				$title_f = $f;
				if(!isset($title_data['sql_field'][$title_f])) continue;
				$f = $title_data['sql_field'][$title_f];


				if($v !== '') {
					//这里缺了2个
					if($v == '-') continue;
					//如果进行了查询拼接,这里将join条件填充好
					$this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
					if(!empty($this_join)) {
						$this_join0 = explode(' ', $this_join[0]);
						$title_data['sql_join'][end($this_join0)] = $this_join;
					}

					//2022-12-27 加入新逻辑，根据逗号切割传过来的值循环处理
					//这里的话，如果用的 = ，可以改为 in直接连接
					$v_arr = explode(',', $v);
					$this_where = array();
					foreach ($v_arr as $v_val){
						$this_where[] = search_like($f, $s, $v_val);
					}

					$where[] = '(' . join(' or ', $this_where) . ')';
					// $where[] = search_like($f, $s, $v);
				}
			}
		}
//		$where[] = "biz_consol.trans_mode = 'FCL'";
		$where[] = "biz_consol.agent_code = '$client_code'";
		$where_str = join(' and ', $where);

		$offset = ($page - 1) * $rows;

		//2023-03-06 将百分比进行排序
		$sort_config = array(
			'normal_lock_c3' => "(count(case when (biz_consol.lock_lv=3 and biz_consol.status='normal') then 1 else null end) / count(case when (biz_consol.status='normal') then 1 else null end))"
		,'cancel_rate' => 'count(case when (biz_consol.status=\'cancel\') then 1 else null end) / count(1)'
		);
		if(isset($sort_config[$sort])){
			$sort = $sort_config[$sort];
		}else{
			$sort = $title_data['sql_field']["biz_consol.{$sort}"];
		}

		//总数这里做个缓存吧,1天重新查1次
		$this->db->limit($rows, $offset);
		//new--start
		$this->db->select('biz_consol.trans_destination,biz_consol.sailing_code,biz_consol.trans_carrier,biz_consol.booking_ETD,biz_consol.trans_ATD,biz_consol.des_ETA' .
			',biz_consol.vessel,biz_consol.voyage,biz_consol.creditor, GROUP_CONCAT(biz_consol.id) as ids' .
			'');
		$this->db->group_by("biz_consol.trans_destination,biz_consol.trans_carrier,biz_consol.vessel,biz_consol.voyage,biz_consol.creditor,biz_consol.sailing_code");

		//这里拼接join数据
		foreach ($title_data['sql_join'] as $j){
			$this->db->join($j[0], $j[1], $j[2]);
		}//将order字段替换为 对应的sql字段
		$this->db->is_reset_select(false);//不重置查询
		$rs = $this->biz_consol_model->get_v3($where_str, $sort, $order);

		$this->db->clear_limit();//清理下limit
		$result["total"] = $this->db->count_all_results('');
		//new--end
		// $rs = $this->biz_consol_model->get($where, $sort, $order);

		//获取客户名称等数据--start
		$client_codes = array_merge(array_column($rs, 'trans_carrier'), array_column($rs, 'creditor'));
		Model('biz_client_model');
		$this->db->select('client_code,client_name,company_name');
		$clients = array_column($this->biz_client_model->no_role_get("client_code in ('" . join('\',\'', $client_codes) . "')"), null, 'client_code');
		//获取客户名称等数据--end

		$rows = array();
		//获取每个操作的权限，根据当前用户，进行合并可查看字段
		$this->load->model('biz_port_model');
		$this->load->model('m_model');
		$id = $offset + 1;
		foreach ($rs as $row) {
			$row['id'] = $id++;
			$row['trans_carrier_name'] = isset($clients[$row['trans_carrier']]) ? $clients[$row['trans_carrier']]['client_name'] : $row['trans_carrier'];
			$row['creditor_name'] = isset($clients[$row['creditor']]) ? $clients[$row['creditor']]['company_name'] : $row['creditor'];
			$rows[] = $row;
		}
		$result["rows"] = $rows;
		echo json_encode($result);
	}
	public function get_data_where_v2()
	{
		//-------这个查询条件想修改成通用的 -------------------------------
		$shipment_no = isset($_REQUEST['shipment_no']) ? trim($_REQUEST['shipment_no']) : '';
		$goods_type = isset($_REQUEST['goods_type']) ? trim($_REQUEST['goods_type']) : '';
		$is_hyf = isset($_REQUEST['is_hyf']) ? (int)$_REQUEST['is_hyf'] : '';
		$lock_lv = isset($_REQUEST['lock_lv']) ? $_REQUEST['lock_lv'] : array();
		$container_no = isset($_REQUEST['container_no']) ? trim($_REQUEST['container_no']) : '';
		$is_apply_shipment = isset($_REQUEST['is_apply_shipment']) ? (int)$_REQUEST['is_apply_shipment'] : 0;
		$operator_si = isset($_REQUEST['operator_si']) ? $_REQUEST['operator_si'] : '';
		$document_si = isset($_REQUEST['document_si']) ? $_REQUEST['document_si'] : '';

		$where = array();
		Model('m_model');
		if ($goods_type != '') $where[] = "biz_consol.id in (select consol_id from biz_shipment where biz_shipment.goods_type = '{$goods_type}')";
		if ($is_hyf !== 0) {
			if ($is_hyf === 1) {
				$where[] = "biz_consol.sign like '%hyf_qr%'";
			} else if ($is_hyf === 2) {
				$where[] = "biz_consol.sign not like '%hyf_qr%'";
			}
		}
		if ($operator_si !== '') {
			if ($operator_si === '0') $where[] = "biz_consol.operator_si = '0'";
			if ($operator_si === '1') $where[] = "biz_consol.operator_si != '0'";
		}
		if ($document_si !== '') {
			if ($document_si === '0') $where[] = "biz_consol.document_si = '0'";
			if ($document_si === '1') $where[] = "biz_consol.document_si != '0'";
		}
		if ($shipment_no != "") {
			Model('biz_shipment_model');
			$shipment = $this->biz_shipment_model->get_one_all("job_no like '%$shipment_no%'");
			if (empty($shipment)) $shipment['consol_id'] = 0;
			$where[] = "id = {$shipment['consol_id']}";
		}
		//2021-06-03 加入查询是否申请consol
		if ($is_apply_shipment == 1) {
			$where[] = "(select consol_id_apply from biz_shipment where biz_shipment.consol_id_apply = biz_consol.id limit 1) != 0";
		}

		if ($lock_lv !== array()) {
			$lock_lv_where = array();
			if (in_array(0, $lock_lv)) $lock_lv_where[] = 'biz_consol.lock_lv = 0';//C0
			if (in_array(1, $lock_lv)) $lock_lv_where[] = 'biz_consol.lock_lv = 1';//
			if (in_array(2, $lock_lv)) $lock_lv_where[] = "biz_consol.lock_lv = 2";//
			if (in_array(3, $lock_lv)) $lock_lv_where[] = "biz_consol.lock_lv = 3";//C3
			if (!empty($lock_lv_where)) {
				$where[] = '(' . join(' OR ', $lock_lv_where) . ' )';
			}
		};//查询锁

		//查询箱号
		if ($container_no !== '') {
			Model('biz_container_model');
			$this->db->select('consol_id');
			$containers = $this->biz_container_model->get("container_no like '%$container_no%'");
			if (!empty($containers)) {
				$container_consol_ids = array_column($containers, 'consol_id');
				$where[] = "biz_consol.id in ('" . join("','", $container_consol_ids) . "')";
			} else {
				$where[] = 'biz_consol.id = 0';
			}
		}

		return $where;
	}

	public function get_consol_box_statistics_by_id($system='')
	{
		$this->db = $this->load->database(strtolower($system),true);
		$ids = postValue('ids', '');
		$ids_arr = filter_unique_array(explode(',', $ids));
		$result = array('code' => 0, 'msg' => 'success', 'data' => array('LCL' => 0, 'AIR' => 0, 'TEU' => 0, 'box_info' => array(),'status_num'=>['normal'=>0,'cancel'=>0,'pending'=>0,'error'=>0,'combine_into'=>0]));

		if (empty($ids_arr)) {//如果没ID,那么传空值回去
			return jsonEcho($result);
		}

		//首先根据IDS进行统计
		$this->db->select('status,trans_mode,box_info');
		$rows = $this->biz_consol_model->no_role_get("id in (" . join(',', $ids_arr) . ")");
		$box_info_array = array();
		$normal = 0;
		$cancel = 0;
		$pending = 0;
		$error = 0;
		$combine_into = 0;
		foreach ($rows as $row) {
			if ($row['trans_mode'] == 'LCL') {
				$result['data']['LCL']++;
			} else if ($row['trans_mode'] == 'AIR') {
				$result['data']['AIR']++;
			}

			if($row['box_info'] != "[]"){
				if($row['status'] == "normal"){
					$normal += $this->get_status_count_num($row['box_info']);
				}else if($row['status'] == "cancel"){
					$cancel += $this->get_status_count_num($row['box_info']);
				}else if($row['status'] == "pending"){
					$pending += $this->get_status_count_num($row['box_info']);
				}else if($row['status'] == "error"){
					$error += $this->get_status_count_num($row['box_info']);
				}else{
					$combine_into += $this->get_status_count_num($row['box_info']);
				}
			}

			$box_info = json_decode($row['box_info'], true);

			if ($box_info) {
				foreach ($box_info as $box) {
					!isset($box_info_array[$box['size']]) && $box_info_array[$box['size']] = 0;
					$box_info_array[$box['size']] += $box['num'];
				}
			}
		}

		foreach ($box_info_array as $key => $box) {
			$result['data']['box_info'][] = $key . '*' . $box;
			if ($key[0] == '2') {
				$result['data']['TEU'] += 1 * $box;
			} else if ($key[0] == '4') {
				$result['data']['TEU'] += 2 * $box;
			}
		}
		$result[] = $box_info_array;
		$result['data']['box_info'] = join(', ', $result['data']['box_info']);
		$result['data']['status_num'] = ['normal'=>$normal,'cancel'=>$cancel,'pending'=>$pending,'error'=>$error,'combine_into'=>$combine_into];
		return jsonEcho($result);
	}

	public function get_by_pod_view_data($client_code='',$system='')
	{
		$this->db = $this->load->database(strtolower($system),true);
		$result = array();
		// $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		// $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : "trans_destination_name";
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		// $biz_type= isset($_GET['biz_type']) ? $_GET['biz_type'] : '';
		// ?vessel=' + row.vessel + '&voyage=' + row.voyage + '&trans_carrier=' + row.trans_carrier + '&creditor=' + row.creditor
		$vessel = getValue('vessel', '');
		$voyage = getValue('voyage', '');
		$trans_carrier = getValue('trans_carrier', '');
		$creditor = getValue('creditor', '');
		$trans_destination = getValue('trans_destination', '');
		$sailing_code = getValue('sailing_code', '');
		$fields = postValue('field', array());
		//获取账单信息,有专项权限的可查看专项数据
		$is_special = 0;
		if (special_test('special_bill')) {
			$is_special = '';
		}

		$where = $this->get_data_where_v2();
		$where[] = "biz_consol.agent_code = '$client_code'";
//		$this->load->model('bsc_user_role_model');
//		$where[] = $this->bsc_user_role_model->get_field_where('biz_consol',array('oversea_cus'));

		$title_data = get_user_title_sql_data('biz_consol', 'biz_consol_index');//获取相关的配置信息
		if(!empty($fields)){
			foreach ($fields['f'] as $key => $field){
				$f = trim($fields['f'][$key]);
				$s = $fields['s'][$key];
				$v = trim($fields['v'][$key]);
				if($f == '') continue;
				//TODO 如果是datetime字段,=默认>=且<=
				$date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
				//datebox的用等于时代表搜索当天的
				if(in_array($f, $date_field) && $s == '='){
					if(!isset($title_data['sql_field'][$f])) continue;
					$f = $title_data['sql_field'][$f];
					if($v != ''){
						$where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
					}
					continue;
				}
				//datebox的用等于时代表搜索小于等于当天最晚的时间
				if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
					if(!isset($title_data['sql_field'][$f])) continue;
					$f = $title_data['sql_field'][$f];
					if($v != ''){
						$where[] = "{$f} $s '$v 23:59:59'";
					}
					continue;
				}

				//把f转化为对应的sql字段
				//不是查询字段直接排除
				$title_f = $f;
				if(!isset($title_data['sql_field'][$title_f])) continue;
				$f = $title_data['sql_field'][$title_f];


				if($v !== '') {
					//这里缺了2个
					if($v == '-') continue;
					//如果进行了查询拼接,这里将join条件填充好
					$this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
					if(!empty($this_join)) {
						$this_join0 = explode(' ', $this_join[0]);
						$title_data['sql_join'][end($this_join0)] = $this_join;
					}

					//2022-12-27 加入新逻辑，根据逗号切割传过来的值循环处理
					//这里的话，如果用的 = ，可以改为 in直接连接
					$v_arr = explode(',', $v);
					$this_where = array();
					foreach ($v_arr as $v_val){
						$this_where[] = search_like($f, $s, $v_val);
					}

					$where[] = '(' . join(' or ', $this_where) . ')';
					// $where[] = search_like($f, $s, $v);
				}
			}
		}

		if ($vessel != 'null') $where[] = "biz_consol.vessel = '$vessel'";
		else $where[] = "biz_consol.vessel = ''";

		if ($voyage != 'null') $where[] = "biz_consol.voyage = '$voyage'";
		else $where[] = "biz_consol.voyage = ''";

		if ($trans_carrier != 'null') $where[] = "biz_consol.trans_carrier = '$trans_carrier'";
		else $where[] = "biz_consol.trans_carrier = ''";

		if ($creditor != 'null') $where[] = "biz_consol.creditor = '$creditor'";
		else $where[] = "biz_consol.creditor is null";

		if ($trans_destination != 'null') $where[] = "biz_consol.trans_destination = '$trans_destination'";
		else $where[] = "biz_consol.trans_destination is null";

		if ($sailing_code != 'null') $where[] = "biz_consol.sailing_code = '$sailing_code'";
		else $where[] = "biz_consol.sailing_code is null";

		$where_str = join(' and ', $where);

		//获取需要的字段--start
		$must_field = array('id','job_no','status','trans_carrier','trans_origin','vessel',
			'creditor','sailing_code','voyage','trans_mode','lock_lv',);
		$selects = array_map(function ($r){
			return "biz_consol.{$r} as $r";
		}, $must_field);
		foreach ($title_data['select_field'] as $key => $val){
			if($key == 'biz_consol.matoufangxing') {
				isset($title_data['base_select_field']['biz_consol.carrier_ref']) && $selects[] = $title_data['base_select_field']['biz_consol.carrier_ref'];
			}
			if(!in_array($val, $selects)) $selects[] = $val;
		}
		//获取需要的字段--end
		$this->db->select(join(',', $selects), false);
		//这里拼接join数据
		foreach ($title_data['sql_join'] as $j){
			$this->db->join($j[0], $j[1], $j[2]);
		}//将order字段替换为 对应的sql字段
		$sort = $title_data['sql_field']["biz_consol.{$sort}"];
		$rs = $this->biz_consol_model->get_v3($where_str, $sort, $order);

		$result["total"] = sizeof($rs);
		//new--end
		$rows = array();
		Model('biz_bill_model');
		$userRole = $this->bsc_user_role_model->get_data_role();
		foreach ($rs as $row) {
//			if (isset($row['matoufangxing']) && $row['matoufangxing'] == "0") {
//				//判断是否甩箱标记
//				$sql = "select cgs_op from api_yunlsp_booking where bill_no = '{$row['carrier_ref']}'";
//				$cgs_op = $this->m_model->query_one($sql);
//				if (!empty($cgs_op['cgs_op'])) $row['matoufangxing'] = $cgs_op['cgs_op'];
//			}

			$shipment_row = $this->m_model->query_array("select id,haiguanfangxing,lock_lv,goods_type from biz_shipment where consol_id = {$row['id']}");
			$row['profit_percent'] = "-";
			//计算USD的利润占比，显示给沈泱看。必须是S2锁的情况下
			if (!empty($shipment_row)) {
				//可以在市场模块看利润占比的人员的id
//				$allow_user = array(20103, 20057, 20215);
//				if (in_array(get_session("id"), $allow_user)) {
				if ($shipment_row[0]["lock_lv"] >= 2) {
					$shipment_ids = join(",", array_column($shipment_row, "id"));
					$sql = "SELECT `type`,sum(amount) as a FROM `biz_bill` where id_type ='shipment' and currency = 'USD' and id_no in ($shipment_ids) GROUP BY `type`";
					// $row["sql"] = $sql;
					$lirun_rs = $this->m_model->query_array($sql);
					$lirun_rs = array_column($lirun_rs, "a", "type");
					if (empty($lirun_rs["sell"]) || empty($lirun_rs["cost"])) {
						$row['profit_percent'] = "0";
					} else {
						$row['profit_percent'] = ($lirun_rs["sell"] - $lirun_rs["cost"]) / $lirun_rs["cost"];
						$row['profit_percent'] = round($row['profit_percent'] * 100, 0) . "%";
					}
				}
//				}
			}
			//查询账单信息

			if(isset($row['usd_bill_info']) || isset($row['cny_bill_info'])){
				if(isset($row['usd_bill_info']) && isset($row['cny_bill_info'])){
					//如果2个字段都存在,直接正常查  这里不做处理
					$bill_where = "id_type = 'consol' and id_no = '{$row['id']}'";
				}else{
					//如果只有其中一个,或者2个都没有
					if (isset($row['usd_bill_info'])) {
						$bill_where = "id_type = 'consol' and id_no = '{$row['id']}' and currency = 'USD'";
					} else if (isset($row['cny_bill_info'])) {
						$bill_where = "id_type = 'consol' and id_no = '{$row['id']}' and currency = 'CNY'";
					} else {
						$bill_where = false;
					}
				}

				if ($bill_where !== false) {
					$bills = $this->biz_bill_model->get($bill_where, 'id', 'desc', $is_special);
					foreach ($bills as $bill) {
						$is_special_str = '';
						if ($bill['is_special'] == 1) {//专项加字
							$is_special_str = '[<span class="special_bill">专</span>]';
						}
						if ($bill['currency'] === 'USD') {
							$row['usd_bill_info'][] = $bill['charge'] . $is_special_str . ':' . $bill['amount'];
						} else if ($bill['currency'] === 'CNY') {
							$row['cny_bill_info'][] = $bill['charge'] . $is_special_str . ':' . $bill['amount'];
						}
					}
				}
			}
			$row = data_role($row, $must_field, explode(',', $userRole['read_text']), 'biz_consol', 'biz_consol_index');
			//海关放行后增加箱信息
			if(!isset($row['haiguanfangxing'])) $row['haiguanfangxing'] = 0;
			if($row['haiguanfangxing'] != 0){
				$box_info = json_decode($row['box_info'],true);
				if(empty($box_info)){
					$biz_container = $this->db->where('consol_id',$row['id'])->get('biz_container')->result_array();
					if(!empty($biz_container)){
						$row['container_info'] = '港区不一致';
					}
				}else{
					foreach ($box_info as $k=>$v){
						$biz_container_count = $this->db->where(['consol_id'=>$row['id'],'container_size'=>$v['size']])->count_all_results('biz_container');
						if($biz_container_count != $v['num']){
							$row['container_info'] = '港区不一致';
						}
					}
				}
			}
			array_push($rows, $row);
		}

		$result["rows"] = $rows;
		echo json_encode($result);
	}

	public function get_status_count_num($arr_str = "[]"){
		$arr = json_decode($arr_str,true);
		$num = 0;
		if(!empty($arr)){
			foreach ($arr as $val){
				if(intval($val['size']) >= 40){
					$num += 2 * $val['num'];
				}else{
					$num += $val['num'];
				}
			}
		}
		return $num;
	}
}
