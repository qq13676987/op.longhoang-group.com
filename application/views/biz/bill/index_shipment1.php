<style>
    .cost{
        color: red;
    }
    .cost a:link{
        color: red;
    }
    .sell{
        color: blue;
    }
    .sell a:link{
        color: blue;
    }
</style>
<script>
    $(function(){
        $('#tt').datagrid({
            url: '/biz_bill/get_data_tab/<?php echo $id_type;?>/<?php echo $id_no;?>?site2=<?=$site?><?php if($is_special == 1) echo '&is_special=1'?>',
            width: 'auto',
            height: $(window).height() - 150,
            onLoadSuccess:function(res){
                $('.currency_total').empty();
                $.each(res.profit, function(i, it){
                    var style = '';
                    if(i == 'total'){
                        style += 'font-weight:800;';
                    }
                    if(!it.is_text){
                        var str = '<div class="currency_total_column" style="' + style + '">\n' +
                            '                        <div class="currency_total_column_row sell">\n' +
                            '                            <div class="currency_total_column_row_left">\n' +
                            '                                ' + it.sell_title + ': \n' +
                            '                            </div>\n' +
                            '                            <div class="currency_total_column_row_right">\n' +
                            '                                ' + it.sell_amount + '\n' +
                            '                            </div>\n' +
                            '                        </div>\n' +
                            '                        <div class="currency_total_column_row cost">\n' +
                            '                            <div class="currency_total_column_row_left">\n' +
                            '                                ' + it.cost_title + ': \n' +
                            '                            </div>\n' +
                            '                            <div class="currency_total_column_row_right">\n' +
                            '                                ' + it.cost_amount + '\n' +
                            '                            </div>\n' +
                            '                        </div>\n' +
                            '                        <div class="currency_total_column_row sell">\n' +
                            '                            <div class="currency_total_column_row_left">\n' +
                            '                                ' + it.profit_title + ': \n' +
                            '                            </div>\n' +
                            '                            <div class="currency_total_column_row_right">\n' +
                            '                                ' + it.profit_amount + '\n' +
                            '                            </div>\n' +
                            '                        </div>\n';
                        if(it.ext !== undefined){
                            $.each(it.ext, function(i2, it2){
                                str += '                        <div class="currency_total_column_row cost">\n' +
                                    '                            <div class="currency_total_column_row_left">\n' +
                                    '                                ' + it2.title + ': \n' +
                                    '                            </div>\n' +
                                    '                            <div class="currency_total_column_row_right">\n' +
                                    '                                ' + it2.amount + '\n' +
                                    '                            </div>\n' +
                                    '                        </div>\n';
                            });
                        }

                        str += '                    </div>';
                    }else{
                        var str = '<div class="currency_total_column" style="' + style + '">\n' +
                            '                        <div class="sell">\n' +
                            '                                ' + it.text + ' \n' +
                            '                        </div>\n' +
                            '                    </div>';
                    }
                    $('.currency_total').append(str);
                });
            },
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    })

    function cost_all_style(value, row, index) {
        var style = '';
        if(row.cost_ban_handle){
            style = 'background-color:#C0C0C0;';
        }
        return style;
    }
    function sell_all_style(value, row, index) {
        var style = '';
        if(row.sell_ban_handle){
            style = 'background-color:#C0C0C0;';
        }
        return style;
    }
    function cost_for(value,row,index){
        var str = '';
        var cost = row.cost;
        if(cost == null){
            cost = '';
        }
        str += '<span class="cost">' + cost + '</span>';
        var cost_exrate = row.cost_exrate;
        if(!(cost_exrate == '' || cost_exrate == undefined)){
            str += '<br/>Exrate: ' + cost_exrate;
        }
        return str;
    }
    function sell_for(value,row,index){
        var str = '';
        var sell = row.sell;
        if(sell == null){
            sell = '';
        }
        str += '<span class="sell">' + sell + '</span>';
        var sell_exrate = row.sell_exrate;
        if(!(sell_exrate == '' || sell_exrate == undefined)){
            str += '<br/><span class="not_imp">Exrate: ' + sell_exrate + '</span>';
        }
        return str;
    }
    function cost_unit_price_for(value,row,index){
        var str = '';
        var cost_unit_price = row.cost_unit_price;
        if(cost_unit_price == null){
            cost_unit_price = '';
        }
        str += '<span class="cost">' + cost_unit_price + '</span>';
        return str;
    }
    function sell_unit_price_for(value,row,index){
        var str = '';
        var sell_unit_price = row.sell_unit_price;
        if(sell_unit_price == null){
            sell_unit_price = '';
        }
        str += '<span class="sell">' + sell_unit_price + '</span>';
        return str;
    }

    function cost_number_for(value,row,index){
        var str = '';
        var cost_number = row.cost_number;
        if(cost_number == null){
            cost_number = '';
        }
        str += '<span class="cost">' + cost_number + '</span>';
        if(row.cost_split_id != "0" && row.cost_split_id != undefined){
            str += ' 拆';
        }
        return str;
    }
    function sell_number_for(value,row,index){
        var str = '';
        var sell_number = row.sell_number;
        if(sell_number == null){
            sell_number = '';
        }
        str += '<span class="sell">' + sell_number + '</span>';
        if(row.sell_split_id != "0" && row.sell_split_id != undefined){
            str += ' 拆';
        }
        return str;
    }
    function cost_amount_for(value,row,index){
        var str = '';
        var cost_amount = row.cost_amount;
        if(cost_amount == null){
            cost_amount = '';
        }
        var cost_actual_flag = row.cost_actual_flag;
        if(cost_actual_flag == '' || cost_actual_flag == undefined){
            cost_actual_flag = '';
        }else{
            cost_actual_flag = ' (' + cost_actual_flag + ')';
        }
        var cost_invoice_flag = row.cost_invoice_flag;
        if(cost_invoice_flag == '' || cost_invoice_flag == undefined){
            cost_invoice_flag = '';
        }else{
            cost_invoice_flag = ' (' + cost_invoice_flag + ')';
        }

        var cost_chongping = row.cost_chongping;
        if(cost_chongping !== '' && cost_chongping !== undefined) cost_chongping = ' (' + cost_chongping + ')';
        else cost_chongping = '';
        str += '<span class="cost">' + cost_amount + cost_invoice_flag + cost_actual_flag + cost_chongping + '</span>';
        //2023-04-23 当开票时, 显示开票号和开票时间
        if(row.cost_invoice_no !== null && row.cost_invoice_no !== undefined){
            var invoice_no_str = row.cost_invoice_no.length > 15 ? row.cost_invoice_no.slice(0, 15) + "..." : row.cost_invoice_no;
            str += "<br/>Invoice: " + row.cost_invoice_no + "<br/>Date: " + row.cost_invoice_date;
        }
        if(row.cost_account_no == 'third party lock') str += "<br><span style='color:red'>third party</span>";
        return str;
    }
    function sell_amount_for(value,row,index){
        var str = '';
        var sell_amount = row.sell_amount;
        if(sell_amount == null){
            sell_amount = '';
        }
        var sell_actual_flag = row.sell_actual_flag;
        if(sell_actual_flag == '' || sell_actual_flag == undefined){
            sell_actual_flag = '';
        }else{
            sell_actual_flag = ' (' + sell_actual_flag + ')';
        }
        var sell_invoice_flag = row.sell_invoice_flag;
        if(sell_invoice_flag == '' || sell_invoice_flag == undefined){
            sell_invoice_flag = '';
        }else{
            sell_invoice_flag = ' (' + sell_invoice_flag + ')';
        }
        var sell_chongping = row.sell_chongping;
        if(sell_chongping !== '' && sell_chongping !== undefined) sell_chongping = ' (' + sell_chongping + ')';
        else sell_chongping = '';
        str += '<span class="sell">' + sell_amount + sell_invoice_flag + sell_actual_flag + sell_chongping + '</span>';
        //2023-04-23 当开票时, 显示开票号和开票时间
        if(row.sell_invoice_no !== null && row.sell_invoice_no !== undefined){
            var invoice_no_str = row.sell_invoice_no.length > 15 ? row.sell_invoice_no.slice(0, 15) + "..." : row.sell_invoice_no;
            str += "<br/><span class=\"not_imp\">Invoice: " + row.sell_invoice_no + "</span><br/><span class=\"not_imp\">Date: " + row.sell_invoice_date + "</span>";
        }
        if(row.sell_account_no == 'third party lock') str += "<br><span style='color:red'>third party</span>";
        return str;
    }
    function cost_vat_for(value,row,index){
        var str = '';
        if(row['id'] == 'Sum:'){
            return str;
        }
        var cost_vat = row.cost_vat;
        if(cost_vat == null){
            cost_vat = '';
        }
        // str += '<span class="cost" onclick="vat_update(' + index + ', \'cost\')" style="cursor: pointer;">' + cost_vat + '</span>';
        str += '<span class="cost">' + cost_vat + '</span>';

        return str;
    }
    function sell_vat_for(value,row,index){
        var str = '';
        if(row['id'] == 'Sum:'){
            return str;
        }
        var sell_vat = row.sell_vat;
        if(sell_vat == null){
            sell_vat = '';
        }
        // str += '<span class="sell" onclick="vat_update(' + index + ', \'sell\')" style="cursor: pointer;">' + sell_vat + '</span>';
        str += '<span class="sell" >' + sell_vat + '</span>';
        return str;
    }
    function cost_confirm_date_for(value,row,index){
        var str = '';
        if(row['id'] == 'Sum:'){
            return str;
        }
        var cost_confirm_date = row.cost_confirm_date;
        if(row.cost_id != null){
            if(cost_confirm_date == '0000-00-00'){
                cost_confirm_date = '<span ><?= lang("未确认") ?></span>';
            }else{
                cost_confirm_date = '<span >' + cost_confirm_date + '<?= lang("确认") ?></span>';
            }
            str += '<span class="cost">' + cost_confirm_date + '</span>';
        }
        return str;
    }
    function sell_confirm_date_for(value,row,index){
        var str = '';
        if(row['id'] == 'Sum:'){
            return str;
        }
        var sell_confirm_date = row.sell_confirm_date;
        if(row.sell_id != null){
            if(sell_confirm_date == '0000-00-00'){
                sell_confirm_date = '<span ><?= lang("未确认") ?></span>';
            }else{
                sell_confirm_date = '<span>' + sell_confirm_date + '<?= lang("确认") ?></span>';
            }
            str += '<span class="sell">' + sell_confirm_date + '</span>';
        }
        return str;
    }
    function cost_invoice_no_for(value,row,index){
        var str = '';
        var cost_invoice_no = row.cost_invoice_no;
        if(cost_invoice_no == null){
            cost_invoice_no = '';
        }
        str += '<span class="cost">' + cost_invoice_no + '</span>';
        return str;
    }
    function sell_invoice_no_for(value,row,index){
        var str = '';
        var sell_invoice_no = row.sell_invoice_no;
        if(sell_invoice_no == null){
            sell_invoice_no = '';
        }
        str += '<span class="sell">' + sell_invoice_no + '</span>';
        return str;
    }
    function cost_close_date_for(value,row,index){
        var str = '';
        var cost_close_date = row.cost_close_date;
        if(cost_close_date == null){
            cost_close_date = '';
        }
        str += '<span class="cost">' + cost_close_date + '</span>';
        return str;
    }
    function sell_close_date_for(value,row,index){
        var str = '';
        var sell_close_date = row.sell_close_date;
        if(sell_close_date == null){
            sell_close_date = '';
        }
        str += '<span class="sell">' + sell_close_date + '</span>';
        return str;
    }
    function creditor_for(value,row,index){
        var str = '';
        var creditor_name = row.creditor_name;
        if(creditor_name == null){
            creditor_name = '';
        }
        var cost_client_inner_department_name = row.cost_client_inner_department_name;
        if(cost_client_inner_department_name !== null && cost_client_inner_department_name !== undefined){
            creditor_name += "(" + cost_client_inner_department_name + ")";
        }
        str += '<span class="cost"  style="cursor: pointer;">' + creditor_name + '</span>';
        str += '<br><span style="color: dodgerblue">' + row.apply_status + '</span>';
        //2023-04-23 创建人发票号发票日期等的显示方式更改
        if(row.cost_created_by !== null && row.cost_created_by !== undefined){
            str += "<br/>CreatedBy: " + row.cost_created_by + "<br/>Date: " + row.cost_created_time;
        }
        return str;
    }
    function debitor_for(value,row,index){
        var str = '';
        var debitor_name = row.debitor_name;
        if(debitor_name == null){
            debitor_name = '';
        }
        var sell_client_inner_department_name = row.sell_client_inner_department_name;
        if(sell_client_inner_department_name !== null && sell_client_inner_department_name !== undefined){
            debitor_name += "(" + sell_client_inner_department_name + ")";
        }
        str += '<span class="sell" >' + debitor_name + '</span>';
        //2023-04-23 创建人发票号发票日期等的显示方式更改
        if(row.sell_created_by !== null && row.sell_created_by !== undefined) {
            str += "<br/><span class=\"not_imp\">CreatedBy: " + row.sell_created_by + "</span><br/><span class=\"not_imp\">Date: " + row.sell_created_time + "</span>";
        }
        return str;
    }
</script>
<table id="tt"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false" showFooter="true" style="height: 500px">
    <thead>
    <tr>
        <!--        <th data-options="field:'id',width:44,sortable:true">--><?//= lang('id');?><!--</th>-->
        <th data-options="field:'charge',width:120"><?= lang('费用名称');?></th>
        <th data-options="field:'charge_code',width:60"><?= lang('费用代码');?></th>
        <th data-options="field:'sell',width:65,formatter:sell_for,styler:sell_all_style"><?= lang('销售币种');?></th>
        <th data-options="field:'sell_unit_price',width:90,sortable:true,formatter:sell_unit_price_for,styler:sell_all_style"><?= lang('销售单价');?></th>
        <th data-options="field:'sell_number',width:70,formatter:sell_number_for,styler:sell_all_style"><?= lang('sell_number');?></th>
        <th data-options="field:'sell_amount',width:150,sortable:true,formatter:sell_amount_for,styler:sell_all_style"><?= lang('销售总价');?></th>
        <th data-options="field:'sell_vat',width:55,formatter:sell_vat_for,styler:sell_all_style"><?= lang('销售税率');?></th>
        <th data-options="field:'sell_confirm_date',width:85,formatter:sell_confirm_date_for,styler:sell_all_style" >
            <?= lang('确认日期');?><br><span title='<?= lang("点击未确认可以确认费用。 点击已确认可以取消确认") ?>' style='color:red;'>(<?= lang("确认") ?>/<?= lang("取消") ?>)</span></th>
        <th data-options="field:'debitor',width:230,formatter:debitor_for,styler:sell_all_style"><?= lang('应收');?></th>
        <th data-options="field:'cost',width:55,formatter:cost_for,styler:cost_all_style"><?= lang('应付');?></th>
        <th data-options="field:'cost_unit_price',width:90,sortable:true,formatter:cost_unit_price_for,styler:cost_all_style"><?= lang('成本单价');?></th>
        <th data-options="field:'cost_number',width:60,formatter:cost_number_for,styler:cost_all_style"><?= lang('成本数量');?></th>
        <th data-options="field:'cost_amount',width:120,sortable:true,formatter:cost_amount_for,styler:cost_all_style"><?= lang('成本总价');?></th>
        <th data-options="field:'cost_vat',width:55,formatter:cost_vat_for,styler:cost_all_style"><?= lang('成本的税率');?></th>
        <th data-options="field:'cost_confirm_date',width:85,formatter:cost_confirm_date_for,styler:cost_all_style"><?= lang('确认日期');?><br><span style='color:red;'>(<?= lang("确认") ?>/<?= lang("取消") ?>)</span></th>
        <th data-options="field:'creditor',width:230,formatter:creditor_for,styler:cost_all_style"><?= lang('成本');?></th>
    </tr>
    </thead>
</table>
<style>
    .currency_total{
        display: flex;
        display:-webkit-flex;
        font-weight: bold;
    }
    .currency_total_column{
        display: flex;
        display:-webkit-flex;
        flex-direction: column;
    }
    .currency_total_column_row{
        display: flex;
        display:-webkit-flex;
        width: 220px;
        padding: 3px 0;
    }
    .currency_total_column_row_left{
        width: 120px;
        text-align: right;
        padding-right: 10px;
    }
    .currency_total_column_row_right{
        width: 100px;
    }
</style>
<div class="currency_total">

</div>