<script type="text/javascript" src="/inc/js/easyui/datagrid-dnd.js"></script>
<style>
    .h{
        font-size: 2em;
        margin-block-start: 0.67em;
        margin-block-end: 0.67em;
        margin-inline-start: 0px;
        margin-inline-end: 0px;
        font-weight: bold;
    }
</style>
<title>table head config</title>
<div>
    <div style="display: inline-block; margin: 10px 0;">
        <span class="h" style='margin-left:15px;'> TABLE：<?= $table_name; ?></span>
        <span class="h" style='margin-left:15px;'> VIEW ：<?= $view_name; ?></span>
        <button type="button" class="easyui-linkbutton" onclick="save()" style="padding: 2px 8px;"><?= lang('SAVE');?></button>
    </div>
</div>
<input type="hidden" id="table_name" value="<?= $table_name;?>">
<input type="hidden" id="view_name" value="<?= $view_name;?>">
<style>
    .box_center>button{
        margin-bottom: 5px;
    }
</style>
<div class="box" style="display: flex;height: 600px;">
    <!--  系统自带的配置(不包含用户已使用的)  -->
    <div class="box_left">
        <div id="tb_base_tool">
            <input id="ss" class="easyui-searchbox" name="title" style="width:300px" data-options="searcher:filter_tb_base,prompt:'<?= lang('请输入搜索表头字段名');?>'">
            <button type="button" class="easyui-linkbutton" onclick="check_all_base()" style="padding: 2px 8px;"><?= lang('全选');?></button>
        </div>
        <ul id="tb_base"></ul>
    </div>
    <!--  中间的按钮  -->
    <div class="box_center" style="display:flex;flex-direction: column;width:70px;height: 600px;margin: 230px 10px;">
        <button type="button" class="easyui-linkbutton" onclick="right()"><?= lang('>');?></button>
        <button type="button" class="easyui-linkbutton" onclick="left()"><?= lang('<');?></button>
    </div>
    <!--  用户自己的配置  -->
    <div class="box_right" style="width: 500px">
        <table id="tb_user"></table>
    </div> 
</div>

<!--其他js代码-->
<script>
    /**
     * 加载user表数据
     */
    function load_user_data(){
        var table_name = $('#table_name').val();
        var view_name = $('#view_name').val();
        ajaxLoading();
        $.ajax({
            type: 'get',
            url:'/sys_config_title/get_user_data?table_name=' + table_name + '&view_name=' + view_name,
            dataType:'json',
            success:function (res) {
                //插入数据
                $('#tb_user').datagrid('loadData', res.rows);
                ajaxLoadEnd();
            },error:function (e) {
                $.messager.alert('<?= lang('提示');?>', e.responseText);
            }
        })
    }

    /**
     * 移动勾选的到右面
     */
    function right() {
        var rows = get_base_select();
        if(rows.length > 0){
            var tb_base = $('#tb_base');
            var tb_user = $('#tb_user');
            // tb_base.datalist('loadData', tb_base.datalist('getData'));
            $('#ss').searchbox("setValue", "");
            filter_tb_base('');
            var select_rows = [];
            $.each(rows, function (i, it) {
                select_rows.push(it);
            });
            $.each(select_rows, function (i, it) {
                var this_index = tb_base.datalist('getRowIndex', it);

                tb_user.datagrid('appendRow', {
                    base_id:it.base_id,
                    id:0,
                    table_name: it.table_name,
                    title: it.title,
                    title_id:it.title_id,
                    width:it.width
                });
                //添加成功后删除指定列
                tb_base.datalist('deleteRow', this_index);
            });
            tb_user.datagrid('loadData', tb_user.datagrid('getData'));
        }else{
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择任意一行再试');?>');
        }
    }

    /**
     * 移动勾选的到左面
     */
    function left(){
        var rows = get_user_select();
        if(rows.length > 0){
            var tb_base = $('#tb_base');
            var tb_user = $('#tb_user');
            var select_rows = [];
            //重新存储一下,不然下面的deleteRow后,easyui会把rows的值删掉,导致报错
            $.each(rows, function (i, it) {
                select_rows.push(it);
            });
            $.each(select_rows, function (i, it) {
                var this_index = tb_user.datagrid('getRowIndex', it);

                tb_base.datalist('appendRow', {
                    base_id : it.base_id,
                    order : it.order,
                    table_name : it.table_name,
                    title : it.title,
                    title_id : it.title_id,
                    width : it.width
                });

                //添加成功后删除指定列
                tb_user.datagrid('deleteRow', this_index);
            });
            tb_base.datagrid('loadData', tb_base.datagrid('getData'));
        }else{
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择任意一行再试');?>');
        }
    }

    /**
     * 置顶,丢到表格最上面
     */
    function topping(table_index){
        var tb_user = $('#tb_user');
        var old_data = tb_user.datagrid('getData'); //
        var old_data_rows = old_data.rows; //
        var this_row = old_data_rows[table_index]; // 当前行数据

        // //将当前行的数据丢到最上面去
        //这里报错是因为刷新了数据, 导致源码里无法获取当行, 不影响
        tb_user.datagrid('deleteRow' , table_index);
        tb_user.datagrid('insertRow' , {
            index: 0,
            row:this_row
        });
        tb_user.datagrid('loadData', tb_user.datagrid('getData'));
        load_input();
    }

    /**
     * 拖动函数, 拖动完成时会结束拖动状态
     */
    function drop(){
        $('#tb_user').datagrid('enableDnd');
    }

    /**
     * 获取base表的选中数据
     */
    function get_base_select() {
        return  $('#tb_base').datalist('getSelections');
    }

    /**
     * 获取base表的选中数据
     */
    function get_user_select() {
        return  $('#tb_user').datagrid('getSelections');
    }

    /**
     * 保存数据
     */
    function save() {
        var old_data = $('#tb_user').datagrid('getData');
        var rows = old_data.rows;
        var msg;
        if(rows.length > 0){
            msg = '<?= lang('是否确认保存该表头配置?');?>';
        }else{
            msg = '<?= lang('检测到用户配置为空,将执行清空操作,清空配置后将使用默认表头,是否确认清空?');?>';
        }
        $.messager.confirm('<?= lang('提示');?>', msg, function (r) {
            if(r){
                ajaxLoading();
                $.ajax({
                    type: 'POST',
                    url: '/sys_config_title/save_user_config_title',
                    data:{
                        view_name:$('#view_name').val(),
                        table_name:$('#table_name').val(),
                        rows:JSON.stringify(rows),
                    },
                    dataType:'json',
                    success: function (res) {
                        ajaxLoadEnd();
                        $.messager.alert('<?= lang('Tips')?>', res.msg);
                        if (res.code == 0) {
                            //保存成功直接关闭页面
                            opener.location.reload(); window.close();
                        } else {

                        }
                    },error:function (e) {
                        ajaxLoadEnd();
                        $.messager.alert("<?= lang('提示');?>", e.responseText);
                    }
                });
            }
        })
    }

    //加载一下表格的输入框
    function load_input(){
        $('.tb_user_width').numberbox({
            min:0,
            max:500,
            onChange:function (newVal, oldVal) {
                if(newVal < 11 && newVal > 0){
                    $.messager.alert("<?= lang('提示');?>", "<?= lang('不支持当前宽度');?>");
                    $(this).numberbox('setValue', oldVal);
                }
            }
        });
        //失去焦点时,自动存到表格里
        $.each($('.tb_user_width'), function (i, it) {
            $(it).textbox('textbox').bind('blur', function(e){
                var opts = $(it).textbox('options');
                var table_index = opts.table_index;
                var val = $(it).textbox('getValue');
                //将原本的数据进行替换

                var tb_user = $('#tb_user');

                var old_data = tb_user.datagrid('getData'); //
                old_data['rows'][table_index]['width'] = val; // 当前行数据
            });
        });
    }

    var base_data = [];

    /**
     * 过滤获取base表的数据
     */
    function filter_tb_base(value, name){
        var tb_base = $('#tb_base');
        var opts = tb_base.datalist('options');
        // var new_data = base_data.rows.filter(el => el[opts.textField] === value);
        var new_data = [];
        value = value.trim().toUpperCase();
        if(value !== ''){
            $.each(base_data.rows,function (i, it) {
                if(it[opts.textField].toUpperCase().indexOf(value) >= 0){
                    new_data.push(it);
                }
            });
        }else{
            new_data = base_data.rows;
        }
        tb_base.datalist('loadData', new_data);
    }

    /**
     * 全选base字段
     */
    function check_all_base(){
        var tb_base = $('#tb_base');
        var rows = tb_base.datalist('getSelections');
        var this_data = tb_base.datalist('getData');
        //如果当前全部选中,那么反选就是全部取消
        if(rows.length === this_data.rows.length) tb_base.datalist('unselectAll');
        else tb_base.datalist('selectAll');
    }

    $(function () {
        //加载base表
        $('#tb_base').datalist({
            url:'/sys_config_title/get_base_data?table_name=' + $('#table_name').val() + '&view_name=' + $('#view_name').val(),
            checkbox:true,
            idField:'base_id',
            valueField:'base_id',
            textField:'title',//title
            title:'<?= lang('系统字段表');?>',
            width: 400,
            height: 600,
            singleSelect: false,
            toolbar:'#tb_base_tool',
            onLoadSuccess:function (data) {
                if(base_data.length === 0) base_data = data;//存储一下值
            }
        });

        $('#tb_user').datagrid({
            singleSelect:false,
            dragSelection: true,//多行拖动
            // dropAccept:'tr.datagrid-row>td:not([field="title"])',
            onStopDrag:function(row){
                load_input();
                //停止拖动时,取消可拖动
                $(this).datagrid('disableDnd');
            },
            idField:'base_id',
            title:'<?= lang('用户配置表');?>',
            width: 500,
            height: 600,
            columns:[[
                {field:'id', title:'<?= lang('id');?>', hidden:true},
                {field:'ck', title:'Item Details',checkbox:true,},
                {field:'title', title:'<?= lang('字段名');?>', width: 150},
                {field:'width', title:'<?= lang('宽度');?>', width: 160, formatter:function (value,row,index) {
                    var str = "";
                    str += "<input class='tb_user_width' data-options='table_index:" + index + ",' value='" + value + "' style='width: 140px;'>";
                    return str;
                 }},
                {field:'drop', title:'<?= lang('拖动');?>', width: 60, formatter:function (value,row,index) {
                    var str = "";
                    str += "<button type=\"button\" onmousedown=\"drop()\"><?= lang('拖动');?></button>";
                    return str;
                }},
                {field:'top', title:'<?= lang('置顶');?>', width: 60, formatter:function (value,row,index) {
                    var str = "";
                    str += "<button type=\"button\" onclick=\"topping(" + index + ")\"><?= lang('置顶');?></button>";
                    return str;
                }},
            ]],
            onLoadSuccess:function(data){
                if(data == undefined) return false;
                //加载表格的表单元素
                load_input();
            }
        });
        load_user_data();
    });
</script>