<!--<script type="text/javascript" src="/inc/third/layer/layer.js"></script>-->
<script type="text/javascript" src="/inc/js/easyui/datagrid-export.js"></script>
<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<link rel="stylesheet" href="/inc/third/layui/css/layui.css">
<style type="text/css">
    .hide{
        display: none;
    }
    .f{
        width:135px;
    }
    .s{
        width:100px;
    }
    .v{
        width:120px;
    }
    .f2{
        width:135px;
    }
    .s2{
        width:100px;
    }
    .v2{
        width:120px;
    }
    .del_tr{
        width: 23.8px;
    }
    .add_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }
    .hidden_invoice_input{
        display: none;
    }
</style>
<script type="text/javascript">
    var is_submit = false;
    var is_update = false;
    var tc_des = "";
    var showlook=1;
    function doSearch() {
        if(is_submit){
            return;
        }
        if(showlook==0) change_look();
        var booking_ETD_start = $('#booking_ETD_start').datebox('getValue');
        var booking_ETD_end = $('#booking_ETD_end').datebox('getValue');

        if(booking_ETD_start == '' || booking_ETD_end == ''){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择开始和结束日期');?>');
            return;
        }
        //如果有选单个用户，那么这个限制
        var user1 = $('#user1').combobox('getValues');
        if(user1.length == 2){
            //做简单版的，第一个有值就行了
            if(new Date(booking_ETD_end).getTime() - new Date(booking_ETD_start).getTime() > 366 * 24 * 3600 * 1000){
                $.messager.alert('<?= lang('提示');?>', '<?= lang('只查询一个人时最多查询1年');?>');
                return;
            }
        }else{
            //新增获取同一年同月份
            if(booking_ETD_start.substring(0,4)!=booking_ETD_end.substring(0,4)){
                $.messager.alert('<?= lang('提示');?>', '<?= lang('开始和结束日期请选择同年');?>');
                return;
            }
            // if(new Date(booking_ETD_end).getTime() - new Date(booking_ETD_start).getTime() > 188 * 24 * 3600 * 1000){
            //     $.messager.alert('<?= lang('提示');?>', '<?= lang('最多查询180天');?>');
            //     return;
            // }
        }
        $('#hide_date').val($('#booking_ETD_start_f').combobox('getValue'))
        ajaxLoading();
        is_submit = true;
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        $('#tt').datagrid({
            url:'/report/get_lrb_data',
            clearSelections:true,
            rowStyler:function(index,row){
                if(row['invoice_date'] != null){
                    // return 'background-color:#A8A8A8;';
                }else if(row['actual_date'] != null){
                    // return 'background-color:#A8A8A8;';
                }else if(row['lock_date'] != null){
                    return 'background-color:#A8A8A8;';
                }else if(row['is_bujiti'] ==1){
                    return 'background-color:#A8A8A8;';
                }

            },
            loadFilter: function(data){
                $('#my_money').textbox('setValue',data.my_money);
                tc_des = data.tc_description;
                return data;
            },
            queryParams: json
        }).datagrid('clearSelections');
        $("#commision").text('***')
        $('#search_booking_ETD_start_f').val($('#booking_ETD_start_f').combobox('getValue'));
        $('#search_booking_ETD_end_f').val($('#booking_ETD_end_f').combobox('getValue'));
        is_update = false;
        set_config();
        $('#chaxun').window('close');
        get_statistics();
        return false;
    }

    function reset(){
        $('.v').textbox('reset');
        return false;
    }

    function ajax_get(url,name) {
        return $.ajax({
            type:'POST',
            url:url,
            dataType:'json',
            success:function (res) {
                ajax_data[name] = res;
            },
            error:function () {
                // $.messager.alert('获取数据错误');
            }
        });
    }

    function tc_lrb_excel(leader) {
        var json = {};
        var dg = $('#tt');
        //获取用户ID
        if(is_update){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('您修改过查询条件，请点击查询后再试');?>');
            return;
        }
        var user1 = $('#user1');
        var user_role1 = $('#user_role1');
        var sales_leader_tc = $('#sales_leader_tc');
        var tc_type = $('#tc_type');
        //将数据与头参数传过去
        var table_fields = dg.datagrid('getColumnFields',true).concat(dg.datagrid('getColumnFields',false));
        json.fields = [];
        for(var i=0; i<table_fields.length; i++){
            var col = dg.datagrid('getColumnOption', table_fields[i]);
            json.fields.push({width:col.boxWidth,field:table_fields[i],title:col.title});
        }
        json.datas = dg.datagrid('getData').rows;
        json = JSON.stringify(json);
        var form_data = {};
        form_data.datas = json;
        form_data.user1 = user1.combobox('getValues').join(',');
        form_data.user_role1 = user_role1.combobox('getValue');
        form_data.sales_leader_tc = sales_leader_tc.combobox('getValue');
        form_data.tc_type = tc_type.combobox('getValue');

        //这里加入限制,booking_ETD_start_f 和 booking_ETD_end_f 必须
        var booking_ETD_start_f = $('#search_booking_ETD_start_f').val();
        var booking_ETD_end_f = $('#search_booking_ETD_end_f').val();
        //这里因为可能会存在,有人选择后,但是不重新查询的问题,所以这里现在更改为,查询后会有一个隐藏域存储当前点击查询后展示的日期字段
        if(booking_ETD_start_f != 'bill.commision_month' || booking_ETD_end_f != 'bill.commision_month'){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请使用计提月份查询后导出');?>');
            return;
        }

        form_data.booking_ETD_start = $('#booking_ETD_start').datebox('getValue');
        form_data.booking_ETD_end = $('#booking_ETD_end').datebox('getValue');
        // GET方式示例，其它方式修改相关参数即可
        if(leader==1){
            var form = $('<form></form>').attr('action','/excel/export_excel?file=tc_lrb').attr('target', '_blank').attr('method','POST');
        }
        if(leader==2){
            var form = $('<form></form>').attr('action','/excel/export_excel?file=tc_leader_lrb').attr('target', '_blank').attr('method','POST');
        }

        $.each(form_data, function (index, item) {
            if(typeof item === 'string'){
                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
            }else{
                $.each(item, function(index2, item2){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                })
            }
        });
        form.appendTo('body').submit().remove();
    }

    var ajax_data = {};

    var combobox = {
        'duty.sales':['sales','name','otherName'],
        'duty.operator':['operator','name','otherName'],
        'duty.customer_service':['customer_service','name','otherName'],
        'duty.oversea_cus':['oversea_cus','name','otherName'],
        'consol.creditor':['booking_agent','client_code', 'company_name'],
        'shipment.hbl_type':['hbl_type','value', 'value'],
    };

    var datebox = [];

    var datetimebox = [];

    $(function () {
        $('#chaxun').window('open');

        // $('#tt').edatagrid({
        //     pageList : [10,20,30,40,50,500,1000],
        // });

        $('#tt').datagrid({
            pageList : [10,20,30,40,50,500,1000],
            width: 'auto',
            height: $(window).height(),
            onSelect:function(index ,row){
                get_statistics();
            },
            onUnselect:function(index ,row){
                get_statistics();
            },
            onSelectAll:function(index ,row){
                get_statistics();
            },
            onUnselectAll:function(index ,row){
                get_statistics();
            },
            onLoadSuccess:function(data){
                is_submit = false;
                ajaxLoadEnd();
                if(data.code != 0){
                    $.messager.alert('<?= lang('提示');?>', data.msg);
                }
                var ext_data = data.ext_data;
                $(this).datagrid('changeColumnSort', {
                    sortOrder: ext_data.order,
                    sortName: ext_data.sort,
                });
            },
            onLoadError:function(d){
                is_submit = false;
                ajaxLoadEnd();
            },
            onDblClickRow:function (index, row) {
                if(row.id_type == 'shipment'){
                    window.open("/biz_shipment/edit/" + row.id_no);
                }else if(row.id_type == 'consol'){
                    window.open("/biz_consol/edit/" + row.id_no);
                }
            }
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
        // ---------------------------------
        var a1 = ajax_get('/bsc_user/get_data_role/sales', 'sales');
        var a2 = ajax_get('/bsc_user/get_data_role/operator', 'operator');
        var a3 = ajax_get('/bsc_user/get_data_role/customer_service', 'customer_service');
        var a4 = ajax_get('/bsc_user/get_data_role/oversea_cus', 'oversea_cus');
        var a5 = ajax_get('/biz_client/get_option/booking_agent', 'booking_agent');
        var a6 = ajax_get('/bsc_dict/get_option/hbl_type', 'hbl_type');

        $('#cx table').on('click' , '.add_tr', function () {
            var index = $('.f').length;
            var str = '<tr>\n' +
                '                <td>\n' +
                '                    <?= lang('查询');?>\n'+
                '                </td>\n'+
                '                <td align="left">\n'+
                '                    <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="\n'+
                '                        onChange:function(newVal, oldVal){\n'+
                '                            var index = $(\'.f\').index(this);\n'+
                '                            if(combobox.hasOwnProperty(newVal) === true){\n'+
                '                                $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n'+
                '                                $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\'>\');\n'+
                '                                $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n'+
                '                                $(\'.v:eq(\' + index + \')\').combobox({\n'+
                '                                    data:ajax_data[combobox[newVal][0]],\n'+
                '                                    valueField:combobox[newVal][1],\n'+
                '                                    textField:combobox[newVal][2],\n'+
                '                                });\n'+
                '                            }else if ($.inArray(newVal, datebox) !== -1){\n'+
                '                                $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n'+
                '                                $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datebox\\\' >\');\n'+
                '                                $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n'+
                '                            }else if ($.inArray(newVal, datetimebox) !== -1){\n'+
                '                                $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n'+
                '                                $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datetimebox\\\' >\');\n'+
                '                                $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n'+
                '                            }else{\n'+
                '                                $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n'+
                '                                $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\'>\');\n'+
                '                                $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n'+
                '                            }\n'+
                '                        }\n'+
                '                    ">\n'+
                // '                        <option value="bill.client_code"><?= lang('client_code');?></option>\n'+
                // '                        <option value="bill.client_code_name"><?= lang('client_code_name');?></option>\n'+
                '                        <option value="bill.charge"><?= lang('charge');?></option>\n'+
                '                        <option value="bill.charge_code"><?= lang('charge_code');?></option>\n'+
                '                        <option value="bill.currency"><?= lang('currency');?></option>\n'+
                '                        <option value="bill.amount"><?= lang('amount');?></option>\n'+
                '                        <option value="bill.account_no"><?= lang('account_no');?></option>\n' +
                '                        <option value="shipment.client_company"><?= lang('client_company');?></option>\n'+
                '                        <option value="shipment.hbl_type"><?= lang('hbl_type');?></option>\n'+
                '                        <option value="shipment_by_consol.job_no"><?= lang('shipment no');?></option>\n' +
                // '                        <option value="shipment.job_no"><?= lang('shipment no');?></option>\n'+
                '                        <option value="shipment.shipper_ref"><?= lang('shipper_ref');?></option>\n'+
                '                        <option value="consol.job_no"><?= lang('consol no');?></option>\n'+
                '                        <option value="consol.creditor"><?= lang('booking_agent');?></option>\n'+
                '                        <option value="consol.carrier_ref"><?= lang('carrier_ref');?></option>\n'+
                '                        <option value="consol.agent_ref"><?= lang('agent_ref');?></option>\n'+
                '                        <option value="shipment_consol.booking_ETD"><?= lang('booking_ETD');?></option>\n'+
                '                        <option value="duty.operator"><?= lang('operator');?></option>\n'+
                //'                        <option value="duty.booking"><?//= lang('booking');?>//</option>\n'+
                '                        <option value="duty.customer_service"><?= lang('customer_service');?></option>\n'+
                //'                        <option value="duty.document"><?//= lang('document');?>//</option>\n'+
                //'                        <option value="duty.finance"><?//= lang('finance');?>//</option>\n'+
                //'                        <option value="duty.sales"><?//= lang('sales');?>//</option>\n'+
                //'                        <option value="duty.marketing"><?//= lang('marketing');?>//</option>\n'+
                //'                        <option value="duty.oversea_cus"><?//= lang('oversea_cus');?>//</option>\n'+
                '                    </select>\n'+
                '                    &nbsp;\n'+
                '                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="\n'+
                '                    ">\n'+
                '                        <option value="like">like</option>\n'+
                '                        <option value="=">=</option>\n'+
                '                        <option value=">=">>=</option>\n'+
                '                        <option value="<="><=</option>\n'+
                '                        <option value="!=">!=</option>\n'+
                '                    </select>\n'+
                '                    &nbsp;\n'+
                '                    <div class="this_v">\n'+
                '                        <input name="field[v][]" class="v easyui-textbox">\n'+
                '                    </div>\n'+
                '                    <button class="del_tr" type="button">-</button>\n'+
                '                </td>\n'+
                '            </tr>';
            $('#cx table tbody').append(str);
            $.parser.parse($('#cx table tbody tr:last-child'));
            return false;
        });

        //删除按钮
        $('#cx table').on('click' , '.del_tr', function () {
            var index = $(this).parents('tr').index();

            $(this).parents('tr').remove();
            return false;
        });

        $('#cx').form({
            onChange: function (target) {
                is_update = true;
            },
        });

        //初始判断count多少,自动调用该方法--start
        $.ajax({
            type: 'GET',
            url: '/sys_config/get_config_search/bill_lrb',
            dataType: 'json',
            success:function (res) {
                // var config = $.parseJSON(res['data']);
                var config = res.data;
                if(config.length > 0){
                    var count = config[0];
                    $('.f:last').combobox('setValue', config[1][0][0]);
                    $('.s:last').combobox('setValue', config[1][0][1]);
                    for (var i = 1; i < count; i++){
                        $('.add_tr:eq(0)').trigger('click');
                        $('.f:last').combobox('setValue', config[1][i][0]);
                        $('.s:last').combobox('setValue', config[1][i][1]);
                    }
                }
            }
        });
        $.when(a1,a2,a3,a4,a5).done(function () {
            $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                var ec_value = $(ec_item).combobox('getValue');
                $(ec_item).combobox('clear').combobox('select', ec_value);
            });
        });

        //初始判断count多少,自动调用该方法--end

        $('#month_select').focus(function(){
            WdatePicker({
                startDate:'%y-%M',
                dateFmt:'yyyy-MM',
                onpicked:function(){
                    month_select()
                }
            });
        });
    });

    function set_config() {
        var f_input = $('.f');
        var count = f_input.length;
        var config = [];
        $.each(f_input, function (index, item) {
            var c_array = [];
            c_array.push($('.f:eq(' + index + ')').combobox('getValue'));
            c_array.push($('.s:eq(' + index + ')').combobox('getValue'));
            config.push(c_array);
        });
        // var config_json = JSON.stringify(config);
        $.ajax({
            type: 'POST',
            url: '/sys_config/save_config_search',
            data:{
                table: 'bill_lrb',
                count: count,
                config: config,
            },
            dataType:'json',
            success:function (res) {

            },
        });
    }

    function month_select(){
        var val = $('#month_select').val();
        var date = new Date(val);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var d = new Date(year, month, 0);
        var end_day = d.getDate();
        $('#booking_ETD_start').datebox('setValue', year + '-' + month + '-01');
        $('#booking_ETD_end').datebox('setValue', year + '-' + month + '-' + end_day);
        $('#month_select').val('');
    }

    function is_special_select(rec){
        if(rec.value == ""){
            window.location.href = "/report/lrb?is_special=";
        }else{
            <?php if($is_special === ''){ ?>
            window.location.href = "/report/lrb";
            <?php } ?>
        }
    }

    function get_statistics(){
        var select = $('#tt').datagrid('getSelections');
        var row = {};
        var data = $('#tt').datagrid('getData').rows;
        if(data.length >= 1){
            $.each(data[0], function(index, item){
                if(typeof item == 'number'){
                    row[index] = 0;
                }else{
                    row[index] = '';
                }
            });
        }
        $.each(select, function (index, item) {
            $.each(item, function(i, val){
                if(typeof val == 'number'){
                    var this_val = parseInt(val * 10000);
                    row[i] += this_val;
                }
            });
        });
        $.each(row, function(index, item){
            if(typeof item == 'number'){
                row[index] = item / 10000;
            }
        });
        row['count_num'] = select.length;
        $('#statistics_form').form('load', row);
    }

    function lock_lv_select(rec){
        //此处为辅助勾选
        //首先清空勾选
        $('input[name="lock_lv[]"]').attr('checked', false);
        $('input[name="consol_lock_lv[]"]').attr('checked', false);

        var val = rec.value;
        if(val == 'NC3'){
            $('#C0').prop('checked', true);
        }else if(val == 'YC3'){
            $('#C3').prop('checked', true);
            $('#S0').prop('checked', true);
            $('#S1').prop('checked', true);
            $('#S2').prop('checked', true);
            $('#S3').prop('checked', true);
        }else if(val == 'NS1'){
            $('#C0').prop('checked', true);
            $('#C3').prop('checked', true);
            $('#S0').prop('checked', true);
        }else if(val == 'YS1'){
            $('#C3').prop('checked', true);
            $('#S1').prop('checked', true);
            $('#S2').prop('checked', true);
            $('#S3').prop('checked', true);
        }else if(val == 'NS2'){
            $('#C0').prop('checked', true);
            $('#C3').prop('checked', true);
            $('#S0').prop('checked', true);
            $('#S1').prop('checked', true);
        }else if(val == 'YS2'){
            $('#C3').prop('checked', true);
            $('#S2').prop('checked', true);
            $('#S3').prop('checked', true);
        }else if(val == 'NS3'){
            $('#C0').prop('checked', true);
            $('#C3').prop('checked', true);
            $('#S1').prop('checked', true);
            $('#S2').prop('checked', true);
        }else if(val == 'YS3'){
            $('#C3').prop('checked', true);
            $('#S3').prop('checked', true);
        }else{
            $.messager.alert('<?= lang('提示');?>', '<?= lang('暂不支持');?>');
        }

    }
</script>
<script>
    //2022-05-24 MARS 提出 制单毛利加个颜色
    function gross_profit_styler(index,row) {
        var str = '';

        if(row.gross_profit >= 0){
            str += 'color: blue;';
        }else{
            str += 'color: red;';
        }

        return str;
    }
</script>
<table id="tt" rownumbers="false" idField="id_no" toolbar="#tb" singleSelect="false" nowrap="false">
    <thead>
    <tr>
        <th title="<?= lang('全选');?>" field="ck" checkbox="true"></th>
        <th data-options="field:'job_no',width:90,sortable:true"><?= lang('JOBNO');?></th>
        <th data-options="field:'num',width:60,sortable:true"><?= lang('序号');?></th>
        <th data-options="field:'trans_ATD',width:120,sortable:true"><?= lang('开航日');?></th>
        <th data-options="field:'carrier_ref',width:120,sortable:true"><?= lang('提单号(O)');?></th>
        <th data-options="field:'consol_no',width:80,sortable:true"><?= lang('CONSOL');?></th>
        <th data-options="field:'box_info_text',width:200"><?= lang('箱型箱量');?></th>
        <th data-options="field:'trans_origin_name',width:200,sortable:true"><?= lang('起运港');?></th>
        <th data-options="field:'trans_destination_name',width:200,sortable:true"><?= lang('目的港');?></th>
        <th data-options="field:'vessel',width:100,sortable:true"><?= lang('船名');?></th>
        <th data-options="field:'voyage',width:80,sortable:true"><?= lang('航次');?></th>
        <th data-options="field:'client_code_name',width:150,sortable:true"><?= lang('委托方');?></th>
        <th data-options="field:'gross_profit_sum',width:120,sortable:true,styler:gross_profit_styler"><?= lang("总毛利({currency})", array('currency' => $local_currency));?><a href="javascript:void(0);" class="more_column l-btn-icon add" onClick="hidden_column(event, ['sell_amount_sum', 'cost_amount_sum'])" style="position: unset;padding-left: 5px;margin-top: 0;"></a></th>
        <th data-options="field:'sell_amount_sum',width:80,sortable:true,hidden:true"><?= lang("总应收({currency})", array('currency' => $local_currency));?></th>
        <th data-options="field:'cost_amount_sum',width:80,sortable:true,hidden:true"><?= lang("总应付({currency})", array('currency' => $local_currency));?></th>
         <?php foreach($currency_table_fields as $currency_table_field){ ?>
            <th data-options="field:'<?= $currency_table_field;?>_gross_profit',width:80,sortable:true"><?= lang("{currency}毛利", array('currency' => $currency_table_field));?><a href="javascript:void(0);" class="more_column l-btn-icon add" onClick="hidden_column(event, ['sell_<?= $currency_table_field;?>_amount_sum', 'cost_<?= $currency_table_field;?>_amount_sum'])" style="position: unset;padding-left: 5px;margin-top: 0;"></a></th>
            <th data-options="field:'sell_<?= $currency_table_field;?>_amount_sum',width:80,sortable:true,hidden:true"><?= lang("{currency}应收", array('currency' => $currency_table_field));?></th>
            <th data-options="field:'cost_<?= $currency_table_field;?>_amount_sum',width:80,sortable:true,hidden:true"><?= lang("{currency}应付", array('currency' => $currency_table_field));?></th>
        <?php } ?>
        <?php if($is_special === ''){ ?>
            <!--专项应收 ，专项应付 ，专项利润。-->
            <th data-options="field:'sell_special_amount',width:120,sortable:true"><?= lang('专项应收');?></th>
            <th data-options="field:'cost_special_amount',width:120,sortable:true"><?= lang('专项应付');?></th>
            <th data-options="field:'special_gross_profit',width:120,sortable:true"><?= lang('专项利润');?></th>
        <?php }?>
        <th data-options="field:'sell_payment_amount',width:120,sortable:true"><?= lang('已销账应收');?></th>
        <th data-options="field:'return_rate',width:120,sortable:true"><?= lang('回款率');?></th>
        <th data-options="field:'invoice_type_name',width:80"><?= lang('开票类型');?></th>
        <th data-options="field:'sales',width:80,sortable:true"><?= lang('销售');?></th>
        <th data-options="field:'sales_group_name',width:120,sortable:true"><?= lang('销售组');?></th>
        <th data-options="field:'customer_service',width:80,sortable:true"><?= lang('客服');?></th>
        <th data-options="field:'operator',width:80,sortable:true"><?= lang('操作');?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:window.location.href= '/report/index';"><?= lang('返回上一页');?>  </a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('查询');?></a>
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#excel_menu',plain:true" iconCls="icon-reload"><?= lang('导出表格');?></a>
            </td>
        </tr>
    </table>
    <div id="excel_menu" style="width:150px;">
        <div data-options="iconCls:'icon-ok'" onclick="$('#tt').datagrid('toExcel','<?= lang('利润表'); ?>.xls');"><?= lang('导出表格');?></div>
    </div>
    <form id="statistics_form" method="post">
        <table>
            <tr>
                <td></td>
                <td><?= lang('票数');?><input class="easyui-numberbox" precision="0" name="count_num" disabled value="0"></td>
                <td><?= lang('毛利');?><input class="easyui-numberbox" precision="2" name="gross_profit_sum" disabled value="0"></td>
                <script>
                    function change_look(){
                        if(showlook==1){
                            $('#tc_description').textbox('setValue',tc_des);
                            showlook =0;
                            $("#kaiguan").html("隐藏");
                        }else{
                            $('#tc_description').textbox('setValue',"***");
                            showlook =1;
                            $("#kaiguan").html("查看");
                        }
                    }

                </script>
            </tr>

        </table>
    </form>
</div>
<div id="chaxun" class="easyui-window" title="<?= lang('query');?>" closed="true" style="width:850px;height:480px;padding:5px;">
    <br>
    <form id="cx" onSubmit="doSearch();">
        <input type="hidden" name="ids">
        <table cellspacing="5" cellpadding="5" style="min-height: 300px">
            <tr>
                <td>
                    <?= lang('查询');?>
                </td>
                <td align="left">
                    <select name="field[f][]" editable="false" class="f2 easyui-combobox" id="booking_ETD_start_f" required="true" data-options="
                        onSelect:function(rec){
                            $('#booking_ETD_end_f').combobox('setValue', rec.value);
                        }
                    ">
                        <option value="consol.trans_ATD"><?= lang('实际离泊');?></option>
                        <option value="shipment.booking_ETD"><?= lang('订舱船期');?></option>
                        <option value="consol.report_date"><?= lang('report_date');?></option>
                    </select>
                    &nbsp;

                    <select name="field[s][]" readonly class="s2 easyui-combobox"  required="true" data-options="
                    ">
                        <option value=">=">>=</option>
                    </select>
                    &nbsp;
                    <div class="this_v">
                        <input name="field[v][]" required id="booking_ETD_start" class="v2 easyui-datebox">
                    </div>
                    <input type="text" class="Wdate" id="month_select" style="width:20px;" autocomplete="off"/>
                </td>
            </tr>
            <tr>
                <td>
                    <?= lang('查询');?>
                </td>
                <td align="left">
                    <select name="field[f][]" editable="false" class="f2 easyui-combobox" id="booking_ETD_end_f" required="true" data-options="
                        onSelect:function(rec){
                            $('#booking_ETD_start_f').combobox('setValue', rec.value);
                        }
                    ">
                        <option value="consol.trans_ATD"><?= lang('实际离泊');?></option>
                        <option value="shipment.booking_ETD"><?= lang('订舱船期');?></option>
                        <option value="consol.report_date"><?= lang('report_date');?></option>
                    </select>
                    &nbsp;

                    <select name="field[s][]" readonly class="s2 easyui-combobox"  required="true" data-options="
                    ">
                        <option value="<="><=</option>
                    </select>
                    &nbsp;
                    <div class="this_v">
                        <input name="field[v][]" required id="booking_ETD_end" class="v2 easyui-datebox">
                    </div>
                </td>
            </tr>
            <tr>
                <td><?=lang('角色')?></td>
                <td>
                    <select class="easyui-combobox" id="user_role1" name="users[0][user_role]" style="width:380px;" data-options="
                            valueField:'user_role',
                            textField:'user_role',
                            url:'/bsc_user_role/get_role/',
                            onSelect:function(res){
                                var booking_ETD_start = $('#booking_ETD_start').datebox('getValue')
                                if(!booking_ETD_start){
                                    alert('<?= lang('请先选择开始时间');?>')
                                    $(this).combobox('clear')
                                    return;
                                }
                                var booking_ETD_end = $('#booking_ETD_end').datebox('getValue')
                                if(!booking_ETD_end){
                                    alert('<?= lang('请先选择结束时间');?>')
                                    $(this).combobox('clear')
                                    return;
                                }
                                var user_role = res.user_role
                                $.post('/report/query_lrb_user', { booking_ETD_start,booking_ETD_end,user_role },
                                    function(res){
                                        $('#user1').combobox('loadData',res)
                                    }, 'json');
                            }
                        ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('提成显示');?></td>
                <td align="left">
                    <select class="easyui-combobox" name="tc_type" id="tc_type" style="width:289px;" data-options="
                        onSelect:function(rec){
                            tc_type_select(rec);
                        },
                    ">
                        <option value=""><?= lang('不显示');?></option>
                        <option value='sales'><?= lang('个人提成');?></option>
                    </select>
                </td>
            </tr>
            <tr>
                <!--按销售姓名等-->
                <td><?= lang('姓名');?></td>
                <td align="left">
                    <select class="easyui-combobox" id="user1" name="users[0][user_ids][]" style="width:665px;" data-options="
                        valueField:'id',
                        textField:'otherName',
                        multiple:true,
                        onShowPanel:function(){
                            var booking_ETD_start = $('#booking_ETD_start').datebox('getValue')
                            if(!booking_ETD_start){
                                alert('<?= lang('请先选择开始时间');?>')
                                return;
                            }
                            var booking_ETD_end = $('#booking_ETD_end').datebox('getValue')
                            if(!booking_ETD_end){
                                alert('<?= lang('请先选择结束时间');?>')
                                return;
                            }
                            var user_role = $('#user_role1').combobox('getValue')
                            if(!user_role){
                                alert('<?= lang('请先选择角色');?>')
                                return;
                            }
                        }
                    ">
                    </select>
                </td>
            </tr>
            <tr style="border-bottom: 1px solid #ccc">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <script>
                function tc_type_select(rec){
                    if(rec.value=="" || rec.value=="sales") {
                        $('#sales_leader_tc').combobox("disable");
                        $('#sales_leader_tc').combobox("setValue","");
                        if(rec.value=="sales"){
                            $('#user1').combobox("options").multiple = false ;
                            $('#user_role1').combobox("setValue",'sales');
                            var booking_ETD_start = $('#booking_ETD_start').datebox('getValue')
                            if(!booking_ETD_start){
                                alert('<?= lang('请先选择开始时间');?>')
                                return;
                            }
                            var booking_ETD_end = $('#booking_ETD_end').datebox('getValue')
                            if(!booking_ETD_end){
                                alert('<?= lang('请先选择结束时间');?>')
                                return;
                            }
                            var user_role = $('#user_role1').combobox('getValue');
                            if(!user_role){
                                alert('<?= lang('请先选择角色');?>')
                                return;
                            }
                            $.post('/report/query_lrb_user', { booking_ETD_start,booking_ETD_end,user_role },
                                function(res){
                                    $('#user1').combobox('loadData',res)
                                }, 'json');
                            $('#sales_leader_tc_tr').hide();
                        }
                    } else{
                        $('#user1').combobox("options").multiple = true ;
                        $('#sales_leader_tc').combobox("enable");
                        $('#sales_leader_tc_tr').show();
                    }
                }
            </script>

            <script>
                function sales_leader_tc_select(rec){
                    if(rec.value=="") {
                        $('#user_role1').combobox("setValue",'sales');
                        $('#user1').combobox("setValue",'');
                        return;
                    }
                    $.getJSON("/bsc_user/get_xiashu/"+rec.value, function(json){
                        var xiashu = '';
                        $.each(json, function(i, field){
                            if(i>0) xiashu+=",";
                            xiashu += field['id'] ;
                        });
                        if(rec.value=='20119' || rec.value=='20117'){  //写死的， 张妍 王俊作为客服领导， 以后再优化
                            $('#user_role1').combobox("setValue",'customer_service');
                        }else{
                            $('#user_role1').combobox("setValue",'sales');
                        }
                        var booking_ETD_start = $('#booking_ETD_start').datebox('getValue')
                        if(!booking_ETD_start){
                            alert('<?= lang('请先选择开始时间');?>')
                            return;
                        }
                        var booking_ETD_end = $('#booking_ETD_end').datebox('getValue')
                        if(!booking_ETD_end){
                            alert('<?= lang('请先选择结束时间');?>')
                            return;
                        }
                        var user_role = $('#user_role1').combobox('getValue');
                        if(!user_role){
                            alert('<?= lang('请先选择角色');?>')
                            return;
                        }
                        $.post('/report/query_lrb_user', { booking_ETD_start,booking_ETD_end,user_role },
                            function(res){
                                $('#user1').combobox('loadData',res)
                            }, 'json');
                        $('#user1').combobox('setValues', xiashu);
                    });
                }
            </script>
            <?php if(menu_role('billing_special_bill')){?>
                <tr>
                    <td><?= lang('是否专项');?></td>
                    <td align="left">
                        <select class="easyui-combobox" name="is_special" id="is_special" style="width:380px;" data-options="
                        onSelect:function(rec){
                            is_special_select(rec);
                        },
                        value: '<?= $is_special;?>',
                    ">
                            <option value="0"><?= lang('非专项');?></option>
                            <option value="1"><?= lang('专项');?></option>
                            <option value=""><?= lang('包含专项');?></option>
                        </select>
                    </td>
                </tr>
            <?php } ?>
            <tr style="display:none;">
                <td>
                    <?= lang('部门');?>
                </td>
                <td align="left">
                    <select class="easyui-combobox" name="group_role" style="width:235px;" data-options="
                            valueField:'user_role',
                            textField:'user_role',
                            url:'/bsc_user_role/get_role/',
                            onLoadSuccess:function(){
                                var data = $(this).combobox('getData');
                                $(this).combobox('select', data[0].user_role);
                            }
                        ">
                    </select>
                    &nbsp;
                    <select class="easyui-combobox" id="group" name="group[]" style="width:110px;" data-options="
                        multiple:true,
                        valueField:'code',
                        textField:'name',
                        url:'/bsc_group/get_option/',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('shipment锁');?></td>
                <td align="left">
                    <div style="display: flex">
                        <div style="display: flex;width: 380px;justify-content: space-between;align-items: center">
                            <span><input name="lock_lv[]" id="S0" type="checkbox" value="S0"><?= lang('s未锁');?></span>
                            <span><input name="lock_lv[]" id="S1" type="checkbox" value="S1"><span title="<?= lang('操作锁');?>"> <?= lang('s1级');?></span></span>
                            <span><input name="lock_lv[]" id="S2" type="checkbox" value="S2"><span title="<?= lang('销售锁');?>"> <?= lang('s2级');?> </span></span>
                            <span><input name="lock_lv[]" id="S3" type="checkbox" value="S3"><span title="<?= lang('财务锁');?>"> <?= lang('s3级');?> </span></span>
                            <span><input name="lock_lv[]" id="C0" type="checkbox" value="C0"><?= lang('c未锁');?></span>
                            <span><input name="lock_lv[]" id="C3" type="checkbox" value="C3"><span title="<?= lang('carrier_cost锁');?>"> <?= lang('c3级');?></span></span>
                        </div>
                        <span>
                            <select class="easyui-combobox" editable="false" style="width:280px" data-options="
                                onSelect:function(rec){
                                    lock_lv_select(rec);
                                },
                            ">
                                <option value=""><?= lang('--请选择--');?></option>
                                <option value="NC3"><?= lang('C3未锁');?></option>
                                <option value="YC3"><?= lang('C3已锁');?></option>
                                <option value="NS1"><?= lang('S1未锁');?></option>
                                <option value="YS1"><?= lang('S1已锁');?></option>
                                <option value="NS2"><?= lang('S2未锁');?></option>
                                <option value="YS2"><?= lang('S2已锁');?></option>
                                <option value="NS3"><?= lang('S3未锁');?></option>
                                <option value="YS3"><?= lang('S3已锁');?></option>
                            </select>

                            <a href="javascript:void;" class="easyui-tooltip" data-options="
                                content: $('<div></div>'),
                                onShow: function(){
                                    $(this).tooltip('arrow').css('left', 20);
                                    $(this).tooltip('tip').css('left', $(this).offset().left);
                                },
                                onUpdate: function(cc){
                                    cc.panel({
                                        width: 300,
                                        height: 'auto',
                                        border: false,
                                        href: '/bsc_help_content/help/lock_query_note'
                                    });
                                }
                            "><?php echo lang('帮助');?></a>
                        </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <?= lang('查询');?>
                </td>
                <td align="left">
                    <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="
                        onChange:function(newVal, oldVal){
                            var index = $('.f').index(this);
                            if(combobox.hasOwnProperty(newVal) === true){
                                $('.v:eq(' + index + ')').textbox('destroy');
                                $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\'>');
                                $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                $('.v:eq(' + index + ')').combobox({
                                    data:ajax_data[combobox[newVal][0]],
                                    valueField:combobox[newVal][1],
                                    textField:combobox[newVal][2],
                                });
                            }else if ($.inArray(newVal, datebox) !== -1){
                                $('.v:eq(' + index + ')').textbox('destroy');
                                $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datebox\' >');
                                $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                            }else if ($.inArray(newVal, datetimebox) !== -1){
                                $('.v:eq(' + index + ')').textbox('destroy');
                                $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datetimebox\' >');
                                $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                            }else{
                                $('.v:eq(' + index + ')').textbox('destroy');
                                $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\'>');
                                $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                            }
                        }
                    ">
                        <!--<option value="bill.client_code"><?= lang('client_code');?></option>-->
                        <!--<option value="bill.client_code_name"><?= lang('client_code_name');?></option>-->
                        <option value="bill.charge"><?= lang('charge');?></option>
                        <option value="bill.charge_code"><?= lang('charge_code');?></option>
                        <option value="bill.currency"><?= lang('currency');?></option>
                        <option value="bill.amount"><?= lang('amount');?></option>
                        <option value="bill.account_no"><?= lang('account_no');?></option>
                        <option value="shipment.client_company"><?= lang('client_company');?></option>
                        <option value="shipment.hbl_type"><?= lang('hbl_type');?></option>
                        <option value="shipment_by_consol.job_no"><?= lang('shipment no');?></option>
                        <!--<option value="shipment.job_no"><?= lang('shipment no');?></option>-->
                        <option value="shipment.shipper_ref"><?= lang('shipper_ref');?></option>
                        <option value="consol.job_no"><?= lang('consol no');?></option>
                        <option value="consol.creditor"><?= lang('booking_agent');?></option>
                        <option value="consol.carrier_ref"><?= lang('carrier_ref');?></option>
                        <option value="consol.agent_ref"><?= lang('agent_ref');?></option>
                        <!--                        <option value="duty.operator">--><?//= lang('operator');?><!--</option>-->
                        <!--                        <option value="duty.booking">--><?//= lang('booking');?><!--</option>-->
                        <!--                        <option value="duty.customer_service">--><?//= lang('customer_service');?><!--</option>-->
                        <!--                        <option value="duty.document">--><?//= lang('document');?><!--</option>-->
                        <!--                        <option value="duty.finance">--><?//= lang('finance');?><!--</option>-->
                        <!--                        <option value="duty.sales">--><?//= lang('sales');?><!--</option>-->
                        <!--                        <option value="duty.marketing">--><?//= lang('marketing');?><!--</option>-->
                        <!--                        <option value="duty.oversea_cus">--><?//= lang('oversea_cus');?><!--</option>-->
                    </select>
                    &nbsp;

                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="
                    ">
                        <option value="like">like</option>
                        <option value="=">=</option>
                        <option value=">=">>=</option>
                        <option value="<="><=</option>
                        <option value="!=">!=</option>
                    </select>
                    &nbsp;
                    <div class="this_v">
                        <input name="field[v][]" class="v easyui-textbox">
                    </div>
                    <button class="add_tr" type="button">+</button>
                </td>
            </tr>
        </table>
        <br><br>
        <button class="easyui-linkbutton" id="submit" onclick="doSearch();" style="width:200px;height:30px;" type="button"><?= lang('search');?></button>
        <button class="easyui-linkbutton" id="reset" onclick="reset();" style="width:200px;height:30px;" type="button"><?= lang('reset');?></button>
        <a href="javascript:void(0);" class="easyui-tooltip" data-options="
            content: $('<div></div>'),
            onShow: function(){
                $(this).tooltip('arrow').css('left', 20);
                $(this).tooltip('tip').css('left', $(this).offset().left);
            },
            onUpdate: function(cc){
                cc.panel({
                    width: 500,
                    height: 'auto',
                    border: false,
                    href: '/bsc_help_content/help/query_condition'
                });
            }
        "><?= lang('帮助');?></a>
    </form>
    <input type="hidden" id="search_booking_ETD_start_f">
    <input type="hidden" id="search_booking_ETD_end_f">
</div>
<input type="hidden" id="hide_date" value="">
