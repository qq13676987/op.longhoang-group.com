<script type="text/javascript">
    var is_submit = false;
    /**
     * 保存数据
     */
    function save_data(){
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $('#fm').form('submit', {
            url: '/bsc_message/set_message_data/<?= $match['id_type'];?>/<?= $id_no;?>/<?= $match['message_type'];?>/<?= $match['message_receiver'];?>',
            onSubmit: function(param){
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                    is_submit = false;
                }
                return validate;
            },
            success:function(res_json){
                var res;
                ajaxLoadEnd();
                is_submit = false;
                try {
                    res = $.parseJSON(res_json);
                    if(res.code == 0){
                        //调用父页面的生成报文按钮
                        parent.create_message('<?= $match['message_type'];?>', '<?= $match['message_receiver'];?>', '<?= $action;?>', '<?= $match['id'];?>', 0);
                        parent.$.messager.alert('Tips', res.msg, 'info', function () {
                        });
                    }else{
                        parent.$.messager.alert('Tips', res.msg);
                    }
                }catch (e) {
                    parent.$.messager.alert('<?= lang('提示');?>', e);
                }
            }
        });
    }
</script>
<style>
    label{
        padding-right:10px;
        display: inline-block;
        vertical-align: middle;
    }
</style>
<body>
<br/>
<form id="fm" method="post">
    Rotation no: <input class="easyui-textbox" name="rotation_no" value="<?= $edi_data['rotation_no'];?>">
    
    <button type="button" onclick="save_data()" title="<?= lang('保存');?>"><?= lang('保存');?></button>
</form>

</body>