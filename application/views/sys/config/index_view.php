<script type="text/javascript">
    function doSearch(){
        $('#tt').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val() ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val() ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val()
        });
        $('#chaxun').window('close');
    }

    $(function () {
        $('#tt').edatagrid({
            url: '/sys_config/get_data',
            saveUrl: '/sys_config/add_data',
            updateUrl: '/sys_config/update_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            },
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height()
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });

</script>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th data-options="field:'id', width:80,sortable:true"><?= lang('id');?></th>
        <th data-options="field:'config_name', width:150,sortable:true,editor:'text'"><?= lang('name');?></th>
        <th data-options="field:'config_text', width:80,sortable:true,editor:'text'"><?= lang('text');?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#tt').edatagrid('addRow',0)"><?= lang('add');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#tt').edatagrid('saveRow')"><?= lang('save');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#tt').edatagrid('cancelRow')"><?= lang('cancel');?></a>
            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>
