<style>

</style>
<body>
<h4 style="color: red;font-size: 16px;">if you have other urgent things to do, please click delay button to postpone to-do-list. Note that the delay function has a limited number of times</h4>
<div id="tabs" class="easyui-tabs" style="width:100%;">
	<div title="New" data-options="selected:true"></div>
	<div title="Read"></div>
</div>
<table id="tt" style="width:1100px;height:450px">
	<thead>
	<tr>
		<th data-options="field:'id',width:80,sortable:true"><?= lang('ID');?></th>
		<th data-options="field:'table_name',width:150,sortable:true"><?= lang('table');?></th>
		<th data-options="field:'id_no',width:100,sortable:true"><?= lang('table ID');?></th>
		<th data-options="field:'txt',width:600,sortable:true,styler:txt_styler_for,formatter:txt_for"><?= lang('Title');?></th>
		<th data-options="field:'notice_user_name',width:100,sortable:true"><?= lang('Notice to');?></th>
		<th data-options="field:'lock_screen_datetime',width:180, formatter:lock_screen_datetime_for"><?= lang('Lock Sreen Time');?></th>
		<th data-options="field:'delay_num_count',width:110"><?= lang('Delay remain times');?></th>
		<th data-options="field:'lock_screen',width:60, formatter:lock_screen_for"><?= lang('is lock');?></th>
		<th data-options="field:'op', width:200, formatter:op_for"><?= lang('button');?></th>
	</tr>
	</thead>
</table>
<div id="tb" style="padding:3px;">
	<table>
		<tr>
			<td style="height: 30px;"></td>
			<td width='10'></td>
			<td>
				<a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="back();" style="color: red;">return back</a>
			</td>
			<td width='10'></td>
			<td>
				<a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="parent_reload();" style="color: blue;">reflash</a>
			</td>
			<td width='10'></td>
			<td>
				<a href="javascript:void(0)" class="easyui-linkbutton" id="one_key" plain="true" onclick="one_key();" style="color: green;">delay all list</a>
			</td>
		</tr>
	</table>
</div>
</body>
<script>
    function one_key(){
        let rows = $("#tt").datagrid('getData').rows
        let str = ''
        if(rows){
            ajaxLoading();
            rows.map(item=>{
                str += item.id+','
            })
            $.post("/bsc_notice/one_key", { ids: str },
                function(res){
                    ajaxLoadEnd();
                    $.messager.alert('<?= lang('提示');?>', res.msg);
                    if(res.code == 1){
                        $("#tt").datagrid('reload')
                    }
                }, "json");
            ajaxLoadEnd();
        }

    }
    function back(){
        location.href = "<?= $back_url;?>";
    }

    /**
     * 按钮的相关模板函数
     */
    function op_for(value, row, index) {
        var str = "";
        if (row.status == 0 && row.lock_screen == 1) {
            let delay_status = $("#tt").datagrid('getData').delay_status
            if(delay_status === false){
                str += '<button type="button" disabled><?= lang('delay 4 hours');?></button >&nbsp;';
            }else{
                str += '<button type="button" onclick="delay_hour(' + row.id + ', 4)"><?= lang('delay 4 hours');?></button >&nbsp;';
            }
            str += '<button type="button" onclick="close_notice(' + row.id + ')"><?= lang('close notice');?></button >';
        }else if (row.status == 0 && row.lock_screen == 0){
            str += '<button type="button" onclick="set_read(' + row.id + ')"><?= lang('I see');?></button >';
        }else{
            str += '<span style="color: #ccc">Read</span>';
        }
        return str;
    }

    function lock_screen_for(value, row) {
        if (value == 1){
            return '<span style="color: red">Yes</span>';
        }else{
            return '<span style="color: #666">No</span>';
        }
    }

    function lock_screen_datetime_for(value, row) {
        if (row.lock_screen == 1){
            return value;
        }else{
            return '<span style="color: #666">None</span>';
        }
    }
    function txt_styler_for(value, row) {
        if(row.lock_screen_status == 2) return "color:#000"
        if(row.lock_screen_status == 1) return "color:#ccc"
        return '';
    }
    function txt_for(value, row) {
        if(row.lock_screen_status == 2) return value+" &emsp;<span style='color: red'>Must Handle</span>"
        if(row.lock_screen_status == 1) return value+" &emsp;<span style='color: blue'>Time is ok</span>"
    }

    function parent_reload(){
        window.parent.location.reload();
    }

    function set_read(id) {
        $.ajax({
            url:'/bsc_notice/notice_read?id=' +id,
            type:'get',
            dataType:'json',
            success:function(res) {
                $('#tt').datagrid('reload');
            }
        });
    }
    var close = false;
	function close_all_notice(){
		let rows = $("#tt").datagrid('getData').rows
		if(rows.length > 0){
			close = true
			ajaxLoading('<?=lang('正在检测可以自动关闭的锁屏通知。')?>')
			for (let i = 0; i < rows.length; i++) {
				$.ajax({
					type: 'POST',
					url: '/bsc_notice/close_notice',
					data: {
						id: rows[i].id,
					},
					success: function (res_json) {
						if(i === rows.length -1){
							ajaxLoadEnd();
							$('#tt').datagrid('reload')
						}
					},
					error:function (e) {
						ajaxLoadEnd();
					}
				});

			}
		}

	}
    function close_notice(id) {
        $.messager.confirm("<?= lang('提示');?>", "<?= lang('confirm to close notice?');?>", function (r) {
            if(r){
                $.ajax({
                    type: 'POST',
                    url: '/bsc_notice/close_notice',
                    data: {
                        id: id,
                    },
                    success: function (res_json) {
                        ajaxLoadEnd();
                        try {
                            var res = $.parseJSON(res_json);
                            $.messager.alert('<?= lang('提示');?>', res.msg);
                            if(res.code == 0){
                                $('#tt').datagrid('reload');
                            }
                        }catch (e) {
                            $.messager.alert('<?= lang('提示');?>', res_json);
                        }
                    },
                    error:function (e) {
                        ajaxLoadEnd();
                        $.messager.alert('<?= lang('提示');?>', e.responseText);
                    }
                });
            }
        })
    }

    /**
     * 延迟锁屏通知时间, 第二个参数单位为秒
     * @param id
     * @param hour
     */
    function delay_hour(id, hour){
        $.messager.confirm("<?= lang('提示');?>", "<?= lang('confirm to delay?');?>", function (r) {
            if(r){
                var time = hour * 3600;
                ajaxLoading();
                $.ajax({
                    type: 'POST',
                    url: '/bsc_notice/delay_notice',
                    data: {
                        id: id,
                        second: time,
                    },
                    success: function (res_json) {
                        ajaxLoadEnd();
                        try {
                            var res = $.parseJSON(res_json);
                            $.messager.alert('<?= lang('提示');?>', res.msg);
                            if(res.code == 0){
                                $('#tt').datagrid('reload');
                            }
                        }catch (e) {
                            $.messager.alert('<?= lang('提示');?>', res_json);
                        }
                    },
                    error:function (e) {
                        ajaxLoadEnd();
                        $.messager.alert('<?= lang('提示');?>', e.responseText);
                    }
                });
            }
        })
    }

    $(function () {
        $('#tabs').tabs({
            border: false,
            onSelect: function (title) {
                if (title == 'New') {
                    var isread = 0;
                }
                if (title == 'Read') {
                    var isread = 1;
                }

                $('#tt').datagrid('load', {isread:isread});
            }
        });

        $('#tt').datagrid({
            url: '/bsc_notice/get_user_noitce_data',
            width:'auto',
            height: $(window).height() - 100,
            rownumbers:false,
            toolbar:'#tb',
            nowrap:false,
            onDblClickRow:function(index, row){
                window.open(row.href);
            },
            onLoadSuccess(data){
                if(data.delay_status === false){
                    $('#one_key').linkbutton({
                        disabled: true
                    });
                }
				if(close === false){
					close_all_notice()
				}
            }
        });
        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });
</script>
