<script type="text/javascript">


    var url = '';
    function doSearch(){
        $('#tt').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val() ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val() ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val()
        });
        $('#chaxun').window('close');
    }

    $(function () {
        $('#tt').edatagrid({
            url: '/biz_auto_rate/get_data/',
            destroyUrl: '/biz_auto_rate/delete_data',
            onDblClickRow:function (index,row) {
                $('#dlg').dialog('open').dialog('center').dialog('setTitle','add');
                $('#fm').form('load', row);
                url = '/biz_auto_rate/update_data?id=' + row.id;
            }
        });
        $('#tt').datagrid({
            width:'auto',
            height: $(window).height(),
        });


        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });
</script>

<table id="tt" style="width:1100px;height:450px" rownumbers="false" idField="id" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
        <tr>
            <th data-options="field:'id',width:80,sortable:true"><?= lang('id');?></th>
            <th data-options="field:'job_no',width:100"><?= lang('SHIPMENT');?></th>
            <th data-options="field:'currency',width:60"><?= lang('currency');?></th>
            <th data-options="field:'unit_price',width:100"><?= lang('unit_price');?></th>
            <th data-options="field:'number',width:100"><?= lang('number');?></th>
            <th data-options="field:'amount',width:120"><?= lang('amount');?></th>
            <th data-options="field:'vat',width:120"><?= lang('vat');?></th>
            <th data-options="field:'client_code_name',width:150,sortable:true"><?= lang('client_code_name');?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($rows as $row){ ?>
        <tr>
            <td><?= $row['id'];?></td>
            <td><?= $row['job_no'];?></td>
            <td><?= $row['currency'];?></td>
            <td><?= $row['unit_price'];?></td>
            <td><?= $row['number'];?></td>
            <td><?= $row['amount'];?></td>
            <td><?= $row['vat'];?></td>
            <td><?= $row['client_code_name'];?></td>
        </tr>
        <?php } ?>
    </tbody>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                分摊后的总金额:<input class="easyui-textbox" disabled value="<?= $sum_amount;?>">
                分摊前的金额:<input class="easyui-textbox" disabled value="<?= $parent_row['amount'];?>">
            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>


