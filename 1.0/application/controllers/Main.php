<?php

class Main extends Common_Controller
{

    public function __construct()
    { 
        parent::__construct();
        $this->load->model('m_model');
    }

    public function index()
    {
        if (get_session('id') == '') {
            redirect('main/login/', 'refresh');
        }
        echo "<script>parent.location.href='/desktop';  </script>";
    }

    /**
     * 登录页面
     */
    public function login()
    {
        $data = array();
        $data['back_url'] = getValue('back_url', '/main');
        $this->load->view('head');
        $this->load->view('login_view', $data);
    }

    /**
     * 退出登录
     */
    public function loginout()
    {
        $this->session->sess_destroy();
        redirect('main/login', 'refresh');
    }

    /**
     * 管理员用的模拟登录别人账号
     * @param string $user_name
     */
    public function admin_login($user_name= '')
    {
        $user_name = urldecode($user_name);
        //保险,还是只能管理员账号才能切,麻烦点好点
        if(!is_admin()){
            redirect('', 'refresh');
        }
        if(get_session('admin_login') != 'admin' && !is_admin()){
            redirect('', 'refresh');
        }
        $this->load->model('bsc_user_model');
        
        $r = Model('bsc_user_model')->get_one('name', $user_name);
        if (!empty($r)) {
            if($r['status'] == 1){
                redirect('', 'refresh');
            }else{
                $this->setUser($r, 0);
                redirect('', 'refresh');
            }
        } else {
            redirect('', 'refresh');
        }

    }

    public function check_login_ajax()
    {
        $this->load->model('bsc_user_model');
        $username = postValue('username', '');
        $password = postValue('password', '');

        $r = $this->bsc_user_model->check_pwd($username, $password);

        if (!empty($r)) {

            if($r['status'] == 1){
                echo json_encode(array('code' => 1, 'msg' => lang('账号已禁用')));
            }else{
                $this->setUser($r);
                $this->login_log();
                echo json_encode(array('code' => 0, 'msg' => lang('登录成功')));
            }
        } else {
            echo json_encode(array('code' => 1, 'msg' => lang('账号名或密码错误')));
        }

    }

    //用户登录的核心函数， $not_pro是否绑定用户设备ID
    protected function setUser($user, $not_pro = 1){
        Model('bsc_user_model');
        $this->session->unset_userdata(array('id', 'title', 'name', 'menu', 'user_role','m_level', 'station', 'group', 'group_range', 'user_range', 'level'));
        $this->load->model('bsc_user_role_model');
        
        $newdata = array();
        $newdata['id']=$user["id"]; 
        $newdata['title']=$user["title"]; 
        $newdata['name']=$user["name"]; 
        $newdata['group']=explode(",", $user['group']); 
        $newdata['user_role']=explode(",", $user['user_role']); 
        $newdata['station']= join(',', filter_unique_array(explode(',', $user['station']))); 
        $newdata['level']=$user["level"]; 
        $newdata['m_level']=$user["m_level"]; 
        
         //如果该用户是助理角色，则group,group_range, user_range赋值给助理。   special_text特殊权限不赋能
        if($user["assistant_flag"]==1){
             $leader_user = Model('bsc_user_model')->get_by_id($user["leader_id"]);
             if(!empty($leader_user)){
                $newdata['name']= $user['name']."({$leader_user['name']}的助理)";
                $newdata['group_range']= $leader_user["group_range"];
                $newdata['user_range']=  explode(",", $this->get_user_range($leader_user['id']));
            }else{
                $newdata['group_range']=""; 
                $newdata['user_range']=  array();   
            }
        }else{
            $newdata['group_range']=$user["group_range"]; 
            $newdata['user_range']=  explode(",", $this->get_user_range($user['id']));  
        }
        
        //如果改用户有代班，再次拼接user_range
        $sql ="select * from bsc_user_daiban where to_user_id = {$user['id']} and status = 1";
        $daiban = $this->m_model->query_one($sql);  //暂时只考虑代1个人班的情况
        if(!empty($daiban)){
            $from_user = $this->bsc_user_model->get_by_id($daiban["from_user_id"]);
            $new_group_range = $newdata["group_range"].",".$from_user["group_range"];
            $newdata['group_range']= join(",",array_unique(explode(",",$new_group_range)));
            $new_user_range = explode(",", $this->get_user_range($from_user['id']));  
            $newdata['user_range']=array_unique(array_merge($newdata["user_range"],$new_user_range));
        }  
            
        setcookie('crm_user', null, time() - 3600, '/'); 
        setcookie('crm_user_type', null, time() - 3600, '/'); 
        $this->session->set_userdata($newdata);  //先set一下， 在下面model里要用到这个session
        
        $all_role = $this->bsc_user_role_model->get_user_role(array('menu', 'special_text'));
        //存取menu
        $newdata['menu'] = $all_role['menu'];
        //登录完成后，给当前用户绑定设备ID
        if($not_pro){
            $computer_code = json_decode(public_decrypt($_COOKIE['computer_code']), true);
            $this->bsc_user_model->update($user['id'], array('login_system_id' => $computer_code['id']));
        }else{
            $this->session->set_userdata(array('admin_login' => 'admin'));
        }
        $this->session->set_userdata($newdata);
    }

    public function get_user_range($user_id){
        // 金金单独处理，直接获取全公司的id,因为就他的权限大，比较慢
        if($user_id == 20057){
            $sql = "select id from bsc_user";
            $rs = $this->m_model->query_array($sql);
            $u = array_column($rs,"id");
            return implode(",",$u);
        }
        //获取这个人的所有下属部门 
        $sql = "select group_range,assistant_flag,user_range from bsc_user where id = $user_id";
        $row = $this->m_model->query_one($sql);
        if($row["assistant_flag"]==1) return "$user_id"; //如果是助理，则无需获取他自己本人的user range，以他上司为准。
        $direct_user_range = $row["user_range"];
        
        $group_range = $row["group_range"];
        $groups = explode(",",$group_range);
        foreach($groups as $g_code){
            if($g_code=="") continue;
            $group_range .=",".$this->get_next_group($g_code);
        }
        $arr = explode(",",$group_range);
        $arr = array_unique($arr);
        $group_range = implode("','",$arr);
        $group_range = "'$group_range'";
        // echo $group_range;// 获取了所有部门
        $sql = "select id from bsc_user where `group` in ($group_range)";
        $rs = $this->m_model->query_array($sql);  //获取了所有下属部门里的人员
        
        $user_range = $this->get_next_user_range($user_id); //获取他自己本人通过 leader id 关联的下属员工
        $user_range .= ",$direct_user_range"; //拼接他的通过user_range的直属下级
        $user_range = join(",",array_unique(explode(",",$user_range)));  //去重
        
        $user_arr = explode(",",$user_range);
         
        foreach($rs as $r){
            if(in_array($r["id"],$user_arr)) continue;   //如果下属部门里的员工本来就是他本人的直接下属员工，可以不用重复查询
            $user_range .= ",".$this->get_next_user_range($r["id"]);
        } 
        
        $arr = explode(",",$user_range);
        $arr = array_filter(array_unique($arr));
        $user_range = implode(",",$arr);
        return $user_range; 
    }
     
    //递归获取下级user id
    public function get_next_user_range($leader_id,$deep=0){
        if($deep>10) return "";  //简单的防止死循环
        $deep++;
        
        $res = "$leader_id";
        $sql="select id from bsc_user where leader_id = $leader_id;";
        $users = $this->m_model->query_array($sql);
        foreach($users as $u){
            $user_id = $u["id"];
            // $res .= ",$user_id,";
            $res .= ",".$this->get_next_user_range($user_id,$deep); 
        }
        return $res;
    }
     
    //递归获取下级部门
    public function get_next_group($group_code){
        $res = $group_code;
        $sql = "select id from bsc_group where group_code = '$group_code'";
        $row = $this->m_model->query_one($sql);
        if(empty($row)) return ",";
        $sql = "select group_code from bsc_group where parent_id={$row["id"]}";
        $groups = $this->m_model->query_array($sql);
        foreach($groups as $g){
            $res .=",".$this->get_next_group($g["group_code"]);
        }
        return $res;
    }
    
    //用思维导图显示整个公司的架构
    public function xmind_all_get_data(){
        $index = 1;
        $data=array(); 
        $data[]=array(
            "id"=>$index,
            "isroot"=>true,
            "topic"=>'金晶'
        ); 
        $pid = $index; 
        //显示分公司
        $sql = "select id,`group` from bsc_user where leader_id = 20057 and status = 0";
        $rs1 = $this->m_model->query_array($sql);
        $ids = array_column($rs1,"id");
        $ids = implode(",",$ids);
        $groups = array_column($rs1,"group");
        $groups = array_unique($groups);
        $groupstring = implode("','",$groups);
        $groupstring = "'".$groupstring."'"; 
        $sql = "select distinct company_code from bsc_group where group_code in ($groupstring);";
        $companys = $this->m_model->query_array($sql);
        $companys = array_column($companys,"company_code");
        foreach($companys as $company){
            $sql="SELECT `group_name` FROM bsc_group WHERE group_code = '$company'";
            $gname = $this->m_model->query_one($sql); 
            $index++;
            //第一层级的分公司显示
            $data[]=array(
                "id"=>$index,
                "parentid"=>$pid,
                "topic"=>$gname["group_name"],
                "expanded"=>false, 
            );   
            $pid2 = $index;
            
            $sql="SELECT `id` FROM bsc_user WHERE `group` in (select group_code from bsc_group where company_code='$company') and id in ($ids) and status = 0";
            $rs3 = $this->m_model->query_array($sql); 
            foreach($rs3 as $row3){ 
                $sql="SELECT `name` FROM bsc_user WHERE id ={$row3["id"]} and status = 0";
                $uname = $this->m_model->query_one($sql); 
                $index++;
                //第一层级的分公司里的人员
                $data[]=array(
                    "id"=>$index,
                    "parentid"=>$pid2,
                    "topic"=>$uname["name"],
                    "expanded"=>false,  
                );  
                $pid3 = $index;
                
                $user_range4 = $this->get_user_range($row3["id"]); 
                $sql="SELECT `group` FROM bsc_user WHERE id in ($user_range4) and status = 0";
                $rs4 = $this->m_model->query_array($sql); 
                $groups4 = array_column($rs4,"group");
                $groups4 = array_unique($groups4); 
                foreach($groups4 as $group4){
                    $sql="SELECT `group_name` FROM bsc_group WHERE group_code = '$group4' and status = 0";
                    $gname = $this->m_model->query_one($sql); 
                    $index++;
                    //第2层级的部门
                    $data[]=array(
                        "id"=>$index,
                        "parentid"=>$pid3,
                        "topic"=>$gname["group_name"],
                        "expanded"=>false, 
                    );   
                    $pid4 = $index;
                    
                    $sql="SELECT `id` FROM bsc_user WHERE `group` = '$group4' and id in($user_range4) and status = 0";
                    $rs5 = $this->m_model->query_array($sql); 
                    foreach($rs5 as $row5){ 
                        $sql="SELECT `name`,group_range,assistant_flag,(select `name` from (bsc_user as b) where b.id = a.leader_id) as leader_name FROM (bsc_user as a) WHERE a.id = {$row5["id"]} and  a.status = 0";
                        $uname = $this->m_model->query_one($sql); 
                        $index++;
                        //第2部门层级里的人
                        if($uname["assistant_flag"]==1) $uname["name"] .= ", {$uname["leader_name"]} 助理";
                        if(strlen($uname["group_range"])>2){
                            $data[]=array(
                                "id"=>$index,
                                "parentid"=>$pid4,
                                "topic"=>"<a href='/main/xmind_by_id/{$row5["id"]}' target='_blank'>".$uname["name"]."⊕</a>",
                                "expanded"=>false,  
                            );  
                        }else{
                            $data[]=array(
                                "id"=>$index,
                                "parentid"=>$pid4,
                                "topic"=>$uname["name"],
                                "expanded"=>false,  
                            );  
                        }
                    }
                }
                
            }
            
        }
        
        $mind=array(
            "meta"=>array(
                "name"=>"duo",
                "author"=>"x@163.com",
                "version"=>"0.2",
            ),
            "format"=>"node_array",
            "data"=>$data

        );
        // print_r($mind);
        // exit();

        file_put_contents("xmind.txt",json_encode($mind)) ;
        echo "ok finished, please reload the pre page";
    }
    
    //用思维导图显示某1个人的权限
    public function xmind_by_id($user_id){ 
        $this->load->view('bsc/user/xmind_by_id',array("user_id"=>$user_id));
    }
    public function xmind_by_id_get_data($user_id){ 
        $sql="SELECT `name` FROM bsc_user WHERE id =$user_id ";
        $uname = $this->m_model->query_one($sql); 
        $data=array(); 
        $data[]=array(
            "id"=>$user_id,
            "isroot"=>true,
            "topic"=>$uname["name"]
        ); 
        
        $user_range = $this->get_user_range($user_id);   
        $sql="SELECT `group` FROM bsc_user WHERE id in ($user_range)";
        $rs = $this->m_model->query_array($sql); 
        $groups = array_column($rs,"group");
        $groups = array_unique($groups); 
        foreach($groups as $key=>$group){
            $sql="SELECT `group_name` FROM bsc_group WHERE group_code = '$group'";
            $gname = $this->m_model->query_one($sql); 
            $new_node = "'".($key+10000000)."'";
            $data[]=array(
                "id"=>$new_node,
                "parentid"=>$user_id,
                "topic"=>$gname["group_name"],
                "expanded"=>false, 
            );   
            $sql="SELECT `id` FROM bsc_user WHERE `group` = '$group' and id in($user_range)";
            $rs = $this->m_model->query_array($sql); 
            foreach($rs as $row){ 
                $sql="SELECT `name` FROM bsc_user WHERE id ={$row["id"]}";
                $uname = $this->m_model->query_one($sql); 
                $data[]=array(
                    "id"=>"'".rand(100000,999999)."'",
                    "parentid"=>$new_node,
                    "topic"=>$uname["name"],
                    "expanded"=>false,  
                );  
            }
        }
        

        $mind=array(
            "meta"=>array(
                "name"=>"duo",
                "author"=>"x@163.com",
                "version"=>"0.2",
            ),
            "format"=>"node_array",
            "data"=>$data

        );

        echo json_encode($mind);
    }

    /**
     * 记录登录日志
     * @param int $type 类型  0是普通, 1是微信, 2是短信
     */
    private function login_log($type = 0){
        //type 0 账号密码登录 1 微信登录 2短信登录
        $sys_login_log_model = Model('m_model');
        $sys_login_log_model::$table = 'sys_login_log';
        $data = array();
        $computer_code = json_decode(public_decrypt($_COOKIE['computer_code']), true);
        $data['system_pass_id'] = $computer_code['id'];
        $data['user_id'] = get_session('id');
        $data['type'] = $type;
        $data['login_time'] = date('Y-m-d H:i:s');
        $this->m_model->save($data);
    }
    
     /**
     * 切换主账号重新登录, 切换后,除了ID还是自己的,其他属性全是主账号的
     */
    public function change_login(){
        $pan_id = (int)getValue('pan_id', 0);
        $user_id = get_session('id');

        Model('bsc_user_model');
//        $this->db->select('id,title,name,menu,user_role,station,group,group_range,user_range,level,pan_id,status');
        $pan_user = $this->bsc_user_model->get_by_id($pan_id);
        //如果主账号不存在,提示
        if(empty($pan_user)) return jsonEcho(array('code' => 1, 'msg' => '主账号不存在,请联系管理员重新设置'));
        if($pan_user['status'] != '0') return jsonEcho(array('code' => 1, 'msg' => '主账号已被禁用,请联系管理员重新设置'));

        //查看当前账号是否有异常
        $this->db->select('id,pan_id,status');
        $user = $this->bsc_user_model->get_by_id($user_id);
        if(empty($user)) return jsonEcho(array('code' => 2, 'msg' => '当前账号不存在,已自动退出登录'));
        if($user['status'] != '0') return jsonEcho(array('code' => 2, 'msg' => '当前账号已被禁用'));

        //这里再查一下当前账号,看是否有关联,没关联不给登录
        $all_pan = array_column(get_user_pan($pan_id), 'id');
        if(!in_array($pan_id, $all_pan)) return jsonEcho(array('code' => 1, 'msg' => '指定账号与当前账号不存在关联关系,请刷新后重试'));

        //执行登录代码
//        $pan_user['id'] = $user_id;

        $this->setUser($pan_user);
        return jsonEcho(array('code' => 0, 'msg' => '切换成功',$pan_user));
    }
}