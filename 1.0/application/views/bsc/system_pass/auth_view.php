<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>check</title>
</head>
<body>
<script language="javascript" src="/inc/lodop/LodopFuncs.js?v=3"></script>
<script type="text/javascript">
    var LODOP; //声明为全局变量
    function getSystemInfo(strINFOType,oResultOB, thisId){
        LODOP=getLodop();
        if(LODOP === undefined){
            //alert('<?//= lang('调用控件失败');?>//');
        }
        if (LODOP.CVERSION) {
            CLODOP.On_Return=function(TaskID,Value){
                if (oResultOB) oResultOB.value=Value;
                if (oResultOB) {
                    oResultOB.val(Value);
                    if (typeof(Storage) !== "undefined") {
                        if(localStorage.getItem(thisId)==null || localStorage.getItem(thisId) == ''){
                            localStorage.setItem(thisId, Value);
                        }else{
                        }
                        get_pass(Value);
                    }
                }
            };
            var strResult=LODOP.GET_SYSTEM_INFO(strINFOType);
        }
        if (!LODOP.CVERSION) return strResult; else return "";
    }

    var get_status = 0;

    function ajaxLoading(){
        load = true;
        $("<div class=\"datagrid-mask\"></div>").css({display:"block",width:"100%",height:$(window).height()}).appendTo("body");
        $("<div class=\"datagrid-mask-msg\"></div>").html("正在获取设备信息,请稍候。。。").appendTo("body").css({display:"block",left:($(document.body).outerWidth(true) - 190) / 2,top:($(window).height() - 45) / 2});
    }
    function ajaxLoadEnd(){
        load = false;
        $(".datagrid-mask").remove();
        $(".datagrid-mask-msg").remove();
    }
    var load = false;
    function get_computer() {
        var need_get = [
            ['MAC', 'NetworkAdapter.1.PhysicalAddress']
            // ,['CPU_no', 'Processor.CPUID']
            // ,['BIOS_no', 'BIOS.SerialNumber']
            // ,['disk_no', 'DiskDrive.1.SerialNumber']
        ];
        var length = need_get.length;
        if(!load){
            ajaxLoading();
        }
        $.each(need_get, function (index, item) {
            if(index >= get_status){
                get_status++;
                //判断localstorage不能开启的话,正常调用查看
                if(typeof(Storage) !== "undefined" && localStorage.getItem(item[0]) !== null && localStorage.getItem(item[0]) !== ''){
                    $('#' + item[0]).val(localStorage.getItem(item[0]));
                }else{
                    $('#' + item[0]).val(getSystemInfo(item[1], $('#' + item[0]), item[0]));
                    return false;
                }
            }
        });
        if(load && get_status >= length){
            clearInterval(timeout);
            get_pass();
            ajaxLoadEnd();
        }
    }

    window.onload = function () {
        // getLodop();
        // window.getCLodop();
    }
    function get_pass(MAC=''){
        if(MAC == ''){
            MAC = $('#MAC').val();
        }
        ajaxLoading();
        $.ajax({
            type:'GET',
            url:'/bsc_system_pass/get_pass?MAC=' + MAC,
            dataType:'json',
            success:function (res) {
                ajaxLoadEnd();
                if(res.code === 0){
                    location.href = '/';
                }else if(res.code === 3){
                    location.href = '/bsc_system_pass/check_pass';
                }
                // location.reload();
            },
            error: function () {
                ajaxLoadEnd();
                $.messager.alert('Tips', 'error');
            }
        });
    }

    function save() {
        var json = {};
        var form_data = $('#fm').serializeArray();
        $.each(form_data, function (index, item) {
            //判断如果name存在,且为string类型
            json[item.name] = item.value;
        });
        if(json.MAC == ''){
            $.messager.alert('Tips', '<?= lang('若无法获取,点击这里{download_url}更新后再试', array('download_url' => '<a href=\'/inc/lodop/CLodop_Setup_for_Win32NT.zip\' target=\'_self\'>下载最新版</a>'));?>');
            return;
        }
        if($('#fm').form('validate')){
            $.ajax({
                type:'POST',
                url:'/bsc_system_pass/pass_apply',
                data:json,
                dataType:'json',
                success:function (res) {
                    $.messager.alert('Tips', res.msg);
                    // location.reload();
                    if(res.code == 0){
                        location.href = '/bsc_system_pass/check_pass';
                    }
                },
                error: function () {
                    $.messager.alert('Tips', 'error');
                }
            });
        }

    }

    var timeout = setInterval(get_computer, 500);
</script>
<!--<input type="hidden" id="disk_no">-->
<!--<input type="hidden" id="CPU_no">-->
<!--<input type="hidden" id="BIOS_no">-->
<form id="fm" method="post">
<!--    <h3 style="color:red">若已安装控件，无法获取，点<a href="http://wenda.leagueshipping.com/?/question/601" target="_blank">这里</a></h3>-->
<!--    <a href='http://wenda.leagueshipping.com/?/question/601' target="_blank">可能需要查看这个设置下</a>-->
    <h3 style="color:red"><?= lang('若无法获取,点击这里{download_url}更新后再试', array('download_url' => '<a href=\'/inc/lodop/CLodop_Setup_for_Win32NT.zip\' target=\'_self\'>下载最新版</a>'));?></h3>
    <hr>
    <br>
    <br><?= lang('申请许可');?>:
    <input type="hidden" name="MAC" id="MAC">
    <table>
        <tr>
            <td><?= lang('姓名');?>:</td>
            <td>
                <input class="easyui-textbox" name="name" id="name" required style="width: 150px;">
            </td>
        </tr>
        <tr>
            <td><?= lang('电话');?></td>
            <td>
                <input class="easyui-textbox" name="telephone" id="telephone" required style="width: 150px;">
            </td>
        </tr>
        <tr>
            <td><?= lang('备注');?></td>
            <td>
                <textarea name="remark" id="remark" style="width: 150px;"></textarea>
            </td>
        </tr>
    </table>
    <button type="button" onclick="save()"><?= lang('提交');?></button>
</form>
</body>
</html>