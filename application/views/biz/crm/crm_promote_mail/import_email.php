<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?= lang('导入Email');?></title>
	<meta name="keywords" content="">
	<meta name="viewport" content="width=1180">
	<script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
	<link rel="stylesheet" href="/inc/third/layui/css/layui.css"/>
	<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
	<link rel="stylesheet" href="/inc/third/handsontable/handsontable.full.min.css"/>
	<script type="text/javascript" src="/inc/third/handsontable/handsontable.full.min.js"></script>
	<style>
		.search_box {
			border: 1px solid #ddd;
			float: left;
			margin: 10px 0px 0px;
			padding: 15px;
			background-color: #edf2f3;
			width: 99%;
		}
		.selected-td{ background: #ff4c42 !important; }
		.layui-this{font-size: 18px !important; font-weight: bolder}
	</style>
</head>
<body>
<div class="layui-tab layui-tab-brief"  style="margin-left: 20px;">
	<ul class="layui-tab-title">
		<li class="layui-this"><?= lang('Import by excel. please input customer name and mailbox two columns at least');?></li>
	</ul>
	<div class="layui-tab-content" style="padding-left: 0px;">
		<div class="layui-tab-item layui-show">
			<div style="margin-bottom: 80px;">
				<div id="handsontable_box" style="width: 99%"></div>
			</div>
			<div id="save_tools" style="display:; width: 100%; text-align: center; border-top: 1px solid #008a6b; padding: 20px 0px;position: fixed; z-index: 999999; bottom: 0px; left: 0px; background-color: white;">
				<button class="layui-btn" id="import_btn" style=" width: 300px;"><?= lang('Import');?></button>
			</div>
		</div>
	</div>
</div>

<!--进度条-->
<div id="progress" style=" display: none;">
	<div style="color: red; height: 35px; line-height: 30px; padding: 10px 25px;"><?= lang('Tips：导入过程中请不要关闭或刷新窗口，否则导入进程会被打断！');?></div>
	<div class="layui-progress layui-progress-big" lay-filter="demo" lay-showPercent="true"
		 style="margin:0px 20px;">
		<div class="layui-progress-bar layui-bg-green" lay-percent="0%"></div>
	</div>
</div>
</body>
<?php
if (isset($_POST['import_data']) && $_POST['import_data'] != ''){
	$default_data = base64_decode($_POST['import_data']);
}else{
	$default_data = '[{},{},{}]';
}
?>
<script src="/inc/third/layuiadmin/layui/layui.js"></script>
<script src="/inc/js/importExcel.js?time=<?php echo time();?>"></script>
<script type="text/javascript">
    var import_data = false;
    layui.config({
        base: '/inc/third/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index',
    }).use(['index', 'element', 'form'], function () {
        var element = layui.element;

        //保存表格里的数据
        $('#import_btn').on('click', function () {
            //var loading = layer.msg('<?//= lang('导入中，请耐心等待...');?>//', {icon:16, time:0, shade:0.5});
            var data = [];
            var param = hot.getData();
            var columns = ['custom', 'email'];

            //重组数据加入键名
            for (i = 0; i < param.length; i++) {
                var temp= {}
                $.each(columns, function (index, val) {
                    temp[val] = param[i][index];
                });

                if (temp['email']==null || temp['email'].trim()=='' || temp['email']=='-') continue;
				data.push(temp);
            }
			layui.element.progress('demo', '0%');
			//载入导入进程
			importExcel.init({
				'data': data,
				'importUrl': '/crm_promote_mail/import_data/?group_id=<?=$_GET['group_id'];?>',
				'columns': columns,
			})
            //$.ajax({
            //    type: 'post',
            //    url: '/crm_promote_mail/import_data/?group_id=<?//=$_GET['group_id'];?>//',
            //    data: {data:data},
            //    success: function (data) {
            //        layer.close(loading);
            //        if (data.code == 1) {
            //            layer.msg('<?//= lang('导入完成。');?>//', {icon:1});
            //        } else {
            //            layer.alert(data.msg, {icon:5});
            //        }
            //    },
            //    error: function(data){
            //        layer.close(loading);
            //        layer.alert('<?//= lang('请求失败：');?>//'+data.status, {icon:5});
            //    },
            //    dataType: "json"
            //});
        });
    });

    $(function () {
        var columns = [
            {data: 'custom', type: 'text', className: "htLeft", width: 400},
            {data: 'email', type: 'text', className: "htLeft", width: 300},
        ];
        var colHeaders = ['<?= lang('客户名称');?>', '<?= lang('邮箱');?>'];
        var settingForHandsontable = {
            columns: columns, //列配置参数
            data: [{},{},{}],//为需要绑定数据集合
            licenseKey: 'non-commercial-and-evaluation',
            colHeaders: colHeaders, // 表头
            rowHeaders: true,
            dropdownMenu: true, //忘了
            fillHandle: true,
            contextMenu: true,//右键显示更多功能,
            filters: true,
            startRows: 0,
            startCols: 0,
        };
        var container = document.getElementById('handsontable_box');
        hot = new Handsontable(container, settingForHandsontable);
    });
</script>
