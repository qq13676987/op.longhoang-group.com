<style> .style-table {
        border-collapse: collapse;
        margin: 50px auto;
        font-size: 0.9em;
        min-width: 400px;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
    }

    .style-table thead tr {
        background-color: #0398dd;
        color: #ffffff;
        text-align: left;
    }

    .style-table th, .style-table td {
        padding: 12px 15px;
    }

    .style-table tbody tr {
        border-bottom: 1px solid #dddddd;
    }

    .style-table tbody tr:nth-of-type(even) {
        background-color: #f3f3f3;
    }

    .style-table tbody tr:last-of-type {
        border-bottom: 2px solid #0398dd;
    }

    .style-table tbody tr.active-row {
        font-weight: bold;
        color: #0398dd;
    }
</style>

    <div style="padding-left:30px;padding-right: 30px">
        <form id="ff" method="post">
            <table class="style-table">
                <thead>
                <tr>
                    <th style="width:10%;">邮件组名</th>
                    <th style="width:25%;">邮件标题</th>
                    <th style="width:10%;">更新时间</th>
                    <th style="width:10%;">跳转复制</th>
                </tr>
                </thead>
                <tbody>
                <?php if(empty($list)):?>
                    <tr>
                        <td>暂无数据</td>
                    </tr>
                <?else:?>
                <?php foreach ($list as $k => $v): ?>
                    <tr>
                        <td style="font-weight: bold;color:#5FB878"><?= $v['send_group_name'] ?></td>
                        <td style="font-weight: bold;color:#009688"><?= $v['mail_title'] ?></td>
                        <td style="color:#01AAED"><?= $v['update_time'] ?></td>

                        <?php if($controller=='add_presend'):?>
                        <td ><a style="font-weight: bold;color:#1E9FFF" href="/crm_promote_mail/add_presend/?group_id=<?= $group_id ?>&c_id=<?= $v['id'] ?>" target="_Parent">点击复制</a></td>
                        <?php elseif($controller=='add_presend_by_search_send_crm'):?>
                            <td ><a style="font-weight: bold;color:#1E9FFF" href="/crm_promote_mail/add_presend_by_search_send_crm?group_ids=<?=$group_id?>&c_id=<?= $v['id'] ?>" target="_Parent">点击复制</a></td>
                        <?php elseif($controller=='add_presend_by_search_send_client'):?>
                            <td ><a style="font-weight: bold;color:#1E9FFF" href="/crm_promote_mail/add_presend_by_search_send_client?group_ids=<?=$group_id?>&c_id=<?= $v['id'] ?>" target="_Parent">点击复制</a></td>
                        <?php elseif($controller=='out'):?>
                            <td ><a style="font-weight: bold;color:#1E9FFF" href="/crm_company_out/add_presend_by_search_send?group_ids=<?=$group_id?>&c_id=<?= $v['id'] ?>" target="_Parent">点击复制</a></td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
                <?php endif;?>
                </tbody>


            </table>
        </form>

    </div>

<script type="text/javascript" src="/inc/layer3.1.1/layer.js"></script>
<script>
    function submitForm() {
        var d = {};
        var formdata = $("#ff").serializeArray();

        $.each(formdata, function () {
            d[this.name] = this.value;
        });

        $.post("/crm_promote_mail/add_consignee_group_data", formdata, function (data) {
            if (data.code == 200) {
                layer.msg(data.msg);
                window.location.href = "/crm_promote_mail/add_consignees?group_id=" + data.id;
            } else {
                layer.msg(data.msg)
            }

        }, "JSON");
    }
</script>