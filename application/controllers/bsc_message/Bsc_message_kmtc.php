<?php

/**
 * ESL 报文相关
 */
class bsc_message_kmtc extends Common_Controller
{
     protected $is_load_session = false;
    public function __construct()
    {
        parent::__construct();
        //这里进行单独控制
        $method = $this->router->fetch_method();
        if(!in_array($method, array('create_message'))){
            $this->load->library('session');
            $this->model =  Model('bsc_message_model');
        }
    }
    

    /**
     * 生成报文 SO
     * @param $consol_id
     */
    public function message_so($consol_id, $file_function = 9){
        $bsc_message_yitong = Controller('bsc_message/bsc_message_yitong');
        $message = $bsc_message_yitong->standard_so($consol_id, $file_function,"KMTC");
        if($message === false){
            return;
        }
        //保存文件txt
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        //KMTC_EDI/日期
        $new_dir = $root_path . '/upload/message/KMTC_EDI/SO/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $consol_id . '.txt';
        $file_path = $new_dir . $file_name;
        $file_content = "\r\n" . message_format($message);
        if(preg_match('/[\x{4e00}-\x{9fa5}]+/u', $file_content, $matches1) === 1){
            echo json_encode(array('code' => 1, 'msg' => '存在中文或中文标点符号,生成失败', $matches1));
            return;
        }
        file_put_contents($file_path, $file_content);

        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => 'SO',
            'receiver' => 'KMTC',
        );
        update_message($consol_id, $message_data, 'SO');
        echo json_encode(array('code' => 0, 'msg' => '文件生成成功'));
    }
}