<?php


class Bsc_group_model extends CI_Model
{
    private $table = 'bsc_group';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_one($f = "", $v = "")
    {
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_where_one($where='', $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function get_option($company_code = "")
    {
        //限制只能看到正常的
        $this->db->where('status', 0);
        if ($company_code != "") $this->db->where('company_code', $company_code);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function total($where = "")
    {
        if ($where != "") $this->db->where($where);
        $this->db->from($this->table);

        return $this->db->count_all_results('');
    }

    public function save($data = array())
    {
        $data["created_time"] = date('Y-m-d H:i:s', time());
        $data["updated_time"] = date('Y-m-d H:i:s', time());
        $data["created_by"] = $this->session->userdata('id');
        $data["created_group"] = implode(',', $this->session->userdata('group') );
        $data["updated_by"] = $this->session->userdata('id');
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id = '', $data = array())
    {
        $data["updated_time"] = date('Y-m-d H:i:s', time());
        $data["updated_by"] = $this->session->userdata('id');
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id = '')
    {
        // delete container
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function no_role_get($where="" ,$sort="id",$order="desc"){
        if($where !="")	$this->db->where($where);
        $this->db->order_by($sort,$order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
    
    
    /**
     * 获取 当前节点的子节点
     * @param string $group_code
     */
    public function get_child_group($group_code = ''){
        $this->db->select('lft,rgt,level');
        $this_group = $this->get_where_one("group_code = '{$group_code}'");
        //查不到直接返回
        if(empty($this_group)) return array();

        $where = array();

        //2023-03-08 查询这个code的子节点
        $where[] = "lft >= {$this_group['lft']}";
        $where[] = "rgt <= {$this_group['rgt']}";
        $where[] = "level >= {$this_group['level']}";

        $where_str = join(' and ', $where);

        $this->db->where($where_str);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
}