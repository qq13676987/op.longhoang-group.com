<?php

class sys_lock_log extends Menu_Controller
{
    protected $menu_name = '';//菜单权限对应的名称
    protected $method_check = array(//需要设置权限的方法
        'index',
        'add_data',
        'update_data',
        'delete_data',
    );

    public function __construct()
    {
        parent::__construct();
        $this->model = Model('sys_lock_log_model');
    }

    public function consol_log(){
        $data = array();
        $consol_id = getValue('consol_id', 0);

        $data['consol_id'] = $consol_id;

        //获取所有相关的shipment的锁日志
        Model('biz_consol_model');
        $this->db->select('id,job_no');
        $consol = $this->biz_consol_model->get_by_id($consol_id);

        Model('biz_shipment_model');
        $this->db->select('id,job_no');
        $shipments = array_column($this->biz_shipment_model->get_shipment_by_consol($consol_id), null, 'id');
        $shipment_ids = array_column($shipments, 'id');

        $where = array();

        $where[] = "(id_type = 'biz_consol' and id_no = $consol_id)";//consol的锁日志
        if(!empty($shipment_ids)) $where[] = "(id_type = 'biz_shipment' and id_no in (" . join(',', $shipment_ids) . "))"; //shipment的锁日志

        $where_str = join(' or ', $where); // 这里是or拼接的,后续添加注意
        $this->db->select('id,(select name from bsc_user where bsc_user.id = sys_lock_log.user_id) as user_id_name,op_time,op_value,id_type,id_no');
        $rs = $this->model->get($where_str);

        $data['rows'] = array();
        foreach ($rs as $row){
            //这里将内容填充进去
            if($row['id_type'] == 'biz_consol'){
                $row['job_no'] = $consol['job_no'];
                $row['op_value'] = 'C' . $row['op_value'];
            }else{
                $row['job_no'] = $shipments[$row['id_no']]['job_no'];
                $row['op_value'] = 'S' . $row['op_value'];
            }

            $data['rows'][] = $row;
        }

        $this->load->view('head');
        $this->load->view('/sys/log/consol/lock_view', $data);
    }
}