<style type="text/css">
    .f {
        width: 115px;
    }

    .s {
        width: 100px;
    }

    .v {
        width: 200px;
    }

    .del_tr {
        width: 23.8px;
    }

    .this_v {
        display: inline;
    }
</style>
<script type="text/javascript">
    function doSearch(client_name) {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if (json.hasOwnProperty(item.name) === true) {
                if (typeof json[item.name] == 'string') {
                    json[item.name] = [json[item.name]];
                    json[item.name].push(item.value);
                } else {
                    json[item.name].push(item.value);
                }
            } else {
                json[item.name] = item.value;
            }
        });
        var url ='/biz_client/get_data?client_name=非活跃客户&import=import'; 
        if(client_name=='非活跃客户' || client_name=='活跃客户'){
            url='/biz_client/get_data?import=import&client_name='+client_name;
        }


        $('#tt').datagrid({
            url: url,
            destroyUrl: '/biz_client/delete_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            },
            queryParams: json,
        }).datagrid('clearSelections');
        set_config();
        $('#chaxun').window('close');
    }

    //上传文件操作
    function UploadFile(file_ctrlname, guid_ctrlname, div_files) {
        var value = $("#" + file_ctrlname).filebox('getValue');
        var files = $("#" + file_ctrlname).next().find('input[type=file]')[0].files;

        var guid = $("#" + guid_ctrlname).val();
        if (value && files[0]) {
            //构建一个FormData存储复杂对象
            var formData = new FormData();
            formData.append('file', files[0]);

            $.ajax({
                url: '/excel/upload_excel/client1', //单文件上传
                type: 'POST',
                processData: false,
                contentType: false,
                data: formData,
                success: function (json) {
                    //转义JSON为对象
                    var data = $.parseJSON(json);
                    $.messager.alert("info", data.msg);
                    $('#tt').edatagrid('reload');
                },
                error: function (xhr, status, error) {
                    $.messager.alert("提示", "操作失败"); //xhr.responseText
                }
            });
        }
    }

    var ajax_data = {};
    var setting = {
        'operator_ids': ['operator', 'id', 'name']
        , 'customer_service_ids': ['customer_service', 'id', 'name']
        , 'sales_ids': ['sales', 'id', 'name']
        , 'marketing_ids': ['marketing', 'id', 'name']
        , 'oversea_cus_ids': ['oversea_cus', 'id', 'name']
        , 'booking_ids': ['booking', 'id', 'name']
        , 'document_ids': ['document', 'id', 'name']
        , 'finance_ids': ['finance', 'id', 'name']
    };

    var datebox = ['created_time', 'updated_time'];

    var datetimebox = [];
    $(function () {
        //添加对话框，上传控件初始化
        $('#file_upload').filebox({
            prompt: 'Choose another file...',
            buttonText: 'choose file',  //按钮文本
            buttonAlign: 'right',   //按钮对齐
            //multiple: true,       //是否多文件方式
            //accept:'jpg|jpeg|gif|pdf|doc|docx|xlsx|xls|zip|rar'
            accept: ".gif,.jpeg,.jpg,.png,.pdf,.doc,.docx,.xlsx,.xls,.zip,.rar", //指定文件类型
        });

        $('#file_save').click(function () {
            UploadFile("file_upload", "AttachGUID", "div_files");//上传处理
        });
        var selectIndex = -1;
        $('#tt').edatagrid({
            // url: '/biz_client/get_data/<?= $role;?>?level=<?= $client_level;?>',
            // destroyUrl: '/biz_client/delete_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            }
        });

        $('#cx').on('keydown', 'input', function (e) {
            if (e.keyCode == 13) {
                doSearch();
            }
        });

        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height() - 30,
            onDblClickRow: function (rowIndex) {
                $(this).datagrid('selectRow', rowIndex);
                var row = $('#tt').datagrid('getSelected');
                if (row) {
                    url = '/biz_client/edit/' + row.id;
                    window.open(url);
                }
            },
            onClickRow: function (index, data) {
                if (index == selectIndex) {
                    $(this).datagrid('unselectRow', index);
                    selectIndex = -1;
                } else {
                    selectIndex = index;
                }
            },
            rowStyler: function (index, row) {


            }
        });
        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
        //新增查询列
        $('#cx table').on('click', '.add_tr', function () {
            var index = $('.f').length;
            var str = '<tr>' +
                '                <td>\n' +
                '                    <?= lang('query');?>\n' +
                '                </td>\n' +
                '                <td align="right">' +
                '                       <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="\n' +
                '                            onChange:function(newVal, oldVal){\n' +
                '                                var index = $(\'.f\').index(this);\n' +
                '                                if(setting.hasOwnProperty(newVal) === true){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                    $(\'.v:eq(\' + index + \')\').combobox({\n' +
                '                                        data:ajax_data[setting[newVal][0]],\n' +
                '                                        valueField:setting[newVal][1],\n' +
                '                                        textField:setting[newVal][2],\n' +
                '                                    });\n' +
                '                                }else if ($.inArray(newVal, datebox) !== -1){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datebox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }else if ($.inArray(newVal, datetimebox) !== -1){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datetimebox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }else{\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }\n' +
                '                            }\n' +
                '                        ">\n' +
                '<?php
                    foreach ($f as $rs) {
                        $item = $rs[0];
                        echo "<option value=\'$item\'>" . lang($item) . "</option>";
                    }
                    ?>' +
                '                    </select>\n' +
                '                    &nbsp;\n' +
                '\n' +
                '                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="\n' +
                '                        ">\n' +
                '                        <option value="like">like</option>\n' +
                '                        <option value="=">=</option>\n' +
                '                        <option value=">=">>=</option>\n' +
                '                        <option value="<="><=</option>\n' +
                '                        <option value="!=">!=</option>\n' +
                '                    </select>\n' +
                '                    &nbsp;\n' +
                '                    <div class="this_v">\n' +
                '                        <input name="field[v][]" class="v easyui-textbox" >\n' +
                '                    </div>\n' +
                '                    <button class="del_tr" type="button">-</button>' +
                '   </td>' +
                '</tr>';
            $('#cx table tbody').append(str);
            $.parser.parse($('#cx table tbody tr:last-child'));
            // var this_sf_f = localStorage.getItem('search_f' + $('.f:last').parents('tr').index() + '_' + path);
            // var this_sf_s = localStorage.getItem('search_s' + $('.f:last').parents('tr').index() + '_' + path);
            // if(this_sf_f != null)$('.f:last').combobox('setValue', this_sf_f);
            // if(this_sf_s != null)$('.s:last').combobox('setValue', this_sf_s);
            // localStorage.setItem('search_count_' + path, index);
            return false;
        });
        //删除按钮
        $('#cx table').on('click', '.del_tr', function () {
            var index = $(this).parents('tr').index();
            // localStorage.removeItem('search_f' + index + '_' + path);
            // localStorage.removeItem('search_s' + index + '_' + path);
            // localStorage.setItem('search_count_' + path, $('.f').length - 2);

            $(this).parents('tr').remove();
            return false;
        });
        var a1 = ajax_get('/bsc_user/get_data_role/operator', 'operator');
        var a2 = ajax_get('/bsc_user/get_data_role/customer_service', 'customer_service');
        var a3 = ajax_get('/bsc_user/get_data_role/sales', 'sales');
        var a4 = ajax_get('/bsc_user/get_data_role/marketing', 'marketing');
        var a5 = ajax_get('/bsc_user/get_data_role/oversea_cus', 'oversea_cus');
        var a6 = ajax_get('/bsc_user/get_data_role/booking', 'booking');
        var a7 = ajax_get('/bsc_user/get_data_role/document', 'document');
        var a8 = ajax_get('/bsc_user/get_data_role/finance', 'finance');
        $.ajax({
            type: 'GET',
            url: '/sys_config/get_config_search/biz_client',
            dataType: 'json',
            success: function (res) {
                // var config = $.parseJSON(res['data']);
                var config = res.data;
                if (config.length > 0) {
                    var count = config[0];
                    $('.f:last').combobox('setValue', config[1][0][0]);
                    $('.s:last').combobox('setValue', config[1][0][1]);
                    for (var i = 1; i < count; i++) {
                        $('.add_tr:eq(0)').trigger('click');
                        $('.f:last').combobox('setValue', config[1][i][0]);
                        $('.s:last').combobox('setValue', config[1][i][1]);
                    }
                }
            }
        });

        $.when(a1, a2, a3, a4, a5, a6, a7, a8).done(function () {
            $.each($('.f.easyui-combobox'), function (ec_index, ec_item) {
                var ec_value = $(ec_item).combobox('getValue');
                $(ec_item).combobox('clear').combobox('select', ec_value);
            });
        });

        //2022-05-23 新增多选
        <?php if(!empty($import)):?>
        multiple();
        <?php endif;?>

        //2022-05-26 新增
        $('#tabs').tabs({
            border: false,
            onSelect: function (title) {
                console.log(2);
                if (title == '全部') {
                    doSearch('全部');
                }
                if (title == '供应商') {
                    doSearch('供应商');
                }
                if (title == '非活跃客户') {
                    doSearch('非活跃客户');
                }
                if (title == '活跃客户') {
                    doSearch('活跃客户');
                }
            }
        });

    });

    //2022-05-23 新增多选
    function multiple() {
        var opt = $('#tt').datagrid('options');
        opt.singleSelect = !opt.singleSelect;

        $('#tt').datagrid('reload');
    }

    function ajax_get(url, name) {
        return $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function (res) {
                ajax_data[name] = res;
            },
            error: function () {
                // $.messager.alert('获取数据错误');
            }
        })
    }

    function set_config() {
        var f_input = $('.f');
        var count = f_input.length;
        var config = [];
        $.each(f_input, function (index, item) {
            var c_array = [];
            c_array.push($('.f:eq(' + index + ')').combobox('getValue'));
            c_array.push($('.s:eq(' + index + ')').combobox('getValue'));
            config.push(c_array);
        });
        // var config_json = JSON.stringify(config);
        $.ajax({
            type: 'POST',
            url: '/sys_config/save_config_search',
            data: {
                table: 'biz_client',
                count: count,
                config: config,
            },
            dataType: 'json',
            success: function (res) {

            },
        });
    }

    function add() {
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            window.open("/biz_client/add");
            // window.open("/biz_client/add/" + row.id);
        } else {
            window.open("/biz_client/add");
        }
    }


    function client_apply() {
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            window.open("/biz_client_apply/add");
            // window.open("/biz_client/add/" + row.id);
        } else {
            window.open("/biz_client_apply/add");
        }
    }

    function client_confirm() {
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            window.open("/biz_client_apply/index");
            // window.open("/biz_client/add/" + row.id);
        } else {
            window.open("/biz_client_apply/index");
        }
    }

    function config() {
        window.open("/biz_client/config_title/");
    }

    function del() {
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            //if(row.step!="未收款"){
            //	alert("不可以删除");
            //}else{
            //$('#tt').edatagrid('destroyRow');
            //}
            //增加一个手动输入的delete验证
            $.messager.prompt('Tips', '确认删除?请输入delete进行确认', function (r) {
                if (r === 'delete') {
                    $('#tt').edatagrid('destroyRow');
                }
            });
        } else {
            alert("No Item Selected");
        }
    }

    function import_excel() {
        $('#import').window('open');
    }

    function client_code_styler(value, row, index) {
        var this_style = '';
        //供应商类1个颜色   #8dffea9c;  
        //warehouse  truck  declaration  carrier  oversea_agent  booking_agent
        var supplier = <?=json_encode($supplier)?>;
        //客户类1个颜色   #d88dff63;     
        //client  factory  logistics_client  booking_agent  oversea_client
        var client = <?=json_encode($client)?>;
        if (row.role === undefined) return '';
        var role = row.role.split(',');
        //同时拥有以上2者的属于深度合作类，用下面颜色

        //深度类， 2者都有的颜色 #8f8dffbf;
        var this_role_type = '';//当前角色类型
        var this_style = '';//当前样式
        //循环判断当前角色属于什么类型
        $.each(role, function (i, it) {
            if (this_role_type != 'supplier' && $.inArray(it, supplier) !== -1) {
                //如果没有类型
                if (this_role_type == '') this_role_type = 'supplier';
                //如果已经是客户类,且满足供应商,变为深度
                if (this_role_type == 'client') {
                    this_role_type = 'deep_client';
                    return false;
                }
            } else if (this_role_type != 'client' && $.inArray(it, client) !== -1) {
                //如果没有类型
                if (this_role_type == '') this_role_type = 'client';
                //如果已经是供应商类,且满足客户,变为深度
                if (this_role_type == 'supplier') {
                    this_role_type = 'deep_client';
                    return false;
                }
            }
        });
        if (this_role_type == 'client') {
            //客户类1个颜色   #d88dff63; 
            this_style += 'background-color: #d88dff63';
        } else if (this_role_type == 'supplier') {
            //供应商类1个颜色   #8dffea9c;  
            this_style += 'background-color: #8dffea9c';
        } else if (this_role_type == 'deep_client') {
            //深度类， 2者都有的颜色 #8f8dffbf;
            this_style += 'background-color: #8f8dffbf';
        }
        return this_style;
    }

    function selectAll() {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if (json.hasOwnProperty(item.name) === true) {
                if (typeof json[item.name] == 'string') {
                    json[item.name] = [json[item.name]];
                    json[item.name].push(item.value);
                } else {
                    json[item.name].push(item.value);
                }
            } else {
                json[item.name] = item.value;
            }
        });

        //清空显示的查询条件
        $('#query').html('');

        var cx_data2 = cx_data;
        $.each(cx_data2, function (index, item) {
            if (item.name == 'f_role') {
                if (item.value != '') {
                    $("#query").append('<span class="query_f">' + item.value + '</span>')
                }
            }
            //判断如果name存在,且为string类型
            if (item.name == 'field[name][]') {
                $("#query").append('<span class="query_f">' + item.value + '</span>')
            }
            if (item.name == 'field[s][]') {
                $("#query").append('<span class="query_s">' + item.value + '</span>')
            }
            if (item.name == 'field[v][]') {
                $("#query").append('<span class="query_v">' + item.value + '</span>')
            }

        });

        json.getType = 1;
        json.page = 1;
        json.rows = 20000;
        var opt = $('#tt').datagrid('options');
        $('#select_ids').val('');

        if(!opt.url){
            return $.messager.alert('<?= lang('Tips')?>', '请查询后再试');
        }
        ajaxLoading();

        var title = [];
        $.each(cx_data, function (index, item) {
            if (item.name == 'crm_user') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
            if (item.name == 'crm_user_type') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
            if (item.name == 'f_role') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
            if (item.name == 'field[v][]') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
        });
        ids = [];
        $.ajax({
            url: opt.url,
            type:'post',
            data: json,
            success:function (res_json) {
                var res;
                ajaxLoadEnd();
                try {
                    res = $.parseJSON(res_json);
                    if (res.code == 0 && res.rows.length > 0) {

                        $.each(res.rows, function (index, item) {
                            ids[index] = item.id;
                        });
                        $.messager.confirm('提示', '是否确认不勾全导入?', function (r) {
                            if(r){
                                if (ids.length < 1) {
                                    alert('未获取到可选择的客户');
                                }
                                var ids_str = ids.join(',');
                                post('/crm_promote_mail/add_presend_by_search_send_client', {ids: ids_str, title: title});
                            }
                        })
                    } else {

                    }
                } catch (e) {
                    $.messager.alert('Tips', res_json);
                }
            }
        });
    }

    //2022-05-23 新增多选
    function import_crm() {
        //获取查询条件
        var cx_data = $('#cx').serializeArray();
        var title = [];
        $.each(cx_data, function (index, item) {
            if (item.name == 'f_role') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
            if (item.name == 'field[v][]') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
        });

        //获取选中id
        var tt = $('#tt');
        var rows = tt.datagrid('getSelections');
        var ids = [];
        $.each(rows, function (i, it) {
            ids.push(it.id);
        });

        if (ids.length < 1) {
            alert('暂无选中，请选择客户');
        }

        post('/crm_promote_mail/add_presend_by_search_send_client', {ids: ids, title: title});
    }

    //表单模拟post
    function post(URL, PARAMS) {
        var temp_form = document.createElement("form");
        temp_form.action = URL;
        temp_form.target = "_blank";
        temp_form.method = "post";
        temp_form.style.display = "none";
        for (var x in PARAMS) {
            var opt = document.createElement("textarea");
            opt.name = x;
            opt.value = PARAMS[x];
            temp_form.appendChild(opt);
        }
        document.body.appendChild(temp_form);
        temp_form.submit();
    }
    
    function userLoadSuccess(node, data) {
        //这里进行循环操作,当找到第一个is_use == true时,选中
        var val = $('#crm_user').val();
        if (val == '') {
            var result = searchIsuse(data);
            $('#user').combotree('setValue', result);
            $('#db_fm').submit();
        }
    }

    /**
     * 查找到最大的第一个可使用的
     * @param data
     */
    function searchIsuse(data) {
        var result = '';
        //循环一遍,查看第一层结构是否有可使用的
        var new_data = [];
        $.each(data, function (i, it) {
            if (it.is_use == true) {
                result = it.id;
                return false;
            }
            new_data.push(it);
        });
        if (result != '') return result;
        //如果没找到,这里循环再递归一下
        $.each(new_data, function (i, it) {
            result = searchIsuse(it.children);
            if (result != '') return result;
        })

        return result;
    }



</script>

<?php if (!empty($import)): ?>
    <div style="display:inline-block">
        <h1> 1、请先勾选需要群发的客户 </h1>
    </div>
    <div style="display:inline-block;padding-left: 30px">
        <a href="javascript:void(0);" class="easyui-linkbutton" onclick="import_crm()">通过勾选导入邮箱群发</a>
        <a href="javascript:void(0);" class="easyui-linkbutton" onclick="selectAll()">直接全部导入邮箱群发</a>
    </div>
<?php endif; ?>
<div id="tabs" class="easyui-tabs" style="width:100%;height:29px;">
    <?php if(empty($_GET["import"])){?>
    <div title="全部"></div>
    <div title="供应商"></div>
    <?php }?>
    <div title="非活跃客户"></div>
    <div title="活跃客户"></div>
</div>
<table id="tt" style="width:1100px;height:450px" rownumbers="false" pagination="true" idField="id" pagesize="1000"
       toolbar="#tb" singleSelect="true" nowrap="true">
    <thead>
    <tr>
        <?php if(!empty($import)):?>
            <th title="全选" field="ck" checkbox="true"></th>
        <?php endif;?>
 
                <th data-options="field:'last_deal_time',width:150">
                    最近成交日期
                </th>
                <th field="id" width="80" sortable="true">
                    ID
                </th>
                <th data-options="field:'client_code',width:100,sortable:true,styler:client_code_styler">
                    客户代码
                </th>
                <th field="client_name" width="100">
                    客户简称
                </th>
                <th field="company_name" width="120">
                    客户全称
                </th>
                <th field="sales_ids" width="100">
                    销售
                </th>
                <th field="customer_service_ids" width="100">
                    客服
                </th>
                <th field="sailing_area" width="120">
                    航线区域
                </th>
                <th field="company_address" width="120">
                    客户地址
                </th>  
                <th field="finance_sign_company_code" width="120">
                    签约子公司
                </th> 
                <th field="website_url" width="120">
                    企业网址
                </th>
                <th field="source_from" width="120">
                    信息来源
                </th>
                <th field="role" width="120">
                    角色
                </th>
                <th field="remark" width="120">
                    备注
                </th>
                <th field="created_by" width="150">
                    创建人
                </th>
                <th field="created_time" width="150">
                    创建时间
                </th>
                <th field="updated_by" width="150">
                    修改人
                </th>
                <th field="updated_time" width="150">
                    修改时间
                </th>
        <?php
        /*
        if (!empty($f)) {
            foreach ($f as $rs) {

                $field = $rs[0];
                $disp = $rs[1];
                $width = $rs[2];

                if ($width == "0") continue;
                if ($field == "id") {
                    echo "<th field=\"$field\" width=\"$width\" sortable=\"true\">" . lang($disp) . "</th>";
                    continue;
                }
                if ($field == 'client_code') {
                    echo "<th data-options=\"field:'{$field}',width:{$width},sortable:true,styler:client_code_styler\">" . lang($disp) . "</th>";
                    continue;
                }
                echo "<th field=\"$field\" width=\"$width\">" . lang($disp) . "</th>";
            }
        }*/
        ?>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
                   onclick="add();"><?= lang('add'); ?></a>-->
                <?php echo $hasDelete ? '<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="del();"> ' . lang('delete') . '</a>' : ''; ?>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true"
                   onclick="javascript:$('#chaxun').window('open');"><?= lang('query'); ?></a>
             <!--   <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm2',plain:true"
                   iconCls="icon-reload"><?/*= lang('config'); */?></a>
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm3',plain:true"
                   iconCls="icon-reload"><?/*= lang('client apply'); */?></a>
                <a href="javascript:void;" class="easyui-tooltip tooltip-f" data-options="
                    content: $('<div></div>'),
                    onShow: function(){
                        $(this).tooltip('arrow').css('left', 20);
                        $(this).tooltip('tip').css('left', $(this).offset().left);
                    },
                    onUpdate: function(cc){
                        cc.panel({
                            width: 300,
                            height: 'auto',
                            border: false,
                            content: '粉色代表客户类 <br/> 紫色代表双向合作类<br/> 绿色代表供应商类',
                        });
                    }
                " title=""><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                <a href="/biz_client_contact/user_list" class="easyui-linkbutton" iconCls="icon-reload" plain="true"
                   target="_blank"><?/*= lang('前台客户管理'); */?></a>-->
                <?php if (!empty($import)): ?>
                    <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="multiple()">多选</a>
                <?php endif; ?>
                <!--href: '/bsc_help_content/help/client_index'-->
            </td>
        </tr>
    </table>
</div>

<div id="mm2" style="width:150px;">
    <div data-options="iconCls:'icon-ok'" onclick="javascript:config()"><?= lang('column'); ?></div>
</div>

<div id="mm3" style="width:150px;">
    <div data-options="iconCls:'icon-ok'" onclick="client_apply()"><?= lang('client apply'); ?></div>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="false" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <form id="cx">
        <table>
            <tr>
                <td>部门</td>
                <td>
                    <select class="easyui-combotree" id="user" style="width:475px;" data-options="
                        valueField:'id',
                        textField:'text',
                        cascadeCheck: false,
                        url:'/bsc_user/get_user_role_tree',
                        onBeforeSelect: function(node){
                            var inp = $('#user');
                            var roots = inp.combotree('tree').tree('getRoots');
                            var result = true;
                            if(result){
                                if(!node.is_use){
                                    return false;
                                }
                                $('#crm_user').val(node.id);
                                $('#crm_user_type').val(node.type);
                            }
                            return result;
                        },
                        onBeforeLoad: function(node, param){
                            //不知道为啥,会自动加载,这里如果不是null直接断掉,后面发现再改成常规代码
                            if(node !== null) return false;
                        },
                        onLoadSuccess:userLoadSuccess,
                    "></select>
                    <input type="hidden" name="crm_user" id="crm_user" value="">
                    <input type="hidden" name="crm_user_type" id="crm_user_type" value="">
                </td>
            </tr>
            <tr>
                <td><?= lang('未合作天数');?></td>
                <td>
                    <input class="easyui-numberspinner" name="whzts" data-options="min:0" style="width: 475px;">
                </td>
            </tr>
            <tr>
                <td>
                    <?= lang('role') ?>
                </td>
                <td align="right">
                    <select class="easyui-combobox" id="f_role" name="f_role" style="width: 475px"
                            data-options="
                        valueField:'value',
                        textField:'value',
                        multiple:true,
                        url:'/bsc_dict/get_option/role',
                    ">

                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('销售多选');?></td>
                <td>
                    <select class="easyui-combobox" id="quit_sales" name="quit_sales" style="width: 475px" data-options="
                        valueField:'id',
                        textField:'name',
                        multiple:true,
                        url:'/bsc_user/get_data_role/sales',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <?= lang('query'); ?>
                </td>
                <td align="right">
                    <select name="field[f][]" class="f easyui-combobox" required="true" data-options="
                            onChange:function(newVal, oldVal){
                                var index = $('.f').index(this);
                                if(setting.hasOwnProperty(newVal) === true){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                    $('.v:eq(' + index + ')').combobox({
                                        data:ajax_data[setting[newVal][0]],
                                        valueField:setting[newVal][1],
                                        textField:setting[newVal][2],
                                    });
                                }else if ($.inArray(newVal, datebox) !== -1){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datebox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }else if ($.inArray(newVal, datetimebox) !== -1){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datetimebox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }else{
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }
                            }
                        ">
                        <?php
                        foreach ($f as $rs) {
                            $item = $rs[0];
                            echo "<option value='$item'>" . lang($item) . "</option>";
                        }
                        ?>
                    </select>
                    &nbsp;

                    <select name="field[s][]" class="s easyui-combobox" required="true" data-options="
                        ">
                        <option value="like">like</option>
                        <option value="=">=</option>
                        <option value=">=">>=</option>
                        <option value="<="><=</option>
                        <option value="!=">!=</option>
                    </select>
                    &nbsp;
                    <div class="this_v">
                        <input name="field[v][]" class="v easyui-textbox">
                    </div>
                    <button class="add_tr" type="button">+</button>
                </td>
            </tr>
        </table>
        <br><br>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;"
                onclick="doSearch();"><?= lang('search'); ?></button>
        <a href="javascript:void;" class="easyui-tooltip" data-options="
            content: $('<div></div>'),
            onShow: function(){
                $(this).tooltip('arrow').css('left', 20);
                $(this).tooltip('tip').css('left', $(this).offset().left);
            },
            onUpdate: function(cc){
                cc.panel({
                    width: 500,
                    height: 'auto',
                    border: false,
                    href: '/bsc_help_content/help/query_condition'
                });
            }
        "><?= lang('help'); ?></a>
    </form>
</div>
<div id="import" class="easyui-dialog" style="background-color: #F4F4F4;border:1px solid #95B8E7;margin-top:10px;"
     data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons',title:'<?= lang('Import'); ?>'">
    <table>
        <tr>
            <td>
                <input class="easyui-filebox" name="file" id="file_upload"
                       data-options="" style="width:400px">
            </td>
        </tr>
    </table>
    <div id="dlg-buttons">
        <a class="easyui-linkbutton" id="file_save" iconCls="icon-ok" style="width:90px"><?= lang('save'); ?></a>
    </div>
    <div id="div_files"></div>
</div>

<input type="hidden" id="select_ids">