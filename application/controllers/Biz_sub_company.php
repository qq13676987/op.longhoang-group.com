<?php

class Biz_sub_company extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_sub_company_model');
        $this->model = $this->biz_sub_company_model;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }


    public $field_edit = array();

    public $field_all = array(
        array('id', 'id', '80', '1','sortable="true"'),
        array('company_code', 'company_code', '100', '3', ''),
        array('company_name', 'company_name', '100', '4', ''),
        array('created_time', 'create_time', '150', '8', 'sortable="true"'),
        array('updated_time', 'update_time', '150', '10', ''),
    );

    public function index()
    {
        if(!is_admin()){
            exit('无权查看');
            return;
        }
        $data = array();

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_sub_company_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }
        // page account
        $rs = $this->sys_config_model->get_one('biz_sub_company_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }

        $this->load->view('head');
        $this->load->view('biz/sub_company/index_view', $data);
    }

    public function get_data()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = array();
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($v1 != "") $where[] = "$f1 $s1 '$v1'";
        if ($v2 != "") $where[] = "$f2 $s2 '$v2'";
        if ($v3 != "") $where[] = "$f3 $s3 '$v3'";
        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $rs = $this->model->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data($client_code = "")
    {
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($temp != "")
                $data[$item] = $temp;
        }
        $id = $this->model->save($data);
        $data['id'] = $id;

        echo json_encode($data);
        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_sub_company";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function update_data()
    {
        $id = $_REQUEST["id"];
        $old_row = $this->model->get_one('id', $id);

        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($old_row[$item] != $temp)
                $data[$item] = $temp;
        }
        if(isset($data['company_code']))unset($data['company_code']);
        $id = $this->model->update($id, $data);
        $data['id'] = $id;
        echo json_encode($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_sub_company";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->model->get_one('id', $id);
        $this->model->mdelete($id);
        echo json_encode(array('success' => true));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_sub_company";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
    }

	public function get_option($type=1,$company_code = '')
	{
		if($type == 1){
			$this->db->select('company_code, company_name,company_title');
			$this->db->where('is_signed = 1');
			$this->db->order_by('id','asc');
			$data = $this->model->get();
		}elseif($type == 2){
			$data = $this->model->get_one('company_code', $company_code);
		}

		echo json_encode($data);
	}

    public function code_isset(){
        $code = isset($_REQUEST['company_code']) ? $_REQUEST['company_code'] : '';
        if($code == ''){
            echo json_encode(array('code' => 1, 'msg' => 'error'));
            return;
        }
        $data = $this->model->get_one('company_code', $code);
        if(empty($data)){
            echo json_encode(array('code' => 2, 'msg' => 'not found'));
        }else{
            echo json_encode(array('code' => 0, 'msg' => 'Already exists'));
        }
    }
}
