<?php


class bsc_upload_model extends CI_Model
{
    private $table = 'bsc_upload';
    public function __construct()
    {
        $this->load->database();
    }

    public function get_one($f = "", $v = "")
    {
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_one_where($where)
    {
        if ($where != '') $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_by_id_no($biz_table = "", $id_no = '')
    {
        $this->db->where('biz_table', $biz_table);
        $this->db->where('id_no', $id_no);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function save($data = array())
    {
        $data["create_time"] = date('Y-m-d H:i:s', time());
        $data["update_time"] = date('Y-m-d H:i:s', time());
        $data["create_by"] = $this->session->userdata('id');
        $data["update_by"] = $this->session->userdata('id');
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();

    }
    
    public function save_auto($data = array(), $user_id = 0)
    {
        $data["create_time"] = date('Y-m-d H:i:s', time());
        $data["update_time"] = date('Y-m-d H:i:s', time());
        $data["create_by"] = $user_id;
        $data["update_by"] = $user_id;
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();

    }

    public function update($id = '', $data = array())
    {
        $data["update_time"] = date('Y-m-d H:i:s', time());
        $data["update_by"] = $this->session->userdata('id');
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id = '')
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function delete_by_file_name($file_name = '')
    {
        $this->db->where('file_name', $file_name);
        $this->db->delete($this->table);

    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function total($where = "")
    {
        if ($where != "") $this->db->where($where);
        $this->db->from($this->table);
        return $this->db->count_all_results('');
    }

    /**
     * 获取上架的文件
     * @param string $where
     * @return mixed
     */
    public function get_approve_file($where = '')
    {
        $this->db->select('id,file_name');

        $this->db->where('approve', 1);
        if ($where != '') $this->db->where($where);

        $query = $this->db->from($this->table);
//           return $query->result_array();
        return $this->db->last_query();
    }
}