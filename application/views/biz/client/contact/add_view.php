<title><?= lang('添加联系人'); ?></title>
<body style="padding: 10px 20px;">
<div style="width: 100%">
    <form id="myform" name="myform" method="post">
        <input type="hidden" name="id" id="id" value="<?=isset($_GET['id'])?(int)$_GET['id']:'0';?>">
        <table>
            <tr style="height: 30px;">
                <td style="width: 60px;"><?= lang('角色'); ?>：</td>
                <td>
                    <select id="role" name="role" class="easyui-combobox" style="width: 280px; height: 25px;"
                            data-options="
                                    required:true,
                                    multiple:false,
                                    editable:false,
    								valueField:'value',
    								textField:'value',
    								url:'/bsc_dict/get_option/role',
    								value:'',
								">
                    </select>
                </td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('姓名'); ?>：</td>
                <td><input class="easyui-textbox" type="text" required="required" name="name" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('telephone'); ?>：</td>
                <td><input class="easyui-textbox" type="text" name="telephone" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('telephone2'); ?>：</td>
                <td><input class="easyui-textbox" type="text" name="telephone2" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('email'); ?>：</td>
                <td><input class="easyui-textbox" type="text" required="required" name="email" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('address'); ?>：</td>
                <td><input class="easyui-textbox" type="text" name="address" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('QQ'); ?>：</td>
                <td><input class="easyui-textbox" type="text" name="live_chat" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('职位'); ?>：</td>
                <td>
                    <input class="easyui-combobox" type="text" name="position" value="" style="width: 280px; height: 25px;"
                           data-options="
                           valueField: 'value',
                           textField: 'name',
                           url:'/bsc_dict/get_option/contact_position',"
                    />
                </td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('航线'); ?>：</td>
                <td>
                    <input class="easyui-combobox" type="text" name="sailing" value="" style="width: 280px; height: 25px;"
                           data-options="
                           valueField: 'value',
                           textField: 'name',
                           url:'/bsc_dict/get_option/export_sailing',"
                    />
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('remark'); ?>：</td>
                <td><input class="easyui-textbox" type="text" name="remark" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td></td>
                <td align="center">
                    <input type="hidden" name="is_submit" value="1">
                    <a href="javascript:void(0);" id="save_data" class="easyui-linkbutton" style="width: 280px;height:30px;margin-top: 10px;"><?= lang('save'); ?></a>
                </td>
            </tr>
        </table>
    </form>
</div>
<link type="text/css" href="/inc/css/wangeditor/style.css" rel="stylesheet">
<script type="text/javascript" src="/inc/js/wangeditor/index.js" ></script>
<script type="text/javascript" src="/inc/js/wangeditor/dist/index.js" ></script>
<script>
    $(function () {
        //关闭当前弹窗
        <?php if (empty($client)):?>
        parent.$.messager.alert('<?= lang('提示'); ?>', '<?= lang('客户不存在!'); ?>');
        setTimeout(function () {
            parent.$('#_window').window('close');
        }, 3000);
        <?php else: ?>

        //异步写入数据
        $('#save_data').click(function () {
            var field = {};
            $.each($('#myform').serializeArray(), function (index, item) {
                field[item.name] = item.value;
            });

            $.ajax({
                url: '/biz_client_contact/add/?client_code=<?=isset($client['client_code'])?$client['client_code']:"";?>',
                type: 'POST',
                data: field,
                dataType: 'json',
                success: function (data) {
                    if (data.code == 1){
                        //刷新父页面的列表控件
                        parent.$('#tt').edatagrid('reload');

                        $.messager.show({
                            title:'<?= lang('提示'); ?>',
                            msg:'<?= lang('保存成功'); ?>',
                            timeout:2000,
                            showType:null,
                            style:{
                                right:'',
                                bottom:''
                            }
                        });

                        setTimeout(function () {
                            //关闭当前弹窗
                            parent.$('#_window').window('close');
                        }, 2000);

                    }else{
                        $.messager.alert("<?= lang('提示'); ?>", '<?= lang('保存失败'); ?>：'+data.msg, 'error');
                    }
                },
                error: function (xhr, status, error) {
                    $.messager.alert("<?= lang('提示'); ?>", "<?= lang('操作失败'); ?>"); //xhr.responseText
                }
            });
        });
        <?php endif; ?>
    })
</script>
</body>
</html>