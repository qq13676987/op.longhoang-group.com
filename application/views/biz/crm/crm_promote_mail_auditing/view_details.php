<div style="padding:10px 40px 10px 40px">
    <form id="ff" method="post">
        <table cellpadding="5">
            <tr>
                <td>收件人组:</td>
                <td> 
                   <a href="/crm_promote_mail/group_consignee_list?group_id=<?php echo $send_group_id;?>" target="_blank">
                    <?php echo $send_group_name ?>
                    </a>
                </td>
            </tr>
            <tr>
                <td>邮件标题:</td>
                <td><?php echo $mail_title ?></td>
            </tr>
            <tr>
                <td>发送人名称:</td>
                <td><?php echo $sender_name ?></td>
            </tr>
            <tr>
                <td>邮件正文:</td>
                <td>
                    <div id="add_content_div">
                        <?php echo $mail_content ?>
                    </div>

                </td>
            </tr>
            <tr>
                <td>操作:</td>
                <td>
                    <input type="radio" name="opt" checked="checked" value="pass">通过并发送
                    <input type="radio" name="opt" value="refuse">拒绝

                </td>
            </tr>
            <tr>
                <td>拒绝原因:</td>
                <td><input class="easyui-textbox" name="reason" data-options="multiline:true" style="height:60px"/></td>
            </tr>

        </table>
        <input name="id" value="<?php echo $id ?>" type="hidden">
    </form>
    <div style="text-align:center;padding:5px">
        <a href="javascript:void(0)" style="padding: 0 16px 0 16px" class="easyui-linkbutton"
           onclick="doIt(<?php echo $id ?>)">执行</a>
    </div>
</div>
<script>
    function doIt(id) {
        layer.confirm('您确定执行？', {
            btn: ['确定', '取消'] //按钮
        }, function () {
            layer.closeAll('dialog');
            pst();

        });
    }

    function pst() {
        var d = {};
        var formdata = $("#ff").serializeArray();

        $.each(formdata, function () {
            d[this.name] = this.value;
        });

        $.post("/crm_promote_mail_auditing/do_it", formdata, function (data) {
            if (data.code == 200) {
                layer.msg(data.msg);
                $('#dg').datagrid('reload');
                $('#w').window('close')

            } else if (data.code == 201) {
                layer.msg(data.msg);
                $('#dg').datagrid('reload');
                $('#w').window('close')
            } else {

                layer.msg(data.msg);

            }

        }, "JSON");
    }


</script>
