<style type="text/css">
    .user_group tr>td:first-child{
        width: 80px;
    }
    .user_group tr>td:nth-child(2){
        width: 350px;
    }
    .user_group tr>td:nth-child(3){
        width: 80px;
    }
    #image img{
        width: 100px;
        height: 100px;
        margin-left: 30px;
        margin-top: 10px;
        border: 1px solid black;
    }
    .show_if{
        display: none;
    }

    .ladder_btn{
        width: 23px;
        height: 23px;
    }
    .table_left{
        width: 442px;
        height: 100%;
    }
    .table_right{
        width: 1000px;
        height: 100%;
    }
    .table_body{
        height: 100vh;
        display: flex;
        flex-direction: row;
    }
    .user_role_iframe{
        width:1200px;
        height: 100%;
    }
    /*    ::-webkit-scrollbar { width: 0 !important }
        html{overflow: -moz-scrollbars-none;}*/
</style>
<div id="cc" class="easyui-layout" style="width:100%;height:800px;">
    <div  data-options="region:'west',title:'<?= lang('user group');?>',split:true,tools:'#add_btn'" style="width:230px;">
        <!--<select class="easyui-combobox" id="search_group" style="width: 90%;" prompt="user group" data-options="
            valueField:'code',
            textField:'name',
            onSelect: function(rec){
                var node = $('#tt').tree('find', rec.code);
                $('#tt').tree('collapseAll');
                $('#tt').tree('expandTo', node.target);
            }
        ">
        </select>
        <br/>-->
        <input class="easyui-textbox" id="search_user" style="width: 95%;" prompt="user name">
        <div class="easyui-panel" style="padding:5px">
            <ul id="tt"></ul>
        </div>
        <div id="add_btn">
        </div>
    </div>
    <div class="user_group" data-options="region:'center',title:'<?= lang('user info');?>',tools:'#edit_btn'" style="padding:5px;">
        <form id="user_form" method="post" > 
            <div class="table_body">
                <div class="table_left">
                    <table width="450" border="0" cellspacing="0">
                        <tr>
                            <td><?= lang('状态');?></td>
                            <td>
<!--                                <input type="text" class="easyui-textbox" style="width:120px;" name="title" id="title" >-->
                                <input type="text" class="easyui-textbox" style="width:80px;" name="id" id="id" readonly>
                                <select name="status" id="status" class="easyui-combobox" readonly style="width: 215px">
                                    <option value="0"><?= lang('Normal');?></option>
                                    <option value="1"><?= lang('Disable');?></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('姓名');?></td>
                            <td> 
                                <input type="text" class="easyui-textbox" style="width:155px;" readonly name="name" id="name" >
                            <a href='http://wenda.leagueshipping.com/?/question/859' target="_blank" title="点击看级别说明">级别</a>：<input name="m_level" id="m_level" readonly class="easyui-numberspinner" value="3" data-options="min:3,max:20,prefix:'M'" style="width:105px;">
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('职位');?></td>
                            <td><input type="text" class="easyui-textbox" readonly style="width:300px;" name="job_title" id="job_title" ></td>
                        </tr>
                        <tr>
                            <td><?= lang('密码');?></td>
                            <td><input type="text" class="easyui-textbox" readonly style="width:300px;" name="password" id="password" ></td>
                        </tr>
                        <!--<tr>-->
                        <!--    <td><?= lang('公司');?></td>-->
                        <!--    <td>-->
                        <!--        <select class="easyui-combobox" style="width:300px;" name="company" id="company" editable="true" data-options="-->
                        <!--    -->
                        <!--    valueField:'value',-->
                        <!--    textField:'value',-->
                        <!--    url:'/bsc_dict/get_option/hbl_type',-->
                        <!--">-->
                        <!--        </select>-->
                        <!--    </td>-->
                        <!--</tr>-->
                        <tr>
                            <td><?= lang('手机');?></td>
                            <td><input type="text" class="easyui-textbox" readonly  style="width:40px;" name="telephone_area_code" id="telephone_area_code" value="" > - <input type="text" readonly class="easyui-textbox"  style="width:250px;" name="telephone" id="telephone" ></td>
                        </tr>
                        <tr>
                            <td><?= lang('邮箱');?></td>
                            <td><input type="text" class="easyui-textbox" readonly style="width:300px;" name="email" id="email"></td>
                        </tr>
                        <tr>
                            <td><?= lang('qq');?></td>
                            <td><input type="text" class="easyui-textbox" readonly style="width:300px;" name="qq" id="qq" ></td>
                        </tr>
                        <tr>
                            <td><?= lang('skype');?></td>
                            <td><input type="text" class="easyui-textbox" readonly style="width:300px;" name="skype" id="skype" ></td>
                        </tr>
                        <tr>
                            <td><?= lang('Group');?></td>
                            <td>
                                <select id="group" name="group" class="easyui-combobox" readonly style="width:300px;" editable="true" data-options="
                            valueField:'code',
                            textField:'name',
                            url:'/bsc_group/get_option',
                            onLoadSuccess: function(){
                                var data = $(this).combobox('getData');
                                $('#search_group').combobox('loadData', data);
                            },
                            ">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="javascript:void;" class="easyui-tooltip" readonly style="color:violet;font-weight:bold;" data-options="content: function(){
                                    var str = '<div>';
                                    str += '岗位角色用于判断该用户是否出现shipment界面里岗位选项里';
                                    str += '</div>';
                                    return str;
                                },onShow: function(){$(this).tooltip('arrow').css('left', 20);$(this).tooltip('tip').css('left', $(this).offset().left); },"><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                                <?= lang('user_role');?>
                            </td>
                            <td>
                                <select class="easyui-combobox" style="width:300px;" readonly name="user_role" id="user_role" editable="false" data-options="
                                    multiple:true,
                                    valueField:'value',
                                    textField:'name',
                                    url:'/bsc_dict/get_option/user_role', 
                                ">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="javascript:void;" class="easyui-tooltip" style="color:violet;font-weight:bold;" data-options="content: function(){
                                    var str = '<div>';
                                    str += '岗位权限用于控制该用户的访问权限';
                                    str += '</div>';
                                    return str;
                                },onShow: function(){$(this).tooltip('arrow').css('left', 20);$(this).tooltip('tip').css('left', $(this).offset().left); },"><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                                <?= lang('岗位权限');?>
                            </td>
                            <td>
                                <select class="easyui-combotree" style="width:300px;" name="station" id="station" editable="false" data-options="
                                    multiple:true,
                                    url:'/bsc_user_role/get_tree',
                                    onChange:function(newValue, oldValue){
                                        station_select(newValue);
                                    },
                                    
                                    cascadeCheck: false,
                                ">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('Group range');?></td>
                            <td>
                                <select class="easyui-combotree default_partner" readonly style="width:300px;" name="group_range" id="group_range" ids="group_range" editable="true" data-options="
                                        multiple:true,
                                        valueField:'id',
                                        textField:'text',
                                        url:'/bsc_user/getAllCompanyTree',
                                        value: '',
                                    ">
                                </select>
                            </td>
                        </tr>
                        <tr style="display:none">
                            <td>
                                <?= lang('User range');?>
                            </td>
                            <td>
                                <select id="user_range" class="easyui-combogrid" readonly name="user_range" style="width:300px;" >
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('直属领导');?></td>
                            <td>  
                                <select id="leader_id" name="leader_id" readonly class="easyui-combobox" style="width:200px;" editable="true" data-options="
                                    valueField:'id',
                                    textField:'name',
                                    url:'/bsc_user/get_option', 
                                    ">
                                </select>
                                <a href="javascript:void;" class="easyui-tooltip" readonly style="color:violet;font-weight:bold;" data-options="content: function(){
                                    var str = '<div>';
                                    str += '一旦设为助理，这个用户就会复制其直属领导的数据范围权限！';
                                    str += '</div>';
                                    return str;
                                },onShow: function(){$(this).tooltip('arrow').css('left', 20);$(this).tooltip('tip').css('left', $(this).offset().left); },"><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                                <?= lang('助理');?>
                                <select name="assistant_flag" id="assistant_flag" readonly class="easyui-combobox" style="width: 50px">
                                    <option value="0"><?= lang('否');?></option>
                                    <option value="1"><?= lang('是');?></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('special_text');?></td>
                            <td>
                                <select id="special_text" name="special_text" readonly class="easyui-combobox" style="width:300px;" editable="false" data-options="
                                multiple:true,
                                valueField:'value',
                                textField:'lang',
                                url:'/bsc_dict/get_option/special_text'">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('主账号2');?></td>
                            <td>  
                                <select id="pan_id" name="pan_id" readonly class="easyui-combobox" style="width:300px;" editable="true" data-options="
                                    valueField:'id',
                                    textField:'name',
                                    url:'/bsc_user/get_option',  
                                    ">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('菜单权限');?></td>
                            <td>
                                <button type="button" onclick="menu_open()"><?= lang('点击设置');?></button>
                            </td>
                        </tr>
                        <!--2022-10-10口岸经理-->
                        <tr id="port_manager" style="visibility:hidden;">
                            <td><?= lang('口岸经理');?></td>
                            <td>
                                <select class="easyui-combogrid select" readonly name="port_area" style="width:300px;" id="port_area"
                                        data-options="
                    multiple : true,
                    panelWidth: '1000px',
                    panelHeight: '300px',
                    url:'/biz_port/get_option?type=FCL&limit=true',
                    idField: 'port_code',              //ID字段
                    textField: 'port_code',    //显示的字段
                    striped: true,
                    editable: false,
                    pagination: false,           //是否分页
                    toolbar : '#trans_origin_div',
                    collapsible: true,         //是否可折叠的
                    method: 'get',
                    columns:[[
                        {field:'port_code',title:'<?= lang('港口代码'); ?>',width:100},
                        {field:'port_name',title:'<?= lang('港口名称'); ?>',width:200},
                        {field:'terminal',title:'<?= lang('码头'); ?>',width:200},
                        {field:'port_manager_area',title:'<?= lang('口岸划分'); ?>',width:200},
                    ]],
                    mode: 'remote',
                    value: '<?=isset($market['loading_code'])?$market['loading_code']:'';?>',
                    emptyMsg : '未找到相应数据!',
                    onSelect: function(index, row){
                        if(row !== undefined){
                            $('#trans_origin').textbox('setValue', row.port_code);
                        }
                    },
                    onShowPanel:function(){
                        var opt = $(this).combogrid('options');
                        var toolbar = opt.toolbar;
                        combogrid_search_focus(toolbar);
                    },
                ">
                                </select>
                            </td>
                        </tr>
                    </table>
                     <a href="javascript:void(0);" class="easyui-linkbutton" id = "save" onclick="save()" style="width:90px"><?= lang('保存个人资料');?></a>
                    <!--<a href="javascript:void(0);" class="easyui-linkbutton" id = "kickoff" onclick="kickoff()" style="width:190px"><?= lang('强制踢下线并禁用');?></a>-->
                    <div class="set_leader" style="display: inline-block;">
                         <!--<a href="javascript:void(0);" class="easyui-linkbutton" id = "set_leader" onclick="set_leader()" style="width:90px"><?= lang('设为部门领导');?></a>-->
                         <!--<a href="javascript:void;" class="easyui-tooltip" style="color:violet;font-weight:bold;" data-options="content: function(){-->
                         <!--           var str = '<div>';-->
                         <!--           str += '设为部门领导，表示这个部门所有的人的上级领导都设为他， 他自己除外！';-->
                         <!--           str += '</div>';-->
                         <!--           return str;-->
                         <!--       },onShow: function(){$(this).tooltip('arrow').css('left', 20);$(this).tooltip('tip').css('left', $(this).offset().left); },"><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>-->
                     </div>
                     
                    <br>
                    <br>
                    <br>
                </div>
                <div class="table_right">
                    <table width="100%" height="100%" border="0" cellspacing="0">
                        <tr>
                            <td colspan="10">
                                <iframe scrolling="auto" class="user_role_iframe" id="user_role_iframe" frameborder="0" id="tracking" ></iframe>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>



            <div id="edit_btn">
            </div>
            <div id="image" class="show_if">
            </div>

            <div id="dlg">
                <img id="simg">
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    var read_key = '1read';
    var edit_key = '1edit';
    var url = '/bsc_user/add_data';

    $.extend($.fn.validatebox.defaults.rules, {
        Num: { //验证手机号
            validator: function(value, param){
                return /^\d*$/.test(value);
            },
            message: '<?= lang('请输入正确的手机号码。');?>'
        },
        phoneNum: { //验证手机号
            validator: function(value, param){
                return /^1[3-9]+\d{9}$/.test(value);
            },
            message: '<?= lang('请输入正确的手机号码。');?>'
        },
        telNum:{ //既验证手机号，又验证座机号
            validator: function(value, param){
                return /(^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$)|(^(()|(\d{3}\-))?(1[358]\d{9})$)/.test(value);
            },
            message: '<?= lang('请输入正确的电话号码。');?>'
        }
    });

    function station_select(values){
        //这里根据当前值,改变iframe的url
        $('#user_role_iframe').attr('src', '/bsc_user_role/index?user_role=' + values.join(','));
    }
    
    function set_leader(){
        var group = $('#group').combobox('getText');
        var group_code = $('#group').combobox('getValue'); 
        var user_id = $('#leader_id').combobox('getValue');
        if(user_id==""){
            alert("未选择领导");
            return;
        }
        var msg = "把 【"+group+"】的所有人的上级领导设置为【"+user_id+"】吗？\n\n请确认！"; 
        if (confirm(msg)==true){  
            $.ajax({
                type: 'GET',
                url: '/bsc_user/update_leader/' + user_id+'/'+group_code,
                dataType: 'json',
                success: function(res){
                    alert(res.msg);  
                }
            });
        }
    }
    function kickoff(){
        window.open("/bsc_user/kickoff/"+$('#id').textbox('getValue'));
    }
    function save(){
        if($('#user_form').form('validate')){
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    // title:$('#title').textbox('getValue'),
                    status:$('#status').combobox('getValue'),
                    assistant_flag:$('#assistant_flag').combobox('getValue'),
                    leader_id:$('#leader_id').combobox('getValue'),
                    pan_id:$('#pan_id').combobox('getValue'),
                    name:$('#name').textbox('getValue'),
                    m_level:$('#m_level').numberspinner('getValue'),
                    password:$('#password').textbox('getValue'),
                    // menu:$('#menu').combobox('getValues').join(','),
                    group:$('#group').combobox('getValues').join(','),
                    group_range:$('#group_range').combobox('getValues').join(','),
                    user_range:$('#user_range').combogrid('getValues').join(','),
                    telephone:$('#telephone').textbox('getValue'),
                    email:$('#email').textbox('getValue'),
                    user_role:$('#user_role').combobox('getValues').join(','),
                    // company:$('#company').combobox('getValue'),
                    station:$('#station').combobox('getValues').join(','),
                    special_text:$('#special_text').combobox('getValues').join(','),
                    // sales_quota: $('#sales_quota').numberbox('getValue'),
                    // points: $('#points').numberbox('getValue'),
                    port_area:$('#port_area').combogrid('getValues').join(','),
                    skype:$('#skype').textbox('getValue'),
                    job_title:$('#job_title').textbox('getValue'),
                    qq:$('#qq').textbox('getValue'),
                    telephone_area_code:$('#telephone_area_code').textbox('getValue'),
                },
                dataType: 'json',
                success: function(res){
                    // $('#tt').tree('reload');
                    alert(res.msg);
                    if(res.code == 0){
                    }else{
                    }
                    //2022-07-05 重新加载树
                    $('#tt').tree('reload');

                }
            });
        }
    }
    $(function () {
        //2022-07-06
        var nodeId;
        $('#tt').tree({
            url:'/bsc_user/getAllCompanyTree?children_closed=true&getUser=true',
            method: 'get',
            animate: true,
            onClick: function(node){
                nodeId = node.id;
                var id_arr = node.id.split('_'); 
                url = '/bsc_user/add_data'; 
                if(id_arr[0] != 'company'){
                    //选中部门的时候 
                    if(id_arr[0].length != 5){
                        $('.set_leader').show();
                    }else{ 
                     //选中个人user的时候 
                        $('.set_leader').hide();
                        $('#save').show();
                    }
                    //刷新输入框的内容
                    $.ajax({
                        type: 'GET',
                        url: '/bsc_user/get_user_one/' + node.id,
                        dataType: 'json',
                        success: function(res){
                            if(res.length == 0){
                                add_click();
                                url = '/bsc_user/add_data';
                                $('#group').combobox('setValue', node.id);
                                // $('#group_range').combobox('setValue', node.id);
                                $('#user_role_iframe').attr('src', '');
                            }else{
                                url = '/bsc_user/update_data?id=' + res.id; 
                                //选中时直接给iframe改连接 
                                station_select(res.station.split(','));
                                $('#user_form').form('load',res);

                                //2022-10-10 新增M10级 口岸经理
                                if(res.m_level>10){
                                    $('#port_manager').css('visibility','visible');
                                }else{
                                    $('#port_manager').css('visibility','hidden');
                                }
                            } 
                        }
                    });
                }else{ 
                    $('#save').hide();
                    $('.set_leader').hide();
                }
                console.log(url);
            },
            formatter:function(node){
                if(node.status == 1){
                    return '<span style="color:red;">' + node.text + '</span>';
                }
                return node.text;
            },
            onLoadSuccess(data){
                if(nodeId!=null){
                    var node = $('#tt').tree('find', nodeId);
                    $('#tt').tree('expandTo', node.target).tree('select', node.target);
                }
            }
        });
        $('#image').on('click', 'img', function (){
            $('#dlg').dialog({
                title: '预览',
                width: 800,
                height:500,
                resizable:true,
                closed: false,
                cache: false,
                modal: true,
                style: {'text-align': 'center'}
            });
            $("#simg").attr("src",$(this).attr('src'));

        });

        $('#search_user').textbox('textbox').blur(function () {
            var val = $('#search_user').textbox('getValue');
            //2022-08-15 搜索时自动展开树
            if(val == '') $("#tt").tree('collapseAll');
            else $("#tt").tree('expandAll');
            $("#tt").tree("doFilter", val);
        });

        $('#search_user').textbox('textbox').keydown(function (event) {
            if(event.keyCode == 13){
                $('#search_user').textbox('textbox').trigger('blur');
            }
        });

        //实现表格下拉框的分页和筛选--start
        var html = '<div id="reportComboGridTb">';
        html += '<form id="reportComboGridTbform">';
        html += '<div style="padding-left: 5px;display: inline-block;"><label>text:</label><input id="to_search_text" class="easyui-textbox to_search"style="width:96px;"data-options="prompt:\'text\'"/></div>';
        html += '<div style="padding-left: 5px;display: inline-block;"><label>group:</label><input id="to_search_group" class="easyui-combobox to_search"style="width:150px;"data-options="prompt:\'group\',valueField:\'value\',textField:\'value\',url:\'/bsc_dict/get_option/group\',"/></div>';
        html += '<div style="padding-left: 1px; display: inline-block;"><form>';
        html += '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="queryReport"><?= lang('search');?></a>';
        html += '</div>';
        html += '</div>';
        $('body').append(html);
        $.parser.parse('#reportComboGridTb');
        
        $('#user_range').combogrid("reset");
        $('#user_range').combogrid({
            panelWidth: '500px',
            width: 300,
            multiple:true,
            idField: 'id',              //ID字段
            textField: 'name',    //显示的字段
            fitColumns : true,
            striped: true,
            editable: false,
            pagination: true,           //是否分页
            pageList : [ 50,100,500 ],
            pageSize : 500,
            toolbar : '#reportComboGridTb',
            rownumbers: true,           //序号
            collapsible: true,         //是否可折叠的
            method: 'post',
            columns:[[
                {field:'name',title:'name',width:120},
                {field:'group',title:'group',width:120},
            ]],
            emptyMsg : '未找到相应数据!',
            onLoadSuccess : function(data) {
                var opts = $(this).combogrid('grid').datagrid('options');
                var vc = $(this).combogrid('grid').datagrid('getPanel').children('div.datagrid-view');
                vc.children('div.datagrid-empty').remove();
                if (!$(this).combogrid('grid').datagrid('getRows').length) {
                    var d = $('<div class="datagrid-empty"></div>').html(opts.emptyMsg || 'no records').appendTo(vc);
                    d.css({
                        position : 'absolute',
                        left : 0,
                        top : 50,
                        width : '100%',
                        fontSize : '14px',
                        textAlign : 'center'
                    });
                }
            },
            url:'/bsc_user/get_limit_data',
        });
        //点击搜索
        $('#queryReport').click(function(){
            var where = {};
            where['text'] = $('#to_search_text').textbox('getValue');
            where['group'] = $('#to_search_group').combobox('getValue');
            // where['field'] = $('#to_search_field').combo('getValue');
            $('#user_range').combogrid('grid').datagrid('load',{
                where:where,
            });
        });
        //text添加输入值改变
        $('.to_search').textbox('textbox').keydown(function (e) {
            if(e.keyCode == 13){
                $('#queryReport').trigger('click');
            }
        });
        //实现表格下拉框的分页和筛选-----end
    });

    //新增一个角色出来
    function add_ladder(e) {
        var this_tr = $(e).parent('td').parent('tr');
        var last_end_val = this_tr.find('.end');
        if(this_tr.parent('tbody').find('tr').length > 1){
            last_end_val = this_tr.parent('tbody').children('tr').last().find('.end');
        }

        var add_tr = $("<tr/>");
        var start_val = last_end_val.numberbox('getValue');
        if(start_val == '9999999999.99'){
            return;
        }
        if(start_val == ''){
            $.messager.alert('Tips', '最后一条结束金额未填写');
            return;
        }

        var tr_content =   "                                    <td>\n" +
            "                                        " +
            "                                    </td>\n" +
            "                                    <td>\n" +
            "                                        <input class=\"easyui-numberbox start\" value=\"" + start_val + "\" name='start' readonly prompt=\"开始金额\" data-options=\"min:0,precision:2\"> -\n" +
            "                                        <input class=\"easyui-numberbox end\" value=\"\" prompt=\"结束金额\" data-options=\"min:" + start_val + ",precision:2\">\n" +
            "                                    </td>\n" +
            "                                    <td>\n" +
            "                                        <input class=\"easyui-numberbox lr\" value=\"\" prompt=\"提成比例（%）\" data-options=\"min:0,precision:2,max:100,suffix:'%'\">\n" +
            "                                    </td>\n" +
            "                                    <td>\n" +
            "                                        <input type=\"hidden\" class=\"role\" value=\"" + this_tr.find('.role').val() + "\">\n" +
            "                                        <input type=\"hidden\" class=\"id\">\n" +
            "                                        <button type=\"button\" class='ladder_btn' onclick=\"del_ladder(this)\">-</button>\n" +
            "                                        <button type=\"button\" onclick=\"save_commision(this)\">保存</button>\n" +
            "                                    </td>"
        add_tr.append(tr_content);
        this_tr.parent('tbody').append(add_tr);
        $.parser.parse(add_tr);
    }
    function del_ladder(e) {
        var this_tr = $(e).parent('td').parent('tr');
        var id = this_tr.find('.id').val();

        if(id != 0){
            if(this_tr.index() != (this_tr.parent('tbody').children('tr').length - 1)){
                $.messager.alert('Tips', '只能先删除最后一个');
                return;
            }
            $.messager.confirm('确认对话框', '是否确认删除该提成设置？', function (r) {
                if(r){
                    this_tr.remove();
                    $.ajax({
                        type:'GET',
                        url:'/bsc_user/del_commision/' + id,
                        dataType:'json',
                        success:function (res) {
                            $.messager.alert('Tips', res.msg);
                        },
                        error:function () {
                            $.messager.alert('Tips', '发生错误，请联系管理员');
                        }
                    })
                }
            });
        }else{
            this_tr.remove();
        }
        //同时进行删除操作
    }

    var is_submit = false;
    function save_commision(e) {
        var this_tr = $(e).parent('td').parent('tr');
        var start_amount = this_tr.find('.start').numberbox('getValue');
        var end_amount = this_tr.find('.end').numberbox('getValue');
        var commision_rate = this_tr.find('.lr').numberbox('getValue');
        var user_id = $('#id').val();
        var user_role = this_tr.find('.role').val();
        var id = this_tr.find('.id').val();
        if(is_submit){
            return;
        }
        if(start_amount == end_amount){
            $.messager.alert('Tips', '开始金额不能和结束金额相同');
            return;
        }
        if(end_amount == ''){
            $.messager.alert('Tips', '结束金额不能为空');
            return;
        }
        if(commision_rate == ''){
            $.messager.alert('Tips', '提成比例不能为空');
            return;
        }
        var url = '/bsc_user/add_commision';
        if(id != 0 && id != ''){
            url = '/bsc_user/update_commision/' + id;
        }
        is_submit = true;
        ajaxLoading();
        $.ajax({
            type:'POST',
            url:url,
            data:{
                user_id:user_id,
                amount:end_amount,
                commision_rate:commision_rate,
                user_role: user_role,
            },
            dataType:'json',
            success:function (res) {
                ajaxLoadEnd();
                is_submit = false;
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg);
                }
            },
            error:function () {
                ajaxLoadEnd();
                is_submit = false;
                $.messager.alert('Tips', '发生错误，请联系管理员');
            }
        });
    }

    function add_click() {
        $('#id').textbox('clear');
        $('#title').textbox('clear');
        $('#name').textbox('clear');
        $('#password').textbox('clear');
        $('#station').combobox('clear');
        $('#telephone').textbox('clear');
        $('#email').textbox('clear');
        $('#user_role').combobox('clear');
        // $('#company').combobox('clear');
        $('#special_text').combobox('clear');
        $('#m_level').numberspinner('clear');
        $('#group_range').combobox('clear');
        $('#user_range').combobox('clear');
        $('#leader_id').combobox('clear');
        $('#pan_id').combobox('clear');
    }
    //field 字段名 如menu,  value 变化的值， ischeck 选中或者取消
    function add_image(field, value,ischeck=true) {
        $('.show_if').css('display', 'none');
        if(field != '' && value != ''){
            var check = '';
            if(ischeck){
                check = 'enable';
            }else{
                check = 'disable';
            }
            var str = '<img src="/inc/image/' + field + '/' + value + '_' + check + '.png">';
            $('#image').append(str);
            $('#image').css('display', 'block');
            return true;
        }else{
            return false;
        }

    }

    //2022-10-10 口岸经理
    function query_report(form_id, combo_id) {
        var where = {};
        var form_data = $('#' + form_id).serializeArray();
        $.each(form_data, function (index, item) {
            if (item.value == "") return true;
            if (where.hasOwnProperty(item.name) === true) {
                if (typeof where[item.name] == 'string') {
                    where[item.name] = where[item.name].split(',');
                    where[item.name].push(item.value);
                } else {
                    where[item.name].push(item.value);
                }
            } else {
                where[item.name] = item.value;
            }
        });
        if (where.length == 0) {
            $.messager.alert('Tips', '请填写需要搜索的值');
            return;
        }
        //判断一下当前是 remote 还是local,如果是remode,走下面这个
        var inp = $('#' + combo_id);
        var opt = inp.combogrid('options');
        if (opt.mode == 'remote') {
            inp.combogrid('grid').datagrid('load', where);
        } else {
            //如果是本地的,那么先存一下
            if (opt.old_data == undefined) opt.old_data = inp.combogrid('grid').datagrid('getData'); // 如果没存过,这里存一下
            var rows = [];
            if (JSON.stringify(where) === '{}') {
                inp.combogrid('grid').datagrid('loadData', opt.old_data.rows);
                return;
            }
            $.each(opt.old_data.rows, function (i, obj) {
                for (var p in where) {
                    var q = where[p].toUpperCase();
                    var v = obj[p].toUpperCase();
                    if (!!v && v.toString().indexOf(q) >= 0) {
                        rows.push(obj);
                        break;
                    }
                }
            });
            if (rows.length == 0) {
                inp.combogrid('grid').datagrid('loadData', []);
                return;
            }

            inp.combogrid('grid').datagrid('loadData', rows);
        }
    }

    /**
     * 下拉选择表格展开时,自动让表单内的查询获取焦点
     */
    function combogrid_search_focus(form_id, focus_num = 0) {
        // var form = $(form_id);
        setTimeout("easyui_focus($($('" + form_id + "').find('.keydown_search')[" + focus_num + "]));", 200);
    }

    /**
     * easyui 获取焦点
     */
    function easyui_focus(jq) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if (data.combogrid !== undefined) {
            return $(jq).combogrid('textbox').focus();
        } else if (data.combobox !== undefined) {
            return $(jq).combobox('textbox').focus();
        } else if (data.datetimebox !== undefined) {
            return $(jq).datetimebox('textbox').focus();
        } else if (data.datebox !== undefined) {
            return $(jq).datebox('textbox').focus();
        } else if (data.textbox !== undefined) {
            return $(jq).textbox('textbox').focus();
        }
    }

    //打开目录的窗口
    function menu_open() {
        var id = $('#id').textbox('getValue');
        if(id == ''){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择任意用户');?>');
            return;
        }

        $('#user_role_iframe').attr('src', '/bsc_menu/user?id=' + id);
    }

</script>
<div id="trans_origin_div">
    <form id="trans_origin_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码'); ?>:</label><input name="port_code" class="easyui-textbox keydown_search"
                                                       style="width:96px;" t_id="trans_origin_click"/>
            <label><?= lang('港口名称'); ?>:</label><input name="port_name" class="easyui-textbox keydown_search"
                                                       style="width:96px;" t_id="trans_origin_click"/>
            <label><?= lang('口岸划分'); ?>:</label><input name="port_manager_area" class="easyui-textbox keydown_search"
                                                       style="width:96px;" t_id="trans_origin_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_origin_click" iconCls="icon-search"
               onclick="query_report('trans_origin_form', 'port_area')"><?= lang('search'); ?></a>
        </div>
    </form>
</div>
