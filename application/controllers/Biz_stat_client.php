<?php


class Biz_stat_client extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->driver('cache');

        $this->load->model('bsc_user_model');
        $this->load->model('bsc_group_model');
        $this->load->model('m_model');
        $this->cache_key = 'stat:'.get_session('id');
    }

    public function index(){
        $data = [];
        $data['action'] = getValue('action', '');
        $default_date = array_map(function ($v){return date('Y-m-d', $v);}, $this->getXday_SdateAndEdate(time(), 30));
        $type_arr = array('sales'=>'user', 'sales_group'=>'group', 'sub_company'=>'sub_company');
        $data['group'] = getValue('group', 'HEAD');
        $data['group_by'] = getValue('group_by', 'sales_group ');
        $data['type'] = isset($type_arr[$_GET['type']]) ? $type_arr[$_GET['type']] : 'sub_company';
        $data['date_range'] = getValue('date_range', join(' ~ ', $default_date));

        $rs = get_user_title('biz_client', 'biz_client_index');
        foreach ($rs as $item){
            $data['search_field'][$item['table_field']] = $item;
        }
        $this->load->view('/biz/stat/stat_client', $data);
    }

    /**
     * 统计往来单位非活跃客户数量
     */
    public function stat_no_active(){
        $return = array('code' => 0, 'data' => [], 'total' => 0);
        $ThreeMonthsAgo = date('Y-m-d', strtotime('-3 month'));
        $history_group = [];
        $params = $this->input->post();
        $sub_query_where[] = "user_role = 'sales'";
        foreach ($params['group'] as $val) {
            list($sub_company, $group) = $this->get_group($val);
            //组下面的全部成员
            if (!empty($group)) {
                $rs = $this->db->where("group_code='{$val}' and status=0")->get('bsc_group')->row_array();
                if ($rs) $history_group[] = $rs['group_name'];
                $sub_query_where[] = "user_id IN ( select id from bsc_user where `group` in('" . join("','", array_keys($group)) . "') )";
            } else {
                $rs = $this->db->where("id", $val)->get('bsc_user')->row_array();
                if ($rs) $history_group[] = $rs['name'];
                $sub_query_where[] = "user_id = {$val}";
            }
        }

        //创建子查询
        $sub_query = $this->db->select('client_code, user_role, user_group, user_id')
            ->where(join(' and ', $sub_query_where), null, false)
            ->group_by('client_code')
            ->get_compiled_select('biz_client_duty');

        $field = "count( * ) AS total";
        $where[] = "biz_client.is_client = 1";

        //有过交易但最近三月内没有交易
        $where[] = "((select booking_ETD from biz_shipment where biz_shipment.client_code = biz_client.client_code order by booking_ETD desc limit 1) <= '{$ThreeMonthsAgo}' or (select booking_ETD from biz_shipment where biz_shipment.client_code = biz_client.client_code order by booking_ETD desc limit 1) is null)";
        if (!empty($where)){
            $where = join(' and ', $where);
        }

        //分组字段
        switch($params['group_by']){
            case 'sales_group':
                $field .= ",groups.group_code, groups.group_name";
                $group_by = 'groups.group_code';
                break;
            case 'sub_company':
                $field .= ",(select group_name from bsc_group where id = groups.parent_id) as group_name";
                $field .= ",(select group_code from bsc_group where id = groups.parent_id) as group_code";
                $group_by = '( SELECT group_code FROM bsc_group WHERE id = groups.parent_id )';
                break;
            default:
                $field .= ",biz_client_duty.user_id as group_code,(select `name` from bsc_user where id = biz_client_duty.user_id) as group_name";
                $group_by = 'biz_client_duty.user_id';
        }

        $this->db->select($field)->where($where, null, false);
        $this->db->join('biz_client', 'biz_client.client_code = biz_client_duty.client_code', 'LEFT');
        $this->db->join('bsc_group as groups', 'groups.group_code = biz_client_duty.user_group', 'LEFT');
        $result = $this->db->group_by($group_by)->order_by('total', 'desc')->get("({$sub_query}) as biz_client_duty")->result_array();
        $return['sql'] = lastquery();
        $total = array_sum(array_column($result, 'total'));
        //分组&排序
        if ($params['group_by'] == 'sales_group' && !empty($group)) {     //按部门分组
            foreach ($result as $item) {
                $group[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $group[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($group, 'total');
            array_multisort($totals, SORT_DESC, $group);
            $return['data'] = $group;
        }
        elseif($params['group_by'] == 'sub_company' && !empty($sub_company))    //按分公司分组
        {
            foreach ($result as $item) {
                $sub_company[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $sub_company[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($sub_company, 'total');
            array_multisort($totals, SORT_DESC, $sub_company);
            $return['data'] = $sub_company;
        }
        elseif($params['group_by'] == 'sales')
        {   //个人
            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }else{

            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }

        //历史条件
        $return['history_group'] = join(' -> ', $history_group);
        $return['code'] = 1;
        return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
    }

    /**
     * 统计往来单位活跃客户数量
     */
    public function stat_active(){
        $return = array('code' => 0, 'data' => [], 'total' => 0);
        $ThreeMonthsAgo = date('Y-m-d', strtotime('-3 month'));
        $history_group = [];
        $params = $this->input->post();
        $sub_query_where[] = "user_role = 'sales'";
        foreach ($params['group'] as $val) {
            list($sub_company, $group) = $this->get_group($val);
            //组下面的全部成员
            if (!empty($group)) {
                $rs = $this->db->where("group_code='{$val}' and status=0")->get('bsc_group')->row_array();
                if ($rs) $history_group[] = $rs['group_name'];
                $sub_query_where[] = "user_id IN ( select id from bsc_user where `group` in('" . join("','", array_keys($group)) . "') )";
            } else {
                $rs = $this->db->where("id", $val)->get('bsc_user')->row_array();
                if ($rs) $history_group[] = $rs['name'];
                $sub_query_where[] = "user_id = {$val}";
            }
        }

        //创建子查询
        $sub_query = $this->db->select('client_code, user_role, user_group, user_id')
            ->where(join(' and ', $sub_query_where), null, false)
            ->group_by('client_code')
            ->get_compiled_select('biz_client_duty');

        $field = "count( * ) AS total";
        $where[] = "biz_client.is_client = 1";
        //三个月内有过交易
        $where[] = "(select booking_ETD from biz_shipment where biz_shipment.client_code = biz_client.client_code order by booking_ETD desc limit 1) >= '{$ThreeMonthsAgo}'";
        if (!empty($where)){
            $where = join(' and ', $where);
        }

        //分组字段
        switch($params['group_by']){
            case 'sales_group':
                $field .= ",groups.group_code, groups.group_name";
                $group_by = 'groups.group_code';
                break;
            case 'sub_company':
                $field .= ",(select group_name from bsc_group where id = groups.parent_id) as group_name";
                $field .= ",(select group_code from bsc_group where id = groups.parent_id) as group_code";
                $group_by = '( SELECT group_code FROM bsc_group WHERE id = groups.parent_id )';
                break;
            default:
                $field .= ",biz_client_duty.user_id as group_code,(select `name` from bsc_user where id = biz_client_duty.user_id) as group_name";
                $group_by = 'biz_client_duty.user_id';
        }

        $this->db->select($field)->where($where, null, false);
        $this->db->join('biz_client', 'biz_client.client_code = biz_client_duty.client_code', 'LEFT');
        $this->db->join('bsc_group as groups', 'groups.group_code = biz_client_duty.user_group', 'LEFT');
        $result = $this->db->group_by($group_by)->order_by('total', 'desc')->get("({$sub_query}) as biz_client_duty")->result_array();
        $return['sql'] = lastquery();
        $total = array_sum(array_column($result, 'total'));
        //分组&排序
        if ($params['group_by'] == 'sales_group' && !empty($group)) {     //按部门分组
            foreach ($result as $item) {
                $group[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $group[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($group, 'total');
            array_multisort($totals, SORT_DESC, $group);
            $return['data'] = $group;
        }
        elseif($params['group_by'] == 'sub_company' && !empty($sub_company))    //按分公司分组
        {
            foreach ($result as $item) {
                $sub_company[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $sub_company[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($sub_company, 'total');
            array_multisort($totals, SORT_DESC, $sub_company);
            $return['data'] = $sub_company;
        }
        elseif($params['group_by'] == 'sales')
        {   //个人
            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }else{

            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }

        //历史条件
        $return['history_group'] = join(' -> ', $history_group);
        $return['code'] = 1;
        return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
    }

    /**
     * 统计往来单位新增数量
     */
    public function stat_add_client(){
        $return = array('code' => 0, 'data' => [], 'total' => 0);
        $history_group = [];
        $params = $this->input->post();
        $where = [];
        $field = "count( * ) AS total";
        //日期范围
        $date_where = array_map(function ($v) {
            return 'biz_client.' . $v;
        }, $this->create_date_where($params));
        if (isset($params['date_range']) && !empty($params['date_range'])) array_push($where, ...$date_where);

        foreach ($params['group'] as $val) {
            list($sub_company, $group) = $this->get_group($val);
            //组下面的全部成员
            if (!empty($group)) {
                $rs = $this->db->where("group_code='{$val}' and status=0")->get('bsc_group')->row_array();
                if ($rs) $history_group[] = $rs['group_name'];
                $where[] = "biz_client.created_by in(select id from bsc_user where `group` in('" . join("','", array_keys($group)) . "'))";
            } else {
                $rs = $this->db->where("id", $val)->get('bsc_user')->row_array();
                if ($rs) $history_group[] = $rs['name'];
                $where[] = "biz_client.created_by = {$val}";
            }
        }

        if (!empty($where)){
            $where = join(' and ', $where);
        }

        //分组字段
        switch($params['group_by']){
            case 'sales_group':
                $field .= ",groups.group_code, groups.group_name";
                $group_by = 'biz_client.created_group';
                break;
            case 'sub_company':
                $field .= ",(select group_name from bsc_group where id = groups.parent_id) as group_name";
                $field .= ",(select group_code from bsc_group where id = groups.parent_id) as group_code";
                $group_by = '( SELECT group_code FROM bsc_group WHERE id = groups.parent_id )';
                break;
            default:
                $field .= ",biz_client.created_by as group_code,(select `name` from bsc_user where id = biz_client.created_by) as group_name";
                $group_by = 'biz_client.created_by';
        }

        $this->db->select($field)->where($where)->join('bsc_group as groups', 'groups.group_code = biz_client.created_group', 'LEFT');
        $result = $this->db->group_by($group_by)->order_by('total', 'desc')->get('biz_client')->result_array();
        $total = array_sum(array_column($result, 'total'));
        $return['sql'] = lastquery();
        //echo lastquery();exit;
        //print_r($result);exit;
        //分组&排序
        if ($params['group_by'] == 'sales_group' && !empty($group)) {     //按部门分组
            foreach ($result as $item) {
                $group[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $group[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($group, 'total');
            array_multisort($totals, SORT_DESC, $group);
            $return['data'] = $group;
        }
        elseif($params['group_by'] == 'sub_company' && !empty($sub_company))    //按分公司分组
        {
            foreach ($result as $item) {
                $sub_company[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $sub_company[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($sub_company, 'total');
            array_multisort($totals, SORT_DESC, $sub_company);
            $return['data'] = $sub_company;
        }
        elseif($params['group_by'] == 'sales')
        {   //个人
            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }else{

            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }

        //历史条件
        $return['history_group'] = join(' -> ', $history_group);
        $return['code'] = 1;
        return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
    }

    /**
     * 统计CRM跟进中的客户数量
     */
    public function crm_status_2(){
        $return = array('code' => 0, 'data' => [], 'total' => 0);
        $history_group = [];
        $params = $this->input->post();
        $where = [];
        $where[] = "apply_status = 2";
        $field = "count( * ) AS total";
        foreach ($params['group'] as $val) {
            list($sub_company, $group) = $this->get_group($val);
            //组下面的全部成员
            if (!empty($group)) {
                $rs = $this->db->where("group_code='{$val}' and status=0")->get('bsc_group')->row_array();
                if ($rs) $history_group[] = $rs['group_name'];
                $where[] = "biz_client_crm.sales_id in(select id from bsc_user where `group` in('" . join("','", array_keys($group)) . "'))";
            } else {
                $rs = $this->db->where("id", $val)->get('bsc_user')->row_array();
                if ($rs) $history_group[] = $rs['name'];
                $where[] = "biz_client_crm.sales_id = {$val}";
            }
        }

        if (!empty($where)){
            $where = join(' and ', $where);
        }

        //分组字段
        switch($params['group_by']){
            case 'sales_group':
                $field .= ",groups.group_code, groups.group_name";
                $group_by = 'user.group';
                break;
            case 'sub_company':
                $field .= ",(select group_name from bsc_group where id = groups.parent_id) as group_name";
                $field .= ",(select group_code from bsc_group where id = groups.parent_id) as group_code";
                $group_by = '( SELECT group_code FROM bsc_group WHERE id = groups.parent_id )';
                break;
            default:
                $field .= ",biz_client_crm.created_by as group_code,(select `name` from bsc_user where id = biz_client_crm.created_by) as group_name";
                $group_by = 'biz_client_crm.created_by';
        }

        $this->db->select($field)->where($where)
            ->join('bsc_user as user', 'user.id = biz_client_crm.sales_id', 'LEFT')
            ->join('bsc_group as groups', 'groups.group_code = user.group', 'LEFT');
        $result = $this->db->group_by($group_by)->order_by('total', 'desc')->get('biz_client_crm')->result_array();
        $total = array_sum(array_column($result, 'total'));
        $return['sql'] = lastquery();
        //echo lastquery();exit;
        //print_r($result);exit;
        //分组&排序
        if ($params['group_by'] == 'sales_group' && !empty($group)) {     //按部门分组
            foreach ($result as $item) {
                $group[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $group[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($group, 'total');
            array_multisort($totals, SORT_DESC, $group);
            $return['data'] = $group;
        }
        elseif($params['group_by'] == 'sub_company' && !empty($sub_company))    //按分公司分组
        {
            foreach ($result as $item) {
                $sub_company[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $sub_company[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($sub_company, 'total');
            array_multisort($totals, SORT_DESC, $sub_company);
            $return['data'] = $sub_company;
        }
        elseif($params['group_by'] == 'sales')
        {   //个人
            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }else{

            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }

        //历史条件
        $return['history_group'] = join(' -> ', $history_group);
        $return['code'] = 1;
        return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
    }

    /**
     * 统计CRM手工新增
     */
    public function crm_add_by_hand(){
        $return = array('code' => 0, 'data' => [], 'total' => 0);
        $history_group = [];
        $params = $this->input->post();
        $where[] = "add_way = 'hand'";
        $field = "count( * ) AS total";

        //日期范围
        $date_where = array_map(function ($v) {
            return 'biz_client_crm.' . $v;
        }, $this->create_date_where($params));
        if (isset($params['date_range']) && !empty($params['date_range'])) array_push($where, ...$date_where);

        foreach ($params['group'] as $val) {
            list($sub_company, $group) = $this->get_group($val);
            //组下面的全部成员
            if (!empty($group)) {
                $rs = $this->db->where("group_code='{$val}' and status=0")->get('bsc_group')->row_array();
                if ($rs) $history_group[] = $rs['group_name'];
                $where[] = "biz_client_crm.sales_id in(select id from bsc_user where `group` in('" . join("','", array_keys($group)) . "'))";
            } else {
                $rs = $this->db->where("id", $val)->get('bsc_user')->row_array();
                if ($rs) $history_group[] = $rs['name'];
                $where[] = "biz_client_crm.sales_id = {$val}";
            }
        }

        if (!empty($where)){
            $where = join(' and ', $where);
        }

        //分组字段
        switch($params['group_by']){
            case 'sales_group':
                $field .= ",groups.group_code, groups.group_name";
                $group_by = 'user.group';
                break;
            case 'sub_company':
                $field .= ",(select group_name from bsc_group where id = groups.parent_id) as group_name";
                $field .= ",(select group_code from bsc_group where id = groups.parent_id) as group_code";
                $group_by = '( SELECT group_code FROM bsc_group WHERE id = groups.parent_id )';
                break;
            default:
                $field .= ",biz_client_crm.created_by as group_code,(select `name` from bsc_user where id = biz_client_crm.created_by) as group_name";
                $group_by = 'biz_client_crm.created_by';
        }

        $this->db->select($field)->where($where)
            ->join('bsc_user as user', 'user.id = biz_client_crm.sales_id', 'LEFT')
            ->join('bsc_group as groups', 'groups.group_code = user.group', 'LEFT');
        $result = $this->db->group_by($group_by)->order_by('total', 'desc')->get('biz_client_crm')->result_array();
        $total = array_sum(array_column($result, 'total'));
        $return['sql'] = lastquery();
        //echo lastquery();exit;
        //print_r($result);exit;
        //分组&排序
        if ($params['group_by'] == 'sales_group' && !empty($group)) {     //按部门分组
            foreach ($result as $item) {
                $group[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $group[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($group, 'total');
            array_multisort($totals, SORT_DESC, $group);
            $return['data'] = $group;
        }
        elseif($params['group_by'] == 'sub_company' && !empty($sub_company))    //按分公司分组
        {
            foreach ($result as $item) {
                $sub_company[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $sub_company[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($sub_company, 'total');
            array_multisort($totals, SORT_DESC, $sub_company);
            $return['data'] = $sub_company;
        }
        elseif($params['group_by'] == 'sales')
        {   //个人
            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }else{

            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }

        //历史条件
        $return['history_group'] = join(' -> ', $history_group);
        $return['code'] = 1;
        return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
    }

    /**
     * 统计CRM线索提取
     */
    public function crm_clue_extraction(){
        $return = array('code' => 0, 'data' => [], 'total' => 0);
        $history_group = [];
        $params = $this->input->post();
        $where[] = "add_way in('clue1', 'clue2')";
        $field = "count( * ) AS total";

        //日期范围
        $date_where = array_map(function ($v) {
            return 'biz_client_crm.' . $v;
        }, $this->create_date_where($params));
        if (isset($params['date_range']) && !empty($params['date_range'])) array_push($where, ...$date_where);

        foreach ($params['group'] as $val) {
            list($sub_company, $group) = $this->get_group($val);
            //组下面的全部成员
            if (!empty($group)) {
                $rs = $this->db->where("group_code='{$val}' and status=0")->get('bsc_group')->row_array();
                if ($rs) $history_group[] = $rs['group_name'];
                $where[] = "biz_client_crm.sales_id in(select id from bsc_user where `group` in('" . join("','", array_keys($group)) . "'))";
            } else {
                $rs = $this->db->where("id", $val)->get('bsc_user')->row_array();
                if ($rs) $history_group[] = $rs['name'];
                $where[] = "biz_client_crm.sales_id = {$val}";
            }
        }

        if (!empty($where)){
            $where = join(' and ', $where);
        }

        //分组字段
        switch($params['group_by']){
            case 'sales_group':
                $field .= ",groups.group_code, groups.group_name";
                $group_by = 'user.group';
                break;
            case 'sub_company':
                $field .= ",(select group_name from bsc_group where id = groups.parent_id) as group_name";
                $field .= ",(select group_code from bsc_group where id = groups.parent_id) as group_code";
                $group_by = '( SELECT group_code FROM bsc_group WHERE id = groups.parent_id )';
                break;
            default:
                $field .= ",biz_client_crm.created_by as group_code,(select `name` from bsc_user where id = biz_client_crm.created_by) as group_name";
                $group_by = 'biz_client_crm.created_by';
        }

        $this->db->select($field)->where($where)
            ->join('bsc_user as user', 'user.id = biz_client_crm.sales_id', 'LEFT')
            ->join('bsc_group as groups', 'groups.group_code = user.group', 'LEFT');
        $result = $this->db->group_by($group_by)->order_by('total', 'desc')->get('biz_client_crm')->result_array();
        $total = array_sum(array_column($result, 'total'));
        $return['sql'] = lastquery();
        //echo lastquery();exit;
        //print_r($result);exit;
        //分组&排序
        if ($params['group_by'] == 'sales_group' && !empty($group)) {     //按部门分组
            foreach ($result as $item) {
                $group[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $group[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($group, 'total');
            array_multisort($totals, SORT_DESC, $group);
            $return['data'] = $group;
        }
        elseif($params['group_by'] == 'sub_company' && !empty($sub_company))    //按分公司分组
        {
            foreach ($result as $item) {
                $sub_company[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $sub_company[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($sub_company, 'total');
            array_multisort($totals, SORT_DESC, $sub_company);
            $return['data'] = $sub_company;
        }
        elseif($params['group_by'] == 'sales')
        {   //个人
            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }else{

            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }

        //历史条件
        $return['history_group'] = join(' -> ', $history_group);
        $return['code'] = 1;
        return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
    }

    /**
     * 统计CRM线索提取
     */
    public function crm_conversion_rate(){
        $return = array('code' => 0, 'data' => [], 'total' => 0);
        $history_group = [];
        $params = $this->input->post();
        $where[] = "biz_workflow.id_type = 2";
        $where[] = "biz_workflow.status = 1";
        $where[] = "biz_workflow.status_text LIKE '%通过%'";
        $field = "count( * ) AS total";

        //日期范围
        $date_where = array_map(function ($v) {
            $v = str_replace('created_time', 'create_time', $v);
            return 'biz_workflow.' . $v;
        }, $this->create_date_where($params));
        if (isset($params['date_range']) && !empty($params['date_range'])) array_push($where, ...$date_where);

        foreach ($params['group'] as $val) {
            list($sub_company, $group) = $this->get_group($val);
            //组下面的全部成员
            if (!empty($group)) {
                $rs = $this->db->where("group_code='{$val}' and status=0")->get('bsc_group')->row_array();
                if ($rs) $history_group[] = $rs['group_name'];
                $where[] = "biz_workflow.create_id in(select id from bsc_user where `group` in('" . join("','", array_keys($group)) . "'))";
            } else {
                $rs = $this->db->where("id", $val)->get('bsc_user')->row_array();
                if ($rs) $history_group[] = $rs['name'];
                $where[] = "biz_workflow.create_id = {$val}";
            }
        }

        if (!empty($where)){
            $where = join(' and ', $where);
        }

        //分组字段
        switch($params['group_by']){
            case 'sales_group':
                $field .= ",groups.group_code, groups.group_name";
                $group_by = 'user.group';
                break;
            case 'sub_company':
                $field .= ",(select group_name from bsc_group where id = groups.parent_id) as group_name";
                $field .= ",(select group_code from bsc_group where id = groups.parent_id) as group_code";
                $group_by = '( SELECT group_code FROM bsc_group WHERE id = groups.parent_id )';
                break;
            default:
                $field .= ",biz_workflow.create_id as group_code,(select `name` from bsc_user where id = biz_workflow.create_id) as group_name";
                $group_by = 'biz_workflow.create_id';
        }

        $this->db->select($field)->where($where)
            ->join('bsc_user as user', 'user.id = biz_workflow.create_id', 'LEFT')
            ->join('bsc_group as groups', 'groups.group_code = user.group', 'LEFT');
        $result = $this->db->group_by($group_by)->order_by('total', 'desc')->get('biz_workflow')->result_array();
        $total = array_sum(array_column($result, 'total'));
        $return['sql'] = lastquery();
        //echo lastquery();exit;
        //print_r($result);exit;
        //分组&排序
        if ($params['group_by'] == 'sales_group' && !empty($group)) {     //按部门分组
            foreach ($result as $item) {
                $group[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $group[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($group, 'total');
            array_multisort($totals, SORT_DESC, $group);
            $return['data'] = $group;
        }
        elseif($params['group_by'] == 'sub_company' && !empty($sub_company))    //按分公司分组
        {
            foreach ($result as $item) {
                $sub_company[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $sub_company[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($sub_company, 'total');
            array_multisort($totals, SORT_DESC, $sub_company);
            $return['data'] = $sub_company;
        }
        elseif($params['group_by'] == 'sales')
        {   //个人
            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }else{

            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }

        //历史条件
        $return['history_group'] = join(' -> ', $history_group);
        $return['code'] = 1;
        return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
    }

    /**
     * 统计邮件发送数量
     */
    public function email_send_count(){
        $return = array('code' => 0, 'data' => [], 'total' => 0);
        $history_group = [];
        $params = $this->input->post();
        $field = "count( * ) AS total";
        $where = [];

        //日期范围
        $date_where = array_map(function ($v) {
            $v = str_replace('created_time', 'send_time', $v);
            return 'crm_promote_mail_log.' . $v;
        }, $this->create_date_where($params));
        if (isset($params['date_range']) && !empty($params['date_range'])) array_push($where, ...$date_where);

        foreach ($params['group'] as $val) {
            list($sub_company, $group) = $this->get_group($val);
            //组下面的全部成员
            if (!empty($group)) {
                $rs = $this->db->where("group_code='{$val}' and status=0")->get('bsc_group')->row_array();
                if ($rs) $history_group[] = $rs['group_name'];
                $where[] = "crm_promote_mail.create_by in(select id from bsc_user where `group` in('" . join("','", array_keys($group)) . "'))";
            } else {
                $rs = $this->db->where("id", $val)->get('bsc_user')->row_array();
                if ($rs) $history_group[] = $rs['name'];
                $where[] = "crm_promote_mail.create_by = {$val}";
            }
        }

        if (!empty($where)){
            $where = join(' and ', $where);
        }

        //分组字段
        switch($params['group_by']){
            case 'sales_group':
                $field .= ",groups.group_code, groups.group_name";
                $group_by = 'user.group';
                break;
            case 'sub_company':
                $field .= ",(select group_name from bsc_group where id = groups.parent_id) as group_name";
                $field .= ",(select group_code from bsc_group where id = groups.parent_id) as group_code";
                $group_by = '( SELECT group_code FROM bsc_group WHERE id = groups.parent_id )';
                break;
            default:
                $field .= ",crm_promote_mail.create_by as group_code,user.name as group_name";
                $group_by = 'crm_promote_mail.create_by';
        }

        $this->db->select($field)->where($where)
            ->join('crm_promote_mail', ' crm_promote_mail.id = crm_promote_mail_log.mail_id', 'LEFT')
            ->join('bsc_user as user', 'user.id = crm_promote_mail.create_by', 'LEFT')
            ->join('bsc_group as groups', 'groups.group_code = user.group', 'LEFT');
        $result = $this->db->group_by($group_by)->order_by('total', 'desc')->get('crm_promote_mail_log')->result_array();
        $total = array_sum(array_column($result, 'total'));
        $return['sql'] = lastquery();
        //echo lastquery();exit;
        //print_r($result);exit;
        //分组&排序
        if ($params['group_by'] == 'sales_group' && !empty($group)) {     //按部门分组
            foreach ($result as $item) {
                $group[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $group[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($group, 'total');
            array_multisort($totals, SORT_DESC, $group);
            $return['data'] = $group;
        }
        elseif($params['group_by'] == 'sub_company' && !empty($sub_company))    //按分公司分组
        {
            foreach ($result as $item) {
                $sub_company[$item['group_code']]['total'] = $item['total'];
                if ($total > 0) {
                    $sub_company[$item['group_code']]['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
            }
            //排序
            $totals = array_column($sub_company, 'total');
            array_multisort($totals, SORT_DESC, $sub_company);
            $return['data'] = $sub_company;
        }
        elseif($params['group_by'] == 'sales')
        {   //个人
            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }else{

            foreach ($result as $index => $item){
                if ($total > 0) {
                    $item['proportion'] = floor(strval(($item['total'] / $total) * 10000)) / 10000 * 100;
                }
                $result[$index] = $item;
            }
            $return['data'] = $result;
        }

        //历史条件
        $return['history_group'] = join(' -> ', $history_group);
        $return['code'] = 1;
        return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
    }

    /**
     * 统计邮件发送结果
     */
    public function get_send_email(){
        $return = array('code' => 0, 'data' => []);
        $params = $this->input->post();
        list($start_date, $end_date) = array_map(function ($v){return date('Y-m-d H:i:s', $v);}, $this->getXday_SdateAndEdate(time(), 15));
        list($sub_company, $group) = $this->get_group($params['group']);

        $field = "count( * ) AS total, DATE_FORMAT(crm_promote_mail_log.send_time, '%m.%d') as send_time";
        $where[] = "crm_promote_mail_log.send_time > '{$start_date}'";
        $where[] = "crm_promote_mail_log.send_time < '{$end_date}'";
        $where[] = "crm_promote_mail.create_by in(select id from bsc_user where `group` in('" . join("','", array_keys($group)) . "'))";
        if (!empty($where)){
            $where = join(' and ', $where);
        }

        //发送总数量
        $this->db->select($field)->where($where)
            ->join('crm_promote_mail', ' crm_promote_mail.id = crm_promote_mail_log.mail_id', 'LEFT')
            ->join('bsc_user as user', 'user.id = crm_promote_mail.create_by', 'LEFT')
            ->join('bsc_group as groups', 'groups.group_code = user.group', 'LEFT')
            ->group_by('DATE_FORMAT(crm_promote_mail_log.send_time, \'%m.%d\')')
            ->order_by('send_time', 'asc');
        $result_all = $this->db->get('crm_promote_mail_log')->result_array();

        //发送成功的数量
        $this->db->select($field)->where($where)
            ->join('crm_promote_mail', ' crm_promote_mail.id = crm_promote_mail_log.mail_id', 'LEFT')
            ->join('bsc_user as user', 'user.id = crm_promote_mail.create_by', 'LEFT')
            ->join('bsc_group as groups', 'groups.group_code = user.group', 'LEFT')
            ->group_by('DATE_FORMAT(crm_promote_mail_log.send_time, \'%m.%d\')')
            ->order_by('send_time', 'asc');
        $result_success = $this->db->where('crm_promote_mail_log.status = 1')->get('crm_promote_mail_log')->result_array();

        //发送失败的数量
        $this->db->select($field)->where($where)
            ->join('crm_promote_mail', ' crm_promote_mail.id = crm_promote_mail_log.mail_id', 'LEFT')
            ->join('bsc_user as user', 'user.id = crm_promote_mail.create_by', 'LEFT')
            ->join('bsc_group as groups', 'groups.group_code = user.group', 'LEFT')
            ->group_by('DATE_FORMAT(crm_promote_mail_log.send_time, \'%m.%d\')')
            ->order_by('send_time', 'asc');
        $result_failed = $this->db->where('crm_promote_mail_log.status = -1')->get('crm_promote_mail_log')->result_array();

        $result_all = array_combine(array_column($result_all, 'send_time'), array_column($result_all, 'total'));
        $result_success = array_combine(array_column($result_success, 'send_time'), array_column($result_success, 'total'));
        $result_failed = array_combine(array_column($result_failed, 'send_time'), array_column($result_failed, 'total'));
        for($i=0; $i<15; $i++){
            $date = date('m.d', strtotime("+{$i} day", strtotime($start_date)));
            $data['date'][] = $date;
            $data['_total'][] = array_key_exists($date, $result_all) === true ? (int)$result_all[$date] : 0;
            $data['_succ'][] = array_key_exists($date, $result_success) === true ? (int)$result_success[$date] : 0;
            $data['_fail'][] = array_key_exists($date, $result_failed) === true ? (int)$result_failed[$date] : 0;

        }

        $return['data'] = $data;
        $return['code'] = 1;
        return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
    }

    /**
     * @获取部门
     */
    private function get_group($group_code=''){
        $group = $this->db->where("group_code='{$group_code}' and status=0")->get('bsc_group')->row_array();
        $group_id = $group ? $group['id'] : 0;
        $sub_company = $this->get_sub_company($group_id);
        $result = Model('bsc_group_model')->get_child_group($group_code);
        $group = [];
        foreach ($result as $index => $item){
            $item['total'] = $item['proportion'] = 0;
            $group[$item['group_code']] = $item;
        }
        return [$sub_company, $group];
    }

    /**
     * @获取分公司
     */
    private function get_sub_company($parent_id = 0){
        $where = "group_type ='分公司' and status=0";
        //只取某个大区的分公司
        if (in_array($parent_id, [2044, 2045, 2046])) {
            $where .= " and parent_id = $parent_id";
        } elseif($parent_id != 1001){
            $where .= " and id = $parent_id";
        }
        $result = $this->db->select('id, parent_id, group_code, group_name')
            ->where($where)
            ->order_by('orderby', 'asc')
            ->get('bsc_group')
            ->result_array();

        $sub_company = [];
        foreach ($result as $index => $item){
            $item['total'] = $item['proportion'] = 0;
            $sub_company[$item['group_code']] = $item;
        }
        return $sub_company;
    }

    private function create_date_where($post){
        $default_date = array_map(function ($v){return date('Y-m-d H:i:s', $v);}, $this->getXday_SdateAndEdate(time()));
        $date_range = isset($post['date_range']) ? $post['date_range'] : join(' ~ ', $default_date);

        $where = [];
        if ($date_range != '') {
            list($start_date, $end_date) = explode(' ~ ', $date_range);
            $where[] = "created_time > '{$start_date} 00:00:00'";
            $where[] = "created_time < '{$end_date} 00:00:00'";
        }

        return $where;
    }

    /**
     * 获取最近X天内的开始时间和结束时间
     * $xDay 当前时间已过去的天数，默认30天前
     * $current_time 当前时间戳
     */
    private function getXday_SdateAndEdate($current_time=0, $xDay=30)
    {
        if (!$current_time) return false;
        $start_time = strtotime("-{$xDay} days", mktime(0,0, 0, date('m',$current_time),date('d',$current_time),date('Y',$current_time)));
        $end_time = mktime(0,0,0, date('m',$current_time),date('d',$current_time),date('Y',$current_time))-1;
        return [$start_time, $end_time];
    }
}

