<?php
   class bsc_user_model extends CI_Model{
        public function __construct(){
            $this->load->database();
        }
        
        public function get_by_id($id=0){
                $this->db->where('id', $id);
                $query = $this->db->get('bsc_user');
                return $query->row_array();
        }
        public function get($where = '', $sort='bsc_user.id', $order="desc"){
            if ($where != "") $this->db->where($where);
            $this->db->order_by($sort, $order);
            $query = $this->db->get('bsc_user');
            return $query->result_array();
        }
        
        public function no_role_get($where="" ,$sort="bsc_user.id",$order="desc"){
           if($where !="")	$this->db->where($where);
           $this->db->order_by($sort,$order);
           $query = $this->db->get('bsc_user');
           return $query->result_array();
       }

       public function total($where = "")
       {
           if ($where != "") $this->db->where($where);

           $this->db->from('bsc_user');
           return $this->db->count_all_results('');
       }

        public function save($data=array()){
            $data['created_by'] = get_session('id');
            $data['created_time'] = date('Y-m-d H:i:s');
            $data['updated_by'] = get_session('id');
            $data['updated_time'] = date('Y-m-d H:i:s');

            $this->db->insert('bsc_user', $data);
            return $this->db->insert_id();
        } 
        
        public function update($id='',$data=array()){
            $data['updated_by'] = get_session('id');
            $data['updated_time'] = date('Y-m-d H:i:s');

            $this->db->where('id', $id);
            $this->db->update('bsc_user', $data);
            return $id;
        }   
        
        public function mdelete($id=''){
                $this->db->where('id', $id);
                $this->db->delete('bsc_user');                                                
        }             
        public function check_pwd($username='',$password=''){
            $this->db->where("(title = '$username' or email= '$username')");
            $this->db->where('password', md5($password));
            $this->db->where('status', 0);
            $query = $this->db->get('bsc_user');
            return  $query->row_array();
        }  
        public function get_option(){      
                $this->db->select('id,title, name, telephone, email,status'); 
                $query = $this->db->get('bsc_user');
                return $query->result_array();
        } 

        public function get_by_role($role=''){  
                $this->db->like('user_role', $role); 
                $this->db->select('id,title, name, telephone, email,group,status');
                $query = $this->db->get('bsc_user');
                return $query->result_array();
        }
       public function get_one($field,$where){
            $this->db->where($field,$where);
            $query = $this->db->get('bsc_user');
            return $query->row_array();
       }
       
       public function get_where_one($where){
           $this->db->where($where);
           $query = $this->db->get('bsc_user');
           return $query->row_array();
       }

       public function get_user_relation_ids($ids = array()){
           $this->db->where_in('id',$ids);
           $this->db->select('id,group');
           $query = $this->db->get('bsc_user');
           return $query->result_array();
       }
       
       public function get_user_field($id, $field = 'name'){
           $this->db->where('id',$id);
           $this->db->select($field);
           $query = $this->db->get('bsc_user');
           return $query->row_array();
       }
       
       /**
        * 根据传过来的数据获取用户信息
        * @param $data array 传过来的数据
        * @param $data_field array  从$data中需要取出的用户ID相关的字段
        * @param $get_field array 需要返回的用户字段
        * @param $return_key string 返回时的键名,不填写就返回原样
        */
       public function getUsersByData($data = array(), $data_field = array(), $get_field = array('id', 'name'), $return_key = ''){
           //首先循环datafield来填充userIds
           $userIds = array();
           if(is_string($data_field)) $data_field = array($data_field);
           foreach ($data_field as $field){
               $userIds = array_merge($userIds, array_column($data, $field));
           }
           
           //合并数据完成去重去空
           $userIds = filter_unique_array($userIds);
           if(empty($userIds)) return array();
           
           //查询用户信息,然后根据某个字段返回,为空就原样返回
           if(!in_array($return_key, $get_field)) $get_field[] = $return_key;
           $this->db->select($get_field);
           $users = $this->get("id in (" . join(',', $userIds) . ")");
           
           if($return_key == '') return $users;
           else return array_column($users, null, $return_key);
       }
       
       /**
        * getUsersByData版本2 可以处理字符串连接的ID串
        */
       public function getUsersByDataVer2($data = array(), $data_field = array(), $get_field = array('id', 'name'), $return_key = ''){
           //首先循环datafield来填充userIds
           $userIds = array();
           if(is_string($data_field)) $data_field = array($data_field);
           foreach ($data_field as $field){
               //这里字段通常是逗号连接的ID串,所以需要提前处理下
               $ids_arr = explode(',', $data[$field]);

               $userIds = array_merge($userIds, $ids_arr);
           }

           //合并数据完成去重去空
           $userIds = filter_unique_array($userIds);
           if(empty($userIds)) return array();

           //查询用户信息,然后根据某个字段返回,为空就原样返回
           if(!in_array($return_key, $get_field)) $get_field[] = $return_key;
           $this->db->select($get_field);
           $users = $this->get("id in (" . join(',', $userIds) . ")");

           if($return_key == '') return $users;
           else return array_column($users, null, $return_key);
       }
   }