<?php


class biz_quotation extends Common_Controller
{
    public function __construct()
    {
        parent::__construct();
        Model('m_model');
    }
    
    public function index()
    {
        $result_json = curl_get("http://china.leagueshipping.com/biz_quotation/get_index_data");
        $result = json_decode($result_json, true);
        $data = $result['data'];
        $this->load->view('head');
        $this->load->view('biz/quotation/index_view', $data);
    }
    
    public function get_list(){
        echo curl_get("http://china.leagueshipping.com/biz_quotation/get_list?" . http_build_query($_GET));
    }
    
    public function get_data_by_shipping_schedule(){
        echo curl_post_body("http://china.leagueshipping.com/biz_quotation/get_data_by_shipping_schedule?" . http_build_query($_GET), array(), http_build_query($_POST));
    }
    public function local_rmb(){
        $loading_port = isset($_GET['loading_port']) ? $_GET['loading_port'] : 'CNSHA';
        $booking_agent = isset($_GET['booking_agent']) ? $_GET['booking_agent'] : 'SHDSCW01';
        $carrier = isset($_GET['carrier']) ? $_GET['carrier'] : 'DXHYYX02';
        $sheetname = isset($_GET['sheetname']) ? $_GET['sheetname'] : '';
        if(empty($sheetname)) exit("can't find sailing_area_code,please contact it!");

        exit("<script>location.href='/biz_auto_rate/local_table?trans_origin=$loading_port&trans_carrier=$carrier&booking_agent=$booking_agent&sailing_area=$sheetname&no_edit=1';</script>");

    }
    
      /**
     * @title 删除一行港口别名
     */
    public function del_port_alias(){
        $return = array('msg'=>'', 'code'=>0);
        $id = (int)$this->input->post('id');
        if ($id > 0){
            $query = $this->db->where('id', $id)->delete('biz_port_alias');
            if ($query){
                $return['code'] = 1;
            }else{
                $return['msg'] = '删除失败！';
            }
        }else{
            $return['msg'] = '删除失败,参数无效！';
        }


        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }
    
    
    /**
     * @title 快速编辑港口别名
     */
    public function port_alias_saveField(){
        $return = array('msg'=>'', 'code'=>0);
        $post  = $this->input->post();

        $query = $this->db->where('id', $post['keyid'])->update('biz_port_alias', array($post['field']=>$post['value']));
        if ($query){
            $return['code'] = 1;
        }else{
            $return['msg'] = '编辑失败！';
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }
}