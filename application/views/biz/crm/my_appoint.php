<!doctype html> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/inc/css/normal.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/icon.css?v=3">
<script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.easyui.min.js?v=11"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.edatagrid.js?v=5"></script>
<script type="text/javascript" src="/inc/js/easyui/datagrid-detailview.js"></script> 
<script type="text/javascript" src="/inc/js/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/inc/js/other.js?v=1"></script>
<style type="text/css">
    .f{
        width:115px;
    }
    .s{
        width:100px;
    }
    .v{
        width:200px;
    }
    .del_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }
</style>
<script type="text/javascript">
    function doSearch() {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] = [json[item.name]];
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        $('#tt').datagrid('load', json);
        set_config();
        $('#chaxun').window('close');
    }
    var ajax_data = {};
    var setting = {
        'operator_ids':['operator', 'id', 'name']
        ,'customer_service_ids':['customer_service', 'id', 'name']
        ,'sales_ids':['sales', 'id', 'name']
        ,'marketing_ids':['marketing', 'id', 'name']
        ,'oversea_cus_ids':['oversea_cus', 'id', 'name']
        ,'booking_ids':['booking', 'id', 'name']
        ,'document_ids':['document', 'id', 'name']
        ,'finance_ids':['finance', 'id', 'name']
    };

    var datebox = ['created_time', 'updated_time'];
 
    var datetimebox = [];
    $(function () {
        var selectIndex = -1;
        $('#tt').edatagrid({
            url: '/biz_crm/get_appoint_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            }
        });
        
        $('#cx').on('keydown', 'input', function (e) {
            if(e.keyCode == 13){
                doSearch();
            }
        });
        
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
            onDblClickRow: function (index, row) {
                url = '/biz_client/edit/' + row.id;
                window.open(url);
            },
        });
        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
               //新增查询列
        $('#cx table').on('click' , '.add_tr', function () {
            var index = $('.f').length;
            var str = '<tr>' +
                '                <td>\n' +
                '                    查询\n'+
                '                </td>\n'+
                '                <td align="right">' +
            '                       <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="\n' +
                '                            onChange:function(newVal, oldVal){\n' +
                '                                var index = $(\'.f\').index(this);\n' +
                '                                if(setting.hasOwnProperty(newVal) === true){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                    $(\'.v:eq(\' + index + \')\').combobox({\n' +
                '                                        data:ajax_data[setting[newVal][0]],\n' +
                '                                        valueField:setting[newVal][1],\n' +
                '                                        textField:setting[newVal][2],\n' +
                '                                    });\n' +
                '                                }else if ($.inArray(newVal, datebox) !== -1){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datebox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }else if ($.inArray(newVal, datetimebox) !== -1){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datetimebox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }else{\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }\n' +
                '                            }\n' +
                '                        ">\n' +
                '<option value=\'id\'>id</option><option value=\'client_code\'>客户代码</option><option value=\'client_name\'>客户简称</option><option value=\'company_name\'>客户全称</option><option value=\'province\'>省</option><option value=\'city\'>市</option><option value=\'area\'>区</option><option value=\'sailing_area\'>航线区域</option><option value=\'company_address\'>客户地址</option><option value=\'contact_name\'>客户联系人</option><option value=\'contact_live_chat\'>客户QQ</option><option value=\'contact_telephone\'>手机号(网络账号)</option><option value=\'contact_telephone2\'>客户座机</option><option value=\'contact_email\'>客户邮箱</option><option value=\'contact_position\'>contact_position</option><option value=\'finance_name\'>财务联系人</option><option value=\'finance_live_chat\'>财务QQ</option><option value=\'finance_telephone\'>财务电话</option><option value=\'finance_email\'>财务邮箱</option><option value=\'finance_payment\'>付款方式</option><option value=\'finance_od_basis\'>超期依据</option><option value=\'finance_payment_month\'>finance_payment_month</option><option value=\'finance_payment_days\'>付款天数</option><option value=\'finance_payment_day_th\'>finance_payment_day_th</option><option value=\'credit_amount\'>授信额度</option><option value=\'finance_status\'>财务状态</option><option value=\'finance_remark\'>财务备注</option><option value=\'finance_sign_company_code\'>finance_sign_company_code</option><option value=\'bank_name_cny\'>银行(CNY)</option><option value=\'bank_account_cny\'>银行帐号(CNY)</option><option value=\'bank_address_cny\'>银行地址(CNY)</option><option value=\'bank_swift_code_cny\'>银行swift(CNY)</option><option value=\'bank_name_usd\'>银行(USD)</option><option value=\'bank_account_usd\'>银行帐号(USD)</option><option value=\'bank_address_usd\'>银行地址(USD)</option><option value=\'bank_swift_code_usd\'>银行swift(USD)</option><option value=\'taxpayer_name\'>纳税人名称</option><option value=\'taxpayer_id\'>纳税人识别号</option><option value=\'taxpayer_address\'>纳税人地址</option><option value=\'taxpayer_telephone\'>纳税人电话</option><option value=\'deliver_info\'>快递地址</option><option value=\'website_url\'>企业网址</option><option value=\'source_from\'>信息来源</option><option value=\'role\'>角色</option><option value=\'remark\'>备注</option><option value=\'sales_id\'>管理人</option><option value=\'read_user_group\'>read_user_group</option><option value=\'created_by\'>创建人</option><option value=\'created_time\'>创建时间</option><option value=\'updated_by\'>修改人</option><option value=\'updated_time\'>修改时间</option><option value=\'client_level\'>client_level</option><option value=\'tag\'>标签</option><option value=\'country_code\'>国家代码</option><option value=\'invoice_email\'>invoice_email</option><option value=\'stop_date\'>stop_date</option><option value=\'dn_accounts\'>dn_accounts</option><option value=\'operator_ids\'>operator_ids</option><option value=\'customer_service_ids\'>customer_service_ids</option><option value=\'sales_ids\'>sales_ids</option><option value=\'marketing_ids\'>marketing_ids</option><option value=\'oversea_cus_ids\'>oversea_cus_ids</option><option value=\'booking_ids\'>booking_ids</option><option value=\'document_ids\'>document_ids</option><option value=\'finance_ids\'>finance_ids</option>' +
                '                    </select>\n' +
                '                    &nbsp;\n' +
                '\n' +
                '                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="\n' +
                '                        ">\n' +
                '                        <option value="like">like</option>\n' +
                '                        <option value="=">=</option>\n' +
                '                        <option value=">=">>=</option>\n' +
                '                        <option value="<="><=</option>\n' +
                '                        <option value="!=">!=</option>\n' +
                '                    </select>\n' +
                '                    &nbsp;\n' +
                '                    <div class="this_v">\n' +
                '                        <input name="field[v][]" class="v easyui-textbox" >\n' +
                '                    </div>\n' +
                '                    <button class="del_tr" type="button">-</button>' +
                '   </td>' +
                '</tr>';
            $('#cx table tbody').append(str);
            $.parser.parse($('#cx table tbody tr:last-child'));
            return false;
        });
        //删除按钮
        $('#cx table').on('click' , '.del_tr', function () {
            var index = $(this).parents('tr').index();
            $(this).parents('tr').remove();
            return false;
        });
        var a1 = ajax_get('/bsc_user/get_data_role/operator', 'operator');
        var a2 = ajax_get('/bsc_user/get_data_role/customer_service', 'customer_service');
        var a3 = ajax_get('/bsc_user/get_data_role/sales', 'sales');
        var a4 = ajax_get('/bsc_user/get_data_role/marketing', 'marketing');
        var a5 = ajax_get('/bsc_user/get_data_role/oversea_cus', 'oversea_cus');
        var a6 = ajax_get('/bsc_user/get_data_role/booking', 'booking');
        var a7 = ajax_get('/bsc_user/get_data_role/document', 'document');
        var a8 = ajax_get('/bsc_user/get_data_role/finance', 'finance');
        $.ajax({
            type: 'GET',
            url: '/sys_config/get_config_search/crm_my_appoint',
            dataType: 'json',
            success:function (res) {
                var config = res.data;
                if(config.length > 0){
                    var count = config[0];
                    $('.f:last').combobox('setValue', config[1][0][0]);
                    $('.s:last').combobox('setValue', config[1][0][1]);
                    for (var i = 1; i < count; i++){
                        $('.add_tr:eq(0)').trigger('click');
                        $('.f:last').combobox('setValue', config[1][i][0]);
                        $('.s:last').combobox('setValue', config[1][i][1]);
                    }
                }
            }
        });

        $.when(a1,a2,a3,a4,a5,a6,a7,a8).done(function () {
            $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                var ec_value = $(ec_item).combobox('getValue');
                $(ec_item).combobox('clear').combobox('select', ec_value);
            });
        });
    });
    
    var is_submit = false;
    
    function update_tag(row) {
        $('#update_tag_div').window('open');

        $('#update_tag_fm').form('clear');
        var from_data = {};
        from_data.id = row.id;
        from_data.tag = row.tag;
        $('.tags').html('');
        $.each(row.tag.split(','), function(i,it){
            add_tags(it);
        });
        $('#update_tag_fm').form('load', from_data);
    }

    function update_tag_save() {
        if(is_submit){
            return;
        }
        $.messager.confirm('确认对话框', '是否确认提交？', function(r) {
            if (r) {
                is_submit = true;
                ajaxLoading();
                var this_window = $('#update_tag_div');
                var form = $('#update_tag_fm');
                add_tags($('#tags').textbox('getValue'));
                form.form('submit', {
                    url: '/biz_client/update_data_ajax',
                    onSubmit: function () {
                        var validate = $(this).form('validate');
                        if (!validate) {
                            ajaxLoadEnd();
                            is_submit = false;
                        }
                        return validate;
                    },
                    success: function (res_json) {
                        var res;
                        try {
                            res = $.parseJSON(res_json);
                        } catch (e) {
                        }
                        if (res == undefined) {
                            ajaxLoadEnd();
                            is_submit = false;
                            $.messager.alert('Tips', '发生错误');
                            return;
                        }
                        this_window.window('close');
                        ajaxLoadEnd();
                        is_submit = false;
                        if (res.code == 0) {
                            $.messager.alert('<?= lang('Tips')?>', res.msg);
                            $('#tt').datagrid('reload');
                        } else {
                             $.messager.alert('<?= lang('Tips')?>', res.msg, 'info', function(r){
                                this_window.window('open');
                            });
                        }

                    }
                });
            }
        });
    }
    
    function ajax_get(url,name) {
        return $.ajax({
            type:'POST',
            url:url,
            dataType:'json',
            success:function (res) {
                ajax_data[name] = res;
            },
            error:function () {
                // $.messager.alert('获取数据错误');
            }
        })
    }
    
    function set_config() {
        var f_input = $('.f');
        var count = f_input.length;
        var config = [];
        $.each(f_input, function (index, item) {
            var c_array = [];
            c_array.push($('.f:eq(' + index + ')').combobox('getValue'));
            c_array.push($('.s:eq(' + index + ')').combobox('getValue'));
            config.push(c_array);
        });
        // var config_json = JSON.stringify(config);
        $.ajax({
            type: 'POST',
            url: '/sys_config/save_config_search',
            data:{
                table: 'crm_my_appoint',
                count: count,
                config: config,
            },
            dataType:'json',
            success:function (res) {

            },
        });
    }
    
    function cancel_follow(id){
        $.messager.confirm('Tips','是否确认撤销指派，撤销后销售将变为之前的销售？',function(r){
            if (r){
                $.ajax({
                    type:'POST',
                    url:'/biz_crm/cancel_follow',
                    data:{
                        id:id,
                    },
                    success:function (res_json) {
                        var res;
                        try{
                            res = $.parseJSON(res_json);
                        }catch (e) {

                        }
                        if(res == undefined){
                            $.messager.alert('Tips', res_json);
                            return
                        }
                        if(res.code == 0){
                            $.messager.alert('Tips', res.msg, 'info', function () {
                                location.reload();
                            });
                        }else{
                            $.messager.alert('Tips', res.msg);
                        }
                    }
                });
            }
        });
    }
    
    function buttons_for(value, row, index){
        var str = "";
        str += '<a href="javascript:void(0)" onclick="cancel_follow(' + row.biz_client_follow_up_id + ')">撤销指派</a>';
        return str;
    }
</script>

<table id="tt" style="width:1100px;height:450px" rownumbers="false" pagination="true" idField="id" pagesize="30" toolbar="#tb" singleSelect="true" nowrap="true">
    <thead>
        <tr>
            <th field="tag" width="111">客户标签</th>
            <th field="client_code" width="100">客户代码</th>
            <th field="client_name" width="100">客户简称</th>
            <th field="province" width="100">省</th>
            <th field="city" width="100">市</th>
            <th field="company_address" width="120">客户地址</th>
            <th field="contact_name" width="120">客户联系人</th>
            <th field="contact_telephone" width="120">手机号(网络账号)</th>
            <th field="contact_telephone2" width="120">客户座机</th>
            <th field="contact_email" width="120">客户邮箱</th>
            <th field="follow_up_day" width="80">指派时长</th>
            <th field="buttons" width="80" formatter="buttons_for">操作</th>
        </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
                   onclick="add();">新增</a>-->
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');">查询</a>
            </td>
        </tr>
    </table>
</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <form id="cx">
        <table>
            <tr>
                <td>
                    角色                </td>
                <td align="right">
                    <select class="easyui-combobox" id="f_role" name="f_role" style="width: 475px"
                    data-options="
                        valueField:'value',
                        textField:'value',
                        url:'/bsc_dict/get_option/role',
                    ">
    
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    查询                </td>
                <td align="right">
                    <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="
                            onChange:function(newVal, oldVal){
                                var index = $('.f').index(this);
                                if(setting.hasOwnProperty(newVal) === true){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                    $('.v:eq(' + index + ')').combobox({
                                        data:ajax_data[setting[newVal][0]],
                                        valueField:setting[newVal][1],
                                        textField:setting[newVal][2],
                                    });
                                }else if ($.inArray(newVal, datebox) !== -1){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datebox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }else if ($.inArray(newVal, datetimebox) !== -1){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datetimebox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }else{
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }
                            }
                        ">
                        <option value='id'>id</option><option value='client_code'>客户代码</option><option value='client_name'>客户简称</option><option value='company_name'>客户全称</option><option value='province'>省</option><option value='city'>市</option><option value='area'>区</option><option value='sailing_area'>航线区域</option><option value='company_address'>客户地址</option><option value='contact_name'>客户联系人</option><option value='contact_live_chat'>客户QQ</option><option value='contact_telephone'>手机号(网络账号)</option><option value='contact_telephone2'>客户座机</option><option value='contact_email'>客户邮箱</option><option value='contact_position'>contact_position</option><option value='finance_name'>财务联系人</option><option value='finance_live_chat'>财务QQ</option><option value='finance_telephone'>财务电话</option><option value='finance_email'>财务邮箱</option><option value='finance_payment'>付款方式</option><option value='finance_od_basis'>超期依据</option><option value='finance_payment_month'>finance_payment_month</option><option value='finance_payment_days'>付款天数</option><option value='finance_payment_day_th'>finance_payment_day_th</option><option value='credit_amount'>授信额度</option><option value='finance_status'>财务状态</option><option value='finance_remark'>财务备注</option><option value='finance_sign_company_code'>finance_sign_company_code</option><option value='bank_name_cny'>银行(CNY)</option><option value='bank_account_cny'>银行帐号(CNY)</option><option value='bank_address_cny'>银行地址(CNY)</option><option value='bank_swift_code_cny'>银行swift(CNY)</option><option value='bank_name_usd'>银行(USD)</option><option value='bank_account_usd'>银行帐号(USD)</option><option value='bank_address_usd'>银行地址(USD)</option><option value='bank_swift_code_usd'>银行swift(USD)</option><option value='taxpayer_name'>纳税人名称</option><option value='taxpayer_id'>纳税人识别号</option><option value='taxpayer_address'>纳税人地址</option><option value='taxpayer_telephone'>纳税人电话</option><option value='deliver_info'>快递地址</option><option value='website_url'>企业网址</option><option value='source_from'>信息来源</option><option value='role'>角色</option><option value='remark'>备注</option><option value='sales_id'>管理人</option><option value='read_user_group'>read_user_group</option><option value='created_by'>创建人</option><option value='created_time'>创建时间</option><option value='updated_by'>修改人</option><option value='updated_time'>修改时间</option><option value='client_level'>client_level</option><option value='tag'>标签</option><option value='country_code'>国家代码</option><option value='invoice_email'>invoice_email</option><option value='stop_date'>stop_date</option><option value='dn_accounts'>dn_accounts</option><option value='operator_ids'>operator_ids</option><option value='customer_service_ids'>customer_service_ids</option><option value='sales_ids'>sales_ids</option><option value='marketing_ids'>marketing_ids</option><option value='oversea_cus_ids'>oversea_cus_ids</option><option value='booking_ids'>booking_ids</option><option value='document_ids'>document_ids</option><option value='finance_ids'>finance_ids</option>                    </select>
                    &nbsp;

                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="
                        ">
                        <option value="like">like</option>
                        <option value="=">=</option>
                        <option value=">=">>=</option>
                        <option value="<="><=</option>
                        <option value="!=">!=</option>
                    </select>
                    &nbsp;
                    <div class="this_v">
                        <input name="field[v][]" class="v easyui-textbox" >
                    </div>
                    <button class="add_tr" type="button">+</button>
                </td>
            </tr>
        </table>
        <br><br>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="doSearch();">查询</button>
        <a href="javascript:void;" class="easyui-tooltip" data-options="
            content: $('<div></div>'),
            onShow: function(){
                $(this).tooltip('arrow').css('left', 20);
                $(this).tooltip('tip').css('left', $(this).offset().left);
            },
            onUpdate: function(cc){
                cc.panel({
                    width: 500,
                    height: 'auto',
                    border: false,
                    href: '/bsc_help_content/help/query_condition'
                });
            }
        ">help</a>
    </form>
</div>
