<style type="text/css">
    .input{
        width: 255px;
    }
    .select{
        width: 262px;
    }
    .textarea{
        width: 255px;
        height: 100px;
    }
    .box_info_num{
        width: 100px;
    }
    .box_info_size{
        width: 100px;
    }
    .box{
        min-width: 1178px;
        display: flex;
        /*border: 1px solid black;*/
        margin: 20px;
    }
    .box-left{
        padding-left: 30px;
        margin-right: 30px;
        flex-grow:2;
        max-width: 368px;
    }
    .box-right{
        margin-right: 30px;
        flex-grow:3;
    }
    .box-title{
    }
    .box-title-span{
        font-size: 24px;
        font-weight: bold;
    }
    .box-hr{
        padding: 5px 0;
    }
    .box-left-input{
        padding: 5px 0;
    }
    .box-left-input-label{
        width:81px;
        margin-right: 19px;
        display:inline-block;
        text-align: right;
    }
    .free_svr_type{
        width: 100px;
    }
    .free_svr_days{
        width: 90px;
    }

    /*进度条*/
    .main {
        width: 80%;
        padding: 0px 15px;
        /* outline: 1px dashed red; */
        overflow: visible;
    }
    .main .item {
        width: 14%;
        float: left;
        height: 30px;
        line-height: 30px;
        background-color: #999;
        margin-right: 10px;
        text-align: center;
        color: #fff;
        font-size: 14px;
        font-family: "微软雅黑";
        /* 内边距 */
        padding: 5px 0 5px 8px;
        /* 相对定位 */
        position: relative;
        cursor: pointer;
    }
    /* 通过伪类::before绘制图形 */
    .item::before {
        content: "";
        width: 0;
        height: 0;
        /* 通过边框让图形显示出来 */
        border-style: solid;
        /* 边框的方向设置顺序： 上边框  右边框 下边框 左边框 ‘上 右 下 左’ */
        border-width: 20px 0 20px 10px;
        border-color: transparent transparent transparent #999;
        border-left-color: #fff;
        /* 绝对定位 */
        position: absolute;
        left: 0;
        top: 0;
        z-index: 2;
    }

    /* 通过伪类::after绘制图形 */
    .item::after {
        content: "";
        width: 0;
        height: 0;
        /* 通过边框让图形显示出来 */
        border-style: solid;
        /* 边框的方向设置顺序： 上边框  右边框 下边框 左边框 ‘上 右 下 左’ */
        border-width: 20px 0 20px 10px;
        border-color: transparent transparent transparent #999;
        /* 绝对定位 */
        position: absolute;
        right: -10px;
        top: 0;
        z-index: 2;
    }
    /* 鼠标悬停 */
    .main .curr {
        background-color: #018c67;
    }
    .main .curr::after {
        content: "";
        width: 0;
        height: 0;
        /* 通过边框让图形显示出来 */
        border-style: solid;
        /* 边框的方向设置顺序： 上边框  右边框 下边框 左边框 ‘上 右 下 左’ */
        border-width: 20px 0 20px 10px;
        border-color: transparent transparent transparent #018c67;
        /* 绝对定位 */
        position: absolute;
        right: -10px;
        top: 0;
        z-index: 2;
    }
    .step2-btn {
        margin: 10px 30px;
        color: #ff5909;
        font-size: 14px;
    }
</style>
<script type="text/javascript">
    function query_report(form_id, combo_id){
        var where = {};
        var form_data = $('#' + form_id).serializeArray();
        $.each(form_data, function (index, item) {
            if(item.value == "") return true;
            if(where.hasOwnProperty(item.name) === true){
                if(typeof where[item.name] == 'string'){
                    where[item.name] =  where[item.name].split(',');
                    where[item.name].push(item.value);
                }else{
                    where[item.name].push(item.value);
                }
            }else{
                where[item.name] = item.value;
            }
        });
        if(where.length == 0){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请填写需要搜索的值');?>');
            return;
        }
        //判断一下当前是 remote 还是local,如果是remode,走下面这个
        var inp = $('#' + combo_id);
        var opt = inp.combogrid('options');
        if(opt.mode == 'remote'){
            inp.combogrid('grid').datagrid('load',where);
        }else{
            //如果是本地的,那么先存一下
            if(opt.old_data == undefined) opt.old_data = inp.combogrid('grid').datagrid('getData'); // 如果没存过,这里存一下
            var rows = [];
            if(JSON.stringify(where) === '{}'){
                inp.combogrid('grid').datagrid('loadData', opt.old_data.rows);
                return;
            }
            $.each(opt.old_data.rows,function(i,obj){
                for(var p in where){
                    var q = where[p].toUpperCase();
                    var v = obj[p];
                    if (!!v && v.toString().indexOf(q) >= 0){
                        rows.push(obj);
                        break;
                    }
                }
            });
            if(rows.length == 0) {
                inp.combogrid('grid').datagrid('loadData', []);
                return;
            }

            inp.combogrid('grid').datagrid('loadData',rows);
        }
    }

    var is_submit = false;
    function submit_start() {
        ajaxLoading();
        is_submit = true;
    }

    function submit_end() {
        ajaxLoadEnd();
        is_submit = false;
    }

    /**
     * 转为操作审核
     */
    function operator_apply(){
        if(is_submit){
            return;
        }
        $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认转为操作审核?');?>', function (r) {
            if(r){
                submit_start();
                $.ajax({
                    url: '/biz_shipment_ebooking/cancel_status?id=<?= $id;?>',
                    type: 'post',
                    data:{
                        status: 0,
                    },
                    success: function (res_json) {
                        var res;
                        try{
                            res = $.parseJSON(res_json);
                        }catch(e){

                        }
                        if(res == undefined){
                            $.messager.alert('<?= lang('提示');?>', res);
                            return;
                        }
                        submit_end();
                        $.messager.alert('<?= lang('提示');?>', res.msg);
                        if(res.code == 0){
                            location.reload();
                        }else{
                        }
                    },
                });
            }
        });
    }

    function submit(){
        var client_contact = $('#client_contact').combobox('getValue');
        if(client_contact == '' || client_contact == 0){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择客户联系人');?>');
            return;
        }

        var client_email = $('#client_email').val();
        if(client_email.indexOf('@') == -1){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('客户联系人邮箱未填写，请重新选择！');?>');
            return;
        }

        var sales_id = $('#sales_id').combogrid('getValue');
        if(sales_id == '' || sales_id == '0'){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择销售');?>');
            return;
        }

        var operator_id = $('#operator_id').combogrid('getValue');
        if(operator_id == '' || operator_id == '0'){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择操作');?>');
            return;
        }
        var trans_origin = $('#trans_origin').textbox('getValue');
        if(trans_origin.length !== 5 && trans_origin.length !== 3){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('起运港未正确选中');?>');
            return;
        }
        var trans_destination = $('#trans_destination').textbox('getValue');
        if(trans_destination.length !== 5 && trans_destination.length !== 3){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('目的港未正确选中');?>');
            return;
        }

        submit_start();
        $('#fm').form('submit', {
            url: '/biz_shipment_ebooking/update_data/<?= $id;?>',
            onSubmit: function () {
                var validate = $(this).form('validate');

                if(!validate){
                    submit_end();
                }
                $('#fm').append($('#service_option_tab').clone().css('display', 'none'));
                return validate;
            },
            success: function (res_json) {
                $('#fm #service_option_tab').remove();
                var res;
                try{
                    res = $.parseJSON(res_json);
                }catch(e){

                }
                submit_end();
                if(res === undefined){
                    $.messager.alert('Tips', res);
                    return;
                }
                $.messager.alert('Tips', res.msg);
                if(res.code == 0){
                    location.reload();
                }
            }
        });
    }

    function rebook(){
        if(is_submit){
            return;
        }
        $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认撤回，重新提交该Eboooking?');?>', function (r) {
            if(r){
                submit_start();
                $.ajax({
                    url: '/biz_shipment_ebooking/cancel_status?id=<?= $id;?>',
                    type: 'post',
                    data:{
                        status: 0,
                    },
                    success: function (res_json) {
                        var res;
                        try{
                            res = $.parseJSON(res_json);
                        }catch(e){

                        }
                        if(res == undefined){
                            $.messager.alert('Tips', res);
                            return;
                        }
                        submit_end();
                        $.messager.alert('Tips', res.msg);
                        if(res.code == 0){
                            location.reload();
                        }else{
                        }
                    },
                });
            }
        });
    }

    function client_code_select(index, row){
        $('#edit_contact_link').attr('href', '/biz_client_contact/index/'+row.client_code);

        $('#client_company').val(row.company_name);

        $('#client_contact').combogrid('clear').combogrid('grid').datagrid('reload', '/biz_client_contact/get_option_by_role/' + row.client_code + '/?roles=logistics_client,client,factory,oversea_client,oversea_agent');
        $('#client_telephone').val('');
        $('#client_email').val('');

        $('#sales_id').combogrid('clear').combogrid('grid').datagrid('reload', "/bsc_user/get_client_user?client_code=" + row.client_code + "&role=sales");
        $('#sales_group').val('');
        var sales_id = $('#sales_id').combogrid('getValue');

        $('#operator_id').combogrid('clear').combogrid('grid').datagrid('reload', "/bsc_user/get_client_user?client_code=" + row.client_code + "&role=operator&sales_id=" + sales_id);
        $('#operator_group').val('');

        $('#customer_service_id').combogrid('clear').combogrid('grid').datagrid('reload', "/bsc_user/get_client_user?client_code=" + row.client_code + "&role=customer_service&sales_id=" + sales_id);
        $('#customer_service_group').val('');

        $('#client_code2').combogrid('clear').combogrid('grid').datagrid('reload', "/biz_client_second_relation/getSecondByClient?client_code=" + row.client_code);
    }

    function client_contact_select(index, row) {
        if (row.email == ''){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('未填写联系人邮箱, 选择失败');?>');
            return false;
        }else {
            $('#client_telephone').val(row.telephone);
            $('#client_email').val(row.email);
        }
    }

    function operator_id_select(index, row){
        $('#operator_group').val(row.group);
    }
    function customer_service_id_select(index, row){
        $('#customer_service_group').val(row.group);
    }
    function sales_id_select(index, row){
        $('#sales_group').val(row.group);
        var client_code = $('#client_code').combogrid('getValue');

        $('#operator_id').combogrid('grid').datagrid('reload', "/bsc_user/get_client_user?client_code=" + client_code + "&role=operator&sales_id=" + row.id);
        $('#customer_service_id').combogrid('grid').datagrid('reload', "/bsc_user/get_client_user?client_code=" + client_code + "&role=customer_service&sales_id=" + row.id);
    }

    //text添加输入值改变
    function keydown_search(e, click_id){
        if(e.keyCode == 13){
            $('#' + click_id).trigger('click');
        }
    }

    function add_box() {
        var length = $('.add_box').length;
        var lang = '';
        var btn = '';
        if(length <= 0){
            lang = "<?= lang('箱型箱量');?>:";
            btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
        }else{
            btn = '<a class="easyui-linkbutton" onclick="del_box(this)" iconCls="icon-remove"></a>';
        }
        var str = "<div class=\"box-left-input add_box\">\n" +
            "                <label class=\"box-left-input-label\" for=\"\">" + lang + "</label>\n" +
            "                <select class=\"easyui-combobox box_info_size\" name=\"box_info[size][]\" style=\"width: 100px\" data-options=\"\n" +
            "                    required:true,\n" +
            "                    valueField:'value',\n" +
            "                    textField:'value',\n" +
            "                    url:'/bsc_dict/get_option/container_size',\n" +
            "                    onHidePanel: function() {\n" +
            "                        var valueField = $(this).combobox('options').valueField;\n" +
            "                        var val = $(this).combobox('getValue');\n" +
            "                        var allData = $(this).combobox('getData');\n" +
            "                        var result = true;\n" +
            "                        for (var i = 0; i < allData.length; i++) {\n" +
            "                            if (val == allData[i][valueField]) {\n" +
            "                                result = false;\n" +
            "                            }\n" +
            "                        }\n" +
            "                        if (result) {\n" +
            "                            $(this).combobox('clear');\n" +
            "                        }\n" +
            "                    }\n" +
            "                \">\n" +
            "                </select>\n" +
            "                X\n" +
            "                <input class=\"easyui-numberbox box_info_num\" required name=\"box_info[num][]\" >\n" + btn +
            "            </div>";
        $('.add_box:eq(' + (length - 1) + ')').after(str);
        $.parser.parse('.add_box:eq(' + length + ')');
    }

    function del_box(e) {
        if(e == undefined){
            $('.add_box').remove();
        }else{
            $(e).parents('.add_box').remove();
        }
    }

    function add_shipment(){
        window.open('/biz_shipment/add?ebooking_id=<?= $id;?>');
    }

    function cancel(){
        if(is_submit){
            return;
        }
        $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认拒绝该Eboooking?');?>', function (r) {
            if(r){
                submit_start();
                $.ajax({
                    url: '/biz_shipment_ebooking/cancel_status?id=<?= $id;?>',
                    type: 'post',
                    data:{
                        status: -1,
                    },
                    success: function (res_json) {
                        var res;
                        try{
                            res = $.parseJSON(res_json);
                        }catch(e){

                        }
                        if(res == undefined){
                            $.messager.alert('<?= lang('提示');?>', res);
                            return;
                        }
                        submit_end();
                        $.messager.alert('<?= lang('提示');?>', res.msg);
                        if(res.code == 0){
                            location.reload();
                        }else{
                        }
                    },
                });
            }
        });
    }

    function service_option() {
        return $.ajax({
            url: '/bsc_dict/get_option/service_option',
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                var str = '<table style="float: left;">';
                var service_options_arr = [];
                try {
                    service_options_arr = $.parseJSON($('#service_options').val());
                } catch(e) {
                }
                var service_options = [];
                $.each(service_options_arr, function(i, it){
                    service_options.push(it[0]);
                });
                str += '<tr style="text-align: center"><th>POL</th></tr>';
                $.each(res, function (index, item) {
                    if(item.orderby <= 100){
                        str += '<tr>';
                        var checked = '';
                        if(item.name !== '' && item.value !== ''){
                            if($.inArray(item.value, service_options) !== -1){
                                checked = 'checked';
                            }
                            str += '<td><input class="service_options" type="checkbox" name="service_options[' + index + '][]" value="' + item.value + '" style="vertical-align:middle;" ' + checked + '>' + item.lang + '</td>';
                        }
                        str += '</tr>';
                    }
                });
                str += '</table>';
                str += '<table style="float: left;">';
                str += '<tr style="text-align: center"><th>POD</th></tr>';
                $.each(res, function (index, item) {
                    if(item.orderby > 100){
                        str += '<tr>';
                        var checked = '';
                        if(item.name !== '' && item.value !== ''){
                            if($.inArray(item.value, service_options) !== -1){
                                checked = 'checked';
                            }
                            str += '<td><input class="service_options" type="checkbox" name="service_options[' + index + '][]" value="' + item.value + '" style="vertical-align:middle;" ' + checked + '>' + item.lang + '</td>';
                        }
                        str += '</tr>';
                    }
                });
                str += '</table>';
                $('#service_options_div').append(str);

            }
        });
    }

    function trans_carrier_select(rec){
        var trans_mode = $('#trans_mode').val();
        var trans_tool = $('#trans_tool').combobox('getValue');
        $('#trans_origin_name').combogrid('grid').datagrid('load', "/biz_port/get_option?type=" + trans_tool + "&carrier=" + rec.client_code + '&limit=true');

        $('#trans_destination').combogrid('grid').datagrid('reload', "/biz_port/get_discharge?type=" + trans_tool + "&carrier=" + rec.client_code);
        $('#trans_destination_inner').textbox('clear');
        $('#trans_destination').combogrid('clear');
        $('#trans_destination_name').textbox('clear');
        $('#trans_destination_terminal').textbox('clear');
    }

    function add_free_svr() {
        var length = $('.free_svr').length;

        var btn = '';
        var lang = '';
        if(length < 4){
            if(length == 0){
                lang = '<?= lang('免箱期');?>';
            }
            if(length < 3){
                btn = '<a class="easyui-linkbutton" onclick="add_free_svr(this)" iconCls="icon-add"></a>';
            }
        }else{
            return;
        }
        var str = '  <div class="free_svr">\n' +
            '                        <label class="box-left-input-label" for="">' + lang + '</label>\n' +
            '                        <select editable="false" class="easyui-combobox free_svr_type" name="free_svr[' + length + '][type]" data-options="\n' +
            '                            valueField:\'value\',\n' +
            '                            textField:\'name\',\n' +
            '                            url: \'/bsc_dict/get_option/free_svr\',\n' +
            '                            onSelect: function(rec){\n' +
            '                                free_svr_type_jc();\n' +
            '                            },\n' +
            '                        ">\n' +
            '                        </select>\n' +
            '                        <input class="easyui-numberspinner free_svr_days" name="free_svr[' + length + '][days]" data-options="min:1,max:30"><?= lang('天');?>\n'+
            btn +
            '                    </div>';

        $('.free_svr:eq(' + (length - 1) + ')').after(str);
        $.parser.parse('.free_svr:eq(' + length + ')');
    }

    function free_svr_type_jc() {
        var free_svr_type = $('.free_svr_type')
        var data = [];
        var jc = false;
        $.each(free_svr_type, function(index,item){
            var val = $(this).combobox('getValue');
            if(val != ''){
                if($.inArray(val, data) === -1){
                    data.push(val);
                }else{
                    jc = true;
                }
            }

        });
        if(jc === true){
            free_svr_cf = true;
        }else{
            free_svr_cf = false
        }
    }

    /**
     * 下拉选择表格展开时,自动让表单内的查询获取焦点
     */
    function combogrid_search_focus(form_id, focus_num = 0){
        // var form = $(form_id);
        setTimeout("easyui_focus($($('" + form_id + "').find('.keydown_search')[" + focus_num + "]));",200);
    }

    /**
     * easyui 获取焦点
     */
    function easyui_focus(jq){
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combogrid !== undefined){
            return $(jq).combogrid('textbox').focus();
        }else if(data.combobox !== undefined){
            return $(jq).combobox('textbox').focus();
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('textbox').focus();
        }else if(data.datebox !== undefined){
            return $(jq).datebox('textbox').focus();
        }else if(data.textbox !== undefined){
            return $(jq).textbox('textbox').focus();
        }
    }

    $(function () {
        service_option();

        //进入下一步操作
        $('#step2').click(function () {
            var id = <?=(int)$id;?>;
            $.ajax({
                url: '/biz_shipment_ebooking/update_status',
                type: 'POST',
                data: {id:id, status:0},
                dataType: 'json',
                success: function (data) {
                    if (data.code == 1){
                        $.messager.show({
                            title:'<?= lang('提示');?>',
                            msg:'<?= lang('操作成功。');?>',
                            timeout:2000,
                            showType:null,
                            style:{
                                right:'',
                                bottom:''
                            }
                        });

                        setTimeout(function () {
                            //刷新页面
                            window.location.reload();
                        }, 2000);

                    }else{
                        $.messager.alert("<?= lang('提示');?>", '<?= lang('操作失败');?>：'+data.msg, 'error');
                        _submit = false;
                    }
                },
                error: function (xhr, status, error) {
                    $.messager.alert("<?= lang('提示');?>", "<?= lang('操作失败');?>"); //xhr.responseText
                }
            });
        });

        var now = Date.parse(new Date()) / 1000;

        //订舱船期范围设置
        $('#booking_ETD').datebox('calendar').calendar({
            //到时候不存在可选择值的，在这里false
            validator: function(date){
                //日期不能超过前后半年
                var star = now - 3600 * 24 * 183;
                var end = now + 3600 * 24 * 183;
                var date_time = Date.parse(date) / 1000;
                if (date_time >= star && date_time <= end) {
                    return true;
                } else {
                    return false;
                }
            }
        });

        $.each($('.keydown_search'), function (i,it) {
            $(it).textbox('textbox').bind('keydown', function(e){keydown_search(e, $(it).attr('t_id'));});
        });
    });
</script>
<body>
<div class="box">
    <div class="box-left">
        <form id="fm" method="post">
            <input type="hidden" name="rand_id" id="rand_id" value="<?= $rand_id;?>">
            <input type="hidden" name="biz_type" id="biz_type" value="<?= $biz_type;?>">
            <input type="hidden" name="trans_mode" id="trans_mode" value="<?= $trans_mode;?>">
            <input type="hidden" name="trans_tool" id="trans_tool" value="<?= $trans_tool;?>">
            <div class="box-title">
                <span class="box-title-span"><?= lang('其他下单');?></span>
            </div>
            <div class="box-hr">
                <hr>
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('委托方');?>:</label>
                <select class="easyui-combogrid select" id="client_code" name="client_code" data-options="
                        panelWidth: '1000px',
                        panelHeight: '300px',
                        url:'/biz_client/get_option/client?other_role=factory,logistics_client,oversea_client,oversea_agent',
                        idField: 'client_code',              //ID字段
                        textField: 'company_name',    //显示的字段
                        fitColumns : true,
                        striped: true,
                        editable: false,
                        pagination: false,           //是否分页
                        toolbar : '#client_code_div',
                        collapsible: true,         //是否可折叠的
                        method: 'post',
                        columns:[[
                            {field:'client_code',title:'<?= lang('client_code');?>',width:100},
                            {field:'company_name',title:'<?= lang('company_name');?>',width:200},
                        ]],
                        emptyMsg : '<?= lang('未找到相应数据!');?>',
                        required:true,
                        mode: 'remote',
                        onSelect: client_code_select,
                        onBeforeLoad:function(param){
                            var val = $('#client_code').combogrid('getValue');
                            if(val != '' && Object.keys(param).length == 0){
                                param.client_code = val;
                            }
                            if(val == '' && Object.keys(param).length == 0) return false;
                        },
                        onShowPanel:function(){
                            var opt = $(this).combogrid('options');
                            var toolbar = opt.toolbar;
                            combogrid_search_focus(toolbar);
                        },
                        value:'<?= $client_code;?>',
                    ">
                </select>
                <input type="hidden" name="client_company" id="client_company" value="<?= $client_company;?>">
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('客户联系人');?>:</label>
                <select class="easyui-combogrid select" id="client_contact" name="client_contact" data-options="
                    panelWidth: '1000px',
                    panelHeight: '300px',
                    <?php if($client_code != '') echo "url:'/biz_client_contact/get_option_by_role/{$client_code}/?roles=logistics_client,client,factory,oversea_client,oversea_agent',"; ?>
                    idField: 'name',              //ID字段
                    textField: 'name',    //显示的字段
                    fitColumns : true,
                    striped: true,
                    editable: false,
                    pagination: false,           //是否分页
                    toolbar : '#client_contact_div',
                    collapsible: true,         //是否可折叠的
                    method: 'post',
                    columns:[[
                        {field:'name',title:'<?= lang('name');?>',width:100},
                        {field:'email',title:'<?= lang('email');?>',width:200},
                        {field:'telephone',title:'<?= lang('telephone');?>',width:200},
                    ]],
                    value:'<?= $client_contact;?>',
                    emptyMsg : '<?= lang('未找到相应数据!');?>',
                    required:true,
                    mode: 'remote',
                    onSelect: client_contact_select,
                    onShowPanel:function(){
                        var client_code = $('#client_code').combogrid('getValue');
                        if(client_code == ''){
                            $(this).combogrid('hidePanel');
                            $.messager.alert('<?= lang('提示');?>', '<?= lang('请先选择委托方');?>');
                            return false;
                        }
                        var opt = $(this).combogrid('options');
                        var toolbar = opt.toolbar;
                        combogrid_search_focus(toolbar);
                    },
                ">
                </select>
                <input type="hidden" name="client_telephone" id="client_telephone" value="<?= $client_telephone;?>">
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('邮箱');?>:</label>
                <input type="text" readonly="true" name="client_email" id="client_email" value="<?= $client_email;?>" style="width:250px;">
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('销售');?>:</label>
                <select class="easyui-combogrid select" id="sales_id" name="sales_id" data-options="
                    panelWidth: '1000px',
                    panelHeight: '300px',
                    idField: 'id',              //ID字段
                    textField: 'name',    //显示的字段
                    striped: true,
                    editable: false,
                    pagination: false,           //是否分页
                    toolbar : '#sales_id_div',
                    collapsible: true,         //是否可折叠的
                    method: 'post',
                    columns:[[
                        {field:'id',title:'<?= lang('id');?>',width:60},
                        {field:'name',title:'<?= lang('name');?>',width:80},
                        {field:'group',title:'<?= lang('group');?>',width:100},
                    ]],
                    emptyMsg : '<?= lang('未找到相应数据!');?>',
                    required:true,
                    mode: 'remote',
                    value: '<?= $sales_id;?>',
                    onSelect: sales_id_select,
                    onShowPanel:function(){
                        var client_code = $('#client_code').combogrid('getValue');
                        if(client_code == ''){
                            $(this).combogrid('hidePanel');
                            $.messager.alert('<?= lang('提示');?>', '<?= lang('请先选择委托方');?>');
                            return false;
                        }
                        var opt = $(this).combogrid('options');
                        var toolbar = opt.toolbar;
                        combogrid_search_focus(toolbar);
                    },
                ">
                </select>
                <input type="hidden" name="sales_group" id="sales_group" value="<?= $sales_group;?>">
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('操作');?>:</label>
                <select class="easyui-combogrid select" id="operator_id" name="operator_id" data-options="
                    panelWidth: '1000px',
                    panelHeight: '300px',
                    idField: 'id',              //ID字段
                    textField: 'name',    //显示的字段
                    striped: true,
                    editable: false,
                    pagination: false,           //是否分页
                    toolbar : '#operator_id_div',
                    collapsible: true,         //是否可折叠的
                    method: 'post',
                    columns:[[
                        {field:'id',title:'<?= lang('id');?>',width:60},
                        {field:'name',title:'<?= lang('name');?>',width:80},
                        {field:'group',title:'<?= lang('group');?>',width:100},
                    ]],
                    emptyMsg : '<?= lang('未找到相应数据!');?>',
                    required:true,
                    mode: 'remote',
                    onSelect: operator_id_select,
                    onShowPanel:function(){
                        var client_code = $('#client_code').combogrid('getValue');
                        if(client_code == ''){
                            $(this).combogrid('hidePanel');
                            $.messager.alert('<?= lang('提示');?>', '<?= lang('请先选择委托方');?>');
                            return false;
                        }
                        var sales_id = $('#sales_id').combogrid('getValue');
                        if(sales_id == '' || sales_id == '0'){
                            $(this).combogrid('hidePanel');
                            $.messager.alert('<?= lang('提示');?>', '<?= lang('请先选择销售');?>');
                            return false;
                        }
                        var opt = $(this).combogrid('options');
                        var toolbar = opt.toolbar;
                        combogrid_search_focus(toolbar);
                    },
                    url:'/bsc_user/get_client_user?client_code=<?= $client_code;?>&role=operator&sales_id=<?= $sales_id;?>',
                    value:'<?= $operator_id;?>',
                ">
                </select>
                <input type="hidden" name="operator_group" id="operator_group" value="<?= $operator_group;?>">
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('客服');?>:</label>
                <select class="easyui-combogrid select" id="customer_service_id" name="customer_service_id" data-options="
                    panelWidth: '1000px',
                    panelHeight: '300px',
                    idField: 'id',              //ID字段
                    textField: 'name',    //显示的字段
                    striped: true,
                    editable: false,
                    pagination: false,           //是否分页
                    toolbar : '#customer_service_id_div',
                    collapsible: true,         //是否可折叠的
                    method: 'post',
                    columns:[[
                        {field:'id',title:'<?= lang('id');?>',width:60},
                        {field:'name',title:'<?= lang('name');?>',width:80},
                        {field:'group',title:'<?= lang('group');?>',width:100},
                    ]],
                    emptyMsg : '<?= lang('未找到相应数据!');?>',
                    required:true,
                    mode: 'remote',
                    url:'/bsc_user/get_client_user?client_code=<?= $client_code;?>&role=customer_service&sales_id=<?= $sales_id;?>',
                    value: '<?= $customer_service_id;?>',
                    onSelect: customer_service_id_select,
                    onShowPanel:function(){
                        var client_code = $('#client_code').combogrid('getValue');
                        if(client_code == ''){
                            $(this).combogrid('hidePanel');
                            $.messager.alert('<?= lang('提示');?>', '<?= lang('请先选择委托方');?>');
                            return false;
                        }
                        var sales_id = $('#sales_id').combogrid('getValue');
                        if(sales_id == '' || sales_id == '0'){
                            $(this).combogrid('hidePanel');
                            $.messager.alert('<?= lang('提示');?>', '<?= lang('请先选择销售');?>');
                            return false;
                        }
                        var opt = $(this).combogrid('options');
                        var toolbar = opt.toolbar;
                        combogrid_search_focus(toolbar);
                    },
                ">
                </select>
                <input type="hidden" name="customer_service_group" id="customer_service_group" value="<?= $customer_service_group;?>">
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('二级委托方');?><a href="javascript:void;" class="easyui-tooltip" data-options="
                    content: $('<div></div>'),
                    onShow: function(){
                        $(this).tooltip('arrow').css('left', 20);
                        $(this).tooltip('tip').css('left', $(this).offset().left);
                    },
                    onUpdate: function(cc){
                        cc.panel({
                            width: 300,
                            height: 'auto',
                            border: false,
                            href: '/bsc_help_content/help/second_client'
                        });
                    }
                "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>:</label>
                <select name="client_code2" id="client_code2" class="easyui-combogrid select" data-options="
                    panelWidth: '1000px',
                    panelHeight: '300px',
                    url:'/biz_client_second_relation/getSecondByClient?client_code=<?= $client_code;?>',
                    value: '<?= $client_code2;?>',
                    idField: 'client_code',              //ID字段
                    textField: 'company_name',    //显示的字段
                    striped: true,
                    editable: false,
                    pagination: false,           //是否分页
                    toolbar : '#client_code2_div',
                    collapsible: true,         //是否可折叠的
                    method: 'post',
                    columns:[[
                        {field:'client_code',title:'<?= lang('client_code');?>',width:100},
                        {field:'company_name',title:'<?= lang('company_name');?>',width:200},
                    ]],
                    emptyMsg : '<?= lang('未找到相应数据!');?>',
                    required:true,
                    mode: 'remote',
                    onShowPanel:function(){
                        var client_code = $('#client_code').combogrid('getValue');
                        if(client_code == ''){
                            $(this).combogrid('hidePanel');
                            $.messager.alert('<?= lang('提示');?>', '<?= lang('请先选择委托方');?>');
                            return false;
                        }
                        var opt = $(this).combogrid('options');
                        var toolbar = opt.toolbar;
                        combogrid_search_focus(toolbar);
                    },
                ">
                </select>
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('承运人');?>:</label>
                <select class="easyui-combobox select" name="trans_carrier" id="trans_carrier" data-options="
                    required:true,
                    valueField:'client_code',
				    textField:'client_name',
                    url:'/biz_client/get_option/carrier',
                    value:'<?php echo $trans_carrier;?>',
                    onSelect:trans_carrier_select,
                    onHidePanel: function() {
                        var valueField = $(this).combobox('options').valueField;
                        var val = $(this).combobox('getValue');
                        var allData = $(this).combobox('getData');
                        var result = true;
                        for (var i = 0; i < allData.length; i++) {
                            if (val == allData[i][valueField]) {
                                result = false;
                            }
                        }
                        if (result) {
                            $(this).combobox('clear');
                        }
                    },
                ">
                </select>
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('起运港');?>:</label>
                <input class="easyui-textbox" style="width: 70px;" name="trans_origin" id="trans_origin" readonly value="<?= $trans_origin;?>">
                <select class="easyui-combogrid" style="width: 187px;" name="trans_origin_name" id="trans_origin_name" data-options="
                    required:true,
                    panelWidth: '1000px',
                    panelHeight: '300px',
                    url:'/biz_port/get_option?type=<?= $trans_tool;?>&carrier=<?= $trans_carrier;?>&limit=true',
                    idField: 'port_name',              //ID字段
                    textField: 'port_name',    //显示的字段
                    striped: true,
                    editable: false,
                    pagination: false,           //是否分页
                    toolbar : '#trans_origin_div',
                    collapsible: true,         //是否可折叠的
                    method: 'get',
                    columns:[[
                        {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                        {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                        {field:'terminal',title:'<?= lang('码头');?>',width:200},
                    ]],
                    mode: 'remote',
                    value: '<?= $trans_origin_name;?>',
                    emptyMsg : '<?= lang('未找到相应数据!');?>',
                    required:true,
                    onSelect: function(index, row){
                        if(row !== undefined){
                            $('#trans_origin').textbox('setValue', row.port_code);
                        }
                    },
                    onShowPanel:function(){
                        var opt = $(this).combogrid('options');
                        var toolbar = opt.toolbar;
                        combogrid_search_focus(toolbar);
                    },
                ">
                </select>
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('目的港代码');?>:</label>
                <input class="easyui-textbox" readonly style="width: 70px;" id="trans_destination_inner" name="trans_destination_inner" value="<?= $trans_destination_inner;?>">
                <select class="easyui-combogrid" style="width: 70px;" name="trans_destination" id="trans_destination" data-options="
                    required:true,
                    panelWidth: '1000px',
                    panelHeight: '300px',
                    idField: 'port_code',              //ID字段
                    textField: 'port_code',    //显示的字段
                    url:'/biz_port/get_discharge?type=<?= $trans_tool;?>&carrier=<?= $trans_carrier?>',
                    striped: true,
                    editable: false,
                    pagination: false,           //是否分页
                    toolbar : '#trans_destination_div',
                    collapsible: true,         //是否可折叠的
                    method: 'get',
                    columns:[[
                        {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                        {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                        {field:'terminal',title:'<?= lang('码头');?>',width:200},
                    ]],
                    mode: 'remote',
                    value: '<?= $trans_destination;?>',
                    emptyMsg : '<?= lang('未找到相应数据!');?>',
                    required:true,
                    onSelect: function(index, row){
                        if(row !== undefined){
                            $('#trans_destination_inner').textbox('setValue', row.inner_port_code);
                            $('#trans_destination_name').textbox('setValue', row.port_name);
                            $('#trans_destination_terminal').textbox('setValue', row.terminal);
                        }
                    },
                    onShowPanel:function(){
                        var opt = $(this).combogrid('options');
                        var toolbar = opt.toolbar;
                        combogrid_search_focus(toolbar, 1);
                    },
                ">
                </select>
                <input class="easyui-textbox" style="width: 112px;" name="trans_destination_name" id="trans_destination_name" value="<?= $trans_destination_name;?>">
                <a href="javascript:void;" class="easyui-tooltip" data-options="
                    content: $('<div></div>'),
                    onShow: function(){
                        $(this).tooltip('arrow').css('left', 20);
                        $(this).tooltip('tip').css('left', $(this).offset().left);
                    },
                    onUpdate: function(cc){
                        cc.panel({
                            width: 300,
                            height: 'auto',
                            border: false,
                            href: '/bsc_help_content/help/discharge_code_inner'
                        });
                    }
                "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('目的港码头');?>:</label>
                <input class="easyui-textbox" style="width: 260px;" name="trans_destination_terminal" id="trans_destination_terminal" value="<?= $trans_destination_terminal;?>">
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('订舱船期');?>:</label>
                <input class="easyui-datebox input" data-options="required:true,editable:false" name="booking_ETD" id="booking_ETD" value="<?php echo $booking_ETD;?>"/>
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('客户单号');?>:</label>
                <input class="easyui-textbox input" name="shipper_ref" id="shipper_ref" value="<?= $shipper_ref;?>">
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('关单号');?>:</label>
                <input class="easyui-textbox input" name="cus_no" id="cus_no" value="<?= $cus_no;?>">
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('MBL');?>:</label>
                <input class="easyui-textbox input" name="carrier_ref" id="carrier_ref" value="<?= $carrier_ref;?>">
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('提单类型');?>:</label>
                <select class="easyui-combobox select" name="hbl_type" id="hbl_type" data-options="
                        required:true,
                        valueField:'value',
                        textField:'name',
                        url:'/bsc_dict/get_hbl_type',
                        value: '<?= $hbl_type;?>',
                        onSelect:function(rec){
                            if(rec.value != 'MBL'){
                                //如果不是MBL,这里自动出现AGENT
                                $('.agent').css('display', 'block');
                            }else{
                                //反之隐藏且清空
                                $('.agent').css('display', 'none');
                                $('#agent_code').combogrid('clear');
                            }
                        }
                    ">
                </select>
            </div>
            <?php if(isset($box_info) && !empty($box_info)){foreach($box_info as $key => $row){
                $this_btn = '<a class="easyui-linkbutton" onclick="del_box(this)" iconCls="icon-remove"></a>';
                $this_lang = '';
                if($key == 0){
                    $this_btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
                    $this_lang = lang('箱型箱量') . ':';
                }
                ?>
                <div class="box-left-input add_box">
                    <label class="box-left-input-label" for=""><?= $this_lang;?></label>
                    <select class="easyui-combobox box_info_size" name="box_info[size][]" style="width: 100px" data-options="
                    valueField:'value',
                    textField:'value',
                    value: '<?= $row['size']?>',
                    url:'/bsc_dict/get_option/container_size',
                    onHidePanel: function() {
                        var valueField = $(this).combobox('options').valueField;
                        var val = $(this).combobox('getValue');
                        var allData = $(this).combobox('getData');
                        var result = true;
                        for (var i = 0; i < allData.length; i++) {
                            if (val == allData[i][valueField]) {
                                result = false;
                            }
                        }
                        if (result) {
                            $(this).combobox('clear');
                        }
                    }
                ">
                    </select>
                    X
                    <input class="easyui-numberbox box_info_num" name="box_info[num][]" value="<?= $row['num']?>">
                    <?= $this_btn;?>
                </div>
            <?php }} ?>
            <?php if(isset($free_svr) && !empty($free_svr)){
                foreach($free_svr as $key => $row){
                    $this_btn = '<a class="easyui-linkbutton" onclick="add_free_svr()" iconCls="icon-add"></a>';
                    $this_lang = '';
                    if($key < 3){
                        if($key == 0){
                            $this_lang = lang('免箱期');
                        }
                    }else{
                        $this_btn = '';
                    }?>
                    <div class="free_svr">
                        <label class="box-left-input-label" for=""><?= $this_lang;?>:</label>
                        <select editable="false" class="easyui-combobox free_svr_type" name="free_svr[<?= $key;?>][type]" data-options="
                            valueField:'value',
                            textField:'name',
                            url: '/bsc_dict/get_option/free_svr',
                            value: '<?= $row['type'];?>',
                            onSelect: function(rec){
                                free_svr_type_jc();
                            },
                        ">
                        </select>
                        <input class="easyui-numberspinner free_svr_days" name="free_svr[<?= $key;?>][days]" data-options="min:1,max:30" value="<?= $row['days'];?>"><?= lang('天');?>
                        <?= $this_btn;?>
                    </div>
                <?php }
            }else{ ?>
                <div class="free_svr">
                    <label class="box-left-input-label" for=""><?= lang('免箱期');?>:</label>
                    <select editable="false" class="easyui-combobox free_svr_type" name="free_svr[0][type]" data-options="
                        valueField:'value',
                        textField:'name',
                        url: '/bsc_dict/get_option/free_svr',
                        onSelect: function(rec){
                            free_svr_type_jc();
                        },
                    ">
                    </select>
                    <input class="easyui-numberspinner free_svr_days" name="free_svr[0][days]" data-options="min:1,max:30"><?= lang('天');?>
                    <a class="easyui-linkbutton" onclick="add_free_svr()" iconCls="icon-add"></a>
                </div>
            <?php }?>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('运输条款');?>:</label>
                <select class="easyui-combobox select" name="trans_term" id="trans_term" data-options="
                        required:true,
                        value:'<?= $trans_term;?>',
                        valueField:'value',
                        textField:'value',
                        url:'/bsc_dict/get_option/trans_term',
                    ">
                </select>
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('贸易方式');?>:</label>
                <select class="easyui-combobox select" name="INCO_term" id="INCO_term" data-options="
                        required:true,
                        valueField:'value',
                        textField:'value',
                        url:'/bsc_dict/get_option/INCO_term',
                        value: '<?= $INCO_term;?>',
                    ">
                </select>
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for="" style="position: relative;top: -95px;"><?= lang('订舱要求');?>:</label>
                <textarea class="textarea" name="requirements" id="requirements"><?= $requirements;?></textarea>
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for="" style="position: relative;top: -95px;"><?= lang('目的港提/送货地址');?>:</label>
                <textarea class="textarea" name="INCO_address" id="INCO_address"><?= $INCO_address;?></textarea>
            </div>
            <div class="box-left-input">
                <label class="box-left-input-label" for=""><?= lang('货物类型');?>:</label>
                <select class="easyui-combobox select" name="goods_type" id="goods_type" data-options="
                    required: true,
                    valueField:'value',
                    textField:'valuename',
                    value: '<?= $goods_type;?>',
                    url:'/bsc_dict/get_option/goods_type',
                ">
                </select>
            </div>
            <div class="box-left-input" style="text-align: right;">
                <?php if($status != 1 && $status != 0):?>
                    <a href="javascript:void(0);" class="easyui-linkbutton" onclick="submit()"><?= lang('修改数据');?></a>
                <?php endif;?>
                <?php if($status == -1):?>
                    <a href="javascript:void(0);" class="easyui-linkbutton" onclick="rebook()"><?= lang('撤回');?></a>
                <?php endif;?>
                <?php if((in_array($sales_id, get_session('user_range')) || in_array($operator_id, get_session('user_range')) || is_admin()) && $status == 0){ ?>
                    <a href="javascript:void(0);" class="easyui-linkbutton" onclick="cancel()"><?= lang('拒绝订舱');?></a>
                <?php } ?>
                <?php if((in_array($sales_id, get_session('user_range')) || in_array($customer_service_id, get_session('user_range')) || is_admin()) && $status == -2){ ?>
                    <a href="javascript:void(0);" class="easyui-linkbutton" onclick="operator_apply()"><?= lang('提交给操作');?></a>
                <?php } ?>
                <?php if((in_array($operator_id, get_session('user_range')) || is_admin()) && $status == 0){ ?>
					<a href="javascript:void(0);" class="easyui-linkbutton" onclick="add_shipment()"><?= lang('create shipment');?></a>
                <?php } ?>
            </div>
        </form>
    </div>
    <div class="box-right">
        <div class="box-title">
            <span class="box-title-span"><?= lang('委托书上传');?></span>
        </div>
        <div class="box-hr">
            <hr>
        </div>
        <div class="box-right-iframe">
            <iframe src="/bsc_upload/upload_files?biz_table=biz_shipment_ebooking&id_no=<?= $id?>" style="width:710px;height:675px;padding:5px;border:0;">
            </iframe>
        </div>
    </div>
</div>
<div id="client_code_div">
    <form id="client_code_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('客户代码');?>:</label><input name="client_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="client_code_click"/>
            <label><?= lang('客户全称');?>:</label><input name="company_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="client_code_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="client_code_click" iconCls="icon-search" onclick="query_report('client_code_form', 'client_code')"><?= lang('查询');?></a>
        </div>
    </form>
</div>
<div id="client_contact_div">
    <form id="client_contact_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('名称');?>:</label><input name="name" class="easyui-textbox keydown_search" style="width:96px;" t_id="client_contact_click"/>
            <label><?= lang('邮箱');?>:</label><input name="email" class="easyui-textbox keydown_search" style="width:96px;" t_id="client_contact_click"/>
            <label><?= lang('电话');?>:</label><input name="telephone" class="easyui-textbox keydown_search" style="width:96px;" t_id="client_contact_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="client_contact_click" iconCls="icon-search" onclick="query_report('client_contact_form', 'client_contact')"><?= lang('查询');?></a>
            <a id="edit_contact_link" href="/biz_client_contact/index/<?php echo $client_code;?>" target="_blank" style="color: #00bbee; margin-left: 20px;"><?= lang('编辑联系人');?></a>
        </div>
    </form>
</div>
<div id="client_code2_div">
    <form id="client_code2_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('客户代码');?>:</label><input name="client_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="client_code2_click"/>
            <label><?= lang('客户全称');?>:</label><input name="company_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="client_code2_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="client_code2_click" iconCls="icon-search" onclick="query_report('client_code2_form', 'client_code2')"><?= lang('查询');?></a>
        </div>
    </form>
</div>
<div id="trans_origin_div">
    <form id="trans_origin_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_origin_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_origin_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_origin_click" iconCls="icon-search" onclick="query_report('trans_origin_form', 'trans_origin_name')"><?= lang('查询');?></a>
        </div>
    </form>
</div>
<div id="trans_destination_div">
    <form id="trans_destination_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_destination_click" iconCls="icon-search" onclick="query_report('trans_destination_form', 'trans_destination')"><?= lang('查询');?></a>
        </div>
    </form>
</div>
<div id="operator_id_div">
    <form id="operator_id_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('名称');?>:</label><input name="name" class="easyui-textbox keydown_search" style="width:96px;" t_id="operator_id_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="operator_id_click" iconCls="icon-search" onclick="query_report('operator_id_form', 'operator_id')"><?= lang('查询');?></a>
        </div>
    </form>
</div>
<div id="customer_service_id_div">
    <form id="customer_service_id_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('名称');?>:</label><input name="name" class="easyui-textbox keydown_search" style="width:96px;" t_id="customer_service_id_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="customer_service_id_click" iconCls="icon-search" onclick="query_report('customer_service_id_form', 'customer_service_id')"><?= lang('查询');?></a>
        </div>
    </form>
</div>
<div id="sales_id_div">
    <form id="sales_id_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('名称');?>:</label><input name="name" class="easyui-textbox keydown_search" style="width:96px;" t_id="sales_id_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="sales_id_click" iconCls="icon-search" onclick="query_report('sales_id_form', 'sales_id')"><?= lang('查询');?></a>
        </div>
    </form>
</div>
<div id="service_option_tab" class="easyui-accordion" data-options="multiple:true" style="width:300px;position: absolute;right: 10px;top: 10px;z-index: 999;">
    <div title="Service Option" id="service_options_div" style="padding:10px;">
        <input type="hidden" id="service_options" value='<?= $service_options;?>'>
    </div>
</div>
</body>
