<?php

/**
 * efreight 报文相关
 */
class bsc_message_efreight extends Common_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bsc_message_model');
        $this->model = $this->bsc_message_model;
    }

    public $unit_codes = array(
        'PACKAGES' => 'PKG',
        'PACKAGE' => 'PKG',
        'CARTONS' => 'CTN',
        'CARTON' => 'CTN',
        'CASE' => 'CAS',
        'CASES' => 'CAS',
        'IRON CASES' => 'CAS',
        'IRON CASE' => 'CAS',
        'WOODEN CASE' => 'CAS',
        'WOODEN CASES' => 'CAS',
        'PLYWOOD CASE' => 'CAS',
        'PLYWOOD CASES' => 'CAS',
        'WOODEN CRATES' => 'CRT',
        'PIECE' => 'PCS',
        'PIECES' => 'PCS',
        'BAGS' => 'BAG',
        'BAG' => 'BAG',
        'ROLLS' => 'ROL',
        'ROLL' => 'ROL',
        'DRUMS' => 'DRM',
        'DRUM' => 'DRM',
        'CRATE' => 'CRT',
        'CRATES' => 'CRT',
        'REEL' => 'REL',
        'REELS' => 'REL',
        'FIBRE DRUMS' => 'DRM',
        'SETS' => 'SET',
        'BOX' => 'BOX',
        'BUNDLE' => 'BDL',
        'BUNDLES' => 'BDL',
        'BARRELS' => 'BBL',
        'BARREL' => 'BBL',
    );

    public $container_size_codes = array(
        '20GP' => '22G0',
        '40GP' => '42G0',
        '40FH' => '4EU0',
        '40HQ' => '45G0',
        '45HQ' => 'L5G0',
        '40RF' => '42R1',
        '40FR' => '42P1',
        '40OT' => '42U0',
        '20OT' => '22U0',
        '40RH' => '45R1',
        '20TK' => '22T0',
        '20FR' => '22P1',
    );
    
    public $OriginAgentCodes = array(
        'vn' => 'VN',
    );

    public function ams_set_data($consol_id = 0){
        $is_reset = isset($_POST['is_reset']) ? (int)$_POST['is_reset'] : 0;

        $data = array();
        $data['consol_id'] = $consol_id;

        Model('biz_consol_model');
        $message = $this->model->get_where_one("id_type = 'consol' and id_no = $consol_id and type = 'AMS' and receiver = 'EFREIGHT'");
        $data['consol'] = $this->biz_consol_model->get_by_id($consol_id);
        $data['message'] = $message;
        if(empty($message) || $is_reset){
            $data['edi_data'] = $this->ams_data($consol_id);
        }else{
            Model('bsc_message_data_model');
            $message_data = $this->bsc_message_data_model->get_where_one("message_id = {$message['id']}");
            if(!empty($message_data)) $data['edi_data'] = json_decode($message_data['send_data'], true);
            else $data['edi_data'] = $this->ams_data($consol_id);
        }
        if(is_array($data['edi_data']))$data['edi_data'] = array_replace_str($data['edi_data'], array('\n' => "\n"));
        if(is_string($data['edi_data'])) exit($data['edi_data']);
        $data['ShippingParties'] = array_column($data['edi_data']['ShippingParties'], null, 'PartyCode');
        foreach ($data['ShippingParties'] as &$row){
            //如果是字符串,那么转成数组
            if(is_string($row['PartyLocation']['Country'])) $row['PartyLocation']['Country'] = array('code' => $row['PartyLocation']['Country'], 'Value' => '');

        }


        Model('biz_shipment_model');
        $HouseNos = array_column($data['edi_data']['Master']['Houses'], 'HouseNo');
        $this->db->select('job_no,client_code,if(client_code2 is null or client_code2 = "",client_code,client_code2) as client_code2');
        $shipments = array_column($this->biz_shipment_model->no_role_get("job_no in ('" . join("','", $HouseNos) . "')"), null, 'job_no');
        $data['shipments'] = $shipments;

        $this->load->view('head');
        $this->load->view('/bsc/message/data/ams_form', $data);
    }

    public function ams_data($consol_id = 0){
        $this->load->model('biz_consol_model');
        $this->load->model('biz_company_model');
        $this->load->model('biz_shipment_model');
        $this->load->model('biz_port_model');
        $this->load->model('biz_container_model');
        $this->load->model('biz_shipment_container_model');
        $this->load->model('bsc_dict_model');
        $this->load->model('bsc_user_model');
        $this->load->model('m_model');
        $this->load->model('biz_sub_company_model');

        $data = array();

        // $this->db->select('id,job_no,carrier_ref,biz_type,trans_mode,voyage,vessel,trans_origin,trans_origin_name,trans_ETD,trans_destination,trans_destination_name,des_ETA,trans_carrier');
        $consol = $this->biz_consol_model->get_one('id', $consol_id);
        if(empty($consol)){
            return 'consol empty';
        }

        $data['Master'] = array();
        $data['Master']['MasterBillNo'] = $consol['carrier_ref'];
        if($consol['biz_type'] == 'export'){
            $data['Master']['OPType'] = 'OE';
        }else if($consol['biz_type'] == 'import'){
            $data['Master']['OPType'] = 'OT';
        }else{
            $data['Master']['OPType'] = 'OE';
        }

        //originAgentCode 根据当前发送人判定，查询公司所属的code
        if(!isset($this->OriginAgentCodes[get_system_type()])){
            return lang('orginagent_code 未配置，请联系管理员');
        }
        $data['Master']['OriginAgentCode'] = $this->OriginAgentCodes[get_system_type()];
        $data['Master']['DestinationAgentCode'] = 'LAX';

        //$carrier_codes改为查询 efreight_trans_carrier
        $carrier_codes = $this->bsc_dict_model->get_one_eq('name', $consol['trans_carrier'], 'efreight_trans_carrier');
        if(empty($carrier_codes)){
            return lang('承运人不存在对应值,请联系管理员');
        }
        $data['Master']['CarrierName'] = $carrier_codes['value'];
        $data['Master']['ConsignmentType'] = $consol['trans_mode'];
        $data['Master']['FilingType'] = '';

        $data['Master']['RouteInformation'] = array();
        $data['Master']['RouteInformation']['ArrivalVessel'] = array();

        $dict_imo = $this->bsc_dict_model->get_one_eq('name',  $consol["vessel"], 'vessel');
        $imo = '';
        if(!empty($dict_imo)) $imo = $dict_imo["ext1"];
        $data['Master']['RouteInformation']['ArrivalVessel']['IMO'] = $imo;
        $data['Master']['RouteInformation']['ArrivalVessel']['voyage'] = $consol['voyage'];
        $data['Master']['RouteInformation']['ArrivalVessel']['Value'] = $consol['vessel'];

        if($consol['trans_ETD'] == '0000-00-00 00:00:00' || $consol['trans_ETD'] == '0000-00-00'){
            $consol['trans_ETD'] = '';
        }
        $data['Master']['RouteInformation']['LoadingPort'] = array();
        $data['Master']['RouteInformation']['LoadingPort']['code'] = $consol['trans_origin'];
        $data['Master']['RouteInformation']['LoadingPort']['date'] = $consol['trans_ETD'];
        $data['Master']['RouteInformation']['LoadingPort']['Value'] = $consol['trans_origin_name'];

        if($consol['des_ETA'] == '0000-00-00 00:00:00' || $consol['des_ETA'] == '0000-00-00'){
            $consol['des_ETA'] = '';
        }
        $data['Master']['RouteInformation']['DischargePort'] = array();
        $data['Master']['RouteInformation']['DischargePort']['code'] = $consol['trans_discharge'];
        $data['Master']['RouteInformation']['DischargePort']['date'] = $consol['des_ETA'];
        $data['Master']['RouteInformation']['DischargePort']['Value'] = $consol['trans_discharge_name'];

        $data['Master']['MasterRefNo'] = $consol['job_no'];

        $containers = $this->biz_container_model->get('consol_id = ' . $consol['id']);

        if(empty($containers)){
            return lang('CONSOL 没有箱数据');
        }
        $data['Master']['MasterContainers'] = array();
        foreach ($containers as $container){
            $Container = array();
            $Container['ContainerNo'] = $container['container_no'];
            $Container['ContainerType'] = $container['container_size'];
            $Container['SealNo'] = $container['seal_no'];

            $data['Master']['MasterContainers'][] = $Container;
        }

        $shipments = $this->biz_shipment_model->no_role_get("consol_id =  '" . $consol['id'] . "'");
        if(empty($shipments)){
            return lang('没有SHIPMENT');
        }
        $data['Master']['Houses'] = array();
        $data['ShippingParties'] = array();
        foreach ($shipments as $shipment){
            $shipment_containers = $this->biz_shipment_container_model->join_container_get_by_id($shipment['id'], $consol_id);
            if(empty($shipment_containers)){
                return lang('{job_no}  container 不存在', array($shipment['job_no']));
            }

            if(strlen($shipment['job_no']) <= 12){
                $HouseNo = $shipment['job_no'];//$HouseNo 最多12位 这里将前面的JH KM等修改未J K
            }
            else if(strlen($shipment['job_no']) == 13){
                $HouseNo = substr($shipment['job_no'], 0,1).substr($shipment['job_no'], 2);//$HouseNo 最多12位 这里将前面的JH KM等修改未J K
            }
            else{
                return lang('{job_no}  当前job_no 编号格式暂不支持', array($shipment['job_no']));
            }
            if(strpos($shipment['good_outers_unit'], 'PALLET') !== false){
                return lang('AMS不允许PALLETS');
            }

            $shipment['mark_nums'] = 'N/M';

            $house = array();
            $house['HouseNo'] = $HouseNo;
            $house['PlaceReceipt'] = array();
            $house['PlaceReceipt']['code'] = $shipment['trans_origin'];
            $house['PlaceReceipt']['date'] = $consol['trans_ETD'];
            $house['PlaceReceipt']['Value'] = $shipment['trans_origin_name'];

            $house['PlaceDelivery'] = array();
            $house['PlaceDelivery']['code'] = $shipment['trans_destination'];
            $house['PlaceDelivery']['date'] = $consol['des_ETA'];
            $house['PlaceDelivery']['Value'] = $shipment['trans_destination_name'];

            $house['HouseContainers'] = array();
            $shipment['mark_nums'] = 'N/M';
            foreach ($shipment_containers as $shipment_container) {
                $house_container = array();
                $house_container['ContainerRefNo'] = $shipment_container['container_no'];
                $house_container['Package'] = $shipment_container['packs'];
                $house_container['Unit'] = $shipment['good_outers_unit'];
                $house_container['CountryOrigin'] = array();
                $house_container['CountryOrigin']['code'] = 'CN';
                $house_container['CountryOrigin']['Value'] = 'CHINA';
                $house_container['GrossWeight'] = $shipment_container['weight'];
                $house_container['CBM'] = $shipment_container['volume'];
                $house_container['Marking'] = $shipment['mark_nums'];
                $house_container['Description'] = $shipment['description'];
                $house['HouseContainers'][] = $house_container;
            }

            //添加ShippingParty
            //SHIPPER
            $temp_address = str_replace("\n", '\n', str_replace("'","\'", $shipment['shipper_address']));
            $shipper = $this->biz_company_model->get_where_one("client_code = '{$shipment['client_code2']}' and company_type = 'shipper' and company_name = '" . str_replace("'","\'", $shipment['shipper_company']) . "' and company_address = '" . $temp_address . "'");
            if(empty($shipper)){
                $shipper = $this->biz_company_model->get_where_one("client_code = '{$shipment['client_code2']}' and company_type = 'shipper' and company_name = '" . str_replace("'","\'", $shipment['shipper_company']) . "'");
            }
            if(empty($shipper)){
                if(empty($shipment['client_code2'])) $shipment['client_code2']=$shipment['client_code'];
                $insert_data = array();
                $insert_data['client_code'] = $shipment['client_code2'];
                $insert_data['company_type'] = 'shipper';
                $insert_data['company_name'] = $shipment['shipper_company'];
                $insert_data['company_address'] = $shipment['shipper_address'];
                $new_id = $this->biz_company_model->save($insert_data);
                $shipper = array('id' => $new_id, 'company_name' => $shipment['shipper_company'], 'company_address' => $shipment['shipper_address'], 'city' => '', 'country' => '', 'postalcode' => '', 'company_contact' => '', 'company_email' => '', 'company_telephone' => '');
            }
            // if(empty($shipper)){
            //     return $shipment['job_no'] . " SHIPPER未找到,请到shipment里点击发货人公司右边'E'进行维护";
            // }
            $ShippingParty = array();
            $ShippingParty['PartyCode'] = $shipper['id'];
            $ShippingParty['PartyType'] = "CU";
            $ShippingParty['PartyName'] = $shipper['company_name'];
            $ShippingParty['PartyLocation'] = array();
            $ShippingParty['PartyLocation']['Address'] = $shipper['company_address'];
            $ShippingParty['PartyLocation']['City'] = $shipper['city'];
            $ShippingParty['PartyLocation']['Country'] = $shipper['country'];
            $ShippingParty['PartyLocation']['PostalCode'] = $shipper['postalcode'];
            $ShippingParty['PartyRemark'] = "";
            $ShippingParty['ContactPersons'] = array();
            $ShippingParty['ContactPersons']['ContactName'] = $shipper['company_contact'];
            $ShippingParty['ContactPersons']['ContactMail'] = $shipper['company_email'];
            $ShippingParty['ContactPersons']['Phone'] = $shipper['company_telephone'];
            $house['ShipperRefCode'] = $shipper['id'];

            $data['ShippingParties'][] = $ShippingParty;

            //consignee
            $temp_address = str_replace("\n", '\n', str_replace("'","\'", $shipment['consignee_address']));
            $consignee = $this->biz_company_model->get_where_one("client_code = '{$shipment['client_code2']}' and company_type = 'consignee' and company_name = '" . str_replace("'","\'", $shipment['consignee_company']) . "' and company_address = '" . $temp_address . "'");
            if(empty($consignee)){
                $consignee = $this->biz_company_model->get_where_one("client_code = '{$shipment['client_code2']}' and company_type = 'consignee' and company_name = '" . str_replace("'","\'", $shipment['consignee_company']) . "'");
            }
            if(empty($consignee)){
                if(empty($shipment['client_code2'])) $shipment['client_code2']=$shipment['client_code'];
                $insert_data = array();
                $insert_data['client_code'] = $shipment['client_code2'];
                $insert_data['company_type'] = 'consignee';
                $insert_data['company_name'] = $shipment['consignee_company'];
                $insert_data['company_address'] = $shipment['consignee_address'];
                $new_id = $this->biz_company_model->save($insert_data);
                $consignee = array('id' => $new_id, 'company_name' => $shipment['consignee_company'], 'company_address' => $shipment['consignee_address'], 'city' => '', 'country' => '', 'postalcode' => '', 'company_contact' => '', 'company_email' => '', 'company_telephone' => '');
            }
            // if(empty($consignee)){
            //     return $shipment['job_no'] . " CONSIGNEE未找到,SHIPPER未找到,请到shipment里点击收货人公司右边'E'进行维护";
            // }
            $ShippingParty = array();
            $ShippingParty['PartyCode'] = $consignee['id'];
            $ShippingParty['PartyType'] = "CU";
            $ShippingParty['PartyName'] = $consignee['company_name'];
            $ShippingParty['PartyLocation'] = array();
            $ShippingParty['PartyLocation']['Address'] = $consignee['company_address'];
            $ShippingParty['PartyLocation']['City'] = $consignee['city'];
            $ShippingParty['PartyLocation']['Country'] = $consignee['country'];
            $ShippingParty['PartyLocation']['PostalCode'] = $consignee['postalcode'];
            $ShippingParty['PartyRemark'] = "";
            $ShippingParty['ContactPersons'] = array();
            $ShippingParty['ContactPersons']['ContactName'] = $consignee['company_contact'];
            $ShippingParty['ContactPersons']['ContactMail'] = $consignee['company_email'];
            $ShippingParty['ContactPersons']['Phone'] = $consignee['company_telephone'];
            $house['ConsigneeRefCode'] = $consignee['id'];
            if($shipment['notify_company'] == 'SAME AS CONSIGNEE')$house['NotifyRefCode'] = $house['ConsigneeRefCode'];

            $data['ShippingParties'][] = $ShippingParty;

            if($shipment['notify_company'] !== 'SAME AS CONSIGNEE'){
                //notify
                $temp_address = str_replace("\n", '\n', str_replace("'","\'", $shipment['notify_address']));
                $notify = $this->biz_company_model->get_where_one("client_code = '{$shipment['client_code2']}' and company_type = 'notify' and company_name =  '" . str_replace("'","\'", $shipment['notify_company']) . "' and company_address = '" . $temp_address . "'");
                if(empty($notify)){
                    $notify = $this->biz_company_model->get_where_one("client_code = '{$shipment['client_code2']}' and company_type = 'notify' and company_name =  '" . str_replace("'","\'", $shipment['notify_company']) . "'");
                }
                if(empty($notify)){
                    if(empty($shipment['client_code2'])) $shipment['client_code2']=$shipment['client_code'];
                    $insert_data = array();
                    $insert_data['client_code'] = $shipment['client_code2'];
                    $insert_data['company_type'] = 'notify';
                    $insert_data['company_name'] = $shipment['notify_company'];
                    $insert_data['company_address'] = $shipment['notify_address'];
                    $new_id = $this->biz_company_model->save($insert_data);
                    $notify = array('id' => $new_id, 'company_name' => $shipment['notify_company'], 'company_address' => $shipment['notify_address'], 'city' => '', 'country' => '', 'postalcode' => '', 'company_contact' => '', 'company_email' => '', 'company_telephone' => '');
                }
                // if(empty($notify)){
                //     return $shipment['job_no'] . " notify未找到,SHIPPER未找到,请到shipment里点击通知人公司右边'E'进行维护";
                // }
                $ShippingParty = array();
                $ShippingParty['PartyCode'] = $notify['id'];
                $ShippingParty['PartyType'] = "CU";
                $ShippingParty['PartyName'] = $notify['company_name'];
                $ShippingParty['PartyLocation'] = array();
                $ShippingParty['PartyLocation']['Address'] = $notify['company_address'];
                $ShippingParty['PartyLocation']['City'] = $notify['city'];
                $ShippingParty['PartyLocation']['Country'] = $notify['country'];
                $ShippingParty['PartyLocation']['PostalCode'] = $notify['postalcode'];
                $ShippingParty['PartyRemark'] = "";
                $ShippingParty['ContactPersons'] = array();
                $ShippingParty['ContactPersons']['ContactName'] = $notify['company_contact'];
                $ShippingParty['ContactPersons']['ContactMail'] = $notify['company_email'];
                $ShippingParty['ContactPersons']['Phone'] = $notify['company_telephone'];
                $house['NotifyRefCode'] = $notify['id'];

                $data['ShippingParties'][] = $ShippingParty;
            }
            $data['Master']['Houses'][] = $house;
        }

        return $data;
    }

    /**
     * 生成报文AMS
     * @param $consol_id
     * @param $file_function
     */
    public function message_ams($consol_id, $file_function = 'A'){
        $this->load->model(array('bsc_message_data_model','bsc_dict_model', 'biz_sub_company_model', 'bsc_user_model'));

        $type = 'AMS';
        $result = array('code' => 1, 'msg' => 'error');

        $message_row = $this->model->get_where_one("id_type = 'consol' and id_no = $consol_id and type = 'AMS' and receiver = 'EFREIGHT'");
        if(empty($message_row)){
            $result['msg'] = lang('请先保存数据后再试');
            echo json_encode($result);
            return;
        }

        $message_data = $this->bsc_message_data_model->get_where_one("message_id = '{$message_row['id']}'");
        if(empty($message_data)){
            $result['msg'] = lang('请先保存数据后再试');
            echo json_encode($result);
            return;
        }

        $send_data = json_decode($message_data['send_data'], true);

        $str_replace = array('\n' => "\n", "&" => "&amp;", "<" => "&lt;", ">" => "&gt;");
        $send_data = array_replace_str($send_data, $str_replace, true);

        $thisUserId = get_session('id');
        $thisUser = $this->bsc_user_model->get_by_id($thisUserId);
        $SubmitterID = $thisUser['email'];

        //OPType
        $ConsignmentType = $send_data['Master']['ConsignmentType'];
        $ConsignmentType_array = array('FCL', 'LCL');
        if(!in_array($ConsignmentType, $ConsignmentType_array)){
            $result['msg'] = lang('不是FCL或LCL');
            echo json_encode($result);
            return;
        }
        $OPType = $send_data['Master']['OPType'];
        $OPType_array = array('OE', 'OT');
        if(!in_array($OPType, $OPType_array)){
            $result['msg'] = lang('业务类型错误');
            echo json_encode($result);
            return;
        }
        //船公司codeSCAC	Carrier Name
        //$carrier_codes改为查询 efreight_trans_carrier
        $isset_dict = $this->bsc_dict_model->get_one_eq('value', $send_data['Master']['CarrierName'], 'efreight_trans_carrier');
        if(empty($isset_dict)){
            $result['msg'] = lang('承运人不存在对应值,请联系管理员');
            echo json_encode($result);
            return;
        }
        $CarrierNamecode = $send_data['Master']['CarrierName'];
        $ConsignmentType = $send_data['Master']['ConsignmentType'];
        //E - Export I - Import B - In-Bond / IT  required Bonded Freight Forwarder T - Tranship / IE S - In-Transit / TandE Transportation and Exportation F - Freight Remain on Board AMS : 只有I , F
        $FilingType = $send_data['Master']['FilingType'];
        $FilingType_array = array('I', 'B', 'F');
        if(!in_array($FilingType, $FilingType_array)){
            $result['msg'] = lang('暂不支持该FilingType');
            echo json_encode($result);
            return;
        }
        $container_size_codes = $this->container_size_codes;

        $Unit_codes = $this->unit_codes;

        //航次填写编号,如果没有填写编号,且大于5位,不给提交
        $voyage = $send_data['Master']['RouteInformation']['ArrivalVessel']['voyage'];
        if(empty($voyage)){
            $result['msg'] = lang('请填写航次');
            echo json_encode($result);
            return;
        }
        if(strlen($voyage) > 5){
            $result['msg'] = lang('AMS 航次不能大于5位,请填写额外参数 voyage_number');
            echo json_encode($result);
            return;
        }
        $vessel = $send_data['Master']['RouteInformation']['ArrivalVessel']['Value'];

        //IMO必须是7位整数
        $imo_number = $send_data['Master']['RouteInformation']['ArrivalVessel']['IMO'];

        if(!is_numeric($imo_number) || strlen($imo_number) != 7){
            $result['msg'] = lang('IMO 必须是7位整数');
            echo json_encode($result);
            return;
        }

        $trans_discharge = $send_data['Master']['RouteInformation']['DischargePort']['code'];
        $trans_discharge_name = $send_data['Master']['RouteInformation']['DischargePort']['Value'];
        $trans_discharge_date = $send_data['Master']['RouteInformation']['DischargePort']['date'];

        $trans_origin = $send_data['Master']['RouteInformation']['LoadingPort']['code'];
        $trans_origin_name = $send_data['Master']['RouteInformation']['LoadingPort']['Value'];
        $trans_origin_date = $send_data['Master']['RouteInformation']['LoadingPort']['date'];
        if(strlen($trans_discharge) != 5){
            $result['msg'] = lang('DischargePort Code必须是5位');
            echo json_encode($result);
            return;
        }
        if(strlen($trans_origin) != 5){
            $result['msg'] = lang('LoadingPort Code必须是5位');
            echo json_encode($result);
            return;
        }

        // if(substr($trans_destination, 0, 2) !== 'US'){//目的港必须是US开头
        //     $result['msg'] = '目的港必须是US开头';
        //     echo json_encode($result);
        //     return;
        // }

        $time = time();

        //开始填入数据
        $MessageID = date('YmdHi', $time) . $message_row['id'];//唯一标识XML值 TODO
        $SenderCode = 'CUZHUN';
        $ManifestType = 'AMS';
        $CompanyCode = 'LEAGUE';
        $BranchCode = 'SHA';
        $SendDateTime = date('YmdHis', $time);
        $Version = '2.0';
        $MasterBillNo = $send_data['Master']['MasterBillNo'];
        //originAgentCode 根据当前发送人判定，查询公司所属的code
        $OriginAgentCode = $send_data['Master']['OriginAgentCode'];

        if(!isset($this->OriginAgentCodes[get_system_type()])){
            $result['msg'] = lang('orginagent_code 未配置，请联系管理员');
            echo json_encode($result);
            return;
        }

        $DestinationAgentCode = $send_data['Master']['DestinationAgentCode'];
        $MasterRefNo = $send_data['Master']['MasterRefNo'];
        $ShippingParty_Code = array();

        $message = "";
        $message .= "<Manifest>";

        $message .= "<Header>";
        $message .= "<MessageID>$MessageID</MessageID>";
        $message .= "<ActionCode>$file_function</ActionCode>";//A -  新增不发生 AS- 新增并发生 R– 修改 D- 删除
        $message .= "<SenderCode>$SenderCode</SenderCode>";//发送方软件供应商代码，需要EFT设置 TODO
        $message .= "<ManifestType>$ManifestType</ManifestType>";//ACI、AMS、AFR、ISF 目前固定AMS
        $message .= "<CompanyCode>$CompanyCode</CompanyCode>";//公司代码 TODO
        $message .= "<SubmitterID>$SubmitterID</SubmitterID>";//Emai地址 以FTP上传Xml时，若Xml文件解析，数据校验发生错误，将通过Email通知错误信息。Email地址以分号(;)分隔
        $message .= "<BranchCode>$BranchCode</BranchCode>";//分公司代码 TODO
        $message .= "<SendDateTime>$SendDateTime</SendDateTime>";//Format:yyyyMMddHHmmss 发送时间
        $message .= "<Version>$Version</Version>";//版本
        $message .= "</Header>";

        $message .= "<Master>";
//        $message .= "<FilerCode></FilerCode>";//发送代码 不必填

        $message .= "<MasterBillNo>$MasterBillNo</MasterBillNo>";//主单号,提单号
        $message .= "<OPType>$OPType</OPType>";//业务类型 OE：海运出口 OI：海运进口
        $message .= "<OriginAgentCode>$OriginAgentCode</OriginAgentCode>";//出口方代理 需要EFT设置 TODO
        $message .= "<DestinationAgentCode>$DestinationAgentCode</DestinationAgentCode>";//进口方代理	需要EFT设置 TODO
        $message .= "<CarrierName code=\"$CarrierNamecode\"></CarrierName>";//船公司简码	船公司4码
        $message .= "<ConsignmentType>$ConsignmentType</ConsignmentType>";//业务模式 LCL
        $message .= "<FilingType>$FilingType</FilingType>";//

        $message .= "<RouteInformation>";//船期信息列表--start
        $message .= "<ArrivalVessel IMO=\"{$imo_number}\" voyage=\"$voyage\">$vessel</ArrivalVessel>";//
        $message .= "<LoadingPort code=\"$trans_origin\" date=\"$trans_origin_date\">$trans_origin_name</LoadingPort>";//
        $message .= "<DischargePort code=\"$trans_discharge\" date=\"$trans_discharge_date\">$trans_discharge_name</DischargePort>";//
        $message .= "</RouteInformation>";//船期信息列表--end

        $message .= "<MasterRefNo>$MasterRefNo</MasterRefNo>";//主票参考号 TODO

        $message .= "<MasterContainers>";//集装箱信息列表--start

        $containers = $send_data['Master']['MasterContainers'];
        foreach ($containers as $container){
            if(isset($container_size_codes[$container['ContainerType']])){
                $ContainerType = $container_size_codes[$container['ContainerType']];
            }else{
                $result['msg'] = lang('存在未配置的箱型{ContainerType}', array($container['ContainerType']));
                echo json_encode($result);
                return;
            }

            $message .= "<Container>";//箱信息--start
            $message .= "<ContainerNo>{$container['ContainerNo']}</ContainerNo>";//箱号
            $message .= "<ContainerType>$ContainerType</ContainerType>";//箱型,需要转换下
            $message .= "<SealNo>{$container['SealNo']}</SealNo>";//箱封号
//            $message .= "<ServiceCode></ServiceCode>";//服务代码	CY: Container Yard CS:Container Station DD:Door-to-Door
//            $message .= "<ShipperOwned></ShipperOwned>";//
            $message .= "</Container>";//箱信息--end
        }
        $message .= "</MasterContainers>";//集装箱信息列表--end

        $message .= "<Houses>";
        $shipments = $send_data['Master']['Houses'];
        foreach ($shipments as $key => $shipment){
            $HouseNo = $shipment['HouseNo'];
            $ShipperRefCode = $shipment['ShipperRefCode'];
            $ConsigneeRefCode = $shipment['ConsigneeRefCode'];
            $NotifyRefCode = $shipment['NotifyRefCode'];
            $ShippingParty_Code[] = $ShipperRefCode;
            $ShippingParty_Code[] = $ConsigneeRefCode;
            $ShippingParty_Code[] = $NotifyRefCode;

            if(strlen($HouseNo) <= 12){
            }
            else if(strlen($HouseNo) == 13){
                $HouseNo = substr($HouseNo, 0,1).substr($HouseNo, 2);//$HouseNo 最多12位 这里将前面的JH KM等修改未J K
            }
            else{
                $result['msg'] = lang('{HouseNo}  当前HouseNo 编号格式暂不支持', array('HouseNo' => $HouseNo));
                echo json_encode($result);
                return;
            }

            $message .= "<House>";
//            $message .= "<LastHBLIndicator></LastHBLIndicator>";//bool 非必填
            $message .= "<HouseNo>{$HouseNo}</HouseNo>";//分单号
//            $message .= "<OriginalHouseNo>SCAC{$shipment['cus_no']}</OriginalHouseNo>";//拆单使用，提供原始Housenumber
            $message .= "<ShipperRefCode>{$ShipperRefCode}</ShipperRefCode>";//发货人参考号
            $message .= "<ConsigneeRefCode>{$ConsigneeRefCode}</ConsigneeRefCode>";//收货人参考号
            $message .= "<NotifyRefCode>{$NotifyRefCode}</NotifyRefCode>";//通知人参考号

            $trans_receipt = $shipment['PlaceReceipt']['code'];
            $trans_receipt_name = $shipment['PlaceReceipt']['Value'];
            $trans_receipt_date = $shipment['PlaceReceipt']['date'];
            if(strlen($trans_receipt) != 5){
                $result['msg'] = lang('{HouseNo}  PlaceReceipt Code必须是5位', array('HouseNo' => $HouseNo));
                echo json_encode($result);
                return;
            }
            $trans_destination = $shipment['PlaceDelivery']['code'];
            $trans_destination_name = $shipment['PlaceDelivery']['Value'];
            $trans_destination_date = $shipment['PlaceDelivery']['date'];
            if(strlen($trans_destination) != 5){
                $result['msg'] = lang('{HouseNo}  PlaceDelivery Code必须是5位', array('HouseNo' => $HouseNo));
                echo json_encode($result);
                return;
            }

//            $message .= "<SecondNotifyParty></SecondNotifyParty>";//
            $message .= "<PlaceReceipt code=\"$trans_receipt\" date=\"$trans_receipt_date\">$trans_receipt_name</PlaceReceipt>";//通知人参考号
            $message .= "<PlaceDelivery code=\"$trans_destination\" date=\"$trans_destination_date\">$trans_destination_name</PlaceDelivery>";//通知人参考号

            $message .= "<HouseContainers>";
            $shipment_containers = $shipment['HouseContainers'];
            foreach ($shipment_containers as $shipment_container){
                if(isset($Unit_codes[$shipment_container['Unit']])){
                    $Unit_code = $Unit_codes[$shipment_container['Unit']];
                }else{
                    $result['msg'] = lang('HouseNo:{HouseNo} 包装类型代码无对应值', array('HouseNo' => $HouseNo));
                    echo json_encode($result);
                    return;
                }
                $Unit_value = $shipment_container['Unit'];
                $CountryOrigin_code = $shipment_container['CountryOrigin']['code'];
                $CountryOrigin_value = $shipment_container['CountryOrigin']['Value'];

                $message .= "<Container>";
                $message .= "<ContainerRefNo>{$shipment_container['ContainerRefNo']}</ContainerRefNo>";//箱号
                $message .= "<Package>{$shipment_container['Package']}</Package>";//件数
                $message .= "<Unit code=\"$Unit_code\">$Unit_value</Unit>";//包装类型代码
                $message .= "<CountryOrigin code=\"$CountryOrigin_code\">$CountryOrigin_value</CountryOrigin>";//原产国代码
                $message .= "<GrossWeight>{$shipment_container['GrossWeight']}</GrossWeight>";//重量（KG）
                $message .= "<CBM>{$shipment_container['CBM']}</CBM>";//体积（CBM）
                $message .= "<Marking>{$shipment_container['Marking']}</Marking>";//唛头
                $message .= "<Description>{$shipment_container['Description']}</Description>";//货物描述
                $message .= "</Container>";
            }

            $message .= "</HouseContainers>";

            $message .= "</House>";
        }
        $message .= "</Houses>";

        $message .= "</Master>";

        $message .= "<ShippingParties>";
        $ShippingParties = $send_data['ShippingParties'];
        foreach ($ShippingParties as $ShippingParty){
            if(!in_array($ShippingParty['PartyCode'], $ShippingParty_Code)){
                continue;
            }
            $PartyCode = $ShippingParty['PartyCode'];
            $PartyType = $ShippingParty['PartyType'];
            $PartyName = $ShippingParty['PartyName'];
            if(empty($PartyName)) continue;
            $Address = $ShippingParty['PartyLocation']['Address'];
            $City = $ShippingParty['PartyLocation']['City'];
//            $Country = $ShippingParty['PartyLocation']['Country'];
            //新增
            $Country = $ShippingParty['PartyLocation']['Country'];
            $PostalCode = $ShippingParty['PartyLocation']['PostalCode'];
            //新增
            $ShippingParty['PartyRemark'] = '';
            $PartyRemark = $ShippingParty['PartyRemark'];


            //SHIPPER
            $message .= "<ShippingParty>";

            $message .= "<PartyCode>$PartyCode</PartyCode>";
            $message .= "<PartyType>$PartyType</PartyType>";//默认给CU 往来单位类型
            $message .= "<PartyName>$PartyName</PartyName>";

            $message .= "<PartyLocation>";

            $message .= "<Address>";
            $message .= "<AddressLine>" . substr($Address, 0, 50) . "</AddressLine>";
            if(strlen($Address) > 50){
                $message .= "<AddressLine>" . substr($Address, 50, 50) . "</AddressLine>";
            }
            $message .= "</Address>";
            $message .= "<City>$City</City>";//城市必填
            $message .= "<Country code=\"{$Country['code']}\">{$Country['Value']}</Country>";
            //如果不是台湾,限制必填
            if(trim($PostalCode) == '' && !in_array($Country, array('TW'))){
                $result['msg'] = lang('客户代码:{PartyCode} <br/>客户名称为:{PartyName}  邮编必填', array('PartyCode' => $PartyCode, 'PartyName' => $PartyName));
                echo json_encode($result);
                return;
            }
            $message .= "<PostalCode>$PostalCode</PostalCode>";//收货人，最好给一下postal code邮编

            $message .= "</PartyLocation>";
            $message .= "<PartyRemark>$PartyRemark</PartyRemark>";//450个字符 备注
            $message .= "<ContactPersons>";//

            $ContactName = $ShippingParty['ContactPersons']['ContactName'];
            $ContactMail = $ShippingParty['ContactPersons']['ContactMail'];
            $Phone = $ShippingParty['ContactPersons']['Phone'];
            $message .= "<Person>";//
            $message .= "<ContactName>$ContactName</ContactName>";//联系人
            $message .= "<ContactMail>$ContactMail</ContactMail>";//邮箱
            $message .= "<Phone>$Phone</Phone>";//电话号码
            $message .= "</Person>";//

            $message .= "</ContactPersons>";//
            $message .= "</ShippingParty>";
        }
        $message .= "</ShippingParties>";
        $message .= "</Manifest>";

        $file_content = '<?xml version="1.0" encoding="UTF-8"?>' . $message;

        //保存文件xml
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        $new_dir = $root_path . '/upload/message/EFREIGHT_EDI/' . $type . '/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $message_row['id_type'] . '.xml';
        $file_path = $new_dir . $file_name;
        file_put_contents($file_path, $file_content);
        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => $type,
            'receiver' => 'EFREIGHT',
        );
        update_message($consol_id, $message_data, $type);
        echo json_encode(array('code' => 0, 'msg' => lang('文件生成成功')));
    }

    private function isf_checking($consol, $shipment, $consol_ext, $shipment_ext){
        if(empty($consol) || empty($shipment) || empty($consol_ext) || empty($shipment_ext)){
            return lang('参数错误');
        }


        $consol_ext_check = array();
        if($shipment_ext['isf_no'] == 'SF10'){
            $consol_check = array('booking_ETD');

            $shipment_check = array('hbl_no', 'hs_code', 'consignee_company', 'consignee_address', 'shipper_address', 'shipper_company', 'client_code');

            $shipment_ext_check = array('isf_no', 'consignee_id_type', 'consignee_id', 'consignee_city', 'consignee_region_code', 'consignee_region', 'consignee_country', 'shipper_city', 'shipper_region_code', 'shipper_region', 'shipper_postalcode', 'shipper_country',  'manufacturer_company', 'manufacturer_address', 'manufacturer_city', 'manufacturer_region_code','manufacturer_region', 'manufacturer_country', 'hs_code', 'scac_code');
        }else if($shipment_ext['isf_no'] == 'SF05'){
            $consol_check = array('booking_ETD', 'trans_destination', 'trans_destination_name');
            $shipment_check = array('hbl_no', 'hs_code', 'consignee_company', 'consignee_address');

            $shipment_ext_check = array('isf_no', 'consignee_id_type', 'consignee_id', 'consignee_city', 'consignee_region_code', 'consignee_region', 'consignee_country', 'hs_code', 'scac_code');
        }else{
            return lang('ISF');
        }

        foreach ($consol_check as $val){
            if(empty($consol[$val])){
                return $val;
            }
        }

        foreach ($shipment_check as $val){
            if(empty($shipment[$val])){
                return $val;
            }
        }

        foreach ($consol_ext_check as $val){
            if(empty($consol_ext[$val])){
                return $val;
            }
        }

        foreach ($shipment_ext_check as $val){
            if(empty($shipment_ext[$val])){
                return $val;
            }
        }


        return true;
    }

    public function isf_set_data($shipment_id = 0){
        $is_reset = isset($_POST['is_reset']) ? (int)$_POST['is_reset'] : 0;
        $isf_type = isset($_GET['isf_type']) ? $_GET['isf_type'] : 'SF10';

        $data = array();
        $data['shipment_id'] = $shipment_id;

        $message = $this->model->get_where_one("id_type = 'shipment' and id_no = $shipment_id and type = 'ISF' and receiver = 'EFREIGHT'");
        $data['message'] = $message;
        if(empty($message) || $is_reset){
            $data['edi_data'] = $this->isf_data($shipment_id, $isf_type);
        }else{
            Model('bsc_message_data_model');
            $message_data = $this->bsc_message_data_model->get_where_one("message_id = {$message['id']}");
            if(!empty($message_data)){
                $data['edi_data'] = json_decode($message_data['send_data'], true);
                $isf_type = $data['edi_data']['ISF']['ISFType'];
            }
            else $data['edi_data'] = $this->isf_data($shipment_id, $isf_type);
        }
        if(is_array($data['edi_data']))$data['edi_data'] = array_replace_str($data['edi_data'], array('\n' => "\n"));
        if(is_string($data['edi_data'])) exit($data['edi_data']);
        $this->load->view('head');

        if($isf_type == 'SF05'){
            $this->load->view('/bsc/message/data/isf_05_form', $data);
        }else if($isf_type == 'SF10'){
            $this->load->view('/bsc/message/data/isf_10_form', $data);
        }
    }

    public function isf_data($shipment_id = 0, $isf_type = ''){
        $this->load->model(array(
            'm_model', 'bsc_user_model', 'bsc_dict_model', 'biz_shipment_container_model',
            'biz_container_model', 'biz_port_model', 'biz_shipment_model', 'biz_company_model',
            'biz_consol_model', 'biz_sub_company_model',
        ));
        $isf_type_array = array('SF10', 'SF05');
        if(!in_array($isf_type, $isf_type_array)){
            return lang('ISF类型错误');
        }

        $shipment = $this->biz_shipment_model->get_by_id($shipment_id);
        if(empty($shipment)){
            return lang('SHIPMENT不存在');
        }
        $consol = $this->biz_consol_model->get_by_id($shipment['consol_id']);
        if(empty($consol)){
            return lang('CONSOL不存在');
        }

        //originAgentCode 根据当前发送人判定，查询公司所属的code
        if(!isset($this->OriginAgentCodes[get_system_type()])){
            return lang('orginagent_code 未配置，请联系管理员');
        }
        $OriginAgentCode = $this->OriginAgentCodes[get_system_type()];
        $DestinationAgentCode = 'LAX';
        $data = array();

        $data['ISF'] = array();

        $data['ISF']['ISFType'] = $isf_type;

//        $message .= "<FilerCode></FilerCode>";//发送代码 不必填
        if($isf_type == 'SF10'){
            //ISF 这里需要加值

            $data['ISF'][$isf_type]['ShipmentFileNumber'] = $shipment['hbl_no'];//ShipmentFileNumber  <ShipmentFileNumber>ID</ShipmentFileNumber> 唯一值即可，直接取值CONSOL编号了
            // $data['ISF'][$isf_type]['ShipmentTypeCode'] = "";//<ShipmentTypeCode>01</ShipmentTypeCode>  TODO
            // 01	Standard or regular filings
            // 02	To Order Shipments
            // 03	HHG/PE
            // 04	Military
            // 05	Diplomatic
            // 06	Carnets
            // 07	US Returned Goods
            // 08	FTZ Shipments
            // 09	International Mail Shipments
            // 10	Outer Continental Shelf Shipments
            // 11	Informal

            $data['ISF'][$isf_type]['AMSSCAC'] = '';
            $data['ISF'][$isf_type]['AMSBLNumber'] = $shipment['hbl_no'];// <AMSBLNumber></AMSBLNumber> HBL no
            $data['ISF'][$isf_type]['AMSBLType'] = 'BM';// <AMSBLType></AMSBLType> OB = Ocean Bill of Lading – used for Regular Bills BM = House Bill of Lading  TODO
            $data['ISF'][$isf_type]['MasterBillNo'] = $consol['carrier_ref'];//主单号,提单号
            $data['ISF'][$isf_type]['OnBoardDate'] = $consol['booking_ETD'];//OnBoardDate 没有ATD数据，直接取订舱船期
            $data['ISF'][$isf_type]['OriginAgentCode'] = $OriginAgentCode;// <OriginAgentCode>Ori1</OriginAgentCode>
            $data['ISF'][$isf_type]['DestinationAgentCode'] = $DestinationAgentCode;// <DestinationAgentCode>DestinationAgentCode1</DestinationAgentCode>
            $data['ISF'][$isf_type]['ImporterRefCode'] = "CONSIGNEE{$shipment['id']}";// <ImporterRefCode>SHA_ZJTPS</ImporterRefCode>  TODO
            $data['ISF'][$isf_type]['SellingParties'] = array();
            $data['ISF'][$isf_type]['SellingParties']['SellingRefCode'] = "SHIPPER{$shipment['id']}";// <SellingParties><SellingRefCode /></SellingParties>  TODO

            $data['ISF'][$isf_type]['ContainerStuffingLocations'] = array();
            $data['ISF'][$isf_type]['ContainerStuffingLocations']['ContainerStuffingLocationRefCode'] = "SHIPPER{$shipment['id']}";// <ContainerStuffingLocations><ContainerStuffingLocationRefCode></ContainerStuffingLocationRefCode></ContainerStuffingLocations>  TODO

            $data['ISF'][$isf_type]['Consolidators'] = array();
            $data['ISF'][$isf_type]['Consolidators']['ConsolidatorRefCode'] = "SHIPPER{$shipment['id']}";// <Consolidators><ConsolidatorRefCode></ConsolidatorRefCode></Consolidators>  TODO

            $data['ISF'][$isf_type]['BuyingParties'] = array();
            $data['ISF'][$isf_type]['BuyingParties']['BuyingRefCode'] = "CONSIGNEE{$shipment['id']}";// <BuyingParties><BuyingRefCode></BuyingRefCode></BuyingParties>  TODO

            $data['ISF'][$isf_type]['Consignees'] = array();
            $data['ISF'][$isf_type]['Consignees']['ConsigneeRefCode'] = "CONSIGNEE{$shipment['id']}";// <Consignees><ConsigneeRefCode></ConsigneeRefCode></Consignees>  TODO

            $data['ISF'][$isf_type]['ContainerShipTos'] = array();
            $data['ISF'][$isf_type]['ContainerShipTos']['ShipToRefCode'] = "CONSIGNEE{$shipment['id']}";// <ContainerShipTos><ShipToRefCode></ShipToRefCode></ContainerShipTos>  TODO

            $data['ISF'][$isf_type]['Manufacturers'] = array();
            $data['ISF'][$isf_type]['Manufacturers']['Manufacturer'] = array();
            $data['ISF'][$isf_type]['Manufacturers']['Manufacturer']['CountryOrigin'] = array();
            $data['ISF'][$isf_type]['Manufacturers']['Manufacturer']['CountryOrigin']['code'] = '';
            $data['ISF'][$isf_type]['Manufacturers']['Manufacturer']['CountryOrigin']['Value'] = '';
            $data['ISF'][$isf_type]['Manufacturers']['Manufacturer']['ManufacturerRefCode'] = "MANUFACTURER{$shipment['id']}";
            $data['ISF'][$isf_type]['Manufacturers']['Manufacturer']['HTSCodes'] = $shipment['hs_code'];
        }else if($isf_type == 'SF05'){
            $data['ISF'][$isf_type]['ShipmentFileNumber'] = $shipment['hbl_no'];
            $data['ISF'][$isf_type]['ShipmentTypeCode'] = '01';
            $data['ISF'][$isf_type]['AMSSCAC'] = '';
            $data['ISF'][$isf_type]['AMSBLNumber'] = $shipment['hbl_no'];
            $data['ISF'][$isf_type]['AMSBLType'] = 'BM';
            $data['ISF'][$isf_type]['MasterBillNo'] = $consol['carrier_ref'];//主单号,提单号
            $data['ISF'][$isf_type]['OnBoardDate'] = $consol['booking_ETD'];
            $data['ISF'][$isf_type]['OriginAgentCode'] = $OriginAgentCode;
            $data['ISF'][$isf_type]['DestinationAgentCode'] = $DestinationAgentCode;
            $data['ISF'][$isf_type]['BookingPartyRefCode'] = "BOOKING{$shipment['id']}";
            $data['ISF'][$isf_type]['PlaceDeliveryCountryCode'] = substr($consol['trans_destination'], 0, 2);//交货地国家代码，取目的港五字代码前2个

            $data['ISF'][$isf_type]['ForeignPortofUnlading'] = array();// -UNLoc codes (United Nations Location Code) can be found at http://www.unece.org/cefact/locode/
            $data['ISF'][$isf_type]['ForeignPortofUnlading']['code'] = $consol['trans_destination'];
            $data['ISF'][$isf_type]['ForeignPortofUnlading']['Value'] = $consol['trans_destination_name'];

            $data['ISF'][$isf_type]['PlaceofDelivery'] = array();// UNLoc codes (United Nations Location Code) can be found at http://www.unece.org/cefact/locode/
            $data['ISF'][$isf_type]['PlaceofDelivery']['code'] = $consol['trans_destination'];
            $data['ISF'][$isf_type]['PlaceofDelivery']['Value'] = $consol['trans_destination_name'];

            $data['ISF'][$isf_type]['ContainerShipTos'] = array();
            $data['ISF'][$isf_type]['ContainerShipTos']['ShipToRefCode'] = "CONSIGNEE{$shipment['id']}";

            $data['ISF'][$isf_type]['HTSCodes'] = $shipment['hs_code'];
        }
        //consignee都有
        //CONSIGNEE
        if(empty($shipment['client_code2'])) $shipment['client_code2']=$shipment['client_code'];
        $temp_address = str_replace("\n", '\n', str_replace("'","\'", $shipment['consignee_address']));
        $consignee = $this->biz_company_model->get_one("company_type = 'consignee' and company_name = '{$shipment['consignee_company']}' and client_code='{$shipment['client_code2']}'");
        if(empty($consignee)){
            $insert_data = array();
            $insert_data['client_code'] = $shipment['client_code2'];
            $insert_data['company_type'] = 'consignee';
            $insert_data['company_name'] = $shipment['consignee_company'];
            $insert_data['company_address'] = $shipment['consignee_address'];
            $new_id = $this->biz_company_model->save($insert_data);
            $consignee = array('id' => $new_id, 'company_name' => $shipment['consignee_company'], 'company_address' => $shipment['consignee_address'], 'city' => '', 'country' => '', 'postalcode' => '', 'company_contact' => '', 'company_email' => '', 'company_telephone' => '');
        }
        //获取扩展配置,如果有,优先使用扩展配置的--start
        if(!empty($shipment_ext)){
            if(!empty($shipment_ext['consignee_city'])) $consignee['city'] = $shipment_ext['consignee_city'];
            if(!empty($shipment_ext['consignee_country'])) $consignee['country'] = $shipment_ext['consignee_country'];
        }
        //获取扩展配置,如果有,优先使用扩展配置的--end
        if(empty($consignee['city']) || empty($consignee['country'])){
            if(empty($consignee['city'])){
                $msg_dlc = lang('城市');
            }
            if(empty($consignee['country'])){
                $msg_dlc = lang('国家代码');
            }
            return lang('{job_no} consignee {msg_dlc} 未填写<br /> {a}', array('job_no' => $shipment['job_no'], 'msg_dlc' => $msg_dlc, 'a' => '<a href="javascript:window.open(\'/biz_company/index/consignee/' . $shipment['client_code2'] . '\')">' . lang('点此添加') . '</a>'));
        }
        $data['ShippingParties'] = array();

        $ShippingParty = array();
        $ShippingParty['PartyCode'] = "CONSIGNEE{$shipment['id']}";
        if($isf_type == 'SF05') $ShippingParty['PartyType'] = "IM,BY,ST";
        else $ShippingParty['PartyType'] = "CN,IM,BY,ST";
        $ShippingParty['PartyName'] = $shipment['consignee_company'];
        $ShippingParty['PartyIDType'] = '';
        // EI  	= Employer Identification Number (IRS ) 1
        // ANI 	= USCBP-assigned Number 2
        // CIN	  =	CBP encrypted Consignee ID 5
        // 34 	  = Social Security Number 3
        // DUN	  =	DUNS Number 6
        // DNS	  =	DUNS+4 Number 7
        // FR	  =	Facility Information Resource Management System (FIRMS) Code 8
        // AEF 	= Passport Number 4
        // 2   	= Standard Carrier Alpha Code (SCAC) 9
        // ZZ    = No Entity ID 0
        $ShippingParty['PartyID'] = '';
        $ShippingParty['PartyLocation'] = array();
        $ShippingParty['PartyLocation']['Address'] = $shipment['consignee_address'];
        $ShippingParty['PartyLocation']['City'] = $consignee['city'];
        $ShippingParty['PartyLocation']['SubDivisionCode'] = '';
        $ShippingParty['PartyLocation']['Region'] = $consignee['region'];
        $ShippingParty['PartyLocation']['PostalCode'] = $consignee['postalcode'];
        $ShippingParty['PartyLocation']['Country'] = array();
        $ShippingParty['PartyLocation']['Country']['code'] = $consignee['country'];
        $ShippingParty['PartyLocation']['Country']['Value'] = '';

        $data['ShippingParties'][] = $ShippingParty;

        if($isf_type == 'SF05'){
            //05只要订舱方（LEAGUESHIPPING）
            $booking = array(
                'company_name' => 'SHANGHAI LEAGUE SHIPPING CO. LTD.',
                'address' => '2ND FLOOR, BUILDING A, BLOCK 1, NO.723 TONGXIN ROAD,HONGKOU DISTRICT, SHANGHAI CHINA',
                'email' => '',
                'telephone' => '',
                'contact' => '',
                'city' => 'SHANGHAI',
                'city_code' => '31',
                'region' => 'SHANGHAI',
                'country' => 'CN',
                'postalcode' => '',
            );
            $ShippingParty = array();

            $ShippingParty['PartyCode'] = "BOOKING{$shipment['id']}";
            $ShippingParty['PartyType'] = "BKP";
            $ShippingParty['PartyName'] = $booking['company_name'];
            $ShippingParty['PartyIDType'] = 'ANI';
            $ShippingParty['PartyID'] = '201704-16979';
            $ShippingParty['PartyLocation'] = array();
            $ShippingParty['PartyLocation']['Address'] = $booking['address'];
            $ShippingParty['PartyLocation']['City'] = $booking['city'];
            $ShippingParty['PartyLocation']['SubDivisionCode'] = $booking['city_code'];
            $ShippingParty['PartyLocation']['Region'] = $booking['region'];
            $ShippingParty['PartyLocation']['PostalCode'] = $booking['postalcode'];
            $ShippingParty['PartyLocation']['Country'] = array();
            $ShippingParty['PartyLocation']['Country']['code'] = $booking['country'];
            $ShippingParty['PartyLocation']['Country']['Value'] = '';

            $data['ShippingParties'][] = $ShippingParty;
        }else if($isf_type == 'SF10'){
            //10 S N G
            $temp_address = str_replace("\n", '\n', str_replace("'","\'", $shipment['shipper_address']));
            $shipper = $this->biz_company_model->get_one("company_type = 'shipper' and company_name = '" . str_replace("'","\'", $shipment['shipper_company']) . "'");
            if(empty($shipper)){
                return lang('{job_no} shipper 未找到对应的COMPANY数据<br /> {a}', array('job_no' => $shipment['job_no'], 'a' => '<a href="javascript:window.open(\'/biz_company/index/shipper/' . $shipment['client_code2'] . '\')">' . lang('点此添加') . '</a>'));
                return ;
            }
            if(empty($shipper['city']) || empty($shipper['postalcode']) || empty($shipper['country'])){
                $msg_dlc = '';
                if(empty($shipper['city'])){
                    $msg_dlc = '城市';
                }
                if(empty($shipper['postalcode'])){
                    $msg_dlc = '邮编';
                }
                if(empty($shipper['country'])){
                    $msg_dlc = '国家代码';
                }
                return lang('{job_no} shipper  {msg_dlc} 未填写<br /> {a}', array('job_no' => $shipment['job_no'], 'msg_dlc' => $msg_dlc, 'a' => '<a href="javascript:window.open(\'/biz_company/index/shipper/' . $shipment['client_code2'] . '\')">' . lang('点此添加') . '</a>'));
            }

            $ShippingParty = array();

            $ShippingParty['PartyCode'] = "SHIPPER{$shipment['id']}";
            $ShippingParty['PartyType'] = "SE,LG,CS";
            $ShippingParty['PartyName'] = $shipment['shipper_company'];
            $ShippingParty['PartyIDType'] = 'ZZ';
            $ShippingParty['PartyID'] = '';
            $ShippingParty['PartyLocation'] = array();
            $ShippingParty['PartyLocation']['Address'] = $shipment['shipper_address'];
            $ShippingParty['PartyLocation']['City'] = $shipper['city'];
            $ShippingParty['PartyLocation']['SubDivisionCode'] = '';
            $ShippingParty['PartyLocation']['Region'] = $shipper['region'];
            $ShippingParty['PartyLocation']['PostalCode'] = $shipper['postalcode'];
            $ShippingParty['PartyLocation']['Country'] = array();
            $ShippingParty['PartyLocation']['Country']['code'] = $shipper['country'];
            $ShippingParty['PartyLocation']['Country']['Value'] = '';

            $data['ShippingParties'][] = $ShippingParty;
            //MANUFACTURER
            $ShippingParty = array();

            $ShippingParty['PartyCode'] = "MANUFACTURER{$shipment['id']}";
            $ShippingParty['PartyType'] = "MF";
            $ShippingParty['PartyName'] = $shipment['shipper_company'];
            $ShippingParty['PartyIDType'] = 'ZZ';
            $ShippingParty['PartyID'] = '';
            $ShippingParty['PartyLocation'] = array();
            $ShippingParty['PartyLocation']['Address'] = $shipment['shipper_address'];
            $ShippingParty['PartyLocation']['City'] = $shipper['city'];
            $ShippingParty['PartyLocation']['SubDivisionCode'] = '';
            $ShippingParty['PartyLocation']['Region'] = $shipper['region'];
            $ShippingParty['PartyLocation']['PostalCode'] = $shipper['postalcode'];
            $ShippingParty['PartyLocation']['Country'] = array();
            $ShippingParty['PartyLocation']['Country']['code'] = $shipper['country'];
            $ShippingParty['PartyLocation']['Country']['Value'] = '';

            $data['ShippingParties'][] = $ShippingParty;
        }
        return $data;
    }

    /**
     * 生成报文AMS
     * @param $consol_id
     * @param $file_function
     */
    public function message_isf($shipment_id, $file_function = 'A'){
        $this->load->model(array('bsc_message_data_model', 'bsc_dict_model', 'biz_sub_company_model', 'bsc_user_model'));

        $type = 'ISF';
        $result = array('code' => 1, 'msg' => 'error');

        $message_row = $this->model->get_where_one("id_type = 'shipment' and id_no = $shipment_id and type = '$type' and receiver = 'EFREIGHT'");
        if (empty($message_row)) {
            $result['msg'] = lang('请先保存数据后再试');
            echo json_encode($result);
            return;
        }

        $message_data = $this->bsc_message_data_model->get_where_one("message_id = '{$message_row['id']}'");
        if (empty($message_data)) {
            $result['msg'] = lang('请先保存数据后再试');
            echo json_encode($result);
            return;
        }

        $send_data = json_decode($message_data['send_data'], true);

        $str_replace = array('\n' => "\n", "&" => "&amp;", "<" => "&lt;", ">" => "&gt;");
        $send_data = array_replace_str($send_data, $str_replace, true);

        $thisUserId = get_session('id');
        $thisUser = $this->bsc_user_model->get_by_id($thisUserId);
        $SubmitterID = $thisUser['email'];

        $time = time();
        $MessageID =  date('Ymd', $time) . $shipment_id;//唯一标识XML值 TODO
        $SenderCode = 'CUZHUN';
        $ManifestType = 'ISF';
        $CompanyCode = 'LEAGUE';//LEAGUE
        $BranchCode = 'SHA';
        $SendDateTime = date('YmdHis', $time);
        $Version = '2.0';

        //开始填入数据
        $message = "";
        $message .= "<ISF>";

        $message .= "<Header>";
        $message .= "<MessageID>$MessageID</MessageID>";
        $message .= "<ActionCode>$file_function</ActionCode>";//A -  新增不发生 AS- 新增并发生 R– 修改 D- 删除
        $message .= "<SenderCode>$SenderCode</SenderCode>";//发送方软件供应商代码，需要EFT设置 TODO
        $message .= "<ManifestType>$ManifestType</ManifestType>";//ACI、AMS、AFR、ISF 目前固定AMS
        $message .= "<CompanyCode>$CompanyCode</CompanyCode>";//公司代码 TODO
        $message .= "<SubmitterID>$SubmitterID</SubmitterID>";//Emai地址 以FTP上传Xml时，若Xml文件解析，数据校验发生错误，将通过Email通知错误信息。Email地址以分号(;)分隔
        $message .= "<BranchCode>$BranchCode</BranchCode>";//分公司代码 TODO
        $message .= "<SendDateTime>$SendDateTime</SendDateTime>";//Format:yyyyMMddHHmmss 发送时间
        $message .= "<Version>$Version</Version>";//版本
        $message .= "</Header>";

        $ISFType = $send_data['ISF']['ISFType'];

        $message .= "<{$ISFType}>";
//        $message .= "<FilerCode></FilerCode>";//发送代码 不必填
        $ISF_data = $send_data['ISF'][$ISFType];
        if($ISFType == 'SF10'){
            //ISF 这里需要加值
            $message .= "<ShipmentFileNumber>{$ISF_data['ShipmentFileNumber']}</ShipmentFileNumber>";//ShipmentFileNumber  <ShipmentFileNumber>ID</ShipmentFileNumber> 唯一值即可，直接取值CONSOL编号了
            // $message .= "<ShipmentTypeCode>01</ShipmentTypeCode>";//<ShipmentTypeCode>01</ShipmentTypeCode>  TODO
            // 01	Standard or regular filings
            // 02	To Order Shipments
            // 03	HHG/PE
            // 04	Military
            // 05	Diplomatic
            // 06	Carnets
            // 07	US Returned Goods
            // 08	FTZ Shipments
            // 09	International Mail Shipments
            // 10	Outer Continental Shelf Shipments
            // 11	Informal


            $message .= "<AMSSCAC>{$ISF_data['AMSSCAC']}</AMSSCAC>";// <AMSSCAC></AMSSCAC> TODO  SCAC
            $message .= "<AMSBLNumber>{$ISF_data['AMSBLNumber']}</AMSBLNumber>";// <AMSBLNumber></AMSBLNumber> HBL no
            $message .= "<AMSBLType>{$ISF_data['AMSBLType']}</AMSBLType>";// <AMSBLType></AMSBLType> OB = Ocean Bill of Lading – used for Regular Bills BM = House Bill of Lading  TODO
            $message .= "<MasterBillNo>{$ISF_data['MasterBillNo']}</MasterBillNo>";//主单号,提单号
            $message .= "<OnBoardDate>{$ISF_data['OnBoardDate']}</OnBoardDate>";//OnBoardDate 没有ATD数据，直接取订舱船期
            $message .= "<OriginAgentCode>{$ISF_data['OriginAgentCode']}</OriginAgentCode>";// <OriginAgentCode>Ori1</OriginAgentCode>
            $message .= "<DestinationAgentCode>{$ISF_data['DestinationAgentCode']}</DestinationAgentCode>";// <DestinationAgentCode>DestinationAgentCode1</DestinationAgentCode>
            $message .= "<ImporterRefCode>{$ISF_data['ImporterRefCode']}</ImporterRefCode>";// <ImporterRefCode>SHA_ZJTPS</ImporterRefCode>  TODO
            $message .= "<SellingParties><SellingRefCode>{$ISF_data['SellingParties']['SellingRefCode']}</SellingRefCode></SellingParties>";// <SellingParties><SellingRefCode /></SellingParties>  TODO
            $message .= "<ContainerStuffingLocations><ContainerStuffingLocationRefCode>{$ISF_data['ContainerStuffingLocations']['ContainerStuffingLocationRefCode']}</ContainerStuffingLocationRefCode></ContainerStuffingLocations>";// <ContainerStuffingLocations><ContainerStuffingLocationRefCode></ContainerStuffingLocationRefCode></ContainerStuffingLocations>  TODO

            $message .= "<Consolidators><ConsolidatorRefCode>{$ISF_data['Consolidators']['ConsolidatorRefCode']}</ConsolidatorRefCode></Consolidators>";// <Consolidators><ConsolidatorRefCode></ConsolidatorRefCode></Consolidators>  TODO
            $message .= "<BuyingParties><BuyingRefCode>{$ISF_data['BuyingParties']['BuyingRefCode']}</BuyingRefCode></BuyingParties>";// <BuyingParties><BuyingRefCode></BuyingRefCode></BuyingParties>  TODO
            $message .= "<Consignees><ConsigneeRefCode>{$ISF_data['Consignees']['ConsigneeRefCode']}</ConsigneeRefCode></Consignees>";// <Consignees><ConsigneeRefCode></ConsigneeRefCode></Consignees>  TODO

            $message .= "<ContainerShipTos><ShipToRefCode>{$ISF_data['ContainerShipTos']['ShipToRefCode']}</ShipToRefCode></ContainerShipTos>";// <ContainerShipTos><ShipToRefCode></ShipToRefCode></ContainerShipTos>  TODO

            $message .= "<Manufacturers>";
            $message .= "<Manufacturer>";
            $message .= "<CountryOrigin code=\"{$ISF_data['Manufacturers']['Manufacturer']['CountryOrigin']['code']}\"></CountryOrigin>";
            $message .= "<ManufacturerRefCode>{$ISF_data['Manufacturers']['Manufacturer']['ManufacturerRefCode']}</ManufacturerRefCode>";
            $message .= "<HTSCodes>";
            $HTSCodes = array_unique(array_filter(explode(',', ts_replace($ISF_data['Manufacturers']['Manufacturer']['HTSCodes']))));
            foreach ($HTSCodes as $this_hs_code){
                if(strlen($this_hs_code) != 6){
                    $result['msg'] = lang('HTSCodes:{hs_code} HS编码只能6位', array('hs_code' => $this_hs_code));
                    echo json_encode($result);
                    return;
                }
                $this_hs_code = trim($this_hs_code);
                $message .= "<HTSCode>$this_hs_code</HTSCode>";
            }

            $message .= "</HTSCodes>";
            $message .= "</Manufacturer>";

            $message .= "</Manufacturers>";
        }else if($ISFType == 'SF05'){
            $message .= "<ShipmentFileNumber>{$ISF_data['ShipmentFileNumber']}</ShipmentFileNumber>";
            $message .= "<ShipmentTypeCode>{$ISF_data['ShipmentTypeCode']}</ShipmentTypeCode>";
            $message .= "<AMSSCAC>{$ISF_data['AMSSCAC']}</AMSSCAC>";
            $message .= "<AMSBLNumber>{$ISF_data['AMSBLNumber']}</AMSBLNumber>";
            $message .= "<AMSBLType>{$ISF_data['AMSBLType']}</AMSBLType>";
            $message .= "<MasterBillNo>{$ISF_data['MasterBillNo']}</MasterBillNo>";
            $message .= "<OnBoardDate>{$ISF_data['OnBoardDate']}</OnBoardDate>";
            $message .= "<OriginAgentCode>{$ISF_data['OriginAgentCode']}</OriginAgentCode>";
            $message .= "<DestinationAgentCode>{$ISF_data['DestinationAgentCode']}</DestinationAgentCode>";
            $message .= "<BookingPartyRefCode>{$ISF_data['BookingPartyRefCode']}</BookingPartyRefCode>";
            $message .= "<PlaceDeliveryCountryCode>{$ISF_data['PlaceDeliveryCountryCode']}</PlaceDeliveryCountryCode>";//交货地国家代码，取目的港五字代码前2个
            $message .= "<ForeignPortofUnlading code=\"{$ISF_data['ForeignPortofUnlading']['code']}\">{$ISF_data['ForeignPortofUnlading']['Value']}</ForeignPortofUnlading>"; // -UNLoc codes (United Nations Location Code) can be found at http://www.unece.org/cefact/locode/
            $message .= "<PlaceofDelivery code=\"{$ISF_data['PlaceofDelivery']['code']}\">{$ISF_data['PlaceofDelivery']['Value']}</PlaceofDelivery>"; // UNLoc codes (United Nations Location Code) can be found at http://www.unece.org/cefact/locode/
            $message .= "<ContainerShipTos>";
            $message .= "<ShipToRefCode>{$ISF_data['ContainerShipTos']['ShipToRefCode']}</ShipToRefCode>";
            $message .= "</ContainerShipTos>";
            $message .= "<HTSCodes>";
            //2023-01-12 HS code改为支持多个
//            if(strlen($ISF_data['HTSCodes']) != 6){
//                $result['msg'] = 'HS编码只能单个且6位';
//                echo json_encode($result);
//                return;
//            }
            $HTSCodes = array_unique(array_filter(explode(',', $ISF_data['HTSCodes'])));
            foreach ($HTSCodes as $this_hs_code){
                if(strlen($this_hs_code) != 6){
                    $result['msg'] = lang('HTSCodes:{hs_code} HS编码只能6位', array('hs_code' => $this_hs_code));
                    echo json_encode($result);
                    return;
                }
                $this_hs_code = trim($this_hs_code);
                $message .= "<HTSCode>$this_hs_code</HTSCode>";
            }
            $message .= "</HTSCodes>";
        }else{
            $result['msg'] = '暂不支持';
            echo json_encode($result);
            return;
        }
        $message .= "</{$ISFType}>";

        $message .= "<ShippingParties>";
        $ShippingParties = $send_data['ShippingParties'];
        foreach ($ShippingParties as $ShippingParty) {
            $PartyCode = $ShippingParty['PartyCode'];
            $PartyType = $ShippingParty['PartyType'];
            $PartyName = $ShippingParty['PartyName'];
            $PartyIDType = $ShippingParty['PartyIDType'];
            $PartyID = $ShippingParty['PartyID'];
            $Address = $ShippingParty['PartyLocation']['Address'];
            $City = $ShippingParty['PartyLocation']['City'];
            $SubDivisionCode = $ShippingParty['PartyLocation']['SubDivisionCode'];
            $Region = $ShippingParty['PartyLocation']['Region'];
            $Country = $ShippingParty['PartyLocation']['Country']['code'];
            $PostalCode = $ShippingParty['PartyLocation']['PostalCode'];

            if(strlen($Address) > 100){
                echo json_encode(array('code' => 1, 'msg' => lang('地址长度不能超过100字符')));
                return;
            }
            //如果是美国和加拿大的话，地区代码必填
            if($Country == 'US' || $Country == 'CA'){
                if($SubDivisionCode == ''){
                    $result['msg'] = lang('PartyCode:{PartyCode} 美国或加拿大的州代码必填', array('PartyCode' => $PartyCode));
                    echo json_encode($result);
                    return;
                }
            }

            if(trim($PartyName) == '') continue;
            //SHIPPER
            $message .= "<ShippingParty>";

            $message .= "<PartyCode>$PartyCode</PartyCode>";
            $message .= "<PartyType>$PartyType</PartyType>";//默认给CU 往来单位类型
            $message .= "<PartyName>$PartyName</PartyName>";
            $message .= "<PartyIDType>{$PartyIDType}</PartyIDType>";//PartyIDType
            if($PartyIDType !== 'ZZ'){
                if($PartyID == ''){
                    $result['msg'] = lang('{PartyName}的PartyID未填写{PartyIDType}', array('PartyName' => $PartyName, 'PartyIDType' => $PartyIDType));
                    echo json_encode($result);
                    return;
                }
                $message .= "<PartyID>{$PartyID}</PartyID>";//$PartyID
            }

            $message .= "<PartyLocation>";

            $message .= "<Address>";
            $message .= "<AddressLine>" . substr($Address, 0, 50) . "</AddressLine>";
            if (strlen($Address) > 50) {
                $message .= "<AddressLine>" . substr($Address, 50, 50) . "</AddressLine>";
            }
            $message .= "</Address>";
            $message .= "<City>$City</City>";//城市必填
            if($SubDivisionCode != '')$message .= "<SubDivisionCode>$SubDivisionCode</SubDivisionCode>";
            $message .= "<Region>{$Region}</Region>";//地区名称必填 TODO
            $message .= "<PostalCode>$PostalCode</PostalCode>";//收货人，最好给一下postal code邮编
            $message .= "<Country code=\"$Country\"></Country>";

            $message .= "</PartyLocation>";
            $message .= "</ShippingParty>";
        }
        $message .= "</ShippingParties>";
        $message .= "</ISF>";

        $file_content = '<?xml version="1.0" encoding="UTF-8"?>' . $message;

        //保存文件xml
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        $new_dir = $root_path . '/upload/message/EFREIGHT_EDI/' . $type . '/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $shipment_id . '.xml';
        $file_path = $new_dir . $file_name;
        file_put_contents($file_path, $file_content);
        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => $type,
            'receiver' => 'EFREIGHT',
        );
        $id = update_message_by_shipment($shipment_id, $message_data, $type);
        echo json_encode(array('code' => 0, 'msg' => lang('文件生成成功'), 'data' => array('id' => $id)));
    }

    /**
     * aci 表单页面
     * @param int $consol_id
     */
    public function aci_set_data($consol_id = 0, $action='insert'){
        $is_reset = isset($_POST['is_reset']) ? (int)$_POST['is_reset'] : 0;

        $data = array();
        $data['consol_id'] = $consol_id;
        $data['action'] = $action;

        Model('biz_consol_model');
        $message = $this->model->get_where_one("id_type = 'consol' and id_no = $consol_id and type = 'ACI' and receiver = 'EFREIGHT'");
        $data['consol'] = $this->biz_consol_model->get_by_id($consol_id);
        $data['message'] = $message;
        if(empty($message) || $is_reset){
            $data['edi_data'] = $this->aci_data($consol_id);
        }else{
            Model('bsc_message_data_model');
            $message_data = $this->bsc_message_data_model->get_where_one("message_id = {$message['id']}");
            if(!empty($message_data)) $data['edi_data'] = json_decode($message_data['send_data'], true);
            else $data['edi_data'] = $this->aci_data($consol_id);
        }
        if(is_array($data['edi_data']))$data['edi_data'] = array_replace_str($data['edi_data'], array('\n' => "\n"));
        if(is_string($data['edi_data'])) exit($data['edi_data']);
        $data['ShippingParties'] = array_column($data['edi_data']['ShippingParties'], null, 'PartyCode');

        // $data['edi_data']['ShippingParties'] = array_column($data['edi_data']['ShippingParties'], null, 'PartyCode');
        Model('biz_shipment_model');
        $HouseNos = array_column($data['edi_data']['Master']['Houses'], 'HouseNo');
        $this->db->select('job_no,client_code,if(client_code2 is null or client_code2 = "",client_code,client_code2) as client_code2');
        $shipments = array_column($this->biz_shipment_model->no_role_get("job_no in ('" . join("','", $HouseNos) . "')"), null, 'job_no');
        $data['shipments'] = $shipments;

        $this->load->view('head');
        $this->load->view('/bsc/message/data/aci_form', $data);
    }

    private function aci_data($consol_id = 0){
        $this->load->model(array('biz_consol_model', 'biz_company_model', 'biz_shipment_model', 'biz_port_model', 'biz_container_model', 'biz_shipment_container_model', 'bsc_dict_model'
        ,'bsc_user_model', 'm_model', 'biz_sub_company_model'
        ));

        //获取或转换对应的数据--start
        $consol = $this->biz_consol_model->get_one('id', $consol_id);
        if (empty($consol)) {
            return 'consol empty';
        }

        $shipments = $this->biz_shipment_model->no_role_get("consol_id =  '" . $consol['id'] . "'");

        $containers = $this->biz_container_model->get('consol_id = ' . $consol['id']);

        $data = array();

        $data['Master'] = array();

        $data['Master']['FilerCode'] = '';//发送代码 不必填
        $data['Master']['MasterBillNo'] = $consol['carrier_ref'];//主单号,提单号

        if($consol['biz_type'] == 'EXPORT'){
            $OPType = 'OE';
        }else if($consol['biz_type'] == 'IMPORT'){
            $OPType = 'OT';
        }else{
            $OPType = 'OE';
        }
        $data['Master']['OPType'] = $OPType;//业务类型 OE：海运出口 OI：海运进口

        //originAgentCode 根据当前发送人判定，查询公司所属的code
        if(!isset($this->OriginAgentCodes[get_system_type()])){
            return lang('orginagent_code 未配置，请联系管理员');
        }
        $OriginAgentCode = $this->OriginAgentCodes[get_system_type()];
        $data['Master']['OriginAgentCode'] = $OriginAgentCode;//出口方代理 需要EFT设置 TODO

        $DestinationAgentCode = 'LAX';
        $data['Master']['DestinationAgentCode'] = $DestinationAgentCode;//进口方代理	需要EFT设置 TODO

        //$carrier_codes改为查询 efreight_trans_carrier
        $carrier_codes = $this->bsc_dict_model->get_one_eq('name', $consol['trans_carrier'], 'efreight_trans_carrier');
        if(empty($carrier_codes)){
            return lang('承运人不存在对应值,请联系管理员');
        }
        $CarrierNamecode = $carrier_codes['value'];
        $data['Master']['CarrierName'] = array();//进口方代理	需要EFT设置 TODO
        $data['Master']['CarrierName']['code'] = $CarrierNamecode;//船公司简码	船公司4码
        $data['Master']['CarrierName']['Value'] = '';//船公司简码	船公司4码

        $ConsignmentType = $consol['trans_mode'];
        if(!in_array($ConsignmentType, array('FCL', 'LCL'))){
            return lang('只能为FCL或LCL');
        }
        $data['Master']['ConsignmentType'] = $ConsignmentType;//业务模式 LCL

        //E - Export I - Import B - In-Bond / IT  required Bonded Freight Forwarder T - Tranship / IE S - In-Transit / TandE Transportation and Exportation F - Freight Remain on Board AMS : 只有I , F
        $data['Master']['FilingType'] = 'S';

        //船期信息列表--start
        $data['Master']['RouteInformation'] = array();

        $data['Master']['RouteInformation']['ArrivalVessel'] = array();

        $dict_imo = $this->bsc_dict_model->get_one_eq('name',  $consol["vessel"], 'vessel');
        $imo_number = '';
        if(!empty($dict_imo)) $imo_number = $dict_imo["ext1"];
        $data['Master']['RouteInformation']['ArrivalVessel']['IMO'] = $imo_number;

        $data['Master']['RouteInformation']['ArrivalVessel']['voyage'] = $consol['voyage'];
        $data['Master']['RouteInformation']['ArrivalVessel']['Value'] = $consol['vessel'];

//        if(substr($consol['trans_origin'], 0, 2) !== 'CN'){//起运港必须是CN开头
//            return '起运港必须是CN开头';
//        }
        // $trans_ETD = date('Y-m-d', strtotime($consol['trans_ETD']));
        // $des_ETA = date('Y-m-d', strtotime($consol['des_ETA']));
        if ($consol['trans_ETD'] == '0000-00-00 00:00:00' || $consol['trans_ETD'] == '0000-00-00') {
            $consol['trans_ETD'] = '';
        }
        if ($consol['des_ETA'] == '0000-00-00 00:00:00' || $consol['des_ETA'] == '0000-00-00') {
            $consol['des_ETA'] = '';
        }
        $data['Master']['RouteInformation']['LoadingPort'] = array();
        $data['Master']['RouteInformation']['LoadingPort']['code'] = $consol['trans_origin'];
        $data['Master']['RouteInformation']['LoadingPort']['date'] = $consol['trans_ETD'];
        $data['Master']['RouteInformation']['LoadingPort']['Value'] = $consol['trans_origin_name'];

        $data['Master']['RouteInformation']['DischargePort'] = array();
        $data['Master']['RouteInformation']['DischargePort']['code'] = $consol['trans_discharge'];
        $data['Master']['RouteInformation']['DischargePort']['date'] = $consol['des_ETA'];
        $data['Master']['RouteInformation']['DischargePort']['Value'] = $consol['trans_discharge_name'];

        $data['Master']['RouteInformation']['DestinationPort'] = array();
        $data['Master']['RouteInformation']['DestinationPort']['code'] = $consol['trans_destination'];
        $data['Master']['RouteInformation']['DestinationPort']['date'] = '';
        $data['Master']['RouteInformation']['DestinationPort']['Value'] = $consol['trans_destination_name'];

        //船期信息列表--end
        $data['Master']['MasterRefNo'] = $consol['job_no'];//主票参考号 TODO

        //集装箱信息列表--start
        $container_size_codes = $this->container_size_codes;
        $MasterContainers = array();
        foreach ($containers as $container){
            //箱信息--start
            $MasterContainer = array();
            $MasterContainer['ContainerNo'] = $container['container_no'];//箱号
            $MasterContainer['ContainerType'] = $container['container_size'];//箱型,需要转换下
            $MasterContainer['SealNo'] = $container['seal_no'];//箱封号
            $MasterContainer['ServiceCode'] = '';//服务代码	CY: Container Yard CS:Container Station DD:Door-to-Door
            $MasterContainer['ShipperOwned'] = '';//
            //箱信息--end
            $MasterContainers[] = $MasterContainer;
        }
        $data['Master']['MasterContainers'] = $MasterContainers;
        //集装箱信息列表--end

        $Unit_codes = $this->unit_codes;


        $ShippingParties = array();
        $Houses = array();

        foreach ($shipments as $shipment){
            $shipment_containers = $this->biz_shipment_container_model->join_container_get_by_id($shipment['id'], $consol_id);
            if(empty($shipment_containers)){
                return lang('{job_no}  container 不存在', array('job_no' => $shipment['job_no']));
            }

            if(strlen($shipment['job_no']) <= 12){
                $HouseNo = $shipment['job_no'];//$HouseNo 最多12位 这里将前面的JH KM等修改未J K
            }
            else if(strlen($shipment['job_no']) == 13){
                $HouseNo = substr($shipment['job_no'], 0,1).substr($shipment['job_no'], 2);//$HouseNo 最多12位 这里将前面的JH KM等修改未J K
            }
            else{
                return lang('{job_no}  当前job_no 编号格式暂不支持', array('job_no' => $shipment['job_no']));
            }

            $House = array();
//            $House['LastHBLIndicator'] = '';//bool 非必填
            $House['HouseNo'] = $HouseNo;//分单号
            $House['OriginalHouseNo'] = '';//拆单使用，提供原始Housenumber

//            $House['SecondNotifyParty'] = '';//
            $House['PlaceReceipt'] = array();//出货地代码
            $House['PlaceReceipt']['code'] = $consol['trans_origin'];
            $House['PlaceReceipt']['date'] = $consol['trans_ETD'];
            $House['PlaceReceipt']['Value'] = $shipment['trans_origin_name'];

            $HouseContainers = array();

            if(!isset($Unit_codes[$shipment['good_outers_unit']])){
                return lang('{job_no} 包装类型代码无对应值', array('job_no' => $shipment['job_no']));
            }
            foreach ($shipment_containers as $shipment_container){
                $HouseContainer = array();

                $HouseContainer['ContainerRefNo'] = $shipment_container['container_no'];//箱号
                $HouseContainer['Package'] = $shipment_container['packs'];//件数
//                $HouseContainer['Unit'] = array();//包装类型
//                $HouseContainer['Unit']['code'] = $Unit_code;//包装类型代码
//                $HouseContainer['Unit']['Value'] = $shipment['good_outers_unit'];//包装类型名称
                $HouseContainer['Unit'] = $shipment['good_outers_unit'];//包装类型名称
                $HouseContainer['CountryOrigin'] = array();//原产国
                $HouseContainer['CountryOrigin']['code'] = 'CN';//原产国代码
                $HouseContainer['CountryOrigin']['Value'] = 'CHINA';//原产国名称
                $HouseContainer['GrossWeight'] = $shipment_container['weight'];//重量（KG）
                $HouseContainer['CBM'] = $shipment_container['volume'];//体积（CBM）
                $HouseContainer['Marking'] = 'N/M';//唛头
                $HouseContainer['Description'] = $shipment['description'];//货物描述

                $HouseContainers[] = $HouseContainer;
            }
            $House['HouseContainers'] = $HouseContainers;


            //开始填充客户数据--start
            //shipper--start
            $shipper = $this->biz_company_model->get_one("client_code = '{$shipment['client_code2']}' and company_type = 'shipper' and company_name = '" . str_replace("'","\'", $shipment['shipper_company']) . "'");
            if(empty($shipper)){
                if(empty($shipment['client_code2'])) $shipment['client_code2']=$shipment['client_code'];
                $insert_data = array();
                $insert_data['client_code'] = $shipment['client_code2'];
                $insert_data['company_type'] = 'shipper';
                $insert_data['company_name'] = $shipment['shipper_company'];
                $insert_data['company_address'] = $shipment['shipper_address'];
                $new_id = $this->biz_company_model->save($insert_data);
                $shipper = array('id' => $new_id, 'company_name' => $shipment['shipper_company'], 'company_address' => $shipment['shipper_address'], 'city' => '', 'country' => '', 'postalcode' => '', 'company_contact' => '', 'company_email' => '', 'company_telephone' => '','country_name'=>'','region'=>'','region_code'=>'');
            }

            $House['ShipperRefCode'] = $shipper['id'];//发货人参考号

            $ShippingParty = array();
            $ShippingParty['PartyCode'] = $shipper['id'];
            $ShippingParty['PartyType'] = 'CU';//默认给CU 往来单位类型
            $ShippingParty['PartyName'] = $shipment['shipper_company'];

            $ShippingParty['PartyLocation'] = array();
            $ShippingParty['PartyLocation']['Address'] = $shipment['shipper_address'];
            $ShippingParty['PartyLocation']['City'] = $shipper['city'];//城市必填
            $ShippingParty['PartyLocation']['Country'] = array();
            $ShippingParty['PartyLocation']['Country']['code'] = $shipper['country'];
            $ShippingParty['PartyLocation']['Country']['Value'] = $shipper['country_name'];
            $ShippingParty['PartyLocation']['SubDivisionCode'] = $shipper['region_code'];//地区代码必填 TODO
            $ShippingParty['PartyLocation']['Region'] = $shipper['region'];//地区名称必填 TODO
            $ShippingParty['PartyLocation']['PostalCode'] = $shipper['postalcode'];//收货人，最好给一下postal code邮编

            $ShippingParty['PartyRemark'] = '';//450个字符 备注

            $ShippingParty['ContactPersons'] = array();
            $ShippingParty['ContactPersons']['ContactName'] = $shipment['shipper_contact'];//联系人
            $ShippingParty['ContactPersons']['ContactMail'] = $shipment['shipper_email'];//邮箱
            $ShippingParty['ContactPersons']['Phone'] = $shipment['shipper_telephone'];//电话号码

            $ShippingParties[] = $ShippingParty;
            //shipper--end

            //consignee--start
            $consignee = $this->biz_company_model->get_one("client_code = '{$shipment['client_code2']}' and company_type = 'consignee' and company_name = '" . str_replace("'","\'", $shipment['consignee_company']) . "'");
            if(empty($consignee)){
                if(empty($shipment['client_code2'])) $shipment['client_code2']=$shipment['client_code'];
                $insert_data = array();
                $insert_data['client_code'] = $shipment['client_code2'];
                $insert_data['company_type'] = 'consignee';
                $insert_data['company_name'] = $shipment['consignee_company'];
                $insert_data['company_address'] = $shipment['consignee_address'];
                $new_id = $this->biz_company_model->save($insert_data);
                $consignee = array('id' => $new_id, 'company_name' => $shipment['consignee_company'], 'company_address' => $shipment['consignee_address'], 'city' => '', 'country' => '', 'postalcode' => '', 'company_contact' => '', 'company_email' => '', 'company_telephone' => '');
            }
            $House['ConsigneeRefCode'] = $consignee['id'];//收货人参考号

            $ShippingParty = array();
            $ShippingParty['PartyCode'] = $consignee['id'];
            $ShippingParty['PartyType'] = 'CU';//默认给CU 往来单位类型
            $ShippingParty['PartyName'] = $shipment['consignee_company'];

            $ShippingParty['PartyLocation'] = array();
            $ShippingParty['PartyLocation']['Address'] = $shipment['consignee_address'];
            $ShippingParty['PartyLocation']['City'] = $consignee['city'];//城市必填
            $ShippingParty['PartyLocation']['Country'] = array();
            $ShippingParty['PartyLocation']['Country']['code'] = $consignee['country'];
            $ShippingParty['PartyLocation']['Country']['Value'] = $consignee['country_name'];
            $ShippingParty['PartyLocation']['SubDivisionCode'] = $consignee['region_code'];//地区代码必填 TODO
            $ShippingParty['PartyLocation']['Region'] = $consignee['region'];//地区名称必填 TODO
            $ShippingParty['PartyLocation']['PostalCode'] = $consignee['postalcode'];//收货人，最好给一下postal code邮编

            $ShippingParty['PartyRemark'] = '';//450个字符 备注

            $ShippingParty['ContactPersons'] = array();
            $ShippingParty['ContactPersons']['ContactName'] = $shipment['consignee_contact'];//联系人
            $ShippingParty['ContactPersons']['ContactMail'] = $shipment['consignee_email'];//邮箱
            $ShippingParty['ContactPersons']['Phone'] = $shipment['consignee_telephone'];//电话号码

            $ShippingParties[] = $ShippingParty;
            //consignee--end

            //notify--start
            $House['NotifyRefCode'] = $House['ConsigneeRefCode'];//通知人参考号
            if($shipment['notify_company'] !== 'SAME AS CONSIGNEE'){
                $notify = $this->biz_company_model->get_one("client_code = '{$shipment['client_code2']}' and company_type = 'notify' and company_name = '" . str_replace("'","\'", $shipment['notify_company']) . "'");
                if(empty($notify)){
                    if(empty($shipment['client_code2'])) $shipment['client_code2']=$shipment['client_code'];
                    $insert_data = array();
                    $insert_data['client_code'] = $shipment['client_code2'];
                    $insert_data['company_type'] = 'notify';
                    $insert_data['company_name'] = $shipment['notify_company'];
                    $insert_data['company_address'] = $shipment['notify_address'];
                    $new_id = $this->biz_company_model->save($insert_data);
                    $notify = array('id' => $new_id, 'company_name' => $shipment['notify_company'], 'company_address' => $shipment['notify_address'], 'city' => '', 'country' => '', 'postalcode' => '', 'company_contact' => '', 'company_email' => '', 'company_telephone' => '');
                }

                $ShippingParty = array();

                $ShippingParty['PartyCode'] = $notify['id'];
                $ShippingParty['PartyType'] = 'CU';//默认给CU 往来单位类型
                $ShippingParty['PartyName'] = $shipment['notify_company'];

                $ShippingParty['PartyLocation'] = array();
                $ShippingParty['PartyLocation']['Address'] = $shipment['notify_address'];
                $ShippingParty['PartyLocation']['City'] = $notify['city'];//城市必填

                $ShippingParty['PartyLocation']['Country'] = array();
                $ShippingParty['PartyLocation']['Country']['code'] = $notify['country'];
                $ShippingParty['PartyLocation']['Country']['Value'] = $notify['country_name'];

                $ShippingParty['PartyLocation']['SubDivisionCode'] = $notify['region_code'];//地区代码必填 TODO
                $ShippingParty['PartyLocation']['Region'] = $notify['region'];//地区名称必填 TODO
                $ShippingParty['PartyLocation']['PostalCode'] = $notify['postalcode'];//收货人，最好给一下postal code邮编

                $ShippingParty['PartyRemark'] = '';//450个字符 备注

                $ShippingParty['ContactPersons'] = array();
                $ShippingParty['ContactPersons']['ContactName'] = $shipment['notify_contact'];//联系人
                $ShippingParty['ContactPersons']['ContactMail'] = $shipment['notify_email'];//邮箱
                $ShippingParty['ContactPersons']['Phone'] = $shipment['notify_telephone'];//电话号码

                $ShippingParties[] = $ShippingParty;
            }
            //notify--end
            $Houses[] = $House;

        }
        $data['Master']['Houses'] = $Houses;
        $data['ShippingParties'] = $ShippingParties;

        return $data;
    }

    /**
     * 生成报文AMS
     * @param $consol_id
     * @param $file_function
     */
    public function message_aci($consol_id, $file_function = 'A'){
        $this->load->model(array('bsc_message_data_model','bsc_dict_model', 'biz_sub_company_model', 'bsc_user_model'));

        $type = 'ACI';
        $result = array('code' => 1, 'msg' => 'error');

        $message_row = $this->model->get_where_one("id_type = 'consol' and id_no = $consol_id and type = '$type' and receiver = 'EFREIGHT'");
        if(empty($message_row)){
            $result['msg'] = lang('请先保存数据后再试');
            echo json_encode($result);
            return;
        }
        $message_data = $this->bsc_message_data_model->get_where_one("message_id = '{$message_row['id']}'");
        if(empty($message_data)){
            $result['msg'] = lang('请先保存数据后再试');
            echo json_encode($result);
            return;
        }

        $send_data = json_decode($message_data['send_data'], true);

        $str_replace = array('\n' => "\n", "&" => "&amp;", "<" => "&lt;", ">" => "&gt;");
        $send_data = array_replace_str($send_data, $str_replace, true);

        //OPType
        $ConsignmentType = $send_data['Master']['ConsignmentType'];
        $ConsignmentType_array = array('FCL', 'LCL');
        if(!in_array($ConsignmentType, $ConsignmentType_array)){
            $result['msg'] = lang('不是FCL或LCL');
            echo json_encode($result);
            return;
        }
        $OPType = $send_data['Master']['OPType'];
        $OPType_array = array('OE', 'OT');
        if(!in_array($OPType, $OPType_array)){
            $result['msg'] = lang('业务类型错误');
            echo json_encode($result);
            return;
        }
        //船公司codeSCAC	Carrier Name
        //$carrier_codes改为查询 efreight_trans_carrier
        $isset_dict = $this->bsc_dict_model->get_one_eq('value', $send_data['Master']['CarrierName']['code'], 'efreight_trans_carrier');
        if(empty($isset_dict)){
            $result['msg'] = lang('承运人不存在对应值,请联系管理员');
            echo json_encode($result);
            return;
        }


        //E - Export I - Import B - In-Bond / IT  required Bonded Freight Forwarder T - Tranship / IE S - In-Transit / TandE Transportation and Exportation F - Freight Remain on Board AMS : 只有I , F
        $FilingType = $send_data['Master']['FilingType'];
        $FilingType_array = array('I', 'B', 'S','F');
        if(!in_array($FilingType, $FilingType_array)){
            $result['msg'] = lang('ACI暂不支持该FilingType');
            echo json_encode($result);
            return;
        }
        $SubmitterID = getUserField(get_session('id'), 'email');


        //船公司codeSCAC	Carrier Name
        $container_size_codes = $this->container_size_codes;

        $Unit_codes = $this->unit_codes;

        //航次填写编号,如果没有填写编号,且大于5位,不给提交
        $voyage = $send_data['Master']['RouteInformation']['ArrivalVessel']['voyage'];
        if(empty($voyage)){
            $result['msg'] = lang('请填写航次');
            echo json_encode($result);
            return;
        }
        if(strlen($voyage) > 5){
            $result['msg'] = lang('ACI 航次不能大于5位,请填写额外参数 voyage_number');
            echo json_encode($result);
            return;
        }
        if(isset($consol_ext['vessel']) && !empty($consol_ext['vessel']))$consol['vessel'] = $consol_ext['vessel'];

        //IMO必须是7位整数
        $vessel = $send_data['Master']['RouteInformation']['ArrivalVessel']['Value'];
        $imo_number = $send_data['Master']['RouteInformation']['ArrivalVessel']['IMO'];
        if(!is_numeric($imo_number) || strlen($imo_number) != 7){
            $result['msg'] = lang('IMO 必须是7位整数');
            echo json_encode($result);
            return;
        }

        //E - Export I - Import B - In-Bond / IT  required Bonded Freight Forwarder T - Tranship / IE S - In-Transit / TandE Transportation and Exportation F - Freight Remain on Board AMS : 只有I , F
        $trans_discharge = $send_data['Master']['RouteInformation']['DischargePort']['code'];
        $trans_discharge_name = $send_data['Master']['RouteInformation']['DischargePort']['Value'];
        $trans_discharge_date = $send_data['Master']['RouteInformation']['DischargePort']['date'];

        $trans_destination = $send_data['Master']['RouteInformation']['DestinationPort']['code'];
        $trans_destination_name = $send_data['Master']['RouteInformation']['DestinationPort']['Value'];
        $trans_destination_date = $send_data['Master']['RouteInformation']['DestinationPort']['date'];

        $trans_origin = $send_data['Master']['RouteInformation']['LoadingPort']['code'];
        $trans_origin_name = $send_data['Master']['RouteInformation']['LoadingPort']['Value'];
        $trans_origin_date = $send_data['Master']['RouteInformation']['LoadingPort']['date'];
//        if(substr($trans_origin, 0, 2) !== 'CN'){//起运港必须是CN开头
//            $result['msg'] = '起运港必须是CN开头';
//            echo json_encode($result);
//            return;
//        }
        // if(substr($trans_discharge, 0, 2) !== 'US'){//目的港必须是US开头
        //     $result['msg'] = '目的港必须是US开头';
        //     echo json_encode($result);
        //     return;
        // }

        $time = time();

        //开始填入数据
        $MessageID = date('YmdHis', $time) . $message_row['id'];//唯一标识XML值 TODO
        $SenderCode = 'CUZHUN';
        $ManifestType = 'ACI';
        $CompanyCode = 'LEAGUE';
        $BranchCode = 'SHA';
        $SendDateTime = date('YmdHis', $time);
        $Version = '2.4';

        $MasterBillNo = $send_data['Master']['MasterBillNo'];
        $OriginAgentCode = $send_data['Master']['OriginAgentCode'];
        $DestinationAgentCode = $send_data['Master']['DestinationAgentCode'];
        $ShippingParty_Code = array();

        $message = "";
        $message .= "<Manifest>";

        $message .= "<Header>";
        $message .= "<MessageID>$MessageID</MessageID>";
        $message .= "<ActionCode>$file_function</ActionCode>";//A -  新增不发生 AS- 新增并发生 R– 修改 D- 删除
        $message .= "<SenderCode>$SenderCode</SenderCode>";//发送方软件供应商代码，需要EFT设置 TODO
        $message .= "<ManifestType>$ManifestType</ManifestType>";//ACI、AMS、AFR、ISF 目前固定AMS
        $message .= "<CompanyCode>$CompanyCode</CompanyCode>";//公司代码 TODO
        $message .= "<SubmitterID>$SubmitterID</SubmitterID>";//Emai地址 以FTP上传Xml时，若Xml文件解析，数据校验发生错误，将通过Email通知错误信息。Email地址以分号(;)分隔
        $message .= "<BranchCode>$BranchCode</BranchCode>";//分公司代码 TODO
        $message .= "<SendDateTime>$SendDateTime</SendDateTime>";//Format:yyyyMMddHHmmss 发送时间
        $message .= "<Version>$Version</Version>";//版本
        $message .= "</Header>";

        $message .= "<Master>";
//        $message .= "<FilerCode></FilerCode>";//发送代码 不必填

        $CarrierNamecode = $send_data['Master']['CarrierName']['code'];
        $ConsignmentType = $send_data['Master']['ConsignmentType'];
        $message .= "<MasterBillNo>$MasterBillNo</MasterBillNo>";//主单号,提单号
        $message .= "<OPType>$OPType</OPType>";//业务类型 OE：海运出口 OI：海运进口
        $message .= "<OriginAgentCode>$OriginAgentCode</OriginAgentCode>";//出口方代理 需要EFT设置 TODO
        $message .= "<DestinationAgentCode>$DestinationAgentCode</DestinationAgentCode>";//进口方代理	需要EFT设置 TODO
        $message .= "<CarrierName code=\"$CarrierNamecode\"></CarrierName>";//船公司简码	船公司4码
        $message .= "<ConsignmentType>$ConsignmentType</ConsignmentType>";//业务模式 LCL
        $message .= "<FilingType>$FilingType</FilingType>";//

        $message .= "<RouteInformation>";//船期信息列表--start
        $message .= "<ArrivalVessel IMO=\"{$imo_number}\" voyage=\"{$voyage}\">{$vessel}</ArrivalVessel>";//
        $message .= "<LoadingPort code=\"{$trans_origin}\" date=\"{$trans_origin_date}\">{$trans_origin_name}</LoadingPort>";//
        $message .= "<DischargePort code=\"{$trans_discharge}\" date=\"{$trans_discharge_date}\">{$trans_discharge_name}</DischargePort>";//
        $message .= "<DestinationPort code=\"{$trans_destination}\" date=\"{$trans_destination_date}\">{$trans_destination_name}</DestinationPort>";//
        $message .= "</RouteInformation>";//船期信息列表--end

        $MasterRefNo = $send_data['Master']['MasterRefNo'];
        $message .= "<MasterRefNo>$MasterRefNo</MasterRefNo>";//主票参考号 TODO

        $message .= "<MasterContainers>";//集装箱信息列表--start

        $containers = $send_data['Master']['MasterContainers'];
        foreach ($containers as $container){
            if(isset($container_size_codes[$container['ContainerType']])){
                $ContainerType = $container_size_codes[$container['ContainerType']];
            }else{
                $result['msg'] = lang('存在未配置的箱型{ContainerType}', array('ContainerType' => $container['ContainerType']));
                echo json_encode($result);
                return;
            }

            $message .= "<Container>";//箱信息--start
            $message .= "<ContainerNo>{$container['ContainerNo']}</ContainerNo>";//箱号
            $message .= "<ContainerType>$ContainerType</ContainerType>";//箱型,需要转换下
            $message .= "<SealNo>{$container['SealNo']}</SealNo>";//箱封号
//            $message .= "<ServiceCode></ServiceCode>";//服务代码	CY: Container Yard CS:Container Station DD:Door-to-Door
//            $message .= "<ShipperOwned></ShipperOwned>";//
            $message .= "</Container>";//箱信息--end
        }
        $message .= "</MasterContainers>";//集装箱信息列表--end

        $message .= "<Houses>";
        $shipments = $send_data['Master']['Houses'];
        foreach ($shipments as $shipment){
            $HouseNo = $shipment['HouseNo'];
            $ShipperRefCode = $shipment['ShipperRefCode'];
            $ConsigneeRefCode = $shipment['ConsigneeRefCode'];
            $NotifyRefCode = $shipment['NotifyRefCode'];
            $ShippingParty_Code[] = $ShipperRefCode;
            $ShippingParty_Code[] = $ConsigneeRefCode;
            $ShippingParty_Code[] = $NotifyRefCode;

            if(strlen($HouseNo) <= 12){
            }
            else if(strlen($HouseNo) == 13){
                $HouseNo = substr($HouseNo, 0,1).substr($HouseNo, 2);//$HouseNo 最多12位 这里将前面的JH KM等修改未J K
            }
            else{
                $result['msg'] = lang('{HouseNo}  当前job_no 编号格式暂不支持', array('HouseNo' => $HouseNo));
                echo json_encode($result);
                return;
            }

            $message .= "<House>";
//            $message .= "<LastHBLIndicator></LastHBLIndicator>";//bool 非必填
            $message .= "<HouseNo>{$HouseNo}</HouseNo>";//分单号
//            $message .= "<OriginalHouseNo>SCAC{$shipment['cus_no']}</OriginalHouseNo>";//拆单使用，提供原始Housenumber
            $message .= "<ShipperRefCode>$ShipperRefCode</ShipperRefCode>";//发货人参考号
            $message .= "<ConsigneeRefCode>$ConsigneeRefCode</ConsigneeRefCode>";//收货人参考号
            $message .= "<NotifyRefCode>$NotifyRefCode</NotifyRefCode>";//通知人参考号

            $trans_receipt = $shipment['PlaceReceipt']['code'];
            $trans_receipt_name = $shipment['PlaceReceipt']['Value'];
            $trans_receipt_date = $shipment['PlaceReceipt']['date'];

//            $message .= "<SecondNotifyParty></SecondNotifyParty>";//
            $message .= "<PlaceReceipt code=\"{$trans_receipt}\" date=\"{$trans_receipt_date}\">{$trans_receipt_name}</PlaceReceipt>";
            if(strlen($trans_receipt) != 5){
                $result['msg'] = lang('{HouseNo}  PlaceReceipt Code必须是5位', array('HouseNo' => $HouseNo));
                echo json_encode($result);
                return;
            }

            $message .= "<HouseContainers>";
            $shipment_containers = isset($shipment['HouseContainers']) ? $shipment['HouseContainers'] : array();
            foreach ($shipment_containers as $shipment_container){
                if(isset($Unit_codes[$shipment_container['Unit']])){
                    $Unit_code = $Unit_codes[$shipment_container['Unit']];
                }else{
                    $result['msg'] = lang('{HouseNo} 包装类型代码无对应值', array('HouseNo' => $HouseNo));
                    echo json_encode($result);
                    return;
                }
                $Unit_value = $shipment_container['Unit'];
                $CountryOrigin_code = $shipment_container['CountryOrigin']['code'];
                $CountryOrigin_value = $shipment_container['CountryOrigin']['Value'];

                $message .= "<Container>";
                $message .= "<ContainerRefNo>{$shipment_container['ContainerRefNo']}</ContainerRefNo>";//箱号
                $message .= "<Package>{$shipment_container['Package']}</Package>";//件数
                $message .= "<Unit code=\"$Unit_code\">{$Unit_value}</Unit>";//包装类型代码
                $message .= "<CountryOrigin code=\"$CountryOrigin_code\">$CountryOrigin_value</CountryOrigin>";//原产国代码
                $message .= "<GrossWeight>{$shipment_container['GrossWeight']}</GrossWeight>";//重量（KG）
                $message .= "<CBM>{$shipment_container['CBM']}</CBM>";//体积（CBM）
                $message .= "<Marking>{$shipment_container['Marking']}</Marking>";//唛头
                $message .= "<Description>{$shipment_container['Description']}</Description>";//货物描述
                $message .= "</Container>";
            }

            $message .= "</HouseContainers>";

            $message .= "</House>";
        }
        $message .= "</Houses>";




        $message .= "</Master>";

        $message .= "<ShippingParties>";
        $ShippingParties = $send_data['ShippingParties'];
        foreach ($ShippingParties as $ShippingParty){
            if(!in_array($ShippingParty['PartyCode'], $ShippingParty_Code)){
                continue;
            }
            $PartyCode = $ShippingParty['PartyCode'];
            $PartyType = $ShippingParty['PartyType'];
            $PartyName = $ShippingParty['PartyName'];
            if(empty($PartyName)) continue;
            $Address = $ShippingParty['PartyLocation']['Address'];
            $City = $ShippingParty['PartyLocation']['City'];
            $Country = $ShippingParty['PartyLocation']['Country'];
            $SubDivisionCode = $ShippingParty['PartyLocation']['SubDivisionCode'];
            $Region = $ShippingParty['PartyLocation']['Region'];
            $PostalCode = $ShippingParty['PartyLocation']['PostalCode'];
            // $PartyRemark = $ShippingParty['PartyRemark'];
            $PartyRemark = '';
            //SHIPPER
            $message .= "<ShippingParty>";

            $message .= "<PartyCode>$PartyCode</PartyCode>";
            $message .= "<PartyType>$PartyType</PartyType>";//默认给CU 往来单位类型
            $message .= "<PartyName>$PartyName</PartyName>";

            $message .= "<PartyLocation>";

            $message .= "<Address>";
            $message .= "<AddressLine>" . substr($Address, 0, 50) . "</AddressLine>";
            if(strlen($Address) > 50){
                $message .= "<AddressLine>" . substr($Address, 50, 50) . "</AddressLine>";
            }
            $message .= "</Address>";
            $message .= "<City>$City</City>";//城市必填
            $message .= "<Country code=\"{$Country['code']}\"></Country>";
            //长度不等于2也不填
            if($SubDivisionCode != '' && strlen($SubDivisionCode) == 2)$message .= "<SubDivisionCode>$SubDivisionCode</SubDivisionCode>";//地区代码必填 TODO
            $message .= "<Region>$Region</Region>";//地区名称必填 TODO
            $message .= "<PostalCode>$PostalCode</PostalCode>";//收货人，最好给一下postal code邮编

            $message .= "</PartyLocation>";
            $message .= "<PartyRemark>$PartyRemark</PartyRemark>";//450个字符 备注
            $message .= "<ContactPersons>";//

            $ContactName = $ShippingParty['ContactPersons']['ContactName'];
            $ContactMail = $ShippingParty['ContactPersons']['ContactMail'];
            $Phone = $ShippingParty['ContactPersons']['Phone'];
            $message .= "<Person>";//
            $message .= "<ContactName>$ContactName</ContactName>";//联系人
            $message .= "<ContactMail>$ContactMail</ContactMail>";//邮箱
            $message .= "<Phone>$Phone</Phone>";//电话号码
            $message .= "</Person>";//

            $message .= "</ContactPersons>";//
            $message .= "</ShippingParty>";
        }
        $message .= "</ShippingParties>";
        $message .= "</Manifest>";

        $file_content = '<?xml version="1.0" encoding="UTF-8"?>' . $message;

        //保存文件xml
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        $new_dir = $root_path . '/upload/message/EFREIGHT_EDI/' . $type . '/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $message_row['id_type'] . '.xml';
        $file_path = $new_dir . $file_name;
        file_put_contents($file_path, $file_content);
        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => $type,
            'receiver' => 'EFREIGHT',
        );
        update_message($consol_id, $message_data, $type);
        echo json_encode(array('code' => 0, 'msg' => lang('文件生成成功'),));
    }

    public function emf_set_data($consol_id = 0, $action='insert')
    {
        $is_reset = isset($_POST['is_reset']) ? (int)$_POST['is_reset'] : 0;

        $data = array();
        $data['consol_id'] = $consol_id;
        $data['action'] = $action;

        Model('biz_consol_model');
        $data['consol'] = $this->biz_consol_model->get_by_id($consol_id);
        $message = $this->model->get_where_one("id_type = 'consol' and id_no = $consol_id and type = 'E-MANIFEST' and receiver = 'EFREIGHT'");
        $data['message'] = $message;
        if (empty($message) || $is_reset) {
            $data['edi_data'] = $this->emf_data($consol_id);
        } else {
            Model('bsc_message_data_model');
            $message_data = $this->bsc_message_data_model->get_where_one("message_id = {$message['id']}");
            if (!empty($message_data)) $data['edi_data'] = json_decode($message_data['send_data'], true);
            else $data['edi_data'] = $this->emf_data($consol_id);
        }
        if(is_array($data['edi_data']))$data['edi_data'] = array_replace_str($data['edi_data'], array('\n' => "\n"));
        if (is_string($data['edi_data'])) exit($data['edi_data']);
        $data['ShippingParties'] = array_column($data['edi_data']['ShippingParties'], null, 'PartyCode');
        foreach ($data['ShippingParties'] as $key => &$row){
            //修复下更新后的数据填充
            if(!isset($data['ShippingParties'][$data['edi_data']['Master']['ConsolidatorRefCode']])){
                $ShippingParty = array();
                $ShippingParty['PartyCode'] = $data['edi_data']['Master']['ConsolidatorRefCode'];
                $ShippingParty['PartyType'] = "CU";
                $ShippingParty['PartyName'] = '';
                $ShippingParty['PartyLocation'] = array();
                $ShippingParty['PartyLocation']['Address'] = '';
                $ShippingParty['PartyLocation']['City'] = '';
                $ShippingParty['PartyLocation']['Country'] = '';
                $ShippingParty['PartyLocation']['SubDivisionCode'] = '';
                $ShippingParty['PartyLocation']['PostalCode'] = '';
                $data['ShippingParties'][$data['edi_data']['Master']['ConsolidatorRefCode']] = $ShippingParty;
            }

            foreach ($data['edi_data']['Master']['Houses'] as $house){
                if(!isset($data['ShippingParties'][$house['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']])){
                    $ShippingParty = array();
                    $ShippingParty['PartyCode'] = $house['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode'];
                    $ShippingParty['PartyType'] = "CU";
                    $ShippingParty['PartyName'] = '';
                    $ShippingParty['PartyLocation'] = array();
                    $ShippingParty['PartyLocation']['Address'] = '';
                    $ShippingParty['PartyLocation']['City'] = '';
                    $ShippingParty['PartyLocation']['Country'] = '';
                    $ShippingParty['PartyLocation']['SubDivisionCode'] = '';
                    $ShippingParty['PartyLocation']['PostalCode'] = '';
                    $data['ShippingParties'][$house['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']] = $ShippingParty;
                }
            }
            //如果是字符串,那么转成数组
            if(is_string($row['PartyLocation']['Country'])) $row['PartyLocation']['Country'] = array('code' => $row['PartyLocation']['Country'], 'Value' => '');

        }

        Model('biz_shipment_model');
        $HouseNos = array_column($data['edi_data']['Master']['Houses'], 'HouseNo');
        $this->db->select('job_no,client_code,if(client_code2 is null or client_code2 = "",client_code,client_code2) as client_code2');
        $shipments = array_column($this->biz_shipment_model->no_role_get("job_no in ('" . join("','", $HouseNos) . "')"), null, 'job_no');
        $data['shipments'] = $shipments;

        $this->load->view('head');
        //  if(is_admin()){
        //     $this->load->view('/bsc/message/data/emf_form20220406', $data);
        //     return;
        // }
        $this->load->view('/bsc/message/data/emf_form', $data);
    }

    public function emf_data($consol_id = 0)
    {
        $this->load->model(array('biz_consol_model', 'biz_shipment_model', 'biz_shipment_container_model', 'biz_container_model', 'biz_sub_company_model', 'bsc_dict_model', 'biz_company_model'));

        $data = array();

        $this->db->select('carrier_ref,trans_carrier,trans_mode,vessel,voyage,trans_ETD,des_ETA,trans_origin,trans_origin_name,trans_discharge,trans_discharge_name,job_no,id,trans_mode,LCL_type');
        $consol = $this->biz_consol_model->get_one('id', $consol_id);
        if (empty($consol)) {
            return 'consol empty';
        }

        $data['Master'] = array();
        $data['Master']['ACISubMasterBillNo'] = array();
        $data['Master']['ACISubMasterBillNo']['code'] = '';
        $data['Master']['ACISubMasterBillNo']['Value'] = '';

        //originAgentCode 根据当前发送人判定，查询公司所属的code
        if(!isset($this->OriginAgentCodes[get_system_type()])){
            return lang('orginagent_code 未配置，请联系管理员');
        }
        $data['Master']['OriginAgentCode'] = $this->OriginAgentCodes[get_system_type()];
        $data['Master']['DestinationAgentCode'] = 'LAX';

        //$carrier_codes改为查询 efreight_trans_carrier
        $carrier_codes = $this->bsc_dict_model->get_one_eq('name', $consol['trans_carrier'], 'efreight_trans_carrier');
        if (empty($carrier_codes)) {
            return lang('承运人不存在对应值,请联系管理员');
        }
        $data['Master']['CarrierName'] = $carrier_codes['value'];
        $data['Master']['MasterBillNo'] = $carrier_codes['value'] . $consol['carrier_ref'];
        $data['Master']['ConsignmentType'] = $consol['trans_mode'];
        $data['Master']['FilingType'] = '';

        $data['Master']['ConsolidatorRefCode'] = '';

        $data['Master']['RouteInformation'] = array();
        $data['Master']['RouteInformation']['ArrivalVessel'] = array();

        $dict_imo = $this->bsc_dict_model->get_one_eq('name', $consol["vessel"], 'vessel');
        $imo = '';
        if (!empty($dict_imo)) $imo = $dict_imo["ext1"];
        $data['Master']['RouteInformation']['ArrivalVessel']['IMO'] = $imo;
        $data['Master']['RouteInformation']['ArrivalVessel']['voyage'] = $consol['voyage'];
        $data['Master']['RouteInformation']['ArrivalVessel']['Value'] = $consol['vessel'];

        if ($consol['trans_ETD'] == '0000-00-00 00:00:00' || $consol['trans_ETD'] == '0000-00-00') {
            $consol['trans_ETD'] = '';
        }
        $data['Master']['RouteInformation']['LoadingPort'] = array();
        $data['Master']['RouteInformation']['LoadingPort']['code'] = $consol['trans_origin'];
        $data['Master']['RouteInformation']['LoadingPort']['date'] = $consol['trans_ETD'];
        $data['Master']['RouteInformation']['LoadingPort']['Value'] = $consol['trans_origin_name'];

        if ($consol['des_ETA'] == '0000-00-00 00:00:00' || $consol['des_ETA'] == '0000-00-00') {
            $consol['des_ETA'] = '';
        }
        $data['Master']['RouteInformation']['DischargePort'] = array();
        $data['Master']['RouteInformation']['DischargePort']['code'] = $consol['trans_discharge'];
        $data['Master']['RouteInformation']['DischargePort']['date'] = $consol['des_ETA'];
        $data['Master']['RouteInformation']['DischargePort']['Value'] = $consol['trans_discharge_name'];

        $data['Master']['RouteInformation']['ACIDischargePort'] = array();
        $data['Master']['RouteInformation']['ACIDischargePort']['code'] = '';
        $data['Master']['RouteInformation']['ACIDischargePort']['subcode'] = '';
        $data['Master']['RouteInformation']['ACIDischargePort']['Value'] = '';

        $data['Master']['RouteInformation']['ACIDestinationPort'] = array();
        $data['Master']['RouteInformation']['ACIDestinationPort']['code'] = '';
        $data['Master']['RouteInformation']['ACIDestinationPort']['subcode'] = '';
        $data['Master']['RouteInformation']['ACIDestinationPort']['Value'] = '';

        $data['Master']['MasterRefNo'] = $consol['job_no'];

        $containers = $this->biz_container_model->get('consol_id = ' . $consol['id']);

        if (empty($containers)) {
            return lang('CONSOL 没有箱数据');
        }
        $data['Master']['MasterContainers'] = array();
        foreach ($containers as $container) {
            $Container = array();
            $Container['ContainerNo'] = $container['container_no'];
            $Container['ContainerType'] = $container['container_size'];
            $Container['SealNo'] = $container['seal_no'];

            $data['Master']['MasterContainers'][] = $Container;
        }

        $shipments = $this->biz_shipment_model->no_role_get("consol_id =  '" . $consol['id'] . "'");
        if (empty($shipments)) {
            return lang('没有SHIPMENT');
        }
        $data['Master']['Houses'] = array();
        $data['ShippingParties'] = array();

        $ConsolidatorRefCode = 'COSOLIDATOR' . $consol_id;
        // if ($consol['trans_mode'] == 'LCL') {
        $data['Master']['ConsolidatorRefCode'] = $ConsolidatorRefCode;
        $ShippingParty = array();
        $ShippingParty['PartyCode'] = $ConsolidatorRefCode;
        $ShippingParty['PartyType'] = "CU";
        $ShippingParty['PartyName'] = '';
        $ShippingParty['PartyLocation'] = array();
        $ShippingParty['PartyLocation']['Address'] = '';
        $ShippingParty['PartyLocation']['City'] = '';
        $ShippingParty['PartyLocation']['Country'] = '';
        $ShippingParty['PartyLocation']['SubDivisionCode'] = '';
        $ShippingParty['PartyLocation']['PostalCode'] = '';

        $data['ShippingParties'][] = $ShippingParty;
        // }

        foreach ($shipments as $shipment) {
            $shipment_containers = $this->biz_shipment_container_model->join_container_get_by_id($shipment['id'], $consol_id);
            if (empty($shipment_containers)) {
                return lang('{job_no}  container 不存在', array('job_no' => $shipment['job_no']));
            }

            if (strlen($shipment['job_no']) <= 12) {
                $HouseNo = $shipment['job_no'];//$HouseNo 最多12位 这里将前面的JH KM等修改未J K
            } else if (strlen($shipment['job_no']) == 13) {
                $HouseNo = substr($shipment['job_no'], 0, 1) . substr($shipment['job_no'], 2);//$HouseNo 最多12位 这里将前面的JH KM等修改未J K
            } else {
                return lang('{job_no}  当前job_no 编号格式暂不支持', array('job_no' => $shipment['job_no']));
            }

            $house = array();
            $house['HouseNo'] = $HouseNo;
            $house['PlaceReceipt'] = array();
            $house['PlaceReceipt']['code'] = $shipment['trans_origin'];
            $house['PlaceReceipt']['date'] = $consol['trans_ETD'];
            $house['PlaceReceipt']['Value'] = $shipment['trans_origin_name'];

            $house['PlaceDelivery'] = array();
            $house['PlaceDelivery']['code'] = $shipment['trans_destination'];
            $house['PlaceDelivery']['date'] = $consol['des_ETA'];
            $house['PlaceDelivery']['Value'] = $shipment['trans_destination_name'];

            $house['HouseContainers'] = array();
            $shipment['mark_nums'] = 'N/M';
            foreach ($shipment_containers as $shipment_container) {
                $house_container = array();
                $house_container['ContainerRefNo'] = $shipment_container['container_no'];
                $house_container['Package'] = $shipment_container['packs'];
                $house_container['Unit'] = $shipment['good_outers_unit'];
                $house_container['CountryOrigin'] = array();
                $house_container['CountryOrigin']['code'] = 'CN';
                $house_container['CountryOrigin']['Value'] = 'CHINA';
                $house_container['GrossWeight'] = $shipment_container['weight'];
                $house_container['CBM'] = $shipment_container['volume'];
                $house_container['Marking'] = $shipment['mark_nums'];
                $house_container['Description'] = $shipment['description'];
                $house['HouseContainers'][] = $house_container;
            }
            //拼进拼出由consol里的LCL类型决定 COLOADER 拼进

            $house['ColoadInSelfFilingIndicator'] = 'false';
            $house['ContainerLoadingPlaces'] = array();
            $house['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode'] = '';
            // if ($consol['LCL_type'] != 'CO LOADER') {
            // if($shipment['trans_mode'] == 'LCL'){
            $house['ColoadInSelfFilingIndicator'] = 'false';
            $house['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode'] = $ConsolidatorRefCode;
            // }
            // }

            //添加ShippingParty
            //SHIPPER
            //统一先name+address查询,查不到再name查
            $temp_address = str_replace("\n", '\n', str_replace("'","\'", $shipment['shipper_address']));
            $shipper = $this->biz_company_model->get_one("client_code = '{$shipment['client_code2']}' and company_type = 'shipper' and company_name = '" . str_replace("'","\'", $shipment['shipper_company']) . "' and company_address = '" . $temp_address . "'");
            if (empty($shipper)) {
                $shipper = $this->biz_company_model->get_one("client_code = '{$shipment['client_code2']}' and company_type = 'shipper' and company_name = '" . str_replace("'","\'", $shipment['shipper_company']) . "'");
            }
            if(empty($shipper)){
                if(empty($shipment['client_code2'])) $shipment['client_code2']=$shipment['client_code'];
                $insert_data = array();
                $insert_data['client_code'] = $shipment['client_code2'];
                $insert_data['company_type'] = 'shipper';
                $insert_data['company_name'] = $shipment['shipper_company'];
                $insert_data['company_address'] = $shipment['shipper_address'];
                $new_id = $this->biz_company_model->save($insert_data);
                $shipper = array('id' => $new_id, 'company_name' => $shipment['shipper_company'], 'company_address' => $shipment['shipper_address'], 'city' => '', 'country' => '', 'postalcode' => '', 'company_contact' => '', 'company_email' => '', 'company_telephone' => '','country_name'=>'','region'=>'','region_code'=>'');
            }
            // if (empty($shipper)) {
            //     return $shipment['job_no'] . " SHIPPER未找到, 请到shipment里点击发货人公司右边'E'进行维护";
            // }
            $ShippingParty = array();
            $ShippingParty['PartyCode'] = $shipper['id'];
            $ShippingParty['PartyType'] = "CU";
            $ShippingParty['PartyName'] = $shipper['company_name'];
            $ShippingParty['PartyLocation'] = array();
            $ShippingParty['PartyLocation']['Address'] = $shipper['company_address'];
            $ShippingParty['PartyLocation']['City'] = $shipper['city'];
            $ShippingParty['PartyLocation']['Country']['code'] = $shipper['country'];
            $ShippingParty['PartyLocation']['Country']['Value'] = $shipper['country_name'];
            $ShippingParty['PartyLocation']['SubDivisionCode'] = $shipper['region_code'];
            $ShippingParty['PartyLocation']['PostalCode'] = $shipper['postalcode'];
            $house['ShipperRefCode'] = $shipper['id'];

            $data['ShippingParties'][] = $ShippingParty;

            //consignee
            $temp_address = str_replace("\n", '\n', str_replace("'","\'", $shipment['consignee_address']));
            $consignee = $this->biz_company_model->get_one("client_code = '{$shipment['client_code2']}' and company_type = 'consignee' and company_name = '" . str_replace("'","\'", $shipment['consignee_company']) . "' and company_address = '" . $temp_address . "'");
            if (empty($consignee)) {
                $consignee = $this->biz_company_model->get_one("client_code = '{$shipment['client_code2']}' and company_type = 'consignee' and company_name = '" . str_replace("'","\'", $shipment['consignee_company']) . "'");
            }
            if(empty($consignee)){
                if(empty($shipment['client_code2'])) $shipment['client_code2']=$shipment['client_code'];
                $insert_data = array();
                $insert_data['client_code'] = $shipment['client_code2'];
                $insert_data['company_type'] = 'consignee';
                $insert_data['company_name'] = $shipment['consignee_company'];
                $insert_data['company_address'] = $shipment['consignee_address'];
                $new_id = $this->biz_company_model->save($insert_data);
                $consignee = array('id' => $new_id, 'company_name' => $shipment['consignee_company'], 'company_address' => $shipment['consignee_address'], 'city' => '', 'country' => '', 'postalcode' => '', 'company_contact' => '', 'company_email' => '', 'company_telephone' => '');
            }
            // if (empty($consignee)) {
            //     return $shipment['job_no'] . " consignee未找到, 请到shipment里点击收货人公司右边'E'进行维护";
            // }
            $ShippingParty = array();
            $ShippingParty['PartyCode'] = $consignee['id'];
            $ShippingParty['PartyType'] = "CU";
            $ShippingParty['PartyName'] = $consignee['company_name'];
            $ShippingParty['PartyLocation'] = array();
            $ShippingParty['PartyLocation']['Address'] = $consignee['company_address'];
            $ShippingParty['PartyLocation']['City'] = $consignee['city'];
            $ShippingParty['PartyLocation']['Country']['code'] = $consignee['country'];
            $ShippingParty['PartyLocation']['Country']['Value'] = $consignee['country_name'];
            $ShippingParty['PartyLocation']['SubDivisionCode'] = $consignee['region_code'];
            $ShippingParty['PartyLocation']['PostalCode'] = $consignee['postalcode'];
            $house['ConsigneeRefCode'] = $consignee['id'];
            if ($shipment['notify_company'] == 'SAME AS CONSIGNEE') $house['NotifyRefCode'] = $house['ConsigneeRefCode'];

            $data['ShippingParties'][] = $ShippingParty;
            if ($shipment['notify_company'] !== 'SAME AS CONSIGNEE') {
                //notify
                $temp_address = str_replace("\n", '\n', str_replace("'","\'", $shipment['notify_address']));
                $notify = $this->biz_company_model->get_one("client_code = '{$shipment['client_code2']}' and company_type = 'notify' and company_name = '" . str_replace("'","\'", $shipment['notify_company']) . "' and company_address = '" . $temp_address . "'");
                if (empty($notify)) {
                    $notify = $this->biz_company_model->get_one("client_code = '{$shipment['client_code2']}' and company_type = 'notify' and company_name = '" . str_replace("'","\'", $shipment['notify_company']) . "'");
                }
                if(empty($notify)){
                    if(empty($shipment['client_code2'])) $shipment['client_code2']=$shipment['client_code'];
                    $insert_data = array();
                    $insert_data['client_code'] = $shipment['client_code2'];
                    $insert_data['company_type'] = 'notify';
                    $insert_data['company_name'] = $shipment['notify_company'];
                    $insert_data['company_address'] = $shipment['notify_address'];
                    $new_id = $this->biz_company_model->save($insert_data);
                    $notify = array('id' => $new_id, 'company_name' => $shipment['notify_company'], 'company_address' => $shipment['notify_address'], 'city' => '', 'country' => '', 'postalcode' => '', 'company_contact' => '', 'company_email' => '', 'company_telephone' => '');
                }
                // if (empty($notify)) {
                //     return $shipment['job_no'] . " notify未找到, 请到shipment里点击通知人公司右边'E'进行维护";
                // }
                $ShippingParty = array();
                $ShippingParty['PartyCode'] = $notify['id'];
                $ShippingParty['PartyType'] = "CU";
                $ShippingParty['PartyName'] = $notify['company_name'];
                $ShippingParty['PartyLocation'] = array();
                $ShippingParty['PartyLocation']['Address'] = $notify['company_address'];
                $ShippingParty['PartyLocation']['City'] = $notify['city'];
                $ShippingParty['PartyLocation']['Country']['code'] = $notify['country'];
                $ShippingParty['PartyLocation']['Country']['Value'] = $notify['country_name'];
                $ShippingParty['PartyLocation']['SubDivisionCode'] = $notify['region_code'];
                $ShippingParty['PartyLocation']['PostalCode'] = $notify['postalcode'];
                $house['NotifyRefCode'] = $notify['id'];


                $data['ShippingParties'][] = $ShippingParty;

            }
            $data['Master']['Houses'][] = $house;
        }

        return $data;
    }

    /**
     * 生成报文AMS
     * @param $consol_id
     * @param $file_function
     */
    public function message_emf($consol_id, $file_function = 'A')
    {
        $this->load->model(array('bsc_message_data_model', 'bsc_dict_model', 'biz_sub_company_model', 'bsc_user_model'));

        $type = 'E-MANIFEST';
        $result = array('code' => 1, 'msg' => 'error');

        $message_row = $this->model->get_where_one("id_type = 'consol' and id_no = $consol_id and type = '$type' and receiver = 'EFREIGHT'");
        if (empty($message_row)) {
            $result['msg'] = lang('请先保存数据后再试');
            echo json_encode($result);
            return;
        }

        $message_data = $this->bsc_message_data_model->get_where_one("message_id = '{$message_row['id']}'");
        if (empty($message_data)) {
            $result['msg'] = lang('请先保存数据后再试');
            echo json_encode($result);
            return;
        }

        $send_data = json_decode($message_data['send_data'], true);

        $str_replace = array('\n' => "\n", "&" => "&amp;", "<" => "&lt;", ">" => "&gt;");
        $send_data = array_replace_str($send_data, $str_replace, true);

        $thisUserId = get_session('id');
        $thisUser = $this->bsc_user_model->get_by_id($thisUserId);
        $SubmitterID = $thisUser['email'];

        //OPType
        $ConsignmentType = $send_data['Master']['ConsignmentType'];
        $ConsignmentType_array = array('FCL', 'LCL');
        if (!in_array($ConsignmentType, $ConsignmentType_array)) {
            $result['msg'] = lang('不是FCL或LCL');
            echo json_encode($result);
            return;
        }
        //船公司codeSCAC	Carrier Name
        //$carrier_codes改为查询 efreight_trans_carrier
        $isset_dict = $this->bsc_dict_model->get_one_eq('value', $send_data['Master']['CarrierName'], 'efreight_trans_carrier');
        if (empty($isset_dict)) {
            $result['msg'] = lang('承运人不存在对应值,请联系管理员');
            echo json_encode($result);
            return;
        }
        $CarrierNamecode = $send_data['Master']['CarrierName'];
        $ConsignmentType = $send_data['Master']['ConsignmentType'];
        //E - Export I - Import B - In-Bond / IT  required Bonded Freight Forwarder T - Tranship / IE S - In-Transit / TandE Transportation and Exportation F - Freight Remain on Board AMS : 只有I , F
        $FilingType = $send_data['Master']['FilingType'];
        $FilingType_array = array('I', 'B', 'F', 'S');
        if (!in_array($FilingType, $FilingType_array)) {
            $result['msg'] = lang('暂不支持该FilingType');
            echo json_encode($result);
            return;
        }
        $container_size_codes = $this->container_size_codes;

        $Unit_codes = $this->unit_codes;

        //航次填写编号,如果没有填写编号,且大于5位,不给提交
        $voyage = $send_data['Master']['RouteInformation']['ArrivalVessel']['voyage'];
        if (empty($voyage)) {
            $result['msg'] = lang('请填写航次');
            echo json_encode($result);
            return;
        }
        if (strlen($voyage) > 5) {
            $result['msg'] = lang('AMS 航次不能大于5位,请填写额外参数 voyage_number');
            echo json_encode($result);
            return;
        }
        $vessel = $send_data['Master']['RouteInformation']['ArrivalVessel']['Value'];

        //IMO必须是7位整数
        $imo_number = $send_data['Master']['RouteInformation']['ArrivalVessel']['IMO'];

        if (!is_numeric($imo_number) || strlen($imo_number) != 7) {
            $result['msg'] = lang('IMO 必须是7位整数');
            echo json_encode($result);
            return;
        }

        $trans_discharge = $send_data['Master']['RouteInformation']['DischargePort']['code'];
        $trans_discharge_name = $send_data['Master']['RouteInformation']['DischargePort']['Value'];
        $trans_discharge_date = $send_data['Master']['RouteInformation']['DischargePort']['date'];

        $trans_origin = $send_data['Master']['RouteInformation']['LoadingPort']['code'];
        $trans_origin_name = $send_data['Master']['RouteInformation']['LoadingPort']['Value'];
        $trans_origin_date = $send_data['Master']['RouteInformation']['LoadingPort']['date'];
        if (strlen($trans_discharge) != 5) {
            $result['msg'] = lang('DischargePort Code必须是5位');
            echo json_encode($result);
            return;
        }
        if (strlen($trans_origin) != 5) {
            $result['msg'] = lang('LoadingPort Code必须是5位');
            echo json_encode($result);
            return;
        }

        // if(substr($trans_destination, 0, 2) !== 'US'){//目的港必须是US开头
        //     $result['msg'] = '目的港必须是US开头';
        //     echo json_encode($result);
        //     return;
        // }

        $time = time();

        //开始填入数据
        $MessageID = date('YmdHi', $time) . $message_row['id'];//唯一标识XML值 TODO
        $SenderCode = 'CUZHUN';
        $ManifestType = 'ACI';
        $CompanyCode = 'LEAGUE';
        $BranchCode = 'SHA';
        $SendDateTime = date('YmdHis', $time);
        $Version = '3.0';
        $MasterBillNo = $send_data['Master']['MasterBillNo'];
        //originAgentCode 根据当前发送人判定，查询公司所属的code
        $OriginAgentCode = $send_data['Master']['OriginAgentCode'];

        if(!isset($this->OriginAgentCodes[get_system_type()])){
            $result['msg'] = lang('orginagent_code 未配置，请联系管理员');
            echo json_encode($result);
            return;
        }

        $DestinationAgentCode = $send_data['Master']['DestinationAgentCode'];
        $MasterRefNo = $send_data['Master']['MasterRefNo'];
        $ConsolidatorRefCode = $send_data['Master']['ConsolidatorRefCode'];
        $ACISubMasterBillNo_code = $send_data['Master']['ACISubMasterBillNo']['code'];
        $ACISubMasterBillNo_Value = $send_data['Master']['ACISubMasterBillNo']['Value'];
        // if(in_array('true', array_column($send_data['Master']['Houses'], 'ColoadInSelfFilingIndicator'))){
        //     if($ACISubMasterBillNo_code == '' || $ACISubMasterBillNo_Value == ''){
        //         $result['msg'] = "ColoadInSelfFilingIndicator 为FALSE时 ACISubMasterBillNo 必填";
        //         echo json_encode($result);
        //         return;
        //     }
        // };
        $ShippingParty_Code = array();

        $message = "";
        $message .= "<Manifest>";

        $message .= "<Header>";
        $message .= "<MessageID>$MessageID</MessageID>";
        $message .= "<ActionCode>$file_function</ActionCode>";//A -  新增不发生 AS- 新增并发生 R– 修改 D- 删除
        $message .= "<SenderCode>$SenderCode</SenderCode>";//发送方软件供应商代码，需要EFT设置 TODO
        $message .= "<ManifestType>$ManifestType</ManifestType>";//ACI、AMS、AFR、ISF 目前固定AMS
        $message .= "<CompanyCode>$CompanyCode</CompanyCode>";//公司代码 TODO
        $message .= "<SubmitterID>$SubmitterID</SubmitterID>";//Emai地址 以FTP上传Xml时，若Xml文件解析，数据校验发生错误，将通过Email通知错误信息。Email地址以分号(;)分隔
        $message .= "<BranchCode>$BranchCode</BranchCode>";//分公司代码 TODO
        $message .= "<SendDateTime>$SendDateTime</SendDateTime>";//Format:yyyyMMddHHmmss 发送时间
        $message .= "<Version>$Version</Version>";//版本
        $message .= "</Header>";

        $message .= "<Master>";
//        $message .= "<FilerCode></FilerCode>";//发送代码 不必填

        $message .= "<MasterBillNo>$MasterBillNo</MasterBillNo>";//主单号,提单号
        if($ACISubMasterBillNo_code !== '' && $ACISubMasterBillNo_Value !== '') $message .= "<ACISubMasterBillNo code=\"$ACISubMasterBillNo_code\">$ACISubMasterBillNo_Value</ACISubMasterBillNo>";//同行Coload Out必填
        $message .= "<OriginAgentCode>$OriginAgentCode</OriginAgentCode>";//出口方代理 需要EFT设置 TODO
        $message .= "<DestinationAgentCode>$DestinationAgentCode</DestinationAgentCode>";//进口方代理	需要EFT设置 TODO
        $message .= "<CarrierName code=\"$CarrierNamecode\"></CarrierName>";//船公司简码	船公司4码
        $message .= "<ConsignmentType>$ConsignmentType</ConsignmentType>";//业务模式 LCL
        $message .= "<FilingType>$FilingType</FilingType>";//
        $message .= "<ConsolidatorRefCode>$ConsolidatorRefCode</ConsolidatorRefCode>";//
        $ShippingParty_Code[] = $ConsolidatorRefCode;

        $ACIDischargePort_code = $send_data['Master']['RouteInformation']['ACIDischargePort']['code'];
        $ACIDischargePort_subcode = $send_data['Master']['RouteInformation']['ACIDischargePort']['subcode'];
        $ACIDischargePort_Value = $send_data['Master']['RouteInformation']['ACIDischargePort']['Value'];

        $ACIDestinationPort_code = $send_data['Master']['RouteInformation']['ACIDestinationPort']['code'];
        $ACIDestinationPort_subcode = $send_data['Master']['RouteInformation']['ACIDestinationPort']['subcode'];
        $ACIDestinationPort_Value = $send_data['Master']['RouteInformation']['ACIDestinationPort']['Value'];

        $message .= "<RouteInformation>";//船期信息列表--start
        $message .= "<ArrivalVessel IMO=\"{$imo_number}\" voyage=\"$voyage\">$vessel</ArrivalVessel>";//
        $message .= "<LoadingPort code=\"$trans_origin\" date=\"$trans_origin_date\">$trans_origin_name</LoadingPort>";//
        $message .= "<DischargePort code=\"$trans_discharge\" date=\"$trans_discharge_date\">$trans_discharge_name</DischargePort>";//
        $message .= "<ACIDischargePort code=\"$ACIDischargePort_code\" subcode=\"$ACIDischargePort_subcode\">$ACIDischargePort_Value</ACIDischargePort>";//
        $message .= "<ACIDestinationPort code=\"$ACIDestinationPort_code\" subcode=\"$ACIDestinationPort_subcode\">$ACIDestinationPort_Value</ACIDestinationPort>";//
        $message .= "</RouteInformation>";//船期信息列表--end

        $message .= "<MasterRefNo>$MasterRefNo</MasterRefNo>";//主票参考号 TODO

        $message .= "<MasterContainers>";//集装箱信息列表--start

        $containers = $send_data['Master']['MasterContainers'];
        foreach ($containers as $container) {
            if (isset($container_size_codes[$container['ContainerType']])) {
                $ContainerType = $container_size_codes[$container['ContainerType']];
            } else {
                $result['msg'] = lang('存在未配置的箱型{ContainerType}', array('ContainerType' => $container['ContainerType']));
                echo json_encode($result);
                return;
            }

            $message .= "<Container>";//箱信息--start
            $message .= "<ContainerNo>{$container['ContainerNo']}</ContainerNo>";//箱号
            $message .= "<ContainerType>$ContainerType</ContainerType>";//箱型,需要转换下
            $message .= "<SealNo>{$container['SealNo']}</SealNo>";//箱封号
//            $message .= "<ServiceCode></ServiceCode>";//服务代码	CY: Container Yard CS:Container Station DD:Door-to-Door
//            $message .= "<ShipperOwned></ShipperOwned>";//
            $message .= "</Container>";//箱信息--end
        }
        $message .= "</MasterContainers>";//集装箱信息列表--end

        $message .= "<Houses>";
        $shipments = $send_data['Master']['Houses'];
        foreach ($shipments as $key => $shipment) {
            $HouseNo = $shipment['HouseNo'];
            $ShipperRefCode = $shipment['ShipperRefCode'];
            $ConsigneeRefCode = $shipment['ConsigneeRefCode'];
            $NotifyRefCode = $shipment['NotifyRefCode'];
            $ShippingParty_Code[] = $ShipperRefCode;
            $ShippingParty_Code[] = $ConsigneeRefCode;
            $ShippingParty_Code[] = $NotifyRefCode;

            if (strlen($HouseNo) <= 12) {
            } else if (strlen($HouseNo) == 13) {
                $HouseNo = substr($HouseNo, 0, 1) . substr($HouseNo, 2);//$HouseNo 最多12位 这里将前面的JH KM等修改未J K
            } else {
                $result['msg'] = lang('{HouseNo}  当前HouseNo 编号格式暂不支持');
                echo json_encode($result);
                return;
            }

            $message .= "<House>";
//            $message .= "<LastHBLIndicator></LastHBLIndicator>";//bool 非必填
            $message .= "<HouseNo>{$HouseNo}</HouseNo>";//分单号
//            $message .= "<OriginalHouseNo>SCAC{$shipment['cus_no']}</OriginalHouseNo>";//拆单使用，提供原始Housenumber
            $message .= "<ShipperRefCode>{$ShipperRefCode}</ShipperRefCode>";//发货人参考号
            $message .= "<ConsigneeRefCode>{$ConsigneeRefCode}</ConsigneeRefCode>";//收货人参考号
            $message .= "<NotifyRefCode>{$NotifyRefCode}</NotifyRefCode>";//通知人参考号

            $trans_receipt = $shipment['PlaceReceipt']['code'];
            $trans_receipt_name = $shipment['PlaceReceipt']['Value'];
            $trans_receipt_date = $shipment['PlaceReceipt']['date'];
            if (strlen($trans_receipt) != 5) {
                $result['msg'] = lang('{HouseNo}  PlaceReceipt Code必须是5位');
                echo json_encode($result);
                return;
            }
            $trans_destination = $shipment['PlaceDelivery']['code'];
            $trans_destination_name = $shipment['PlaceDelivery']['Value'];
            $trans_destination_date = $shipment['PlaceDelivery']['date'];
            if (strlen($trans_destination) != 5) {
                $result['msg'] = lang('{HouseNo}  PlaceDelivery Code必须是5位');
                echo json_encode($result);
                return;
            }

//            $message .= "<SecondNotifyParty></SecondNotifyParty>";//
            $message .= "<PlaceReceipt code=\"$trans_receipt\" date=\"$trans_receipt_date\">$trans_receipt_name</PlaceReceipt>";//通知人参考号
            $message .= "<PlaceDelivery code=\"$trans_destination\" date=\"$trans_destination_date\">$trans_destination_name</PlaceDelivery>";//通知人参考号

            $message .= "<HouseContainers>";
            $shipment_containers = $shipment['HouseContainers'];
            foreach ($shipment_containers as $shipment_container) {
                if (isset($Unit_codes[$shipment_container['Unit']])) {
                    $Unit_code = $Unit_codes[$shipment_container['Unit']];
                } else {
                    $result['msg'] = lang('HouseNo:{HouseNo} 包装类型代码无对应值', array('HouseNo' => $HouseNo));
                    echo json_encode($result);
                    return;
                }
                $Unit_value = $shipment_container['Unit'];
                $CountryOrigin_code = $shipment_container['CountryOrigin']['code'];
                $CountryOrigin_value = $shipment_container['CountryOrigin']['Value'];

                $message .= "<Container>";
                $message .= "<ContainerRefNo>{$shipment_container['ContainerRefNo']}</ContainerRefNo>";//箱号
                $message .= "<Package>{$shipment_container['Package']}</Package>";//件数
                $message .= "<Unit code=\"$Unit_code\">$Unit_value</Unit>";//包装类型代码
                $message .= "<CountryOrigin code=\"$CountryOrigin_code\">$CountryOrigin_value</CountryOrigin>";//原产国代码
                $message .= "<GrossWeight>{$shipment_container['GrossWeight']}</GrossWeight>";//重量（KG）
                $message .= "<CBM>{$shipment_container['CBM']}</CBM>";//体积（CBM）
                $message .= "<Marking>{$shipment_container['Marking']}</Marking>";//唛头
                $message .= "<Description>{$shipment_container['Description']}</Description>";//货物描述
                $message .= "</Container>";
            }

            $message .= "</HouseContainers>";

            $message .= "<ColoadInSelfFilingIndicator>{$shipment['ColoadInSelfFilingIndicator']}</ColoadInSelfFilingIndicator>";
            if($shipment['ColoadInSelfFilingIndicator'] == 'true'){
                $message .= "<ContainerLoadingPlaces>";
                $message .= "<ContainerLoadingPlaceRefCode>{$shipment['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']}</ContainerLoadingPlaceRefCode>";
                $message .= "</ContainerLoadingPlaces>";
            }
            $ShippingParty_Code[] = $shipment['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode'];
            $message .= "</House>";
        }
        $message .= "</Houses>";

        $message .= "</Master>";

        $message .= "<ShippingParties>";
        $ShippingParties = $send_data['ShippingParties'];
        foreach ($ShippingParties as $ShippingParty) {
            if(!in_array($ShippingParty['PartyCode'], $ShippingParty_Code)){
                continue;
            }
            $PartyCode = $ShippingParty['PartyCode'];
            $PartyType = $ShippingParty['PartyType'];
            $PartyName = $ShippingParty['PartyName'];
            if(empty($PartyName)) continue;
            $Address = $ShippingParty['PartyLocation']['Address'];
            $City = $ShippingParty['PartyLocation']['City'];
            $SubDivisionCode = $ShippingParty['PartyLocation']['SubDivisionCode'];
//            $Country = $ShippingParty['PartyLocation']['Country'];
            //新增
            $Country = $ShippingParty['PartyLocation']['Country']['code'];
            $PostalCode = $ShippingParty['PartyLocation']['PostalCode'];
            //如果是美国和加拿大的话，地区代码必填
            if($Country == 'US' || $Country == 'CA'){
                if($SubDivisionCode == ''){
                    $result['msg'] = lang('PartyCode:{PartyCode} 美国或加拿大的州代码必填', array('PartyCode' => $PartyCode));
                    echo json_encode($result);
                    return;
                }
            }

            //SHIPPER
            $message .= "<ShippingParty>";

            $message .= "<PartyCode>$PartyCode</PartyCode>";
            $message .= "<PartyType>$PartyType</PartyType>";//默认给CU 往来单位类型
            $message .= "<PartyName>$PartyName</PartyName>";

            $message .= "<PartyLocation>";

            $message .= "<Address>";
            $message .= "<AddressLine>" . substr($Address, 0, 50) . "</AddressLine>";
            if (strlen($Address) > 50) {
                $message .= "<AddressLine>" . substr($Address, 50, 50) . "</AddressLine>";
            }
            $message .= "</Address>";
            $message .= "<City>$City</City>";//城市必填
            if($SubDivisionCode !== '')$message .= "<SubDivisionCode>$SubDivisionCode</SubDivisionCode>";

            $message .= "<Country code=\"$Country\"></Country>";
            $message .= "<PostalCode>$PostalCode</PostalCode>";//收货人，最好给一下postal code邮编

            $message .= "</PartyLocation>";
            $message .= "</ShippingParty>";
        }
        $message .= "</ShippingParties>";
        $message .= "</Manifest>";

        $file_content = '<?xml version="1.0" encoding="UTF-8"?>' . $message;

        //保存文件xml
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        $new_dir = $root_path . '/upload/message/EFREIGHT_EDI/' . $type . '/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $message_row['id_type'] . '.xml';
        $file_path = $new_dir . $file_name;
        file_put_contents($file_path, $file_content);
        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => $type,
            'receiver' => 'EFREIGHT',
        );
        update_message($consol_id, $message_data, $type);
        echo json_encode(array('code' => 0, 'msg' => lang('文件生成成功')));
    }
}