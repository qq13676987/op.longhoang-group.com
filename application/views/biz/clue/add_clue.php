<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="/inc/third/layui/css/layui.css">
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<title>Client_apply-EDIT</title>
<script type="text/javascript">
    function checkvalue() {
        var vv = $('#country_code').textbox('getValue');
        if (vv == "") {
            layer.alert("国家代码必填！", {icon:5});
            return false;
        }

        var vv = $('#company_name').textbox('getValue');
        if (vv == "") {
            layer.alert("客户全称必填！", {icon:5});
            return false;
        }

        var vv = $('#client_source').combobox('getValues');
        if (vv == '') {
            layer.alert("客户来源必填！", {icon:5});
            return false;
        }

        var vv = $('#booking_ETD').textbox('getValue');
        if (vv == "") {
            layer.alert("bookingETD必填！", {icon:5});
            return false;
        }
        return true;
    }

    var default_partner_request = [];

    //查找相似度
    function search_similar() {
        var company_name = $('#company_name').textbox('getValue');
        if (company_name === '') {
            $.messager.alert('Tips', '先填写全称');
            return;
        }
        ajaxLoading();
        is_search = true;
        $.ajax({
            type: 'GET',
            url: '/biz_client/search?company_name=' + company_name,
            success: function (res_json) {
                ajaxLoadEnd();
                var res;
                try {
                    res = $.parseJSON(res_json);
                } catch (e) {

                }
                if (res == undefined) {
                    $.messager.alert('Tips', '发生错误,请联系管理员');
                } else {
                    if (res.code == 0) {
                        var status = res.data.client_level==0?'停用':'正常';
                        var msg = res.msg + '<br />最接近的为“' + res.data.company_name + '”,相似度为' + res.data.percent + '%，状态：'+status;
                        layer.alert(msg);
                    } else {
                        layer.alert('Tips', res.msg);
                    }

                }
            },
        });
    }

    $(function () {
        //实现表格下拉框的分页和筛选--start
        var html = '<div id="qcc_searchTb">';
        html += '<form id="qcc_searchform">';
        html += '<div style="padding-left: 5px;display: inline-block;"><label><?= lang('公司名称');?>:</label><input name="keyword" id="qcc_keyword" class="easyui-textbox to_search"style="width:200px;"data-options="prompt:\'text\'"/></div>';

        html += '<div style="padding-left: 1px; display: inline-block;"><form>';
        html += '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="qcc_query"><?= lang('search');?></a>';
        html += '</div>';
        html += '</div>';
        $('body').append(html);
        $.parser.parse('#qcc_searchTb');

        $('#qcc_search').combogrid("reset");
        $('#qcc_search').combogrid({
            panelWidth: '700px',
            panelHeight: '300px',
            prompt: '<?= lang('企查查更精确');?>',
            // multiple:true,
            idField: 'name',              //ID字段
            textField: 'name',    //显示的字段
            fitColumns: true,
            striped: true,
            editable: false,
            pagination: true,           //是否分页
            pageList: [20, 50, 100],
            pageSize: 20,
            toolbar: '#qcc_searchTb',
            rownumbers: true,           //序号
            collapsible: true,         //是否可折叠的
            method: 'get',
            columns: [[
                {field: 'Name', title: '<?= lang('Name');?>', width: 250},
                {field: 'CreditCode', title: '<?= lang('CreditCode');?>', width: 150},
                //{field: 'OperName', title: '<?= lang('OperName');?>', width: 150},
                //{field: 'No', title: '<?= lang('No');?>', width: 150},
            ]],
            emptyMsg: '未找到相应数据!',
            onBeforeLoad: function (param) {
                if (Object.keys(param).length < 3) {
                    return false;
                }
            },
            onSelect: function (index, row) {
                var load = layer.load(1);
                is_qcc_search = true;
                $.ajax({
                    type: 'post',
                    url: '/biz_client/get_last_shipment/',
                    data: {company_name:row.Name},
                    dataType: 'json',
                    async: true,
                    success:function (data) {
                        layer.close(load);
                        if (data.code == 0){
                            var msg = row.Name + data.msg + '记录，当前处于保护状态，不能使用该抬头！';
                            layer.alert(msg, {icon:5});
                        }
                        var this_val = row.Name;
                        //选中时给下面的赋值
                        var input = $('#company_name');
                        input.textbox('setValue', this_val);
                        //选中时给下面的赋值
                        var input = $('#taxpayer_id');
                        // var this_val = row.No;
                        var this_val = row.CreditCode;
                        input.textbox('setValue', this_val);
                        //选中时给下面的赋值
                        var input = $('#taxpayer_name');
                        input.textbox('setValue', this_val);
                        //自动选择中国
                        var input = $('#country_code');
                        input.combobox('setValue', 'CN');
                    }
                });
            },
            onLoadSuccess: function (data) {
                var opts = $(this).combogrid('grid').datagrid('options');
                var vc = $(this).combogrid('grid').datagrid('getPanel').children('div.datagrid-view');
                vc.children('div.datagrid-empty').remove();
                if (!$(this).combogrid('grid').datagrid('getRows').length) {
                    var d = $('<div class="datagrid-empty"></div>').html(opts.emptyMsg || 'no records').appendTo(vc);
                    d.css({
                        position: 'absolute',
                        left: 0,
                        top: 50,
                        width: '100%',
                        fontSize: '14px',
                        textAlign: 'center'
                    });
                }
            },
            onShowPanel: function () {
                //展开面板时
                var company_name = $('#company_name').textbox('getValue');
                var form_data = {
                    'keyword': company_name,
                };
                $('#qcc_searchform').form('load', form_data);
                $('#qcc_query').trigger('click');
            },
            url: '/api_qcc/GetList_1027',
        });
        //点击搜索
        $('#qcc_query').click(function () {
            var where = {};
            var form_data = $('#qcc_searchform').serializeArray();
            $.each(form_data, function (index, item) {
                if (where.hasOwnProperty(item.name) === true) {
                    if (typeof where[item.name] == 'string') {
                        where[item.name] = where[item.name].split(',');
                        where[item.name].push(item.value);
                    } else {
                        where[item.name].push(item.value);
                    }
                } else {
                    where[item.name] = item.value;
                }
            });
            $('#qcc_search').combogrid('grid').datagrid('load', where);
        });
        //text添加输入值改变
        $('.to_search').textbox('textbox').keydown(function (e) {
            if (e.keyCode == 13) {
                $('#qcc_query').trigger('click');
            }
        });

        //异步写入数据
        $('#save_data').click(function () {
            if(checkvalue() == true) {
                var load = layer.load(1);
                var field = {};
                $.each($('#myform').serializeArray(), function (index, item) {
                    field[item.name] = item.value;
                });

                $.ajax({
                    url: '/biz_clue/add_clue/',
                    type: 'POST',
                    data: field,
                    dataType: 'json',
                    success: function (data) {
                        layer.close(load);
                        if (data.code == 1) {
                            //刷新父页面的列表控件
                            parent.$('#tt').edatagrid('reload');
                            layer.alert('添加成功。', {icon:1}, function () {
                                var index = parent.layer.getFrameIndex(window.name);
                                parent.layer.close(index); //再执行关闭
                            });
                        } else {
                            layer.alert(data.msg, {icon:5});
                        }
                    },
                    error: function (xhr, status, error) {
                        layer.close(load);
                        layer.alert('网络请求失败！', {icon:5});
                    }
                });
            }
        });

        $('#company_name').textbox({
            onChange:function (newValue, oldValue) {
                if (newValue != '' && newValue != oldValue)
                {
                    var load = layer.load(1);
                    //显示分析结果
                    $('#search_client').attr('src', '/biz_crm/clue_analysis/?company_name=' + newValue);
                    //提示
                    $.ajax({
                        type: 'post',
                        url: '/biz_client/get_last_shipment/',
                        data: {company_name:newValue},
                        dataType: 'json',
                        async: true,
                        success:function (data) {
                            layer.close(load);
                            if (data.code == 0){
                                var msg = '“'+ newValue + '”' + data.msg + '记录，不能作为线索使用！';
                                layer.alert(msg, {icon:5});
                                $('#company_name').textbox('setValue', '');
                            }
                        }
                    });
                }
            }
        })

        var load = layer.load(1, {offset: ['200px', '850px']});
        $('#search_client').on('load',function () {
            layer.close(load);
        })
    });
</script>
<style type="text/css">
    input:disabled, select:disabled, textarea:disabled {
        background-color: #d6d6d6;
        cursor: default;
    }
    .form_table {float: left;width: 520px;}
    .form_table tr td:first-child {padding-left: 15px; color: #023e62}
</style>
<body>
<div id="tb" class="easyui-tabs" style="height: 600px;">
        <form id="myform" name="f" method="post" onsubmit="return false;">
            <!--基础信息-->
            <div style="padding: 20px 20px 20px 30px; background: #f6f8f5; height: 600px; width: 450px; float: left; border-right:1px solid #eee;">
                <div data-options="region:'west',tools:'#tt'" title="Basic">
                    <div style="display: inline-block;vertical-align: top">
                    <table class="form_table" style="padding:10px; width: 450px; overflow:hidden;" border="0" cellspacing="0">
                        <tr style="height: 35px;">
                            <td style="width: 90px;"><?= lang('国家代码'); ?></td>
                            <td>
                                <select class="easyui-combobox" required name="country_code"
                                        id="country_code" style="width: 300px; height: 30px;" data-options="
                                value:'',
                                valueField:'value',
                                textField:'namevalue',
                                url:'/bsc_dict/get_option/country_code',
                                onHidePanel: function() {
                                    var valueField = $(this).combobox('options').valueField;
                                    var val = $(this).combobox('getValue');
                                    var allData = $(this).combobox('getData');
                                    var result = true;
                                    for (var i = 0; i < allData.length; i++) {
                                        if (val == allData[i][valueField]) {
                                            result = false;
                                        }
                                    }
                                    if (result) {
                                        $(this).combobox('clear');
                                    }
                                }
                            ">
                                </select>
                            </td>
                        </tr>
                        <tr style="height: 35px;">
                            <td><?= lang('客户全称'); ?> </td>
                            <td>
                                <input class="easyui-textbox" required  name="company_name" id="company_name"
                                       style="width:300px; height: 30px;" value=""/>
                            </td>
                        </tr>
                        <tr style="height: 35px;">
                            <td><?= lang('客户来源'); ?></td>
                            <td>
                                <select class="easyui-combobox" id="client_source" name="client_source" style="width:300px; height: 30px;" data-options="
                                    required:true,
                                    editable:false,
                                    multivalue:false,
                                    valueField:'value',
                                    textField:'name',
                                    value: '',
                                    data:[{name:'提单', value:'提单'}]
                                ">
                                </select>
                            </td>
                        </tr>
                        <tr style="height: 35px;">
                            <td><?= lang('bookingETD'); ?> </td>
                            <td>
                                <input class="easyui-textbox" required name="etd" id="booking_ETD" style="width:300px; height: 30px;" value=""/>
                            </td>
                        </tr>
                        <tr style="height: 35px;">
                            <td><?= lang('船名'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="vessel" id="vessel"
                                       style="width:300px; height: 30px;" value=""/>
                            </td>
                        </tr>
                        <tr style="height: 35px;" id="company_name_en_tr">
                            <td><?= lang('起运港'); ?> </td>
                        <td>
                            <input class="easyui-textbox" name="pol" id="POL"
                                   style="width:300px;height: 30px;" value=""/>
                        </td>
                        </tr>
                        <tr style="height: 35px;">
                            <td><?= lang('目的港'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="pod" id="POD"
                                       data-options="" style="width:300px; height: 30px;" value=""/>
                            </td>
                        </tr>
                        <tr style="height: 35px;">
                            <td><?= lang('航线'); ?></td>
                            <td>
                                <select class="easyui-combobox" id="export_sailing" name="export_sailing" style="width:300px; height: 30px;"
                                        data-options="
                                    editable:false,
                                    multivalue:false,
                                    valueField:'value',
                                    textField:'name',
                                    value:'',
                                    url:'/bsc_dict/get_option/export_sailing'
                                ">
                                </select>
                            </td>
                        </tr>
                        <tr style="height: 35px;">
                            <td><?= lang('运输模式'); ?></td>
                            <td>
                                <select class="easyui-combobox" id="trans_mode" name="trans_mode" style="width:300px; height: 30px;" data-options="
                                    editable:false,
                                    multivalue:false,
                                    valueField:'value',
                                    textField:'name',
                                    value:'',
                                    url:'/bsc_dict/get_option/client_trans_mode',
                                    onChange:function(newValue,oldValue){
                                        $('#trans_mode').val(newValue.join(','));
                                    }
                                ">
                                </select>
                            </td>
                        </tr>
                        <tr style="height: 35px;">
                            <td><?= lang('产品类型'); ?></td>
                            <td>
                                <select class="easyui-combobox" id="product_type" name="product_type" style="width:300px; height: 30px;" data-options="
                                    editable:false,
                                    multivalue:false,
                                    valueField:'value',
                                    textField:'name',
                                    value:'',
                                    url:'/bsc_dict/get_option/product_type',
                                    onChange:function(newValue,oldValue){
                                        $('#product_type').val(newValue.join(','));
                                    }
                                ">
                                </select>
                            </td>
                        </tr>
                        <tr style="height: 35px;">
                            <td><?= lang('产品描述'); ?></td>
                            <td>
                                <textarea style="width:295px; height: 80px;" class="textarea b" name="product_details" id="product_details"></textarea>
                            </td>
                        </tr>
                    </table>
                    </div>
                </div>
            </div>
            <!--查询往来单中同名客户-->
            <div style="padding: 10px 0px 10px 0px; width: 690px; float: right; overflow: hidden">
                <iframe src="/biz_crm/clue_analysis/?company_name=" id="search_client" style="width:690px; min-height: 800px; border:none;"></iframe>
            </div>
            <!--底部按钮-->
            <div id="save_tools" style="width: 100%; text-align: center; padding: 20px 0px; border-top:1px solid #588c98; position: fixed; z-index: 999; bottom: 0px; background-color:#f6f8f5;">

                <button class="layui-btn" id="save_data" style=" width: 300px; ">确认新增</button>
                </button>
            </div>
        </form>
</div>
</body>