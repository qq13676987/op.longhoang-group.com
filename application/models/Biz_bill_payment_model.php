<?php
   class Biz_bill_payment_model extends CI_Model{
       private $table = 'biz_bill_payment';
       

        public $field_all = array(
            array("id", "id", "60", "10"),//ID
            array("created_by", "created_by", "60", "10"),//创建人
            array("created_time", "created_time", "100", "10"),//创建日期
            array("updated_by", "updated_by", "60", "10"),//修改人
            array("updated_time", "updated_time", "100", "10"),//修改日期
            array("other_side_company", "other_side_company", "100", "10"),//收付款对方抬头
            array("payment_company", "payment_company", "80", "10"),//我司抬头
	    array("payment_bank", "payment_bank", "80", "10"),//结算银行
            array("currency", "currency", "60", "10"),//币种
            array("payment_amount", "payment_amount", "120", "10"),//核销金额
            array("payment_date", "payment_date", "100", "10"),//核销日期
            array("payment_way", "payment_way", "80", "10"),//结算方式
            array("payment_no", "payment_no", "100", "10"),//核销号
            array("payment_user_id", "payment_user_id", "60", "10"),//核销号
            array("lock", "lock", "0", "10"),//核销号
            array("lock_date", "lock_date", "100", "10"),//审核日期
            array("lock_user_id", "lock_user_id", "60", "10"),//审核人
            array("tax_doc_no", "tax_doc_no", "60", "10"),//审核人
            array("tax_doc_no_time", "tax_doc_no_time", "100", "10"),//审核日期
        );
    
        public function getFieldEdit(){
            $field_edit = array();
            foreach ($this->field_all as $row) {
                $v = $row[0];
                if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
                array_push($field_edit, $v);
            }
            return $field_edit;
        }

       public function __construct()
       {
           parent::__construct();
           $this->load->database();
       }

       public function get_one($f="",$v=""){
           $this->db->where($f, $v);
           $query = $this->db->get($this->table);
           return $query->row_array();
       }

       public function get_where_one($where = ""){
           if($where != '')$this->db->where($where);
           $query = $this->db->get($this->table);
           return $query->row_array();
       }

       public function get($where="" ,$sort="id",$order="desc"){
           if($where !="")	$this->db->where($where, null, false);
           $this->db->order_by($sort,$order);
           $query = $this->db->get($this->table);
           return $query->result_array();
       }

       public function no_role_get($where = "", $sort = "id", $order = "desc")
       {
           if ($where != "") $this->db->where($where);
           $this->db->order_by($sort, $order);
           $query = $this->db->get($this->table);
           return $query->result_array();
       }

       public function total($where=""){
           if($where !="")	$this->db->where($where);
           $this->db->from($this->table);
           return  $this->db->count_all_results('');
       }

       public function save($data=array()){
           $data["created_time"] = date('Y-m-d H:i:s', time());
           $data["updated_time"] = date('Y-m-d H:i:s', time());
           $data["created_by"] =  $this->session->userdata('id');
           $data["updated_by"] =  $this->session->userdata('id');
           $this->db->insert($this->table, $data);
           return $this->db->insert_id();
       }

       public function mbacth_update($data, $wfield = 'id')
       {
           $this->db->update_batch($this->table, $data, $wfield);
           return $this->db->affected_rows();
       }

       public function update($id='',$data=array()){
           $data["updated_time"] = date('Y-m-d H:i:s', time());
           $data["updated_by"] =  $this->session->userdata('id');
           $this->db->where('id', $id);
           $this->db->update($this->table, $data);
           return $id;
       }

       public function mdelete($id=''){
           // delete container
           $this->db->where('id', $id);
           $this->db->delete($this->table);
       }

   }