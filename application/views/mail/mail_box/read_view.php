<title><?= lang('查看已发送邮件');?></title>
<body>
<div style="width: 100%; background: rgba(216,225,255,0.77); border-bottom: 1px solid rgba(97,149,168,0.73);">
    <table style="padding: 10px 20px 0px;">
        <tr style="height: 40px;">
            <td colspan="2" style="font-weight: bolder; font-size: 16px;"><?php echo $data['mail_title']; ?></td>
        </tr>
    </table>
    <table style="padding: 0px 20px 20px;">
        <tr>
            <td><?= lang('收件人');?>：</td>
            <td>
                <a href="javascript:void(0)" id="addressee" style="font-size: 14px;font-weight: bolder"><?= lang('查看{count}个收件人', array('count' => $count));?></a>
            </td>
        </tr>
        <tr>
            <td><?= lang('发送时间');?>：</td>
            <td>
                <?php echo $data['plan_send_time']; ?>
            </td>
        </tr>
    </table>
</div>
<div style="width: 100%; height: auto;">
    <div style="width: 95%; height: auto; margin: 0px auto">
        <?php
        if($data['template_id']==48){
            echo lang('请联系客户查看邮件。');
        }else{
            echo $data['mail_content'];
        } ?>
    </div>
</div>

<!--弹窗-->
<div id="_window" style="display: none; overflow:hidden;">
    <iframe scrolling="auto" id='openIframe' frameborder="0"  src="/mail_box/addressee/?id=<?php echo $data["id"]; ?>" style="width:100%;height:100%; overflow: hidden"></iframe>
</div>
<script>
    $(function () {
        //编辑内容是否不存在
        $('#addressee').click(function () {
            <?php
            if ($count == 0){
            ?>
            parent.$.messager.alert('<?= lang('确认');?>', '<?= lang('暂无收件人！');?>');
            <?php }else{?>
            //打开收件人窗口
            $('#_window').window({
                title: '<?= lang('收件人列表');?>',
                minimizable: false,
                collapsible: false,
                maximizable: false,
                closable: true,
                border: 'thin',
                width: '800',
                height: '400',
                left:300,
                top:100,
                modal: true,
            });
            <?php }?>
        });

        $('#_window').window({
            title: '<?= lang('收件人列表');?>',
            minimizable: false,
            collapsible: false,
            maximizable: false,
            closable: true,
            border: 'thin',
            width: '800',
            height: '400',
            left:300,
            top:100,
            modal: true,
        });
    })
</script>
</body>
</html>