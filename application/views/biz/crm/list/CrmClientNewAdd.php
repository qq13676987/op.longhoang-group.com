<!doctype html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CRM客户新增详情</title>
<link href="/inc/css/normal.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/icon.css?v=3">
<script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.easyui.min.js?v=11"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.edatagrid.js?v=5"></script>
<script type="text/javascript" src="/inc/js/easyui/datagrid-detailview.js"></script>
<script type="text/javascript" src="/inc/js/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/inc/js/other.js?v=1"></script>
<script type="text/javascript" src="/inc/js/set_search.js?v=5"></script>
<style type="text/css">
    .f{
        width:115px;
    }
    .s{
        width:100px;
    }
    .v{
        width:200px;
    }
    .del_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }

</style>
<script type="text/javascript">
    function doSearch() {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] = [json[item.name]];
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        var opt = $('#tt').datagrid('options');
        opt.url = '/biz_crm/getCrmNewAdd';

        $('#tt').datagrid('load', json);
        set_config();
        $('#chaxun').window('close');
    }
    var ajax_data = {};
    var setting = {
        'operator_ids':['operator', 'id', 'name']
        ,'customer_service_ids':['customer_service', 'id', 'name']
        ,'sales_ids':['sales', 'id', 'name']
        ,'marketing_ids':['marketing', 'id', 'name']
        ,'oversea_cus_ids':['oversea_cus', 'id', 'name']
        ,'booking_ids':['booking', 'id', 'name']
        ,'document_ids':['document', 'id', 'name']
        ,'finance_ids':['finance', 'id', 'name']
    };

    var datebox = ['created_time', 'updated_time'];

    var datetimebox = [];
    $(function () {
        var selectIndex = -1;
        $('#tt').edatagrid({
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            }
        });

        $('#cx').on('keydown', 'input', function (e) {
            if(e.keyCode == 13){
                doSearch();
            }
        });

        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
            //添加视图,展开后显示consol
            view: detailview,
            detailFormatter:function(index,row){
                return '<div style="padding:2px"><table class="ddv"></table></div>';
            },
            onExpandRow: function(index,row){
                //<th data-options="field:'sales_name',width:100,">销售</th>
                //        <th data-options="field:'add_crm_count',width:1100,">新增CRM客户数</th>
                var ddv = $('#tt').datagrid('getRowDetail',index).find('table.ddv');

                ddv.datagrid({
                    loadMsg:'',
                    row_id:row.id,
                    height:'auto',
                    remoteSort: false,
                    data: row.users,
                    columns:[[
                        {field:'sales_name', width:100, title:'销售'},
                        {field:'add_crm_count', width:1100, title:'新增CRM客户数'},
                    ]],
                    onResize:function(){
                        $('#tt').datagrid('fixDetailRowHeight',index);
                    },
                    onLoadSuccess:function(data2){
                        setTimeout(function(){
                            $('#tt').datagrid('fixDetailRowHeight',index);
                        },0);
                    },
                    view: detailview,
                    detailFormatter:function(index2,row2){
                        return '<div style="padding:2px"><table class="ddv2"></table></div>';
                    },
                    onExpandRow: function(index2,row2){
                        //展开子时,如果父选中了,那么自动触发选中
                        var ddv2 = ddv.datagrid('getRowDetail',index2).find('table.ddv2');
                        var json = {};
                        var cx_data = $('#cx').serializeArray();
                        $.each(cx_data, function (index, item) {
                            if(json.hasOwnProperty(item.name) === true){
                                if(typeof json[item.name] == 'string'){
                                    json[item.name] =  json[item.name].split(',');
                                    json[item.name].push(item.value);
                                }else{
                                    json[item.name].push(item.value);
                                }
                            }else{
                                json[item.name] = item.value;
                            }
                        });
                        var sales_id = row2.sales_id;

                        ddv2.datagrid({
                            url:'/biz_crm/getCrmNewAddBySales?sales_id=' + sales_id,
                            loadMsg:'',
                            row_id:row.id,
                            idField: 'id',
                            queryParams: json,
                            remoteSort: false,
                            columns:[[
                                {field:'client_name', width:100, title:'客户简称'},
                                {field:'province', width:100, title:'省'},
                                {field:'city', width:100, title:'市'},
                                {field:'company_address', width:120, title:'客户地址'},
                                {field:'contact_name', width:120, title:'客户联系人'},
                                {field:'contact_telephone', width:120, title:'手机号(网络账号)'},
                                {field:'contact_telephone2', width:120, title:'客户座机'},
                                {field:'contact_email', width:120, title:'客户邮箱'},
                            ]],
                            onResize:function(){
                                $('#tt').datagrid('fixDetailRowHeight',index);
                                $('#tt').datagrid('fixRowHeight',index);
                                ddv.datagrid('fixDetailRowHeight',index2);
                                ddv.datagrid('fixRowHeight',index2);
                            },
                            rowStyler:function(index,row){
                                var style = '';

                                return style;
                            },

                            onDblClickRow: function(rowIndex2, row2) {
                                url = '/biz_crm/edit/'+row2.id;
                                window.open(url);
                            },
                            onClickCell: function(index2,field2,value2){
                                copy(value, false);
                            },
                            onLoadSuccess:function(data2){
                                setTimeout(function(){
                                    $('#tt').datagrid('fixDetailRowHeight',index);
                                    $('#tt').datagrid('fixRowHeight',index);
                                    ddv.datagrid('fixDetailRowHeight',index2);
                                    ddv.datagrid('fixRowHeight',index2);
                                },0);
                            }
                        });
                        $('#tt').datagrid('fixDetailRowHeight',index);
                        $('#tt').datagrid('fixRowHeight',index);
                        ddv.datagrid('fixDetailRowHeight',index2);
                        ddv.datagrid('fixRowHeight',index2);
                    }
                });
                $('#tt').datagrid('fixDetailRowHeight',index);

            }

        });
        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
        doSearch();
        //新增查询列
        $('#cx table').on('click' , '.add_tr', function () {
            var index = $('.f').length;
            var str = '<tr>' +
                '                <td>\n' +
                '                    查询\n'+
                '                </td>\n'+
                '                <td align="right">' +
                '                       <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="\n' +
                '                            onChange:function(newVal, oldVal){\n' +
                '                                var index = $(\'.f\').index(this);\n' +
                '                                if(setting.hasOwnProperty(newVal) === true){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                    $(\'.v:eq(\' + index + \')\').combobox({\n' +
                '                                        data:ajax_data[setting[newVal][0]],\n' +
                '                                        valueField:setting[newVal][1],\n' +
                '                                        textField:setting[newVal][2],\n' +
                '                                    });\n' +
                '                                }else if ($.inArray(newVal, datebox) !== -1){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datebox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }else if ($.inArray(newVal, datetimebox) !== -1){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datetimebox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }else{\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }\n' +
                '                            }\n' +
                '                        ">\n' +
                '<option value=\'id\'>id</option><option value=\'client_code\'>客户代码</option><option value=\'client_name\'>客户简称</option><option value=\'company_name\'>客户全称</option><option value=\'province\'>省</option><option value=\'city\'>市</option><option value=\'area\'>区</option><option value=\'sailing_area\'>航线区域</option><option value=\'company_address\'>客户地址</option><option value=\'contact_name\'>客户联系人</option><option value=\'contact_live_chat\'>客户QQ</option><option value=\'contact_telephone\'>手机号(网络账号)</option><option value=\'contact_telephone2\'>客户座机</option><option value=\'contact_email\'>客户邮箱</option><option value=\'contact_position\'>contact_position</option><option value=\'finance_name\'>财务联系人</option><option value=\'finance_live_chat\'>财务QQ</option><option value=\'finance_telephone\'>财务电话</option><option value=\'finance_email\'>财务邮箱</option><option value=\'finance_payment\'>付款方式</option><option value=\'finance_od_basis\'>超期依据</option><option value=\'finance_payment_month\'>finance_payment_month</option><option value=\'finance_payment_days\'>付款天数</option><option value=\'finance_payment_day_th\'>finance_payment_day_th</option><option value=\'credit_amount\'>授信额度</option><option value=\'finance_status\'>财务状态</option><option value=\'finance_remark\'>财务备注</option><option value=\'finance_sign_company_code\'>finance_sign_company_code</option><option value=\'bank_name_cny\'>银行(CNY)</option><option value=\'bank_account_cny\'>银行帐号(CNY)</option><option value=\'bank_address_cny\'>银行地址(CNY)</option><option value=\'bank_swift_code_cny\'>银行swift(CNY)</option><option value=\'bank_name_usd\'>银行(USD)</option><option value=\'bank_account_usd\'>银行帐号(USD)</option><option value=\'bank_address_usd\'>银行地址(USD)</option><option value=\'bank_swift_code_usd\'>银行swift(USD)</option><option value=\'taxpayer_name\'>纳税人名称</option><option value=\'taxpayer_id\'>纳税人识别号</option><option value=\'taxpayer_address\'>纳税人地址</option><option value=\'taxpayer_telephone\'>纳税人电话</option><option value=\'deliver_info\'>快递地址</option><option value=\'website_url\'>企业网址</option><option value=\'source_from\'>信息来源</option><option value=\'role\'>角色</option><option value=\'remark\'>备注</option><option value=\'sales_id\'>管理人</option><option value=\'read_user_group\'>read_user_group</option><option value=\'created_by\'>创建人</option><option value=\'created_time\'>创建时间</option><option value=\'updated_by\'>修改人</option><option value=\'updated_time\'>修改时间</option><option value=\'client_level\'>client_level</option><option value=\'tag\'>标签</option><option value=\'country_code\'>国家代码</option><option value=\'invoice_email\'>invoice_email</option><option value=\'stop_date\'>stop_date</option><option value=\'dn_accounts\'>dn_accounts</option><option value=\'operator_ids\'>operator_ids</option><option value=\'customer_service_ids\'>customer_service_ids</option><option value=\'sales_ids\'>sales_ids</option><option value=\'marketing_ids\'>marketing_ids</option><option value=\'oversea_cus_ids\'>oversea_cus_ids</option><option value=\'booking_ids\'>booking_ids</option><option value=\'document_ids\'>document_ids</option><option value=\'finance_ids\'>finance_ids</option>' +
                '                    </select>\n' +
                '                    &nbsp;\n' +
                '\n' +
                '                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="\n' +
                '                        ">\n' +
                '                        <option value="like">like</option>\n' +
                '                        <option value="=">=</option>\n' +
                '                        <option value=">=">>=</option>\n' +
                '                        <option value="<="><=</option>\n' +
                '                        <option value="!=">!=</option>\n' +
                '                    </select>\n' +
                '                    &nbsp;\n' +
                '                    <div class="this_v">\n' +
                '                        <input name="field[v][]" class="v easyui-textbox" >\n' +
                '                    </div>\n' +
                '                    <button class="del_tr" type="button">-</button>' +
                '   </td>' +
                '</tr>';
            $('#cx table tbody').append(str);
            $.parser.parse($('#cx table tbody tr:last-child'));
            return false;
        });
        //删除按钮
        $('#cx table').on('click' , '.del_tr', function () {
            var index = $(this).parents('tr').index();
            $(this).parents('tr').remove();
            return false;
        });
        var a1 = ajax_get('/bsc_user/get_data_role/operator', 'operator');
        var a2 = ajax_get('/bsc_user/get_data_role/customer_service', 'customer_service');
        var a3 = ajax_get('/bsc_user/get_data_role/sales', 'sales');
        var a4 = ajax_get('/bsc_user/get_data_role/marketing', 'marketing');
        var a5 = ajax_get('/bsc_user/get_data_role/oversea_cus', 'oversea_cus');
        var a6 = ajax_get('/bsc_user/get_data_role/booking', 'booking');
        var a7 = ajax_get('/bsc_user/get_data_role/document', 'document');
        var a8 = ajax_get('/bsc_user/get_data_role/finance', 'finance');
        $.ajax({
            type: 'GET',
            url: '/sys_config/get_config_search/crm_CrmClientNewAdd',
            dataType: 'json',
            success:function (res) {
                var config = res.data;
                if(config.length > 0){
                    var count = config[0];
                    $('.f:last').combobox('setValue', config[1][0][0]);
                    $('.s:last').combobox('setValue', config[1][0][1]);
                    for (var i = 1; i < count; i++){
                        $('.add_tr:eq(0)').trigger('click');
                        $('.f:last').combobox('setValue', config[1][i][0]);
                        $('.s:last').combobox('setValue', config[1][i][1]);
                    }
                }
            }
        });

        $.when(a1,a2,a3,a4,a5,a6,a7,a8).done(function () {
            $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                var ec_value = $(ec_item).combobox('getValue');
                $(ec_item).combobox('clear').combobox('select', ec_value);
            });
        });

    });

    var is_submit = false;

    function ajax_get(url,name) {
        return $.ajax({
            type:'POST',
            url:url,
            dataType:'json',
            success:function (res) {
                ajax_data[name] = res;
            },
            error:function () {
                // $.messager.alert('获取数据错误');
            }
        })
    }

    function set_config() {
        var f_input = $('.f');
        var count = f_input.length;
        var config = [];
        $.each(f_input, function (index, item) {
            var c_array = [];
            c_array.push($('.f:eq(' + index + ')').combobox('getValue'));
            c_array.push($('.s:eq(' + index + ')').combobox('getValue'));
            config.push(c_array);
        });
        // var config_json = JSON.stringify(config);
        $.ajax({
            type: 'POST',
            url: '/sys_config/save_config_search',
            data:{
                table: 'crm_CrmClientNewAdd',
                count: count,
                config: config,
            },
            dataType:'json',
            success:function (res) {

            },
        });
    }
</script>
<table id="tt" style="width:1100px;height:450px" rownumbers="false" idField="id" toolbar="#tb" singleSelect="true" nowrap="true" remoteSort="false">
<!--     pagination="true"  pagesize="30" -->
    <thead>
    <tr>
        <th data-options="field:'group_name',width:150,sortable:true">销售组</th>
        <th data-options="field:'company_name',width:100,sortable:true">公司名称</th>
        <th data-options="field:'add_crm_count',width:1100,sortable:true">新增CRM客户数</th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
                   onclick="add();">新增</a>-->
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');">查询</a>
            </td>
        </tr>
    </table>
</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <form id="cx">
        <input type="hidden" name="crm_user_type" value="<?= $crm_user_type;?>">
        <input type="hidden" name="crm_user" value="<?= $crm_user;?>">
        <input type="hidden" name="crm_date_first" value="<?= $crm_date_first;?>">
        <input type="hidden" name="crm_date_end" value="<?= $crm_date_end;?>">
        <table>
            <tr>
                <td>
                    角色                </td>
                <td align="right">
                    <select class="easyui-combobox" id="f_role" name="f_role" style="width: 475px"
                            data-options="
                        valueField:'value',
                        textField:'value',
                        url:'/bsc_dict/get_option/role',
                    ">

                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    查询                </td>
                <td align="right">
                    <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="
                            onChange:function(newVal, oldVal){
                                var index = $('.f').index(this);
                                if(setting.hasOwnProperty(newVal) === true){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                    $('.v:eq(' + index + ')').combobox({
                                        data:ajax_data[setting[newVal][0]],
                                        valueField:setting[newVal][1],
                                        textField:setting[newVal][2],
                                    });
                                }else if ($.inArray(newVal, datebox) !== -1){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datebox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }else if ($.inArray(newVal, datetimebox) !== -1){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datetimebox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }else{
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }
                            }
                        ">
                        <option value='id'>id</option><option value='client_code'>客户代码</option><option value='client_name'>客户简称</option><option value='company_name'>客户全称</option><option value='province'>省</option><option value='city'>市</option><option value='area'>区</option><option value='sailing_area'>航线区域</option><option value='company_address'>客户地址</option><option value='contact_name'>客户联系人</option><option value='contact_live_chat'>客户QQ</option><option value='contact_telephone'>手机号(网络账号)</option><option value='contact_telephone2'>客户座机</option><option value='contact_email'>客户邮箱</option><option value='contact_position'>contact_position</option><option value='finance_name'>财务联系人</option><option value='finance_live_chat'>财务QQ</option><option value='finance_telephone'>财务电话</option><option value='finance_email'>财务邮箱</option><option value='finance_payment'>付款方式</option><option value='finance_od_basis'>超期依据</option><option value='finance_payment_month'>finance_payment_month</option><option value='finance_payment_days'>付款天数</option><option value='finance_payment_day_th'>finance_payment_day_th</option><option value='credit_amount'>授信额度</option><option value='finance_status'>财务状态</option><option value='finance_remark'>财务备注</option><option value='finance_sign_company_code'>finance_sign_company_code</option><option value='bank_name_cny'>银行(CNY)</option><option value='bank_account_cny'>银行帐号(CNY)</option><option value='bank_address_cny'>银行地址(CNY)</option><option value='bank_swift_code_cny'>银行swift(CNY)</option><option value='bank_name_usd'>银行(USD)</option><option value='bank_account_usd'>银行帐号(USD)</option><option value='bank_address_usd'>银行地址(USD)</option><option value='bank_swift_code_usd'>银行swift(USD)</option><option value='taxpayer_name'>纳税人名称</option><option value='taxpayer_id'>纳税人识别号</option><option value='taxpayer_address'>纳税人地址</option><option value='taxpayer_telephone'>纳税人电话</option><option value='deliver_info'>快递地址</option><option value='website_url'>企业网址</option><option value='source_from'>信息来源</option><option value='role'>角色</option><option value='remark'>备注</option><option value='sales_id'>管理人</option><option value='read_user_group'>read_user_group</option><option value='created_by'>创建人</option><option value='created_time'>创建时间</option><option value='updated_by'>修改人</option><option value='updated_time'>修改时间</option><option value='client_level'>client_level</option><option value='tag'>标签</option><option value='country_code'>国家代码</option><option value='invoice_email'>invoice_email</option><option value='stop_date'>stop_date</option><option value='dn_accounts'>dn_accounts</option><option value='operator_ids'>operator_ids</option><option value='customer_service_ids'>customer_service_ids</option><option value='sales_ids'>sales_ids</option><option value='marketing_ids'>marketing_ids</option><option value='oversea_cus_ids'>oversea_cus_ids</option><option value='booking_ids'>booking_ids</option><option value='document_ids'>document_ids</option><option value='finance_ids'>finance_ids</option>                    </select>
                    &nbsp;

                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="
                        ">
                        <option value="like">like</option>
                        <option value="=">=</option>
                        <option value=">=">>=</option>
                        <option value="<="><=</option>
                        <option value="!=">!=</option>
                    </select>
                    &nbsp;
                    <div class="this_v">
                        <input name="field[v][]" class="v easyui-textbox" >
                    </div>
                    <button class="add_tr" type="button">+</button>
                </td>
            </tr>
        </table>
        <br><br>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="doSearch();">查询</button>
        <a href="javascript:void;" class="easyui-tooltip" data-options="
            content: $('<div></div>'),
            onShow: function(){
                $(this).tooltip('arrow').css('left', 20);
                $(this).tooltip('tip').css('left', $(this).offset().left);
            },
            onUpdate: function(cc){
                cc.panel({
                    width: 500,
                    height: 'auto',
                    border: false,
                    href: '/bsc_help_content/help/query_condition'
                });
            }
        ">help</a>
    </form>
</div>