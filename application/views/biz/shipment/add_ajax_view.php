<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>shipment-Add</title>

<script type="text/javascript">
    var is_special_char = false;

    function qzb(str){
        str = str.replace(new RegExp('·', "g"), '`');
        str = str.replace(new RegExp('！', "g"), '!');
        str = str.replace(new RegExp('￥', "g"), '$');
        str = str.replace(new RegExp('【', "g"), '[');
        str = str.replace(new RegExp('】', "g"), ']');
        str = str.replace(new RegExp('】', "g"), ']');
        str = str.replace(new RegExp('；', "g"), ';');
        str = str.replace(new RegExp('：', "g"), ':');
        str = str.replace(new RegExp('“', "g"), '"');
        str = str.replace(new RegExp('”', "g"), '"');
        str = str.replace(new RegExp('’', "g"), '\'');
        str = str.replace(new RegExp('‘', "g"), '\'');
        str = str.replace(new RegExp('、', "g"), '');
        str = str.replace(new RegExp('？', "g"), '?');
        str = str.replace(new RegExp('《', "g"), '<');
        str = str.replace(new RegExp('》', "g"), '>');
        str = str.replace(new RegExp('，', "g"), ',');
        str = str.replace(new RegExp('。', "g"), '.');
        str = str.replace(new RegExp(' ', "g"), ' ');
        str = str.replace(new RegExp('　', "g"), ' ');
        str = str.replace(new RegExp('（', "g"), '(');
        str = str.replace(new RegExp('）', "g"), ')');
        return str;
    }
    
    /**
     * 下拉选择表格展开时,自动让表单内的查询获取焦点
     */
    function combogrid_search_focus(form_id, focus_num = 0){
        // var form = $(form_id);
        setTimeout("easyui_focus($($('" + form_id + "').find('.keydown_search')[" + focus_num + "]));",200);
    }

    /**
     * easyui 获取焦点
     */
    function easyui_focus(jq){
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combogrid !== undefined){
            return $(jq).combogrid('textbox').focus();
        }else if(data.combobox !== undefined){
            return $(jq).combobox('textbox').focus();
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('textbox').focus();
        }else if(data.datebox !== undefined){
            return $(jq).datebox('textbox').focus();
        }else if(data.textbox !== undefined){
            return $(jq).textbox('textbox').focus();
        }
    }
    
    /**
     * 自动转大写的输入事件
     */
    function toUpperCaseChange(newValue, oldValue){
        $(this).textbox('setValue', newValue.toUpperCase());
    }

    function changeClient(data, is_load = false) {
        // reload_user_by_sales(data.client_code);
        // $.each($('.userList'), function (index,item) {
        //     var id = $(this).attr('id').replace('_id','');
        //     if(id !== 'finance'){
        //         var key = id + '_ids';
        //         if(data[key] === undefined || data[key] == '')data[key] = 0;
        //         $(this).combobox('reload', '/bsc_user/get_data_role/' + id + '?userIds=' + data[key]);
        //     }
        // });
        //2022-01-18 修改后自动清空下面联动的
        $('#sales_id').combobox('reload', '/bsc_user/get_client_user?client_code=' + data.client_code + '&role=sales&sales_id=');
        $('#client_contact').combobox('reload', '/biz_client_contact/get_option/' + data.client_code);
        //这里同时得重新加载二级客户的数据
        $('#client_code2').combogrid('grid').datagrid('reload', '/biz_client_second_relation/getSecondByClient?client_code=' + data.client_code);
        changeClient2(data, is_load);
        
        
        //2022-01-18 修改后自动清空下面联动的
        //清空操作
        if(!is_load){
            $('#sales_id').combobox('setValue', '0');
            $('#sales_group').val('');
            $('#operator_id').combobox('setValue', '0');
            $('#operator_group').val('');
            $('#customer_service_id').combobox('setValue', '0');
            $('#customer_service_group').val('');
            $('#client_contact').combobox('clear');
        }
    }
    
    /**
     * 修改二级客户
     */
    function changeClient2(data, is_load = false) {
        $('#shipper_company2').combogrid('grid').datagrid('reload', '/biz_company/get_option/shipper/'+data.client_code);
        $('#consignee_company2').combogrid('grid').datagrid('reload', '/biz_company/get_option/consignee/'+data.client_code);
        $('#notify_company2').combogrid('grid').datagrid('reload', '/biz_company/get_option/notify/'+data.client_code);

        //清空操作
        if(!is_load){
            $('#shipper_company2').combogrid('clear');
            $('#consignee_company2').combogrid('clear');
            $('#notify_company2').combogrid('clear');
            //相关的也得清除
            $('#shipper_address').val('');
            $('#shipper_contact').val('');
            $('#shipper_telephone').val('');
            $('#shipper_email').val('');

            $('#consignee_address').val('');
            $('#consignee_contact').val('');
            $('#consignee_telephone').val('');
            $('#consignee_email').val('');

            $('#notify_address').val('');
            $('#notify_contact').val('');
            $('#notify_telephone').val('');
            $('#notify_email').val('');
        }
    }
    
    //刷新user的url和数据
    function reload_user_by_sales(client_code, sales_id = 0) {
        $.each($('.userList'), function (index,item) {
            var id = $(this).attr('id').replace('_id','');
            if(id !== 'finance'){
                var key = id + '_ids';
                // if(data[key] === undefined || data[key] == '')data[key] = 0;

                $(this).combobox('reload', '/bsc_user/get_client_user?client_code=' + client_code + '&role=' + id + '&sales_id=' + sales_id);
                // $(this).combobox('reload', '/bsc_user/get_data_role/' + id + '?userIds=' + data[key]);
            }
        });
    }

    function goEditUrl(url) {
        var clientCode = $('#client_code2').combogrid('getValue');
        if (!clientCode) {
            alert('clientCode is empty');
        } else {
            window.open(url + '/' + clientCode)
        }
    }
    function add_box() {
        var length = $('.add_box').length;
        var lang = '';
        var btn = '';
        if(length <= 0){
            lang = "<?= lang('箱型箱量');?>";
            btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
        }else{
            btn = '<a class="easyui-linkbutton" onclick="del_box(this)" iconCls="icon-remove"></a>';
        }
        var str = '<tr class="add_box" >\n' +
            '                            <td>' + lang + '</td>\n'+
            '                            <td>\n'+
            '                                <select class="easyui-combobox box_info_size" name="box_info[size][]" data-options="\n'+
            '                                valueField:\'value\',\n'+
            '                                textField:\'value\',\n'+
            '                                url:\'/bsc_dict/get_option/container_size\',\n'+
            "                                onHidePanel: function() {  \n" +
            "                                   var valueField = $(this).combobox('options').valueField;  \n" +
            "                                   var val = $(this).combobox('getValue');  \n" +
            "                                   var allData = $(this).combobox('getData');  \n" +
            "                                   var result = true;  \n" +
            "                                   for (var i = 0; i < allData.length; i++) {  \n" +
            "                                       if (val == allData[i][valueField]) {  \n" +
            "                                           result = false;  \n" +
            "                                       }  \n" +
            "                                   }  \n" +
            "                                   if (result) {  \n" +
            "                                       $(this).combobox('clear');  \n" +
            "                                   } " +
            "                                }" +
            '                                "></select>\n'+
            '                                X\n'+
            '                                <input class="easyui-numberbox box_info_num" name="box_info[num][]">\n'+
            '                                ' + btn + '\n'+
            '                            </td>\n'+
            '                        </tr>';
        if(length == 0){
            $('#trans_mode').parents('tr').after(str);
        }else{
            $('.add_box:eq(' + (length - 1) + ')').after(str);
        }
        $.parser.parse('.add_box:eq(' + length + ')');
    }

    function del_box(e) {
        if(e == undefined){
            $('.add_box').remove();
        }else{
            $(e).parents('tr').remove();
        }
    }
    function reload_select(id = '', type='combobox'){
        if(type == 'combobox'){
            $('#trans_carrier').combobox('reload');
            $('#shipper_company2').combogrid('grid').datagrid('reload');
            $('#consignee_company2').combogrid('grid').datagrid('reload');
            $('#notify_company2').combogrid('grid').datagrid('reload');
            $('#client_company').combogrid('grid').datagrid('reload');
        }
    }

    function trans_tool_select(rec){
        if(rec.value == 'AIR'){
            //当运输工具是AIR，以下格子的名称做对应的修改：
            //2021-06-04 16:00 汪庭彬
            $('#trans_carrier').parent('td').siblings('td').html('Air line'+`<span style="color: red;font-size: 14px">*</span>`);
            $('#cus_no').parent('td').siblings('td').text('B/L NO');
            $('#booking_ETD').parent('td').siblings('td').html('Booking ETD'+`<span style="color: red;font-size: 14px">*</span>`);
        }else{
            $('#trans_carrier').parent('td').siblings('td').html('<?= lang('承运人');?>'+`<span style="color: red;font-size: 14px">*</span>`);
            $('#cus_no').parent('td').siblings('td').text('<?= lang('关单号');?>');
            $('#booking_ETD').parent('td').siblings('td').html('<?= lang('订舱船期');?>'+`<span style="color: red;font-size: 14px">*</span>`);
        }
    }

    function trans_carrier_select(rec){
        var trans_mode = $('#trans_mode').val();
        var trans_tool = $('#trans_tool').combobox('getValue');
        $('#trans_origin').combogrid('grid').datagrid('load', "/biz_port/get_option?type=" + trans_tool + "&carrier=" + rec.client_code + '&limit=true');
        $('#trans_discharge').combogrid('grid').datagrid('reload', "/biz_port/get_option?type=" + trans_tool + "&carrier=" + rec.client_code + '&limit=true');

        $('#trans_destination').combogrid('grid').datagrid('reload', "/biz_port/get_discharge?type=" + trans_tool + "&carrier=" + rec.client_code);
        
        $('#trans_origin').combogrid('clear');
        $('#trans_origin_name').textbox('clear');
        $('#trans_discharge').combogrid('clear');
        $('#trans_discharge_name').textbox('clear');
        $('#trans_destination_inner').textbox('clear');
        $('#trans_destination').combogrid('clear');
        $('#trans_destination_name').textbox('clear');
        $('#trans_destination_terminal').textbox('clear');
    }

    function biz_type_select(res){
        var trans_carrier = $('#trans_carrier').combobox('getValue');
        
        $('#biz_type').val(res.biz_type);
        $('#trans_mode').val(res.trans_mode);
        if(res.trans_mode == 'AIR'){
            $('#trans_tool').combobox('select', 'AIR');
        }else{
            $('#trans_tool').combobox('select', 'SEA');
        }
        var trans_tool = $('#trans_tool').combobox('getValue');
        //重新加载
        $('#trans_origin').combogrid('grid').datagrid('load', "/biz_port/get_option?type=" + trans_tool + '&carrier=' + trans_carrier + '&limit=true');
        $('#trans_discharge').combogrid('grid').datagrid('reload', "/biz_port/get_option?type=" + trans_tool + '&carrier=' + trans_carrier + '&limit=true');

        $('#trans_destination').combogrid('grid').datagrid('reload', "/biz_port/get_discharge?type=" + trans_tool + '&carrier=' + trans_carrier);
        if(res.trans_mode == 'FCL'){
            add_box();
        }else{
            del_box();
            //如果是LCL免用箱和免用时间禁用 运输方式自动CFS-CFS
            if(res.trans_mode == 'LCL'){
                $('#trans_term').combobox('setValue', 'CFS-CFS');
            }
        }
    }

    function checkName(val, type = 0){
        // var reg = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");\
        if(type == 0){
            var reg = new RegExp(/[^\x00-\x7f]/g);
        }else{
            var reg = new RegExp(/[^\x00-\x7f|\u4e00-\u9fa5]/g);
        }
        var rs = "";
        for (var i = 0, l = val.length; i < val.length; i++) {
            if(val.substr(i, 1).match(reg)) rs += val.substr(i, 1);
        }
        return rs;
    }

    function service_options_check(e){
        return false;
    }

    var opener = window.opener;

    function create_client_tool(div_id, id){
        var html = '<div id="' + div_id + '_div">' +
            '<form id="' + div_id + '_form" method="post">' +
            '<div style="padding-left: 5px;display: inline-block;">' +
            '<label><?= lang('客户代码');?>:</label><input name="client_code" class="easyui-textbox keydown_search" t_id="' + id + '" style="width:96px;"/>' +
            '<label><?= lang('客户全称');?>:</label><input name="company_name" class="easyui-textbox keydown_search" t_id="' + id + '" style="width:96px;"/>' +
            '</div>' +
            '<form>' +
            '<div style="padding-left: 1px; display: inline-block;">' +
            '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="' + id + '_click" onclick="query_report(\'' + div_id + '\', \'' + id + '\')"><?= lang('search');?></a>' +
            '</div>' +
            '</div>';
        $('body').append(html);
        $.parser.parse('#' + div_id + '_div');
        return div_id;
    }

    function create_company_tool(div_id, id){
        var html = '<div id="' + div_id + '_div">' +
            '<form id="' + div_id + '_form" method="post">' +
            '<div style="padding-left: 5px;display: inline-block;">' +
            '<label><?= lang('客户公司');?>:</label><input name="company_name" class="easyui-textbox keydown_search" t_id="' + id + '" style="width:96px;"/>' +
            '</div>' +
            '<form>' +
            '<div style="padding-left: 1px; display: inline-block;">' +
            '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="' + id + '_click" onclick="query_report(\'' + div_id + '\', \'' + id + '\')"><?= lang('search');?></a>' +
            '</div>' +
            '</div>';
        $('body').append(html);
        $.parser.parse('#' + div_id + '_div');
        return div_id;
    }

    function query_report(div_id, load_id){
        var where = {};
        var form_data = $('#' + div_id + '_form').serializeArray();
        $.each(form_data, function (index, item) {
            if(item.value == "") return true;
            if(where.hasOwnProperty(item.name) === true){
                if(typeof where[item.name] == 'string'){
                    where[item.name] =  where[item.name].split(',');
                    where[item.name].push(item.value);
                }else{
                    where[item.name].push(item.value);
                }
            }else{
                where[item.name] = item.value;
            }
        });
        if(where.length == 0){
            $.messager.alert('Tips', 'Search Value');
            return;
        }
        $('#' + load_id).combogrid('grid').datagrid('load',where);
    }

    //text添加输入值改变
    function keydown_search(e, click_id){
        if(e.keyCode == 13){
            $('#' + click_id + '_click').trigger('click');
        }
    }

    function combogrid_onBeforeLoad(param, id){
        if($('#' + id).combogrid('getValue') != '' && Object.keys(param).length == 0)param.q = $('#' + id).combogrid('getValue');
        if($('#' + id).combogrid('getValue') == '' && Object.keys(param).length == 0) return false;
        else return true;
    }

    /**
     * 获取当前控件的指定数据, 默认为获取值
     * @param target
     * @param method
     * @returns {jQuery|*|*|jQuery}
     */
    function getVal(target, method = 'getValue') {
        var data = $.data(target);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combobox !== undefined){
            return $(target).combobox(method);
        }else if(data.datetimebox !== undefined){
            return $(target).datetimebox(method);
        }else if(data.datebox !== undefined){
            return $(target).datebox(method);
        }else if(data.textbox !== undefined){
            return $(target).textbox(method);
        }else{
            if(method == 'getValue') return $(target).val();
            if(method == 'clear') return  $(target).val('');
            else return false;
        }
    }

    var is_message = false;

    $(function () {
        var a1 = service_option();
        //服务选项事件
        //service_options 选择哪个隐藏哪个
        $('#service_options_div').on('click', '.service_options', function () {
            service_options_check(this);
        });

        $.when(a1).done(function () {
            $.each($('.service_options'), function (index, item) {
                service_options_check(item);
            });
        });
        //初始值
        $('#client_contact').combobox('setValue', '<?= es_encode($client_contact);?>');
        $('#shipper_company').val('<?= es_encode($shipper_company);?>');
        $('#consignee_company').val('<?= es_encode($consignee_company);?>');
        $('#notify_company').val('<?= es_encode($notify_company);?>');
        
        dangerous_info($('#goods_type').combobox('getValue'));

        var client_company_config = {
            panelWidth: '1000px',
            panelHeight: '300px',
            idField: 'company_name',              //ID字段
            textField: 'company_name',    //显示的字段
            // multiple:true,
            // fitColumns : true,
            // striped: true,
            editable: false,
            pagination: false,           //是否分页
            // pageList : [30,50,100],
            // pageSize : 50,
            // toolbar : '#' + create_client_tool('client_company', 'client_company') + '_div',
            // rownumbers: true,           //序号
            collapsible: true,         //是否可折叠的
            method: 'post',
            columns:[[
                {field:'client_code',title:'<?= lang('客户代码');?>',width:100},
                {field: 'company_name', title: '<?= lang('客户全称');?>', width: 300},
                {field: 'tag', title: '<?= lang('标签');?>', width: 580, formatter: function(value,row,index){
                    if (value){
                        return value;
                    } else {
                        return "<a href='/biz_client/edit/" + row.client_code + "' target='_blank'><?= lang('维护标签');?></a>";
                    }
                }},
            ]],
            emptyMsg : 'No data!',
            mode: 'remote',
            onBeforeLoad:function(param){
                return combogrid_onBeforeLoad(param, 'client_company');
            },
            onBeforeSelect:function(index, row){
                // if(row.stop_bool==false){
                //     alert('<?= lang('代理已过期请重新设置');?>');
                //     return false;
                // }
            },
            onSelect: function(index, row){
                if(row.company_address == '') row.company_address = ' ';
                if(row.contact_email == '') row.contact_email = ' ';
                if(row.contact_telephone2 == '') row.contact_telephone2 = ' ';
                $('#client_address').val(row.company_address);
                $('#client_contact').combobox('setValue', row.client_contact);
                $('#client_telephone').combobox('setValue', row.contact_telephone2);
                $('#client_email').combobox('setValue', row.contact_email);
                $('#client_code').textbox('setValue',row.client_code);
                // $('#client_code2').combogrid('setValue',row.client_code);
                changeClient(row);
                if(row.email_verify == undefined){
                    alert('<?= lang('请添加当前客户的联系人');?>');
                }
                if(row.email_verify == 0){
                    alert('<?= lang('当前客户的联系人邮箱未验证');?>');
                }
            },
            onLoadSuccess: function(datas){
                var data = datas.rows;
                var val = $('#client_code').textbox('getValue');

                var is_changeClient = false;
                if(val == '') return false;
                $.each(data, function(index,item){
                    if(item.client_code == val){
                        changeClient(item, true);
                        is_changeClient = true;
                    }
                });
                if(!is_changeClient){
                    changeClient({client_code:val}, true);
                }
            },
        };
        //2022-09-01 由于第一个表格下拉会出现表头宽度错误的问题,这里用一个隐藏,没什么用的作为替代,保证原本的没问题
        $('#client_company2').combogrid("reset");
        $('#client_company2').combogrid(client_company_config);
        client_company_config.url = '/biz_client/get_option/client?other_role=logistics_client,oversea_client,oversea_agent,factory&is_tag=true';
        client_company_config.toolbar = '#' + create_client_tool('client_company', 'client_company') + '_div';
        client_company_config.value = '<?= es_encode($client_company);?>';
        $('#client_company').combogrid("reset");
        $('#client_company').combogrid(client_company_config);

        //二级委托方--start
        client_company_config.idField = 'client_code';
        client_company_config.textField = 'company_name'
        client_company_config.value = '<?= $client_code2;?>';
        client_company_config.url = '/biz_client_second_relation/getSecondByClient?client_code=<?= $client_code;?>';
        client_company_config.toolbar = '#' + create_client_tool('client_code2', 'client_code2') + '_div';
        client_company_config.columns = [[
            {field: 'client_code', title: '<?= lang('client_code');?>', width: 100},
            {field: 'company_name', title: '<?= lang('company_name');?>', width: 600},
        ]];
        client_company_config.onSelect = function(index, row){
            //现在shipper等根据这个字段来了,所以统一
            changeClient2(row);
        };
        client_company_config.onLoadSuccess = function(data){
            var client_code = $(this).combogrid('getValue');
            changeClient2({client_code:client_code},true);

            if(data.rows.length <= 1){//如果只有1条,那么隐藏DIV
                $('.client_code2_div').css('display', 'none');
            }else{
                $('.client_code2_div').css('display', 'block');
            }
        };
        $('#client_code2').combogrid("reset");
        $('#client_code2').combogrid(client_company_config);
        //二级委托方--end

        var company_config = {
            panelWidth: '1000px',
            panelHeight: '300px',
            idField: 'id',              //ID字段
            textField: 'company_name',    //显示的字段
            // multiple:true,
            // fitColumns : true,
            striped: true,
            editable: false,
            pagination: false,           //是否分页
            // pageList : [30,50,100],
            // pageSize : 50,
            toolbar : '#' + create_company_tool('shipper_company2', 'shipper_company2') + '_div',
            // rownumbers: true,           //序号
            collapsible: true,         //是否可折叠的
            method: 'post',
            columns:[[
                {field:'company_name',title:'<?= lang('客户公司');?>',width:200},
                {field:'company_address',title:'<?= lang('客户地址');?>',width:400},
                {field:'company_contact',title:'<?= lang('客户联系人');?>',width:100},
                {field:'company_telephone',title:'<?= lang('客户电话');?>',width:100},
                {field:'company_email',title:'<?= lang('客户邮箱');?>',width:100},
            ]],
            emptyMsg : 'No Data!',
            mode: 'remote',
            onSelect: function(index, row){
                if(row.company_address == '') row.company_address = ' ';
                if(row.company_contact == '') row.company_contact = ' ';
                if(row.company_telephone == '') row.company_telephone = ' ';
                if(row.company_email == '') row.company_email = ' ';
                $('#shipper_company').val(row.company_name);
                $('#shipper_address').val(row.company_address);
                $('#shipper_contact').val(row.company_contact);
                $('#shipper_telephone').val(row.company_telephone);
                $('#shipper_email').val(row.company_email);
            },
            value:'<?= es_encode($shipper_company);?>',
        };
        $('#shipper_company2').combogrid("reset");
        $('#shipper_company2').combogrid(company_config);

        company_config.toolbar = '#' + create_company_tool('consignee_company2', 'consignee_company2') + '_div';
        company_config.onSelect = function(index, row){
            if(row.company_address == '') row.company_address = ' ';
            if(row.company_contact == '') row.company_contact = ' ';
            if(row.company_telephone == '') row.company_telephone = ' ';
            if(row.company_email == '') row.company_email = ' ';
            $('#consignee_company').val(row.company_name);
            $('#consignee_address').val(row.company_address);
            $('#consignee_contact').val(row.company_contact);
            $('#consignee_telephone').val(row.company_telephone);
            $('#consignee_email').val(row.company_email);
        };
        company_config.value = '<?= es_encode($consignee_company);?>';
        $('#consignee_company2').combogrid("reset");
        $('#consignee_company2').combogrid(company_config);

        company_config.toolbar = '#' + create_company_tool('notify_company2', 'notify_company2') + '_div';
        company_config.onSelect = function(index, row){
            if(row.company_address == '') row.company_address = ' ';
            if(row.company_contact == '') row.company_contact = ' ';
            if(row.company_telephone == '') row.company_telephone = ' ';
            if(row.company_email == '') row.company_email = ' ';
            $('#notify_company').val(row.company_name);
            $('#notify_address').val(row.company_address);
            $('#notify_contact').val(row.company_contact);
            $('#notify_telephone').val(row.company_telephone);
            $('#notify_email').val(row.company_email);
        };
        company_config.value = '<?= es_encode($notify_company);?>';
        $('#notify_company2').combogrid("reset");
        $('#notify_company2').combogrid(company_config);

        $.each($('.keydown_search'), function (i,it) {
            $(it).textbox('textbox').bind('keydown', function(e){keydown_search(e, $(it).attr('t_id'));});
        });

        var service_option_tab = $("#service_option_tab").find(".panel-tool");
        $(service_option_tab[0]).prepend('<a href="javascript:void(0)" class="icon-save" onclick="$(\'#f\').form(\'submit\');" title="save" style="margin-right:15px;"></a>');

        $('#f').form({
            url: '/biz_shipment/add_data_ajax/',
            onSubmit: function(){
                var isValid = $(this).form('validate');
                var vv = $('#trans_origin').textbox('getValue');
                if (vv == "") {
                    alert("<?= lang('{field} must', array('field' => lang('起运港代码')));?>！");
                    return false;
                }
                var vv = $('#biz_type1').combobox('getValue');
                if (vv == " " || vv == "") {
                    alert("<?= lang('{field} must', array('field' => lang('业务类型')));?>！");
                    return false;
                }
                $.each($('textarea'), function(index, item){
                    var str = $(this).val();
                    var id = $(this).prop('id');
                    var ids = {
                        shipper_address: ['shipper_address', '<?= lang('发货人地址');?>'],
                        consignee_address: ['consignee_address', '<?= lang('收货人地址');?>'],
                        notify_address: ['notify_address', '<?= lang('通知人地址');?>'],
                        description: ['description', '<?= lang('英文货物描述');?>'],
                        mark_nums: ['mark_nums', '<?= lang('货物唛头');?>'],
                    };
                    str = qzb(str);
                    $(this).val(str);
                    var newstr = $(this).val();
                    if(ids[id] !== undefined){
                        var special_char = checkName(newstr);
                    }else{
                        // var special_char = checkName(newstr, 1);
                        var special_char = "";
                    }
                    if(special_char !== ''){
                        $.messager.alert('error', ids[id][1] + 'Invalid charactors:<br/>' + special_char);
                        is_special_char = true;
                        return false;
                    }else{
                        is_special_char = false;
                        $(this).val($(this).val().toUpperCase());
                    }
                });
                if(is_special_char){
                    return false;
                }
                $('#f').append($('#service_option_tab').clone().css('display', 'none'));
                $('.icon-save').attr('onclick', '');
                return true;
            },
            success:function(res_json){
                $('#f #service_option_tab').remove();
                try {
                    var res = $.parseJSON(res_json);

                    if(res.code !== 444)alert(res.msg);
                    $('#hs_code_tip').html('');

                    $('.icon-save').attr('onclick', "$('#f').form('submit');");

                    if(res.code == 0){//新增成功
                        try{opener.$('#tt').edatagrid('reload');}catch (e) {}
                        location.href = '/biz_shipment/edit/' + res.data.id;
                    }else if(res.code == 2){//代码tsl hscode错误
                        $('#hs_code_tip').html('<a href="/bsc_dict/read/hs_code/TSL" target="_blank" style="color:red;">hs_code</a>');
                    }else if(res.code == 444){//出现特殊字符
                        // GET方式示例，其它方式修改相关参数即可
                        var form = $('<form></form>').attr('action','/other/ts_text').attr('method','POST').attr('target', '_blank');
                        var json_data = {};
                        json_data['text'] = res.text;
                        json_data['ts_str[]'] = res.ts_str;
                        json_data['msg'] = res.msg;
                        $.each(json_data, function (index, item) {
                            if(typeof item === 'string'){
                                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                            }else{
                                $.each(item, function(index2, item2){
                                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                                })
                            }
                        });

                        form.appendTo('body').submit().remove();
                    }
                }catch (e) {
                    alert(res_json);
                    $('.icon-save').attr('onclick', "$('#f').form('submit');");
                }
            }
        });
        
        var now = Date.parse(new Date()) / 1000;
        
        //订舱船期范围设置
        $('#booking_ETD').datebox('calendar').calendar({
            //到时候不存在可选择值的，在这里false
            validator: function(date){
                //日期不能超过前后半年
                var star = now - 3600 * 24 * 183;
                var end = now + 3600 * 24 * 183;
                var date_time = Date.parse(date) / 1000;
                if (date_time >= star && date_time <= end) {
                    return true;
                } else {
                    return false;
                }
            }
        });
    });

    /**
     * 加载服务选项数据
     * @returns {*|jQuery|boolean|{getAllResponseHeaders, abort, setRequestHeader, readyState, getResponseHeader, overrideMimeType}}
     */
    function service_option() {
        return $.ajax({
            url: '/bsc_dict/get_option/service_option',
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                var str = '<table style="float: left;">';
                var service_options_arr = [];
                try {
                    service_options_arr = $.parseJSON($('#service_options').val());
                } catch(e) {
                }
                var service_options = [];
                $.each(service_options_arr, function(i, it){
                    service_options.push(it[0]);
                });
                str += '<tr style="text-align: center"><th>POL</th></tr>';
                $.each(res, function (index, item) {
                    if(item.orderby <= 100){
                        str += '<tr>';
                        var checked = '';
                        if(item.name !== '' && item.value !== ''){
                            if($.inArray(item.value, service_options) !== -1){
                                checked = 'checked';
                            }
                            str += '<td><input class="service_options" type="checkbox" name="service_options[' + index + '][]" value="' + item.value + '" style="vertical-align:middle;" ' + checked + '>' + item.lang + '</td>';
                        }
                        str += '</tr>';
                    }
                });
                str += '</table>';
                str += '<table style="float: left;">';
                str += '<tr style="text-align: center"><th>POD</th></tr>';
                $.each(res, function (index, item) {
                    if(item.orderby > 100){
                        str += '<tr>';
                        var checked = '';
                        if(item.name !== '' && item.value !== ''){
                            if($.inArray(item.value, service_options) !== -1){
                                checked = 'checked';
                            }
                            str += '<td><input class="service_options" type="checkbox" name="service_options[' + index + '][]" value="' + item.value + '" style="vertical-align:middle;" ' + checked + '>' + item.lang + '</td>';
                        }
                        str += '</tr>';
                    }
                });
                str += '</table>';
                $('#service_options_div').append(str);

            }
        });
    }

    /**
     * 展开目的港服务
     */
    function add_INCO_term_option() {
        // $('.INCO_term_option').css('display', 'table-row');
    }

    /**
     * 关闭目的港服务且清理掉数据
     */
    function del_INCO_term_option() {
        // $('input[name="INCO_option[]"]').removeAttr('checked');
        // $('#INCO_address').val('');
        // $('.INCO_term_option').css('display', 'none');
    }
    
    function dangerous_info(goods_type) {
        //如果不为普通货物，贼显示危险品详细信息
        if(goods_type == 'GC' || goods_type == 'RF'){
            $('.dangerous').css('display','none');
            $('.special_size').css('display','none');
        }else{
            if(goods_type == 'DR'){
                $('#isdangerous').textbox({required: true});
                // $('#ism_spec').textbox({required: true});
                $('#UN_no').textbox({required: true});
                // $('#IMDG_page').textbox({required: true});
                // $('#packing_type').textbox({required: true});
                // $('#flash_point').textbox({required: true});
                // $('#IMDG_code').textbox({required: true});
                // $('#is_pollution_sea').textbox({required: true});
                // $('#ems_no').textbox({required: true});
                // $('#MFAG_NO').textbox({required: true});
                // $('#e_contact_name').textbox({required: true});
                // $('#e_contact_tel').textbox({required: true});

                $('#long').textbox({required: false,value:''});
                $('#wide').textbox({required: false,value:''});
                $('#high').textbox({required: false,value:''});

                $('.dangerous').css('display','table-row');
                $('.special_size').css('display','none');
            }else if(goods_type == 'FR'){
                $('#isdangerous').textbox({required: false,value:''});
                // $('#ism_spec').textbox({required: false,value:''});
                $('#UN_no').textbox({required: false,value:''});
                // $('#IMDG_page').textbox({required: false,value:''});
                // $('#packing_type').textbox({required: false,value:''});
                // $('#flash_point').textbox({required: false,value:''});
                // $('#IMDG_code').textbox({required: false,value:''});
                // $('#is_pollution_sea').textbox({required: false,value:''});
                // $('#ems_no').textbox({required: false,value:''});
                // $('#MFAG_NO').textbox({required: false,value:''});
                // $('#e_contact_name').textbox({required: false,value:''});
                // $('#e_contact_tel').textbox({required: false,value:''});

                $('#long').textbox({required: true});
                $('#wide').textbox({required: true});
                $('#high').textbox({required: true});

                $('.dangerous').css('display','none');
                $('.special_size').css('display','table-row');
            }
        }
    }

    //2022-07-11 新增
    function get_client(){
        var client=$('#client_code').textbox('getValue');
        if(client==''){
            window.open("/biz_client/index/client");
        }else{
            window.open("/biz_client/edit/"+client);
        }
    }
</script>
<style type="text/css">
    .hide{
        display: none;
    }
    .dangerous{
        display: none;
    }
    .special_size{
        display: none;
    }
    .box_info_num{
        width: 100px;
    }
    .box_info_size{
        width: 100px;
    }
    .INCO_term_option{
        display: none;
    }
    input:read-only,select:read-only,textarea:read-only
    {
        background-color: #F5F5F5;
        cursor: default;
    }
    input:disabled,select:disabled,textarea:disabled
    {
        background-color: #d6d6d6;
        cursor: default;
    }
</style>
<body style="margin:10px;">
<div id="service_option_tab" class="easyui-accordion" data-options="multiple:true" style="width:300px;position: absolute;right: 10px;top: 10px;z-index: 999;">
    <div title="<?= lang('Service Option');?>" id="service_options_div" style="padding:10px;">
        <input type="hidden" id="service_options" value='<?= $service_options;?>'>
    </div>
</div>
<table>
    <tr>
        <td>
            <h2> <?= lang('新增');?> </h2></td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td></td>
    </tr>
</table>

<div class="easyui-tabs" style="width:100%;height:850px">
    <div title="<?= lang('Basic Registration');?>" style="padding:1px">
        <form id="f" name="f" method="post" action="/biz_shipment/add_data/">
            <input type="hidden" name="__token__" value="<?php echo setToken('shipment_add');?>">
            <input type="hidden" name="consol_id" value="<?php echo $consol_id;?>">
            <input type="hidden" name="ebooking_id" value="<?php echo $ebooking_id;?>">
            <div class="easyui-layout" style="width:100%;height:780px;">
                <div data-options="region:'west',tools:'#tt'" title="Basic" style="width:350px;">
                    <div class="easyui-accordion" data-options="multiple:true" style="width:340px;height1:300px;">
                        <div title="<?= lang('Client');?>" style="padding:10px;">
                            <input name="client_code" id="client_code" class="easyui-textbox" readonly value="<?= $client_code;?>" style="width:90px;" >

                            <div style="display: none;">
                                <select id="client_company2" class="easyui-combogrid" editable="true" style="width:175px;"></select>
                            </div>
                            <select id="client_company" class="easyui-combogrid" editable="true" name="client_company" style="width:175px;"></select>
                            <a style="cursor: pointer"  onclick="get_client()" target="_blank"><?= lang('E');?></a>
                            |
                            <a href="javascript: reload_select('client');"><?= lang('R');?></a>
                            <div class="client_code2_div" style="display: none">
                                <?= lang('第二委托方');?>
                                <a href="javascript:void;" class="easyui-tooltip" data-options="
                                    content: $('<div></div>'),
                                    onShow: function(){
                                        $(this).tooltip('arrow').css('left', 20);
                                        $(this).tooltip('tip').css('left', $(this).offset().left);
                                    },
                                    onUpdate: function(cc){
                                        cc.panel({
                                            width: 300,
                                            height: 'auto',
                                            border: false,
                                            href: '/bsc_help_content/help/second_client'
                                        });
                                    }
                                "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a><br/>
                                <select name="client_code2" id="client_code2" class="easyui-combogrid" style="width:270px;" ></select><br>
                            </div>
                            <br><?= lang('委托方联系人');?>:
                            <br><select  name="client_contact" id="client_contact" class="easyui-combobox" style="width:300px;"
                                         data-options="
                                valueField:'name',
								textField:'name',
								url:'',
								onSelect: function(rec){
									$('#client_telephone').combobox('setValue',rec.telephone);
									$('#client_email').combobox('setValue',rec.email);
								},
								onLoadSuccess:function(){
								    var data = $(this).combobox('getData');
								    $('#client_telephone').combobox('loadData', data);
                                    $('#client_email').combobox('loadData',  data);
								},
                            "
                            ></select>
                            <br><?= lang('委托方电话'); ?>:
                            <br><select name="client_telephone" id="client_telephone" class="easyui-combobox" style="width:300px;" data-options="
                                editable:false,
                                valueField:'telephone',
                                textField:'telephone',
                                value:'<?= $client_telephone;?>',
                            "></select>
                            <br><?= lang('委托方邮箱'); ?>:
                            <br><select name="client_email" id="client_email" class="easyui-combobox" style="width:300px;" data-options="
                                editable:false,
                                valueField:'email',
                                textField:'email',
                                value:'<?= $client_email;?>',
                            "></select>
                            <br><?= lang('委托方地址'); ?>:
                            <br><textarea name="client_address" id="client_address" style="height:60px;width:300px;"><?php echo $client_address; ?></textarea>
                        </div>
						<div style="display: none">
							<div title="<?= lang('Shipper');?>" style="overflow:auto;padding:10px">
								<select id="shipper_company2" class="easyui-combogrid" style="width:255px;"></select>
								<input type="hidden" id="shipper_company" name="shipper_company">
								<a href="javascript: goEditUrl('/biz_company/index/shipper');"><?= lang('E'); ?></a>
								|
								<a href="javascript: reload_select('shipper');"><?= lang('R');?></a>
								<br><?= lang('发货人地址'); ?>:
								<br><textarea name="shipper_address" id="shipper_address" style="height:60px;width:300px;"><?php echo $shipper_address; ?></textarea>
								<br><?= lang('发货人联系人'); ?>:
								<br><textarea name="shipper_contact" id="shipper_contact" style="height:40px;width:300px;"><?php echo $shipper_contact; ?></textarea>
								<br><?= lang('发货人电话'); ?>:
								<br><textarea name="shipper_telephone" id="shipper_telephone" style="height:40px;width:300px;"><?php echo $shipper_telephone; ?></textarea>
								<br><?= lang('发货人邮箱'); ?>:
								<br><textarea name="shipper_email" id="shipper_email" style="height:40px;width:300px;"><?php echo $shipper_email; ?></textarea>
							</div>
							<div title="<?= lang('Consignee');?>" style="padding:10px;">
								<select id="consignee_company2" class="easyui-combogrid" style="width:255px;"></select>
								<input type="hidden" id="consignee_company" name="consignee_company">
								<a href="javascript: goEditUrl('/biz_company/index/consignee');"><?= lang('E'); ?></a>
								|
								<a href="javascript: reload_select('consignee');"><?= lang('R');?></a>
								<br><?= lang('收货人地址'); ?>:
								<br><textarea name="consignee_address" id="consignee_address" style="height:60px;width:300px;" ><?php echo $consignee_address; ?></textarea>
								<br><?= lang('收货人联系人'); ?>:
								<br><textarea name="consignee_contact" id="consignee_contact" style="height:40px;width:300px;"><?php echo $consignee_contact; ?></textarea>
								<br><?= lang('收货人电话'); ?>:
								<br><textarea name="consignee_telephone" id="consignee_telephone" style="height:40px;width:300px;"><?php echo $consignee_telephone; ?></textarea>
								<br><?= lang('收货人邮箱'); ?>:
								<br><textarea name="consignee_email" id="consignee_email" style="height:40px;width:300px;"><?php echo $consignee_email; ?></textarea>
							</div>
							<div title="<?= lang('Notify Party');?>" style="padding:10px;">
								<select id="notify_company2" class="easyui-combogrid" style="width:255px;"></select>
								<input type="hidden" id="notify_company" name="notify_company">
								<a href="javascript: goEditUrl('/biz_company/index/notify');"><?= lang('E'); ?></a>
								|
								<a href="javascript: reload_select('notify');"><?= lang('R');?></a>
								<br><?= lang('通知人地址'); ?>:
								<br><textarea name="notify_address" id="notify_address" style="height:60px;width:300px;"><?php echo $notify_address; ?></textarea>
								<br><?= lang('通知人联系人'); ?>:
								<br><textarea name="notify_contact" id="notify_contact" style="height:40px;width:300px;"><?php echo $notify_contact; ?></textarea>
								<br><?= lang('通知人电话'); ?>:
								<br><textarea name="notify_telephone" id="notify_telephone" style="height:40px;width:300px;"><?php echo $notify_telephone; ?></textarea>
								<br><?= lang('通知人邮箱'); ?>:
								<br><textarea name="notify_email" id="notify_email" style="height:40px;width:300px;"><?php echo $notify_email; ?></textarea>
							</div>
						</div>
                    </div>
                    <div id="tt">
                        <a href="javascript:void(0)" class="icon-save" onclick="$('#f').form('submit');" title="save"
                           style="margin-right:15px;"></a>
                    </div>
                </div>
                <div data-options="region:'center',title:'Details'">
                    <div style="float: left;">
                        <table width="500" border="0" cellspacing="0">
                            <tr>
                                <td><?= lang('update_biz_type');?><span style="color: red;font-size: 14px">*</span></td>
                                <td>
                                    <select class="easyui-combobox" id="biz_type1" style="width:180px;" data-options="
                                        required:true,
                                        editable:false,
                                        valueField:'name',
                                        textField:'name',
                                        url:'/bsc_dict/get_biz_type',
                                        value: '<?php echo $biz_type . ' ' . $trans_mode;?>',
                                        onSelect: function(res){
                                            biz_type_select(res);
                                        },
                                    ">
                                    </select>
                                    <input type="hidden" name="trans_mode" id="trans_mode" value="<?= $trans_mode;?>">
                                    <input type="hidden" name="biz_type" id="biz_type" value="<?= $biz_type;?>">
                                    <select name="trans_tool" id="trans_tool" class="easyui-combobox" style="width:75px;" data-options="
                                        editable:false,
                                        valueField:'name',
                                        textField:'name',
                                        url:'/bsc_dict/get_option/trans_tool',
                                        value:'<?= $trans_tool;?>',
                                        onSelect:function(rec){
                                            trans_tool_select(rec);
                                        },
                                        onLoadSuccess: function(){
                                            var val = $(this).combobox('getValue');
                                            var rec = {value:val};
                                            trans_tool_select(rec);
                                        }
                                    ">
                                    </select>
                                </td>
                            </tr>
                            <?php if(isset($box_info) && !empty($box_info)){foreach($box_info as $key => $row){
                                $this_btn = '<a class="easyui-linkbutton" onclick="del_box(this)" iconCls="icon-remove"></a>';
                                $this_lang = '';
                                if($key == 0){
                                    $this_btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
                                    $this_lang = lang('box_info');
                                }
                                ?>

                                <tr class="add_box">
                                    <td><?= $this_lang;?></td>
                                    <td>
                                        <select class="easyui-combobox box_info_size" name="box_info[size][]" data-options="
                                        valueField:'value',
                                        textField:'value',
                                        url:'/bsc_dict/get_option/container_size',
                                        value: '<?= $row['size']?>',
                                        onHidePanel: function() {
                                            var valueField = $(this).combobox('options').valueField;
                                            var val = $(this).combobox('getValue');
                                            var allData = $(this).combobox('getData');
                                            var result = true;
                                            for (var i = 0; i < allData.length; i++) {
                                                if (val == allData[i][valueField]) {
                                                    result = false;
                                                }
                                            }
                                            if (result) {
                                                $(this).combobox('clear');
                                            }
                                        }
                                    "></select>
                                        X
                                        <input class="easyui-numberbox box_info_num" required name="box_info[num][]" value="<?= $row['num'];?>">
                                        <?= $this_btn;?>
                                    </td>
                                </tr>
                            <?php }} ?>
                            <tr>
                                <td><?= lang('booking_ETD');?><span style="color: red;font-size: 14px">*</span> </td>
                                <td>
                                    <input class="easyui-datebox" editable="false" name="booking_ETD" id="booking_ETD"  style="width:255px;" value="<?php echo $booking_ETD;?>"  />
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('sailing_code');?></td>
                                <td>
                                    <input class="easyui-textbox" name="sailing_code" id="sailing_code" style="width:255px;" value="<?= $sailing_code;?>" data-options="onChange:toUpperCaseChange">
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_status'); ?><span style="color: red;font-size: 14px">*</span></td>
                                <td>
                                    <select class="easyui-combobox" name="status" id="status" style="width:255px;" data-options="
                                        required:true,
                                        editable:false,
                                        valueField:'value',
                                        textField:'name',
                                        url:'/bsc_dict/get_option/bill_status',
                                        value:'<?php echo $status == '' ? 'normal' : $status; ?>',">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_hbl_no'); ?></td>
                                <td>
                                    <input class="easyui-textbox" name="hbl_no" id="hbl_no" style="width:255px;" value=""
                                           />
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_hbl_type'); ?></td>
                                <td>
                                    <select id="hbl_type" class="easyui-combobox" editable="false" name="hbl_type" style="width:255px;" data-options="
                                        valueField:'value',
                                        textField:'name',
                                        url:'/bsc_dict/get_hbl_type',
                                        value: '<?php echo $hbl_type;?>',
                                    ">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('批复号');?></td>
                                <td>
                                    <input type="text" class="easyui-textbox" name="AGR_no" id="AGR_no" style="width: 255px;" value="<?= $AGR_no;?>">
                                </td>
                            </tr>

                            <tr>
                                <td><?= lang('update_shipper_ref'); ?> </td>
                                <td>
                                    <input class="easyui-textbox" name="shipper_ref" id="shipper_ref" style="width:255px;"
                                           value="<?php echo $shipper_ref; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_release_type'); ?> </td>
                                <td>
                                    <select id="release_type" class="easyui-combobox" editable="false"
                                            name="release_type" style="width:100px;" data-options="
        								valueField:'value',
        								textField:'value',
        								url:'/bsc_dict/get_option/release_type',
        								value:'<?php echo $release_type; ?>',
        								onSelect: function(rec){
                                            if(rec.value=='TER'){
                                                $('#release_type_remark_box').css('display', '');
                                            } else {
                                                $('#release_type_remark').combobox('setValue','');
                                                $('#release_type_remark_box').css('display', 'none');
                                            }

                                        },
    								">
                                    </select>
                                    <a href="javascript:void;" class="easyui-tooltip" data-options="
                                        content: $('<div></div>'),
                                        onShow: function(){
                                            $(this).tooltip('arrow').css('left', 20);
                                            $(this).tooltip('tip').css('left', $(this).offset().left);
                                        },
                                        onUpdate: function(cc){
                                            cc.panel({
                                                width: 300,
                                                height: 'auto',
                                                border: false,
                                                href: '/bsc_help_content/help/release_type'
                                            });
                                        }
                                    "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>

                                    <span id="release_type_remark_box" style="display:none;">
                                        <select class="easyui-combobox" id="release_type_remark" name="release_type_remark" label="State:" labelPosition="top" style="width:134px;">
                                            <option value="0"><?= lang('请选择放单方式');?></option>
                                            <option value="1"><?= lang('等通知电放');?></option>
                                            <option value="2"><?= lang('直接电放');?></option>
                                        </select>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_trans_carrier'); ?><span style="color: red;font-size: 14px">*</span></td>
                                <td>
                                    <select id="trans_carrier" class="easyui-combobox" editable="true" name="trans_carrier" style="width:255px;" data-options="
                                        valueField:'client_code',
								        textField:'client_name',
                                        url:'/biz_client/get_option/carrier',
                                        value:'<?php echo $trans_carrier; ?>',
                                        onSelect:function(rec){
                                            trans_carrier_select(rec);
                                        },
                                        onHidePanel: function() {
                                            var valueField = $(this).combobox('options').valueField;
                                            var val = $(this).combobox('getValue');
                                            var allData = $(this).combobox('getData');
                                            var result = true;
                                            for (var i = 0; i < allData.length; i++) {
                                                if (val == allData[i][valueField]) {
                                                    result = false;
                                                }
                                            }
                                            if (result) {
                                                $(this).combobox('clear');
                                            }
                                        },
                                    ">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_trans_origin');?><span style="color: red;font-size: 14px">*</span></td>
                                <td>
                                    <select class="easyui-combogrid" style="width: 80px;" name="trans_origin" id="trans_origin" data-options="
                                        required:true,
                                        panelWidth: '1000px',
                                        panelHeight: '300px',
                                        idField: 'port_code',              //ID字段
                                        textField: 'port_code',    //显示的字段
                                        url:'/biz_port/get_option?type=<?= $trans_tool;?>&carrier=<?= $trans_carrier;?>&limit=true',
                                        striped: true,
                                        editable: false,
                                        pagination: false,           //是否分页
                                        toolbar : '#trans_origin_div',
                                        collapsible: true,         //是否可折叠的
                                        method: 'get',
                                        columns:[[
                                            {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                                            {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                                            {field:'terminal',title:'<?= lang('码头');?>',width:200},
                                        ]],
                                        mode: 'remote',
                                        value: '<?= $trans_origin;?>',
                                        emptyMsg : 'No data!',
                                        onSelect: function(index, row){
                                            if(row !== undefined){
                                                $('#trans_origin_name').textbox('setValue', row.port_name);
                                            }
                                        },
                                        onShowPanel:function(){
                                            var opt = $(this).combogrid('options');
                                            var toolbar = opt.toolbar;
                                            combogrid_search_focus(toolbar, 1);
                                        },
                                    ">
                                    </select>
                                    <input class="easyui-textbox" style="width: 170px;" name="trans_origin_name" id="trans_origin_name" value="<?= $trans_origin_name;?>">
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_trans_discharge');?><span style="color: red;font-size: 14px">*</span></td>
                                <td>
                                    <select class="easyui-combogrid" style="width: 80px;" name="trans_discharge" id="trans_discharge" data-options="
                                        panelWidth: '1000px',
                                        panelHeight: '300px',
                                        idField: 'port_code',              //ID字段
                                        textField: 'port_code',    //显示的字段
                                        url:'/biz_port/get_option?type=<?= $trans_tool;?>&carrier=<?= $trans_carrier;?>&limit=true',
                                        striped: true,
                                        editable: false,
                                        pagination: false,           //是否分页
                                        toolbar : '#trans_discharge_div',
                                        collapsible: true,         //是否可折叠的
                                        method: 'get',
                                        columns:[[
                                            {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                                            {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                                            {field:'terminal',title:'<?= lang('码头');?>',width:200},
                                        ]],
                                        mode: 'remote',
                                        value: '<?= $trans_discharge;?>',
                                        emptyMsg : 'No data!',
                                        onSelect: function(index, row){
                                            if(row !== undefined){
                                                $('#trans_discharge_name').textbox('setValue', row.port_name);
                                            }
                                        },
                                        onShowPanel:function(){
                                            var opt = $(this).combogrid('options');
                                            var toolbar = opt.toolbar;
                                            combogrid_search_focus(toolbar, 1);
                                        },
                                    ">
                                    </select>
                                    <input class="easyui-textbox" style="width: 170px;" name="trans_discharge_name" id="trans_discharge_name" value="<?= $trans_discharge_name;?>">
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_trans_destination');?><span style="color: red;font-size: 14px">*</span></td>
                                <td>
                                    <input class="easyui-textbox" readonly style="width: 80px;" id="trans_destination_inner" name="trans_destination_inner" value="<?= $trans_destination_inner;?>">
                                    <select class="easyui-combogrid" style="width: 70px;" name="trans_destination" id="trans_destination" data-options="
                                        panelWidth: '1000px',
                                        panelHeight: '300px',
                                        idField: 'port_code',              //ID字段
                                        textField: 'port_code',    //显示的字段
                                        url:'/biz_port/get_discharge?type=<?= $trans_tool;?>&carrier=<?= $trans_carrier?>',
                                        striped: true,
                                        editable: false,
                                        pagination: false,           //是否分页
                                        toolbar : '#trans_destination_div',
                                        collapsible: true,         //是否可折叠的
                                        method: 'get',
                                        columns:[[
                                            {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                                            {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                                            {field:'terminal',title:'<?= lang('码头');?>',width:200},
                                        ]],
                                        mode: 'remote',
                                        value: '<?= $trans_destination;?>',
                                        emptyMsg : 'No data!',
                                        onSelect: function(index, row){
                                            if(row !== undefined){
                                                $('#trans_destination_inner').textbox('setValue', row.inner_port_code);
                                                $('#trans_destination_name').textbox('setValue', row.port_name);
                                                $('#sailing_area').textbox('setValue', row.sailing_area);
                                                $('#sailing').textbox('setValue', row.sailing_lang);
                                                $('#trans_destination_terminal').textbox('setValue', row.terminal);
                                            }
                                        },
                                        onShowPanel:function(){
                                            var opt = $(this).combogrid('options');
                                            var toolbar = opt.toolbar;
                                            combogrid_search_focus(toolbar, 1);
                                        },
                                    ">
                                    </select>
                                    <input class="easyui-textbox" style="width: 97px;" name="trans_destination_name" id="trans_destination_name" value="<?= $trans_destination_name;?>">
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('trans_destination_terminal');?></td>
                                <td>
                                    <input class="easyui-textbox" style="width: 255px;" name="trans_destination_terminal" id="trans_destination_terminal" value="<?= $trans_destination_terminal;?>">
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('sailing_area');?></td>
                                <td>
                                    <input class="easyui-textbox" name="sailing_area" id="sailing_area" style="width:80px;" readonly value="<?= $sailing_area;?>">
                                    <input class="easyui-textbox" id="sailing" style="width:170px;" readonly>
                                </td>

                            </tr>
                            <tr>
                                <td><?= lang('关单号');?></td>
                                <td>
                                    <input class="easyui-textbox" id="cus_no" name="cus_no" style="width: 245px;" value="<?= $cus_no;?>" data-options="onChange:toUpperCaseChange">
                                    <a href="javascript:void;" class="easyui-tooltip" data-options="
                                        content: $('<div></div>'),
                                        onShow: function(){
                                            $(this).tooltip('arrow').css('left', 20);
                                            $(this).tooltip('tip').css('left', $(this).offset().left);
                                        },
                                        onUpdate: function(cc){
                                            cc.panel({
                                                width: 300,
                                                height: 'auto',
                                                border: false,
                                                href: '/bsc_help_content/help/cus_no'
                                            });
                                        }
                                    "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('trans_term');?><span style="color: red;font-size: 14px">*</span></td>
                                <td>
                                    <select id="trans_term" class="easyui-combobox"  editable="false" name="trans_term" style="width:255px;" data-options="
                                    value:'<?= $trans_term;?>',
                                    valueField:'value',
                                    textField:'value',
                                    url:'/bsc_dict/get_option/trans_term',
                                    ">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_INCO_term'); ?></td>
                                <td>
                                    <select id="INCO_term" class="easyui-combobox" editable="false" name="INCO_term" style="width:255px;" data-options="
                                        valueField:'value',
                                        textField:'value',
                                        url:'/bsc_dict/get_option/INCO_term',
                                        onSelect: function(rec){
                                            $destination_SO = ['DDU', 'DDP', 'DAP'];
                                            if($.inArray(rec.value, $destination_SO) !== -1){
                                                add_INCO_term_option();
                                            }else{
                                                del_INCO_term_option();
                                            }
                                        },
                                        onLoadSuccess: function(){
                                            $('#INCO_term').combobox('select', '<?php echo $INCO_term;?>');
                                        }
                                    ">
                                    </select>
                                </td>
                            </tr>
                            <!--<tr class="INCO_term_option">-->
                            <!--    <td><?= lang('INCO_option');?></td>-->
                            <!--    <td>-->
                            <!--        <input type='checkbox' name='INCO_option[]' value='custom' style='vertical-align:middle;' <?php if(in_array('custom',$INCO_option)) echo 'checked';?> ><?= lang('custom');?>-->
                            <!--        <input type='checkbox' name='INCO_option[]' value='turcking' style='vertical-align:middle;' <?php if(in_array('turcking',$INCO_option)) echo 'checked';?>><?= lang('turcking');?>-->
                            <!--        <input type='checkbox' name='INCO_option[]' value='warehouse' style='vertical-align:middle;' <?php if(in_array('warehouse',$INCO_option)) echo 'checked';?>><?= lang('warehouse');?>-->
                            <!--    </td>-->
                            <!--</tr>-->
                            <!--<tr class="INCO_term_option">-->
                            <tr class="">
                                <td><?= lang('INCO_address');?></td>
                                <td>
                                    <textarea name="INCO_address" id="INCO_address" cols="30" style="height: 60px;width: 248px;"><?= $INCO_address;?></textarea>
                                </td>
                            </tr>
                            <!--                            <tr>
                                <td><?/*= lang('update_issue_date'); */?></td>
                                <td>
                                    <input class="easyui-datebox" name="issue_date" id="issue_date" style="width:255px;"
                                           value="<?php /*echo $issue_date; */?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td><?/*= lang('update_expiry_date'); */?></td>
                                <td>
                                    <input class="easyui-datebox" name="expiry_date" id="expiry_date" style="width:255px;"
                                           value="<?php /*echo $expiry_date; */?>"/>
                                </td>
                            </tr>-->
                            <tr>
                                <td><?= lang('requirements');?></td>
                                <td>
                                    <textarea name="requirements" id="requirements" style="height:60px;width:248px;"><?php echo $requirements;?></textarea><br>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    --------------------------------------------------------------------------
                                </td>
                            </tr>
                            <?php
                            foreach ($userList as $key => $value) {
                                $group_key = $key . '_group';
                                if($key == 'oversea_cus')continue;
                                ?>
                                <tr>
                                    <td><?php echo lang($key); ?><?=$key!='customer_service'?"<span style='color: red;font-size: 14px'>*</span>":''?></td>
                                    <td>
                                        <select class="easyui-combobox userList" id="<?php echo $key; ?>_id" editable="false" name="<?php echo $key; ?>_id" style="width:255px;"
                                                data-options="
                                        valueField:'id',
                                        textField:'name',
                                        url:'/bsc_user/get_client_user?client_code=<?= $client_code;?>&role=<?php echo $key; ?>&sales_id=<?= $sales_id;?>',
                                        value:'<?php echo $value;?>',
                                        onSelect: function(rec){
                                            $('#<?= $group_key;?>').val(rec.group);
                                            <?php if($key == 'sales'){ ?>
        									    //如果是销售,选择同时还刷新其他的下拉框
        									    var client_code = $('#client_code').textbox('getValue');
        									    reload_user_by_sales(client_code, rec.id);
                                            <?php } ?>
                                        },
                                        onLoadSuccess:function(){
                                            var val = $(this).combobox('getValue');
                                            var data = $(this).combobox('getData');
                                            var reset = true;
                                            if(val == 0 && data.length == 2){
                                                <?php if($key == 'sales'){ ?>
                                                var client_code = $('#client_code').textbox('getValue');
                                                reload_user_by_sales(client_code, data[1].id);
                                                <?php } ?>
                                                $(this).combobox('setValue', data[1].id);
                                                $('#<?= $group_key;?>').val(data[1].group);
                                            }else{
                                                $.each(data, function(index,item){
                                                    if(item.id == val){
                                                        reset = false;
                                                    }
                                                });
                                                if(reset){
                                                    $(this).combobox('setValue', 0);
                                                     $('#<?= $group_key;?>').val('');
                                                }
                                            }
                                        },
                                        onHidePanel: function() {
    										var valueField = $(this).combobox('options').valueField;
    										var val = $(this).combobox('getValue');
    										var allData = $(this).combobox('getData');
    										var result = true;
    										for (var i = 0; i < allData.length; i++) {
    											if (val == allData[i][valueField]) {
    												result = false;
    											}
    										}
    										if (result) {
    											$(this).combobox('clear');
    										}
        								},
                                        ">
                                        </select>
                                        <input type="hidden" id="<?php echo $key;?>_group" name="<?php echo $key;?>_group" style="width:255px;" value="<?php echo $$group_key;?>">
                                </tr>
                                <?php
                            }
                            ?>

                        </table>
                    </div>
                    <div style="float: left;width: 400px;">
                        <table width="400" border="0" cellspacing="0">
                            <tr>
                                <td><?= lang('update_description'); ?><span style="color: red;font-size: 14px">*</span> </td>
                                <td>
                                    <textarea name="description" id="description" style="height:60px;width:245px;"/><?php echo $description; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('description_cn'); ?> </td>
                                <td>
                                    <textarea name="description_cn" id="description_cn" style="height:60px;width:245px;"/><?php echo $description_cn; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_mark_nums'); ?> </td>
                                <td>
                                    <textarea name="mark_nums" id="mark_nums" style="height:60px;width:245px;"/><?php echo $mark_nums; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('remark'); ?> </td>
                                <td>
                                    <textarea name="remark" id="remark" style="height:60px;width:245px;"/><?php echo $remark; ?></textarea>
                                </td>
                            </tr>
                            <!--<tr>
                                <td><?/*= lang('good_describe');*/?> </td>
                                <td>
                                    <textarea name="good_describe" id="good_describe" style="height:60px;width:245px;"/><?php /*echo $good_describe;*/?></textarea>
                                </td>
                            </tr>-->
                            <tr>
                                <td><?= lang('货物件数'); ?> <span style="color: red;font-size: 14px">*</span></td>
                                <td>
                                    <input class="easyui-textbox" name="good_outers" id="good_outers" style="width:100px;"
                                           value="<?php echo $good_outers; ?>"/>
                                     
                                    <select name="good_outers_unit" id="good_outers_unit" class="easyui-combobox" style="width:155px;" data-options="
                                        valueField:'name',
                                        textField:'name',
                                        value:'<?= $good_outers_unit;?>',
                                        url: '/bsc_dict/get_option/packing_unit',
                                        onHidePanel: function() {
                                            var valueField = $(this).combobox('options').valueField;
                                            var val = $(this).combobox('getValue');
                                            var allData = $(this).combobox('getData');
                                            var result = true;
                                            for (var i = 0; i < allData.length; i++) {
                                                if (val == allData[i][valueField]) {
                                                    result = false;
                                                }
                                            }
                                            if (result) {
                                                $(this).combobox('clear');
                                            }
                                        },
                                        onSelect: function (data) {
                                            var good_outers = $('#good_outers').val();
                                            var all = $('#good_outers_unit').combobox('getData');
                                            var name1 = data.name;
                                            var name2 = data.name;
                                            $.each(all,function(index,item){
                                                   if(data.value == item.value && data.name != item.name){
                                                        name2 = item.name;
                                                   }
                                            })
                                            var len1 = name1.length;
                                            var len2 = name2.length;
                                            if(good_outers > 1){
                                                if(len2>len1){
                                                    $('#good_outers_unit').combobox('setValue', name2);
                                                }
                                            }else{
                                                if(len2<len1){
                                                    $('#good_outers_unit').combobox('setValue', name2);
                                                }
                                            }
                                        }
                                    ">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('货物重量'); ?> <span style="color: red;font-size: 14px">*</span></td>
                                <td>
                                    <input class="easyui-textbox" name="good_weight" id="good_weight" style="width:100px;"
                                           value="<?php echo $good_weight; ?>"/>
                                    <select readonly name="good_weight_unit" id="good_weight_unit" class="easyui-combobox"style="width:155px;">
                                        <option value="KGS">KGS</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('货物体积'); ?> <span style="color: red;font-size: 14px">*</span></td>
                                <td>
                                    <input class="easyui-textbox" name="good_volume" id="good_volume" style="width:100px;"
                                           value="<?php echo $good_volume; ?>"/>
                                    <select readonly name="good_volume_unit" id="good_volume_unit" class="easyui-combobox"style="width:155px;">
                                        <option value="CBM">CBM</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('货物类型');?><span style="color: red;font-size: 14px">*</span></td>
                                <td>
                                    <select class="easyui-combobox" name="goods_type" id="goods_type" style="width: 255px;" data-options="
                                    valueField:'value',
                                    textField:'valuename',
                                    value:'<?= $goods_type;?>',
                                    url:'/bsc_dict/get_option/goods_type',
                                    onSelect:function(rec){
                                        dangerous_info(rec.value);
                                    },
                                ">

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('hs_code');?> </td>
                                <td>
                                    <input class="easyui-textbox" name="hs_code" id="hs_code"  style="width:255px;" value="<?php echo $hs_code;?>" prompt="<?= lang('Please separate them with commas');?>" />
                                    <span id="hs_code_tip" style="color: red"></span>
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('isdangerous');?></td>
                                <td>
                                    <input class="easyui-textbox" id="isdangerous" name="isdangerous" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('ism_spec');?></td>
                                <td>
                                    <input class="easyui-textbox" id="ism_spec" name="ism_spec" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('UN_no');?></td>
                                <td>
                                    <input class="easyui-textbox" id="UN_no" name="UN_no" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('IMDG_page');?></td>
                                <td>
                                    <input class="easyui-textbox" id="IMDG_page" name="IMDG_page" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('packing_type');?></td>
                                <td>
                                    <input class="easyui-textbox" id="packing_type" name="packing_type" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('flash_point');?></td>
                                <td>
                                    <input class="easyui-textbox" id="flash_point" name="flash_point" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('IMDG_code');?></td>
                                <td>
                                    <input class="easyui-textbox" id="IMDG_code" name="IMDG_code" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('is_pollution_sea');?></td>
                                <td>
                                    <input class="easyui-textbox" id="is_pollution_sea" name="is_pollution_sea" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('ems_no');?></td>
                                <td>
                                    <input class="easyui-textbox" id="ems_no" name="ems_no" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('MFAG_NO');?></td>
                                <td>
                                    <input class="easyui-textbox" id="MFAG_NO" name="MFAG_NO" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('e_contact_name');?></td>
                                <td>
                                    <input class="easyui-textbox" id="e_contact_name" name="e_contact_name" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('e_contact_tel');?></td>
                                <td>
                                    <input class="easyui-textbox" id="e_contact_tel" name="e_contact_tel" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="special_size">
                                <td><?= lang('long');?></td>
                                <td>
                                    <input class="easyui-textbox" id="long" name="long" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="special_size">
                                <td><?= lang('wide');?></td>
                                <td>
                                    <input class="easyui-textbox" id="wide" name="wide" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="special_size">
                                <td><?= lang('high');?></td>
                                <td>
                                    <input class="easyui-textbox" id="high" name="high" style="width: 255px;">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br>

                </div>
            </div>
        </form>
    </div>
</div>

<div id="trans_origin_div">
    <form id="trans_origin_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_origin_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_origin_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_origin_click" iconCls="icon-search" onclick="query_report('trans_origin', 'trans_origin')"><?= lang('search');?></a>
        </div>
    </form>
</div>
<div id="trans_discharge_div">
    <form id="trans_discharge_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_discharge_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_discharge_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_discharge_click" iconCls="icon-search" onclick="query_report('trans_discharge', 'trans_discharge')"><?= lang('search');?></a>
        </div>
    </form>
</div>
<div id="trans_destination_div">
    <form id="trans_destination_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <label><?= lang('码头');?>:</label><input name="terminal" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_destination_click" iconCls="icon-search" onclick="query_report('trans_destination', 'trans_destination')"><?= lang('search');?></a>
        </div>
    </form>
</div>
</body>
