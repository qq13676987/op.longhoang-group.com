<?php
   class Biz_client_model extends CI_Model{
       private $table = 'biz_client';

       public function __construct(){
            $this->load->database();
        }

        public $field_all = array(
           array("id", "ID", "80", "1"),
           array("client_code", "client_code", "100", "2"),
           array("client_name", "client_name", "100", "2"),
           array("company_name", "company_name", "120", "3"),
           array("company_name_en", "company_name_en", "120", "3"),
           // array("sales_ids", "sales_ids", "100", "11"),
           array("customer_service_ids", "customer_service_ids", "100", "11"),
           array("province", "province", "0", "3"),
           array("city", "city", "0", "3"),
           array("area", "area", "0", "3"),
           array("sailing_area", "sailing_area", "120", "3"),
           array("company_address", "company_address", "120", "4"),
           array("contact_name", "contact_name", "120", "5"),
           array("contact_live_chat", "contact_live_chat", "120", "6"),
           array("contact_telephone", "contact_telephone", "120", "0"),
           array("contact_telephone2", "contact_telephone2", "120", "0"),
           array("contact_email", "contact_email", "120", "0"),
           array("contact_position", "position", "120", "6"),
           array("finance_payment", "finance_payment", "120", "6"),
           array("finance_od_basis", "finance_od_basis", "120", "6"),
           array("finance_payment_month", "finance_payment_month", "120", "6", 'finance_payment_month_day'),
           array("finance_payment_days", "finance_payment_days", "120", "6"),
           array("finance_payment_day_th", "finance_payment_day_th", "120", "6", 'finance_payment_month_day'),
           array("credit_amount", "credit_amount", "120", "6"),
           array("finance_status", "finance_status", "120", "6"),
           array("finance_remark", "finance_remark", "120", "6"),
           array("finance_sign_company_code", "finance_sign_company_code", "120", "6"),
			array("finance_belong_company_code", "finance_belong_company_code", "120", "6"),
           array("finance_free_overdue_date", "finance_free_overdue_date", "0", "6"),
           array("bank_name_cny", "bank_name_cny", "120", "6"),
           array("bank_account_cny", "bank_account_cny", "120", "6"),
           array("bank_address_cny", "bank_address_cny", "120", "6"),
           array("bank_swift_code_cny", "bank_swift_code_cny", "120", "6"),
           array("bank_name_usd", "bank_name_usd", "120", "6"),
           array("bank_account_usd", "bank_account_usd", "120", "6"),
           array("bank_address_usd", "bank_address_usd", "120", "6"),
           array("bank_swift_code_usd", "bank_swift_code_usd", "120", "6"),
           array("taxpayer_name", "taxpayer_name", "120", "6"),
           array("taxpayer_id", "taxpayer_id", "120", "6"),
           array("taxpayer_address", "taxpayer_address", "120", "6"),
           array("taxpayer_telephone", "taxpayer_telephone", "120", "6"),
           array("deliver_info", "deliver_info", "120", "6"),
           array("website_url", "website_url", "120", "6"),
           array("source_from", "source_from", "120", "6"),
           array("role", "role", "120", "6"),
           array("remark", "remark", "120", "6"),
           array("sales_id", "sales_id", "0", "6"),
           array("read_user_group", "read_user_group", "0", "6"),
           array("created_by", "created_by", "150", "10"),
           array("created_time", "created_time", "150", "10"),
           array("updated_by", "updated_by", "150", "10"),
           array("updated_time", "updated_time", "150", "11"),
           array("client_level", "client_level", "0", "11"),
           array("tag", "tag", "0", "11"),
           array("country_code", "country_code", "0", "11"),
           array("invoice_email", "invoice_email", "0", "11"),
           array("stop_date", "stop_date", "0", "11"),
           array("dn_accounts", "dn_accounts", "0", "11"),
           array("dn_title", "dn_title", "0", "11"),
           array("dn_address", "dn_address", "0", "11"),
           array("apply_remark", "apply_remark", "0", "11"),
           array("approve_user", "approve_user", "0", "11"),
           array("apply_type", "apply_type", "0", "11"),
           array("temp_stop_date", "temp_stop_date", "0", "11"),
           array("export_sailing", "export_sailing", "0", "11"),
           array("trans_mode", "trans_mode", "0", "11"),
           array("product_type", "product_type", "0", "11"),
           array("product_details", "product_details", "0", "11"),
           array("client_source", "client_source", "0", "11"),
           array("client_level_remark", "client_level_remark", "0", "11"),
           array("client_level_remark_id", "client_level_remark_id", "0", "11"),
       );

        public function get_by_id($id=0){
                $this->db->where('id', $id);
                $query = $this->db->get($this->table);
                return $query->row_array();
        }
        public function get_one($f="",$v=""){
            $this->db->where($f, $v);
            $this->db->limit(1);
            $query = $this->db->get($this->table);
            return $query->row_array();
        }
        public function get_where_one($where = ''){
           $this->db->where($where);
           $query = $this->db->get($this->table);
           return $query->row_array();
        }

        public function get($where="" ,$sort="id",$order="desc"){
			if($where !="")	$this->db->where($where, NULL, false);
            $this->db->order_by($sort,$order);
            // $role_where = $this->read_role();
            // if($role_where != ''){
                // $this->db->where($role_where, NULL, false);
            // }
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

       public function total($where=""){
           if($where !="")	$this->db->where($where, NULL, false);
        //   $role_where = $this->read_role();
        //   if($role_where != ''){
            //   $this->db->where($role_where, NULL, false);
        //   }
           $this->db->from($this->table);
           return  $this->db->count_all_results('');
       }

        public function no_role_get($where="" ,$sort="id",$order="desc"){
            if($where !="")	$this->db->where($where, null, false);
            $this->db->order_by($sort,$order, true);
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

        public function no_role_total($where=""){
            if($where !="")	$this->db->where($where, null, false);
            $this->db->from($this->table);
            return  $this->db->count_all_results('');
        }

        public function get_last_code($prefix){
            $sql = "select * from biz_client WHERE client_code REGEXP '^" . $prefix . "[0-9]{2}$' ORDER BY client_code desc";
            $query = $this->db->query($sql);
            return $query->row_array();
        }

        public function save($data=array()){
            $data["created_time"] = date('y-m-d H:i:s',time());
            $data["updated_time"] = date('y-m-d H:i:s',time());
            $data["created_by"] =  $this->session->userdata('id');
            $data["updated_by"] =  $this->session->userdata('id');
            $data["created_group"] = implode(',', $this->session->userdata('group') );

            $this->db->insert($this->table, $data);
            return $this->db->insert_id();
        }

        public function update($id='',$data=array()){
				$data["updated_by"] =  $this->session->userdata('id');
                $this->db->where('id', $id);
                $this->db->update($this->table, $data);
				return $id;
        }

        public function mdelete($id='')
        {
            // delete company
            $this->db->where('id', $id);
            $this->db->delete($this->table);
        }

       /**
        * 这个是用于日常查询等使用, group的话,为了速度可以选择另外个 get_by_duty_group 查询
        * 使用该方法时,记得前面都加上distinct
        * @param string $where
        * @param string $sort
        * @param string $order
        * @return mixed
        */
        public function get_by_duty($where="" ,$sort="biz_client.id",$order="desc", $join_more = ""){
            if($where !="")	$this->db->where($where, NULL, false);

            $this->db->order_by($sort,$order, true);
            $this->db->join("biz_client_duty", "biz_client.client_code = biz_client_duty.client_code " . $join_more, "left");
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

       /**
        *
        * @param string $where
        * @return mixed
        */
       public function total_by_duty($where="", $join_more = ""){
           if($where !="")	$this->db->where($where, NULL, false);

           $this->db->select('count(DISTINCT biz_client.client_code) as count', false);
           $this->db->join("biz_client_duty", "biz_client.client_code = biz_client_duty.client_code " . $join_more, "left");
           $query = $this->db->get($this->table);
           return $query->row_array()['count'];
       }

       /**
        * 新版根据biz_client_duty 整理需要的where条件的函数
        * @param $is_str bool
        * @return
        */
        public function read_role_version2($is_str = true){
            $where = array();
            if(!is_admin()){
                $userId = get_session('id');
                $group = join(',', get_session('group'));
                //获取当前拥有的组查看权限
                $rangeGroup = explode(',', get_session('group_range'));
                $rangeGroup[] = -1;
                $rangeGroup = filter_unique_array($rangeGroup);//去重复和空值

                //获取当前拥有的个人查看权限
                $rangeUser = get_session('user_range');
                $rangeUser[] = $userId;
                $rangeUser[] = -1;//-1是全开放
                $rangeUser = filter_unique_array($rangeUser);//去重复和空值

                //进行where条件的拼接
                $where[] = "biz_client_duty.user_id in (" . join(',', $rangeUser) . ")";
                $where[] = "biz_client_duty.user_group in ('" . join('\',\'', $rangeGroup) . "')";
            }
            //如果不穿字符串,就传数组过去
            if(!$is_str) return $where;
            if(empty($where)) return "";
            return '(' . join(' or ', $where) . ')';
        }

       /**
        * 获取查询权限的where
        * @param array $where_array
        * @param bool $is_str
        * @return array|string
        * 2022-10-25 这个版本数据量大时可能会出现速度的问题,到时候再想办法优化了
        */
        public function get_read_role_where($where_array = array(), $is_str = true){
            $result_where = array();
            $where_array = filter_unique_array($where_array);
            foreach ($where_array as $where){
//                $result_where[] = "client_code in (SELECT biz_client.client_code FROM `biz_client` INNER JOIN `biz_client_duty` ON `biz_client`.`client_code` = `biz_client_duty`.`client_code` and {$where} GROUP BY biz_client.client_code)";
                $result_where[] = "client_code in (SELECT biz_client_duty.client_code FROM biz_client_duty WHERE 1 = 1 and {$where} GROUP BY biz_client_duty.client_code)";
            }
            //如果不穿字符串,就传数组过去
            if(!$is_str) return $result_where;
            if(empty($result_where)) return "1 = 1";
            return '(' . join(' and ', $result_where) . ')';
        }

        /**
        * 根据传过来的数据获取客户信息
        * @param $data array 传过来的数据
        * @param $data_field array  从$data中需要取出的客户ID相关的字段
        * @param $get_field array 需要返回的用户字段
        * @param $return_key string 返回时的键名,不填写就返回原样
        */
        public function getClientsByData($data = array(), $data_field = array(), $get_field = array('client_code', 'client_name'), $return_key = ''){
            //首先循环datafield来填充userIds
            $clientCodes = array();
            if(is_string($data_field)) $data_field = array($data_field);
            foreach ($data_field as $field){
                $clientCodes = array_merge($clientCodes, array_column($data, $field));
            }

            //合并数据完成去重去空
            $clientCodes = filter_unique_array($clientCodes);
            if(empty($clientCodes)) return array();

            //查询用户信息,然后根据某个字段返回,为空就原样返回
            if(!in_array($return_key, $get_field)) $get_field[] = $return_key;
            $this->db->select($get_field);
            $rows = $this->no_role_get("client_code in ('" . join('\',\'', $clientCodes) . "')");

            if($return_key == '') return $rows;
            else return array_column($rows, null, $return_key);
        }
   }
