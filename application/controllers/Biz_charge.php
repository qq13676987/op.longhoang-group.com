<?php


class biz_charge extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_charge_model');
        $this->model = $this->biz_charge_model;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "insert_time") continue;
            array_push($this->field_edit, $v);
        }
    }


    public $field_edit = array();

    public $field_all = array(
        array('id', 'id', '80', '1', 'sortable="true"'),
        array('charge_code', 'charge_code', '100', '3', 'editor="textbox"'),
        array('charge_name', 'charge_name_English', '300', '4', 'editor="textbox"'),
        array('charge_name_cn', 'charge_name_Local', '200', '5', 'editor="textbox"'),
        array('explain', 'explain', '100', '6', 'editor="textbox"'),
        array('vat', 'vat', '100', '6', 'editor="textbox"'),
        array('insert_time', 'insert_time', '100', '6', 'sortable="true"'),
    );

    public function get_option()
    {
        $this->db->select('charge_code,charge_name_cn,explain,vat');
        $rs = $this->model->get_option();
        $rows = array();
        foreach ($rs as $row){
	    //2022-08-18 由于有人使用服务费做账,这里将这2个费用代码屏蔽,避免做账出现问题
            if(in_array($row['charge_code'], array('FWF','FWFS'))) continue;
            $row['charge_name_long'] = $row['charge_code'] . '(' . lang($row['charge_name_cn']) . ')';
            array_push($rows, $row);
        }
        echo json_encode($rows);
    }
    
    /**
     * 获取特殊的费用代码
     */
    public function get_option_ts(){
        $type = getValue('type', '');

        $data = array();
        if($type === 'consol_si'){//用在截单管理的biiling里
            //consol_si的话只获取
            $charge_code = array('CDF', 'VGM', 'WJF2');

            $where = array();

            $where[] = "charge_code in ('" . join('\',\'', $charge_code) . "')";

            $where_str = join(' and ', $where);

            $this->db->select('charge_code,charge_name_cn');
            $rs = $this->model->get($where_str);

            foreach ($rs as $row){
                $row['charge_name_long'] = $row['charge_code'] . '(' . $row['charge_name_cn'] . ')';

                $data[] = $row;
            }
        }

        return jsonEcho($data);
    }

    public function index()
    {
        if(!is_admin()) {
            // exit ("你不是管理员");
        }
        $data = array();

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_charge_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }

        $this->load->view('head');
        $this->load->view('biz/charge/index_view', $data);
    }

    public function get_data()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

        //-------where -------------------------------
        $where = array();
        if (!empty($fields)) {
            foreach ($fields['f'] as $key => $field) {
                $f = trim($fields['f'][$key]);
                $s = isset($fields['s'][$key]) ? trim($fields['s'][$key]) : '';
                $v = isset($fields['v'][$key]) ? trim($fields['v'][$key]) : '';

                //如果条件不完整就跳过
                if ($f == '' || $s == '' || $v == '') continue;
                $where[] = search_like($f, $s, $v);
            }
        }
        $where = join(' and ', $where);

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $rs = $this->model->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data()
    {
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($temp != "")
                $data[$item] = trim(ts_replace($temp));
        }
        $charge_code = $this->model->get_one('charge_code', $data['charge_code']);
        if(!empty($charge_code)){
            $data['isError'] = true;
            $data['msg'] = lang("费用代码已存在");
            echo json_encode($data);
            return;
        }
        // $charge_code = $this->model->get_one('charge_name_cn', $data['charge_name_cn']);
        // if(!empty($charge_code)){
        //     $data['isError'] = true;
        //     $data['msg'] = lang("中文费用名称已存在");
        //     echo json_encode($data);
        //     return;
        // }
        
        $id = $this->model->save($data);
        $data['id'] = $id;


        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_charge";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        echo json_encode($data);
    }

    public function update_data()
    {
        $id = $_REQUEST["id"];
        $old_row = $this->model->get_one('id', $id);

        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($old_row[$item] != $temp)
                $data[$item] = trim(ts_replace($temp));
        }
        if(isset($data['charge_code'])){
            $charge_code = $this->model->get_one("charge_code = '{$data['charge_code']}' and id != '{$id}'");
            if(!empty($charge_code)){
                $data['isError'] = true;
                $data['msg'] = lang("费用代码已存在");
                echo json_encode($data);
                return;
            }
        }
        if(isset($data['charge_name_cn'])){
            $charge_name_cn = $this->model->get_where_one("charge_name_cn = '{$data['charge_name_cn']}' and id != '{$id}'");
            if(!empty($charge_name_cn)){
                $data['isError'] = true;
                $data['msg'] = lang("中文费用名称已存在");
                echo json_encode($data);
                return;
            }
        }
        $id = $this->model->update($id, $data);
        $data['id'] = $id;

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_charge";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        echo json_encode($data);
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->model->get_one('id', $id);
        $this->model->mdelete($id);

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_charge";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
        echo json_encode(array('success' => true));
    }
}