<?php

class Biz_consol extends Menu_Controller
{
    protected $admin_field = array('operator', 'customer_service', 'marketing', 'oversea_cus', 'booking', 'document');//, 'finance');

    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_consol_model');
        $this->load->model('biz_shipment_model');
        $this->load->model('bsc_user_model');
        $this->load->model('bsc_user_role_model');
        $this->load->model('m_model');
        $this->load->helper('cz_consol');

        $this->field_all = $this->biz_consol_model->field_all;

        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            $this->lock_filed[$row[4]][] = $v;
            array_push($this->field_edit, $v);
        }
        foreach ($this->admin_field as $val) {
            $this->lock_filed[1][] = $val;
        }
    }

    public $lock_filed = array();   //锁字段，锁哪个格子

    public $field_edit = array();

    public function index()
    {
        $data = array();

        $data["hasDelete"] = false;

        $this->load->view('head');
        $this->load->view('biz/consol/index_view', $data);
    }

    /**
     * 新增consol接口
     * @return bool|void
     */
    public function add_data_ajax()
    {
        $shipment_id = isset($_REQUEST["shipment_id"]) ? $_REQUEST["shipment_id"] : "";
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            if(isset($_POST[$item])){
                $temp = $_POST[$item];
                if (is_string($temp)) $temp = ts_replace(trim($temp));

                $data[$item] = $temp;
            }
        }
        //检查大前提，业务类型必须选

        //查询carrier_ref即MBL是否已存在
        if (isset($data['carrier_ref']) && !empty($data['carrier_ref'])) {
            $carrier_ref_isset = $this->biz_consol_model->get_where_one("carrier_ref = '{$data['carrier_ref']}'");
            if (!empty($carrier_ref_isset)) return error(1, lang("MBL已存在于CONSOL: {$carrier_ref_isset['job_no']}"));
        }

        $this->update_company($data);
        $data['sign'] = isset($_POST['sign']) ? join(',', $_POST['sign']) : '';
        $data["job_no"] = '';

        //box_info-start
        $data['box_info'] = postValue('box_info', array());
        box_info_handle($data['box_info']);
        $data['box_info'] = json_encode($data['box_info']);
        //box_info-end
        //free_svr--start
        $data['free_svr'] = postValue('free_svr', array());
        free_svr_handle($data['free_svr']);
        $data['free_svr'] = json_encode($data['free_svr']);
        //free_svr--end

        $data['price_flag'] = 0;
        //如果有shipment_id,判断下shipment是否有订舱服务
        if (!empty($shipment_id)) {
            $this->db->select('service_options');
            $shipment = Model('biz_shipment_model')->get_by_id($shipment_id);
            if (!empty($shipment)) {
                $service_options_json = $shipment['service_options'];
                $service_options = array_column(json_decode($service_options_json, true), 0);

                //没有订舱服务就是 consol只能选择约号订舱 和客户自订舱 2种
                if (!in_array('booking', $service_options)) {
                    if ($data['customer_booking'] != '2' && $data['customer_booking'] != '1') {
                        echo json_encode(array('code' => 1, 'msg' => 'shipment未勾选订舱服务,只能选择约号订舱 和客户自订舱'));
                        return;
                    }
                }else{
                    if ($data['customer_booking'] == '1') return jsonEcho(array('code' => 1, 'msg' => 'shipment已勾选订舱服务, 不能选择客户自订舱'));
                }
            }
        }

        if (!checkToken('consol_add')) return error(1, lang('请勿重复提交,请刷新页面后再试'));
        $id = $this->biz_consol_model->save($data);
        if (!$id) return error(1, lang('新增失败，请联系管理员'));

        $job_no = consol_job_no_rule($id);
        $data['job_no'] = $job_no;
        $this->biz_consol_model->update($id, array('job_no' => $job_no));

        //将consol相关的duty进行处理--start
        $duty = array();
        $duty['biz_table'] = 'biz_consol';
        $duty['id_no'] = $id;

        $this->get_role_id_group($duty);
        $this->load->model('biz_duty_model');
        $this->biz_duty_model->save($duty);
        //将consol相关的duty进行处理--end
        $data['id'] = $id;
        // 如果是有shipment_id 的话,这里自动将shipment绑定到consol上
        if ($shipment_id != "") {
            $new_data = array();
            $new_data["consol_id"] = $id;
            $this->biz_shipment_model->update($shipment_id, $new_data);
            update_bill_ETD('shipment', $shipment_id);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment";
            $log_data["key"] = $shipment_id;
            $log_data['master_table_name'] = "biz_consol";
            $log_data['master_key'] = $id;
            $log_data["action"] = "bind_update";
            $log_data["value"] = json_encode($new_data);
            log_rcd($log_data);
            $this->consol_duty_update($id);
        }
        // 如果是改配的，需要把old consol的shipment，移动到新consol旗下. 关单号清零
        if ($data["gaipei_old_id"] > 0) {
            $gaipei_old_id = $data["gaipei_old_id"];
            $old_consol = $this->biz_consol_model->get_by_id($gaipei_old_id);
            $update_data_old_id = array(
                'status' => 'cancel',
                'cancel_msg' => 2,//2是改配
                'carrier_ref' => $old_consol['carrier_ref'] . '-gaipei',
            );
            $this->biz_consol_model->update($gaipei_old_id, $update_data_old_id);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_consol";
            $log_data["key"] = $gaipei_old_id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($update_data_old_id);
            log_rcd($log_data);

            $this->load->model("m_model");
            $sql = "update biz_shipment set consol_id = $id, cus_no='' where consol_id = {$data['gaipei_old_id']}";
            $this->m_model->query($sql);
            // record the log
            $sql = "select id,cus_no from biz_shipment where consol_id = $id";
            $shipment_rs = $this->m_model->query_array($sql);
            foreach ($shipment_rs as $rr) {
                $new_data = array();
                $new_data["consol_id"] = $id;
                $log_data = array();
                $log_data["table_name"] = "biz_shipment";
                $log_data["key"] = $rr["id"];
                $log_data['master_table_name'] = "biz_consol";
                $log_data['master_key'] = $id;
                $log_data["action"] = "gaipei_update";
                $log_data["value"] = json_encode($new_data);
                log_rcd($log_data);
            }

        }

        consol_status($id);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_consol";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        echo json_encode(array('code' => 0, 'msg' => '新增成功', 'data' => $data));
    }

    public function edit($id = 0)
    {
		$this->load->view('head');
        $consol = $this->biz_consol_model->get_duty_one("id = '{$id}'");
        if (empty($consol)) {
            echo 'consol不存在';
            return;
        }
        $data = $consol;

        pass_role($data, $this->admin_field);
        $userId = get_session('id');
        $userBsc = $this->bsc_user_model->get_by_id($userId);

        $userRole = $this->bsc_user_role_model->get_data_role();
        $userBsc = array_merge($userBsc, $userRole);

        $must_field = array('id', 'job_no', 'created_time', 'created_by', 'updated_by', 'updated_time', 'show_fields');
        $data1 = data_role($data, $must_field, explode(',', $userRole['read_text']),'consol');

        $data1["id"] = $id;
        $data1["sales_id"] = issetGet($data, 'sales_id', 0);

        $data['lock_lv'] = issetGet($data, 'lock_lv', 0);
        $data1['lock_lv'] = $data['lock_lv'];

        $userBscEdit1 = explode(",", (isset($userBsc['edit_text']) ? $userBsc['edit_text'] : ''));
        $userBscEdit = array();
        $lock_field = array();
        for ($i = 1; $i <= $data['lock_lv']; $i++) {
            if (isset($this->lock_filed[$i])) $lock_field = array_merge($lock_field, $this->lock_filed[$i]);
        }
        $g_edit_table = false;
        foreach ($userBscEdit1 as $row) {
            if (strpos($row, 'consol.') !== false) {
                $arr_temp = explode(".", $row);
                if (!in_array($arr_temp[1], $lock_field)) array_push($userBscEdit, $arr_temp[1]);
            } else if ($row == 'consol') {
                // 全表
                $g_edit_table = true;
                break;
            }
        }
        if ($g_edit_table) {
            $userBscEdit = array_diff(array_keys($data), $lock_field);
            //填充入当前相关的人员
            $userBscEdit = array_merge($userBscEdit, $this->admin_field);
        }
        $data1["agent_code"] = $data["agent_code"];

        if ($data['creditor'] === 'XGALQC01' && $data['trans_carrier'] === 'XGALQC01' && !empty($data['agent_ref'])) {//ESL时,承运人和船公司单号不能修改,只能退关
            $diff = array('trans_carrier', 'agent_ref');
            $userBscEdit = checkboxDisabledField($data, $userBscEdit, $diff, '由于订舱代理和承运人是ESL 且代理单号已填写 ,当前字段已锁定');
        }
        //订舱勾上后，指定一些字段不能修改  2021-05-24
        if ($data['dingcangwancheng'] != 0 && $consol['biz_type'] == 'export') {
            $diff = array('booking_ETD', 'cy_close', 'trans_origin', 'trans_discharge', 'trans_destination', 'trans_origin_name', 'trans_discharge_name', 'trans_destination_name', 'trans_carrier', 'sailing_code', 'quotation_id', 'creditor', 'carrier_agent', 'biz_type', 'trans_mode', 'trans_tool', 'payment', 'payment_third');
            $userBscEdit = checkboxDisabledField($data, $userBscEdit, $diff, '由于订舱完成已勾选,当前字段已锁定');
            if($data['trans_carrier']=="JXHY01") $userBscEdit = checkboxDisabledField($data, $userBscEdit, array('carrier_ref','closing_date','carrier_cutoff'), 'KML航运模块锁定');
        }
        //2022-10-17 客户自订舱时, 运价锁住 http://wenda.leagueshipping.com/?/question/1637
        if($data['customer_booking'] === '1'){
            $diff = array('quotation_id');
            $userBscEdit = checkboxDisabledField($data, $userBscEdit, $diff, '由于订舱方式为客户自订舱,当前字段已锁定', $diff);
        }


        //预配勾上后，指定一些字段不能修改  2021-05-24
        //2021-12-08 trans_discharge_code 取消掉了
        //2021-12-17 louzhuang 取消掉了
        if ($data['yupeiyifang'] != 0 && $consol['biz_type'] == 'export') {
            $diff = array('carrier_ref', 'vessel', 'voyage', 'feeder', 'feeder_voyage');
            //2022-10-18 如果没有订舱的时候, 日期不锁
            if($data['booking_id'] !== '0') $diff = array_merge($diff, array('closing_date','carrier_cutoff'));
            $userBscEdit = checkboxDisabledField($data, $userBscEdit, $diff, '由于预配已勾选,当前字段已锁定');
        }

        //截单日期限制改到截单SI上
        //2021-12-29 截关日期加入限制
        //2022-10-09 代理邮箱在截单勾后不能修改
        if ($data['document_si'] != 0 && $consol['biz_type'] == 'export') {
            $diff = array('closing_date', 'carrier_cutoff');
            $userBscEdit = checkboxDisabledField($data, $userBscEdit, $diff, '由于截单SI已勾选,当前字段已锁定');
        }

        //2021-10-22 由于大部分ATD都没有值,且部分情况不会有ATD导致,所以这里锁后临时开启ATD格子,开1个月
        if ($data['lock_lv'] > 0) {
            //2022-04-29 如果实际离泊未填,那么可以修改计划离泊
            if ($data['trans_ATD'] == '0000-00-00 00:00:00') {
                $userBscEdit[] = 'trans_ETD';
                $userBscEdit[] = 'trans_ATD';
            }
            //2022-06-20 C3锁后,如果承运人未填,那么也能修改一次, 由于填写承运人后,会自动清空港口数据,所以改到做账那面
        }
        if ($data['lock_lv'] > 0 && $data['release_type'] == '') {
            $userBscEdit[] = 'release_type';
        }

        $data1['G_userBscEdit'] = $userBscEdit;

        $data1['r_e'] = false;
        if (isset($_POST['edit_text']) || isset($_POST['read_text'])) $data1['r_e'] = true;

        $data1['free_svr'] = json_decode($data1['free_svr'], true);
        $data1['box_info'] = json_decode($data1['box_info'], true);
        //设置页面需要的userList需要修改的话，修改admin_field和field_all里面的对应字段xxx_id和xxx_group
        foreach ($this->admin_field as $val) {
            $data1['userList'][$val] = $data1[$val . '_id'];
        }
        $this->load->model('biz_shipment_model');

        //查询consol的所有shipment
        //consol的页面，如果C3则显示C3锁， 如果关联的shipment也锁了，则显示shipment的锁的进度。比如3个shipment， 分别是S1,S2,S3， 则consol里显示S1， 如果是S2,S2,S3,则显示S2。 依此类推。
        $this->db->select('id, lock_lv,client_code,INCO_term,good_outers,good_weight,good_volume,good_outers_unit,goods_type,service_options');
        $shipments = $this->biz_shipment_model->get_shipment_by_consol($id);
        $data1['shipment_good_outers'] = array_sum(array_column($shipments, 'good_outers'));
        $data1['shipment_good_weight'] = array_sum(array_column($shipments, 'good_weight'));
        $data1['shipment_good_volume'] = array_sum(array_column($shipments, 'good_volume'));
        $data1['shipment_goods_type'] = join(",", array_column($shipments, 'goods_type'));
        $hbl_type = $this->db->where('consol_id',$id)->get('biz_shipment')->row_array();
        $data1['hbl_type'] = empty($hbl_type)?'':$hbl_type['hbl_type'];
        $shipments_lock_lv = array_column($shipments, 'lock_lv');
        $data1['shipment_lock_lv'] = 0;
        if (in_array('0', $shipments_lock_lv)) {
            $data1['shipment_lock_lv'] = 0;
        } else if (in_array('1', $shipments_lock_lv)) {
            $data1['shipment_lock_lv'] = 1;
        } else if (in_array('2', $shipments_lock_lv)) {
            $data1['shipment_lock_lv'] = 2;
        } else if (in_array('3', $shipments_lock_lv)) {
            $data1['shipment_lock_lv'] = 3;
        }
        //将所有shipment的服务选项进行合并
        $service_options_jsons = array_column($shipments, 'service_options');
        $service_options_arr = array();
        foreach ($service_options_jsons as $service_options_json){
            $service_options_json = $service_options_json;
            $service_options = array_column(json_decode($service_options_json, true), 0);
            $service_options_arr = filter_unique_array(array_merge($service_options_arr, $service_options));
        }
        $data1['service_options_arr'] = $service_options_arr;


        //获取当前锁等级的日志
        Model('sys_lock_log_model');
        $this->db->select('id,(select name from bsc_user where bsc_user.id = sys_lock_log.user_id) as user_name,max(op_time) as op_time_max,op_value');
        $this->db->order_by('op_value', 'asc');
        $this->db->group_by('id_type,id_no,op_value');
        $data1['lock_log'] = $this->sys_lock_log_model->get("id_type = 'biz_consol' and id_no = '$id'");

        //必须相等的字段判定
        $this->load->model('biz_shipment_model');
        $shipment_diff = $this->must_eq_field($data);
        $data1['shipment_diff'] = $shipment_diff[0];
        //取
        $data1['booking_error'] = sizeof(array_column($shipment_diff[1], 'booking_error')) > 0 ? true : false;
        $data1['agent_lock'] = sizeof(array_column($shipment_diff[1], 'agent_lock')) > 0 ? true : false;
        $data1['mbl_shipments'] = sizeof(array_column($shipment_diff[1], 'mbl_shipments')) > 0 ? true : false;
        //如果MBL 且多个shipment,那么报错
        if ($data1['mbl_shipments'] == true) {
            $data1['G_userBscEdit'] = checkboxDisabledField($data, $data1['G_userBscEdit'], array('dingcangshenqing','dingcangwancheng', 'yupeiyifang', 'xiangyijingang', 'operator_si', 'matoufangxing', 'chuanyiqihang'), '由于 当前相关shipment存在MBL,且shipment为多个,当前字段已锁定');
        }
        //2021-11-19 如果操作SI勾上后,那么SI相关字段不给修改
        if ($data['operator_si'] != '0' && $consol['biz_type'] == 'export') {
            Model('biz_shipment_si_model');
            $si_field = $this->biz_shipment_si_model->getSiField();

            //非MBL时,description允许修改
            if (!$data1['mbl_shipments'] && $data['document_si'] == '0') {
//                $si_field = array_diff($si_field, array('description'));
            }
            $si_field[] = 'agent_email';
            $si_field[] = 'agent_company';
            $si_field[] = 'agent_address';
            $si_field[] = 'agent_contact';
            $si_field[] = 'agent_telephone';
            $si_field[] = 'agent_code';

//            $data1['G_userBscEdit'] = array_diff($data1['G_userBscEdit'], $si_field);
            $data1['G_userBscEdit'] = checkboxDisabledField($data, $data1['G_userBscEdit'], $si_field, '由于 操作SI 已勾选,当前字段已锁定', $si_field);
        }

        //2022-08-11 有订舱时,让订舱填写,无订舱时,让操作填写
        //2022-08-19 新增截ENS时间字段 http://wenda.leagueshipping.com/?/question/1441
        if ($data['booking_id'] != 0 && //有订舱人员时
            !in_array($data['booking_id'], get_session('user_range')) && // 没有订舱人员权限时
            get_session('id') != $data['marketing_id'] && // 不是当前市场时
            !in_array('marketing', get_session('user_role'))) { //没有市场角色时
            $dingcang_field = array('closing_date', 'carrier_cutoff', 'trans_discharge_code', 'close_ens_date');
            $data1['G_userBscEdit'] = array_diff($data1['G_userBscEdit'], $dingcang_field);
        }

        //获取当前consol的所有shipment的 client_code 对应ID
        $client_codes = array_column($shipments, 'client_code');
        $biz_client_model = Model('biz_client_model');
        $this->db->select('id');
        $client_ids = array_column($biz_client_model->no_role_get("client_code in ('" . join("','", $client_codes) . "')"), 'id');
        $data1['shipment_client_ids'] = join(',', $client_ids);

        //2021-06-07 汪庭彬 新加入 http://wenda.leagueshipping.com/?/question/161
        $shipments_INCO_term = array_column($shipments, 'INCO_term');
        $is_FOB = false;
        if (in_array('FOB', $shipments_INCO_term)) {
            $is_FOB = true;
        }
        $data1['is_fob'] = $is_FOB;

        //2022-03-01 获取consol是否被合并过
        Model('biz_consol_combine_model');
        $combine_log = $this->biz_consol_combine_model->get_where_one("market_consol_id = {$id} and status = 0");
        $data1['is_combine'] = false;
        if (!empty($combine_log)) $data1['is_combine'] = true;

        //2022-07-15 关联shipment
        $shipment=$this->db->where('consol_id',$data1['id'])->get('biz_shipment')->row_array();
        if(empty($shipment)){
            $data1['shipment_arr']=array();
        }else{
            $data1['shipment_arr']=$shipment;

            $data1['agent_company'] =str_replace(array("\r\n","\n","\r")," ",$data['agent_company']);
            $data1['agent_address'] =str_replace(array("\r\n","\n","\r")," ",$data['agent_address']);
            $data1['agent_contact'] =str_replace(array("\r\n","\n","\r")," ",$data['agent_contact']);
            $data1['agent_telephone'] =str_replace(array("\r\n","\n","\r")," ",$data['agent_telephone']);

            $data1['shipment_arr']['shipper_address'] =str_replace(array("\r\n","\n","\r")," ",$shipment['shipper_address']);
            $data1['shipment_arr']['shipper_contact'] =str_replace(array("\r\n","\n","\r")," ",$shipment['shipper_contact']);
            $data1['shipment_arr']['shipper_telephone'] =str_replace(array("\r\n","\n","\r")," ",$shipment['shipper_telephone']);
            $data1['shipment_arr']['shipper_email'] =str_replace(array("\r\n","\n","\r")," ",$shipment['shipper_email']);

            $data1['shipment_arr']['consignee_address'] =str_replace(array("\r\n","\n","\r")," ",$shipment['consignee_address']);
            $data1['shipment_arr']['consignee_contact'] =str_replace(array("\r\n","\n","\r")," ",$shipment['consignee_contact']);
            $data1['shipment_arr']['consignee_telephone'] =str_replace(array("\r\n","\n","\r")," ",$shipment['consignee_telephone']);
            $data1['shipment_arr']['consignee_email'] =str_replace(array("\r\n","\n","\r")," ",$shipment['consignee_email']);

            $data1['shipment_arr']['notify_address'] =str_replace(array("\r\n","\n","\r")," ",$shipment['notify_address']);
            $data1['shipment_arr']['notify_contact'] =str_replace(array("\r\n","\n","\r")," ",$shipment['notify_contact']);
            $data1['shipment_arr']['notify_telephone'] =str_replace(array("\r\n","\n","\r")," ",$shipment['notify_telephone']);
            $data1['shipment_arr']['notify_email'] =str_replace(array("\r\n","\n","\r")," ",$shipment['notify_email']);

            $data1['shipment_arr']['agent_address'] =str_replace(array("\r\n","\n","\r")," ",$shipment['agent_address']);
            $data1['shipment_arr']['agent_contact'] =str_replace(array("\r\n","\n","\r")," ",$shipment['agent_contact']);
            $data1['shipment_arr']['agent_telephone'] =str_replace(array("\r\n","\n","\r")," ",$shipment['agent_telephone']);
        }
		if ($data1['from_db'] != '' && $data1['from_id_no'] && $data1['biz_type'] == 'import') {
			$data1['G_userBscEdit'] = array_diff($data1['G_userBscEdit'], array('biz_type','box_info', 'trans_carrier', 'trans_origin', 'trans_discharge', 'trans_destination', 'trans_destination_terminal','booking_ETD','customer_booking','booking_agent','release_type','hbl_no','hbl_type','sailing_area','trans_term','INCO_term','client_code','client_company','client_address','client_contact','client_telephone','client_email','client_code2','shipper_company','shipper_address','shipper_contact','shipper_telephone','shipper_email','consignee_company','consignee_address','consignee_contact','consignee_telephone','consignee_email','notify_company','notify_address','notify_contact','notify_telephone','notify_email','client2_address','client2_contact','client2_telephone','client2_email','cus_no','trans_destination_name','trans_destination_inner','trans_discharge_name','trans_origin_name','trans_tool','agent_code','agent_company','agent_address','agent_contact','agent_telephone','agent_email','creditor'));
		}
        //2022-06-28 新增tabs
        $data1['tab_title']  = isset($_GET['tab_title']) ? $_GET['tab_title'] : '';
        $this->load->view('biz/consol/edit_ajax_view', $data1);
    }

    public function update_data_ajax($id = 0)
    {
        $old_row = $this->biz_consol_model->get_one('id', $id);

        // if($old_row['lock_lv'] == 3) return jsonEcho(array('code' => 1, 'msg' => 'CONSOL已锁, 无法修改'));
        $field = $this->field_edit;

        $data = array();
        $date_field = array("trans_ATA", "trans_ETA", "trans_ETD", "trans_ATD", "booking_ETD", "closing_date", "carrier_cutoff");
        foreach ($field as $item) {
            if(isset($_POST[$item])){
                $temp = $_POST[$item];
                //数组不处理

                if(is_string($temp)) $temp = ts_replace(trim($temp));//如果是字符串,进行字符替换和去除两边空格

                //值为***的代表没有可读权限
                if($temp !== '***'){
                    //判断日期字段，如果小于2000年就置空
                    if (in_array($item, $date_field) && $temp < "2000-01-01") $temp = null;

                    if($old_row[$item] == $temp) continue;//如果没改值,不处理
                    $data[$item] = $temp;
                }
            }
        }
        //如果有shipment_id,判断下shipment是否有订舱服务
        $this->db->select('id,hbl_type');
        $shipments = Model('biz_shipment_model')->get_shipment_by_consol($id);
        //进行一些数据的过滤 如果某些条件达成时, 会将提交过来的数据过滤掉
        //这里是直接从edit里复制过来的,原本是打算代码里加入一个函数,传入字段名获取对应的 相关字段, 但是算了,后面如果加的很多,再进行封装了. 这里可能参数得改为data + 当前情况名称来进行判断返回了 TODO
        //2021-11-19 如果操作SI勾上后,那么SI相关字段不给修改
        if ($old_row['operator_si'] != '0') {
            Model('biz_shipment_si_model');
            $si_field = $this->biz_shipment_si_model->getSiField();

            //非MBL时,description允许修改
            $hbl_types = array_column($shipments, 'hbl_type');
            if (!in_array('MBL', $hbl_types) && $old_row['document_si'] == '0') {
                $si_field = array_diff($si_field, array('description'));
            }
            $si_field[] = 'agent_email';
            $si_field[] = 'agent_company';
            $si_field[] = 'agent_address';
            $si_field[] = 'agent_contact';
            $si_field[] = 'agent_telephone';
            $si_field[] = 'agent_code';

            //这里将data里的这些相关字段剔除
            foreach ($si_field as $f){
                if(isset($data[$f])){
                    unset($data[$f]);
                }
            }
        }

        //2021-07-22 汪庭彬 退关逻辑优化 http://wenda.leagueshipping.com/?/question/289
        if (isset($data['status']) && $data['status'] == 'cancel') {
            // //如果存在任何账单，则无法退关  20220522 取消该规则
            // Model('biz_bill_model');
            // $bill = $this->biz_bill_model->get_where_one("id_type = 'consol' and id_no = $id");
            // if(!empty($bill)){
            //     echo json_encode(array('code' => 1, 'msg' => '请删除当前CONSOL账单后再退关'));
            //     return;
            // }
            //2021-08-26 张路遥提出consol退关时自动C3 http://wenda.leagueshipping.com/?/question/401
            //2021-09-29 张路遥补充，当有市场的时候自动锁C3
            if ($old_row['lock_lv'] == 0 && $old_row['marketing_id'] != 0) $data['lock_lv'] = 3;
        }
        //查询carrier_ref是否已存在
        if (isset($data['carrier_ref']) && !empty($data['carrier_ref'])) {
            $carrier_ref_isset = $this->biz_consol_model->get_where_one("carrier_ref = '{$data['carrier_ref']}' and id != $id");
            if (!empty($carrier_ref_isset)) {
                echo json_encode(array('code' => 1, 'msg' => 'MBL已存在于CONSOL:' . $carrier_ref_isset['job_no']));
                return;
            }
        }

        if ($old_row['trans_mode'] == 'error') {
            echo json_encode(array('code' => 1, 'msg' => '运输模式错误'));
            return;
        }

        //2021-09-13 张路遥提出 当船名航次被修改时，自动清空 计划靠泊 开港时间 截港时间 实际靠泊 计划离泊 实际离泊
        if (isset($data['vessel']) || isset($data['voyage'])) {
            $data['cy_open'] = '0000-00-00 00:00:00';
            $data['cy_close'] = '0000-00-00 00:00:00';
            $data['trans_ETA'] = '0000-00-00 00:00:00';
            $data['trans_ATA'] = '0000-00-00 00:00:00';
            $data['trans_ETD'] = '0000-00-00 00:00:00';
            $data['trans_ATD'] = '0000-00-00 00:00:00';
        }

        $this->update_company($data, $old_row);

        $data['sign'] = isset($_POST['sign']) ? join(',', $_POST['sign']) : '';
        if ($data['sign'] == $old_row['sign']) unset($data['sign']);

        $data['show_fields'] = isset($_POST['show_fields']) ? join(',', $_POST['show_fields']) : '';
        if ($data['show_fields'] == $old_row['show_fields']) unset($data['show_fields']);

        //box_info处理--start LCL
        $trans_mode = isset($_POST['trans_mode']) ? $_POST['trans_mode'] : $old_row['trans_mode'];
        if ($trans_mode == 'FCL' || $trans_mode == 'LCL') {
            if (isset($_POST['box_info'])) {
                $data['box_info'] = $_POST['box_info'];
                box_info_handle($data['box_info']);
                $data['box_info'] = json_encode($data['box_info']);
            }
        } else {
            $data['box_info'] = json_encode(array());
        }
        if (isset($data['box_info']) && $data['box_info'] == $old_row['box_info']) unset($data['box_info']);
        //box_info处理--end

        //free_svr--start
        if (isset($_POST['free_svr'])) {
            $data['free_svr'] = $_POST['free_svr'];
            free_svr_handle($data['free_svr']);
            $data['free_svr'] = json_encode($data['free_svr']);
            if ($data['free_svr'] == $old_row['free_svr']) unset($data['free_svr']);
        }
        //free_svr--end

        //如果是ESL,MBL直接查询最新的,自动填入--start
        $trans_carrier = isset($data['trans_carrier']) ? $data['trans_carrier'] : $old_row['trans_carrier'];
        $creditor = isset($data['creditor']) ? $data['creditor'] : $old_row['creditor'];
        if ($trans_carrier === 'XGALQC01' && $creditor == 'XGALQC01') {
            $agent_ref = isset($data['agent_ref']) ? $data['agent_ref'] : $old_row['agent_ref'];
            if (empty($agent_ref)) {
                $this->db->order_by('agent_ref', 'desc');
                $last_carrier_ref = $this->biz_consol_model->get_where_one("agent_ref like 'SHNQ%'");
                $data['agent_ref'] = 'SHNQ' . str_pad((int)substr($last_carrier_ref['agent_ref'], 4) + 1, 5, '0', STR_PAD_LEFT);
            }
        }
        //如果是ESL,MBL直接查询最新的,自动填入--end

        //2022-03-23 如果勾的时间晚于截单时间,那么给晚截填入值
        if(!empty($data['closing_date'])){
            if (strtotime($old_row['operator_si']) > strtotime($data['closing_date'])) {
                $data['late_operator_si'] = date('Y-m-d H:i:s');
            } else {
                $data['late_operator_si'] = '0000-00-00 00:00:00';
            }
        }

        // $this->price_flag_handle($id,$data, $quotation_id);
        $this->biz_consol_model->update($id, $data);
        consol_status($id);
        update_bill_ETD('consol', $id);
        //强关联字段--start
        $connect_field = array('booking_ETD');
        $shipment_data = array();
        foreach ($connect_field as $val) {
            if (isset($data[$val])) {
                $shipment_data[$val] = $data[$val];
            }
        }
        //如果有shipment_id,判断下shipment是否有订舱服务
        $this->db->select('job_no,service_options');
        $shipments = Model('biz_shipment_model')->get_shipment_by_consol($id);
        if (!empty($shipments)) {
            $customer_booking = isset($data['customer_booking']) ? $data['customer_booking'] : $old_row['customer_booking'];
            foreach ($shipments as $shipment) {
                $service_options_json = $shipment['service_options'];
                $service_options = array_column(json_decode($service_options_json, true), 0);
                //没有订舱服务就是 consol只能选择约号订舱 和客户自订舱 2种
                if (!in_array('booking', $service_options)) {
                    if ($customer_booking != '2' && $customer_booking != '1') {
                        echo json_encode(array('code' => 1, 'msg' => $shipment['job_no'] . '未勾选订舱服务,只能选择约号订舱 和客户自订舱'));
                        return;
                    }
                }else{
                    if ($customer_booking == '1') return jsonEcho(array('code' => 1, 'msg' => 'shipment已勾选订舱服务, 不能选择客户自订舱'));
                }
            }
        }
        //如果存在shipment强关联字段的修改,那么进行同步修改
        if (!empty($shipment_data)) {
            $this->load->model('biz_shipment_model');
            $shipments = $this->biz_shipment_model->no_role_get("consol_id = '$id'");

            foreach ($shipments as $shipment) {
                $this->biz_shipment_model->update($shipment['id'], $shipment_data);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_shipment";
                $log_data["key"] = $shipment['id'];
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($shipment_data);
                log_rcd($log_data);
            }
        }
        //强关联字段--end

        //将consol相关的duty进行处理--start
        $duty = array();
        $duty['biz_table'] = 'biz_consol';
        foreach ($this->admin_field as $val) {
            if (isset($_POST[$val . '_id'])) {
                $duty[$val . '_id'] = $_POST[$val . '_id'];
                $duty[$val . '_group'] = $_POST[$val . '_group'];
            }
        }
        $this->load->model('biz_duty_model');
        $this->biz_duty_model->update_by_idno($id, $duty);
        if (isset($data['status']) && $data['status'] == 'cancel') cancel_market_mail($id);
        //将consol相关的duty进行处理--end

        if (!empty($data)) {
            $data['id'] = $id;
            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_consol";
            $log_data["key"] = $id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        } else {
            $data['id'] = $id;
        }

        echo json_encode(array('code' => 0, 'msg' => lang('保存成功'), 'data' => $data));
    }

    /**
     * 必填字段验证
     */
    private function must_field_check($data = array()){
        if(isset($data['box_info'])){
            $box_info = json_decode($data['box_info'], true);
            if (empty($box_info)) exit(json_encode(array('code' => 1, 'msg' => lang('箱型箱量不能为空'))));
        }

        //必填字段检查
        $must_field = array("customer_booking", 'booking_ETD', 'trans_tool', 'trans_origin', 'trans_discharge', 'trans_destination', 'trans_mode', 'creditor', 'hs_code', 'trans_carrier','trans_carrier_second', 'trans_mode', 'trans_term', 'release_type');
        foreach ($must_field as $val) {
            if (!isset($data[$val])) exit(json_encode(array('code' => 1, 'msg' => lang($val) . " 必填的")));
        }

        //2022-12-02 张星莹提出 ESL 时 如果没有AP_code 那么不能提交
        if($data['trans_carrier'] == 'XGALQC01'){
            if (!isset($data['AP_code'])) exit(json_encode(array('code' => 1, 'msg' => lang('AP_code') . " 必填的")));

            if (empty($data['AP_code'])) exit(json_encode(array('code' => 1, 'msg' => lang('AP_code') . " 必填的")));
        }

        if (strlen($data["trans_origin"]) != 5) exit(json_encode(array('code' => 1, 'msg' => 'port code is 5 letters')));
        if (strlen($data["trans_discharge"]) != 5) exit(json_encode(array('code' => 1, 'msg' => 'port code is 5 letters')));
        if (strlen($data["trans_destination"]) != 5) exit(json_encode(array('code' => 1, 'msg' => 'port code is 5 letters')));

        if ($data["customer_booking"] == 2) {
            if (empty($data["AP_code"])) exit(json_encode(array('code' => 1, 'msg' => '客户约号订舱，约号必填')));
//            if (empty($data["quotation_id"])) exit(json_encode(array('code' => 1, 'msg' => '客户约号订舱，运价必选')));
        }
        if ($data["customer_booking"] == 0) { //常规订舱
            if (empty($data["payment"])) exit(json_encode(array('code' => 1, 'msg' => '常规订舱，付款条约必填')));
//            if (empty($data["quotation_id"])) exit(json_encode(array('code' => 1, 'msg' => '常规订舱，运价必选')));
//            if (empty($data["sailing_code"])) exit(json_encode(array('code' => 1, 'msg' => '常规订舱，航线代码必填')));
            if (empty($data["marketing_id"])) exit(json_encode(array('code' => 1, 'msg' => '常规订舱，市场岗位必填')));
        }
    }

    public function update_checkbox($id=0){
        if ($id == 0) $id = $_REQUEST["id"];
        $old_row = $this->biz_consol_model->get_duty_one("id = '{$id}'");
        $shipments = $this->biz_shipment_model->get_shipment_by_consol($id);
        if(empty($old_row)){
            echo json_encode(array('code' => 1, 'msg' => 'no consol exist'));
            return;
        }
        if($old_row['lock_lv'] > 0){
            echo json_encode(array('code' => 1, 'msg' => 'locked, no editable! '));
            return;
        }
		$shipment_data = [];
		$pre_alert = postValue("pre_alert");
		if($pre_alert!=""){
			if(strlen($pre_alert)>2){
				if($old_row['agent_code'] == '' || $old_row['agent_company'] == '') exit(json_encode(array('code' => 1, 'msg' => lang('请先保存海外代理'))));
				$shipment_data['pre_alert'] = date('Y-m-d H:i:s');
				$op = $this->load->database('china',true);
				//分公司edi发送
				$dict = $op->where("catalog = 'branch_office' and value = '{$old_row['agent_code']}'")->get('bsc_dict')->row_array();
				if(!empty($dict)){
					$url = "http://{$dict['ext1']}/api/biz_edi_ebooking_consol_insert/?from_db=".get_system_type()."&id_no=$id&key=".md5(date('Y-m-d'));
					$res = file_get_contents($url);
					$res = json_decode($res,true);
					if(empty($res)){
						return jsonEcho(array('code' => 1, 'msg' => lang('EDI send failure. Message: '.$res['msg'])));
					}
					if($res['code']!=1){
						return jsonEcho(array('code' => 1, 'msg' => lang('EDI send failure. Message: '.$res['msg'])));
					}
				}
				//----------------------------------
			}else{
				$shipment_data['pre_alert'] = '0';
			}
		}
		$steps_field = postValue('field','');
		$data[$steps_field] = postValue($steps_field,'0');
		if($steps_field != 'pre_alert') $this->biz_consol_model->update($id, $data);
		$log_data = array();
		$log_data["table_name"] = "biz_consol";
		$log_data["key"] = $id;
		$log_data["action"] = "update";
		$log_data["value"] = json_encode($data);
		log_rcd($log_data);
		if(!empty($shipment_data) && !empty($shipments)){
			$shipment_ids = join(',',array_column($shipments,'id'));
			$this->db->where("id in ({$shipment_ids})")->update('biz_shipment', $shipment_data);
			foreach ($shipments as $v){
				$log_data = array();
				$log_data["table_name"] = "biz_shipment";
				$log_data["key"] = $v['id'];
				$log_data["action"] = "update";
				$log_data["value"] = json_encode($shipment_data);
				log_rcd($log_data);
			}

		}

		echo json_encode(array('code' => 0, 'msg' => 'ok'));die;
        $ext_data = array('is_mbl' => false);
        if (sizeof($shipments) == 1 && $shipments[0]['hbl_type'] == 'MBL') {
            $ext_data['is_mbl'] = true;
        }

        $services = isset($shipments[0]['service_options'])?array_column(json_decode($shipments[0]['service_options'],true),0):array();

        $data = array();
        $dingcangshenqing = postValue("dingcangshenqing");
        $dingcangwancheng = postValue("dingcangwancheng");
        $yupeiyifang = postValue("yupeiyifang");
        $yifangdan = postValue("yifangdan");
        $xiangyijingang = postValue("xiangyijingang");
        $operator_si = postValue("operator_si");
        $vgm_confirmation = postValue("vgm_confirmation");
        $document_si = postValue("document_si");
        $matoufangxing = postValue("matoufangxing");
        $chuanyiqihang = postValue("chuanyiqihang");
        // 监听各个节点并设置逻辑
        if($dingcangshenqing!=""){
            if(strlen($dingcangshenqing)>2){
                $data['dingcangshenqing'] = date('Y-m-d H:i:s');
                if(sizeof($shipments)==0) exit(json_encode(array('code' => 1, 'msg' => 'Please bind shipment')));
                if(empty($shipments[0]['dadanwancheng']))  exit(json_encode(array('code' => 1, 'msg' => '请先到shipment勾选【打单】')));
                if(!in_array("booking",$services))  exit(json_encode(array('code' => 1, 'msg' => '请先到shipment勾选service option【订舱服务】')));
                $this->must_field_check($old_row);
                //发送订舱邮件给订舱人员，待加代码

                //发送订舱邮件给订舱人员
                //获取订舱人员
                if($old_row['booking_id'] !== '0'){
//                    add_template_mail(13, '', getUserField($old_row['booking_id'], 'email'),'', '', 'consol', $id);
                }

                //生成一条通知
                $time = time();
                $date = date('Y-m-d H:i:s', $time);
                $lock_date = date('Y-m-d H:i:s', $time + 4 * 3600);

                //锁屏通知----通知时间任意,但是 lock_screen_datetime 锁屏时间 为4小时后
                // if($old_row['customer_booking'] !== '1'){
                if($old_row['dingcangwancheng'] === '0'){
//                    add_notice("job no： {$old_row['job_no']}， 订舱申请已超过4个小时未处理，请及时处理！", array("/biz_consol/edit/{$id}", "/biz_shipment/index_by_consol/{$id}"), $old_row['booking_id'], '', $date, $date, 'biz_consol', $id, 1, $lock_date, 'dcwc_gx');
                }

                // }
            }else{
                if(strlen($old_row['dingcangwancheng'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【订舱】')));
                $data['dingcangshenqing'] = '0';
            }
        }
        if($dingcangwancheng!=""){
            if(strlen($dingcangwancheng)>2){
                $data['dingcangwancheng'] = date('Y-m-d H:i:s');
                if(sizeof($shipments)>0 && strlen($old_row['dingcangshenqing'])<2)  exit(json_encode(array('code' => 1, 'msg' => '存在shipment，需要先勾选【订舱申请】')));
                //有订舱岗位 就不让操作勾选， 无订舱就允许操作勾选
                if ($old_row['booking_id'] != 0 && $old_row['booking_id'] != get_session('id') && $old_row['operator_id'] == get_session('id')) exit (json_encode(array('code' => 1, 'msg' => '当前票存在订舱人员，操作人员无法勾选')));

                if($old_row['trans_mode'] === 'FCL' && $old_row['biz_type'] == 'export' && $old_row['trans_tool'] == 'SEA'){
                    if (empty($old_row['carrier_agent'])) exit(json_encode(array('code' => 1, 'msg' => '船代必填')));
                }
                if($old_row['trans_carrier']=='JXHY01' && ($old_row['vessel']=='' || $old_row['voyage']=='')) exit(json_encode(array('code' => 1, 'msg' => 'KML的订舱，船名航次必填')));

                if (empty($old_row['trans_carrier'])) exit(json_encode(array('code' => 1, 'msg' => '承运人必填')));

                //发送邮件给申请订舱的人
                //获取订舱人员
                if($old_row['operator_id'] !== '0'){
//                    add_template_mail(14, '', getUserField($old_row['operator_id'], 'email'), '', '', 'consol', $id);
                }
                //锁屏通知----如果勾上后,将通知删除?
//                stop_notice('biz_consol', $id, 'dcwc_gx');
            }else{
                if(strlen($old_row['yupeiyifang'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【预配】')));
                $where = array('id_type'=>'consol', 'id_no'=>$id);
                $count = $this->db->where($where)->from('biz_bill')->count_all_results();
                if($count > 0) exit(json_encode(array('code' => 1, 'msg' => 'delete Billing first!')));
                $data['dingcangwancheng'] = '0';
            }
        }
        if($yupeiyifang!=""){
            if(strlen($yupeiyifang)>2){
                $data['yupeiyifang'] = date('Y-m-d H:i:s');
                if(strlen($old_row['dingcangwancheng'])<2 && in_array("booking",$services))  exit(json_encode(array('code' => 1, 'msg' => '先勾选【订舱】')));
                if($old_row['close_ens_date'] === '0000-00-00 00:00:00')  exit(json_encode(array('code' => 1, 'msg' => 'Please fill ENS date')));
                $this->update_checkbox_yupei($old_row);

//                //根据shipment生成客户中台的任务
//                $closing_date = $old_row["closing_date"];
//                foreach ($shipments as $srow) {
//                    $url = substr(md5("op{$srow['id']}"), 8, 16);
//                    $sql = "update biz_shipment set SI_outer_link ='$url' where id ={$srow['id']}";
//                    $this->m_model->query($sql);
//                    $url = "http://f.bianmachaxun.com/temp/i/$url";
//                    //查询一下是否有任务，有的话更新结束日期
//                    $time = time();
//                    $date = date('Y-m-d H:i:s');
//                    $userId = get_session('id');
//                    $sql = "select id from biz_client_contact_list_task where task_type='SI截单' and id_no = '{$srow['id']}' and id_type = 'shipment'";
//                    $task = $this->m_model->query_one($sql);
//                    if (empty($task)) {
//                        $sql = "INSERT INTO `biz_client_contact_list_task`(`client_code`, `email`, `task_status`, `task_type`, `task_name`, `id_type`, `id_no`, `url`, `start_time`, `close_time`, `created_by`, `created_time`) VALUES ('{$srow['client_code']}', '{$srow['client_email']}', 1, 'SI截单', 'SI截单', 'shipment', '{$srow['id']}', '{$url}', '{$date}', '{$closing_date}', '{$userId}', '{$date}');";
//                        $this->m_model->query($sql);
//                    }
//                    //插入报关资料上传的任务
//                    $sql = "select id from biz_client_contact_list_task where task_type='报关资料上传' and id_no = '{$srow['id']}' and id_type = 'shipment'";
//                    $task = $this->m_model->query_one($sql);
//                    if (empty($task) && strpos($srow["service_options"], "custom_declaration") !== false) {
//                        $url = "http://f.bianmachaxun.com/bsc_upload/upload_files?biz_table=biz_shipment&type=baoguanziliao&id_no={$srow['id']}";
//                        $sql = "INSERT INTO `biz_client_contact_list_task`(`client_code`, `email`, `task_status`, `task_type`, `task_name`, `id_type`, `id_no`, `url`, `start_time`, `close_time`, `created_by`, `created_time`) VALUES ('{$srow['client_code']}', '{$srow['client_email']}', 1, '报关资料上传', '报关资料上传', 'shipment', '{$srow['id']}', '{$url}', '{$date}', '{$closing_date}', '{$userId}', '{$date}');";
//                        $this->m_model->query($sql);
//                    }
//
//                }
            }else{
                $data['quotation_id'] = '0';
                $data['carrier_ref'] = ' ';
                $data['dingcangwancheng'] = '0';
                $data['yupeiyifang'] = '0';
                $data['vessel'] = '';
                $data['voyage'] = '';
                $data['trans_discharge_code'] = '';
                $data['closing_date'] = '0';
                $data['carrier_cutoff'] = '0';
                $data['cy_open'] = '0000-00-00 00:00:00';
                $data['cy_close'] = '0000-00-00 00:00:00';
                $data['trans_ETA'] = '0000-00-00 00:00:00';
                $data['trans_ATA'] = '0000-00-00 00:00:00';
                $data['trans_ETD'] = '0000-00-00 00:00:00';
                $data['trans_ATD'] = '0000-00-00 00:00:00';
                if(strlen($old_row['operator_si'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【操作SI】')));
                foreach($shipments as $shipment){
                    $where = array('id_type'=>'shipment', 'id_no'=>$shipment['id']);
                    $count = $this->db->where($where)->from('biz_bill')->count_all_results();
                    if($count > 0) exit(json_encode(array('code' => 1, 'msg' => '取消“预配”必须删除shipment里的Billing!')));
                }
                $data['yupeiyifang'] = '0';
            }
        }
        if($yifangdan!=""){
            if(strlen($yifangdan)>2){
                $data['yifangdan'] = date('Y-m-d H:i:s');
            }else{
                $data['yifangdan'] = '0';
            }
        }
        if($xiangyijingang!=""){
            if(strlen($xiangyijingang)>2){
                $data['xiangyijingang'] = date('Y-m-d H:i:s');
                if(strlen($old_row['yupeiyifang'])<2)  exit(json_encode(array('code' => 1, 'msg' => '先勾选【预配】')));
            }else{
                if(strlen($old_row['operator_si'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【操作SI】')));
                $data['xiangyijingang'] = '0';
            }
        }
        if($operator_si!=""){
            if(strlen($operator_si)>2){
                $data['operator_si'] = date('Y-m-d H:i:s');
                if(sizeof($shipments)==0) exit(json_encode(array('code' => 1, 'msg' => '请先绑定shipment')));
                if(strlen($old_row['yupeiyifang'])<2)  exit(json_encode(array('code' => 1, 'msg' => '先勾选【预配】')));
                //操作SI只能操作勾,其他人不行

                //邮件自动给截单
                //不是当前操作不允许勾选
                if (!leader_check('operator', $old_row['operator_id'])) exit(json_encode(array('code' => 1, 'msg' => '操作SI只能由当票操作勾选')));
                if(strtotime($old_row['closing_date']) < 1) exit(json_encode(array('code' => 1, 'msg' => '请先填写”截单日期“并保存后再尝试勾选”VGM“选项！')));
                //2021-11-18 6、	操作勾选这个si的时候，先判断shipment是否mbl， 如果mbl，所有数据都带过来
                if (sizeof($shipments) == 1 && $shipments[0]['hbl_type'] == 'MBL') {
                    $ext_data['is_mbl'] = true;
                }

                //2022-03-23 如果勾的时间晚于截单时间,那么给晚截填入值
                if (strtotime($data['operator_si']) > strtotime($old_row['closing_date'])) {
                    $data['late_operator_si'] = date('Y-m-d H:i:s');
                    $ext_data['msg'] = '<span style="color:red;">截单完成,但已晚于系统截单日期,请于对应截单人员确认是否存在晚截费</span>';
                } else {
                    $data['late_operator_si'] = '0000-00-00 00:00:00';
                }

                //发送邮件给截单
                if($old_row['document_id'] !== '0'){
//                    add_template_mail(18, '', getUserField($old_row['document_id'], 'email'), '', '', 'consol', $id);
                }
            }else{
                //不是当前截单报错
                if (!leader_check('document', $old_row['document_id']) && $old_row['document_id'] != 0) exit(json_encode(array('code' => 1, 'msg' => '请联系截单人员取消,若未设置截单人员,请设置后再试')));
                if(strlen($old_row['document_si'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【截单SI】')));

                $data['operator_si'] = '0';

                //邮件自动给操作
                if($old_row['operator_id'] !== '0' && $old_row['document_id'] !== '0'){
                    //取消勾选的时候,发送截单退回给操作
//                    $subject = 'CONSOL: ' . $old_row['job_no'] . '截单被退回';
//                    add_market_mail_table($old_row['operator_id'], 'biz_consol', $id, $subject, $subject, '<a href="/biz_consol/edit/' . $id . '" target="_blank">点此查看</a>', '截单提醒', '', true);

//                    add_template_mail(20, '', getUserField($old_row['operator_id'], 'email'), '', '', 'consol', $id);
                }
            }
        }
        if($vgm_confirmation!=""){
            if(strlen($vgm_confirmation)>2){
                $data['vgm_confirmation'] = date('Y-m-d H:i:s');
                if ((strtotime($old_row['trans_ETA']) - time())<0) exit(json_encode(array('code' => 1, 'msg' => '计划靠泊不能小于今天')));
                if ((strtotime($old_row['trans_ETA']) - time())>3600*48) exit(json_encode(array('code' => 1, 'msg' => '请在计划靠泊前48小时内发送vgm')));
            }else{
                $data['vgm_confirmation'] = '0';
            }
        }
        if($document_si!=""){
            if(strlen($document_si)>2){
                $data['document_si'] = date('Y-m-d H:i:s');
                if(strlen($old_row['operator_si'])<2)  exit(json_encode(array('code' => 1, 'msg' => '先勾选【操作SI】')));
                //邮件自动给操作
                if($old_row['operator_id'] !== '0' && $old_row['document_id'] !== '0'){
//                    add_template_mail(19, '', getUserField($old_row['operator_id'], 'email'), '', '', 'consol', $id);
                }
            }else{
                if(strlen($old_row['chuanyiqihang'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【船开】')));
                $data['document_si'] = '0';
                //2022-10-20 如果shipment存在签发的, 那么这里不能取消勾选
                foreach ($shipments as $shipment){
                    if($shipment['tidanqianfa'] !== '0') exit(json_encode(array('code' => 1, 'msg' => '先取消【签发】, SHIPMENT:' . $shipment['job_no'])));
                }
                //2022-10-19 截单SI勾取消的时候 自动生成改单费标签 http://wenda.leagueshipping.com/?/question/1646
//                $tag_result = add_tag('改单费', '财务类', 'biz_consol', $id);
//                if($tag_result['code'] == 1) exit(json_encode(array('code' => 1, 'msg' => $tag_result['msg'])));
            }
        }
        if($matoufangxing!=""){
            if(strlen($matoufangxing)>2){
                if(empty($old_row['yupeiyifang'])) exit(json_encode(array('code' => 1, 'msg' => '先勾选【预配】')));
                $data['matoufangxing'] = date('Y-m-d H:i:s');
            }else{
                if(strlen($old_row['chuanyiqihang'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【船开】')));
                $data['matoufangxing'] = '0';
            }
        }
        if($chuanyiqihang!=""){
            if(strlen($chuanyiqihang)>2){
//                if(empty($old_row['matoufangxing'])) exit(json_encode(array('code' => 1, 'msg' => '先勾选【码放】')));
                if($old_row['trans_ATD'] == '0000-00-00 00:00:00'){
                    exit(json_encode(array('code' => 1, 'msg' => '请输入 ATD in consol first!')));
                }
                $data['chuanyiqihang'] = date('Y-m-d H:i:s');
            }else{
                if(!empty($consol) && $consol['trans_ATD'] != '0000-00-00 00:00:00'){
                    exit(json_encode(array('code' => 1, 'msg' => '请清空 ATD in consol !')));
                }
                $data['chuanyiqihang'] = '0';
            }
        }
        $this->biz_consol_model->update($id, $data);
        $log_data = array();
        $log_data["table_name"] = "biz_consol";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);


        echo json_encode(array('code' => 0, 'msg' => '提交成功', 'ext_data' => $ext_data));

    }

    public function update_checkbox_yupei($old_row=array()){
        if (empty(trim($old_row['carrier_ref']))) exit(json_encode(array('code' => 1, 'msg' => 'MBL必填')));
        if (strtotime($old_row['closing_date']) < strtotime("2021-01-01")) exit(json_encode(array('code' => 1, 'msg' => '操作截单日期必填')));
        if (strtotime($old_row['carrier_cutoff']) < strtotime("2021-01-01")) exit(json_encode(array('code' => 1, 'msg' => '截关日期必填')));
        if (empty($old_row['vessel'])) exit(json_encode(array('code' => 1, 'msg' => 'vessel必填')));
        if (empty($old_row['voyage']) && $old_row['trans_mode'] !== 'AIR') exit(json_encode(array('code' => 1, 'msg' => 'voyage必填')));
        if (empty($old_row['trans_discharge_code'])) exit(json_encode(array('code' => 1, 'msg' => '卸港代码必填')));
        //有订舱岗位 就不让操作勾选， 无订舱就允许操作勾选
        if ($old_row['booking_id'] != 0 && $old_row['booking_id'] != get_session('id') && $old_row['operator_id'] == get_session('id')) exit(json_encode(array('code' => 1, 'msg' => '当前票存在订舱人员，操作人员无法勾选')));
    }

    /**
     * 根据shipment判断consol类型
     * shipment修改时，根据关联的consol_id来修改对应consol的业务类型
     * 如果未绑定，则在绑定时，修改修改consol的业务类型
     * @param $consol_id
     * @return bool|string
     */
    private function judge_by_shipment($consol_id)
    {//, $trans_mode
        //查询当前关联的consol所有的shipment的trans_mode
        $this->load->model('biz_shipment_model');

        if ($consol_id != 0) {
            $c_s_trans_mode = $this->biz_shipment_model->get_tm_group($consol_id);
        } else {
            return false;
        }

        //提取group查询后的trans_mode
        $trans_mode_array = array_column($c_s_trans_mode, 'trans_mode');

        $consol_trans_mode = $this->trans_mode_handle($trans_mode_array);

        //false代表错误提示
        if ($consol_trans_mode !== false) {
            return $consol_trans_mode;
        } else {
            return false;
        }

    }

    /**
     * 传入由AIR, LCL, FCL任意组成的一维数组判断类型
     * @param $s_trans_mode_array , 例：array('AIR','LCL', 'FCL')
     * @return bool|string
     */
    private function trans_mode_handle($s_trans_mode_array)
    {
        //1、全是FCL 那该consol 就是FCL ,且consol的箱型箱量按shipment的自动计算
        //2、FCL 和LCL 同时存在 则为LCL ,且consol的箱型箱量只可修改，不会计算
        //3、当consol里，只要有AIR时，提示用户shipment出错
        if (sizeof($s_trans_mode_array) == 1) {
            if (in_array('AIR', $s_trans_mode_array)) {
                return 'AIR';
            } else if (in_array('LCL', $s_trans_mode_array)) {
                return 'LCL';
            } else if (in_array('FCL', $s_trans_mode_array)) {
                return 'FCL';
            } else if (in_array('BULK', $s_trans_mode_array)) {
                return 'BULK';
            } else {
                return false;
            }
        } else if (sizeof($s_trans_mode_array) == 2) {
            if (in_array('AIR', $s_trans_mode_array) || in_array('BULK', $s_trans_mode_array)) {
                return 'error';
            } else if (in_array('LCL', $s_trans_mode_array)) {
                return 'LCL';
            } else {
                return false;
            }
        } else if (sizeof($s_trans_mode_array) >= 3) {
            return 'error';
        } else {
            return '';
        }
    }

    /**
     * shipment里必须和consol相等的字段
     * @param $consol
     * @param string $type
     * @return array|string
     */
    protected function must_eq_field($consol, $type = '')
    {
        $consol_id = isset($consol['id']) ? (int)$consol['id'] : null;
        $field = array("biz_type", "trans_carrier", 'trans_origin', 'trans_discharge', 'trans_destination', 'booking_ETD');//goods_type
        //查询consol的所有shipment
        $shipements = $this->biz_shipment_model->get_shipment_by_consol($consol_id);

        //与shipment对比强关联字段是否相等,不相等直接返回错误，并提示错误字段
        $diff_field = array();
        $this->load->model('biz_client_model');

        //2021-06-11 13:44 1、箱型箱量关联去除 1， consol和shipment一样自由编辑，但是原本的关系改为验证，如果不匹配，左上提示不匹配
        $consol_trans_mode = $this->judge_by_shipment($consol['id']);
        $shipments_box_info = array();
        if ($consol_trans_mode !== false && $consol_trans_mode != 'error') {
            foreach ($shipements as $shipement){
                $this_box_info = json_decode($shipement['box_info'], true);
                $shipments_box_info = merge_box_info($shipments_box_info, $this_box_info);
            }
        }

        $consol_box_info = json_decode($consol['box_info'], true);

        $shipements_trans_tool = array_unique(array_column($shipements, 'trans_tool'));
        $shipements_trans_mode = array_unique(array_column($shipements, 'trans_mode'));
        $shipements_hbl_type = array_unique(array_column($shipements, 'hbl_type'));

        $shipements_good_outers = array_column($shipements, 'good_outers');
        $shipements_goods_type = array_column($shipements, 'goods_type');
        $shipements_good_outers_unit = array_column($shipements, 'good_outers_unit');
        $shipements_good_weight = array_column($shipements, 'good_weight');
        $shipements_good_volume = array_column($shipements, 'good_volume');

        //获取consol的箱信息
        Model('biz_container_model');
        $this->db->select('container_no');
        $containers = $this->biz_container_model->get('consol_id = ' . $consol['id']);

        //获取所有shipment的container

        $shipements_id = array_column($shipements, 'id');
        if (!empty($shipements_id)) {
            Model('biz_shipment_container_model');
            $this->db->select('container_no');
            $shipments_containers = $this->biz_shipment_container_model->get("shipment_id in (" . join(',', $shipements_id) . ")");

            foreach ($shipements as $row) {
                foreach ($field as $fval) {
                    if (isset($row[$fval])) {
                        if ($row[$fval] != $consol[$fval]) $diff_field[$row['id']][$fval] = lang($fval);
                    } else {
                        continue;
                    }
                }
                //2021-11-17 如果是MBL,那么shipment超过1个 报警
                if (sizeof($shipements) > 1 && in_array('MBL', $shipements_hbl_type)) {
                    $diff_field[$row['id']]['mbl_shipments'] = 'MBL不能存在多个shipment';
                }
                // if (sizeof(filter_unique_array($shipements_goods_type)) > 1) {
                //     $diff_field[$row['id']]['goods_type'] = '货物类型不一致';
                // }
                //2021-09-14 新增如果shipment全为MBL时，那么consol放单方式必须和shipment相同 http://wenda.leagueshipping.com/?/question/450
                if (in_array('MBL', $shipements_hbl_type) && sizeof($shipements_hbl_type) == 1) {
                    if ($consol['release_type'] != $row['release_type']) {
                        $diff_field[$row['id']]['release_type'] = lang('release_type');
                    }
                }
                //2022-10-26 hscode 改为 存在逗号时 不验证. 从id大于 89118开始检测
                if($consol['id']>89118){
                    if(!strstr($consol['hs_code'], ',') && $row['hs_code'] !== $consol['hs_code']){
                        $diff_field[$row['id']]['hs_code'] = lang('hs_code 和 consol 不一致');
                    }
                }
                //consol的箱号集合必须和shipment相同,不然报错
                if ($consol['document_si'] != '0' && !empty(array_diff(array_column($containers, 'container_no'), filter_unique_array(array_column($shipments_containers, 'container_no'))))) {
                    $diff_field[$row['id']]['container_neq'] = 'consol与shipment箱号校验不一致';
                }
                //新加入 全部为SEA 且LCL时，CONSOL可以为FCL
                //&& (sizeof($shipements_trans_tool) !== 1 || !in_array('SEA', $shipements_trans_tool) || sizeof($shipements_trans_mode) !== 1 || !in_array('LCL', $shipements_trans_mode))
                // if($consol['trans_mode'] != $consol_trans_mode ){
                //     $diff_field[$row['id']]['trans_mode'] = lang('trans_mode');
                // }
                //后续这里添加
                if ($row['hbl_type'] == 'MBL') {
                    $diff_field[$row['id']]['agent_lock'] = lang('hbl_type');
                } else {

                }

                //shipment存在没有勾选订舱服务的，且订舱代理不是客户自订舱，标红高亮订舱
                $service_options = array_column(json_decode($row['service_options'], true), 0);

                if (!in_array('booking', $service_options)) {
//                    if ($consol['creditor'] != '客户自订舱') {
//                        $diff_field[$row['id']]['booking_error'] = lang('creditor');
//                    }
                }

                if ($consol_trans_mode === false || $consol_trans_mode == 'error') {
                    $diff_field[$row['id']]['trans_mode'] = "运输模式出错";
                } else {
                    if ($consol_trans_mode == 'FCL') {
                        $shipments_box_info_size = array_column($shipments_box_info, 'size');
                        array_multisort($shipments_box_info, SORT_ASC, $shipments_box_info_size);

                        $consol_box_info_size = array_column($consol_box_info, 'size');
                        array_multisort($consol_box_info, SORT_ASC, $consol_box_info_size);
                        if ($shipments_box_info != $consol_box_info) {
                            $diff_field[$row['id']]['box_info'] = "箱型箱量之和不等于CONSOL";
                        }
                    }
                }
                //2021-08-19 13:21分 马冬青要求去除下面3个验证
                // if(array_sum($shipements_good_outers) != $consol['good_outers'])$diff_field[$row['id']]['good_outers'] = "件数之和不等于CONSOL";
                // if(strval((array_sum($shipements_good_weight) * 1000)) != strval(($consol['good_weight']  * 1000)))$diff_field[$row['id']]['good_weight'] = "毛重之和不等于CONSOL";
                // if(strval(array_sum($shipements_good_volume) * 1000) != strval(($consol['good_volume']  * 1000)))$diff_field[$row['id']]['good_outers'] = "体积之和不等于CONSOL";
                //2021-08-03 consol件数单位逻辑修改为，如果只有一种，那么consol shipment必须相等，反之consol为PACKAGES
                if (sizeof($shipements_good_outers_unit) == 1) {
                    if ($consol['good_outers_unit'] != $shipements_good_outers_unit[0]) $diff_field[$row['id']]['good_outers_unit'] = lang('good_outers_unit');
                } else {

                }
            }
        }
        //组合页面文字描述
        $result = '';
        $not_result_field = array('agent_lock', 'booking_error', 'mbl_shipments');
        foreach ($diff_field as $key => $val) {
            foreach ($not_result_field as $nv) {
                unset($val[$nv]);
            }
            if (!empty($val)) {
                $result .= 'shipment:' . $key . ', <span style="color:red"> ' . lang('warn') . '</span>:' . join(',', $val) . '<br>';
            }
        }
        return array($result, $diff_field);
    }

    /**
     * 用于处理duty数据的，首次添加的时候
     * @param $duty
     */
    private function get_role_id_group(&$duty)
    {
        $userRole = $this->session->userdata('user_role');
//        $user = $this->bsc_user_model->get_one('id', $this->session->userdata('id'));

        //获取伙伴的id，group数据后，以id为键名
//        $partners = array_column($this->bsc_user_model->get_user_relation_ids($default_partner_ids), null, 'id');
        foreach ($this->admin_field as $val) {
            $mr_id = 0;
            $mr_group = '';
            if (in_array($val, $userRole)) {
                //不为空，且当前权限人员存在时
//                $mr_id = $this->session->userdata('id');
//                $mr_group = join(',', $this->session->userdata('group'));
            }

            $duty[$val . '_id'] = isset($_POST[$val . '_id']) && !empty($_POST[$val . '_id']) ? $_POST[$val . '_id'] : $mr_id;
            $duty[$val . '_group'] = isset($_POST[$val . '_group']) && !empty($_POST[$val . '_group']) ? $_POST[$val . '_group'] : $mr_group;
        }
    }

    /**
     * 根据当前shipment的duty进行更新consol
     * @param $consol_id int
     */
    public function consol_duty_update($consol_id){
        if(empty($consol_id)) return false;
        Model('biz_duty_model');
        $this->db->select('id,user_id,user_group');
        $shipments_duty = $this->biz_duty_model->get("id_type = 'biz_shipment' and id_no in (select id from biz_shipment where consol_id = $consol_id)");
        $consol_duty = array();
        $consol_duty['biz_table'] = 'biz_consol';

        $sales_ids = array();
        $sales_groups = array();
        foreach ($shipments_duty as $shipment_duty){
            if(!in_array($shipment_duty['user_id'], $sales_ids))$sales_ids[] = $shipment_duty['user_id'];
            if(!in_array($shipment_duty['user_group'], $sales_groups))$sales_groups[] = $shipment_duty['user_group'];
        }

        $consol_duty['sales_id'] = join(',', $sales_ids);
        $consol_duty['sales_group'] = join(',', $sales_groups);
        $this->biz_duty_model->update_by_idno($consol_id, $consol_duty);
        //将consol的销售进行更新--end

        //TODO 增加一个 如果consol的当前agent,不存在当前的销售,则直接清空agent
    }

    /**
     * 根据客户代码和对应的company公司数据 更新company信息
     * @param $data
     * @param array $consol
     */
    private function update_company($data, $consol = array())
    {
        //shipper consignee notify 同步--start
        $biz_company_model = Model('biz_company_model');
        //不存在查询下shipment
        if (!isset($data['client_code'])) {
            if (empty($consol['agent_code'])) {
                return;
            }
            $data['agent_code'] = $consol['agent_code'];
        }
        if (!isset($data['shipper_company'])) {
            if (!isset($consol['shipper_company'])) {
                return;
            }
            $data['shipper_company'] = $consol['shipper_company'];
        }
        if (!isset($data['consignee_company'])) {
            if (!isset($consol['consignee_company'])) {
                return;
            }
            $data['consignee_company'] = $consol['consignee_company'];
        }
        if (!isset($data['notify_company'])) {
            if (!isset($consol['notify_company'])) {
                return;
            }
            $data['notify_company'] = $consol['notify_company'];
        }
        $shipper = $biz_company_model->get_where_one("client_code = '{$data['agent_code']}' and company_type = 'shipper' and company_name = '" . es_encode($data['shipper_company'], "'", false) . "'");
        if (!empty($shipper)) {
            //如果有值更新,直接进行同步
            $update_shipper = array();
            if (isset($data['shipper_address']) && $data['shipper_address'] !== $shipper['company_address']) {
                $update_shipper['company_address'] = $data['shipper_address'];
            }
            if (isset($data['shipper_contact']) && $data['shipper_contact'] !== $shipper['company_contact']) {
                $update_shipper['company_contact'] = $data['shipper_contact'];
            }
            if (isset($data['shipper_telephone']) && $data['shipper_telephone'] !== $shipper['company_telephone']) {
                $update_shipper['company_telephone'] = $data['shipper_telephone'];
            }
            if (isset($data['shipper_email']) && $data['shipper_email'] !== $shipper['company_email']) {
                $update_shipper['company_email'] = $data['shipper_email'];
            }
            if (!empty($update_shipper)) {
                $biz_company_model->update($shipper['id'], $update_shipper);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_company";
                $log_data["key"] = $shipper['id'];
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($update_shipper);
                log_rcd($log_data);
            }
        }
        //consignee
        $consignee = $biz_company_model->get_where_one("client_code = '{$data['agent_code']}' and company_type = 'consignee' and company_name = '" . es_encode($data['consignee_company'], "'", false) . "'");
        if (!empty($consignee)) {
            //如果有值更新,直接进行同步
            $update_consignee = array();
            if (isset($data['consignee_address']) && $data['consignee_address'] !== $consignee['company_address']) {
                $update_consignee['company_address'] = $data['consignee_address'];
            }
            if (isset($data['consignee_contact']) && $data['consignee_contact'] !== $consignee['company_contact']) {
                $update_consignee['company_contact'] = $data['consignee_contact'];
            }
            if (isset($data['consignee_telephone']) && $data['consignee_telephone'] !== $consignee['company_telephone']) {
                $update_consignee['company_telephone'] = $data['consignee_telephone'];
            }
            if (isset($data['consignee_email']) && $data['consignee_email'] !== $consignee['company_email']) {
                $update_consignee['company_email'] = $data['consignee_email'];
            }
            if (!empty($update_consignee)) {
                $biz_company_model->update($consignee['id'], $update_consignee);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_company";
                $log_data["key"] = $consignee['id'];
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($update_consignee);
                log_rcd($log_data);
            }
        }
        //notify
        $notify = $biz_company_model->get_where_one("client_code = '{$data['agent_code']}' and company_type = 'notify' and company_name = '" . es_encode($data['notify_company'], "'", false) . "'");
        if (!empty($notify)) {
            //如果有值更新,直接进行同步
            $update_notify = array();
            if (isset($data['notify_address']) && $data['notify_address'] !== $notify['company_address']) {
                $update_notify['company_address'] = $data['notify_address'];
            }
            if (isset($data['notify_contact']) && $data['notify_contact'] !== $notify['company_contact']) {
                $update_notify['company_contact'] = $data['notify_contact'];
            }
            if (isset($data['notify_telephone']) && $data['notify_telephone'] !== $notify['company_telephone']) {
                $update_notify['company_telephone'] = $data['notify_telephone'];
            }
            if (isset($data['notify_email']) && $data['notify_email'] !== $notify['company_email']) {
                $update_notify['company_email'] = $data['notify_email'];
            }
            if (!empty($update_notify)) {
                $biz_company_model->update($notify['id'], $update_notify);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_company";
                $log_data["key"] = $notify['id'];
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($update_notify);
                log_rcd($log_data);
            }
        }
        //shipper consignee notify 同步--end
    }

    //新版的get_data查询
    public function get_data()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $fields = isset($_POST['field']) ? $_POST['field'] : array();

        $where = $this->get_data_where();
        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_consol', 'biz_consol_index');//获取相关的配置信息
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];


                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') $v = '';
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) $title_data['sql_join'][$title_data['base_data'][$title_f]['sql_join']] = $this_join;

                    $where[] = search_like($f, $s, $v);
                }
            }
        }

        $where_str = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $this->db->limit($rows, $offset);
        //获取需要的字段--start
        $must_field = array('id', 'status', 'lock_lv', 'good_outers', 'good_volume', 'good_weight', 'trans_mode', 'biz_type');
        $selects = array_map(function ($r){
            return "biz_consol.{$r} as $r";
        }, $must_field);
        foreach ($title_data['select_field'] as $key => $val){
            if($key == 'biz_consol.job_no'){
                isset($title_data['base_select_field']['biz_consol.goods_type']) && $selects[] = $title_data['base_select_field']['biz_consol.goods_type'];
                isset($title_data['base_select_field']['biz_consol.document_si']) && $selects[] = $title_data['base_select_field']['biz_consol.document_si'];
            }
            if($key == 'biz_consol.trans_origin'){
                isset($title_data['base_select_field']['biz_consol.trans_origin_name']) && $selects[] = $title_data['base_select_field']['biz_consol.trans_origin_name'];
            }
            if($key == 'biz_consol.trans_discharge'){
                isset($title_data['base_select_field']['biz_consol.trans_discharge_name']) && $selects[] = $title_data['base_select_field']['biz_consol.trans_discharge_name'];
            }
            if($key == 'biz_consol.trans_destination'){
                isset($title_data['base_select_field']['biz_consol.trans_destination_name']) && $selects[] = $title_data['base_select_field']['biz_consol.trans_destination_name'];
            }
            if(!in_array($val, $selects)) $selects[] = $val;
        }
        //获取需要的字段--end
        $this->db->select(join(',', $selects), false);
        //这里拼接join数据
        foreach ($title_data['sql_join'] as $j){
            $this->db->join($j[0], $j[1], $j[2]);
        }//将order字段替换为 对应的sql字段
        $sort = $title_data['sql_field']["biz_consol.{$sort}"];
        $this->db->is_reset_select(false);//不重置查询
        $rs = $this->biz_consol_model->get_v3($where_str, $sort, $order);

        $this->db->clear_limit();//清理下limit
        $result["total"] = $this->db->count_all_results('');

        $rows = array();
        //获取每个操作的权限，根据当前用户，进行合并可查看字段
        $userRole = $this->bsc_user_role_model->get_data_role();
        foreach ($rs as $row) {
            $row = data_role($row, $must_field, explode(',', $userRole['read_text']), 'biz_consol', 'biz_consol_index');
            array_push($rows, $row);
        }

        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function get_data_where()
    {
        //-------这个查询条件想修改成通用的 -------------------------------
        $shipment_no = isset($_REQUEST['shipment_no']) ? trim($_REQUEST['shipment_no']) : '';
        $goods_type = isset($_REQUEST['goods_type']) ? trim($_REQUEST['goods_type']) : '';
        $is_hyf = isset($_REQUEST['is_hyf']) ? (int)$_REQUEST['is_hyf'] : '';
        $lock_lv = isset($_REQUEST['lock_lv']) ? $_REQUEST['lock_lv'] : array();
        $container_no = isset($_REQUEST['container_no']) ? trim($_REQUEST['container_no']) : '';
        $is_apply_shipment = isset($_REQUEST['is_apply_shipment']) ? (int)$_REQUEST['is_apply_shipment'] : 0;
        $operator_si = isset($_REQUEST['operator_si']) ? $_REQUEST['operator_si'] : '';
        $document_si = isset($_REQUEST['document_si']) ? $_REQUEST['document_si'] : '';

        $where = array();
        Model('m_model');
        if ($goods_type != '') $where[] = "biz_consol.id in (select consol_id from biz_shipment where biz_shipment.goods_type = '{$goods_type}')";
        if ($is_hyf !== 0) {
            if ($is_hyf === 1) {
                $where[] = "biz_consol.sign like '%hyf_qr%'";
            } else if ($is_hyf === 2) {
                $where[] = "biz_consol.sign not like '%hyf_qr%'";
            }
        }
        if ($operator_si !== '') {
            if ($operator_si === '0') $where[] = "biz_consol.operator_si = '0'";
            if ($operator_si === '1') $where[] = "biz_consol.operator_si != '0'";
        }
        if ($document_si !== '') {
            if ($document_si === '0') $where[] = "biz_consol.document_si = '0'";
            if ($document_si === '1') $where[] = "biz_consol.document_si != '0'";
        }
        if ($shipment_no != "") {
            Model('biz_shipment_model');
            $shipment = $this->biz_shipment_model->get_one_all("job_no like '%$shipment_no%'");
            if (empty($shipment)) $shipment['consol_id'] = 0;
            $where[] = "id = {$shipment['consol_id']}";
        }
        //2021-06-03 加入查询是否申请consol
        if ($is_apply_shipment == 1) {
            $where[] = "(select consol_id_apply from biz_shipment where biz_shipment.consol_id_apply = biz_consol.id limit 1) != 0";
        }

        if ($lock_lv !== array()) {
            $lock_lv_where = array();
            if (in_array(0, $lock_lv)) $lock_lv_where[] = 'biz_consol.lock_lv = 0';//C0
            if (in_array(1, $lock_lv)) $lock_lv_where[] = 'biz_consol.lock_lv = 1';//
            if (in_array(2, $lock_lv)) $lock_lv_where[] = "biz_consol.lock_lv = 2";//
            if (in_array(3, $lock_lv)) $lock_lv_where[] = "biz_consol.lock_lv = 3";//C3
            if (!empty($lock_lv_where)) {
                $where[] = '(' . join(' OR ', $lock_lv_where) . ' )';
            }
        };//查询锁

        //查询箱号
        if ($container_no !== '') {
            Model('biz_container_model');
            $this->db->select('consol_id');
            $containers = $this->biz_container_model->get("container_no like '%$container_no%'");
            if (!empty($containers)) {
                $container_consol_ids = array_column($containers, 'consol_id');
                $where[] = "biz_consol.id in ('" . join("','", $container_consol_ids) . "')";
            } else {
                $where[] = 'biz_consol.id = 0';
            }
        }

        return $where;
    }

    public function get_data_statistics_v2()
    {
        $result = array();
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $fields = isset($_POST['field']) ? $_POST['field'] : array();

        $where = $this->get_data_where();
        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_consol', 'biz_consol_index');//获取相关的配置信息
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];


                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') $v = '';
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) $title_data['sql_join'][$title_data['base_data'][$title_f]['sql_join']] = $this_join;

                    $where[] = search_like($f, $s, $v);
                }
            }
        }

        if(empty($where)) return jsonEcho(array('code' => 1, 'msg' => '请查询后再试'));

        $where_str = join(' and ', $where);
        //-----------------------------------------------------------------

        $this->db->select("sum(biz_consol.good_outers) as good_outers" .
            ", sum(biz_consol.good_volume) as good_volume" .
            ", sum(biz_consol.good_weight) as good_weight" .
            ", group_concat(biz_consol.box_info SEPARATOR ';') as box_info" .
            ",SUM((case when (biz_consol.biz_type='export' and biz_consol.trans_mode = 'LCL') then biz_consol.good_volume else 0 end)) as ex_LCL_good_volume" .
            ",SUM((case when (biz_consol.biz_type='import' and biz_consol.trans_mode = 'LCL') then biz_consol.good_volume else 0 end)) as im_LCL_good_volume" .
            ",SUM((case when (biz_consol.biz_type='export' and biz_consol.trans_mode = 'LCL') then biz_consol.good_weight else 0 end)) as ex_LCL_good_weight" .
            ",SUM((case when (biz_consol.biz_type='import' and biz_consol.trans_mode = 'LCL') then biz_consol.good_weight else 0 end)) as im_LCL_good_weight" .
            ",SUM((case when (biz_consol.biz_type='export' and biz_consol.trans_mode = 'AIR') then biz_consol.good_volume else 0 end)) as ex_AIR_good_volume" .
            ",SUM((case when (biz_consol.biz_type='import' and biz_consol.trans_mode = 'AIR') then biz_consol.good_volume else 0 end)) as im_AIR_good_volume" .
            ",SUM((case when (biz_consol.biz_type='export' and biz_consol.trans_mode = 'AIR') then biz_consol.good_weight else 0 end)) as ex_AIR_good_weight" .
            ",SUM((case when (biz_consol.biz_type='import' and biz_consol.trans_mode = 'AIR') then biz_consol.good_weight else 0 end)) as im_AIR_good_weight" .
            "");
        //这里拼接join数据
        foreach ($title_data['sql_join'] as $j){
            $this->db->join($j[0], $j[1], $j[2]);
        }//将order字段替换为 对应的sql字段
        $sort = $title_data['sql_field']["biz_consol.{$sort}"];
        $rs = $this->biz_consol_model->get_v3($where_str, $sort, $order);

        $rows = array();
        $box_info_sum = array();
        foreach ($rs as $row) {
            //将箱型箱量全部加起来
            $this_box_info = explode(';', $row['box_info']);
            unset($row['box_info']);
            foreach ($this_box_info as $this_box) {
                $this_box_arr = json_decode($this_box, true);
                if(empty($this_box_arr)) $this_box_arr = array();
                $box_info_sum = merge_box_info($box_info_sum, $this_box_arr);
            }
            array_push($rows, $row);
        }
        $rows[0]['teu'] = 0;
        $rows[0]['box_info'] = array();
        foreach ($box_info_sum as $box) {
            $rows[0]['box_info'][] = $box['size'] . "*" . $box['num'];
            if ($box['size'][0] == '2') {
                $rows[0]['teu'] += 1 * $box['num'];
            } else if ($box['size'][0] == '4') {
                $rows[0]['teu'] += 2 * $box['num'];
            }
        }
        $rows[0]['box_info'] = join(',', $rows[0]['box_info']);

        $result['code'] = 0;
        $result['msg'] = '获取成功';
        $result["data"] = $rows[0];
        echo json_encode($result);
    }

    public function add($id = 0)
    {
        //根据不同的type跳转至不同的view 默认为 export_FCL_SEA
        pass_role(array(), $this->admin_field);
        $shipment_id = isset($_REQUEST["shipment_id"]) ? $_REQUEST["shipment_id"] : "";
        $gaipei_old_id = isset($_REQUEST["gaipei_old_id"]) ? $_REQUEST["gaipei_old_id"] : 0;

        //如果新增空白的consol，必须是市场角色
        if ($gaipei_old_id == 0 && $shipment_id == "" && !in_array('marketing', get_session('user_role'))) {
            echo lang('没有权限，请联系市场新增');
            return;
        }

        $field = $this->field_edit;
        //将需要的duty字段加入----start
        foreach ($this->admin_field as $val) {
            $field[] = $val . '_id';
            $field[] = $val . '_group';
        }
        //将需要的duty字段加入----end
        $data = array();
        foreach ($field as $item) {
            $data[$item] = "";
        }

        if ($id != 0) {
            $row = $this->biz_consol_model->get_duty_one("id = '{$id}'");
            if(!empty($row)) $data = $row;
        }
        $data['gaipei_old_id'] = $gaipei_old_id;
        $data['carrier_ref'] = '';  //去除复制过来的MBL
        $data['status'] = 'normal';
        $data['sailing_code'] = '';
        $data["sales_id"] = '';
        //2022-05-09 由于新版港口选取上线,重新选择承运人会导致港口清空重选的问题,这里 将承运人剔除不能复制 trans_carrier
        $empty_fields = array('floor_price', 'price_flag', 'price_flag_msg', 'freight_affirmed', 'pay_buy_flag', 'booking_confirmation', 'AGR_no', 'sailing_code', 'quotation_id', 'customer_booking', 'creditor', 'box_info', 'free_svr', 'marketing_id', 'marketing_group', 'booking_id', 'booking_group', 'document_id', 'document_group');
        foreach ($empty_fields as $tf) {
            if ($tf == 'box_info' || $tf == 'free_svr') {
                $data[$tf] = '{}';
                continue;
            }
            $data[$tf] = null;
        }
        $data['agent_lock'] = false;
        $data['quotation_freight_rate'] = "";
        //2021-07-05 航线代码不复制 http://wenda.leagueshipping.com/?/question/247
        $data['sailing_code'] = '';
        //没有关联的话,默认勾上
        $data['service_options_arr'] = array('booking');
        if ($shipment_id != "") {
            $this->load->model('biz_shipment_model');
            $shipment = $this->biz_shipment_model->get_duty_one("id = '{$shipment_id}'");
            if (empty($shipment)) exit("shipment not exist");
            if (!empty($shipment['consol_id'])) exit("the shipment has been binded by another consol_id " . $shipment["consol_id"]);
             if (empty($shipment['dadanwancheng'])) exit(lang("Please tick the booking order checkbox in top of page."));

            $service_options_json = $shipment['service_options'];
            $service_options = array_column(json_decode($service_options_json, true), 0);
            //没有订舱服务就是 consol只能选择约号订舱 和客户自订舱 2种
            if (!in_array('booking', $service_options)) {
                $data['customer_booking'] = 1;
                if($shipment['hbl_type']!='MBL') exit(lang("无订舱服务时，shipment的提单类型选择MBL，这样不用选海外代理"));
            }
            $data['service_options_arr'] = $service_options;

            $copy_field = array("hs_code", "biz_type", "trans_carrier",  "description", "mark_nums", "trans_mode", "box_info", "status",  "trans_origin", "trans_discharge", "trans_destination", "trans_origin_name", "trans_discharge_name", "trans_destination_name", 'booking_ETD', 'sailing_area', 'AGR_no', 'requirements', 'trans_tool', 'good_outers', 'good_weight', 'good_outers_unit', 'good_volume', 'trans_term', 'release_type', 'operator_id', 'operator_group', 'customer_service_id', 'customer_service_group', 'trans_destination_terminal', 'trans_destination_inner',
                'goods_type'
            );

            foreach ($copy_field as $item) {
                $data[$item] = $shipment[$item];
            }

            //此时还会查询当前shipment是否有相关联的ebooking,如果有,运价和订舱代理  航线代码  市场全部取自那面
            Model('biz_shipment_ebooking_model');
            $this->db->select('quotation_id,free_svr,agent_code,carrier_ref'); //
            $ebooking = $this->biz_shipment_ebooking_model->get_where_one("shipment_id = {$shipment_id}");
            if (!empty($ebooking)) {
                //此刻查询运价
                $data['free_svr'] = $ebooking['free_svr'];
                //代理带过来 --先漏掉吧, 让他们自己选, 不然下面的MBL可能也会贴上去的, 虽然这个都是HBL
                
                $data['carrier_ref'] = $ebooking['carrier_ref'];
            }

            //关于收发通，如果是MBL，则完全copy from shipment， 如果是HBL，则shipper是league 或 Kimxin, consignee是shipment agent。
            if ($shipment['hbl_type'] == 'MBL') {
                $copy_field = array('shipper_company', 'shipper_address','shipper_contact', 'shipper_email', 'shipper_telephone', "consignee_company", "consignee_address", "consignee_contact", "consignee_email", 'consignee_telephone', 'notify_company', 'notify_address', 'notify_contact', 'notify_telephone', 'notify_email');
                foreach ($copy_field as $item) {
                    $data[$item] = $shipment[$item];
                }
                $data['agent_lock'] = true;
            }else{
//                if($shipment['hbl_type'] == 'LEAGUESHIPPING'){
//                    $data['shipper_company'] = "SHANGHAI LEAGUE SHIPPING CO. LTD";
//                    $data['shipper_address'] = "2ND FLOOR,BUILDING A,BLOCK 1, NO.723 TONGXIN ROAD,HONGKOU DISTRICT,SHANGHAI CHINA TEL:021-61406155";
//                    $data['shipper_contact'] = "";
//                    $data['shipper_email'] = "";
//                    $data['shipper_telephone'] = "";
//                }else{
//                    $data['shipper_company'] = "SHANGHAI KIMXIN SUPPLY CHAIN MANAGEMENT CO.,LTD";
//                    $data['shipper_address'] = "ADD.:2 FLOOR,BUILDING A, BLOCK 1,HUAYUAN ROAD NO., 128 HONGKOU ,SHANGHAI ,CHINA";
//                    $data['shipper_contact'] = "MAIL:RYAN_JIN@FOXMAIL.COM";
//                    $data['shipper_email'] = "";
//                    $data['shipper_telephone'] = "TEL: +86-21-61070755";
//                }
            }

            $data["sales_id"] = $shipment['sales_id'];
        }

        $data["shipment_id"] = $shipment_id;
        $data['free_svr'] = json_decode($data['free_svr'], true);
        $data['box_info'] = json_decode($data['box_info'], true);
        foreach ($this->admin_field as $val) {
            $data['userList'][$val] = $data[$val . '_id'];
        }


        $this->load->view('head');
        $this->load->view('biz/consol/add_ajax_view', $data);
    }

    public function get_error_consol($shipment_id = 0)
    {
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
        $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 20;
        $orderfield = isset($_POST['orderfield']) ? $_POST['orderfield'] : 'id';
        $order = isset($_POST['order']) ? $_POST['order'] : 'DESC';
        $fields = isset($_POST['fields']) ? $_POST['fields'] : array();

        $this->db->select('trans_origin, trans_destination');
        $shipment = Model('biz_shipment_model')->get_by_id($shipment_id);
        if (empty($shipment)) {
            echo json_encode(array());
            return;
        }

        $where = array();

        //查询条件目前有承运人和订舱船期
        if (isset($fields['trans_carrier']) && !empty($fields['trans_carrier'])) {
            Model('biz_client_model');
            $this->db->select('client_code');
            $rows = $this->biz_client_model->no_role_get("client_name like '%{$fields['trans_carrier']}%'");
            if (empty($rows)) $rows[0]['client_code'] = '';
            $rs = array_column($rows, 'client_code');
            $where[] = "trans_carrier in ('" . join("','", $rs) . "')";
        }

        if (isset($fields['booking_ETD_start']) && !empty($fields['booking_ETD_start'])) $where[] = "booking_ETD >= '{$fields['booking_ETD_start']}'";
        if (isset($fields['booking_ETD_end']) && !empty($fields['booking_ETD_end'])) $where[] = "booking_ETD <= '{$fields['booking_ETD_end']}'";

        if (isset($fields['job_no']) && !empty($fields['job_no'])) $where[] = "job_no like '%" . trim($fields['job_no']) . "%'";

        //2021-06-03 17:21:30 添加已申请的不显示 汪庭彬
        //2022-06-30 由于可以同时申请多个和审核多个的上限,已申请的不能看到已下线
//        $where[] = "((select consol_id_apply from biz_shipment where biz_shipment.consol_id_apply = biz_consol.id limit 1) is null)";
        $where[] = "trans_origin = '{$shipment['trans_origin']}'";
        $where[] = "trans_destination = '{$shipment['trans_destination']}'";
        //2021-06-11 只查看pending
        //3、申请errorlist改为pendinglist，只有	pending的consol
        $where[] = "status = 'pending'";

        $where = join(' AND ', $where);
        //  echo $where; exit();
        // $rs = $this->biz_consol_model->no_role_total($where);
        // $rs = $this->biz_consol_model->no_role_get($where,$orderfield,$order);
        $this->db->select('id,job_no,trans_carrier,(select client_name from biz_client where biz_client.client_code = biz_consol.trans_carrier) as trans_carrier_name,booking_ETD,box_info');
        $offset = ($page - 1) * $limit;
        $this->db->limit($limit, $offset);
        $rs = $this->biz_consol_model->no_role_get($where);
        $rows = array();
        foreach ($rs as $row) {
            $row['box_info_json'] = $row['box_info'];

            $box_info = json_decode($row['box_info'], true);
            $box_info = array_map(function ($val) {
                return $val['size'] . '*' . $val['num'];
            }, $box_info);

            $row['box_info'] = join(',', $box_info);
            $rows[] = $row;
        }

        echo json_encode($rows);
    }

    public function lock()
    {
        $id = postValue('id', 0);
        $level = postValue('level', 0);
        $result = array('code' => 1, 'msg' => 'error');
        if ($id != 0) {
            $data = array();
            $old_row = $this->biz_consol_model->get_one('id', $id);
            //2021-10-18 http://wenda.leagueshipping.com/?/question/533 锁C1 和 S1的时候，检查 consol的实际开航日，如果为空不让锁
            //2021-10-18 老板又说C1可以不受实际开航日限制
            // if($level != 0 && $old_row['trans_ATD'] == '0000-00-00 00:00:00'){
            //     echo json_encode(array('code' => 1, 'msg' => '请先填写实际开航日'));
            //     return;
            // }

            $this->db->select('id,consol_id,job_no,lock_lv');
            Model('biz_shipment_model');
            $shipments = $this->biz_shipment_model->get_shipment_by_consol($old_row['id']);
            $lock_msg = array();
            //这里循环,检测当前是否有shipment
            foreach ($shipments as $shipment) {
                if ($shipment['lock_lv'] > 0) $lock_msg[] = "{$shipment['job_no']}(已锁)";
            }
            //如果有讯息,代表有未锁的
            if (!empty($lock_msg)) {
                echo json_encode(array('code' => 1, 'msg' => '解锁失败, 请查看当前consol下的shipment是否已全部解锁。详情:' . join(',', $lock_msg)));
                return;
            }

            //2021-12-14 金晶要求consol锁的时候,专项金额之和必须等于0 http://wenda.leagueshipping.com/?/question/642
            //只限制锁的时候
            if ($level > $old_row['lock_lv']) {
                //查询consol的专项账单
                Model('biz_bill_model');
                $this->db->select('sum(amount * exrate) as local_amount', false);
                $special_bills = $this->biz_bill_model->get("id_type = 'consol' and id_no = '$id'", 'id', 'desc', 1);
                $local_amount = $special_bills[0]['local_amount'];
                // $local_amount = array_sum(array_column($special_bills, 'local_amount'));
                if ($local_amount != 0) {
                    echo json_encode(array('code' => 1, 'msg' => '保存失败, 专项费用之和不等于0'));
                    return;
                }
            }

            if (!empty($old_row)) {
                $data['lock_lv'] = $level;
                $this->biz_consol_model->update($id, array('lock_lv' => $level));

                lock_log('biz_consol', $id, $level);

                $data['id'] = $id;

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_consol";
                $log_data["key"] = $id;
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($data);
                log_rcd($log_data);
                $result = array('code' => 0, 'msg' => 'success');
            }
        }
        echo json_encode($result);
    }

    /**
     * 批量锁，给财务用的，直接锁到3
     */
    public function batch_lock(){
        //没批量锁C3权限
        if(!menu_role('consol_batch_lock3')) return error(1, lang('没有权限'));

        $consol_ids = isset($_POST['consol_ids']) ? $_POST['consol_ids'] : '';
        $lock_lv = 3;

        $consol_ids_array = array_filter(explode(',', $consol_ids));

        $lock_consols = array();

        $consol_no_lock = array();

        Model('biz_duty_model');

        //批量检测consol
        Model('biz_consol_model');
        Model('biz_bill_model');
        $result_data = array();
        $result_data[0] = array('key_name' => 'CONSOL专项金额之和不等于0', 'data' => array());
        $success = 0;
        $fail = 0;
        if(!empty($consol_ids_array)){
            $this->db->select('id,job_no,lock_lv,trans_ATD');
            $consols = $this->biz_consol_model->no_role_get("id in (" . join(',', $consol_ids_array) . ") and lock_lv < $lock_lv");

            foreach ($consols as $consol){
                //2021-10-18 http://wenda.leagueshipping.com/?/question/533 锁C1 和 S1的时候，检查 consol的实际开航日，如果为空不让锁
                //2021-10-18 老板又说C1可以不受实际开航日限制
                // if($consol['trans_ATD'] == '0000-00-00 00:00:00'){
                //     echo json_encode(array('code' => 1, 'msg' => $consol['job_no'] . ',请先填写实际开航日'));
                //     return;
                // }
                //2021-12-14 金晶要求consol锁的时候,专项金额之和必须等于0 http://wenda.leagueshipping.com/?/question/642
                //只限制锁的时候
                //查询consol的专项账单
                $this->db->select('(amount * exrate) as local_amount');
                $special_bills = $this->biz_bill_model->get("id_type = 'consol' and id_no = '{$consol['id']}'", 'id', 'desc', 1);
                if(array_sum(array_column($special_bills, 'local_amount')) != 0){
                    $result_data[0]['data'][] = $consol['job_no'];
                    $fail++;
                    continue;
                }
                $lock_consol = array('id' => $consol['id'], 'lock_lv' => $lock_lv);
                $lock_consols[] = $lock_consol;
                $success++;
            }
        }

        $result_data[] = array('key_name' => 'CONSOL未锁的SHIPMENT', 'data' => $consol_no_lock);

        if(empty($lock_consols)) return success(0, '没有可修改的值:<br />成功:' . $success . '票<br/ > 失败:' . $fail . '票', array('data' => $result_data));

        //批量修改数据
        foreach ($lock_consols as $lock_consol){
            $update_data = $lock_consol;
            $this_id = $update_data['id'];
            unset($update_data['id']);
            $this->biz_consol_model->update($this_id, $update_data);

            lock_log('biz_consol', $this_id, $update_data['lock_lv']);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_consol";
            $log_data["key"] = $this_id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($update_data);
            log_rcd($log_data);
        }
        $result_data[] = array('key_name' => 'CONSOL已锁', 'data' => $lock_consols);

        return success(0, '修改成功<br />成功:' . $success . '票<br/ > 失败:' . $fail . '票', array('data' => $result_data));
    }

    /**
     * 查看 当前值是否重复
     * @return bool
     */
    public function select_value_exist()
    {
        $field = postValue('field', '');
        $value = postValue('value', '');
        $id = postValue('id', 0);

        $field_config = array('carrier_ref', 'agent_ref');
        if(!in_array($field, $field_config)) return error(1, lang('不支持当前字段'));

        if ($value == '') return error(1, lang('未找到'));

        $this->db->select('id,job_no');
        $consol = $this->biz_consol_model->get_where_one("{$field} = '{$value}' and id != {$id}");
        if (empty($consol)) return error(1, lang('未找到'));

        return success(0, lang('success'), array('data' => $consol));
    }

    /**
     * consol绑定日志
     */
    public function combine_log()
    {
        $id = getValue('id', '');
        //获取当前consol的被绑定记录
        Model('biz_consol_combine_model');
        $this->db->select('id, created_by, created_time,consol_id,' .
            '(select job_no from biz_consol where biz_consol.id = biz_consol_combine.consol_id) as job_no,' .
            '(select carrier_ref from biz_consol where biz_consol.id = biz_consol_combine.consol_id) as carrier_ref,' .
            '(select name from bsc_user where bsc_user.id = biz_consol_combine.created_by) as created_by_name');
        $combine_log = $this->biz_consol_combine_model->get("market_consol_id = $id");
        $data['rows'] = $combine_log;

        $this->load->view('head');
        $this->load->view('/biz/consol/combine/log_view', $data);
    }

    /**
     * 改配操作
     */
    public function gaipei()
    {
        $id = (int)postValue('id', 0);

        //查询当前consol
        $consol = $this->biz_consol_model->get_by_id($id);
        if (empty($consol)) {
            return jsonEcho(array('code' => 1, 'msg' => 'consol不存在'));
        }
        if ($consol['cancel_msg'] == 2) {
            return jsonEcho(array('code' => 1, 'msg' => 'consol已改配,请勿重复操作'));
        }
        if ($consol['lock_lv'] > 0) {
            return jsonEcho(array('code' => 1, 'msg' => 'consol已锁不能改配'));
        }
        if ($consol['carrier_ref'] == '') {
            return jsonEcho(array('code' => 1, 'msg' => 'MBL为空不能改配'));
        }
        //并单不让改配
        Model('biz_consol_combine_model');
        $combine_log = $this->biz_consol_combine_model->get_one("market_consol_id", $id);
        if (!empty($combine_log)) {
            return jsonEcho(array('code' => 1, 'msg' => '参与过合并,不允许改配'));
        }
        //有账单不让改配
        Model('biz_bill_model');
        $bill = $this->biz_bill_model->get_where_one("id_type = 'consol' and id_no = '$id'");
        if (!empty($bill)) {
            //2021-12-08 新增如果是专项的话,提示修改
            if ($bill['is_special'] == 1) {
                return jsonEcho(array('code' => 1, 'msg' => '当前consol存在专项账单，不允许改配'));
            } else {
                return jsonEcho(array('code' => 1, 'msg' => '当前consol存在账单，不允许改配'));
            }
        }


        return jsonEcho(array('code' => 0, 'msg' => '请在新页面里进行改配!'));
    }

    public function get_shipment_info($consol_id = 0)
    {
        $field = getValue('field', '');
        if ($consol_id == 0 || $field == '') {
            echo json_encode(array('code' => 1, 'msg' => '参数错误'));
            return;
        }
        $data = array();
        $field_array = filter_unique_array(explode(',', $field));
        foreach ($field_array as $this_field) {
            if (!in_array($this_field, $this->field_edit)) {
                if (sizeof($field_array) == 1) {
                    echo json_encode(array('code' => 1, 'msg' => '参数错误'));
                    return;
                } else {
                    continue;
                }

            }
            //件毛体 取shipment订舱总和

            Model('biz_shipment_model');
            $this->db->select('id,' . $this_field);
            $shipments = $this->biz_shipment_model->no_role_get("consol_id = '$consol_id'");
            if (empty($shipments)) {
                if (sizeof($field_array) == 1) {
                    echo json_encode(array('code' => 1, 'msg' => '获取失败, 不存在SHIPMENT'));
                    return;
                } else {
                    continue;
                }
            }
            $eq_field = array('free_svr');
            //数组第一个值其实没啥用,可以删了,删了后面记得改下
            $merge_field_config = array(
                'description' => array('description', "\r\n"),
                'mark_nums' => array('mark_nums', "\r\n"),
                'hs_code' => array('hs_code', ","),
            );
            $merge_field = array_column($merge_field_config, 0);

            //2021-09-30 多个不能获取改为部分字段，返回内容改为去重拼接
            if (!in_array($this_field, $merge_field)) {
                if (sizeof($shipments) > 1 && !in_array($this_field, $eq_field)) {
                    if (sizeof($field_array) == 1) {
                        echo json_encode(array('code' => 1, 'msg' => '获取失败, 不能获取多个shipment'));
                        return;
                    } else {
                        continue;
                    }
                }
                if (sizeof(array_unique(array_column($shipments, $this_field))) > 1) {
                    if (sizeof($field_array) == 1) {
                        echo json_encode(array('code' => 1, 'msg' => '获取失败, SHIPMENT ' . lang($field) . '不相同'));
                        return;
                    } else {
                        continue;
                    }
                }
                $data[$this_field] = $shipments[0][$this_field];

            } else {
                $join_str = $merge_field_config[$this_field][1];
                $data[$this_field] = join($join_str, filter_unique_array(array_column($shipments, $this_field)));
            }

        }


        echo json_encode(array('code' => 0, 'msg' => '获取成功', 'data' => $data));
    }

    /**
     * 同步consol的某个字段到shipment里
     */
    public function tb_shipment_by_consol_field()
    {
        $id = postValue('id', '');
        $field = postValue('field', '');
        $result = array('code' => 1, 'msg' => '参数错误');
        //允许同步的字段
        $field_configs = array('booking_ETD', 'port_info');
        if (!in_array($field, $field_configs)) {
            $result['msg'] = '该字段未配置';
            echo json_encode($result);
            return;
        }
        //consol id 不能为空
        if (empty($id) || empty($field)) {
            echo json_encode($result);
            return;
        }
        //port_info是一堆
		$tb_fields = array(
			'trans_carrier','trans_origin','trans_origin_name',
			'trans_discharge','trans_discharge_name',
			'trans_destination','trans_destination_inner','trans_destination_name','booking_ETD'
		);
		$field = join(',', $tb_fields);


        $this->db->select('id, ' . $field);
        $consol = $this->biz_consol_model->get_by_id($id);
        if (empty($consol)) {
            $result['msg'] = 'CONSOL 不存在';
            echo json_encode($result);
            return;
        }

        Model('biz_shipment_model');
        $this->db->select('id,consol_id,' . $field);
        $shipments = $this->biz_shipment_model->get_shipment_by_consol($id);

        //shipment的consol的id不能出现0且不能出现多个值
        foreach ($shipments as $shipment) {
            if ($shipment['consol_id'] == 0) continue;

            $update_data = array();
            foreach ($tb_fields as $tb_field){
                $update_data[$tb_field] = $consol[$tb_field];
            }
            $this->biz_shipment_model->update($shipment['id'], $update_data);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment";
            $log_data["key"] = $shipment['id'];
            $log_data["master_table_name"] = 'biz_consol';
            $log_data["master_key"] = $consol['id'];
            $log_data["action"] = 'update';
            $log_data["value"] = json_encode($update_data);
            log_rcd($log_data);
        }

        $result['code'] = 0;
        $result['msg'] = '推送成功';
        echo json_encode($result);
    }

    public function get_shipment_sum($consol_id = 0)
    {
        if ($consol_id == 0) {
            echo json_encode(array('code' => 1, 'msg' => '参数错误'));
            return;
        }
        //件毛体 取shipment订舱总和
        $data = array();
        Model('biz_shipment_model');
        $this->db->select('id,good_outers,good_outers_unit,good_weight,good_volume');
        $shipments = $this->biz_shipment_model->no_role_get("consol_id = '$consol_id'");
        if (empty($shipments[0]['good_outers_unit'])) $shipments[0]['good_outers_unit'] = '';
        $data['good_outers'] = array_sum(array_column($shipments, 'good_outers'));
        $data['good_weight'] = array_sum(array_column($shipments, 'good_weight'));
        $data['good_volume'] = array_sum(array_column($shipments, 'good_volume'));
        $data['good_outers_unit'] = $shipments[0]['good_outers_unit'];
        echo json_encode(array('code' => 0, 'msg' => '获取成功', 'data' => $data));
    }
    
    public function table_tree($type = 'read')
    {
        $data = array();
        $this->field_edit = array();
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            $this->lock_filed[$row[4]][] = $v;
            if (isset($row[5]) && !empty($row[5])) {
                $this->field_edit[$row[5]][] = $v;
            } else {
                array_push($this->field_edit, $v);
            }
        }
        //将需要的duty字段加入----start
        foreach ($this->admin_field as $val) {
            $this->field_edit['staff info'][] = $val;
        }
        //将需要的duty字段加入----end
        $temp = array();
        foreach ($this->field_edit as $key => $val) {
            if (is_array($val)) {
                $cd_temp = array();
                foreach ($val as $v) {
                    array_push($cd_temp, array("id" => "1$type." . $v, "text" => lang($v)));
                }
                array_push($temp, array('id' => "1$type." . $key, 'text' => lang($key), 'children' => $cd_temp, 'not_add' => 1));
            } else {
                array_push($temp, array("id" => "1$type." . $val, "text" => lang($val)));
            }
        }
        array_push($data, array("id" => "1$type", "text" => lang("$type"), "children" => $temp));
        echo json_encode($data);
    }
    
     /**
     * 保存汇率日期
     * 2023-05-26 保存汇率日期,同时刷新账单的bill_ETD和汇率
     */
    public function save_ex_rate_date(){
        $id = (int)postValue('id', 0);
        $ex_rate_date = postValue('ex_rate_date', '0000-00-00');
        
        $this->db->select("id,ex_rate_date");
        $old_row = Model('biz_consol_model')->get_by_id($id);
        if(empty($old_row)) return jsonEcho(array('code' => 1, 'msg' => lang("数据不存在")));
        
        $data = array();
        if($ex_rate_date !== $old_row['ex_rate_date']) $data['ex_rate_date'] = $ex_rate_date;
        
        //这里会不会出现后面引用的时候,只想刷新数据的调用呢
        if(empty($data)) return jsonEcho(array('code' => 0, 'msg' => lang("保存成功")));
        
        Model('biz_consol_model')->update($id, $data);
        
        log_rec('biz_consol', $id, 'update', $data);
        
        //修改成功后, 在这里对账单进行刷新
        update_bill_ETD('consol', $id);
       
        //获取shipment数量
        $this->db->select("id");
        $shipments = Model("biz_shipment_model")->get_shipment_by_consol($id);
        
        foreach ($shipments as $shipment){
             update_bill_ETD('shipment', $shipment['id']);
        }
        
        $bill_ETD = get_bill_ETD("consol", $id);
        
        return jsonEcho(array('code' => 0, 'msg' => lang("保存成功"), 'data' => array('bill_ETD' => $bill_ETD)));
    }

	public function basic_steps($id=0){
		$this->load->view('head');
		$year = isset($_GET['year'])?$_GET['year']:0;
		$field = $this->field_edit;
		$data = array();
		foreach ($field as $item) {
			$data[$item] = "";
		}
		if ($id != 0) {
			if($year != 0){
				$consol = Model('biz_consol_history_model')->get_one('id', $id,$year);
			}else{
				$consol = $this->biz_consol_model->get_duty_one('id='.$id);
			}
			if (!empty($consol)) {
				$data = $consol;
			} else {
				echo 'consol不存在';
				return;
			}
		}
		pass_role($data, $this->admin_field);
		$userId = $this->session->userdata('id');
		$userBsc = $this->bsc_user_model->get_by_id($userId);

		$userRole = $this->bsc_user_role_model->get_data_role();
		$userBsc = array_merge($userBsc, $userRole);

		//页面传假值
		$userBsc['edit_text'] = isset($_POST['edit_text']) ? $_POST['edit_text'] : $userBsc['edit_text'];
		$userBsc['read_text'] = isset($_POST['read_text']) ? $_POST['read_text'] : $userBsc['read_text'];

		$data1 = array();
		$g_read_table = false; //全表读
		$g_edit_table = false; //全表可写
		foreach ($data as $key => $value) {
			$data1[$key] = '***';
		}

		$userBscRead = explode(",", (isset($userBsc['read_text']) ? $userBsc['read_text'] : ''));
		foreach ($userBscRead as $row) {
			if (strpos($row, 'consol.') !== false) {
				$arr_temp = explode(".", $row);
				$key = $arr_temp[1];
				if (in_array($key, $this->admin_field)) {
					$data1[$key . '_id'] = $data[$key . '_id'];
					$data1[$key . '_group'] = $data[$key . '_group'];
				} else {
					$data1[$key] = array_key_exists($key, $data) ? $data[$key] : '';
				}
			} else if ($row == 'consol') {
				// 全表
				$g_read_table = true;
				break;
			}
		}
		if ($g_read_table) {
			$data1 = $data;
		}
		$data1["id"] = $id;
		$data['lock_lv'] = isset($data['lock_lv']) ? $data['lock_lv'] : 0;
		$data1['lock_lv'] = $data['lock_lv'];
		$userBscEdit1 = explode(",", (isset($userBsc['edit_text']) ? $userBsc['edit_text'] : ''));
		$userBscEdit = array();
		$lock_field = array();
		for ($i = 1; $i <= $data['lock_lv']; $i++) {
			if (isset($this->lock_filed[$i])) $lock_field = array_merge($lock_field, $this->lock_filed[$i]);
		}
		foreach ($userBscEdit1 as $row) {
			if (strpos($row, 'consol.') !== false) {
				$arr_temp = explode(".", $row);
				if (!in_array($arr_temp[1], $lock_field)) array_push($userBscEdit, $arr_temp[1]);
			} else if ($row == 'consol') {
				// 全表
				$g_edit_table = true;
				break;
			}
		}
		if ($g_edit_table) {
			$userBscEdit = array_diff(array_keys($data), $lock_field);
		}
		$data1['G_userBscEdit'] = $userBscEdit;
		//必须相等的字段判定
		$this->load->model('biz_shipment_model');
		$shipment_diff = $this->must_eq_field($data);
		//取
		$data1['mbl_shipments'] = sizeof(array_column($shipment_diff[1], 'mbl_shipments')) > 0 ? true : false;
		//如果MBL 且多个shipment,那么报错
		if ($data1['mbl_shipments'] == true) {
			//
			$data1['G_userBscEdit'] = checkboxDisabledField($data, $data1['G_userBscEdit'], array('dingcangshenqing','dingcangwancheng', 'yupeiyifang', 'xiangyijingang', 'operator_si', 'matoufangxing', 'chuanyiqihang'), '由于 当前相关shipment存在MBL,且shipment为多个,当前字段已锁定');
		}
		//2021-11-19 如果操作SI勾上后,那么SI相关字段不给修改
		if ($data['operator_si'] != '0') {
			Model('biz_shipment_si_model');
			$si_field = $this->biz_shipment_si_model->getSiField();

			//非MBL时,description允许修改
			if (!$data1['mbl_shipments'] && $data['document_si'] == '0') {
//                $si_field = array_diff($si_field, array('description'));
			}
			$si_field[] = 'agent_email';
			$si_field[] = 'agent_company';
			$si_field[] = 'agent_address';
			$si_field[] = 'agent_contact';
			$si_field[] = 'agent_telephone';
			$si_field[] = 'agent_code';

//            $data1['G_userBscEdit'] = array_diff($data1['G_userBscEdit'], $si_field);
			$data1['G_userBscEdit'] = checkboxDisabledField($data, $data1['G_userBscEdit'], $si_field, '由于 操作SI 已勾选,当前字段已锁定', $si_field);
		}

		//截单管理那里才能改
//		$data1['G_userBscEdit'] = array_diff($data1['G_userBscEdit'], array('document_si'));
		//2022-07-15 关联shipment
		$shipment=$this->db->where('consol_id',$data1['id'])->get('biz_shipment')->row_array();
		$data1['shipment'] = $shipment;
		$data1['shipment_id'] = -1;
		if(empty($shipment)){
			$data1['shipment_arr']=array();
		}else {
			$data1['shipment_id'] = $shipment['id'];
		}
		$data1['year'] = $year;
		$this->load->view('/biz/consol/basic_steps_view',$data1);
	}
}
