<style type="text/css">
    .f {
        width: 115px;
    }

    .s {
        width: 100px;
    }

    .v {
        width: 200px;
    }

    .del_tr {
        width: 23.8px;
    }

    .this_v {
        display: inline;
    }

    .span_count {
        background-color: #FF455B;
        display: inline-block;
        padding: 0 2px;
        text-align: center;
        vertical-align: middle;
        font-style: normal;
        color: #fff;
        overflow: hidden;
        line-height: 16px;
        height: 16px;
        font-size: 12px;
        border-radius: 4px;
        font-weight: 200;
        margin-left: 1px;
        white-space: nowrap;
    }

    .hide {
        display: none;
    }

</style>
<!--这里是查询框相关的代码-->
<script>
    var query_option = {};//这个是存储已加载的数据使用的

    var table_name = 'biz_client',//需要加载的表名称
        view_name = 'biz_client_index';//视图名称
    var add_tr_str = "";//这个是用于存储 点击+号新增查询框的值, load_query_box会根据接口进行加载


    /**
     * 加载查询框
     */
    function load_query_box(){
        ajaxLoading();
        $.ajax({
            type:'GET',
            url: '/sys_config_title/get_user_query_box?view_name=' + view_name + '&table_name=' + table_name,
            dataType:'json',
            success:function (res) {
                ajaxLoadEnd();
                if(res.code == 0){
                    //加载查询框
                    $('#cx table').append(res.data.box_html);
                    //需要调用下加载器才行
                    $.parser.parse($('#cx table tbody .query_box'));

                    add_tr_str = res.data.add_tr_str;
                    query_option = res.data.query_option;
                    var table_columns = [[
                    ]];//表的表头

                    //填充表头信息
                    $.each(res.data.table_columns, function (i, it) {
                        //读取列配置,然后这里进行合并
                        var column_config;
                        try {
                            column_config = JSON.parse(it.column_config,function(k,v){
                                if(v && typeof v === 'string') {
                                    return v.indexOf('function') > -1 || v.indexOf('FUNCTION') > -1 ? new Function(`return ${v}`)() : v;
                                }
                                return v;
                            });
                        }catch (e) {
                            column_config = {};
                        }
                        var this_column_config = {field:it.table_field, title:it.title, width:it.width, sortable: it.sortable, formatter: get_function(it.table_field + '_for'), styler: get_function(it.table_field + '_styler')};
                        //合并一下
                        $.extend(this_column_config, column_config);

                        table_columns[0].push(this_column_config);
                    });

                    //渲染表头
                    $('#tt').datagrid({
                        columns:table_columns,
                        width: 'auto',
                        height: $(window).height() - 30,
                        onDblClickRow: function (index, row) {
                            url = '/biz_client/edit/' + row.id;
                            window.open(url);
                        },
                        rowStyler: function (index, row) {


                        }
                    });
                    $(window).resize(function () {
                        $('#tt').datagrid('resize');
                    });

                    //为了避免初始加载时, 重复加载的问题,这里进行了初始加载ajax数据
                    var aj = [];
                    $.each(res.data.load_url, function (i,it) {
                        aj.push(get_ajax_data(it, i));
                    });
                    //加载完毕触发下面的下拉框渲染
                    //$_GET 在other.js里封装
                    var this_url_get = {};
                    $.each($_GET, function (i, it) {
                        this_url_get[i] = it;
                    });
                    var auto_click = this_url_get.auto_click;

                    $.when.apply($,aj).done(function () {
                        //这里进行字段对应输入框的变动
                        $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                            var ec_value = $(ec_item).combobox('getValue');
                            var v_inp = $('.v:eq(' + ec_index + ')');
                            var s_val = $('.s:eq(' + ec_index + ')').combobox('getValue');
                            var v_val = v_inp.textbox('getValue');
                            //如果自动查看,初始值为空
                            if(auto_click === '1') v_val = '';

                            $(ec_item).combobox('clear').combobox('select', ec_value);
                            //这里比对get数组里的值
                            $.each(this_url_get, function (trg_index, trg_item) {
                                //切割后,第一个是字段,第二个是符号
                                var trg_index_arr = trg_index.split('-');
                                //没有第二个参数时,默认用等于
                                if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';
                                //用一个删一个
                                //如果当前的选择框的值,等于get的字段值
                                if(ec_value === trg_index_arr[0] && s_val === trg_index_arr[1]){
                                    v_val = trg_item;//将v改为当前get的
                                    delete this_url_get[trg_index];
                                }
                            });
                            //没找到就正常处理
                            $('.v:eq(' + ec_index + ')').textbox('setValue', v_val);
                        });
                        //判断是否有自动查询参数
                        var no_query_get = ['auto_click'];//这里存不需要塞入查询的一些特殊变量
                        $.each(no_query_get, function (i,it) {
                            delete this_url_get[it];
                        });
                        //完全没找到的剩余值,在这里新增一个框进行选择
                        $.each(this_url_get, function (trg_index, trg_item) {
                            add_tr();//新增一个查询框
                            var trg_index_arr = trg_index.split('-');
                            //没有第二个参数时,默认用等于
                            if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';

                            $('#cx table tbody tr:last-child .f').combobox('setValue', trg_index_arr[0]);
                            $('#cx table tbody tr:last-child .s').combobox('setValue', trg_index_arr[1]);
                            $('#cx table tbody tr:last-child .v').textbox('setValue', trg_item);
                        });

                        //这里自动执行查询,显示明细
                        if(auto_click === '1'){
                            doSearch();
                        }
                    });
                }else{
                    $.messager.alert("<?= lang('提示');?>", res.msg);
                }
            },error:function (e) {
                ajaxLoadEnd();
                $.messager.alert("<?= lang('提示');?>", e.responseText);
            }
        });
    }

    /**
     * 新增一个查询框
     */
    function add_tr() {
        $('#cx table tbody').append(add_tr_str);
        $.parser.parse($('#cx table tbody tr:last-child'));
        var last_f = $('#cx table tbody tr:last-child .f');
        var last_f_v = last_f.combobox('getValue');
        last_f.combobox('clear').combobox('select', last_f_v);
        return false;
    }

    /**
     * 删除一个查询
     */
    function del_tr(e) {
        //删除按钮
        var index = $(e).parents('tr').index();
        $(e).parents('tr').remove();
        return false;
    }

    //执行加载函数
    $(function () {
        load_query_box();
    });
</script>
<script>
    function setValue(jq, val) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if (data.combobox !== undefined) {
            return $(jq).combobox('setValue', val);
        } else if (data.datetimebox !== undefined) {
            return $(jq).datetimebox('setValue', val);
        } else if (data.datebox !== undefined) {
            return $(jq).datebox('setValue', val);
        } else if (data.textbox !== undefined) {
            return $(jq).textbox('setValue', val);
        }
    }
    //模板--start
    function client_code_styler(value, row, index) {
        var this_style = '';
        //供应商类1个颜色   #8dffea9c;
        //warehouse  truck  declaration  carrier  oversea_agent  booking_agent
        var supplier = ['warehouse', 'truck', 'declaration', 'carrier', 'oversea_agent', 'booking_agent', 'container_supplier']
        //客户类1个颜色   #d88dff63;
        //client  factory  logistics_client  booking_agent  oversea_client
        var client = ['client', 'factory', 'logistics_client', 'oversea_client'];
        if (row.role === undefined) return '';
        var role = row.role.split(',');
        //同时拥有以上2者的属于深度合作类，用下面颜色

        //深度类， 2者都有的颜色 #8f8dffbf;
        var this_role_type = '';//当前角色类型
        var this_style = '';//当前样式
        //循环判断当前角色属于什么类型
        $.each(role, function (i, it) {
            if (this_role_type != 'supplier' && $.inArray(it, supplier) !== -1) {
                //如果没有类型
                if (this_role_type == '') this_role_type = 'supplier';
                //如果已经是客户类,且满足供应商,变为深度
                if (this_role_type == 'client') {
                    this_role_type = 'deep_client';
                    return false;
                }
            } else if (this_role_type != 'client' && $.inArray(it, client) !== -1) {
                //如果没有类型
                if (this_role_type == '') this_role_type = 'client';
                //如果已经是供应商类,且满足客户,变为深度
                if (this_role_type == 'supplier') {
                    this_role_type = 'deep_client';
                    return false;
                }
            }
        });
        if (this_role_type == 'client') {
            //客户类1个颜色   #d88dff63;
            this_style += 'background-color: #d88dff63';
        } else if (this_role_type == 'supplier') {
            //供应商类1个颜色   #8dffea9c;
            this_style += 'background-color: #8dffea9c';
        } else if (this_role_type == 'deep_client') {
            //深度类， 2者都有的颜色 #8f8dffbf;
            this_style += 'background-color: #8f8dffbf';
        }
        return this_style;
    }
    //模板--end
</script>
<script type="text/javascript">
    var client_name = '';

    function doSearch(a = '') {
        if (a != '') client_name = a;
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if (json.hasOwnProperty(item.name) === true) {
                if (typeof json[item.name] == 'string') {
                    json[item.name] = [json[item.name]];
                    json[item.name].push(item.value);
                } else {
                    json[item.name].push(item.value);
                }
            } else {
                json[item.name] = item.value;
            }
        });
        var url = '/biz_client/get_data_v2';
        if (client_name != '<?= lang('全部客户');?>') {
            url = '/biz_client/get_data_v2?client_name=' + client_name;
        }


        $('#tt').datagrid({
            url: url,
            destroyUrl: '/biz_client/delete_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            },
            queryParams: json,
        }).datagrid('clearSelections');
        set_config();
        $('#chaxun').window('close');
    }

    function resetSearch(){
        $.each($('.v'), function(i,it){
            setValue($(it), '');
        });
    }
    
    //打开新增窗口
    function add(){
        window.open('/biz_client/add_basic');
    }
    $(function () {
        $('#cx').on('keydown', 'input', function (e) {
            if (e.keyCode == 13) {
                doSearch();
            }
        });

        //2022-05-26 新增
        $('#tabs').tabs({
            border: false,
            onSelect: function (title) {
                console.log(2);
                if (title == '<?= lang('全部客户');?>') {
                    doSearch('全部客户');
                }
                if (title == '<?= lang('供应商');?>') {
                    doSearch('供应商');
                }
                if (title == '<?= lang('非活跃客户');?>') {
                    doSearch('非活跃客户');
                }
                if (title == '<?= lang('活跃客户');?>') {
                    doSearch('活跃客户');
                }
                if (title == '<?= lang('异常');?>') {
                    doSearch('异常');
                }
                if (title == '<?= lang('黑名单');?>') {
                    doSearch('黑名单');
                }
            }
        });
    });

    function userLoadSuccess(node, data) {
        //这里进行循环操作,当找到第一个is_use == true时,选中
        var val = $('#crm_user').val();
        if (val == '') {
            var result = searchIsuse(data);
            $('#user').combotree('setValue', result);
            $('#db_fm').submit();
        }
    }

    /**
     * 查找到最大的第一个可使用的
     * @param data
     */
    function searchIsuse(data) {
        var result = '';
        //循环一遍,查看第一层结构是否有可使用的
        var new_data = [];
        $.each(data, function (i, it) {
            if (it.is_use == true) {
                result = it.id;
                return false;
            }
            new_data.push(it);
        });
        if (result != '') return result;
        //如果没找到,这里循环再递归一下
        $.each(new_data, function (i, it) {
            result = searchIsuse(it.children);
            if (result != '') return result;
        })

        return result;
    }

</script>
<table id="tt" style="width:1100px;height:450px" rownumbers="false" pagination="true" idField="id" pagesize="30" toolbar="#tb" singleSelect="true" nowrap="true">
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add();"><?= lang('新增');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true"
                   onclick="javascript:$('#chaxun').window('open');"><?= lang('查询'); ?></a>
                <a href="javascript:void;" class="easyui-tooltip tooltip-f" data-options="
                    content: $('<div></div>'),
                    onShow: function(){
                        $(this).tooltip('arrow').css('left', 20);
                        $(this).tooltip('tip').css('left', $(this).offset().left);
                    },
                    onUpdate: function(cc){
                        cc.panel({
                            width: 300,
                            height: 'auto',
                            border: false,
                            content: '粉色代表客户类 <br/> 紫色代表双向合作类<br/> 绿色代表供应商类',
                        });
                    }
                " title=""><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true"
                   onclick="config()"><?= lang('表头设置'); ?></a>
                <?php if(menu_role('contact_verified_client')):?>
                    <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="window.open('/biz_client_contact/verified_client/')"><?= lang('已验证客户');?></a>
                <?php endif?>
            </td>
        </tr>
    </table>
</div>

<div id="chaxun" class="easyui-window" title="Query" closed="false" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <form id="cx">
        <table>
            <tr>
                <td><?= lang('未合作天数'); ?></td>
                <td>
                    <input class="easyui-numberspinner" name="whzts" data-options="min:0" style="width: 475px;">
                </td>
            </tr>
            <tr>
                <td>
                    <?= lang('role') ?>
                </td>
                <td align="right">
                    <select class="easyui-combobox" id="f_role" name="f_role" style="width: 475px"
                            data-options="
                        valueField:'value',
                        textField:'value',
                        multiple:true,
                        url:'/bsc_dict/get_option/role',
                    ">

                    </select>
                </td>
            </tr>
        </table>
        <br><br>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;"
                onclick="doSearch();"><?= lang('search'); ?></button>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="resetSearch();"><?= lang('重置');?></button>

        <a href="javascript:void;" class="easyui-tooltip" data-options="
            content: $('<div></div>'),
            onShow: function(){
                $(this).tooltip('arrow').css('left', 20);
                $(this).tooltip('tip').css('left', $(this).offset().left);
            },
            onUpdate: function(cc){
                cc.panel({
                    width: 500,
                    height: 'auto',
                    border: false,
                    href: '/bsc_help_content/help/query_condition'
                });
            }
        "><?= lang('help'); ?></a>
    </form>
</div>
