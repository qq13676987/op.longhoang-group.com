<?php

class biz_company extends Menu_Controller
{
    protected $menu_name = 'company';//菜单权限对应的名称
    protected $method_check = array(//需要设置权限的方法
        'index',
        'add_data',
        'update_data',
        'delete_data',
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_company_model');
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }

    public $field_edit = array();

    public $field_all = array(
        array("id", "ID", "80", "1"),
        array("company_type", "company_type No", "100", "2"),
        array("company_name", "company_name Company", "120", "3"),
        array("company_address", "company_address", "120", "4"),
        array("company_contact", "company_contact", "120", "5"),
        array("company_telephone", "company_telephone", "120", "6"),
        array("company_email", "company_email", "120", "6"),
        array("country", "country", "80", "6"),
        array("country_name", "country_name", "80", "6"),
        array("region", "region", "80", "6"),
        array("region_code", "region_code", "80", "6"),
        array("city", "city", "60", "6"),
        array("postalcode", "postalcode", "80", "6"),
        array("created_by", "created_by", "150", "10"),
        array("created_time", "created_time", "150", "10"),
        array("updated_by", "updated_by", "150", "10"),
        array("updated_time", "updated_time", "150", "11")
    );

    public function index($company_type = "", $client_code = "")
    {
        $data = array();

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_company_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }
        // page account
        $rs = $this->sys_config_model->get_one('biz_company_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }

        $data["company_type"] = $company_type;
        $data["client_code"] = $client_code;
        $this->load->view('head');
        $this->load->view('biz/company/index_view', $data);
    }
    
    public function get_data($company_type = "", $client_code = "")
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------这个查询条件想修改成通用的 -------------------------------
        //-------这个查询条件想修改成通用的 -------------------------------
        $where = array();
        $f1 = 'company_type';
        $s1 = '=';
        $v1 = "$company_type";
        $f2 = 'company_name';
        $s2 = 'like';
        $v2 = isset($_REQUEST['company_name']) ? ($_REQUEST['company_name']) : '';
        $v2 = "%$v2%"; 
        $f3 = 'client_code';
        $s3 = '=';
        $v3 = "$client_code";
        if ($v1 != "") $where[] = "$f1 $s1 '$v1'";
        if ($v2 != "" && $where != "") $where[] = "$f2 $s2 '$v2'";
        if ($v3 != "" && $client_code != "") $where[] = "$f3 $s3 '$v3'";
        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->biz_company_model->total($where);
        $this->db->limit($rows, $offset);
        $this->db->select('*,(select name from bsc_user where bsc_user.id = biz_company.created_by) as created_by_name,(select name from bsc_user where bsc_user.id = biz_company.updated_by) as updated_by_name');
        $rs = $this->biz_company_model->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function get_option($company_type = "", $client_code = "", $biz_type = '')
    {
        $rows = array();
        $this->load->model('biz_company_model');
        //2021-08-31 由于shipment修改页面的选中改为了表格，所以这里加了查询条件
        $company_name = isset($_POST['company_name']) ? check_param($_POST['company_name']) : '';
        $where = array();
        if($company_name != '') $where[] = "company_name like '%$company_name%'";
        $where = join(' and ', $where);
        if(!empty($where)) $this->db->where($where);
		$this->db->limit(50);
        $rs = $this->biz_company_model->get_option($company_type, $client_code);
        //array_push($rows, array("company_name"=>" ","value"=>""));
        $jh = array(
            "id" => -2,
            "company_name"=>"SHANGHAI LEAGUE SHIPPING CO. LTD.",
            "company_address"=>"2ND FLOOR, BUILDING A, BLOCK 1, NO.723
TONGXIN ROAD,HONGKOU DISTRICT, SHANGHAI CHINA","company_email" => " ", "company_telephone" => ' ',"company_contact" => ' '
        );
        $jx = array(
            "id" => -3,
            "company_name"=>"SHANGHAI KIMXIN SUPPLY CHINA MANAGEMENT CO.,LTD",
            "company_address"=>"2ND FLOOR, BUILDING A, BLOCK 1, NO.723
TONGXIN ROAD,HONGKOU DISTRICT, SHANGHAI CHINA","company_email" => " ", "company_telephone" => ' ',"company_contact" => ' '
        );
        $this->load->model('bsc_user_model');
        $user = $this->bsc_user_model->get_one('id', $this->session->userdata('id'));
        $company = array();
        // if($user['company'] == 'LEAGUESHIPPING'){
            // $company = $jh;
        // }else if($user['company'] == 'KIMXIN SUPPLY CHAIN'){
            // $company = $jx;
        // }
        $this->load->model('biz_client_model');
        $client = $this->biz_client_model->get_one('client_code', $client_code);
        if(empty($client)) $client = array('contact_name' => '', 'contact_telephone2' => '', 'contact_email' => '');
        $client['company_contact'] = $client['contact_name'];
        $client['company_telephone'] = $client['contact_telephone2'];
        $client['company_email'] = $client['contact_email'];
        if($company_type == 'notify'){
            array_push($rows, array("id" => -1, "company_name"=>"SAME AS CONSIGNEE","company_address"=>" ","company_email" => " ", "company_telephone" => ' ',"company_contact" => ' '));
        }else if($company_type == 'consignee'){
            //出口填入agent,进口填入聚瀚或精心
            if($biz_type == 'export'){
                // $rows[] = $client;
            }else if($biz_type == 'import'){
                $rows[] = $jh;
                $rows[] = $jx;
            }
        }else if($company_type == 'shipper'){
            //出口填入填入聚瀚或精心,进口填入agent
            if($biz_type == 'export'){
                $rows[] = $jh;
                $rows[] = $jx;
            }else if($biz_type == 'import'){
                // $rows[] = $client;
            }
        }
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        echo json_encode($rows);
    }
    
    public function get_options($company_type = "", $client_code = ''){
        $company_name = isset($_POST['company_name']) ? $_POST['company_name'] : '';
        $this->load->model('biz_company_model');
        $this->db->limit(50);
        $where = array();
        if(!empty($company_name))$where[] = "company_name like '%$company_name%'";
        $where[] = "company_type = '$company_type'";
        $where[] = "client_code = '$client_code'";
        $where = join(' AND ', $where);
        $rs = $this->biz_company_model->get($where);
        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        echo json_encode($rows);
    }
    
    public function add($company_type = "", $client_code = ""){
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $data[$item] = '';
        }
        $data['company_type'] = $company_type;
        $data['client_code'] = $client_code;
        
        $this->load->view('head');
        $this->load->view('/biz/company/add_view', $data);
    }

    public function add_data($company_type = "", $client_code = "")
    {
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? ts_replace(trim($_POST[$item])) : '';
            $mach = check_banjiao($temp);
            if(sizeof($mach[0]) > 0){
                $data['isError'] = true;
                $data['code'] = 444;
                $data['msg'] = lang($item) . lang('存在特殊字符'). ': ' . join('', $mach[0]);
                $data['ts_str'] = $mach[0];
                $data['text'] = $temp;
                echo json_encode($data);
                return false;
            }
            $data[$item] = strtoupper(trim($temp));
        }
        //因mars要求，这里加入了中国6位数字，美国5位数字，加拿大3字符+空格+3字符
        $postalcode_rule = array(
            'CN' => array('/^[0-9]{6}$/', '中国邮编必须为6位数字'),    
            'US' => array('/^[0-9]{5}$/', '美国邮编必须为5位数字'),    
            'CA' => array('/^[0-9a-zA-Z]{3}[ ]{1}[0-9a-zA-Z]{3}$/', '加拿大邮编必须为3位字符+空格+3位字符'),    
        );
        if(isset($postalcode_rule[$data['country']])){
            $check_postalcode = preg_match($postalcode_rule[$data['country']][0], $data['postalcode']);
            if(!$check_postalcode){
                $data['code'] = 1;
                $data['msg'] = lang($postalcode_rule[$data['country']][1]);
                echo json_encode($data);
                return;
            }
        }
        
        $data['company_type'] = $company_type;
        $data['client_code'] = $client_code;
        $id = $this->biz_company_model->save($data);
        $data['id'] = $id;
        if(!$id){
            $data['code'] = 1;
            $data['msg'] = lang('保存失败');
            echo json_encode($data);
            return;
        }
        
        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_company";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        
        $data['code'] = 0;
        $data['msg'] = lang('保存成功');
        echo json_encode($data);
    }
    
    public function edit($id = 0){
        $data = $this->biz_company_model->get_by_id($id);
        if(empty($data)){
            echo '未获取到';
            return;
        }
        
        $field = $this->field_edit;
        
        $this->load->view('head');
        $this->load->view('/biz/company/edit_view', $data);
    }
    public function edit_iframe(){
        $id = getValue('id', 0);
        $reload_input = getValue('reload_input', '');
        $data = $this->biz_company_model->get_by_id($id);
        if(empty($data)){
            echo '未获取到';
            return;
        }
        $data['reload_input'] = $reload_input;

        $this->load->view('head');
        $this->load->view('/biz/company/edit_iframe_view', $data);
    }

    public function update_data()
    {
        $id = $_REQUEST["id"];
        $old_row = $this->biz_company_model->get_by_id($id);

        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? ts_replace(trim($_POST[$item])) : '';
            $mach = check_banjiao($temp);
            if(sizeof($mach[0]) > 0){
                $data['isError'] = true;
                $data['code'] = 444;
                $data['msg'] = lang($item) . lang('存在特殊字符') . ': ' . join('', $mach[0]);
                $data['ts_str'] = $mach[0];
                $data['text'] = $temp;
                echo json_encode($data);
                return false;
            }
            $data[$item] = strtoupper(trim($temp));
        }
        if(isset($data['postalcode'])){
            //因mars要求，这里加入了中国6位数字，美国5位数字，加拿大3字符+空格+3字符
            $postalcode_rule = array(
                'CN' => array('/^[0-9]{6}$/', '中国邮编必须为6位数字'),    
                'US' => array('/^[0-9]{5}$/', '美国邮编必须为5位数字'),    
                'CA' => array('/^[0-9a-zA-Z]{3}[ ]{1}[0-9a-zA-Z]{3}$/', '加拿大邮编必须为3位字符+空格+3位字符'),    
            );
            $country = isset($data['country']) ? $data['country'] : $old_row['country'];
            if(isset($postalcode_rule[$country])){
                $check_postalcode = preg_match($postalcode_rule[$country][0], $data['postalcode']);
                if(!$check_postalcode){
                    $data['code'] = 1;
                    $data['msg'] = lang($postalcode_rule[$country][1]);
                    echo json_encode($data);
                    return;
                }
            }
        }
        $id = $this->biz_company_model->update($id, $data);
        $data['id'] = $id;

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_company";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        
        $data['code'] = 0;
        $data['msg'] = lang('保存成功');
        echo json_encode($data);
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->biz_company_model->get_one('id', $id);
        $this->biz_company_model->mdelete($id);
        echo json_encode(array('success' => true));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_company";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
    }


    public function config_title()
    {
        $this->load->model('sys_config_model');
        $data = array();
        $rs = $this->sys_config_model->get_one('biz_company_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = $rs['config_text'];
        } else {
            $data["f"] = json_encode($this->field_all);
        }
        $rs = $this->sys_config_model->get_one('biz_company_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }
        $this->load->view('head');
        $this->load->view('sys/config/config_view', $data);
    }

    public function config_title_save()
    {
        $this->load->model('sys_config_model');
        if (!empty($_POST["slct_ziduan"])) {
            $response = $_POST["slct_ziduan"];
            $s = array();
            foreach ($response as $r) {
                $s[] = $r[3];
            }
            array_multisort($s, SORT_ASC, $response);
            $response = json_encode($response);
            $this->sys_config_model->save("biz_company_table", $response);
        }
        echo "<script> opener.location.reload(); window.close(); </script>";
    }

    public function config_number_save()
    {
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->save('biz_company_table_row_num', $_REQUEST["num"]);
        echo "<script> opener.location.reload(); window.close(); </script>";
    }

    public function config_init()
    {
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->mdelete('biz_company_table');
        $rs = $this->sys_config_model->mdelete('biz_company_table_row_num');
        echo "<script> opener.location.reload(); window.close(); </script>";
    }

    public function get_options_by_consol($consol_id = 0, $company_type = ''){
        $company_name = isset($_POST['company_name']) ? $_POST['company_name'] : '';
        Model('biz_consol_model');
        $this->db->select('id,agent_code');
        $consol = $this->biz_consol_model->get_by_id($consol_id);
        Model('biz_shipment_model');
        $this->db->select('id,client_code');
        $shipments = $this->biz_shipment_model->get_shipment_by_consol($consol_id);

        $client_codes = array_merge(array($consol['agent_code']), array_column($shipments, 'client_code'));

        $client_codes = array_filter(array_unique($client_codes));

        $this->load->model('biz_company_model');
        $this->db->limit(50);
        $where = array();
        if(!empty($company_name))$where[] = "company_name = '$company_name'";
        $where[] = "company_type = '$company_type'";
        $where[] = "client_code in ('" . join('\',\'', $client_codes) . "')";
        $where = join(' AND ', $where);
        $rs = $this->biz_company_model->get($where);
        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        echo json_encode($rows);
    }
    
    /**
     * 该方法用于查询所有库, 进行搜索匹配使用, 最多提示10条
     */
    public function getSearchAll(){
        $company_type = getValue('company_type', '');
        $company_name = postValue('company_name', '');

        $where = array();

        if($company_type !== '') $where[] = "company_type = '{$company_type}'";
        if($company_name !== '') $where[] = "company_name like '%$company_name%'";

        $where_str = join(' and ', $where);

        $this->db->limit(10);

        //这里需要group一下,去掉相同的值
        $this->db->group_by('company_name,company_type');
        $rs = $this->biz_company_model->get($where_str);

        $result = array('rows' => array());
        foreach ($rs as $row) {
            $result['rows'][] = $row;
        }
        echo json_encode($result);
    }
}
