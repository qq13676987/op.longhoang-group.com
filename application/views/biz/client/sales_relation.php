<style type="text/css">

</style>
<script>

    $(function(){
        //写入sales_op勾选后自动存储的事件
        $('.sales_op').click(function () {
            var t = $(this);
            var sales_id = t.attr('sales_id');
            var role = t.attr('role');
            var user_id = t.attr('user_id');
            var client_code = $('#client_code').val();

            var checked = t.prop('checked');
            var checked_val;
            if(checked) checked_val = 1;
            else checked_val = 0;
            //如果选中,那么执行ajax进行新增或删除数据
            $.ajax({
                url: '/biz_client/save_sales_relation_data',
                type: 'POST',
                data: {
                    sales_id: sales_id,
                    role: role,
                    user_id: user_id,
                    client_code: client_code,
                    checked: checked_val,
                },
                dataType: 'json',
                success:function (res) {
                    if(res.code !== 0){
                        $.messager.alert("<?= lang('提示');?>", res.msg);
                    }
                },error:function (e) {
                    $.messager.alert("<?= lang('提示');?>", e.responseText);
                }
            });
        });
    });
</script>
<fieldset>
    <legend>销售人员绑定</legend>
    <input type="hidden" id="client_code" value="<?= $client_code;?>">
    <table border="1" cellspacing="0" width="500">
        <tr>
            <td>人员</td>
            <td>角色</td>
            <?php foreach ($sales_ids as $sales_id){ ?>
                <td><?= getUserName($sales_id);?></td>
            <?php } ?>
        </tr>
        <?php foreach($rows as $row){ ?>
            <tr>
                <td><?= $row['name'];?></td>
                <td><?= $row['role'];?></td>
                <?php foreach ($row['sales'] as $sale){ ?>
                    <td><input class="sales_op" type="checkbox" sales_id="<?= $sale['id'];?>" role="<?= $row['role'];?>" user_id="<?= $row['id'];?>" <?= $sale['checked'];?>></td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
</fieldset>