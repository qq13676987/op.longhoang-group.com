<style>
    .signature_div{
        min-height: 50px;
    }
    #add_content_div {
        margin: 0 10px;
        border: 1px solid #ccc;
    }
    #toolbar-container {
        border-bottom: 1px solid #ccc;
    }
    #editor-container {
        height: 500px;
    }
</style>
<!--add_presend-->
<div align="center"><h1> <?= lang('2、创建邮件内容');?>  </h1></div>
<div class="easyui-panel" title="<?= lang('增加预发送');?>" style="overflow:scroll;width:100%;height:850px;position:relative;z-index: 0">
    <div style="padding:10px 40px 10px 40px;">
        <form id="ff" method="post">
            <table cellpadding="5">
                <tr>
                    <td><?= lang('收件人组');?>:</td>
                    <td>
                        <input type="hidden" name="recipient_group" value="<?= $recipient_group[0]['id'];?>">
                        <?= $recipient_group[0]['group_name'];?>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('收件人邮箱');?>:</td>
                    <td>
                        <button class="easyui-linkbutton" type="button" onclick="window.open('/crm_promote_mail/group_consignee_list?group_id=<?= $recipient_group[0]['id'];?>');"><?= lang('查看收件人组成员');?></button>
                        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:$('#group').window('open');" ><?= lang('复制一封历史邮件');?></a>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('计划发送时间');?>:</td>
                    <td><input class="easyui-datetimebox" type="text" name="plan_send_time" style="width:255px;"
                               value=""/></td>
                </tr>
                <tr>
                    <td><?= lang('邮件标题');?>:</td>
                    <td><input class="easyui-textbox" type="text" name="mail_title"
                               data-options="required:true" value="<?= $mail_title;?>"  style="width:1100px;"/></td>
                </tr>
                <tr>
                    <td><?= lang('发送人名称');?>:</td>
                    <td><input class="easyui-textbox" type="text" name="sender_name"
                               data-options="required:true" value="<?= $sender_name;?>" style="width:1100px;"/></td>
                </tr>
                <tr>
                    <td><?= lang('邮件正文');?>:</td>
                    <td>
                        <div id="add_content_div" style="margin:0px">
                            <div id="toolbar-container"></div>
                            <div id="editor-container"></div>
                        </div>
                        <textarea name="mail_content" id="add_content_textarea" style="display: none;"
                                  data-options="required:true"><?= $mail_content; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('签名档');?></td>
                    <td>
                        <textarea style="width:1100px;height:80px;" name="signature" id="signature_textarea" ><?= $signature;?></textarea>
                        <!--<div id="signature_div"></div>-->
                        <!--<textarea name="signature" id="signature_textarea" style="display: none;" data-options="required:true"><?= $signature;?></textarea>-->
                    </td>
                </tr>
                <tr>
                    <td> </td>
                    <td>
                        <a href="javascript:void(0)" style="padding: 0 16px 0 16px" class="easyui-linkbutton" onclick="subFf()"><?= lang('下一步预览并审核');?></a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="javascript:history.go(-1);" style="padding: 0 16px 0 16px" class="easyui-linkbutton"><?= lang('返回上一步');?></a>
                    </td>
                </tr>
            </table>
        </form>
        <div style="text-align:left; padding:5px">

        </div>
    </div>
</div>

<div id="group" class="easyui-window" title="Query" closed="true"  style="width:800px;height:600px;overflow: hidden;"  data-options="modal:true,">
    <?php if($search=='crm'):?>
        <iframe style="width:100%;height:100%;"  frameborder="0" src="/crm_promote_mail/add_consignee_group/add_presend_by_search_send_crm?group_id=<?=$recipient_group[0]['id'];?>"></iframe>
    <?php elseif($search=='client'):?>
        <iframe style="width:100%;height:100%;"  frameborder="0" src="/crm_promote_mail/add_consignee_group/add_presend_by_search_send_client?group_id=<?=$recipient_group[0]['id'];?>"></iframe>
    <?php elseif($search=='out'):?>
        <iframe style="width:100%;height:100%;"  frameborder="0" src="/crm_promote_mail/add_consignee_group/out?group_id=<?=$recipient_group[0]['id'];?>"></iframe>
    <?php endif; ?>
</div>

<script type="text/javascript" src="/inc/layer3.1.1/layer.js"></script>
<!-- 引入 css -->
<link type="text/css" href="/inc/css/wangeditor/style.css" rel="stylesheet">
<!-- 引入 js -->
<script type="text/javascript" src="/inc/js/wangeditor/index.js" ></script>
<script type="text/javascript" src="/inc/js/wangeditor/dist/index.js" ></script>
<script>
    const E = window.wangEditor;
    var add_content_div, $add_content_textarea, signature_div, $signature_textarea;
    E.Boot.registerModule(window.WangEditorPluginUploadAttachment.default)

    // E.Boot.registerModule(window.WangEditorPluginUploadAttachment.default);

    // window.editor = E.createEditor({
    //     selector: '#editor-container',
    //     config: {
    //         placeholder: 'Type here...',
    //         hoverbarKeys: {
    //             attachment: {
    //                 menuKeys: ['downloadAttachment'],
    //             },
    //         },
    //         MENU_CONF: {
    //             uploadAttachment: {
    //                 server: '',
    //                 fieldName: 'custom-fileName',
    //                 onInsertedAttachment(elem) {
    //                     console.log('inserted attachment ---- ', elem)
    //                 },
    //             }
    //         }
    //     }
    // })

    $(function () {
        $add_content_textarea = $('#add_content_textarea');
        // 编辑器配置
        const editorConfig = {
            placeholder: '<?= lang('请输入内容');?>',
            onChange(editor){
                // 当编辑器选区、内容变化时，即触发
                const content = editor.children;
                const contentStr = JSON.stringify(content);
                // document.getElementById('textarea-1').value = contentStr
                // console.log(contentStr);


                // const html = editor.getHtml();
                // document.getElementById('textarea-2').value = html
                $add_content_textarea.val(editor.getHtml());
                // console.log('content', editor.children);
                // console.log('html', editor.getHtml())
            },
            hoverbarKeys: {
                attachment: {
                    menuKeys: ['downloadAttachment'],
                },
            },
            MENU_CONF: {
                uploadAttachment: {
                    server: '/bsc_upload/file_upload',
                    fieldName: 'file',
                    meta:{
                        is_save_data: 0,
                    },
                    timeout: 5 * 1000, // 5s
                    // metaWithUrl: true, // meta 拼接到 url 上

                    maxFileSize: 10 * 1024 * 1024, // 10M

                    onBeforeUpload(file) {
                        console.log('onBeforeUpload', file)
                        return file // 上传 file 文件
                        // return false // 会阻止上传
                    },
                    onProgress(progress) {
                        console.log('onProgress', progress)
                    },
                    onSuccess(file, res) {
                        console.log('onSuccess', file, res)
                    },
                    onFailed(file, res) {
                        alert(res.message)
                        console.log('onFailed', file, res)
                    },
                    onError(file, err, res) {
                        alert(err.message)
                        console.error('onError', file, err, res)
                    },
                    // 上传成功后，用户自定义插入文件
                    customInsert(res, file, insertFn) {
                        var url = "https://opchina.oss-cn-shanghai.aliyuncs.com" + res['data'][0]['url'] || {};

                        // 插入附件到编辑器
                        //手动插入
                        var file_name = `${file.name}`;
                        var file_a = "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><a href=\"" + url + "\" target='_blank'>" + file_name + "</a><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                        editor.setHtml(editor.getHtml() + file_a);
                        // editor.setHtml();
                        // insertFn(`${file.name}`, url);
                    },

                    // // 用户自定义上传
                    // customUpload(file: File, insertFn: Function) {
                    //   console.log('customUpload', file)

                    //   return new Promise(resolve => {
                    //     // 插入一个文件，模拟异步
                    //     setTimeout(() => {
                    //       const src = `https://www.w3school.com.cn/i/movie.ogg`
                    //       insertFn(`customUpload-${file.name}`, src)
                    //       resolve('ok')
                    //     }, 500)
                    //   })
                    // },

                    // // 自定义选择
                    // customBrowseAndUpload(insertFn: Function) {
                    //   alert('自定义选择文件，如弹出图床')
                    //   // 自己上传文件
                    //   // 上传之后用 insertFn(fileName, link) 插入到编辑器
                    // },
                },
                uploadImage: {
                    fieldName: 'file',
                    server:'/bsc_upload/file_upload',
                    meta:{
                        is_save_data: 0,
                    },
                    customInsert(res,insertFn){
                        // 图片上传并返回了结果，想要自己把图片插入到编辑器中
                        // 例如服务器端返回的不是 { errno: 0, data: [...] } 这种格式，可使用 customInsert
                        // result 即服务端返回的接口
                        // var url = document.location.protocol + '//' + window.location.host + "/download/read_img?file_path=" + result['data'][0]['url'];
                        var url = "https://opchina.oss-cn-shanghai.aliyuncs.com" + res['data'][0]['url'];

                        // insertImgFn 可把图片插入到编辑器，传入图片 src ，执行函数即可
                        insertFn(url);
                    },
                }
            },
        };
// 工具栏配置
        const toolbarConfig = {
            insertKeys: {
                index: 0,
                keys: ['uploadAttachment'],
            },
        };

// 创建编辑器
        const editor = E.createEditor({
            html: $add_content_textarea.val(),
            selector: '#editor-container',
            config: editorConfig,
            mode: 'default' // 或 'simple' 参考下文
        });
        // editor.dangerouslyInsertHtml();
// 创建工具栏
        const toolbar = E.createToolbar({
            editor,
            selector: '#toolbar-container',
            config: toolbarConfig,
            mode: 'default' // 或 'simple' 参考下文
        })

        return;
        // add_content_div = new E('#add_content_div');
        //
        // add_content_div.config.onchange = function (html) {
        //     // 第二步，监控变化，同步更新到 textarea
        //     $add_content_textarea.val(html)
        // };
        // add_content_div.config.uploadImgServer = '/bsc_upload/file_upload';
        // add_content_div.config.uploadImgParams = {
        //     is_save_data: 0,
        // };
        // add_content_div.config.uploadImgHooks = {
        //     // 图片上传并返回了结果，想要自己把图片插入到编辑器中
        //     // 例如服务器端返回的不是 { errno: 0, data: [...] } 这种格式，可使用 customInsert
        //     customInsert: function (insertImgFn, result) {
        //         // result 即服务端返回的接口
        //         // var url = document.location.protocol + '//' + window.location.host + "/download/read_img?file_path=" + result['data'][0]['url'];
        //         var url = "https://opchina.oss-cn-shanghai.aliyuncs.com" + result['data'][0]['url'];
        //
        //         // insertImgFn 可把图片插入到编辑器，传入图片 src ，执行函数即可
        //         insertImgFn(url);
        //     }
        // };
        // // add_content_div.config.uploadImgShowBase64 = true;
        // // window.toolbar = E.createToolbar({
        // //     add_content_div,
        // //     selector: '#toolbar-container',
        // //     config: {
        // //         insertKeys: {
        // //             index: 0,
        // //             keys: ['uploadAttachment'],
        // //         },
        // //     }
        // // })
        //
        // add_content_div.config.uploadFileName = 'file';
        // add_content_div.create();
        //
        // // 第一步，初始化 textarea 的值
        // // $add_content_textarea.val(add_content_div.txt.html());
        // add_content_div.txt.html($add_content_textarea.val());
    });

</script>
<script>
    function subFf() {
        var d = {};
        var formdata = $("#ff").serializeArray();

        $.each(formdata, function () {
            d[this.name] = this.value;
        });

        $.post("/crm_promote_mail/add_presend_data", formdata, function (data) {
            if (data.code == 200) {
                layer.msg(data.msg);
                setTimeout(function () {
                    window.location.href = data.url;
                }, 2000);

            } else {

                layer.msg(data.msg);

            }

        }, "JSON");
    }
</script>