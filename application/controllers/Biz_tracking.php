<?php

class biz_tracking extends Menu_Controller
{
    public function index(){
        $data['id_type'] = isset($_GET['id_type'])?$_GET['id_type']:'biz_shipment';
        $data['id_no'] = isset($_GET['id_no'])?$_GET['id_no']:0;
        $data['trans_tool'] = '';
        $data['trans_carrier'] = '';
        if($data['id_type'] == 'biz_shipment'){
            $shipment = $this->db->where('id',$data['id_no'])->get('biz_shipment')->row_array();
            if(!empty($shipment)){
                $data['trans_tool'] = $shipment['trans_tool'];
                $data['trans_carrier'] = $shipment['trans_carrier'];
            }
        }
        $this->load->view('head');
        $this->load->view('biz/tracking/index_view',$data);
    }

    public function get_data($id_type='biz_shipment',$id_no=0){

        $where = array();
        $where[] = "id_type = '{$id_type}'";
        $where[] = "id_no = $id_no";
        $where = join(' and ', $where);
        $data = $this->db->where($where)->order_by('id','desc')->get('biz_tracking')->result_array();
        if(empty($data)){
            return jsonEcho(['rows'=>[],'total'=>0]);
        }
        foreach ($data as $k=>$v){
            $port = $this->db->where('port_code',$v['position'])->get('biz_port')->row_array();
            $data[$k]['position'] = !empty($port)?$port['port_name']:$v['position'];
        }
        return jsonEcho(['rows'=>$data,'total'=>count($data)]);
    }

    public function add_data(){
        $field = ['id_no','id_type','remark','node','node_time','position'];
        $param = [];
        foreach ($field as $v){
            if(isset($_POST[$v]) && !empty($_POST[$v])){
                $param[$v] = $_POST[$v];
            }
        }
        if(!isset($param['node']) || !isset($param['node_time']) || !isset($param['position'])){
            return jsonEcho(['code'=>0,'msg'=>'参数错误']);
        }
        $param['created_time'] = $param['updated_time'] = date('Y-m-d H:i:s');
        $this->db->insert('biz_tracking',$param);
        $id = $this->db->insert_id();
        if($id){
            $log_data = array();
            $log_data["table_name"] = "biz_tracking";
            $log_data["key"] = $id;
            $log_data["action"] = "insert";
            $log_data["value"] = json_encode($param);
            log_rcd($log_data);
            return jsonEcho(['code'=>1,'msg'=>'新增成功']);
        }
        return jsonEcho(['code'=>0,'msg'=>'操作失败']);
    }

    public function edit_data(){
        $field = ['id','remark','node','node_time','position'];
        $param = [];
        foreach ($field as $v){
            if(isset($_POST[$v]) && !empty($_POST[$v])){
                $param[$v] = $_POST[$v];
            }
        }
        if(!isset($param['id'])){
            return jsonEcho(['code'=>0,'msg'=>'参数错误']);
        }
        $param['updated_time'] = date('Y-m-d H:i:s');

        $this->db->where('id',$param['id'])->update('biz_tracking',$param);
        $log_data = array();
        $log_data["table_name"] = "biz_tracking";
        $log_data["key"] = $param['id'];
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($param);
        log_rcd($log_data);
        return jsonEcho(['code'=>1,'msg'=>'编辑成功']);

    }

    public function del_data(){
        $id = isset($_POST['id'])?$_POST['id']:'';
        if(empty($id)){
            return jsonEcho(['code'=>0,'msg'=>'参数错误']);
        }
        $old_row = $this->db->where('id',$id)->get('biz_tracking')->row_array();
        $this->db->where('id',$id)->delete('biz_tracking');
        $log_data = array();
        $log_data["table_name"] = "biz_tracking";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
        return jsonEcho(['code'=>1,'msg'=>'删除成功']);

    }

    public function port($id_type='biz_shipment',$id_no=0){
        $data = [];
        if($id_type == 'biz_shipment'){
            $shipment = $this->db->where('id',$id_no)->get('biz_shipment')->row_array();
            if(!empty($shipment)){
                $data = $this->db->where("port_code in ('{$shipment['trans_origin']}','{$shipment['trans_discharge']}','{$shipment['trans_destination']}')")->group_by('port_code')->get('biz_port')->result_array();
            }
        }
        return jsonEcho($data);
    }
}