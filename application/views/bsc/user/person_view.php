<style>

    fieldset {
        width:800px;
        height:750px;
        /*background:url("/inc/image/person_ship.jpg");*/
        background-size:cover;
    }

</style>
<link rel="stylesheet" href="/inc/third/layui/css/layui.css" media="all">
<link id="layuicss-laydate" rel="stylesheet" href="/inc/third/layui/css/modules/laydate/default/laydate.css?v=5.0.9" media="all">
<link id="layuicss-layer" rel="stylesheet" href="/inc/third/layui/css/modules/layer/default/layer.css?v=3.1.1" media="all">
<script src="/inc/third/layui/layui.all.js?v=1" charset="utf-8"></script>
<form action='/bsc_user/person' method="post" id='caozuo' name='caozuo'>
  <div style="padding-left: 80px;padding-top: 20px;margin:10px">
      <h1 ><?= lang('修改个人资料');?> id-<?php echo $user['id'];?></h1>
      <br> 
      <div style="border: #28b3d4 solid 3px;width:500px;"></div>
      <table style="margin-top:10px">
          <tr style="height: 40px">
              <td><?= lang('name');?>:</td>
              <td>
                  <input style="width:245px;" class="easyui-textbox" <?= $button ? '' : 'readonly';?> name="name" id="name" value="<?php echo $user["name"];?>" disabled="disabled">
              </td>
          </tr>
          <tr style="height: 40px">
              <td><?= lang('mobile phone');?>:</td>
              <td>
                  <input style="width:195px;" class="easyui-textbox" <?= $button ? '' : 'readonly';?> name="telephone" id="telephone" value="<?php echo $user["telephone"];?>" >
                  <!--data-options="validType:'phoneNum'"-->
              </td>
          </tr>
          <tr style="height: 40px">
              <td><?= lang('telephone');?>:</td>
              <td>
                  <input style="width:245px;" class="easyui-textbox" <?= $button ? '' : 'readonly';?> name="telephone_desk" id="telephone_desk" value="<?php echo $user["telephone_desk"];?>" >
              </td>
          </tr>
          <tr style="height: 40px">
              <td><?= lang('password');?>:</td>
              <td>
                  <input style="width:245px;" class="easyui-textbox" <?= $button ? '' : 'readonly';?> name="password" id="password" value="" data-options="type:'password'">
              </td>
          </tr>
            <tr style="height: 40px">
              <td><?= lang('email');?>:</td>
              <td>
                  <input style="width:245px;" class="easyui-textbox" <?= $button ? '' : 'readonly';?> name="email" id="email" value="<?php echo $user["email"];?>" data-options="validType:'email'">
              </td>
            </tr>
          <tr style="height: 40px">
              <td><?= lang('qq');?>:</td>
              <td>
                  <input style="width:245px;" class="easyui-textbox" <?= $button ? '' : 'readonly';?> name="qq" id="qq" value="<?php echo $user["qq"];?>" data-options="">
              </td>
          </tr>
          <tr style="height: 40px">
              <td><?= lang('skype');?>:</td>
              <td>
                  <input style="width:245px;" class="easyui-textbox" <?= $button ? '' : 'readonly';?> name="skype" id="skype" value="<?php echo $user["skype"];?>" data-options="">
              </td>
          </tr>
          <tr style="height: 50px">
              <td><?= lang('job_description');?>:</td>
              <td>
                   <textarea  name="job_description" id="job_description" <?= $button ? '' : 'readonly';?> style="height:30px;width:240px;border: 1px solid #95b8e7; border-radius: 5px; outline:none"/><?php echo $user["job_description"];?></textarea>
              </td>
          </tr> 
            <!--<tr style="height: 40px">-->
            <!--  <td><?= lang('email password');?>:</td>-->
            <!--  <td>-->
            <!--      <input style="width:245px;" class="easyui-textbox" name="email_password" id="email_password" value="<?php echo $user["email_password"];?>">-->
            <!--  </td>-->
            <!--</tr>-->
          <tr style="height: 50px">
              <td style="width:130px"><?= lang('signature');?>:</td>
              <td>
                   <textarea  name="signature" id="signature" <?= $button ? '' : 'readonly';?> style="height:60px;width:240px;border: 1px solid #95b8e7; border-radius: 5px; outline:none"/><?php echo $user["signature"];?></textarea>
              </td>
              <td align="left"> 
                  <?php
                  if($button==1){
                      ?>
                      <input id="submit" name="s" type="submit" value='<?= lang('submit');?>' style="margin-left:10px;height:30px;background:#e0ecff;border-radius: 5px; border: 1px solid #95b8e7;padding-left: 20px;padding-right: 20px;color: #515a6e;">
                      <input name='ac' id='ac' type='hidden' value='1'>
                      <?php
                  }
                  ?>
              </td>

          </tr>
      </table>
      <br>
      <div style="border:#28b3d4 solid 1px;width:800px"></div> 
      <?php
      if($button==0) exit();
      $token = $user["id"];
      $three_rand = rand(100,999);  //迷惑性数字，实际是没用的
      $three_rand2 = rand(100,999); //迷惑性数字，实际是没用的
      $time = time();
      $token = $three_rand.$time.$token.$three_rand2;
      $token = base64_encode(base64_encode($token));
      ?>
      <!--临时密码（财务专用）：-->
      <!--<input type="text" name="token" id="token" style="width:505px;border: 1px solid #95b8e7; border-radius: 5px; outline:none;margin-top: 20px" value="<?php echo $token;?>"/></input>-->
      <br><br><br><br><br><br>
  </div>
 </form>
<script type="text/javascript">
    $.extend($.fn.validatebox.defaults.rules, {
        phoneNum: { //验证手机号
            validator: function(value, param){
                return /^1[3-8]+\d{9}$/.test(value);
            },
            message: '<?= lang('请输入正确的手机号码。');?>'
        },
        telNum:{ //既验证手机号，又验证座机号
            validator: function(value, param){
                return /(^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$)|(^(()|(\d{3}\-))?(1[358]\d{9})$)/.test(value);
            },
            message: '<?= lang('请输入正确的电话号码。');?>'
        }
    });
    $('#submit').click(function () {
        if(!$('#caozuo').form('validate')){
            $.messager.alert('<?= lang('Tips');?>','error!');
            return false;
        }
    });
</script>