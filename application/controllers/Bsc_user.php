<?php

class bsc_user extends Common_Controller
{
    public $country_code = 'CN';
    public function __construct()
    {

        parent::__construct();
        $this->load->model('bsc_user_model');
        $this->load->model('m_model');
        $host = explode('.', $_SERVER['HTTP_HOST']);
		$this->country_code = get_system_config("SYSTEM_COUNTRY");
    }

    public $field_default = array(
        // "title",
        // "password",
        // "name",
        // // "menu",
        // "group",
        // "group_range",
        // "user_range",
        // "telephone",
        // "telephone_desk",
        // 'telephone_area_code',
        // "email",
        // "user_role",
        "station",
        // "company",
        // 'status',
        // 'leader_id',
        // 'pan_id',
        // 'special_text',
        // 'job_description',
        // 'email_edm',
        // 'email_password',
        // 'sales_quota',
        // 'points',
        // 'm_level',
        // 'assistant_flag', 
        // 'port_area',
        // 'skype',
        // 'job_title',
        // 'qq'
    );

    public function index()
    {
        // echo lang('请到 国内 系统维护');
        // return;
        //管理员权限才能进
        if(!menu_role('yonghuguanli')) exit("无权查看");
        $station = explode(',', get_session('station'));
        $this->load->model('bsc_user_role_model');
        $data['user_role'] = $this->bsc_user_role_model->get();
        if (is_admin()) {

            $this->db->select('id,user_role');

            $this->load->view('head');
            $this->load->view('bsc/user/admin_index_view', $data);
        } else {
            echo lang("无权查看");
            return;
            //新加个HR可以进
            if(!in_array('16', $station)) exit('无权查看');
            $this->load->view('head');
            $this->load->view('bsc/user/index_view', $data);
        }
    }

    public function get_user_tree2($role = '')
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $limit = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "bsc_user.group";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $more_ids = getValue('more_ids', '');

        $where = array();
        $where[]="bsc_user.status=0";
        //2023-08-01 由于国内国外销售选择这里 要通用, 所以取消了 查询销售时的 下拉框限制
        if($role != 'sales'){
            $where[]="bsc_user.country = '{$this->country_code}'";
        }
        
        if(!empty($role)){
            $where[] = "bsc_user.user_role like '%" . $role . "%'";
        }
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        if(!empty($name)){
            $where[] = "(bsc_user.name like '%{$name}%' or bsc_user.name_en like '%{$name}%')";
        }

        $group = isset($_POST['group']) ? $_POST['group'] : '';
        if(!empty($group)){
            $where[] = "(bsc_group.group_name like '%{$group}%' or bsc_group.group_name_en like '%{$group}%')";
        }

        $where = join(' and ', $where);

        if($more_ids !== '') $where .= " or bsc_user.id in ($more_ids)";

        $result["total"] = $this->db->where($where)->join('bsc_group','bsc_group.group_code=bsc_user.group')->count_all_results('bsc_user');

        $offset = ($page - 1) * $limit;
        $rs=$this->db->select('bsc_user.id,bsc_user.country as user_country,bsc_user.name,bsc_user.name_en,bsc_user.group,bsc_group.group_name,bsc_group.group_name_en,bsc_group.country_code as group_country')->from('bsc_user')->where($where)->join('bsc_group','bsc_group.group_code=bsc_user.group')->limit($limit, $offset)->order_by($sort, $order)->get()->result_array();
         
        $rows = array();
        foreach ($rs as $row){
            $row['other_name'] = "{$row['name']} [{$row['user_country']}]<br/><span style=\"color:#808080\">{$row['name_en']}</span>";
            $row['other_group_name'] = "{$row['group_name']} [{$row['group_country']}]<br/><span style=\"color:#808080\">{$row['group_name_en']}</span>";
            $rows[] = $row;
        }
        
        
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    /**
     * 获取客户相关的销售
     */
    public function get_client_user(){
        $sales_id = getValue('sales_id', '');
        $client_code = getValue('client_code', '');
        $role = getValue('role', '');
        $name = trim(postValue('name', ''));

        $data = array();
        $data[] = array('id' => 0, 'user_id' => 0, 'name' => '-', 'group' => '');

        //下面分为2种情况
        //1、role == 销售，那么去client里获取

        //2、role != 销售, 那么去biz_client_sales_op获取勾选的

        Model('biz_client_model');
//        $this->db->select('id,client_code, sales_ids, operator_ids, customer_service_ids');
        $this->db->select('id,client_code');
        $client = $this->biz_client_model->get_one('client_code', $client_code);
        if(empty($client)) return jsonEcho(array());
        $client = array_merge($client, Model('biz_client_duty_model')->get_client_data_one($client['client_code']));

        $sales_ids = filter_unique_array(explode(',', $client['sales_ids']));
        $user_ids = array();
        //如果只有一个销售,那么将往来客户里的返回
        if(sizeof($sales_ids) == 1){
            $user_ids = filter_unique_array(explode(',', $client[$role . '_ids']));
        }

        //当role为sales时,查询client的sales_ids进行获取
        //新加一个只有1个销售也走这里
        if(($role == 'sales' && !empty($client_code)) || sizeof($sales_ids) == 1){
            //销售只有1个时,这里优先取user_ids
            if(sizeof($sales_ids) == 1){
                $sales_ids = $user_ids;
            }
            if(empty($sales_ids)) $sales_ids[] = '0';
            
            $where = array();
            
            $where[] = 'id in (' . join(',', $sales_ids) . ')';
            // $where[] = "country_code = '{$this->country_code}'";
            if(!empty($name)) $where[] = "name like '%{$name}%'";
            
            $where = join(' and ', $where);

            Model('bsc_user_model');
            $this->db->select('id, name, group');
            $rs = $this->bsc_user_model->get($where);
        }else{
            //如果没有销售ID,那么返回空值
            if(empty($client_code) || empty($sales_id) || empty($role)){
                return jsonEcho($data);
            }
            $where = array();
            $where[] = "sales_id = $sales_id and client_code = '{$client_code}' and role = '$role'";
            if(!empty($name)) $where[] = "bsc_user.name like '%{$name}%'";
            $where = join(' and ', $where);

            //获取数据
            $biz_client_sales_op_model = Model('m_model');
            $biz_client_sales_op_model::$table = 'biz_client_sales_op';

            $this->db->select('user_id as id, bsc_user.name, bsc_user.group');
            $this->db->join('bsc_user', 'bsc_user.id = biz_client_sales_op.user_id', 'LEFT');
            $rs = $biz_client_sales_op_model->get($where);
        }

        foreach ($rs as $row){
            $data[] = $row;
        }

        return jsonEcho($data);
    }

    public function get_option()
    {
        $type = isset($_GET['type']) ? $_GET['type'] : 0;
        $q = trim(postValue('q', ''));
        $q_field = getValue('q', 'name');
        $where = array();
        if($type == 1){
            $where[] = "bsc_user.status = 0";
        }
        $where[] = "bsc_user.country_code = '{$this->country_code}'";
        if(!empty($q)) $where[] = "bsc_user.{$q_field} like '%{$q}%'";
        $where_str = join(' and ', $where);

        $this->db->select("bsc_user.*,bsc_user.country as user_country,bsc_user.name_en,bsc_group.group_name,bsc_group.group_name_en,bsc_group.country_code as group_country");
        $this->db->join("bsc_group", "bsc_group.group_code = bsc_user.group", "LEFT");
        $rs = $this->bsc_user_model->get($where_str);
        $rows = array();
        foreach ($rs as $row) {
            $row['other_name'] = $row['name'];
            $row['otherName'] = $row['name'];
            array_push($rows, $row);
        }

        echo json_encode($rows);

    }

    public function get_data_role($role = '')
    {
        $leader_users = isset($_GET['leader_users']) ? $_GET['leader_users'] : 0;  //如果是1则限制为登录用户的下属
        $userIds = isset($_GET['userIds']) ? $_GET['userIds'] : '';
        $tid = isset($_GET['tid']) ? $_GET['tid'] : '';
        $new_r = isset($_GET['new_r']) ? $_GET['new_r'] : '';
        $otherIds = isset($_GET['otherIds']) ? $_GET['otherIds'] : '';
        $status = getValue('status', '');
        $q = trim(postValue('q', ''));
        $q_field = getValue('q_field', 'name');

        $where = array();
//        $where[] = "bsc_user.country_code = '{$this->country_code}'";
        
        if($userIds !== '') $where[] = "bsc_user.id in ($userIds)";//如果指定了用户ID,那么直接填充进去
        $new_role = array_filter(explode(',', $new_r));
        $new_role[] = $role;
        foreach ($new_role as $key => $val){
//            if($val == 'sales'){
                $new_role[$key] = "bsc_user.user_role like '%$val%'";
//            }else{
//                $new_role[$key] = "bsc_user.user_role like '%$val%' and bsc_user.country_code = '{$this->country_code}'";
//            }
           
        }
        //2022-09-21 由于crm需要获取离职的,这里加入了查询
        if($status === '1') $where[] = "bsc_user.status = 1";
        if($status === '0') $where[] = "bsc_user.status = 0";
        if($role != 'sales'){
           
        }
        if(!empty($tid))$new_role[] = "bsc_user.id in ()";
        if(!empty($otherIds)) $new_role[] = "bsc_user.id in ($otherIds)";
        if($q !== '') $where[] = "bsc_user.{$q_field} like '%{$q}%'";
        $where[] = '(' . join(' or ', $new_role) . ')';
        
        $where_str = join(' and ', $where);
        $this->db->select("bsc_user.*,bsc_user.country as user_country,bsc_user.name_en,bsc_group.group_name,bsc_group.group_name_en,bsc_group.country_code as group_country");
        $this->db->join("bsc_group", "bsc_group.group_code = bsc_user.group", "LEFT");
        $rs = $this->bsc_user_model->get($where_str, 'bsc_user.id');
        $rows = array();
        $rows[] = array('id' => 0, 'name' => '', 'other_name' => '-','group_country' => '', 'group' => '', 'group_name_en' => '', 'name_en' => '', 'user_country' => '');
        $user_range = get_session("user_range");
        
        //获取最长的名字长度, 英文名长度, 英文部门名长度
        foreach ($rs as $row) {
            $row['o_name'] = $row['name'];
            $row['other_name'] = $row['name'];
            $row['otherName'] = $row['name'];
            //2022-06-27 马嬿雯不受离职用户限制限制
            if(!in_array(get_session('id'), array(20061))){
                // if(!empty($row["status"])&&$row["status"] != $status) continue;  //禁止的用户不显示， 2022.8.17 金晶需要开放离职员工查询
                // 离职员工，在查询的地方都应该显示。 在填写的格子，都不应该显示。 需要补充下
            }
            if($leader_users && !in_array($row["id"],$user_range)) continue;
            array_push($rows, $row);
        }
        echo json_encode($rows);
    }

    /**
     * 查看个人信息页面
     * @param int $user_id
     */
    public function person_info($user_id=0)
    {
        if($user_id==0) exit("no user");
        $data['user'] = $this->m_model->query_one("select bsc_user.*,(select group_name from bsc_group where group_code =bsc_user.group) as group_name from bsc_user where id = $user_id");
        $data["button"] = 0;
        $this->load->view('head');
        $this->load->view('bsc/user/person_view', $data);
    }

    /**
     * 个人资料
     */
    public function person()
    {
        $user_id = get_session('id');
        if (!empty($_REQUEST["ac"])) {
            $is_clear = false;//为true就自动下线
            $data = array(
                'telephone' => $_REQUEST['telephone'],
                'telephone_desk' => $_REQUEST['telephone_desk'],
                'job_description' => $_REQUEST['job_description'],
                'email' => $_REQUEST['email'],
                'qq' => $_REQUEST['qq'],
                'signature' => $_REQUEST['signature'],
            );
            $password = trim(postValue('password', ''));
            //如果填写了密码,这里进行加密
            if(!empty($password)) {
                $data['password'] = md5($password);
                $is_clear = true;
            }
            $op_model = Model('op_model');
            $op_model->set_table('bsc_user');
            $id = get_session('id');
            $op_model->update($id, $data);
            
            // record the log
            $log_data = array();
            $log_data["table_name"] = "bsc_user";
            $log_data["key"] = $id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
            
            echo "<script>alert('success！');</script>";
            if($is_clear){
                redirect('main/loginout', 'refresh');
            }else{
                redirect('bsc_user/person');
            }
       
        }
        $data['user'] = $this->m_model->query_one("select bsc_user.*,(select group_name from bsc_group where group_code =bsc_user.group) as group_name from bsc_user where id = " . $user_id);
        $data["button"] = 1;
        $qrcode = $this->db->where(['id_no'=>$data['user']['id'],'biz_table'=>'bsc_user','type_name'=>'名片二维码'])->order_by('id','desc')->get('bsc_upload')->row_array();
        if(!empty($qrcode)){
            $data['qrcode'] = $qrcode['file_name'];
        }else{
            $data['qrcode'] = '';
        }
        $this->load->view('head');
        $this->load->view('bsc/user/person_view', $data);
    }

    //标签页面显示
    public function job_label_tags($id = 0,$is_button=0)
    {
        $data['id'] = $id;
        //根据id获取job_label
        $job_label = $this->db->select('job_label')->where('id', $id)->get('bsc_user')->row_array();
        $job_label = $job_label['job_label'];
        $data['job_label'] = $job_label;
        $data["is_button"] = $is_button;
        $this->load->view('bsc/user/tag.php', $data);
    }

    public function getAllCompanyTree(){
        $id = getValue('id', '');
        if($id != '') return jsonEcho(array());
        $userId = get_session('id');
        //直接获取到分公司的全部数据
        $getUser = getValue('getUser', 'false');
        $children_closed = getValue('children_closed', 'false');

        Model('bsc_group_model');
        $this->db->select('id as idk,parent_id,group_code as id,group_name_en as text,group_code');
        // $where = "group_type = 'HEAD'";
        $where = "group_type = '总部'";
        $companys = $this->bsc_group_model->get($where);

        foreach ($companys as $company){
            $company['children'] = $this->getChildrenGroup($company['idk'], array(), $getUser, $children_closed);
            $company['type'] = 'sub_company';
        }

        return jsonEcho(array($company));
    }

    /**
     * 递归获取所有子group
     */
    private function getChildrenGroup($parent_id, $data = array(), $getUser = 'false', $children_closed = 'false'){
        if($parent_id == 0) return $data;
        //如果处理完了,直接返回值
        $groupRange = filter_unique_array(explode(',', get_session('group_range')));
        //首先查询第一个的父ID是多少
        $this->db->select('id as idk,parent_id,group_code as id,group_name_en as text,group_code');
        $groups = $this->bsc_group_model->get("parent_id = {$parent_id}");// and country_code = '{$this->country_code}'
        if(empty($groups)) return $data;

        foreach ($groups as $key => $group){
            $group['is_use'] = false;
            $group['type'] = 'group';
            if($children_closed === 'true') $group['state'] = 'closed';
            if(in_array($group['id'], $groupRange) || is_admin()) $group['is_use'] = true;

            $group['children'] = $this->getChildrenGroup($group['idk'], array(), $getUser, $children_closed);
            if($getUser === 'true'){
                //需要人员的时候,直接填充
                $user_where = "`group` = '{$group['id']}'";
                if(is_admin()) $user_where = "`group` = '{$group['id']}'";

                Model('bsc_user_model');
                $this->db->select('id, `group`, name_en as text,status');
                $users = $this->bsc_user_model->get($user_where, 'status' , 'asc');
                $group['children'] = array_merge($group['children'], $users);
            }
            $data[] = $group;
        }

        return $data;
    }

    public function get_limit_data()
    {
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $postwhere = isset($_POST['where']) ? $_POST['where'] : array();
        $where = array();
        
        $where[] = "bsc_user.country_code = '{$this->country_code}'";
        //-------$where-------------------------------
        if (isset($postwhere['text']) && $postwhere['text'] != '') $where[] = "name like '%" . $postwhere['text'] . "%'";
        if (isset($postwhere['group']) && $postwhere['group'] != '') $where[] = "group = '" . $postwhere['group'] . "'";

        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->bsc_user_model->total($where);
        $this->db->limit($rows, $offset);
        $rs = $this->bsc_user_model->get($where, $sort, $order);
        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function get_user_tree1()
    {
        $data = array();
        $this->load->model('biz_sub_company_model');
        $this->load->model('bsc_user_model');
        $this->load->model('bsc_group_model');
        $companys = $this->biz_sub_company_model->get();

        foreach ($companys as $row){
            $temp = array();
            $this->db->where("country_code = '{$this->country_code}'");
            $groups = $this->bsc_group_model->get_option($row['company_code']);
            foreach ($groups as $group_row){
                array_push($temp, array('id' => $group_row['group_code'], 'text' => $group_row['group_name']));
            }
            array_push($data, array('id' => $row['company_code'], 'text' => $row['company_name'], "children" => $temp));
        }

        echo json_encode($data);
    }

    /**
     * 获取一条用户数据
     * @param string $id
     */
    public function get_user_one($id = '')
    {
        $user = $this->bsc_user_model->get_one('id', $id);
        if(empty($user)){
            echo json_encode(array());
            return;
        }
        if(empty($user['special_text'])) $user['special_text'] = '';
        $user['station'] = Model("bsc_user_role_relation_model")->get_user_role_ids($user['id']);

        echo json_encode($user);
    }

    /**
     * 获取当前用户的领导,也就是group_range包含当前客户的group的
     */
    public function get_leader_user(){
        $this->db->select('id,name');
        $rs = $this->bsc_user_model->get("user_role not in ('finance_cost', 'finance_sell', 'finance') and bsc_user.country_code = '{$this->country_code}'");

        $data = array();
        foreach ($rs as $row) {
            $data[] = $row;
        }

        return jsonEcho($data);
    }

    public function add_data()
    {
        //2023-08-11 国外关闭新增
        return jsonEcho(array('code' => 1, 'msg' => lang('暂不支持')));
        $field = $this->field_default;
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_REQUEST[$item]) ? $_REQUEST[$item] : '';
            $data[$item] = $temp;
        }
        $this->db->select('id');
        $user = $this->bsc_user_model->get_one('telephone', $data['telephone']);
        if(!empty($user)){
            echo json_encode(array('code' => 1, 'msg' => '手机号已存在'));
            return;
        }
        $this->db->select('id');
        $user = $this->bsc_user_model->get_one('title', $data['title']);
        if(!empty($user)){
            echo json_encode(array('code' => 1, 'msg' => 'code已存在'));
            return;
        }
        $data["password"] = md5($data["password"]);

        //2022-07-11 新增
        $data['title']=time();

        $id = $this->bsc_user_model->save($data);
        
        Model("bsc_user_role_relation_model")->batch_save($id, $data['station']);

        $ur['user_range'] = $id;
        $this->bsc_user_model->update($id, $ur);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "bsc_user";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        $data['id'] = $id;

        echo json_encode(array('code' => 0, 'msg' => '新增成功'));
//        echo json_encode(array(
//            $data
//        ));
    }

    public function update_data()
    {
        $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : 0;
        if ($id == 0) {
            echo "id should not be 0";
            exit;
        }

        $old_row = $this->bsc_user_model->get_one('id', $id);
        $field = $this->field_default;

        $data = array();
        foreach ($field as $item) {
            if(isset($_REQUEST[$item])){
                $temp = $_REQUEST[$item];
                if ($old_row[$item] != $temp)
                    $data[$item] = $temp;
            }
        }
        // $data['telephone'] = isset($data['telephone']) ? $data['telephone'] : $old_row['telephone'];
        // $telephone = $this->bsc_user_model->get_where_one('telephone = \'' . $data['telephone'] . '\' and id != ' . $id);
        // if(!empty($telephone)){
        //     echo json_encode(array('code' => 1, 'msg' => '手机号已存在'));
        //     return;
        // }
        // if (isset($data['password']) && strlen($data["password"]) < 25) {
        //     $data["password"] = md5($data["password"]);
        // }

        // if(isset($data['group_range']) && $data['group_range'] == ''){
        //     $data['user_range'] = $id;
        // }
        
        if(isset($data['station'])){
            Model("bsc_user_role_relation_model")->batch_save($id, $data['station']);
        }

        // $this->bsc_user_model->update($id, $data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "bsc_user";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        $data["id"] = $id;

        echo json_encode(array('code' => 0, 'msg' => lang('修改成功')));

    }

    //获取职业标签
    public function job_label_get()
    {
        $data = $this->db->select('job_label')->where('job_label !=', '')->get('bsc_user')->result_array();
        $data = array_column($data, 'job_label');
        $data = join(",", $data);
        $data = explode(",", $data);
        $data = array_unique($data);
        sort($data);
        echo json_encode($data);
    }

    //修改user的job_label
    public function post_job_label()
    {
        if (!empty($_POST)) {
            $id = $_POST['user_id'];
            $job_label = $_POST['job_label'];
            $data = array(
                'job_label' => $job_label,
            );
            $bl = $this->db->where('id', $id)->update('bsc_user', $data);
            if ($bl) {
                echo json_encode(array('code' => 0, 'msg' => '添加成功'));
            } else {
                echo json_encode(array('code' => 1, 'msg' => '添加失败'));
            }
        }
    }

    /**
     * 根据当前用户获取所能看到的所有权限
     */
    public function get_user_role_tree(){
        $dataType = getValue('dataType', '0');
        $userId = get_session('id');
        $userRange = filter_unique_array(array_merge(get_session('user_range'), array($userId)));
        $groupRange = filter_unique_array(explode(',', get_session('group_range')));

        // 获取所有相关的user_range数据
        $where = "id in (" . join(',', $userRange) . ") and user_role like '%sales%' and bsc_user.country_code = '{$this->country_code}'";
        $this->db->select("id,group,name as text, (select id from bsc_group where bsc_group.group_code = bsc_user.group) as parent_id");
        $users = $this->bsc_user_model->get($where);
        foreach ($users as &$user){
            $user['is_use'] = true;
            $user['type'] = 'user';
        }

        //首先将所有组获取
        Model('bsc_group_model');
        $this->db->select("id as idk,parent_id,group_code as id,group_name as text,group_type");
//        $group_where = "group_code in ('" . join('\',\'', array_column($users, 'group')) . "')";
        // $group_where = "group_type in ('总部', '分公司', '销售部')";
        $group_where = "(level = 1 or (level > 1 and country_code = '{$this->country_code}'))";
        $groups = $this->bsc_group_model->get($group_where);
        //不需要用户的版本

        foreach ($groups as $key => $group){
            if($group['group_type'] == '分公司') $groups[$key]['type'] = 'sub_company';
            else if($group['group_type'] == '总部') $groups[$key]['type'] = 'head';
            else $groups[$key]['type'] = 'group';
            $groups[$key]['is_use'] = false;
            if($group['group_type'] != '总部')$groups[$key]['state'] = 'closed';
            //如果当前用户有组权限,或是管理员,就能使用
            if(in_array($group['id'], $groupRange) || is_admin()) $groups[$key]['is_use'] = true;
            $groups[$key]['children'] = array();
        }
        if($dataType === '0'){
            $groups = array_merge($groups, $users);
        }


        //将相关的部门全部获取
        $data = $this->getGroupTree($groups);

        //把上级


        echo json_encode($data);
    }

    private function getGroupTree($groups, $parent_id = 0){
        $data = array();
        foreach($groups as $key => $group){
            if($group['parent_id'] == $parent_id){
                //用户不处理
                if($group['type'] != 'user'){
                    $group['children'] = $this->getGroupTree($groups,$group['idk']);

                    if(empty($group['children'])) continue;
                    // if(empty($group['children'])) unset($group['children']);
                }
                $data[] = $group;
            }
        }

        return $data;
    }
    
    public function get_user_tree($role='', $filter = false)
    {
        $data = array();
        $this->load->model('biz_sub_company_model');
        $this->load->model('bsc_user_model');
        $this->load->model('bsc_group_model');
        $companys = $this->biz_sub_company_model->get();


        foreach ($companys as $row){
            $temp = array();
            $groups = $this->bsc_group_model->get_option($row['company_code']);
            foreach ($groups as $group_row){
                $user_temp = array();
                //加入新限制, 不能看到自己
                $role_where = ' and id != ' . get_session('id');
                if($role != ''){
                    $role_where = " and user_role like '%" . $role . "%'";
                }
                $users = $this->bsc_user_model->get("group = '" . $group_row['group_code'] . "'$role_where");
                foreach ($users as $user_row){
                    array_push($user_temp, array('id' => $user_row["id"], "text" => $user_row["name"], 'status' => $user_row['status']));
                }
                $user_temp_sort = array_column($user_temp, 'status');
                array_multisort($user_temp_sort, SORT_ASC, $user_temp);
                if($filter) {
                    if(!empty($user_temp))array_push($temp, array('id' => $group_row['group_code'], 'text' => $group_row['group_name'],"children" => $user_temp));
                }else{
                    $tt = array('id' => $group_row['group_code'], 'text' => $group_row['group_name'],  "children" => $user_temp);
                    if(!empty($tt['children']))$tt['state'] = 'closed';
                    array_push($temp, $tt);
                }
            }
            if($filter) {
                if(!empty($temp))array_push($data, array('id' => 'company_' . $row['company_code'], 'text' => $row['company_name'], "children" => $temp));
            }else{
                $tt = array('id' => 'company_' . $row['company_code'], 'text' => $row['company_name'], "children" => $temp);
                if(!empty($tt['children']))$tt['state'] = 'closed';
                array_push($data, $tt);
            }
        }

        echo json_encode($data);
    }

    
    /**
     * OA通讯录模块
     */
    public function address_list()
    {
        $data = $this->db->select('company_name,company_code')->get('biz_sub_company')->result_array();
        //改变company_code
        foreach ($data as $k => $v) {
            $data[$k]['company_code'] = 'company_' . $v['company_code'];
        }
        $data['company'] = $data;
        //标签 2022
        $job_label = $this->db->select('job_label')->where('job_label !=', '')->get('bsc_user')->result_array();
        $job_label = array_column($job_label, 'job_label');
        $job_label = join(",", $job_label);
        $job_label = explode(",", $job_label);
        $job_label = array_unique($job_label);
        sort($job_label);
        $data['job_label'] = $job_label;
        $data['user_role'] = $this->db->where('catalog','user_role')->get('bsc_dict')->result_array();
        $this->load->view('head');
        // $this->load->view('/bsc/user/address_list', $data);
        $this->load->view('/bsc/user/Mail_list', $data);
    }

    public function getNewTree(){
        $data = [
            'id' => '1',
            'text' => 'OA',
            'state' => 'open',
            'url' => '',
            'children' => [
//                [
//                    'id' => '20',
//                    'text' => '公告',
//                    'url' => ' '
//                ],
//                [
//                    'id' => '14',
//                    'text' => '销售技巧',
//                    'url' => '/biz_download_file/catalog?pid=337'
//                ],
//                [
//                    'id' => '13',
//                    'text' => '流程规章',
//                    'url' => '/biz_download_file/catalog?pid=309'
//                ],
//                [
//                    'id' => '14',
//                    'text' => '公司留言板',
//                    'url' => '/bbs_master/index'
//                ],
                [
                    'id' => '11',
                    'text' => lang('我的OA'),
                    'state' => 'open',
                    'url' => '',
                    'children'=>[
//                        [
//                            'id' => '111',
//                            'text' => '个人奖金',
//                            'url' => '',
//                            'children'=>[
//                                // [
//                                //     'id' => '1111',
//                                //     'text' => '传统奖金',
//                                //     'url' => '/report/tc_index',
//                                // ],
//                                // [
//                                //     'id' => '1112',
//                                //     'text' => '补贴上报',
//                                //     'url' => '/bsc_user_commision_additional/kpi_user',
//                                // ],
//                                // [
//                                //     'id' => '1113',
//                                //     'text' => '完成度',
//                                //     'url' => '/biz_crm/crm_set_num_complete/',
//                                // ],
//                                [
//                                    'id' => '1114',
//                                    'text' => '浮动业绩指标',
//                                    'url' => '/bsc_user_commision_additional/index?user_id='.get_session('id'),
//                                    // ],
//                                    // [
//                                    //     'id' => '1115',
//                                    //     'text' => '业务扣款',
//                                    //     'url' => '/bsc_user/other_fee?id='.get_session('id'),
//                                    // ],
//                                    // [
//                                    //     'id' => '1116',
//                                    //     'text' => '滞纳金',
//                                    //     'url' => '/bsc_user/late_fee?id='.get_session('id'),
//                                ],
//                                [
//                                    'id' => '1117',
//                                    'text' => '邮箱验证奖励',
//                                    'url' => '/biz_client_contact/verify_email',
//                                ]
//                            ]
//                        ],
//                        [
//                            'id' => '112',
//                            'text' => '我的审批',
//                            'url' => '/biz_workflow/index',
//                        ],
                        [
                            'id' => '113',
                            'text' => lang('个人资料'),
                            'url' => '/bsc_user/person',
                        ],
//                        [
//                            'id' => '114',
//                            'text' => '福利申请',
//                            'url' => '/biz_client_gift/index',
//                        ],
//                        [
//                            'id' => '117',
//                            'text' => '报销申请',
//                            'url' => '/biz_bill_reimburse/index',
//                        ],
//                        [
//                            'id' => '115',
//                            'text' => '代班设置',
//                            'url' => '/bsc_user_daiban/index',
//                        ],
//                        [
//                            'id' => '116',
//                            'text' => '推送设置',
//                            'url' => '/biz_client_push_config/index?id_type=bsc_user&client_code='.get_session('id'),
//                        ]
                    ]
                ],
                //  [
                //      'id' => '22',
                //      'text' => '团队OA',
                //      'state' => 'open',
                //      'url' => '',
                //      'children'=>[
                //          [
                //              'id' => '221',
                //              'text' => '团队毛利',
                //              'url' => '',
                //          ],
                //          [
                //              'id' => '222',
                //              'text' => '团队完成度',
                //              'url' => '',
                //          ]
                //      ]
                //  ],
//                [
//                    'id' => '15',
//                    'text' => '入职模块',
//                    'state' => 'open',
//                    'url' => '',
//                    'children'=>[
//                        [
//                            'id' => '151',
//                            'text' => '创建账号',
//                            'url' => '/bsc_user/index',
//                        ],
//                        [
//                            'id' => '152',
//                            'text' => '登记办公用品',
//                            'url' => '/bsc_user/device',
//                        ],
//                        [
//                            'id' => '153',
//                            'text' => '入职登记表',
//                            'url' => '/bsc_user/person_info_index',
//                        ],
//                        [
//                            'id' => '154',
//                            'text' => '查看员工手册',
//                            'url' => '/biz_download_file/catalog?pid=309&search=员工手册',
//                        ],
//                        [
//                            'id' => '155',
//                            'text' => '下载签收函',
//                            'url' => '/bsc_user/sign_download',
//                        ]
//                    ]
//                ],
            ]
        ];
//        if(!is_admin() && get_session('id') != 20061){
//            array_shift($data['children'][0]['children']);
//        }
        return jsonEcho(array($data));
    }

    //公司信息
    public function mail_list()
    {
        $title = empty($_GET['title']) ? '' : $_GET['title'];
        $type = empty($_GET['type']) ? '' : $_GET['type'];
        $data = array();
        $data['title'] = isset($title) ? $title : '';
        $data['type'] = isset($type) ? $type : '';
        $this->load->view('head');
        $this->load->view('/bsc/user/address_list', $data);
    }

    public function get_data()
    {
        //新增标题
        $title = getValue('title');
        $type = getValue('type');

        $result = array();
        $page = postValue('page', 1);
        $rows = postValue('rows', 10);
        $sort = postValue('sort', 'id');
        $order = postValue('order', 'desc');
        $status = postValue('status', '0');

        //-------where -------------------------------
        $where = array();
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($f1 == 'group') $f1 = '(select group_name from bsc_group where group_code = `group`)';
        if ($f2 == 'group') $f2 = '(select group_name from bsc_group where group_code = `group`)';
        if ($f3 == 'group') $f2 = '(select group_name from bsc_group where group_code = `group`)';

        if ($v1 != "") $where[] = search_like($f1, $s1, $v1);
        if ($v2 != "") $where[] = search_like($f2, $s2, $v2);
        if ($v3 != "") $where[] = search_like($f3, $s3, $v3);

        $sub_company = isset($_REQUEST['sub_company']) ? $_REQUEST['sub_company'] : '';
        if ($sub_company != "") $where[] = search_like('group', 'like', $sub_company);

        $user_role = isset($_REQUEST['user_role']) ? $_REQUEST['user_role'] : '';
        if ($user_role != "") $where[] = search_like('user_role', 'like', $user_role);
        //判断$type,不为空
        if (!empty($type)) {
            switch ($type) {
                case 1:
                    //判断$title,不为空
                    if (!empty($title)) {
                        //判断是公司还是部门
                        //company_SHYL  SHYL-OPERATOR-01
                        if (strpos($title, '_')) {
                            $title = trim(str_replace("company_", '', $title));
                            $where[] = "bsc_user.group like '{$title}%'";
                        } else {
                            $where[] = "bsc_user.group like '{$title}%'";
                        }
                    }
                    break;
                case 2:
                    //判断$title,不为空
                    //job_label和公司判断
                    if (!empty($title)) {
                        $title = explode(',', $title);
                        $arr = array();
                        $this->load->library('Pinyin');
                        $pingying = new Pinyin();
                        foreach ($title as $k => $v) {
                            //获取新的code
                            if(!is_numeric($v)){
                                // $string = $pingying->get($v, true, true);
                                $bool=$this->db->select('company_code')->where('company_name',$v)->get('biz_sub_company')->row_array();
                                if(!empty($bool)){
                                    $string=$bool['company_code'];
                                    $arr[] = "bsc_user.group like '{$string}%'";
                                }

                            }
                            $v = trim($v);
                            $arr[] = "bsc_user.job_label like '%{$v}%'";
                            $arr[] = "bsc_user.name like '%{$v}%'";
                            $arr[] = "bsc_user.telephone like '{$v}%'";
                            $arr[] = "bsc_user.id like '{$v}'";
                            $arr[] = "bsc_user.email like '%{$v}%'";
                            $arr[] = "bsc_user.name_en like '%{$v}%'";
                            $arr[] = "bsc_user.user_role like '%{$v}%'";
                        }
                        if (!empty($arr)) $where[] = '(' . join(' or ', $arr) . ')';
                    }
                    break;
            }
        }
        if($status != '')$where[] = 'status = ' . $status;
        $where = join(' and ', $where);
        $offset = ($page - 1) * $rows;
        $result["total"] = $this->bsc_user_model->total($where);
        $this->db->limit($rows, $offset);
        //country
        $this->db->select('id,qq,name,name_en,group as group_code,(select group_name from bsc_group where group_code = `group`) as `group`,email,telephone_desk,telephone,user_role,job_description,job_label');
        $rs = $this->bsc_user_model->get($where, $sort, $order);
        $rows = array();

        foreach ($rs as $row) {
            $sub_company = $this->db->query("select group_name from bsc_group where id = (select parent_id from bsc_group where group_code = '{$row["group_code"]}')")->row_array();
            $row["sub_company"] = !empty($sub_company['group_name'])?$sub_company['group_name']:"";
            $photo = $this->db->where(['biz_table'=>'bsc_user','type_name'=>'入职登记表照片','id_no'=>$row['id']])->order_by('id','desc')->get('bsc_upload')->row_array();
            $row["photo"] = empty($photo)?'':"<img src='{$photo['file_name']}' onclick='zoom_in(this)' width='50px'>";
            if($row["photo"] != '' && (is_admin() || in_array('16', explode(',',get_session('station'))))){
                $row["photo"] .= "<span style='color: green;font-size:16px;' onclick='del_photo({$row['id']})'>删</span>";
            }
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    /**
     * 个人信息
     */
    public function list_details()
    {
        $title = empty($_GET['title']) ? '' : $_GET['title'];
        $data = array();
        $title = isset($title) ? $title : '';
        if (is_numeric($title)) {
            $data = $this->db->where('id', $title)->get('bsc_user')->row_array();
        } else {
            $data = $this->db->where('name', $title)->get('bsc_user')->row_array();
        }
        $station = '';
		//2024-01-12 通讯录这里没用使用station的读取, 导致显示错误
		$data['station'] = Model("bsc_user_role_relation_model")->get_user_role_ids($data['id']);

		$quaqnxian = explode(',',$data['station']);
        foreach ($quaqnxian as $v){
            if(!empty($v)){
                $station .= $this->db->where('id',$v)->get('bsc_user_role')->row_array()['user_role'].',';
            }
        }
        $data['quanxian'] = rtrim($station,',');
        //查看是否有在代班
        $data['from_user_name'] = "";
        $data['from_user_id'] = "";
        $daiban=$this->db->where('to_user_id',$data['id'])->where('status',1)->get('bsc_user_daiban')->row_array();
        if(empty($daiban)){
            //2022-08-22 新增代班，自己被代班也显示
            $daiban2=$this->db->where('from_user_id',$data['id'])->where('status',1)->get('bsc_user_daiban')->row_array();

            if(empty($daiban2)){
                $data['daiban']=0;
            }else{
                $data['daiban']=2;
                //当前账户被谁代班
                $name_2=$this->db->select('name')->where('id',$daiban2['to_user_id'])->get('bsc_user')->row_array();
                $data['from_user_id'] = $daiban2['from_user_id'];

                $data['to_user_name'] = $name_2['name'];
                $data['to_user_id'] = $daiban2['to_user_id'];
            }
        }else{
            $data['daiban']=1;
            // 提示当前为谁代班
            $name_1=$this->db->select('name')->where('id',$daiban['from_user_id'])->get('bsc_user')->row_array();
            $data['from_user_name'] = $name_1['name'];
            $data['from_user_id'] = $daiban['from_user_id'];
            $data['to_user_id'] = $daiban['to_user_id'];
        }

        //是否需要设置销售团队提成
//        $bsc_user_commision_leader = $this->db->where('user_id',$data["id"])->get('bsc_user_commision_leader')->row_array();
//        // print_r($bsc_user_commision_leader);
//        if(!empty($bsc_user_commision_leader)) $data["commision_rate_leader"] = 1;
        $upload = $this->db->where(array('biz_table'=>'bsc_user','type_name'=>'入职登记表照片','id_no'=>$data['id']))->order_by('id','desc')->get('bsc_upload')->row_array();
        if(!empty($upload)){
            $data['img'] = $upload['file_name'];
        }else{
            $data['img'] = '';
        }
//        $quotation_market = $this->db->select('*')->where('user_id', $data['id'])->get('biz_quotation_market')->row_array();
//        $this->load->vars(array('market'=>$quotation_market));
        $this->load->vars(array('market'=>array()));
        $this->load->view('head');
        $this->load->view('/bsc/user/list_details', $data);
    }

    // xmind格式显示上下级领导关系
    public function xmind_leader(){
        $this->load->view('bsc/user/xmind_leader');
    }
    public function xmind_leader_get_data(){
        $sql="SELECT id,name,leader_id FROM bsc_user WHERE `id`=20057 and `status` = 0";
        $rs = $this->m_model->query_array($sql);
        $data=array();
        foreach ($rs as $k=>$v){
            $data[]=array(
                "id"=>$v['id'],
                "isroot"=>true,
                "topic"=>$v['name']
            );
            $this->findchild($v["id"],$data);
        }

        $mind=array(
            "meta"=>array(
                "name"=>"duo",
                "author"=>"x@163.com",
                "version"=>"0.2",
            ),
            "format"=>"node_array",
            "data"=>$data

        );

        echo json_encode($mind);
    }
    public function findchild($leader_id,&$data){
        $sql="SELECT id,name,leader_id FROM bsc_user WHERE `leader_id`='{$leader_id}' and `status` = 0";
        $rs = $this->m_model->query_array($sql);
        if(!empty($rs)){
            foreach ($rs as $k=>$v){
                //var_dump($v);
                $data[]=array(
                    "id"=>$v['id'],
                    "parentid"=>$v['leader_id'],
                    "topic"=>$v['name'],
                    "expanded"=>false,
                );
                $this->findchild($v["id"],$data);
            }

        }
    }
    // xmind格式显示上下级bm关系---找小姜
    public function xmind_leader_group(){
        $this->load->view('bsc/user/xmind_leader_group');
    }
    public function xmind_leader_group_get_data(){
        $sql="SELECT id,group_name,parent_id FROM bsc_group WHERE level = 1 and `status` = 0";
        $rs = $this->m_model->query_array($sql);
        $data=array();
        foreach ($rs as $k=>$v){
            $data[]=array(
                "id"=>$v['id'],
                "isroot"=>true,
                "topic"=>$v['group_name']
            );
            $this->findchild_group($v["id"],$data);
        }

        $mind=array(
            "meta"=>array(
                "name"=>"duo",
                "author"=>"x@163.com",
                "version"=>"0.2",
            ),
            "format"=>"node_array",
            "data"=>$data

        );

        echo json_encode($mind);
    }
    public function findchild_group($parent_id,&$data){
        $sql="SELECT id,group_name,parent_id FROM bsc_group WHERE `parent_id`='{$parent_id}'";
        $rs = $this->m_model->query_array($sql);
        if(!empty($rs)){
            foreach ($rs as $k=>$v){
                //var_dump($v);
                $data[]=array(
                    "id"=>$v['id'],
                    "parentid"=>$v['parent_id'],
                    "topic"=>$v['group_name'],
                    "expanded"=>false,
                );
                $this->findchild_group($v["id"],$data);
            }

        }
    }
}
