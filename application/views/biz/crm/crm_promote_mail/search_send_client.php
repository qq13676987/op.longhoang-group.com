<meta charset="UTF-8">
<style>
    .query_total{
        font-size: 20px;
    }
    .query_next{
        color: red;
        font-size: 20px;
    }
</style>
<body class="easyui-layout" data-options="fit: true" style="padding: 0px;">
<div>
    <form action="/crm_promote_mail/add_presend_by_search_send_client" method="POST" id="search_form">
        <input type="hidden" name="search_type" value="1">

        <div class="form_div search_form" style="padding-top: 150px;width: 100%">
            <div style="padding-bottom: 20px; margin-left: 500px;">
                <h1> 1、请设定您要查询的范围  </h1>
                <div class="query_next hide">
                    搜索到 <span class="query_total">0</span> 个收件人&nbsp;
                    <button type="button query_button" form_id="search_form">点击生成收件人组</button>
                </div>
            </div>
            <div style="width:50%;margin-left: 500px;">
                <table>
                    <tr>
                        <td><?= lang('客户简称'); ?> </td>
                        <td>
                            <input class="easyui-textbox" name="field[client_name]" id="client_name" style="width:249px;"/>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('客户地区');?></td>
                        <td>
                            <select class="easyui-combobox" name="field[province]" id="province"  data-options="
                                    valueField:'cityname',
                                    textField:'cityname',
                                    url:'/city/get_province',
                                    onSelect: function(rec){
                                        if(rec != undefined){
                                            $('#city').combobox('reload', '/city/get_city/' + rec.id);
                                        }
                                    }," style="width: 80px;"></select>

                            <select class="easyui-combobox" name="field[city]" id="city" style="width: 80px;" data-options="
                                        valueField:'cityname',
                                        textField:'cityname',
                                        onSelect: function(rec){
                                            if(rec != undefined){
                                                $('#area').combobox('reload', '/city/get_area/' + rec.id);
                                            }
                                        },
                                        onLoadSuccess:function(){
                                            var this_data = $(this).combobox('getData');
                                            var this_val = $(this).combobox('getValue');
                                            var rec = this_data.filter(el => el['cityname'] === this_val);
                                            if(rec.length > 0)rec = rec[0];
                                            $('#area').combobox('reload', '/city/get_area/' + rec.id);
                                        }
                                    "></select>

                            <select class="easyui-combobox" name="field[area]" id="area" style="width: 80px;" data-options="
                                        valueField:'cityname',
                                        textField:'cityname',
                                    "></select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right;padding-top: 20px"><button type="button" onclick="query_total('search_form')">点击提交到下一步</button></td>
                    </tr>
                </table>
            </div>
        </div>
    </form>
</div>

</body>
<script>

    //点击查一查
    function query_total(form_id) {
        ajaxLoading();
        $('#' + form_id).form('submit', {
            url:'/crm_promote_mail/add_presend_by_search_send_client?get_total=true',
            success:function (res_json) {
                var res;
                ajaxLoadEnd();
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {
                    $.messager.alert('<?= lang('提示');?>', res_json);
                    return;
                }
                //$.messager.alert('<?//= lang('提示');?>//', res.msg);
                if(res.code == 0){
                    //提示存在多少条,并出现下一步按钮,点击进入总数页面
                    query_total_show(res['data']['total'], true);
                }else{
                }
            }
        });
    }

    //控制展示文本框的
    function query_total_show(total = 0, is_show = false) {
        $('.query_next').addClass('hide');
        $('.query_total').text(0);
        if(is_show){
            $('.query_total').text(total);
            $('.query_next').removeClass('hide');
        }else{

        }
    }

    //点击查一查
    function query(form_id) {
        $('#' + form_id).submit();
    }


    $(function () {

        $('.query_button').on('click', function () {
            var form_id = $(this).attr('form_id');
            query(form_id);
        });
    });
</script>
</html>
