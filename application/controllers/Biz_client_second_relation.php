<?php

/**
 * Class biz_client_second_relation
 * 该控制器用于client关联的开票用户,实际影响代码在biz_client/get_bill_client
 */
class biz_client_second_relation extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->model = Model('biz_client_second_relation_model');
    }

    public function index(){
        $data =  array();
        $data['client_code'] = getValue('client_code', '');

        $this->load->view('head');
        $this->load->view('/biz/client/second/relation/index_view', $data);
    }

    /**
     * 新增页面
     */
    public function add(){
        $data = array();
        $data['client_code'] = getValue('client_code', '');
        if(empty($data['client_code'])) exit('参数错误');
        $this->load->view('head');
        $this->load->view('/biz/client/second/relation/add_view', $data);
    }

    /**
     * 往来单位修改页面的关联显示数据
     */
    public function get_data(){
        $result = array('rows' => array(), 'total' => 0);
        $client_code = getValue('client_code', '');

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        if(empty($client_code)) return jsonEcho($result);

        $where = array();

        $where[] = "client_code = '$client_code'";

        $where = join(' and ', $where);

        $result["total"] = $this->model->total($where);

        $offset = ($page - 1) * $rows;
        $this->db->limit($rows, $offset);
        $this->db->select('id,is_share_billing, (select company_name from biz_client where biz_client.client_code = biz_client_second_relation.relation_client_code) as relation_client_code_name');
        $rs = $this->model->get($where, $sort, $order);

        foreach ($rs as $row){
            $result['rows'][] = $row;
        }

        return jsonEcho($result);

    }

    /**
     * 删除一条开票关联
     */
    public function delete_data(){
        $id = (int)postValue('id', 0);

        $result = array('success' => false);

        //查询数据
        $old_row = $this->model->get_one('id', $id);
        if(empty($old_row)) return jsonEcho($result);

        $this->model->mdelete($id);
        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_client_second_relation";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);

        $result['success'] = true;
        return jsonEcho($result);
    }
    /**
     * 获取数据接口
     */
	public function serachClientData($type=1){
		if(isset($_REQUEST['ext1']) && !empty($_REQUEST['ext1'])){
			$sql = "select id, client_code, company_name from biz_client where id in (SELECT id_no FROM `biz_tag_relation` where tag_id = {$_REQUEST['ext1']} and id_type = 'biz_client')";
			$rs = $this->db->query($sql)->result_array();
			return jsonEcho($rs);
		}
		$company_name = trim((string)postValue('company_name', ''));
		$company_search = match_chinese($company_name);
		$client_code = trim(postValue('client_code', ''));

		//先精确查询，无结果再模糊查询。
		$sql = "select id, client_code, company_name from biz_client where client_level = 1 and (company_search = '{$company_search}' or client_name = '{$company_name}')";
		$rs = $this->db->query($sql)->result_array();
		if(!empty($rs)){
			array_unshift($rs,['client_code' => ' ', 'company_name' => '-']);
			return jsonEcho($rs);
		}

		$datas = array();

		//定义需要传过去的字段
		$field = array('id', 'company_name', 'client_code','company_address','contact_telephone','contact_email');

		//如果参数为空,那么返回也为空
		if($company_name === '' && $client_code == '' && !isset($_GET['value'])) return jsonEcho(array(array('client_code' => ' ', 'company_name' => '-')));

		//查询数据
		Model('biz_client_model');
		$where = array();
		$where[] = "client_level = 1";
		if($company_search != ''){
			$where[] = "(company_search like '%{$company_search}%' or client_name like '%{$company_name}%')";
		}
		if($client_code != ''){
			$where[] = "client_code like '%{$client_code}%'";
		}
		$bsc_dict = Model('bsc_dict_model')->get_option('role', "ext1 = '1'");
//        $bsc_dict = $this->db->where(['catalog'=>'role','ext1'=>1])->get('bsc_dict')->result_array();
		$where_or = [];
		foreach ($bsc_dict as $k=>$v){
			$where_or[] = "role like '%{$v['value']}%'";
		}
		$where_or[] = "role = ''";
		$where[] = "(".join(' or ',$where_or).")";
		$where = join(' and ', $where);

		//限制只显示3条,如果出现4条,那么提示查询超过3条
		$limit = 6;
		$this->db->limit($limit);
		$this->db->select($field);
		$rs = $this->biz_client_model->no_role_get($where);
		$datas = array();
		if(sizeof($rs) > 5){
			$datas[] = array('id' => 0, 'company_name' => '客户类条数不能超过3条');
		}else{
			foreach ($rs as $row){
				$datas[] = $row;
			}
		}
		if($type == 1){
			//查询供应商
			Model('biz_client_model');
			$where = array();
			$where[] = "client_level = 1";
			if($company_search != ''){
				$where[] = "(company_search like '%{$company_search}%' or client_name like '%{$company_name}%')";
			}
			if($client_code != ''){
				$where[] = "client_code like '%{$client_code}%'";
			}
			//只要报关行的
			if(isset($_GET['value']) && $_GET['value'] == 'declaration'){
				$datas = [];
				$where[] = "role like '%declaration%'";
			}else{
				//这里改为从字典表取值。 上面客户类也一样
				$bsc_dict = Model('bsc_dict_model')->get_option('role', "ext1 = '2'");
//                $bsc_dict = $this->db->where(['catalog'=>'role','ext1'=>2])->get('bsc_dict')->result_array();
				$where_or = [];
				foreach ($bsc_dict as $k=>$v){
					$where_or[] = "role like '%{$v['value']}%'";
				}
				$where[] = "(".join(' or ',$where_or).")";
			}
			$where = join(' and ', $where);

			$limit = 666;
			$this->db->limit($limit);
			$this->db->select($field);
			$rs = $this->biz_client_model->no_role_get($where);
			foreach ($rs as $row){
				$datas[] = $row;
			}
		}

		array_unshift($datas,['client_code' => ' ', 'company_name' => '-']);
		return jsonEcho($datas);
	}
    public function get_golbal_data(){
        $company_name = trim((string)postValue('company_name', ''));
        $company_search = match_chinese($company_name);
        $client_code = trim(postValue('client_code', ''));

        $datas = array();

        //定义需要传过去的字段
        $field = array('id', 'company_name', 'client_code');

        //如果参数为空,那么返回也为空
        if($company_name === '' && $client_code == '') return jsonEcho(array(array('client_code' => ' ', 'company_name' => '-')));

        //查询数据
        Model('biz_client_model');
        $where = array();
        if($company_search != ''){
            $where[] = "(company_search like '%{$company_search}%' or client_name like '%{$company_name}%')";
        }
        if($client_code != ''){
            $where[] = "client_code like '%{$client_code}%'";
        }
        $where = join(' and ', $where);

        //限制只显示3条,如果出现4条,那么提示查询超过3条
        $limit = 6;
        $this->db->limit($limit);
        $this->db->select($field);
        $rs = $this->biz_client_model->no_role_get($where);
        $datas = array();
        if(sizeof($rs) > 5){
            $datas[] = array('id' => 0, 'company_name' => '客户类条数不能超过3条');
        }else{
            foreach ($rs as $row){
                $datas[] = $row;
            }
        }
        array_unshift($datas,['client_code' => ' ', 'company_name' => '-']);
        return jsonEcho($datas);
    }
    /**
     * 新增数据
     */
    public function add_data(){
        $client_code = getValue('client_code', '');
        $relation_client_code = postValue('relation_client_code', '');

        $result = array('code' => 1, 'msg' => '参数错误');

        //参数不能为空
        if(empty($client_code) || empty($relation_client_code)) return jsonEcho($result);

        //2个clientcode必须数据库里有值,
        Model('biz_client_model');
        //此处用不走权限的no_role_get获取
        $this->db->select('id');
        $clients = $this->biz_client_model->no_role_get("client_code in ('" . $client_code . "', '" .$relation_client_code. "')");
        if(sizeof($clients) != 2){
            $result['msg'] = '二级委托方不能与当前客户相同';
            return jsonEcho($result);   
        }

        //如果有关联过,提示已关联
        $relation = $this->model->get_one_client_second_relation($client_code, $relation_client_code);
        if(!empty($relation)){
            $result['msg'] = '当前客户已关联';
            return jsonEcho($result);
        }

        //如果未关联,关联
        $this->model->save($client_code, $relation_client_code);

        $result['code'] = 0;
        $result['msg'] = '关联成功!!';
        return jsonEcho($result);
    }
    
    /**
     * 获取客户相关的二级客户
     */
    public function getSecondByClient(){
        $client_code = getValue('client_code', ''); //关联code

        $post_client_code = trim(postValue('client_code', ''));
        $post_company_name = trim(postValue('company_name', ''));

        $data = array('rows' => array(), 'total' => 0);

        $where =array();
        if($post_client_code !== '') $where[] = "biz_client.client_code = '{$post_client_code}'";
        if($post_company_name !== '') $where[] = "biz_client.company_name = '{$post_company_name}'";
        $where_str = join(' and ', $where);

        //默认填充
        $this->db->join('biz_client', 'biz_client.client_code = biz_client_second_relation.relation_client_code', 'left');
        $this->db->select('biz_client.client_code,biz_client.company_name');
        if($where_str !== '') $this->db->where($where_str);
        $relations = $this->model->get_client_second_relation($client_code);

        $this->db->select('client_code,company_name');
        $client = Model('biz_client_model')->get_one('client_code', $client_code);
        if(empty($client)) $client = array('client_code' => '', 'company_name' => '');

        $data['rows'][] = $client;

        foreach ($relations as $relation){
            $data['rows'][] = $relation;
        }

        return jsonEcho($data);
    }

    //bill权限
    public function is_share_billing_change(){
        $data=array(
            'is_share_billing'=>$_POST['is_share_billing'],
        );
        $bl=$this->db->where('id',$_POST['id'])->update('biz_client_second_relation', $data);
	// record the log
	$log_data = array();
	$log_data["table_name"] = "biz_client_second_relation";
	$log_data["key"] = $_POST['id'];
	$log_data["action"] = "update";
	$log_data["value"] = json_encode($data);
	log_rcd($log_data);
        if($bl){
            echo json_encode(array('msg'=>'修改成功'));
        }else{
            echo json_encode(array('msg'=>'修改失败'));
        }

    }
}
