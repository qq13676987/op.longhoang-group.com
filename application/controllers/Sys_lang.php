<?php


class Sys_lang extends Menu_Controller
{
    protected $menu_name = '';//菜单权限对应的名称
    protected $method_check = array(//需要设置权限的方法
        'index',
        'add_data',
        'update_data',
        'delete_data',
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sys_lang_model');
        $this->model = $this->sys_lang_model;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }


    public $field_edit = array();

    public $field_all = array(
        array('id', 'id', '80', '1', 'sortable="true"'),
        array('page_view', 'page_view', '400', '4', 'editor="textbox"'),
        array('cn', 'cn', '400', '3', 'editor="textbox"'),
        array('en', 'en', '400', '6', 'editor="textbox"'),
    );

    public function index($carrier = '')
    {
        $data = array();

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_port_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }
        // page account
        $rs = $this->sys_config_model->get_one('biz_port_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }
        $data["carrier"] = $carrier;
        $this->load->view('head');
        $this->load->view('sys/lang/index_view', $data);
    }

    public function get_data($carrier = '')
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = array();
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($v1 != "") $where[] = search_like($f1, $s1, $v1);
        if ($v2 != "") $where[] = search_like($f2, $s2, $v2);
        if ($v3 != "") $where[] = search_like($f3, $s3, $v3);
        if($carrier != "") $where[] = "carrier = '$carrier'";
        $where = join(' and ', $where);
        //-----------------------------------------------------------------
        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
//        $this->db->select('*, (select client_name from biz_client where client_code = carrier) as carrier_name,(select pre_alert from biz_port_country_sailing where SUBSTR(port_code, 1,2) = biz_port_country_sailing.country_code) as pre_alert');
        $rs = $this->model->get($where, $sort, $order);
        $rows = array();
        foreach ($rs as $row) {

            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data($carrier = '')
    {
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? trim(($_POST[$item])) : '';
            $data[$item] = $temp;
        }
        $id = $this->model->save($data);
        $data['id'] = $id;

        echo json_encode($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "sys_lang";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function update_data()
    {
        $id = $_REQUEST["id"];
        $old_row = $this->model->get_one('id', $id);

        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? trim(($_POST[$item])) : '';
            if ($old_row[$item] != $temp)
                $data[$item] = $temp;
        }
        $id = $this->model->update($id, $data);
        $data['id'] = $id;
        echo json_encode($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "sys_lang";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->model->get_one('id', $id);
        $this->model->mdelete($id);

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "sys_lang";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
        return jsonEcho(array('code' => 0, 'msg' => '删除成功'));
    }

    public function delete_datas(){
        $ids = $_REQUEST['ids'];
        $ids_array = array_filter(explode(',', $ids));
        if(empty($ids_array)) return jsonEcho(array('code' => 1, 'msg' => '请选择一行后再试'));

        $delete_datas = array();

        $rs = $this->model->get("id in ('" . join('\',\'', $ids_array) . "')");
        foreach ($rs as $row){
            if(empty($row)){
                continue;
            }

            $delete_datas[] = $row;
        }

        foreach ($delete_datas as $delete_data){
            $id = $delete_data['id'];
            $this->model->mdelete($id);

            //save the operation log
            $log_data = array();
            $log_data["table_name"] = "sys_lang";
            $log_data["key"] = $id;
            $log_data["action"] = "delete";
            $log_data["value"] = json_encode($delete_data);
            log_rcd($log_data);
        }
        return jsonEcho(array('code' => 0, 'msg' => '删除成功,已删除' . sizeof($delete_datas) . '条数据'));
    }

    /**
     * 设置语言包
     */
    public function setlang(){
        $lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
        setcookie('lang', $lang, time() + 3600 * 24 * 180, '/');
        echo json_encode(array('code' => 0, 'msg' => lang('Setup succeeded')));
    }

    public function create_lang(){
        $str = "<?php \n";
        $str .= "\$lang = array();\n";

        $sql = "SELECT distinct page_view FROM `sys_lang` ";
        $rs = Model('m_model')->query_array($sql);
        foreach ($rs as $row){
            $page_view = $row["page_view"];
            $str .= "\$lang['$page_view'] = array();\n";

            $sql = "SELECT cn,en FROM `sys_lang` where page_view = '$page_view'";
            $rs2 = Model('m_model')->query_array($sql);
            foreach ($rs2 as $row2){
                $str .= "\$lang['$page_view']['" . str_replace("'", "\'", $row2['cn']) . "'] = '" . str_replace("'", "\'", $row2['en']) . "';\n";
            }
        }
        file_put_contents("application/language/english/en_lang.php",$str);
        echo "ok111";
    }
}