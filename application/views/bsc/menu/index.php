<style>
    .input{
        width: 255px;
    }
    .textarea{
        width: 255px;
    }
    .select{
        width: 255px;
    }
</style>
<script type="text/javascript">
    function save_tree(){

    }

    /**
     * 打开新增页面
     */
    function add(){
        var form_data = {},order = 0;
        form_data.parent_id = selected_node.id;
        //新增节点的时候,order取当前最后的order+1
        var children = selected_node.children;
        if(children == undefined) children = {};
        var children_length = children.length;
        if(children_length > 0) order = selected_node.children[selected_node.children.length - 1].order;
        form_data.order = parseInt(order) + 1;
        $('#add_form').form('load', form_data);
        $('#add_div').window('open').window("resize",{top:$(document).scrollTop() + ($(window).height()-260) * 0.5});
    }

    /**
     * 保存新增数据
     */
    function add_save() {
        $.messager.confirm('<?= lang('提示');?>', '是否确认提交?', function (r) {
            if(r){
                $('#add_form').form('submit', {
                    url:'/bsc_menu/add_data',
                    onSubmit: function(){
                        var isValid = $(this).form('validate');
                        return isValid;    // 返回false终止表单提交
                    },
                    success: function(res_json){
                        var res;
                        try {
                            res = $.parseJSON(res_json);
                        }catch (e) {

                        }
                        if(res == undefined){
                            $.messager.alert(res_json);
                            return;
                        }
                        $.messager.alert('<?= lang('提示');?>', res.msg);
                        if(res.code == 0){
                            $('#tt').tree('reload');
                            $('#add_div').window('close');
                        }
                    },
                });
            }
        });
    }

    /**
     * 打开修改页面
     */
    function update(){
        $('#update_form').form('load', selected_node);
        $('#update_div').window('open').window("resize",{top:$(document).scrollTop() + ($(window).height()-260) * 0.5});
    }

    /**
     * 保存修改数据
     */
    function update_save() {
        $.messager.confirm('<?= lang('提示');?>', '是否确认提交?', function (r) {
            if(r){
                $('#update_form').form('submit', {
                    url:'/bsc_menu/update_data',
                    onSubmit: function(){
                        var isValid = $(this).form('validate');
                        return isValid;    // 返回false终止表单提交
                    },
                    success: function(res_json){
                        var res;
                        try {
                            res = $.parseJSON(res_json);
                        }catch (e) {

                        }
                        if(res == undefined){
                            $.messager.alert(res_json);
                            return;
                        }
                        $.messager.alert('<?= lang('提示');?>', res.msg);
                        if(res.code == 0){
                            $('#tt').tree('reload');
                            $('#update_div').window('close');
                        }
                    },
                });
            }
        });
    }

    /**
     * 删除数据
     */
    function del(){
        $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认删除该目录?(可能会导致该目录无法使用)');?>', function (r) {
            if(r){
                $.ajax({
                    type:'POST',
                    url:'/bsc_menu/delete_data',
                    data:{
                        id: selected_node.id,
                    },
                    dataType:'json',
                    success:function (res) {
                        $.messager.alert('<?= lang('提示');?>', selected_node.name + res.msg);
                        if(res.code == 0){
                            var node = $('#tt').tree('find', selected_node.id);
                            $('#tt').tree('remove', node.target);
                        }
                    },
                    error:function (e) {
                        console.log(e);
                        $.messager.alert('<?= lang('提示');?>', '错误');
                    }
                });
            }
        });

    }

    var selected_node = {};

    $(function () {
        $('#tt').tree({
            url:'/bsc_menu/get_tree',
            dnd:true,
            formatter:function(node){
                var str = '';
                if(node.display == 1){
                    str = node.name;
                }else{
                    str = '<span style="color: #666666;">' + node.name + '</span>';
                }
                if(node.is_menu == 1){
                    str += '<span style="color: red;">*</span>';
                }
                return str;
            },
            onContextMenu:function(e, node){
                e.preventDefault();
                // 查找节点
                selected_node = node;
                $('#tt').tree('select', node.target);
                // 显示快捷菜单
                $('#mm').menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
            },
            onDblClick:function(node){
                selected_node = node;
                update();
            },
            onDrop:function (target, node, point) {
                //移动进的节点
                //被移动的节点
                var order,p_node;

                if(point == 'append'){
                    var p_node = $('#tt').tree('getNode', target);
                    $.each(p_node.children, function (i,it) {
                        if(it.id == node.id){
                            order = i;
                        }
                    });
                }else if(point == 'top' || point == 'bottom'){
                    //是top就获取当前父节点
                    p_node = $('#tt').tree('getParent', target)
                    if(p_node == null) p_node = {id:0};
                    var bro_node = $('#tt').tree('getNode', target);
                    //是top就获取当前父节点
                    var b_order = parseInt(bro_node.order);
                    if(point == 'top') order = b_order - 1;
                    if(point == 'bottom') order = b_order + 1;
                }else{
                    return false;
                }
                if(order !== undefined){
                    $.ajax({
                        type:'POST',
                        url:'/bsc_menu/update_data',
                        data:{
                            id:node.id,
                            order: order,
                            parent_id: p_node.id,
                        },
                        success:function (res) {

                        },
                        error:function (e) {
                            console.log(e);
                            $.messager.alert('<?= lang('提示');?>', '错误');
                        }
                    });
                }
            }
        });
    });
</script>
<body>
    <div class="">
        <div class="tree_tool">
            <table>
                <tr>
                    <td>桌面暂时不支持子目录方式显示</td>
                </tr>
            </table>
        </div>
        <div class="tree">
            <ul id="tt"></ul>
        </div>
    </div>

    <!--    右键菜单定义如下：-->
    <div id="mm" class="easyui-menu" style="width:120px;">
        <div onclick="add()" data-options="iconCls:'icon-add'">新增节点</div>
        <div onclick="update()" data-options="iconCls:'icon-pencil'">修改节点</div>
        <div onclick="del()" data-options="iconCls:'icon-remove'">删除节点</div>
    </div>

    <!--新增节点-->
    <div id="add_div" class="easyui-window" style="width:345px;height:250px" data-options="title:'window',modal:true,closed:true">
        <form id="add_form">
            <input type="hidden" name="parent_id">
            <input type="hidden" name="order">
            <table>
                <tr>
                    <td><?= lang('目录代码');?></td>
                    <td>
                        <input class="easyui-textbox input" name="code">
                    </td>
                </tr>
                <tr>
                    <td><?= lang('目录名称');?></td>
                    <td>
                        <input class="easyui-textbox input" name="name">
                    </td>
                </tr>
                <tr>
                    <td><?= lang('链接');?></td>
                    <td>
                        <input class="easyui-textbox input" name="url">
                    </td>
                </tr>
                <tr>
                    <td><?= lang('参数');?></td>
                    <td>
                        <input class="easyui-textbox input" name="param">
                    </td>
                </tr>
                <tr>
                    <td><?= lang('是否显示');?></td>
                    <td>
                        <input type="radio" name="display" id="add_display_true" value="1" ><label for="add_display_true">显示</label>
                        <input type="radio" name="display" id="add_display_false" value="0" checked><label for="add_display_false">隐藏</label>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('类型');?></td>
                    <td
                    <select class="easyui-combobox select" editable="false" name="m_type" id="add_m_type">
                        <?php foreach ($m_type_option as $v){ ?>
                            <option value="<?= $v;?>"><?= lang($v);?></option>
                        <?php } ?>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('图标');?></td>
                    <td>
                        <input class="easyui-textbox input" name="icon">
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <button onclick="add_save()" type="button" class="easyui-linkbutton" style="width: 180px;"><?= lang('保存');?></button>
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <!--修改节点-->
    <div id="update_div" class="easyui-window" style="width:345px;height:250px" data-options="title:'window',modal:true,closed:true">
        <form id="update_form">
            <input type="hidden" name="id">
            <table>
                <tr>
                    <td><?= lang('目录代码');?></td>
                    <td>
                        <input class="easyui-textbox input" name="code" readonly>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('目录名称');?></td>
                    <td>
                        <input class="easyui-textbox input" name="name">
                    </td>
                </tr>
                <tr>
                    <td><?= lang('链接');?></td>
                    <td>
                        <input class="easyui-textbox input" name="url">
                    </td>
                </tr>
                <tr>
                    <td><?= lang('参数');?></td>
                    <td>
                        <input class="easyui-textbox input" name="param">
                    </td>
                </tr>
                <tr>
                    <td><?= lang('是否显示');?></td>
                    <td>
                        <input type="radio" name="display" id="update_display_true" value="1"><label for="update_display_true">显示</label>
                        <input type="radio" name="display" id="update_display_false" value="0"><label for="update_display_false">隐藏</label>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('类型');?></td>
                    <td
                        <select class="easyui-combobox select" name="m_type" id="update_m_type">
                            <?php foreach ($m_type_option as $v){ ?>
                            <option value="<?= $v;?>"><?= lang($v);?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('图标');?></td>
                    <td>
                        <input class="easyui-textbox input" name="icon">
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <button onclick="update_save()" type="button" class="easyui-linkbutton" style="width: 180px;"><?= lang('保存');?></button>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</body>