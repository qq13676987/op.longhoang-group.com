<?php

/**
 * 该方法根据某种规则获取数据
 * 后续可能会加入一些循环标签之内的
 * Class Ddd
 */
class Variable_content {
    private $separator = '.';//分隔符
    private $V_MODE = 0;//0为 CI框架的数据库查询获取
    private $master_table_primary_key = 'id';//主表的主键

    public $master_table_id = array();//主表的ID,决定了主数据,,
    public $sql = array();//执行过的相关sql语句
    public $initial_variable = array();//初始变量和其对应的值, 如 'demo' => 'hhhhhhh'
    public $data = array();//将查询的相关数据进行缓存,
    public $variable_rule = '/\$\{(.*?)}/i'; //匹配变量的正则
    public $clean_rule = '/[\$|{|}]/';//清理掉多余的值的正则
    public $VariablesValue = array(); // 相关的变量,会进行存储
    public $parameter = array();//额外关联参数,用来控制关联关系用的

    /**
     * 用于配置表名简写等,键名可任意,只要不重复即可
     * @var array
     */
    private $table_alias = array(
        'shipment' => 'biz_shipment',
        'wlk_apply' => 'biz_bill_payment_wlk_apply',
        'wlk_apply_relation' => 'biz_bill_payment_wlk_apply_relation',
        'group' => 'bsc_group',
        'consol' => 'biz_consol',
        'client' => 'biz_client',
        'client_account' => 'biz_client_account',
        'overseas_account' => 'bsc_overseas_account',
        'sub_company' => 'biz_sub_company',
        'bill' => 'biz_bill',
        'charge' => 'biz_charge',
        'duty' => 'biz_duty_new',
        'user' => 'bsc_user',
        'truck' => 'biz_shipment_truck',
        'container' => 'biz_container',
        'shipment_container' => 'biz_shipment_container',
        'port' => 'biz_port',
        'dict' => 'bsc_dict',
        'invoice' => 'biz_bill_invoice',
        'soc' => 'biz_consol_company_soc',
        'soc_tixinagdian' => 'biz_consol_company_soc_tixiangdian',
        'log' => 'sys_log',
        'kml' => 'biz_kml',
        'si_change_apply' => 'biz_si_change_apply',
    );

    /**
     * 用于配置特殊关系,一般的1对1可直接 表A.表A字段-表B字段.表B
     * 键名为特殊关系名,表A.特殊关系.表B, 例子如下
     * string: id_type = 'shipment'
     * field: id_no = shipment.id
     * @var array
     */
    private $join_fields = array(
        '_shipment_si_change_apply' => array(
            array('id_type', '=', 'shipment', 'string'),
            array('id_no', '=', 'id', 'field'),
        ),
        '_shipment_bill' => array(
            array('id_type', '=', 'shipment', 'string'),
            array('id_no', '=', 'id', array('parameter6', 'invoice_id')),
//            array('type', '=', 'sell', 'string'),
            array('charge_code', '!=', 'YJ', 'string'),
            array('client_code', '=', 'client_code', 'parameter'),
            array('invoice_id', 'in', 'invoice_id', 'parameter3'),
            array('confirm_date', '=', 'is_confirm_date', 'parameter5'),
        ),
        '_shipment_bill_sell' => array(
            array('id_type', '=', 'shipment', 'string'),
            array('id_no', '=', 'id', array('parameter6', 'invoice_id')),
            array('client_code', '=', 'client_code', 'parameter'),
            array('charge_code', '!=', 'YJ', 'string'),
            array('type', '=', 'sell', 'string'),
            array('invoice_id', 'in', 'invoice_id', 'parameter3'),
            array('confirm_date', '=', 'is_confirm_date', 'parameter5'),
            array('currency', '=', 'currency', 'parameter3'),
        ),
        '_shipment_bill_cost' => array(
            array('id_type', '=', 'shipment', 'string'),
            array('id_no', '=', 'id', array('parameter6', 'invoice_id')),
            array('client_code', '=', 'client_code', 'parameter'),
            array('charge_code', '!=', 'YJ', 'string'),
            array('type', '=', 'cost', 'string'),
            array('invoice_id', 'in', 'invoice_id', 'parameter3'),
            array('confirm_date', '=', 'is_confirm_date', 'parameter5'),
        ),
        '_consol_bill' => array(
            array('id_type', '=', 'consol', 'string'),
            array('id_no', '=', 'id', 'field'),
            array('charge_code', '!=', 'YJ', 'string'),
            array('invoice_id', '=', 'invoice_id', 'parameter3'),
            array('confirm_date', '=', 'is_confirm_date', 'parameter5'),
        ),
        '_consol_duty' => array(
            array('biz_table', '=', 'biz_consol', 'string'),
            array('id_no', '=', 'id', 'field'),
        ),
        '_shipment_duty' => array(
            array('biz_table', '=', 'biz_shipment', 'string'),
            array('id_no', '=', 'id', 'field'),
        ),
        '_shipment_account' => array(
            array('id', '=', 'account_id', 'parameter2'),
        ),
        '_shipment_sub_company' => array(
            array('company_code', '=', 'sub_company_code', 'parameter2'),
        ),
        '_consol_sub_company' => array(
            array('company_code', '=', 'sub_company_code', 'parameter2'),
        ),
        '_shipment_truck_par' => array(
            array('shipment_id', '=', 'id', 'field'),
            array('id', '=', 'truck_id', 'parameter4'),
        ),
        '_bill_shipment_wlk' => array(
            array('id', '=', 'id_no', 'field'),
        ),
        '_bill_consol_wlk' => array(
            array('id', '=', 'id_no', 'field'),
        ),
        '_consol_log' => array(
            array('table_name', '=', 'biz_consol', 'string'),
            array('`key`', '=', 'id', 'field'),
        ),
        '_shipment_log' => array(
            array('table_name', '=', 'biz_shipment', 'string'),
            array('`key`', '=', 'id', 'field'),
        ),
        '_consol_soc' => array(
            array('gs_soc_tixianghao', '=', 'gs_soc_tixianghao', 'parameter'),
        ),
        '_sub_company_currency_account' => array(
            array('client_code', '=', 'company_code', 'field'),
            array('biz_type', '=', 'account_currency', 'parameter2'),
        ),
    );

    public function __construct($master_table_id = 0, $initial_variable = array())
    {
        $this->master_table_id = $master_table_id;
        $this->initial_variable = $initial_variable;
        //这里如何处理初次加载呢, 进来的时候,必须带有一条主数据才行
        //这里载入必须带有一个主表的ID

        $this->initial_variable['attached'] = array();
    }

    /**
     * 将带变量的内容转换为对应的值
     * @param string $content
     * @param bool $is_replace
     * @return mixed|string
     */
    public function get($content = "", $is_replace = true){
        //首先将内容里的所有变量提取出来
        $variables = $this->getVariables($content);

        //将变量塞入规则中进行数据的获取
        foreach ($variables as $variable){
            $variableData = $this->getByVariable($variable, 0, true);
            //替换变量对应的值
            if(!is_array($variableData) && $is_replace) $content = str_replace($variable, $variableData, $content);//将变量替换为值
        }

        return $content;
    }

    /**
     * 清理掉多余的值, 保留完整的变量
     * @param $str
     * @return mixed
     */
    public function cleanVariable($str){
        return preg_replace ($this->clean_rule, '', $str);
    }

    /**
     * 根据变量获取数据
     * @param string $variable 变量
     * @param int $num 获取第几个
     * @param bool $is_special 是否特殊
     * @return mixed
     */
    public function getByVariable($variable = '', $num = 0, $is_special = false){
        $variable = $this->cleanVariable($variable);
        //先检测$initial_variable是否存在,存在直接替换

        if(isset($this->initial_variable[$variable])){
            $variable_value = $this->initial_variable[$variable];
            // return $this->initial_variable[$variable];
        } else{
            $prefix = explode($this->separator, $variable);
            $field = array_pop($prefix);

            $variable_value = $this->getDataField(join($this->separator, $prefix), $field, $num, $is_special);
            // return $this->getDataField(join($this->separator, $prefix), $field, $num, $is_special);
        }
        //记录一下变量和相关的值
        $this->VariablesValue[$variable] = $variable_value;

        return $variable_value;
    }

    public function getVariablesValue(){
        return $this->VariablesValue;
    }

    /**
     * 获取已加载的数据
     */
    public function getLoadedData(){
        return $this->data;
    }

    /**
     * 获取具体的某个值,如果未查询过,那么这里先进行查询再进行处理
     * @param $key
     * @param $field
     * @param int $num
     * @param bool $is_special
     * @return mixed|string
     */
    private function getDataField($key, $field, $num = 0, $is_special = false){
        //如果field后面有个[数字] 那么覆盖掉这里的num
        if(preg_match("/\[(.*?)\]/i", $field, $matches)){
            $num = $matches[1];
            $field = str_replace($matches[0], '', $field);
        }
        //2024-06-20 新加入表名那里也可以选择第几个的数量, 且优先于前面的
        $key_arr = explode($this->separator, $key);
        if(preg_match("/\[(.*?)\]/i", $key_arr[sizeof($key_arr) - 1], $matches)){
//            $num = $matches[1];
//            $key_arr[sizeof($key_arr) - 1] = str_replace($matches[0], '', $key_arr[sizeof($key_arr) - 1]);
//            $key = join($this->separator, $key_arr);
        }
        //如果field是sum这些, 那么先`执行特殊获取
        if(in_array($field, array('sum', 'count', 'avg'))){
            return $this->getSpecialValue($key, $field, $num);
        }

        //提前验证, 这样 sum 那些都能走特殊来处理
        if($this->issetData($key)){
            //不存在具体的值,返回空
            if($is_special && $specialValue = $this->getSpecialValue($key, $field, $num)){
                $value = $specialValue;
            }else{
                //如果数据不存在,返回空
                if(!isset($this->data[$key][$num][$field])) return "";
                $value = $this->data[$key][$num][$field];
            }


            return $value;
        }else{
            //如果不存在数据,重新查一下,
            $this->getData($key . $this->separator . $field);

            if(isset($this->data[$key][$num][$field])) return $this->getDataField($key, $field, $num);
            return '';
        }
    }

    private function setDataField($value, $key, $field, $num = 0){
        return $this->data[$key][$num][$field] = $value;
    }

    /**
     * 特殊值要进行的一些处理
     * @param $key
     * @param $field
     * @param int $num
     * @return string
     */
    private function getSpecialValue($key, $field, $num = 0){
        //键名为字段名, 数组第一个值为连接字符,后面可添加
        $special_field = array(
            'box_info', 'charge_en', 'abc','vn_ex_rate', 'vn_payment_vnd_amount2', 'vn_payment_amount', 'vn_number_to_word', 'description_attached' , 'mark_nums_attached', 'shipper_attached', 'consignee_attached', 'notify_attached', 'bn_sn_info_attached', 'sum', 'count', 'avg','vat_amount1', 'ETD_d_M_y', 'ETA_d_M_y', 'booking_ETD_d/m/y'
        );

        $condition_arr = explode($this->separator, $key);
        if(!in_array($field, $special_field)){
            return false;
        }else {
            $str = '';
            //2023-08-04 总和平均值等, 放到这里来了
            if(in_array($field, array('sum', 'count', 'avg'))){
                //这里的sum等, 是多出来的
                $this_field = array_pop($condition_arr);
                $key = join($this->separator, $condition_arr);
                //获取当前的数据
                $this_data = $this->getData($condition_arr);
                //常规的直接获取总和就行了
                $this_val = 0;

                foreach ($this_data as $key => $row){
                    if(!isset($row[$this_field])) {
                        $row[$this_field] = $this->getDataField($key, $this_field, $key, true);
                    }
                    if(end($condition_arr) == 'bill'){
                        //如果是bill的, type = cost 时, 金额要变成负数
                        //这里没必要限制字段了, 默认自己配的时候就用的金额字段
                        if($row['type'] == 'cost'){
                            $row[$this_field] = -$row[$this_field];
                        }
                    }

                    //

                    if($field == 'sum'){
                        $this_val += $row[$this_field];
                    }else if($field == 'count'){
                        //数量
                        $this_val += 1;
                    }else if($field == 'avg'){
                        //平均值 这里为了 方便 就写成当前的 除以总数吧
                        $this_val += $row[$this_field] / sizeof($this_data);
                    }
                }
                //先暂时这样,后面有其他的再格式化成别的样子
                $str = number_format($this_val, 2, '.', '');
            }
            if($field == 'booking_ETD_d/m/y'){
                $date = $this->getDataField($key, 'booking_ETD', $num);
                $str = date('d/m/y', strtotime($date));
            }
            if($field == 'ETA_d_M_y'){
                $date = $this->getDataField($key, 'trans_ETA', $num);
                $str = date('d M y', strtotime($date));
            }
            if($field == 'ETD_d_M_y'){
                $date = $this->getDataField($key, 'booking_ETD', $num);
                $str = date('d M y', strtotime($date));
            }
            if($field == 'vat_amount1'){
                //求vn_payment_amount
                $data = $this->getData($condition_arr);
                $vat = $this->getDataField($key, "vat", $num);
                $amount = $this->getDataField($key, "amount", $num);

                $payment_sum = 0;
                $amount_sum = 0;
                //先按单一币种做

                $vat_num = (int)$vat / 100;
                //税额
                $vat_amount = round($amount * (1 + $vat_num), 2);

                $payment_sum += $vat_amount;
                $amount_sum += $amount;
                $sum = round($payment_sum - $amount_sum, 2);

                $str =  number_format($sum, 2, '.', '');
            }
            if($field == 'shipper_attached' || $field == 'consignee_attached' || $field == 'notify_attached'){
                if(isset($this->data[$key][$num][$field])){
                    return $this->data[$key][$num][$field];
                }
                $result = array();

                //0 是 字段前缀名, 1 是超出长度就切割, 2是作为att的前缀, 3 是att的排序
                $config_arr = array(
                    'shipper_attached' => array('shipper', 5, '*', 7),
                    'consignee_attached' => array('consignee', 7, '**', 9),
                    'notify_attached' => array('notify', 5, '***', 11),
                );
                $this_config = $config_arr[$field];
                $field_prefix = $this_config[0];

                //company
                $company = $this->getDataField($key, "{$field_prefix}_company", $num);
                $result[] = $company;

                //address
                $address = $this->getDataField($key, "{$field_prefix}_address", $num);
                foreach (explode("\r\n", $address) as $val){
                    $result[] = $val;
                }

                //info
                $info = $this->getDataField($key, "{$field_prefix}_info", $num, 1, false);
                foreach (explode("\r\n", $info) as $val){
                    $result[] = $val;
                }

                //这里对数据进行切割,一行当他50个字
                // $result = cut_array_length($result, 50, 99);

                //切割完成后进行 替换特殊字符
                $result = array_filter($result);

                //如果超过 5行,切割到 attached里
                if(sizeof($result) > $this_config[1]) {
                    $attached = array_slice($result, $this_config[1]);
                    $result = array_slice($result, 0, $this_config[1]);

                    $result[sizeof($result) - 1] .= $this_config[2];//shipper等内容结尾拼上*
                    $attached[0] = $this_config[2] . $attached[0];//attach 内容前排拼上*

                    $description = $this->getDataField($key, "description", $num, 0, false);
                    //多余的贴到品名那里取
                    $this->setDataField($description . "\r\n" . join("\r\n", $attached), $key, 'description', $num);
//                    $this->initial_variable['attached'][$this_config[0]] = array('data' => join("\r\n", $attached), 'order' => $this_config[3]);
                    $this->setDataField(join("\r\n", $result), $key, $field, $num);
                }


                $str = join("\r\n", $result);
            }
            if($field == 'description_attached'){
                //获取前手动获取下shipper那几个,
                $this->getDataField($key, 'shipper_attached', $num, 1);
                $this->getDataField($key, 'consignee_attached', $num, 1);
                $this->getDataField($key, 'notify_attached', $num, 1);

                $description = $this->getDataField($key, 'description', $num);
                $description = explode("\r\n", $description);
                $new_description = array();
                foreach ($description as $val){
                    //每行超过15字节,换行
                    //按照单词来分
                    //首先根据空格全切
                    $val_arr = explode(' ', trim($val));
                    $this_str = array();
                    foreach ($val_arr as $val2){
                        //当拼接后超过40字节,直接换行
                        if(strlen(join(' ', $this_str)) + strlen($val2) <= 40){
                            $this_str[] = $val2;
                        }else{
                            $new_description[] = join(' ', $this_str);
                            $this_str = array();
                            $this_str[] = $val2;
                        }
                    }
                    $new_description[] = join(' ', $this_str);
                }
                if(sizeof($new_description) > 10){
                    $new_description = join("\r\n", $new_description);
                    $description = 'SEE ATTACHED';

                    $this->initial_variable['attached']['description'] = array('data' => $new_description, 'order' => 3);
                }else{
                    $description = join("\r\n", $description);
                }
                $str =  $description;
            }
            if($field == 'mark_nums_attached'){
                $mark_nums = $this->getDataField($key, 'mark_nums', $num);
                $mark_nums = explode("\r\n", $mark_nums);
                $new_mark_nums = array();

                foreach ($mark_nums as $val){
                    //每行超过20字节,换行
                    //按照单词来分
                    //首先根据空格全切
                    $val_arr = explode(' ', trim($val));
                    $this_str = array();
                    foreach ($val_arr as $val2){
                        //当拼接后超过20字节,直接换行
                        if(strlen(join(' ', $this_str)) + strlen($val2) <= 20){
                            $this_str[] = $val2;
                        }else{
                            $new_mark_nums[] = join(' ', $this_str);
                            $this_str = array();
                            $this_str[] = $val2;
                        }
                    }
                    $new_mark_nums[] = join(' ', $this_str);
                }

                if(sizeof($new_mark_nums) > 5){

                    $new_mark_nums = join("\r\n", $new_mark_nums);
                    $mark_nums = 'SEE ATTACHED';

                    $this->initial_variable['attached']['mark_nums'] = array('data' => $new_mark_nums, 'order' => 5);
                }else{
                    $mark_nums = join("\r\n", $mark_nums);
                }
                $str =  $mark_nums;
            }
            if($field == 'charge_en'){
                $this_charge = $this->getDataField($key, 'charge_code', $num);
                $charge = Model('biz_charge_model')->get_one('charge_code', $this_charge);
                $str = $charge['charge_name'];
            }

            if($field == 'vn_ex_rate' || $field == 'abc'){
                if(isset($this->parameter['vn_ex_rate']) && !empty($this->parameter['vn_ex_rate'])){
                    $ex_rate = $this->parameter['vn_ex_rate'];
                }else{
                    //获取美元转人民币的汇率
                    if($condition_arr[0] == 'shipment'){
                        $id_type = 'shipment';
                    }else{
                        $id_type = 'consol';
                    }
                    $id_no = $this->getDataField($condition_arr[0], 'id', 0);
                    $currency = $this->getDataField($key, 'vn_this_local_currency', 0, 1);
                    $type = 'sell';//因为这个模板是单独的, 所以随便取一个了
                    $this_currecy = 'USD';//折算为该币种

                    //获取日期
                    $bill_ETD = get_bill_ETD($id_type, $id_no);
                    //获取当前汇率
                    $ex_rate = round(get_ex_rate($bill_ETD, $this_currecy, strtolower($type), $currency), 4);
                }
                $this->setDataField($ex_rate, $key, 'vn_ex_rate', $num);
                $this->setDataField($ex_rate, $key, $field, $num);
                $str = $ex_rate;
            }
            if($field == 'vn_payment_vnd_amount2'){
                //金额折算成越南币的
                $ex_rate = $this->getDataField($key, 'vn_ex_rate', 0, 1);
                $amount = $this->getDataField($key, 'vn_payment_amount', $num);

                //获取折算金额
                $this_amount = round(strval($amount * 100) * strval($ex_rate * 1000000) / 100000000, 2);
                $this->setDataField($this_amount, $key, $field, $num);

                return $this_amount;
            }

            if($field == 'vn_payment_amount'){
                //加上税率的金额
                $vat = $this->getDataField($key, "vat", $num);//税率
                $amount = $this->getDataField($key, "amount", $num);//当前总金额

                $vat_num = (int)$vat / 100;
                //税额
                $vat_amount = round($amount * (1 + $vat_num), 2);

                $this->setDataField($vat_amount, $key, $field, $num);

                return $vat_amount;
            }

            if ($field == 'box_info') {
                //shipment且为LCL取container的值
                if ($condition_arr[0] == 'shipment' && $this->getDataField($key, 'trans_mode', $num) == 'LCL') {
                    Model('biz_container_model');
                    Model('biz_shipment_model');
                    $consol_id = $this->getDataField('shipment', 'consol_id');
                    $shipment_containers = $this->getData('shipment.id-shipment_id.shipment_container');

                    $box_info = array();
                    foreach ($shipment_containers as $row) {
                        $this_container = $this->biz_container_model->get_where_one("consol_id = '$consol_id' and container_no = '" . $row['container_no'] . "'");
                        if (isset($box_info[$this_container['container_size']])) {//存在+1,不存在添加
                            $box_info[$this_container['container_size']] += 1;
                        } else {
                            $box_info[$this_container['container_size']] = 1;
                        }
                    }

                    $box_info_str = array();
                    foreach ($box_info as $key => $val) {
                        $box_info_str[] = $key . 'X' . $val;
                    }
                    $box_info_str = join(', ', $box_info_str);
                    if (!empty($box_info_str)) {
                        $str = 'PART OF ' . $box_info_str;
                    }
                }else {
                    $value = json_decode($this->getDataField($key, $field, $num), true);

                    $box_info_str = array();
                    foreach ($value as $key => $val) {
                        $box_info_str[] = $val['size'] . 'X' . $val['num'];
                    }

                    $str = join(', ', $box_info_str);
                }
            }
            return $str;
        }

    }

    /**
     * 判断当前是否有加载过数据
     * @param $data_key
     * @return bool
     */
    private function issetData($data_key){
        return isset($this->data[$data_key]);
    }

    public $i = 0;

//     /**
//      * 根据变量名获取数据
//      * 该方法后续可优化 TODO
//      * @param string $variable
//      * @return array
//      */
//     public function getData($variable = ''){
//         $data = array();
//         if(empty($variable)) return $data;
//         if(is_array($variable)) $variable = join($this->separator, $variable);
//         $this->i++;
//         //CI的数据库查询
//         if($this->V_MODE === 0){
//             $CI =& get_instance();
//             $condition = explode($this->separator, $variable);
//             $count = sizeof($condition) / 2;

//             //consol.id-consol_id.shipment.cus_no  4  c2
//             //consol.id  2  c1
//             $is_select = false;
//             for ($i = 0; $i < $count; $i++){
//                 //0 0
//                 //1 2
//                 //2 4
//                 $this_table_key = $i * 2;
// //                $this_table_key = ($i - 1) * 2;
//                 $this_table = $condition[$this_table_key];

//                 //判断是否有转化表名,如果有转化,这里直接进行相关的处理
//                 if(!isset($this->table_alias[$this_table])) return "";

//                 $table_name = $this->table_alias[$this_table];


//                 //不包含最后的字段的完整数组
//                 $data_key = join($this->separator, array_slice($condition, 0, 2 * $i + 1));

//                 //如果有数据,跳过当前,没数据进入下一步
//                 if($this->issetData($data_key)) continue;

//                 //是否进行过一次查询,没有查询的话, 数据不存在就进行获取数据
//                 if($is_select){
//                     if(!$this->issetData($data_key)){//不存在加载数据
//                         if($this->i == 8) exit();
//                         $this->getData($data_key);
//                     }
//                     return $data;
//                 }

//                 if($i <= 0){
//                   $where = "{$this->master_table_primary_key} = {$this->master_table_id}";
//                 }else{
//                     //是否为特殊条件
//                     $this_join_key = $this_table_key - 1;
//                     $table_key = join($this->separator, array_slice($condition, 0, 2 * ($i - 1) + 1));
//                     if(isset($this->join_fields[$condition[$this_join_key]])){
//                         $where = array();
//                         $this_join = $condition[$this_join_key];
//                         foreach ($this->join_fields[$this_join] as $jf_key => $jf_val) {
//                             $join_field = array();
//                             if(is_string($jf_val[3])) {
//                                 $join_field[] = $jf_val[3];
//                             }
//                             if(is_array($jf_val[3])) $join_field = $jf_val[3];

//                             //如果没有连接参数,这里不给数据
//                             if(empty($join_field)) {
//                                 $where[] = "0 = 1";
//                                 continue;
//                             }

//                             //进行连接类型的判定
//                             if ($join_field[0] == 'field'){
//                                 //biz_bill.id_no = biz_shipment.id
//                                 $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' .  $this->getDataField($table_key, $jf_val[2]) . '\'';
//                             }else if ($join_field[0] == 'string'){
//                                 //biz_bill.id_type = 'shipment'
//                                 $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' . $jf_val[2] . '\'';
//                             }else if($join_field[0] == 'parameter'){
//                                 $jf_val[2] = isset($this->parameter[$jf_val[2]]) && !empty($this->parameter[$jf_val[2]]) ? $this->parameter[$jf_val[2]] : $this->getDataField($table_key, $jf_val[2]);

//                                 $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' .  $jf_val[2] . '\'';
//                             }else if($join_field[0] == 'parameter2'){//只获取传入参数的
//                                 $jf_val[2] = isset($this->parameter[$jf_val[2]]) ? $this->parameter[$jf_val[2]] : '';

//                                 $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' .  $jf_val[2] . '\'';
//                             }else if($join_field[0] == 'parameter3'){//有传入参数取值,没有取
//                                 if(isset($this->parameter[$jf_val[2]])){
//                                     $jf_val[2] = $this->parameter[$jf_val[2]];
//                                     //如果存在空值时,其他值不能一起显示

//                                     if($jf_val[1] == 'in') $where[] = "{$jf_val[0]} in ('" . join('\',\'', filter_unique_array(explode(',', $jf_val[2]))) . "')" ;
//                                     else $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' .  $jf_val[2] . '\'';
//                                 }
//                             }else if($join_field[0] == 'parameter4'){//没有传入值就变为默认状态
//                                 if(!empty($this->parameter[$jf_val[2]])){
//                                     $jf_val[2] = $this->parameter[$jf_val[2]];
//                                     $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' .  $jf_val[2] . '\'';
//                                 }
//                             }else if($join_field[0] == 'parameter5'){//没有传入值就变为默认状态
//                                 if(!empty($this->parameter[$jf_val[2]])){
//                                     $jf_val[2] = $this->parameter[$jf_val[2]];
//                                     if($jf_val[2] == '1'){
//                                         $jf_val[2] = '\'0000-00-00\'';
//                                     }else{
//                                         $jf_val[1] = '=';
//                                         $jf_val[2] = "'{$jf_val[2]}'";
//                                     }
//                                     $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' ' .  $jf_val[2] . '';
//                                 }
//                             }else if($join_field[0] == 'parameter6'){
//                                 if(empty($join_field[1])) {
//                                     $where[] = "0 = 1";
//                                     continue;
//                                 }
//                                 //2022-09-20 由于导出时,账单需要根据当前是否有账单ID进行关联,这里又改为这个版本
//                                 //有参数则取参数的规则,没有不处理
//                                 if(!empty($this->parameter[$join_field[1]])){
//                                     continue;
//                                 }else{
//                                     //没参数走原本的field
//                                     $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' .  $this->getDataField($table_key, $jf_val[2]) . '\'';
//                                 }
//                             }else{//其他情况统一作为string处理
//                                 //biz_bill.id_type = 'shipment'
//                                 $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' . $jf_val[2] . '\'';
//                             }
//                         }
//                         $where = join(' and ', $where);
//                     }else{
//                         $_field = explode('-', $condition[$this_join_key]);

//                         if(sizeof($_field) > 1){
//                             $where = $_field[1] . ' = \'' . $this->getDataField($table_key, $_field[0]) . '\'';
//                         }else{
//                             $where = $condition[$this_table_key - 1] . ' = \'' . $this->getDataField($table_key, $condition[$i * 2 - 1]) . '\'';
//                         }
//                     }
//                 }
//                 if($i == $count - 1){//最后一次判断type
//                     $is_select = true;
//                     //如果表名是div的 话, 就不用管了, 直接特殊处理
//                     if($table_name == 'div') continue;
//                     try{
//                         $data = $this->data[$data_key] = $CI->db->where($where)->get($table_name)->result_array();
//                         $this->saveDebugData();
//                     }catch (Exception $e){

//                     }

//                 }
//             }
//         }

//         return $data;
//     }

    /**
     * 根据变量名获取数据
     * 该方法后续可优化 TODO
     * @param string $variable
     * @return array
     */
    public function getData($variable = ''){
        $data = array();
        if(empty($variable)) return $data;
        if(is_array($variable)) $variable = join($this->separator, $variable);
        //2023-08-10 如果有数据 这里 返回获取到的数据
        if($this->issetData($variable)) return $this->data[$variable];
        $this->i++;
        //CI的数据库查询
        if($this->V_MODE === 0){
            $CI =& get_instance();
            $condition = explode($this->separator, $variable);
            $setDataCondition = $condition;
            //1 2 1
            //3 4 2
            //5 6 3
            // 所以这里 / 2 后 向上取整即可
            $count = ceil(sizeof($condition) / 2);

            //consol.id-consol_id.shipment.cus_no  4  c2
            //consol.id  2  c1
            //这里的核心思想是 传参进来后, 根据 参数 以三个作为1组来拼接sql语句  A B C D E
            $is_select = false;
            for ($i = 0; $i < $count; $i++) {
                //0
                //1 0 2 where 1
                //2 2 4 where 3
                $this_table_key = $i * 2;
//                $this_table_key = ($i - 1) * 2;

                $this_table = $condition[$this_table_key];
                //如果表名有[]的, 自动清除 且后面自动额外生成一个对应的data数据
                if(preg_match("/\[(.*?)\]/i", $this_table, $matches)) {
//                    $setDataCondition[$this_table_key] =
                    $this_table = str_replace($matches[0], '', $this_table);
                }

                //判断是否有转化表名,如果有转化,这里直接进行相关的处理
                //没有配置过的表名直接返回空是为了防止出现报错等
                if (!isset($this->table_alias[$this_table])) return "";

                $table_name = $this->table_alias[$this_table];


                //不包含最后的字段的完整数组
                $data_key = join($this->separator, array_slice($condition, 0, 2 * $i + 1));

                $is_end = $i == $count - 1;

                //是否为最后一步
                if ($is_end) {
                    //判断是否有数据, 有数据直接返回, 但是这里和最上面的有点重复了, 先加这里保险吧
                    if ($this->issetData($data_key)) return $this->data[$data_key];
                }else{
                    //如果不是最后一步, 判断是否 有数据,
                    if ($this->issetData($data_key)) continue;
                }

                //如果最初阶段, 那么只需要查询主表就可以了
                if($i == 0){
                    $where = "{$this->master_table_primary_key} = {$this->master_table_id}";
                }else{
                    //是否为特殊条件
                    $this_join_key = $this_table_key - 1;
                    $table_key = join($this->separator, array_slice($condition, 0, 2 * ($i - 1) + 1));
                    if(isset($this->join_fields[$condition[$this_join_key]])){
                        //特殊条件获取数组配置后, 进行对应的拼接
                        $where = array();
                        $this_join = $condition[$this_join_key];
                        foreach ($this->join_fields[$this_join] as $jf_key => $jf_val) {
                            $join_field = array();
                            if(is_string($jf_val[3])) {
                                $join_field[] = $jf_val[3];
                            }
                            if(is_array($jf_val[3])) $join_field = $jf_val[3];

                            //如果没有连接参数,这里不给数据
                            if(empty($join_field)) {
                                $where[] = "0 = 1";
                                continue;
                            }

                            //进行连接类型的判定
                            if ($join_field[0] == 'field'){
                                //biz_bill.id_no = biz_shipment.id
                                $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' .  $this->getDataField($table_key, $jf_val[2]) . '\'';
                            }else if ($join_field[0] == 'string'){
                                //biz_bill.id_type = 'shipment'
                                $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' . $jf_val[2] . '\'';
                            }else if($join_field[0] == 'parameter'){
                                $jf_val[2] = isset($this->parameter[$jf_val[2]]) && !empty($this->parameter[$jf_val[2]]) ? $this->parameter[$jf_val[2]] : $this->getDataField($table_key, $jf_val[2]);

                                $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' .  $jf_val[2] . '\'';
                            }else if($join_field[0] == 'parameter2'){//只获取传入参数的
                                $jf_val[2] = isset($this->parameter[$jf_val[2]]) ? $this->parameter[$jf_val[2]] : '';

                                $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' .  $jf_val[2] . '\'';
                            }else if($join_field[0] == 'parameter3'){//有传入参数取值,没有取
                                if(isset($this->parameter[$jf_val[2]])){
                                    $jf_val[2] = $this->parameter[$jf_val[2]];
                                    //如果存在空值时,其他值不能一起显示

                                    if($jf_val[1] == 'in') $where[] = "{$jf_val[0]} in ('" . join('\',\'', filter_unique_array(explode(',', $jf_val[2]))) . "')" ;
                                    else $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' .  $jf_val[2] . '\'';
                                }
                            }else if($join_field[0] == 'parameter4'){//没有传入值就变为默认状态
                                if(!empty($this->parameter[$jf_val[2]])){
                                    $jf_val[2] = $this->parameter[$jf_val[2]];
                                    $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' .  $jf_val[2] . '\'';
                                }
                            }else if($join_field[0] == 'parameter5'){//没有传入值就变为默认状态
                                if(!empty($this->parameter[$jf_val[2]])){
                                    $jf_val[2] = $this->parameter[$jf_val[2]];
                                    if($jf_val[2] == '1'){
                                        $jf_val[2] = '\'0000-00-00\'';
                                    }else{
                                        $jf_val[1] = '=';
                                        $jf_val[2] = "'{$jf_val[2]}'";
                                    }
                                    $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' ' .  $jf_val[2] . '';
                                }
                            }else if($join_field[0] == 'parameter6'){
                                if(empty($join_field[1])) {
                                    $where[] = "0 = 1";
                                    continue;
                                }
                                //2022-09-20 由于导出时,账单需要根据当前是否有账单ID进行关联,这里又改为这个版本
                                //有参数则取参数的规则,没有不处理
                                if(!empty($this->parameter[$join_field[1]])){
                                    continue;
                                }else{
                                    //没参数走原本的field
                                    $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' .  $this->getDataField($table_key, $jf_val[2]) . '\'';
                                }
                            }else{//其他情况统一作为string处理
                                //biz_bill.id_type = 'shipment'
                                $where[] = $jf_val[0] . ' ' . $jf_val[1] . ' \'' . $jf_val[2] . '\'';
                            }
                        }
                        $where = join(' and ', $where);
                    }else{
                        //非特殊条件的话, 直接拼接即可
                        $_field = explode('-', $condition[$this_join_key]);

                        if(sizeof($_field) > 1){
                            $where = $_field[1] . ' = \'' . $this->getDataField($table_key, $_field[0]) . '\'';
                        }else{
                            $where = $condition[$this_table_key - 1] . ' = \'' . $this->getDataField($table_key, $condition[$i * 2 - 1]) . '\'';
                        }
                    }
                }
                //正常情况也查询
                $is_select = true;
                try{
                    $data = $this->data[$data_key] = $CI->db->where($where)->get($table_name)->result_array();
                    foreach ($data as $key => $row){
                        $this->data["{$data_key}[{$key}]"] = array($row);
                    }
                    $this->saveDebugData();
                }catch (Exception $e){

                }

                //如果是最后一步,且已查询, 那么直接返回数据
                if($is_end){
                    return $data;
                }
            }
        }

        return $data;
    }

    /**
     * 存储BUG数据
     */
    private function saveDebugData(){
        switch ($this->V_MODE){
            case 0:
                $this->sql[] = lastquery();
                break;
        }
    }

    /**
     * 返回所有变量
     * @param $content
     * @param bool $isClear
     * @return mixed
     */
    public function getVariables($content, $isClear = false)
    {
        $matches = array();
        preg_match_all($this->variable_rule, $content, $matches);

        return !$isClear ? $matches[0] : $matches[1];
    }

    /**
     * 获取debug数据
     */
    public function getDebugInfo(){
        dump($this->VariablesValue);
        dump($this->data);
        dump($this->sql);
    }

    /**
     * 重置信息等
     */
    public function reset(){
        $this->initial_variable = array();
        $this->sql = array();
        $this->data = array();
    }


    /**
     * @param int $V_MODE
     */
    public function setVMODE($V_MODE)
    {
        $this->V_MODE = $V_MODE;
    }

    /**
     * @param string $separator
     */
    public function setSeparator($separator)
    {
        $this->separator = $separator;
    }

    /**
     * @param string $variable_rule
     */
    public function setVariableRule($variable_rule)
    {
        $this->variable_rule = $variable_rule;
    }

    /**
     * @param string $clean_rule
     */
    public function setCleanRule($clean_rule)
    {
        $this->clean_rule = $clean_rule;
    }

    /**
     * @param array|int $master_table_id
     */
    public function setMasterTableId($master_table_id)
    {
        $this->master_table_id = $master_table_id;
        //如果重新设置过主键的id,那么清空一下数据
        $this->reset();
    }

    /**
     * @param string $master_table_primary_key
     */
    public function setMasterTablePrimaryKey($master_table_primary_key)
    {
        $this->master_table_primary_key = $master_table_primary_key;
    }

    /**
     * @param array $initial_variable
     */
    public function setInitialVariable($initial_variable)
    {
        $this->initial_variable = $initial_variable;
    }

    /**
     * @param array $parameter
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
    }
}
