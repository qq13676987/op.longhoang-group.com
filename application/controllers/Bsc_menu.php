<?php
class Bsc_menu extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->model = Model('bsc_menu_model');;
    }

    protected $m_type_option = array('模块', '导航', '按钮','选项卡');

    /**
     * 目录维护页面
     */
    public function index(){
        if(!is_admin()){
            echo '不是管理员';
            return;
        }
        $data = array();
        $data['m_type_option'] = $this->m_type_option;

        $this->load->view('head');
        $this->load->view('/bsc/menu/index', $data);
    }

    /**
     * 目录维护页面
     */
    public function user(){
        $data = array();
        $data['id'] = getValue('id', 0);

        //这里查询一下当前用户的menu设置
        //目录的信息得关联查询

        $menu = array_column(Model('bsc_menu_relation_model')->get_menu_relation('bsc_user', $data['id']), 'menu_id');
        $data['menu'] = join(',', $menu);

        $this->load->view('head');
        $this->load->view('/bsc/menu/user_view', $data);
    }

    public function save_data(){
        $id = postValue('id', '');
        $id_type = postValue('id_type', '');
        $menu = postValue('menu', '');


        Model('bsc_menu_relation_model')->batch_save($id_type, $id, $menu);

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = $id_type;
        $log_data["key"] = $id;
        $log_data["action"] = 'update_menu';
        $log_data["value"] = json_encode($menu);
        log_rcd($log_data);

        return jsonEcho(array('code' => 0, 'msg' => '保存成功'));
    }

    /**
     * 获取树状数据
     * @param $location string 显示地址,用于获取具体显示哪里的树,如果未指定则返回全部,且显示位置作为根目录
     */
    public function get_tree(){
        $where = array();

        $where[] = 'is_delete = 0';
        $where = join(' and ', $where);

        $this->db->select('id,code,m_type,param,name,parent_id,display,url,icon,order');
        $rs = $this->model->get($where, 'order', 'asc');

        $data = tree($rs, 0, 'children', 'parent_id');
        echo json_encode($data);
    }

    /**
     * 新增数据
     */
    public function add_data(){
        $fields = $this->model->get_field();
        $result = array('code' => 1, 'msg' => '新增失败');

        $data = array();
        foreach ($fields as $field){
            if(isset($_POST[$field])){
                $data[$field] = $_POST[$field];
            }
        }

        $id = $this->model->save($data);
        if($id){
            $log_data = array();
            $log_data["table_name"] = "bsc_menu";
            $log_data["key"] = $id;
            $log_data["action"] = 'insert';
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);

            $result['code'] = 0;
            $result['msg'] = '新增成功';
        }
        echo json_encode($result);
    }

    /**
     * 修改数据
     */
    public function update_data(){
        $fields = $this->model->get_field();
        $result = array('code' => 1, 'msg' => '参数错误');
        $id = $_REQUEST['id'];

        $old_row = $this->model->get_one('id', $id);
        if(empty($old_row)){
            echo json_encode($result);
            return;
        }
        $data = array();
        foreach ($fields as $field){
            if(isset($_POST[$field]) && $_POST[$field] != $old_row[$field]){
                $data[$field] = $_POST[$field];
            }
        }

        if(!empty($data)){
            $this->model->update($old_row['id'], $data);

            $log_data = array();
            $log_data["table_name"] = "bsc_menu";
            $log_data["key"] = $id;
            $log_data["action"] = 'insert';
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        }
        $result['code'] = 0;
        $result['msg'] = '修改成功';
        echo json_encode($result);
    }

    /**
     * 删除数据
     */
    public function delete_data(){
        $id = postValue('id', '');
        $result = array('code' => 1, 'msg' => '参数错误');

        if(empty($id)){
            echo json_encode($result);
            return;
        }

        $old_row = $this->model->get_one('id', $id);
        if($old_row['is_delete'] == 1){
            $result['msg'] = '删除成功,请刷新';
            echo json_encode($result);
            return;
        }

        $update_data = array();
        $update_data['is_delete'] = 1;

        $result['code'] = 0;
        $result['msg'] = '删除成功';
        if(!empty($update_data)){
            $this->model->update($old_row['id'], $update_data);

            $log_data = array();
            $log_data["table_name"] = "bsc_menu";
            $log_data["key"] = $old_row['id'];
            $log_data["action"] = 'update';
            $log_data["value"] = json_encode($update_data);
            log_rcd($log_data);
        }
        echo json_encode($result);
    }

    /**
     * 获取桌面信息
     */ 
    public function desktop(){
        $tree = get_menu(1); 
        Model('bsc_menu_relation_model');
        $menu_id = $this->bsc_menu_relation_model->getRoleAuth() ;
        foreach($tree as $key=>$value){
            //数据库就不加字段了, crm点击是打开新窗口
            if($value['code'] == 'CRM'){//如果是CRM管理的话, 这里打开新窗口
                $tree[$key]['onClick'] = "window.open('{$value['href']}');";
            }
            if(!in_array($value["id"],$menu_id)) unset($tree[$key]);
        } 
        
        // var_dump($tree);
        $menu = json_encode($tree);
        echo $menu;
    }

    public function startmenu(){
//        $menu = isset($_COOKIE[$menu_key]) ? $_COOKIE[$menu_key] : array();
//        if(empty($menu)){
            $tree = get_menu(2, true);

            $change_users = get_user_pan(get_session('id'));
            $change_users_children = array();
            foreach ($change_users as $change_user){
                $change_users_children[] = array('id' => 'change_user_' . $change_user['id'], 'text' => $change_user['name'], 'href' => '/main/change_login?pan_id=' . $change_user['id'], 'window' => 'no');
            }

            $tree[] = array('id' => 'change_user', 'text' => lang('Change User'), 'href' => '#', 'children' => $change_users_children);
            //登录和用户名的固定在最下面  如果想加入菜单里也可以,需要将name改为{user}可替换内容(暂时没做)
            $tree[] = array('id' => 'login', 'text' => lang('User') . ':' . get_session('name'), 'href' => '/main/login', 'window' => 'jump');
            $tree[] = array('id' => 'loginout', 'text' => lang('login out'), 'href' => '/main/loginout', 'window' => 'jump');
            //把目录存下来,1小时更新1次
            $menu = json_encode($tree);
//            setcookie($menu_key, $menu,time() + 3600,'/');
//        }
        echo $menu;
    }

    /**
     * 右键菜单
     */
    public function contextmenu(){
        $tree = get_menu(3, true, 'no_window');

        $change_users = get_user_pan(get_session('id'));
        $change_users_children = array();

        foreach ($change_users as $change_user){
            $change_users_children[] = array('id' => 'change_user_' . $change_user['id'], 'text' => $change_user['name'], 'href' => '/main/change_login?pan_id=' . $change_user['id'], 'window' => 'no');
        }

        $tree[] = '-';
        $tree[] = array('text' => 'Refresh', 'href' => "javascript:void(0);", 'onClick' => "javascript:$('body').app('refreshapp')");
        //把目录存下来,1小时更新1次
        $menu = json_encode($tree);
        echo $menu;
    }
}