<script type="text/javascript">
    var is_submit = false;
    function reset_data(){
        var json = {'is_reset':'1'};
        // GET方式示例，其它方式修改相关参数即可
        var form = $('<form></form>').attr('action','').attr('method','POST');

        $.each(json, function (index, item) {
            if(typeof item === 'string'){
                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
            }else{
                $.each(item, function(index2, item2){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                })
            }
        });

        form.appendTo('body').submit().remove();
    }

    /**
     * 保存数据
     */
    function save_data(){
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $('#fm').form('submit', {
            url: '/bsc_message/set_message_data/shipment/<?= $shipment_id;?>/ISF/EFREIGHT',
            onSubmit: function(param){
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                    is_submit = false;
                }

                return validate;
            },
            success:function(res_json){
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误请联系管理员');
                    return;
                }
                ajaxLoadEnd();
                is_submit = false;
                if(res.code == 0){
                    create_edi();
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.href = "";
                    });
                }else{
                    $.messager.alert('Tips', res.msg);
                }
            }
        });
    }

    function create_edi(action = 'insert'){
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $.ajax({
            type: 'GET',
            url: '/bsc_message/create_message/<?= $shipment_id;?>/EFREIGHT/ISF/' + action,
            success: function (res_json) {
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误请联系管理员');
                    return;
                }
                ajaxLoadEnd();
                is_submit = false;
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.reload();
                    });
                }else{
                    $.messager.alert('Tips', res.msg);
                }
            },
            error: function () {
                ajaxLoadEnd();
                is_submit = false;
                $.messager.alert('Tips', '发生错误');
            }
        });
    }

    function send_edi(action, receiver, id) {
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $.ajax({
            type: 'GET',
            url: '/bsc_message/send_message/' + id + '/' + action + '/' + receiver,
            success: function (res_json) {
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误请联系管理员');
                    return;
                }
                ajaxLoadEnd();
                is_submit = false;
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.reload();
                    });
                }else{
                    $.messager.alert('Tips', res.msg);
                }
            },
            error: function () {
                ajaxLoadEnd();
                is_submit = true;
                $.messager.alert('Tips', 'error');
            }
        });
    }

    function read_message(id) {
        window.open('/bsc_message/file_read/' + id);
    }

    function ajax_get(url,name) {
        return $.ajax({
            type:'POST',
            url:url,
            dataType:'json',
            success:function (res) {
                ajax_data[name] = res;
            },
            error:function () {
                // $.messager.alert('获取数据错误');
            }
        });
    }

    function isf_type_open(type) {
        location.href = "?isf_type=" + type;
    }

    var ajax_data = {};
    $(function () {
        var a1 = ajax_get('/city/get_country','country_code');
        $.when(a1).done(function () {
            $('.country').combobox('loadData', ajax_data.country_code);
        });
        var a2 = ajax_get('/biz_port/get_option','port');
        $.when(a2).done(function () {
            $('.port').combobox('loadData', ajax_data.port);
        });

    });
</script>
<style type="text/css">
    .head5{
        font-weight: bold;
    }
    .b_table{
        width: 100%;
        text-align: left;
    }
    .input{
        width: 200px;
    }
    .input_textarea{
        width: 200px;
        height: 50px;
    }
    .a_table{

    }
    input:read-only,select:read-only,textarea:read-only
    {
        background-color: #d6d6d6;
        cursor: default;
    }
</style>
<body>
<button onclick="reset_data()" title="初始化数据">初始化</button>
<button onclick="save_data()" title="保存">保存数据</button>
<?php if(empty($message)){ ?>
    <button onclick="create_edi()" title="生成报文">生成报文</button>
<?php }else{?>
    <button onclick="create_edi()" title="生成报文">生成报文</button>
    <br />
    发送情况:
    <?php if($message['status'] == 0){?>
        未发送
    <?php }else if($message['status'] == 1){
        echo $message['send_time'];?>
        发送成功
    <?php }else if($message['status'] == 2){?>
        发送修改成功
    <?php }?>
    <br />
    <button class="read_message" onclick="read_message(<?= $message['id'];?>)">查看报文</button>
    <button class="send" onclick="send_edi('API', '<?= $message['receiver'];?>', <?= $message['id'];?>)">发送报文</button>
<?php }?>
<br/>
<button onclick="isf_type_open('SF05');">SF05</button>
<br/>
<p style="color:red;font-size:20px;">该页面除第一次生成时，后续进入该页面，都是读取上一次的，如果某些信息修改后需要重新获取，请点击初始化</p>
<p style="color:red;font-size:20px;">如果已经保存过了,想切换到SF05,那么请先点击初始化后,再点击SF05</p>
<form id="fm" method="post">
    <h2><?= $edi_data['ISF']['ISFType'];?></h2>
    <table class="a_table">
         <input type="hidden" name="ISF[ISFType]" value="<?= $edi_data['ISF']['ISFType'];?>">
        <tr>
            <td>ShipmentFileNumber</td>
            <td>
                <input class="easyui-textbox input" readonly name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][ShipmentFileNumber]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['ShipmentFileNumber']; ?>">
            </td>
            <td>AMSSCAC</td>
            <td>
                <input class="easyui-textbox input" required name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][AMSSCAC]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['AMSSCAC']; ?>">
            </td>
            <td>AMSBLNumber</td>
            <td>
                <input class="easyui-textbox input" required name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][AMSBLNumber]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['AMSBLNumber']; ?>">
            </td>
            <td>AMSBLType</td>
            <td>
                <select class="easyui-combobox input" required name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][AMSBLType]" data-options="
                    value:'<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['AMSBLType']; ?>',
                ">
                    <option value="OB">Ocean Bill of Lading – used for Regular Bills</option>
                    <option value="BM">House Bill of Lading</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>MasterBillNo</td>
            <td>
                <input class="easyui-textbox input" required name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][MasterBillNo]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['MasterBillNo']; ?>">
            </td>
            <td>OnBoardDate</td>
            <td>
                <input class="easyui-datebox input" required name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][OnBoardDate]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['OnBoardDate']; ?>">
            </td>
            <td>OriginAgentCode</td>
            <td>
                <input class="easyui-textbox input" readonly name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][OriginAgentCode]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['OriginAgentCode']; ?>">
            </td>
            <td>DestinationAgentCode</td>
            <td>
                <input class="easyui-textbox input" readonly name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][DestinationAgentCode]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['DestinationAgentCode']; ?>">
            </td>
        </tr>
        <tr>
            <td>ImporterRefCode</td>
            <td>
                <input class="easyui-textbox input" readonly name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][ImporterRefCode]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['ImporterRefCode']; ?>">
            </td>
            <td>SellingParties</td>
            <td>
                <input class="easyui-textbox input" readonly name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][SellingParties][SellingRefCode]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['SellingParties']['SellingRefCode']; ?>">
            </td>
            <td>ContainerStuffingLocations</td>
            <td>
                <input class="easyui-textbox input" readonly name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][ContainerStuffingLocations][ContainerStuffingLocationRefCode]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['ContainerStuffingLocations']['ContainerStuffingLocationRefCode']; ?>">
            </td>
            <td>Consolidators</td>
            <td>
                <input class="easyui-textbox input" readonly name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][Consolidators][ConsolidatorRefCode]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['Consolidators']['ConsolidatorRefCode']; ?>">
            </td>
        </tr>
        <tr>
            <td>BuyingParties</td>
            <td>
                <input class="easyui-textbox input" readonly name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][BuyingParties][BuyingRefCode]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['BuyingParties']['BuyingRefCode']; ?>">
            </td>
            <td>Consignees</td>
            <td>
                <input class="easyui-textbox input" readonly name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][Consignees][ConsigneeRefCode]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['Consignees']['ConsigneeRefCode']; ?>">
            </td>
            <td>ContainerShipTos</td>
            <td>
                <input class="easyui-textbox input" readonly name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][ContainerShipTos][ShipToRefCode]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['ContainerShipTos']['ShipToRefCode']; ?>">
            </td>
        </tr>
    </table>
    <h3>Manufacturers</h3>
    <table>
        <tr>
            <td>CountryOrigin</td>
            <td>
                <select class="easyui-combobox country input" required name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][Manufacturers][Manufacturer][CountryOrigin][code]" data-options="
                    valueField:'code',
                    textField:'namecode_en',
                    value:'<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['Manufacturers']['Manufacturer']['CountryOrigin']['code'];?>',
                    onSelect:function(rec){
                        console.log(rec);
                    },
                ">
                </select>
                <input type="hidden" name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][Manufacturers][Manufacturer][CountryOrigin][Value]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['Manufacturers']['Manufacturer']['CountryOrigin']['Value']; ?>">
            </td>
            <td>ManufacturerRefCode</td>
            <td>
                <input class="easyui-textbox input" readonly name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][Manufacturers][Manufacturer][ManufacturerRefCode]" value="<?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['Manufacturers']['Manufacturer']['ManufacturerRefCode']; ?>">
            </td>
            <td><?= lang('HS CODE');?> <span style="color:red;">多HS用逗号隔开</span></td>
            <td>
                <textarea style="width:155px;" name="ISF[<?= $edi_data['ISF']['ISFType']; ?>][Manufacturers][Manufacturer][HTSCodes]"><?= $edi_data['ISF'][$edi_data['ISF']['ISFType']]['Manufacturers']['Manufacturer']['HTSCodes']; ?></textarea>
            </td>
        </tr>
    </table>
    <h2>ShippingParties</h2>
    <table class="b_table">
        <?php foreach ($edi_data['ShippingParties'] as $key => $ShippingParty){ ?>
            <tr>
                <td>
                    <fieldset>
                        <legend>ShippingParty <?= $key + 1;?></legend>
                        <table>
                            <tr>
                                <td>PartyCode</td>
                                <td>
                                    <input class="easyui-textbox input" required readonly name="ShippingParties[<?= $key;?>][PartyCode]" value="<?= $ShippingParty['PartyCode'];?>">
                                </td>
                                <td>PartyType</td>
                                <td>
                                    <input class="easyui-textbox input" required readonly name="ShippingParties[<?= $key;?>][PartyType]"  value="<?= $ShippingParty['PartyType'];?>">
                                </td>
                                <td>PartyName</td>
                                <td>
                                    <textarea class="input_textarea" name="ShippingParties[<?= $key;?>][PartyName]" required><?= $ShippingParty['PartyName'];?></textarea>
                                </td>
                                <td>
                                    PartyIDType
                                    <a href="javascript:void;" class="easyui-tooltip" data-options="
                                        content: $('<div></div>'),
                                        onShow: function(){
                                            $(this).tooltip('arrow').css('left', 20);
                                            $(this).tooltip('tip').css('left', $(this).offset().left);
                                        },
                                        onUpdate: function(cc){
                                            cc.panel({
                                                width: 500,
                                                height: 'auto',
                                                border: false,
                                                href: '/bsc_help_content/help/consignee_id_type'
                                            });
                                        }
                                    "><img src="/inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                                </td>
                                <td>
                                    <select class="easyui-combobox input" required  name="ShippingParties[<?= $key;?>][PartyIDType]" data-options="
                                        value: '<?= $ShippingParty['PartyIDType'];?>',
                                    ">
                                        <option value="EI">EI</option>
                                        <option value="ANI">ANI</option>
                                        <option value="CIN">CIN</option>
                                        <option value="34">34</option>
                                        <option value="DUN">DUN</option>
                                        <option value="DNS">DNS</option>
                                        <option value="FR">FR</option>
                                        <option value="AEF">AEF</option>
                                        <option value="2">2</option>
                                        <option value="ZZ">ZZ</option>
                                    </select>
                                </td>
                                <td>PartyID</td>
                                <td>
                                    <input class="easyui-textbox input" name="ShippingParties[<?= $key;?>][PartyID]" value="<?= $ShippingParty['PartyID'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td><h4>PartyLocation</h4></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>
                                    <textarea class="input_textarea" name="ShippingParties[<?= $key;?>][PartyLocation][Address]" required><?= $ShippingParty['PartyLocation']['Address'];?></textarea>
                                </td>
                                <td>Country</td>
                                <td>
                                    <select class="easyui-combobox country input" required name="ShippingParties[<?= $key;?>][PartyLocation][Country][code]" data-options="
                                        valueField:'code',
                                        textField:'namecode_en',
                                        value:'<?= $ShippingParty['PartyLocation']['Country']['code'];?>',
                                        onSelect: function(rec){
                                            if(rec == undefined)return;
                                            $('#Region<?= $key;?>').combobox('reload', '/city/get_province?country=' + rec.cityname).combobox('clear');
                                        },
                                    ">
                                    </select>
                                    <input type="hidden" name="ShippingParties[<?= $key;?>][PartyLocation][Country][Value]">
                                </td>
                                <td><span>Region</span></td>
                                <td>
                                    <select class="easyui-combobox input" id="Region<?= $key;?>" name="ShippingParties[<?= $key;?>][PartyLocation][Region]" data-options="
                                        valueField:'cityname_en',
                                        textField:'cityname_en',
                                        value: '<?= $ShippingParty['PartyLocation']['Region'];?>',
                                        <?php if(!empty($ShippingParty['PartyLocation']['Country']['code'])) echo 'url:\'/city/get_province/0/code?country=' . $ShippingParty['PartyLocation']['Country']['code'] . '\',';?>
                                        onSelect:function(rec){
                                            if(rec == undefined) return;
                                            var city_code = rec.code;
                                            $('#SubDivisionCode<?= $key;?>').val(city_code);
                                        },
                                        onLoadSuccess: function(){
                                            var val = $(this).combobox('getValue');
                                            $(this).combobox('clear').combobox('select', val);
                                        }
                                    ">
                                    </select>
                                    <input type="hidden" id="SubDivisionCode<?= $key;?>"  name="ShippingParties[<?= $key;?>][PartyLocation][SubDivisionCode]" value="<?= $ShippingParty['PartyLocation']['SubDivisionCode'];?>">
                                </td>
                                <td>City</td>
                                <td>
                                    <input required class="easyui-textbox input" name="ShippingParties[<?= $key;?>][PartyLocation][City]" value="<?= $ShippingParty['PartyLocation']['City'];?>">
                                </td>

                                <td>PostalCode</td>
                                <td>
                                    <input class="easyui-textbox input" required name="ShippingParties[<?= $key;?>][PartyLocation][PostalCode]" value="<?= $ShippingParty['PartyLocation']['PostalCode'];?>">
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        <?php } ?>
    </table>
</form>
</body>