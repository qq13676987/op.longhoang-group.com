<title>shipments</title>
<script type="text/javascript" src="/inc/js/easyui/datagrid-export.js"></script>
<style type="text/css">
    .f{
        width:115px;
    }
    .s{
        width:100px;
    }
    .v{
        width:120px;
    }
    .del_tr{
        width: 23.8px;
    }
    .add_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }
    #cx tr:nth-child(-n+2){
        display: none;
    }
    .all_t{
        color: blue;
        font-weight: 800;
    }
</style>
<!--这里是查询框相关的代码-->
<script>
    var query_option = {};//这个是存储已加载的数据使用的

    var table_name = 'biz_shipment',//需要加载的表名称
        view_name = 'biz_shipment_index';//视图名称
    var add_tr_str = "";//这个是用于存储 点击+号新增查询框的值, load_query_box会根据接口进行加载


    /**
     * 加载查询框
     */
    function load_query_box(){
        ajaxLoading();
        $.ajax({
            type:'GET',
            url: '/sys_config_title/get_user_query_box?view_name=' + view_name + '&table_name=' + table_name,
            dataType:'json',
            success:function (res) {
                ajaxLoadEnd();
                if(res.code == 0){
                    //加载查询框
                    $('#cx table').append(res.data.box_html);
                    //需要调用下加载器才行
                    $.parser.parse($('#cx table tbody .query_box'));

                    add_tr_str = res.data.add_tr_str;
                    query_option = res.data.query_option;
                    var table_columns = [[
                        {field:'label', title:'<?= lang('标签');?>', width:'60',},
                        {field:'node', title:'<?= lang('节点');?>', width:'60',},
                    ]];//表的表头

                    //填充表头信息
                    $.each(res.data.table_columns, function (i, it) {
                        //读取列配置,然后这里进行合并
                        var column_config;
                        try {
                            column_config = JSON.parse(it.column_config,function(k,v){
                                if(v && typeof v === 'string') {
                                    return v.indexOf('function') > -1 || v.indexOf('FUNCTION') > -1 ? new Function(`return ${v}`)() : v;
                                }
                                return v;
                            });
                        }catch (e) {
                            column_config = {};
                        }
                        var this_column_config = {field:it.table_field, title:it.title, width:it.width, sortable: it.sortable, formatter: get_function(it.table_field + '_for'), styler: get_function(it.table_field + '_styler')};
                        //合并一下
                        $.extend(this_column_config, column_config);

                        table_columns[0].push(this_column_config);
                    });

                    //渲染表头

                    $('#tt').datagrid({
                        width:'auto',
                        height: $(window).height() - 26,
                        columns:table_columns,
                        rowStyler:function(index,row){
                            switch(row.status){
                                case 'cancel':
                                    return 'color:red;';
                                    break;
                                case 'normal':
                                    return 'color:blue;';
                                    break;
                            }
                        },
                        onDblClickRow: function(rowIndex, row) {
                            url = '/biz_shipment/edit/'+row.id;
                            window.open(url);
                        },
                        onClickRow: function(index, data) {
                            if (index == selectIndex) {
                                $('#tt').datagrid('unselectRow', index);
                                selectIndex = -1;
                                $('#node_fm').form('clear');
                            } else {
                                $.each($('.status'), function (i, it) {
                                    var inp = $(it);
                                    var name = inp.attr('name');
                                    var checked = false;
                                    if(data[name] != undefined) {
                                        if(data[name] != '0') checked = true;
                                        inp.prop('checked', checked);
                                    }
                                });
                                selectIndex = index;
                            }
                        },
                        onSelect:function(index ,row){
                            get_statistics();
                        },
                        onUnselect:function(index ,row){
                            get_statistics();
                        },
                        onSelectAll:function(index ,row){
                            get_statistics();
                        },
                        onUnselectAll:function(index ,row){
                            get_statistics();
                        },

                        // detail view,expandrows

                        view: detailview,
                        detailFormatter:function(index,row){
                            return '<div style="padding:2px"><table class="ddv"></table></div>';
                        },
                        onExpandRow: function(index,row){
                            var ddv = $(this).datagrid('getRowDetail', index).find('table.ddv');
                            ddv.datagrid({
                                url: '/biz_shipment/shipment_children_get?parent_id=' + row.id,
                                fitColumns: false,
                                singleSelect: true,
                                rownumbers: true,
                                loadMsg: '',
                                height: 'auto',
                                columns: [[
                                    {field: 'id', title: 'Id', width: 80, align: 'left', halign: 'center'},
                                    {field: 'booking_ETD', title: 'booking_ETD', width: 120, align: 'left', halign: 'center'},
                                    {field: 'job_no', title: 'job_no', width: 150, align: 'left', halign: 'center'},
                                    {field: 'cus_no', title: '关单号', width: 150, align: 'left', halign: 'center'},
                                ]],
                                onResize: function () {
                                    $('#tt').datagrid('fixDetailRowHeight', index);
                                },

                                onLoadSuccess: function () {
                                    setTimeout(function () {
                                        $('#tt').datagrid('fixDetailRowHeight', index);
                                    }, 0);
                                },
                                onDblClickRow: function (rowIndex) {
                                    $(this).datagrid('selectRow', rowIndex);
                                    var row = $('.ddv').datagrid('getSelected');
                                    if (row) {
                                        url = '/biz_shipment/edit/' + row.id;
                                        window.open(url);
                                    }
                                }
                            });
                            $('#tt').datagrid('fixDetailRowHeight',index);

                        }
                    });
                    $(window).resize(function () {
                        $('#tt').datagrid('resize');
                    });

                    //为了避免初始加载时, 重复加载的问题,这里进行了初始加载ajax数据
                    var aj = [];
                    $.each(res.data.load_url, function (i,it) {
                        aj.push(get_ajax_data(it, i));
                    });
                    //加载完毕触发下面的下拉框渲染
                    //$_GET 在other.js里封装
                    var this_url_get = {};
                    $.each($_GET, function (i, it) {
                        this_url_get[i] = it;
                    });
                    var auto_click = this_url_get.auto_click;

                    $.when.apply($,aj).done(function () {
                        //这里进行字段对应输入框的变动
                        $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                            var ec_value = $(ec_item).combobox('getValue');
                            var v_inp = $('.v:eq(' + ec_index + ')');
                            var s_val = $('.s:eq(' + ec_index + ')').combobox('getValue');
                            var v_val = v_inp.textbox('getValue');
                            //如果自动查看,初始值为空
                            if(auto_click === '1') v_val = '';

                            $(ec_item).combobox('clear').combobox('select', ec_value);
                            //这里比对get数组里的值
                            $.each(this_url_get, function (trg_index, trg_item) {
                                //切割后,第一个是字段,第二个是符号
                                var trg_index_arr = trg_index.split('-');
                                //没有第二个参数时,默认用等于
                                if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';
                                //用一个删一个
                                //如果当前的选择框的值,等于get的字段值
                                if(ec_value === trg_index_arr[0] && s_val === trg_index_arr[1]){
                                    v_val = trg_item;//将v改为当前get的
                                    // $('.s:eq(' + ec_index + ')').combobox('setValue', trg_index_arr[1]);//设置S的值
                                    delete this_url_get[trg_index];
                                }
                            });
                            //没找到就正常处理
                            $('.v:eq(' + ec_index + ')').textbox('setValue', v_val);
                        });
                        //判断是否有自动查询参数
                        var no_query_get = ['auto_click'];//这里存不需要塞入查询的一些特殊变量
                        $.each(no_query_get, function (i,it) {
                            delete this_url_get[it];
                        });
                        //完全没找到的剩余值,在这里新增一个框进行选择
                        $.each(this_url_get, function (trg_index, trg_item) {
                            add_tr();//新增一个查询框
                            var trg_index_arr = trg_index.split('-');
                            //没有第二个参数时,默认用等于
                            if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';

                            $('#cx table tbody tr:last-child .f').combobox('setValue', trg_index_arr[0]);
                            $('#cx table tbody tr:last-child .s').combobox('setValue', trg_index_arr[1]);
                            $('#cx table tbody tr:last-child .v').textbox('setValue', trg_item);
                        });

                        //这里自动执行查询,显示明细
                        if(auto_click === '1'){
                            doSearch();
                        }
                    });
                }else{
                    $.messager.alert("<?= lang('提示');?>", res.msg);
                }
            },error:function (e) {
                ajaxLoadEnd();
                $.messager.alert("<?= lang('提示');?>", e.responseText);
            }
        });
    }

    /**
     * 新增一个查询框
     */
    function add_tr() {
        $('#cx table tbody').append(add_tr_str);
        $.parser.parse($('#cx table tbody tr:last-child'));
        var last_f = $('#cx table tbody tr:last-child .f');
        var last_f_v = last_f.combobox('getValue');
        last_f.combobox('clear').combobox('select', last_f_v);
        return false;
    }

    /**
     * 删除一个查询
     */
    function del_tr(e) {
        //删除按钮
        var index = $(e).parents('tr').index();
        $(e).parents('tr').remove();
        return false;
    }

    //执行加载函数
    $(function () {
        load_query_box();
    });
</script>
<!--用于easyui 的一些不太重要的模板等函数-->
<script>
    function job_no_for(value,row,index) {
        var consol_no = '<br><span style="color:#808080">' + row.consol_no + '</span>';
        var carrier_ref = '<br><span style="color:#808080">' + row.carrier_ref + '</span>';
        if(row.consol_no == null){
            row.consol_no = '';
            consol_no = '';
        }
        if(row.carrier_ref == null){
            row.carrier_ref = '';
            carrier_ref = '';
        }
        return row.job_no + consol_no + carrier_ref;
    }

    function trans_carrier_name_for(value, row, index) {
        if(row.vessel == null){
            row.vessel = '';
        }
        if(row.voyage == null){
            row.voyage = '';
        }
        var str = '';
        if(row.trans_carrier_name == null){
            str += row.trans_carrier;
        }else{
            str += row.trans_carrier_name;
        }
        str += '<br><span style="color:#808080">' + row.vessel + ' ' + row.voyage + '</span>';
        return str;
    }

    function job_no_styler(value, row, index) {
        var style = '';
        if(row.lock_lv == '1') style += 'background-color:#6699CC';
        if(row.lock_lv == '2') style += 'background-color:#660099';
        if(row.lock_lv == '3') style += 'background-color:#666666';
        return style;
    }
    function booking_ETD_for(value, row, index) {
        if(row.creditor_name == null){
            row.creditor_name = '';
        }
        var str = '';
        str += value;
        str += '<br><span style="color:#808080">' + row.creditor_name + '</span>';
        return str;
    }
    function created_by_name_for(value, row, index) {
        var str = value;
        //将销售换行拼接
        str += "<br>" + row.sales_name;

        return str;
    }

    function trans_origin_for(value, row, index) {
        return row.trans_origin + '<br><span style="color:#808080">' + row.trans_origin_name + '</span>';
    }
    function trans_discharge_for(value, row, index) {
        return row.trans_discharge + '<br><span style="color:#808080">' + row.trans_discharge_name + '</span>';
    }
    function trans_destination_for(value, row, index) {
        return row.trans_destination + '<br><span style="color:#808080">' + row.trans_destination_name + '</span>';
    }
</script>
<script type="text/javascript">
    var url;
    function doSearch(){
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });

        $('#tt').datagrid({
            url: '/biz_shipment/get_data',
            // destroyUrl: '/biz_shipment/delete_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            },
            queryParams: json
        }).datagrid('clearSelections');
        set_config();
        $('#chaxun').window('close');
    }

    var more_searchs = false;
    function more_search(){
        var ed = $('#cx tr:nth-child(-n+2)');
        if(more_searchs){
            ed.css('display', 'none');
        }else{
            ed.css('display', 'table-row');
        }
        more_searchs = !more_searchs;
    }

    function resetSearch(){
        $('.v').textbox('reset');
    }

    function get_statistics(){
        var select = $('#tt').datagrid('getSelections')
            ,good_outers = 0
            ,good_volume = 0
            ,good_weight = 0
            ,box_info_arr = {}
            ,teu = 0
            ,ex_LCL_good_volume = 0
            ,ex_LCL_good_weight = 0
            ,im_LCL_good_volume = 0
            ,im_LCL_good_weight = 0
            ,ex_AIR_good_volume = 0
            ,ex_AIR_good_weight = 0
            ,im_AIR_good_volume = 0
            ,im_AIR_good_weight = 0;
        $.each(select, function (index, item) {
            var this_good_outers = item.good_outers != null ? parseInt(item.good_outers) : 0;
            var this_good_volume = item.good_volume != null ? parseFloat(item.good_volume) : 0.00;
            var this_good_weight = item.good_weight != null ? parseFloat(item.good_weight) : 0.00;
            good_outers += this_good_outers;
            good_volume += this_good_volume;
            good_weight += this_good_weight;
            if(item.trans_mode == 'LCL'){
                if(item.biz_type == 'export'){
                    ex_LCL_good_weight += this_good_weight;
                    ex_LCL_good_volume += this_good_volume;
                }else if(item.biz_type == 'import'){
                    im_LCL_good_weight += this_good_weight;
                    im_LCL_good_volume += this_good_volume;
                }
            }else if(item.trans_mode == 'AIR'){
                if(item.biz_type == 'export'){
                    ex_AIR_good_weight += this_good_weight;
                    ex_AIR_good_volume += this_good_volume;
                }else if(item.biz_type == 'import'){
                    im_AIR_good_weight += this_good_weight;
                    im_AIR_good_volume += this_good_volume;
                }
            }

            //统计箱型箱量
            try{
                var box_info_json = JSON.parse(item.box_info_json);
            }catch (e) {
                var box_info_json = {};
            }
            $.each(box_info_json, function (index2, item2) {
                if(box_info_arr[item2.size] === undefined) box_info_arr[item2.size] = 0;
                box_info_arr[item2.size] += parseInt(item2.num);
            });
        });
        var box_info = [];
        $.each(box_info_arr, function (index, item) {
            box_info.push(index + '*' + item);
            if(index.substring(0,1) == '2'){
                teu += 1 * item;
            }else if(index.substring(0,1) == '4'){
                teu +=  2 * item;
            }
        });
        box_info = box_info.join(', ');
        $('#good_outers').textbox('setValue', good_outers);
        $('#good_volume').textbox('setValue', good_volume);
        $('#good_weight').textbox('setValue', good_weight);
        $('#box_info').textbox('setValue', box_info);
        $('#teu').text(teu);
        $('#ex_LCL_good_volume').text(ex_LCL_good_volume.toFixed(2));
        $('#ex_LCL_good_weight').text(ex_LCL_good_weight.toFixed(2));
        $('#im_LCL_good_volume').text(im_LCL_good_volume.toFixed(2));
        $('#im_LCL_good_weight').text(im_LCL_good_weight.toFixed(2));
        $('#ex_AIR_good_volume').text(ex_AIR_good_volume.toFixed(2));
        $('#ex_AIR_good_weight').text(ex_AIR_good_weight.toFixed(2));
        $('#im_AIR_good_volume').text(im_AIR_good_volume.toFixed(2));
        $('#im_AIR_good_weight').text(im_AIR_good_weight.toFixed(2));
    }

    $(function(){
        $('#chaxun').window('open');
        $('#cx').on('keydown', 'input', function (e) {
            if(e.keyCode == 13){
                doSearch();
            }
        });

        //节点点击后自动保存
        $('.status').change(function () {
            var row = $('#tt').datagrid('getSelected');
            if (row){
                var val = 0;
                var inp = $(this);
                var checked = $(this).prop('checked');
                if(checked) {
                    var day1 = new Date();
                    var month = '';
                    if (day1.getMonth() + 1 < 10) {
                        month = '0' + (day1.getMonth() + 1);
                    } else {
                        month = day1.getMonth() + 1;
                    }
                    val = day1.getFullYear() + "-" + month + "-" + day1.getDate() + ' ' + day1.getHours() + ':' + day1.getMinutes() + ':' + day1.getSeconds();
                }

                var field = $(this).attr('name');
                var data = {};
                var shipment_id = row.id;
                data[field] = val;
                $.ajax({
                    url: '/biz_shipment/update_checkbox/' + shipment_id,
                    type: 'POST',
                    data:data,
                    dataType:'json',
                    success: function (res) {
                        if(res.code == 1){
                            $.messager.alert('Tips', res.msg);
                            inp.prop('checked', !checked)
                        }else if(res.code == 2){
                            $.messager.alert('Tips', res.msg);
                        }
                    },error:function () {
                        $.messager.alert('Tips', '发生错误');
                    }
                });
            }else{
                $.messager.alert("tips","No Item Selected");
            }
        });
    });

    function add() {
        window.open("/biz_shipment/add/?consol_id=&ebooking_id=0&page=export_FCL_SEA") ;
    }
    function copy(){
        var row = $('#tt').datagrid('getSelected');
        if (row){
            <?php
            if(empty($_GET["ebooking"])) {
                echo 'window.open("/biz_shipment/add/" + row.id) ;';
            }else{
                echo 'window.open("/biz_shipment/add/" + row.id + "?ebooking_id='.$_GET["ebooking"].'") ;';
            }
            ?>


        }else{
            $.messager.alert("tips","No Item Selected");
        }
    }
    /**
     * 跳转到EBOOKING
     */
    function ebooking(){
        var url = "http://" + window.location.host;
        window.parent.$('body').app('createwindow', {text:"Ebooking", href: url + '/biz_shipment_ebooking/index?status=0', uuid: 'eboooking20220324'});
    }
    function fee(){
        var row = $('#tt').datagrid('getSelected');
        if (row){
            $('#f').window('open');
            document.getElementById('fee_f').src='/biz_fee/index/shipment/'+row.plate_no;
        }else{
            $.messager.alert("tips","No Item Selected");
        }
    }
    function config(){
        window.open("/sys_config_title/index_user?table_name=" + table_name + "&view_name=" + view_name) ;
    }
    function upload(){
        var row = $('#tt').datagrid('getSelected');
        if (row){
            window.open("/upload/index/biz_shipment/" + row.id) ;    // 待修改
        }else{
            $.messager.alert("tips","No Item Selected");
        }
    }
    function upload_file(type=''){
        var row = $('#tt').datagrid('getSelected');
        if (row){
            window.open("/bsc_upload/index/"+type+"/" + row.id) ;
        }else{
            $.messager.alert("tips","No Item Selected");
        }
    }
    function del(){
        var row = $('#tt').datagrid('getSelected');
        if (row){
            //if(row.step!="未收款"){
            //	alert("不可以删除");
            //}else{
            $('#tt').edatagrid('destroyRow');
            //}

        }else{
            alert("No Item Selected");
        }
    }
    function save(){
        $('#fm').form('submit',{
            url: url,
            onSubmit: function(){
                return $(this).form('validate');
            },
            success: function(result){
                $('#dlg').dialog('close');
                $('#tt').datagrid('reload');
            }
        });
    }

    var singleSelect = true;
    function multiple() {
        if(singleSelect){
            singleSelect = false;
        }else{
            singleSelect = true;
        }
        $('#tt').datagrid({singleSelect: singleSelect});
    }
    function ajax_get(url,name, is_cache = true) {
        ajaxLoading();
        return $.ajax({
            type:'POST',
            url:url,
            success:function (res_json) {
                ajaxLoadEnd();
                try{
                    var res = $.parseJSON(res_json);
                    ajax_data[name] = res;
                }catch (e) {

                }
            },
            error:function () {
                ajaxLoadEnd();
                // $.messager.alert('获取数据错误');
            }
        });
    }

    var not_all = false;
    function reload_all_data(first_page) {
        var tt = $('#tt');
        // tt.datagrid('getO')
        var options = tt.datagrid('options');
        if(!not_all){
            tt.datagrid('gotoPage', {
                page: first_page,
                callback: function(page){
                    tt.datagrid('selectAll');
                    var row_data = tt.datagrid('getData');
                    var this_total = (page - 1) * options.pageSize + row_data.rows.length;
                    if(row_data.rows.length !== 0 && this_total < row_data.total){
                        reload_all_data(page + 1);
                    }
                }
            })
        }else{
            not_all = false;
        }
    }

    function check_all_statistics(){
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });

        $.ajax({
            type:'POST',
            url:'/biz_shipment/get_data_statistics',
            data:cx_data,
            dataType:'json',
            success:function(rec){
                var data = rec.data;
                $('#good_outers').textbox('setValue', data.good_outers);
                $('#good_volume').textbox('setValue', data.good_volume);
                $('#good_weight').textbox('setValue', data.good_weight);
                $('#box_info').textbox('setValue', data.box_info);
                $('#teu').text(data.teu);
                $('#ex_LCL_good_volume').text(data.ex_LCL_good_volume);
                $('#ex_LCL_good_weight').text(data.ex_LCL_good_weight);
                $('#im_LCL_good_volume').text(data.im_LCL_good_volume);
                $('#im_LCL_good_weight').text(data.im_LCL_good_weight);
                $('#ex_AIR_good_volume').text(data.ex_AIR_good_volume);
                $('#ex_AIR_good_weight').text(data.ex_AIR_good_weight);
                $('#im_AIR_good_volume').text(data.im_AIR_good_volume);
                $('#im_AIR_good_weight').text(data.dataim_AIR_good_weight);
            },error:function(){
                $.messager.alert('Tips', '发生错误');
            }
        });

    }

    function fyqrd_jh_excel() {
        var select = $('#tt').datagrid('getSelections');
        if(select.length > 0){
            var json = {};
            var cx_data = $('#fyqrd_form').serializeArray();
            var is_export = true;
            var end_each = false;
            // var status_exp = 0;
            $.each(cx_data, function (index, item) {
                //判断如果name存在,且为string类型
                if(json.hasOwnProperty(item.name) === true){
                    if(typeof json[item.name] == 'string'){
                        json[item.name] = json[item.name].split(',');
                        json[item.name].push(item.value);
                    }else{
                        json[item.name].push(item.value);
                    }
                }else{
                    json[item.name] = item.value;
                }
            });
            var url = [];
            $.each(select, function(s_index, s_item){
                // GET方式示例，其它方式修改相关参数即可

                var id = s_item.id;

                var parameter = '';
                $.each(json, function (index, item) {
                    if(typeof item === 'string'){
                        parameter += '&' + index + '=' + item;
                    }else{
                        $.each(item, function(index2, item2){
                            parameter += '&parameter[' + index2 + ']=' + item2;
                        })
                    }
                });
                url.push('/export/post_export?template_id=16&data_table=shipment&id=' + id + parameter);
            });
            var i = 0;
            function open_all(url, i){
                window.open(url[i], 'aaa_' + i);
                i++;
                if(i < url.length){
                    open_all(url, i);
                }
            }
            open_all(url, i);
        }else{
            $.messager.alert('Tips', '<?= lang('No columns selected')?>');
        }
    }

    function export_selected(){
        var param = {};
        var tt = $('#tt');
        param.filename = 'SHIPMENT表格导出.xls';
        param.rows = tt.datagrid('getSelections');
        if(param.rows.length == 0){
            param = param.filename;
        }else{
            param.worksheet = 'SHIPMENT';
        }
        tt.datagrid('toExcel', param);
    }

    function batch_lock(lock_lv = 3){
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            $.messager.confirm('确认对话框', '是否确认将选择的对应SHIPMENT锁住？', function(r){
                if (r){
                    // 退出操作;
                    ajaxLoading();
                    var shipment_ids = [];
                    $.each(rows, function(index, item){
                        if($.inArray(item.id, shipment_ids) == -1)shipment_ids.push(item.id);
                    });
                    $.ajax({
                        type: 'POST',
                        url: '/biz_shipment/batch_lock',
                        data: {
                            shipment_ids: shipment_ids.join(','),
                            lock_lv: lock_lv,
                        },
                        success: function (res_json) {
                            var res;
                            try{
                                res = $.parseJSON(res_json);
                            }catch(e){

                            }
                            if(res == undefined){
                                ajaxLoadEnd();
                                $.messager.alert('Tips', res_json);
                                return;
                            }
                            ajaxLoadEnd();
                            if(res.code == 0){
                                excel_datas = {};
                                excel_param = {filename: '锁失败数据', caption: 'SHIPMENT锁数据表格', worksheet: 'SHIPMENT锁'};
                                excel_param.fields = ['SHIPMENT', '原因'];
                                $.each(res.data, function(index, item){
                                    // excel_param.fields.push(item.key_name);
                                    $.each(item.data, function(id_index,id_item){
                                        if(excel_datas[id_index] == undefined) excel_datas[id_index] = {};
                                        excel_datas[id_index]['SHIPMENT'] = id_item;
                                        excel_datas[id_index]['原因'] = item.key_name;
                                    });
                                });

                                var alert_msg = res.msg;
                                if(Object.keys(excel_datas).length > 0){
                                    alert_msg += "<br /> <a href=\"javascript:void(0);\" onclick=\"export_Excel()\">点击下载</a>";
                                }
                                $.messager.alert({
                                    title:'Tips',
                                    msg: alert_msg,
                                    icon: 'info',
                                    width: '400px',
                                    height: '200px',
                                });
                            }else{
                                $.messager.alert('Tips', res.msg);
                            }
                        }
                    });
                }
            });
        }else {
            $.messager.alert('<?= lang('Tips')?>', '<?= lang('No columns selected')?>');
        }
    }

    function batch_lock3(){
        $.messager.confirm('确认对话框', '是否确认将当前查询条件下所有SHIPMENT锁住？', function(r){
            if (r){
                // 退出操作;
                ajaxLoading();
                var json = {};
                var cx_data = $('#cx').serializeArray();
                // var status_exp = 0;
                $.each(cx_data, function (index, item) {
                    //判断如果name存在,且为string类型
                    if(json.hasOwnProperty(item.name) === true){
                        if(typeof json[item.name] == 'string'){
                            json[item.name] = json[item.name].split(',');
                            json[item.name].push(item.value);
                        }else{
                            json[item.name].push(item.value);
                        }
                    }else{
                        json[item.name] = item.value;
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: '/biz_shipment/batch_lock3',
                    data: json,
                    success: function (res_json) {
                        var res;
                        try{
                            res = $.parseJSON(res_json);
                        }catch(e){

                        }
                        if(res == undefined){
                            ajaxLoadEnd();
                            $.messager.alert('Tips', res_json);
                            return;
                        }
                        ajaxLoadEnd();
                        if(res.code == 0){
                            excel_datas = {};
                            excel_param = {filename: '锁失败数据', caption: 'SHIPMENT锁数据表格', worksheet: 'SHIPMENT锁'};
                            excel_param.fields = [];
                            $.each(res.data, function (index, item) {
                                excel_param.fields.push(item.key_name);
                                $.each(item.data, function (id_index, id_item) {
                                    if (excel_datas[id_index] == undefined) excel_datas[id_index] = {};
                                    excel_datas[id_index][item.key_name] = id_item;
                                });
                            });

                            var alert_msg = res.msg;
                            if (Object.keys(excel_datas).length > 0) {
                                alert_msg += "<br /> <a href=\"javascript:void(0);\" onclick=\"export_Excel()\">点击下载</a>";
                            }
                            $.messager.alert({
                                title: 'Tips',
                                msg: alert_msg,
                                icon: 'info',
                                width: '400px',
                                height: '200px',
                            });
                        }else{
                            $.messager.alert('Tips', res.msg);
                        }
                    }
                });
            }
        });
    }

    function lock_lv_select(rec){
        //此处为辅助勾选
        //首先清空勾选
        $('input[name="lock_lv[]"]').attr('checked', false);
        $('input[name="consol_lock_lv[]"]').attr('checked', false);

        var val = rec.value;
        if(val == 'C0'){
            $('#C0').prop('checked', true);
        }else if(val == 'C3'){
            $('#C3').prop('checked', true);
            $('#S0').prop('checked', true);
        }else if(val == 'CALL'){
            $('#C0').prop('checked', true);
            $('#C3').prop('checked', true);
            $('#S0').prop('checked', true);
        }else if(val == 'S1'){
            $('#C3').prop('checked', true);
            $('#S1').prop('checked', true);
        }else if(val == 'S2'){
            $('#C3').prop('checked', true);
            $('#S2').prop('checked', true);
        }else if(val == 'S3'){
            $('#C3').prop('checked', true);
            $('#S3').prop('checked', true);
        }else if(val == 'SN3'){
            $('#C0').prop('checked', true);
            $('#C3').prop('checked', true);
            $('#S0').prop('checked', true);
            $('#S1').prop('checked', true);
            $('#S2').prop('checked', true);
        }else{
            $.messager.alert('Tips', '暂不支持');
        }
    }


</script>
<script>
    $(function () {
        setInterval(()=>{
            $.getJSON("/sys_desktop/count", function(res){
                $("#ebooking_count").text("【"+res.Ebooking+"】")
            });
        },60000)
    })
</script>
<table id="tt"
       rownumbers="false" pagination="true"  idField="id"
       pagesize="30" toolbar="#tb"  singleSelect="true" nowrap="false">
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add();"><?= lang('add');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="copy();"><?= lang('copy');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm2',plain:true" iconCls="icon-reload"><?= lang('config');?></a>
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm4',plain:true" iconCls="icon-reload"><?= lang('export');?></a>
                <?php if(menu_role('shipment_batch_lock1')){ ?>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-lock" plain="true" onclick="batch_lock(1);" title="<?= lang('op主管锁,要求C3先锁(没市场除外)');?>"><?= lang('S1锁');?></a>
                <?php } ?>
                <?php if(menu_role('shipment_lock2')){ ?>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-lock" plain="true" onclick="batch_lock(2);" title="<?= lang('sales销售锁');?>"><?= lang('S2锁');?></a>
                <?php } ?>
                <?php if(menu_role('shipment_lock3')){ ?>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-lock" plain="true" onclick="batch_lock(3);" title="<?= lang('财务锁');?>"><?= lang('S3锁');?></a>
                <?php } ?>
                <?php if(in_array(get_session('id'), array(20061)) || is_admin()){ ?>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-lock" plain="true" onclick="batch_lock3();" title="<?= lang('财务锁');?>"><?= lang('直接锁到S3');?></a>
                <?php } ?>
                <!--<span style="color:red;">原批量锁功能已搬到 利润表</span>-->
                <a href="javascript:void;" class="easyui-tooltip tooltip-f" data-options="
                    content: $('<div></div>'),
                    onShow: function(){
                        $(this).tooltip('arrow').css('left', 20);
                        $(this).tooltip('tip').css('left', $(this).offset().left);
                    },
                    onUpdate: function(cc){
                        cc.panel({
                            width: 300,
                            height: 'auto',
                            border: false,
                            href: '/bsc_help_content/help/shipment_index'
                        });
                    }
                " title=""><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>


            </td>
        </tr>
        <tr>
            <td>
                <form id="node_fm">
                </form>
            </td>
        </tr>
    </table>

</div>
<div class="statistics">
    box_info<input class="easyui-textbox" id="box_info" readonly style="width: 300px;" value="">
    件数<input class="easyui-textbox" id="good_outers" readonly style="width: 50px;" value="0">
    毛重<input class="easyui-textbox" id="good_weight" readonly style="width: 50px;" value="0.00">
    体积<input class="easyui-textbox" id="good_volume" readonly style="width: 50px;" value="0.00">
    <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="javascript:reload_all_data(1);"><?= lang('全选');?></a>
    <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="javascript:check_all_statistics();"><?= lang('快速统计');?></a>
    <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="javascript:$('#tt').datagrid('clearSelections');not_all=true"><?= lang('全不选');?></a>
    总数:
    <span class="all_t">teu</span>:
    <span id="teu">0</span>
    <span class="all_t">ex-LCL</span>:
    <span id="ex_LCL_good_weight">0</span>KGS/<span id="ex_LCL_good_volume">0</span>CBM
    <span class="all_t">im-LCL</span>:
    <span id="im_LCL_good_weight">0</span>KGS/<span id="im_LCL_good_volume">0</span>CBM
    <span class="all_t">ex-AIR</span>:
    <span id="ex_AIR_good_weight">0</span>KGS/<span id="ex_AIR_good_volume">0</span>CBM
    <span class="all_t">im-AIR</span>:
    <span id="im_AIR_good_weight">0</span>KGS/<span id="im_AIR_good_volume">0</span>CBM
</div>
<div id="mm2" style="width:150px;">
    <div data-options="iconCls:'icon-ok'" onclick="javascript:config()"><?= lang('column');?></div>
    <div data-options="iconCls:'icon-ok'" onclick="javascript:multiple()"><?= lang('multiple');?></div>
</div>
<div id="mm4" style="width:150px;">
    <div data-options="iconCls:'icon-ok'" onclick="export_selected()"><?= lang('原始表格导出');?></div>
</div>
<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:700px;height:480px;padding:5px;">
    <br><br>
    <form id="cx">
        <table>
            <tr>
                <td><?= lang('carrier_ref');?></td>
                <td align="left">
                    <input name="carrier_ref" class="easyui-textbox"  style="width:395px;">
                </td>
            </tr>
            <tr>
                <td><?= lang('锁级');?></td>
                <td align="left">
                    <input name="lock_lv[]" id="S0" type="checkbox" value="0">s未锁
                    <input name="lock_lv[]" id="S1" type="checkbox" value="1"><span title="操作锁"> s1级</span>
                    <input name="lock_lv[]" id="S2" type="checkbox" value="2"><span title="销售锁"> s2级 </span>
                    <input name="lock_lv[]" id="S3" type="checkbox" value="3"><span title="财务锁"> s3级 </span>

                    <input name="consol_lock_lv[]" id="C0" type="checkbox" value="0">c未锁
                    <input name="consol_lock_lv[]" id="C3" type="checkbox" value="3"><span title="carrier_cost锁"> c3级</span>
                    <select class="easyui-combobox" editable="false" style="width:400px" data-options="
                            onSelect:function(rec){
                                lock_lv_select(rec);
                            },
                        ">
                        <option value="">--请选择--</option>
                        <option value="C0">c未锁</option>
                        <option value="C3">c3级(s未锁)</option>
                        <option value="CALL">S1未锁(C未锁+c3级)</option>
                        <option value="S1">s1级(操作锁)</option>
                        <option value="S2">s2级(销售锁)</option>
                        <option value="S3">s3级(财务锁)</option>
                        <option value="SN3">s3未锁(C未锁+C3级+S1级+S2级)</option>
                    </select>

                    <a href="javascript:void;" class="easyui-tooltip" data-options="
                            content: $('<div></div>'),
                            onShow: function(){
                                $(this).tooltip('arrow').css('left', 20);
                                $(this).tooltip('tip').css('left', $(this).offset().left);
                            },
                            onUpdate: function(cc){
                                cc.panel({
                                    width: 300,
                                    height: 'auto',
                                    border: false,
                                    href: '/bsc_help_content/help/lock_query_note'
                                });
                            }
                        "><?php echo lang('help');?></a>
                </td>
            </tr>
           <!-- <tr>
                <td><?= lang('label');?></td>
                <td align="left">
                    <input name="label[]" type="checkbox" value="1">扣单
                    <input name="label[]" type="checkbox" value="2">未开票
                    <input name="label[]" type="checkbox" value="3">亏损
                    <input name="label[]" type="checkbox" value="4">未核销
                </td>
            </tr>
            <tr>
                <td><?= lang('节点');?> </td>
                <td align="left">
                    <input name="step_info[]" type="checkbox" value="dadanwancheng">打单
                    <input name="step_info[]" type="checkbox" value="haiguancangdan">舱单
                    <input name="step_info[]" type="checkbox" value="baoguanjieshu">报关
                    <input name="step_info[]" type="checkbox" value="xiangyijingang">进港
                    <input name="step_info[]" type="checkbox" value="haiguanfangxing">海放
                    <input name="step_info[]" type="checkbox" value="matoufangxing">码放
                    <input name="step_info[]" type="checkbox" value="peizaifangxing">配载
                    <input name="step_info[]" type="checkbox" value="chuanyiqihang">船开
                    <input name="step_info[]" type="checkbox" value="tidanqueren">SI
                    <input name="step_info[]" type="checkbox" value="tidanqianfa">B/L签发
                    <input name="step_info[]" type="checkbox" value="yizuofeiyong">操作锁
                </td>
            </tr>-->
        </table>

        <br><br>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="doSearch();"><?= lang('search'); ?></button>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="resetSearch();"><?= lang('重置'); ?></button>

        <a href="javascript:void;" class="easyui-tooltip" data-options="
            content: $('<div></div>'),
            onShow: function(){
                $(this).tooltip('arrow').css('left', 20);
                $(this).tooltip('tip').css('left', $(this).offset().left);
            },
            onUpdate: function(cc){
                cc.panel({
                    width: 500,
                    height: 'auto',
                    border: false,
                    href: '/bsc_help_content/help/query_condition'
                });
            }
        "><?php //echo lang('help');?></a>
        <a href="javascript:void(0)" id="more_field" onclick="javascript:more_search();"><?= lang('more'); ?></a>
    </form>
</div>
