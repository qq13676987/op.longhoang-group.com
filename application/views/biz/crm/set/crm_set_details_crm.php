<body >
<div >
    <table class="easyui-datagrid" id="tt"  rownumbers="false" pagination="false" idField="id"
           singleSelect="true" nowrap="false"  fitColumns="true" height="1000px" >
        <thead>
        <tr>
            <th data-options="field:'created_time',width:80,align:'center'">创建日期</th>
            <th data-options="field:'user_name',width:80,align:'center'">当前销售</th>
            <th data-options="field:'client_company',width:80,align:'center'">客户名称</th>
            <th data-options="field:'export_sailing',width:80,align:'center'">出口航线</th>
            <th data-options="field:'trans_mode',width:80,align:'center'">运输方式</th>
        </tr>
        </thead>
 
    </table>
</div>
</body>
<script>
    $('#tt').datagrid({
        url:'/biz_crm/crm_set_details_crm_get_data/<?=$group?>/<?=$type?>?date_s=<?=$date_s?>&date_e=<?=$date_e?>',
    });
</script>