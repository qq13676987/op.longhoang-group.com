<?php
if ( ! function_exists('create_shipment_hbl'))
{
    function create_shipment_hbl($shipment_id){
        $CI =& get_instance();
        $template_ids = array('LEAGUESHIPPING' => '22', 'KIMXIN SUPPLY CHAIN' => '37', '41', '42');
        $upload_type = array('OBL' => 'B/L original', 'TER' => 'B/L telex realse', 'SWB' => 'B/L telex realse');
    
        $CI->load->model(array('bsc_upload_model', 'bsc_template_model', 'biz_shipment_model'));
        $templates = array_column($CI->bsc_template_model->get("id in (" . join(',', $template_ids) . ")"), null, 'id');
        $url = "http://china.leagueshipping.com/export/export_word/3";
        $upload_shipment = array();
        
        $CI->db->select('id, release_type, hbl_type');
        $shipment = $CI->biz_shipment_model->get_by_id($shipment_id);
        
        $shipment_uploads = $CI->bsc_upload_model->get_by_id_no('shipment', $shipment['id']);
        if(!isset($upload_type[$shipment['release_type']])){
            return false;
        }
        
        $this_upload_type = $upload_type[$shipment['release_type']];
        
        $shipments_upload_ts =  array_column($shipment_uploads, 'type_name');
        if(in_array($this_upload_type, $shipments_upload_ts)){
            return true;
        }
       
        //TODO 疑问:如果是MBL的，选择哪个模板呢，另外的常州那2个模板呢
        if(!isset($template_ids[$shipment['hbl_type']])){
            return false;
        }
        
        $template = $templates[$template_ids[$shipment['hbl_type']]];
        $this_url = $url . "?file_name={$template['file_template']}&data_table=shipment&id={$shipment['id']}";
        $post = array('parameter' => array('B_L_number' => 'THREE'));
        $headers = [
        ];
        $result_json = curl_post_body($this_url,$headers, http_build_query($post));
        $result = json_decode($result_json, true);
        
        if(!isset($result['code']) || (isset($result['code']) && $result['code'] != 0)){
            return false;
        }
       
        //上传文件
        //根据文件路径剪切文件到原本的上传目录，然后保存上传记录到该shipment
        $pdf_path = "http://pdf.leagueshipping.com" . $result['file_path'];
        $file = file_get_contents($pdf_path);
        
        $pdf_name = explode('/', $template['file_template'])[2];
        $new_name = strstr($pdf_name, '.', true) . '-' . time() . rand(1000,9999) .  '.pdf';
        $root_path = dirname(BASEPATH);
    
        $path = date('Ymd');
        $new_dir = $root_path . '/upload/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $new_path = $new_dir . $new_name;
        $new_path = str_replace('\\', DIRECTORY_SEPARATOR, $new_path);
        $new_path = str_replace('/', DIRECTORY_SEPARATOR, $new_path);
        file_put_contents($new_path, $file);
        $this_upload_type = $upload_type[$shipment['release_type']];
        
        $upload_data = array();
        $upload_data['file_name'] = '/' . $path . '/' . $new_name;
        $upload_data['biz_table'] = 'biz_shipment';
        $upload_data['type_name'] = $this_upload_type;
        $upload_data['id_no'] = $shipment['id'];
        $upload_data['approve'] = 1;
        $CI->bsc_upload_model->save($upload_data);
        
        return true;
    }
}

if(!function_exists('shipment_client_is_pay_buy')){
    /**
     * 判断shipment的client是否是付买状态
     * @param int $shipment_id
     * @param string $finance_od_basis
     * @param string $finance_payment_days
     * @return bool
     */
    function shipment_client_is_pay_buy($shipment_id = 0, $finance_od_basis = '', $finance_payment_days = ''){
        $CI =& get_instance();
        $CI->load->model('biz_shipment_model');

        //查询shipment对应client的 付买状态
        if($finance_od_basis == '' || $finance_payment_days == ''){
            $CI->db->select('biz_shipment.id, biz_client.finance_od_basis,biz_client.finance_payment_days');
            $CI->db->join('biz_client', 'biz_client.client_code = biz_shipment.client_code', 'LEFT');
            $shipment = $CI->biz_shipment_model->get_where_one_all("biz_shipment.id = $shipment_id");
            if(empty($shipment)){
                return false;
            }
            $finance_od_basis = $shipment['finance_od_basis'];
            $finance_payment_days = $shipment['finance_payment_days'];
        }

        if($finance_od_basis == 'daily' && $finance_payment_days == 0){
            return true;
        }else{
            return false;
        }
    }
}

if(!function_exists('shipment_consol_is_pay_buy')){
    /**
     * 判断shipment的consol是否是付买状态
     * @param int $shipment_id
     * @param int $pay_buy_flag
     * @return bool
     */
    function shipment_consol_is_pay_buy($shipment_id = 0, $pay_buy_flag = 0){
        $CI =& get_instance();
        $CI->load->model('biz_shipment_model');

        //查询shipment对应client的 付买状态
        if($pay_buy_flag == 0){
            $CI->db->select('biz_shipment.id, biz_shipment.consol_id,biz_consol.pay_buy_flag');
            $CI->db->join('biz_consol', 'biz_consol.id = biz_shipment.consol_id', 'LEFT');
            $shipment = $CI->biz_shipment_model->get_where_one_all("biz_shipment.id = $shipment_id");
            if(empty($shipment)){
                return false;
            }
            //如果没有绑定consol,返回不是付买
            if($shipment['consol_id'] == 0){
                return false;
            }
            $pay_buy_flag = $shipment['pay_buy_flag'];
        }

        if($pay_buy_flag == 1){
            return true;
        }else{
            return false;
        }
    }
}

if(!function_exists('get_shipment_field_all')){
    /**
     * 获取shipment的表格配置
     * @return array
     */
    function get_shipment_field_all(){
        return $field_all = array(
            array("shipper_ref", "shipper_ref", "105", "9", '1', 'client info'),
            array("id", "ID", "0", "1", '1'),
            array("client_code", "Client Code", "0", "0", '1', 'client info'),
            array("client_company", "Client Company", "130", "3", '1', 'client info'),
            array("trans_origin", "trans_origin", "0", "9", '1', 'booking owner'),
            array("trans_origin_name", "trans_origin_name", "60", "9", '1', 'booking owner'),
            array("trans_discharge", "trans_discharge", "0", "9", '1', 'booking owner'),
            array("trans_discharge_name", "trans_discharge_name", "56", "9", '1', 'booking owner'),
            array("trans_destination", "trans_destination", "0", "9", '1', 'booking owner'),
            array("trans_destination_terminal", "trans_destination_terminal", "0", "9", '1', 'booking owner'),
            array("trans_destination_name", "trans_destination_name", "105", "9", '1', 'booking owner'),
            array("job_no", "Job No", "147", "2", '1'),
            array("booking_ETD", "booking_ETD", "103", "9", '1', 'booking owner'),
            array("trans_carrier", "trans_carrier", "149", "9", '1', 'booking owner'),
            array("biz_type", "biz_type", "47", "0", '1'),
            array("status", "status", "50", "0", '1'),
            array("client_address", "Client_Address", "0", "4", '1', 'client info'),
            array("client_contact", "client_contact", "0", "5", '1', 'client info'),
            array("client_telephone", "client_telephone", "0", "6", '1', 'client info'),
            array("shipper_company", "shipper_company", "120", "7", '1', 'booking info'),
            array("shipper_address", "shipper_address", "0", "8", '1', 'booking info'),
            array("shipper_contact", "shipper_contact", "0", "9", '1', 'booking info'),
            array("shipper_telephone", "shipper_telephone", "0", "9", '1', 'booking info'),
            array("shipper_email", "shipper_email", "0", "9", '1', 'booking info'),
            array("consignee_company", "consignee_company", "120", "9", '1', 'booking info'),
            array("consignee_address", "consignee_address", "0", "9", '1', 'booking info'),
            array("consignee_contact", "consignee_contact", "0", "9", '1', 'booking info'),
            array("consignee_telephone", "consignee_telephone", "0", "9", '1', 'booking info'),
            array("consignee_email", "consignee_email", "0", "9", '1', 'booking info'),
            array("notify_company", "notify_company", "0", "9", '1', 'booking info'),
            array("notify_address", "notify_address", "0", "9", '1', 'booking info'),
            array("notify_contact", "notify_contact", "0", "9", '1', 'booking info'),
            array("notify_telephone", "notify_telephone", "0", "9", '1', 'booking info'),
            array("notify_email", "notify_email", "0", "9", '1', 'booking info'),
            array("trans_mode", "trans_mode", "80", "9", '1'),
            array("trans_tool", "trans_tool", "80", "9", '1'),
            array("description", "description", "0", "9", '1'),
            array("description_cn", "description_cn", "0", "9", '1'),
            array("mark_nums", "mark_nums", "0", "9", '1'),
            array("release_type", "release_type", "60", "9", '1'),
            array("sailing_code", "sailing_code", "60", "9", '1', 'booking owner'),//航线代码
            array("sailing_area", "sailing_area", "60", "9", '1', 'booking owner'),//航线区域
            array("good_outers", "good_outers", "60", "9", '1', 'booking cargo'),
            array("good_outers_unit", "good_outers_unit", "60", "9", '1', 'booking cargo'),
            array("good_weight", "good_weight", "60", "9", '1', 'booking cargo'),
            array("good_weight_unit", "good_weight_unit", "60", "9", '1', 'booking cargo'),
            array("good_volume", "good_volumn", "60", "9", '1', 'booking cargo'),
            array("good_volume_unit", "good_volumn_unit", "60", "9", '1', 'booking cargo'),
            // array("good_describe", "good_describe", "0", "9", '1', 'booking cargo'),
            array("good_commodity", "good_commodity", "0", "9", '1', 'booking cargo'),
            array("INCO_term", "INCO_term", "0", "9", '1', 'booking cargo'),
            array("hbl_no", "hbl_no", "60", "9", '1'),
            array("hbl_type", "hbl_type", "0", "9", '1'),
            array("consol_id", "consol_id", "0", "9", '1'),
            array("warehouse_no", "warehouse_code", "0", "0", '1'),//仓库编号
            array("warehouse_code", "warehouse_code", "0", "0", '1'),//仓库code
            array("warehouse_contact", "warehouse_contact", "0", "0", '1'),//仓库联系人
            array("warehouse_address", "warehouse_address", "0", "0", '1'),//仓库联系人地址
            array("warehouse_telephone", "warehouse_telephone", "0", "0", '1'),//仓库联系人电话
            array("warehouse_request", "warehouse_request", "0", "0", '1'),//进仓要求
            array("warehouse_charge", "warehouse_charge", "0", "0", '1'),//收费标准
            array("warehouse_in_date", "warehouse_in_date", "0", "0", '1'),//入库时间
            array("warehouse_in_num", "warehouse_in_num", "0", "0", '1'),//入库数量
            array("created_by", "created_by", "150", "10", '1'),
            array("created_time", "created_time", "150", "10", '1'),
            array("updated_by", "updated_by", "150", "10", '1'),
            array("updated_time", "updated_time", "150", "11", '1'),
            array("dadanwancheng", "dadanwancheng", "0", "0", '1', 'step info'),
            array("haiguancangdan", "haiguancangdan", "0", "0", '1', 'step info'),
            array("baoguanjieshu", "baoguanjieshu", "0", "0", '1', 'step info'),
            array("xiangyijingang", "xiangyijingang", "0", "0", '1', 'step info'),
            array("haiguanfangxing", "haiguanfangxing", "0", "0", '1', 'step info'),
            array("matoufangxing", "matoufangxing", "0", "0", '1', 'step info'),
            array("peizaifangxing", "peizaifangxing", "0", "0", '1', 'step info'),
            array("chuanyiqihang", "chuanyiqihang", "0", "0", '1', 'step info'),
            array("tidanqueren", "tidanqueren", "0", "0", '1', 'step info'),
            array("tidanqianfa", "tidanqianfa", "0", "0", '1', 'step info'),
            array("yizuofeiyong", "yizuofeiyong", "0", "0", '1', 'step info'),
            array("box_info", "box_info", "150", "0", '1', 'booking owner'),
            array("AGR_no", "AGR_no", "0", "0", '1', 'booking owner'),
            array("dangergoods_id", "dangergoods_id", "0", "0", '1', 'booking cargo'),
            array("hs_code", "hs_code", "0", "0", '1', 'booking cargo'),
            array("goods_type", "goods_type", "0", "0", '1', 'booking cargo'),
            array("requirements", "requirements", "0", "0", '1', 'booking cargo'),
            array("service_options", "service_options", "0", "0", '1'),
            array("INCO_option", "INCO_term_option", "0", "0", '1'),
            array("INCO_address", "INCO_term_sp_address", "0", "0", '1'),

            array("cus_no", "cus_no", "0", "2", '1'),
            array("client_email", "client_email", "0", "6", '1', 'client info'),
            array("trans_term", "trans_term", "0", "9", '1'),
            array("remark", "remark", "0", "9", '1'),
            array("consol_id_apply", "consol_id_apply", "0", "9", '1'),
//        array("truck_date", "truck_date", "111", "6", '2'),
//        array("truck_company", "truck_company", "0", "6", '2'),
//        array("truck_address", "truck_address", "0", "6", '2'),
//        array("truck_contact", "truck_contact", "0", "6", '2'),
//        array("truck_telephone", "truck_telephone", "0", "6", '2'),
//        array("truck_remark", "truck_remark", "0", "6", '2'),
//        array("trans_ETD", "trans_ETD", "60", "9", '3'),
//        array("trans_ATD", "trans_ATD", "60", "9", '3'),
//        array("trans_ETA", "trans_ETA", "60", "9", '3'),
//        array("trans_ATA", "trans_ATA", "60", "9", '3'),
            array("issue_date", "issue_date", "60", "9", '1'),
//        array("expiry_date", "expiry_date", "60", "9", '3'),
        );
    }
}

if(!function_exists('get_shipment_admin_field')) {
    /**
     * 获取shipment的admin_field
     */
    function get_shipment_admin_field(){
        return array('operator', 'customer_service', 'sales');//, 'finance');
    }
}

if(!function_exists('get_SI_temp_link')){
    /**
     * 获取SI临时链接
     **/
    function get_SI_temp_link($id = 0, $is_json = true){
         $CI =& get_instance();
        
        $CI->load->model('m_model');
        $url = substr(md5("op$id"), 8, 16);
        $sql = "update biz_shipment set SI_outer_link ='$url' where id =$id";
        $CI->m_model->query($sql);
        $url = "http://f.bianmachaxun.com/temp/i/$url";
        $data = array('url' => $url);
        //这里如果存在就只修改结束时间
        $shipment = Model('biz_shipment_model')->get_by_id($id);
        if(!empty($shipment)){
            $closing_date = '0000-00-00 00:00:00';
            if($shipment['consol_id'] != 0){
                $consol = Model('biz_consol_model')->get_by_id($shipment['consol_id']);
                if(!empty($consol)) $closing_date = $consol['closing_date'];
            }
            //查询一下是否有任务，有的话更新结束日期
            $sql = "select id from biz_client_contact_list_task where task_type='SI截单' and  id_no = '{$id}' and id_type = 'shipment'";
            $task = $CI->m_model->query_one($sql);
            $time = time();
            $date = date('Y-m-d H:i:s');
            $userId = get_session('id');
            if(!empty($task)){
                $sql = "update biz_client_contact_list_task set close_time = '{$closing_date}' where id = '{$task['id']}'";
            }else{
                // 状态0 已结束， 1未结束
                $sql = "INSERT INTO `op_china`.`biz_client_contact_list_task`(`client_code`, `email`, `task_status`, `task_type`, `task_name`, `id_type`, `id_no`, `url`, `start_time`, `close_time`, `created_by`, `created_time`) VALUES ('{$shipment['client_code']}', '{$shipment['client_email']}', 1, 'SI截单', 'SI截单', 'shipment', '{$shipment['id']}', '{$url}', '{$date}', '{$closing_date}', '{$userId}', '{$date}');";
                
            }
            $CI->m_model->query($sql);
        }
        //pctz_jh
        $data['pctz_jh'] = '';
        $data['pctz_jx'] = '';
        //获取做箱通知模板
        Model('bsc_template_model');
        $template = $CI->bsc_template_model->get_one('id', 25);
        if(!empty($template)){
            if(empty($template['parameter'])){
                $data['pctz_jh'] = '/export/export_word?file_name=' . $template['file_template'] . '&data_table=' . $template['data_table'] . '&id=' . $id;
            }else{
                $data['pctz_jh'] = '/bsc_template/set_parameter?url=/export/export_word&file_name=' . $template['file_template'] . '&data_table=' . $template['data_table'] . '&id=' . $id . '&parameter=' . $template['parameter'];
            }
        }
        //pctz_jx
        $template = $CI->bsc_template_model->get_one('id', 30);
        if(!empty($template)){
            if(empty($template['parameter'])){
                $data['pctz_jx'] = '/export/export_word?file_name=' . $template['file_template'] . '&data_table=' . $template['data_table'] . '&id=' . $id;
            }else{
                $data['pctz_jx'] = '/bsc_template/set_parameter?url=/export/export_word&file_name=' . $template['file_template'] . '&data_table=' . $template['data_table'] . '&id=' . $id . '&parameter=' . $template['parameter'];
            }
        }
        if($is_json) echo json_encode(array('code' => 0, 'msg' => '生成成功', 'data' => $data));
        return $url;
        $CI =& get_instance();
        
        //判断consol的截单日期是否超过且是否存在已通过的SI
        Model('biz_shipment_model');
        Model('biz_consol_model');
        Model('biz_shipment_si_model');
        $CI->db->select('consol_id');
        $shipment = $CI->biz_shipment_model->get_by_id($id);
        if(empty($shipment)){//shipment是否存在
            if($is_json) echo json_encode(array('code' => 1, 'msg' => 'shipment 不存在'));
            return false;
        }
        if($shipment['consol_id'] == 0){//consol是否绑定
            if($is_json) echo json_encode(array('code' => 1, 'msg' => '不存在consol, 请检查'));
            return false;
        }
        //提单确认勾选?

        $CI->db->select('closing_date');
        $consol = $CI->biz_consol_model->get_by_id($shipment['consol_id']);
        if(empty($consol)){//consol是否存在
            if($is_json) echo json_encode(array('code' => 1, 'msg' => 'consol 不存在'));
            return false;
        }
        //如果当前时间超过截单日期
        if(strtotime($consol['closing_date']) <= time()){//是否超出截单日期   
            if($is_json) echo json_encode(array('code' => 1, 'msg' => '已超出截单日期或未设置截单日期,生成失败'));
            return false;
        }

        //是否已通过
        $CI->db->select('id');
        $is_confirm = $CI->biz_shipment_si_model->get_where_one('shipment_id = ' . $id . ' and status = 1');
        if(!empty($is_confirm)){//是否已经确认过了
            if($is_json) echo json_encode(array('code' => 1, 'msg' => '客户已确认,生成失败'));
            return false;
        }

        //SI是否存在status为0的
        $CI->db->select('id');
        $this_si = $CI->biz_shipment_si_model->get_where_one('shipment_id = ' . $id . ' and status = 0');
        $url = "http://client.leagueshipping.com/temp";
        $now = time();
        //当前时间改为截单时间
        // $end_time = $now + 365 * 24 * 3600;
        $end_time = strtotime($consol['closing_date']);
        $param = array('id' => $id, 'type' => 'SI', 'start' => $now);
        $timestamp_key = temp_link_key_encode($end_time, $param);
        $url .= '?timestamp_key=' . urlencode($timestamp_key);
        if(!empty($this_si)){
            //直接跳转到SI修改页面?
            if($is_json) echo json_encode(array('code' => 2, 'msg' => '已存在待审核SI,请驳回后再试'));
            return false;
        }
        // $url = str_replace('&', '&amp;', $url);
        $data = array('url' => $url);
        if($is_json) echo json_encode(array('code' => 0, 'msg' => '生成成功', 'data' => $data));
        return $url;
    }
}

if(!function_exists('cz_shipment_merge_shipment_check')){
    function cz_shipment_merge_shipment_check($shipmentA_id = 0,$shipmentB_id = 0){
        $CI =& get_instance();

        if ($shipmentA_id == 0 || $shipmentB_id == '') return '参数错误';

        $CI->db->select('id,consol_id,parent_id');
        $shipmentA = $CI->biz_shipment_model->get_one_all('id', $shipmentA_id);
        $CI->db->select('id,consol_id');
        $shipmentB = $CI->biz_shipment_model->get_one_all('id', $shipmentB_id);
        if (empty($shipmentA)) return 'shipmentA不存在';
        if (empty($shipmentB)) return 'shipmentB不存在';
        if ($shipmentA['id'] == $shipmentB['id']) return 'shipmentA不能等于shipmentB';

        //1、必须是同一个CONSOL
        if ($shipmentA['consol_id'] != $shipmentB['consol_id']) return '必须是同一个consol';
        Model('biz_bill_model');
        $CI->db->select('id');
        $bill = $CI->biz_bill_model->get_where_one("id_type = 'shipment' and id_no = '$shipmentA_id'");
        //2、A 不能有费用， B 可以有费用。
        if (!empty($bill)) return 'shipmentA不能有费用';

        //3、A本身不能是子shipment
        if ($shipmentA['parent_id'] != 0) return 'shipmentA不能是子shipment';

        return 0;
    }
}

if(!function_exists('cz_shipment_merge_shipment')) {
    function cz_shipment_merge_shipment($shipmentA_id = 0,$shipmentB_id = '')
    {
        $CI =& get_instance();

       // $CI->db->force_master();
        $shipmentA = $CI->biz_shipment_model->get_one_all('id', $shipmentA_id);
       // $CI->db->force_master();
        $shipmentB = $CI->biz_shipment_model->get_one_all('id', $shipmentB_id);
        $merge_check = cz_shipment_merge_shipment_check($shipmentA_id, $shipmentB_id);
        if($merge_check){
            return $merge_check;
        }

        //合并后：
        //1、B shipment复制生成1个一模一样的子shipment B，包含B的主字段和container这个页的内容，billing标签的不用复制，job_no变成B-1 。 如果B-1存在，则不重复生成。
       // $CI->db->force_master();
        $shipmentB_copy = $CI->biz_shipment_model->get_one_all('job_no', $shipmentB['job_no'] . '-1');
        if (empty($shipmentB_copy)) {
            //不存在生成一条
            $add_shipmentB = $shipmentB;
            $add_shipmentB['job_no'] = $add_shipmentB['job_no'] . '-1';
            $add_shipmentB['parent_id'] = $add_shipmentB['id'];
            $add_shipmentB['consol_id'] = 0;
            unset($add_shipmentB['id']);

            $shipmentB_copy_id = $CI->biz_shipment_model->save($add_shipmentB);

            if (!$shipmentB_copy_id) return 'shipmentB复制失败';

            //继承duty
            $CI->load->model('biz_duty_model');
           // $CI->db->force_master();
            $this_duty = $CI->biz_duty_model->get_where_one("biz_table = 'biz_shipment' and id_no = '{$shipmentB['id']}'");
            if (!empty($this_duty)) {
                unset($this_duty['d_id']);
                $this_duty['id_no'] = $shipmentB_copy_id;
                $duty_id = $CI->biz_duty_model->save($this_duty);

                $log_data = array();
                $log_data["table_name"] = "biz_duty_new";
                $log_data["key"] = $duty_id;
                $log_data["master_table_name"] = 'biz_shipment';
                $log_data["master_key"] = $shipmentB_copy_id;
                $log_data["action"] = "insert";
                $log_data["value"] = json_encode($this_duty);
                log_rcd($log_data);
            }
            //继承危险品数据
            if ($add_shipmentB['dangergoods_id'] != 0) {
                $CI->load->model('biz_shipment_dangergoods_model');
               // $CI->db->force_master();
                $this_dangergoods = $CI->biz_shipment_dangergoods_model->get_one('id', $shipmentB['dangergoods_id']);
                if (!empty($this_dangergoods)) {
                    unset($this_dangergoods['id']);
                    $this_dangergoods['shipment_id'] = $shipmentB_copy_id;
                    $danger_goods_id = $CI->biz_shipment_dangergoods_model->save($this_dangergoods);
                    $CI->biz_shipment_model->update($shipmentB_copy_id, array('danger_goods_id', $danger_goods_id));

                    // record the log
                    $log_data = array();
                    $log_data["table_name"] = "biz_shipment_dangergoods";
                    $log_data["key"] = $danger_goods_id;
                    $log_data["master_table_name"] = 'biz_shipment';
                    $log_data["master_key"] = $shipmentB_copy_id;
                    $log_data["action"] = "insert";
                    $log_data["value"] = json_encode($this_dangergoods);
                    log_rcd($log_data);
                }
            }

            //继承箱数据
            Model('biz_shipment_container_model');
           // $CI->db->force_master();
            $shipmentB_containers = $CI->biz_shipment_container_model->get("shipment_id = {$shipmentB['id']}");
            if (!empty($shipmentB_containers)) {
                foreach ($shipmentB_containers as $shipmentB_container) {
                    //不存在给数据加新值
                    $shipmentB_copy_container = array();
                    $shipmentB_copy_container['shipment_id'] = $shipmentB_copy_id;
                    $shipmentB_copy_container['container_no'] = $shipmentB_container['container_no'];
                    $shipmentB_copy_container['packs'] = $shipmentB_container['packs'];
                    $shipmentB_copy_container['weight'] = $shipmentB_container['weight'];
                    $shipmentB_copy_container['volume'] = $shipmentB_container['volume'];

                    $new_container_id = $CI->biz_shipment_container_model->save($shipmentB_copy_container);

                    // record the log
                    $log_data = array();
                    $log_data["table_name"] = "biz_shipment_container";
                    $log_data["key"] = $new_container_id;
                    $log_data["master_table_name"] = 'biz_shipment';
                    $log_data["master_key"] = $shipmentB_copy_id;
                    $log_data["action"] = "insert";
                    $log_data["value"] = json_encode($shipmentB_copy_container);
                    log_rcd($log_data);
                }
            }

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment";
            $log_data["key"] = $shipmentB_copy_id;
            $log_data["action"] = "insert";
            $log_data["value"] = json_encode($add_shipmentB);
            log_rcd($log_data);
        } else {
            $shipmentB_copy_id = 0;
        }
       // $CI->db->force_master();
        $shipmentA_childs = $CI->biz_shipment_model->no_role_get("parent_id = {$shipmentA['id']}");
        $shipmentA_update = array();
        $shipmentA_update['consol_id'] = 0;
        //箱型箱量需要进行合并
        if (empty($shipmentA_childs)) {
            //2、A本身无子shipment， A shipment变成B的子shipment， 状态还是normal， 父shipment是B.（parent_id加上，其他好像啥都不用变）
            $shipmentA_update['parent_id'] = $shipmentB['id'];
        } else {
            //3、A 本身有子shipment，那A的子shipment的parent_id全部修改为B， A本身的状态改为cancel并踢出consol
            $shipmentA_child_update_datas = array();
            foreach ($shipmentA_childs as $shipmentA_child) {
                $shipmentA_child_update_data = array();
                $shipmentA_child_update_data['id'] = $shipmentA_child['id'];
                $shipmentA_child_update_data['parent_id'] = $shipmentB['id'];
                $shipmentA_child_update_datas[] = $shipmentA_child_update_data;
            }

            foreach ($shipmentA_child_update_datas as $shipmentA_child_update_data) {
                $this_id = $shipmentA_child_update_data['id'];
                unset($shipmentA_child_update_data['id']);
                $CI->biz_shipment_model->update($this_id, $shipmentA_child_update_data);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_shipment";
                $log_data["key"] = $this_id;
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($shipmentA_child_update_data);
                log_rcd($log_data);
            }

            $shipmentA_update['status'] = 'cancel';
        }
        $CI->biz_shipment_model->update($shipmentA['id'], $shipmentA_update);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment";
        $log_data["key"] = $shipmentA['id'];
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($shipmentA_update);
        log_rcd($log_data);

        //4、最后： B主shipment本身根据自己的所有子shipment，计算一遍container数据和件毛体数据
        Model('biz_shipment_combine_model');
        $shipment_combine_data = array();
        $shipment_combine_data['shipmentA_id'] = $shipmentA['id'];
        $shipment_combine_data['shipmentB_id'] = $shipmentB['id'];
        $shipment_combine_data['shipmentB_copy_id'] = $shipmentB_copy_id;
        $shipment_combine_data['shipmentA_child_ids'] = join(',', array_column($shipmentA_childs, 'id'));
        $shipment_combine_data['consol_id'] = $shipmentA['consol_id'];
        $shipment_combine_id = $CI->biz_shipment_combine_model->save($shipment_combine_data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment_combine";
        $log_data["key"] = $shipment_combine_id;
        $log_data["master_table_name"] = 'biz_shipment';
        $log_data["master_key"] = $shipmentA['id'];
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($shipment_combine_data);
        log_rcd($log_data);

        //将shipmentB的箱型箱量加上shipmentA的
        $shipmentB_update = array();
        $shipmentB_update['box_info'] = json_encode(merge_box_info(json_decode($shipmentA['box_info'], true), json_decode($shipmentB['box_info'], true)));
        $CI->biz_shipment_model->update($shipmentB['id'], $shipmentB_update);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment";
        $log_data["key"] = $shipmentB['id'];
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($shipmentB_update);
        log_rcd($log_data);

        $CI->load->helper('cz_container');
        cz_child_container_sum($shipmentB['id']);

        return 0;
    }
}
