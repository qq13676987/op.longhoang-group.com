<?php

class biz_containershipment extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_shipment_container_model');
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
        $this->load->helper('cz_container');
    }

    public $field_edit = array();

    public $field_all = array(
        array("id", "ID", "80", "1"),
        array("container_no", "container_no", "100", "2"),
        array("packs", "packs", "120", "4"),
        array("weight", "weight", "120", "5"),
        array("vgm", "vgm", "120", "6"),
        array("volume", "volume", "120", "6"),
        array("en_description", "en_description", "120", "6"),//2023-04-17 
        array("created_by", "created_by", "150", "10"),
        array("created_time", "created_time", "150", "10"),
        array("updated_by", "updated_by", "150", "10"),
        array("updated_time", "updated_time", "150", "11")
    );

    public function index($shipment_id = "", $consol_id = "")
    {
        $data = array();
        $data['operator_si_check'] = isset($_GET['operator_si_check'])?$_GET['operator_si_check']:1;
        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_shipment_container_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }
        $shipment = Model('biz_shipment_model')->get_duty_one("id = '{$shipment_id}'");
        
        $data["lock_lv"] = $shipment['lock_lv'];
        $data["shipment_id"] = $shipment_id;
        $data["consol_id"] = $consol_id;
        if($shipment['parent_id'] != 0){
            $parent_shipment = Model('biz_shipment_model')->get_duty_one("id = '{$shipment['parent_id']}'");
            $data["consol_id"] = $parent_shipment['consol_id'];
        }
        $this->load->view('head');
        $this->load->view('biz/containershipment/index_view', $data);
    }

    public function get_data($shipment_id = "", $consol_id = '')
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------这个查询条件想修改成通用的 -------------------------------
        $f1 = 'shipment_id';
        $s1 = '=';
        $v1 = "$shipment_id";
        $f2 = 'container_no';
        $s2 = 'like';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $where = "";
        if ($v1 != "") $where = "$f1 $s1 '$v1'";
        if ($v2 != "" && $where != "") $where .= " and " . search_like($f2, $s2, $v2);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->biz_shipment_container_model->total($where);
        $this->db->limit($rows, $offset);
        $this->db->join('biz_container bc', "bc.container_no = biz_shipment_container.container_no AND bc.consol_id = '$consol_id'", 'LEFT');
        $this->db->select('biz_shipment_container.*,bc.container_size,bc.seal_no,bc.container_tare');
    
       
        $rs = $this->biz_shipment_container_model->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data($shipment_id = "", $consol_id = "")
    {
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? trim($_POST[$item]) : '';
            if($temp == '' && !in_array($item, array('vgm'))){
                $data['code'] = 1;
                $data['msg'] = $item . lang('不能为空');
                $data['isError'] = true;
                echo json_encode($data);
                return;
            }
            $data[$item] = $temp;
        }
        $data['shipment_id'] = $shipment_id;
        $this->load->model('biz_shipment_model');
        $this->load->model('biz_container_model');
        $container_size = trim($_POST['container_size']);
        $seal_no = trim($_POST['seal_no']);
        $limit = cz_shipment_container_limit($container_size, $shipment_id);
        if($limit === true){
            $id = $this->biz_shipment_container_model->save($data);
            $data['id'] = $id;
            
            $data['container_size'] = $container_size;
            $data['seal_no'] = $seal_no;
            cz_container_add($data, $shipment_id, $consol_id);
    
            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment_container";
            $log_data["key"] = $id;
            $log_data["action"] = "insert";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
            
            $data['code'] = 0;
            $data['msg'] = '保存成功';
            echo json_encode($data);
        }else{
            $data['code'] = 1;
            $data['msg'] = $limit;
            $data['isError'] = true;
            echo json_encode($data);
        }
    }

    public function update_data()
    {
        $id = $_REQUEST["id"];
        $old_row = $this->biz_shipment_container_model->get_by_id($id);

        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? trim($_POST[$item]) : '';
            if(!empty($temp)){
                $data[$item] = $temp;
            }
        }
        $this->load->model('biz_shipment_model');
        $this->load->model('biz_container_model');
        $container_size = trim($_POST['container_size']);
        $seal_no = trim($_POST['seal_no']);
        
        $limit = cz_shipment_container_limit($container_size, $old_row['shipment_id'],$id);
        if($limit === true){
            $id = $this->biz_shipment_container_model->update($id, $data);
            $data['id'] = $id;
            
            $data['container_size'] = $container_size;
            $data['seal_no'] = $seal_no;
            cz_container_add($data, $old_row['shipment_id']);
            
    
            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment_container";
            $log_data["key"] = $id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
            
            $data['code'] = 0;
            $data['msg'] = '保存成功';
            echo json_encode($data);
        }else{
            $data['code'] = 1;
            $data['msg'] = $limit;
            $data['isError'] = true;
            echo json_encode($data);
        }
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->biz_shipment_container_model->get_one('id', $id);
        $this->biz_shipment_container_model->mdelete($id);
        $this->load->model('biz_shipment_model');
        $this->load->model('biz_container_model');
        cz_container_handle($old_row, $old_row['shipment_id']);
        echo json_encode(array('success' => true, 'msg' => '删除成功', 'code' => 0));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment_container";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
    }
}