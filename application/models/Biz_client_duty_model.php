<?php
class Biz_client_duty_model extends CI_Model{
    private $table = 'biz_client_duty';

    //参数1是可以设置的相关角色, 参数2 为 空值和all all的话保存时,默认添加一条全开放权限, 参数3 必填的角色
    public $client_role_config = array(
        'client' => array(array('sales', 'operator', 'customer_service'), '', array('sales', 'operator')),
        'oversea_client' => array(array('sales', 'operator', 'customer_service', 'oversea_cus'), '', array('sales', 'operator')),
        'logistics_client' => array(array('sales', 'operator', 'customer_service'), '', array('sales', 'operator')),
        'oversea_agent' => array(array('sales', 'operator', 'customer_service', 'oversea_cus'), 'all' , array('sales', 'operator')),
        'factory' => array(array('sales', 'operator', 'customer_service'), '', array('sales', 'operator')),
            'warehouse' => array(array(), 'all', array()),
            'feeder' => array(array(), 'all', array()),
        // 'warehouse' => array(array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'sales', 'finance'), 'all', array()),
            'truck' => array(array(), 'all', array()),
        // 'truck' => array(array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'sales', 'finance'), 'all', array()),
            'declaration' => array(array(), 'all', array()),
        // 'declaration' => array(array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'sales', 'finance'), 'all', array()),
            'carrier' => array(array(), 'all', array()),
        // 'carrier' => array(array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'sales', 'finance'), 'all', array()),
            'booking_agent' => array(array(), 'all', array()),
        // 'booking_agent' => array(array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'sales', 'finance'), 'all', array()),
        // 'wanglaikuan' => array(array('operator', 'finance', 'marketing'), 'all', array('operator', 'finance', 'marketing')),
        'wanglaikuan' => array(array('operator', 'finance', 'marketing'), 'all', array('operator', 'finance', 'marketing')),
        'other' => array(array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'sales', 'finance'), 'all', array()), //这个默认不开,但是小田自己可以设置开放
        'container_supplier' => array(array(), '', array()),//这个是mars用的箱管,暂时不设置权限,后面提出再仔细询问
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_one($f="",$v=""){
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_where_one($where = ""){
        if($where != '')$this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get($where="" ,$sort="id",$order="desc"){
        if($where !="")	$this->db->where($where, NULL, false);
        $this->db->order_by($sort,$order, false);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function total($where=""){
        if($where !="")	$this->db->where($where, NULL, false);
        $this->db->from($this->table);
        return  $this->db->count_all_results('');
    }

    public function no_role_get($where="" ,$sort="id",$order="desc"){
        if($where !="")	$this->db->where($where, null, false);
        $this->db->order_by($sort,$order, true);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function no_role_total($where=""){
        if($where !="")	$this->db->where($where, null, false);
        $this->db->from($this->table);
        return  $this->db->count_all_results('');
    }

    public function save($data=array()){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    //获取单条client相关的权限人员信息
    public function get_client_data_one($client_code){
        $this->db->where('client_code', $client_code);
        $query = $this->db->get($this->table);
        $rows = $query->result_array();
        //获取到数据后,开始根据用户角色进行,链接填充

        $user_role = array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'sales', 'finance');

        //这里得加个原本read_user_group里对应的all,
        $data = array('is_all' => false, 'read_user_group' => array());
        foreach ($user_role as $urval){
            //填充ids
            $data[$urval . '_ids'] = array();
            foreach ($rows as $key => $row){
                if(!in_array($row['user_group'], $data['read_user_group'])) $data['read_user_group'][] = $row['user_group'];
                if($row['user_id'] == -1){
                    $data['is_all'] = true;
                    if(!in_array('all', $data['read_user_group'])) $data['read_user_group'][] = 'all';
                }
                if($row['user_role'] == $urval) {
                    $data[$urval . '_ids'][] = $row['user_id'];
                    unset($rows[$key]);
                }
            }
            $data[$urval . '_ids'] = join(',', filter_unique_array($data[$urval . '_ids']));
        }
        $data['read_user_group'] = join(',', $data['read_user_group']);
        return $data;
    }

    //获取client相关的权限人员信息,
    public function get_client_data($client_codes){
        $this->db->where("client_code in ('" . join('\',\'', $client_codes) . "')");
        $query = $this->db->get($this->table);
        $rows = $query->result_array();
        //获取到数据后,开始根据用户角色进行,链接填充

        $user_role = array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'sales', 'finance');

        //这里得加个原本read_user_group里对应的all,
        $data = array();
        $default_row = array('is_all' => false, 'read_user_group' => '');
//        foreach ($user_role as $urval){
//            //填充ids
//            $default_row[$urval . '_ids'] = array();
//        }
        foreach ($rows as $key => $row) {

            if (!isset($data[$row['client_code']])) $data[$row['client_code']] = $default_row;
            $read_user_group = explode(',', $data[$row['client_code']]['read_user_group']);

            if (!in_array($row['user_group'], $read_user_group)) $read_user_group[] = $row['user_group'];
            if ($row['user_id'] == -1) {
                $data[$row['client_code']]['is_all'] = true;
                if (!in_array('all', $read_user_group)) $read_user_group[] = 'all';
            }
            foreach ($user_role as $urval) {
                if ($row['user_role'] == $urval) {
                    //不存在创建个,存在转为数组使用
                    if (!isset($data[$row['client_code']][$urval . '_ids'])) $role_ids_arr = array();
                    else $role_ids_arr = explode(',', $data[$row['client_code']][$urval . '_ids']);
                    $role_ids_arr[] = $row['user_id'];
                    unset($rows[$key]);

                    //结束时转回字符串
                    $data[$row['client_code']][$urval . '_ids'] = join(',', filter_unique_array($role_ids_arr));
                }
            }
            $data[$row['client_code']]['read_user_group'] = join(',', filter_unique_array($read_user_group));
        }
        return $data;
    }

    //将一个client_code的人员配置转到另外个client_code上,目前用于申请客户, 通过时,移交过去
    public function client_code_update($client_code1, $client_code2){
        $data = array(
            'client_code' => $client_code2
        );

        $this->db->where('client_code', $client_code1);
        $this->db->update($this->table, $data);

        //去除当前客户下,重复的人员数据
        $delete_sql = "delete from biz_client_duty where client_code = '{$client_code2}' AND id not in (select mid from (select min(id) as mid from biz_client_duty bcd where bcd.client_code = '{$client_code2}' GROUP BY bcd.client_code,bcd.client_role, bcd.user_role,bcd.user_id) a)";
        $this->db->query($delete_sql);
    }

    public function mdelete($id=''){
        // delete container
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
}