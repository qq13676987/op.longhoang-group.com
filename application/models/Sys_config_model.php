<?php
   class sys_config_model extends CI_Model{
       private $table = 'sys_config';

       public function __construct(){
            $this->load->database();
            if ($this->session->userdata('id')==''){   
                redirect('main/login', 'refresh');
            } 
            $this->user_id =  $this->session->userdata('id');
        }
        public $user_id = ''; 
		 
        public function get_one($config_name=''){
            $this->db->where('user_id', $this->user_id);
            $this->db->where('config_name', $config_name);
            $query = $this->db->get($this->table);
            return $query->row_array();
        }

        public function get_config($config_name = ''){
            $this->db->where('user_id', $this->user_id);
            $this->db->where('config_name', $config_name);
            $query = $this->db->get($this->table);
            $row = $query->row_array();
            if(empty($row)) $row['config_text'] = '';
            return $row['config_text'];
        }
        
        public function get_one_zero($config_name=''){
           $this->db->where('user_id', 0);
           $this->db->where('config_name', $config_name);
           $query = $this->db->get($this->table);
           return $query->row_array();
        }
        
        public function get($where=""){
            if($where != '') $this->db->where($where);
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

       public function total($where=""){
           if($where !="")	$this->db->where($where);
           $this->db->from($this->table);
           return  $this->db->count_all_results('');
       }

        public function update($config_name='', $config_text=''){
            $data = array(
               'config_text' => $config_text 
            );
            $this->db->where('user_id', $this->user_id);
            $this->db->where('config_name', $config_name);
            $this->db->update($this->table, $data);
        }
        
        public function update_zero($config_name='', $config_text=''){
           $data = array(
               'config_text' => $config_text
           );
           $this->db->where('user_id', 0);
           $this->db->where('config_name', $config_name);
           $this->db->update($this->table, $data);
       }

        public function mdelete($config_name=''){ 
            $this->db->where('user_id', $this->user_id);
            $this->db->where('config_name', $config_name);
            $this->db->delete($this->table);
        }

        public function insert($config_name='', $config_text=''){
            $data = array(
                'user_id' => $this->user_id,
                'config_name' => $config_name,
                'config_text' => $config_text 
            );
            $this->db->insert($this->table, $data);
            return $this->db->insert_id();

        }                
        
        public function insert_zero($config_name='', $config_text=''){
           $data = array(
               'user_id' => 0,
               'config_name' => $config_name,
               'config_text' => $config_text
           );
           $this->db->insert($this->table, $data);
            return $this->db->insert_id();
        }
        
        public function save($config_name='', $config_text='', $is_log = false){
            //判断是否有记录， 
            $rs = $this->get_one($config_name); 
            if (!empty($rs['config_name'])){     //有记录， 更新       
                 $this->update($config_name, $config_text);
                 $action = 'update';
            }  else {            //无记录，则新增
                $rs['id'] = $this->insert($config_name, $config_text);
                 $action = 'insert';
            }           
            //是否记录日志
            if($is_log){
                // record the log
                $log_data = array();
                $log_data["table_name"] = "sys_config";
                $log_data["key"] = $rs['id'];
                $log_data["action"] = $action;
                $log_data["value"] = json_encode(array('config_text' => $config_text));
                log_rcd($log_data);
            }
        }

       public function save_zero($config_name='', $config_text='', $is_log = false){
           //判断是否有记录，
           $rs = $this->get_one_zero($config_name);
           if (!empty($rs['config_name'])){     //有记录， 更新
               $this->update_zero($config_name, $config_text);
               $action = 'update';
           }  else {            //无记录，则新增
               $rs['id'] = $this->insert_zero($config_name, $config_text);
               $action = 'insert';
           }
           //是否记录日志
           if($is_log){
               // record the log
               $log_data = array();
               $log_data["table_name"] = "sys_config";
               $log_data["key"] = $rs['id'];
               $log_data["action"] = $action;
               $log_data["value"] = json_encode(array('config_text' => $config_text));
               log_rcd($log_data);
           }
       }

   }
?>
