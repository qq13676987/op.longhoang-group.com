<!doctype html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/inc/css/normal.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/icon.css?v=3">
<script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.easyui.min.js?v=11"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.edatagrid.js?v=5"></script>
<script type="text/javascript" src="/inc/js/easyui/datagrid-detailview.js"></script>
<script type="text/javascript" src="/inc/js/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/inc/js/other.js?v=1"></script>
<script type="text/javascript" src="/inc/js/set_search.js?v=5"></script>
<style type="text/css">
    .panel-body{
        overflow:hidden;
    }
</style>
<script type="text/javascript">

    $(function () {
        $('#tabs2').tabs({
            border:false,
            onSelect:function(title){
                console.log(title);
                if(title=='非活跃客户'){
                    if (document.getElementById("iframe_1").src==""){
                        document.getElementById("iframe_1").src="/biz_crm/customer_inactive/";
                    }
                }
                if(title=='我的指派'){
                    if (document.getElementById("iframe_2").src==""){
                        document.getElementById("iframe_2").src="/biz_crm/my_appoint/";
                    }
                }
            }
        });

    });

</script>

<div id="tabs2" class="easyui-tabs" style="width:100%;height:780px;">
    <div title="非活跃客户" style="padding:5px">
        <iframe scrolling="auto" frameborder="0" id="iframe_1" src="/biz_crm/customer_inactive/" style="width:100%;height: 100%;padding-top: 10px"></iframe>
    </div>
    <div title="我的指派" style="padding:5px">
        <iframe scrolling="auto" frameborder="0" id="iframe_2" style="width:100%;height: 100%;"></iframe>
    </div>
</div>





