<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title><?= lang('welcome'); ?>！</title>

    <link rel="stylesheet" href="/inc/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/default/easyui.css?v=1">
    <link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/icon.css?v=3">
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="/inc/js/easyui/jquery.easyui.min.js?v=27"></script>

    <script type="text/javascript" src="/inc/js/JPlaceholder.js"></script>

    <script type="text/javascript" src="/inc/js/jquery.cookie.js"></script>
    <style>
        .login_main .textbox{
            height: 38px !important;
            border: 1px solid #ccc;
            border-radius: 0;
        }
        .login_main .textbox a{
            height: 38px !important;
        }
    </style>
</head>

<body>


<div id="form1" name="form1" >

    <!--div class="top"><a href="javascript:void(0);" target="_blank"><img src="/inc/image/logo.png" alt="" /></a></div-->

    <div class="banner">

        <div class="banner1">

            <div id=imgPlay>

                <!--ul class="imgs" id="actor"-->

                <ul class="imgs">

                    <li><img title="" src="/inc/image/login_01.png"></LI>

                    <!--li><a href="#" target=_blank><img title="" src="/inc/image/login_01.png"></a></LI-->

                </ul>

                <div class="num" id="numInner"></div>

                <div class="login">

                    <h2 class="login_title"><?= lang('Global System'); ?></h2>

                    <div class="login_main">
                        <p>
                            <input id="username" name="username" placeholder="<?= lang('用户名或邮箱');?>">
                        </p>

                        <p><input type="password" name="password" value="" id="password" placeholder="<?= lang('密码');?>"/></p>

                        <div>
                            <input type="checkbox" name="remember" id="remember"/><?= lang('记住密码'); ?>
                        </div>

                        <p><a id="btn_login" href="#" class="login_btn"><?= lang('登录'); ?></a></p>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="foot">

        <p><?= lang('Long Hoang'); ?></p>

    </div>

    <script type="text/javascript" src="/inc/js/lanrentuku.js"></script>

</div>

<script type="text/javascript">

    var cookie_id = "15dBAC785-7N5B-4131-85EA-16ARTBEACD98";



    function EnEight(str) {
        var monyer = new Array();
        var i;
        for (i = 0; i < str.length; i++)
            monyer += "\\" + str.charCodeAt(i).toString(8);
        return monyer;
    }

    function DeEight(str) {
        var monyer = new Array();
        var i;
        var s = str.split("\\");
        for (i = 1; i < s.length; i++)
            monyer += String.fromCharCode(parseInt(s[i], 8));
        return monyer;
    }


    function SetUsrAndPwd() {
        if ($("#remember").is(":checked")) {
            var usr = $("#username").val();
            var pwd = $("#password").val();
            $.cookie(cookie_id, usr);
            $.cookie(cookie_id + usr, EnEight(pwd));
        } else {
            $.cookie(cookie_id, '', {expires: -1});
            $.cookie(cookie_id + usr, '', {expires: -1});
        }
    }

    function GetPwdAndChk() {

        var usr = $('#username').val();

        var pwd = $.cookie(cookie_id + usr);

        if (pwd != null) {

            $('#remember').prop("checked", true);

            $('#password').val(DeEight(pwd));

        }

    }

    $(document).ready(function () {
        $("#btn_login").on("click", function () {
            var usr = $("input[name='username']").val();
            var pwd = $('#password').val();

            if (usr == null || usr == "") {
                alert("请输入用户名");
                $('#username').focus();
                return;
            }

            if (pwd == null || pwd == "") {

                alert("请输入密码");

                $('#password').focus();

                return;

            }
            
            ajaxLoading();

            $.ajax({
                url: '/main/check_login_ajax',
                type: 'POST',
                data: {
                    username: usr,
                    password: pwd,
                },
                dataType: 'json',
                success: function (res) {
                    alert(res.msg);
                    ajaxLoadEnd();
                    if (res.code == 0) {
                        location.href = '<?= $back_url;?>';
                    }
                },
                error: function (e) {
                    ajaxLoadEnd();
                    alert(e.responseText);
                }
            });

            // $("#form1")[0].submit();

            SetUsrAndPwd();

        });

        $('#form1').on('keydown', 'input',function (e) {
            var theEvent = window.event || e;
            var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
            if (code == 13) {
                $('#btn_login').trigger('click');
            }
        });
    });

</script>
</body>

</html> 