<?php

class Biz_bill extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_bill_model');
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }

    }

    public $field_edit = array();

    public $field_all = array(
        array("id", "ID", "55", "1"),
        array("type", "type", "40", "2"),
        array("id_type", "id_type", "102", "2"),
        array("id_no", "id_no", "0", "4"),
        array("charge", "charge", "85", "5"),
        array("charge_code", "charge_code", "60", "5"),
        array("client_code", "client", "150", "6"),
        array("currency", "cost", "55", "5"),
        array("unit_price", "unit_price", "70", "6"),
        array("number", "number", "50", "6"),
        array("amount", "amount", "111", "6"),
        array("confirm_date", "confirm_date", "99", "6", ),
        array("remark", "remark", "99", "6"),
        array("vat", "vat", "55", "6"),
        array("vat_withhold", "vat_withhold", "55", "6"),
        array("account_no", "account_no", "80", "6"),
        array("commision_flag", "commision_flag", "0", "6"),
        array("created_by", "created_by", "60", "10"),
        array("created_time", "created_time", "150", "10"),
        array("updated_by", "updated_by", "60", "10"),
        array("updated_time", "updated_time", "150", "11")
    );

    public function index($id_type = "", $id_no = "")
    {
        $type = getValue('type', '');
        $id_type = getValue('id_type', $id_type);
        $id_no = getValue('id_no', $id_no);
        $data = array();
        $data["type"] = $type;

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_bill_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }

        $data["id_type"] = $id_type;
        $data["id_no"] = $id_no;
        $data["creditor_default"] = '';
        $data["debitor_default"] = "";
        $data["ex_rate_date"] = "0000-00-00";
        $lock_lv = 0;
        $this->load->model('biz_consol_model');
        $containers = array();
        $container_sizes = array();
        $trans_destination = '';
        $box_info = array();
        $data['lock_lv'] = 0;
        $data['consol_id'] = '';
        if ($id_type == "shipment") {
            $this->load->model('biz_shipment_model');
            $this->db->select("biz_shipment.*, (select email_verify from biz_client_contact_list where biz_client_contact_list.client_code = biz_shipment.client_code and biz_client_contact_list.name = biz_shipment.client_contact and biz_client_contact_list.telephone = biz_shipment.client_email limit 1) as client_email_verify,(select id from biz_client_contact_list where biz_client_contact_list.client_code = biz_shipment.client_code limit 1) as is_contact");
            $row = $this->biz_shipment_model->get_by_id($id_no);
            if($row['parent_id'] != 0){
                echo lang("子shipment不允许做账");
                return;
            }
            if($row['consol_id'] == 0){
                echo lang("请绑定consol后再做账");
                return;
            }
            //2023-12-12 加入了 如果联系人没选择, 或者 邮件未验证时, 不能输入账单
			if(empty($row['is_contact']) && $row['from_id_no']<111){
                echo lang('请添加当前客户的联系人');
                return;
            }
            // if($row['client_email_verify'] !== '1'){
            //     echo lang('当前客户的联系人邮箱未验证');
            //     return;
            // }
            
            $data['shipment'] = $row;
            $data["debitor_default"] = $row["client_code"];
            $data["creditor_default"] = '';
            $data['consol_id'] = $row['consol_id'];
            if($row['consol_id'] != 0){
                $consol_row = $this->biz_consol_model->get_by_id($row['consol_id']);
                if($consol_row['creditor'] != '客户自订舱')$data["creditor_default"] = $consol_row['creditor'];
                //2022-04-21 原:ATD未输入时,不能做账,  新: ATD和ETD同时未填写时,不能做账
                if($consol_row['trans_ATD'] == '0000-00-00 00:00:00' && $consol_row['trans_ETD'] == '0000-00-00 00:00:00'){
                    // exit("请先输入实际离泊时间 或 预计离泊时间后，再来做费用。如果是预收费用，建议填写ETD，待开船后再修改（上海港已对接API的会自动修改）");
                }
                $data['ex_rate_date'] = $consol_row['ex_rate_date'];
            }else{
                //2022-04-02 如果没有consol不允许做账
                // exit("未绑定consol,无法做账");
            }
            $lock_lv = $row['lock_lv'];
            $trans_destination = $row['trans_destination_name'];
            //获取箱信息
            $biz_shipment_container_model = Model('biz_shipment_container_model');
            $containers = $biz_shipment_container_model->join_container_get_by_id($row['id'], $row['consol_id']);

            $container_sizes = array_count_values(array_filter(array_column($containers, 'container_size')));
           
            $box_info = json_decode($row['box_info'], true);
            $data['lock_lv'] = $row['lock_lv'];
        }
        if ($id_type == "consol") {
            $row = $this->biz_consol_model->get_by_id($id_no);
            // if ($row['dingcangwancheng'] == 0 && $row["trans_mode"]=='FCL' && $row["biz_type"]=='export' && $row['lock_lv']<3){
            //     exit('勾选“订舱接受”才能输入Billing!');
            // }

            $data["creditor_default"] = $row["creditor"];
            if($row["creditor"] == '客户自订舱') $data["creditor_default"] = '';
            $data["debitor_default"] = '';
            $lock_lv = $row['lock_lv'];
            $trans_destination = $row['trans_destination_name'];
            
            //获取箱信息
            $biz_container_model = Model('biz_container_model');
            $containers = $biz_container_model->get("consol_id = '{$row['id']}'");
            $container_sizes = array_count_values(array_filter(array_column($containers, 'container_size')));
            
            $box_info = json_decode($row['box_info'], true);
            $data['lock_lv'] = $row['lock_lv'];
            $data['consol_id'] = $row['id'];
            $data['ex_rate_date'] = $row['ex_rate_date'];
            
            Model('biz_shipment_model');
            $this->db->select('id, lock_lv');
            $shipments = $this->biz_shipment_model->get_shipment_by_consol($id_no);
            $shipments_lock_lv = array_column($shipments, 'lock_lv');
            $data['shipment_lock_lv'] = 0;
            if(in_array('0', $shipments_lock_lv)){
                $data['shipment_lock_lv'] = 0;
            }else if(in_array('1', $shipments_lock_lv)){
                $data['shipment_lock_lv'] = 1;
            }else if(in_array('2', $shipments_lock_lv)){
                $data['shipment_lock_lv'] = 2;
            }else if(in_array('3', $shipments_lock_lv)){
                $data['shipment_lock_lv'] = 3;
            }
        }
        //kml相关, 这里感觉是有错误的。 目前的KML的主单并无billing
        if ($id_type == "kml") {
            $row = Model('biz_kml_model')->get_where_one("id = '$id_no'");   
        }
        $data['trans_destination'] = $trans_destination;
        $data['containers'] = $containers;
        $data['container_sizes'] = $container_sizes;
        $data["lock_lv"] = $lock_lv;
        $data["box_info"] = $box_info;

        if($data["ex_rate_date"] == '0000-00-00') $data["ex_rate_date"] = '';

        $this->load->model('bsc_user_role_model');
        $userRole = $this->bsc_user_role_model->get_user_role(array('delete_text'));

        $userRoleDelete = explode(",", (isset($userRole['delete_text']) ? $userRole['delete_text'] : ''));
        $data["hasDelete"] = in_array("bill", $userRoleDelete);

        $is_special = isset($_GET['is_special']) ? $_GET['is_special'] : 0;
        $data['is_special'] = $is_special;
        $this->load->view('head');

        if($id_type == 'shipment'){
			$is_tab = getValue('is_tab',0);
			if(!empty($data['shipment']['from_db']) && !empty($data['shipment']['from_id_no']) && $is_tab == 0){
				if($data['shipment']['biz_type'] == 'export'){
					$data['pol_site'] = strtoupper(get_system_type());
					$data['pod_site'] = strtoupper($data['shipment']['from_db']);
					$data['pol_id'] = $data['shipment']['id'];
					$data['pod_id'] = $data['shipment']['from_id_no'];
				}
				if($data['shipment']['biz_type'] == 'import'){
					$data['pol_site'] = strtoupper($data['shipment']['from_db']);
					$data['pod_site'] = strtoupper(get_system_type());
					$data['pol_id'] = $data['shipment']['from_id_no'];
					$data['pod_id'] = $data['shipment']['id'];
				}
				$this->load->view('biz/bill/index_shipment_tab', $data);
			}else{
				$this->load->view('biz/bill/index_shipment_view', $data);
			}
        }else{
            $this->load->view('biz/bill/index_consol_view', $data);
        }
    }
	public function index1($id_type = "", $id_no = "")
	{
		$this->db = $this->load->database(strtolower(getValue('site',get_system_type())),true);
		$data = array();
		$data['site'] = getValue('site',strtoupper(get_system_type()));
		$data["id_type"] = $id_type;
		$data["id_no"] = $id_no;
		$this->load->model('biz_consol_model');
		$this->load->model('biz_shipment_model');
		$row = $this->biz_shipment_model->get_by_id($id_no);
		if($row['parent_id'] != 0){
			echo lang('该票不允许做账');
			return;
		}
		$data['shipment'] = $row;
		$data['consol_id'] = $row['consol_id'];
		$is_special = isset($_GET['is_special']) ? $_GET['is_special'] : 0;
		$data['is_special'] = $is_special;
		$this->load->view('head');
		$this->load->view('biz/bill/index_shipment1', $data);
	}

    public function get_data($id_type = "", $id_no = "")
    {

        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "currency";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $is_special = isset($_GET['is_special']) ? $_GET['is_special'] : 0;
        $where = '';

        //-------这个查询条件想修改成通用的 -------------------------------
        $where .= "id_type = '$id_type' and id_no = '$id_no'";
        //-----------------------------------------------------------------

//        $offset = ($page - 1) * $rows;
//        $result["total"] = $this->biz_bill_model->total($where);
//        $this->db->limit($rows, $offset);
		$type = strtoupper(get_system_type());
        $this->db->select("*,(select company_name_en from biz_client where client_code = biz_bill.client_code) as client_code_name,(select name from bsc_user where id = biz_bill.created_by) as created_by,(select biz_charge.charge_name from biz_charge where biz_charge.charge_code = biz_bill.charge_code) as charge_name_en,(select count(id) from bsc_overseas_account where id = biz_bill.bank_account_id and url like '%$type%') as is_local,(select title_cn from bsc_overseas_account where id = biz_bill.bank_account_id) as bank_account");
        $cost_rs = $this->biz_bill_model->get($where . " and type = 'cost'", $sort, $order, $is_special);
        $this->db->select("*,(select company_name_en from biz_client where client_code = biz_bill.client_code) as client_code_name,(select name from bsc_user where id = biz_bill.created_by) as created_by,(select biz_charge.charge_name from biz_charge where biz_charge.charge_code = biz_bill.charge_code) as charge_name_en,(select count(id) from bsc_overseas_account where id = biz_bill.bank_account_id and url like '%$type%') as is_local,(select title_cn from bsc_overseas_account where id = biz_bill.bank_account_id) as bank_account");
        $sell_rs = $this->biz_bill_model->get($where . " and type = 'sell'", $sort, $order, $is_special);
        //获取权限
        $this->load->model('bsc_user_role_model');
        $userRole = $this->bsc_user_role_model->get_user_role(array('read_text'));
        $userRoleRead = explode(',', $userRole['read_text']);//bill.cost,bill.sell
        $must_field = array('charge', 'remark', 'charge_code', 'rate_id', 'created_by', 'created_time', 'updated_by', 'updated_time');

        //获取权限
        $rows = array();
        $ts_field = array('rate_id', 'id_type', 'id_no', 'charge_code', 'charge', 'charge_name_en');
        $all_row = array('sell_id_type' => $id_type, 'cost_id_type' => $id_type, 'sell_id_no' => $id_no, 'cost_id_no' => $id_no, 'sell_created_by' => null, 'cost_created_by' => null, 'created_time' => null, 'created_group' => null, 'updated_by' => null, 'updated_time' => null, 'id_type' => null, 'id_no' => null, 'charge_code' => null, 'charge' => null, 'charge_name_en' => null, 'cost_id' => null, 'cost' => null, 'cost_unit_price' => null, 'cost_number' => null, 'cost_amount' => null, 'cost_vat' => null, 'cost_vat_withhold' => null, 'cost_check_no' => null, 'cost_confirm_date' => '0000-00-00', 'cost_invoice_id' => 0,'sell_invoice_id' => 0, 'cost_invoice_no' => null, 'cost_close_date' => null, 'creditor' => null, 'sell_id' => null, 'sell' => null, 'sell_unit_price' => null, 'sell_number' => null, 'sell_amount' => null, 'sell_vat' => null, 'sell_vat_withhold' => null, 'sell_check_no' => null, 'sell_confirm_date' => '0000-00-00', 'sell_invoice_no' => null, 'sell_close_date' => null, 'debitor' => null, 'sell_remark' => null, 'cost_remark' => null, 'rate_id' => null,);
        if(sizeof($cost_rs) >= sizeof($sell_rs)){
            $first_rs = $cost_rs;
            $first_type = 'cost';

            $two_rs = $sell_rs;
            $two_type = 'sell';
        }else{
            $first_rs = $sell_rs;
            $first_type = 'sell';

            $two_rs = $cost_rs;
            $two_type = 'cost';
        }

        //如果是shipment,将对应consol的账单查出,且添加标记表示,无法修改
        if($id_type == 'shipment'){
            $c_where = "id_type = 'shipment' and id_no = '" . $id_no . "'";
            $child = false;
            $this->db->select('*,(select company_name_en from biz_client where client_code = biz_bill.client_code) as client_code_name,(select name from bsc_user where id = biz_bill.created_by) as created_by,(select invoice_id from biz_bill bb where bb.id = biz_bill.parent_id) as invoice_id, (select payment_id from biz_bill bb where bb.id = biz_bill.parent_id) as payment_id,(select biz_charge.charge_name from biz_charge where biz_charge.charge_code = biz_bill.charge_code) as charge_name_en');
            $consol_cost_rs = $this->biz_bill_model->get($c_where . " and type = 'cost'", $sort, $order, $is_special, $child);
            $this->db->select('*,(select company_name_en from biz_client where client_code = biz_bill.client_code) as client_code_name,(select name from bsc_user where id = biz_bill.created_by) as created_by,(select invoice_id from biz_bill bb where bb.id = biz_bill.parent_id) as invoice_id, (select payment_id from biz_bill bb where bb.id = biz_bill.parent_id) as payment_id,(select biz_charge.charge_name from biz_charge where biz_charge.charge_code = biz_bill.charge_code) as charge_name_en');
            $consol_sell_rs = $this->biz_bill_model->get($c_where . " and type = 'sell'", $sort, $order, $is_special, $child);
            $ban_handle = array('ban_handle' => true);
            array_walk($consol_cost_rs, function (&$arr, $this_key, $ban_handle) {
                $arr = array_merge($arr, $ban_handle);
            }, $ban_handle);
            array_walk($consol_sell_rs, function (&$arr, $this_key, $ban_handle) {
                $arr = array_merge($arr, $ban_handle);
            }, $ban_handle);
            $cost_rs = array_merge($cost_rs, $consol_cost_rs);
            $sell_rs = array_merge($sell_rs, $consol_sell_rs);
            if(sizeof($consol_cost_rs) >= sizeof($consol_sell_rs)){
                $first_rs = $cost_rs;
                $first_type = 'cost';

                $two_rs = $sell_rs;
                $two_type = 'sell';
            }else{
                $first_rs = $sell_rs;
                $first_type = 'sell';

                $two_rs = $cost_rs;
                $two_type = 'cost';
            }
        }

        foreach ($first_rs as $crow){
            $row = $all_row;
            $crow = $this->data_role($crow, $must_field, $userRoleRead);
            foreach ($crow as $crkey => $crval){
                if($crkey == 'currency'){
                    $row[$first_type] = $crval;
                    continue;
                }
                if($crkey == 'client_code'){
                    if($first_type == 'cost'){
                        $row['creditor'] = $crval;
                        $row['creditor_name'] = $crow[$crkey . '_name'];
                    }else{
                        $row['debitor'] = $crval;
                        $row['debitor_name'] = $crow[$crkey . '_name'];
                    }
                    continue;
                }
                if(!in_array($crkey, $ts_field)){
                    $row[$first_type . '_' . $crkey] = $crval;
                }else{
                    $row[$crkey] = $crval;
                }
            }
            $rows[] = $row;
        }

        foreach ($two_rs as $srow){
            //根据charge_code 和币种来进行匹配
            $srow = $this->data_role($srow, $must_field, $userRoleRead);
            $rows_key = false;
            foreach ($rows as $this_key => $this_row){
                //优先 匹配 费用名称+币种
                if($this_row['charge_code'] == $srow['charge_code'] && $this_row[$first_type] == $srow['currency'] && $this_row[$two_type] == null){
                    $rows_key = $this_key;
                    break;
                }
                //2023-11-30 mars 由于USD没和VND 对上 提出  未匹配到币种, 就匹配费用名称
                if($this_row['charge_code'] == $srow['charge_code'] && $this_row[$two_type] == null){
                    $rows_key = $this_key;
                    break;
                }
            }
            if($rows_key !== false){//存在则添加到该后面
                $row = $rows[$rows_key];
            }else{//,不存在新增一个row
                $row = $all_row;
            }
            foreach ($srow as $srkey => $srval){
                if($srkey == 'currency'){
                    $row[$two_type] = $srval;
                    continue;
                }
                if($srkey == 'client_code'){
                    if($two_type == 'sell'){
                        $row['debitor'] = $srval;
                        $row['debitor_name'] = $srow[$srkey . '_name'];
                    }else{
                        $row['creditor'] = $srval;
                        $row['creditor_name'] = $srow[$srkey . '_name'];
                    }
                    continue;
                }
                if(!in_array($srkey, $ts_field)){
                    $row[$two_type . '_' . $srkey] = $srval;
                }else{
                    if($row[$srkey] == null){
                        $row[$srkey] = $srval;
                    }
                }
            }
            if($rows_key !== false){//存在则添加到该后面
                $rows[$rows_key] = $row;
            }else{//,不存在新增一个row
                $rows[] = $row;
            }
        }

        //获取 invoice_no 等数据
        $this->load->model('biz_bill_invoice_model');
        $this->load->model('biz_bill_payment_model');
        $bill_ids = filter_unique_array(array_merge(array_column($rows, 'sell_id'), array_column($rows, 'cost_id')));

        foreach($rows as $key => $val){
            $sell_invoice_id = isset($val["sell_invoice_id"])?$val["sell_invoice_id"]:0;
            $cost_invoice_id = isset($val["cost_invoice_id"])?$val["cost_invoice_id"]:0;

            $sell_payment_id = isset($val["sell_payment_id"])?$val["sell_payment_id"]:0;
            $cost_payment_id = isset($val["cost_payment_id"])?$val["cost_payment_id"]:0;
            $rows[$key]["sell_invoice_flag"] = "";
            $rows[$key]["sell_actual_flag"] = "";
            $rows[$key]["cost_invoice_flag"] = "";
            $rows[$key]["cost_actual_flag"] = "";

            if($sell_invoice_id>0){
                $this->db->select('id,invoice_no,invoice_date, invoice_type,(select GROUP_CONCAT(invoice_no) from biz_bill_invoice bbi where bbi.parent_id = biz_bill_invoice.id) as children_invoice_no');
                $temp = $this->biz_bill_invoice_model->get_one("id",$sell_invoice_id);
                $rows[$key]["sell_invoice_no"] = $temp["invoice_no"];
                if($temp['children_invoice_no'] != null) $rows[$key]["sell_invoice_no"] = $temp["children_invoice_no"];
                $rows[$key]["sell_invoice_date"] = $temp["invoice_date"];
                if($temp["invoice_date"] !="0000-00-00" && $temp["invoice_date"] != null)  $rows[$key]["sell_invoice_flag"] = lang("开票");
                else $rows[$key]["sell_invoice_flag"] = lang("已提交开票");

                if($temp['invoice_type'] == '0'){
                    $rows[$key]["sell_invoice_flag"] = lang("纸票");
                }else if($temp['invoice_type'] == '1'){
                    $rows[$key]["sell_invoice_flag"] = lang("电票");
                }else if($temp['invoice_type'] == '999'){
                    $rows[$key]["sell_invoice_flag"] = lang("形票");
                }else if($temp['invoice_type'] == '1001'){
                    $rows[$key]["sell_invoice_flag"] = lang("无票");
                }else if($temp['invoice_type'] == '2'){
                    $rows[$key]["sell_invoice_flag"] = lang("专票");
                }
            }
            if($sell_payment_id > 0){
                $temp = $this->biz_bill_payment_model->get_one("id",$sell_payment_id);
                if(!empty($temp))  $rows[$key]["sell_actual_flag"] = "<span title=\"{$temp['payment_date']}\">" . lang("核销") . "</span>";
            }
            if($cost_invoice_id>0){
                $this->db->select('id,invoice_no,invoice_date, (select GROUP_CONCAT(invoice_no) from biz_bill_invoice bbi where bbi.parent_id = biz_bill_invoice.id) as children_invoice_no');
                $temp = $this->biz_bill_invoice_model->get_one("id",$cost_invoice_id);
                $rows[$key]["cost_invoice_no"] = $temp["invoice_no"];
                if($temp['children_invoice_no'] != null) $rows[$key]["cost_invoice_no"] = $temp["children_invoice_no"];
                if(!empty($temp))  $rows[$key]["cost_invoice_flag"] = lang("开票");
            }
            if($cost_payment_id > 0){
                $temp = $this->biz_bill_payment_model->get_one("id",$cost_payment_id);
                if(!empty($temp) && $temp["payment_date"]!="0000-00-00" && $temp["payment_date"]!= null)  $rows[$key]["cost_actual_flag"] = "<span title=\"{$temp['payment_date']}\">" . lang("核销") . "</span>";
            }
        }

        //排序--start
        usort($rows, function ($a, $b){
            //分为两种情况排序,
            //1 是币种, 2是
            // 首先根据币种排序
            $a_sell_id = isset($a['sell_id']) ? $a['sell_id'] : 999999999;
            $b_sell_id = isset($b['sell_id']) ? $b['sell_id'] : 999999999;
            $a_currency = isset($a['sell']) ? $a['sell'] : $a['cost'];
            $b_currency = isset($b['sell']) ? $b['sell'] : $b['cost'];
            $a_charge_code = $a['charge_code'];
            $b_charge_code = $b['charge_code'];
            //如果是这三个代码, 优先沉底
            if(in_array($a_charge_code, array('DCFWF', 'ZHFWF', 'MDGFWF'))){
                return 1;
            }else if(in_array($b_charge_code, array('DCFWF', 'ZHFWF', 'MDGFWF'))){
                return -1;
            }
            //如果币种 排序等级一样, 这时候 再费用名称正常排序
            $currencyCompare = strcmp($a_currency, $b_currency);
            if ($currencyCompare !== 0) {
                return -$currencyCompare;
            }else{
                // return strcmp($a_charge_code, $b_charge_code);
                return $a_sell_id-$b_sell_id;
            }
        });

        $new_rows = array();
        $i = 0;
        foreach ($rows as $key => $row){
            //判断如果
            $row['id'] = $i;
            $i++;
            if($row['sell_invoice_id'] != 0){
                $row['sell_invoice_data'] = $this->db->where('id',$row['sell_invoice_id'])->get('biz_bill_invoice')->row_array();
            }
            $new_rows[] = $row;
        }
        $rows = $new_rows;
        //排序--end

        // 如果是type = si的场景，限制只显示 VGM 和舱单费
        $type = getValue('type', '');
        if($type=="SI"){
            $keep_field = array("VGM","CDF");
            for($key = sizeof($rows)-1; $key>=0;$key--){
                if(!in_array($rows[$key]["charge_code"],$keep_field)) array_splice($rows, $key, 1);
            }
        }
        // 场景显示END

        $current_currency = Model('sys_config_model')->get_one_zero("CURRENT_CURRENCY")['config_text'];
        $result["rows"] = $rows;
        $result["total"] = sizeof($rows);
        $result["profit"] = array();//汇率数组
        $profit = array(
            'sell_title' => lang("{currency} total sell", array('currency' => $current_currency)),
            'cost_title' => lang("{currency} total cost", array('currency' => $current_currency)),
            'profit_title' => lang("{currency} total profit", array('currency' => $current_currency)),
            'sell_amount' => 0,
            'cost_amount' => 0,
            'profit_amount' => 0,
        );
        //usd--start
        //2023-01-28这里最后对金额进行统计
		//这里的版本改为 USD和AED
		$current_currency = get_system_config('CURRENT_CURRENCY');

		$bill_ETD = get_bill_ETD($id_type, $id_no);
		//利润的相关币种数据
		$profit_currency = array(
			'total' => array(
				'sell_title' => lang("total sell", array('currency' => $current_currency)),
				'cost_title' => lang("total cost", array('currency' => $current_currency)),
				'profit_title' => lang("total profit", array('currency' => $current_currency)),
				'profit_amount' => 0,
				'currency' => $current_currency,
				'currency_symbol' => '',//符号后续可以在币种表里配置
				'sell_amount' => 0.00,
				'sell_local_amount' => 0.00,
				'cost_amount' => 0.00,
				'cost_local_amount' => 0.00,
				'sum' => 0.00,
				'rate_sum' => 0.00
			),
		);
		foreach ($rows as $row){
			//second只获取当前,first将除second以外的币种都折算汇总
			//sell和cost由于在一个数组,所以这里统计2次
			foreach (array('cost', 'sell') as $val){
				$this_currency = $row[$val];
				if(empty($this_currency)) continue;
				if(!isset($profit_currency[$this_currency])){
					$profit_currency[$this_currency] = array(
						'sell_title' => lang("{currency} sell", array('currency' => $this_currency)),
						'cost_title' => lang("{currency} cost", array('currency' => $this_currency)),
						'profit_title' => lang("{currency} profit", array('currency' => $this_currency)),
						'profit_amount' => 0,
						'currency' => $this_currency,
						'currency_symbol' => '',//符号后续可以在币种表里配置
						'sell_amount' => 0.00,
						'cost_amount' => 0.00,
						'sum' => 0.00,
						'rate_sum' => 0.00
					);
				}
				//下面代表不用转换的,所以直接取amount
				$this_amount = $row["{$val}_amount"];
//                if($val == 'cost') $this_amount = -$this_amount;
				$profit_currency[$this_currency]['sum'] += $this_amount;
				$profit_currency[$this_currency]['rate_sum'] += $this_amount;

				$profit_currency[$this_currency]["{$val}_amount"] += $this_amount;

				$profit_currency[$this_currency]['profit_amount'] = round($profit_currency[$this_currency]['sell_amount'] - $profit_currency[$this_currency]['cost_amount'], 2);

				//统计total 其他币种都折算成 本地币种, KML这里是USD
//                $profit_currency['total']["{$val}_amount"] += amount_currency_change($row["{$val}_amount"], $bill_ETD, $row[$val], $val, $current_currency);
				//由于实时折算可能会产生一些误会,所以这里使用local进行统计
				$profit_currency['total']["{$val}_amount"] += $row["{$val}_local_amount"];

				$profit_currency['total']['profit_amount'] = round($profit_currency['total']['sell_amount'] - $profit_currency['total']['cost_amount'], 2);
			}
		}
		//把total移动到最后面
		$total_profit = $profit_currency['total'];
		unset($profit_currency['total']);
		$profit_currency['total'] = $total_profit;
		//2024-03-15 新加入如果当前关联了form_db和from_id_no, 那么获取那面的账单数据, 统计profit
		if($id_type == 'shipment'){
			$this->db->select("id,from_db,from_id_no");
			$shipment = Model('biz_shipment_model')->get_by_id($id_no);

			//如果有值, 那么链接对应数据库
			if($shipment['from_db'] !== ''){
				//存在时, 先在中间加一个 VN profit:

				$from_db = $this->load->database($shipment['from_db'], true);
				$from_bill_rows = $from_db->query("select type,bill_ETD,currency,local_amount from biz_bill where id_type = 'shipment' and id_no = '{$shipment['from_id_no']}'")->result_array();

				if(!empty($from_bill_rows)){
					$profit_currency["{$shipment['from_db']}"] = array(
						'text' => lang("海外{from_db}利润转入", array('from_db' => strtoupper($shipment['from_db']))),
						'is_text' => true,
					);
				}

				foreach ($from_bill_rows as $row){
					$this_profit_key = "{$shipment['from_db']} {$row['currency']}";
					$this_currency = $row['currency'];
					if(empty($this_currency)) continue;
					if(!isset($profit_currency[$this_profit_key])){
						$profit_currency[$this_profit_key] = array(
							'sell_title' => lang("{currency} sell", array('currency' => $this_currency)),
							'cost_title' => lang("{currency} cost", array('currency' => $this_currency)),
							'profit_title' => lang("{currency} profit", array('currency' => $this_currency)),
							'profit_amount' => 0,
							'currency' => $this_currency,
							'currency_symbol' => '',//符号后续可以在币种表里配置
							'sell_amount' => 0.00,
							'cost_amount' => 0.00,
							'sum' => 0.00,
							'rate_sum' => 0.00
						);
					}
					//下面代表不用转换的,所以直接取amount
					$this_amount = $row["local_amount"];
					$val = $row['type'];
					$profit_currency[$this_profit_key]['sum'] += $this_amount;
					$profit_currency[$this_profit_key]['rate_sum'] += $this_amount;

					$profit_currency[$this_profit_key]["{$val}_amount"] += $this_amount;

					$profit_currency[$this_profit_key]['profit_amount'] = round($profit_currency[$this_profit_key]['sell_amount'] - $profit_currency[$this_profit_key]['cost_amount'], 2);
				}
			}
		}

		foreach($profit_currency as $k => $row){
			foreach($row as $key => $val) {
				if (is_numeric($val)) {
					$profit_currency[$k][$key] = number_format($val, 2, '.', '');
				}
			}
		}
		$result['profit'] = $profit_currency;
		echo json_encode($result);
	}

	public function get_data_tab($id_type = "", $id_no = "")
	{
		$current_currency = get_system_config('CURRENT_CURRENCY');
		$this->db = $this->load->database(strtolower(getValue('site2',get_system_type())),true);
		$result = array();
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : "currency";
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$is_special = isset($_GET['is_special']) ? $_GET['is_special'] : 0;
		$where = '';

		//-------这个查询条件想修改成通用的 -------------------------------
		$where .= "id_type = '$id_type' and id_no = '$id_no'";
		//-----------------------------------------------------------------
		$this->db->select('*,(select company_name from biz_client where client_code = biz_bill.client_code) as client_code_name,(select name from bsc_user where id = biz_bill.created_by) as created_by_name,(select group_name from bsc_group where bsc_group.group_code = biz_bill.client_inner_department) as client_inner_department_name');
		$cost_rs = $this->biz_bill_model->get($where . " and type = 'cost'", $sort, $order, $is_special);
		$this->db->select('*,(select company_name from biz_client where client_code = biz_bill.client_code) as client_code_name,(select name from bsc_user where id = biz_bill.created_by) as created_by_name,(select group_name from bsc_group where bsc_group.group_code = biz_bill.client_inner_department) as client_inner_department_name,(select finance_belong_company_code from biz_client where biz_client.client_code = biz_bill.client_code) as finance_belong_company_code');
		$sell_rs = $this->biz_bill_model->get($where . " and type = 'sell'", $sort, $order, $is_special);

		//获取权限
		$rows = array();
		$ts_field = array('rate_id', 'id_type', 'id_no', 'charge_code', 'charge');
		$all_row = array('sell_id_type' => $id_type, 'cost_id_type' => $id_type, 'sell_id_no' => $id_no, 'cost_id_no' => $id_no, 'sell_created_by' => null, 'cost_created_by' => null, 'sell_created_by_name' => null, 'cost_created_by_name' => null, 'sell_client_inner_department' => null, 'cost_client_inner_department' => null, 'sell_client_inner_department_name' => null, 'cost_client_inner_department_name' => null, 'created_time' => null, 'created_group' => null, 'updated_by' => null, 'updated_time' => null, 'id_type' => null, 'id_no' => null, 'charge_code' => null, 'charge' => null, 'cost_id' => null, 'cost' => null, 'cost_unit_price' => null, 'cost_number' => null, 'cost_amount' => null, 'cost_vat' => null, 'cost_check_no' => null, 'cost_confirm_date' => '0000-00-00', 'cost_invoice_id' => 0,'sell_invoice_id' => 0, 'cost_invoice_no' => null, 'cost_close_date' => null, 'creditor' => null, 'sell_id' => null, 'sell' => null, 'sell_unit_price' => null, 'sell_number' => null, 'sell_amount' => null, 'sell_vat' => null, 'sell_vat_withhold' => null, 'sell_check_no' => null, 'sell_confirm_date' => '0000-00-00', 'sell_invoice_no' => null, 'sell_close_date' => null, 'debitor' => null, 'sell_remark' => null, 'cost_remark' => null, 'rate_id' => null,'sell_finance_belong_company_code'=>null,'sell_account_no'=>null,'cost_account_no'=>null);
		if(sizeof($cost_rs) >= sizeof($sell_rs)){
			$first_rs = $cost_rs;
			$first_type = 'cost';

			$two_rs = $sell_rs;
			$two_type = 'sell';
		}else{
			$first_rs = $sell_rs;
			$first_type = 'sell';

			$two_rs = $cost_rs;
			$two_type = 'cost';
		}

		//如果是shipment,将对应consol的账单查出,且添加标记表示,无法修改
		if($id_type == 'shipment'){
			$c_where = "id_type = 'shipment' and id_no = '" . $id_no . "'";
			$child = false;
			$this->db->select('*,(select company_name from biz_client where client_code = biz_bill.client_code) as client_code_name,(select name from bsc_user where id = biz_bill.created_by) as created_by_name,(select group_name from bsc_group where bsc_group.group_code = biz_bill.client_inner_department) as client_inner_department_name,(select invoice_id from biz_bill bb where bb.id = biz_bill.parent_id) as invoice_id, (select payment_id from biz_bill bb where bb.id = biz_bill.parent_id) as payment_id');
			$consol_cost_rs = $this->biz_bill_model->get($c_where . " and type = 'cost'", $sort, $order, $is_special, $child);
			$this->db->select('*,(select company_name from biz_client where client_code = biz_bill.client_code) as client_code_name,(select name from bsc_user where id = biz_bill.created_by) as created_by_name,(select group_name from bsc_group where bsc_group.group_code = biz_bill.client_inner_department) as client_inner_department_name,(select invoice_id from biz_bill bb where bb.id = biz_bill.parent_id) as invoice_id, (select payment_id from biz_bill bb where bb.id = biz_bill.parent_id) as payment_id');
			$consol_sell_rs = $this->biz_bill_model->get($c_where . " and type = 'sell'", $sort, $order, $is_special, $child);
			$ban_handle = array('ban_handle' => true);
			array_walk($consol_cost_rs, function (&$arr, $this_key, $ban_handle) {
				$arr = array_merge($arr, $ban_handle);
			}, $ban_handle);
			array_walk($consol_sell_rs, function (&$arr, $this_key, $ban_handle) {
				$arr = array_merge($arr, $ban_handle);
			}, $ban_handle);
			$cost_rs = array_merge($cost_rs, $consol_cost_rs);
			$sell_rs = array_merge($sell_rs, $consol_sell_rs);
			if(sizeof($consol_cost_rs) >= sizeof($consol_sell_rs)){
				$first_rs = $cost_rs;
				$first_type = 'cost';

				$two_rs = $sell_rs;
				$two_type = 'sell';
			}else{
				$first_rs = $sell_rs;
				$first_type = 'sell';

				$two_rs = $cost_rs;
				$two_type = 'cost';
			}
		}

		foreach ($first_rs as $crow){
			$row = $all_row;
			foreach ($crow as $crkey => $crval){
				if($crkey == 'currency'){
					$row[$first_type] = $crval;
					continue;
				}
				if($crkey == 'client_code'){
					if($first_type == 'cost'){
						$row['creditor'] = $crval;
						$row['creditor_name'] = $crow[$crkey . '_name'];
					}else{
						$row['debitor'] = $crval;
						$row['debitor_name'] = $crow[$crkey . '_name'];
					}
					continue;
				}
				if(!in_array($crkey, $ts_field)){
					$row[$first_type . '_' . $crkey] = $crval;
				}else{
					$row[$crkey] = $crval;
				}
			}
			$rows[] = $row;
		}

		foreach ($two_rs as $srow){
			//根据charge_code 和币种来进行匹配
			$rows_key = false;
			foreach ($rows as $this_key => $this_row){
				if($this_row['charge_code'] == $srow['charge_code'] && $this_row[$first_type] == $srow['currency'] && $this_row[$two_type] == null){
					$rows_key = $this_key;
					break;
				}
				//2023-11-30 mars 由于USD没和VND 对上 提出  未匹配到币种, 就匹配费用名称
				if($this_row['charge_code'] == $srow['charge_code'] && $this_row[$two_type] == null){
					$rows_key = $this_key;
					break;
				}
			}
			if($rows_key !== false){//存在则添加到该后面
				$row = $rows[$rows_key];
			}else{//,不存在新增一个row
				$row = $all_row;
			}
			foreach ($srow as $srkey => $srval){
				if($srkey == 'currency'){
					$row[$two_type] = $srval;
					continue;
				}
				if($srkey == 'client_code'){
					if($two_type == 'sell'){
						$row['debitor'] = $srval;
						$row['debitor_name'] = $srow[$srkey . '_name'];
					}else{
						$row['creditor'] = $srval;
						$row['creditor_name'] = $srow[$srkey . '_name'];
					}
					continue;
				}
				if(!in_array($srkey, $ts_field)){
					$row[$two_type . '_' . $srkey] = $srval;
				}else{
					if($row[$srkey] == null){
						$row[$srkey] = $srval;
					}
				}
			}
			if($rows_key !== false){//存在则添加到该后面
				$rows[$rows_key] = $row;
			}else{//,不存在新增一个row
				$rows[] = $row;
			}
		}

		//获取 invoice_no 等数据
		$this->load->model('biz_bill_invoice_model');
		$this->load->model('biz_bill_payment_model');

		$bill_invoice_table_name = get_table_name('biz_bill_invoice');

		foreach($rows as $key => $val){
			$sell_invoice_id = isset($val["sell_invoice_id"])?$val["sell_invoice_id"]:0;
			$cost_invoice_id = isset($val["cost_invoice_id"])?$val["cost_invoice_id"]:0;

			$sell_payment_id = isset($val["sell_payment_id"])?$val["sell_payment_id"]:0;
			$cost_payment_id = isset($val["cost_payment_id"])?$val["cost_payment_id"]:0;
			$rows[$key]["sell_invoice_flag"] = "";
			$rows[$key]["sell_actual_flag"] = "";
			$rows[$key]["cost_invoice_flag"] = "";
			$rows[$key]["cost_actual_flag"] = "";

			$rows[$key]["cost_chongping"] = '';
			$rows[$key]["sell_chongping"] = '';
			$rows[$key]["sell_created_by"] = $val['sell_created_by_name'];
			$rows[$key]["cost_created_by"] = $val['cost_created_by_name'];
			if($sell_invoice_id>0){
				$this->db->select("id,invoice_no,invoice_date, invoice_type,use_type,(select GROUP_CONCAT(invoice_no) from {$bill_invoice_table_name} biz_bill_invoice where biz_bill_invoice.parent_id = biz_bill_invoice.id) as children_invoice_no");
				$temp = $this->biz_bill_invoice_model->get_one("id",$sell_invoice_id);
				if(!empty($temp)){
					$rows[$key]["sell_invoice_no"] = $temp["invoice_no"];
					if($temp['children_invoice_no'] != null) $rows[$key]["sell_invoice_no"] = $temp["children_invoice_no"];
					$rows[$key]["sell_invoice_date"] = $temp["invoice_date"];
					if($temp["invoice_date"] !="0000-00-00" && $temp["invoice_date"] != null)  $rows[$key]["sell_invoice_flag"] = lang("开票");
					else $rows[$key]["sell_invoice_flag"] = lang("已提交开票");

					if($temp['invoice_type'] == '0'){
						$rows[$key]["sell_invoice_flag"] = lang("纸票");
					}else if($temp['invoice_type'] == '1'){
						$rows[$key]["sell_invoice_flag"] = lang("电票");
					}else if($temp['invoice_type'] == '999'){
						$rows[$key]["sell_invoice_flag"] = lang("形票");
					}else if($temp['invoice_type'] == '1001'){
						if($temp['use_type'] == 'DN') $rows[$key]["sell_invoice_flag"] = "DN";
						else $rows[$key]["sell_invoice_flag"] = lang("发票S");
					}else if($temp['invoice_type'] == '2'){
						$rows[$key]["sell_invoice_flag"] = lang("专票");
					}
				}

			}
			if($sell_payment_id > 0){
				$temp = $this->biz_bill_payment_model->get_one("id",$sell_payment_id);
				if(!empty($temp))  $rows[$key]["sell_actual_flag"] = "<span title=\"{$temp['payment_date']}\">".lang("核销")."</span>";
			}
			if($cost_invoice_id>0){
				$this->db->select("id,invoice_no,invoice_date, (select GROUP_CONCAT(invoice_no) from {$bill_invoice_table_name} biz_bill_invoice where biz_bill_invoice.parent_id = biz_bill_invoice.id) as children_invoice_no");
				$temp = $this->biz_bill_invoice_model->get_one("id",$cost_invoice_id);
				if(!empty($temp)){
					$rows[$key]["cost_invoice_no"] = $temp["invoice_no"];
					$rows[$key]["cost_invoice_date"] = $temp["invoice_date"];
					if($temp['children_invoice_no'] != null) $rows[$key]["cost_invoice_no"] = $temp["children_invoice_no"];
				}
				if(!empty($temp))  $rows[$key]["cost_invoice_flag"] = lang("开票");
			}
			if($cost_payment_id > 0){
				$temp = $this->biz_bill_payment_model->get_one("id",$cost_payment_id);
				if(!empty($temp) && $temp["payment_date"]!="0000-00-00" && $temp["payment_date"]!= null)  $rows[$key]["cost_actual_flag"] = "<span title=\"{$temp['payment_date']}\">".lang("核销")."</span>";
			}
			$rows[$key]['apply_status'] = '';
		}
		$result["rows"] = $rows;
		$result["total"] = sizeof($rows);
		//2024-03-15 新加入如果当前关联了form_db和from_id_no, 那么获取那面的账单数据, 统计profit
		$from_bill_rows = $this->db->query("select type,bill_ETD,currency,local_amount,amount from biz_bill where id_type = 'shipment' and id_no = '{$id_no}'")->result_array();
		$site = getValue('site2',strtoupper(get_system_type()));
		$from_current_currency = $this->db->where('config_name','CURRENT_CURRENCY')->get('sys_config')->row_array()['config_text'];
		//利润的相关币种数据
		$profit_currency = array(
			'total' => array(
				'sell_title' => lang("total {currency} sell", array('currency' => $current_currency)),
				'cost_title' => lang("total {currency} cost", array('currency' => $current_currency)),
				'profit_title' => lang("total {currency} profit", array('currency' => $current_currency)),
				'profit_amount' => 0,
				'currency' => $current_currency,
				'currency_symbol' => '',//符号后续可以在币种表里配置
				'sell_amount' => 0.00,
				'sell_local_amount' => 0.00,
				'cost_amount' => 0.00,
				'cost_local_amount' => 0.00,
				'sum' => 0.00,
				'rate_sum' => 0.00
			),
		);
		foreach ($from_bill_rows as $row){
			$this_profit_key = "{$site} {$row['currency']}";
			$this_currency = $row['currency'];
			if(empty($this_currency)) continue;
			if(!isset($profit_currency[$this_profit_key])){
				$profit_currency[$this_profit_key] = array(
					'sell_title' => lang("{currency} sell", array('currency' => $this_currency)),
					'cost_title' => lang("{currency} cost", array('currency' => $this_currency)),
					'profit_title' => lang("{currency} profit", array('currency' => $this_currency)),
					'profit_amount' => 0,
					'currency' => $this_currency,
					'currency_symbol' => '',//符号后续可以在币种表里配置
					'sell_amount' => 0.00,
					'cost_amount' => 0.00,
					'sum' => 0.00,
					'rate_sum' => 0.00
				);
			}
			//下面代表不用转换的,所以直接取amount
			$this_amount = $row["amount"];
			$val = $row['type'];
			$profit_currency[$this_profit_key]['sum'] += $this_amount;
			$profit_currency[$this_profit_key]['rate_sum'] += $this_amount;

			$profit_currency[$this_profit_key]["{$val}_amount"] += $this_amount;

			$profit_currency[$this_profit_key]['profit_amount'] = round($profit_currency[$this_profit_key]['sell_amount'] - $profit_currency[$this_profit_key]['cost_amount'], 2);
			$profit_currency['total']["{$val}_amount"] += $row['local_amount'];

			$profit_currency['total']['profit_amount'] = round($profit_currency['total']['sell_amount'] - $profit_currency['total']['cost_amount'], 2);
		}
		if(!empty($from_bill_rows)){
			$bill_ETD = $from_bill_rows[0]['bill_ETD'];
			$ex_rate = get_ex_rate($bill_ETD, $from_current_currency, 'sell', $current_currency);
			if($ex_rate != 0){
				$profit_currency['total']["sell_amount"] = amount_currency_change($profit_currency['total']["sell_amount"], $bill_ETD, $from_current_currency, 'sell', $current_currency);
				$profit_currency['total']["cost_amount"] = amount_currency_change($profit_currency['total']["cost_amount"], $bill_ETD, $from_current_currency, 'cost', $current_currency);
				$profit_currency['total']['profit_amount'] = round($profit_currency['total']['sell_amount'] - $profit_currency['total']['cost_amount'], 2);
				$profit_currency['total']['ext'] = array(array('title' => lang('Exrate'), 'amount' => $ex_rate));
			}else{
				$profit_currency['total']["sell_amount"] = lang("未获取到{from_currency}转{to_currency}的汇率", array('from_currency' => $from_current_currency, 'to_currency' => $current_currency));
				$profit_currency['total']["cost_amount"] = lang("未获取到{from_currency}转{to_currency}的汇率", array('from_currency' => $from_current_currency, 'to_currency' => $current_currency));
				$profit_currency['total']['profit_amount'] = lang("未获取到{from_currency}转{to_currency}的汇率", array('from_currency' => $from_current_currency, 'to_currency' => $current_currency));
			}
		}

		//把total移动到最后面
		$total_profit = $profit_currency['total'];
		unset($profit_currency['total']);
		$profit_currency['total'] = $total_profit;
		foreach($profit_currency as $k => $row){
			foreach($row as $key => $val) {
				if (is_numeric($val)) {
					$profit_currency[$k][$key] = number_format($val, 2, '.', '');
				}
			}
		}
		$result['profit'] = $profit_currency;
		echo json_encode($result);
	}
    
    /**
     * billing新增函数
     * 2022-02-10这里执行了大改,将原本各种合并操作分离出来进行了封装
     */
    public function add_data($id_type = "", $id_no = "", $type_arr = array('cost', 'sell'))
    {
        //校验是否有匹配的值,匹配的就将传过来的金额和数量进行增加
        //需要校验的:费用名称,收款方,币种,税率,发票号
        $creditor_name = '';
        $debitor_name = '';
        $sell_id = 0;
        $cost_id = 0;

        //默认费用名称字段
        $result = array('code' => 1, 'msg' => []);
        foreach ($type_arr as $val) {
//            $id_type = postValue($val . '_id_type', $id_type);
//            $id_no = postValue($val . '_id_no', $id_no);
            $val_lang = '';
            if($val == 'cost') $val_lang = 'creditor';
            if($val == 'sell') $val_lang = 'debitor';

            $data = array();
            $data['charge_code'] = postValue('charge_code', '');
            $data['unit_price'] =  postValue($val . '_unit_price', '');
            $data['number'] = postValue($val . '_number', '');

            if($data['unit_price'] == '' && $data['number'] == ''){
                continue;
            }
            
            if($val == 'cost'){
                $data['client_code'] = postValue('creditor', '');
            }else if($val == 'sell'){
                $data['client_code'] = postValue('debitor', '');
            }

            $data['remark'] = postValue($val. '_remark', '');
            $data['currency'] = postValue($val, '');
            $data['vat'] = postValue($val . '_vat', '0%');
            $data['is_special'] = postValue('is_special', 0);
            $data['split_mode'] = postValue($val . '_split_mode', 0);
            $data['rate_id'] = postValue($val . '_rate_id', 0);
            $data['vat_withhold'] = postValue($val . '_vat_withhold', '0%');
            $data['bank_account_id'] = postValue($val . '_bank_account_id', 0);
            $data['id_type'] = $id_type;
            $data['id_no'] = $id_no;
            $data['type'] = $val;
            
            //2022-06-06 新增账单放到了biz_bill_payment_wlk_apply里了, 这里加入一个限制,如果是往来款业务,那么税率只能为0% 和 无票
            if($data['client_code'] == 'WLKYW01'){ //必须是往来款业务才触发
                if(!in_array($data['vat'], array('无票', '0%'))){
                    $result['msg'][] = lang("{type}:往来款业务的税率只能选择 0% 或 无票", array("type" => lang($val_lang)));
                    continue;
                }
            }

            $add_req = add_bill($data);

            $result['msg'][] = lang($val_lang) . ':' . $add_req['msg'];
            if($add_req['code'] !== 0){
                continue;
            }
            $result['code'] = 0;
        }
        if(!empty($creditor_name))$result['creditor_name'] = $creditor_name;
        if(!empty($debitor_name))$result['debitor_name'] = $debitor_name;
        if(!empty($sell_id))$result['sell_id'] = $sell_id;
        if(!empty($cost_id))$result['cost_id'] = $cost_id;
        $result['msg'] = join(',',$result['msg']);
        echo json_encode($result);
    }

    public function update_data()
    {
        $this->load->model('bsc_user_role_model');
        $this->load->model('biz_client_model');
        $this->load->model('biz_charge_model');
        $this->load->model('biz_bill_currency_model');
        $userRole = $this->bsc_user_role_model->get_user_role(array('edit_text'));
        $userRoleEdit = explode(',', $userRole['edit_text']);

        $type_arr = array('cost', 'sell');
        $r = true;
        $creditor_name = '';
        $debitor_name = '';
        
        //获取修改权限
        $bsc_user_role_model = Model('bsc_user_role_model');
        $user_role = $bsc_user_role_model->get_user_role(array('edit_text'));
        $userRoleEdit = explode(',', $user_role['edit_text']);//bill.cost,bill.sell
        
        $result = array('code' => 1, 'msg' => []);
        foreach ($type_arr as $val){
            $id_type = isset($_POST[$val . '_id_type']) ? $_POST[$val . '_id_type'] : '';
            $id_no = isset($_POST[$val . '_id_no']) ? $_POST[$val . '_id_no'] : '';
    
            if($val == 'cost') $val_lang = 'creditor';
            if($val == 'sell') $val_lang = 'debitor';
            
            
            if($id_type == 'shipment'){
                $this->load->model('biz_shipment_model');
                $type_row = $this->biz_shipment_model->get_by_id($id_no);
            }else if($id_type == 'consol'){
                $this->load->model('biz_consol_model');
                $type_row = $this->biz_consol_model->get_by_id($id_no);
                
                $this->load->model('biz_shipment_model');
                $this->db->select('id');
                $shipments = $this->biz_shipment_model->get_shipment_by_consol($id_no);
                if(empty($shipments)){
                    $result = array('code' => 1, 'msg' => lang("请绑定SHIPMENT后再做账"), 'isError' => true);
                    echo json_encode($result);
                    return;
                }
            }else{
                continue;
            }
            //2021-09-23 汪庭彬 马冬青提出 http://wenda.leagueshipping.com/?/question/479
            // if($type_row['status'] == 'cancel'){
            //     $result = array('code' => 1, 'msg' => '已退关，无法做账', 'isError' => true);
            //     echo json_encode($result);
            //     return;
            // }
            
            // if(empty($type_row['booking_ETD'])){
            //     $result['msg'][] = lang("{type}订舱日期不能为空", array("type" => $val_lang));
            //     continue;
            // }
            $bill_ETD = get_bill_ETD($id_type, $id_no);
            $booking_ETD_end = date('Y-m-t', strtotime($bill_ETD));
            $charge_name_key = 'charge_name_cn';
            
            $data = array();
            $id = isset($_POST[$val . '_id']) ? $_POST[$val . '_id'] : '';
            $old_row = $this->biz_bill_model->get_by_id($id);
            //已确认无法修改
			if(!empty($old_row) && $old_row['confirm_date'] != '0000-00-00'){
				$result['msg'][] = lang("{type} cancel confirm date first", array("type" => $val_lang));
				continue;
			}
			if(!empty($old_row) && !empty($old_row['account_no'])){
				$result['msg'][] = lang("{type} cancel account_no first", array("type" => $val_lang));
				continue;
			}
			if(!empty($old_row) && $old_row['invoice_id'] != 0){
				$result['msg'][] = lang("{type} cancel invoice no first", array("type" => $val_lang));
				continue;
			}
			if(!empty($old_row) && $old_row['payment_id'] != 0){
				$result['msg'][] = lang("{type} cancel write off first", array("type" => $val_lang));
				continue;
			}
			if(!empty($old_row) && $old_row['split_id'] != 0){
				$result['msg'][] = lang("{type} not revise split billing", array("type" => $val_lang));
				continue;
			}
			if(!empty($old_row) && $old_row['parent_id'] != 0){
				$result['msg'][] = lang("{type} revise it in consol", array("type" => $val_lang));
				continue;
			}

			$data['charge_code'] = isset($_POST['charge_code']) ? $_POST['charge_code'] : '';
			$charge = $this->biz_charge_model->get_one('charge_code', $data['charge_code']);
			if(empty($charge)){
				$result['msg'][] = lang("{type} not correct charge name", array("type" => $val_lang));
				continue;
			}
            $data['remark'] = isset($_POST[$val . '_remark']) ? $_POST[$val . '_remark'] : '';
            $data['currency'] = isset($_POST[$val]) ? $_POST[$val] : $old_row[''];
            $data['unit_price'] = isset($_POST[$val . '_unit_price']) ? $_POST[$val . '_unit_price'] : '';
            $data['number'] = isset($_POST[$val . '_number']) ? $_POST[$val . '_number'] : '';
            $data['bank_account_id'] = isset($_POST[$val . '_bank_account_id']) ? $_POST[$val . '_bank_account_id'] : 0;
            $data['amount'] = round($data['unit_price'] * $data['number'], 2);
            $data['bill_ETD'] = $bill_ETD;
            if($data['unit_price'] == '' && $data['number'] == '' && $data['amount'] == ''){
                continue;
            }
            //如果单价 数量或者总价为0 则不新增
            if($data['unit_price'] == 0 || $data['number'] == 0 || $data['amount'] == 0){
                $result['msg'][] = lang("{type}:总金额不能为0", array("type" => $val_lang));
                continue;
            }
            
            //无应收或者应付的权限提示
            if(!in_array('bill', $userRoleEdit)){
                if(!in_array('bill.' . $val, $userRoleEdit)){//不存在权限弹出
                    $result['msg'][] = lang("{type}:无权修改", array("type" => $val_lang));
                    continue;
                }
            }
            
            $system_type = get_system_type();
            $currency_row = Model('bsc_dict_model')->no_role_get("catalog = 'currency_{$system_type}' and name = '{$data['currency']}'");
            if(empty($currency_row)){
                $currency_row = Model('bsc_dict_model')->no_role_get("catalog = 'currency' and name = '{$data['currency']}'");
                if(empty($currency_row)){
                    $result['msg'][] = lang("{type}:币种错误", array("type" => $val_lang));
                    continue;
                }
            }

            
            $data['vat'] = isset($_POST[$val . '_vat']) ? $_POST[$val . '_vat'] : '';
            $data['vat_withhold'] = isset($_POST[$val . '_vat_withhold']) ? $_POST[$val . '_vat_withhold'] : '';
            if($val == 'cost'){
                $data['client_code'] = isset($_POST['creditor']) ? $_POST['creditor'] : '';
                $client = $this->biz_client_model->get_one('client_code', $data['client_code']);
                if(!empty($client)){
                    $creditor_name = $client['company_name'];
                    $client_role = explode(',', $client['role']);
                    if(in_array('agent', $client_role)) $charge_name_key = 'charge_name';
                    if(in_array('oversea_client', $client_role)) $charge_name_key = 'charge_name';
                }
            }else if($val == 'sell'){
                $data['client_code'] = isset($_POST['debitor']) ? $_POST['debitor'] : '';
                $client = $this->biz_client_model->get_one('client_code', $data['client_code']);
                if(!empty($client)){
                    $debitor_name = $client['company_name'];
                    $client_role = explode(',', $client['role']);
                    if(in_array('agent', $client_role)) $charge_name_key = 'charge_name';
                    if(in_array('oversea_client', $client_role)) $charge_name_key = 'charge_name';
                }
            }
            if(empty($client)){
                $result['msg'][] = lang("{type}:请选择正确的客户名称", array("type" => $val_lang));
                continue;
            }
            $data['charge'] = isset($charge[$charge_name_key]) ? $charge[$charge_name_key] : '';
            if($client['finance_payment'] == 'after work')$data['overdue_date'] = get_overdue_date($client, $type_row['booking_ETD']);
            if(isset($_POST[$val . '_split_mode'])) $data['split_mode'] = $_POST[$val . '_split_mode'];
            //汇率是否等于本地汇率
            if($data['currency'] == LOCAL_CURRENCY){
                $amount = $data['amount'];
                if(empty($old_row)) $old_row["currency"]="";
                if($data['currency'] != $old_row['currency']) $data['exrate'] = 1;
            }else{
                //查出符合当月
                // $data['exrate'] = get_ex_rate($booking_ETD_end, $this_currency, $val);  
                $data['exrate'] = get_ex_rate($booking_ETD_end, $data['currency'], $val);
                
                // $local_amount = strval($data['amount'] * 100) * strval($data['exrate'] * 1000000);
                $amount = amount_currency_change($data['amount'], $booking_ETD_end, $data['currency']);
                // $amount = $local_amount / 100000000;
            }
            $data['local_amount'] = $amount;

            if($data['client_code'] == ''){
                continue;
            }
            if($id != ''){//id有值为修改
               
                if(empty($data)){
                    $result['code'] = 0;
                    $result['msg'][] = lang("{type}:修改成功", array("type" => $val_lang));
                    continue;
                }
                $id = $this->biz_bill_model->update($id, $data);
                bill_child($id);
                $result['code'] = 0;
                $result['msg'][] = lang("{type}:修改成功", array("type" => $val_lang));
                $action = 'update';
                
                $this->bill_update_queue($id, $old_row, $data, 'update');
            }else{
                $data['type'] = $val;
                $data['id_type'] = $id_type;
                $data['id_no'] = $id_no;
                $this->add_data($data['id_type'], $data['id_no'], array($data['type']));
                $r = false;
                continue;
            }
            $data['id'] = $id;

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_bill";
            $log_data["key"] = $id;
            $log_data["action"] = $action;
            $log_data["master_table_name"] = "biz_" . $id_type;
            $log_data["master_key"] = $id_no;
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        }
        if($r){
            if(!empty($creditor_name))$result['creditor_name'] = $creditor_name;
            if(!empty($debitor_name))$result['debitor_name'] = $debitor_name;
            echo json_encode($result);
        }
    }

    /**
     * 批量开票
     */
    public function batch_invoice()
    {
        $invoice_id = isset($_POST['invoice_id']) ? $_POST['invoice_id'] : 0;
        $ids = isset($_POST['ids']) ? $_POST['ids'] : '';
//        $title = isset($_POST['title']) ? $_POST['title'] : '';
        $result = array('code' => 1, 'msg' => lang("参数错误"));
        if (empty($ids)) {// || empty($title)
            echo json_encode($result);
            return;
        }

        $msg_more = '';
        $ids = explode(',', $ids);
        $bill_datas = $this->biz_bill_model->not_role_get("id in ('" . join('\',\'', $ids) . "')", 'id', 'desc');
//        $confirm_dates = array_column($bill_datas, 'confirm_date');
//        //如果去除空值后,总长度小于原本的,说明存在未确认的
//        if(sizeof(array_filter($confirm_dates), function ($var){if(!empty($var) && $var != '0000-00-00'){return $var;}}) < $confirm_dates){
//            echo json_encode(array('code' => 1, 'msg' => '请确认后再进行开票'));
//            return;
//        }
        // Model('biz_bill_currency_model');
        $time_end = date('Y-m-t', time());
        // $currency_local = $this->biz_bill_currency_model->get_where_one("name = '" . LOCAL_CURRENCY . "' and start_time <= '$time_end'");
        $local_amount_sum = 0;
        //2021-07-28 汪庭彬 http://wenda.leagueshipping.com/?/question/14 开票必须确认
        $not_confirm_date = array();
        $invoice_ex_rate_arr = array();
        $invoice_ex_rate = 0;
        foreach ($bill_datas as $bill_data){
            if($bill_data['type'] == 'cost'){
                $bill_data['amount'] = -$bill_data['amount'];
            }
            //2021-07-28 10:58 上架后mars商量成本不需要确认就可以开票
            if($bill_data['type'] == 'sell' && $bill_data['confirm_date'] == '0000-00-00') $not_confirm_date[] = $bill_data;
            // if(!isset($currency_this[$bill_data['currency']]))$currency_this[$bill_data['currency']] = $this->biz_bill_currency_model->get_where_one("name = '{$bill_data['currency']}' and start_time <= '$time_end'");
            // $invoice_ex_rate = $currency_local['invoice_value'] / $currency_this[$bill_data['currency']]['invoice_value'];
            //2022-07-27 使用一个变量存储下这个汇率,月份相同的就不重复查了
            // if(isset($invoice_ex_rate_arr[$time_end])) $invoice_ex_rate = $invoice_ex_rate_arr[$time_end];
            // else $invoice_ex_rate = $invoice_ex_rate_arr[$time_end] = get_ex_rate($time_end, $bill_data['currency'], 'invoice');
            // $local_amount_sum += $bill_data['amount'] * $invoice_ex_rate;
            $local_amount_sum += amount_currency_change($bill_data['amount'], $time_end, $bill_data['currency'], 'invoice');
            if($bill_data['invoice_id'] != 0 && $bill_data['invoice_id'] != $invoice_id){
                $result['msg'] = lang("ID {id} 的账单已开票，请联系财务撤销开票后再试", array("id" => $bill_data['id']));
                echo json_encode($result);
                return;
            }
        }

        $data = array();
        // $data['invoice_type'] = isset($_POST['invoice_type']) ? trim($_POST['invoice_type']) : null;
        //发票类型默认为无
        $data['invoice_type'] = '1001';

        if(!empty($not_confirm_date) && $data['invoice_type'] != 1001){
            $result['msg'] = lang("ID {id} 的账单请确认后再进行开票", array("id" => join(',', array_column($not_confirm_date, 'id'))));
            echo json_encode($result);
            return;
        }

        $this->load->model('biz_bill_invoice_model');
        //根据发票号获取数据,如果存在,则更新且原本金额基础上增加

        $invoice_no = isset($_POST['invoice_no']) ? trim($_POST['invoice_no']) : '';
        $data['invoice_amount'] = $local_amount_sum;
        $data['invoice_ex_rate'] = $invoice_ex_rate;
        // $data['invoice_company'] = isset($_POST['invoice_company']) ? trim($_POST['invoice_company']) : '';
        //2023-04-14 这里给他们默认为他们的地区字母
        $data['invoice_company'] = get_system_type();
        $data['remark'] = isset($_POST['remark']) ? ts_replace(trim($_POST['remark'])) : '';
        $data['disabled'] = isset($_POST['disabled']) ? trim($_POST['disabled']) : '';
        $data['invoice_date'] = isset($_POST['invoice_date']) ? trim($_POST['invoice_date']) : '0000-00-00';
        $data['buyer_client_code'] = isset($_POST['buyer_client_code']) ? trim($_POST['buyer_client_code']) : '';
        $data['buyer_company'] = isset($_POST['buyer_company']) ? trim($_POST['buyer_company']) : '';
        $data['buyer_taxpayer_id'] = isset($_POST['buyer_taxpayer_id']) ? trim($_POST['buyer_taxpayer_id']) : '';
        //获取对应的client信息
        $biz_client_model = Model('biz_client_model');
        if(($data['invoice_type'] !== '1001' && $data['invoice_type'] !== '999')){
            $client = $biz_client_model->get_one('client_code',$bill_data["client_code"]);

            $client = array_filter(array_map(function ($val){
                return trim($val);
            }, $client));

            if((empty($data['buyer_company']) || empty($data['buyer_taxpayer_id']))){
                $result['msg'] = lang("请到往来单位填写该客户对应的纳税人名称或纳税人识别号后再提交");
                echo json_encode($result);
                return;
            }
            //2022-08-11 开票申请提交的时候，如果是USD，则判断开票抬头必须要有美元账号 这里因为和账单直接挂钩,如果账单全为USD,那么这里触发事件
            $currency_arr = filter_unique_array(array_column($bill_datas, 'currency'));
            if(sizeof($currency_arr) == 1 && in_array('USD', $currency_arr)){
                if(empty($client['bank_account_usd'])) return jsonEcho(array('code' => 1, 'msg' => lang("当前为美元开票, 请添加当前客户对应的USD银行账号信息"),$data['buyer_company']));
            }else{
                if(!empty($client) && (empty($client['taxpayer_address']) || empty($client['taxpayer_telephone']) || empty($client['bank_name_cny']) || empty($client['bank_account_cny']))){
                    $result['msg'] = lang("客户为 {buyer_company} 的纳税人地址或纳税人联系人电话,银行(CNY),银行账号(CNY)未填写", array("buyer_company" => $data['buyer_company']));
                    echo json_encode($result);
                    return;
                }
            }

        }
        $data['currency'] = isset($_POST['currency']) ? trim($_POST['currency']) : '';
        $data['vat'] = isset($_POST['vat']) ? trim($_POST['vat']) : '0%';
        //当金额是负数不受规则控制
        $invoice_amount = isset($_POST['invoice_amount']) ? $_POST['invoice_amount'] : '';

        if($invoice_amount > 0){
            //cost开放 6%的专票
            if(in_array($data['invoice_type'], array(2, 3)) && $data['vat'] == '0%'){//专票
                $result['msg'] = lang("专票税率不能为{vat}", array('vat' => "0%"));
                echo json_encode($result);
                return;
            }else if($data['invoice_type'] == 0 && $data['vat'] != '0%'){//普票
                $result['msg'] = lang("普票税率必须为{vat}", array("vat" => "0%"));
                echo json_encode($result);
                return;
            }else if($data['invoice_type'] == 1){//电子普票
                //如果是纯COST,那么开放到0%和6%
                $bill_datas_type = filter_unique_array(array_column($bill_datas, 'type'));
                if(sizeof($bill_datas_type) == 1 && in_array('cost', $bill_datas_type)){
                    if(!in_array($data['vat'], array('0%', '6%'))){
                        $result['msg'] = lang("电票税率必须为{vat}", array('vat' => lang("0%或6%")));
                        echo json_encode($result);
                        return;
                    }
                }else{
                    if($data['vat'] != '0%'){
                        $result['msg'] = lang("电票税率必须为{vat}", array('vat' => lang("0%")));
                        echo json_encode($result);
                        return;
                    }
                }

            }else if($data['invoice_type'] == 2 && in_array('USD', array_column($bill_datas, 'currency'))){ // 专票不能有美元
                $result['msg'] = lang("美元不能申请专票");
                echo json_encode($result);
                return;
            }
        }


        //如果是形式发票,自动生成单号
        $is_dn = false;
        $account_id = isset($_POST['account_id']) ? (int)$_POST['account_id'] : 0;
        if(empty($invoice_no)){
            // if($data['invoice_type'] == 999){
            //     $data['payment_bank'] = isset($_POST['payment_bank']) ? $_POST['payment_bank'] : '';
            //     if(empty($data['invoice_date']) || $data['invoice_date'] == '0000-00-00') $data['invoice_date'] = date('Y-m-d');
            //     if(empty($invoice_no))$invoice_no = dn_invoice_no_rule($data['payment_bank']);
            // }else if($data['invoice_type'] == 1001){
            //     $data['use_type'] = isset($_POST['use_type']) ? $_POST['use_type'] : '';
            //     if(empty($data['invoice_date']) || $data['invoice_date'] == '0000-00-00') $data['invoice_date'] = date('Y-m-d');
            //     //前缀--start
            //     Model('bsc_dict_model');
            //     $dict = $this->bsc_dict_model->get_one('name', $data['use_type'], 'use_type');
            //     //前缀--end

            //     //查询配置最新编号--star
            //     Model('sys_config_model');
            //     $config_num = $this->sys_config_model->get_one_zero('USE_TYPE_NOTE_' . $dict['value']);
            //     $this_num = 0;
            //     if(!empty($config_num)) $this_num = $config_num['config_text'];
            //     //查询配置最新编号--end

            //     //最后8位数字,以同前缀最后一条+1
            //     $prefix = $dict['value'];
            //     $invoice_no_length = 5;
            //     $num = str_pad($this_num + 1, $invoice_no_length, '0', STR_PAD_LEFT);
            //     if(empty($config_num)){
            //         $this->sys_config_model->insert_zero('USE_TYPE_NOTE_' . $dict['value'], $this_num + 1);
            //     }else{
            //         $this->sys_config_model->update_zero('USE_TYPE_NOTE_' . $dict['value'], $this_num + 1);
            //     }
            //     if(empty($invoice_no))$invoice_no = $prefix . $num;

            //     //如果是DN的，那么此处生成文件且返回
            //     if ($prefix == 'DN'){
            //         $data['account_id'] = $account_id;
            //         if($account_id == 0){
            //             $result['msg'] = lang("未配置当前客户的debit note account");
            //             echo json_encode($result);
            //             return;
            //         }
            //         $is_dn = true;
            //     }
            // }
        }

        $data['invoice_no'] = $invoice_no;

        if($invoice_no !== ''){
            $this->db->where('`lock` != 999');
            $old_invoice = $this->biz_bill_invoice_model->get_one('invoice_no', $invoice_no);

            //存在该发票号则获取金额,其余以当前为准
            if(!empty($old_invoice)){
                //若该发票号已核销或已审核,则无法合并
                if($old_invoice['lock'] > 1){
                    $result['code'] = 2;
                    $result['msg'] = lang("该发票号无法合并");
                    echo json_encode($result);
                    return;
                }
                //开票后修改,不能是已存在发票号
                if($invoice_id > 0){
                    $result['msg'] = lang("不能修改为已存在发票号");
                    echo json_encode($result);
                    return;
                }
                $old_invoice_bill_datas = $this->biz_bill_model->not_role_get("invoice_id = {$old_invoice['id']}", 'id', 'desc');
                $old_invoice_amount = 0;
                foreach ($old_invoice_bill_datas as $old_invoice_bill_data){
                    if($old_invoice_bill_data['type'] == 'cost'){
                        $old_invoice_bill_data['amount'] = -$old_invoice_bill_data['amount'];
                    }
                    // if(!isset($currency_this[$old_invoice_bill_data['currency']]))$currency_this[$old_invoice_bill_data['currency']] = $this->biz_bill_currency_model->get_where_one("name = '{$old_invoice_bill_data['currency']}' and start_time <= '$time_end'");
                    // $currency_local['invoice_value'] / $currency_this[$bill_data['currency']]['invoice_value']
                    // $old_invoice_amount += $old_invoice_bill_data['amount'] * get_ex_rate($time_end, $bill_data['currency'], 'invoice');
                    $old_invoice_amount += amount_currency_change($old_invoice_bill_data['amount'], $time_end, $bill_data['currency'], 'invoice');
                }
                $data['invoice_amount'] += $old_invoice_amount;
                $invoice_id = $old_invoice['id'];
            }
        }

        //2021-08-27 由于拆分开票的功能上线，林倩提出将该判断取消
        // //超过99999.99不给开票
        // if($invoice_no == '' && $data['invoice_amount'] > 99999.99 && $data['invoice_type'] < 999){
        //     $result['msg'] = '金额超出99999.99,不予开票';
        //     echo json_encode($result);
        //     return;
        //     // $msg_more .= ',金额超出99999.99,不予开票';
        // }
//        if($data['invoice_amount'] > 99999.99 && $data['invoice_type'] >= 999){
//            $msg_more .= ',' . lang("金额超出99999.99,不予开票");
//        }
        if($invoice_id == 0){//1、未发票，新增
            $invoices = filter_unique_array(array_column($bill_datas, 'invoice_id'));
            if(sizeof($invoices) > 0){//去除未绑定
                $result['msg'] = lang("请勿选择已开票数据");
                echo json_encode($result);
                return;
            }
            $action = 'insert';
            //添加发票数据--start
            if(!empty($data['invoice_date']) && $data['invoice_date'] != '0000-00-00'){
                $data['lock'] = 1;
            }
            $data['invoice_user_id'] = $this->session->userdata('id');

            $invoice_id = $this->biz_bill_invoice_model->save($data);
            //添加发票数据--end
        }else{//2、已发票，修改
            $invoices = filter_unique_array(array_column($bill_datas, 'invoice_id'));
            if(sizeof($invoices) > 1){//去除未绑定
                $result['msg'] = lang("请勿同时修改多个开票信息");
                echo json_encode($result);
                return;
            }
            //只能修改lock=0的
            $invoice_info = $this->biz_bill_invoice_model->get_one('id', $invoice_id);
            if(empty($invoice_info)){//不存在报错
                $result['msg'] = lang("发票信息不存在");
                echo json_encode($result);
                return;
            }
            $action = 'update';
            //修改发票数据--start
            if($invoice_info['lock'] == 10) {
                $data['lock'] = 2;
                $data['invoice_user_id'] = $this->session->userdata('id');
            }else{
                $data['lock'] = 1;
            }
            $invoice_id = $this->biz_bill_invoice_model->update($invoice_id, $data);
            //修改发票数据--end
        }

        if($invoice_id > 0){
            $data['id'] = $invoice_id;
            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_bill_invoice";
            $log_data["key"] = $invoice_id;
            $log_data["action"] = $action;
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        }
        //绑定对应的发票ID--start
        $update_data = array();
        $biz_client_model = Model('biz_client_model');


        //2022-07-26 查询授信额度改为在外面, 因为他们会几千个账单处理,这里会卡死
        //获取授信额度进行过期日期的计算
        $this->db->select('id, finance_payment, finance_od_basis, finance_payment_month, finance_payment_day_th, finance_payment_days');
        $bill_clients = array_column($biz_client_model->get("client_code in ('" . join('\',\'', array_column($bill_datas, 'client_code')) . "')"), null, 'client_code');
//        if(!empty($client) && $client['finance_payment'] == 'after invoice')$this_data['overdue_date'] = get_overdue_date($client, $data['invoice_date']);
        foreach ($bill_datas as $bill_data){
            //判定当前bill invoice_id为0的,给予绑定
            if($bill_data['invoice_id'] != 0){
                continue;
            }
            $this_data = array();
            $this_data['id'] = $bill_data['id'];
            $this_data['invoice_id'] = $invoice_id;

            //获取授信额度进行过期日期的计算
            $bill_client = isset($bill_clients[$bill_data['client_code']]) ? $bill_clients[$bill_data['client_code']] : array();
//            $this->db->select('id, finance_payment, finance_od_basis, finance_payment_month, finance_payment_day_th, finance_payment_days');
//            $client = $biz_client_model->get_one('client_code', $bill_data['client_code']);
            if(!empty($bill_client) && $bill_client['finance_payment'] == 'after invoice')$this_data['overdue_date'] = get_overdue_date($bill_client, $data['invoice_date']);
            $update_data[] = $this_data;

            $log_data = array();
            $log_data["table_name"] = "biz_bill";
            $log_data["key"] = $bill_data['id'];
            $log_data["master_table_name"] = "biz_" . $bill_data['id_type'];
            $log_data["master_key"] = $bill_data['id_no'];
            $log_data["action"] = 'update';
            $log_data["value"] = json_encode($this_data);
            log_rcd($log_data);
        }
        //这里可以考虑拆为300条一次
        $pc = 300;
        for ($i = 0; $i < sizeof($update_data) / $pc; $i++){
            $this_update_data = array_slice($update_data, $i * $pc, $pc);
            $this->biz_bill_model->mbacth_update($this_update_data);
        }
//        $this->biz_bill_model->mbacth_update($update_data);

        if($is_dn){
            $result['pdf_path'] = 'http://china.leagueshipping.com/other/dn_invoice_pdf_by_no?invoice_no=' . $data['invoice_no'] . '&shipment_id=' . $bill_datas[0]['id_no'];
        }
        //绑定对应的发票ID--end
        $result['code'] = 0;
        if($action == 'insert'){
            $result['msg'] = lang('开票成功') . $msg_more;

        }else if($action == 'update'){
            $result['msg'] = lang("修改成功") . $msg_more;
        }
        echo json_encode($result);
    }

    public function auto_create(){
    	$d = date('mY');
    	$invoice = $this->db->where("invoice_no like '$d%' and length(invoice_no) = 9")->order_by('invoice_no','desc')->get('biz_bill_invoice')->row_array();
    	if(empty($invoice)){
    		$invoice_no = $d.'001';
    		return jsonEcho(['code'=>1,'invoice_no'=>$invoice_no]);
		}
		$num = str_pad((int)substr($invoice['invoice_no'], -3, 3) + 1, 3, '0', STR_PAD_LEFT);
		return jsonEcho(['code'=>1,'invoice_no'=>$d.$num]);
	}
    public function auto_create_third_party(){
		$this->db = $this->load->database('china',true);
    	$d = date('mY');
    	$invoice = $this->db->where("invoice_no like '$d%' and length(invoice_no) = 9")->order_by('invoice_no','desc')->get('biz_bill_invoice')->row_array();
    	if(empty($invoice)){
    		$invoice_no = $d.'001';
    		return jsonEcho(['code'=>1,'invoice_no'=>$invoice_no]);
		}
		$num = str_pad((int)substr($invoice['invoice_no'], -3, 3) + 1, 3, '0', STR_PAD_LEFT);
		return jsonEcho(['code'=>1,'invoice_no'=>$d.$num]);
	}

    /**
     * 批量删除开票
     */
    public function batch_del_invoice(){
        //一次只能销毁一个开票信息
        $invoice_id = isset($_POST['invoice_id']) ? $_POST['invoice_id'] : 0;
        $is_special = isset($_GET['is_special']) ? check_param($_GET['is_special']) : 0;
//        $title = isset($_POST['title']) ? $_POST['title'] : 0;
        if($invoice_id == 0) {
            echo json_encode(array('code' => 1, 'msg' => 'error'));
            return;
        }
        $this->load->model('biz_bill_invoice_model');
        $invoice = $this->biz_bill_invoice_model->get_one('id', $invoice_id);
        if(empty($invoice)){
            echo json_encode(array('code' => 1, 'msg' => lang("开票信息错误")));
            return;
        }
        if(!empty($invoice)){
            //2022-08-03 没有财务的角色时,如果撤销,只能撤销自己的,且发票号和发票日期不能都有值
            //2022-08-08 应收应付财务加入
            if(!in_array('finance', get_session('user_role')) && !in_array('finance_sell', get_session('user_role')) && !in_array('finance_cost', get_session('user_role'))){
                if($invoice['created_by'] != get_session('id')) return jsonEcho(array('code' => 1, 'msg' => lang("不是发票创建人,无法撤销")));

                //发票管理里的查询是 发票号为空 or 发票时间 为 0000
                if($invoice['invoice_no'] !== '' || $invoice['invoice_date'] !== '0000-00-00') return jsonEcho(array('code' => 1, 'msg' => lang("发票已存在开票日期或发票号,请联系财务撤销")));
            }

            $invoice_update = array('lock' => 999);
            $this->biz_bill_invoice_model->update($invoice_id, $invoice_update);
            
            $log_data = array();
            $log_data["table_name"] = "biz_bill_invoice";
            $log_data["key"] = $invoice_id;
            $log_data["action"] = 'update';
            $log_data["value"] = json_encode($invoice_update);
            log_rcd($log_data);

            //销毁时将相关的同ID一并置为0
            $bills = $this->biz_bill_model->not_role_get("invoice_id = '$invoice_id'", 'id', 'desc');
            $update_data = array();
            foreach ($bills as $bill){
                $data = array();
                $data['id'] = $bill['id'];
                $data['invoice_id'] = 0;
                $update_data[] = $data;

                $log_data = array();
                $log_data["table_name"] = "biz_bill";
                $log_data["key"] = $bill['id'];
                $log_data["master_table_name"] = 'biz_bill_invoice';
                $log_data["master_key"] = $invoice_id;
                $log_data["action"] = 'update';
                $log_data["value"] = json_encode($data);
                log_rcd($log_data);
            }
            $this->biz_bill_model->mbacth_update($update_data);
            //绑定对应的发票ID--end
            echo json_encode(array('code' => 0, 'msg' => lang("撤销开票成功")));
        }else{
            echo json_encode(array('code' => 1, 'msg' => lang("该开票信息不允许撤销开票")));
        }

    }

    /**
     * 核销
     */
    public function batch_write_off(){
        $ids = isset($_POST['ids']) ? $_POST['ids'] : '';

        if (empty($ids)) {
            echo json_encode(array('code' => 1, 'msg' => lang("参数错误")));
            return;
        }
        Model('biz_bill_payment_model');
        $ids_array = explode(',', $ids);
        $ids_array = array_filter(array_map(function ($val){
            return (int)$val;
        }, $ids_array));
        if(empty($ids_array)){
            echo json_encode(array('code' => 1, 'msg' => lang("参数错误")));
            return;
        }
        $this->db->select('id, type, currency, local_amount,amount,invoice_id');
        $bills = $this->biz_bill_model->get('id in (' . join(',', $ids_array) . ')', 'id', 'desc', "");
        if(empty($bills)){
            echo json_encode(array('code' => 1, 'msg' => lang("未找到账单")));
            return;
        }
        $local_amount_sum = 0;
        foreach ($bills as $bill){
            if($_POST['currency']=='CNY'){
                $this_local_amount = $bill['type'] == 'sell' ? $bill['local_amount'] : -$bill['local_amount'];
                $local_amount_sum += $this_local_amount;
            }else{
                $this_local_amount = $bill['type'] == 'sell' ? $bill['amount'] : -$bill['amount'];
                $local_amount_sum += $this_local_amount;
            }
        }

        $data = array();
        if(isset($_POST['payment_date']) && !empty($_POST['payment_date'])){
            $data['payment_date'] = $_POST['payment_date'];
        }
        isset($_POST['payment_amount']) && $data['payment_amount'] = $_POST['payment_amount'];
        isset($_POST['payment_way']) && $data['payment_way'] = $_POST['payment_way'];
        isset($_POST['payment_bank']) && $data['payment_bank'] = $_POST['payment_bank'];
        isset($_POST['payment_no']) && $data['payment_no'] = trim($_POST['payment_no']);
        isset($_POST['currency']) && $data['currency'] = trim($_POST['currency']);//2022-06-13 加入币种
        isset($_POST['other_side_company']) && $data['other_side_company'] = trim($_POST['other_side_company']);//2022-06-15 加入收付款对方抬头
        $data['payment_user_id'] = get_session('id');
        
        //判断收款银行的抬头和开票的抬头要一致 
        // $invoice_ids = array_column($bills,'invoice_id');
        // $invoice_ids = array_unique($invoice_ids); 
        // $invoice_ids = join(",",$invoice_ids);
        // $sql = "select (SELECT ext1 FROM `bsc_dict` where catalog = 'invoice_title' and  `name` = biz_bill_invoice.invoice_company) as invoice_company from biz_bill_invoice where id in ($invoice_ids);";
        // $invoice_company = Model('m_model')->query_array($sql); 
        // $invoice_company = array_column($invoice_company,'invoice_company'); 
        // $invoice_company = array_filter($invoice_company);
        
        // $sql = "SELECT * FROM `bsc_dict` where catalog = 'payment_bank' and  `name` = '{$data['payment_bank']}';";
        // $bankrow = $this->m_model->query_one($sql); 
        // if(empty($bankrow)) exit(json_encode(array('code' => 1, 'msg' => '无此银行')));
        // if(!in_array($bankrow['ext2'],$invoice_company) && sizeof($invoice_company)>0) exit(json_encode(array('code' => 1, 'msg' => '收款抬头和开票抬头不一致，核销失败！'))); 
        
        $id = $this->biz_bill_payment_model->save($data);  

        if($id <= 0){
            echo json_encode(array('code' => 1, 'msg' => lang("核销失败")));
            return;
        }
        $log_data = array();
        $log_data["table_name"] = "biz_bill_payment";
        $log_data["key"] = $id;
        $log_data["action"] = 'insert';
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        //给上面的bill批量关联
        $bills_ids = array_column($bills, 'id');
        $this->payment_id = $id;
        
        $batch_bill = array_map(function ($val){
            return array('id' => $val, 'payment_id' => $this->payment_id);
        }, $bills_ids);
        
        
        $this->biz_bill_model->mbacth_update($batch_bill);

        if(isset($data['payment_amount']) && $local_amount_sum != $data['payment_amount']){
            echo json_encode(array('code' => 0, 'msg' => lang("核销成功,核销金额与实际bill金额不一致,计算:{local_amount_sum},当前:{payment_amount}", array('local_amount_sum' => $local_amount_sum, 'payment_amount' => $data['payment_amount']))));
            return;
        }
        echo json_encode(array('code' => 0, 'msg' => lang("success")));
    }

    public function batch_del_write_off(){
        $payment_id = isset($_POST['payment_id']) ? (int)$_POST['payment_id'] : 0;
        if(empty($payment_id)){
            echo json_encode(array('code' => 1, 'msg' => lang('No id')));
            return;
        }
        Model('biz_bill_payment_model');
        $old_row = $this->biz_bill_payment_model->get_one('id', $payment_id);

        if($old_row['lock_date'] !== '0000-00-00'){
            echo json_encode(array('code' => 1, 'msg' => lang("Please cancel write off review")));
            return;
        }
        
        //2021-08-31 如果不是当前核销人不允许撤销核销 马嬿雯提出  http://wenda.leagueshipping.com/?/question/415
        if(!is_admin() && get_session('id') != $old_row['payment_user_id']){
            echo json_encode(array('code' => 1, 'msg' => lang('Not a correct User')));
            return;
        }

        if(!empty($old_row)){
            //给该payment_id的账单修改为0
            $this->db->select('id, payment_id');
            $bills = $this->biz_bill_model->get('payment_id = ' . $payment_id, 'id', 'desc', "");
            if(empty($bills)){
                echo json_encode(array('code' => 1, 'msg' => lang('No payment id,error')));
                return;
            }

            $this->biz_bill_payment_model->mdelete($payment_id);

            $log_data = array();
            $log_data["table_name"] = "biz_bill_payment";
            $log_data["key"] = $payment_id;
            $log_data["action"] = 'delete';
            $log_data["value"] = json_encode($old_row);
            log_rcd($log_data);

            $bill_updates = array_map(function ($row){
                return array('id' => $row['id'], 'payment_id' => 0);
            }, $bills);
            $this->biz_bill_model->mbacth_update($bill_updates);

            $log_data = array();
            $log_data["table_name"] = "biz_bill_payment";
            $log_data["key"] = $payment_id;
            
            $log_data["action"] = 'payment_delete';
            $log_data["value"] = json_encode(array('ids' => array_column($bills, 'id')));
            log_rcd($log_data);
        }
        echo json_encode(array('code' => 0, 'msg' => lang("撤销成功")));
    }
    
    /**
     * 审核
     */
    public function batch_examine(){
        $payment_ids = isset($_POST['payment_ids']) ? $_POST['payment_ids'] : '';

        if (empty($payment_ids)) {
            echo json_encode(array('code' => 1, 'msg' => lang('参数错误')));
            return;
        }
        Model('biz_bill_payment_model');
        $payment_ids_array = explode(',', $payment_ids);
        $payment_ids_array = array_filter(array_map(function ($val){
            return (int)$val;
        }, $payment_ids_array));
        if(empty($payment_ids_array)){
            echo json_encode(array('code' => 1, 'msg' => lang('参数错误')));
            return;
        }
        $this->db->select('id,lock,lock_date,lock_user_id');
        $payments = $this->biz_bill_payment_model->get('id in (' . join(',', $payment_ids_array) . ')');

        $data = array();
        if(!isset($_POST['lock_date']) || (isset($_POST['lock_date']) && empty($_POST['lock_date']))){
            echo json_encode(array('code' => 1, 'msg' => lang('审核时间不能为空')));
            return;
        }
        $data['lock_date'] = $_POST['lock_date'];
        $data['lock_user_id'] = $this->session->userdata('id');
        $datas = array();
        foreach ($payments as $payment){
            $data['id'] = $payment['id'];
            $datas[] = $data;

            $log_data = array();
            $log_data["table_name"] = "biz_bill_payment";
            $log_data["key"] = $payment['id'];
            $log_data["action"] = 'update';
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        }

        $this->biz_bill_payment_model->mbacth_update($datas);

        echo json_encode(array('code' => 0, 'msg' => lang('审核成功'),lastquery()));
    }

    /**
     * 新的审核
     */
    public function batch_examine_payment(){
        $data=$this->db->where('payment_date',$_POST['payment_date'])->where('other_side_company',$_POST['other_side_company'])->where('payment_way',$_POST['payment_way'])->where('payment_bank',$_POST['payment_bank'])->where('payment_no',$_POST['payment_no'])->where('lock_date','0000-00-00')->where('lock_user_id','0')->get('biz_bill_payment')->result_array();

        if(empty($data)){
            echo json_encode(array('code' => 1, 'msg' => lang('暂无审核票')));
            return;
        }

        //获取审核时间
        $lock_date = $_POST['lock_date'];
        //获取id
        $ids=array_column($data,'id');
        $ids=implode(',',$ids);

        $_POST=array(
            'payment_ids'=>$ids,
            'lock_date'=>$lock_date,
        );
        return $this->batch_examine();
    }

    /**
     * 撤销审核
     */
    public function batch_del_examine()
    {
        $payment_id = isset($_POST['payment_id']) ? (int)$_POST['payment_id'] : 0;
        if (empty($payment_id)) {
            echo json_encode(array('code' => 1, 'msg' => lang('参数错误')));
            return;
        }
        $this->load->model('biz_bill_payment_model');

        $old_row = $this->biz_bill_payment_model->get_one('id', $payment_id);
        if (empty($old_row)) {
            echo json_encode(array('code' => 1, 'msg' => lang('核销信息不存在')));
            return;
        }
        if($old_row['lock_date'] === '0000-00-00'){
            echo json_encode(array('code' => 1, 'msg' => lang('Not reviewed')));
            return;
        }
        //2021-08-31 如果不是当前核销人不允许撤销核销 马嬿雯提出  http://wenda.leagueshipping.com/?/question/415
        if(!is_admin() && get_session('id') != $old_row['lock_user_id']){
            echo json_encode(array('code' => 1, 'msg' => lang('Not correct user')));
            return;
        }
        
        $data = array();
        $data['lock_date'] = '0000-00-00';
        $data['lock_user_id'] = 0;
        $this->biz_bill_payment_model->update($payment_id, $data);

        $log_data = array();
        $log_data["table_name"] = "biz_bill_payment";
        $log_data["key"] = $payment_id;
        $log_data["action"] = 'update';
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        echo json_encode(array('code' => 0, 'msg' => lang('success')));
    }

    public function index_all()
    {
        // if(!in_array('finance', get_session('user_role')) && !in_array('finance_sell', get_session('user_role')) && !in_array('finance_cost', get_session('user_role'))){
        //     echo lang("不是财务");
        //     return;
        // }
        $data = array();
//        $title = isset($_GET['title']) ? $_GET['title'] : 'cost';
//        $data['title'] = $title;
        $field_arr = $this->field_edit;
        $data['field_arr'] = $field_arr;

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_bill_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }
        // page account
        $rs = $this->sys_config_model->get_one('biz_bill_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }

         $is_special = isset($_GET['is_special']) ? $_GET['is_special'] : 0;
        $data['is_special'] = $is_special;
        
        //2023-05-31 获取当前币种
        $data['currency_table_fields'] = array_column(Model('bsc_dict_model')->get_option("currency"), 'value');
        $data['local_currency'] = get_system_config("CURRENT_CURRENCY");
        $this->load->view('head');
        $this->load->view('biz/bill/index_all_split_view', $data);
    }

    //2022-11-08 新版的get_data_all
    public function get_data_all()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $is_special = '';

        //-------这个查询条件想修改成通用的 -------------------------------

        Model('biz_bill_invoice_model');
        Model('biz_bill_payment_model');
        $where = array();

        $title_data = get_user_title_sql_data('biz_bill', 'biz_bill_index');//获取相关的配置信息
        $this->all_where($where, $sort);
         
        $parent_id = isset($_REQUEST['parent_id']) ? trim($_REQUEST['parent_id']) : '';
        if($parent_id=='0'){
            $where[]="biz_bill.parent_id = '$parent_id'";
        }

        $where = join(' and ', $where);
        //-----------------------------------------------------------------
        $offset = ($page - 1) * $rows;
        $this->db->limit($rows, $offset);
        $this->db->is_reset_select(false);//不重置查询
        $selects = array(
            'biz_bill.id as id',
            'biz_bill.id_type as id_type',
            'biz_bill.id_no as id_no',
            'biz_bill.currency as currency',
            'biz_bill.local_amount as local_amount',
            'biz_bill.amount as amount',
            'biz_bill.invoice_id as invoice_id',
            'biz_bill.payment_id as payment_id',
            'biz_bill.client_code as client_code',
            'biz_bill.type as type',
            'biz_bill.vat as vat',
            'biz_bill.commision_flag as commision_flag',
            'biz_bill.currency as currency',
        );
        $selects[] = $title_data['base_select_field']['biz_bill.lock_date'];
        $selects[] = $title_data['base_select_field']['biz_bill.client_code_name'];

        foreach ($title_data['select_field'] as $key => $val){
            if($key == 'biz_bill.commision_flag') isset($title_data['base_select_field']['biz_bill.commision_month']) && $selects[] = $title_data['base_select_field']['biz_bill.commision_month'];

            if(!in_array($val, $selects)) $selects[] = $val;
        }
        $this->db->select(join(',', $selects), false);
        //获取需要的字段--end

        $rs = Model('biz_bill_model')->join_d_get($where, $sort, $order, $is_special, true, false);
        $result[] = lastquery();
        $this->db->clear_limit();//清理下limit
        $result["total"] = $this->db->count_all_results('');
        $rows = array();

        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function batch_confirm_date(){
        Model('biz_bill_model');
        $ids = isset($_POST['ids']) ? check_param($_POST['ids']) : '';
        $ts = isset($_POST['ts']) ? (int)$_POST['ts'] : 0;
        $is_special = isset($_POST['is_special']) ? check_param($_POST['is_special']) : '';
        $rows = array_column($this->biz_bill_model->get("id in ($ids)", 'id', 'desc', $is_special), null, 'id');
        $ids_array = explode(',', $ids);
        if(empty($ids_array)){
            echo json_encode(array('code' => 1, 'msg' => lang("参数错误")));
            return;
        }
        $confirm_date = isset($_POST['confirm_date']) && $_POST['confirm_date'] != '' ? $_POST['confirm_date'] : '0000-00-00';
        //检测付款方是否为同一个,如果不是,检测币种,税率是否相同
        $client_codes = array_unique(array_column($rows, 'client_code'));
        $currencys = array_unique(array_column($rows, 'currency'));
        $vats = array_unique(array_column($rows, 'vat'));
        if($ts === 0 && sizeof($client_codes) > 1 && (sizeof($currencys) > 1 || sizeof($vats) > 1)){
            echo json_encode(array('code' => 1, 'msg' => lang("修改失败,原因:币种或税率不同")));
            return;
        }

        //只有创建人自己可以解锁
        $update_datas = array();
        foreach ($ids_array as $id){
            //已开票无法修改
            if(!isset($rows[$id])) continue;
            if($rows[$id]['invoice_id'] != 0){
                echo json_encode(array('code' => 1, 'msg' => lang("修改失败,原因:已开票")));
                return;
            }
            $update_data = array();
            $update_data['id'] = $id;
            $update_data['confirm_date'] = $confirm_date;
            // $update_data['account_no'] = $account_no;
            $update_datas[] = $update_data;
        }
        
        foreach ($update_datas as $row){
            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_bill";
            $log_data["key"] = $row['id'];
            $log_data["master_table_name"] = "biz_" . $rows[$row['id']]['id_type'];
            $log_data["master_key"] = $rows[$row['id']]['id_no'];
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($row);
            log_rcd($log_data);
        }

        $this->biz_bill_model->mbacth_update($update_datas);
        echo json_encode(array('code' => 0, 'msg' => lang("修改成功")));
    }

    public function table_tree($type = 'read')
    {
        $data = array();
        $this->field_edit = array('cost','sell');
        $temp = array();
        foreach ($this->field_edit as $key => $val) {
            if(is_array($val)){
                $cd_temp = array();
                foreach ($val as $v){
                    array_push($cd_temp, array("id" => "1$type." . $v, "text" => lang($v)));
                }
                array_push($temp, array('id' => "1$type." . $key, 'text' => lang($key), 'children' => $cd_temp, 'not_add' => 1));
            }else{
                array_push($temp, array("id" => "1$type." . $val, "text" => lang($val)));
            }
        }
        array_push($data, array("id" => "1$type", "text" => lang("$type"), "children" => $temp));
        echo json_encode($data);
    }

    private function data_role($data, $must_field, $userRole){
        if(!in_array('bill', $userRole)){
            if(!in_array('bill.cost', $userRole)){
                if($data['type'] == 'cost' || $data['type'] == null){
                    foreach ($data as $key => $val) {
                        if(!in_array($key, $must_field)){
                            $data[$key] = '***';
                        }
                    }
                }
            }
            if(!in_array('bill.sell', $userRole)){
                if($data['type'] == 'sell' || $data['type'] == null){
                    foreach ($data as $key => $val) {
                        if(!in_array($key, $must_field)){
                            $data[$key] = '***';
                        }
                    }
                }
            }
        }

        return $data;
    }

    /**
     * 刷新账单里的本地价格
     */
    public function reload_bill_local(){
        //1、刷新的只能是未开票的
        //2、
        $start_time = isset($_POST['start_time']) ? $_POST['start_time'] : '';
        $end_time = isset($_POST['end_time']) ? $_POST['end_time'] : '';
        // if(is_admin()){
        //     $start_time = getValue('start_time');
        //     $end_time = getValue('end_time');
        //     // $id = isset($_GET['id']) ? $_GET['id'] : 0;
        // }
        //invoice_id = 0 " .
            // "and 
        $this->db->select('id,amount, type,currency, IF(id_type = \'shipment\', (select booking_ETD from biz_shipment where id = biz_bill.id_no), (select booking_ETD from biz_consol where id = biz_bill.id_no)) as booking_ETD, bill_ETD');
        
        $bills = $this->biz_bill_model->not_role_get("bill_ETD >= '$start_time' " .
        "and bill_ETD <= '$end_time'");
        if(is_admin()){
            // $bills = $this->biz_bill_model->not_role_get("id = $id");
        }
        if (empty($bills)){
            echo json_encode(array('code' => 1, 'msg' => lang("未获取到数据")));
            return;
        }
        
        $this->load->model('biz_bill_currency_model');
        $update_data = array();
        $jump = 0;
		$currency_locals = array();
		$currency_thiss = array();
		foreach ($bills as $bill){

			$booking_ETD_end = date('Y-m-t', strtotime($bill['bill_ETD']));
			$this_update = array();
			$this_update['exrate'] = get_ex_rate($booking_ETD_end, $bill['currency'], 'sell');
			$this_update['local_amount'] = amount_currency_change($bill['amount'], $bill['bill_ETD'], $bill['currency'], $bill['type']);

			$this_update['id'] = $bill['id'];
			$update_data[] = $this_update;
		}
		$this->biz_bill_model->mbacth_update($update_data);
		echo json_encode(array('code' => 0, 'msg' => 'success', $update_data));
	}
    
    public function batch_commision_flag(){

//        $ids_array = explode(',', $ids);
        $update_data = array();

        $update_field = postValue('update_field', array());
        if(empty($update_field)){
            echo json_encode(array('code' => 1, 'msg' => lang("参数错误,请刷新页面")));
            return;
        }

        $is_search = isset($update_field['is_search']) ? $update_field['is_search'] : 'false';// 获取是否查询
        //如果查询,那么这里带入查询条件获取
        if($is_search === 'false'){
            $ids = isset($update_field['ids']) ? $update_field['ids'] : '';
            $ids = explode(',', $ids);
            $bill_datas = $this->biz_bill_model->not_role_get("id in ('" . join('\',\'', $ids) . "')", 'id', 'desc');
        }else{
            //这里是走查询条件的
            $where = array();

            $sort = "id";
            $order = 'desc';

            $this->all_where_v2($where, $sort);
            //提取出bill_ETD
            $field = postValue('field', array());
            $bill_ETD_search = false;

            //只有bill_etd时间差在2个月内才能使用
            if(!empty($field)){
                $bill_ETD_count = 0;
                $bill_ETD = array();
                foreach ($field['f'] as $k => $f){
                    if($f == 'biz_bill.trans_ATD'){
                        $bill_ETD_count++;
                        $bill_ETD[] = array('s' => $field['s'][$k], 'v' => $field['v'][$k]);
                    }
                }
                if($bill_ETD_count === 2){ // 如果出现了2次,且对应的符号为一个大于一个小于
                    $bill_ETD_s = array_column($bill_ETD, 's');
                    $bill_ETD_v = array_column($bill_ETD, 'v');
                    if(in_array('>=', $bill_ETD_s) && in_array('<=', $bill_ETD_s)){
                        //这2个值不能为空
                        if($bill_ETD_v[0] !== '' && $bill_ETD_v[1] !== ''){
                            //时间之差不能超过2个月
                            if(abs(strtotime($bill_ETD_v[0]) - strtotime($bill_ETD_v[1])) <= 31 * 2 * 24 * 3600){
                                $bill_ETD_search = true;
                            }
                        }
                    }
                }
            }


            if(!$bill_ETD_search){ // 如果为false,那么提示
                echo json_encode(array('code' => 1, 'msg' => lang("ATD必须在设置两个月内")));
                return;
            }
            $where = join(' and ', $where);

            $is_special = '';
            $this->db->select('biz_bill.id,biz_bill.id_type,biz_bill.id_no,biz_bill.commision_flag');
            $this->db->limit(3000);
            $bill_datas = $this->biz_bill_model->join_d_get($where, $sort, $order, $is_special);
        }
        $parent_ids = array();
        foreach ($bill_datas as $row){
            $data = array();
            $data['commision_flag'] = isset($update_field['commision_flag']) ? (int)$update_field['commision_flag'] : 0;
            $data['commision_month'] = isset($update_field['commision_month']) ? $update_field['commision_month'] . '-01' : '0000-00-00';
            
            if($row['commision_flag'] == $data['commision_flag']){
                continue;
            }
            
            if($row['id_type'] == 'consol'){
                //重新修改下子SHIPMENT的计提
                $parent_ids[] = $row['id'];
            }
            
            //日志就不记录了
            // // record the log
            // $log_data = array();
            // $log_data["table_name"] = "biz_bill";
            // $log_data["key"] = $row['id'];
            // $log_data["master_table_name"] = "biz_" . $row['id_type'];
            // $log_data["master_key"] = $row['id_no'];
            // $log_data["action"] = "update";
            // $log_data["value"] = json_encode($data);
            // log_rcd($log_data);

            $data['id'] = $row['id'];
            $update_data[] = $data;
        }
        if(empty($update_data)){
            echo json_encode(array('code' => 1, 'msg' => lang("没有可修改的值")));
            return;
        }
        if(!empty($parent_ids)) {
            $sql = "update biz_bill set commision_flag = '{$data['commision_flag']}',commision_month = '{$data['commision_month']}' where parent_id in ('" . join('\',\'', filter_unique_array($parent_ids)). "');";
            $this->db->query($sql);
        }
        
        $this->biz_bill_model->mbacth_update($update_data);
        echo json_encode(array('code' => 0, 'msg' => lang("保存成功,计提月份:{commision_month}, 本次最多只处理3000条,若过多,请结束后再次点击计提处理", array('commision_month' => $update_field['commision_month']))));
    }

	private function all_where_v2(&$where, &$sort){
		$title_data = get_user_title_sql_data('biz_bill', 'biz_bill_index');//获取相关的配置信息

		//需要处理的特殊排序字段
		$sort = $title_data['sql_field']["biz_bill.{$sort}"];

		//-------这个查询条件想修改成通用的 -------------------------------
		$user_role = isset($_REQUEST['user_role']) ? trim($_REQUEST['user_role']) : '';
		$user = isset($_REQUEST['user']) ? $_REQUEST['user'] : array();
		$fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

		$currency = isset($_REQUEST['currency']) ? trim($_REQUEST['currency']) : '';
		$type = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
		$groups = isset($_REQUEST['group']) ? $_REQUEST['group'] : array();
		$group_role = isset($_REQUEST['group_role']) ? trim($_REQUEST['group_role']) : '';
		$commision_flag = isset($_REQUEST['commision_flag']) ? trim($_REQUEST['commision_flag']) : '';

		Model('biz_bill_invoice_model');
		Model('biz_bill_payment_model');

		$where = array();
		if ($currency != "") $where[] = "biz_bill.currency = '$currency'";
		if ($commision_flag != "") $where[] = "biz_bill.commision_flag = '$commision_flag'";
		if ($type !== "") {
			if($type == 1) {//未开票未销账
				$where[] = "biz_bill.invoice_id = 0 and biz_bill.payment_id = 0";
			}else if ($type == 2) {//已开票未销账
				$where[] = "biz_bill.invoice_id != 0 and biz_bill.payment_id = 0";
			}else if ($type == 3) {//未销账
				$where[] = "biz_bill.payment_id = 0";
			}else if ($type == 4) {//未开票已销账
				$where[] = "biz_bill.invoice_id = 0 and biz_bill.payment_id != 0";
			}else if ($type == 5) {//待审核
				// $this->db->select('id');
				// $no_lock_pm = $this->biz_bill_payment_model->get("lock_date = '0000-00-00'");
				// if(empty($no_lock_pm)) $no_lock_pm[] = -1;
				// $no_lock_pm_ids = array_column($no_lock_pm, 'id');
				// $where[] = "payment_id in (" . join(',', $no_lock_pm_ids) . ")";
				$where[] = "biz_bill.payment_id in (select id from biz_bill_payment where biz_bill_payment.lock_date = '0000-00-00')";
			}else if ($type == 6) {//审核完成
				// $this->db->select('id');
				// $no_lock_pm = $this->biz_bill_payment_model->get("lock_date != '0000-00-00'");
				// if(empty($no_lock_pm)) $no_lock_pm[] = -1;
				// $no_lock_pm_ids = array_column($no_lock_pm, 'id');
				// $where[] = "payment_id in (" . join(',', $no_lock_pm_ids) . ")";
				$where[] = "biz_bill.payment_id in (select id from biz_bill_payment where biz_bill_payment.lock_date != '0000-00-00')";
			}
		}
		$user = join("','", array_filter($user));
		if ($user_role != "" && !empty($user))
		{
			$where[] = "((biz_bill.id_no in (select id_no from biz_duty where biz_duty.biz_table = 'biz_shipment' and biz_duty." . $user_role . "_id in ('$user')) and biz_bill.id_type = 'shipment') " .
				"or (biz_bill.id_no in (select id_no from biz_duty where biz_duty.biz_table = 'biz_consol' and biz_duty." . $user_role . "_id in ('$user')) and biz_bill.id_type = 'consol'))";
		}
		$groups = join("','", array_filter($groups));
		if($group_role != "" && !empty($groups)){
			$where[] = "((biz_bill.id_no in (select id_no from biz_duty where biz_duty.biz_table = 'biz_shipment' and biz_duty." . $group_role . "_group in ('$groups')) and biz_bill.id_type = 'shipment') " .
				"or (biz_bill.id_no in (select id_no from biz_duty where biz_duty.biz_table = 'biz_consol' and biz_duty." . $group_role . "_group in ('$groups')) and biz_bill.id_type = 'consol'))";
		}
		if(!empty($fields)){
			foreach ($fields['f'] as $key => $field){
				$f = trim($fields['f'][$key]);
				$s = $fields['s'][$key];
				$v = trim($fields['v'][$key]);
				$v_arr = array_unique(explode("\r\n", $v));
				if($f == '') continue;
				$v_arr = array_filter($v_arr, function ($r){
					$r = trim($r);
					if($r === "") return false;
					else return true;
				});

				//TODO 如果是datetime字段,=默认>=且<=
				$date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
				//datebox的用等于时代表搜索当天的
				if(in_array($f, $date_field) && $s == '='){
					if(!isset($title_data['sql_field'][$f])) continue;
					$f = $title_data['sql_field'][$f];
					if($v != ''){
						$where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
					}
					continue;
				}
				//datebox的用等于时代表搜索小于等于当天最晚的时间
				if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
					if(!isset($title_data['sql_field'][$f])) continue;
					$f = $title_data['sql_field'][$f];
					if($v != ''){
						$where[] = "{$f} $s '$v 23:59:59'";
					}
					continue;
				}
				//这里 job_no 和mbl 单独处理
				$f_arr = explode('.', $f);
				if($f_arr[0] == 'consol'){
					if(!empty($v_arr)){
						if($f_arr[1] == 'creditor'){
							$this_where = array();
							foreach ($v_arr as $vv){
								$this_where[] = search_like('company_search', $s, match_chinese($vv));
							}
							$this_where = join(' or ', $this_where);
							$clients = Model('biz_client_model')->no_role_get($this_where);
							$client_codes = join("','", array_column($clients, 'client_code'));
							$this_where = "creditor in ('$client_codes')";
						}else{
							$this_where = array();
							foreach ($v_arr as $vv){
								if($f_arr[1] == 'trans_ATD'){
									if($s == '>=') $vv = $vv . ' 00:00:00';
									if($s == '<=') $vv = $vv . ' 23:59:59';
								}
								$this_where[] = search_like($f_arr[1], $s, $vv);
							}
							$this_where = join(' or ', $this_where);
						}

						$this->db->select('id');
						$consols = Model('biz_consol_model')->no_role_get($this_where);
						$consols_ids = join(',', array_column($consols, 'id'));
						if(empty($consols_ids)){
							$consols_ids = 0;
							$shipments_ids = 0;
						}else{
							$this->db->select('id');
							$shipments = Model('biz_shipment_model')->no_role_get("consol_id in ($consols_ids)");
							$shipments_ids = join(',', array_column($shipments, 'id'));
							if(empty($shipments_ids)){
								$shipments_ids = 0;
							}
						}
						$where[] = "((biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)) or (biz_bill.id_type = 'consol' and biz_bill.id_no in ($consols_ids)))";
					}
					continue;
				}else if($f_arr[0] == 'shipment_by_consol'){
					$biz_shipment_model = Model('biz_shipment_model');
					$biz_consol_model = Model('biz_consol_model');

					if(!empty($v_arr)){
						$this_where = array();
						foreach ($v_arr as $vv){
							$this_where[] = search_like($f_arr[1], $s, $vv);
						}
						$this_where = join(' or ', $this_where);
						$this->db->select('id,consol_id');
						$shipments = $biz_shipment_model->no_role_get($this_where);
						$shipments_ids = join(',', array_column($shipments, 'id'));
						if(empty($shipments_ids)){
							$shipments_ids = 0;
							$consols_ids = 0;
						}else{
							$consol_ids = join(',', array_filter(array_column($shipments, 'consol_id')));
							$this->db->select('id');
							if(empty($consol_ids))$consol_ids = 0;
							$consols = $biz_consol_model->no_role_get("id in ($consol_ids)");
							$consols_ids = join(',', array_column($consols, 'id'));
							if(empty($consols_ids))$consols_ids = 0;
						}
						$where[] = "((biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)) or (biz_bill.id_type = 'consol' and biz_bill.id_no in ($consols_ids)))";
					}
				}else if($f_arr[0] == 'shipment'){
					$biz_shipment_model = Model('biz_shipment_model');
					if(!empty($v_arr)){
						$this->db->select('id');
						$this_where = array();
						foreach ($v_arr as $vv){
							$this_where[] = search_like($f_arr[1], $s, $vv);
						}
						$this_where = join(' or ', $this_where);
						// $shipments = $biz_shipment_model->no_role_get($this_where);
						// $shipments_ids = join(',', array_column($shipments, 'id'));
						// if(empty($shipments_ids))$shipments_ids = 0;
						// $where[] = "biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)";
						$shipment_table = Model('biz_shipment_model')->getTableName();
						$where[] = "biz_bill.id_type = 'shipment' and biz_bill.id_no in (select id from {$shipment_table} where $this_where)";
					}
				}

				//把f转化为对应的sql字段
				//不是查询字段直接排除
				$title_f = $f;
				if(!isset($title_data['sql_field'][$title_f])) continue;
				$f = $title_data['sql_field'][$title_f];

				//这里缺了2个
				$this_where = array();
				foreach ($v_arr as $this_v){
					if($this_v == '-')  continue;
					$this_where[] = search_like($f, $s, $this_v);
				}
				if(!empty($this_where)) {
					//如果进行了查询拼接,这里将join条件填充好
					$this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
					if(!empty($this_join)) {
						$this_join0 = explode(' ', $this_join[0]);
						$title_data['sql_join'][end($this_join0)] = $this_join;
					}
					$where[] = "(" . join(' or ', $this_where) . ")";
				}
			}
		}
		//获取可查看组权限
		$bsc_user_role_model = Model('bsc_user_role_model');
		// $where[] = $bsc_user_role_model->get_bill_role();
		//获取cost和sell查看权限
		$user_role = $bsc_user_role_model->get_user_role(array('read_text'));
		$userRoleRead = explode(',', $user_role['read_text']);//bill.cost,bill.sell
		$type_where = array();
		if(in_array('bill', $userRoleRead) || in_array('bill.cost', $userRoleRead)){
			$type_where[] = "biz_bill.type = 'cost'";
		}
		if(in_array('bill', $userRoleRead) || in_array('bill.sell', $userRoleRead)){
			$type_where[] = "biz_bill.type = 'sell'";
		}
		if(sizeof($type_where) > 1) $type_where = array();
		if(!empty($type_where))$where[] = '(' . join(' or ', $type_where) . ')';

		//这里拼接join数据
		foreach ($title_data['sql_join'] as $j){
			$this->db->join($j[0], $j[1], $j[2], false);
		}
	}
    
    /**
     * 无规则修改数据
     */
    public function batch_update_data(){
        $ids = $_REQUEST["ids"];
//        $ids_array = explode(',', $ids);
        $update_data = array();
        $ids = explode(',', $ids);
        $bill_datas = $this->biz_bill_model->not_role_get("id in ('" . join('\',\'', $ids) . "')", 'id', 'desc');
        foreach ($bill_datas as $row){
            $old_row = $row;

            $field = $this->field_edit;

            $data = array();
            foreach ($field as $item) {
                $temp = isset($_POST[$item]) ? $_POST[$item] : '';
                if ($temp != "") {
                    if (array_key_exists($item, $old_row) && $old_row[$item] != $temp)
                        $data[$item] = is_array($temp) ? $temp : trim($temp);
                }
            }

            if(!empty($data)){
                $split_mode = $old_row['split_mode'];
                if(isset($data['split_mode'])) $split_mode = $data['split_mode'];
                bill_child($old_row['id'], $split_mode);
                
                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_bill";
                $log_data["key"] = $row['id'];
                $log_data["master_table_name"] = "biz_" . $row['id_type'];
                $log_data["master_key"] = $row['id_no'];
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($data);
                log_rcd($log_data);

                $data['id'] = $row['id'];
                $update_data[] = $data;
            }

        }
        if(empty($update_data)){
            echo json_encode(array('code' => 1, 'msg' => lang("没有可修改的值")));
            return;
        }
        $this->biz_bill_model->mbacth_update($update_data);
        echo json_encode(array('code' => 0, 'msg' => lang("保存成功")));
    }

    public function reload_child_bill(){
        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $split_mode = isset($_POST['split_mode']) ? $_POST['split_mode'] : 0;
        $diy_datas = isset($_POST['diy_datas']) ? $_POST['diy_datas'] : array();
        $box_size = isset($_POST['box_size']) ? $_POST['box_size'] : '';
        $this->biz_bill_model->update($id, array('split_mode' => $split_mode, 'box_size' => $box_size));
        $result = bill_child($id, $split_mode, $diy_datas);
        $return = array('code' => 1 , 'msg' => lang("参数错误"));
        if($result === 1){
            $return['msg'] = lang("账单不存在");
        }else if($result === 2){
            $return['msg'] = lang("已确认或已开票无法再次同步");
        }else if($result === 3){
            $return['msg'] = lang("不存在可同步账单或箱信息不完整");
        }else if($result === 4){
            $return['msg'] = lang("已拆分,同步失败");
        }else if($result === 5){
            $return['msg'] = lang("已销账,同步失败");
        }else{
            $return['code'] = 0;
            $return['msg'] = lang("同步成功");
        }
        echo json_encode($return);
    }
    
    public function get_shipments_bill_percent($consol_id = 0, $split_mode = 0, $bill_id = 0){
        if($consol_id == 0){
            echo json_encode(array('code' => 0, 'msg' => lang("获取成功"), 'data' => array()));
            return;
        }
        $box_size = getValue('box_size', '');
        $bill = $this->biz_bill_model->get_one('id', $bill_id);
        if(empty($bill)) return jsonEcho(array('code' => 1, 'msg' => lang('账单不存在'), 'data' => array()));
        //2024-03-08 如果状态是4 per container 时
        if($split_mode == 4){
            $consol = Model('biz_consol_model')->get_where_one("id = {$bill['id_no']}");
            $msg = array();
            //1是consol必须是FCL，
            if($consol['trans_mode'] !== 'FCL'){
                $msg[] = lang("请将 consol 修改为 FCL");
            }
            $shipments = Model('biz_shipment_model')->get_shipment_by_consol($consol['id']);
            $all_shipment_box_info = array();
            foreach ($shipments as $shipment){
                //2是所有shipment也都是FCL，任意1个非FCL都不行
                if($shipment['trans_mode'] !== 'FCL'){
                    $msg[] = lang("请将 shipment({job_no}) 修改为 FCL", array('job_no' => $shipment['job_no']));
                }

                $all_shipment_box_info = merge_box_info($all_shipment_box_info, json_decode($shipment['box_info'], true),'+');
            }

            //3是consol的箱型箱量需要正好等于shipment箱型箱量的总和，以订舱页面的箱型箱量为准（因为container页面输入往往滞后）。
            $box_info = merge_box_info(json_decode($consol['box_info'], true), $all_shipment_box_info, '-');

            if(!empty($box_info)) {
                $msg[] = lang("请将 consol 箱型箱量 修改为全部 shipment 箱型箱量 之和");
            }
            if(!empty($msg)){
                return jsonEcho(array('code' => 1, 'msg' => join(',<br/>', $msg), 'data' => array(),$box_info));
            }

            //第四点在页面上 和 get_shipment_consol_percent方法里
            //4是输入billing的时候，必须指明是20GP还是40GP（billing得加个字段），并且数量必须和箱型箱量严格对应，不对应不让分配。

            $bill['box_size'] = $box_size;
        }



        if($split_mode != 3){
            $shipments_consol_percent_info = get_shipment_consol_percent($consol_id, $split_mode, $bill);
        }else{
            $this->db->select('id_no, number, amount, local_amount');
            $child_bills = array_column($this->biz_bill_model->get("parent_id = {$bill['id']} and id_type = 'shipment'", 'id', 'desc', $bill['is_special'], false), null, 'id_no');
            if(empty($child_bills)){//没有默认按票
                $shipments_consol_percent_info = get_shipment_consol_percent($consol_id, 0, $bill);
            }else{
                $shipments_consol_percent_info = array();
                foreach ($child_bills as $child_bill){
                    $shipments_consol_percent_info[] = array(
                        'id' => $child_bill['id_no'],
                        'number' => $child_bill['number'],
                        'amount' => $child_bill['amount'],
                        'local_amount' => $child_bill['local_amount'],
                    );
                }
            }
        }
        $shipments_consol_percent_info = array_column($shipments_consol_percent_info, null, 'id');
        $shipment_ids = array_column($shipments_consol_percent_info, 'id');
        if(empty($shipment_ids)){
            $shipments_consol_percent_info = array();
        }else{
            $biz_shipment_model = Model('biz_shipment_model');
            $this->db->select('id, job_no');
            $shipments = $biz_shipment_model->no_role_get("id in (" . join(',', $shipment_ids) . ")");
            foreach ($shipments as $shipment){
                $shipments_consol_percent_info[$shipment['id']]['job_no'] = $shipment['job_no'];
            }
        }
        echo json_encode(array('code' => 0, 'msg' => lang("获取成功"), 'data' => $shipments_consol_percent_info));
    }
    
    /**
     * 批量确认自己的订单
     */
    public function batch_confirm_self_bill(){
        $type = isset($_POST['type']) ? check_param($_POST['type']) : '';
        $id_type = isset($_POST['id_type']) ? check_param($_POST['id_type']) : '';
        $id_no = isset($_POST['id_no']) ? check_param($_POST['id_no']) : '';
        $is_special = isset($_POST['is_special']) ? check_param($_POST['is_special']) : '';
        $confirm_date = isset($_POST['confirm_date']) && $_POST['confirm_date'] != '' ? $_POST['confirm_date'] : '0000-00-00';
        // $account_no = isset($_POST['account_no']) && $_POST['account_no'] != '' ? $_POST['account_no'] : null;
        
        $type_where = '';
        if($type !== ''){
            $type_where = " and type = '$type'";
        }
        //确认的时候,是全部批量,还是只确认未确认的
        $this->db->select('id,created_by,confirm_date,invoice_id');
        $rows = array_column($this->biz_bill_model->get("id_type = '$id_type' and id_no = $id_no and confirm_date = '0000-00-00'" . $type_where, 'id', 'desc', $is_special), null, 'id');

        //检测付款方是否为同一个,如果不是,检测币种,税率是否相同

        //只有创建人自己可以解锁
        $update_datas = array();
        foreach ($rows as $row){
            //已开票无法修改
            if($row['invoice_id'] != 0){
                continue;
            }
            //判断是否是创建人,不是创建人无法修改
            if($row['created_by'] != $this->session->userdata('id') && $this->session->userdata('level') != 9999){
                continue;
            }
            $update_data = array();
            $update_data['id'] = $row['id'];
            $update_data['confirm_date'] = $confirm_date;
            // $update_data['account_no'] = $account_no;
            $update_datas[] = $update_data;
        }
        if(empty($rows) || empty($update_datas)){
            echo json_encode(array('code' => 1, 'msg' => lang("没有可确认的账单")));
            return;
        }

        foreach ($update_datas as $row){
            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_bill";
            $log_data["key"] = $row['id'];
            $log_data["master_table_name"] = "biz_" . $id_type;
            $log_data["master_key"] = $id_no;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($row);
            log_rcd($log_data);
        }
        $this->biz_bill_model->mbacth_update($update_datas);
        echo json_encode(array('code' => 0, 'msg' => lang("修改成功")));
    }
    
    public function auto_remark(){
        $id_type = isset($_GET['id_type']) ? $_GET['id_type'] : '';
        $id_no = isset($_GET['id_no']) ? $_GET['id_no'] : 0;

        $remark = '';
        if($id_type == 'shipment'){
            Model('biz_shipment_model');
            $shipment = $this->biz_shipment_model->get_by_id($id_no);
            if(!empty($shipment)){
                //ETD:{booking_ETD} 客户编号:{shipper_ref} JOBNO:{job_no}
                $remark .= lang("ETD:{booking_ETD} JOBNO:{job_no}", array('booking_ETD' => $shipment['booking_ETD'], 'shipper_ref' => $shipment['shipper_ref'], 'job_no' => $shipment['job_no']));

                if($shipment['consol_id'] != 0){
                    Model('biz_consol_model');
                    $consol = $this->biz_consol_model->get_by_id($shipment['consol_id']);
                    $carrier_ref = $shipment['cus_no'];
                    //如果关单号没有,才取consol的MBL
                    if($carrier_ref == '') $carrier_ref = $consol['carrier_ref'];

                    $remark = lang("MV:{vessel} V.{voyage} B/L:{carrier_ref} ", array('vessel' => $consol['vessel'], 'voyage' => $consol['voyage'], 'carrier_ref' => $carrier_ref)) . $remark;
                }

            }
        }
        $data['remark'] = $remark;
        Model('biz_bill_currency_model');
        $time_end = date('Y-m-t', time());
        $data['invoice_ex_rate'] = get_ex_rate($time_end, 'USD', 'invoice');
        
        echo json_encode(array('code' => 0, 'msg' => lang("获取成功"), 'data' => $data));
    }

    private function all_where(&$where, &$sort){
        $title_data = get_user_title_sql_data('biz_bill', 'biz_bill_index');//获取相关的配置信息

        //需要处理的特殊排序字段
        $sort = $title_data['sql_field']["biz_bill.{$sort}"];

        //-------这个查询条件想修改成通用的 -------------------------------
        $user_role = isset($_REQUEST['user_role']) ? trim($_REQUEST['user_role']) : '';
        $user = isset($_REQUEST['user']) ? $_REQUEST['user'] : array();
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

        $currency = isset($_REQUEST['currency']) ? trim($_REQUEST['currency']) : '';
        $type = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
        $groups = isset($_REQUEST['group']) ? $_REQUEST['group'] : array();
        $group_role = isset($_REQUEST['group_role']) ? trim($_REQUEST['group_role']) : '';
        $commision_flag = isset($_REQUEST['commision_flag']) ? trim($_REQUEST['commision_flag']) : '';
        
        Model('biz_bill_invoice_model');
        Model('biz_bill_payment_model');

        $where = array();
        if ($currency != "") $where[] = "biz_bill.currency = '$currency'";
        if ($commision_flag != "") $where[] = "biz_bill.commision_flag = '$commision_flag'";
        if ($type !== "") {
            if($type == 1) {//未开票未销账
                $where[] = "biz_bill.invoice_id = 0 and biz_bill.payment_id = 0";
            }else if ($type == 2) {//已开票未销账
                $where[] = "biz_bill.invoice_id != 0 and biz_bill.payment_id = 0";
            }else if ($type == 3) {//未销账
                $where[] = "biz_bill.payment_id = 0";
            }else if ($type == 4) {//未开票已销账
                $where[] = "biz_bill.invoice_id = 0 and biz_bill.payment_id != 0";
            }else if ($type == 5) {//待审核
                // $this->db->select('id');
                // $no_lock_pm = $this->biz_bill_payment_model->get("lock_date = '0000-00-00'");
                // if(empty($no_lock_pm)) $no_lock_pm[] = -1;
                // $no_lock_pm_ids = array_column($no_lock_pm, 'id');
                // $where[] = "payment_id in (" . join(',', $no_lock_pm_ids) . ")";
                $where[] = "biz_bill.payment_id in (select id from biz_bill_payment where biz_bill_payment.lock_date = '0000-00-00')";
            }else if ($type == 6) {//审核完成
                // $this->db->select('id');
                // $no_lock_pm = $this->biz_bill_payment_model->get("lock_date != '0000-00-00'");
                // if(empty($no_lock_pm)) $no_lock_pm[] = -1;
                // $no_lock_pm_ids = array_column($no_lock_pm, 'id');
                // $where[] = "payment_id in (" . join(',', $no_lock_pm_ids) . ")";
                $where[] = "biz_bill.payment_id in (select id from biz_bill_payment where biz_bill_payment.lock_date != '0000-00-00')";
            }else if ($type == 7) {//已核销
                $where[] = "biz_bill.payment_id = 0";
            }
        }
        $user = join("','", array_filter($user));
        if ($user_role != "" && !empty($user))
        {
            $where[] = "((biz_bill.id_no in (select id_no from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and user_role = '$user_role' and biz_duty_new.user_id in ('$user')) and biz_bill.id_type = 'shipment') " .
                "or (biz_bill.id_no in (select id_no from biz_duty_new where biz_duty_new.id_type = 'biz_consol' and user_role = '$user_role' and biz_duty_new.user_id in ('$user')) and biz_bill.id_type = 'consol'))";
        }
        $groups = join("','", array_filter($groups));
        if($group_role != "" && !empty($groups)){
            $where[] = "((biz_bill.id_no in (select id_no from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' user_role = '$group_role' and biz_duty_new.user_group in ('$groups')) and biz_bill.id_type = 'shipment') " .
                "or (biz_bill.id_no in (select id_no from biz_duty_new where biz_duty_new.id_type = 'biz_consol' user_role = '$group_role' and biz_duty_new.user_group in ('$groups')) and biz_bill.id_type = 'consol'))";
        }
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                $v_arr = array_unique(explode("\r\n", $v));
                if($f == '') continue;
                $v_arr = array_filter($v_arr, function ($r){
                    $r = trim($r);
                    if($r === "") return false;
                    else return true;
                });

                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }
                //这里 job_no 和mbl 单独处理
                $f_arr = explode('.', $f);
                if($f_arr[0] == 'consol'){
                    if(!empty($v_arr)){
                        if($f_arr[1] == 'creditor'){
                            $this_where = array();
                            foreach ($v_arr as $vv){
                                $this_where[] = search_like('company_search', $s, match_chinese($vv));
                            }
                            $this_where = join(' or ', $this_where);
                            $clients = Model('biz_client_model')->no_role_get($this_where);
                            $client_codes = join("','", array_column($clients, 'client_code'));
                            $this_where = "creditor in ('$client_codes')";
                        }else{
                            $this_where = array();
                            foreach ($v_arr as $vv){
                                if($f_arr[1] == 'trans_ATD'){
                                    if($s == '>=') $vv = $vv . ' 00:00:00';
                                    if($s == '<=') $vv = $vv . ' 23:59:59';
                                }
                                $this_where[] = search_like($f_arr[1], $s, $vv);
                            }
                            $this_where = join(' or ', $this_where);
                        }

                        $this->db->select('id');
                        $consols = Model('biz_consol_model')->no_role_get($this_where);
                        $consols_ids = join(',', array_column($consols, 'id'));
                        if(empty($consols_ids)){
                            $consols_ids = 0;
                            $shipments_ids = 0;
                        }else{
                            $this->db->select('id');
                            $shipments = Model('biz_shipment_model')->no_role_get("consol_id in ($consols_ids)");
                            $shipments_ids = join(',', array_column($shipments, 'id'));
                            if(empty($shipments_ids)){
                                $shipments_ids = 0;
                            }
                        }
                        $where[] = "((biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)) or (biz_bill.id_type = 'consol' and biz_bill.id_no in ($consols_ids)))";
                    }
                    continue;
                }else if($f_arr[0] == 'shipment_by_consol'){
                    $biz_shipment_model = Model('biz_shipment_model');
                    $biz_consol_model = Model('biz_consol_model');

                    if(!empty($v_arr)){
                        $this_where = array();
                        foreach ($v_arr as $vv){
                            $this_where[] = search_like($f_arr[1], $s, $vv);
                        }
                        $this_where = join(' or ', $this_where);
                        $this->db->select('id,consol_id');
                        $shipments = $biz_shipment_model->no_role_get($this_where);
                        $shipments_ids = join(',', array_column($shipments, 'id'));
                        if(empty($shipments_ids)){
                            $shipments_ids = 0;
                            $consols_ids = 0;
                        }else{
                            $consol_ids = join(',', array_filter(array_column($shipments, 'consol_id')));
                            $this->db->select('id');
                            if(empty($consol_ids))$consol_ids = 0;
                            $consols = $biz_consol_model->no_role_get("id in ($consol_ids)");
                            $consols_ids = join(',', array_column($consols, 'id'));
                            if(empty($consols_ids))$consols_ids = 0;
                        }
                        $where[] = "((biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)) or (biz_bill.id_type = 'consol' and biz_bill.id_no in ($consols_ids)))";
                    }
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];

                //这里缺了2个
                $this_where = array();
                foreach ($v_arr as $this_v){
                    if($this_v == '-') $this_v = '';
                    $this_where[] = search_like($f, $s, $this_v);
                }
                if(!empty($this_where)) {
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) $title_data['sql_join'][$title_data['base_data'][$title_f]['sql_join']] = $this_join;
                    $where[] = "(" . join(' or ', $this_where) . ")";
                }
            }
        }
        //获取可查看组权限
        $bsc_user_role_model = Model('bsc_user_role_model');
        // $where[] = $bsc_user_role_model->get_bill_role();
        //获取cost和sell查看权限
        $user_role = $bsc_user_role_model->get_user_role(array('read_text'));
        $userRoleRead = explode(',', $user_role['read_text']);//bill.cost,bill.sell
        $type_where = array();
        if(in_array('bill', $userRoleRead) || in_array('bill.cost', $userRoleRead)){
            $type_where[] = "biz_bill.type = 'cost'";
        }
        if(in_array('bill', $userRoleRead) || in_array('bill.sell', $userRoleRead)){
            $type_where[] = "biz_bill.type = 'sell'";
        }
        if(sizeof($type_where) > 1) $type_where = array();
        if(!empty($type_where))$where[] = '(' . join(' or ', $type_where) . ')';

        //这里拼接join数据
        foreach ($title_data['sql_join'] as $j){
            $this->db->join($j[0], $j[1], $j[2], false);
        }
    }
    
    private function bill_update_queue($id, $old_data, $new_data, $action = 'update'){
        return false;
        return bill_update_queue($id, $old_data, $new_data, $action);
    }
    
    public function child_bill(){
        $id = getValue('id', '');
        if(empty($id)) exit(lang("参数错误"));
        $data = array();

        $where = array();

        $where[] = "parent_id = $id";
        $where[] = "id_type = 'shipment'";

        $where_str = join(' and ', $where);

        $this->db->select('id,id_type,id_no,currency,unit_price,number,amount,vat,exrate' .
            ',(select company_name from biz_client where biz_client.client_code = biz_bill.client_code) as client_code_name' .
            ',(select job_no from biz_shipment where biz_shipment.id = biz_bill.id_no) as job_no');
        $data['rows'] = $this->biz_bill_model->get($where_str, 'id', 'desc', '', '');

        //将总金额提前整理
        $data['sum_amount'] = array_sum(array_column($data['rows'], 'amount'));
        //原总金额
        $this->db->select('id,amount,exrate');
        $data['parent_row'] = $this->biz_bill_model->get_one('id', $id);

        $this->load->view('head');
        $this->load->view('biz/bill/child_view', $data);
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_data = $this->biz_bill_model->get_one('id', $id);
		if($old_data['parent_id'] != 0){
			echo json_encode(array('code' => 1, 'msg' => lang("Please delete it in consol billing page")));
			return;
		}
		if($old_data['confirm_date'] != '0000-00-00') return jsonEcho(array('code' => 1, 'msg' => lang("Please cancel confirm_date")));
		if(!empty($old_data['account_no'])) return jsonEcho(array('code' => 1, 'msg' => lang("Please cancel account_no in finance page")));
		if($old_data['invoice_id'] != 0){
			echo json_encode(array('code' => 1, 'msg' => lang("Please cancel invoice no")));
			return;
		}
		if($old_data['payment_id'] != 0){
			echo json_encode(array('code' => 1, 'msg' => lang("Please cancel write off")));
			return;
		}
		if($old_data['split_id'] != 0){
			echo json_encode(array('code' => 1, 'msg' => lang("Has Split,not support to delete")));
			return;
		}
		if($old_data['commision_flag'] == 1){
			echo json_encode(array('code' => 1, 'msg' => lang("has commision_flag, not support to delete")));
			return;
		}

        $this->bill_update_queue($id, $old_data, array(), 'delete');

        $this->biz_bill_model->mdelete($id);

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_bill";
        $log_data["key"] = $id;
        $log_data["master_table_name"] = "biz_" . $old_data['id_type'];
        $log_data["master_key"] = $old_data['id_no'];
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_data);
        log_rcd($log_data);

        echo json_encode(array('code' => 0, 'msg' => lang("删除成功")));
    }

    public function delete_datas(){
        $ids = $_REQUEST['ids'];
        $ids_array = array_filter(explode(',', $ids));
        if(empty($ids_array)){
            echo json_encode(array('code' => 1, 'msg' => lang("参数错误")));
            return;
        }

        foreach ($ids_array as $id){
			$old_data = $this->biz_bill_model->get_one('id', $id);
			if(empty($old_data)){
				continue;
			}
			if($old_data['parent_id'] != 0){
				continue;
			}
			if($old_data['confirm_date'] != '0000-00-00'){
				echo json_encode(array('code' => 0, 'msg' => 'Please cancel the  confirm date.'));
				return;
			}
			if(!empty($old_data['account_no'])){
				echo json_encode(array('code' => 0, 'msg' => 'Please cancel the account_no in finance menu'));
				return;
			}
			if($old_data['invoice_id'] != 0){
				echo json_encode(array('code' => 0, 'msg' => 'Please cancel the invoice'));
				return;
			}
			if($old_data['payment_id'] != 0){
				echo json_encode(array('code' => 0, 'msg' => 'please cancel the write off'));
				return;
			}
			if($old_data['split_id'] != 0){
				echo json_encode(array('code' => 1, 'msg' => 'Has been split, so delete failure'));
				return;
			}
			if($old_data['commision_flag'] == 1){
				echo json_encode(array('code' => 1, 'msg' => 'Has commision flag, not be delete'));
				return;
			}

            if($old_data['invoice_id'] != 0){
                continue;
            }

            $this->bill_update_queue($id, $old_data, array(), 'delete');

            $this->biz_bill_model->mdelete($id);

            //save the operation log
            $log_data = array();
            $log_data["table_name"] = "biz_bill";
            $log_data["key"] = $id;
            $log_data["master_table_name"] = "biz_" . $old_data['id_type'];
            $log_data["master_key"] = $old_data['id_no'];
            $log_data["action"] = "delete";
            $log_data["value"] = json_encode($old_data);
            log_rcd($log_data);
        }


        echo json_encode(array('code' => 0, 'msg' => lang('删除成功')));
    }
    
    public function aa($currency = LOCAL_CURRENCY, $type = 'sell', $local_currency = LOCAL_CURRENCY)
    {
        $date = "2023-04-10";
        $type_arr = array('sell', 'cost', 'invoice');
        //其他的直接
        if(!in_array($type, $type_arr)) return 0;
        if($currency == $local_currency) return 1;

        //2023-02-02 由于表结构的更新 改为直接获取了
        $row = Model('biz_bill_currency_model')->get_where_one("currency_from in ('{$currency}', '{$local_currency}') and currency_to in ( '{$local_currency}', '{$currency}') and start_time <= '{$date}'");

        //第一次查询查一遍, 直接取最新的
        if(empty($row)){
            $row = Model('biz_bill_currency_model')->get_where_one("currency_from in ('{$currency}', '{$local_currency}') and currency_to in ( '{$local_currency}', '{$currency}')");
        }
        
        //如果没查到,汇率为0
        if(empty($row)) return 0;
        
        //2023-04-10 currency_from 是$currency时, 直接返回, to 是 $currency 时, 得用1 / 汇率
        $this_rate = $row["{$type}_value"];
        if($row['currency_to'] == $currency){
            $this_rate = round(1 / $this_rate, 4);
        }
        
        echo $this_rate;
    }
    
    
    public function extra_fee(){
        $data['f'] = array(
            array('id', 'id'),
            array('收入/支出', 'in_out'),
            array('费用名称', 'charge_code'),
            array('费用日期', 'charge_date'),
            array('币种', 'currency'),
            array('金额', 'amount'),
            array('收付对象', 'client_name'),
            array('核销日期', 'write_off_date'),
            array('创建人', 'created_by'),
            array('创建时间', 'created_time'),
            array('更新人', 'updated_by'),
            array('更新时间', 'updated_time'),
        );
        $this->load->view('head');
        $this->load->view('/biz/bill/extra_fee/index',$data);
    }

    public function get_extra_fee_data(){
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $offset = ($page - 1) * $rows;
        $where = array();
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($v1 != "") $where[] = search_like($f1, $s1, $v1);
        if ($v2 != "") $where[] = search_like($f2, $s2, $v2);
        if ($v3 != "") $where[] = search_like($f3, $s3, $v3);
        $where = join(' and ', $where);
        $result = [];
        if(!empty($where)){
            $result['total'] = $this->db->where($where)->count_all_results('biz_bill_extra_fee');
            $data = $this->db->where($where)->limit($rows, $offset)->order_by($sort,$order)->get('biz_bill_extra_fee')->result_array();
        }else{
            $data = $this->db->limit($rows, $offset)->order_by($sort,$order)->get('biz_bill_extra_fee')->result_array();
            $result['total'] = $this->db->count_all_results('biz_bill_extra_fee');
        }
        if(empty($data)){
            return jsonEcho(['rows'=>0,'total'=>0]);
        }
        $rows = [];
        foreach($data as $k=>$v){
            $v['created_by_name'] = getUserField($v['created_by'],'name');
            $v['updated_by_name'] = getUserField($v['updated_by'],'name');
            $charge = $this->db->where('charge_code',$v['charge_code'])->get('biz_charge')->row_array();
            if(!empty($charge)){
                $v['charge_name'] = $charge['charge_name_cn'];
            }else{
                $v['charge_name'] = '';
            }
            array_push($rows,$v);
        }
        $result['rows'] = $rows;
        return jsonEcho($result);
    }

    public function extra_fee_add(){
        $field = ['in_out','charge_code','charge_date','currency','amount','client_name','write_off_date'];
        $param = [];
        foreach ($field as $v){
            if(isset($_POST[$v]) && !empty($_POST[$v])){
                $param[$v] = $_POST[$v];
            }else{
                if($v != 'write_off_date'){
                    return jsonEcho(['code'=>0,'msg'=>'please input mandatory items']);
                }
            }
        }

        $param['updated_by'] = $param['created_by'] = get_session('id');
        $param['updated_time'] = $param['created_time'] = date('Y-m-d H:i:s');
        $param['exchage_rate'] = get_ex_rate($param['charge_date'], $param['currency'], 'cost');;
        $param['local_amount'] = amount_currency_change($param['amount'], $param['charge_date'], $param['currency'], 'cost');
        $this->db->insert('biz_bill_extra_fee',$param);
        $id = $this->db->insert_id();
        if($id){
            $log_data = array();
            $log_data["table_name"] = "biz_bill_extra_fee";
            $log_data["key"] = $id;
            $log_data["action"] = "insert";
            $log_data["value"] = json_encode($param);
            log_rcd($log_data);
			return jsonEcho(['code'=>1,'msg'=>'add success']);
		}
		return jsonEcho(['code'=>0,'msg'=>'failure']);
    }

    public function extra_fee_edit(){
        $field = ['id','in_out','charge_code','charge_date','currency','amount','client_name','write_off_date'];
        $param = [];
        foreach ($field as $v){
            if(isset($_POST[$v]) && !empty($_POST[$v])){
                $param[$v] = $_POST[$v];
            }else{
                if($v != 'write_off_date'){
                    return jsonEcho(['code'=>0,'msg'=>'please input mandatory items']);
                }
            }
        }

        $param['updated_by'] = get_session('id');
        $param['updated_time'] = date('Y-m-d H:i:s');
        $param['exchage_rate'] = get_ex_rate($param['charge_date'], $param['currency'], 'cost');;
        $param['local_amount'] = amount_currency_change($param['amount'], $param['charge_date'], $param['currency'], 'cost');
        $this->db->where('id',$param['id'])->update('biz_bill_extra_fee',$param);

        $log_data = array();
        $log_data["table_name"] = "biz_bill_extra_fee";
        $log_data["key"] = $param['id'];
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($param);
        log_rcd($log_data);
        return jsonEcho(['code'=>1,'msg'=>'edit success']);

    }

    public function extra_fee_del(){
        $id = isset($_POST['id'])?$_POST['id']:'';
        if(empty($id)){
            return jsonEcho(['code'=>0,'msg'=>'error']);
        }
        $old_row = $this->db->where('id',$id)->get('biz_bill_extra_fee')->row_array();
        $this->db->where('id',$id)->delete('biz_bill_extra_fee');
        $log_data = array();
        $log_data["table_name"] = "biz_bill_extra_fee";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
        return jsonEcho(['code'=>1,'msg'=>'success']);

    }
    
    //2022-08-25 新增历史费用
    public function history_bill(){
        $data = array();

        $data['id_no'] = getValue('id_no', '');
        $data['client_code'] = getValue('client_code', '');
        $data['client_code2'] = getValue('client_code2', '');

        //获取客户中文
        if(!empty($data['client_code'])){
            $name=$this->db->where('client_code',$data['client_code'])->get('biz_client')->row_array();
            $data['company_name']=$name['company_name'];
        }else{
            $data['company_name']='';
        }

        $data['job_no'] = isset($_GET['job_no']) ? $_GET['job_no'] : '';

        //获取shipment的id
        if (!empty($data['job_no'])) {
            $shipment = $this->db->where('job_no', $data['job_no'])->where("(client_code in ('{$data['client_code']}','{$data['client_code2']}') or client_code2 in ('{$data['client_code']}','{$data['client_code2']}'))")->get('biz_shipment')->row_array();
            if (!empty($shipment['id'])) {
                $data['id'] = $shipment['id'];
            } else {
                $data['id'] = 0;
            }
        } else {
            $data['id'] = 0;
        }

        $this->load->view('head');
        $this->load->view('biz/bill/history_bill', $data);
    }
    
     //获取历史数据表格
    public function history_bill_get($id_type = "", $id_no = "")
    {
        //新增 2022-6-13
        //设置专项
        $is_special = isset($_GET['is_special']) ? $_GET['is_special'] : 0;

        $result = array();
        $rs = $this->db->where('id_type', $id_type)->where('id_no', $id_no)->get('biz_bill')->result_array();

        $lirun = 0;
        $rows = array();

        $ci = 0;
        $local_amount = 0;
        // $t_cny = 0;
        $local_currency = get_system_config("CURRENT_CURRENCY");
        // $t_usd = 0;
        foreach ($rs as $row) {
            if ($row['type'] == 'sell') {
                $client = $this->db->select('client_name')->where('client_code', $row["client_code"])->get('biz_client')->row_array();
                $row["client_name"] = "";
                if (!empty($client)) $row["client_name"] = $client["client_name"];
                $local_amount += $row['local_amount'];
                $lirun += $row['local_amount'];
                // if ($row['currency'] == 'CNY') {
                //     $row['amount_cny'] = $row['amount'];
                //     $row['amount_usd'] = "";
                //     $lirun_cny += $row['amount'];
                //     $t_cny += $row['amount'];
                // } else {
                //     $row['amount_cny'] = "";
                //     $row['amount_usd'] = $row['amount'];
                //     $lirun_usd += $row['amount'];
                //     $t_usd += $row['amount'];
                // }

                //新增 2022-6-13
                //获取发票号
                if ($row['invoice_id'] != 0) {
                    $invoice_data = $this->db->where('id', $row['invoice_id'])->get('biz_bill_invoice')->row_array();
                    $row['invoice_no'] = $invoice_data['invoice_no'];
                } else {
                    $row['invoice_no'] = '';
                }

                //新增 2022-6-13
                //获取核销日期
                if ($row['payment_id'] != 0) {
                    $payment_data = $this->db->where('id', $row['payment_id'])->get('biz_bill_payment')->row_array();
                    $row['payment_date'] = $payment_data['payment_date'];
                } else {
                    $row['payment_date'] = '';
                }

                array_push($rows, $row);
                $ci++;
            }
        }
        if ($ci > 1) {
            array_push($rows, array("type" => "SellTotal", "charge" => "----------", "currency" => $local_currency, "amount" => $local_amount));
        }

        $ci = 0;
        $local_amount = 0;
        foreach ($rs as $row) {
            if ($row['type'] == 'cost') {

                //新增 2022-6-13
                //判断是否存在父id
                if ($row['parent_id'] != '0') {
                    $row['parent_id'] = '1';
                } else {
                    $row['parent_id'] = '0';
                }

                $client = $this->db->select('client_name')->where('client_code', $row["client_code"])->get('biz_client')->row_array();
                $row["client_name"] = "";
                if (!empty($client)) $row["client_name"] = $client["client_name"];
                $local_amount += $row['local_amount'];
                $lirun -= $row['local_amount'];
                // if ($row['currency'] == 'CNY') {
                //     $row['amount_cny'] = $row['amount'];
                //     $row['amount_usd'] = "";
                //     $lirun_cny -= $row['amount'];
                //     $t_cny += $row['amount'];
                // } else {
                //     $row['amount_cny'] = "";
                //     $row['amount_usd'] = $row['amount'];
                //     $lirun_usd -= $row['amount'];
                //     $t_usd += $row['amount'];
                // }

                //新增 2022-6-13
                //获取发票号
                if ($row['invoice_id'] != 0) {
                    $invoice_data = $this->db->where('id', $row['invoice_id'])->get('biz_bill_invoice')->row_array();
                    $row['invoice_no'] = $invoice_data['invoice_no'];
                } else {
                    $row['invoice_no'] = '';
                }

                //新增 2022-6-13
                //获取核销日期
                if ($row['payment_id'] != 0) {
                    $payment_data = $this->db->where('id', $row['payment_id'])->get('biz_bill_payment')->row_array();
                    if(empty($payment_data['payment_date'])) $payment_data['payment_date'] = '';
                    $row['payment_date'] = $payment_data['payment_date'];
                } else {
                    $row['payment_date'] = '';
                }

                array_push($rows, $row);
                $ci++;
            }
        }
        if ($ci > 1) {
            array_push($rows, array("type" => "CostTotal", "charge" => "----------", "currency" => $local_currency, "amount" => $local_amount));
        }

        array_push($rows, array("charge" => " ", "currency" => "", "amount" => ""));
        array_push($rows, array("charge" => lang('毛利润'), "currency" => $local_currency, "amount" => $lirun));
        $result["rows"] = $rows;
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    //2022-08-25 新增历史数据
    public function copy_bill(){
        $ids = explode(',', $_POST['ids']);
        $id_no=$_POST['id_no'];
        $msg=array();
        foreach ($ids as $k => $v){
            $row = $this->db->where('id', $v)->get('biz_bill')->row_array();

            $data=array(
                'id_no'=>$id_no,
                'id_type'=>'shipment',
                'type'=>$row['type'],
                'charge_code'=>$row['charge_code'],
                'charge'=>$row['charge'],
                'currency'=>$row['currency'],
                'unit_price'=>$row['unit_price'],
                'vat'=>$row['vat'],
                'remark'=>$row['remark'],
                'client_code'=>$row['client_code'],
                'rate_id'=>$row['rate_id'],
                'is_special'=>$row['is_special'],
                'split_mode'=>$row['split_mode'],
                'number'=>$row['number'],
            );
            $result=add_bill($data);

            if($result['code']=='1'){
                echo json_encode($result);
                return;
            }else{
                $msg[]=$result['msg'];
            }

        }

        $msg = implode(',', $msg);
        echo json_encode(array('code'=>0,'msg'=>$msg));
    }
    
    public function third_party($debitor=''){
        $op = $this->load->database('china',true);
        $data = [];
		$dict = $op->query("SELECT `bsc_dict`.`value`, (select company_name from biz_client where client_code = bsc_dict.value) as receive_party FROM `bsc_dict` WHERE `catalog` = 'branch_office' AND (`name` = 'u".get_system_type()."' or `name` = '".get_system_type()."')")->row_array();
		if(empty($dict)){
			return jsonEcho(['code'=>0,'msg'=>'Not valid receive party']);
		}
        $data['receive_party'] = $dict['receive_party'];
        $data['receive_party_code'] = $dict['value'];
        return jsonEcho(['code'=>1,'data'=>$data]);
    }
    public function save_third_party($third_party_type=1){
        $op = $this->load->database('china',true);
        $op_write = $this->load->database('china',true);
        $param_fields = ['bill_ids','receive_party','agent_party','job_no','payer_party'];
        $param = [];
        foreach ($param_fields as $param_field){
            $param[$param_field] = isset($_POST[$param_field])?$_POST[$param_field]:'';
        }
        $bill_fields = ['charge_code','currency','unit_price','number','amount','amount_no_tax','local_amount','vat','confirm_date','client_code','remark','account_no','overdue_date','bill_ETD','parent_id','exrate'];
        $bill_ids = explode(',',$param['bill_ids']);
        $msg = [];
        foreach ($bill_ids as $bill_id){
            $bill = $this->db->where('id',$bill_id)->get('biz_bill')->row_array();
            if(!empty($op->where("billing_id = $bill_id and receive_party = '{$param['receive_party']}'")->get('biz_bill_third_party')->row_array())){
                $msg[] = 'already added before';
                continue;
            }else{
                $msg[] = 'add success';
            }
            foreach (['cost','sell'] as $type){
                $add = [];
                foreach ($bill_fields as $bill_field){
                    $add[$bill_field] = $bill[$bill_field];
                }
                $add['charge_name'] = $bill['charge'];
                $add['billing_id'] = $bill_id;
                $add['job_no'] = $param['job_no'];
                $add['agent_party'] = $param['agent_party'];
                $add['receive_party'] = $param['receive_party'];
                $add['from'] = get_system_type();
                $add['type'] = $type;
                if($type == 'cost' && $third_party_type == 1) $add['client_code'] = $param['receive_party'];
                if($type == 'sell' && $third_party_type == 1) $add['client_code'] = $param['payer_party'];
                if($type == 'cost' && $third_party_type == 2) $add['client_code'] = $param['payer_party'];
                if($type == 'sell' && $third_party_type == 2) $add['client_code'] = $param['receive_party'];
                $add["updated_time"] = $add["created_time"] = date('y-m-d H:i:s', time());
                $add["updated_by"] = $add["created_by"] = get_session('id');
				$op_write->insert('biz_bill_third_party',$add);
				$log_data = array();
				$log_data["table_name"] = "biz_bill_third_party";
				$log_data["key"] = $op_write->insert_id();
				$log_data["action"] = "insert";
				$log_data["value"] = json_encode($add);
				$log_data["insert_time"] = date('Y-m-d H:i:s');
				$log_data["insert_date"] = date('Y-m-d');
				$log_data["insert_dtime"] = date('H:i:s');
				$log_data["user_id"] = get_session('id');
				$op_write->insert('sys_log',$log_data);
            }
        }
        $this->db->where_in('id',$bill_ids)->update('biz_bill',['account_no'=>'third party lock']);
        $msg = join(',',$msg);
        return jsonEcho(['code'=>1,'msg'=>$msg]);
    }

    /**
     * 2024-04-24 根据当前匹配条件获取当前账单对应的第三方账单, 获取到时, 返回当前的抬头及发票号
     */
    public function get_third_party_data($type=1){
        $op = $this->load->database('china',true);
        $param_fields = array('bill_ids','receive_party','agent_party','job_no','payer_party');
        $data = array('rows' => array());
        $param = array();
        foreach ($param_fields as $param_field){
            $param[$param_field] = isset($_POST[$param_field])?$_POST[$param_field]:'';
        }
        $bill_ids = explode(',',$param['bill_ids']);
        //如果没配模板那么统一返回空的
        $third_party_template_id = get_system_config('third_party_template_id');
        if(!empty($third_party_template_id)){
            $data['rows'] = $bill_third_party_rows = $op->select("biz_bill_third_party.*,(select company_name_en from biz_client where biz_client.client_code = biz_bill_third_party.agent_party) as agent_party_name")->where("billing_id in ('" . join('\',\'', $bill_ids) . "') and receive_party = '{$param['receive_party']}'")->get('biz_bill_third_party')->result_array();
        }

        return jsonEcho(array('code' => 0,'msg' => "获取成功", 'data' => $data));
    }

    public function third_party_payment(){
        $data['f'] = array(
			array('Apply party', 'biz_bill_third_party.receive_party'),
			array('Client code', 'biz_bill_third_party.client_code'),
			array('Agent party', 'biz_bill_third_party.agent_party'),
			array('sell/cost', 'biz_bill_third_party.type'),
			array('charge_name', 'biz_bill_third_party.charge_code'),
			array('unit price', 'biz_bill_third_party.unit_price'),
			array('number', 'biz_bill_third_party.number'),
			array('amount', 'biz_bill_third_party.amount'),
			array('currency', 'biz_bill_third_party.currency'),
			array('JOB NO', 'job_no'),
			array('invoice_no', 'i.invoice_no'),
			array('invoice_date', 'i.invoice_date'),
			array('invoice_user', 'i.invoice_user_id'),
			array('payment_no', 'p.payment_no'),
			array('payment_date', 'p.payment_date'),
			array('payment_user', 'p.payment_user_id'),
			array('payment_bank', 'p.payment_bank'),
        );
		$op = $this->load->database('china',true);
		$data['currency_table_fields'] = array_filter(array_column($op->distinct()->select('currency')->get('biz_bill_third_party')->result_array(), 'currency'));
        $this->load->view('head');
        $this->load->view('biz/bill/third_party_payment',$data);
    }
    public function get_third_party_payment(){
        $op = $this->load->database('china',true);
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------这个查询条件想修改成通用的 -------------------------------
        $where = array();
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($v1 != ""){
            if(in_array($f1,['biz_bill_third_party.client_code'])){
                if($s1 == 'like'){
                    $where[] = "$f1 in (select client_code from biz_client_code_global where company_name_en like '%$v1%' or company_search_en like '%$v1%')";
                }else{
                    $where[] = "$f1 in (select client_code from biz_client_code_global where company_name_en $s1 '$v1' or company_search_en $s1 '$v1')";
                }
            }else{
                $where[] = search_like($f1, $s1, $v1);
            }
        }
        if ($v2 != ""){
            if(in_array($f2,['biz_bill_third_party.client_code'])){
                if($s2 == 'like'){
                    $where[] = "$f2 in (select client_code from biz_client_code_global where company_name_en like '%$v2%' or company_search_en like '%$v2%')";
                }else{
                    $where[] = "$f2 in (select client_code from biz_client_code_global where company_name_en $s2 '$v2' or company_search_en $s2 '$v2')";
                }
            }else{
                $where[] = search_like($f2, $s2, $v2);
            }
        }
        if ($v3 != ""){
            if(in_array($f3,['biz_bill_third_party.client_code'])){
                if($s3 == 'like'){
                    $where[] = "$f3 in (select client_code from biz_client_code_global where company_name_en like '%$v3%' or company_search_en like '%$v3%')";
                }else{
                    $where[] = "$f3 in (select client_code from biz_client_code_global where company_name_en $s3 '$v3' or company_search_en $s3 '$v3')";
                }
            }else{
                $where[] = search_like($f3, $s3, $v3);
            }
        }
		$code_type = postValue('code_type','');
		if($code_type != ''){
			$dict = $this->db->where("catalog = 'branch_office' and name = '".strtoupper(get_system_type())."'")->get('bsc_dict')->row_array();
			if(!empty($dict)){
				if($code_type == 1) $where[] = "receive_party = '{$dict['value']}'";
				if($code_type == 2) $where[] = "agent_party = '{$dict['value']}'";
			}
		}
        $where = join(' and ', $where);
        //-----------------------------------------------------------------
        $offset = ($page - 1) * $rows;
        $op->limit($rows, $offset);
		$op->select("biz_bill_third_party.*,(select company_name_en from biz_client_code_global where client_code = biz_bill_third_party.receive_party) as receive_party_name,(select company_name_en from biz_client_code_global where client_code = biz_bill_third_party.client_code) as client_code_name,(select company_name_en from biz_client_code_global where client_code = biz_bill_third_party.agent_party) as agent_party_name,(select charge_name_cn from biz_charge where charge_code = biz_bill_third_party.charge_code) as charge,(select name from bsc_user where id = biz_bill_third_party.created_by) as created_by_name,(select name from bsc_user where id = biz_bill_third_party.updated_by) as updated_by_name,i.invoice_no,i.invoice_date,i.invoice_company,i.invoice_user_id,(select name from bsc_user where id = i.invoice_user_id) as invoice_user,p.payment_no,p.payment_date,p.payment_user_id,(select name from bsc_user where id = p.payment_user_id) as payment_user,p.payment_bank,(select `name` from bsc_dict where catalog = 'invoice_type' and `value` = i.invoice_type) as invoice_type",null,false);
		$op->join('biz_bill_invoice i','i.id = biz_bill_third_party.invoice_id','left');
		$op->join('biz_bill_payment p','p.id = biz_bill_third_party.payment_id','left');
        if(!empty($where)){
            $rs = $op->where($where)->order_by($sort, $order)->get('biz_bill_third_party')->result_array();
			$op->join('biz_bill_invoice i','i.id = biz_bill_third_party.invoice_id','left');
			$op->join('biz_bill_payment p','p.id = biz_bill_third_party.payment_id','left');
            $result["total"] = $op->where($where)->count_all_results('biz_bill_third_party');
        }else{
            $rs = $op->order_by($sort, $order)->get('biz_bill_third_party')->result_array();
			$op->join('biz_bill_invoice i','i.id = biz_bill_third_party.invoice_id','left');
			$op->join('biz_bill_payment p','p.id = biz_bill_third_party.payment_id','left');
            $result["total"] = $op->count_all_results('biz_bill_third_party');
        }
        $rows = array();
        foreach ($rs as $row) {
            if(empty($row['charge'])) $row['charge'] = $row['charge_name'];
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }
	public function del_third_party($id=''){
		$op = $this->load->database('china',true);
		$op_write = $this->load->database('china',true);
		$row = $op->where('id',$id)->get('biz_bill_third_party')->row_array();
		if(!empty($row)){
			$from = $this->load->database($row['from'],true);
			if($row['created_by'] != get_session('id') && get_session('id') != getUserField($row['created_by'],'leader_id')){
				return jsonEcho(['code'=>0,'msg'=>'no permission!']);
			}
			$rows = $op->where('billing_id',$row['billing_id'])->get('biz_bill_third_party')->result_array();
			$op_write->where('billing_id',$row['billing_id'])->delete('biz_bill_third_party');
			$from->where('id',$row['billing_id'])->update('biz_bill',['account_no'=>'']);
			foreach ($rows as $r){
				$log_data = array();
				$log_data["table_name"] = "biz_bill_third_party";
				$log_data["key"] = $r['id'];
				$log_data["action"] = "delete";
				$log_data["value"] = json_encode($r);
				$log_data["insert_time"] = date('Y-m-d H:i:s');
				$log_data["insert_date"] = date('Y-m-d');
				$log_data["insert_dtime"] = date('H:i:s');
				$log_data["user_id"] = get_session('id');
				$op_write->insert('sys_log',$log_data);
			}
			return jsonEcho(['code'=>1,'msg'=>lang('删除成功')]);
		}
		return jsonEcho(['code'=>0,lang('数据不存在')]);
	}

	/**
	 * 批量开票
	 */
	public function batch_invoice_third_party()
	{
		$this->db = $this->load->database('china',true);
		$invoice_id = isset($_POST['invoice_id']) ? $_POST['invoice_id'] : 0;
		$ids = isset($_POST['ids']) ? $_POST['ids'] : '';
//        $title = isset($_POST['title']) ? $_POST['title'] : '';
		$result = array('code' => 1, 'msg' => '参数错误');
		if (empty($ids)) {// || empty($title)
			echo json_encode($result);
			return;
		}

		$msg_more = '';
		$ids = explode(',', $ids);
		$bill_datas = $this->db->where_in('id',$ids)->get('biz_bill_third_party')->result_array();
		$time_end = date('Y-m-t', time());
		$local_amount_sum = 0;
		$not_confirm_date = array();
		$invoice_ex_rate_arr = array();
		$invoice_ex_rate = 0;
		$from_ids = [];
		$from_db = $this->load->database($bill_datas[0]['from'],true);
		foreach ($bill_datas as $bill_data){
			if($bill_data['type'] == 'cost'){
				$bill_data['amount'] = -$bill_data['amount'];
			}
			//2022-07-27 使用一个变量存储下这个汇率,月份相同的就不重复查了
			if(isset($invoice_ex_rate_arr[$time_end])) $invoice_ex_rate = $invoice_ex_rate_arr[$time_end];
			else $invoice_ex_rate = $invoice_ex_rate_arr[$time_end] = get_ex_rate($time_end, $bill_data['currency'], 'invoice');
			$local_amount_sum += $bill_data['amount'] * $invoice_ex_rate;
			if($bill_data['invoice_id'] != 0 && $bill_data['invoice_id'] != $invoice_id){
				$result['msg'] = 'ID ' . $bill_data['id'] . '的账单已开票，请撤销后再试';
				echo json_encode($result);
				return;
			}
			$from_data = $from_db->where("id = {$bill_data['billing_id']} and client_code = '{$bill_data['client_code']}'")->get('biz_bill')->row_array();
			if(!empty($from_data)) array_push($from_ids,$bill_data['billing_id']);

		}

		$data = array();
		$data['invoice_type'] = '1001';

		$this->load->model('biz_bill_invoice_model');
		//根据发票号获取数据,如果存在,则更新且原本金额基础上增加

		$invoice_no = isset($_POST['invoice_no']) ? trim($_POST['invoice_no']) : '';
		$data['invoice_amount'] = $local_amount_sum;
		$data['invoice_ex_rate'] = $invoice_ex_rate;
		$data['invoice_company'] = get_system_type();
		$data['remark'] = isset($_POST['remark']) ? ts_replace(trim($_POST['remark'])) : '';
		$data['disabled'] = isset($_POST['disabled']) ? trim($_POST['disabled']) : '';
		$data['invoice_date'] = isset($_POST['invoice_date']) ? trim($_POST['invoice_date']) : '0000-00-00';
		$data['buyer_client_code'] = isset($_POST['buyer_client_code']) ? trim($_POST['buyer_client_code']) : '';
		$data['buyer_company'] = isset($_POST['buyer_company']) ? trim($_POST['buyer_company']) : '';
		$data['buyer_taxpayer_id'] = isset($_POST['buyer_taxpayer_id']) ? trim($_POST['buyer_taxpayer_id']) : '';
		$data['goods_name'] = isset($_POST['goods_name']) ? trim($_POST['goods_name']) : '';
		$data['goods_specification'] = isset($_POST['goods_specification']) ? trim($_POST['goods_specification']) : '';
		$data['goods_unit'] = isset($_POST['goods_unit']) ? trim($_POST['goods_unit']) : '';
		$data['more_email'] = isset($_POST['more_email']) ? trim($_POST['more_email']) : '';

		//2023-02-21 先将发票的开票抬头code存起来
		$this->db->select('company_code');
		$sub_company = Model('biz_sub_company_model')->get_where_one("company_name = '{$data['invoice_company']}'");
		$data['invoice_company_code'] = !empty($sub_company) ? $sub_company['company_code'] : '';

		//获取对应的client信息
		$biz_client_model = Model('biz_client_model');
		if(($data['invoice_type'] !== '1001' && $data['invoice_type'] !== '999')){
			$client = $biz_client_model->get_one('client_code',$bill_data["client_code"]);

			$client = array_filter(array_map(function ($val){
				return trim($val);
			}, $client));

			if((empty($data['buyer_company']) || empty($data['buyer_taxpayer_id']))){
				$result['msg'] = '请到往来单位填写该客户对应的纳税人名称或纳税人识别号后再提交';
				echo json_encode($result);
				return;
			}

			if(!empty($client) && (empty($client['taxpayer_address']) || empty($client['taxpayer_telephone'])
//                        || empty($client['bank_name_cny']) || empty($client['bank_account_cny'])
				)){
//                    $result['msg'] = '客户为 ' . $data['buyer_company'] . ' 的纳税人地址或纳税人联系人电话,银行(CNY),银行账号(CNY)未填写';
				$result['msg'] = '客户为 ' . $data['buyer_company'] . ' 的纳税人地址或纳税人联系人电话未填写';
				echo json_encode($result);
				return;
			}
//            }

		}
		$data['currency'] = isset($_POST['currency']) ? trim($_POST['currency']) : '';
		$data['vat'] = isset($_POST['vat']) ? trim($_POST['vat']) : '0%';
		//当金额是负数不受规则控制
		$invoice_amount = isset($_POST['invoice_amount']) ? $_POST['invoice_amount'] : '';

		if($invoice_amount > 0){
			//cost开放 6%的专票
			if(in_array($data['invoice_type'], array(2, 3)) && $data['vat'] == '0%'){//专票
				$result['msg'] = '专票税率不能为0';
				echo json_encode($result);
				return;
			}else if($data['invoice_type'] == 0 && $data['vat'] != '0%'){//普票
				$result['msg'] = '普票税率必须为0%';
				echo json_encode($result);
				return;
			}else if($data['invoice_type'] == 1){//电子普票
				//如果是纯COST,那么开放到0%和6%
				$bill_datas_type = filter_unique_array(array_column($bill_datas, 'type'));
				if(sizeof($bill_datas_type) == 1 && in_array('cost', $bill_datas_type)){
					if(!in_array($data['vat'], array('0%', '6%'))){
						$result['msg'] = '电票税率必须为0%';
						echo json_encode($result);
						return;
					}
				}else{
					if($data['vat'] != '0%'){
						$result['msg'] = '电票税率必须为0%';
						echo json_encode($result);
						return;
					}
				}

			}else if($data['invoice_type'] == 2 && in_array('USD', array_column($bill_datas, 'currency'))){ // 专票不能有美元
				$result['msg'] = '美元不能申请专票';
				echo json_encode($result);
				return;
			}
		}

		//如果是形式发票,自动生成单号
		$is_dn = false;
		$account_id = isset($_POST['account_id']) ? (int)$_POST['account_id'] : 0;
		if(empty($invoice_no)){
			if($data['invoice_type'] == 999){
				$data['payment_bank'] = isset($_POST['payment_bank']) ? $_POST['payment_bank'] : '';
				if(empty($data['invoice_date']) || $data['invoice_date'] == '0000-00-00') $data['invoice_date'] = date('Y-m-d');
				if(empty($invoice_no))$invoice_no = dn_invoice_no_rule($data['payment_bank']);
			}else if($data['invoice_type'] == 1001){
				$data['use_type'] = isset($_POST['use_type']) ? $_POST['use_type'] : '';
				if(empty($data['invoice_date']) || $data['invoice_date'] == '0000-00-00') $data['invoice_date'] = date('Y-m-d');
				//前缀--start
				Model('bsc_dict_model');
				$dict = $this->bsc_dict_model->get_one('name', $data['use_type'], 'use_type');
				//前缀--end

				//查询配置最新编号--star
				Model('sys_config_model');
				$config_num = $this->sys_config_model->get_one_zero('USE_TYPE_NOTE_' . $dict['value']);
				$this_num = 0;
				if(!empty($config_num)) $this_num = $config_num['config_text'];
				//查询配置最新编号--end

				//最后8位数字,以同前缀最后一条+1
				$prefix = $dict['value'];
				$invoice_no_length = 5;
				$num = str_pad($this_num + 1, $invoice_no_length, '0', STR_PAD_LEFT);
				if(empty($config_num)){
					$this->sys_config_model->insert_zero('USE_TYPE_NOTE_' . $dict['value'], $this_num + 1);
				}else{
					$this->sys_config_model->update_zero('USE_TYPE_NOTE_' . $dict['value'], $this_num + 1);
				}
				if(empty($invoice_no))$invoice_no = $prefix . $num;

				//如果是DN的，那么此处生成文件且返回
				if ($prefix == 'DN'){
					$data['account_id'] = $account_id;
					if($account_id == 0){
						$result['msg'] = '未配置当前客户的debit note account';
						echo json_encode($result);
						return;
					}
					$is_dn = true;
				}
			}
		}

		//如果发票号为空 且 发票日期也为空, 那么这里判断
		if(empty($invoice_no) && $data['invoice_date'] === '0000-00-00'){
			//如果税率为9% 那么 goods_name 必须为运费
			if($data['vat'] == '9%' && $data['goods_name'] !== '运输服务*运费'){
				$result['msg'] = lang('税率9% 时 必须选择 运输服务*运费');
				echo json_encode($result);
				return;
			}
			//2023-12-26 新加入百望云服务名称检测, ext2 有参数时
			$this->db->limit(1);
			$bwy_goods_code = Model('bsc_dict_model')->get_option("bwy_goods_code", "name = '{$data['goods_name']}'");

			if(!empty($bwy_goods_code) && $bwy_goods_code[0]['ext2'] !== ''){
				$temp_invoice_id = postValue('temp_invoice_id', '');
				//2023-12-26 如果临时发票ID不为空时, 特定业务信息不能为空
				if(!empty($temp_invoice_id)){
					$invoice_order_info = Model('biz_bill_invoice_order_info_model')->get_where_one("temp_invoice_id = '{$temp_invoice_id}'");

					if(empty($invoice_order_info)){
						$result['msg'] = lang('请填写{field}后再试', array('field' => lang('特定业务信息')));
						echo json_encode($result);
						return;
					}
				}
			}
		}

		$data['invoice_no'] = $invoice_no;

		if($invoice_no !== ''){
			$this->db->where('`lock` != 999');
			$old_invoice = $this->biz_bill_invoice_model->get_one('invoice_no', $invoice_no);

			//存在该发票号则获取金额,其余以当前为准
			if(!empty($old_invoice)){
				//若该发票号已核销或已审核,则无法合并
				if($old_invoice['lock'] > 1){
					$result['code'] = 2;
					$result['msg'] = '该发票号无法合并';
					echo json_encode($result);
					return;
				}
				//开票后修改,不能是已存在发票号
				if($invoice_id > 0){
					$result['msg'] = '不能修改为已存在发票号';
					echo json_encode($result);
					return;
				}
				$old_invoice_bill_datas = $this->db->where("invoice_id = {$old_invoice['id']}")->get("biz_bill_third_party")->result_array();
				$old_invoice_amount = 0;
				foreach ($old_invoice_bill_datas as $old_invoice_bill_data){
					if($old_invoice_bill_data['type'] == 'cost'){
						$old_invoice_bill_data['amount'] = -$old_invoice_bill_data['amount'];
					}
					$old_invoice_amount += $old_invoice_bill_data['amount'] * get_ex_rate($time_end, $bill_data['currency'], 'invoice');
				}
				$data['invoice_amount'] += $old_invoice_amount;
				$invoice_id = $old_invoice['id'];
			}
		}


		if($data['invoice_amount'] > 99999.99 && $data['invoice_type'] >= 999){
			$msg_more .= ',金额超出99999.99,不予开票';
		}
		if($invoice_id == 0){//1、未发票，新增
			$invoices = filter_unique_array(array_column($bill_datas, 'invoice_id'));
			if(sizeof($invoices) > 0){//去除未绑定
				$result['msg'] = '请勿选择已开票数据';
				echo json_encode($result);
				return;
			}
			$action = 'insert';
			//添加发票数据--start
			if(!empty($data['invoice_date']) && $data['invoice_date'] != '0000-00-00'){
				$data['lock'] = 1;
			}
			$data['invoice_user_id'] = $this->session->userdata('id');

			$invoice_id = $this->biz_bill_invoice_model->save($data);
			//添加发票数据--end
		}else{//2、已发票，修改
			$invoices = filter_unique_array(array_column($bill_datas, 'invoice_id'));
			if(sizeof($invoices) > 1){//去除未绑定
				$result['msg'] = '请勿同时修改多个开票信息';
				echo json_encode($result);
				return;
			}
			//只能修改lock=0的
			$invoice_info = $this->biz_bill_invoice_model->get_one('id', $invoice_id);
			if(empty($invoice_info)){//不存在报错
				$result['msg'] = '发票信息不存在';
				echo json_encode($result);
				return;
			}
			$action = 'update';
			//修改发票数据--start
			if($invoice_info['lock'] == 10) {
				$data['lock'] = 2;
				$data['invoice_user_id'] = $this->session->userdata('id');
			}else{
				$data['lock'] = 1;
			}
			$invoice_id = $this->biz_bill_invoice_model->update($invoice_id, $data);
			//修改发票数据--end
		}

		if($invoice_id > 0){
			$data['id'] = $invoice_id;
			// record the log
			$log_data = array();
			$log_data["table_name"] = "biz_bill_invoice";
			$log_data["key"] = $invoice_id;
			$log_data["action"] = $action;
			$log_data["value"] = json_encode($data);
			log_rcd($log_data);

			//2023-12-25 由于 加入了 申请开票时的 特定业务信息填写, 所以这里加入了 临时发票ID, 将生成的特定业务信息进行删除
			if(!empty($temp_invoice_id)){
				$this->db->query("update biz_bill_invoice_order_info set invoice_id = '{$invoice_id}' where temp_invoice_id = '{$temp_invoice_id}'");
			}
		}
		//绑定对应的发票ID--start
		$update_data = array();
		$biz_client_model = Model('biz_client_model');


		//2022-07-26 查询授信额度改为在外面, 因为他们会几千个账单处理,这里会卡死
		//获取授信额度进行过期日期的计算
		$this->db->select('id, finance_payment, finance_od_basis, finance_payment_month, finance_payment_day_th, finance_payment_days');
		$bill_clients = array_column($biz_client_model->get("client_code in ('" . join('\',\'', array_column($bill_datas, 'client_code')) . "')"), null, 'client_code');
//        if(!empty($client) && $client['finance_payment'] == 'after invoice')$this_data['overdue_date'] = get_overdue_date($client, $data['invoice_date']);
		$result['url'] = '';
		foreach ($bill_datas as $bill_data){

			//判定当前bill invoice_id为0的,给予绑定
			if($bill_data['invoice_id'] != 0){
				continue;
			}
			$this_data = array();
			$this_data['id'] = $bill_data['id'];
			$this_data['invoice_id'] = $invoice_id;

			//获取授信额度进行过期日期的计算
			$bill_client = isset($bill_clients[$bill_data['client_code']]) ? $bill_clients[$bill_data['client_code']] : array();
//            $this->db->select('id, finance_payment, finance_od_basis, finance_payment_month, finance_payment_day_th, finance_payment_days');
//            $client = $biz_client_model->get_one('client_code', $bill_data['client_code']);
			if(!empty($bill_client) && $bill_client['finance_payment'] == 'after invoice')$this_data['overdue_date'] = get_overdue_date($bill_client, $data['invoice_date']);
			$update_data[] = $this_data;

			$log_data = array();
			$log_data["table_name"] = "biz_bill_third_party";
			$log_data["key"] = $bill_data['id'];
			$log_data["action"] = 'update';
			$log_data["value"] = json_encode($this_data);
			log_rcd($log_data);
		}
		//这里可以考虑拆为300条一次
		$pc = 300;
		for ($i = 0; $i < sizeof($update_data) / $pc; $i++){
			$this_update_data = array_slice($update_data, $i * $pc, $pc);
			$this->db->update_batch('biz_bill_third_party',$this_update_data,'id');
		}
//        $this->biz_bill_model->mbacth_update($update_data);

		if($is_dn){
			$result['pdf_path'] = '/other/dn_invoice_pdf_by_no?invoice_no=' . $data['invoice_no'] . '&shipment_id=' . $bill_datas[0]['id_no'];
		}
		//绑定对应的发票ID--end
		$result['code'] = 0;
		if($action == 'insert'){
			$result['msg'] = '开票成功' . $msg_more;

		}else if($action == 'update'){
			$result['msg'] = '修改成功' . $msg_more;
		}
		if(!empty($from_ids)) $result['tb'] = $this->tb_from_invoice($from_ids,$bill_datas[0]['from']);
		echo json_encode($result);
	}

	private function tb_from_invoice($from_ids,$from)
	{
		$this->db = $this->load->database($from,true);
		$invoice_id = isset($_POST['invoice_id']) ? $_POST['invoice_id'] : 0;
		$result = array('code' => 1, 'msg' => '参数错误');
		if (empty($from_ids)) {
			return $result;
		}

		$msg_more = '';
		$ids = $from_ids;
		$bill_datas = $this->biz_bill_model->not_role_get("id in ('" . join('\',\'', $ids) . "')", 'id', 'desc');

		$time_end = date('Y-m-t', time());
		$local_amount_sum = 0;
		$not_confirm_date = array();
		$invoice_ex_rate_arr = array();
		$invoice_ex_rate = 0;
		foreach ($bill_datas as $bill_data){
			if($bill_data['type'] == 'cost'){
				$bill_data['amount'] = -$bill_data['amount'];
			}
			if($bill_data['type'] == 'sell' && $bill_data['confirm_date'] == '0000-00-00') $not_confirm_date[] = $bill_data;
			if(isset($invoice_ex_rate_arr[$time_end])) $invoice_ex_rate = $invoice_ex_rate_arr[$time_end];
			else $invoice_ex_rate = $invoice_ex_rate_arr[$time_end] = get_ex_rate($time_end, $bill_data['currency'], 'invoice');
			$local_amount_sum += $bill_data['amount'] * $invoice_ex_rate;
			if($bill_data['invoice_id'] != 0 && $bill_data['invoice_id'] != $invoice_id){
				$result['msg'] = 'ID ' . $bill_data['id'] . '的账单已开票，请撤销后再试';
				return $result;
			}
		}

		$data = array();
		$data['invoice_type'] = isset($_POST['invoice_type']) ? trim($_POST['invoice_type']) : null;

		if(!empty($not_confirm_date) && $data['invoice_type'] != 1001){
			$result['msg'] = 'ID 为' . join(',', array_column($not_confirm_date, 'id')) . '的账单未确认，无法开票';
			return $result;
		}

		$this->load->model('biz_bill_invoice_model');
		//根据发票号获取数据,如果存在,则更新且原本金额基础上增加

		$invoice_no = isset($_POST['invoice_no']) ? trim($_POST['invoice_no']) : '';
		$data['invoice_amount'] = $local_amount_sum;
		$data['invoice_ex_rate'] = $invoice_ex_rate;
		$data['invoice_company'] = isset($_POST['invoice_company']) ? trim($_POST['invoice_company']) : '';
		$data['remark'] = isset($_POST['remark']) ? ts_replace(trim($_POST['remark'])) : '';
		$data['disabled'] = isset($_POST['disabled']) ? trim($_POST['disabled']) : '';
		$data['invoice_date'] = isset($_POST['invoice_date']) ? trim($_POST['invoice_date']) : '0000-00-00';
		$data['buyer_client_code'] = isset($_POST['buyer_client_code']) ? trim($_POST['buyer_client_code']) : '';
		$data['buyer_company'] = isset($_POST['buyer_company']) ? trim($_POST['buyer_company']) : '';
		$data['buyer_taxpayer_id'] = isset($_POST['buyer_taxpayer_id']) ? trim($_POST['buyer_taxpayer_id']) : '';

		//2023-02-21 先将发票的开票抬头code存起来
		$this->db->select('company_code');
		$sub_company = Model('biz_sub_company_model')->get_where_one("company_name = '{$data['invoice_company']}'");
		if(in_array($from,['china','china3'])) $data['invoice_company_code'] = !empty($sub_company) ? $sub_company['company_code'] : '';

		//获取对应的client信息
		$biz_client_model = Model('biz_client_model');
		if(($data['invoice_type'] !== '1001' && $data['invoice_type'] !== '999')){
			$client = $biz_client_model->get_one('client_code',$bill_data["client_code"]);
			if(!empty($client)){
				$client = array_filter(array_map(function ($val){
					return trim($val);
				}, $client));
			}

			if((empty($data['buyer_company']) || empty($data['buyer_taxpayer_id']))){
				$result['msg'] = '请到往来单位填写该客户对应的纳税人名称或纳税人识别号后再提交';
				return $result;
			}
			//2022-08-11 开票申请提交的时候，如果是USD，则判断开票抬头必须要有美元账号 这里因为和账单直接挂钩,如果账单全为USD,那么这里触发事件
			if(!empty($client) && (empty($client['taxpayer_address']) || empty($client['taxpayer_telephone'])
				)){
				$result['msg'] = '客户为 ' . $data['buyer_company'] . ' 的纳税人地址或纳税人联系人电话未填写';
				return $result;
			}

		}
		$data['currency'] = isset($_POST['currency']) ? trim($_POST['currency']) : '';
		$data['vat'] = isset($_POST['vat']) ? trim($_POST['vat']) : '0%';
		//当金额是负数不受规则控制
		$invoice_amount = isset($_POST['invoice_amount']) ? $_POST['invoice_amount'] : '';

		if($invoice_amount > 0){
			//cost开放 6%的专票
			if(in_array($data['invoice_type'], array(2, 3)) && $data['vat'] == '0%'){//专票
				$result['msg'] = '专票税率不能为0';
				return $result;
			}else if($data['invoice_type'] == 0 && $data['vat'] != '0%'){//普票
				$result['msg'] = '普票税率必须为0%';
				return $result;
			}else if($data['invoice_type'] == 1){//电子普票
				//如果是纯COST,那么开放到0%和6%
				$bill_datas_type = filter_unique_array(array_column($bill_datas, 'type'));
				if(sizeof($bill_datas_type) == 1 && in_array('cost', $bill_datas_type)){
					if(!in_array($data['vat'], array('0%', '6%'))){
						$result['msg'] = '电票税率必须为0%';
						return $result;
					}
				}else{
					if($data['vat'] != '0%'){
						$result['msg'] = '电票税率必须为0%';
						return $result;
					}
				}

			}else if($data['invoice_type'] == 2 && in_array('USD', array_column($bill_datas, 'currency'))){ // 专票不能有美元
				$result['msg'] = '美元不能申请专票';
				return $result;
			}
		}

		//如果是形式发票,自动生成单号
		$is_dn = false;
		$account_id = isset($_POST['account_id']) ? (int)$_POST['account_id'] : 0;
		if(empty($invoice_no)){
			if($data['invoice_type'] == 999){
				$data['payment_bank'] = isset($_POST['payment_bank']) ? $_POST['payment_bank'] : '';
				if(empty($data['invoice_date']) || $data['invoice_date'] == '0000-00-00') $data['invoice_date'] = date('Y-m-d');
				if(empty($invoice_no))$invoice_no = dn_invoice_no_rule($data['payment_bank']);
			}else if($data['invoice_type'] == 1001){
				$data['use_type'] = isset($_POST['use_type']) ? $_POST['use_type'] : '';
				if(empty($data['invoice_date']) || $data['invoice_date'] == '0000-00-00') $data['invoice_date'] = date('Y-m-d');
				//前缀--start
				Model('bsc_dict_model');
				$dict = $this->bsc_dict_model->get_one('name', $data['use_type'], 'use_type');
				//前缀--end

				//查询配置最新编号--star
				Model('sys_config_model');
				$config_num = $this->sys_config_model->get_one_zero('USE_TYPE_NOTE_' . $dict['value']);
				$this_num = 0;
				if(!empty($config_num)) $this_num = $config_num['config_text'];
				//查询配置最新编号--end

				//最后8位数字,以同前缀最后一条+1
				$prefix = $dict['value'];
				$invoice_no_length = 5;
				$num = str_pad($this_num + 1, $invoice_no_length, '0', STR_PAD_LEFT);
				if(empty($config_num)){
					$this->sys_config_model->insert_zero('USE_TYPE_NOTE_' . $dict['value'], $this_num + 1);
				}else{
					$this->sys_config_model->update_zero('USE_TYPE_NOTE_' . $dict['value'], $this_num + 1);
				}
				if(empty($invoice_no))$invoice_no = $prefix . $num;

				//如果是DN的，那么此处生成文件且返回
				if ($prefix == 'DN'){
					$data['account_id'] = $account_id;
					if($account_id == 0){
						$result['msg'] = '未配置当前客户的debit note account';
						return $result;
					}
					$is_dn = true;
				}
			}
		}

		//如果发票号为空 且 发票日期也为空, 那么这里判断
		if(empty($invoice_no) && $data['invoice_date'] === '0000-00-00'){
			//如果税率为9% 那么 goods_name 必须为运费
			if($data['vat'] == '9%' && $data['goods_name'] !== '运费'){
				$result['msg'] = lang('税率9% 时 必须选择 运费');
				return $result;
			}
			//2023-12-26 新加入百望云服务名称检测, ext2 有参数时
			$this->db->limit(1);
			$bwy_goods_code = Model('bsc_dict_model')->get_option("bwy_goods_code", "name = '{$data['goods_name']}'");

			if(!empty($bwy_goods_code) && $bwy_goods_code[0]['ext2'] !== ''){
				$temp_invoice_id = postValue('temp_invoice_id', '');
				//2023-12-26 如果临时发票ID不为空时, 特定业务信息不能为空
				if(!empty($temp_invoice_id)){
					$invoice_order_info = Model('biz_bill_invoice_order_info_model')->get_where_one("temp_invoice_id = '{$temp_invoice_id}'");

					if(empty($invoice_order_info)){
						$result['msg'] = lang('请填写{field}后再试', array('field' => lang('特定业务信息')));
						return $result;
					}
				}
			}
		}

		$data['invoice_no'] = $invoice_no;

		if($invoice_no !== ''){
			$this->db->where('`lock` != 999');
			$old_invoice = $this->biz_bill_invoice_model->get_one('invoice_no', $invoice_no);

			//存在该发票号则获取金额,其余以当前为准
			if(!empty($old_invoice)){
				//若该发票号已核销或已审核,则无法合并
				if($old_invoice['lock'] > 1){
					$result['code'] = 2;
					$result['msg'] = '该发票号无法合并';
					return $result;
				}
				//开票后修改,不能是已存在发票号
				if($invoice_id > 0){
					$result['msg'] = '不能修改为已存在发票号';
					return $result;
				}
				$old_invoice_bill_datas = $this->biz_bill_model->not_role_get("invoice_id = {$old_invoice['id']}", 'id', 'desc');
				$old_invoice_amount = 0;
				foreach ($old_invoice_bill_datas as $old_invoice_bill_data){
					if($old_invoice_bill_data['type'] == 'cost'){
						$old_invoice_bill_data['amount'] = -$old_invoice_bill_data['amount'];
					}
					$old_invoice_amount += $old_invoice_bill_data['amount'] * get_ex_rate($time_end, $bill_data['currency'], 'invoice');
				}
				$data['invoice_amount'] += $old_invoice_amount;
				$invoice_id = $old_invoice['id'];
			}
		}

		if($data['invoice_amount'] > 99999.99 && $data['invoice_type'] >= 999){
			$msg_more .= ',金额超出99999.99,不予开票';
		}
		if($invoice_id == 0){//1、未发票，新增
			$invoices = filter_unique_array(array_column($bill_datas, 'invoice_id'));
			if(sizeof($invoices) > 0){//去除未绑定
				$result['msg'] = '请勿选择已开票数据';
				return $result;
			}
			$action = 'insert';
			//添加发票数据--start
			if(!empty($data['invoice_date']) && $data['invoice_date'] != '0000-00-00'){
				$data['lock'] = 1;
			}
			$data['invoice_user_id'] = $this->session->userdata('id');

			$invoice_id = $this->biz_bill_invoice_model->save($data);
			//添加发票数据--end
		}else{//2、已发票，修改
			$invoices = filter_unique_array(array_column($bill_datas, 'invoice_id'));
			if(sizeof($invoices) > 1){//去除未绑定
				$result['msg'] = '请勿同时修改多个开票信息';
				return $result;
			}
			//只能修改lock=0的
			$invoice_info = $this->biz_bill_invoice_model->get_one('id', $invoice_id);
			if(empty($invoice_info)){//不存在报错
				$result['msg'] = '发票信息不存在';
				return $result;
			}
			$action = 'update';
			//修改发票数据--start
			if($invoice_info['lock'] == 10) {
				$data['lock'] = 2;
				$data['invoice_user_id'] = $this->session->userdata('id');
			}else{
				$data['lock'] = 1;
			}
			$invoice_id = $this->biz_bill_invoice_model->update($invoice_id, $data);
			//修改发票数据--end
		}

		if($invoice_id > 0){
			if(isset($_GET['id_no']) && !empty($_GET['id_no'])){
				$this->db->where(['id_no'=>$_GET['id_no'],'biz_table'=>'biz_bill_invoice','type_name'=>'费用确认单'])->update('bsc_upload',['id_no'=>$invoice_id]);
			}
			$data['id'] = $invoice_id;
			// record the log
			$log_data = array();
			$log_data["table_name"] = "biz_bill_invoice";
			$log_data["key"] = $invoice_id;
			$log_data["action"] = $action;
			$log_data["value"] = json_encode($data);
			log_rcd($log_data);
		}
		//绑定对应的发票ID--start
		$update_data = array();
		$biz_client_model = Model('biz_client_model');


		//2022-07-26 查询授信额度改为在外面, 因为他们会几千个账单处理,这里会卡死
		//获取授信额度进行过期日期的计算
		$this->db->select('id, finance_payment, finance_od_basis, finance_payment_month, finance_payment_day_th, finance_payment_days');
		$bill_clients = array_column($biz_client_model->get("client_code in ('" . join('\',\'', array_column($bill_datas, 'client_code')) . "')"), null, 'client_code');
		$result['url'] = '';
		foreach ($bill_datas as $bill_data){

			//判定当前bill invoice_id为0的,给予绑定
			if($bill_data['invoice_id'] != 0){
				continue;
			}
			$this_data = array();
			$this_data['id'] = $bill_data['id'];
			$this_data['invoice_id'] = $invoice_id;

			//获取授信额度进行过期日期的计算
			$bill_client = isset($bill_clients[$bill_data['client_code']]) ? $bill_clients[$bill_data['client_code']] : array();
			if(!empty($bill_client) && $bill_client['finance_payment'] == 'after invoice')$this_data['overdue_date'] = get_overdue_date($bill_client, $data['invoice_date']);
			$update_data[] = $this_data;

			$log_data = array();
			$log_data["table_name"] = "biz_bill";
			$log_data["key"] = $bill_data['id'];
			$log_data["master_table_name"] = "biz_" . $bill_data['id_type'];
			$log_data["master_key"] = $bill_data['id_no'];
			$log_data["action"] = 'update';
			$log_data["value"] = json_encode($this_data);
			log_rcd($log_data);
		}
		//这里可以考虑拆为300条一次
		$pc = 300;
		for ($i = 0; $i < sizeof($update_data) / $pc; $i++){
			$this_update_data = array_slice($update_data, $i * $pc, $pc);
			$this->biz_bill_model->mbacth_update($this_update_data);
		}

		if($is_dn){
			$result['pdf_path'] = '/other/dn_invoice_pdf_by_no?invoice_no=' . $data['invoice_no'] . '&shipment_id=' . $bill_datas[0]['id_no'];
		}
		//绑定对应的发票ID--end
		$result['code'] = 0;
		if($action == 'insert'){
			$result['msg'] = '开票成功' . $msg_more;

		}else if($action == 'update'){
			$result['msg'] = '修改成功' . $msg_more;
		}
		return $result;
	}

	/**
	 * 批量删除开票
	 */

	public function batch_del_invoice_third_party(){
		$this->db = $this->load->database('china',true);
		//一次只能销毁一个开票信息
		$invoice_id = isset($_POST['invoice_id']) ? $_POST['invoice_id'] : 0;
		if($invoice_id == 0) {
			echo json_encode(array('code' => 1, 'msg' => 'error'));
			return;
		}
		$this->load->model('biz_bill_invoice_model');
		$invoice = $this->biz_bill_invoice_model->get_one('id', $invoice_id);
		if(empty($invoice)){
			echo json_encode(array('code' => 1, 'msg' => '开票信息错误'));
			return;
		}
		if(!empty($invoice)){
			//2022-08-03 没有财务的角色时,如果撤销,只能撤销自己的,且发票号和发票日期不能都有值
			//2022-08-08 应收应付财务加入
			if(!in_array('finance', get_session('user_role')) && !in_array('finance_sell', get_session('user_role')) && !in_array('finance_cost', get_session('user_role'))){
				if($invoice['created_by'] != get_session('id')) return jsonEcho(array('code' => 1, 'msg' => '不是发票创建人,无法撤销'));
			}

			$invoice_update = array('lock' => 999);
			$this->biz_bill_invoice_model->update($invoice_id, $invoice_update);

			$log_data = array();
			$log_data["table_name"] = "biz_bill_invoice";
			$log_data["key"] = $invoice_id;
			$log_data["action"] = 'update';
			$log_data["value"] = json_encode($invoice_update);
			log_rcd($log_data);

			//销毁时将相关的同ID一并置为0
			$bills = $this->db->where("invoice_id = '$invoice_id'")->get('biz_bill_third_party')->result_array();
			$update_data = array();
			foreach ($bills as $bill){
				$data = array();
				$data['id'] = $bill['id'];
				$data['invoice_id'] = 0;
				$update_data[] = $data;

				$log_data = array();
				$log_data["table_name"] = "biz_bill_third_party";
				$log_data["key"] = $bill['id'];
				$log_data["master_table_name"] = 'biz_bill_invoice';
				$log_data["master_key"] = $invoice_id;
				$log_data["action"] = 'update';
				$log_data["value"] = json_encode($data);
				log_rcd($log_data);
			}
			$from_db = $this->load->database($bills[0]['from'],true);
			$billing_ids = join(',',array_column($bills,'billing_id'));
			$billings = $from_db->where("id in ($billing_ids)")->get('biz_bill')->result_array();
			$from_db->where_in('id',array_column($billings,'invoice_id'))->delete('biz_bill_invoice');
			$from_db->where_in('id',array_column($bills,'billing_id'))->update('biz_bill',['invoice_id'=>0]);
			$this->db->update_batch('biz_bill_third_party',$update_data,'id');
			//绑定对应的发票ID--end
			echo json_encode(array('code' => 0, 'msg' => '撤销开票成功'));
		}else{
			echo json_encode(array('code' => 1, 'msg' => '该开票信息不允许撤销开票'));
		}

	}

	/**
	 * 核销
	 */
	public function batch_write_off_third_party(){
		$this->db = $this->load->database('china',true);
		$ids = isset($_POST['ids']) ? $_POST['ids'] : '';

		if (empty($ids)) {
			echo json_encode(array('code' => 1, 'msg' => '参数错误'));
			return;
		}
		Model('biz_bill_payment_model');
		$ids_array = explode(',', $ids);
		$ids_array = array_filter(array_map(function ($val){
			return (int)$val;
		}, $ids_array));
		if(empty($ids_array)){
			echo json_encode(array('code' => 1, 'msg' => '参数错误'));
			return;
		}
		$this->db->select('id, type, currency, local_amount,amount,invoice_id,from,client_code,billing_id');
		$bills = $this->db->where('id in (' . join(',', $ids_array) . ')')->get('biz_bill_third_party')->result_array();
		if(empty($bills)){
			echo json_encode(array('code' => 1, 'msg' => '未找到账单'));
			return;
		}
		$local_amount_sum = 0;
		$from_ids = [];
		$from_db = $this->load->database($bills[0]['from'],true);
		foreach ($bills as $bill){
			if($_POST['currency']=='CNY'){
				$this_local_amount = $bill['type'] == 'sell' ? $bill['local_amount'] : -$bill['local_amount'];
				$local_amount_sum += $this_local_amount;
			}else{
				$this_local_amount = $bill['type'] == 'sell' ? $bill['amount'] : -$bill['amount'];
				$local_amount_sum += $this_local_amount;
			}
			$from_data = $from_db->where("id = {$bill['billing_id']} and client_code = '{$bill['client_code']}'")->get('biz_bill')->row_array();
			if(!empty($from_data)) array_push($from_ids,$bill['billing_id']);

		}

		$data = array();
		if(isset($_POST['payment_date']) && !empty($_POST['payment_date'])){
			$data['payment_date'] = $_POST['payment_date'];
		}
		isset($_POST['payment_amount']) && $data['payment_amount'] = $_POST['payment_amount'];
		isset($_POST['payment_way']) && $data['payment_way'] = $_POST['payment_way'];
		isset($_POST['payment_bank']) && $data['payment_bank'] = $_POST['payment_bank'];
		isset($_POST['payment_bank_id']) && $data['payment_bank_id'] = $_POST['payment_bank_id'];
		isset($_POST['payment_no']) && $data['payment_no'] = trim($_POST['payment_no']);
		isset($_POST['currency']) && $data['currency'] = trim($_POST['currency']);//2022-06-13 加入币种
		isset($_POST['other_side_company']) && $data['other_side_company'] = trim($_POST['other_side_company']);//2022-06-15 加入收付款对方抬头
		$data['payment_user_id'] = get_session('id');

		//判断收款银行的抬头和开票的抬头要一致
		$invoice_ids = array_column($bills,'invoice_id');
		$invoice_ids = array_unique($invoice_ids);
		$invoice_ids = join(",",$invoice_ids);
		$sql = "select (SELECT ext1 FROM `bsc_dict` where catalog = 'invoice_title' and  `name` = biz_bill_invoice.invoice_company) as invoice_company from biz_bill_invoice where id in ($invoice_ids);";
		$invoice_company = Model('m_model')->query_array($sql);
		$invoice_company = array_column($invoice_company,'invoice_company');
		$invoice_company = array_filter($invoice_company);

		$sql = "SELECT *,(select company_title from biz_sub_company where company_code = bsc_overseas_account.company_code) as company_title FROM `bsc_overseas_account` where `id` = '{$data['payment_bank_id']}';";
		$bankrow = $this->m_model->query_one($sql);
		if(empty($bankrow)) exit(json_encode(array('code' => 1, 'msg' => '无此银行')));
		if(!in_array($bankrow['company_title'],$invoice_company) && sizeof($invoice_company)>0) exit(json_encode(array('code' => 1, 'msg' => '收款抬头和开票抬头不一致，核销失败！')));

		$id = $this->biz_bill_payment_model->save($data);

		if($id <= 0){
			echo json_encode(array('code' => 1, 'msg' => '核销失败'));
			return;
		}
		$log_data = array();
		$log_data["table_name"] = "biz_bill_payment";
		$log_data["key"] = $id;
		$log_data["action"] = 'insert';
		$log_data["value"] = json_encode($data);
		log_rcd($log_data);

		//给上面的bill批量关联
		$bills_ids = array_column($bills, 'id');
		$this->payment_id = $id;

		$batch_bill = array_map(function ($val){
			return array('id' => $val, 'payment_id' => $this->payment_id);
		}, $bills_ids);


		$this->db->update_batch('biz_bill_third_party',$batch_bill,'id');

		$tb = '';
		if(!empty($from_ids)) $tb = $this->tb_from_write_off($from_ids,$bills[0]['from']);
		if(isset($data['payment_amount']) && $local_amount_sum != $data['payment_amount']){
			echo json_encode(array('code' => 0,'tb'=>$tb, 'msg' => '核销成功,核销金额与实际bill金额不一致,计算:' . $local_amount_sum . ',当前:' . $data['payment_amount']));
			return;
		}
		echo json_encode(array('code' => 0, 'tb'=>$tb,'msg' => '核销成功'));
	}
	private function tb_from_write_off($from_ids,$from){
		$this->db = $this->load->database($from,true);
		Model('biz_bill_payment_model');
		$ids_array = $from_ids;
		$ids_array = array_filter(array_map(function ($val){
			return (int)$val;
		}, $ids_array));
		if(empty($ids_array)){
			return array('code' => 1, 'msg' => '参数错误');
		}
		$this->db->select('id, type, currency, local_amount,amount,invoice_id');
		$bills = $this->biz_bill_model->get('id in (' . join(',', $ids_array) . ')', 'id', 'desc', "");
		if(empty($bills)){
			return array('code' => 1, 'msg' => '未找到账单');
		}
		$local_amount_sum = 0;
		foreach ($bills as $bill){
			if($_POST['currency']=='CNY'){
				$this_local_amount = $bill['type'] == 'sell' ? $bill['local_amount'] : -$bill['local_amount'];
				$local_amount_sum += $this_local_amount;
			}else{
				$this_local_amount = $bill['type'] == 'sell' ? $bill['amount'] : -$bill['amount'];
				$local_amount_sum += $this_local_amount;
			}
		}

		$data = array();
		if(isset($_POST['payment_date']) && !empty($_POST['payment_date'])){
			$data['payment_date'] = $_POST['payment_date'];
		}
		isset($_POST['payment_amount']) && $data['payment_amount'] = $_POST['payment_amount'];
		isset($_POST['payment_way']) && $data['payment_way'] = $_POST['payment_way'];
		isset($_POST['payment_bank']) && $data['payment_bank'] = $_POST['payment_bank'];
		if(in_array($from,['china','china3'])) isset($_POST['payment_bank_id']) && $data['payment_bank_id'] = $_POST['payment_bank_id'];
		isset($_POST['payment_no']) && $data['payment_no'] = trim($_POST['payment_no']);
		isset($_POST['currency']) && $data['currency'] = trim($_POST['currency']);//2022-06-13 加入币种
		isset($_POST['other_side_company']) && $data['other_side_company'] = trim($_POST['other_side_company']);//2022-06-15 加入收付款对方抬头
		$data['payment_user_id'] = get_session('id');

		//判断收款银行的抬头和开票的抬头要一致
		$invoice_ids = array_column($bills,'invoice_id');
		$invoice_ids = array_unique($invoice_ids);
		$invoice_ids = join(",",$invoice_ids);
		$sql = "select (SELECT ext1 FROM `bsc_dict` where catalog = 'invoice_title' and  `name` = biz_bill_invoice.invoice_company) as invoice_company from biz_bill_invoice where id in ($invoice_ids);";
		$invoice_company = Model('m_model')->query_array($sql);
		$invoice_company = array_column($invoice_company,'invoice_company');
		$invoice_company = array_filter($invoice_company);
		if(in_array($from,['china','china3'])){
			$sql = "SELECT *,(select company_title from biz_sub_company where company_code = bsc_overseas_account.company_code) as company_title FROM `bsc_overseas_account` where `id` = '{$data['payment_bank_id']}';";
			$bankrow = $this->m_model->query_one($sql);
			if(empty($bankrow)) return array('code' => 1, 'msg' => '无此银行');
			if(!in_array($bankrow['company_title'],$invoice_company) && sizeof($invoice_company)>0) return array('code' => 1, 'msg' => '收款抬头和开票抬头不一致，核销失败！');
		}
		$id = $this->biz_bill_payment_model->save($data);

		if($id <= 0){
			return array('code' => 1, 'msg' => '核销失败');
		}
		$log_data = array();
		$log_data["table_name"] = "biz_bill_payment";
		$log_data["key"] = $id;
		$log_data["action"] = 'insert';
		$log_data["value"] = json_encode($data);
		log_rcd($log_data);

		//给上面的bill批量关联
		$bills_ids = array_column($bills, 'id');
		$this->payment_id = $id;

		$batch_bill = array_map(function ($val){
			return array('id' => $val, 'payment_id' => $this->payment_id);
		}, $bills_ids);


		$this->biz_bill_model->mbacth_update($batch_bill);

		if(isset($data['payment_amount']) && $local_amount_sum != $data['payment_amount']){
			return array('code' => 0, 'msg' => '核销成功,核销金额与实际bill金额不一致,计算:' . $local_amount_sum . ',当前:' . $data['payment_amount']);
		}
		return array('code' => 0, 'msg' => '核销成功');
	}

	public function batch_del_write_off_third_party(){
		$this->db = $this->load->database('china',true);
		$payment_id = isset($_POST['payment_id']) ? (int)$_POST['payment_id'] : 0;
		if(empty($payment_id)){
			echo json_encode(array('code' => 1, 'msg' => '参数错误'));
			return;
		}
		Model('biz_bill_payment_model');
		$old_row = $this->biz_bill_payment_model->get_one('id', $payment_id);

		if($old_row['lock_date'] !== '0000-00-00'){
			echo json_encode(array('code' => 1, 'msg' => '当前核销信息已审核,撤销核销失败'));
			return;
		}

		//2021-08-31 如果不是当前核销人不允许撤销核销 马嬿雯提出  http://wenda.leagueshipping.com/?/question/415
		if(!is_admin() && get_session('id') != $old_row['payment_user_id']){
			echo json_encode(array('code' => 1, 'msg' => '不是核销人，撤销核销失败'));
			return;
		}

		if(!empty($old_row)){
			//给该payment_id的账单修改为0
			$this->db->select('id, payment_id,from,billing_id');
			$bills = $this->db->where('payment_id = ' . $payment_id)->get('biz_bill_third_party')->result_array();
			if(empty($bills)){
				echo json_encode(array('code' => 1, 'msg' => '当前核销信息不存在账单,撤销核销失败'));
				return;
			}
			$from_db = $this->load->database($bills[0]['from'],true);
			$billing_ids = join(',',array_column($bills,'billing_id'));
			$billings = $from_db->where("id in ($billing_ids)")->get('biz_bill')->result_array();
			$from_db->where_in('id',array_column($billings,'payment_id'))->delete('biz_bill_payment');
			$from_db->where_in('id',array_column($bills,'billing_id'))->update('biz_bill',['payment_id'=>0]);
			$this->biz_bill_payment_model->mdelete($payment_id);

			$log_data = array();
			$log_data["table_name"] = "biz_bill_payment";
			$log_data["key"] = $payment_id;
			$log_data["action"] = 'delete';
			$log_data["value"] = json_encode($old_row);
			log_rcd($log_data);

			$bill_updates = array_map(function ($row){
				return array('id' => $row['id'], 'payment_id' => 0);
			}, $bills);
			$this->db->update_batch('biz_bill_third_party',$bill_updates,'id');

			$log_data = array();
			$log_data["table_name"] = "biz_bill_payment";
			$log_data["key"] = $payment_id;

			$log_data["action"] = 'payment_delete';
			$log_data["value"] = json_encode(array('ids' => array_column($bills, 'id')));
			log_rcd($log_data);
		}
		echo json_encode(array('code' => 0, 'msg' => '撤销成功'));
	}

	public function get_invoice($id = 0){
		$this->db = $this->load->database('china',true);
		$this->load->model('biz_bill_invoice_model');
		$data = $this->biz_bill_invoice_model->get_one('id', $id);
		echo json_encode(array('code' => 0, 'msg' => 'success' , 'data' => $data));
	}


}
