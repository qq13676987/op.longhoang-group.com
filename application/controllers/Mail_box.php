<?php

/**
 * 邮件通道控制器
 * Class mail_sender
 */
class   mail_box extends Menu_Controller
{
    public $Variable_content;
    public function __construct()
    {
        parent::__construct();

        $this->model = Model('mail_sender_model');
        $this->field_all = $this->model->field_all;//相关的ALL
        $this->field_edit = array(); //相关的可编辑字段
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }

        $this->table = 'mail_box';
        $this->field = [
            'mail_title'=>'邮件标题',
            'plan_send_time'=>'发送时间',
        ];

        $this->load->library('Variable_content');
        $this->Variable_content = new Variable_content();
        $this->Variable_content->setVariableRule("/\{(.*?)}/i");
        $this->Variable_content->setCleanRule("/[{|}]/");

        $this->load->helper('cz_shipment');
    }

    /**
     * @title 列表视图
     */
    public function index(){
        $data = array();
        $this->load->view('head');//引入easyui 头部JS文件等,如果这里完全不同,可以不用引入, 自己script引入即可
        $this->load->view("mail/mail_box/index_view", $data);//只是额外引入的js文件,直接加在这个对应页面即可
    }

    /**
     * @title 查看已发送邮件
     */
    public function read(){
        $id = (int)$this->input->get('id');
        $data = [];
        $count = 0;

        if ($id > 0){
            $data = $this->db->select('*')->where('id', $id)->get($this->table)->row_array();
            if ($data){
                //收件人数量
                $count = $this->db->select('count(*) as count')->where('mail_box_id', $id)->get('mail_receiver')->row_array()['count'];
            }
        }

        $this->load->vars('count', $count);
        $this->load->view('head');
        $this->load->view('mail/mail_box/read_view', ['data'=>$data]);
    }

    /**
     * 替换变量的值
     * @param string $str 内容
     * @param array $data 可传入的变量
     * @return string
     */
    private function replace_variable($str = '', $data = array()){
        //获取所有变量
        preg_match_all('/{([A-Za-z0-9._-]*)}/', $str, $replace_str_arr);
        foreach ($replace_str_arr[0] as $replace_str){
            $val_key = preg_replace ('/[{|}]/', '', $replace_str);//将变量从{abc} 处理为 abc
            $val = isset($data[$val_key]) ? $data[$val_key] : '';//获取参数里的内容

            $str = str_replace($replace_str, $val, $str);//将变量替换为值
        }
        return $str;
    }

    /**
     * 获取debug数据
     */
    public function getDebugInfo(){
        dump($this->data);
        dump($this->sql);
    }

    /**
     * 读取模板的待发送页面
     */
    public function read_template(){
        $data = array();

        $template_id = postValue('template_id', 0);
        $this->db->select("id,mail_title,mail_content,default_mail_sender_id");
        $row = Model('mail_template_model')->get_where_one("id = '{$template_id}'");
        if(empty($row)) exit('模板不存在');

        //获取传过来的变量
        $variable = postValue('variable', array());

        $data['template_id'] = $row['id'];
        $data['mail_title'] = $this->replace_variable($row['mail_title'], $variable);
        $data['mail_content'] = $this->replace_variable($row['mail_content'], $variable);
        $data['mail_sender_id'] = $row['default_mail_sender_id'];
        $data['ccs'] = postValue('ccs', '');
        $data['email'] = postValue('email', '');

        $this->load->view('head');
        $this->load->view('mail/mail_box/read_template_view', array('data'=>$data));
    }

    /**
     * 发送邮件
     */
    public function send_mail(){
        $mail_sender_id = postValue('mail_sender_id', 0);
        $template_id = postValue('template_id', 0);
        $ccs = postValue('ccs', array());
        $mail_title = postValue('mail_title', '');
        $mail_content = postValue('mail_content', '');
        $email = postValue('email',  array());
        $id_type = postValue('id_type', '');
        $id_no = postValue('id_no', '');
        if(empty($mail_sender_id)) return jsonEcho(array('code' => 1, 'msg' => '发件人不存在'));
        if(empty($email)) return jsonEcho(array('code' => 1, 'msg' => '收件人不存在'));
        if(empty($mail_title)) return jsonEcho(array('code' => 1, 'msg' => '标题不存在'));
        if(empty($mail_content)) return jsonEcho(array('code' => 1, 'msg' => '内容不存在'));
        $template = Model('mail_template_model')->get_where_one("id = '{$template_id}'");
        if($template_id == 10){
            if(!empty($_POST['hide_str'])){
                $mail_content = str_replace('<p><span style="color: rgb(225, 60, 57);"><strong>附件 </strong></span>(以下附件可点击下载)：</p>',$_POST['hide_str'],$mail_content);
            }
            $mail_content = str_replace('[a]',' ',$mail_content);
        }
        if(!empty($ccs)){
            if(gettype($ccs) == 'string'){
                $ccs = explode(',',$ccs);
            }
            $ccs = array_filter($ccs);
            foreach ($ccs as $val){
                if(!check_email($val)){
                    return jsonEcho(array('code' => 1, 'msg' => '请输入正确抄送人邮箱'));
                    die;
                }
            }
        }
        //2023-02-03 生成邮件前验证下通道是否能用
        $mail_sender = Model('mail_sender_model')->get_where_one("id = {$mail_sender_id}");
        if(empty($mail_sender)) return jsonEcho(array('code' => 1, 'msg' => '发件人邮箱配置不存在'));
        $this->load->library('Mail');
        $mail = new Mail();
        $mail::$SMTP_host = $mail_sender['smtp_host'];
        $mail::$SMTP_username = $mail_sender['smtp_username'];
        $mail::$SMTP_password = $mail_sender['smtp_password'];
        $mail::$SMTP_port = $mail_sender['smtp_port'];

        if($mail_sender['ssl_enable'] === '0') $is_ssl = false;
        else $is_ssl = true;
        $mail::$is_ssl = $is_ssl;
        try{
            $mail->testSmtpConnect();
            //连接成功正常走下去
        }catch (\Exception $e){
            return jsonEcho(array('code' => 1, 'msg' => '邮箱配置信息填写不正确, 发送失败'));
        }

        //2022-10-12 抄送人默认加一个自己
        // $ccs_arr = explode(',', $ccs);
        // $this_email = getUserField(get_session('id'), 'email');
        // if(!in_array($this_email, $ccs_arr)) $ccs .= ",$this_email";
        $ccs = join(",",$ccs);
        $email = join(",",$email);
        $mail_box_id = add_mail($email, $mail_title, $mail_content, $ccs, $template_id, "", $mail_sender_id, 1, $id_type, $id_no);


        //这里其实是 回调函数的意思，发送邮件后做什么动作
        if($template_id==28 && $id_type=='shipment'){
            $this->db->query("update `biz_bill_apply` set `status` = 0 where id_type = 'shipment' and id_no = '$id_no' and `status` = -2");
        }
        if($template_id==29 && $id_type=='consol'){
//            $this->db->query("update `biz_bill_apply` set `status` = 1 where id_type = 'consol' and id_no = '$id_no' and `status` = -2");
            //由于新状态的上限,这里状态默认为0
            $this->db->query("update `biz_bill_apply` set `status` = 0 where id_type = 'consol' and id_no = '$id_no' and `status` = -2");
        }

        return jsonEcho(array('code' => 0, 'msg' => '已添加到队列,正在发送中', 'data' => array('mail_box_id' => $mail_box_id)));
    }

    /**
     * @title 收件人
     */
    public function addressee(){
        $id = (int)$this->input->get('id');
        $data = $this->db->select('*')->where('mail_box_id', $id)->get('mail_receiver')->result_array();
        $this->load->view('mail/mail_box/addressee_view', ['data'=>$data]);
    }

    /**
     * @title 列表数据
     */
    public function get_data(){
        if (is_ajax())
        {
            $data = array('total'=>0, 'rows'=>[]);
            $page = isset($_POST['page']) && (int)$_POST['page'] > 0 ? (int)$_POST['page'] : 1;
            $rows = isset($_POST['rows']) && (int)$_POST['rows'] > 0 ? (int)$_POST['rows'] : 10;
            $sort = isset($_POST['sort'])?$_POST['sort']:'id';
            $order = isset($_POST['order'])?$_POST['order']:'desc';
            $count = $this->db->select('count(*) count')->get($this->table)->row_array()['count'];
            if ($count > 0)
            {
                $where = [];
                $param = ['template_id','mail_sender_id','email','mail_title','mail_content'];
                foreach ($param as $v){
                    if(isset($_POST[$v]) && !empty($_POST[$v])){
                        $val = $_POST[$v];
                        if($v == 'email'){
                            $where_or = [];
                            foreach (explode(',',$val) as $vv){
                                $where_or[] = "mail_receiver.email like '%{$vv}%'";
                            }
                            $where[] = '('.join(" or ",$where_or).')';
                        }
                        if($v == 'template_id'){
                            $where[] = "mail_box.template_id = $val";
                        }
                        if($v == 'mail_sender_id'){
                            $where[] = "mail_receiver.mail_sender_id = $val";
                        }
                        if($v == 'mail_title' || $v == 'mail_content'){
                            $where[] = "mail_box.$v like '%{$val}%'";
                        }
                    }
                }
                if(!empty($where)){
                    $where = join(' and ',$where);
                }
                $page_total = ceil($count / $rows);
                if ( $page > $page_total ) $page = $page_total;
                $pages = $rows * ($page-1);

                $field = "{$this->table}.id, {$this->table}.mail_title, {$this->table}.plan_send_time, {$this->table}.created_time, mail_receiver.email,mail_receiver.ccs,(select smtp_username from mail_sender where id = mail_receiver.mail_sender_id) as send_email, {$this->table}.mail_content, {$this->table}.template_id";
                $this->db->select($field)->limit($rows, $pages);
                $this->db->from($this->table);
                $this->db->join("mail_receiver", "mail_receiver.mail_box_id = {$this->table}.id", "left");
                $this->db->where($where);
                $this->db->order_by($sort,$order);
                $data['total'] = $count;
                $data['rows'] = $this->db->get()->result_array();
            }

            return $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    /**
     * @title 删除一条
     */
    public function delete_data(){
        $id = isset($_POST['id']) && (int)$_POST['id'] > 0 ? (int)$_POST['id'] : 0;
        $return = array('msg'   => '', 'code'  => 0);
        $data = [];

        if ($id > 0) {
            $data = $this->db->select('*')->where('id', $id)->get($this->table)->row_array();
            if (empty($data)){
                $return['msg'] = '参数无效！';
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            $this->db->where('id', $id);
            $query = $this->db->delete($this->table);

            if ($query){
                $return['code'] = 1;
            }else{
                $return['msg'] = '删除失败！';
            }
        }else{
            $return['msg'] = '参数无效！';
        }

        // 记录日志的例子
        $log_data = array();
        $log_data["table_name"] = $this->table;
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($data);//这里得查询下原本的完整数据
        log_rcd($log_data);

        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * 根据模板获取邮件的页面
     */
    public function template_get_mail(){
        $data = array();
        $data['mail_type'] = getValue('mail_type', '');
        $data['id'] = getValue('id', 0);

        $this->load->view('head');
        $this->load->view('/mail/mail_box/template_get_mail', $data);
    }

    /**
     * 获取相关的模板
     */
    public function get_template_by_mail_type(){
        $mail_type = getValue('mail_type', '');

        $rs = Model('mail_template_model')->get("mail_type = '{$mail_type}'");;

        $rows = array();
        foreach ($rs as $row){
            $rows[] = $row;
        }

        return jsonEcho($rows);
    }

    /**
     * 根据模板发送邮件, 内容根据模板id和传过来的id进行模板参数的数据替换
     */
    public function send_by_template(){
        $data = array();
        $input_value = array();
        $other_email = "";
        $lead_name = "";
        $user_id =  get_session('id');

        $template_id = getValue('template_id', 0);
        $master_table_id = getValue('id', 0);
        $mail_type = getValue('mail_type', '');
//        $reset = getValue('reset', 'false');
        //获取模板信息
        $template = Model('mail_template_model')->get_where_one("id = '{$template_id}'");
        if(empty($template)) exit("模板不存在");
        if($mail_type == 'shipment'){
            $data['shipment'] = $this->db->where('id',$master_table_id)->get('biz_shipment')->row_array();
        }
        //加载主键后转化内容
        $this->Variable_content->setMasterTableId($master_table_id);

        $mail_title = $this->Variable_content->get($template['mail_title'],false);
        $mail_content = $this->Variable_content->get($template['mail_content'],false);
        $arr = $this->Variable_content->getLoadedData();
        $ccs_other = '';
        $my_variable = array();
        //申请提单的邮件
        if($template_id==23){
            $client_code = $arr['shipment'][0]['client_code'];
            $sql = "select GROUP_CONCAT(id) as ids,client_code, sum(IF(type = 'cost', -local_amount,local_amount)) as sum_amount from biz_bill where payment_id = 0 and  client_code = '$client_code' and parent_id = 0";
            $query = $this->db->query($sql);
            $amount_row = $query->row_array();
            $amount = 0;
            if(!empty($amount_row)){
                $amount = $amount_row['sum_amount'];
            }
            $credit = $arr['shipment.client_code-client_code.client'][0]['credit_amount'];
            $over_amount = ($amount - $credit);
            if($over_amount<0) $over_amount = 0;
            if($credit < $amount){
                $my_variable['koudan_reason'] = "授信金额：$credit  未核销欠款：$amount ，已超出 $over_amount 元！";
            } else {
                $my_variable['koudan_reason'] = " ";
            }
            //授信日期超出
            $sql = "SELECT GROUP_CONCAT(bb.id) as ids,sum(bb.amount) as s,bill_ETD from biz_bill bb where type = 'sell' and payment_id = 0 and client_code = '{$client_code}' and overdue_date < now() order by bill_ETD asc";
            $query = $this->db->query($sql);
            $overdue= $query->row_array();
            if(!empty($overdue)){
                $my_variable['koudan_reason'] .= " 付款时间超出账期的金额是： {$overdue['s']}元! 最早1笔ETD: {$overdue['bill_ETD']}";
                if($over_amount<$overdue['s']) $over_amount= $overdue["s"];
            }
            if($amount_row['ids'] == '' && $overdue['ids'] == ''){
                $my_variable['invoice_no_list'] = '';
            }else{
                $ids = trim($amount_row['ids'].','.$overdue['ids'],',');
                $invoice_no_list = $this->db->query("select GROUP_CONCAT(invoice_no) as invoice_nos from biz_bill_invoice where id in (select invoice_id from biz_bill where id in ({$ids}))")->row_array();
                if(strlen($invoice_no_list['invoice_nos'])>25) $invoice_no_list['invoice_nos'] = substr($invoice_no_list['invoice_nos'],0,25)."...";
                $my_variable['invoice_no_list'] = $invoice_no_list['invoice_nos'];
            }
            if(strlen($my_variable['koudan_reason'])>2){
                $my_variable['overdue_amount'] =  $over_amount;
                $client = $this->db->query("select company_name from biz_client where client_code in (select client_code from biz_bill where id_type = 'shipment' and id_no = $master_table_id and type='sell') and over_credit = 1 and client_code != '{$client_code}'")->row_array();
                if(!empty($client)){
                    $my_variable['koudan_reason'] .= "<span style='color: red'> &emsp;关联扣单 {$client['company_name']}</span>";
                }
            }

            $biz_client_model = Model('biz_client_model');
            $client = $biz_client_model->get_one('client_code', $client_code);
            $my_variable['finance_describe'] = '';
            if($client['finance_od_basis'] == 'monthly'){
                $my_variable['finance_describe'] =  '离泊后  (month) 个月的 (day)日付款';
                $my_variable['finance_describe'] = str_replace('(month)', $client['finance_payment_month'], $my_variable['finance_describe']);
                $my_variable['finance_describe'] = str_replace('(day)', $client['finance_payment_day_th'], $my_variable['finance_describe']);

            }else if($client['finance_od_basis'] == 'daily'){
                $my_variable['finance_describe'] =  '离泊后  (day) 天付款 '  ;
                $my_variable['finance_describe'] = str_replace('(day)', $client['finance_payment_days'], $my_variable['finance_describe']);
            }
            //发给销售的领导
            $sales_user_id = $arr["shipment._shipment_duty_new_sales.biz_duty_new"][0]['user_id'];
            $leader = get_leader_row($sales_user_id);
            $other_email = $leader['email'];
            $lead_name =  $leader['name'];
        }
        //提成申请的邮件
        if($template_id==26){
            $month = date('Y-m-25',strtotime($_GET['start_time']));
            $late_fee = $this->db->query("select amount from bsc_user_express_fee where user_id = {$_GET['id']} and type='LATE_FEE' and date = '{$month}'")->row_array();
            if(empty($late_fee) || $late_fee['amount'] == 0){
                die('请联系 【马嬿雯】 帮你输入 '.substr($month,0,7).' 滞纳金后申请');
            }
            $commision_user = $this->db->where('id',$_GET['id'])->get('bsc_user')->row_array();
            $my_variable['sales_name'] = $commision_user['name'];
            $my_variable['sales_group_name'] = $this->db->where('group_code',$commision_user['group'])->get('bsc_group')->row_array()['group_name'];
            $my_variable['commision_month'] = date('Y年m月',strtotime($_GET['start_time']));
            $my_variable['payment'] = '已收款';
            $my_variable['commision1'] = $_GET['my_money'];
            $my_variable['commision2'] = $_GET['commision_total'];
            $my_variable['total'] = $my_variable['commision1'] + $my_variable['commision2'];
            //收件人是申请人的领导，抄送给口岸领导
            $leader = get_leader_row(get_session('id'));
            $other_email = $leader['email'];
            $lead_name =  $leader['name'];
            if($leader['m_level'] < 11){
                $other_leader = get_leader_row(get_session('id'),11);
                if($other_leader['id']!= 20057) $ccs_other = $other_leader['email'];
            }
        }
        //补费申请 shipment & consol
        if($template_id==28 || $template_id==29){
            if($template_id==28){
                $id_type = 'shipment';
            }else{
                $id_type = 'consol';
            }
//            $billings = $this->db->where(['id_type'=>$id_type,'id_no'=>$master_table_id])->where("status in (0,1,2,-2)")->get('biz_bill_apply')->result_array();
            //改为未拒绝或未结束流程的
            $billings = $this->db->select('biz_bill_apply.*')->where(['biz_bill_apply.id_type'=>$id_type,'biz_bill_apply.id_no'=>$master_table_id])->where("biz_bill_apply.status != -1 and biz_workflow_status.is_end = 0")->join('biz_workflow_status', "biz_bill_apply.id = biz_workflow_status.id_no and biz_workflow_status.id_type = 'biz_bill_apply'")->get('biz_bill_apply')->result_array();
            $str = '';
            $amount = 0;
            if(!empty($billings)){
                foreach ($billings as $k=>$v){
                    $new_amount = $v['amount'];
                    $cny_amount = $v['amount'];
                    if($v['type']=='cost')  $new_amount = 0- $v['amount'];
                    if($v['currency'] == 'CNY'){
                        $amount += $new_amount;
                        $cny_amount = $v['amount'];
                    }else{
                        $bill_currency = get_ex_rate(date("Y-m-d"),$v['currency'],'sell');
                        $amount += $new_amount * $bill_currency;
                        $cny_amount = $v['amount'] * $bill_currency;
                    }
                    $v['client_name'] = $this->db->query("select company_name from biz_client where client_code = '{$v['client_code']}'")->row_array()['company_name'];
                    $num = $k + 1;
                    if($v['id_type'] == 'shipment' && $v['type'] == 'cost'){
                        $bill_sell = $this->db->where("id_type = 'shipment' and id_no = {$v['id_no']} and charge_code = '{$v['charge_code']}'")->get('biz_bill')->result_array();
                        if(!empty($bill_sell)){
                            $bill_amount = 0;
                            foreach ($bill_sell as $sell){
                                if($sell['type'] == 'cost') $sell['local_amount'] = 0 - $sell['local_amount'];
                                $bill_amount += $sell['local_amount'];
                            }
                            $dif_amount = $bill_amount - $cny_amount;
                            $str .= "<p><span style='color:red;'>($num)</span> 类型:{$v['type']}</p><p>抬头:{$v['client_name']}</p><p>名称:{$v['charge']}</p><p>币种:{$v['currency']}</p><p>金额:{$v['amount']}</p><p>备注:{$v['remark']}</p><p style='color:green;'>有对应应收,差额为¥{$dif_amount}</p><br>";
                        }else{
                            $str .= "<p><span style='color:red;'>($num)</span> 类型:{$v['type']}</p><p>抬头:{$v['client_name']}</p><p>名称:{$v['charge']}</p><p>币种:{$v['currency']}</p><p>金额:{$v['amount']}</p><p>备注:{$v['remark']}</p><p style='color: red'>无对应应收</p><br>";
                        }
                    }else{
                        $str .= "<p><span style='color:red;'>($num)</span> 类型:{$v['type']}</p><p>抬头:{$v['client_name']}</p><p>名称:{$v['charge']}</p><p>币种:{$v['currency']}</p><p>金额:{$v['amount']}</p><p>备注:{$v['remark']}</p><br>";
                    }

                }
            }else{
                die('先到billing新增补费信息');
            }
            $sell_num = 0;
            $cost_num = 0;
            if($id_type == 'shipment'){
                $bills = $this->db->where(['id_type'=>'shipment','id_no'=>$master_table_id])->get('biz_bill')->result_array();
            }else{
                $sql = "select * from biz_bill where id_type = 'shipment' and id_no in(select id from biz_shipment where consol_id = '{$master_table_id}')";
                $bills = $this->db->query($sql)->result_array();
            }

            if(!empty($bills)){
                foreach ($bills as $v){
                    if($v['type'] == 'sell'){
                        $sell_num += $v['local_amount'];
                    }elseif($v['type'] == 'cost'){
                        $cost_num += $v['local_amount'];
                    }
                }
            }
            $my_variable['profit'] = $sell_num - $cost_num + $amount;
            $my_variable['list'] = $str;
            $my_variable['total_amount'] = $amount;

            if($template_id==28){
                //如果shipment发给销售的m8领导+抄送口岸领导
                $sales_user_id = $arr["shipment._shipment_duty_new_sales.biz_duty_new"][0]['user_id'];
                $leader = get_leader_row($sales_user_id);
                $other_email = $leader['email'];
                $lead_name =  $leader['name'];
                if($leader['m_level'] < 11){
                    $other_leader = get_leader_row($sales_user_id,11);
                    $ccs_other = $other_leader['email'];
                }
                //加个销售本人自己
                $other_email .= ",".getUserField($sales_user_id , 'email');
                //加申请的直属领导+当票操作的领导
                $current_leader =  get_leader_row($user_id);
                $other_email .= ",".$current_leader['email'];
                $operator_user = $this->db->query("select id,email from bsc_user where id = (select leader_id from bsc_user where id = (select user_id from biz_duty_new where id_type = 'biz_shipment' and id_no = {$master_table_id} and user_role = 'operator' ))")->row_array();
                $other_email .= ",".$operator_user['email'];
                //去除一下空值和重复
                $other_email = join(',', filter_unique_array(explode(',', $other_email)));
            }
            if($template_id==29){
                $user_info = get_leader_row($user_id);
                if($user_info){
                    $other_email = $user_info['email'];
                    $lead_name = $user_info['name'];
                }
            }
        }
        // pre-alert 邮件申请
        if($template_id==30){
            $shipment = $this->db->where('id',$master_table_id)->get('biz_shipment')->row_array();
            $consol = $this->db->where('id',$shipment['consol_id'])->get('biz_consol')->row_array();
            if($consol['trans_ETD'] < '2021-12-31 00:00:00'){
                die('请先填写ETD');
            }
            if($shipment['hbl_type'] == 'MBL'){
                die('必须是HBL才能发送');
            }
            $original = $this->db->where(['biz_table'=>'biz_consol','id_no'=>$shipment['consol_id']])->where("type_name like '%B/L%'")->order_by('id','desc')->get('bsc_upload')->row_array();
            $release = $this->db->where(['biz_table'=>'biz_shipment','id_no'=>$master_table_id])->where("type_name like '%B/L%'")->order_by('id','desc')->get('bsc_upload')->row_array();
            $debite_note = $this->db->where(['biz_table'=>'biz_shipment','id_no'=>$master_table_id,'type_name'=>"debite note"])->order_by('id','desc')->get('bsc_upload')->row_array();
            if(!empty($original)){
                $my_variable['mbl_bl_link'] = "<a href='/download?id_type=biz_consol&id_no={$shipment['consol_id']}&file_path=/upload{$original['file_name']}'>mbl_bl_link</a>";
            }else{
                die('请先上传MBL,HBL,DEBITE NOTE');
            }
            if(!empty($release)){
                $my_variable['hbl_bl_link'] = "<a href='/download?id_type=biz_shipment&id_no={$master_table_id}&file_path=/upload{$release['file_name']}'>hbl_bl_link</a>";
            }else{
                die('请先上传MBL,HBL,DEBITE NOTE');
            }
            if(!empty($debite_note)){
                $my_variable['debite_note_link'] = "<a href='/download?id_type=biz_shipment&id_no={$master_table_id}&file_path=/upload{$debite_note['file_name']}'>debite_note_link</a>";
            }else{
                die('请先上传MBL,HBL,DEBITE NOTE');
            }
            $duty = $this->db->where(['id_type'=>'biz_consol','id_no'=>$shipment['consol_id'],'user_role'=>'sales'])->get('biz_duty_new')->row_array();
            if($duty){
                $user_info = $this->db->where('id',$duty['user_id'])->get('bsc_user')->row_array();
                $other_email = $user_info['email'];
                $lead_name = $user_info['name'];
            }
        }

        //申请盯箱的邮件--start
        if($template_id == 12){
            //2022-12-19 收件人加上 市场, 操作, 操作主管
            //市场
            if(isset($arr["shipment.consol_id-id.consol._consol_duty_new_marketing.biz_duty_new.user_id-id.bsc_user"]) && !empty($arr["shipment.consol_id-id.consol._consol_duty_new_marketing.biz_duty_new.user_id-id.bsc_user"])) {
                $other_email .= ',' . $arr["shipment.consol_id-id.consol._consol_duty_new_marketing.biz_duty_new.user_id-id.bsc_user"][0]['email'];
            }
            //操作
            if(isset($arr["shipment._shipment_duty_new_operator.biz_duty_new.user_id-id.bsc_user"]) && !empty($arr["shipment._shipment_duty_new_operator.biz_duty_new.user_id-id.bsc_user"])) {
                $other_email .= ',' . $arr["shipment._shipment_duty_new_operator.biz_duty_new.user_id-id.bsc_user"][0]['email'];
                //操作主管
                $other_email .= ',' . getUserField($arr["shipment._shipment_duty_new_operator.biz_duty_new.user_id-id.bsc_user"][0]['leader_id'], 'email');
            }
            //去除一下空值和重复
            $other_email = join(',', filter_unique_array(explode(',', $other_email)));
            $my_variable['ys_amount'] = $this->db->query("select sum(local_amount) as CNY from biz_bill where id_type = 'shipment' and id_no = '{$master_table_id}' and charge_code = 'DXF2' and type = 'sell'")->row_array()['CNY'];
        }
        //申请盯箱的邮件--end

        //申请改单的邮件--start
        if($template_id == 46){
            //加上 销售, 操作主管
            if(isset($arr["shipment._shipment_duty_new_sales.biz_duty_new.user_id-id.bsc_user"]) && !empty($arr["shipment._shipment_duty_new_sales.biz_duty_new.user_id-id.bsc_user"])) {
                $other_email .= ',' . $arr["shipment._shipment_duty_new_sales.biz_duty_new.user_id-id.bsc_user"][0]['email'];
            }

            //操作
            if(isset($arr["shipment._shipment_duty_new_operator.biz_duty_new.user_id-id.bsc_user"]) && !empty($arr["shipment._shipment_duty_new_operator.biz_duty_new.user_id-id.bsc_user"])) {
                //操作主管
                $other_email .= ',' . getUserField($arr["shipment._shipment_duty_new_operator.biz_duty_new.user_id-id.bsc_user"][0]['leader_id'], 'email');
            }
            //去除一下空值和重复
            $other_email = join(',', filter_unique_array(explode(',', $other_email)));
        }
        //申请改单的邮件--end

        //预配通知的邮件--start
        if($template_id == 10){
            $other_email = $arr['shipment'][0]["client_email"];
            $consol_id = $arr['shipment'][0]["consol_id"];
            $my_variable['si_link'] = get_SI_temp_link($arr['shipment'][0]['id'], false);

            $consol = $this->db->where('id',$consol_id)->get('biz_consol')->row_array();
            if(empty($consol)){
                die('无consol');
            }
            if($consol['yupeiyifang'] == '0'){
                die('请先勾选预配勾');
            }

            $this->load->model("bsc_upload_model");
            $this->db->select('id,file_name,type_name');
            $this->db->order_by('id', 'asc');
            $uploads = array_column($this->bsc_upload_model->get("biz_table = 'biz_consol' and id_no = '{$arr['shipment'][0]['consol_id']}' and type_name in ('s/o', '配舱通知')"), null, 'type_name');
            $a = array();
            $variable['a'] = '';
            if(!empty($uploads)){
                foreach ($uploads as $upload){
                    $a[] = "<a href=\"/download?id_type=biz_consol&id_no={$consol_id}&file_path=/upload{$upload['file_name']}\" target=\"_blank\">{$upload['type_name']}</a>";
                }
                $variable['a'] = join('<br/>', $a);
            }

            $input_value['client_closing_date'] = $arr['shipment.consol_id-id.consol'][0]["closing_date"];
        }
        //预配通知的邮件--end

        //预录放行单的邮件--start
        if($template_id == 44){
            if(empty($data['shipment']['consol_id'])){
                die('无consol');
            }
            $upload1 = $this->db->where(['biz_table'=>'biz_shipment','type_name'=>'customs_release','id_no'=>$master_table_id])->get('bsc_upload')->row_array();
            $upload2 = $this->db->where(['biz_table'=>'biz_shipment','type_name'=>'customs_document','id_no'=>$master_table_id])->get('bsc_upload')->row_array();
            $upload3 = $this->db->where(['biz_table'=>'biz_shipment','type_name'=>'customs_proxy','id_no'=>$master_table_id])->get('bsc_upload')->row_array();
            if(empty($upload1) && empty($upload2) && empty($upload3)){
                die('无预录放行单');
            }
            $link = '<div>';
            if(!empty($upload1)){
                $link .= "<a href='".base_url()."download?id_type=biz_shipment&id_no={$master_table_id}&file_path=/upload{$upload1['file_name']}'>放行单</a><br>";
            }
            if(!empty($upload2)){
                $link .= "<a href='".base_url()."download?id_type=biz_shipment&id_no={$master_table_id}&file_path=/upload{$upload2['file_name']}'>预录单</a><br>";
            }
            if(!empty($upload3)){
                $link .= "<a href='".base_url()."download?id_type=biz_shipment&id_no={$master_table_id}&file_path=/upload{$upload3['file_name']}'>委托书</a><br>";
            }
            $link .= "</div>";
            $my_variable['link'] = $link;
            $other_email = $data['shipment']['client_email'];
        }
        //预录放行单的邮件--end

        //邮箱验证的邮件--start
        if($template_id == 48){
            $mail_box = $this->db->where("template_id = 48 and id_type = 'biz_client_contact_list' and id_no = $master_table_id")->order_by('created_time','desc')->get('mail_box')->row_array();
            if(!empty($mail_box)){
                if((time() - strtotime($mail_box['created_time'])) < 3600*72){
                    die($mail_box['created_time']." Mail alread sent,please resend it in three days if you want。");
                }
            }
            $source = get_system_type();
            $contact = $this->db->where('id',$master_table_id)->get('biz_client_contact_list')->row_array();
            $verify_email = $contact['email'];
            $my_variable['company_name'] = $this->db->query("select company_name from biz_client where client_code = (select client_code from biz_client_contact_list where id = $master_table_id)")->row_array()['company_name'];
            $my_variable['link'] = "http://client.longhoang-group.com/api/email_verify?rand={$master_table_id}&source={$source}&user_id=$user_id&email={$verify_email}&key=".md5(date("Y-m-d")."cz");
            $other_email = $verify_email;
        }
        //邮箱验证的邮件--end

        //一键催操作报关单上传--start
        if($template_id == 50){
            $other_email = $this->db->query("select email from bsc_user where id = (select user_id from biz_duty_new where id_type = 'biz_shipment' and id_no = $master_table_id and user_role = 'operator' limit 1)")->row_array()['email'];
        }
        //一键催操作报关单上传--end

        $my_variable['other_email'] =  $other_email;
        $my_variable['lead_name'] =  $lead_name;
        // print_r($arr);
        $this->Variable_content->setInitialVariable($my_variable);
        $mail_title = $this->Variable_content->get($template['mail_title']);
        $mail_content = $this->Variable_content->get($template['mail_content']);
        //$this->Variable_content->getDebugInfo();


        //通道不使用默认的,先读取,如果当前用户的邮箱有进行
        // $this->Variable_content->getDebugInfo();
        $this_email = getUserField(get_session('id'), 'email');
        $where = array();
        $where[] = "smtp_username in ('{$this_email}', 'service01@vip.longhoang-group.com')";//auto的自动开放,再加上他们自己的 
        $where = join(' and ' , $where);

        $this->db->select('id,smtp_username');
        $sender = Model('mail_sender_model')->get_where_one($where);
        if(isset($sender['smtp_username'])){
            $mail_sender_id = $sender['id'];
        }else{
            $mail_sender_id = $template['default_mail_sender_id'];
        }
        $ccs = $template['default_ccs'];
        if(!empty($ccs_other)){
            $ccs = $ccs.';'.$ccs_other;
        }
        $email = $template['default_receiver'].','.$other_email;
        $email = trim($email,',');
        //暂存和初始化不要了
//        //如果不重置
//        if($reset === 'false'){
//            //查询当前是否有未发送的
//            $this->db->select("mail_box.template_id,mail_box.mail_title,mail_box.mail_content,mail_receiver.mail_sender_id");
//            $this->db->join("mail_receiver", 'mail_receiver.mail_box_id = mail_box.id', 'right');
//            $box = Model('mail_box_model')->get("mail_box.template_id = '{$template_id}' and mail_box.mail_status = 0 and mail_box.created_by = '{$user_id}' and mail_receiver.send_status = 0");
//            if(!empty($box)){
//                $mail_title = $box['mail_title'];
//                $mail_content = $box['mail_content'];
//                $mail_sender_id = $box['mail_sender_id'];
//                $email = $box['email'];
//                $ccs = $box['ccs'];
//            }
//        } 
        $data['mail_title'] = $mail_title;
        $data['mail_content'] = $mail_content;
        $data['template_id'] = $template_id;
        $data['mail_sender_id'] = $mail_sender_id;
        $data['email'] = $email;
        $data['ccs'] = $ccs;
        $data['mail_type'] = $mail_type;
        $data['id'] = $master_table_id;
        $data['mail_template_input'] = $this->db->where('template_id',$template_id)->get('mail_template_input')->result_array();
        $tmp_content = $data['mail_content'];
        if(!empty($data['mail_template_input'])){
            foreach ($data['mail_template_input'] as $v){
                $str = "[#".$v['input_name_en'].']';
                $tmp_content = str_replace($str,"<span id='{$v['input_name_en']}{$v['input_name_en']}'>{$str}</span>",$tmp_content);
            }
        }
        $data['mail_content'] = $tmp_content;
        $data['input_value'] = $input_value;
        $data['template'] = $template;
        if($template_id == 10){
            $data['mail_content'] = str_replace('[a]','<strong>[a]</strong>',$data['mail_content']);
        }
        $this->load->view('head');
        $this->load->view('/mail/mail_box/send_by_template', $data);
    }

    /**
     * 历史记录
     */
    public function history(){
        $id_type = getValue('id_type', '');
        $id_no = getValue('id_no', 0);

        $data = array();
        $data['id_type'] = $id_type;
        $data['id_no'] = $id_no;

        $this->load->view("head");
        $this->load->view("/mail/mail_box/history", $data);
    }

    /**
     * 表格获取数据
     */
    public function get_history_data()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "mail_box.id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $id_type = getValue('id_type', '');
        $id_no = getValue('id_no', '0');
        $user_id = get_session('id');

        //-------这个查询条件想修改成通用的 -------------------------------
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $where = array();

        if($id_type !== '' && $id_no !== '0'){
            $where[] = "mail_box.id_type = '{$id_type}'";
            $where[] = "mail_box.id_no = {$id_no}";
        }else{
            $where[] = "mail_box.created_by = $user_id";
        }


        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = trim($fields['s'][$key]);
                $v = trim($fields['v'][$key]);
                if(!in_array($f, $field)) continue;
                if($v != '')$where[] = search_like($f, $s, $v);
            }
        }

        $where = join(' and ', $where);
        //-----------------------------------------------------------------


        $offset = ($page - 1) * $rows;
        $this->db->join("mail_receiver", 'mail_receiver.mail_box_id = mail_box.id', 'right');
        $this->db->join("mail_sender", 'mail_receiver.mail_sender_id = mail_sender.id', 'left');
        $result["total"] = Model('mail_box_model')->total($where);
        $this->db->limit($rows, $offset);
        $this->db->join("mail_receiver", 'mail_receiver.mail_box_id = mail_box.id', 'right');
        $this->db->join("mail_sender", 'mail_receiver.mail_sender_id = mail_sender.id', 'left');
        $this->db->select('mail_box.id,mail_box.mail_title,mail_box.mail_content,(select name from bsc_user where bsc_user.id = mail_box.created_by) as created_by_name, ' .
            'mail_box.created_time,mail_receiver.email,mail_receiver.send_status,mail_receiver.ccs,mail_receiver.send_result,mail_sender.smtp_username' .
            '');
        $rs = Model('mail_box_model')->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            switch ($row['send_status']){
                case 0:
                    $row['send_result'] = '';
                    $row['send_status'] = '<font color="#f4a460">排队发送</font>';
                    break;
                case 1:
                    $row['send_result'] = '';
                    $row['send_status'] = '<font color="green">成功发送</font>';
                    break;
                case 2:
                    $row['send_result'] = '';
                    $row['send_status'] = '<font color="blue">正在发送中...</font>';
                    break;
                case -1:
                    $row['send_status'] = '<font color="red">发送失败</font>';
                    break;
            }
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    /**
     * 查看当前申请盯箱是否发送了
     * @return bool
     */
    public function check_sqdx_mail($template_id=12){
        $id_type = postValue('id_type', '');
        $id_no = postValue('id_no', '');
        return jsonEcho($this->check_mail_isset($template_id, $id_type, $id_no));
    }

    /**
     * 判断shipment或者consol里 某个邮件是否发送过
     * @param int $template_id  模板ID
     * @param $id_type
     * @param $id_no
     * @return array
     */
    private function check_mail_isset($template_id, $id_type, $id_no){
        $this->db->join('mail_box', 'mail_box.id = mail_receiver.mail_box_id', 'LEFT');
        $this->db->select('mail_receiver.send_status,mail_receiver.send_result');
        $this->db->order_by('mail_receiver.id', 'desc');//查最新的
        $mail = Model('mail_receiver_model')->get_where_one("mail_box.id_type = '{$id_type}' and mail_box.id_no = $id_no and mail_box.template_id = {$template_id}");
        //不存在邮件
        if(empty($mail)) return array('code' => 1, 'msg' => lang('未发送邮件'));

        //这里由于查的一条,如果他们发多次的话,最新那条发送失败了,还是得重新发一封的,就不改了
        //存在邮件时
        if($mail['send_status'] === '-1') return array('code' => 1, 'msg' => lang('发送失败, 原因:') . $mail['send_result']);
        if($mail['send_status'] === '0') return array('code' => 2, 'msg' => lang('正在发送队列中,请等待几分钟后再试'));
        if($mail['send_status'] === '2') return array('code' => 2, 'msg' => lang('邮件正在发送中,请稍后再试'));

        //邮件发送成功
        if($mail['send_status'] === '1') return array('code' => 0, 'msg' => lang('发送成功'));
        //其他情况都不给通过
        return array('code' => 1, 'msg' => lang('邮件状态错误'));
    }
}