<?php

class biz_container extends Menu_Controller
{
    protected $menu_name = 'container';//菜单权限对应的名称
    protected $method_check = array(//需要设置权限的方法
        'index',
        'add_data',
        'update_data',
        'delete_data',
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_container_model');
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
        $this->load->helper('cz_container');
    }

    public $field_edit = array();

    public $field_all = array(
        array("id", "ID", "80", "1"),
        array("container_no", "container_no", "100", "2"),
        array("seal_no", "seal_no container", "120", "3"),
        array("container_size", "container_size", "120", "4"),
        array("container_tare", "container_tare", "120", "4"),
        array("vgm", "vgm", "60", "4"),
        array("created_by", "created_by", "150", "10"),
        array("created_time", "created_time", "150", "10"),
        array("updated_by", "updated_by", "150", "10"),
        array("updated_time", "updated_time", "150", "11")
    );

    public function index($consol_id = "", $si = "")
    {
        $data = array();

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_container_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }
        // page account
        $rs = $this->sys_config_model->get_one('biz_container_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }
        $this->load->model('biz_consol_model');
        $consol = $this->biz_consol_model->get_one('id', $consol_id);
        if (empty($consol)) {
            echo "consol不存在,无法编辑";
            $data['lock_lv'] = 9999;
        } else {
            $data['lock_lv'] = $consol['lock_lv'];
//            if ($consol['operator_si'] != '0') {
//                $data['lock_lv'] = 3;
//                echo "<span style='color:red;'>操作SI已勾选,无法修改箱信息</span>";
//            }
        }

        $data["consol_id"] = $consol_id;

        //2022-07-19 新增
        $data['si'] = $si;

        $this->load->view('head');
        $this->load->view('biz/container/index_view', $data);
    }

    public function get_data($consol_id = "", $si = "")
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------这个查询条件想修改成通用的 -------------------------------
        $f1 = 'consol_id';
        $s1 = '=';
        $v1 = "$consol_id";
        $f2 = 'container_no';
        $s2 = 'like';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';

        $where = "";
        if ($v1 != "") $where = "$f1 $s1 '$v1'";
        if ($v2 != "" && $where != "") $where .= " and " . search_like($f2, $s, $v2);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->biz_container_model->total($where);
        $this->db->limit($rows, $offset);
        $rs = $this->biz_container_model->get($where, $sort, $order);
        $this->load->model("m_model");
        $rows = array();

        foreach ($rs as $row) {
            $sql = "SELECT SUM(biz_shipment_container.`packs`) AS packs, vgm,SUM(biz_shipment_container.`weight`) AS weight, SUM(biz_shipment_container.`volume`) AS volume FROM biz_shipment, biz_shipment_container
WHERE `biz_shipment_container`.`shipment_id` = `biz_shipment`.id AND `biz_shipment`.`consol_id` = " . $row["consol_id"] . " AND `biz_shipment_container`.`container_no` = '" . $row["container_no"] . "'";
            $temp_rs = $this->m_model->sqlquery($sql);
            $temp_row = $temp_rs->row_array();

            $row["packs"] = 0;
            $row["weight"] = 0;
            $row["volume"] = 0;
            $row["vgm"] = 0;
            if ($temp_row["packs"] != null) {
                $row["packs"] = $temp_row["packs"];
                $row["weight"] = $temp_row["weight"];
                $row["volume"] = $temp_row["volume"];
                $row["vgm"] = $temp_row["vgm"];
            }
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data($consol_id = "")
    {
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($temp != "")
                $data[$item] = $temp;
        }
        $data['consol_id'] = $consol_id;
        $this->load->model('biz_consol_model');
        $limit = cz_consol_container_limit($data, $consol_id);
        if ($limit === true) {
            $id = $this->biz_container_model->save($data);
            $data['id'] = $id;
            $this->container_handle($data, $consol_id);


            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_container";
            $log_data["key"] = $id;
            $log_data["action"] = "insert";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
            echo json_encode($data);
        } else {
            $data['msg'] = $limit;
            $data['isError'] = true;
            echo json_encode($data);
        }
    }

    public function update_data()
    {
        $id = $_REQUEST["id"];
        $old_row = $this->biz_container_model->get_by_id($id);

        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            $data[$item] = $temp;
        }
        $this->load->model('biz_consol_model');
        $limit = $this->container_limit($data, $old_row['consol_id'], $id);
        if ($limit === true) {
            $id = $this->biz_container_model->update($id, $data);
            $data['id'] = $id;
            echo json_encode($data);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_container";
            $log_data["key"] = $id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        } else {
            $data['msg'] = $limit;
            $data['isError'] = true;
            echo json_encode($data);
        }
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->biz_container_model->get_by_id($id);
        $this->biz_container_model->mdelete($id);
        echo json_encode(array('success' => true));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_container";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
    }

    private function container_handle($data, $consol_id)
    {
        //查询是否有对应consol
        if ($consol_id == 0) {//未绑定的
            return false;
        } else {
            $this->load->model('biz_container_model');
            $this->load->model('biz_shipment_container_model');
            $this->db->select('SUM(packs) as packs, SUM(weight) as weight, SUM(volume) as volume');
            $this->db->where("container_no = '{$data['container_no']}'");
            $sum_container = $this->biz_shipment_container_model->get_one('shipment_id in', '(select id from biz_shipment where consol_id = ' . $consol_id . ')', false);
            $container_update = array(
                'packs' => $sum_container['packs'],
                'weight' => $sum_container['weight'],
                'volume' => $sum_container['volume'],
            );
            if (empty($data['container_size']) && isset($_POST['container_size']) && $_POST['container_size'] != '') $container_update['container_size'] = $_POST['container_size'];
            if (isset($_POST['seal_no']) && $_POST['seal_no'] != '') $container_update['seal_no'] = $_POST['seal_no'];
            $this->biz_container_model->update($data['id'], $container_update);

            //save the operation log
            $log_data = array();
            $log_data["table_name"] = "biz_container";
            $log_data["key"] = $data['id'];
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($container_update);
            log_rcd($log_data);

            return true;
        }
    }
}
