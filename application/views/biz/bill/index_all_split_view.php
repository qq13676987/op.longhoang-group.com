<script type="text/javascript" src="/inc/js/easyui/datagrid-export.js"></script>
<style type="text/css">
    .hide{
        display: none;
    }
    .f{
        width:120px;
    }
    .s{
        width:100px;
    }
    .v{
        width:120px;
    }
    .del_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }
    .hidden_invoice_input{
        display: none;
    }
</style>
<!--这里是查询框相关的代码-->
<script>
    var query_option = {};//这个是存储已加载的数据使用的

    var table_name = 'biz_bill',//需要加载的表名称
        view_name = 'biz_bill_index';//视图名称
    var add_tr_str = "";//这个是用于存储 点击+号新增查询框的值, load_query_box会根据接口进行加载
    var more_options = [{field:'consol.job_no', title:'consol_no', editor:'textbox', editor_config:{multiline:true}},{field:'shipment_by_consol.job_no', title:'<?= lang('shipment no');?>', editor:'textbox', editor_config:{multiline:true}}, {field:'consol.carrier_ref', title:'MBL(CONSOL)', editor:'textbox', editor_config:{multiline:true}}];//这个是用于存储 点击+号新增查询框的值, load_query_box会根据接口进行加载


    /**
     * 加载查询框
     */
    function load_query_box(){
        ajaxLoading();
        $.ajax({
            type:'POST',
            url: '/sys_config_title/get_user_query_box?view_name=' + view_name + '&table_name=' + table_name,
            data: {
                more_options:more_options,
            },
            dataType:'json',
            success:function (res) {
                ajaxLoadEnd();
                if(res.code == 0){
                    //加载查询框
                    $('#cx table').append(res.data.box_html);
                    //需要调用下加载器才行
                    $.parser.parse($('#cx table tbody .query_box'));

                    add_tr_str = res.data.add_tr_str;
                    query_option = res.data.query_option;
                    $.each(more_options, function (i, it) {
                        query_option[it.field] = it;
                    });
                    var table_columns = [[
                        {field:'ck', checkbox: true, title:'全选'},
                    ]];//表的表头

                    //填充表头信息
                    $.each(res.data.table_columns, function (i, it) {
                        table_columns[0].push({field:it.table_field, title:it.title, width:it.width, sortable: it.sortable, formatter: get_function(it.table_field + '_for'), styler: get_function(it.table_field + '_styler')});
                    });

                    //渲染表头

                    $('#tt').datagrid({
                        width:'auto',
                        height: $(window).height(),
                        columns:table_columns,
                        onSelect:function(index ,row){
                            get_statistics();
                        },
                        onUnselect:function(index ,row){
                            get_statistics();
                        },
                        onSelectAll:function(index ,row){
                            get_statistics();
                        },
                        onUnselectAll:function(index ,row){
                            get_statistics();
                        },
                        onDblClickRow:function (index, row) {
                            if(row.id_type == 'shipment'){
                                window.open("/biz_shipment/edit/" + row.id_no);
                            }else if(row.id_type == 'consol'){
                                window.open("/biz_consol/edit/" + row.id_no);
                            }
                        }
                    });
                    $(window).resize(function () {
                        $('#tt').datagrid('resize');
                    });

                    //为了避免初始加载时, 重复加载的问题,这里进行了初始加载ajax数据
                    var aj = [];
                    $.each(res.data.load_url, function (i,it) {
                        aj.push(get_ajax_data(it, i));
                    });
                    //加载完毕触发下面的下拉框渲染
                    //$_GET 在other.js里封装
                    var this_url_get = {};
                    $.each($_GET, function (i, it) {
                        this_url_get[i] = it;
                    });
                    $.when.apply($,aj).done(function () {
                        //这里进行字段对应输入框的变动
                        $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                            var ec_value = $(ec_item).combobox('getValue');
                            var v_inp = $('.v:eq(' + ec_index + ')');
                            var s_val = $('.s:eq(' + ec_index + ')').combobox('getValue');
                            var v_val = v_inp.textbox('getValue');

                            $(ec_item).combobox('clear').combobox('select', ec_value);
                            //这里比对get数组里的值
                            $.each(this_url_get, function (trg_index, trg_item) {
                                //切割后,第一个是字段,第二个是符号
                                var trg_index_arr = trg_index.split('-');
                                //没有第二个参数时,默认用等于
                                if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';
                                //用一个删一个
                                //如果当前的选择框的值,等于get的字段值
                                if(ec_value === trg_index_arr[0] && s_val === trg_index_arr[1]){
                                    v_val = trg_item;//将v改为当前get的
                                    // $('.s:eq(' + ec_index + ')').combobox('setValue', trg_index_arr[1]);//设置S的值
                                    delete this_url_get[trg_index];
                                }
                            });
                            //没找到就正常处理
                            $('.v:eq(' + ec_index + ')').textbox('setValue', v_val);
                        });
                        //判断是否有自动查询参数
                        var auto_click = this_url_get.auto_click;
                        var no_query_get = ['auto_click'];//这里存不需要塞入查询的一些特殊变量
                        $.each(no_query_get, function (i,it) {
                            delete this_url_get[it];
                        });
                        //完全没找到的剩余值,在这里新增一个框进行选择
                        $.each(this_url_get, function (trg_index, trg_item) {
                            add_tr();//新增一个查询框
                            var trg_index_arr = trg_index.split('-');
                            //没有第二个参数时,默认用等于
                            if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';

                            $('#cx table tbody tr:last-child .f').combobox('setValue', trg_index_arr[0]);
                            $('#cx table tbody tr:last-child .s').combobox('setValue', trg_index_arr[1]);
                            $('#cx table tbody tr:last-child .v').textbox('setValue', trg_item);
                        });

                        //这里自动执行查询,显示明细
                        if(auto_click === '1'){
                            doSearch();
                        }
                    });
                }else{
                    $.messager.alert("<?= lang('提示');?>", res.msg);
                }
            },error:function (e) {
                ajaxLoadEnd();
                $.messager.alert("<?= lang('提示');?>", e.responseText);
            }
        });
    }

    /**
     * 新增一个查询框
     */
    function add_tr() {
        $('#cx table tbody').append(add_tr_str);
        $.parser.parse($('#cx table tbody tr:last-child'));
        var last_f = $('#cx table tbody tr:last-child .f');
        var last_f_v = last_f.combobox('getValue');
        last_f.combobox('clear').combobox('select', last_f_v);
        return false;
    }

    /**
     * 删除一个查询
     */
    function del_tr(e) {
        //删除按钮
        var index = $(e).parents('tr').index();
        $(e).parents('tr').remove();
        return false;
    }

    //执行加载函数
    $(function () {
        load_query_box();
    });
</script>
<!--easyui 使用的一些不太重要的函数-->
<script>
    function invoice_type_for(value,row,index){
        var str = '';
        var this_val = row.invoice_type;
        if(this_val == '2'){
            str = 'D/N';
        }
        if(this_val == '1001'){
            str = 'D/N';
        }
        if(this_val == '999'){
            str = 'D/N';
        }
        if(this_val == '1'){
            str = 'D/N';
        }
        if(this_val == '0'){
            str = 'D/N';
        }
        return str;
    }

    function commision_flag_for(value, row, index){
        var str = '';
        var this_val = row.commision_flag;
        if(this_val == 1){
            str = row.commision_month;
            //切成月份
            console.log(this_val);
            str = str.substring(0,7);
        }
        if(this_val == 0){
            str = 'Not commission';
        }
        return str;
    }

    function id_type_for(value, row, index){
        var str = value;
        if(row.is_special == 1){
            str += '(专项)';
        }else{

        }
        return str;
    }
</script>
<script type="text/javascript">
    function doSearch() {
        var title = $('#query_title').combobox('getValue');
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        $('#tt').datagrid({
            url:'/biz_bill/get_data_all?title=' + title + '<?php if($is_special == 1) echo '&is_special=1'?>',
            clearSelections:true,
            rowStyler:function(index,row){
                if(row['invoice_date'] != null){
                    // return 'background-color:#A8A8A8;';
                }else if(row['payment_date'] != null){
                    // return 'background-color:#A8A8A8;';
                }else if(row['lock_date'] != null){
                    return 'background-color:#A8A8A8;';
                }

            },
            queryParams: json
        }).datagrid('clearSelections');
        set_config();
        $('#chaxun').window('close');
        get_statistics();
        return false;
    }

    function config(){
        window.open("/sys_config_title/index_user?table_name=" + table_name + "&view_name=" + view_name) ;
    }

    function cktz_excel() {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        var rows = $('#tt').datagrid('getSelections');
        json['ids'] = [];
        $.each(rows, function (index, item) {
            json['ids'].push(item.id);
        });
        json['ids'] = json['ids'].join(',');
        // GET方式示例，其它方式修改相关参数即可
        var form = $('<form></form>').attr('action','/excel/export_excel?file=<?php if($is_special == 1)echo 'cktz2_v2'; else echo 'cktz1_v2';?>').attr('method','POST');

        $.each(json, function (index, item) {
            if(typeof item === 'string'){
                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
            }else{
                $.each(item, function(index2, item2){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                })
            }
        });

        form.appendTo('body').submit().remove();
    }

    function znj_excel() {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        var is_export = true;
        var end_each = false;
        // var status_exp = 0;
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        // GET方式示例，其它方式修改相关参数即可
        var form = $('<form></form>').attr('action','/excel/export_excel?file=znj_v2').attr('method','POST');

        $.each(json, function (index, item) {
            if(typeof item === 'string'){
                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
            }else{
                $.each(item, function(index2, item2){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                })
            }
        });

        form.appendTo('body').submit().remove();
    }

    function cktz2_excel() {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        var is_export = false;
        // var status_exp = 0;
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
            if(item.value === 'biz_bill.client_code'){
                if(cx_data[index + 2].value !== ''){
                    is_export = 1;
                }
            }
            if(item.value === 'biz_bill.client_code_name'){
                if(cx_data[index + 2].value !== ''){
                    is_export = 1;
                }
            }
        });
        if(is_export !== 1){// && status_exp !== 2
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请填写{client_code_name}或{client_code}', array('client_code_name' => lang('client_code_name'), 'client_code' => lang('client_code')));?>');
            return;
        }
        // GET方式示例，其它方式修改相关参数即可
        var form = $('<form></form>').attr('action','/excel/export_excel?file=<?php if($is_special == 1)echo 'cktz_hw2_v2'; else echo 'cktz_hw1_v2';?>').attr('method','POST');

        $.each(json, function (index, item) {
            if(typeof item === 'string'){
                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
            }else{
                $.each(item, function(index2, item2){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                })
            }
        });

        form.appendTo('body').submit().remove();
    }

    function bill_details() {
        var param = {};
        var tt = $('#tt');
        param.filename = '账单明细.xls';
        param.rows = tt.datagrid('getSelections');
        if(param.rows.length == 0){
            param.rows = tt.datagrid('getData').originalRows;
        }else{
            param.worksheet = '账单明细';
        }
        tt.datagrid('toExcel', param);
    }

    function resetSearch(){
        $('.v').textbox('reset');
    }

    function get_statistics(){
        var select = $('#tt').datagrid('getSelections');
        var ids = [];
        var row = {};
        $('#statistics_fm').form('clear');

        if(select.length > 0){
            var title = select[0].type;
            $.each(select, function (index, item) {
                ids.push(item.id);
                var amount = parseInt(Math.round(Number(item['amount']) * 100));
                if(item.type == 'cost'){
                    amount = - amount;
                }
                if(row[item.currency + "_amount"] == undefined) row[item.currency + "_amount"] = 0;
                if(row[item.currency + "_invoice_amount"] == undefined) row[item.currency + "_invoice_amount"] = 0;
                if(row[item.currency + "_actual_amount"] == undefined) row[item.currency + "_actual_amount"] = 0;
                if(row[item.currency + "_not_invoice_amount"] == undefined) row[item.currency + "_not_invoice_amount"] = 0;
                if(row[item.currency + "_not_actual_amount"] == undefined) row[item.currency + "_not_actual_amount"] = 0;

                row[item.currency + "_amount"] += amount != null ? amount : 0;
                if(item['invoice_id'] != 0) row[item.currency + "_invoice_amount"] += amount;
                else row[item.currency + "_not_invoice_amount"] += amount;
                if(item['payment_id'] != 0) row[item.currency + "_actual_amount"] += amount;
                else row[item.currency + "_not_actual_amount"] += amount;
            });
        }

        ids = ids.join(',');
        $('#cx input[name="ids"]').val(ids);
        $.each(row, function(i, v){
            $('#' + i).numberbox('setValue', v / 100);
        });
        // $('#cny_amount').numberbox('setValue', cny_amount);
        // $('#cny_invoice_amount').numberbox('setValue', cny_invoice_amount);
        // $('#cny_not_invoice_amount').numberbox('setValue', cny_not_invoice_amount);
        // $('#cny_actual_amount').numberbox('setValue', cny_actual_amount);
        // $('#cny_not_actual_amount').numberbox('setValue', cny_not_actual_amount);
        // $('#usd_amount').numberbox('setValue', usd_amount);
        // $('#usd_invoice_amount').numberbox('setValue', usd_invoice_amount);
        // $('#usd_not_invoice_amount').numberbox('setValue', usd_not_invoice_amount);
        // $('#usd_actual_amount').numberbox('setValue', usd_actual_amount);
        // $('#usd_not_actual_amount').numberbox('setValue', usd_not_actual_amount);

        // //aed
        // $('#aed_amount').numberbox('setValue', aed_amount);
        // $('#aed_invoice_amount').numberbox('setValue', aed_invoice_amount);
        // $('#aed_not_invoice_amount').numberbox('setValue', aed_not_invoice_amount);
        // $('#aed_actual_amount').numberbox('setValue', aed_actual_amount);
        // $('#aed_not_actual_amount').numberbox('setValue', aed_not_actual_amount);
    }

    function confirm_date(){
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            var form_data = {};
            form_data['ids'] = [];
            $.each(rows, function (index, item) {
                form_data['ids'].push(item.id);
            });

            $('#confirm_date_form').form('load', form_data);
            $('#confirm_date_div').dialog('open');
        }else {
            $.messager.alert('<?= lang('Tips')?>', '没有数据');
        }

    }

    var is_submit = false;
    function save_confirm_date() {
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $('#confirm_date_form').form('submit', {
            url: '/biz_bill/batch_confirm_date',
            onSubmit: function () {
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                    is_submit = false;
                }
                return validate;
            },
            success: function (res_json) {
                var res;
                try{
                    res = $.parseJSON(res_json);
                }catch(e){
                }
                if(res == undefined){
                    ajaxLoadEnd();
                    is_submit = false;
                    $.messager.alert('Tips', '发生错误');
                    return;
                }
                $('#confirm_date_div').window('close');
                ajaxLoadEnd();
                is_submit = false;
                $.messager.alert('<?= lang('Tips')?>', res.msg);
                if(res.code == 0){
                    $('#tt').datagrid('reload');
                }else{
                    $('#confirm_date_div').dialog('open');
                }

            }
        });
    }

    //核销 核销对帐号， 发票号
    var invoice_info = {};
    function invoice() {
        var row = $('#tt').datagrid('getSelections');
        var title = $('#query_title').combobox('getValue');
        var result = '';
        if(row.length > 0){
            var ids = [];
            var invoice_id = row[0]['invoice_id'];
            var client_code = row[0]['client_code'];
            var type = row[0]['type'];
            var currency = [];
            var vat = row[0]['vat'];
            var local_amount = 0;
            var amount = 0;
            var id_type = [];
            var id_no = [];
            $.each(row,function (index,item) {
                if(invoice_id != item['invoice_id']){//如果有不同的开票ID,提示错误
                    result = 'Can not select invoiced item';
                    return;
                }
                if(client_code != item['client_code']){
                    result = 'Client is different';
                    return;
                }
                // if(currency != item['currency']){
                //     result = '币种不同,无法开票';
                //     return;
                // }
                if(vat != item['vat']){
                    result = 'Vat is different';
                    return;
                }
                if($.inArray(item['currency'], currency) == -1){//获取币种
                    currency.push(item['currency']);
                }
                if($.inArray(item['id_type'], id_type) == -1){//获取币种
                    id_type.push(item['id_type']);
                }
                if($.inArray(item['id_no'], id_no) == -1){//获取币种
                    id_no.push(item['id_no']);
                }
                if(item['currency'] !== '<?= LOCAL_CURRENCY;?>' && item['local_amount'] === null){
                    result = 'Exchange currency fail';
                    return;
                }else{
                    local_amount += parseFloat(item['local_amount']);
                }
                if(item.type == 'cost'){
                    amount -= item['amount'] * 100;
                }else{
                    amount += item['amount'] * 100;
                }
                ids.push(item['id']);
            });
            $('#jump_client').attr('client_code', client_code);
            if(result != ''){
                $.messager.alert('<?= lang('error')?>', result);
                return;
            }
            if(type == 'cost'){
                local_amount = -local_amount;
            }
            var lock = 0;
            invoice_info = {};
            $('#invoice_form').form('clear');
            if(invoice_id != 0){
                var get_invoice = $.ajax({
                    type: 'GET',
                    url: '/biz_bill_invoice/get_invoice/' + invoice_id,
                    dataType: 'json',
                    success: function (res) {
                        if(res.data.invoice_date == '0000-00-00')res.data.invoice_date = null;
                        $('#invoice_form').form('load', res.data);
                        lock = res.data.lock;
                    }
                })
            }else{
                get_invoice = '';
            }
            $.when(get_invoice ).done(function ( v1) {
                if(lock != 0 && lock != 10){
                    $.messager.alert('Tips', 'Please cancel the invoice first');
                    return false;
                }
                if(currency.length == 1){
                    var remark_url = '/biz_bill/auto_remark?id_type=' + id_type + '&id_no=' + id_no;
                    if(id_type.length > 1 || id_no.length > 1) remark_url = '/biz_bill/auto_remark';
                    $.ajax({
                        type: 'GET',
                        url: remark_url,
                        success: function (res_json) {
                            var res;
                            try{
                                res = $.parseJSON(res_json);
                            }catch(e){

                            }
                            if(res == undefined){
                                $.messager.alert('Tips', 'error,Contact Admin!');
                            }else{
                                //如果是美元,额外拼接一部分
                                var remark = res.data.remark;
                                if(currency[0] == 'USD'){
                                    remark = 'USD' + (amount / 100) + "\n exrate:" + res.data.invoice_ex_rate + "\n" + remark;
                                    if(type == 'cost'){
                                        local_amount = -(amount / 100 * res.data.invoice_ex_rate);
                                    }else{
                                        local_amount = amount / 100 * res.data.invoice_ex_rate;
                                    }
                                    $('#invoice_amount').numberbox('setValue', local_amount);
                                }
                                $('#remark').val(remark);
                            }
                        }
                    })
                }

                ids = ids.join(',');
                var invoice_form_data = {
                    ids:ids,
                    title:title,
                    invoice_id:invoice_id,
                    buyer_client_code:'',
                    buyer_company:'',
                    buyer_taxpayer_id:'',
                    currency: 'CNY',
                    vat:vat,
                };
                $('#invoice_form').form('load', invoice_form_data);
                $('#buyer_company').combobox('setValue', row[0]['client_code']).combobox('reload');
                if($('#invoice_amount').numberbox('getValue') == ''){
                    $('#invoice_amount').numberbox('setValue', local_amount);
                }

                $('#invoice').dialog('open').dialog('center').dialog('setTitle', '<?= lang('invoice');?>');
            });

        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }

    }
	function auto_create(){
		$.post("/biz_bill/auto_create", {},
				function(res){
					if(res.code == 1){
						$('#invoice_no').textbox('setValue',res.invoice_no)
					}
				}, "json");
	}
    function invoice_save() {
        // if($('#use_type').combobox('getValue') == 'DN'){
        //     $.messager.alert('Tips', '无法DN开票');
        //     return;
        // }
        ajaxLoading();
        $('#invoice').dialog('close');
        $('#invoice_form').form('submit', {
            url: '/biz_bill/batch_invoice' + '<?php if($is_special == 1) echo '?is_special=1'?>',
            onSubmit: function () {
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                    $('#invoice').dialog('open');
                }
                return validate;
            },
            success: function (res_json) {
                // $('#dlg').addClass('hide');
                var res;
                try{
                    res = $.parseJSON(res_json);
                }catch(e){
                }
                if(res == undefined){
                    $.messager.alert('Tips', 'Error');
                }else{
                    if(res.code == 0){
                        $('#tt').datagrid('reload');
                    }else{
                        $('#invoice').dialog('open');
                    }
                    $.messager.alert('Tips', res.msg);
                }
                ajaxLoadEnd();
            },
        });
    }

    /**
     * 取消开票
     */
    function qx_invoice() {
        var row = $('#tt').datagrid('getSelections');
        if(row.length > 0){
            $.messager.prompt('Tips', 'Confirm Delete?Please type the [delete] to Confirm', function (r) {
                if(r === 'delete'){
                    var invoice_id = row[0]['invoice_id'];
                    var msg = "";
                    $.each(row,function (index,item) {
                        if(item['invoice_id'] != invoice_id){
                            msg = 'Please select the same invoice no';
                            return;
                        }
                    });
                    if(msg !== ""){
                        $.messager.alert('<?= lang('error')?>', msg);
                        return;
                    }
                    $.ajax({
                        type: 'POST',
                        url: '/biz_bill/batch_del_invoice' + '<?php if($is_special == 1) echo '?is_special=1'?>',
                        data:{
                            // title: title,
                            invoice_id: invoice_id,
                        },
                        dataType: 'json',
                        success: function (res) {
                            $.messager.alert('Tips', res.msg);
                            if(res.code == 0){
                                $('#tt').datagrid('reload');
                            }
                        }
                    });
                }
            });
        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }
    }

    function dn_invoice(){
        var data = $('#tt').datagrid('getSelections');
        if(data.length > 0){
            var ids = [];
            //客户,税率,币种必须一致
            var client_codes = [];
            var currencys = [];
            var vats = [];
            var sum_amount = 0;

            var not_window = '';
            var item;
            $.each(data, function(index, item2){
                item = item2;
                var type = item.type;
                if(type == 'cost'){
                    not_window = "Only Sell Billing accepted";
                    return false;
                }else if(type == 'sell'){
                    if(item.id == null){
                        not_window = 'No billing';
                        return false;
                    }
                    if(item.invoice_id != 0){
                        not_window = 'Invoice issued';
                        return false;
                    }
                    if(item.confirm_date === '0000-00-00'){
                        not_window = 'Not confirmed';
                        return false;
                    }
                    var amount = item.amount * 100;
                    sum_amount += amount != null ? amount : 0;

                    ids.push(item.id);
                    if($.inArray(item.client_code, client_codes) === -1)client_codes.push(item.client_code);
                    if($.inArray(item.currency, currencys) === -1)currencys.push(item.currency);
                    if($.inArray(item.vat, vats) === -1)vats.push(item.vat);
                }
            });
            if(currencys.length > 1){
                not_window = 'Currency not the same';
            }
            if(not_window !== ''){
                $.messager.alert('Tips', not_window + ',Can not apply');
                return;
            }
            sum_amount = sum_amount / 100;
            //客户是否相同
            if(client_codes.length > 1){
                $.messager.alert('<?= lang('Tips')?>', 'Client not the same');
                return;
            }

            //判断币种税率是否相同
            if(vats.length > 1){
                $.messager.alert('<?= lang('Tips')?>','<?= lang('Rate not the same')?>');
                return;
            }
            ids = ids.join(',');

            $('#dn_buyer_company').combobox('setValue', item.client_code_name).combobox('reload');
            $('#dn_account_id').combobox('reload',  '/bsc_dict/by_s_get_client_account/' + client_codes[0]);
            var from_data = {'ids' : ids,'buyer_company' : item.client_code_name,'currency' : 'CNY', 'vat' : vats[0], 'invoice_amount' : sum_amount * data[0]['exrate']};
            $('#dn_invoice_form').form('load', from_data);
            $('#dn_invoice_div').dialog('open');
        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }
    }

    function save_dn_invoice() {
        if(is_submit){
            return;
        }
        var form_data = $('#dn_invoice_form').serializeArray();
        var isValid = $('#dn_invoice_form').form('validate');
        var json = {};
        $.each(form_data, function (index, item) {
            json[item.name] = item.value;
        });
        if(isValid){
            ajaxLoading();
            is_submit = true;
            $.ajax({
                type: 'POST',
                url: '/biz_bill/batch_invoice',
                data: json,
                success:function (res_json) {
                    var res;
                    try{
                        res = $.parseJSON(res_json);
                    }catch(e){

                    }
                    is_submit = false;
                    ajaxLoadEnd();
                    if(res == undefined){
                        $.messager.alert('Tips', res_json);
                    }else{
                        $.messager.alert('Tips', res.msg);
                        if(res.code == 0){
                            $.messager.confirm('tip', 'Download D/N?：', function(r){
                                if (r){
                                    if(res.pdf_path != '' && res.pdf_path != undefined)window.open(res.pdf_path);
                                    $('#tt').datagrid('reload');
                                    $('#dn_invoice_div').dialog('close')
                                }
                            });

                        }
                    }
                },
                error: function () {
                    $.messager.alert('Tips', 'error');
                    is_submit = false;
                    ajaxLoadEnd();
                }
            })
        }

    }

    function write_off() {
        var row = $('#tt').datagrid('getSelections');
        if(row.length > 0){
            if(row[0].type == 'cost'){
                $('.actual_amount').text('<?= lang('actual_amount2');?>');
            }else if(row[0].type == 'sell'){
                $('.actual_amount').text('<?= lang('actual_amount');?>');
            }
            var ids = [],currency = [],local_amount = 0,amount = 0,this_currency,payment_amount = 0,other_side_company = [], other_side_company_data = [], msg ="";
            $.each(row,function (index,item) {
                var this_local_amount = item.local_amount;
                var this_amount = item.amount;
                if(item.type == 'cost'){
                    this_local_amount = -this_local_amount;
                    this_amount = -this_amount;
                }
                local_amount += parseInt(this_local_amount * 100);
                amount += parseInt(this_amount * 100);
                ids.push(item['id']);
                if($.inArray(item['currency'], currency) === -1)currency.push(item['currency']);
                if($.inArray(item['client_code'], other_side_company) === -1){
                    other_side_company.push(item['client_code']);
                    other_side_company_data.push({client_code:item['client_code'], company_name:item['client_code_name']})
                }//填充对方客户名称
                if(item.payment_id != 0){
                    if(item.payment_id == '-99'){
                        msg = "Already applied";
                    }else{
                        msg = 'Alread paid'
                    }
                    return false;
                }
            });
            if(msg !== ""){
                $.messager.alert('<?= lang('Tips')?>', msg);
                return;
            }
            if(currency.length == 1){//只有一种的话,自动取第一种
                this_currency = currency[0];
                if(this_currency == 'USD') $('#payment_currency').combobox({readonly: false});
                else if(this_currency == 'CNY')$('#payment_currency').combobox({readonly: true});
            }else if(currency.length >= 2){//超过一种时,默认为CNY
                this_currency = 'CNY';
                $('#payment_currency').combobox({readonly: true});
            }else{
                $('#payment_currency').combobox({readonly: false});
                $.messager.alert('<?= lang('Tips')?>','error');
                return;
            }
            if(this_currency == 'CNY'){
                payment_amount = local_amount / 100;
            }else{
                payment_amount = amount / 100;
            }

            var title = '<?= lang('write_off');?>';
            var write_off_data = {};
            write_off_data.ids = ids.join(',');
            write_off_data.currency = this_currency;
            write_off_data.payment_amount = payment_amount;
            write_off_data.other_side_company = other_side_company[0];
            write_off_data.payment_no = '';//2022-05-10 自动清空凭据号
            $('#payment_local_amount_hidden').val(local_amount / 100);
            $('#payment_amount_hidden').val(amount / 100);

            $('#other_side_company').combobox('loadData', other_side_company_data);

            $('#write_off_form').form('load', write_off_data);

            $('#write_off').dialog('open').dialog('center').dialog('setTitle', title);
        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }
    }

    function write_off_save() {
        $('#write_off').dialog('close');
        var payment_no = $('#payment_no').textbox('getValue').trim();
        if(payment_no == ''){
            $.messager.confirm('<?= lang('提示');?>', 'no SN?', function(r){
                if(r){
                    write_off_form_submit();
                }else{
                    $('#write_off').dialog('open');
                    ajaxLoadEnd();
                }
            });
        }else{
            write_off_form_submit();
        }


    }

    function write_off_form_submit(){
        ajaxLoading();
        $('#write_off_form').form('submit', {
            url: '/biz_bill/batch_write_off' + '<?php if($is_special == 1) echo '?is_special=1'?>',
            onSubmit: function () {
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                    $('#write_off').dialog('open');
                }
                return validate;
            },
            success: function (result) {
                // $('#dlg').addClass('hide');
                var data;
                try{
                    data = $.parseJSON(result);
                }catch(e){
                }
                if(data == undefined){
                    $.messager.alert('Tips', '发生错误');
                }else{
                    $.messager.alert('Tips', data.msg);
                    if(data.code == 0){

                        $('#tt').datagrid('reload');
                    }else{
                        $('#write_off').dialog('open');
                    }
                }
                ajaxLoadEnd();
            }
        });
    }

    function qx_write_off() {
        //取消的必须是lock为2的,
        var row = $('#tt').datagrid('getSelections');
        $.messager.prompt('Tips', '<?php echo lang('Please input [delete] to confirm:');?>', function (r) {
            if(r === 'delete'){
                if(row.length > 0){
                    var payment_id = 0;
                    var msg = "";
                    $.each(row,function (index,item) {
                        payment_id = item.payment_id;
                        if(item.payment_id == 0){
                            msg = 'Not write_off';
                            return false;
                        }
                    });
                    if(msg !== ""){
                        $.messager.alert('Tips', msg);
                        return false;
                    }
                    //选多个
                    ajaxLoading();
                    $.ajax({
                        type: 'POST',
                        url: '/biz_bill/batch_del_write_off',
                        data:{
                            payment_id:payment_id,
                        },
                        success: function (res_json) {
                            // $('#dlg').addClass('hide');
                            var res;
                            try{
                                res = $.parseJSON(res_json);
                            }catch(e){
                            }
                            if(res == undefined){
                                $.messager.alert('Tips', 'error');
                            }else{
                                $.messager.alert('Tips', res.msg);
                                if(res.code == 0){
                                    $('#tt').datagrid('reload');
                                }
                            }
                            ajaxLoadEnd();
                        }
                    });

                }else{
                    $.messager.alert('<?= lang('Tips')?>','<?= lang('No columns selected')?>');
                }
            }
        });
    }

    //审核 修改审核日期
    function examine() {
        var row = $('#tt').datagrid('getSelections');
        if(row.length > 0){
            var payment_ids = [];
            var msg = "";
            $.each(row,function (index,item) {
                if($.inArray(item.payment_id, payment_ids) === -1)payment_ids.push(item.payment_id);
                if(item.payment_id == 0){
                    msg = 'Not write_off';
                    return false;
                }
                if(item.lock_date != '0000-00-00'){
                    msg = 'Have examined';
                    return false;
                }
            });
            if(msg !== ""){
                $.messager.alert('<?= lang('Tips')?>',msg);
                return;
            }
            // $('#examine_form input[name="title"]').val(title);
            payment_ids = payment_ids.join(',');
            var examine_data = {};
            examine_data.payment_ids = payment_ids;
            $('#examine_form').form('load', examine_data);
            $('#examine').dialog('open').dialog('center').dialog('setTitle', '<?= lang('examine');?>');
        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }
    }

    function examine_save() {
        ajaxLoading();
        $('#examine').dialog('close');
        $('#examine_form').form('submit', {
            url: '/biz_bill/batch_examine',
            onSubmit: function () {
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                    $('#examine').dialog('open');
                }
                return validate;
            },
            success: function (res_json) {
                // $('#dlg').addClass('hide');
                var res;
                try{
                    res = $.parseJSON(res_json);
                }catch(e){
                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误');
                }else{
                    if(res.code == 0){
                        $('#tt').datagrid('reload');
                    }else{
                        $('#examine').dialog('open');
                    }
                    $.messager.alert('<?= lang('Tips')?>', res.msg);
                }
                ajaxLoadEnd();
            }
        });
    }

    //撤销审核
    function qx_examine() {
        //取消的必须是lock为3的,
        var row = $('#tt').datagrid('getSelections');
        if (row.length > 0) {
            var payment_id = 0;
            var msg = "";
            $.each(row, function (index, item) {
                payment_id = item.payment_id;
                if(item.payment_id == 0 || item.lock_date == '0000-00-00'){
                    msg = "Not write_off,Failure";
                    return false;
                }
            });
            if (msg !== "") {
                $.messager.alert('Tips', msg);
                return false;
            }
            $.messager.confirm('Tip','Cancel of Audit？',function(r) {
                if (r) {
                    $.ajax({
                        type: 'POST',
                        url: '/biz_bill/batch_del_examine',
                        data: {
                            payment_id: payment_id,
                        },
                        dataType: 'json',
                        success: function (res) {
                            $.messager.alert('Tips', res.msg);
                            if (res.code == 0) {
                                $('#tt').datagrid('reload');
                            }
                        }
                    });

                }
            });
        } else {
            $.messager.alert('<?= lang('Tips')?>', '<?= lang('No columns selected')?>');
        }
    }

    function reload_bill_local() {
        $('#reload_bill_local').dialog('open');
        // $.ajax({
        //     type: 'GET',
        //     url: '/biz_bill/reload_bill_local',
        //     data:{
        //     },
        //     dataType: 'json',
        //     success: function (res) {
        //         $.messager.alert('Tips', res.msg);
        //         if(res.code == 0){
        //             $('#tt').datagrid('reload');
        //         }
        //     }
        // });
    }

    function reload_bill_local_save() {
        $('#reload_bill_local_form').form('submit', {
            url: '/biz_bill/reload_bill_local',
            onSubmit: function () {
                return $(this).form('validate');
            },
            onBeforeLoad:function(){
                ajaxLoading();
            },
            success: function (result) {
                ajaxLoadEnd();
                var res = $.parseJSON(result);
                $.messager.alert('<?= lang('info')?>', res.msg);
                $('#reload_bill_local').dialog('close');
                $('#reload_bill_local_form').form('reset');
                $('#tt').datagrid('reload');
            }
        });
    }

    function reload_all_data(first_page) {
        var tt = $('#tt');
        // tt.datagrid('getO')
        var options = tt.datagrid('options');

        tt.datagrid('gotoPage', {
            page: first_page,
            callback: function(page){
                tt.datagrid('selectAll');
                var row_data = tt.datagrid('getData');
                var this_total = (page - 1) * options.pageSize + row_data.rows.length;
                if(row_data.rows.length !== 0 && this_total < row_data.total){
                    reload_all_data(page + 1);
                }
            }
        })
    }

    function update_bill() {
        //取消的必须是lock为3的,
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            $('#update_bill').dialog('open');
            var ids = [];
            $.each(rows, function (index, item) {
                ids.push(item.id);
            });
            ids = ids.join(',');

            $('#update_bill input[name="ids"]').val(ids);
        }else {
            $.messager.alert('<?= lang('error')?>', '<?= lang('No columns selected')?>');
        }
    }

    function save_bill() {
        $('#update_bill_form').form('submit', {
            url: '/biz_bill/batch_update_data',
            onSubmit: function () {
                return $(this).form('validate');
            },
            success: function (res) {
                res = $.parseJSON(res);
                $.messager.alert('Tips', res.msg);
                if(res.code == 0){
                    $('#update_bill').dialog('close');
                    $('#tt').datagrid('reload');
                }
            }
        });
    }

    function jump_client(e){
        var client_code = $(e).attr('client_code');
        window.open("/biz_client/edit_by_client/" + client_code);
    }

    function invoice_type_select(rec) {
        if(rec.value == '999'){//形式发票
            $('.hidden_invoice_input.invoice_type999').css('display', 'table-cell');
            $('#invoice_payment_bank').combobox({'required' : true});

            $('.hidden_invoice_input.invoice_type1001').css('display', 'none');
            $('#use_type').combobox({'required' : false});
        }else if(rec.value == '1001'){//无发票
            $('.hidden_invoice_input.invoice_type1001').css('display', 'table-cell');
            $('#use_type').combobox({'required' : true});

            $('.hidden_invoice_input.invoice_type999').css('display', 'none');
            $('#invoice_payment_bank').combobox({'required' : false});
        }else{
            $('.hidden_invoice_input.invoice_type999').css('display', 'none');
            $('#invoice_payment_bank').combobox({'required' : false});

            $('.hidden_invoice_input.invoice_type1001').css('display', 'none');
            $('#use_type').combobox({'required' : false});
        }
        if(rec.value == '1001'){
            $('#invoice_company').combobox({required:false});
        }else{
            $('#invoice_company').combobox({required:true});
        }
    }

    //发票明细
    function invoice_read_list(){
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 1 || rows.length == 0){
            $.messager.alert('Tips', '请选中一条数据');
            return;
        }
        var row = rows[0];
        var invoice_id = row['invoice_id'];
        if(invoice_id === '0'){
            $.messager.alert('Tips', '请选择已开票的数据');
            return;
        }
        $('#invoice_read_list').window('open');
        $("#invoice_read_list_iframe").attr('src', '/biz_bill_invoice/invoice_child_read?id=' + invoice_id);
    }
    
    //导出DN
    function export_debit_note(){
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            if(rows.length > 1){
                $.messager.alert('<?= lang('Tips')?>', '<?= lang('只能选择一行')?>');
                return;
            }
            var row = rows[0];
            var id = row.id_no;
            var json = {'parameter[client_code]':row.client_code};

            var form = $('<form></form>').attr('action','/export/export_word?template_id=1&pdf=1&data_table=shipment&id=' + id).attr('method','POST').attr('target', '_blank');

            $.each(json, function (index, item) {
                if(typeof item === 'string'){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                }else{
                    $.each(item, function(index2, item2){
                        form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                    });
                }
            });
    
            form.appendTo('body').submit().remove();
        }else {
            $.messager.alert('<?= lang('提示')?>', '<?= lang('No columns selected')?>');
        }
    }
    function batch_commision(commision = 0, is_search = false){
		var row = $('#tt').datagrid('getSelections');
		if (row.length > 0 || is_search) {
			var ids = [];
			var msg = '';
			$.each(row, function (index, item) {
				if(commision == 1 && item.commision_flag == commision){
					msg = "<?= lang("请勿选择已计提的账单") ?>";
					return false;
				}
				if(commision == 0 && item.commision_flag == 0){
					msg = "<?= lang("请勿选择未计提的账单") ?>";
					return false;
				}

				ids.push(item.id);
			});

			if(msg !== ''){
				$.messager.alert('Tips', msg);
				return;
			}
			if(!is_search){
				if(ids.length == 0) msg = '<?= lang("当前无修改值") ?>';
			}
			ids = ids.join(',');

			var json = {};
			var cx_data = $('#cx').serializeArray();
			$.each(cx_data, function (index, item) {
				//判断如果name存在,且为string类型
				if(json.hasOwnProperty(item.name) === true){
					if(typeof json[item.name] == 'string'){
						json[item.name] =  json[item.name].split(',');
						json[item.name].push(item.value);
					}else{
						json[item.name].push(item.value);
					}
				}else{
					json[item.name] = item.value;
				}
			});
			json.update_field = {};
			json.update_field.ids = ids;
			json.update_field.commision_flag = commision;
			json.update_field.is_search = is_search;

			if(commision == 1){
				var company_alert = {title:'Tips',msg:'<?= lang("是否确认勾选计提?请选择计提时间") ?>',fn:function (r) {
						if(r){
							json.update_field.commision_month = r;
							ajaxLoading();
							$.ajax({
								type: 'POST',
								url: '/biz_bill/batch_commision_flag',
								data:json,
								success: function (res_json) {
									var res;
									try{
										res = $.parseJSON(res_json);
									}catch(e){
									}
									ajaxLoadEnd();
									if(res == undefined){
										$.messager.alert('Tips', res_json);
										return false;
									}

									$.messager.alert('Tips', res.msg);
									if(res.code == 0){
										$('#tt').datagrid('reload');
									}
								}
							});
						}
					}, input:"<input type=\"text\" class=\"Wdate\" id=\"month_select\" onclick=\"WdatePicker({startDate:'%y-%M',dateFmt:'yyyy-MM'});\" style=\"width:266px;\" autocomplete=\"off\"/>\n", input_val:"$('#month_select').val()"};
				$.messager.promptext(company_alert);
			}else{
				$.messager.confirm('<?= lang("确认对话框") ?>', '<?= lang("是否确认撤销勾选计提？") ?>', function(r){
					if(r){
						json.update_field.commision_month = '0000-00-00';
						ajaxLoading();
						$.ajax({
							type: 'POST',
							url: '/biz_bill/batch_commision_flag',
							data:json,
							success: function (res_json) {
								var res;
								try{
									res = $.parseJSON(res_json);
								}catch(e){
								}
								ajaxLoadEnd();
								if(res == undefined){
									$.messager.alert('Tips', res_json);
									return false;
								}

								$.messager.alert('Tips', res.msg);
								if(res.code == 0){
									$('#tt').datagrid('reload');
								}
							}
						});
					}
				});
			}


		} else {
			$.messager.alert('<?= lang("提示") ?>', '<?= lang("未选中行") ?>');
		}
	}
</script>

<table id="tt"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="false" nowrap="false">
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#tt').edatagrid('cancelRow')"><?= lang('cancel');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm3',plain:true" iconCls="icon-reload"><?= lang('config');?></a>
                <?php if(menu_role('bill_invoice')){ ?>
                <a href="javascript:void(0)" class="easyui-menubutton" iconCls="icon-write_off" plain="true" data-options="menu:'#mm2'" onclick="invoice()"><?= lang('invoice');?></a>
                <?php } ?>
                <?php if(menu_role('qx_bill_invoice')){ ?>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-write_off" plain="true" onclick="qx_invoice()"><?= lang('Cancel invoice');?></a>
                <?php } ?>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-write_off" plain="true" onclick="write_off()"><?= lang('write_off');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-write_off" plain="true" onclick="qx_write_off()"><?= lang('Cancel write off');?></a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-write_off" plain="true" onclick="examine()"><?= lang('write off review');?></a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-write_off" plain="true" onclick="qx_examine()"><?= lang('Cancel of write off review');?></a>
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#excel_menu',plain:true" iconCls="icon-reload"><?= lang('export excel');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="reload_all_data(1);"><?= lang('check all');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="update_bill();"><?= lang('对账号');?></a>
				<a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm_batch_commision'" plain="false"><?= lang('Commistion_month');?></a>
<!--                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="invoice_read_list();">--><?//= lang('发票明细');?><!--</a>-->
                <?php if(menu_role('bill_batch_confirm_date')){ ?>
                    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="confirm_date();"><?= lang('批量确认');?></a>
                <?php } ?> 
                    <a href="/biz_bill/extra_fee" target="_blank" class="easyui-linkbutton" ><?= lang('extra_fee');?></a> 
                    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="window.open('/biz_bill/third_party_payment');"><?= lang('third party payment');?></a>
            </td>
        </tr>
    </table>
    <div id="mm2" style="width:150px;display:none;">
        <div data-options="iconCls:'icon-ok'" onclick="invoice();"><?= lang('普通开票');?></div>
    </div>
    <div id="mm3" style="width:150px;">
        <div data-options="iconCls:'icon-ok'" onclick="javascript:config()"><?= lang('column');?></div>
    </div>
	<div id="mm_batch_commision" style="width:150px;display:none;">
		<div data-options="iconCls:'icon-ok'" onclick="batch_commision(1);"><?= lang('add_month');?></div>
		<div data-options="iconCls:'icon-ok'" onclick="batch_commision(0);"><?= lang('del_month');?></div>
	</div>
    <div id="excel_menu" style="width:150px;">
<!--        <div data-options="iconCls:'icon-ok'" onclick="cktz_excel()">--><?//= lang('催款通知');?><!--</div>-->
<!--        <div data-options="iconCls:'icon-ok'" onclick="znj_excel()">--><?//= lang('滞纳金表');?><!--</div>-->
        <div data-options="iconCls:'icon-ok'" onclick="cktz2_excel()"><?= lang('SOA');?></div>
        <div data-options="iconCls:'icon-ok'" onclick="bill_details()"><?= lang('原始账单明细');?></div>
        <!--<div data-options="iconCls:'icon-ok'" onclick="export_debit_note()"><?= lang('Debit Note');?></div>-->
    </div>
    <form id="statistics_fm">
        <table>
            <?php for($i = 0; $i < sizeof($currency_table_fields); $i++){ 
                $this_currency = $currency_table_fields[$i];
            ?>
                <tr>
                    <td><?= $this_currency;?>:</td>
                    <td><?= lang('amount');?><input class="easyui-numberbox" precision="2" id="<?= $this_currency;?>_amount" disabled value="0"></td>
                    <td><?= lang('invoice_amount');?><input class="easyui-numberbox" precision="2" id="<?= $this_currency;?>_invoice_amount" disabled value="0"></td>
                    <td><?= lang('not_invoice_amount');?><input class="easyui-numberbox" precision="2" id="<?= $this_currency;?>_not_invoice_amount" disabled value="0"></td>
                    <td><?= lang('actual_amount');?><input class="easyui-numberbox" precision="2" id="<?= $this_currency;?>_actual_amount" disabled value="0"></td>
                    <td><?= lang('not_actual_amount');?><input class="easyui-numberbox" precision="2" id="<?= $this_currency;?>_not_actual_amount" disabled value="0"></td>
                </tr>
            <?php } ?>
            </tr>
        </table>
    </form>
    
</div>

<div id="invoice" class="easyui-dialog" style="width:800px;" data-options="closed:true,modal:true,border:'thin',buttons:'#invoice-buttons'">
    <form id="invoice_form" method="post" novalidate style="margin:0;padding:10px 10px">
        <input type="hidden" name="invoice_id">
        <input type="hidden" name="ids">
        <!--        <input type="hidden" name="title">-->
        <table style="border-collapse:collapse;">
<!--            <tr>-->
<!--                <td style="color:red;font-weight:bold;" colspan="3">--><?//= lang('申请开票');?><!--:&nbsp;&nbsp;<a href="javascript:void(0);" onclick="jump_client(this)" id="jump_client">--><?//= lang('点击维护纳税人');?><!--</a><br><br></td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>-->
<!--            </tr>-->
            <tr>
                <!--<td><?= lang('invoice_type');?></td>-->
                <!--<td>-->
                <!--    <select name="invoice_type" editable="false" class="easyui-combobox" style="width:111px;"  data-options="-->
                <!--        required:true,-->
                <!--        valueField:'value',-->
                <!--        textField:'name',-->
                <!--        url:'/bsc_dict/get_option/invoice_type',-->
                <!--        onSelect: function(rec){-->
                <!--            invoice_type_select(rec);-->
                <!--        }-->
                <!--    ">-->
                <!--    </select>-->
                <!--</td>-->
                <td><?= lang('buyer_company');?></td>
                <td>
                    <select id="buyer_company" readonly class="easyui-combobox"  style="width:188px;" data-options="
                        valueField:'client_code',
                        textField:'company_name',
                        url:'/biz_client/get_client/?q_field=client_code',
                        mode: 'remote',
                        onBeforeLoad:function(param){
                            if($(this).combobox('getValue') == '')return false;
                            if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                        },
                        onLoadSuccess:function(){
                            var val = $(this).combobox('getValue');
                            $(this).combobox('clear').combobox('select', val);
                        },
                        onSelect: function(res){
                            $('#invoice_form input[name=\'buyer_client_code\']').val(res.client_code);
                            $('#invoice_form input[name=\'buyer_company\']').val(res.taxpayer_name);
                            $('#invoice_form input[name=\'buyer_taxpayer_id\']').val(res.taxpayer_id);
                        },
                    ">
                    </select>
                    <input type="hidden" name="buyer_client_code">
                    <input type="hidden" name="buyer_company">
                    <input type="hidden" name="buyer_taxpayer_id">
                </td>
                <td> </td>
                <td>

                </td>
            </tr>
            <tr>
<!--                <td>--><?//= lang('vat');?><!--</td>-->
<!--                <td>-->
<!--                    <input type="text" name="vat" readonly class="easyui-textbox" style="width:111px;">-->
<!--                </td>-->
                <td><?= lang('invoice_amount');?></td>
                <td><input name="invoice_amount" id="invoice_amount" readonly precision="2" class="easyui-numberbox" style="width:188px;"></td>
                <td class="hidden_invoice_input invoice_type999"><?= lang('payment_bank');?></td>
                <td class="hidden_invoice_input invoice_type999">
                    <select name="payment_bank" id="invoice_payment_bank" class="easyui-combobox" style="width: 90px" data-options="
                        valueField:'name',
                        textField:'name',
                        url:'/bsc_dict/get_option/payment_bank',
                    ">
                    </select>
                </td>
                <!--<td class="hidden_invoice_input invoice_type1001"><?= lang('use_type');?></td>-->
                <!--<td class="hidden_invoice_input invoice_type1001">-->
                <!--    <select name="use_type" id="use_type" class="easyui-combobox" style="width: 90px" data-options="-->
                <!--        valueField:'name',-->
                <!--        textField:'name',-->
                <!--        url:'/bsc_dict/get_option/use_type',-->
                <!--    ">-->
                <!--    </select>-->
                <!--</td>-->
            </tr>
            <tr class="invoice_apply">
                <td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
            </tr>
            <tr> <td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
            </tr>
<!--            <tr>-->
<!--                <td style="color:red;font-weight:bold;">--><?//= lang('执行开票'); ?><!--:<br><br></td>-->
<!--                <td>-->
<!--                </td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>-->
<!--                <br><br>-->
<!--            </tr>-->
            <tr>
                <td><?= lang('invoice_no');?></td>
				<td><input name="invoice_no" id="invoice_no" class="easyui-textbox" style="width:111px;"><a href="javascript:;" onclick="auto_create()"><?=lang('auto create')?></a></td>
                <td><?= lang('invoice_date');?></td>
                <td><input name="invoice_date" required="required" id="invoice_date" class="easyui-datebox" style="width:188px;"></td>
                <!--<td><?= lang('invoice_company');?></td>-->
                <!--<td>-->
                <!--    <select name="invoice_company" id="invoice_company" class="easyui-combobox" style="width:111px;" data-options="-->
                <!--        required: true,-->
                <!--        editable:false,-->
                <!--        valueField:'company_name',-->
                <!--        textField:'company_name',-->
                <!--    ">-->
                <!--    </select>-->
                <!--</td>-->
            </tr>
            <tr>
<!--                <td>--><?//= lang('disabled');?><!--</td>-->
<!--                <td><input name="disabled" id="disabled" class="easyui-textbox" style="width:111px;"></td>-->
                <td><?= lang('remark');?></td>
                <td colspan="4"><textarea name="remark" id="remark" style="width:460px;height: 50px"></textarea></td>
            </tr>
        </table>
    </form>
    <div id="invoice-buttons">
        <a id="invoice_save_button" href="javascript:invoice_save()" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px"><?= lang('save');?></a>
    </div>
</div>

<div id="write_off" class="easyui-dialog" style="width:900px;" data-options="closed:true,modal:true,border:'thin',buttons:'#write_off-buttons'">
    <form id="write_off_form" method="post" novalidate style="margin:0;padding:10px 10px">
        <input type="hidden" name="ids">
        <input type="hidden" id="payment_local_amount_hidden">
        <input type="hidden" id="payment_amount_hidden">
        <table>
            <tr>
                <td><?= lang('payment_date');?></td>
                <td><input name="payment_date" required id="payment_date" class="easyui-datebox" style="width: 150px"></td>
                <td class="payment_amount"><?= lang('payment_amount');?></td>
                <td><input name="payment_amount" required id="payment_amount" class="easyui-numberbox" precision="2" style="width: 150px" readonly="readonly"></td>
                <td><?= lang('核销币种');?></td>
                <td>
                    <select class="easyui-combobox" name="currency" id="payment_currency" style="width: 150px" data-options="
                        url:'/biz_bill_currency/get_currency/sell',
                        editable:false,
                        valueField:'name',
                        textField:'name',
                        required:true,
                        onSelect:function(rec){
                            //这个代码如果后面加入其它币种不会兼容的 只针对当前的 CNY 和USD来
                            if(rec.name == 'CNY') $('#payment_amount').numberbox('setValue', $('#payment_local_amount_hidden').val());
                            else $('#payment_amount').numberbox('setValue', $('#payment_amount_hidden').val());
                        }
                    "></select>
                </td>
                <td><?= lang('对方抬头');?></td>
                <td>
                    <select class="easyui-combobox" name="other_side_company" id="other_side_company" style="width: 150px" data-options="
                        editable:false,
                        valueField:'client_code',
                        textField:'company_name',
                        required:true,
                    "></select>
                </td>
            </tr>
            <tr>
                <td><?= lang('payment_way');?></td>
                <td>
                    <select name="payment_way" required id="payment_way" class="easyui-combobox" style="width: 150px" data-options="
                        valueField:'value',
                        textField:'value',
                        url:'/bsc_dict/get_option/payment_way',
                    ">
                    </select>
                </td>
                <td><?= lang('payment_bank');?></td>
                <td>
                    <select name="payment_bank" id="payment_bank" class="easyui-combobox" style="width: 150px" data-options="
                        valueField:'name',
                        textField:'name',
                        url:'/bsc_dict/get_option/payment_bank',
                    ">
                    </select>
                </td>
                <td><?= lang('payment_no');?></td>
                <td><input name="payment_no" id="payment_no" class="easyui-textbox" style="width: 150px"></td>
            </tr>
        </table>
    </form>
    <div id="write_off-buttons">
        <a href="javascript:write_off_save()" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px">Save</a>
    </div>
</div>

<div id="examine" class="easyui-dialog" style="width:600px;"
     data-options="closed:true,modal:true,border:'thin',buttons:'#examine-buttons'">
    <form id="examine_form" method="post" novalidate style="margin:0;padding:10px 10px">
        <input type="hidden" name="payment_ids">
        <table>
            <tr>
                <td><?= lang('lock_date');?></td>
                <td><input name="lock_date" id="lock_date" required="required" class="easyui-datebox" style="width: 90px"></td>
            </tr>
        </table>
    </form>
    <div id="examine-buttons">
        <a href="javascript:examine_save()" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px">Save</a>
    </div>
</div>

<div id="chaxun" class="easyui-window" title="<?= lang('query');?>" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <form id="cx" onSubmit="doSearch();">
        <input type="hidden" name="ids">
        <table>
            <tr>
                <td><?= lang('type');?>:</td>
                <td>
                    <input name="field[f][]" type="hidden" value="biz_bill.type">
                    <input name="field[s][]" type="hidden" value="=">
                    <select name="field[v][]"  id="query_title"  class="easyui-combobox" style="width:150px;" >
                        <option value="">ALL</option>
                        <option value="cost">cost</option>
                        <option value="sell">sell</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('status');?>:</td>
                <td>
					<select name="type"  id="type"  class="easyui-combobox" style="width:150px;">
						<option value="">--<?= lang('select');?>--</option>
						<?php if(menu_role('bill_invoice')){ ?>
							<option value="1"><?= lang('not invoice not write off');?></option>
							<option value="2"><?= lang('have invoiced and not write off');?></option>
							<option value="4"><?= lang('not invoice and have writed off');?></option>
						<?php } ?>
						<option value="3"><?= lang('not write off');?></option>
						<option value="7"><?= lang('have writed off');?></option>
						<option value="5"><?= lang('reviewing');?></option>
						<option value="6"><?= lang('have reviewed');?></option>
					</select>
                </td>
            </tr>
            <tr>
                <!--按销售姓名等-->
                <td><?= lang('user');?></td>
                <td align="right">
                    <select class="easyui-combobox" name="user_role" style="width:150px;" data-options="
                            valueField:'user_role',
                            textField:'user_role',
                            url:'/bsc_user_role/get_role/?status=',
                            onSelect:function(res){
                                $('#user').combobox('reload', '/bsc_user/get_data_role/' + res.user_role + '?status=').combobox('setValue', '');
                            },
                            onLoadSuccess:function(){
                                var data = $(this).combobox('getData');
                                $(this).combobox('select', data[0].user_role);
                            }
                        ">
                    </select>
                    &nbsp;
                    <select class="easyui-combobox" id="user" name="user[]" style="width:268px;" data-options="
                        valueField:'id',
                        textField:'name',
                        multiple:true,
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <?= lang('group');?>
                </td>
                <td align="right">
                    <select class="easyui-combobox" name="group_role" style="width:150px;" data-options="
                            valueField:'user_role',
                            textField:'user_role',
                            url:'/bsc_user_role/get_role/',
                            onLoadSuccess:function(){
                                var data = $(this).combobox('getData');
                                $(this).combobox('select', data[0].user_role);
                            }
                        ">
                    </select>
                    &nbsp;
                    <select class="easyui-combobox" id="group" name="group[]" style="width:269px;" data-options="
                        multiple:true,
                        valueField:'code',
                        textField:'name',
                        url:'/bsc_group/get_option/',
                    ">
                    </select>
                </td>
            </tr>
            <!--<tr>-->
            <!--    <td><?= lang('计提标记');?></td>-->
            <!--    <td align="left">-->
            <!--        <input type="radio" name="commision_flag" value="" checked>全部-->
            <!--        <input type="radio" name="commision_flag" value="0">否-->
            <!--        <input type="radio" name="commision_flag" value="1">是-->
            <!--        <input type="hidden" name="field[f][]" value="biz_bill.commision_month">-->
            <!--        <input type="hidden" name="field[s][]" value=">=">-->
            <!--        <input type="hidden" name="field[v][]" id="commision_month_start">-->
            <!--        <input type="hidden" name="field[f][]" value="biz_bill.commision_month">-->
            <!--        <input type="hidden" name="field[s][]" value="<=">-->
            <!--        <input type="hidden" name="field[v][]" id="commision_month_end">-->
            <!--        &nbsp;&nbsp;-->
            <!--        <input type="text" class="Wdate" id="commision_month_select" onclick="WdatePicker({startDate:'%y-%M',dateFmt:'yyyy-MM',onpicked:function(){ var val = $('#commision_month_select').val(); $('#commision_month_start').val(val + '-01'); $('#commision_month_end').val(val + '-31');}});" style="width:100px;" autocomplete="off"/>-->
            <!--    </td>-->
            <!--</tr>-->
            <!--<tr>-->
            <!--    <td><?= lang('内部结算');?></td>-->
            <!--    <td>-->
            <!--        <input type="hidden" name="field[f][]" value="biz_bill.client_inner_department">-->
            <!--        <input type="hidden" name="field[s][]" value="=">-->
            <!--        <select class="easyui-combotree" name="field[v][]" style="width:400px;" data-options="-->
            <!--            idField:'group_code',-->
            <!--            url:'/bsc_group/get_tree',-->
            <!--            onBeforeSelect: function(node){-->
            <!--                if(node.group_type === '分公司' || node.group_type === '总部'){-->
            <!--                     $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择部门');?>');-->
            <!--                     return false;-->
            <!--                }-->
            <!--                else return true;-->
            <!--            },-->
            <!--        ">-->
            <!--        </select>-->
            <!--    </td>-->
            <!--</tr>-->
        </table>
        <br><br>
        <button class="easyui-linkbutton" id="submit" onclick="doSearch();" style="width:200px;height:30px;" type="button"><?= lang('search');?></button>
        <!--<button class="easyui-linkbutton" id="reset" onclick="reset();" style="width:200px;height:30px;" type="button"><?= lang('reset');?></button>-->
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="resetSearch();">重置</button>

        <a href="javascript:void(0);" class="easyui-tooltip" data-options="
        content: $('<div></div>'),
        onShow: function(){
            $(this).tooltip('arrow').css('left', 20);
            $(this).tooltip('tip').css('left', $(this).offset().left);
        },
        onUpdate: function(cc){
            cc.panel({
                width: 500,
                height: 'auto',
                border: false,
                href: '/bsc_help_content/help/query_condition'
            });
        }
    "><?= lang('help');?></a>
    </form>
</div>

<div id="update_bill" class="easyui-dialog" title="update" style="padding:5px;" data-options="closed:true,modal:true,border:'thin'">
    <form id="update_bill_form" method="post">
        <input type="hidden" name="ids">
        <table>
            <tr>
                <td><?= lang('account_no');?></td>
                <td>
                    <input class="easyui-textbox" name="account_no">
                </td>
            </tr>
        </table>
        <div>
            <button type="button" onclick="javascript:save_bill();" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px"><?= lang('submit');?></button>
            <button type="button" onclick="javascript:$('#update_bill').dialog('close')" class="easyui-linkbutton" iconCls="icon-cancel" style="width:90px"><?= lang('cancel');?></button>
        </div>
    </form>
</div>

<div id="split_bill" class="easyui-dialog" title="拆分" style="width:250px;padding:5px;" data-options="closed:true,modal:true,border:'thin'">
    <form id="split_bill_form" method="post">
        <input type="hidden" name="id">
        <input type="hidden" id="split_bill_amount">
        <table>
            <tr>
                <td><?= lang('money');?> 1</td>
                <td>
                    <input class="easyui-numberbox" name="amount[]" id="split_bill_amount_1" required data-options="precision:2,onChange:function(newVal, oldVal){
                            split_bill_js(newVal, oldVal);
                        }">
                </td>
            </tr>
            <tr>
                <td><?= lang('money');?> 2</td>
                <td>
                    <input class="easyui-numberbox" id="split_bill_amount_2" name="amount[]" required data-options="precision:2,onChange:function(newVal, oldVal){
                            split_bill_js2(newVal, oldVal);
                        }">
                </td>
            </tr>
        </table>
        <div>
            <button type="button" onclick="javascript:save_split_bill();" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px"><?= lang('submit');?></button>
        </div>
    </form>
</div>

<div id="confirm_date_div" title="确认账单" class="easyui-dialog" style="width:350px;height:200px" data-options="title:'window',modal:true,closed:true">
    <form id="confirm_date_form" method="post">
        <input type="hidden" name="ids">
        <input type="hidden" name="ts" value="1">
        <table>
            <tr>
                <td>确认日期</td>
                <td>
                    <input class="easyui-datebox" name="confirm_date" style="width: 255px;">
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <a href="javascript:void(0);" onclick="save_confirm_date()" class="easyui-linkbutton"><?= lang('save');?></a>
                </td>
                <td></td>
            </tr>
        </table>
    </form>
</div>

<!--发票明细-->
<div id="invoice_read_list" class="easyui-window" title="<?= lang('发票明细');?>" closed="true" style="width:840px;height:475px;padding:5px;" data-options="onResize:function(width,height){
        var this_css = {width: width - 40,height: height - 70};
        $('#invoice_read_list_iframe').css(this_css);
    }">
    <iframe id="invoice_read_list_iframe" style="width:800px;height:405px;padding:5px;border:0px;">

    </iframe>
</div>

<!--DN开票申请-->
<div id="dn_invoice_div" class="easyui-dialog" style="width:400px;height:300px" data-options="title:'window',modal:true,closed:true">
    <form id="dn_invoice_form">
        <input type="hidden" name="ids">
        <input type="hidden" name="invoice_type" value="1001">
        <input type="hidden" name="payment_bank" value="DN">
        <input type="hidden" name="currency">
        <input type="hidden" name="vat">
        <input type="hidden" name="use_type" value="DN">
        <table>
            <tr>
                <td><?= lang('buyer_company');?></td>
                <td>
                    <select name="buyer_company" id="dn_buyer_company" class="easyui-combobox" style="width:250px;" readonly data-options="
                        valueField:'company_name',
                        textField:'company_name',
                        url:'/biz_client/get_client/?q_field=company_name',
                        mode: 'remote',
                        onBeforeLoad:function(param){
                            if($(this).combobox('getValue') == '')return false;
                            if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                        },
                        onLoadSuccess:function(){
                            var val = $(this).combobox('getValue');
                            $(this).combobox('clear').combobox('select', val);
                        },
                        onSelect: function(res){
                            $('#dn_invoice_form input[name=\'buyer_client_code\']').val(res.client_code);
                        },
                    ">
                    </select>
                    <input type="hidden" name="buyer_client_code">
                </td>
            </tr>
            <tr>
                <td><?= lang('invoice_amount');?></td>
                <td>
                    <input name="invoice_amount" class="easyui-numberbox" readonly precision="4" style="width:250px;">
                </td>
            </tr>
            <tr>
                <td><span style="color:blue;" title="该选项取值于当票客户的debit note account">银行账户信息</span></td>
                <td>
                    <select name="account_id" id="dn_account_id" class="easyui-combobox" style="width:230px;" data-options="
                        editable:false,
                        valueField:'id',
                        textField:'title_cn',
                    ">
                    </select>
                    <a href="http://wenda.leagueshipping.com/?/question/395" target="_blank">提示</a>
                </td>
            </tr>
            <tr>
                <td><?= lang('remark');?></td>
                <td>
                    <textarea name="remark" id="dn_ramark" style="width:250px;"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right;">
                    <a href="javascript:void(0);" onclick="save_dn_invoice()" class="easyui-linkbutton"><?= lang('save');?></a>
                </td>
            </tr>
        </table>
    </form>
</div>
