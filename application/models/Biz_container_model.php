<?php
   class biz_container_model extends CI_Model{
        public function __construct(){
            $this->load->database();
        }
        public function get_by_id($id=0){
                $this->db->where('id', $id);
                $query = $this->db->get('biz_container');
                return $query->row_array();
        } 
        public function get_one($f="",$v=""){
                $this->db->where($f, $v);
                $query = $this->db->get('biz_container');
                return $query->row_array();
        } 
        public function get_where_one($where = ''){
                $this->db->where($where);
                $query = $this->db->get('biz_container');
                return $query->row_array();
        } 
		public function get_option($consol_id){      
				$this->db->where('consol_id', $consol_id);  
                $query = $this->db->get('biz_container'); 
                return $query->result_array(); 
        } 
        public function get($where="" ,$sort="id",$order="desc"){ 
			if($where !="")	$this->db->where($where);	
			$this->db->order_by($sort,$order);		
            $query = $this->db->get('biz_container');
            return $query->result_array();
        }
        
        public function no_role_get($where="" ,$sort="id",$order="desc"){
           if($where !="")	$this->db->where($where);
           $this->db->order_by($sort,$order);
           $query = $this->db->get('biz_container');
           return $query->result_array();
        }
		
		public function total($where=""){  
			if($where !="")	$this->db->where($where);	
            $this->db->from('biz_container');
            return  $this->db->count_all_results('');
        }  	           
        
        public function save($data=array()){
            $data["created_time"] = date('y-m-d H:i:s',time());
            $data["updated_time"] = date('y-m-d H:i:s',time());
            $data["created_by"] =  $this->session->userdata('id');
            $data["updated_by"] =  $this->session->userdata('id');
            $data["created_group"] = implode(',', $this->session->userdata('group') );
            $this->db->insert('biz_container', $data);
            return $this->db->insert_id();
        } 
        
        public function update($id='',$data=array()){
				$data["updated_by"] =  $this->session->userdata('id');
                $this->db->where('id', $id);
                $this->db->update('biz_container', $data);   
				return $id;
        }   
        
        public function mdelete($id=''){ 
				// delete container
                $this->db->where('id', $id);
                $this->db->delete('biz_container');                                                
        }         
        
        public function get_container_size_count($consol_id = 0, $where = '',$filed = ''){
            if($filed != '') $filed = ',' . $filed;
            $this->db->select('container_size, count(*) as count' . $filed);
            $this->db->group_by('container_size');
            if($where != '') $this->db->where($where);
            $this->db->where('consol_id', $consol_id);
            $query = $this->db->from('biz_container');
            return $query->get()->result_array();
        }
   }