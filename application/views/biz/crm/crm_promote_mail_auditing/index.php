<table id="dg" class="easyui-datagrid" title="邮件审核"
       data-options="singleSelect:true,url:'/crm_promote_mail_auditing/get_data',method:'get',pageSize:20" pagination="true"
       toolbar="#toolbar">
    <thead>
    <tr>
        <th data-options="field:'id',width:40">ID</th>
        <th data-options="field:'send_group',width:200">发送组|发送组id</th>
        <th data-options="field:'mail_title',width:300">邮件标题</th>
        <th data-options="field:'sender_name',width:150">发送名称</th>
        <th data-options="field:'status_str',width:80">状态</th>
        <th data-options="field:'create_name',width:80,">创建人</th>
        <th data-options="field:'create_time',width:180,">创建时间</th>
        <th data-options="field:'operate',width:220,">操作</th>
    </tr>
    </thead>
</table>
<div id="w" class="easyui-window" title="审核并发送" closed="true" data-options="iconCls:'icon-save',minimizable:false,tools:'#tt'" style="width:500px;height:400px;padding:10px;">

</div>


<script type="text/javascript" src="/inc/layer3.1.1/layer.js"></script>
<script>
    /**
     * 审核(拒绝)或发送发送
     */
    function view_details(id){
        $("#w").html("");

        $.get("/crm_promote_mail_auditing/view_details?id="+id,function(data){


            var obj = JSON.parse(data);
            if(obj.code==200){
                $("#w").append(obj.ht);
                $('#w').window('open');
                $.parser.parse("#w");

            }
            else {
                layer.msg(obj.msg)
            }
        });

    }
</script>

<script>

    function sendMail(id){
        layer.confirm('您确定群发当前邮件？', {
            btn: ['确定','取消'] //按钮
        }, function()
        {
            layer.closeAll('dialog');
            $.getJSON("/crm_promote_mail_send/send?id="+id,function(data){

                if(data.code==200){
                    layer.msg(data.msg);
                    $('#dg').datagrid('reload');
                }
                else {
                    layer.msg(data.msg);
                }

            });

        });
    }
    
    function edit(id) {
        window.open('/crm_promote_mail/edit?id=' + id);
    }
</script>
