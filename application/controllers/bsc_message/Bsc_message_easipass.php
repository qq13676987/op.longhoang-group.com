<?php


class bsc_message_easipass extends Common_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bsc_message_model');
        $this->model = $this->bsc_message_model;
    }

    /**
     * 亿通SO
     */
    public function message_so($consol_id, $file_function = 9){
        $time = time();
        $file_sum = 0;
        $file_content = '';
        $consol = Model('biz_consol_model')->get_by_id($consol_id);
        if(empty($consol)) return jsonEcho(array('code' => 1, 'msg' => lang('consol不存在')));

        $shipments = Model('biz_shipment_model')->get_shipment_by_consol($consol_id);
        if(empty($shipments)) return jsonEcho(array('code' => 1, 'msg' => lang('请绑定shipment后再试')));
        $this->db->select("client_code,client_name");
        $carrier = Model('biz_client_model')->get_where_one("client_code = '{$consol['trans_carrier']}'");

        //00  记录00		HEAD RECORD头记录				M
        $message = array();
        $message['00'] = array();
        $message['00'][] = '00';//RECORD ID		记录类型标识	9(2)	：00（“：”是不需要的，下同）  M
        $message['00'][] = 'IFTMBF';//MESSAGE TYPE		报文类型	X(6)	IFTMBF     M
        $message['00'][] = 'BOOKING';//FILE DESCRIPTION		文件说明	X(35)	BOOKING    C
        $message['00'][] = $file_function;//FILE FUNCTION		文件功能	X(2)	2=增加3=删除4=变更9=原始    M
        $message['00'][] = '312233460';//SENDER CODE		发送方代码	X(13)      M  TODO
        $message['00'][] = $carrier['client_name'];//RECEIVER CODE		接收方代码	X(13)      M   这个好像基本都是简称, 只有极少数是其他的 比如星瀚是 71785616-4
        $message['00'][] = date('YmdHi', $time);//FILE CREATE TIME		文件建立时间	9(12)	CCYYMMDDHHMM     M
        $file_sum++;

        //02   记录02		BOOKING订舱信息				M
        $message['02'] = array();
        $message['02'][] = '02';//RECORD ID		记录类型标识	9(2)	：02	M
        $message['02'][] = $consol['job_no'];//BOOKING NO.ID		运编号	X(35)		M
        $message['02'][] = $consol['carrier_ref'];//B\L NO.		提单号	X(20)	中谷订舱必填	C
        $message['02'][] = $consol['trans_term'];//DELIVERY TERM		交货条款	X(9)	CY-CY(pier-pier/port):30CY-CFS(pier/port-door):29CFS-CY(door-pier/port):28CFS-CFS(door-door):27	M
        $message['02'][] = "";//BOOKING PARTY		订舱人说明	X(70)	货代方企业名称	C
        $message['02'][] = $carrier['client_name'];//ISSUE PARTY CODE		签单人代码	X(13)	即接受订舱的人的代码	M  TODO
        $message['02'][] = "";//ISSUE PARTY		签单人说明	X(35)		C
        $message['02'][] = "";//APPLICANT		询价单位	X(13)	向船公司询价的单位代码，中谷舱必填	C
        $message['02'][] = "";//FOB BK PARTY		国外订舱单位	X(13)	国外FOB货订舱单位的代码	C
        $message['02'][] = "";//B/L TRANSHIP ID		转船标识	X(1)	Y/N  C
        $message['02'][] = "";//BATCH ID		分批	X(1)	Y/N	C
        $message['02'][] = "";//SHIPMENT DATE		装期	9(8)	CCYYMMDD	C
        $message['02'][] = "";//EXPIRY DATE		效期	9(8)	CCYYMMDD	C
        $message['02'][] = "";//QUOTATION NO.		运费协议号	X(30)	MAERSK订舱必选	C   注：QUOTATION NO.项填本票订舱货的运价是通过向中集询价后定下的特殊运价编号。
        $message['02'][] = "";//CHARGE TYPE		费率本代码	X(1)		C
        $message['02'][] = $consol['AGR_no'];//S/C NO.		合约号	X(30)		M
        $message['02'][] = "";//BOND NO.		订舱号	X(15)	FESCO用作订舱号	C
        $message['02'][] = "";//SLOT CHARTER ID		舱位互用标识	X(1)		C
        $message['02'][] = "";//EMAIL		订舱人邮件	X(250)		C
        $message['02'][] = "";//NAME		订舱人名字	X(250)		C
        $message['02'][] = "";//TEL		订舱人电话	X(250)		C
        $message['02'][] = "";//Commodity Group Code			X(70)	YML专用	C
        $message['02'][] = "";//invoiceTitle		发票抬头	X(200)	FESCO专用	C
        $message['02'][] = "";//shipmentCondition		Shipment condition 	X(70)	FESCO专用	C
        $file_sum++;

        //03    记录03		B/L提单信息				C
        $message['03'] = array();
        $message['03'][] = '03';//RECORD ID		记录类型标识	9(2)	：03	M
        $release_type_config = array('OBL' => 'ORI', 'SWB' => 'EXP', 'TER' => 'TER');
        if(isset($release_type_config[$consol['release_type']])) {
            $release_type = $release_type_config[$consol['release_type']];
        }else{
            return jsonEcho(array('code' => 1, 'msg' => lang('不支持当前放单方式')));
        }
        $message['03'][] = $release_type;//TYPE OF B/L FORM		提单类型	X(4)	ORI:BILL OF LADING；EXP:WAYBILL；TER:电放	M
        $message['03'][] = $consol['trans_origin'];//PLACE OF B/L ISSUE CODE		提单签发地代码	X(5)	中谷非必填	M/C
        $message['03'][] = $consol['trans_origin_name'];//PLACE OF B/L ISSUE		提单签发地	X(35)	中谷非必填	C/M
        $message['03'][] = "";//DATE OF ISSUE		签发日期	9(8)	CCYYMMDD	C
        $message['03'][] = "03";//NUMBERS OF ORIGINAL B/L		正本提单份数	9(2)		M
        $message['03'][] = "";//PREPAID AT		预付地点	X(35)		C
        $message['03'][] = "";//PAYABLE AT		到付地点	X(35)		C
        $file_sum++;

        //11    记录11		VESSEL船舶信息				M
        $message['11'] = array();
        $message['11'][] = '11';//RECORD ID		记录类型标识	9(2)	11	M
        $message['11'][] = '';//VESSEL CODE		船舶呼号	X(9)	船名存在时该项为必选	O  TODO
        $message['11'][] = $consol['vessel'];//VESSEL		船名	X(35)	‘要求装运日期’为空时，该项必选；中谷订舱必填	O
        $message['11'][] = $consol['voyage'];//VOYAGE		航次	X(10)		O
        $message['11'][] = '';//SHIPPING LINE CODE		船舶经营人代码	X(13)		C
        $message['11'][] = '';//SHIPPING LINE CODE		船舶经营人代码	X(13)		C
        $message['11'][] = '';//SHIPPING LINE		船舶经营人	X(35)		C
        $message['11'][] = '';//B/L CARRY CODE		提单承运人代码	X(13)		C
        $message['11'][] = '';//B/L CARRY		提单承运人	X(35)		C
        $message['11'][] = '';//REQUESTED SHIPMENT DATE		要求装运日期	9(8)	CCYYMMDD。‘船名/航次’为空时,该项必选	O
        $message['11'][] = $consol['sailing_code'];//TRADE CODE		航线代码	X(10)		C
        $message['11'][] = '';//TRADE		航线	X(35)		C
        $message['11'][] = '';//PRE.VESSEL CODE		前程运输船名代码	X(9)		C
        $message['11'][] = '';//PRE.VESSEL		前程运输船名	X(35)		C
        $message['11'][] = '';//PRE.VOYAGE		前程运输航次	X(6)	包括航向	C
        $message['11'][] = '';//PRE.ETD		前程船预计开航日	9(8)	CCYYMMDD 中谷专用	C
//        $message['11'][] = '';//PRE. CARRY CODE		"前程运输承运人代码 	"	X(35)	中谷专用	C
//        $message['11'][] = '';//2023-05-24 不知道是啥,报错提示17行,我就加了
        $file_sum++;

        //12    记录12		PORTS地点信息				M    该记录在MAERSK订舱时除8字段9字段外都为必选
        $message['12'] = array();
        $message['12'][] = '12';//RECORD ID		记录类型标识	9(2)	12	M
        $message['12'][] = '';//PLACE CODE OF RECEIPT		收货地代码	X(5)	OOCL、HLC、中谷的订舱要求必选	O
        $message['12'][] = '';//PLACE OF RECEIPT		收货地	X(35)	中谷订舱必填	C
        $message['12'][] = $consol['trans_origin'];//LOAD PORT CODE		装货港代码	X(5)	OOCL、中谷的订舱要求必选	O
        $message['12'][] = $consol['trans_origin_name'];//LOAD PORT		装货港	X(35)	中谷订舱必填	C
        $message['12'][] = $consol['trans_discharge'];//DISCHARGE PORT CODE		卸货港代码	X(5)	OOCL的订舱要求必选	M
        $message['12'][] = $consol['trans_discharge_name'];//DISCHARGE PORT		卸货港	X(35)	中谷订舱必填	C
        $message['12'][] = '';//TRANSFER PORT CODE		中转港代码	X(5)		C
        $message['12'][] = '';//TRANSFER PORT		中转港	X(35)		C
        $message['12'][] = $consol['trans_destination'];//PLACE OF DELIVERY CODE		交货地代码	X(5)	OOCL、中谷的订舱要求必选	O
        $message['12'][] = $consol['trans_destination_name'];//PLACE OF DELIVERY		交货地	X(35)	中谷订舱必填	C
        $message['12'][] = '';//FINAL DESTINATION CODE		目的地代码	X(5)		C
        $message['12'][] = '';//FINAL DESTINATION CODE		目的地	X(35)		C
        $message['12'][] = '';//Depot Code		堆场代码	X(5)	外地堆场专用(apl订舱专用)	C
        $message['12'][] = '';//CTN PORT		提箱港口代码	X(5)	异地提箱专用，否则无须填写	C
        $message['12'][] = '';//CTN PORT NAME		提箱港口	X(35)		C
        $file_sum++;

//        //13    记录13		OPTI    ON PORTS可选港信息				C
//        $message['13'] = array();
//        $message['13'][] = '13';//RECORD ID		记录类型标识	9(2)	13	M
//        $message['13'][] = '';//OPT.DISCH.PORT CODE		可选卸货港代码	X(5)	中谷订舱非必填	M/C
//        $message['13'][] = '';//OPT.DISCH.PORT		可选卸货港	X(35)	中谷订舱非必填	C/M
//        $message['13'][] = '';//OPT.PLACE OF DELIVERY CODE		可选交货地代码	X(5)		C
//        $message['13'][] = '';//OPT.PLACE OF DELIVERY		可选交货地	X(70)		C

//        //14    记录14		FREIGHT CLAUSE运费条款				C
//        $message['14'] = array();
//        $message['14'][] = '14';//RECORD ID		记录类型标识	9(2)	14	M
//        $message['14'][] = '';//FR.CLAUSE.CODE		运费条款代码	X(3)	中谷订舱必填	C
//        $message['14'][] = '';//FREIGHT CLAUSE-I(1-2)		运费条款	X(40)		M   注；本记录填运费条款有特殊的描述：第三地付款、第一程运费预付/第二程运费到付等，本记录信息用于提单。

//        //15    记录15		FREIGHT&CHARGES DETAILS运费及费用细目				C
//        $message['15'] = array();
//        $message['15'][] = '15';//RECORD ID		记录类型标识	9(2)	15	M
//        $message['15'][] = '';//FR.CH.CODE		运费及费用代码	X(3)		C
//        $message['15'][] = '';//FR.CH.REMARK		运费及费用说明	X(50)		C
//        $message['15'][] = '';//PREPAID OR COLLECT		付款方式	X(1)	P/C/F/R/E/L	M   注:付款方式：[P]=预付[C]=到付[F]=免费[R]=备注记录[E]=第三地付款[L]=第一程预付/第二程到付。
//        $message['15'][] = '';//PAYABLE AT(E)第三		地付款地点代码	X(5)		C
//        $message['15'][] = '';//PAYEE CODE		收款人代码	X(13)		C
//        $message['15'][] = '';//QUANTITY		数量	9(6).999		C
//        $message['15'][] = '';//CURRENCY		币种	X(3)		C
//        $message['15'][] = '';//RATE OF FR.CH.		费率	9(10).999		C
//        $message['15'][] = '';//UNIT OF QUANTITY		数量单位	X(3)		C
//        $message['15'][] = '';//AMOUNT		金额	9(10).99		C

//        //17    记录17		REMARKS其他信息				C
//        $message['17'] = array();
//        $message['17'][] = '17';//RECORD ID		记录类型标识	9(2)	17	M
//        $message['17'][] = '';//REMARKS(1-5)		备注	X(70)		M
//        $message['17'][] = '';//CURRENCY		币种	X(3)		C
//        $message['17'][] = '';//CARGO AMOUNT		货物金额	9(10).999		C
//        $message['17'][] = '';//MODE OF STUFFING		装箱方式	X(35)	自选/内装/自派车队等	C
//        $message['17'][] = '';//SPEC VANNING		特殊装载要求	X(5)	用户填写特殊装载要求代码	C

        //18    记录18		EXT REMARKS 补充备注信息				C
        $message['18'] = array();
        $message['18'][] = '18';//RECORD ID		记录类型标识	9(2)	18	M
        $apply_remark = $shipments[0]['requirements'];
        $apply_remark_array = filter_unique_array(explode("\r\n", $apply_remark));
        $apply_remark_array = cut_array_length($apply_remark_array,5,60);

        for ($i = 0; $i < 5; $i++){
            $message['18'][] = isset($apply_remark_array[$i]) ? $apply_remark_array[$i] : '';//REMARKS(1-5)		备注	X(70)		M
        }
        $file_sum++;

        $description = $consol['description'];

        //20    记录20		SHIPPER发货人				M
        $message['20'] = array();
        $message['20'][] = '20';//RECORD ID		记录类型标识	9(2)	20	M
        $message['20'][] = '';//SHIPPER CODE		发货人代码	X(13)		C/M
        $company_name = cut_array_length(array($consol['shipper_company']), 35, 3);
        $length = 9;//这是字段长度  shipper是8
        $address_size = $length - sizeof($company_name);//获取下名称占用的长度
        $address = filter_unique_array(explode("\r\n", $consol['shipper_address']));
        $address = cut_array_length($address, 35, $address_size + 1);//切一下地址
        //高丽提出, 如果超长 ,那么拼到品名里
        if(sizeof($address) >= $address_size + 1) {
            $description .= "\r\n" . "* {$consol['shipper_address']}";
            $address = array('*');
        }
        krsort($company_name);//反向排序下,直接unshift插入最前面
        foreach ($company_name as $val){
            //把名称的都塞到地址最前面
            array_unshift($address, $val);
        }
        for ($i = 0; $i < $length ; $i++){
            $message['20'][] = isset($address[$i]) ? $address[$i] : '';
        }
        $file_sum++;

        //21    记录21		CONSIGNEE收货人				M
        $message['21'] = array();
        $message['21'][] = '21';//RECORD ID		记录类型标识	9(2)	21	M
        $message['21'][] = '';//CONSIGNEE CODE		收货人代码	X(13)		C/M
        $company_name = cut_array_length(array($consol['consignee_company']), 35, 3);
        $length = 9;//这是字段长度
        $address_size = $length - sizeof($company_name);//获取下名称占用的长度
        $address = filter_unique_array(explode("\r\n", $consol['shipper_address']));
        $address = cut_array_length($address, 35, $address_size + 1);//切一下地址
        //高丽提出, 如果超长 ,那么拼到品名里
        if(sizeof($address) >= $address_size + 1) {
            $description .= "\r\n" . "** {$consol['consignee_address']}";
            $address = array('**');
        }
        krsort($company_name);//反向排序下,直接unshift插入最前面
        foreach ($company_name as $val){
            //把名称的都塞到地址最前面
            array_unshift($address, $val);
        }
        for ($i = 0; $i < $length ; $i++){
            $message['21'][] = isset($address[$i]) ? $address[$i] : '';
        }
        $file_sum++;

        //22    记录22		NOTIFY PARTY通知人				M
        $message['22'] = array();
        $message['22'][] = '22';//RECORD ID		记录类型标识	9(2)	22	M
        $message['22'][] = '';//NOTIFY CODE		通知人代码	X(13)	可以有9个通知人	C/M
        $company_name = cut_array_length(array($consol['notify_company']), 35, 3);
        $length = 9;//这是字段长度
        $address_size = $length - sizeof($company_name);//获取下名称占用的长度
        $address = filter_unique_array(explode("\r\n", $consol['notify_address']));
        $address = cut_array_length($address, 35, $address_size + 1);//切一下地址
        //高丽提出, 如果超长 ,那么拼到品名里
        if(sizeof($address) >= $address_size + 1) {
            $description .= "\r\n" . "*** {$consol['consignee_address']}";
            $address = array('***');
        }
        krsort($company_name);//反向排序下,直接unshift插入最前面
        foreach ($company_name as $val){
            //把名称的都塞到地址最前面
            array_unshift($address, $val);
        }
        for ($i = 0; $i < $length ; $i++){
            $message['22'][] = isset($address[$i]) ? $address[$i] : '';
        }
        $file_sum++;

//        //23    记录23		ALSO NOTIFY PARTY第三方通知人APL订舱专用				C
//        $message['23'] = array();
//        $message['23'][] = '23';//RECORD ID		记录类型标识	9(2)	23	M
//        $message['23'][] = '';//NOTIFY CODE		通知人代码	X(13)	中谷订舱非必填	C/M
//        $message['23'][] = '';//ALSONOTIFY(1-6)		第三方通知人	X(35)	中谷订舱非必填	M/C

//        //26    记录26		CP（ZIM）				C
//        $message['26'] = array();
//        $message['26'][] = '26';//RECORD ID		记录类型标识	9(2)	26	M
//        $message['26'][] = '';//CP CODE		CP代码	X(13)		C/M
//        $message['26'][] = '';//CP(1-9)		CP	X(35)		M/C

        //41    记录41		CARGO OF BOOKING订舱货物				M
        //注：1)货物有两种包装，如是大包装(如托盘)，则必须同时填小包装件数和包装类型；
        //        2)CARGO ID(货物标识)的填法:S=普通,R=冷冻,D=危险,O=非标，C=化工
        $message['41'] = array();
        $message['41'][] = '41';//RECORD-ID		记录类型标识	9(2)	41	M
        $message['41'][] = '001';//CARGO SEQUENCE NO.		货物序号	9(3)		M
        $message['41'][] = '';//CARGO CODE		货类代码	X(12)	海关HS码 中谷订舱必填	C
        $goods_type_config = array('GC' => 'S', 'DR' => 'D');
        if(isset($goods_type_config[$consol['goods_type']])) $cargo_id = $goods_type_config[$consol['goods_type']];
        else return jsonEcho(array('code' => 1, 'msg' => lang("当前货物类型不支持")));
        $message['41'][] = $cargo_id;//CARGO ID		货物标识	X(1)	S/R/D/O	M

        $good_outers = $consol['good_outers'];
        $message['41'][] = $good_outers;//NUMBERS OF PKGS-1		第一层包装件数	9(6)	INSIDE PACKAGE	M

        //查询字典表获取该code
        $iso_code = Model('bsc_dict_model')->get_one('name', $consol['good_outers_unit'], 'packing_unit');
        if(!empty($iso_code)){
            $good_outers_unit_CODE = $iso_code['value'];
        }else{
            return jsonEcho(array('code' => 1, 'msg' => lang('未获取到当前包装单位配置')));
        }
        $good_volume = $consol['good_volume'];
        $good_weight = $consol['good_weight'];
        $good_outers_unit = $consol['good_outers_unit'];

        $message['41'][] = $good_outers_unit_CODE;//CODE OF PKGS-1		第一层包装类型	X(2)		M
        $message['41'][] = $good_outers_unit;//PACKAGES DES-1		第一层包装说明	X(35)		M
        $message['41'][] = $good_weight;//CARGO GROSS WT-1		第一层包装皮重	9(18)	单位:千克；所有整数位加小数位共18位	M
        $message['41'][] = $good_volume;//CARGO MEASUREMENT-1		第一层包装尺码	9(18)	单位:立方米；小数点后可保留5位数字，所有整数位加小数位共18位	M
        $message['41'][] = '';//NUMBERS OF PACKAGES-2		第二层包装件数	9(6)	INSIDE INSIDEPACKAGE	C
        $message['41'][] = '';//CODE OF PKGS-2		第二层包装类型	X(2)		C
        $message['41'][] = '';//PACKAGES DES.-2		第二层包装说明	X(35)		C
        $message['41'][] = '';//CARGO GROSS WT-2		第二层包装皮重	9(18)	单位:千克；所有整数位加小数位共18位	C
        $message['41'][] = '';//CARGO MEASUREMENT-2		第二层包装尺码	9(18)	单位:立方米；所有整数位加小数位共18位	C
        $message['41'][] = $good_weight;//CARGO GROSS WEIGHT		货毛重	9(18)	单位:千克；所有整数位加小数位共18位	M
        $message['41'][] = '';//CARGO NET WEIGHT		货净重	9(18)	单位:千克；所有整数位加小数位共18位	C
        $message['41'][] = '';//QUARANTINE CODING		检疫代码	X(1)		C
        $message['41'][] = '';//QUARANTINE CODING		检疫名称	X(35)		C
        $file_sum++;

//        //43    记录43		DANGEROUS,REEFER&OOG危险品,冷藏和超标信息				C
//        //注：温度中，除正(+)负(-)号及小数点外，最多只能三位数字。
//        $message['43'] = array();
//        $message['43'][] = '43';//RECORD ID		记录类型标识	9(2)	43	M
//        $message['43'][] = '';//CLASS		危险品分类	X(5)		O
//        $message['43'][] = '';//PAGE		危险品页号	X(7)		C
//        $message['43'][] = '';//UNDG NO.		联合国危险品编号	9(4)	当危险品时必须	O
//        $message['43'][] = '';//LABEL		危险品标签	X(32)		C
//        $message['43'][] = '';//FLASH POINT		危险货物闪点	X(5)	摄氏	C
//        $message['43'][] = '';//EMS NO.		船运危险品应急措施号	X(7)		C
//        $message['43'][] = '';//MFAG NO.		医疗急救指南号	X(4)		C
//        $message['43'][] = '';//MPT(MARINE POLLUTANT)		海运污染	X(1)	Y=有污染N=无污染	C
//        $message['43'][] = '';//EMERGENCY CONTACT		应急联系	X(35)		C
//        $message['43'][] = '';//REEFER VENTILATION FLUX		冷藏通风量	X(10)	注释：仅当冷藏箱存在时该项为必选，否则为可选	O
//        $message['43'][] = '';//TEMPERATURE ID		温度计量单位	X(1)	C=摄氏F=华氏	C
//        $message['43'][] = '';//TEMPERATURE SETTING		设置温度	X(5)	见注	C
//        $message['43'][] = '';//MIN.TEMPERATURE		冷藏最低温度	X(5)	见注	C
//        $message['43'][] = '';//MAX.TEMPERATURE		冷藏最高温度	X(5)	见注	C
//        $message['43'][] = '';//OVER LENGTH FRONT		前超	9(4)	厘米	C
//        $message['43'][] = '';//OVER LENGTH BACK		后超	9(4)	厘米	C
//        $message['43'][] = '';//OVER WIDTH LEFT		左超	9(4)	厘米	C
//        $message['43'][] = '';//OVER WIDTH RIGHT		右超	9(4)	厘米	C
//        $message['43'][] = '';//OVER HEIGHT		超高	9(4)	厘米	C
//        $message['43'][] = '';//IMDG Amend No		危险品更改号	X(2)	Kline订舱必填	C
//        $message['43'][] = '';//PACKING_GROUP		包装类别	X(30)	(I,II,III)	C
//        $message['43'][] = '';//Actual size L		实际尺寸长	9(6)	厘米	C
//        $message['43'][] = '';//Actual size W		实际尺寸宽	9(6)	厘米	C
//        $message['43'][] = '';//Actual size H		实际尺寸高	9(6)	厘米	C
//        $message['43'][] = '';//VARIANT			X(30)	(a,b,c)	C
//        $message['43'][] = '';//TECHNICALNAME			X(50)		C
//        $message['43'][] = '';//PACKING CODE		包装代码	X(50)		C
//        $message['43'][] = '';//OUT PACKAGE		外包装数量	X(50)		C
//        $message['43'][] = '';//proper shipping name		正式海洋运输名称	X(50)		C
//        $message['43'][] = '';//GROSS WT (OUTER)		货毛重 (危险品)	9(12)	can be up to 5 decimal place	C
//        $message['43'][] = '';//NET WT (OUTER)		货净重 (危险品)	9(12)	can be up to 5 decimal place	C
//        $message['43'][] = '';//EMERGENCY CONTACT PERSON		应急联系人	X(10)		C
//        $message['43'][] = '';//S/L/G		危险品形态	X(1)	G=GAS L=LIQUID S=SOLID P=PASTE	C
//        $message['43'][] = '';//LABEL-2		危险品标签 (2)	X(5)		C
//        $message['43'][] = '';//LABEL-3		危险品标签 (3)	X(5)		C
//        $message['43'][] = '';//LABEL-4		危险品标签 (4)	X(5)		C
//        $message['43'][] = '';//LABEL-5		危险品标签 (5)	X(5)		C

        //44    记录44		MARKS&NOS.唛头				M
        $message['44'] = array();
        $message['44'][] = '44';//RECORD ID		记录类型标识	9(2)	44	M
        $mark_nums = filter_unique_array(explode("\r\n", $consol['mark_nums']));
        $mark_nums = cut_array_length($mark_nums, 30, 11);
        //高丽提出, 如果超长 ,那么拼到品名里
        if(sizeof($mark_nums) >= 11) {
            $description .= "\r\n" . "**** {$consol['mark_nums']}";
            $mark_nums = array('****');
        }
        for ($i = 0; $i < 10 ; $i++) {
            $message['44'][] = isset($mark_nums[$i]) ? $mark_nums[$i] : '';//MARKS(1-10)		唛头	X(35)		M
        }
        //2023-06-29 由于上海亿通的说, 如果字段超长导致 报文错误 ,会暂存, 所以这里加入 自动拼接一段超长的报文
        //这里可能会出现超过10时, 变成第11行的情况, 再看吧
        $message['44'][] = "11111111111111111111111111111111111111111111111111111111111111111111111111111111";
        $file_sum++;

        //47    记录47		CARGO DESCRIPTION货物描述				M
            // $message['47'] = array();
        // $message['47'][] = '47';//RECORD ID		记录类型标识	9(2)	47	M
        // $description = filter_unique_array(explode("\r\n", $description));
        // $description = cut_array_length($description, 60, 5);
        // for ($i = 0; $i < 5 ; $i++) {
        //     $message['47'][] = isset($description[$i]) ? $description[$i] : '';//CARGO DESCRIPTION(1-5)		货物描述	X(70)		M
        // }
        //记录47		CARGO DESCRIPTION货物描述				M
        $description = filter_unique_array(explode("\r\n", $description));
        $description = cut_array_length($description, 60, 99);
        $message['47'] = array();
        //品名多切点,要循环的  向上取整,因为最后剩余的要循环的
        $for_num = 5;
        $description_for_num = ceil(sizeof($description) / $for_num);
        for($i = 0; $i < $description_for_num; $i++){
            $this_message = array();
            $this_message[] = "47";//RECORD ID		记录类型标识	9(2)	47	M

            for ($j = 0 + $for_num * $i; $j < 5 + $for_num * $i ; $j++) {
                $this_message[] = isset($description[$j]) ? $description[$j] : '';//CARGO DESCRIPTION(1-5)		货物描述	X(70)		M
            }
            $message['47'][] = $this_message;

            $file_sum++;
        }
        //由于下面会循环,这里提前处理下
        // $file_content .= message_format($message, true) . "\r\n";
        // $file_sum += sizeof($message);
        // $message = array();

        //48    记录48		CONTAINER OF BOOKING订舱预配箱				M
        $box_info = json_decode($consol['box_info'], true);
        $message['48'] = array();
        $trans_mode_config = array("FCL" => 'F', 'LCL' => 'L');
        if(isset($trans_mode_config[$consol['trans_mode']])) $trans_mode = $trans_mode_config[$consol['trans_mode']];
        else return jsonEcho(array('code' => 1, 'msg' => lang('不支持当前运输模式')));

        $container_owner_config = array('999' => 'T', 'SOC' => 'Y', 'COC' => 'N');
        if(isset($container_owner_config[$consol['container_owner']])) $container_owner = $container_owner_config[$consol['container_owner']];
        else return jsonEcho(array('code' => 1, 'msg' => lang('不支持当前箱东类型')));
        foreach ($box_info as $row){
            $size_iso = Model('bsc_dict_model')->get_one('value', $row['size'],'container_size');

            $this_message = array();
            $this_message[] = '48';//RECORD ID		记录类型标识	9(2)	48	M
            $this_message[] = $size_iso['name'];//CTN.SIZE&TYPE		集装箱尺寸类型	X(4)		M
            $this_message[] = $row['num'];//CTN.NUMBERS		集装箱箱数	9(6)		M
            $this_message[] = $trans_mode;//CTN.STATUS		集装箱状态	X(1)	F=整箱L=拼箱	M
            $this_message[] = '';//MASTER LCL NO.		主拼号	X(20)		C
            $this_message[] = '';//MODE OF STUFFING		装箱方式	X(35)	自选/内装/自派车队等	C
            $this_message[] = '';//VANNING DEPOT		装箱地点代码	X(13)		C
            $this_message[] = '';//VANNING DEPOT		装箱地点说明	X(35)		C
            $this_message[] = $container_owner;//CONTAINER SOC.		货主箱标志	X(1)	T=已提箱（仅限ONE）；Y=货主箱；N=非货主箱；	M
            $this_message[] = '';//CTN.NO.		箱号	X(500)	"SOC或已提箱必须填写 ， COC不允许填写
            $this_message[] = '';//如有多个箱号，以逗号分隔"	C
            $this_message[] = '';//L or G		代箱信息	X(1)	L=冷代干G=高代平	C
            $this_message[] = '';//OVER WEIGHT		超重箱标志	X(1)	Y=超重箱N=非超重箱	C
            $this_message[] = '';//ACTIVE REEFER		冷箱标志	X(1)	Y=冷箱N=非冷箱	C
            $this_message[] = '';//PLAN VANNING DATE		预计提箱日期	9(8)	格式：YYYYMMDD	C
            $this_message[] = '';//OGG		超限箱标志	X(1)	MOL华北专用	C
            $this_message[] = '';//SPECIAL REQUEST		特殊放箱要求	X(20)	特殊放箱要求（ONE），用途待定	C
            $this_message[] = '';//CTN.NUMBERS OF PACKAGES		预配箱内货物件数	9(6)	多箱为总件数	C
            $this_message[] = '';//CARGO WEIGHT		预配箱内货重	9(18)	"多箱为总货重
//            $this_message[] = '';//单位:千克；所有整数位加小数位共18位"	C
//            $this_message[] = '';//CARGO MEASUREMENT		预配箱内货物尺码	9(18)	"多箱为总尺码
//            $this_message[] = '';//单位:立方米；所有整数位加小数位共18位"	C
            $message['48'][] = $this_message;
            $file_sum++;
        }
        // $file_content .= message_format($message, true) . "\r\n";
        // $message = array();

//        //51    记录51		CONTAINER DETAIL已知箱号的集装箱细目				C
//        $message['51'] = array();
//        $message['51'][] = '51';//RECORD-ID		记录类型标识	9(2)	51	M
//        $message['51'][] = '';//CTN.NO.		箱号	X(20)		M
//        $message['51'][] = '';//CTN.TYPE&SIZE		箱型尺寸	X(4)		M
//        $message['51'][] = '';//SEAL NO.		铅封号	X(50)		M
//        $message['51'][] = '';//CTN.NUMBERS OF PACKAGES		箱内货物件数	9(6)		M
//        $message['51'][] = '';//CARGO NET WEIGHT		箱内货重	9(18)	单位:千克；所有整数位加小数位共18位	M
//        $message['51'][] = '';//CARGO TARE WEIGHT		箱皮重	9(18)	单位:千克；所有整数位加小数位共18位	C

        //99    记录99		TRAILER RECORD尾记录				M
        $message['99'] = array();
        $message['99'][] = '99';
        $message['99'][] = ++$file_sum;//16 记录文件总数
        $file_content .= message_format($message, true, array(array("?", "??"), array(":", "?:"), array("'", "?'"), array("+", "?+")));

        //接下来将报文进行转化
        //保存文件txt
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        //ESL_EDI/日期
        $new_dir = $root_path . '/upload/message/EASIPASS_EDI/SO/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $consol_id . '.txt';
        $file_path = $new_dir . $file_name;
        file_put_contents($file_path, $file_content);

        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => 'SO',
            'receiver' => 'EASIPASS',
        );
        update_message($consol_id, $message_data, 'SO');
        return jsonEcho(array('code' => 0, 'msg' => lang('文件生成成功')));
    }

    public function message_si($consol_id, $file_function = 9){
        $time = time();
        $file_sum = 0;
        $file_content = '';
        $consol = Model('biz_consol_model')->get_by_id($consol_id);
        if(empty($consol)) return jsonEcho(array('code' => 1, 'msg' => lang('consol不存在')));

        $shipments = Model('biz_shipment_model')->get_shipment_by_consol($consol_id);
        if(empty($shipments)) return jsonEcho(array('code' => 1, 'msg' => lang('请绑定shipment后再试')));
        $this->db->select("client_code,client_name");
        $carrier = Model('biz_client_model')->get_where_one("client_code = '{$consol['trans_carrier']}'");

        //记录00		HEAD RECORD头记录				M
        $message['00'] = array();
        $message['00'][] = "00";//RECORD ID		记录类型标识	9(2)	00	M
        $message['00'][] = "IFTMIN";//MESSAGE TYPE		报文类型	X(6)	IFTMIN	M
        $message['00'][] = "BOOKING";//FILE DESCRIPTION		文件说明	X(35)	BOOKING	C
        $message['00'][] = $file_function;//FILE FUNCTION		文件功能	X(2)	2=增加3=删除4=变更9=原始	M
        $message['00'][] = "312233460";//SENDER CODE		发送方代码	X(13)		M
        $message['00'][] = $carrier['client_name'];//RECEIVER CODE		接收方代码	X(13)		M
        $message['00'][] = date('Ymdhi', $time);//FILE CREATE TIME		文件建立时间	9(12)	CCYYMMDDHHMM	M
        $file_sum++;

        //记录02		BOOKING订舱信息				M
        $message['02'] = array();
        $message['02'][] = "02";//RECORD ID		记录类型标识	9(2)	02	M
        $message['02'][] = $consol['job_no'];//REFERENCE NO.		运编号	X(35)	一般为流水号	M
        $message['02'][] = $consol['carrier_ref'];//B\L NO.		提单号	X(20)	船公司的提单号，中谷订舱必填	C
        $message['02'][] = $consol['trans_term'];//DELIVERY TERM		交货条款	X(9)	CY-CY(pier-pier/port):30CY-CFS(pier/port-door):29CFS-CY(door-pier/port):28CFS-CFS(door-door):27	M
        $message['02'][] = "";//BOOKING PARTY		订舱人说明	X(70)	货代方企业名称或代码	C
        $message['02'][] = $carrier['client_name'];//ISSUE PARTY CODE		签单人代码	X(13)	即接受订舱的人的代码	M
        $message['02'][] = "";//ISSUE PARTY		签单人说明	X(35)		C
        $message['02'][] = "";//APPLICANT		询价单位	X(13)	向船公司询价的单位代码	C
        $message['02'][] = "";//FOB BK PARTY		国外订舱单位	X(13)	国外FOB货订舱单位的代码	C
        $message['02'][] = "";//B/L TRANSHIP ID		转船标识	X(1)	Y/N	C
        $message['02'][] = "";//BATCH ID		分批	X(1)	Y/N	C
        $message['02'][] = "";//SHIPMENT DATE		装期	9(8)	CCYYMMDD	C
        $message['02'][] = "";//EXPIRY DATE		效期	9(8)	CCYYMMDD	C
        $message['02'][] = "";//QUOTATION NO.		运费协议号	X(30)	MAERSK为必选	C   注：QUOTATION NO.项填本票订舱货的运价是通过向中集询价后定下的特殊运价编号。
        $message['02'][] = "";//CHARGE TYPE		费率本代码	X(1)		C
        $message['02'][] = "";//S/C NO.		合约号	X(30)		C
        $message['02'][] = "";//BOOKING NO.		船公司订舱编号	X(20)	船公司系统内部订舱编号	C
        $message['02'][] = "";//SLOT CHARTER ID		舱位互用标识	X(1)		C
        $message['02'][] = "";//AMS CODE		AMS代码	X(15)		C
        $message['02'][] = "";//SCAC CODE		SCAC代码	X(15)	（美森）	C
        $message['02'][] = "";//EMAIL		订舱人邮件	X(250)		C
        $message['02'][] = "";//NAME		订舱人名字	X(250)		C
        $message['02'][] = "";//TEL		订舱人电话	X(250)		C
        $message['02'][] = "";//Commodity Group Code			X(70)	继承YML专用字段，与FESCO无关	C
        $message['02'][] = "";//invoiceTitle		发票抬头	X(200)	FESCO专用	C
        $message['02'][] = "";//shipmentCondition		Shipment condition 	X(70)	FESCO专用	C
        $file_sum++;

        //记录03		B/L提单信息				C
        $message['03'] = array();
        $message['03'][] = "03";//RECORD ID		记录类型标识	9(2)	03	M
        $release_type_config = array('OBL' => 'ORI', 'SWB' => 'EXP', 'TER' => 'TER');
        if(isset($release_type_config[$consol['release_type']])) {
            $release_type = $release_type_config[$consol['release_type']];
        }else{
            return jsonEcho(array('code' => 1, 'msg' => lang('不支持当前放单方式')));
        }
        $message['03'][] = $release_type;//TYPE OF B/L FORM		提单类型	X(4)	ORI：BILL OF LADING；EXP：WAYBILL；TER：电放	M
        $message['03'][] = $consol['trans_origin'];//PLACE OF B/L ISSUE CODE		提单签发地代码	X(5)		M/C
        $message['03'][] = $consol['trans_origin_name'];//PLACE OF B/L ISSUE		提单签发地	X(35)	中谷订舱必填	C/M
        $message['03'][] = "";//DATE OF ISSUE		签发日期	9(8)	CCYYMMDD	C
        $message['03'][] = "03";//NUMBERS OF ORIGINAL B/L		正本提单份数	9(2)	中谷默认3	M
        $message['03'][] = "";//PREPAID AT		预付地点	X(35)		C
        $message['03'][] = "";//PAYABLE AT		到付地点	X(35)		C
        $message['03'][] = "";//NUMBERS OF COPY B/L		副本提单数	9(2)		C
        $message['03'][] = "";//HB/L		是否有HB/L	X(1)	“Y”或者“N”。如果是订MOL的，填写"Y"的话，请到WEB上传相关附件	C
        $message['03'][] = "";//DOC FREIGHTED		提单显示运费	X(1)	"“Y”或者“N”。填写""Y""为显示，填写""N""或者为空的话不显示 （美森）"	C
        $message['03'][] = "";//NVOCC Indication flag		AMS/ACI自发/代发/真实信息栏	X(1)	"空:No HBL      M:Carrier file HBL (M) AMS/ACI 代发      A:Shipper file HBL (A) AMS/ACI 自发      :AMS/ACI 真实      （ZIM）"	C
        $file_sum++;

        //记录04		B/L ADDITIONAL INFORMATION提单信息附加信息				C
//        $message['04'] = array();
//        $message['04'][] = "04";//RECORD ID		记录类型标识	9(2)	04	M
//        $message['04'][] = "";//TYPE OF B/L FORM		提单类型	X(4)	"CHB：CSAV HOUSE BILL
//        $message['04'][] = "";//CMW：CSAV MASTER WAYBILL
//        $message['04'][] = "";//CML：CSAV MASTER BILL OF LADING"	M
//        $message['04'][] = "";//ADDITIONAL INFO 1		附加信息1	X(35)	04/2为CHB时填HBL Number	C
//        $message['04'][] = "";//ADDITIONAL INFO 2		附加信息2	X(35)	04/2为CHB时填SCAC CODE	C
//        $message['04'][] = "";//ADDITIONAL INFO 3		附加信息3	X(35)		C
//        $message['04'][] = "";//ADDITIONAL INFO 4		附加信息4	X(35)		C
//        $message['04'][] = "";//ADDITIONAL INFO 5		附加信息5	X(35)		C

        //记录11		VESSEL船舶信息				M
        $message['11'] = array();
        $message['11'][] = "11";//RECORD ID		记录类型标识	9(2)	11	M
        $message['11'][] = "";//VESSEL CODE		船舶呼号	X(9)	船名存在时该项为必选	O
        $message['11'][] = $consol['vessel'];//VESSEL		船名	X(35)	‘要求装运日期’为空时，该项必选，中谷订舱必填	O
        $message['11'][] = $consol['voyage'];//VOYAGE		航次	X(10)		O
        $message['11'][] = "";//SHIPPING LINE CODE		船舶经营人代码	X(13)		C
        $message['11'][] = "";//SHIPPING LINE		船舶经营人	X(35)		C
        $message['11'][] = "";//B/L CARRY CODE		提单承运人代码	X(13)		C
        $message['11'][] = "";//B/L CARRY		提单承运人	X(35)		C
        $message['11'][] = "";//REQUESTED SHIPMENT DATE		要求装运日期	9(8)	CCYYMMDD。‘船名/航次’为空时,该项必选	O
        $message['11'][] = $consol['sailing_code'];//TRADE CODE		航线代码	X(10)		C
        $message['11'][] = "";//TRADE		航线	X(35)		C
        $message['11'][] = "";//PRE.VESSEL CODE		前程运输船名代码	X(9)		C
        $message['11'][] = "";//PRE.VESSEL		前程运输船名	X(35)		C
        $message['11'][] = "";//PRE.VOYAGE		前程运输航次	X(6)	包括航向	C
        $message['11'][] = "";//PRE.ETD		前程船预计开航日	9(8)	CCYYMMDD 中谷专用	C
        $message['11'][] = "";//PRE. CARRY CODE		"前程运输承运人代码 	"	X(35)	中谷专用	C
        $file_sum++;

        //记录12		PORTS地点信息				M
        //P&O、MAERSK做SI时，该记录内除8字段9字段外所有字段都必须提供
        $message['12'] = array();
        $message['12'][] = "12";//RECORD ID		记录类型标识	9(2)	12	M
        $message['12'][] = "";//PLACE CODE OF RECEIPT		收货地代码	X(5)	OOCL、HLC、中谷的订舱要求必选	O
        $message['12'][] = "";//PLACE OF RECEIPT		收货地	X(35)	中谷订舱必填	C
        $message['12'][] = $consol['trans_origin'];//LOAD PORT CODE		装货港代码	X(5)	OOCL、中谷的订舱要求必选	O
        $message['12'][] = $consol['trans_origin_name'];//LOAD PORT		装货港	X(35)	中谷订舱必填	C
        $message['12'][] = $consol['trans_discharge'];;//DISCHARGE PORT CODE		卸货港代码	X(5)	OOCL、中谷的订舱要求必选	M
        $message['12'][] = $consol['trans_discharge_name'];;//DISCHARGE PORT		卸货港	X(35)	中谷订舱必填	C
        $message['12'][] = "";//TRANSFER PORT CODE		中转港代码	X(5)		C
        $message['12'][] = "";//TRANSFER PORT		中转港	X(35)		C
        $message['12'][] = $consol['trans_destination'];//PLACE OF DELIVERY CODE		交货地代码	X(5)	OOCL、中谷的订舱要求必选	O
        $message['12'][] = $consol['trans_destination_name'];//PLACE OF DELIVERY		交货地	X(35)	中谷订舱必填	C
        $message['12'][] = "";//FINAL DESTINATION CODE		目的地代码	X(5)		C
        $message['12'][] = "";//FINAL DESTINATION CODE		目的地	X(35)		C
        $file_sum++;

        //记录13		OPTION PORTS可选港信息				C
//        $message['13'] = array();
//        $message['13'][] = "13";//RECORD ID		记录类型标识	9(2)	13	M
//        $message['13'][] = "";//OPT.DISCH.PORT CODE		可选卸货港代码	X(5)		M/C
//        $message['13'][] = "";//OPT.DISCH.PORT		可选卸货港	X(35)		C/M
//        $message['13'][] = "";//OPT.PLACE OF DELIVERY CODE		可选交货地代码	X(5)		C
//        $message['13'][] = "";//OPT.PLACE OF DELIVERY		可选交货地	X(70)		C

        //记录14		FREIGHT CLAUSE运费条款				C
        //注；本记录填运费条款有特殊的描述：第三地付款、第一程运费预付/第二程运费到付等，本记录信息用于提单。
//        $message['14'] = array();
//        $message['14'][] = "14";//RECORD ID		记录类型标识	9(2)	14	M
//        $message['14'][] = "";//FR.CLAUSE.CODE		运费条款代码	X(3)		C
//        $message['14'][] = "";//FREIGHT CLAUSE-I(1-2)		运费条款	X(40)		M

        //记录15		FREIGHT&CHARGES DETAILS运费及费用细目				C
//        $message['15'] = array();
//        $message['15'][] = "15";//RECORD ID		记录类型标识	9(2)	15	M
//        $message['15'][] = "";//FR.CH.CODE		运费及费用代码	X(3)		C
//        $message['15'][] = "";//FR.CH.REMARK		运费及费用说明	X(35)		C
//        $message['15'][] = "";//PREPAID OR COLLECT		付款方式	X(1)	P/C/F/R/E/L  中谷订舱必填	M  注:付款方式：[P]=预付[C]=到付[F]=免费[R]=备注记录[E]=第三地付款[L]=第一程预付/第二程到付。
//        $message['15'][] = "";//PAYABLE AT(E)		第三地付款地点代码	X(5)		C
//        $message['15'][] = "";//PAYABLE AT		第三地付款地点全称	X(35)		C
//        $message['15'][] = "";//QUANTITY		数量	9(6).999		C
//        $message['15'][] = "";//CURRENCY		币种	X(3)		C
//        $message['15'][] = "";//RATE OF FR.CH.		费率	9(10).999		C
//        $message['15'][] = "";//UNIT OF QUANTITY		数量单位	X(3)		C
//        $message['15'][] = "";//AMOUNT		金额	9(10).99		C

        //记录17		REMARKS其他信息				C
//        $message['17'] = array();
//        $message['17'][] = "17";//RECORD ID		记录类型标识	9(2)	17	M
//        $message['17'][] = "";//REMARKS(1-5)		备注	X(70)		M
//        $message['17'][] = "";//CURRENCY		币种	X(3)		C
//        $message['17'][] = "";//CARGO AMOUNT		货物金额	9(10).999		C
//        $message['17'][] = "";//MODE OF STUFFING		装箱方式	X(35)	自选/内装/自派车队等	C
//        $message['17'][] = "";//SPECIAL TERM REMARKS		特殊条款备注	X(100)		C

        //记录18		EXT REMARKS 补充备注信息				C
        $message['18'] = array();
        $message['18'][] = "18";//RECORD ID		记录类型标识	9(2)	18	M
        $remark = $consol['document_remark'];

        $remark_array = filter_unique_array(explode("\r\n", $remark));
        $remark_array = cut_array_length($remark_array,60,5);
        for ($i = 0; $i < 5; $i++){
            $message['18'][] = isset($remark_array[$i]) ? $remark_array[$i] : '';//REMARKS(1-5)		备注	X(70)		M
        }

        $file_sum++;

        $description = $consol['description'];

        //记录20		SHIPPER发货人				M
        $message['20'] = array();
        $message['20'][] = "20";//RECORD ID		记录类型标识	9(2)	20	M
        $message['20'][] = "";//SHIPPER CODE		发货人代码	X(13)		C/M
        $company_name = cut_array_length(array($consol['shipper_company']), 35, 3);
        $length = 8;//这是字段长度 shipper是8
        $address_size = $length - sizeof($company_name);//获取下名称占用的长度
        $address = filter_unique_array(explode("\r\n", $consol['shipper_address']));
        $address = cut_array_length($address, 35, $address_size + 1);//切一下地址
        //高丽提出, 如果超长 ,那么拼到品名里
        if(sizeof($address) >= $address_size + 1) {
            $description .= "\r\n" . "* {$consol['shipper_address']}";
            $address = array('*');
        }
        krsort($company_name);//反向排序下,直接unshift插入最前面
        foreach ($company_name as $val){
            //把名称的都塞到地址最前面
            array_unshift($address, $val);
        }
        for ($i = 0; $i < $length ; $i++){
            $message['20'][] = isset($address[$i]) ? $address[$i] : '';
        }
        $file_sum++;

        //记录21		CONSIGNEE收货人				M
        $message['21'] = array();
        $message['21'][] = "21";//RECORD ID		记录类型标识	9(2)	21	M
        $message['21'][] = "";//CONSIGNEE CODE		收货人代码	X(13)		C/M
        $company_name = cut_array_length(array($consol['consignee_company']), 35, 3);
        $length = 9;//这是字段长度 shipper是8
        $address_size = $length - sizeof($company_name);//获取下名称占用的长度
        $address = filter_unique_array(explode("\r\n", $consol['consignee_address']));
        $address = cut_array_length($address, 35, $address_size + 1);//切一下地址
        //高丽提出, 如果超长 ,那么拼到品名里
        if(sizeof($address) >= $address_size + 1) {
            $description .= "\r\n" . "** {$consol['consignee_address']}";
            $address = array('**');
        }
        krsort($company_name);//反向排序下,直接unshift插入最前面
        foreach ($company_name as $val){
            //把名称的都塞到地址最前面
            array_unshift($address, $val);
        }
        for ($i = 0; $i < $length ; $i++){
            $message['21'][] = isset($address[$i]) ? $address[$i] : '';
        }
        $file_sum++;

        //记录22		NOTIFY PARTY通知人				M
        $message['22'] = array();
        $message['22'][] = "22";//RECORD ID		记录类型标识	9(2)	22	M
        $message['22'][] = "";//NOTIFY CODE		通知人代码	X(13)	可以有9个通知人	C/M
        $company_name = cut_array_length(array($consol['notify_company']), 35, 3);
        $length = 9;//这是字段长度 shipper是8
        $address_size = $length - sizeof($company_name);//获取下名称占用的长度
        $address = filter_unique_array(explode("\r\n", $consol['notify_address']));
        $address = cut_array_length($address, 35, $address_size + 1);//切一下地址
        //高丽提出, 如果超长 ,那么拼到品名里
        if(sizeof($address) >= $address_size + 1) {
            $description .= "\r\n" . "*** {$consol['notify_address']}";
            $address = array('***');
        }
        krsort($company_name);//反向排序下,直接unshift插入最前面
        foreach ($company_name as $val){
            //把名称的都塞到地址最前面
            array_unshift($address, $val);
        }
        for ($i = 0; $i < 8 ; $i++){
            $message['22'][] = isset($address[$i]) ? $address[$i] : '';
        }
        $file_sum++;

        //记录23		SHIPPER真实发货人 注：APLMBF SI中，此字段作为第三方通知人使用 C1  C
//        $message['23'] = array();
//        $message['23'][] = "";//RECORD ID		记录类型标识	9(2)	23	M
//        $message['23'][] = "";//SHIPPER CODE		真实发货人代码	X(13)		C/M
//        $message['23'][] = "";//SHIPPER(1-9)		真实发货人	X(35)		M/C

        //记录24		CONSIGNEE真实收货人				C
//        $message['24'] = array();
//        $message['24'][] = "";//RECORD ID		记录类型标识	9(2)	24	M
//        $message['24'][] = "";//CONSIGNEE CODE		真实收货人代码	X(13)		C/M
//        $message['24'][] = "";//CONSIGNEE(1-9)		真实收货人	X(35)		M/C

        //记录25		NOTYFY PARTY真实通知人				C
//        $message['25'] = array();
//        $message['25'][] = "";//RECORD ID		记录类型标识	9(2)	25	M
//        $message['25'][] = "";//NOTIFY CODE		真实通知人代码	X(13)		C/M
//        $message['25'][] = "";//NOTIFY(1-9)		真实通知人	X(35)		M/C

        //记录26		CP（ZIM）				C
//        $message['26'] = array();
//        $message['26'][] = "";//RECORD ID		记录类型标识	9(2)	26	M
//        $message['26'][] = "";//CP CODE		CP代码	X(13)		C/M
//        $message['26'][] = "";//CP(1-9)		CP	X(35)		M/C

        //记录41		CARGO OF BOOKING订舱货物				M
        //注：1)货物有两种包装，如是大包装(如托盘)，则必须同时填小包装件数和包装类型；
        //2)CARGO ID(货物标识)的填法:S=普通,R=冷冻,D=危险,O=非标
        $message['41'] = array();
        $message['41'][] = "41";//RECORD-ID		记录类型标识	9(2)	41	M
        $message['41'][] = "001";//CARGO SEQUENCE NO.		货物序号	9(3)		M
        $message['41'][] = "";//CARGO CODE		货类代码	X(12)	海关HS码，中谷、HMM订舱必填	C
        $goods_type_config = array('GC' => 'S', 'DR' => 'D');
        if(isset($goods_type_config[$consol['goods_type']])) $cargo_id = $goods_type_config[$consol['goods_type']];
        else return jsonEcho(array('code' => 1, 'msg' => lang("当前货物类型不支持")));
        $message['41'][] = $cargo_id;//CARGO ID		货物标识	X(1)	S/R/D/O	M
        $good_outers = $consol['good_outers'];
        $message['41'][] = $good_outers;//NUMBERS OF PKGS-1		第一层包装件数	9(6)	INSIDE PACKAGE	M

        //查询字典表获取该code
        $iso_code = Model('bsc_dict_model')->get_one('name', $consol['good_outers_unit'], 'packing_unit');
        if(!empty($iso_code)){
            $good_outers_unit_CODE = $iso_code['value'];
        }else{
            return jsonEcho(array('code' => 1, 'msg' => lang("未获取到当前包装单位配置")));
        }
        $good_volume = $consol['good_volume'];
        $good_weight = $consol['good_weight'];
        $good_outers_unit = $consol['good_outers_unit'];
        $message['41'][] = $good_outers_unit_CODE;//CODE OF PKGS-1		第一层包装类型	X(2)		M
        $message['41'][] = $good_outers_unit;//PACKAGES DES-1		第一层包装说明	X(35)		M
        $message['41'][] = $good_weight;//CARGO GROSS WT-1		第一层包装皮重	9(18)	单位:千克；所有整数位加小数位共18位	M
        $message['41'][] = $good_volume;//CARGO MEASUREMENT-1		第一层包装尺码	9(18)	单位:立方米；所有整数位加小数位共18位	M
        $message['41'][] = "";//NUMBERS OF PACKAGES-2		第二层包装件数	9(6)	INSIDE INSIDEPACKAGE	C
        $message['41'][] = "";//CODE OF PKGS-2		第二层包装类型	X(2)		C
        $message['41'][] = "";//PACKAGES DES.-2		第二层包装说明	X(35)		C
        $message['41'][] = "";//CARGO GROSS WT-2		第二层包装皮重	9(18)	单位:千克；所有整数位加小数位共18位	C
        $message['41'][] = "";//CARGO MEASUREMENT-2		第二层包装尺码	9(18)	单位:立方米；所有整数位加小数位共18位	C
        $message['41'][] = $good_weight;//CARGO GROSS WEIGHT		货毛重	9(18)	单位:千克；所有整数位加小数位共18位	M
        $message['41'][] = "";//CARGO NET WEIGHT		货净重	9(18)	单位:千克；所有整数位加小数位共18位	C
        $message['41'][] = "";//QUARANTINE CODING		检疫代码	X(1)		C
        $message['41'][] = "";//QUARANTINE CODING		检疫名称	X(35)		C
        $file_sum++;

        //由于下面会循环,这里提前处理下
//        $file_content .= message_format($message, true) . "\r\n";
//        $file_sum += sizeof($message);
//        $message = array();

//        //记录42		SPLIT GOODS PLACEMENT 箱货对应细目				C
//        $containers = Model('biz_container_model')->get("consol_id = {$consol_id}");
//        if(empty($containers)) return jsonEcho(array('code' => 1, 'msg' => lang("请先输入箱数据后再试")));
//        $message['42'] = array();
//        foreach ($containers as $container){
//            $sql = "SELECT SUM(biz_shipment_container.`packs`) AS packs, vgm,SUM(biz_shipment_container.`weight`) AS weight, SUM(biz_shipment_container.`volume`) AS volume FROM biz_shipment, biz_shipment_container
//WHERE `biz_shipment_container`.`shipment_id` = `biz_shipment`.id AND `biz_shipment`.`consol_id` = " . $container["consol_id"] . " AND `biz_shipment_container`.`container_no` = '" . $row["container_no"] . "'";
//            $temp_row = Model('m_model')->sqlquery($sql)->row_array();
//
//            $size_iso = Model('bsc_dict_model')->get_one('value', $container['size'],'container_size');
//
//            $message['42'][] = "42";//RECORD-ID		记录类型标识	9(2)	42	M
//            $message['42'][] = $container['container_no'];//CTN.NO.		箱号	X(20)	HMM订舱必填	M
//            $message['42'][] = $size_iso['name'];//CTN.TYPE&SIZE		箱型尺寸	X(4)		C
//            $message['42'][] = $container['seal_no'];//SEAL NO.		铅封号	X(30)		C
//            $message['42'][] = $temp_row['packs'];//CTN.NUMBERS OF PACKAGES		箱内货物件数	9(6)	HMM订舱必填	C
//            $message['42'][] = $temp_row['weight'];//CARGO WEIGHT		箱内货重	9(18)	单位:千克；所有整数位加小数位共18位，HMM订舱必填	C
//            $message['42'][] = $container['container_tare'];//CARGO TARE WEIGHT		箱皮重	9(18)	单位:千克；所有整数位加小数位共18位，HMM订舱必填	C
//            $message['42'][] = $temp_row['volume'];//CARGO MEASUREMENT		箱内货物尺码	9(18)	单位:立方米；所有整数位加小数位共18位，HMM订舱必填	C
//            $message['42'][] = "";//REMARKS(1-5)		备注	X(70)	HMM订舱必填	C
//        }


        //记录43		DANGEROUS,REEFER&OOG危险品,冷藏和超标信息				C
        //注：温度中，除正(+)负(-)号及小数点外，最多只能三位数字。
        //	如果是订MOL的，冷藏通风口填写选项（共11项）如下：
        //	"1/2 OPEN； 1/4 OPEN； 1/5 OPEN； 1/8 OPEN； 3/4 OPEN； 5%  OPEN； 10% OPEN； 15% OPEN；"	 CLOSED； NONE； OPEN
//        $message['43'] = array();
//        $message['43'][] = "43";//RECORD ID		记录类型标识	9(2)	43	M
//        $message['43'][] = "";//CLASS		危险品分类	X(5)	货物为危险品时为必选	O
//        $message['43'][] = "";//PAGE		危险品页号	X(7)		C
//        $message['43'][] = "";//UNDG NO.		联合国危险品编号	9(4)	货物为危险品时为必选	O
//        $message['43'][] = "";//LABEL		危险品标签	X(32)		C
//        $message['43'][] = "";//FLASH POINT		危险货物闪点	X(5)	摄氏	C
//        $message['43'][] = "";//EMS NO.		船运危险品应急措施号	X(6)		C
//        $message['43'][] = "";//MFAG NO.		医疗急救指南号	X(4)		C
//        $message['43'][] = "";//MPT(MARINE POLLUTANT)		海运污染	X(1)	Y=有污染N=无污染	C
//        $message['43'][] = "";//EMERGENCY CONTACT		应急联系	X(35)		C
//        $message['43'][] = "";//REEFER VENTILATION FLUX		冷藏通风量(口)	X(10)	注释：仅当冷藏箱存在时该项为必选，否则为可选	O
//        $message['43'][] = "";//TEMPERATURE ID		温度计量单位	X(1)	C=摄氏F=华氏	C
//        $message['43'][] = "";//TEMPERATURE SETTING		设置温度	X(5)	见注	C
//        $message['43'][] = "";//MIN.TEMPERATURE		冷藏最低温度	X(5)	见注	C
//        $message['43'][] = "";//MAX.TEMPERATURE		冷藏最高温度	X(5)	见注	C
//        $message['43'][] = "";//OVER LENGTH FRONT		前超	9(4)	厘米	C
//        $message['43'][] = "";//OVER LENGTH BACK		后超	9(4)	厘米	C
//        $message['43'][] = "";//OVER WIDTH LEFT		左超	9(4)	厘米	C
//        $message['43'][] = "";//OVER WIDTH RIGHT		右超	9(4)	厘米	C
//        $message['43'][] = "";//OVER HEIGHT		超高	9(4)	厘米	C

        //记录44		MARKS&NOS.唛头				M
        $message['44'] = array();
        $message['44'][] = "44";//RECORD ID		记录类型标识	9(2)	44	M
        $mark_nums = filter_unique_array(explode("\r\n", $consol['mark_nums']));
        $mark_nums = cut_array_length($mark_nums, 30, 11);
        //高丽提出, 如果超长 ,那么拼到品名里
        if(sizeof($mark_nums) >= 11) {
            $description .= "\r\n" . "**** {$consol['mark_nums']}";
            $mark_nums = array('****');
        }
        for ($i = 0; $i < 10 ; $i++) {
            $message['44'][] = isset($mark_nums[$i]) ? $mark_nums[$i] : '';//MARKS(1-10)		唛头	X(35)		M
        }
        //2023-06-29 由于上海亿通的说, 如果字段超长导致 报文错误 ,会暂存, 所以这里加入 自动拼接一段超长的报文
        //这里可能会出现超过10时, 变成第11行的情况, 再看吧
        $message['44'][] = "11111111111111111111111111111111111111111111111111111111111111111111111111111111";
        $file_sum++;

        //记录47		CARGO DESCRIPTION货物描述				M
        $description = filter_unique_array(explode("\r\n", $description));
        $description = cut_array_length($description, 60, 99);
        $message['47'] = array();
        //品名多切点,要循环的  向上取整,因为最后剩余的要循环的
        $for_num = 5;
        $description_for_num = ceil(sizeof($description) / $for_num);
        for($i = 0; $i < $description_for_num; $i++){
            $this_message = array();
            $this_message[] = "47";//RECORD ID		记录类型标识	9(2)	47	M

            for ($j = 0 + $for_num * $i; $j < 5 + $for_num * $i ; $j++) {
                $this_message[] = isset($description[$j]) ? $description[$j] : '';//CARGO DESCRIPTION(1-5)		货物描述	X(70)		M
            }
            $message['47'][] = $this_message;

            $file_sum++;
        }

        //记录48		CONTAINER OF BOOKING订舱预配箱				M
        $message['48'] = array();
        $box_info = json_decode($consol['box_info'], true);

        $trans_mode_config = array("FCL" => 'F', 'LCL' => 'L');
        if(isset($trans_mode_config[$consol['trans_mode']])) $trans_mode = $trans_mode_config[$consol['trans_mode']];
        else return jsonEcho(array('code' => 1, 'msg' => lang("不支持当前运输模式({trans_mode})", array('trans_mode' => $consol['trans_mode']))));

        $container_owner_config = array('999' => 'T', 'SOC' => 'Y', 'COC' => 'N');
        if(isset($container_owner_config[$consol['container_owner']])) $container_owner = $container_owner_config[$consol['container_owner']];
        else return jsonEcho(array('code' => 1, 'msg' => lang("不支持当前箱东类型")));
        foreach ($box_info as $row){
            $size_iso = Model('bsc_dict_model')->get_one('value', $row['size'],'container_size');

            $this_message = array();
            $this_message[] = "48";//RECORD ID		记录类型标识	9(2)	48	M
            $this_message[] = $size_iso['name'];//CTN.SIZE&TYPE		集装箱尺寸类型	X(4)		M
            $this_message[] = $row['num'];//CTN.NUMBERS		集装箱箱数	9(6)		M
            $this_message[] = $trans_mode;//CTN.STATUS		集装箱状态	X(1)	F=整箱L=拼箱	M
            $this_message[] = "";//MASTER LCL NO.		主拼号	X(20)	中谷：如果是分单，主单号必填	C
            $this_message[] = "";//MODE OF STUFFING		装箱方式	X(35)	自选/内装/自派车队等	C
            $this_message[] = "";//VANNING DEPOT		装箱地点代码	X(13)		C
            $this_message[] = "";//VANNING DEPOT		装箱地点说明	X(35)		C
            $this_message[] = $container_owner;//CONTAINER SOC.		货主箱标志	X(1)	Y=货主箱N=非货主箱	M

            $message['48'][] = $this_message;
            $file_sum++;
        }

        //记录51		CONTAINER DETAIL 1已知箱号的集装箱细目1				C
        $containers = Model('biz_container_model')->get("consol_id = {$consol_id}");
        if(empty($containers)) return jsonEcho(array('code' => 1, 'msg' => lang("请先输入箱数据后再试")));
        $message['51'] = array();
        foreach ($containers as $container){
            $sql = "SELECT SUM(biz_shipment_container.`packs`) AS packs, vgm,SUM(biz_shipment_container.`weight`) AS weight, SUM(biz_shipment_container.`volume`) AS volume FROM biz_shipment, biz_shipment_container
WHERE `biz_shipment_container`.`shipment_id` = `biz_shipment`.id AND `biz_shipment`.`consol_id` = " . $container["consol_id"] . " AND `biz_shipment_container`.`container_no` = '" . $container["container_no"] . "'";
            $temp_row = Model('m_model')->sqlquery($sql)->row_array();

            $size_iso = Model('bsc_dict_model')->get_one('value', $container['container_size'],'container_size');

            $this_message = array();
            $this_message[] = "51";//RECORD-ID		记录类型标识	9(2)	51	M
            $this_message[] = $container['container_no'];//CTN.NO.		箱号	X(20)		M
            $this_message[] = $size_iso['name'];//CTN.TYPE&SIZE		箱型尺寸	X(4)		M
            $this_message[] = $container['seal_no'];//SEAL NO.		铅封号	X(30)		M
            $this_message[] = $temp_row['packs'];//CTN.NUMBERS OF PACKAGES		箱内货物件数	9(6)		M
            $this_message[] = $temp_row['weight'];//CARGO NET WEIGHT		箱内货重	9(18)	单位:千克；所有整数位加小数位共18位	M
            $this_message[] = $container['container_tare'];//CARGO TARE WEIGHT		箱皮重	9(18)	单位:千克；所有整数位加小数位共18位	C
            $this_message[] = $temp_row['volume'];//CARGO MEASUREMENT		箱内货物尺码	9(18)	单位:立方米（inttra订舱要求）；所有整数位加小数位共18位 中谷订舱必填	C
            $this_message[] = "";//REMARKS(1-5)		备注	X(70)		C
            $this_message[] = "";//Wooden Package Classification		木质包装分类	X(2)	"HLC专用：
            $this_message[] = "";//NA (not applicable), NT (not treated and not certified), TC (treated and certified), PR (processed)"	C
            $this_message[] = "";//REEFER VENTILATION FLUX		冷藏通风量(口)	X(10)	注释：仅当冷藏箱存在时该项为必选，否则为可选	C
            $this_message[] = "";//TEMPERATURE ID		温度计量单位	X(1)	C=摄氏F=华氏	C
            $this_message[] = "";//TEMPERATURE SETTING		设置温度	X(5)	见注	C
            $file_sum++;

            $message['51'][] = $this_message;
        }

        //记录52		CONTAINER DETAIL 1 已知箱号的集装箱细目1（补）				C
        // $message['52'] = array();
        // $message['52'][] = "52";//RECORD ID		记录类型标识	9(2)	52	M
        // $description = filter_unique_array(explode("\r\n", $consol['description']));
        // $description = cut_array_length($description, 60, 5);
        // for ($i = 0; $i < 8 ; $i++){
        //     $message['52'][] = isset($description[$i]) ? $description[$i] : '';//CARGO DESCRIPTION（1-5）		货物品名	X(70)	箱内货物品名	M
        // }
        // $file_sum++;

        //记录53		CONTAINER DETAIL 2已知箱号的集装箱细目2				C
//        $message['53'] = array();
//        $message['53'][] = "";//RECORD ID		记录类型标识	9(2)	53	M
//        $message['53'][] = "";//CARGO CODE		货类代码（HS代码）	X(12)	海关HS CODE	M
//        $message['53'][] = "";//NUMBERS OF PACKAGES		货物件数	9(6)	该HS code对应货物件数	C
//        $message['53'][] = "";//CARGO WEIGHT		货毛重	9(18)	该HS code对应货物重量。单位:千克；所有整数位加小数位共18位	C
//        $message['53'][] = "";//MEASUREMENT		货物体积	9(18)		C
//        $message['53'][] = "";//CODE OF PKGS		包装类型代码	X(2)	该HS code对应货物包装代码	C
//        $message['53'][] = "";//PACKAGES DES		包装说明	X(35)	该HS code对应货物包装说明	C
//        $message['53'][] = "";//CARGO VALUE		箱内货物价值	9(8)	该HS code对应货物货值	C
//        $message['53'][] = "";//CURRENCY CODE		金额类型代码	X(3)	该HS code对应货物货值货币类型	C

        //记录54		CONTAINER DETAIL 2 已知箱号的集装箱细目2（补）				C
//        $message['54'] = array();
//        $message['54'][] = "";//RECORD ID		记录类型标识	9(2)	54	M
//        $message['54'][] = "";//CARGO DESCRIPTION（1-5）		货物品名	X(70)	53记录无法记录完的品名。hmm的53/4取54品名的前150位	M

        //99    记录99		TRAILER RECORD尾记录				M
        $message['99'] = array();
        $message['99'][] = '99';
        $message['99'][] = ++$file_sum;//16 记录文件总数
        $file_content .= message_format($message, true, array(array("?", "??"), array(":", "?:"), array("'", "?'"), array("+", "?+")));

        //接下来将报文进行转化
        //保存文件txt
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        //ESL_EDI/日期
        $new_dir = $root_path . '/upload/message/EASIPASS_EDI/SI/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $consol_id . '.txt';
        $file_path = $new_dir . $file_name;
        file_put_contents($file_path, $file_content);

        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => 'SI',
            'receiver' => 'EASIPASS',
        );
        update_message($consol_id, $message_data, 'SI');
        return jsonEcho(array('code' => 0, 'msg' => lang('文件生成成功')));
    }
}