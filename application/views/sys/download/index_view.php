<script type="text/javascript">
    function doSearch(){
        var json = {};
        var cx_data = $('#fm').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if (json.hasOwnProperty(item.name) === true) {
                if (typeof json[item.name] == 'string') {
                    json[item.name] = json[item.name].split(',');
                    json[item.name].push(item.value);
                } else {
                    json[item.name].push(item.value);
                }
            } else {
                json[item.name] = item.value;
            }
        });
        var opt = $('#tt').datagrid('options');
        opt.url = '/download/get_data';
        $('#tt').datagrid('load',json);
        $('#chaxun').window('close');
    }
    $(function () {
        $('#tt').edatagrid({
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            }
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height()
        });
        doSearch();

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });

</script>

<table id="tt" style="width:1100px;height:450px" rownumbers="false" pagination="true" idField="id" pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th data-options="field:'id',width:80"><?= lang('ID');?></th>
        <th data-options="field:'file_name',width:500"><?= lang('文件名');?></th>
        <th data-options="field:'user_name',width:60"><?= lang('下载人');?></th>
        <th data-options="field:'insert_time',width:150"><?= lang('下载时间');?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <form id="fm" method="post">
        <input type="hidden" name="id_type" value="<?= $id_type;?>">
        <input type="hidden" name="id_no" value="<?= $id_no;?>">
        <table>
            <tr>
                <td>
                    <input class="easyui-textbox" name="file_name">
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="doSearch();"><?= lang('查询');?></a>
                </td>
                <td width='80'></td>
            </tr>
        </table>
    </form>
</div>
