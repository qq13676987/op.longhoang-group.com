<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
    <meta name="keywords" content="">
    <meta name="viewport" content="width=1180">
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css"/>
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
</head>
<body>
<table class="layui-table" style="margin: 0px 0px 20px;">
    <colgroup>
        <col width="250">
        <col width="100">
        <col width="">
    </colgroup>
    <thead>
    <tr>
        <th><?= lang('查看人员');?></th>
        <th><?= lang('查看次数');?></th>
        <th><?= lang('最近访问时间');?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    if ($data):
        foreach($data as $item):
            ?>
            <tr>
                <td>
                    <span style="color: #ff6000; font-weight: bolder"><?=$item['user_name'];?></span> /
                    <span style="color: #316da9; font-weight: "><?=$item['user_group'];?></span>
                </td>
                <td><?=$item['num'];?></td>
                <td><?=$item['create_time'];?></td>
            </tr>
        <?php
        endforeach;
    else:
        ?>
        <tr><td colspan="3" style="color: #7a7a7a; text-align: center"><?= lang('暂无浏览日志！');?></td></tr>
    <?php endif;?>
    </tbody>
</table>
</body>
<script type="text/javascript" src="/inc/third/layui/layui.js"></script>
<script type="text/javascript">
    $(function () {
        layui.use(['element', 'form'], function () {
            form = layui.form;
        });
    })
</script>