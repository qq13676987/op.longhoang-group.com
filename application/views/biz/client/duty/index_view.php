<title><?= lang('往来单位');?>-<?= lang('角色权限维护');?></title>
<style>
    .role_duty_top{
        padding:20px 0 0 0;
    }
    .role_duty_box{
        width: 500px;
        margin-left: 10px;
    }
    .role_duty_top{
        display: flex;
        line-height: 24px;
    }
    .role_duty_bottom{
        display: flex;
        padding-top: 10px;
    }
    .role_duty_left{
        width: 60px;
        height: 25px;
        padding: 0 20px 0 10px;
    }
    .role_duty_bottom{
        min-height: 40px;
    }
    .role_duty:not(:last-child) .role_duty_bottom{
        border-bottom: 1px solid black;
    }
</style>
<!--标签相关css-->
<style type="text/css">
    .topic-tag{
        display:inline-block
    }
    .topic-tag .text {
        display: inline-block;
        height: 16px;
        line-height: 16px;
        padding: 2px 5px;
        background-color: #99cfff;
        font-size: 12px;
        color: #fff;
        border-radius: 4px;
        cursor: pointer;
    }
    .topic-tag {
        margin: 0 5px 2px 0;
    }
    .topic-tag .text:hover, .topic-tag .close:hover{
        background-color: #339dff;
    }
    .tag_box .topic-tag .text{
        border-radius: 4px 0 0 4px;
        float: left;
    }
    .tag_box .topic-tag .close{
        float: left;
    }
    .topic-tag-apply .text{
        background-color: #cccccc;
    }
    .topic-tag .close {
        width: 20px;
        height: 20px;
        background-color: #66b7ff;
        text-align: center;
        line-height: 20px;
        color: #fff;
        font-size: 10px;
        opacity: 1;
        filter: alpha(opacity=100);
        border-radius: 0 4px 4px 0;
        display: inline-block;
        text-shadow: 0 1px 0 #fff;
        cursor: pointer;
    }
    .add_tags_body_input{
        display: inline-block;
        vertical-align: middle;
        width: 200px !important;
        margin: 0 5px 10px 0;
        padding: 6px;
        resize: none;
        box-shadow: none;
        height: 22px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        float: left;
    }
    .add_tags_body_type{
        display: inline-block;
        vertical-align: middle;
        width: 100px !important;
        margin: 0 5px 10px 0;
        padding: 6px;
        resize: none;
        box-shadow: none;
        height: 36px;
        font-size: 14px;
        line-height: 1.42857143;
        color: #555;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 4px;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        float: left;
    }
    .add_tags_body_input_btn:hover{
        background-color: rgb(22,155,213);
    }
    .add_tags_body_input_btn{
        margin: 0 10px 10px 0;
        border: none !important;
        background-color: rgb(94,180,214);
        color: #fff;
        min-width: 76px;
        height: 34px;
        padding: 0 10px;
        line-height: 34px;
        font-size: 14px;
        display: inline-block;
        font-weight: 400;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border-radius: 4px;
        box-sizing: border-box;
    }
    .add_tags_body h3{
        margin-top: 20px;
        color: #333;
        font-size: 100%;
        line-height: 1.7;
        margin-bottom: 10px;
    }
    .add_tags_body{
        padding:10px;
    }
</style>
<body>
    
    <div class="role_duty_box">
        <?php if(is_admin()){ ?>
        <div class="role_duty">
            <div class="role_duty_top">
                <div class="role_duty_left">
                    <?= lang('全开放');?>:
                </div>
                <div class="role_duty_right" >
                    <input id="all_checked" data-options="width:60,height:26,checked:false,onChange:all_change">
                </div>
            </div>
            <div class="role_duty_bottom">
            <!--<span style="color:red;"> <?= lang('下面维护岗位的使用方法： 选择人员后，点击“新增”按钮。');?></span>-->
                <div class="role_duty_left">
                    </div>
                <div class="role_duty_right tag_box ">
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <input type="hidden" id="client_code" value="<?= $client_code;?>">
    <input type="hidden" id="client_role" value="<?= $client_role;?>">
    <input type="hidden" id="crm_id" value="<?= $crm_id;?>">
    <!--标签相关JS代码-->
    <script type="text/javascript">
        /**
         * 添加一个tag标签
         * @param tag_name 标签名
         * @param tag_role 标签类型, 目前有this_tag 右上角的,system_tag 系统推荐标签
         * @param user_id 用户ID
         * @param is_edit 是否可修改
         */
        function add_tag_html(tag_name, tag_role, user_id, is_edit = true) {
            var span_class = "topic-tag";

            //如果不存在,生成一个
            var role_tag_name = tag_role + '_role_tag';
            if($("." + role_tag_name).length == 0){
                var topic_bar_html = "<div class=\"topic-bar clearfix " + role_tag_name + "\" style=\"margin: 0;\"></div>";
                $('.' + role_tag_name + '_box').append(topic_bar_html);
            }

            var tag_html = "<span class=\"" + span_class + "\">\n" +
                "                    <a class=\"text\">" + tag_name + "</a>\n" +
                "<a class=\"close\" onclick=\"click_del_role_user(this, '" + tag_role + "', '" + user_id + "', " + is_edit +")\">X</a>" +
                "                </span>";
            $("." + role_tag_name).append(tag_html);
        }

        /**
         * 点击添加标签
         * @param e 当前按钮
         * @param user_role 角色
         * @param is_edit 是否可编辑
         */
        function click_add_role_user(e, user_role, is_edit) {
            if(!is_edit){
                $.messager.alert('<?= lang('提示');?>', '<?= lang('无权新增');?>');
                return false;
            }
            var client_code = $('#client_code').val();
            var client_role = $('#client_role').val();
            var grid_input = $('#' + user_role);
            var user_id = grid_input.combogrid('getValue');
            var post_data = {};
            post_data.client_code = client_code;
            post_data.client_role = client_role;
            post_data.user_role = user_role;
            post_data.user_id = user_id;
            add_data(post_data, function (res) {
                if(res.code == 0) {
                    var user_name = grid_input.combogrid('grid').datagrid('getSelected').name;
                    add_tag_html(user_name, user_role, user_id);
                    grid_input.combogrid('clear');
                }else{

                }
            })
        }

        //新增函数
        function add_data(post_data, fn = function () {}) {
            ajaxLoading();
            $.ajax({
                type: 'POST',
                url: '/biz_client_duty/add_data',
                data: post_data,
                success: function (res_json) {
                    ajaxLoadEnd();
                    try {
                        var res = $.parseJSON(res_json);
                        if(res.code == 0){
                            fn(res);
                        }else{
                            $.messager.alert('<?= lang('提示');?>', res.msg);
                        }
                    }catch (e) {
                        $.messager.alert('<?= lang('提示');?>', res_json);
                    }
                },
                error:function (e) {
                    ajaxLoadEnd();
                    $.messager.alert('<?= lang('提示');?>', e.responseText);
                }
            });
        }

        /**
         * 点击删除当前tag
         * @param e 当前按钮
         * @param user_role 角色
         * @param user_id 用户id
         * @param is_edit 是否可编辑
         */
        function click_del_role_user(e, user_role, user_id, is_edit) {
            if(!is_edit){
                $.messager.alert('<?= lang('提示');?>', '<?= lang('无权删除');?>');
                return false;
            }
            var client_code = $('#client_code').val();
            var client_role = $('#client_role').val();
            var post_data = {};
            post_data.client_code = client_code;
            post_data.client_role = client_role;
            post_data.user_role = user_role;
            post_data.user_id = user_id;
            $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认删除当前人员相关权限');?>', function (r) {
                if(r){
                    delete_data(post_data, function (res) {
                        if(res.code == 0){
                            $(e).parents('.topic-tag').remove();
                        }else{
                        }
                    });
                }
            })
        }

        //删除的函数,参数2位需要执行的函数
        function delete_data(post_data, fn = function () {}) {
            ajaxLoading();
            $.ajax({
                type: 'POST',
                url: '/biz_client_duty/delete_data',
                data: post_data,
                success: function (res_json) {
                    ajaxLoadEnd();
                    try {
                        var res = $.parseJSON(res_json);
                        if(res.code == 0){
                            fn(res);
                        }else{
                            $.messager.alert('<?= lang('提示');?>', res_json);
                        }
                    }catch (e) {
                        $.messager.alert('<?= lang('提示');?>', res_json);
                    }
                },
                error:function (e) {
                    ajaxLoadEnd();
                    $.messager.alert('<?= lang('提示');?>', e.responseText);
                }
            });
        }

        function setting_sales_op() {
            var client_code = $('#client_code').val();
            window.open("/biz_client/sales_relation?client_code=" + client_code);
        }

        //加载当前tag
        function load_data() {
            var client_code = $('#client_code').val();
            var client_role = $('#client_role').val();
            var crm_id = $('#crm_id').val();
            //分为2部分,1是当前的tag,2是公共模板的tag
            $.ajax({
                type: 'GET',
                url: '/biz_client_duty/get_client_duty?client_code=' + client_code + '&client_role=' + client_role + '&crm_id=' + crm_id,
                success: function (res_json) {
                    ajaxLoadEnd();
                    try {
                        var res = $.parseJSON(res_json);
                        if(res.code == 0){
                            //系统推荐的
                            $.each(res.data, function (i, it) {
                                var is_edit = it.is_edit ? 'false' : 'true';
                                var role_duty_html = "<div class=\"role_duty\">\n" +
                                    "            <div class=\"role_duty_top\">\n" +
                                    "                <div class=\"role_duty_left\">\n" +
                                    "                    " + it.user_role_lang + "\n" +
                                    "                </div>\n" +
                                    "                <div class=\"role_duty_right\" >\n" +
                                    "                    <select id=\"" + it.user_role + "\" class=\"easyui-combogrid\" style=\"width: 255px\" data-options=\"\n" +
                                    "            readonly: " + is_edit + ",\n" +
                                    "            panelWidth: '550px',\n" +
                                    "            panelHeight: '400px',\n" +
                                    "            width: 255,\n" +
                                    "            idField: 'id',              //ID字段\n" +
                                    "            textField: 'name',    //显示的字段\n" +
                                    "            striped: true,\n" +
                                    "            editable: false,\n" +
                                    "            pagination: true,           //是否分页\n" +
                                    "            pageList: [30, 100, 500],\n" +
                                    "            pageSize: 500,\n" +
                                    "            toolbar: '#' + create_company_tool('" + it.user_role + "'),\n" +//
                                    "            rownumbers: true,           //序号\n" +
                                    "            collapsible: true,         //是否可折叠的\n" +
                                    "            method: 'post',\n" +
                                    "            columns: [[\n" +
                                    "                {field: 'other_name', title: '<?= lang('姓名');?>'},\n" +//, width: 150
                                    "                {field: 'other_group_name', title: '<?= lang('部门');?>'},\n" +//, width: 250
                                    "            ]],\n" +
                                    "            emptyMsg: '未找到相应数据!',\n" +
                                    "            url: '/bsc_user/get_user_tree2/" + it.user_role + "',\"></select>\n" +
                                    "                    <button onclick=\"click_add_role_user(this, '" + it.user_role +"', " + it.is_edit + ")\"><?= lang('新增')?></button>\n" +
                                    <?php if($is_config_sales_op === 'true'){ ?>"                    <button onclick=\"setting_sales_op()\"><?= lang('设置关联');?></button>\n" + <?php } ?>
                                    "                </div>\n" +
                                    "            </div>\n" +
                                    "            <div class=\"role_duty_bottom\">\n" +
                                    "                <div class=\"role_duty_left\">\n" +
                                    "                </div>\n" +
                                    "                <div class=\"role_duty_right tag_box " + it.user_role + "_role_tag_box\">\n" +
                                    "                </div>\n" +
                                    "            </div>\n" +
                                    "        </div>";
                                $('.role_duty_box').append(role_duty_html);
                                $.each(it.data, function (i2, it2) {
                                    add_tag_html(it2.user_name, it.user_role, it2.user_id, it.is_edit)
                                })
                            });

                            $.parser.parse('.role_duty_box');

                            $('#all_checked').switchbutton({checked: res.all_checked});
                            $.each($('.keydown_search'), function (i, it) {
                                $(it).textbox('textbox').bind('keydown', function (e) {
                                    keydown_search(e, $(it).attr('t_id'));
                                });
                            });
                        }else{
                            $.messager.alert('<?= lang('提示');?>', res.msg);
                        }
                    }catch (e) {
                        $.messager.alert('<?= lang('提示');?>', res_json);
                    }
                },
                error:function (e) {
                    ajaxLoadEnd();
                    $.messager.alert('<?= lang('提示');?>', e.responseText);
                }
            });
        }

        function create_company_tool(id) {
            var div_id = id + '_div';
            var click_id = id + '_click';
            var form_id = id + '_form';
            if($('#' + div_id).length > 0) return div_id;
            var html = '<div id="' + div_id + '">' +
                '<form id="' + form_id + '" method="post">' +
                '<div style="padding-left: 5px;display: inline-block;"><label><?= lang('姓名');?> : </label><input name="name" class="easyui-textbox keydown_search" t_id="' + click_id + '" style="width:96px"data-options="prompt:\'text\'"/></div>' +
                '<div style="padding-left: 5px;display: inline-block;"><label><?= lang('部门');?> : </label><input name="group" class="easyui-textbox keydown_search" t_id="' + click_id + '" style="width:96px"data-options="prompt:\'text\'"/></div>' +
                '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="' + click_id + '" onclick="query_report(\'' + form_id + '\', \'' + id + '\')"><?= lang('search');?></a>' +
                '</form>' +
                '</div>';
            $('#' + id).parent().append(html);
            $.parser.parse('#' + div_id);
            return div_id;
        }

        function query_report(form_id, load_id) {
            var where = {};
            var form_data = $('#' + form_id).serializeArray();
            $.each(form_data, function (index, item) {
                if (item.value == "") return true;
                if (where.hasOwnProperty(item.name) === true) {
                    if (typeof where[item.name] == 'string') {
                        where[item.name] = where[item.name].split(',');
                        where[item.name].push(item.value);
                    } else {
                        where[item.name].push(item.value);
                    }
                } else {
                    where[item.name] = item.value;
                }
            });
            if (where.length == 0) {
                $.messager.alert('<?= lang('提示');?>', '<?= lang('请填写需要搜索的值');?>');
                return;
            }
            $('#' + load_id).combogrid('grid').datagrid('load', where);
        }

        //text添加输入值改变
        function keydown_search(e, click_id) {
            if (e.keyCode == 13) {
                $('#' + click_id).trigger('click');
            }
        }

        load_data();

        $(function () {

        });
    </script>
    <!--easyui 相关绑定函数-->
    <script type="text/javascript">
        var is_auto_all_change = false;
        function all_change(checked) {
            if(is_auto_all_change){
                is_auto_all_change = false;
                return;
            }
            var client_code = $('#client_code').val();
            var client_role = $('#client_role').val();

            var post_data = {};
            post_data.client_code = client_code;
            post_data.client_role = client_role;
            post_data.user_role = '';
            post_data.user_id = -1;
            if(checked){
                $.messager.confirm("<?= lang('提示');?>", "<?= lang('确认要全部开放吗? 请小心处理哦');?>", function (r) {
                   if(r){
                       //如果选中,新增
                       add_data(post_data);
                   }else{
                       is_auto_all_change = true;
                       $('#all_checked').switchbutton('uncheck');
                   }
                });
            }else{
                $.messager.confirm("<?= lang('提示');?>", "<?= lang('确认要取消全部开放吗? 请小心处理哦');?>", function (r) {
                    if(r){
                        //反之,删除
                        delete_data(post_data);
                    }else{
                        is_auto_all_change = true;
                        $('#all_checked').switchbutton('check');
                    }
                });
            }
        }
    </script>
</body>

