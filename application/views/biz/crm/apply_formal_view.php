<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>Client-Edit</title>
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<script type="text/javascript">
    var is_update = false;

    window.onbeforeunload = function (e) {
        // 兼容IE8和Firefox 4之前的版本
        //检测是否有字段变动
        e = e || window.event;
        var dialogText = '您有未保存的内容,是否确认退出';

        if(is_update){
            if (e) {
                e.returnValue = dialogText;
            }
            // Chrome, Safari, Firefox 4+, Opera 12+ , IE 9+
            return dialogText;
        }
    };

    $(function(){
        $('#company_name').textbox('setValue', '<?= es_encode($company_name);?>');
        $('#client_name').textbox('setValue', '<?= es_encode($client_name);?>');
        role_select();
        $('#f').form({
            onChange: function (target) {
                if($(target).attr('id') !== 'finance_describe'){
                    is_update = true;
                }
            }
        });
        $('#tb').tabs({
            border: false,
            onSelect: function (title) {
                if (title == "Contact list") {
                    if (document.getElementById("contact_list").src == "") {
                        document.getElementById("contact_list").src = "/biz_client_contact/index_crm?crm_id=<?= $crm_id;?>";
                    }
                }
                if (title == "Account list") {
                    if (document.getElementById("account_list").src == "") {
                        document.getElementById("account_list").src = "/biz_client_account/index/<?php echo $client_code;?>";
                    }
                }
                if (title == "每日跟进记录") {
                    if (document.getElementById("follow_up_list").src == "") {
                        document.getElementById("follow_up_list").src = "/biz_client_follow_up_log/index/?crm_id=<?= $crm_id;?>";
                    }
                }
                if(title == 'Log'){
                    if (document.getElementById("log").src==""){
                        document.getElementById("log").src="/sys_log/index/biz_client/<?= $id?>";
                    }
                }
            }

        });
        $('.finance_od_basis').click(function(){
            // var val = $(this).val();
            // if(val == 'monthly'){
            //     $('#finance_payment_month').textbox('enable');
            //     $('#finance_payment_day_th').textbox('enable');
            //     $('#finance_payment_days').textbox('disable');
            // }else if(val == 'daily'){
            //     $('#finance_payment_month').textbox('disable');
            //     $('#finance_payment_day_th').textbox('disable');
            //     $('#finance_payment_days').textbox('enable');
            // }
            get_finance_describe();
        });

        $('.finance_payment').click(function () {
            get_finance_describe();
        });

        //初始值
        var finance_payment = $('#finance_payment').val();
        if(finance_payment !=null && finance_payment != ''){
            $(".finance_payment[value='"+ finance_payment +"']").attr("checked",true);
        }

        var finance_od_basis = $('#finance_od_basis').val();
        if(finance_od_basis !=null && finance_od_basis != ''){
            $(".finance_od_basis[value='"+ finance_od_basis +"']").attr("checked",true);
        }
        $('.finance_od_basis:checked').trigger('click');
        $('.tags').on('click', '.tag-remove', function () {
            var tag_val = $('#tag').val();
            var tag_val_arr = tag_val.split(',');
            var this_val = $(this).siblings('.tag-span').text();
            tag_val_arr.splice($.inArray(this_val,tag_val_arr),1);
            $('#tag').val(tag_val_arr.join(','));
            $(this).parent('.tag').remove();
        });
    })

    function add_tags(value) {
        var val = value;
        if(val != ''){
            var tag_val = $('#tag').val();
            if(tag_val === ''){
                $('#tag').val(val);
            }else{
                $('#tag').val(tag_val + ',' + val);
            }

            var tag = '<div class="tag">' +
                '<span class="tag-span">' + val + '</span>' +
                '<a href="javascript:;" class="tag-remove"></a>' +
                '</div>';
            $('.tags').append(tag);
        }
    }

    function role_select() {
        if($('#role1').length > 0){
            $('#role').val($('#role1').combobox('getValues'));
        }
    }

    function checkvalue() {
        $('#f').form('submit', {
                url: '/biz_crm/create_apply_data/',
                onSubmit: function(){
                    var vv = $('#country_code').textbox('getValue');
                    if (vv == "") {
                        alert("<?= lang('国家代码必填') ;?>");
                        return false;
                    }
                    var vv = $('#client_name').textbox('getValue');
                    if (vv == "") {
                        alert("<?= lang('客户简称 必填') ;?>");
                        return false;
                    }
                    var vv = $('#company_name').textbox('getValue');
                    if (vv == "") {
                        alert("<?= lang('客户全称 必填') ;?>");
                        return false;
                    }
                    var vv = $('#company_address').val();
                    if (vv == "") {
                        alert("<?= lang('客户地址 必填');?>");
                        return false;
                    }

                    $('#role').val($('#role1').combobox('getValues'));
                    var vv = $('#role1').combobox('getValues');
                    if(vv == ''){
                        alert("<?= lang('角色 必填');?>");
                        return false;
                    }
                    <?php if($country_code=="CN"){ ?>
                    var vv = $('#bank_name_cny').textbox('getValue');
                    if (vv == "") {
                        alert("<?= lang('银行名称(CNY) 必填');?>");
                        return false;
                    }
                    var vv = $('#bank_account_cny').textbox('getValue');
                    if (vv == "") {
                        alert("<?= lang('银行账号(CNY) 必填');?>");
                        return false;
                    }
                    var vv = $('#taxpayer_name').textbox('getValue');
                    if (vv == "") {
                        alert("<?= lang('纳税人名称 必填');?>");
                        return false;
                    }
                    var vv = $('#taxpayer_id').textbox('getValue');
                    if (vv == "") {
                        alert("<?= lang('纳税人识别号 必填');?>");
                        return false;
                    }
                    var vv = $('#taxpayer_address').textbox('getValue');
                    if (vv == "") {
                        alert("<?= lang('纳税人地址 必填');?>");
                        return false;
                    }
                    var vv = $('#taxpayer_telephone').textbox('getValue');
                    if (vv == "") {
                        alert("<?= lang('纳税人电话 必填');?>");
                        return false;
                    }
                    <?php }else{?>
                    var vv = $('#bank_name_usd').textbox('getValue');
                    if (vv == "") {
                        alert("<?= lang('银行名称(USD) 必填');?>");
                        return false;
                    }
                    var vv = $('#bank_account_usd').textbox('getValue');
                    if (vv == "") {
                        alert("<?= lang('银行账号(USD) 必填');?>");
                        return false;
                    }

                        <?php } ?>
            // var validate = $('#form').form('validate');
            // if(!validate){
            //     alert("有字段未填写,请检查是否有红色输入框");
            //     return false;
            // }
            $('#dn_accounts').val($('#dn_accounts1').combobox('getValues').join(','));
        let bank_name_cny = $('#bank_name_cny').textbox('getValue');
        if(bank_name_cny.length < 5){
            $('#bank_name_cny').textbox('setValue','')
        }
        let bank_account_cny = $('#bank_account_cny').textbox('getValue');
        if(bank_account_cny.length < 5){
            $('#bank_account_cny').textbox('setValue','')
        }
        let bank_name_usd = $('#bank_name_usd').textbox('getValue');
        if(bank_name_usd.length < 5){
            $('#bank_name_usd').textbox('setValue','')
        }
        let bank_account_usd = $('#bank_account_usd').textbox('getValue');
        if(bank_account_usd.length < 5){
            $('#bank_account_usd').textbox('setValue','')
        }
        let taxpayer_name = $('#taxpayer_name').textbox('getValue');
        if(taxpayer_name.length < 5){
            $('#taxpayer_name').textbox('setValue','')
        }
        let taxpayer_address = $('#taxpayer_address').textbox('getValue');
        if(taxpayer_address.length < 5){
            $('#taxpayer_address').textbox('setValue','')
        }
        let taxpayer_telephone = $('#taxpayer_telephone').textbox('getValue');
        if(taxpayer_telephone.length < 5){
            $('#taxpayer_telephone').textbox('setValue','')
        }
        let taxpayer_id = $('#taxpayer_id').textbox('getValue');
        if(taxpayer_id.length < 5){
            $('#taxpayer_id').textbox('setValue','')
        }
        is_update = false;
        return true;
    },
        success:function(res_json){
            var res = $.parseJSON(res_json);
            console.log(res);
            if(res.code == 1){
                layer.alert('申请已提交，请等待审批。', {icon:1}, function () {
                    location.href='/biz_crm/edit/<?=$id;?>';
                });
            }else{
                layer.alert('申请失败：'+res.msg, {icon:5});
            }
        }
    });
    }

    //获取综合描述
    function get_finance_describe(){
        var finance_payment = $('.finance_payment:checked').val();
        var finance_od_basis = $('.finance_od_basis:checked').val();

        var lang = '';
        if(finance_od_basis == 'monthly'){
            $('#finance_payment_days').textbox('disable');

            var finance_payment_month = $('#finance_payment_month').numberbox('getValue');
            var finance_payment_day_th = $('#finance_payment_day_th').numberbox('getValue');

            if(finance_payment == 'after work'){
                lang = '<?= lang('Payment on the (day)th day of (month) months after work');?>';
            }else if(finance_payment == 'after invoice') {
                lang = '<?= lang('Payment on the (day)th day of (month) months after invoice');?>';
            }

            lang = lang.replace('(month)', finance_payment_month).replace('(day)', finance_payment_day_th);

        }else if(finance_od_basis == 'daily'){
            $('#finance_payment_month').textbox('disable');
            $('#finance_payment_day_th').textbox('disable');


            var finance_payment_days = $('#finance_payment_days').numberbox('getValue');

            if(finance_payment == 'after work'){
                lang = '<?= lang('Payment (day) days after work');?>';
            }else if(finance_payment == 'after invoice') {
                lang = '<?= lang('Payment (day) days after invoice');?>';
            }
            lang = lang.replace('(day)', finance_payment_days);
        }
        $('#finance_describe').textbox('setValue',lang);
    }
    function reload_select(type='combobox'){
        if(type == 'combobox'){
            $('#bank_name_cny').combobox('reload');
            $('#bank_name_usd').combobox('reload');
        }
    }
    function select_all(id, name) {
        var ed = $('#' + id);
        var data = ed.combobox('getData');
        var this_val = ed.combobox('getValues');
        var val = [];
        $.each(data, function (index,item) {
            val.push(item[name]);
        });
        val = val.join(',');
        this_val = this_val.join(',');
        if(val !== this_val){
            ed.combobox('setValues', val);
        }else{
            ed.combobox('setValues', '');
        }
    }


    function overdue_info(client_code){
        window.open('/biz_bill/overdue_info/' + client_code);
    }

    //查找相似度
    function search_similar() {
        var company_name = $('#company_name').textbox('getValue');
        if(company_name === ''){
            $.messager.alert('Tips', '先填写全称');
            return;
        }
        ajaxLoading();
        is_search = true;
        $.ajax({
            type:'GET',
            url:'/biz_client/search?company_name=' + company_name + '&client_code=<?= $client_code;?>',
            success:function (res_json) {
                ajaxLoadEnd();
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误,请联系管理员');
                }else{
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg + '<br />最接近的为' + res.data.company_name + ',相似度为' + res.data.percent + '%');
                    }else{
                        $.messager.alert('Tips', res.msg);
                    }

                }
            },
        });
    }

    function re_select(id){
        var inp = $('#' + id);
        var val = inp.combobox('getValue');
        inp.combobox('reload').combobox('clear').combobox('select', val);
    }

    function open_tab(title){
        $('#tb').tabs('select', title);
    }
    function approved(){
        var crm_id = $('#crm_id').val();

        $.messager.confirm('Tips', '是否确认通过申请', function (r) {
            if(r){
                $.post("/biz_crm/approved_client", {id:crm_id}, function(data){
                    if(data.code == 0){
                        //通过的话,关闭页面
                        try{window.opener.$('#tt').edatagrid('reload');}catch (e) {}
                        alert(data.msg);
                        window.close();
                    }else{
                        $.messager.alert('Tips', data.msg);
                    }
                },"Json");
            }
        });
    }
    /**
     * 下拉选择表格展开时,自动让表单内的查询获取焦点
     */
    function combogrid_search_focus(form_id, focus_num = 0) {
        // var form = $(form_id);
        setTimeout("easyui_focus($($('" + form_id + "').find('.keydown_search')[" + focus_num + "]));", 200);
    }

    /**
     * easyui 获取焦点
     */
    function easyui_focus(jq) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if (data.combogrid !== undefined) {
            return $(jq).combogrid('textbox').focus();
        } else if (data.combobox !== undefined) {
            return $(jq).combobox('textbox').focus();
        } else if (data.datetimebox !== undefined) {
            return $(jq).datetimebox('textbox').focus();
        } else if (data.datebox !== undefined) {
            return $(jq).datebox('textbox').focus();
        } else if (data.textbox !== undefined) {
            return $(jq).textbox('textbox').focus();
        }
    }

    function query_report(form_id, combo_id){
        var where = {};
        var form_data = $('#' + form_id).serializeArray();
        $.each(form_data, function (index, item) {
            if(item.value == "") return true;
            if(where.hasOwnProperty(item.name) === true){
                if(typeof where[item.name] == 'string'){
                    where[item.name] =  where[item.name].split(',');
                    where[item.name].push(item.value);
                }else{
                    where[item.name].push(item.value);
                }
            }else{
                where[item.name] = item.value;
            }
        });
        if(where.length == 0){
            $.messager.alert('Tips', '请填写需要搜索的值');
            return;
        }
        //判断一下当前是 remote 还是local,如果是remode,走下面这个
        var inp = $('#' + combo_id);
        var opt = inp.combogrid('options');
        if(opt.mode == 'remote'){
            inp.combogrid('grid').datagrid('load',where);
        }else{
            //如果是本地的,那么先存一下
            if(opt.old_data == undefined) opt.old_data = inp.combogrid('grid').datagrid('getData'); // 如果没存过,这里存一下
            var rows = [];
            if(JSON.stringify(where) === '{}'){
                inp.combogrid('grid').datagrid('loadData', opt.old_data.rows);
                return;
            }
            $.each(opt.old_data.rows,function(i,obj){
                for(var p in where){
                    var q = where[p].toUpperCase();
                    var v = obj[p].toUpperCase();
                    if (!!v && v.toString().indexOf(q) >= 0){
                        rows.push(obj);
                        break;
                    }
                }
            });
            if(rows.length == 0) {
                inp.combogrid('grid').datagrid('loadData', []);
                return;
            }

            inp.combogrid('grid').datagrid('loadData',rows);
        }
    }
</script>
<style>
    input:disabled, select:disabled, textarea:disabled{
        background-color: #d6d6d6;
        cursor: default;
    }
    input:read-only, select:read-only, textarea:read-only{
        background-color: #F0F0F0;
        cursor: default;
    }
    .tag{
        position: relative;
        display: inline-block;
        background-color: #00BFFF;
        height: 18px;
        line-height: 18px;
        margin-right: 2px;
    }
    .tag-span{
        font-size: 12px;
        text-align: center;
        color: #fff;
        border-radius: 2px;
        padding-left: 6px;
        padding-right: 16px;
    }
    .tag-remove {
        background: url('/inc/image/tagbox_icons.png') no-repeat -16px center;
        position: absolute;
        display: block;
        width: 16px;
        height: 16px;
        right: 2px;
        top: 50%;
        margin-top: -8px;
        opacity: 0.6;
        filter: alpha(opacity=60);
        vertical-align: ;
    }
    .hide{
        display: none;
    }
</style>
<body style="margin:10px;">
<table>
    <tr>
        <td>
            <h2> Edit </h2></td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td><?php echo $client_code; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $company_name; ?></td>
        <td></td>
        <td>
            <span style="font-size: 15px;font-weight: bold;color: red;">状态: <?= getUserName($sales_ids);?>的领导 "<?= getUserName($leader_id)?>" 审核</span>
            <!--button  class="<?= $crm_apply_status == 3 && (in_array($leader_id,get_session("user_range")) || is_admin()) ? '' : 'hide';?>" onclick="approved()">审核通过</button-->
        </td>
    </tr>
</table>

<div id="tb" class="easyui-tabs" style="width:100%;height:850px">
    <div title="Basic info" style="padding:1px">
        <form id="f" name="f" method="post">
            <input type="hidden" name="crm_id" id="crm_id" value="<?= $crm_id;?>">
            <div class="easyui-layout" style="width:100%;height:780px;">
                <div data-options="region:'west',tools:'#tt'" title="Basic" style="width:450px;">
                    <table width="" border="0" cellspacing="0" style="padding:20px 20px;">
                        <tr>
                            <td><?= lang('country_code'); ?></td>
                            <td>
                                <input class="easyui-textbox" name="country_code" id="country_code"  readonly="true" style="width:80px;" value="<?php echo $country_code; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('company_name'); ?> </td>
                            <td>
                                <input class="easyui-textbox" disabled data-options="value:'<?=$company_name;?>'" name="company_name" id="company_name" style="width:190px;"/>
                                <button type="button" onclick="search_similar()"><?= lang('search similar');?></button>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('client_name'); ?> </td>
                            <td>
                                <input class="easyui-textbox" disabled data-options="value:'<?=$client_name;?>'" name="client_name" id="client_name" style="width:255px;"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('client area');?></td>
                            <td>
                                <select class="easyui-combobox" disabled name="province" id="province" data-options="
                                valueField:'cityname',
                                textField:'cityname',
                                url:'/city/get_province',
                                value: '<?= $province;?>',
                                onSelect: function(rec){
                                    if(rec != undefined){
                                        $('#city').combobox('reload', '/city/get_city/' + rec.id);
                                    }
                                },
                                onLoadSuccess:function(){
                                    var this_data = $(this).combobox('getData');
                                    var this_val = $(this).combobox('getValue');
                                    var rec = this_data.filter(el => el.cityname === this_val);
                                    if(rec.length > 0)rec = rec[0];
                                    $('#city').combobox('reload', '/city/get_city/' + rec.id);
                                }
                                " style="width: 80px;"></select>

                                <select class="easyui-combobox" disabled name="city" id="city" style="width: 80px;" data-options="
                                    valueField:'cityname',
                                    textField:'cityname',
                                    value: '<?= $city;?>',
                                    onSelect: function(rec){
                                        if(rec != undefined){
                                            $('#area').combobox('reload', '/city/get_area/' + rec.id);
                                        }
                                    },
                                    onLoadSuccess:function(){
                                        var this_data = $(this).combobox('getData');
                                        var this_val = $(this).combobox('getValue');
                                        var rec = this_data.filter(el => el['cityname'] === this_val);
                                        if(rec.length > 0)rec = rec[0];
                                        $('#area').combobox('reload', '/city/get_area/' + rec.id);
                                    }
                                "></select>

                                <select class="easyui-combobox" disabled name="area" id="area" style="width: 80px;" data-options="
                                    valueField:'cityname',
                                    textField:'cityname',
                                    value: '<?= $area;?>',
                                "></select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('company_address'); ?></td>
                            <td>
                                <textarea style="width: 255px;height: 40px" class="textarea b" disabled name="company_address" id="company_address"><?= $company_address ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('role'); ?> <a href="javascript:void;" class="easyui-tooltip" data-options="
                                content: '<div>下拉框内容是根据contact来的,<br/>' +
                                '1、下拉框里没有您需要的角色?在contact里添加对应角色的联系人后刷新页面.' +
                                '</div>',
                            "><?= lang('提示!');?></a></td>
                            <td>
                                <select id="role1" disabled class="easyui-combobox" name="role1" style="width:255px;" data-options="required:true,
                                    multiple:true,
                                    editable:false,
    								valueField:'value',
    								textField:'value',
    								url:'/bsc_dict/get_option/role',
    								value:'<?php echo $role; ?>',
    								onChange: function(newVal, oldVal){
    								    role_select();
    								},
								">
                                </select>
                                <!-- 这是之前的根据联系获取角色 /biz_client_contact/get_role/<?= $client_code;?> -->
                                <!--此处版本又回到了以前的,不过这里角色验证改到后台了,如果后台角色与联系人验证不通过,且当前为正式客户,那么不给保存-->
                                <input type="hidden" id="role" name="role" value="<?php echo $role; ?>">
                                <a href="javascript:void;" class="easyui-tooltip" data-options="
                                    content: $('<div></div>'),
                                    onShow: function(){
                                        $(this).tooltip('arrow').css('left', 20);
                                        $(this).tooltip('tip').css('left', $(this).offset().left);
                                    },
                                    onUpdate: function(cc){
                                        cc.panel({
                                            width: 500,
                                            height: 'auto',
                                            border: false,
                                            href: '/bsc_help_content/help/role'
                                        });
                                    }
                                "><?= lang('help');?></a>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('sales'); ?></td>
                            <td>
                                <input class="easyui-textbox" disabled style="width: 255px"
                                       value="<?php foreach (explode(',', $sales_ids) as $r) {
                                           echo getUserName($r) . ',';
                                       } ?>">
                                <input type="hidden" name="sales_ids" value="<?= $sales_ids;?>">
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('操作'); ?></td>
                            <td>
                                <select class="easyui-combogrid" id="operator_id" style="width: 255px" name="operator_id"
                                        data-options="
                                        panelWidth: '475px',
                                        panelHeight: '230px',
                                        required: true,
                                        idField: 'id',              //ID字段
                                        textField: 'name',    //显示的字段
                                        value:'<?=$operator_id;?>',
                                        striped: true,
                                        editable: false,
                                        url:'/bsc_user/get_data_role/operator/?status=0',
                                        pagination: false,           //是否分页
                                        toolbar : '#operator_id_div',
                                        collapsible: true,         //是否可折叠的
                                        method: 'post',
                                        columns:[[
                                            {field:'name',title:'联系人',width:100},
                                            {field:'telephone',title:'电话/手机号',width:120},
                                            {field:'email',title:'邮箱',width:250},
                                        ]],
                                        emptyMsg : '未找到相应数据!',
                                        mode: 'remote',
                                        onShowPanel:function(){
                                            var opt = $(this).combogrid('options');
                                            var toolbar = opt.toolbar;
                                            combogrid_search_focus(toolbar);
                                        },
                                ">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('客服'); ?></td>
                            <td>
                                <select class="easyui-combogrid" id="service_id" style="width: 255px" name="service_id"
                                        data-options="
                                        panelWidth: '475px',
                                        panelHeight: '230px',
                                        required: true,
                                        idField: 'id',              //ID字段
                                        textField: 'name',    //显示的字段
                                        value:'<?=$service_id;?>',
                                        striped: true,
                                        editable: false,
                                        url:'/bsc_user/get_data_role/customer_service/?status=0',
                                        pagination: false,           //是否分页
                                        toolbar : '#service_id_div',
                                        collapsible: true,         //是否可折叠的
                                        method: 'post',
                                        columns:[[
                                            {field:'name',title:'联系人',width:100},
                                            {field:'telephone',title:'电话/手机号',width:120},
                                            {field:'email',title:'邮箱',width:250},
                                        ]],
                                        emptyMsg : '未找到相应数据!',
                                        mode: 'remote',
                                        onShowPanel:function(){
                                            var opt = $(this).combogrid('options');
                                            var toolbar = opt.toolbar;
                                            combogrid_search_focus(toolbar);
                                        },
                                ">
                                </select>
                            </td>
                        </tr>
                        <input type="hidden" name="read_user_group" id="read_user_group" value="<?= $read_user_group;?>">
                        <?php
                        if ($crm_apply_status == 2):
                            ?>
                            <tr>
                                <td> </td>
                                <td>
                                    <br><br>
                                    <a class="easyui-linkbutton" href="javascript:void(0);" onclick="checkvalue()">确认提交申请</a>
                                </td>
                            </tr>
                        <?php endif;?>
                    </table>
                    <br><br> <br><br> <br><br>
                    <div id="tt">
                        <?php
                        if ($crm_apply_status == 2):
                            ?>
                            <a href="javascript:void(0)" class="icon-save" onclick="checkvalue()" title="save"
                               style="margin-right:15px;"></a>
                        <?php endif;?>
                    </div>
                </div>
                <div data-options="region:'center',title:'Finance'">
                    <table width="1100" border="0" cellspacing="0" style="padding:20px 20px;">
                        <tr>
                            <td><?= lang('银行名称(USD)'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_name_usd" id="bank_name_usd" style="width:255px;" value="<?=$bank_name_usd;?>"/>
                            </td>
                        </tr>
                        <tr>

                            <td><?= lang('bank_account_usd'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_account_usd" id="bank_account_usd" style="width:255px;" value="<?=$bank_account_usd;?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('bank_address_usd'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_address_usd" id="bank_address_usd" style="width:255px;" value="<?=$bank_address_usd;?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('bank_swift_code_usd'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_swift_code_usd" id="bank_swift_code_usd" style="width:255px;" value="<?=$bank_swift_code_usd;?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>----------------------------------</td>
                            <td>
                                --------------------------------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('银行名称(CNY)'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_name_cny" id="bank_name_cny" style="width:255px;" value="<?=$bank_name_cny;?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('bank_account_cny'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_account_cny" id="bank_account_cny" style="width:255px;" value="<?=$bank_account_cny;?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('bank_address_cny'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_address_cny" id="bank_address_cny" style="width:255px;" value="<?=$bank_address_cny;?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>----------------------------------</td>
                            <td>
                                --------------------------------------------------------
                            </td>
                        </tr>
                        <script>
                            function get_taxpayer(){
                                company_name = $('#company_name').textbox('getValue');
                                $.ajax({
                                    type: 'POST',
                                    url: '/api/bw_search_company?company_name='+company_name,
                                    data: '',
                                    dataType: 'json',
                                    success: function (res) {
                                        ddd = res.response.result[0];
                                        $('#taxpayer_name').textbox('setValue',ddd.name);
                                        $('#taxpayer_id').textbox('setValue',ddd.taxId);
                                        $('#taxpayer_address').textbox('setValue',ddd.location);
                                        $('#taxpayer_telephone').textbox('setValue',ddd.fixedPhone);

                                    },
                                    error: function () {
                                        $.messager.alert('Tips', 'error');
                                    }
                                });
                            }
                        </script>
                        <tr>
                            <td><?= lang('taxpayer_name'); ?> <a href='javascript:get_taxpayer();'>点击获取</a></td>
                            <td>
                                <input class="easyui-textbox" name="taxpayer_name" id="taxpayer_name"   style="width:255px;" value="<?=$taxpayer_name;?>"/>
                            </td>
                        </tr>
                        <tr>

                            <td><?= lang('taxpayer_id'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="taxpayer_id" id="taxpayer_id"   style="width:255px;" value="<?=$taxpayer_id;?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('taxpayer_address'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="taxpayer_address"   id="taxpayer_address" style="width:255px;" value="<?=$taxpayer_address;?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('taxpayer_telephone'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="taxpayer_telephone"   id="taxpayer_telephone" style="width:255px;" value="<?=$taxpayer_telephone;?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>----------------------------------</td>
                            <td>
                                --------------------------------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('invoice');?>接收 <?= lang('email');?></td>
                            <td>
                                <input class="easyui-textbox" name="invoice_email" id="invoice_email" style="width:255px;" value="<?=$invoice_email;?>">
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('debit note account');?></td>
                            <td>
                                <select class="easyui-combobox" id="dn_accounts1" style="width:255px;" data-options="
                                    valueField : 'id',
                                    textField : 'title_cn',
                                    url:'/bsc_dict/get_client_account',
                                    value: '<?=$dn_accounts;?>',
                                    multiple:true,
                                ">
                                </select>
                                <input type="hidden" id="dn_accounts" name="dn_accounts">
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('debit note title'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="dn_title" id="dn_title" style="width:255px;" value="<?=$dn_title;?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                提示： 国内公司cny银行和纳税人必填； 国外公司usd必填；
                            </td>
                        </tr>
                        <?php
                        $default_partner = array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'finance');
                        foreach ($default_partner as $k){
                            $vk = $k . "_ids";
                            $v = isset($$vk) ? $$vk : '';
                            if(empty($v)) continue;
                            ?>
                            <tr>
                                <td><?= lang($k); ?></td>
                                <td>
                                    <input class="easyui-textbox" disabled style="width: 255px"
                                           value="<?php foreach (explode(',', $v) as $r) {
                                               echo getUserName($r) . ',';
                                           } ?>">
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </form>
    </div>
    <div title="Contact list" style="padding:1px">
        <iframe scrolling="auto" frameborder="0" id="contact_list" style="width:100%;height:750px;"></iframe>
    </div>
    <div title="每日跟进记录" style="padding:1px">
        <iframe scrolling="auto" frameborder="0" id="follow_up_list" style="width:100%;height:750px;"></iframe>
    </div>
    <?php if(is_admin()){?>
        <div title="Log" style="padding:10px">
            <iframe scrolling="auto" frameborder="0" id="log" style="width:100%;height:750px;"></iframe>
        </div>
    <?php }?>
</div>
<div id="window" style="display:none;">
    <iframe id="window_iframe" name="window_iframe" frameborder="0"></iframe>
</div>
<div id="operator_id_div">
    <form id="operator_id_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label>搜索操作:</label><input name="q" class="easyui-textbox keydown_search" style="width:96px;"
                                       t_id="operator_id_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="operator_id_click" iconCls="icon-search"
               onclick="query_report('operator_id_form', 'operator_id')">查询</a>
        </div>
    </form>
</div>
<div id="service_id_div">
    <form id="service_id_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label>搜索客服:</label><input name="q" class="easyui-textbox keydown_search" style="width:96px;"
                                       t_id="service_id_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="service_id_click" iconCls="icon-search"
               onclick="query_report('service_id_form', 'service_id')">查询</a>
        </div>
    </form>
</div>
</body>