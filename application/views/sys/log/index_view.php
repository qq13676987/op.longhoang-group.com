<script type="text/javascript">
    function doSearch(){
        $('#tt').datagrid('load',{
            master_table_name: $('#master_table_name').val(),
            master_key: $('#master_key').val(),
            table_name: $('#table_name').val(),
            key: $('#key').val(),
            action: $('#action').val(),
        });
    }

    //查询当前页中，匹配value的值
    function search_cu_page(){
        var field = $('#field').combobox('getValue');
        var field_value = $('#field_value').textbox('getValue');
        var data = $('#tt').datagrid('getData');
        var newData = [];
        $.each(data.rows, function(index, item){
            if(item.value[field] !== undefined){
                if(field_value != ''){
                    if(item.value[field].indexOf(field_value) >= 0) newData.push(item);
                }else{
                    newData.push(item);
                }
            }
        });
        $('#tt').datagrid('loadData', newData);
    }
    function read_value(index) {
        $('#tt').datagrid('selectRow', index);
        var rows = $('#tt').datagrid('getSelections');
        if (rows.length == 1 ) {
            $('#value_view table').empty();
            var str = '';
            $.each(rows[0].value, function (index, item) {
                try {
                    var item = JSON.parse(item);
                } catch(e) {
                }
                if(typeof item == 'object' && item ){
                    $.each(item, function (index2, item2) {
                        item[index2] = Object.values(item2).join(':');
                    })
                    item = Object.values(item).join(',');
                }
                str += '<tr>';
                str += '<td>' + index +'</td>';
                if(item == null) item = '';
                str += '<td>' + item +'</td>';
                str += '</tr>';
            });
            $('#value_view table').append(str);
            $('#value_view').window('open');
        }
    }

    function rollback(index){
        $('#tt').datagrid('selectRow', index);
        var rows = $('#tt').datagrid('getSelections');
        if (rows.length == 1 ) {
            var id = rows[0].id;
            $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认回滚？');?>', function(r){
                if (r){
                    // 退出操作;
                    $.ajax({
                        type:'GET',
                        url: '/sys_log/rollback_log/' + id,
                        dataType: 'json',
                        success:function(res){
                            $.message.alert('<?= lang('提示');?>', res.msg);
                        },
                        error: function(){
                            $.message.alert('<?= lang('提示');?>', '<?= lang('error');?>');
                        }
                    });
                }
            });
        }
    }
    function value_for(value, row ,index){
        var str = [];
        $.each(row.value, function (index, item) {
            try {
                var item = JSON.parse(item);
            } catch(e) {
            }
            if(typeof item == 'object' && item ){
                $.each(item, function (index2, item2) {
                    item[index2] = Object.values(item2).join(':');
                })
                item = Object.values(item).join(',');
            }
            if(item == null) item = '';
            str.push(index +':' + item);
        });
        str = str.join(', ');
        str = '<a onclick="read_value(' + index + ')">'+str+'</a>';
        return str;
    }
    function operate_for(value, row ,index){
        var str = '';
        <?php if(is_admin()){ ?>
        str += '<button onclick="rollback(' + index + ')">rollback</button>';
        <?php } ?>
        return str;
    }

    function get_child(tn = ''){
        var url = '/sys_log/get_data/';
        var opt = $('#tt').datagrid('options');
        if(tn == ''){
            url += "<?= $table_name;?>/<?= $key_no;?>";
            $('#master_table_name').val('');
            $('#master_key').val('');
            opt.url = url;
            doSearch();
        }else{
            url += tn + "/all";
            $('#master_table_name').val('<?= $table_name;?>');
            $('#master_key').val('<?= $key_no;?>');

            opt.url = url;
            doSearch();
        }
    }
    $(function(){
        $('#tt').edatagrid({
            url: '/sys_log/get_data/<?= $table_name;?>/<?= $key_no;?>'
        });
        $('#tt').datagrid({
            width:'auto',
            height: $(window).height(),
            onLoadSuccess:function(data){
                var field_data = [];
                $.each(data.rows, function(index,item){
                    $.each(item.value, function(i,it){
                        if($.inArray(field_data, i) === -1) field_data.push(i);
                    });
                });
                var array = [];
                var item_array = [];
                $.each(field_data, function(index,item){
                    if($.inArray(item, item_array) == -1){
                        array.push({value:item,text:item});
                        item_array.push(item);
                    }
                });
                $('#field').combobox('loadData', array);
            }
        });
        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });

</script>

<table id="tt" style="width:1100px;height:450px"
       iconCls="icon-search"
       rownumbers="false" pagination="true"  idField="id"
       pagesize="50" toolbar="#tb"  singleSelect="true" nowrap="false">
    <thead>
    <?php
    if(!empty($f)){
        foreach ($f as $rs){

            $field = $rs[0];
            $disp = $rs[1];
            $width = $rs[2];
            $attr = isset($rs[4]) ? $rs[4] : '';

            if ($width =="0") continue;
            echo "<th data-options=\"field: '$field', width:'$width',$attr\" >" . lang($disp) . "</th>";
        }
    }
    ?>
    <?php if(is_admin()){ ?>
        <th data-options="field: 'operate',width:'200',formatter:operate_for"><?= lang('操作');?></th>
    <?php } ?>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <input type="hidden" id="master_table_name" name="master_table_name">
        <input type="hidden" id="master_key" name="master_key">
        <tr>
            <td><button onclick="get_child('')"><?= lang('返回主日志');?></button></td>
            <?php foreach($child_table_name as $key => $tn){
                $tn['button_name'] = $tn['table_name'];
                if($tn['table_name'] == 'biz_bill'){
                    $tn['button_name'] = lang('bill');
                }
                ?>
                <td><button onclick="get_child('<?= $tn['table_name'];?>')"><?= $tn['button_name'];?></button></td>
            <?php } ?>
            <td><button onclick="javascript:location.href='/download/download_log?id_type=<?= $table_name;?>&id_no=<?= $key_no;?>';"><?= lang('下载日志');?></button></td>
        </tr>
        <tr>
            <td><?= lang('查询字段');?></td>
            <td>
                <select class="easyui-combobox" id="field" name="field" style="width:150px;">
                </select>
            </td>
            <td><?= lang('值');?></td>
            <td>
                <input class="easyui-textbox" id="field_value" name="field_value" style="width:150px;">
            </td>
            <td>
                <a href="javascript: void(0)" class="easyui-linkbutton" plain="true" onclick="search_cu_page()"><?= lang('搜索');?></a>
            </td>
        </tr>
        <!-- <tr>
                <td>
                    <span style="font-size:12px;"><?/*= lang('table_name');*/?></span>
                    <input id="table_name" style="border:1px solid #ccc">
                </td>
                <td>
                    <span style="font-size:12px;"><?/*= lang('order number');*/?></span>
                    <input id="key" style="border:1px solid #ccc">
                </td>
                <td>
                    <span style="font-size:12px;"><?/*= lang('action');*/?></span>
                    <select id="action" style="border:1px solid #ccc">
                        <option value=""></option>
                        <option value="insert">insert</option>
                        <option value="update">update</option>
                        <option value="delete">delete</option>
                    </select>
                </td>

            </tr>-->
    </table>

</div>
<div id="value_view" class="easyui-window" data-options="title:'window',iconCls:'icon-save',modal:true, closed: true, width:600, height:400">
    <table>

    </table>
</div>