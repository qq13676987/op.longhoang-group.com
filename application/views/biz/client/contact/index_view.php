<script type="text/javascript">
    function doSearch() {
        $('#tt').datagrid('load', {
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val(),
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val(),
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val()
        });
        $('#chaxun').window('close');
    }

    function buttons_for(value, row, index) {
        var str = '';
        if (row.is_default == false) {
            str += '<button onclick="set_default(' + row.id + ', \'' + row.client_code + '\')"><?= lang('设置默认');?></button>';
        } else {
            str += '<?= lang('当前默认');?>';
        }
        return str;
    }

    function telephone_for(value, row, index) {
        var str = value;


        return str;
    }
    
    function email_for(value, row, index){
        if(value.length < 3) return value;
        // if(row.ok == -1) return value + "<span style='color: red'>&nbsp;地址错误</span>";
        // if(row.ok == 0) return value + "<span style='color: #ccc'>处理中</span>";
        if(row.email_verify == 1) return value + "<span style='color: blue;'>&nbsp;<?= lang('已验证');?></span>";
        if(row.email_verify == 0) return value + `<span onclick="verify_email(${row.id},'${value}')" style='color: green;cursor: pointer'>&nbsp;<?= lang('未验证');?></span>`;
        return value + `<span onclick="verify_email(${row.id},'${value}')" style='color: green;cursor: pointer'>&nbsp;<?= lang('未验证');?></span>`;
    }
    
    //打开iframe弹窗，主要用于edatagrid头部工具栏
    function open_window(url, title){
        $('#openIframe')[0].src=url;
        $('#_window').window({
            title:title,
            minimizable:false,
            collapsible:true,
            maximizable:false,
            closable:true,
            border:'thin',
            width:'400',
            height:'490',
            modal:true,
        });
    }
    
    function verify_email(id,email){
        if(!id || !email) return;
        iframe_window({title:'<?= lang('邮箱验证');?>',  height : 400, width:1100}, '/mail_box/send_by_template?template_id=48&id='+id+'&mail_type=biz_client_contact_list');
        // $('#email_iframe').attr('src',)
        // $('#email_window').window('open')
    }

    $(function () {
        $('#tt').edatagrid({
            url: '/biz_client_contact/get_data/<?php echo $client_code;?>',
            saveUrl: '/biz_client_contact/add_data/<?php echo $client_code;?>',
            updateUrl: '/biz_client_contact/update_data/',
            destroyUrl: '/biz_client_contact/delete_data',
            onDblClickRow: function (index, row) {
                var url = '/biz_client_contact/edit/?id='+row.id;
                var title = '<?= lang('edit');?>';
                open_window(url, title);
            },
            onError: function (index, data) {
                if(data.code=='0'){
                    $.messager.confirm('确认',data.msg,function(r){
                        if (r){
                            window.open('/other/force_pass_email?email='+data.email);
                        }
                    });
                }else{
                    $.messager.alert('error', data.msg);
                }
            }
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
            rowStyler: function(index,row){
                var str = '';
                if (row.ok == -1){
                    str += 'color:red;';
                }else if(row.ok == 1){
                    str += 'color:green;';
                }
                return str;
            }
        });


        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
        
        //表头工具栏“add”事件
        $('#add').click(function () {
            var url = '/biz_client_contact/add/?client_code=<?=$client_code?>';
            var title = '<?= lang('add');?>';
            open_window(url, title);
        });

        //表头工具栏“edit”事件
        $('#edit').click(function () {
            var rows = $('#tt').datagrid('getSelections');
            if (rows.length < 1) {
                $.messager.alert('系统消息', '请选择编辑对象！');
                return;
            }
            var data = rows[0];
            var url = '/biz_client_contact/edit/?id='+rows[0].id;
            var title = '<?= lang('edit');?>';
            open_window(url, title);
        });
    });

</script>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th field="id" width="80" sortable="true"><?= lang('ID');?></th>
        <th field="role" width="120"><?= lang('角色');?>
        </th>
        <th field="name" width="100" ><?= lang('姓名');?></th>
        <th field="telephone" width="150" formatter="telephone_for"><?= lang('手机号');?></th>
        <th field="telephone2" width="100" ><?= lang('座机');?></th>
        <th field="email" width="180" formatter="email_for"><?= lang('邮箱');?></th>
        
        <!--<th field="menu" width="300" editor="{type:'combobox',-->
        <!--options:{-->
        <!--multiple:true,-->
        <!--valueField:'value',-->
        <!--textField:'value',-->
        <!--url:'/biz_client_contact/client_user_menu_option'-->
        <!--}}">前台权限</th>-->
        </th>
        <!--<th field="sales_id" width="150">默认销售</th>-->
        <!--<th field="user_range" width="150">主子账号</th>-->
        <th field="live_chat" width="100" sortable="true" ><?= lang('QQ');?></th>
        <th field="position" width="100" ><?= lang('职位');?></th>
        <th field="remark" width="100" ><?= lang('备注');?></th>
        <th field="sailing" width="400" ><?= lang('航线');?>
        </th>
        <th field="create_time" width="150"><?= lang('创建时间');?></th>
        <th field="update_time" width="150"><?= lang('修改时间');?></th>
        <th field="buttons" width="80" data-options="formatter: buttons_for"></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton <?= menu_role('client_contact_add') ? '' : 'hide';?>" iconCls="icon-add" id="add" plain="true"><?= lang('add'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton <?= menu_role('client_contact_save') ? '' : 'hide';?>" iconCls="icon-save" id="edit" plain="true"><?= lang('编辑'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton <?= menu_role('client_contact_delete') ? '' : 'hide';?>" iconCls="icon-remove" plain="true"
                   onclick="javascript:$('#tt').edatagrid('destroyRow')"><?= lang('delete'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true"
                   onclick="javascript:$('#chaxun').window('open');"><?= lang('query'); ?></a>
            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                <?= lang('查询'); ?> 1
            </td>
            <td align="right">
                <select name="f1" id="f1" class="easyui-combobox" required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs) {
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1" id="s1" class="easyui-combobox" required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v1" class="easyui-textbox" style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('查询'); ?> 2
            </td>
            <td align="right">
                <select name="f2" id="f2" class="easyui-combobox" required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs) {
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2" id="s2" class="easyui-combobox" required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v2" class="easyui-textbox" style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('查询'); ?> 3
            </td>
            <td align="right">
                <select name="f3" id="f3" class="easyui-combobox" required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs) {
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s3" id="s3" class="easyui-combobox" required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v3" class="easyui-textbox" style="width:120px;">
            </td>
        </tr>
    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()"
       style="width:200px;height:30px;"><?= lang('查询'); ?></a>
</div>

<!--弹窗-->
<div id="_window" style="display: none; overflow:hidden;">
    <iframe scrolling="auto" id='openIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
</div>
 