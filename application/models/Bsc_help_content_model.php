<?php
   class Bsc_help_content_model extends CI_Model{
       private $table = 'bsc_help_content';

       public function __construct()
       {
           parent::__construct();
           $this->load->database();
       }

       public function get_one($f="",$v=""){
           $this->db->where($f, $v);
           $query = $this->db->get($this->table);
           return $query->row_array();
       }

       public function get($where="" ,$sort="id",$order="desc"){
           if($where !="")	$this->db->where($where);
           $this->db->order_by($sort,$order);
           $query = $this->db->get($this->table);
           return $query->result_array();
       }

       public function total($where=""){
           if($where !="")	$this->db->where($where);
           $this->db->from($this->table);
           return  $this->db->count_all_results('');
       }

       public function save($data=array()){

           $this->db->insert($this->table, $data);
           return $this->db->insert_id();
       }

       public function update($id='',$data=array()){
           $this->db->where('id', $id);
           $this->db->update($this->table, $data);
           return $id;
       }

       public function mdelete($id=''){
           // delete container
           $this->db->where('id', $id);
           $this->db->delete($this->table);
       }

   }