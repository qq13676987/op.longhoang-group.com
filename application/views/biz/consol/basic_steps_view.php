<ul style="list-style: none;background-color:#0099cc;padding-top: 5px;margin:0;padding-left: 5px;">
    <li style="display: flex;align-items: end;width: 99vw;flex-wrap: wrap">
        <?php if($biz_type == 'import'):?>
            <?php echo show_steps('biz_consol.dingcangshenqing','biz_consol',$id,in_array('dingcangshenqing',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_consol.dingcangwancheng','biz_consol',$id,in_array('dingcangwancheng',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_consol.yupeiyifang','biz_consol',$id,in_array('yupeiyifang',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_shipment.cfs_in','biz_consol',$id,in_array('cfs_in',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_consol.operator_si','biz_consol',$id,in_array('operator_si',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_consol.document_si','biz_consol',$id,in_array('document_si',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_shipment.tidanqianfa','biz_consol',$id,in_array('tidanqianfa',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_shipment.chuanyiqihang','biz_consol',$id,in_array('chuanyiqihang',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_shipment.flag_ata','biz_consol',$id,in_array('flag_ata',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_shipment.haiguanfangxing','biz_consol',$id,in_array('haiguanfangxing',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_shipment.flag_truck','biz_consol',$id,in_array('flag_truck',$G_userBscEdit),$year); ?>
			<?php echo show_steps('biz_shipment.pre_alert','biz_consol',$id,false,$year);?>
        <?php else: ?>
            <?php echo show_steps('biz_consol.dingcangshenqing','biz_consol',$id,in_array('dingcangshenqing',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_consol.dingcangwancheng','biz_consol',$id,in_array('dingcangwancheng',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_consol.yupeiyifang','biz_consol',$id,in_array('yupeiyifang',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_consol.yifangdan','biz_consol',$id,in_array('yifangdan',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_consol.xiangyijingang','biz_consol',$id,in_array('xiangyijingang',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_consol.operator_si','biz_consol',$id,in_array('operator_si',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_consol.vgm_confirmation','biz_consol',$id,in_array('vgm_confirmation',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_consol.document_si','biz_consol',$id,in_array('document_si',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_consol.matoufangxing','biz_consol',$id,in_array('matoufangxing',$G_userBscEdit),$year); ?>
            <?php echo show_steps('biz_consol.chuanyiqihang','biz_consol',$id,in_array('chuanyiqihang',$G_userBscEdit),$year); ?>
			<?php echo show_steps('biz_shipment.pre_alert','biz_consol',$id,false,$year);?>
       <?php endif;?>
    </li>
</ul>

<script>
    $(function(){
        $('.status').change(function () {
            var val = 0;
            var inp = $(this);
            var checked = $(this).prop('checked');
            if (checked) {
                var day1 = new Date();
                var month = '';
                if (day1.getMonth() + 1 < 10) {
                    month = '0' + (day1.getMonth() + 1);
                } else {
                    month = day1.getMonth() + 1;
                }
                var val = day1.getFullYear() + "-" + month + "-" + day1.getDate() + ' ' + day1.getHours() + ':' + day1.getMinutes() + ':' + day1.getSeconds();

            }

            var field = $(this).attr('id');
            var data = {};
            data[field] = val;
            data['field'] = field;
            $.ajax({
                url: '/biz_consol/update_checkbox/<?php echo $id;?>',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function (res) {
                    if (res.code == 0) {
						parent.$.messager.alert('Tips', res.msg);
						location.reload()
                    } else {
                        parent.$.messager.alert('Tips', res.msg);
                        inp.prop('checked', !checked)
                    }
                }, error: function () {
                    parent.$.messager.alert('Tips', '发生错误');
                }
            });
        });
    })
</script>
