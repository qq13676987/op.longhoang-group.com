<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= lang('收件人');?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css" media="all">
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
</head>
<body>
<table class="layui-table" style="margin: 0px;">
    <colgroup>
        <col width="100">
        <col width="250">
        <col width="250">
        <col>
    </colgroup>
    <thead>
    <tr>
        <th><?= lang('发送状态');?></th>
        <th><?= lang('邮箱');?></th>
        <th><?= lang('抄送邮箱');?></th>
        <th><?= lang('失败原因');?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($data as $key => $item){
        ?>
        <tr>
            <td>
                <?php
                switch ($item['send_status']){
                    case 0:
                        echo '<font color="#f4a460">' . lang('待发送') . '</font>';
                        break;
                    case 1:
                        echo '<font color="green">' . lang('成功发送') . '</font>';
                        break;
                    case 2:
                        echo '<font color="blue">' . lang('正在发送中...') . '</font>';
                        break;
                    case -1:
                        echo '<font color="red">' . lang('发送失败') . '</font>';
                        break;
                }
                ?>
            </td>
            <!--<td><?=$item['name'];?></td>-->
            <td><?=$item['email'];?></td>
            <td><?=$item['ccs'];?></td>
            <td><?=$item['send_status'] == -1?$item['send_result']:''?></td>
        </tr>
    <?php }?>
    </tbody>
</table>
</body>
</html>
<script>
    $(function(){
        setTimeout(()=>{
            location.reload();
        },10000)
    })
</script>