<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<form id="fm" action="<?= $url;?>" method="post">
    <table width="400">
        <?php if(in_array('company', $parameter)){?>
            <tr>
                <td>所属公司</td>
                <td>
                    <select class="easyui-combobox" style="width:300px;" name="parameter[company]" id="company" editable="true" data-options="
                        value: 'LEAGUESHIPPING',
                        required:true,
                        valueField:'value',
                        textField:'value',
                        url:'/bsc_dict/get_option/hbl_type',
                    ">
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('bill_client', $parameter)){?>
            <tr>
                <td><?= lang('抬头');?></td>
                <td>
                    <select class="easyui-combobox" style="width:300px;" name="parameter[client_code]" id="client_code" data-options="
                        editable:false,
                        onSelect:function(rec){
                            $('#dn_account').combobox('reload', '/bsc_dict/by_s_get_client_account/' + rec.value);
                        },
                    ">
                        <option value="">--<?= lang('请选择');?>--</option>
                        <?php foreach ($bill_client as $row){?>
                            <option value="<?= $row['client_code'];?>"><?= $row['company_name'];?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php }?>
        <?php if(in_array('vn_ex_rate', $parameter)){?>
            <tr>
                <td><?= lang('汇率');?></td>
                <td>
                    <input class="easyui-numberbox" style="width:300px" name="parameter[vn_ex_rate]" id="vn_ex_rate" data-options="
                        min:0,
                        precision:4,
                    ">
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('client_account', $parameter)){ ?>
            <tr>
                <td>Bank Info</td>
                <td>
                    <select class="easyui-combobox" editable="false" id="client_account" name="parameter[account_id]" style="width:300px;">
                        <?php foreach ($client_account as $row){?>
                            <option value="<?= $row['id'];?>"><?= $row['title_cn'];?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('dn_currency', $parameter)){ ?>
            <tr>
                <td><?= lang('币种');?></td>
                <td>
                    <select class="easyui-combobox" editable="false" id="currency" name="parameter[currency]" style="width:300px;">
                        <?php foreach ($dn_currency as $row){?>
                            <option value="<?= $row['value'];?>"><?= $row['value'];?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('dn_account', $parameter)){ ?>
            <tr>
                <td><span style="color:blue;" title="该选项取值于当票客户的debit note account">Bank info</span></td>
                <td>
                    <select class="easyui-combobox" id="dn_account" name="parameter[account_id]" style="width:300px;" data-options="
                        editable:false,
                        valueField:'id',
                        textField:'title_cn',
                    ">
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('bill_invoice_no', $parameter)){ ?>
            <tr>
                <td>Invoice No.</td>
                <td>
                    <select class="easyui-combobox"  id="parameter" style="width:300px;" data-options="multiple:true,onChange:function(newValue,oldValue){
                            var data = $('#parameter').combobox('getValues');
                            data = data.join(',');
                            $('#parameter1').val(data);
                        }">
                        <option value="0">-</option>
                        <?php foreach ($bill_invoice_no as $row){?>
                            <option value="<?= $row['id'];?>"><?= $row['invoice_no'];?></option>
                        <?php } ?>
                    </select>
                    <input name="parameter[invoice_id]" id="parameter1" type="hidden"  >
                    <br>
                    <span style="color:red;font-size:10px;">已开票的需选择发票号</span>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('dn_invoice_no', $parameter)){ ?>
            <tr>
                <td>Invoice No.</td>
                <td>
                    <select class="easyui-combobox" name="parameter[invoice_id]" style="width:300px;">
                        <option value="0">-</option>
                        <?php foreach ($dn_invoice_no as $row){?>
                            <option value="<?= $row['id'];?>"><?= $row['invoice_no'];?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('truck_one', $parameter)){ ?>
            <tr>
                <td>truck</td>
                <td>
                    <select class="easyui-combobox" name="parameter[truck_id]" style="width:300px;">
                        <option value="0">-</option>
                        <?php foreach ($truck_one as $row){?>
                            <option value="<?= $row['id'];?>"><?= $row['truck_company'];?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('is_confirm_date', $parameter)){ ?>
            <tr>
                <td>账单确认日期</td>
                <td>
                    <select class="easyui-combobox" name="parameter[is_confirm_date]" style="width:300px;">
                        <option value="">全部</option>
                        <option value="1">未确认</option>
                        <?php foreach ($confirm_dates as $row){?>
                            <option value="<?= $row['confirm_date'];?>"><?= $row['confirm_date'];?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('B_L_number', $parameter)){ ?>
            <tr>
                <td>提单份数</td>
                <td>
                    <select class="easyui-combobox" name="parameter[B_L_number]" style="width:300px;">
                        <?php foreach ($B_L_number as $row){?>
                            <option value="<?= $row['value'];?>"><?= $row['name'];?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('gs_soc_tixianghao', $parameter)){ ?>
            <tr>
                <td>提箱号</td>
                <td>
                    <select class="easyui-combobox" name="parameter[gs_soc_tixianghao]" style="width:300px;">
                        <?php foreach ($gs_soc_tixianghao as $row){?>
                            <option value="<?= $row;?>"><?= $row;?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('charge_name_type', $parameter)){ ?>
            <tr>
                <td>charge Name </td>
                <td>
                    <select class="easyui-combobox" name="parameter[charge_name_type]" style="width:300px;">
                        <option value="en"><?= lang('English');?></option>
                        <option value="cn"><?= lang('中文');?></option>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('dn_template_title', $parameter)){ ?>
            <tr>
                <td><?= lang('DN模板标题');?></td>
                <td>
                    <select class="easyui-combobox" name="parameter[dn_template_title]" style="width:300px;">
                        <option value="TAX INVOICE">TAX INVOICE</option>
                        <option value="INVOICE">INVOICE</option>
                        <option value="STATEMENT">STATEMENT</option>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('hold_without_tax', $parameter)){ ?>
            <tr>
                <td><?= lang('免税额');?></td>
                <td>
                    <select class="easyui-combobox" name="parameter[hold_without_tax]" style="width:300px;">
                        <?php foreach ($hold_without_tax as $row){?>
                            <option value="<?= $row['value'];?>"><?= $row['name'];?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('this_local_currency', $parameter)){ ?>
            <tr>
                <td><?= lang('免税额');?></td>
                <td>
                    <select class="easyui-combobox" name="parameter[this_local_currency]" style="width:300px;">
                        <?php foreach ($this_local_currency as $row){?>
                            <option value="<?= $row['value'];?>"><?= $row['name'];?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('convert_currency', $parameter)){ ?>
            <tr>
                <td><?= lang('转换币种');?></td>
                <td>
                    <select class="easyui-combobox" name="parameter[convert_currency]" style="width:300px;">
                        <?php foreach ($convert_currency as $row){?>
                            <option value="<?= $row['value'];?>"><?= $row['name'];?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
        <?php if(in_array('third_party_payment', $parameter)){?>
            <tr>
                <td><?= lang('third party payment');?></td>
                <td>
                    <select class="easyui-combobox" style="width:300px;" name="parameter[company_client_code]" id="client_code" data-options="
                        editable:false,
                    ">
                        <option value="">--<?= lang('请选择');?>--</option>
                        <?php foreach ($third_party_payment as $row){?>
                            <option value="<?= $row['value'];?>"><?= $row['company_name_en'];?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php }?>
    </table>
    <button id="submit" type="submit"><?= lang('提交'); ?></button>
</form>

