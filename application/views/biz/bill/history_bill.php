<body>
<div style="padding:20px">
    <!--<span style="color:#79a1d7;font-weight: bold;font-size: 16px;line-height: 20px;vertical-align: middle;">客户名称：</span>-->
    <!--<span style="color:red;font-size: 15px;line-height: 20px;vertical-align: middle;margin-right: 20px"><?=$company_name?></span>-->
    <span style="color:#79a1d7;font-weight: bold;font-size: 16px;line-height: 20px;vertical-align: middle;"><?= lang('shipment no');?>：</span>
    <input id="job_no" class="easyui-textbox" style="vertical-align: middle;" value="<?= $job_no ?>">
    <a id="btn" class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="vertical-align: middle;"><?= lang('搜索');?></a>
    <?= lang('快速直达');?>：
    <?php
    $rs = $this->db->query("select distinct (select job_no from biz_shipment where id = biz_bill.id_no) as job_no, id_no from biz_bill where biz_bill.id_type = 'shipment' and (client_code = '$client_code' or client_code = '$client_code2') order by id desc limit 5")->result_array();
    foreach ($rs as $r) {
        if($r["id_no"]==$id_no) continue;
        $url = "?job_no={$r['job_no']}&id_no=$id_no&client_code=$client_code&client_code2=$client_code2";
        echo "<a href='$url'>{$r['job_no']} </a> | ";
    }
    if($job_no=="" && !empty($rs) && isset($url)) echo "<script>location.href='$url';</script>";
    ?>
    <br>
    <span style="color:red;"><?= lang('表格底部有个确认费用,勾选后点确认可以快速复制输入');?></span>
</div>
<table id="ddv" class="easyui-datagrid" style="width:auto;"
       title="" iconCls="icon-search"
       rownumbers="false" idField="id"
       toolbar="#tb" singleSelect="false" checkbox="true">
    <thead>

    <tr>
        <th title="<?= lang('全选');?>" field="ck" checkbox="true"></th>
        <th field="type" width="80" align="center"><?= lang('类型');?></th>
        <th field="bill_ETD" width="100" align="center" editor="shijian"><?= lang('业务日期');?></th>
        <!--<th field="charge_code" width="130" align="left">charge code</th>  -->
        <th field="charge" width="130" align="center"><?= lang('费用名称');?></th>
        <th field="currency" width="100" align="center"><?= lang('币种');?></th>
        <th field="amount" width="100" align="center" data-options="formatter:usdPrice"><?= lang('金额');?></th>
        <th field="vat" width="50" align="center"><?= lang('税率');?></th>
        <th field="client_name" width="220" align="center"><?= lang('费用对象');?></th>
        <th field="remark" width="180" align="center"><?= lang('备注');?></th>
        <!--        <th field="invoice_no" width="100" align="center">发票号</th>-->
        <!--        <th field="payment_date" width="100" align="center">核销日期</th>-->
    </tr>

    </thead>

</table>
<br>
<div style="text-align: right">
    <a id="copy" class="easyui-linkbutton" data-options="iconCls:'icon-add'"
       style="vertical-align: middle;margin-right: 20px"><?= lang('确认复制');?></a>
</div>
</body>
<script>


    $(function () {
        $('#ddv').edatagrid({
            url: '/biz_bill/history_bill_get/shipment/<?=$id?>',
        });

        $('#ddv').datagrid({
            height:'430',
            onLoadSuccess: function (data) {
                // var verification_m = 0;
                //循环判断操作为新增的不能选择
                for (var i = 0; i < data.rows.length; i++) {
                    // console.log(data.rows[i].parent_id);
                    //根据operate让某些行不可选
                    if (data.rows[i].parent_id != "0") {
                        $("input[type='checkbox']")[i + 1].disabled = true;
                    }
                }

            },
            onClickRow: function(rowIndex, rowData){
                //加载完毕后获取所有的checkbox遍历
                $("input[type='checkbox']").each(function(index, el){
                    //如果当前的复选框不可选，则不让其选中
                    if (el.disabled == true) {
                        $('#ddv').datagrid('unselectRow', index - 1);
                        if (rowData.stopDate != ""&&rowData.stopDate!=null) {
                            $.messager.show({
                                title:"<?= lang('提示');?>",
                                msg:"<?= lang('此企业信息已禁止，不可以选择！');?>"
                            })
                        }
                    }
                })
            },
            onCheckAll:function(rows){
                //加载完毕后获取所有的checkbox遍历
                $("input[type='checkbox']").each(function(index, el){
                    //如果当前的复选框不可选，则不让其选中
                    if (el.disabled == true) {
                        $('#ddv').datagrid('unselectRow', index - 1);
                    }
                })
            },
            rowStyler: function (index, row) {
                var styler = "";
                if (row.type == 'sell') {
                    styler += 'color:blue;';
                }
                if (row.type == 'SellTotal') {
                    styler += 'color:blue;font-weight:bold;';
                }

                if (row.type == 'cost') {
                    styler += 'color:red;';
                }
                if (row.type == 'CostTotal') {

                    styler += 'color:red;font-weight:bold;';

                }
                if (row.charge == '<?= lang('毛利润');?>') {
                    styler += 'color:black;font-weight:bold;';
                }
                if (row.parent_id == '1') {
                    styler += 'background-color:#C0C0C0;';
                }

                return styler;

            }
        });

        $('#btn').bind('click', function () {
            var job_no = $('#job_no').textbox('getValue');
            window.location.href = '/biz_bill/history_bill?job_no=' + job_no + '&id_no=<?=$id_no?>&client_code=<?=$client_code?>&client_code2=$client_code2';
        });

        $('#copy').bind('click', function () {
            //获取选中数据
            var rows = $('#ddv').datagrid('getSelections');




            var ids = [];
            for (let index = 0; index < rows.length; index++) {

                if (typeof (rows[index]['id']) != 'undefined') {
                    if(rows[index]['parent_id']=='0'){
                        ids.push(rows[index]['id']);
                    }

                }
            }

            if (ids.length < 1) {
                alert('<?= lang('请选择有效数据复制!');?>');
                return;
            }

            //把ids变成以,隔开字符串
            ids = ids.join(",");
            console.log(ids);

            //确是否复制
            $.messager.confirm('<?= lang('提示');?>', '<?= lang('您确定要复制这个费用吗?');?>', function (r) {
                if (r) {
                    $.ajax({
                        type: "POST",
                        dataType: "json",//预期服务器返回的数据类型
                        url: '/biz_bill/copy_bill',
                        data: {ids: ids, id_no: '<?=$id_no;?>'},
                        success: function (data) {
                            alert(data.msg);

                            //关闭当前窗口
                            window.parent.$('#history').window('close')
                            //刷新父页面表格
                            window.parent.$('#tt').edatagrid('reload');
                        },
                        error: function () {

                        }
                    });
                }
            });

        });
    });

    function usdPrice(val, row) {
        if (val != "") {
            return '$' + val + '';
        } else {
            return "";
        }
    }


</script>
