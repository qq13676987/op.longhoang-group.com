<script type="text/javascript">
    function doSearch(isread){
        $('#tt').datagrid('load',{
            isread:isread
        });
    }
    $(function () {
        $('#tt').edatagrid({
            url: '/bsc_notice/get_user_noitce_data',
        });
        $('#tt').datagrid({
            width: 'auto',
            height: '442px',
            onDblClickRow: function (index,row) {
                //已读操作
                if(row.isread != 1){
                    $.ajax({
                        url:'/bsc_notice/notice_read?id=' +row.id,
                        type:'get',
                        dataType:'json',
                        success:function(res) {
                            $('#tt').datagrid('reload');
                        }
                    });
                }
                //打开对应窗口
                if(row.href != null){
                    window.open(row.href);
                }
            }
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });

        $('.datagrid-view2').css('left',0);
    });
    function setRowBgColor(index, row) {
        if(row.isread < 0){

        }
        <?php foreach($read_color as $key => $val){?>
        else if(row.isread == <?= $key;?>){
            return '<?= $val;?>';
        }
        <?php }?>
    }

    function tpl_status(val, row) {
        if(row.isread != 1) {
            return '<a href="javascript:void(0);" onclick="set_read('+row.id+')" class="easyui-linkbutton" data-options="iconCls:\'icon-ok\'">知道了</a>';
        }else{
            return '<span style="color: #5a5a5d;">已读</span>';
        }
    }

    function set_read(id) {
        $.ajax({
            url:'/bsc_notice/notice_read?id=' +id,
            type:'get',
            dataType:'json',
            success:function(res) {
                $('#tt').datagrid('reload');
            }
        });
    }
</script>
<a href="javascript:void(0);" onclick="doSearch(0)">未处理</a>|<a href="javascript:void(0);" onclick="doSearch(1)">已处理</a> (提示:鼠标双击后就表示'已处理')
<table id="tt" style="width:1100px;height:450px"
       rownumbers="false"  idField="id"
       pagesize="30" singleSelect="true" nowrap="false" rowStyler="setRowBgColor">
    <thead>
    <tr>
        <th field="txt" width="300" ><?= lang('txt')?></th>
        <th field="create_time" width="100" align="center"><?= lang('create_time')?></th>
        <th field="status" width="100" align="center" data-options="formatter:tpl_status" ><?= lang('阅读状态')?></th>
        <th field="id" width="80" align="center" ><?= lang('ID')?></th>
    </tr>
    </thead>
</table>
