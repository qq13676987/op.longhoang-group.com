<?php
function consol_status($consol_id = 0, $status = ''){
    return false;
    Model('biz_consol_model');
    Model('biz_shipment_model');
    $CI =& get_instance();
    $CI->db->select('status,yupeiyifang,customer_booking');
    //$CI->db->force_master();
    $consol = $CI->biz_consol_model->get_by_id($consol_id);
    if($consol_id == 0){
        return false;
    }
    $CI->db->select('id,status');
    //$CI->db->force_master();
    $shipments = $CI->biz_shipment_model->no_role_get("consol_id = $consol_id");

    //如果存在shipment，那么改为normal
    if($status == '') $status = $consol['status'];
    $consol_update = array();
    if(!empty($shipments)){
        if($status == 'cancel'){
            //2021-08-25 由于群里徐婕有一票无预配有MBL无法解绑 张路遥提出 如果consol退关，那么不进行任何处理
            // //如果consol是cancel，shipment是error
            $shipment_updates = array();
            // foreach ($shipments as $shipment){
            //     $shipment_update = array();
            //     $shipment_update['status'] = 'error';
            //     if($shipment_update['status'] == $shipment['status']) unset($shipment_update['status']);

            //     if(!empty($shipment_update)){
            //         $shipment_update['id'] = $shipment['id'];
            //         $shipment_updates[] = $shipment_update;
            //     }
            // }
            // $consol_update['status'] = 'cancel';
        }else{
            //如果consol是normal，shipment是normal
            $shipment_updates = array();
            foreach ($shipments as $shipment){
                $shipment_update = array();
                $shipment_update['status'] = 'normal';
                if($shipment_update['status'] == $shipment['status']) unset($shipment_update['status']);

                if(!empty($shipment_update)){
                    $shipment_update['id'] = $shipment['id'];
                    $shipment_updates[] = $shipment_update;
                }
            }
            $consol_update['status'] = 'normal';
        }
        foreach ($shipment_updates as $shipment_update){
            $shipment_id = $shipment_update['id'];
            unset($shipment_update['id']);
            $CI->biz_shipment_model->update($shipment_id, $shipment_update);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment";
            $log_data["key"] = $shipment_id;
            $log_data["action"] = "status_update";
            $log_data["value"] = json_encode($shipment_update);
            log_rcd($log_data);
        }
    }else{
        //不存在进入逻辑判断
        //如果consol为normal且无shipment，根据预配标记进行修改
        if($status == 'normal'){
            //2021-09-14 张路遥提出，常规订舱原流程，非常规订舱，走cancel
            if($consol['customer_booking'] === '0'){
                if(empty($consol['yupeiyifang']) || $consol['yupeiyifang'] == '0'){
                    $consol_update['status'] = 'error';
                }else{
                    $consol_update['status'] = 'pending';
                    $consol_update['pending_datetime'] = date('Y-m-d H:i:s');
                }
            }else{
                $consol_update['status'] = 'cancel';
            }
        }else if($status == 'pending'){
            $consol_update['status'] = $status;
            if(empty($consol['yupeiyifang']) || $consol['yupeiyifang'] == '0'){
                $consol_update['status'] = 'error';
            }
        }else if($status == 'error'){
            $consol_update['status'] = $status;
            if(!empty($consol['yupeiyifang']) && $consol['yupeiyifang'] != '0'){
                $consol_update['status'] = 'pending';
                $consol_update['pending_datetime'] = date('Y-m-d H:i:s');
            }
        }else{
            $consol_update['status'] = $consol['status'];
        }
    }
    if(isset($consol_update['status']) && $consol['status'] == $consol_update['status'])unset($consol_update['status']);
    if(!empty($consol_update)){
        $CI->biz_consol_model->update($consol_id, $consol_update);
        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_consol";
        $log_data["key"] = $consol_id;
        $log_data["action"] = "status_update";
        $log_data["value"] = json_encode($consol_update);
        log_rcd($log_data);
    }
}

//创建市场邮件
function cancel_market_mail($consol_id){
    $CI =& get_instance();
    Model('biz_consol_model');
    $CI->db->select('id,job_no,booking_ETD,(select client_name from biz_client where biz_client.client_code = biz_consol.trans_carrier) as trans_carrier_name,trans_destination_name,box_info,status,carrier_ref');
    $consol = $CI->biz_consol_model->get_by_id($consol_id);

    if(empty($consol)) return false;
    if($consol['status'] != 'cancel') return false;

    Model('biz_duty_model');
    $CI->db->select('marketing_id');
    $consol_duty = $CI->biz_duty_model->get_where_one("biz_table = 'biz_consol' and id_no = $consol_id");
    if(empty($consol_duty)) return false;

    $time = time();
    $created_by = get_session('id');
    $recipient_by = $consol_duty['marketing_id'];
    if($recipient_by == 0) return true;
    if($created_by ==  $recipient_by) return true;

    $box_info = json_decode($consol['box_info'], true);
    $consol['box_info_text'] = array();
    foreach ($box_info as $box){
        $consol['box_info_text'][] = $box['num'] . 'X' . $box['size'];
    }
    $consol['box_info_text'] = join(',', $consol['box_info_text']);
    
    //退关时加入查询是否有申请中的shipment,有的话自动拒绝,且发送邮件
    Model('biz_shipment_model');
    $CI->db->select('id,job_no');
    $apply_shipments = $CI->biz_shipment_model->no_role_get("consol_id_apply = $consol_id");
    foreach ($apply_shipments as $apply_shipment){
        //原本的拒绝掉
        $apply_data = array('consol_id_apply' => 0);
        $CI->biz_shipment_model->update($apply_shipment['id'], $apply_data);
        
        $log_data = array();
        $log_data["table_name"] = "biz_shipment";
        $log_data["key"] = $apply_shipment['id'];
        $log_data["action"] = "apply_update";
        $log_data["value"] = json_encode($apply_data);
        log_rcd($log_data);
        $request_data = array('id' => $apply_shipment['id'], 'push_role' => 'operator', 'shipment_job_no' => $apply_shipment['job_no'], 'consol_job_no' => $consol['job_no'],'is_approved' => 0);
        save_queue('SHIPMENT申请绑定CONSOL','biz_shipment', $apply_shipment['id'], $request_data, '', 'shipment_apply_consol');
    }    
    
    
    
    //生成一条闹钟数据，且添加一条队列
    //2021-08-19 16:22 张路遥 邮件内容内，在consol号后加入提单号，并且标红
    $add_data = array();
    $add_data['recipient_by'] = $recipient_by;
    $add_data['plan_send_time'] = date('Y-m-d H:i:s', $time);
    $add_data['actual_send_time'] = '0000-00-00 00:00:00';
    $add_data['category'] = '订舱退关';
    $add_data['subject'] = "退关提醒!/{$consol['job_no']}/{$consol['carrier_ref']}/{$consol['booking_ETD']}/{$consol['trans_carrier_name']}/{$consol['trans_destination_name']}/{$consol['box_info_text']}";
    $add_data['content'] = "{$consol['job_no']}/<span style='color:red;'>{$consol['carrier_ref']}</span>/{$consol['booking_ETD']}/{$consol['trans_carrier_name']}/{$consol['trans_destination_name']}/{$consol['box_info_text']}<br/>CONSOL已退关，请知悉";
    $add_data['auto_content'] = '';

    Model('biz_market_mail_model');
    $this_id = $CI->biz_market_mail_model->save($add_data);

    // record the log
    $log_data = array();
    $log_data["table_name"] = "biz_market_mail";
    $log_data["key"] = $this_id;
    $log_data["action"] = "insert";
    $log_data["value"] = json_encode($add_data);
    log_rcd($log_data);
    return true;
}