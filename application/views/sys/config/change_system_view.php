<body>
<div style="margin-top: 100px;margin-left: 50px;">
    <span style="display: block;font-size: 1.5em;font-weight: bold;padding: 10px;"><?= lang('请选择想要切换的系统');?></span>
    <div style="width:100%;display:flex;display: -webkit-flex;flex-wrap: wrap">
        <?php foreach ($rows as $row){ ?>
            <div style="margin-right: 30px;margin-left: 20px;margin-bottom: 20px;">
                <a class="easyui-linkbutton trans_mode_div" href="<?= $row['url'];?>" target="_parent" style="padding: 0.55em 2.5em;"><?= $row['name'];?></a>
            </div>
        <?php } ?>
    </div>
</div>
</body>