<script type="text/javascript">
		var url; 
        $(function(){
			var selectIndex = -1;
            $('#tt').edatagrid({
                url: '/biz_shipment/get_data?sub=<?= $parent_id;?>',
                destroyUrl: '/biz_shipment/delete_data',
                onError: function (index, data) {
                    $.messager.alert('error', data.msg);
                }
            });
			$('#tt').datagrid({
				width:'auto',
				height: $(window).height(),
				onDblClickRow: function(rowIndex) {
					$(this).datagrid('selectRow', rowIndex); 
					var row = $('#tt').datagrid('getSelected');
					if (row){ 
						url = '/biz_shipment/edit/'+row.id;
						window.open(url);
					}
				},
				onClickRow: function(index, data) {
					if (index == selectIndex) { 
						$(this).datagrid('unselectRow', index); 
						selectIndex = -1;
					}else{
						selectIndex = index;
					}   
				},
				

				// detail view,expandrows

				view: detailview,
                detailFormatter:function(index,row){
                    return '<div style="padding:2px"><table class="ddv"></table></div>';
                }, 
                onExpandRow: function(index,row){ 
                    var ddv = $(this).datagrid('getRowDetail',index).find('table.ddv'); 
                    ddv.datagrid({ 
                        url:'/biz_fee/get_data/shipment/' + row.plate_no,  
                        fitColumns:false, 
                        singleSelect:true, 
                        rownumbers:true,    
                        loadMsg:'', 
                        height:'auto', 
                        columns:[[ 
                            {field:'plan_date',title:'计划日期',width:100,align:'left'}, 
                            {field:'rcv_date',title:'实际日期',width:100,align:'left'}, 
                            {field:'f_name',title:'费用科目',width:150,align:'left'}, 
                            {field:'f_amount',title:'费用金额',width:100,align:'left'}, 
                            {field:'f_object',title:'交易对象',width:100,align:'left'}, 
                            {field:'remark',title:'备注',width:180,align:'left'},
                            {field:'id',title:'id',width:100}
                        ]], 
                        onResize:function(){ 
                            $('#tt').datagrid('fixDetailRowHeight',index); 
                        },

                        onLoadSuccess:function(){ 
                            setTimeout(function(){ 
                                $('#tt').datagrid('fixDetailRowHeight',index); 
                            },0); 
                        } 
                    });  
                    $('#tt').datagrid('fixDetailRowHeight',index);

                }
			}); 
			$(window).resize(function () {
				$('#tt').datagrid('resize');
			});
				
        });
        function del(){
            var row = $('#tt').datagrid('getSelected');
            if (row){
                //if(row.step!="未收款"){
                //	alert("不可以删除");
                //}else{
                $('#tt').edatagrid('destroyRow');
                //}

            }else{
                alert("No Item Selected");
            }
        }

        function box_info_for(value, row, index){
            var str = '';
            try{
                var box_info_arr = $.parseJSON(value);
            }catch(e){
                var box_info_arr = [{size:'****',num:'*'}];
            }
            var box_info_text = [];
            $.each(box_info_arr, function (i, it) {
                box_info_text.push(it.size + 'x' + it.num);
            });
            str += box_info_text.join(', ');
            return str;
        }
</script>
    
<table id="tt"
            rownumbers="false" pagination="true"  idField="id"
            pagesize="30" toolbar="#tb"  singleSelect="true" nowrap="false">
        <thead>
            <tr>
			<th field="id" width="77" sortable="true">
				ID
			</th> 
			<th field="cus_no" width="110">
				关单号
			</th>
			<th field="client_company" width="248">
				委托方公司
			</th>
			<th field="trans_origin_name" width="80">
				起运港全称
			</th> 
			<th field="trans_destination_name" width="105">
				目的港全称
			</th>
			<th field="job_no" width="147" sortable="true">
				shipment_no
			</th>
			<th field="shipper_ref" width="105">
				客户单号
			</th>
			<th field="box_info" width="150" data-options="formatter:box_info_for">
				箱型箱量
			</th>      
			<th field="created_by" width="150">
				创建人
			</th>
			<th field="created_time" width="150">
				创建时间
			</th>
			<th field="updated_by" width="150">
				更新人
			</th>
			<th field="updated_time" width="150">
				更新时间
			</th>
            </tr>
        </thead>
</table>

   <div id="tb" style="padding:3px;">
        <table>
		<tr>
        <td>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="del();"><?= lang('delete');?></a>

        </td>
        </tr>
		</table>
         
    </div>
