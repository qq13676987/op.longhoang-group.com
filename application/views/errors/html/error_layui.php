<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>error</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css" media="all">
    <style>
        .box{
            width: 100%;
            position: absolute;
            top: 30%;
            text-align: center;
        }
    </style>
</head>
<body>
<?php
if (!isset($icon) || $icon==''){
    $icon = "layui-icon-password";
}
?>
<div class="box">
    <i class="layui-icon <?=$icon;?>" style="font-size: 80px; color: #ff460e;"></i>
    <div style="font-size: 16px; color: #fb4712; line-height: 25px;">
        <i class="layui-icon layui-icon-speaker" style="font-size: 20px; color: #ff460e;"></i>
        <?php echo $msg;?>
    </div>
</div>
</body>
</html>