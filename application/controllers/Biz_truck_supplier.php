<?php

class biz_truck_supplier extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_truck_supplier_model');
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "user_id") continue;
            array_push($this->field_edit, $v);
        }
    }

    public $field_edit = array();

    public $field_all = array(
        array("id", "ID", "80", "1"),
        array("truck_company", "truck_company", "100", "2"),
        array("truck_address", "truck_address", "120", "3"),
        array("truck_contact", "truck_contact", "120", "3"),
        array("truck_telephone", "truck_telephone", "120", "3"),
        array("truck_remark", "truck_remark", "120", "3"),
        array("GP20", "GP20", "120", "3"),
        array("GP40", "GP40", "120", "3"),
    );

    public function get_option($client_code = ''){
        //根据user_id版本暂时废弃,改为client_code版本
        // $data = $this->biz_truck_supplier_model->get_option("user_id = '" . $this->session->userdata('id') . "'");
        $data = $this->biz_truck_supplier_model->get_option("(client_code = '" . $client_code . "')");
        $rows = array();
        foreach ($data as $row){
            $rows[] = $row;
        }
        echo json_encode($rows);
    }
}