<style>
	.hidden_invoice_input{
		display: none;
	}
</style>
<script>
    $(function(){
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
			remoteSort:false,
			onSelect:function(index ,row){
				get_statistics();
			},
			onUnselect:function(index ,row){
				get_statistics();
			},
			onSelectAll:function(index ,row){
				get_statistics();
			},
			onUnselectAll:function(index ,row){
				get_statistics();
			},
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    })
	function get_auto_remark(fn) {
		$.ajax({
			type: 'GET',
			url: remark_url,
			dataType:'json',
			success: function (res) {
				fn(res);
			},error:function (e) {
				$.messager.alert('<?= lang('提示');?>', e.responseText);
			}
		})
	}
	function jump_client(e){
		var client_code = $(e).attr('client_code');
		window.open("/biz_client/edit_by_client/" + client_code);
	}
	function invoice_type_select(rec) {
		if(rec.value == '999'){//形式发票
			$('.hidden_invoice_input.invoice_type999').css('display', 'table-cell');
			$('#invoice_payment_bank').combobox({'required' : true});

			$('.hidden_invoice_input.invoice_type1001').css('display', 'none');
			$('#use_type').combobox({'required' : false});
		}else if(rec.value == '1001'){//无发票
			$('.hidden_invoice_input.invoice_type1001').css('display', 'table-cell');
			$('#use_type').combobox({'required' : true});

			$('.hidden_invoice_input.invoice_type999').css('display', 'none');
			$('#invoice_payment_bank').combobox({'required' : false});
		}else{
			$('.hidden_invoice_input.invoice_type999').css('display', 'none');
			$('#invoice_payment_bank').combobox({'required' : false});

			$('.hidden_invoice_input.invoice_type1001').css('display', 'none');
			$('#use_type').combobox({'required' : false});
		}
		var invoice_amount = $('#invoice_amount').numberbox('getValue');
		var invoice_company_config = {};
		if(rec.value == '1001'){
			invoice_company_config.required = false;
		}else{
			invoice_company_config.required = true;
		}
		if(invoice_amount > 0){
			invoice_company_config.url = '';
		}
		$('#invoice_company').combobox(invoice_company_config);

		company_type_remark();
	}
	function company_type_remark(is_msg = false) {
		if(remark_url == '') return false;

		var invoice_company = $('#invoice_company').combobox('getValue');
		var invoice_type = $('#invoice_type').combobox('getValue');
		$('#remark').attr('readonly', true);

		function msg(is_msg, result, msg_title) {
			if(is_msg) return $.messager.alert('<?= lang('提示');?>', msg_title);
			return result;
		}

		//开票抬头必须选择
		if(invoice_company === ''){
			return msg(is_msg, false, '<?= lang('请选择开票抬头后再试');?>');
		}
		//开票类型必须选择
		if(invoice_type === ''){
			return msg(is_msg, false, '<?= lang('请选择开票类型后再试');?>');
		}

		remark_url += '&invoice_type=' + invoice_type + '&invoice_company=' + invoice_company;

		get_auto_remark(function (res) {
			//2023-10-12 如果是全电时, 格式必须固定
			var remark = res.data.remark;

			var currency = $('#invoice_bill_currency').val();

			if(currency === 'USD'){
				var amount = $('#invoice_bill_amount').val();
				remark = 'USD' + (amount / 100) + "\n本发票只接受美元付款USD开票汇率:" + res.data.invoice_ex_rate + "\n" + remark;
			}

			$('#remark').removeAttr('readonly');
			$('#remark').val(remark);
		});
	}
	function invoice_for(value,row,index){
		if(row.invoice_id > 0){
			let str = '';
			str += `<span style='color: #0f9d58'>发票号:${row.invoice_no}</span>`
			str += `<br><span style='color: #0c7cd5'>发票类型:D/N</span>`
			str += `<br><span style='color: #00F7DE'>开票抬头:${row.invoice_company}</span>`
			str += `<br><span style='color: #00FF00'>开票时间:${row.invoice_date}</span>`
			str += `<br><span style='color: #3f464f'>开票人:${row.invoice_user}</span>`
			return str;
		}
		return '';
	}
	function payment_for(value,row,index){
		if(row.payment_id > 0){
			let str = '';
			str += `<span style='color: #0f9d58'>核销流水号:${row.payment_no}</span>`
			str += `<br><span style='color: #00F7DE'>核销结算银行:${row.payment_bank}</span>`
			str += `<br><span style='color: #00FF00'>核销时间:${row.payment_date}</span>`
			str += `<br><span style='color: #3f464f'>核销人:${row.payment_user}</span>`
			return str;
		}
		return '';
	}
	function get_statistics(){
		var select = $('#tt').datagrid('getSelections');
		var ids = [];
		var row = {};
		$('#statistics_fm').form('clear');

		if(select.length > 0){
			var title = select[0].type;
			$.each(select, function (index, item) {
				ids.push(item.id);
				var amount = parseInt(Math.round(Number(item['amount']) * 100));
				if(item.type == 'cost'){
					amount = - amount;
				}
				if(row[item.currency + "_amount"] == undefined) row[item.currency + "_amount"] = 0;
				if(row[item.currency + "_write_off_amount"] == undefined) row[item.currency + "_write_off_amount"] = 0;
				if(row[item.currency + "_not_write_off_amount"] == undefined) row[item.currency + "_not_write_off_amount"] = 0;

				row[item.currency + "_amount"] += amount != null ? amount : 0;
				if(item['payment_id'] != 0) row[item.currency + "_write_off_amount"] += amount;
				else row[item.currency + "_not_write_off_amount"] += amount;
			});
		}

		ids = ids.join(',');
		$('#cx input[name="ids"]').val(ids);
		$.each(row, function(i, v){
			$('#' + i).numberbox('setValue', v / 100);
		});
	}
</script>
<script>

	//核销 核销对帐号， 发票号
	var remark_url = "";//备注获取的链接
	var invoice_remark = "";//用于验证的备注
	var invoice_info = {};
	function invoice() {
		var row = $('#tt').datagrid('getSelections');
		var title = 'ALL';
		var result = '';
		if(row.length > 0){
			var ids = [];
			var from = row[0]['from'];
			var invoice_id = row[0]['invoice_id'];
			var client_code = row[0]['client_code'];
			var type = row[0]['type'];
			var currency = [];
			var vat = row[0]['vat'];
			var local_amount = 0;
			var amount = 0;
			var id_type = [];
			var id_no = [];
			$.each(row,function (index,item) {
				if(invoice_id != item['invoice_id']){//如果有不同的开票ID,提示错误
					result = 'Can not select invoiced item';
					return;
				}
				if(client_code != item['client_code']){
					result = 'Client is different';
					return;
				}
				if(from != item['from']){
					result = 'from is different';
					return;
				}
				if(item['from'] == '<?=get_system_type()?>'){
					result = 'not current from';
					return;
				}
				// if(currency != item['currency']){
				//     result = '币种不同,无法开票';
				//     return;
				// }
				if(vat != item['vat']){
					result = 'Vat is different';
					return;
				}
				if($.inArray(item['currency'], currency) == -1){//获取币种
					currency.push(item['currency']);
				}
				if($.inArray(item['id_type'], id_type) == -1){//获取币种
					id_type.push(item['id_type']);
				}
				if($.inArray(item['id_no'], id_no) == -1){//获取币种
					id_no.push(item['id_no']);
				}
				if(item['currency'] !== '<?= LOCAL_CURRENCY;?>' && item['local_amount'] === null){
					result = 'Exchange currency fail';
					return;
				}else{
					local_amount += parseFloat(item['local_amount']);
				}
				if(item.type == 'cost'){
					amount -= item['amount'] * 100;
				}else{
					amount += item['amount'] * 100;
				}
				ids.push(item['id']);
			});
			$('#jump_client').attr('client_code', client_code);
			if(result != ''){
				$.messager.alert('<?= lang('error')?>', result);
				return;
			}
			if(type == 'cost'){
				local_amount = -local_amount;
			}
			var lock = 0;
			invoice_info = {};
			$('#invoice_form').form('clear');
			if(invoice_id != 0){
				var get_invoice = $.ajax({
					type: 'GET',
					url: '/biz_bill/get_invoice/' + invoice_id,
					dataType: 'json',
					success: function (res) {
						if(res.data.invoice_date == '0000-00-00')res.data.invoice_date = null;
						$('#invoice_form').form('load', res.data);
						lock = res.data.lock;
					}
				})
			}else{
				get_invoice = '';
			}
			$.when(get_invoice ).done(function ( v1) {
				if(lock != 0 && lock != 10){
					$.messager.alert('Tips', 'Please cancel the invoice first');
					return false;
				}
				if(currency.length == 1){
					var remark_url = '/biz_bill/auto_remark?id_type=' + id_type + '&id_no=' + id_no;
					if(id_type.length > 1 || id_no.length > 1) remark_url = '/biz_bill/auto_remark';
					$.ajax({
						type: 'GET',
						url: remark_url,
						success: function (res_json) {
							var res;
							try{
								res = $.parseJSON(res_json);
							}catch(e){

							}
							if(res == undefined){
								$.messager.alert('Tips', 'error,Contact Admin!');
							}else{
								//如果是美元,额外拼接一部分
								var remark = res.data.remark;
								if(currency[0] == 'USD'){
									remark = 'USD' + (amount / 100) + "\n exrate:" + res.data.invoice_ex_rate + "\n" + remark;
									if(type == 'cost'){
										local_amount = -(amount / 100 * res.data.invoice_ex_rate);
									}else{
										local_amount = amount / 100 * res.data.invoice_ex_rate;
									}
									$('#invoice_amount').numberbox('setValue', local_amount);
								}
								$('#remark').val(remark);
							}
						}
					})
				}

				ids = ids.join(',');
				var invoice_form_data = {
					ids:ids,
					title:title,
					invoice_id:invoice_id,
					buyer_client_code:'',
					buyer_company:'',
					buyer_taxpayer_id:'',
					currency: 'CNY',
					vat:vat,
				};
				$('#invoice_form').form('load', invoice_form_data);
				$('#buyer_company').combobox('setValue', row[0]['client_code']).combobox('reload');
				if($('#invoice_amount').numberbox('getValue') == ''){
					$('#invoice_amount').numberbox('setValue', local_amount);
				}

				$('#invoice').dialog('open').dialog('center').dialog('setTitle', '<?= lang('invoice');?>');
			});

		}else{
			$.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
		}

	}
	function auto_create(){
		$.post("/biz_bill/auto_create_third_party", {},
				function(res){
					if(res.code == 1){
						$('#invoice_no').textbox('setValue',res.invoice_no)
					}
				}, "json");
	}

	function invoice_save() {
		ajaxLoading();
		$('#invoice').dialog('close');
		$('#invoice_form').form('submit', {
			url: '/biz_bill/batch_invoice_third_party',
			onSubmit: function () {
				var validate = $(this).form('validate');
				if(!validate){
					ajaxLoadEnd();
					$('#invoice').dialog('open');
				}
				return validate;
			},
			success: function (res_json) {
				// $('#dlg').addClass('hide');
				var res;
				try{
					res = $.parseJSON(res_json);
				}catch(e){
				}
				if(res == undefined){
					$.messager.alert('Tips', '发生错误');
				}else{
					if(res.code == 0){
						$('#tt').datagrid('reload');
					}else{
						$('#invoice').dialog('open');
					}
					$.messager.alert('Tips', res.msg);
				}
				ajaxLoadEnd();
			},
		});
	}

	/**
	 * 取消开票
	 */
	function qx_invoice() {
		var row = $('#tt').datagrid('getSelections');
		if(row.length > 0){
			$.messager.prompt('Tips', '确认删除?请输入delete进行确认', function (r) {
				if(r === 'delete'){
					var invoice_id = row[0]['invoice_id'];
					var msg = "";
					$.each(row,function (index,item) {
						if(item['invoice_id'] != invoice_id){
							msg = '请勿选择不同开票信息';
							return;
						}
					});
					if(msg !== ""){
						$.messager.alert('<?= lang('error')?>', msg);
						return;
					}
					ajaxLoading()
					$.ajax({
						type: 'POST',
						url: '/biz_bill/batch_del_invoice_third_party',
						data:{
							// title: title,
							invoice_id: invoice_id,
						},
						dataType: 'json',
						success: function (res) {
							ajaxLoadEnd()
							$.messager.alert('Tips', res.msg);
							if(res.code == 0){
								$('#tt').datagrid('reload');
							}
						}
					});
				}
			});
		}else{
			$.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
		}
	}

	function write_off() {
		var row = $('#tt').datagrid('getSelections');
		if(row.length > 0){
			$('#write_off_form').form('clear')
			if(row[0].type == 'cost'){
				$('.actual_amount').text('<?= lang('actual_amount2');?>');
			}else if(row[0].type == 'sell'){
				$('.actual_amount').text('<?= lang('actual_amount');?>');
			}
			var ids = [],currency = [],local_amount = 0,amount = 0,this_currency,payment_amount = 0,other_side_company = [], other_side_company_data = [], msg ="";
			var company = row[0].client_code
			var from = row[0].from
			$.each(row,function (index,item) {
				var this_local_amount = item.local_amount;
				var this_amount = item.amount;
				if(item.type == 'cost'){
					this_local_amount = -this_local_amount;
					this_amount = -this_amount;
				}
				local_amount += parseInt(this_local_amount * 100);
				amount += parseInt(this_amount * 100);
				ids.push(item['id']);
				if($.inArray(item['currency'], currency) === -1)currency.push(item['currency']);
				if($.inArray(item['client_code'], other_side_company) === -1){
					other_side_company.push(item['client_code']);
					other_side_company_data.push({client_code:item['client_code'], company_name:item['client_code_name']})
				}//填充对方客户名称
				if(item.payment_id != 0){
					if(item.payment_id == '-99'){
						msg = "已申请费用";
					}else{
						msg = '存在已核销的账单'
					}
					return false;
				}
				if(item['client_code'] != company) msg = '客户名称不一致';
				if(item['from'] != from) msg = 'from不一致';
				if(item['from'] == '<?=get_system_type()?>') msg = 'not current from'
			});
			if(msg !== ""){
				$.messager.alert('<?= lang('Tips')?>', msg);
				return;
			}
			$('#payment_bank').combobox('reload','/bsc_dict/get_client_account1/'+company)
			$('#write_off_href').attr('href','/biz_client/edit/'+company)
			if(currency.length == 1){//只有一种的话,自动取第一种
				this_currency = currency[0];
				if(this_currency == 'USD') $('#payment_currency').combobox({readonly: false});
				else if(this_currency == 'CNY')$('#payment_currency').combobox({readonly: true});
			}else if(currency.length >= 2){//超过一种时,默认为CNY
				this_currency = 'CNY';
				$('#payment_currency').combobox({readonly: true});
			}else{
				$('#payment_currency').combobox({readonly: false});
				$.messager.alert('<?= lang('Tips')?>','发生异常');
				return;
			}
			if(this_currency == 'CNY'){
				payment_amount = local_amount / 100;
			}else{
				payment_amount = amount / 100;
			}

			var title = '<?= lang('write_off');?>';
			var write_off_data = {};
			write_off_data.ids = ids.join(',');
			write_off_data.currency = this_currency;
			write_off_data.payment_amount = payment_amount;
			write_off_data.other_side_company = other_side_company[0];
			write_off_data.payment_no = '';//2022-05-10 自动清空凭据号
			$('#payment_local_amount_hidden').val(local_amount / 100);
			$('#payment_amount_hidden').val(amount / 100);

			$('#other_side_company').combobox('loadData', other_side_company_data);

			$('#write_off_form').form('load', write_off_data);

			$('#write_off').dialog('open').dialog('center').dialog('setTitle', title);
		}else{
			$.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
		}
	}

	function write_off_save() {
		$('#write_off').dialog('close');
		var payment_no = $('#payment_no').textbox('getValue').trim();
		if(payment_no == ''){
			$.messager.confirm('<?= lang('提示');?>', '是否确定不填流水号?', function(r){
				if(r){
					write_off_form_submit();
				}else{
					$('#write_off').dialog('open');
					ajaxLoadEnd();
				}
			});
		}else{
			write_off_form_submit();
		}


	}

	function write_off_form_submit(){
		ajaxLoading();
		$('#write_off_form').form('submit', {
			url: '/biz_bill/batch_write_off_third_party',
			onSubmit: function () {
				var validate = $(this).form('validate');
				if(!validate){
					ajaxLoadEnd();
					$('#write_off').dialog('open');
				}
				return validate;
			},
			success: function (result) {
				// $('#dlg').addClass('hide');
				var data;
				try{
					data = $.parseJSON(result);
				}catch(e){
				}
				if(data == undefined){
					$.messager.alert('Tips', '发生错误');
				}else{
					$.messager.alert('Tips', data.msg);
					if(data.code == 0){

						$('#tt').datagrid('reload');
					}else{
						$('#write_off').dialog('open');
					}
				}
				ajaxLoadEnd();
			}
		});
	}

	function qx_write_off() {
		//取消的必须是lock为2的,
		var row = $('#tt').datagrid('getSelections');
		$.messager.prompt('Tips', '确认删除?请输入delete进行确认', function (r) {
			if(r === 'delete'){
				if(row.length > 0){
					var payment_id = 0;
					var msg = "";
					$.each(row,function (index,item) {
						payment_id = item.payment_id;
						if(item.payment_id == 0){
							msg = '该账单未核销,撤销失败';
							return false;
						}
					});
					if(msg !== ""){
						$.messager.alert('Tips', msg);
						return false;
					}
					//选多个
					ajaxLoading();
					$.ajax({
						type: 'POST',
						url: '/biz_bill/batch_del_write_off_third_party',
						data:{
							payment_id:payment_id,
						},
						success: function (res_json) {
							// $('#dlg').addClass('hide');
							var res;
							try{
								res = $.parseJSON(res_json);
							}catch(e){
							}
							if(res == undefined){
								$.messager.alert('Tips', '发生错误');
							}else{
								$.messager.alert('Tips', res.msg);
								if(res.code == 0){
									$('#tt').datagrid('reload');
								}
							}
							ajaxLoadEnd();
						}
					});

				}else{
					$.messager.alert('<?= lang('Tips')?>','<?= lang('No columns selected')?>');
				}
			}
		});
	}
</script>
<table id="tt"
	   rownumbers="false" pagination="true" idField="id"
	   pagesize="30" toolbar="#tb" singleSelect="false" nowrap="false">
	<thead>
	<tr>
		<th field="ck" checkbox="true" >check all</th>
		<th field="from" sortable="true" width="60" >from</th>
		<th field="receive_party_name" sortable="true" width="260" >Receive party</th>
		<th field="agent_party_name" sortable="true" width="260" >Agent party</th>
		<th field="type" width="80"  sortable="true" >sell/cost</th>
		<th field="charge" width="120" sortable="true"  >charge_name</th>
		<th field="unit_price" width="120" sortable="true" >unit price</th>
		<th field="number" width="120"  sortable="true" >number</th>
		<th field="amount" width="120" sortable="true">amount</th>
		<th field="currency" width="120" sortable="true" >currency</th>
		<th field="vat" width="60"  sortable="true">vat</th>
		<th field="client_code_name" width="260"  sortable="true">Client code</th>
		<th field="job_no" width="120" sortable="true">JOB NO</th>
		<th field="invoice" width="150" formatter="invoice_for" sortable="true">invoice</th>
		<th field="payment" width="150" formatter="payment_for" sortable="true">payment</th>
		<th field="created_by_name" width="80" sortable="true">created_by</th>
		<th field="created_time" width="140" sortable="true">created_time</th>
		<th field="updated_by_name" width="80" sortable="true">updated_by</th>
		<th field="updated_time" width="140" sortable="true">updated_time</th>
	</tr>
	</thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="open_chaxun()"><?= lang('query');?></a>
				<a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="del()"><?= lang('delete');?></a>
				<?php if(in_array('finance',get_session('user_role'))):?>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-write_off" plain="true"  onclick="invoice()"><?= lang('invoice');?></a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-write_off" plain="true" onclick="qx_invoice()"><?= lang('Cancel invoice');?></a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-write_off" plain="true" onclick="write_off()"><?= lang('write_off');?></a>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-write_off" plain="true" onclick="qx_write_off()"><?= lang('Cancel write off');?></a>
				<?php endif;?>
            </td>
            <td width='80'></td>
        </tr>
    </table>
	<form id="statistics_fm">
		<table>
			<?php for($i = 0; $i < sizeof($currency_table_fields); $i++){
				$this_currency = $currency_table_fields[$i];
				?>
				<tr>
					<td><?= $this_currency;?>:</td>
					<td><?= lang('amount');?><input class="easyui-numberbox" precision="2" id="<?= $this_currency;?>_amount" disabled value="0"></td>
					<td><?= lang('write_off_amount');?><input class="easyui-numberbox" precision="2" id="<?= $this_currency;?>_write_off_amount" disabled value="0"></td>
					<td><?= lang('not_write_off_amount');?><input class="easyui-numberbox" precision="2" id="<?= $this_currency;?>_not_write_off_amount" disabled value="0"></td>
				</tr>
			<?php } ?>
		</table>
	</form>
</div>
<div id="invoice" class="easyui-dialog" style="width:800px;" data-options="closed:true,modal:true,border:'thin',buttons:'#invoice-buttons'">
	<form id="invoice_form" method="post" novalidate style="margin:0;padding:10px 10px">
		<input type="hidden" name="invoice_id">
		<input type="hidden" name="ids">
		<!--        <input type="hidden" name="title">-->
		<table style="border-collapse:collapse;">
			<!--            <tr>-->
			<!--                <td style="color:red;font-weight:bold;" colspan="3">--><?//= lang('申请开票');?><!--:&nbsp;&nbsp;<a href="javascript:void(0);" onclick="jump_client(this)" id="jump_client">--><?//= lang('点击维护纳税人');?><!--</a><br><br></td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>-->
			<!--            </tr>-->
			<tr>
				<!--<td><?= lang('invoice_type');?></td>-->
				<!--<td>-->
				<!--    <select name="invoice_type" editable="false" class="easyui-combobox" style="width:111px;"  data-options="-->
				<!--        required:true,-->
				<!--        valueField:'value',-->
				<!--        textField:'name',-->
				<!--        url:'/bsc_dict/get_option/invoice_type',-->
				<!--        onSelect: function(rec){-->
				<!--            invoice_type_select(rec);-->
				<!--        }-->
				<!--    ">-->
				<!--    </select>-->
				<!--</td>-->
				<td><?= lang('buyer_company');?></td>
				<td>
					<select id="buyer_company" readonly class="easyui-combobox"  style="width:188px;" data-options="
                        valueField:'client_code',
                        textField:'company_name',
                        url:'/biz_client/get_client/?q_field=client_code',
                        mode: 'remote',
                        onBeforeLoad:function(param){
                            if($(this).combobox('getValue') == '')return false;
                            if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                        },
                        onLoadSuccess:function(){
                            var val = $(this).combobox('getValue');
                            $(this).combobox('clear').combobox('select', val);
                        },
                        onSelect: function(res){
                            $('#invoice_form input[name=\'buyer_client_code\']').val(res.client_code);
                            $('#invoice_form input[name=\'buyer_company\']').val(res.taxpayer_name);
                            $('#invoice_form input[name=\'buyer_taxpayer_id\']').val(res.taxpayer_id);
                        },
                    ">
					</select>
					<input type="hidden" name="buyer_client_code">
					<input type="hidden" name="buyer_company">
					<input type="hidden" name="buyer_taxpayer_id">
				</td>
				<td> </td>
				<td>

				</td>
			</tr>
			<tr>
				<!--                <td>--><?//= lang('vat');?><!--</td>-->
				<!--                <td>-->
				<!--                    <input type="text" name="vat" readonly class="easyui-textbox" style="width:111px;">-->
				<!--                </td>-->
				<td><?= lang('invoice_amount');?></td>
				<td><input name="invoice_amount" id="invoice_amount" readonly precision="2" class="easyui-numberbox" style="width:188px;"></td>
				<td class="hidden_invoice_input invoice_type999"><?= lang('payment_bank');?></td>
				<td class="hidden_invoice_input invoice_type999">
					<select name="payment_bank" id="invoice_payment_bank" class="easyui-combobox" style="width: 90px" data-options="
                        valueField:'name',
                        textField:'name',
                        url:'/bsc_dict/get_option/payment_bank',
                    ">
					</select>
				</td>
				<!--<td class="hidden_invoice_input invoice_type1001"><?= lang('use_type');?></td>-->
				<!--<td class="hidden_invoice_input invoice_type1001">-->
				<!--    <select name="use_type" id="use_type" class="easyui-combobox" style="width: 90px" data-options="-->
				<!--        valueField:'name',-->
				<!--        textField:'name',-->
				<!--        url:'/bsc_dict/get_option/use_type',-->
				<!--    ">-->
				<!--    </select>-->
				<!--</td>-->
			</tr>
			<tr class="invoice_apply">
				<td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
			</tr>
			<tr> <td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>
			</tr>
			<!--            <tr>-->
			<!--                <td style="color:red;font-weight:bold;">--><?//= lang('执行开票'); ?><!--:<br><br></td>-->
			<!--                <td>-->
			<!--                </td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td><td>&nbsp;&nbsp;</td>-->
			<!--                <br><br>-->
			<!--            </tr>-->
			<tr>
				<td><?= lang('invoice_no');?></td>
				<td><input name="invoice_no" id="invoice_no" class="easyui-textbox" style="width:111px;"><a href="javascript:;" onclick="auto_create()"><?=lang('auto create')?></a></td>
				<td><?= lang('invoice_date');?></td>
				<td><input name="invoice_date" required="required" id="invoice_date" class="easyui-datebox" style="width:188px;"></td>
				<!--<td><?= lang('invoice_company');?></td>-->
				<!--<td>-->
				<!--    <select name="invoice_company" id="invoice_company" class="easyui-combobox" style="width:111px;" data-options="-->
				<!--        required: true,-->
				<!--        editable:false,-->
				<!--        valueField:'company_name',-->
				<!--        textField:'company_name',-->
				<!--    ">-->
				<!--    </select>-->
				<!--</td>-->
			</tr>
			<tr>
				<!--                <td>--><?//= lang('disabled');?><!--</td>-->
				<!--                <td><input name="disabled" id="disabled" class="easyui-textbox" style="width:111px;"></td>-->
				<td><?= lang('remark');?></td>
				<td colspan="4"><textarea name="remark" id="remark" style="width:460px;height: 50px"></textarea></td>
			</tr>
		</table>
	</form>
	<div id="invoice-buttons">
		<a id="invoice_save_button" href="javascript:invoice_save()" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px"><?= lang('save');?></a>
	</div>
</div>

<div id="write_off" class="easyui-dialog" style="width:900px;" data-options="closed:true,modal:true,border:'thin',buttons:'#write_off-buttons'">
	<form id="write_off_form" method="post" novalidate style="margin:0;padding:10px 10px">
		<input type="hidden" name="ids">
		<input type="hidden" id="payment_local_amount_hidden">
		<input type="hidden" id="payment_amount_hidden">
		<table>
			<tr>
				<td><?= lang('payment_date');?></td>
				<td><input name="payment_date" required id="payment_date" class="easyui-datebox" style="width: 150px"></td>
				<td class="payment_amount"><?= lang('payment_amount');?></td>
				<td><input name="payment_amount" required id="payment_amount" class="easyui-numberbox" precision="2" style="width: 150px" readonly="readonly"></td>
				<td><?= lang('核销币种');?></td>
				<td>
					<select class="easyui-combobox" name="currency" id="payment_currency" style="width: 150px" data-options="
                        url:'/biz_bill_currency/get_currency/sell',
                        editable:false,
                        valueField:'name',
                        textField:'name',
                        required:true,
                        onSelect:function(rec){
                            //这个代码如果后面加入其它币种不会兼容的 只针对当前的 CNY 和USD来
                            if(rec.name == 'CNY') $('#payment_amount').numberbox('setValue', $('#payment_local_amount_hidden').val());
                            else $('#payment_amount').numberbox('setValue', $('#payment_amount_hidden').val());
                        }
                    "></select>
				</td>
				<td><?= lang('对方抬头');?></td>
				<td>
					<select class="easyui-combobox" name="other_side_company" id="other_side_company" style="width: 150px" data-options="
                        editable:false,
                        valueField:'client_code',
                        textField:'company_name',
                        required:true,
                    "></select>
				</td>
			</tr>
			<tr>
				<td><?= lang('payment_way');?></td>
				<td>
					<select name="payment_way" required id="payment_way" class="easyui-combobox" style="width: 150px" data-options="
                        valueField:'value',
                        textField:'value',
                        url:'/bsc_dict/get_option/payment_way',
                    ">
					</select>
				</td>
				<td><?= lang('payment_bank');?></td>
				<td>
					<select class="easyui-combobox" name="payment_bank"
							id="payment_bank" style="width: 150px" data-options="
                                    valueField : 'title_cn_currency',
                                    textField : 'title_cn_currency',
                                    editable:false,
                                    required:true,
                                    onSelect:function(rec){
                                        $('#payment_bank_id').val(rec.id)
                                    }
                                ">
					</select>
					<input type="hidden" name="payment_bank_id" id="payment_bank_id">
				</td>
				<td><?= lang('payment_no');?></td>
				<td><input name="payment_no" id="payment_no" class="easyui-textbox" style="width: 150px"></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td colspan="4"> <span style="color: red"> 如果没有银行账号，请到往来单位绑定归属公司和银行账号</span>&emsp;<a href="#" target="_blank" id="write_off_href">跳转</a></td>
			</tr>
		</table>
	</form>
	<div id="write_off-buttons">
		<a href="javascript:write_off_save()" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px">保存</a>
	</div>
</div>
<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
	<form id="search_fm">
		<input type="radio" name="code_type" value="1" style="margin-left: 45px"> as receive party &emsp;
		<input type="radio" name="code_type" value="2"> as agent party
	</form>
    <table>
        <tr>
            <td>
                <?= lang('query');?> 1
            </td>
            <td align="right">
                <select name="f1"   id="f1"  class="easyui-combobox"  required="true" style="width:150px;" data-options=" onSelect:function(v){
                change_person(v,1)

        }">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$rs[1]'>$rs[0]</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1"  id="s1"  class="easyui-combobox"  required="true" style="width:100px;" >
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;<span class="v1">
                    <input id="v1" class="easyui-textbox "  style="width:120px;">
                </span>



            </td>
        </tr>
        <tr>
            <td>
                <?= lang('query');?> 2
            </td>
            <td align="right">
                <select name="f2"  id="f2"  class="easyui-combobox"  required="true" style="width:150px;" data-options=" onSelect:function(v){
                change_person(v,2)

        }">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$rs[1]'>$rs[0]</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2"  id="s2"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <span class="v2">
                    <input id="v2" class="easyui-textbox"  style="width:120px;">
                </span>

            </td>
        </tr>

        <tr>
            <td>
                <?= lang('query');?> 3
            </td>
            <td align="right">
                <select name="f3"  id="f3"  class="easyui-combobox"  required="true" style="width:150px;"data-options=" onSelect:function(v){
                change_person(v,3)

        }">
                    <?php
                    foreach ($f as $rs){
                        echo "<option value='$rs[1]'>$rs[0]</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s3"  id="s3"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <span class="v3">
                    <input id="v3" class="easyui-textbox"  style="width:120px;">
                </span>

            </td>
        </tr>

    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;"><?= lang('search');?></a>
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:do_reset()" style="width:200px;height:30px;"><?= lang('reset');?></a>
</div>
<script type="text/javascript">
	$(function(){
		open_chaxun()
	})
	function del(){
		var row = $('#tt').datagrid('getSelected');
		if(row){
			$.messager.confirm('Confirm', '<?=lang('确认删除?')?>', function(r){
				if (r){
					$.post("/biz_bill/del_third_party/"+row.id, {},
							function(res){
								$.messager.alert('Tips',res.msg)
								if(res.code == 1){
									$('#tt').datagrid('reload')
									$('#tt').datagrid('clearSelections')
								}
							}, "json");
				}
			});
		}else{
			$.messager.alert('Tips','<?=lang('请先选择一条数据')?>')
		}
	}
    let v1status = '';
    let v2status = '';
    let v3status = '';
    function open_chaxun(){
        let v1 = $('#f1').combobox('getValue')
        let v2 = $('#f2').combobox('getValue')
        let v3 = $('#f3').combobox('getValue')
        change_person({value:v1},1)
        change_person({value:v2},2)
        change_person({value:v3},3)
        $('#chaxun').window('open')
    }
	function change_person(v,index){
		let ele = 'v'+index
		if(v.value == 'p.payment_date' ||  v.value == 'i.invoice_date'){
			$('.'+ele).html(`<input id='${ele}' class='easyui-datebox'  style='width:120px;'>`)
			$('#'+ele).datebox()
			if(index == 1){
				v1status = '1'
			}else if(index == 2){
				v2status = '1'
			}else if(index == 3){
				v3status = '1'
			}
		}else if(v.value == 'p.payment_user_id' ||  v.value == 'i.invoice_user_id'){
			$('.'+ele).html(`<input id='${ele}' class='easyui-combobox'  style='width:120px;'>`)
			$('#'+ele).combobox({
				valueField: 'id',
				textField: 'otherName',
				url: '/bsc_user/get_data_role',
			})
			if(index == 1){
				v1status = '2'
			}else if(index == 2){
				v2status = '2'
			}else if(index == 3){
				v3status = '2'
			}
		}else if(v.value == 'i.invoice_type'){
			$('.'+ele).html(`<input id='${ele}' class='easyui-combobox'  style='width:120px;'>`)
			$('#'+ele).combobox({
				valueField:'value',
				textField:'name',
				url:'/bsc_dict/get_option/invoice_type',
			})
			if(index == 1){
				v1status = '2'
			}else if(index == 2){
				v2status = '2'
			}else if(index == 3){
				v3status = '2'
			}
		}else if(v.value == 'currency'){
			$('.'+ele).html(`<input id='${ele}' class='easyui-combobox'  style='width:120px;'>`)
			$('#'+ele).combobox({
				valueField:'name',
				textField:'name',
				url:'/bsc_dict/get_option/currency',
			})
			if(index == 1){
				v1status = '2'
			}else if(index == 2){
				v2status = '2'
			}else if(index == 3){
				v3status = '2'
			}
		}else if(v.value == 'type'){
			$('.'+ele).html(`<input id='${ele}' class='easyui-combobox'  style='width:120px;'>`)
			$('#'+ele).combobox({
				valueField:'name',
				textField:'name',
				data:[{name:'sell'},{name:'cost'}],
			})
			if(index == 1){
				v1status = '2'
			}else if(index == 2){
				v2status = '2'
			}else if(index == 3){
				v3status = '2'
			}
		}else if(v.value == 'charge_code'){
			$('.'+ele).html(`<input id='${ele}' class='easyui-combobox'  style='width:120px;'>`)
			$('#'+ele).combobox({
				valueField:'charge_code',
				textField:'charge_name_long',
				url:'/biz_charge/get_option',
			})
			if(index == 1){
				v1status = '2'
			}else if(index == 2){
				v2status = '2'
			}else if(index == 3){
				v3status = '2'
			}
		}else if(v.value == 'receive_party' || v.value == 'agent_party'){
			$('.'+ele).html(`<input id='${ele}' class='easyui-combobox'  style='width:120px;'>`)
			$('#'+ele).combobox({
				editable:false,
				valueField:'value',
				textField:'company_name',
				url:'/bsc_dict/get_branch_office',
			})
			if(index == 1){
				v1status = '2'
			}else if(index == 2){
				v2status = '2'
			}else if(index == 3){
				v3status = '2'
			}
		}else{
			$('.'+ele).html(`<input id='${ele}' class='easyui-textbox'  style='width:120px;'>`)
			$('#'+ele).textbox()
			if(index == 1){
				v1status = '3'
			}else if(index == 2){
				v2status = '3'
			}else if(index == 3){
				v3status = '3'
			}
		}
	}
	function doSearch(){
		let data = $('#search_fm').serializeArray();
		if(data.length == 0){
			$.messager.alert('Tips','please select "as receive party" or "as agentparty"')
			return
		}
		let v1 = '';
		let v2 = '';
		let v3 = '';
		if(v1status == '1'){
			v1 = $('#v1').datebox('getValue')
		}else if(v1status == '2'){
			v1 = $('#v1').combobox('getValue')
		}else if(v1status == '3'){
			v1 = $('#v1').textbox('getValue')
		}
		if(v2status == '1'){
			v2 = $('#v2').datebox('getValue')
		}else if(v2status == '2'){
			v2 = $('#v2').combobox('getValue')
		}else if(v2status == '3'){
			v2 = $('#v2').textbox('getValue')
		}
		if(v3status == '1'){
			v3 = $('#v3').datebox('getValue')
		}else if(v3status == '2'){
			v3 = $('#v3').combobox('getValue')
		}else if(v3status == '3'){
			v3 = $('#v3').textbox('getValue')
		}
		$('#tt').datagrid('clearSelections')
		$('#tt').datagrid({
			url: '/biz_bill/get_third_party_payment/',
			queryParams:{
				code_type:data[0].value,
				f1: $('#f1').combobox('getValue'),
				s1: $('#s1').combobox('getValue'),
				v1: v1 ,
				f2: $('#f2').combobox('getValue'),
				s2: $('#s2').combobox('getValue'),
				v2: v2 ,
				f3: $('#f3').combobox('getValue'),
				s3: $('#s3').combobox('getValue'),
				v3: v3 ,
			}
		});
		$('#chaxun').window('close');
	}

	function do_reset(){
		$('#search_fm').form('clear')
		if(v1status == '1'){
			$('#v1').datebox('clear')
		}else if(v1status == '2'){
			$('#v1').combobox('clear')
		}else if(v1status == '3'){
			$('#v1').textbox('clear')
		}
		if(v2status == '1'){
			$('#v2').datebox('clear')
		}else if(v2status == '2'){
			$('#v2').combobox('clear')
		}else if(v2status == '3'){
			$('#v2').textbox('clear')
		}
		if(v3status == '1'){
			$('#v3').datebox('clear')
		}else if(v3status == '2'){
			$('#v3').combobox('clear')
		}else if(v3status == '3'){
			$('#v3').textbox('clear')
		}
	}
</script>
