<?php


class bsc_message extends Common_Controller
{
    protected $is_load_session = false;
    public function __construct()
    {
        parent::__construct();
        //这里进行单独控制
        $method = $this->router->fetch_method();
        if(!in_array($method, array('create_message', 'message_form', 'logwing_ftp_read_out', 'ams_read_out'))){
            $this->load->library('session');
            $this->model =  Model('bsc_message_model');
        }
        //这些方法默认不加载session,不然会报错

        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }

    public $field_edit = array();

    public $field_all = array(
        array('id', 'id', '80', '1', 'sortable="true"'),
        array('receiver', 'receiver', '100', '6', 'sortable="true"'),
        array('file_path', 'file_path', '0', '3', 'editor="textbox"'),
        array('type', 'type', '100', '4', 'editor="textbox"'),
        array('consol_id', 'consol_id', '100', '5', 'editor="textbox"'),
        array('status', 'status', '100', '6', 'editor="textbox" data-options="formatter:status_for"'),
        array('created_by', 'created_by', '100', '6', 'editor="textbox"'),
        array('created_group', 'created_group', '0', '6', 'editor="textbox"'),
        array('created_time', 'created_time', '150', '6', 'sortable="true"'),
        array('updated_by', 'updated_by', '100', '6', 'sortable="true"'),
        array('updated_time', 'updated_time', '150', '6', 'sortable="true"'),

    );

    public function index(){
        $data = array();

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('bsc_message_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }
        // page account
        $rs = $this->sys_config_model->get_one('bsc_message_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }

        //需要做成新页面, 根据当前的 consol数据来显示当前的相关报文


        $this->load->view('head');
        $this->load->view('bsc/message/index_view', $data);
    }

    /**
     * 2023-04-10 新的edi页面
     */
    public function edi(){
        $data = array();

        $data['id_type'] = $id_type = getValue('id_type', '');
        $data['id_no'] = $id_no = (int)getValue('id_no', 0);

        $where = array();
        $file_name = "";
        if($id_type == 'consol'){
            $this_row = Model('biz_consol_model')->get_by_id($id_no);

            $file_name = "{$this_row['job_no']}";
            $where[] = "(FIND_IN_SET('{$this_row['creditor']}', bsc_message_match.booking_agent) or bsc_message_match.booking_agent = '')";
            $where[] = "(FIND_IN_SET('{$this_row['trans_carrier']}', bsc_message_match.trans_carrier) or bsc_message_match.trans_carrier = '')";
        }

        //如果没有匹配条件, 直接跳过
        $where[] = "bsc_message_match.id_type = '{$id_type}'";
        if(empty($where)) $where[] = "1 = 0";
        $where_str = join(' and ', $where);


        //这里加入了match表的匹配检测
        $sql = "SELECT bsc_message_match.id,bsc_message.id as message_id, bsc_message.status as message_status, bsc_message_match.is_diy_data" .
            ", bsc_message_match.insert_code, bsc_message_match.update_code, bsc_message_match.delete_code, bsc_message_match.send_type" .
            ", bsc_message_match.message_receiver_name,bsc_message_match.message_receiver, bsc_message_match.message_type, bsc_message_match.message_type_name" .
            ", bsc_message_match.trans_carrier, bsc_message_match.booking_agent" .
            ", bsc_message.send_time, bsc_message.status, bsc_message.file_path" .
            ", (select count(*) from bsc_message_send where bsc_message_send.message_id = bsc_message.id) as send_count" .
            " FROM bsc_message_match" .
            " LEFT JOIN bsc_message ON bsc_message.receiver = bsc_message_match.message_receiver and bsc_message.type = bsc_message_match.message_type AND bsc_message.id_no = '{$id_no}' AND bsc_message.id_type = bsc_message_match.id_type" .
            " WHERE {$where_str}";
        $rows = Model("m_model")->query_array($sql);
        $data['rows'] = array();
        foreach ($rows as $row){
            //获取最后一次发送的信息
            $row['return_msg'] = "";
            $row['send_by_name'] = "";
            if(!empty($row['message_id'])){
                $this->db->order_by('id', 'desc');
                $this->db->select("(select bsc_user.name from bsc_user where bsc_user.id = bsc_message_send.send_by) as send_by_name,return_msg");
                $message_send = Model('bsc_message_send_model')->get_where_one("message_id = {$row['message_id']}");
                if(!empty($message_send)){
                    $row['return_msg'] = $message_send['return_msg'];
                    $row['send_by_name'] = $message_send['send_by_name'];
                }
            }
            //文件名由 job_no 和
            if(!empty($row['file_path'])){
                $file_type = explode('.', $row['file_path']);
                $row['file_name'] = "{$file_name}." . $file_type[1];
            }
            //承运人转为简称
            $this->db->select("client_name");
            $temp = Model('biz_client_model')->get("client_code in ('" . join('\',\'', explode(',', $row['trans_carrier'])) . "')");
            $row['trans_carrier_name'] = join(',', array_column($temp, 'client_name'));

            //订舱代理转为 全称
            $this->db->select("company_name");
            $temp = Model('biz_client_model')->get("client_code in ('" . join('\',\'', explode(',', $row['booking_agent'])) . "')");
            $row['booking_agent_name'] = join(',', array_column($temp, 'company_name'));

            if(empty($row['send_time']) || $row['send_time'] === '0000-00-00 00:00:00') $row['send_time'] = "未发送";

            $data['rows'][] = $row;
        }

        $this->load->view('/bsc/message/edi_view', $data);
    }

    /**
     * 获取发送记录的数据
     */
    public function get_send_log(){
        $page = intval(postValue('page', 1));
        $rows = intval(postValue('rows', 30));
        $sort = postValue('sort', 'id');
        $order = postValue('order', 'desc');
        $message_id = getValue('id', '');
        $result = array();

        $where = array();

        $where[] = "bsc_message_send.message_id = '{$message_id}'";

        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $this->db->limit($rows, $offset);
        $this->db->select('bsc_message_send.id,bsc_message_send.return_msg, bsc_message_send.status, bsc_message_send.send_time' .
            ',(select name from bsc_user where bsc_user.id = bsc_message_send.send_by) as send_by_name' .
            '');
        $this->db->is_reset_select(false);//不重置查询
        $rs = Model("bsc_message_send_model")->get($where, $sort, $order);

        $this->db->clear_limit();//清理下limit
        $result['total'] = $this->db->count_all_results('');

        $rows = array();
        foreach ($rs as $row) {
            $row['status_name'] = $row['status'] === '0' ? '发送失败' : '发送成功';

            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function get_data($client_code = '')
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $where = array();
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = (trim($fields['f'][$key]));
                $s = ($fields['s'][$key]);
                $v = (trim($fields['v'][$key]));
                if($f == '') continue;
                $pre = 'bsc_message.';
                if($f == 'job_no' || $f == 'trans_carrier'){
                    $pre = 'bc.';
                }
                if($v != '')$where[] = search_like($pre . $f, $s, $v);
            }
        }

        //加一个能看到该consol的才可以看到该报文
        $admin_field = array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document');//, 'finance');
        Model('bsc_user_role_model');
        $rs = $this->bsc_user_role_model->get_field_where_new($admin_field, 'bc');

        $where[] = "bsc_message.id_type = 'consol'";
        $where[] = $rs['role_where'];

        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $this->db->join('biz_consol bc', 'bsc_message.id_no = bc.id', 'LEFT');
        $this->db->join('biz_duty_new', "biz_duty_new.id_type = CONCAT('biz_', 'consol') and biz_duty_new.id_no = bsc_message.id_no", 'LEFT');
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $this->db->select('bsc_message.*, bc.job_no, bc.trans_carrier, (select client_name from biz_client where biz_client.client_code = bc.trans_carrier) as trans_carrier_name' .
            ',(select name from bsc_user where id = bsc_message.created_by) as created_by_name' .
            ',(select name from bsc_user where id = bsc_message.updated_by) as updated_by_name' .
            '');
        $this->db->join('biz_consol bc', 'bsc_message.id_no = bc.id', 'LEFT');
        $this->db->join('biz_duty_new', "biz_duty_new.id_type = CONCAT('biz_', 'consol') and biz_duty_new.id_no = bsc_message.id_no", 'LEFT');
        $rs = $this->model->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function message($consol_id = 0){

        $data = array();
        $message = $this->model->get_one_where("id_type = 'consol' and id_no = '$consol_id'");
        $data['message'] = $message;
        $esl = $this->model->get_one_where("id_type = 'consol' and id_no = '$consol_id' and receiver = 'ESL'");
        $data['ESL'] = $esl;
        $esl = $this->model->get_one_where("id_type = 'consol' and id_no = '$consol_id' and receiver = 'KMTC'");
        $data['KMTC'] = $esl;
        $yitong = $this->model->get_one_where("id_type = 'consol' and id_no = '$consol_id' and type = 'MANIFEST' and receiver = 'yitong'");
        $data['yitong'] = $yitong;
        $yitong_iftcps = $this->model->get_one_where("id_type = 'consol' and id_no = '$consol_id' and type = 'IFTCPS' and receiver = 'yitong'");
        $data['yitong_iftcps'] = $yitong_iftcps;
        $SINOTRANS = $this->model->get_one_where("id_type = 'consol' and id_no = '$consol_id' and receiver = 'SINOTRANS'");
        $data['SINOTRANS'] = $SINOTRANS;
        $LOGWING = $this->model->get_one_where("id_type = 'consol' and id_no = '$consol_id' and type = 'SO' and receiver = 'LOGWING'");
        $data['LOGWING'] = $LOGWING;
        $LOGWING_SI = $this->model->get_one_where("id_type = 'consol' and id_no = '$consol_id' and type = 'SI' and receiver = 'LOGWING'");
        $data['LOGWING_SI'] = $LOGWING_SI;
        $ams = $this->model->get_one_where("id_type = 'consol' and id_no = '$consol_id' and type = 'AMS' and receiver = 'EFREIGHT'");
        $data['ams'] = $ams;
        $aci = $this->model->get_one_where("id_type = 'consol' and id_no = '$consol_id' and type = 'ACI' and receiver = 'EFREIGHT'");
        $data['aci'] = $aci;
        $emf = $this->model->get_one_where("id_type = 'consol' and id_no = '$consol_id' and type = 'E-MANIFEST' and receiver = 'EFREIGHT'");
        $data['emf'] = $emf;



        $this->load->model('biz_consol_model');
        $consol = $this->biz_consol_model->get_one('id', $consol_id);
        $data['consol'] = $consol;

        // print_r($message);

        $data['id'] = empty($message) ? 0 : $message['id'];
        $this->load->view('head');
        $this->load->view('/bsc/message/send_view', $data);
    }

    public function message_by_shipment($shipment_id = 0){

        $data = array();

        $isf = $this->model->get_one_where("id_type = 'shipment' and id_no = '$shipment_id' and type = 'ISF' and receiver = 'EFREIGHT'");
        $data['isf'] = $isf;

        $this->load->model('biz_shipment_model');
        $shipment = $this->biz_shipment_model->get_one('id', $shipment_id);
        $data['shipment'] = $shipment;
        //打单没勾不能用
        if($shipment['dadanwancheng'] == '0'){
            exit("请勾选 打单 后再试");
        }

        $this->load->view('head');
        $this->load->view('/bsc/message/send_shipment_view', $data);
    }

    public function message_by_kml(){
        echo "kml 报文";
    }

    public function message_manifest($consol_id = 0){
        $data = array();
        $message = $this->model->get_one_where("id_type = 'consol' and id_no = '$consol_id'");
        $data['message'] = $message;
        $yitong = $this->model->get_one_where("id_type = 'consol' and id_no = '$consol_id' and receiver = 'yitong'");
        $data['yitong'] = $yitong;
        $this->load->model('biz_consol_model');
        $consol = $this->biz_consol_model->get_one('id', $consol_id);
        $data['consol'] = $consol;

        $data['id'] = empty($message) ? 0 : $message['id'];
        $this->load->view('head');
        $this->load->view('/bsc/message/send_manifest_view', $data);
    }

    /**
     * 生成报文
     * @param int consol_id
     * @param string $carrier
     * @param string $type
     * @param string file_function
     */
    public function create_message($id_no = 0,$carrier = '', $type = '', $file_function = 'insert'){
        $match_id = getValue('match_id', 0);
        if($type == 'BOOKING_AGENT_EBOOKING'){
            if($carrier == 'OUR_COMPANY'){
                if($file_function == 'insert'){

                }else if($file_function == 'update'){
                }else{
    
                }
                $bsc_message_our_company = Controller('bsc_message/bsc_message_our_company');
                return $bsc_message_our_company->ebooking_booking_agent($id_no);
            }
        }
        if($type == 'AGENT_EBOOKING'){
            if($carrier == 'OUR_COMPANY'){
                if($file_function == 'insert'){

                }else if($file_function == 'update'){
                }else{
    
                }
                $bsc_message_our_company = Controller('bsc_message/bsc_message_our_company');
                return $bsc_message_our_company->ebooking_agent($id_no);
            }
        }
        if($type === 'AMS'){
            if($file_function == 'insert'){
                $file_function = "A";

            }else if($file_function == 'update'){
                $file_function = "R";

            }else if($file_function == 'delete'){
                $file_function = "D";
            }
            $bsc_message_efreight = Controller('bsc_message/bsc_message_efreight');
            return $bsc_message_efreight->message_ams($id_no, $file_function);
        }

        if($type == 'ISF'){
            if($file_function == 'insert'){
                $file_function = "A";

            }else if($file_function == 'update'){
                $file_function = "R";

            }else if($file_function == 'delete'){
                $file_function = "D";
            }
            $bsc_message_efreight = Controller('bsc_message/bsc_message_efreight');
            return $bsc_message_efreight->message_isf($id_no, $file_function);
        }

        if($type == 'ACI'){
            if($file_function == 'insert'){
                $file_function = "A";

            }else if($file_function == 'update'){
                $file_function = "R";

            }else if($file_function == 'delete'){
                $file_function = "D";
            }
            $bsc_message_efreight = Controller('bsc_message/bsc_message_efreight');
            return $bsc_message_efreight->message_aci($id_no, $file_function);
        }
        if($type == 'E-MANIFEST'){
            if($file_function == 'insert'){
                $file_function = "A";

            }else if($file_function == 'update'){
                $file_function = "R";

            }else if($file_function == 'delete'){
                $file_function = "D";
            }
            $bsc_message_efreight = Controller('bsc_message/bsc_message_efreight');
            return $bsc_message_efreight->message_emf($id_no, $file_function);
        }
        //其他情况统一调用对应的控制器, 来调用方法
        //获取当前的配置信息
        $this->load->database();
        $match = $this->db->query("select * from bsc_message_match where id = '{$match_id}'")->row_array();
        if(empty($match)) return jsonEcho(array('code' => 1, 'msg' => lang('未获取到对应的配置信息')));
        
        $controller = $match['controller'];
        $method = $match['method'];
        $id_type = $match['id_type'];
        $receiver = $match['message_receiver'];
        $type = $match['message_type'];
        $file_ext_name = $match['file_ext_name'];
        
        if($match['is_diy_data'] !== '0'){
            $message_row = $this->db->query("select * from bsc_message where id_type = '{$id_type}' and id_no = $id_no and type = '{$type}' and receiver = '{$receiver}'")->row_array();
            if(empty($message_row)) return jsonEcho(array('code' => 1, 'msg' => lang('请先保存数据后再试')));
            
            $message_data = $this->db->query("select * from bsc_message_data where message_id = '{$message_row['id']}'")->row_array();
            if(empty($message_data))  return jsonEcho(array('code' => 1, 'msg' => lang('请先保存数据后再试')));

            //将post参数合并一下
            $_POST = array_merge($_POST, json_decode($message_data['send_data'], true));
        }
        
        // $file_function = '';
        session_write_close();
        
        $controller = Controller("bsc_message/{$controller}");
        //这里只考虑传这俩吧, 其他的 好像无所谓了, shipment和consol其实可以从match表里就设置好的
        $result = $controller->$method($id_no, $file_function);
        //不为0代表失败
        if($result['code'] !== 0) return jsonEcho($result);
        if(!isset($result['data']['file_content'])) return jsonEcho(array('code' => 1, 'msg' => lang('未生成文件内容')));
        
        $file_content = $result['data']['file_content'];
        
        //成功时, 生成对应的文件
        //接下来将报文进行转化
        //保存文件txt
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        //ESL_EDI/日期
        $new_dir = "{$root_path}/upload/message/{$receiver}_EDI/{$type}/{$path}/";
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        //文件名
        $file_name = isset($result['data']['file_name']) ? $result['data']['file_name'] . '.' . $file_ext_name : time() . $id_no . '.' . $file_ext_name;
        $file_path = $new_dir . $file_name;
        file_put_contents($file_path, $file_content);

        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
        );
        $message_id = update_message_by_id_type($message_data, $id_type, $id_no, $type, $receiver);

        return jsonEcho(array('code' => 0, 'msg' => lang('文件生成成功'), 'data' => array('id' => $message_id)));
    }

    /**
     * 更新文件信息
     * @param $consol_id
     * @param $new_file_path
     * @param $type
     */
    public function update_message($consol_id, $data, $message_type){
        return update_message($consol_id, $data, $message_type);
    }

    /**
     * 更新文件信息
     * @param $consol_id
     * @param $new_file_path
     * @param $type
     */
    public function update_message_by_shipment($shipment_id, $data, $message_type){
        return update_message_by_shipment($shipment_id, $data, $message_type);
    }

    /**
     * 修改的新逻辑，报文数据不再由代码里读取了，生成代码只做验证等，不参与数据获取，
     * 2021-06-24 16:05
     */
    public function message_form($id_type = '', $id_no = 0, $type = '', $carrier = '',$action = 'insert'){
        //进入EDI页面，点击生成，此刻按照规则生成参数，该填写的留给他们
        if($id_type == 'consol'){
            if($type == 'AMS'){
                if($carrier == 'EFREIGHT'){
                    $bsc_message_efreight = Controller('bsc_message/bsc_message_efreight');
                    return $bsc_message_efreight->ams_set_data($id_no);
                }
            }
            if($type == 'E-MANIFEST'){
                if($carrier == 'EFREIGHT'){
                    $bsc_message_efreight = Controller('bsc_message/bsc_message_efreight');
                    return $bsc_message_efreight->emf_set_data($id_no, $action);
                }
            }
            if($type == 'ACI'){
                if($carrier == 'EFREIGHT'){
                    $bsc_message_efreight = Controller('bsc_message/bsc_message_efreight');
                    return $bsc_message_efreight->aci_set_data($id_no, $action);
                }
            }
            if($type == 'SI'){
                if($carrier == 'LOGWING'){
                    $bsc_message_efreight = Controller('bsc_message/bsc_message_logwing');
                    return $bsc_message_efreight->si_set_data($id_no, $action);
                }
            }
        }
        if($id_type == 'shipment'){
            if($carrier == 'EFREIGHT'){
                $bsc_message_efreight = Controller('bsc_message/bsc_message_efreight');
                if($type == 'ISF'){
                    return $bsc_message_efreight->isf_set_data($id_no);
                }
            }
        }
        $match_id = getValue('match_id');
        //其他情况统一调用对应的控制器, 来调用方法
        //获取当前的配置信息
        $this->load->database();
        $match = $this->db->query("select * from bsc_message_match where id = '{$match_id}'")->row_array();
        if(empty($match)) return jsonEcho(array('code' => 1, 'msg' => lang('未获取到对应的配置信息')));
        
        $controller = $match['controller'];
        $method = $match['form_method'];
        $id_type = $match['id_type'];
        $receiver = $match['message_receiver'];
        $type = $match['message_type'];
        
        session_write_close();
        
        $controller = Controller("bsc_message/{$controller}");
        $controller->$method(array('match' => $match, 'action' => $action, 'id_no' => $id_no));
    }

    public function set_message_data($id_type = '', $id_no = 0, $type = '', $receiver = ''){
        if(empty($_POST)){
            echo json_encode(array('code' => 1, 'msg' => '参数错误'));
            return;
        }
        $message_old = $this->model->get_where_one("id_type = '$id_type' and id_no = '$id_no' and type = '$type' and receiver = '$receiver'");
        $message = array();
        if(empty($message_old)){
            $message['id_type'] = $id_type;
            $message['id_no'] = $id_no;
            $message['type'] = $type;
            $message['receiver'] = $receiver;
            $message['file_path'] = '';
            $message_id = $this->model->save($message);

            //save the operation log
            $log_data = array();
            $log_data["table_name"] = "bsc_message";
            $log_data["key"] = $message_id;
            $log_data["action"] = 'insert';
            $log_data["value"] = json_encode($message);
            log_rcd($log_data);
        }else{
            $message_id = $message_old['id'];
        }

        Model('bsc_message_data_model');

        $message_data_old = $this->bsc_message_data_model->get_where_one("message_id = '$message_id'");
        $message_data = array();
        $send_data = json_encode(array_trim($_POST));
        $message_data['send_data'] = $send_data;
        if(empty($message_data_old)){
            $message_data['message_id'] = $message_id;
            $message_data_id = $this->bsc_message_data_model->save($message_data);

            $data_action = 'insert';
        }else{
            $message_data_id = $message_data_old['id'];
            $this->bsc_message_data_model->update($message_data_id, $message_data);

            $data_action = 'update';
        }


        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "bsc_message_data";
        $log_data["key"] = $message_data_id;
        $log_data["action"] = $data_action;
        $log_data["value"] = json_encode($message_data);
        log_rcd($log_data);

        echo json_encode(array('code' => 0, 'msg' => '保存成功'));
    }

    /**
     * 发送报文
     * @param int $id
     * @param string $send_type
     * @param string $receiver
     */
    public function send_message($id = 0, $send_type = '', $receiver = ''){
        $message = $this->model->get_one('id', $id);
        if(empty($message)){
            return jsonEcho(array('code' => 1, 'msg' => lang("报文不存在, 发送失败")));
            return;
        }
        if(empty($message['file_path'])){
            return jsonEcho(array('code' => 1, 'msg' => lang('未获取到报文, 发送失败')));
        }
        $this->load->model('biz_consol_model');
        if($message['id_type'] == 'consol'){
            $consol_id = $message['id_no'];
        }else{
            $consol_id = $message['consol_id'];
        }
        $consol = $this->biz_consol_model->get_by_id($consol_id);
        $upload = false;
        $data_send = array();
        //发送前 验证当前箱型箱量和box_info是否匹配
        $send_check = $this->send_check($message['id_type'], $message['id_no'], $message['type']);
        if($send_check) return jsonEcho(array('code' => 1, 'msg' => $send_check));

        // if(!in_array(base_url(), array('http://china.leagueshipping.com/'))) return jsonEcho(array('code' => 1, 'msg' => '当前环境不允许发送'));

        if($send_type == 'FTP'){
            if($receiver == 'ESL'){
                $file_path = dirname(BASEPATH) . '/upload/message/ESL_EDI/SO' . $message['file_path'];
                $upload_path = '/CNEDI/CNSHA/EDIBOOKINGV2/' . $consol['job_no'] . '.txt';
                $file_config = array('host' => 'www.emiratesline.com', 'username' => 'CNEDIuser', 'password' => 'Kdh94jsdfkh');
                $upload = $this->ftp_upload($file_path,$upload_path,$file_config);
                $result['msg'] = '';
            }
            if($receiver == 'KMTC'){
                $file_path = dirname(BASEPATH) . '/upload/message/KMTC_EDI/SO' . $message['file_path'];
                $upload_path = '/' . $consol['job_no'] . '.txt';
                $file_config = array('host' => '210.109.49.181', 'port' => 24, 'username' => 'SKXC00', 'password' => 'kmtc1234!');
                $upload = $this->ftp_upload($file_path,$upload_path,$file_config);
                $result['msg'] = '';
            }
            if($receiver == 'LOGWING'){
                //修改为LOGWING的FTP服务器
                $file_path = dirname(BASEPATH) . '/upload/message/LOGWING_EDI/' . $message['type'] . $message['file_path'];
                $upload_path = '/' . strtolower($message['type']) . '/in/' . $consol['job_no'] . '.txt';
                $file_config = array('host' => 'ftp.booking001.com', 'port' => 21, 'username' => 'FWDEH2F', 'password' => 'T53Q72hT');
                $upload = $this->ftp_upload($file_path,$upload_path,$file_config);
                $result['msg'] = '';
            }
            //2023-06-29
            if($receiver == 'EASIPASS'){
                //修改为LOGWING的FTP服务器
                $file_path = dirname(BASEPATH) . '/upload/message/EASIPASS_EDI/' . $message['type'] . $message['file_path'];
                $upload_path = '/in/' . $consol['job_no'] . '.txt';
                $file_config = array('host' => 'psedi.easipass.com', 'port' => 21, 'username' => 'shjhcw', 'password' => '9djtPUp4');
                $upload = $this->ftp_upload($file_path,$upload_path,$file_config);
                $result['msg'] = '';
            }
        }
        if($send_type == 'API'){
            if($receiver == 'SINOTRANS'){
                $file_path = dirname(BASEPATH) . '/upload/message/SINOTRANS_EDI/SO' . $message['file_path'];
                $data = file_get_contents($file_path);
                $api_json = $this->API_upload($receiver, $data);
                $api_result = json_decode($api_json, true);
                $data_send['return_msg_all'] = $api_json;
                if(isset($api_result['HTTP_STATUS']) && $api_result['HTTP_STATUS'] == 0){
                    $upload = true;
                }
                $data_send['return_msg'] = $result['msg'] = isset($api_result['HTTP_MSG']) ? $api_result['HTTP_MSG'] : '发送失败';
            }

            if($receiver == 'yitong'){
                //2022-09-20 亿通舱单发送前,如果只有一个shipment,那么发送时,对比下cus_no和mbl是否相等
                $shipments = Model("biz_shipment_model")->get_shipment_by_consol($consol['id']);
                //这里为了防止带@符号的被拦截掉,这里把最后一位的@过滤
                if(sizeof($shipments) == 1 && str_replace('@', '', $shipments[0]['cus_no']) != $consol['carrier_ref']) return jsonEcho(array('code' => 1, 'msg' => '检测到shipment关单号与consol的MBL不一致,发送失败'));

                $t = date("Y-m-d H:i:s");
                $d = urlencode($t);
                $sign = "shanghaijx37d7d164-5709-4b9a-9663-3d628f8dd3fc$t";
                $sign = md5($sign);
                $method = "cargoedi.demessage.msgrec.post";
                $docName = $consol['job_no'].'_'.time();//报文文件名称    提单号+时间戳
                $file = './upload/message/yitong_EDI/MANIFEST'.$message['file_path'];
                $body = file_get_contents($file);
                // $body = iconv('UTF-8', 'GBK//IGNORE', $body);
                $body = mb_convert_encoding($body, "UTF-8", "GBK");
                //exit($body);
                $body = base64_encode($body);
                // exit($body);
                $url = "http://api.nbeport.com/router/rest";//客户端发报文接口
                $data = array(
                    'user_id'=>'shanghaijx',
                    'format'=>'json',
                    'timestamp'=>$d,
                    'sign'=>$sign,
                    'method'=>'cargoedi.demessage.msgrec.post',
                    'deaId'=>'SHJXGYL',
                    'userId'=>'SHJXGYL',
                    'docName'=>$docName,
                    'docType'=>'PREMFT3',
                    'docLength'=>'0',
                    'docContent'=>$body,
                    'channel'=>'WSTOM',
                    'destDeaId'=>'NBEPORT',
                    'port'=>'CNSHA'
                );
                $res = $this->http_post($url, $data);
                // $res = '{"code":"T","msg":"","data":{"result":"true","errorInfo":null,"gid":"37054bdd6bb64068a328835b3a030ef3"}}';
                $data_send['return_msg_all'] = $res;
                $res = json_decode($res,true);
                if($res['code'] == 'T') {
                    if($res['data']['result'] == 'true'){
                        $upload = true;
                    }else{
                    }
                }
                $data_send['return_msg'] = $result['msg'] = $res['msg'];
            }

            if($receiver == 'EFREIGHT'){
                $file_path = dirname(BASEPATH) . '/upload/message/EFREIGHT_EDI/' . $message['type'] . $message['file_path'];
                $data = file_get_contents($file_path);
                $api_result =  $this->API_upload($receiver, $data);
                $data_send['return_msg_all'] = $api_result;
                if($api_result==""){
                    $upload = true;
                }
                $data_send['return_msg'] = $result['msg'] = $api_result;
            }
            if($receiver == 'OUR_COMPANY'){
                $file_path = dirname(BASEPATH) . "/upload/message/{$receiver}_EDI/{$message['type']}{$message['file_path']}";
                $json = file_get_contents($file_path);
                //这里的url都存到了文件里, 所以这里需要提取出来再转化回去
                $data = json_decode($json, true);
                $url = $data['url'];unset($data['url']);
                
                $api_json =  $this->API_upload($receiver, json_encode($data), $url);
                $api_result = json_decode($api_json, true);
                $data_send['return_msg_all'] = $api_json;
                if($api_result==""){
                    $upload = true;
                }
                $data_send['return_msg'] = $result['msg'] = $api_result['msg'];
            }
        }

        $data_send['message_id'] = $message['id'];
        $data_send['file_path'] = $message['file_path'];
        $data_send['send_by'] = get_session('id');
        $data_send['send_time'] = date('Y-m-d H:i:s', time());
        if($upload){
            $data_send['status'] = 1;
            $data = array();
            $data['status'] = 1;
            $data['send_by'] = $this->session->userdata('id');
            $data['send_time'] = date('Y-m-d H:i:s', time());
            $this->model->update($id, $data);

            //AMS ACI ISF 发送成功后 生成一笔对应的账单
            if(in_array($message['type'], array('AMS', 'E-MANIFEST', 'ISF', 'ACI'))){
                //统一收14元给促准 自动填入对账号auto_日期, 一个shipment收一笔
                $bill_data = array();
                if($message['type'] == 'AMS'){
                    $bill_data['charge_code'] = 'AMS';
                }else if($message['type'] == 'ACI'){
                    $bill_data['charge_code'] = 'ACI';
                }else if($message['type'] == 'ISF'){
                    $bill_data['charge_code'] = 'ISF';
                }else if($message['type'] == 'E-MANIFEST'){
                    $bill_data['charge_code'] = 'EMF';
                }

                if($message['id_type'] == 'shipment'){
                    $shipments = array(Model('biz_shipment_model')->get_by_id($message['id_no']));
                }else if($message['id_type'] == 'consol'){
                    $shipments = Model('biz_shipment_model')->get_shipment_by_consol($message['id_no']);
                }else{
                    $shipments = array();
                }
                foreach ($shipments as $shipment){
                    $bill_data['type'] = 'cost';
                    $bill_data['vat'] = '0%';
                    $bill_data['is_special'] = 0;
                    $bill_data['split_mode'] = 0;
                    $bill_data['rate_id'] = 0;
                    $bill_data['id_type'] = 'shipment';
                    $bill_data['id_no'] = $shipment['id'];
                    $bill_data['currency'] = 'CNY';//固定为RMB
                    $bill_data['unit_price'] = 14; // 总价代码里会自动算的
                    $bill_data['number'] = 1; // 如果是consol发的那几个,那么取shipment数量,如果是shipment那么取1
                    $bill_data['client_code'] = 'HZCZXX01'; // 客户固定为促准
                    $bill_data['account_no'] = 'auto_' . date('Ymd'); // 对账号固定
                    $bill_data['remark'] = 'EDI 发送自动生成'; // 备注固定
                    add_bill($bill_data, true);
                    //这个出错好像也没法恢复,就不判定了
                }
            }

            if(!empty($res)){
                $data['gid'] = $res['data']['gid'];
            }
            //save the operation log
            $log_data = array();
            $log_data["table_name"] = "bsc_message";
            $log_data["key"] = $id;
            $log_data["action"] = 'update';
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);

            $result['code'] = 0;
            $data_send['return_msg'] = $result['msg'] = '发送成功';
        }
        Model('bsc_message_send_model');
        $this->bsc_message_send_model->save($data_send);
        echo json_encode($result);
    }

    /**
     * 发送报文前的一些验证
     * @param string $id_type
     * @param string $id_no
     * @return string|bool false代表成功,其他值都代表失败
     */
    private function send_check($id_type = '', $id_no = '', $type = ''){
        if($id_type == 'consol'){
            //SO时候没有箱数据
            if($type !== 'SO'){
                $consol = Model('biz_consol_model')->get_where_one("id = '{$id_no}'");

                if($consol['trans_mode'] == 'FCL'){
                    //把箱型箱量提取出来,然后统计后与container进行比较--start
                    $consol_container = Model('biz_container_model')->get("consol_id = '{$id_no}'");
                    //将箱信息整理为box_info相同类型
                    $container_box_info = array();
                    $container_size = array_column($consol_container, 'container_size');
                    $rows = array_count_values(array_filter($container_size));
                    foreach ($rows as $key => $row){
                        $container_box_info[] = array('size' => $key, 'num' => $row);
                    }
                    //排序--start
                    $s = array_column($container_box_info, 'size');
                    array_multisort($s, SORT_DESC, $container_box_info);

                    $consol_box_info = json_decode($consol['box_info'], true);
                    $s = array_column($consol_box_info, 'size');

                    //根据箱型排序后比较2个数组是否相同
                    array_multisort($s, SORT_DESC, $consol_box_info);
                    if ($container_box_info != $consol_box_info) return "consol的 container 箱型箱量与 订舱的箱型箱量 不符,处理后请重新生成报文";
                    //把箱型箱量提取出来,然后统计后与container进行比较--end
                }

            }
        }else if($id_type == 'shipment'){
            return false;
        }else{
            return false;
        }
    }

    private function http_post($url,$data){
        $postData = http_build_query($data);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT,'Opera/9.80 (Windows NT 6.2; Win64; x64) Presto/2.12.388 Version/12.15');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // stop verifying certificate
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        $r = curl_exec($curl);
        curl_close($curl);
        return $r;
    }

    /**
     * FTP同步文件
     * @param string $file_path
     * @param string $upload_path
     * @param array $ftp_config
     */
    private function ftp_upload($file_path, $upload_path, $ftp_config){
        //判断文件是否存在
        if(!file_exists($file_path)){
            return false;
        }
        $upload = ftp_upload($ftp_config, $file_path, $upload_path);
        return $upload;
    }

    private function API_upload($receiver, $data, $url = '', $header = array()){
        $request_json = false;
        if($url == ''){
            if($receiver == 'SINOTRANS'){//外运
                //测试环境数据--start
                // $url = 'https://apitest.i.sinotrans.com:8065/HYNetService/v1/APIBookingServlet';
                // $header = array(
                //     'KeyId:6910add0-4621-4abd-b6c7-ad65880f5852',
                //     'BusinessCode:JXGYLAPI',
                //     'Content-Type:text/xml',
                // );
                //测试环境数据--end
                $url = 'http://180.169.151.151/Api/XML/PilotRunOnPrd';
                $header = array(
                    'KeyId:94ec1a50-487b-451f-891e-5be1d1cbc8e4',
                    'BusinessCode:JXGYL',
                    'Content-Type:text/xml',
                );
    
                $body = $data;
                $request_json = curl_post_body($url,$header,$body);
            }else if($receiver == 'EFREIGHT'){//EFREIGHT
                //测试环境数据--start
                // $url = 'http://edi.gsf24.cn/api/OpenApi?CustomerCode=CUZHUN&scac=TEST&AppKey=CF46D05342F51663';
                // $header = array(
                //     'Content-Type:text/xml',
                // );
                //测试环境数据
    
                //环境数据--start
                $url = 'http://edi.gsf24.cn/api/OpenApi?CustomerCode=LEAGUE&scac=SZFG&AppKey=CF46D05342F51663';
                $header = array(
                    'Content-Type:text/xml',
                );
                //环境数据--end  
                $body = $data;
                $request_json = curl_post_body($url,$header,$body);
            }
        }else{
            //环境数据--end  
            $body = $data;
            $request_json = curl_post_body($url, $header, $body);
        }


        $api_log = array(
            'send_msg' => json_encode($data, JSON_UNESCAPED_UNICODE),
            'msg' => $request_json,
            'create_time' => date('Y-m-d H:i:s'),
            'type' => 'bsc_message_' . $receiver,
        );
        Model('sys_api_log_model')->save($api_log);

        return $request_json;
    }

    /**
     * 获取文件
     */
    public function logwing_ftp_read_out($id = 0)
    {
        $bsc_message_logwing = Controller('bsc_message/bsc_message_logwing');
        $bsc_message_logwing->ftp_read_out($id);
    }

    /**
     * 获取文件
     */
    public function ams_read_out($id = 0)
    {
        $bsc_message_efreight = Controller('bsc_message/bsc_message_efreight');
        $bsc_message_efreight->ams_read_out($id);
    }

    /**
     *
     */
    public function file_read($id = 0){
        $message = $this->model->get_one('id', $id);
        if(empty($message)){
            echo '文件不存在';
            return;
        }
        $data = $message;
        if($message['file_path'] == ''){
            echo '报文未生成';
            return;
        }
        $path_arr = explode('.', $message['file_path']);
        $receiver = $message['receiver'];

        $file_path_prefix = dirname(BASEPATH) . '/upload/message/' . $receiver . '_EDI/' . $message['type'];
        $file_content = file_get_contents($file_path_prefix . $message['file_path']);

        if($message['type'] == 'SI'){
            echo '不支持';
            return;
        }

        if($path_arr[1] == 'txt'){
            $file_content_html = array(
                'job_no' => '',
                'payment' => '',
                'trans_origin' => '',
                'mark_nums' => '',
                'trans_discharge' => '',
                'hs_code' => '',
                'trans_destination' => '',
                'container' => array(),
                'description' => '',
                'shipper' => '',
                'consignee' => '',
                'good_outers' => '',
                'good_outers_unit' => '',
                'good_weight' => '',
                'good_volume' => '',
            );
            $file_content_array = explode("\r\n", $file_content);
            if($receiver == 'ESL' || $receiver == 'KMTC' || $receiver == 'yitong'){
                foreach ($file_content_array as $row){
                    $row_len = strlen($row);

                    if($row_len > 0 && $row[$row_len - 1] == '\''){
                        $row = substr($row, 0, $row_len);
                    }
                    $row_array = explode(':', $row);
                    if($row_array[0] == '02'){
                        $file_content_html['job_no'] = $row_array[1];
                        continue;
                    }
                    if($row_array[0] == '12'){
                        $file_content_html['trans_origin'] = $row_array[2];
                        $file_content_html['trans_discharge'] = $row_array[6];
                        $file_content_html['trans_destination'] = $row_array[10];
                        continue;
                    }
                    if($row_array[0] == '14'){
                        $file_content_html['payment'] = $row_array[1] . '  ' . $row_array[2];
                        continue;
                    }
                    if($row_array[0] == '20'){
                        unset($row_array[0]);
                        $file_content_html['shipper'] = join("\n", array_filter($row_array));
                        continue;
                    }
                    if($row_array[0] == '21'){
                        unset($row_array[0]);
                        $file_content_html['consignee'] = join("\n", array_filter($row_array));
                        continue;
                    }
                    if($row_array[0] == '22'){
                        unset($row_array[0]);
                        $file_content_html['notify'] = join("\n", array_filter($row_array));
                        continue;
                    }
                    if($row_array[0] == '41'){
                        unset($row_array[0]);
                        $file_content_html['hs_code'] = $row_array[2];
                        $file_content_html['good_outers'] = $row_array[4];
                        $file_content_html['good_outers_unit'] = $row_array[6];
                        $file_content_html['good_weight'] = $row_array[7];
                        $file_content_html['good_volume'] = $row_array[8];
                        continue;
                    }
                    if($row_array[0] == '44'){
                        unset($row_array[0]);
                        $file_content_html['mark_nums'] = join("\n", array_filter($row_array));
                        continue;
                    }
                    if($row_array[0] == '47'){
                        unset($row_array[0]);
                        $file_content_html['description'] = join("\n", array_filter($row_array));
                        continue;
                    }
                    if($row_array[0] == '48'){
                        $container = array();
                        $container['container_size'] = $row_array[1] . '*' . $row_array[2];
                        $container['trans_mode'] = lang('trans_mode') . ':' . $row_array[3];
                        $container['SOC'] = 'SOC:' . $row_array[8];
                        $file_content_html['container'][] = $container;
                        continue;
                    }
                    //   
                }
            }
            else if($receiver == 'LOGWING'){
                foreach ($file_content_array as $row){
                    $row_len = strlen($row);

                    if($row_len > 0 && $row[$row_len - 1] == '\''){
                        $row = substr($row, 0, $row_len - 1);
                    }
                    if(substr_count($row, 'UNB+UNOC:2+EH2F:ZZZ+LOGWING:UN+') > 0){
                        $row = str_replace('UNB+UNOC:2+EH2F:ZZZ+LOGWING:UN+', '', $row);
                        $row_array = explode('+', $row);
                        $file_content_html['job_no'] = $row_array[1];
                    }
                    if(substr_count($row, 'LOC+9+') > 0){
                        $row = str_replace('LOC+9+', '', $row);
                        $row_array = explode(':', $row);
                        $file_content_html['trans_origin'] = $row_array[sizeof($row_array) - 1];
                    }
                    if(substr_count($row, 'LOC+11+') > 0){
                        $row = str_replace('LOC+11+', '', $row);
                        $row_array = explode(':', $row);
                        $file_content_html['trans_discharge'] = $row_array[sizeof($row_array) - 1];
                        continue;
                    }
                    if(substr_count($row, 'LOC+7+') > 0){
                        $row = str_replace('LOC+7+', '', $row);
                        $row_array = explode(':', $row);
                        $file_content_html['trans_destination'] = $row_array[sizeof($row_array) - 1];
                        continue;
                    }
                    if(substr_count($row, 'CPI+4++') > 0){
                        $row = str_replace('CPI+4++', '', $row);
                        $file_content_html['trans_destination'] = $row;
                        continue;
                    }
                    if(substr_count($row, 'NAD+CZ+++') > 0){
                        $row = str_replace('NAD+CZ+++', '', $row);
                        $row_array = explode(':', $row);
                        $file_content_html['shipper'] = $row_array[0];
                        continue;
                    }
                    if(substr_count($row, 'NAD+CN+++') > 0){
                        $row = str_replace('NAD+CN+++', '', $row);
                        $row_array = explode(':', $row);
                        $file_content_html['consignee'] = $row_array[0];
                        continue;
                    }
                    if(substr_count($row, 'NAD+NI+++') > 0){
                        $row = str_replace('NAD+NI+++', '', $row);
                        $row_array = explode(':', $row);
                        $file_content_html['notify'] = $row_array[0];
                        continue;
                    }
                    if(substr_count($row, 'PIA+5+') > 0){
                        $row = str_replace('PIA+5+', '', $row);
                        $row_array = explode(':', $row);
                        $file_content_html['hs_code'] = $row_array[0];
                        continue;
                    }
                    if(substr_count($row, 'GID+1+') > 0){
                        $row = str_replace('GID+1+', '', $row);
                        $row_array = explode(':', $row);
                        $file_content_html['good_outers'] = $row_array[0];
                        $file_content_html['good_outers_unit'] = $row_array[sizeof($row_array) - 1];
                        continue;
                    }
                    if(substr_count($row, 'MEA+AAE+WT+KGM:') > 0){
                        $row = str_replace('MEA+AAE+WT+KGM:', '', $row);
                        $file_content_html['good_weight'] = $row;
                        continue;
                    }

                    if(substr_count($row, 'MEA+AAE+AAW+MTQ:') > 0){
                        $row = str_replace('MEA+AAE+AAW+MTQ:', '', $row);
                        $file_content_html['good_volume'] = $row;
                        continue;
                    }
                    if(substr_count($row, 'FTX+AAA+++') > 0){
                        $row = str_replace('FTX+AAA+++', '', $row);
                        $file_content_html['description'][] = $row;
                        continue;
                    }
                    if(substr_count($row, 'EQD+CN++') > 0){
                        $row = str_replace('EQD+CN++', '', $row);
                        $row_array = explode('+', $row);
                        $container['container_size'][] = $row_array[0];
                        continue;
                    }
                    if(substr_count($row, 'EQN+') > 0){
                        $row = str_replace('EQN+', '', $row);
                        $container['container_num'][] = $row;
                        continue;
                    }
                }
                $file_content_html['mark_nums'] = '';
                $file_content_html['description'] = join("\n", $file_content_html['description']);
                $containers = array();
                foreach ($container['container_size'] as $key => $row){
                    $this_container = array();
                    $this_container['container_size'] = $row . '*' . $container['container_num'][$key];
                    $containers[] = $this_container;
                }
                $file_content_html['container'] = $containers;
            }
            else if($receiver == 'SINOTRANS'){
                echo '正在建设中';
                return;
            }else{
                $file_content_html = join("<br/>", $file_content_array);
                echo $file_content_html;
                return;
            }
            $data['file_content_html'] = $file_content_html;

            $file_content_all = join('<br />', explode("\n", $file_content));
            $data['file_content_all'] = $file_content_all;
            $this->load->view('/bsc/message/file_read', $data);
        }else{
            // $file_content_html = join('><br />      <', explode('><', $file_content));
            //一飞ISF报文预览 2021-05-28 mars提出
            if($message['type'] == 'ISF' && $message['receiver'] == 'EFREIGHT'){
                $message_array = XML_decode($file_content);
                if(isset($message_array['SF10'])){//代表发的ISF10
                    $ShippingParties = array_column($message_array['ShippingParties']['ShippingParty'], null, 'PartyCode');
                    $MANUFACTURER = $ShippingParties['MANUFACTURER' . $message['id_no']];
                    $MANUFACTURER_name = $MANUFACTURER['PartyName'];
                    $MANUFACTURER_address = is_array($MANUFACTURER['PartyLocation']['Address']['AddressLine']) ? join('', $MANUFACTURER['PartyLocation']['Address']['AddressLine']) : $MANUFACTURER['PartyLocation']['Address']['AddressLine'];

                    $SHIPPER = $ShippingParties['SHIPPER' . $message['id_no']];
                    $SHIPPER_name = $SHIPPER['PartyName'];
                    $SHIPPER_address = is_array($SHIPPER['PartyLocation']['Address']['AddressLine']) ? join('', $SHIPPER['PartyLocation']['Address']['AddressLine']) : $SHIPPER['PartyLocation']['Address']['AddressLine'];

                    $CONSIGNEE = $ShippingParties['CONSIGNEE' . $message['id_no']];
                    $CONSIGNEE_name = $CONSIGNEE['PartyName'];
                    $CONSIGNEE_address = is_array($CONSIGNEE['PartyLocation']['Address']['AddressLine']) ? join('', $CONSIGNEE['PartyLocation']['Address']['AddressLine']) : $CONSIGNEE['PartyLocation']['Address']['AddressLine'];
                    $CONSIGNEE_record = $CONSIGNEE['PartyID'];

                    $Manufacturer = $message_array['SF10']['Manufacturers']['Manufacturer'];
                    $countryorigin = $Manufacturer['CountryOrigin']['@attributes']['code'];

                    if(is_array($Manufacturer['HTSCodes']['HTSCode'])){
                        $Manufacturer['HTSCodes']['HTSCode'] = array_filter($Manufacturer['HTSCodes']['HTSCode']);
                    }else{
                        $Manufacturer['HTSCodes']['HTSCode'] = $Manufacturer['HTSCodes']['HTSCode'];
                    }

                    $hts_code = is_array($Manufacturer['HTSCodes']['HTSCode']) ? join(',', $Manufacturer['HTSCodes']['HTSCode']) : $Manufacturer['HTSCodes']['HTSCode'];

                    $data['message']['manufacturer_name'] = $MANUFACTURER_name;//生产厂家的名字
                    $data['message']['manufacturer_address'] = join('', $MANUFACTURER['PartyLocation']['Address']['AddressLine']);//生产厂家的地址
                    $data['message']['seller_name'] = $SHIPPER_name;//卖家的名字
                    $data['message']['seller_address'] = $SHIPPER_address;//卖家的地址
                    $data['message']['buyer_name'] = $CONSIGNEE_name;//买家的名字
                    $data['message']['buyer_address'] = $CONSIGNEE_address;//买家的地址
                    $data['message']['ship_name'] = $CONSIGNEE_name;//清关后第一个收货点的名字
                    $data['message']['ship_address'] = $CONSIGNEE_address;//清关后第一个收货点的地址
                    $data['message']['importer_record'] = $CONSIGNEE_record;//进口商的登记号，可以是FTZ，IRS，EIN，SSN 或CBP NO
                    $data['message']['consignee_record'] = $CONSIGNEE_record;//收货人登记号，同上
                    $data['message']['manufacturer_countryorigin'] = $countryorigin;//货物原产地
                    $data['message']['hts_code'] = $hts_code;//商品编码HTS CODE前6位
                    $data['message']['container_stuff_name'] = $SHIPPER_name;//container_stuff 名称
                    $data['message']['container_stuff_address'] = $SHIPPER_address;//container_stuff 地址
                    $data['message']['mbl'] = $message_array['SF10']['MasterBillNo'];
                    $data['message']['hbl'] = $message_array['SF10']['AMSSCAC'] . $message_array['SF10']['AMSBLNumber'];
                    $data['message']['scac_code'] = $message_array['SF10']['AMSSCAC'];
                    $this->load->view('bsc/message/read/EFREIGHT/isf_view', $data);
                    return;
                }
            }else if($message['type'] == 'MANIFEST' && $message['receiver'] == 'yitong'){//亿通舱单预览
                try {
                    $message_array = XML_decode($file_content);
                } catch(\Exception $e) {
                    echo '文件存在中文';
                    return;
                }
                $data = array();
                $shipments = array();

                $body = $message_array['body'];
                if(isset($body['Billbody']) && !isset($body['Billbody']['reference'])) $body = $body['Billbody'];
                else if(isset($body['Billbody']) && isset($body['Billbody']['reference'])) $body = array($body['Billbody']);

                // print_r($body);
                foreach ($body as $billbody){
                    $reference = $billbody['reference'];

                    $goodsdetail = $billbody['goodsdetail'];

                    $detail = $billbody['detail'];
                    $baseinfo = $detail['baseinfo'];
                    $goods = $detail['goods'];
                    $shipper = $baseinfo['shipper'];
                    $consignee = $baseinfo['consignee'];
                    $notifyparty = $baseinfo['notifyparty'];

                    $freight = $detail['freight'];
                    $port = $freight['port'];
                    $vessel = $freight['vessel'];
                    $containers = $freight['container'];

                    $shipment = array();
                    $shipment['billofladingno'] = $reference['billofladingno'];
                    $shipment['cargoid'] = $goodsdetail['cargoid'];
                    $shipment['baseinfo'] = $detail['baseinfo'];

                    $shipment['co_portofloading'] = $port['co_portofloading'];
                    $shipment['portofloading'] = $port['portofloading'];
                    $shipment['co_portofdischarge'] = $port['co_portofdischarge'];
                    $shipment['portofdischarge'] = $port['portofdischarge'];
                    $shipment['co_placeofdelivery'] = $port['co_placeofdelivery'];
                    $shipment['placeofdelivery'] = $port['placeofdelivery'];

                    $shipment['carrier'] = $vessel['carrier'];
                    $shipment['oceanvessel'] = $vessel['oceanvessel'];
                    $shipment['voyno'] = $vessel['voyno'];
                    $shipment['shipagent'] = $vessel['shipagent'];

                    $shipment['billofladingtype'] = $baseinfo['billofladingtype'];
                    $shipment['placeofissuecode'] = $baseinfo['placeofissuecode'];
                    $shipment['placeofissue'] = $baseinfo['placeofissue'];
                    $shipment['numberofcopys'] = $baseinfo['numberofcopys'];
                    $shipment['paymenttermcode'] = $baseinfo['paymenttermcode'];
                    $shipment['shippingitem'] = $baseinfo['shippingitem'];

                    if(is_array($shipper['shipperaddress'])) $shipper['shipperaddress'] = join('', $shipper['shipperaddress']);
                    $shipment['shippercode'] = $shipper['shippercode'];
                    $shipment['shippername'] = $shipper['shippername'];
                    $shipment['shipperaddress'] = $shipper['shipperaddress'];
                    $shipment['shippercountry'] = $shipper['shippercountry'];
                    $shipment['shippertele'] = $shipper['shippertele'];
                    $shipment['shipperAEO'] = '';

                    $shipment['consigneecode'] = $consignee['consigneecode'];
                    $shipment['consigneename'] = $consignee['consigneename'];
                    $shipment['consigneeaddress'] = $consignee['consigneeaddress'];
                    $shipment['consigneecountry'] = $consignee['consigneecountry'];
                    $shipment['consigneetele'] = empty($consignee['consigneetele']) ? '' : $consignee['consigneetele'];
                    $shipment['consigneeAEO'] = '';
                    $shipment['consigneeperson'] = $consignee['consigneeperson'];
                    $shipment['consigneepersontele'] = $consignee['consigneepersontele'];

                    $shipment['notifycode'] = $notifyparty['notifycode'];
                    $shipment['notifyname'] = $notifyparty['notifyname'];
                    $shipment['notifyaddress'] = $notifyparty['notifyaddress'];
                    $shipment['notifycountry'] = $notifyparty['notifycountry'];
                    $shipment['notifytele'] = empty($notifyparty['notifytele']) ? '' : $notifyparty['notifytele'];
                    $shipment['notifyAEO'] = '';

                    //提前转化为一维数组，可能会出现里面有参数是对象的
                    if(is_array($goods['description']['text']))$goods['description']['text'] = array_map(function($v){
                        if(is_array($v)) $v = join('', $v);
                        return $v;
                    }, $goods['description']['text']);

                    $shipment['containers'] = array();
                    if(is_string($goods['description']['text'])) $goods['description']['text'] = array($goods['description']['text']);
                    if(isset($containers['containers'])) $containers = $containers['containers'];
                    if(isset($containers['containerno'])) $containers = array($containers);
                    foreach ($containers as $container){

                        $shipment['containers'][] = array(
                            'containerno' => $container['containerno'],
                            'sealno' => $container['sealno'],
                            'containertype' => $container['containertype'],
                            'description' => join(',', $goods['description']['text']),
                            'hscode' => join(',', $goodsdetail['hscode']),
                            'containernoofpkgs' => $container['containernoofpkgs'],
                            'containerpackaging' => $container['containerpackaging'],
                            'containergrossweight' => $container['containergrossweight'],
                            'containercbm' => $container['containercbm'],
                            'containernoofpkgs' => $container['containernoofpkgs'],
                        );
                    }
                    $shipments[] = $shipment;
                }
                $down = getValue('down', 0);
                if($down==1){
                    header("Content-type: application/vnd.ms-excel");
                    header("Content-Disposition: attachment; filename=预配舱单导出" . join(' ', array_column($shipments, 'billofladingno')) . ".xls");
                }else{
                    echo "<a href='/bsc_message/file_read/$id?down=1'>excel</a><br><br>";
                }
                $data['shipments'] = $shipments;
                $this->load->view('bsc/message/read/yitong/manifest_view', $data);
                return;
            }
            header("Content-type: text/xml");
            $file_content_html = $file_content;
            echo $file_content_html;
        }

    }
}
