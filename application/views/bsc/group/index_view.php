<script type="text/javascript">
    function doSearch(){
        $('#tt').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val() ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val() ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val()
        });
        $('#chaxun').window('close');
    }

    function add() {
        $('#group_code_name').textbox('readonly', false);
        $('#group_code_num').textbox('readonly', false);
        $('#company_code').textbox('readonly', false);

        $('#dlg').dialog('open').dialog('setTitle', '<?= lang('新增部门');?>');
        $('#fm').form('clear');
        url = '/bsc_group/add_data';
    }

    function add_zb() {
        // $('#group_code_name').textbox('readonly', false);
        // $('#group_code_num').textbox('readonly', false);
        // $('#company_code').textbox('readonly', false);

        $('#zb_dlg').dialog('open').dialog('setTitle', '<?= lang('新增总部');?>');
        $('#zb_fm').form('clear');
        url = '/bsc_group/add_data';
    }

    function add_fgs() {
        // $('#group_code_name').textbox('readonly', false);
        // $('#group_code_num').textbox('readonly', false);
        // $('#company_code').textbox('readonly', false);

        $('#fgs_dlg').dialog('open').dialog('setTitle', '<?= lang('新增分公司');?>');
        $('#fgs_fm').form('clear');
        url = '/bsc_group/add_data';
    }
    
    var is_submit = false;

    function submit() {
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $('#fm').form('submit', {
            url: url,
            onSubmit: function(param){
                var group_code = $('#group_code_prefix').textbox('getValue') + '-' +  $('#group_code_name').textbox('getValue').toUpperCase() + '-' + $('#group_code_num').textbox('getValue');
                param.group_code = group_code;
                var validate = $(this).form('validate');
                if(url == '/bsc_group/add_data' && $('.group_code_title').text() !== ''){
                    $.messager.alert('Tips', '组代码重复');
                    validate = false;
                }
                param.group_name = $('#group_name1').textbox('getValue') + $('#group_name2').textbox('getValue') + $('#group_name3').textbox('getValue');
                if(!validate){
                    ajaxLoadEnd();
                    is_submit = false;
                }
                return validate;
            },
            success:function(res_json){
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                ajaxLoadEnd();
                is_submit = false;
                if(res == undefined){
                    $.messager.alert('Tips', res);
                    return;
                }
                $('#dlg').dialog('close');
                $('#tt').datagrid('reload');
            }
        });
    }

    function zb_submit() {
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $('#zb_fm').form('submit', {
            url: url,
            onSubmit: function(param){
                param.group_type = 'HEAD';
                param.group_code = $('#zb_company_code').textbox('getValue');
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                    is_submit = false;
                }
                return validate;
            },
            success:function(res_json){
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                ajaxLoadEnd();
                is_submit = false;
                if(res == undefined){
                    $.messager.alert('Tips', res);
                    return;
                }
                $('#zb_dlg').dialog('close');
                $('#tt').datagrid('reload');
            }
        });
    }

    function fgs_submit() {
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $('#fgs_fm').form('submit', {
            url: url,
            onSubmit: function(param){
                param.group_type = 'BRANCH';
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                    is_submit = false;
                }
                return validate;
            },
            success:function(res_json){
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                ajaxLoadEnd();
                is_submit = false;
                if(res == undefined){
                    $.messager.alert('Tips', res);
                    return;
                }
                $('#fgs_dlg').dialog('close');
                $('#tt').datagrid('reload');
            }
        });
    }

    function reload_client_group() {
        var row = $('#tt').datagrid('getSelected');
        if (row){
            var group_name = row.group_name; 
            if(group_name.indexOf("财务") == -1){
                alert('财务部门才可以');
                return 0;
            }
            $.messager.confirm('确认对话框', '是否确认授权所有往来单位？', function(r) {
                if (r) {
                    $.ajax({
                        type: 'POST',
                        url: '/biz_client/read_group_by_client',
                        data:{
                            group: row.group_code,
                        },
                        dataType: 'json',
                        success: function (res) {
                            $.messager.alert('Tips', res.msg);
                        }
                    });
                }
            });
        }else{
            $.messager.alert("Tips","No Item Selected");
        }
    }

    function group_type_select(rec){
        var value = rec.value;
        //这里判断是HEAD或分公司,
        if(rec.group_type == 'HEAD'){
            //总部
        }else if(rec.group_type == 'BRANCH'){

        }else{

        }
    }

    function group_code_name_change(newValue, oldValue) {
        var inp = $(this);
        var value = newValue.replace(/[^\a-zA-Z0-9]/g, '').toUpperCase();
        inp.combobox('setValue', value);

        //获取当前选中的哪条数据
        var data = inp.combobox('getData');
        var row = data.filter(el => el['ext1'] === newValue);
        if(row.length > 0) {
            $('#group_type').combobox('setValue', row[0]['value']);
            $('#group_name2').textbox('setValue', row[0]['value']);
        }
    }

    var url = "/bsc_group/add_data";

    $(function () {
        $('#tt').edatagrid({
            url: '/bsc_group/get_data/',
            saveUrl: '/bsc_group/add_data/',
            updateUrl: '/bsc_group/update_data/',
            destroyUrl: '/bsc_group/delete_data',
            onError:function(index, row){
                $.messager.alert('error', row.msg);
            },
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
            onDblClickRow: function (index, row) {
                if(row.group_type == 'HEAD'){
                    $('#zb_company_code').textbox('readonly', true);
                    $('#zb_fm').form('load', row);
                    $('#zb_dlg').dialog('open').dialog('setTitle', '<?= lang('编辑总部');?>');
                }else if(row.group_type == 'BRANCH'){

                    $('#fgs_group_code').textbox('readonly', true);
                    $('#fgs_fm').form('load', row);
                    $('#fgs_dlg').dialog('open').dialog('setTitle', '<?= lang('编辑分公司');?>');
                }else{
                    $('#group_type').textbox('readonly', true);
                    $('#group_code_name').textbox('readonly', true);
                    $('#group_code_num').textbox('readonly', true);
                    $('#company_code').textbox('readonly', true);
                    $('#dlg').dialog('open').dialog('setTitle', '<?= lang('edit');?>');
                    $('#fm').form('load', row);
                    //名字需要单独处理
                    var group_name = row.group_name;
                    var prefit = row.company_name + row.group_type;
                    //首先将company_name 切割出来,剩下的是group_name2
                    $('#group_name1').textbox('setValue', row.company_name);
                    $('#group_name2').textbox('setValue', row.group_type);
                    $('#group_name3').textbox('setValue', group_name.replace(prefit, ''));

                    //code赋值
                    var group_code = row.group_code.split('-');
                    $.each(group_code,function (index,item) {
                        $('.group_code:eq(' + index + ')').textbox('setValue', item);
                    });
                }
                url = '/bsc_group/update_data?id=' + row.id;
            },
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });

        // $('#group_code_name').textbox('textbox').keyup(function () {
        //     var val = $(this).val().toUpperCase();$(this).val(val.replace(/[^\a-zA-Z0-9]/g, ''));
        // });

        $('#group_code_num').textbox('textbox').blur(function () {
            if(url == '/bsc_group/add_data'){
                var val = $('#group_code_prefix').textbox('getValue') + '-' +  $('#group_code_name').textbox('getValue').toUpperCase() + '-' + $('#group_code_num').textbox('getValue');
                $.ajax({
                    url: '/bsc_group/code_isset?group_code=' + val,
                    type: 'GET',
                    dataType: 'json',
                    success: function (res) {
                        if (res.code == 0) {//表示有值
                            $('.group_code_title').css('color', 'red');
                            $('.group_code_title').text('not ok');
                        }else{
                            $('.group_code_title').text('');
                        }
                    }
                });
            }
        });
    });

</script>
<style type="text/css">
    input:read-only,select:read-only,textarea:read-only
    {
        background-color: #d6d6d6;
        cursor: default;
    }
</style>
<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <?php if (!empty($f)) {
            foreach ($f as $rs) {

                $field = $rs[0];
                $disp = $rs[1];
                $width = $rs[2];
                $attr = $rs[4];

                if ($width == "0") continue;

                echo "<th field=\"$field\" width=\"$width\" $attr>" . lang($disp) . "</th>";
            }
        }
        ?>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:add()"><?= lang('新增部门');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:add_fgs()"><?= lang('新增分公司');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:add_zb()"><?= lang('新增总部');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#tt').edatagrid('destroyRow')">delete</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');">查询</a>
                <!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:reload_client_group()"><?= lang('授权所有往来单位');?></a>-->
            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                <?= lang('query');?> 1
            </td>
            <td align="right">
                <select name="f1"  id="f1"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1"  id="s1"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v1" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('query');?> 2
            </td>
            <td align="right">
                <select name="f2"  id="f2"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2"  id="s2"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v2" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('query');?> 3
            </td>
            <td align="right">
                <select name="f3"  id="f3"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s3"  id="s3"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v3" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;"><?= lang('search');?></a>
</div>

<!-- 新增部门 -->
<div id="dlg" class="easyui-dialog" style="background-color: #F4F4F4;border:1px solid #95B8E7;margin-top:10px;"
     data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons'">
    <form id="fm" method="post" novalidate style="margin:0;padding:10px 10px">
        <table>

            <tr>
                <td><?= lang('company_code');?></td>
                <td>
                    <select class="easyui-combotree" name="parent_id" id="parent_id" style="width: 250px" data-options="
                        required: true,
                        url:'/bsc_group/get_tree',
                        onSelect: function(rec){
                            if(rec.group_type == 'BRANCH'){
                                $('#group_code_prefix').textbox('setValue', rec.group_code);
                                $('#bm_company_code').val(rec.group_code);
                                $('#group_name1').textbox('setValue', rec.group_name);
                            }else{
                                $('#group_code_prefix').textbox('setValue', rec.company_code);
                                $('#bm_company_code').val(rec.company_code);
                                //这里赋值给group_name1,
                                $('#group_name1').textbox('setValue', rec.company_name);
                            }

                        },
                    ">
                    </select>
                    <input type="hidden" name="company_code" id="bm_company_code">
                </td>
            </tr>
            <tr>
                <td><?= lang('group_code');?></td>
                <td>
                    <input class="easyui-textbox group_code" id="group_code_prefix" readonly style="width: 60px">-
                    <input class="easyui-combobox group_code" id="group_code_name" style="width: 110px" data-options="
                        required:true,
                        valueField:'ext1',
                        textField:'ext1',
                        url:'/bsc_dict/get_option/group_type',
                        onChange: group_code_name_change,
                    ">-
                    <input class="easyui-textbox group_code" id="group_code_num" required style="width: 65px">
                    <br/><span class="group_code_title"></span>
                </td>
            </tr>
            <tr>
                <td><?= lang('group_type');?></td>
                <td>
                    <select class="easyui-combobox" name="group_type" id="group_type" readonly style="width: 250px" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        url:'/bsc_dict/get_option/group_type',
                        onSelect:group_type_select,
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('group_name');?></td>
                <td>
                    <input class="easyui-textbox" id="group_name1" readonly required style="width: 80px">
                    <input class="easyui-textbox" id="group_name2" readonly required style="width: 80px">
                    <input class="easyui-textbox" id="group_name3" style="width: 85px">
<!--                    <input class="easyui-textbox" name="group_name" id="group_name" required style="width: 200px">-->
                </td>
            </tr>
            <tr>
                <td><?= lang('status');?></td>
                <td>
                    <input type="radio" name="status" id="status_0" value="0" ><label for="status_0">正常</label>
                    <input type="radio" name="status" id="status_1" value="1" ><label for="status_1">禁用</label>
                </td>
            </tr>
        </table>
    </form>
    <div id="dlg-buttons">
        <a href="javascript:submit()" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px"><?= lang('save');?></a>
    </div>
</div>

<!-- 新增HEAD -->
<div id="zb_dlg" class="easyui-dialog" style="background-color: #F4F4F4;border:1px solid #95B8E7;margin-top:10px;" data-options="closed:true,modal:true,border:'thin',buttons:'#zb_dlg-buttons'">
    <form id="zb_fm" method="post" novalidate style="margin:0;padding:10px 10px">
        <table>
            <tr>
                <td><?= lang('group_name');?></td>
                <td><input class="easyui-textbox" name="group_name" required style="width: 200px"></td>
            </tr>
            <tr>
                <td><?= lang('group_code');?></td>
                <td>
                    <input class="easyui-textbox" name="company_code" id="zb_company_code" required style="width: 200px" >
                </td>
            </tr>
            <tr>
                <td><?= lang('status');?></td>
                <td>
                    <input type="radio" name="status" id="zb_status_0" value="0" ><label for="zb_status_0">正常</label>
                    <input type="radio" name="status" id="zb_status_1" value="1" ><label for="zb_status_1">禁用</label>
                </td>
            </tr>
        </table>
    </form>
    <div id="zb_dlg-buttons">
        <a href="javascript:zb_submit()" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px"><?= lang('save');?></a>
    </div>
</div>
<!-- 新增分公司 -->
<div id="fgs_dlg" class="easyui-dialog" style="background-color: #F4F4F4;border:1px solid #95B8E7;margin-top:10px;" data-options="closed:true,modal:true,border:'thin',buttons:'#fgs_dlg-buttons'">
    <form id="fgs_fm" method="post" novalidate style="margin:0;padding:10px 10px">
        <table>
            <tr>
                <td><?= lang('company_code');?></td>
                <td>
                    <select class="easyui-combotree" name="parent_id" style="width: 200px" data-options="
                        required: true,
                        url:'/bsc_group/get_tree?group_type=HEAD',
                        onSelect: function(rec){
                            $('#fgs_company_code').val(rec.company_code);
                        },
                    ">
                    </select>
                    <input type="hidden" name="company_code" id="fgs_company_code">
                </td>
            </tr>
            <tr>
                <td><?= lang('group_name');?></td>
                <td><input class="easyui-textbox" name="group_name" required style="width: 200px"></td>
            </tr>
            <tr>
                <td><?= lang('group_code');?></td>
                <td>
                    <input class="easyui-textbox" name="group_code" id="fgs_group_code" required style="width: 200px">
                </td>
            </tr>
            <tr>
                <td><?= lang('status');?></td>
                <td>
                    <input type="radio" name="status" id="fgs_status_0" value="0" ><label for="fgs_status_0">正常</label>
                    <input type="radio" name="status" id="fgs_status_1" value="1" ><label for="fgs_status_1">禁用</label>
                </td>
            </tr>
        </table>
    </form>
    <div id="fgs_dlg-buttons">
        <a href="javascript:fgs_submit()" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px"><?= lang('save');?></a>
    </div>
</div>