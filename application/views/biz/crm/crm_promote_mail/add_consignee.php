<!--add_consignee-->
<div class="easyui-panel" title="增加收件人">
    <div style="padding:10px 60px 20px 60px">
        <form id="ff" method="post">

            <input type="hidden" name="group_id"
                   value="<?php echo $group_id ?>"/>
            <table cellpadding="5">
                <tr>
                    <td>收件人组名:</td>
                    <td>
                        <input class="easyui-textbox" type="text" name="group_name"
                               data-options="required:true," disabled="disabled" value="<?php echo $group_name ?>"/>
                    </td>
                </tr>
                <tr>
                    <td>姓名:</td>
                    <td><input class="easyui-textbox" type="text" name="name"
                               data-options="required:true"/></td>
                </tr>
                <tr>
                    <td>邮件地址:</td>
                    <td><input class="easyui-textbox" type="text" name="email"
                               data-options="required:true,validType:'email'"/></td>
                </tr>
                <tr>
                    <td>性别:</td>
                    <td>
                        <input type="radio" name="gender" checked="checked" value="男">男
                        <input type="radio" name="gender" value="女">女

                    </td>
                </tr>
                <tr>
                    <td>手机号:</td>
                    <td><input class="easyui-textbox" type="text" name="phone"
                        /></td>
                </tr>


                </tr>
                <tr>
                    <td></td>
                    <td><a href="javascript:void(0)" class="easyui-linkbutton" style="padding: 0px 16px 0px 16px"
                           onclick="subForm()">确定</a> 
                           <a href="javascript:void(0)" class="easyui-linkbutton" style="padding: 0px 16px 0px 16px"
                           onclick="history.back();">返回</a></td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script type="text/javascript" src="/inc/layer3.1.1/layer.js"></script>
<script>
    function subForm() {
        var d = {};
        var formdata = $("#ff").serializeArray();

        $.each(formdata, function () {
            d[this.name] = this.value;
        });

        $.post("/crm_promote_mail/add_consignee_data", formdata, function (data) {
            if (data.code == 200) {
                layer.msg(data.msg);
                setTimeout(function () {
                    window.location.href = data.url;
                }, 2000);

            } else {

                layer.msg(data.msg);

            }

        }, "JSON");
    }
</script>
