<?php

class Biz_download_file_model extends CI_Model
{

    private $table = 'biz_download_file';

    public function __construct()
    {
        $this->load->database();
    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function get_where_one($where = '')
    {
        if(!empty($where)) $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function update($id = 0, $data = array())
    {
        $data["updated_time"] = date('Y-m-d H:i:s');
        $data["updated_by"] =  $this->session->userdata('id');
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }


    public function save($data=array()){

        $data["created_time"] = date('Y-m-d H:i:s');
        $data["updated_time"] = date('Y-m-d H:i:s');
        $data["created_by"] =  $this->session->userdata('id');
        $data["updated_by"] =  $this->session->userdata('id');
        $this->db->insert($this->table, $data);

        return $this->db->insert_id();

    }

    public function mdelete($id = '')
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    /**
     * 判断当前节点是否是指定节点的子节点
     * @param $node
     * @param $parentNode
     * @return bool
     */
    public function isChildNode($node, $parentNode){
        if($node['lft'] > $parentNode['lft'] && $node['rgt'] < $parentNode['rgt']) return true;
        else return false;
    }

    /**
     * 获取子节点数量
     * @param $node array
     * @return float|int
     */
    public function getChildNodeNumber($node){
        return ($node['rgt'] - $node['lft'] - 1) / 2;
    }

    /**
     * 获取所有相关的父节点
     * @param $nodeId
     * @return mixed
     */
    public function getSinglePath($nodeId){
        $sql = "select parent.* from {$this->table} as node,{$this->table} as parent where node.lft between parent.lft AND parent.rgt AND node.id = {$nodeId} order by parent.lft";
        $rs = $this->db->query($sql);
        return $rs->result_array();
    }
}