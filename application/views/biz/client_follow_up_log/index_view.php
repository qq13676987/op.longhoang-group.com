<script type="text/javascript" src="/inc/third/wangEditor/wangEditor.min.js"></script>
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<script type="text/javascript" src="/inc/third/layui/layui.js"></script>
<style>
    /**
    * 修改datagrid表格样式
    **/
    .datagrid-body td {height: 40px !important;}
    .datagrid-header, .datagrid-htable {height: 40px !important; background: #eceff0; border-bottom: none;}
    .datagrid-header-row td{border-width: 0px 1px 1px 0px; border-style: solid; border-color: #ddd;}
    .panel-body-noheader {border-top:0px !important;}
    .tabs {padding-left:0px;}
    .tabs-inner {height: 25px !important;}
    .tabs li.tabs-selected a.tabs-inner {border-bottom: 1px solid #F4F4F4;}
    .tabs-header {background: none;}
    .tabs li a.tabs-inner{padding: 0px 20px;}
    .tabs li.tabs-selected a.tabs-inner{background: #F4F4F4}
    .datagrid-row-over {background: #eceeeb;}
    .datagrid-header td.datagrid-header-over {background:#e5e7e8;color: #000000;cursor: default;}
    .layui-layer-lan .layui-layer-title {background: #325763 !important;}
    #cx table tr td {height: 30px; min-width: 50px;}
    #cx table tr td button {width: 23px;}
</style>
<script type="text/javascript">
    function doSearch(){
        $('#tt').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val() ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val() ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val()
        });
        $('#chaxun').window('close');
    }

    function del() {
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            //if(row.step!="未收款"){
            //	alert("不可以删除");
            //}else{
            //$('#tt').edatagrid('destroyRow');
            //}
            //增加一个手动输入的delete验证
            $.messager.prompt('<?= lang('提示');?>', '<?= lang('确认删除?请输入delete进行确认');?>', function (r) {
                if(r === 'delete'){
                    $('#tt').edatagrid('destroyRow');
                }
            });
        } else {
            alert("No Item Selected");
        }
    }

    function add(){
        parent.layer.open({
            type: 2,
            title: '<?= lang('添加日志');?>',
            shade: false,
            area: ['900px', '600px'],
            maxmin: false,
            shade: 0.5,
            scrollbar: false,
            shadeClose: true,
            content: '/biz_client_follow_up_log/add/?crm_id=<?= $crm_id;?>',
            btn: ['<?= lang('保存');?>', '<?= lang('取消');?>'],
            yes: function (e, layero) {
                var l = parent.window["layui-layer-iframe" + e]
                    , r = "LAY-module-submit"
                    , n = layero.find("iframe").contents().find("#LAY-module-submit");
                l.layui.form.on("submit(" + r + ")", function (f) {
                    var field = f.field;
                    $.ajax({
                        type: 'post',
                        url: '/biz_client_follow_up_log/add_data/?crm_id=<?= $crm_id;?>',
                        data: field,
                        success: function (data) {
                            if (data.code == 1) {
                                parent.layer.msg(data.msg, {
                                    icon: 1,
                                    shade: [0.5, '#393D49'],
                                }, function () {
                                    $('#tt').datagrid('reload');
                                    parent.layer.close(e);
                                });
                            } else {
                                parent.layer.alert(data.msg, {title: '<?= lang('系统信息');?>', icon: 5});
                            }

                        },
                        error: function (data) {
                            parent.layer.msg('<?= lang('请求失败，错误代码：');?>' + data.status);
                        },
                        dataType: "json"
                    });

                });
                n.trigger("click");
            }
        });
    }

    function open_content(id){
        $('#content_iframe').attr('src', '/biz_quotation_demand/noat_content/' + id + '/biz_client_follow_up_log');
        $('#content_div').window('open');
    }

    const E = window.wangEditor;
    var add_content_div,$add_content_textarea,edit_content_div,$edit_content_textarea;

    $(function () {
        add_content_div = new E('#add_content_div');
        $add_content_textarea = $('#add_content_textarea');
        edit_content_div = new E('#edit_content_div');
        $edit_content_textarea = $('#edit_content_textarea');

        add_content_div.config.onchange = function (html) {
            // 第二步，监控变化，同步更新到 textarea
            $add_content_textarea.val(html)
        };
        add_content_div.config.uploadImgServer = '/bsc_upload/file_upload';
        add_content_div.config.uploadImgParams = {
            is_save_data: 0,
        };
        add_content_div.config.uploadFileName = 'file';
        add_content_div.create();

        // 第一步，初始化 textarea 的值
        $add_content_textarea.val(add_content_div.txt.html());

        edit_content_div.config.onchange = function (html) {
            // 第二步，监控变化，同步更新到 textarea
            $edit_content_textarea.val(html)
        };
        // edit_content_div.config.uploadImgShowBase64 = true;
        edit_content_div.config.uploadImgServer = '/bsc_upload/file_upload';
        edit_content_div.config.uploadImgParams = {
            is_save_data: 0,
        };
        edit_content_div.config.uploadFileName = 'file';
        edit_content_div.create();

        // 第一步，初始化 textarea 的值
        $edit_content_textarea.val(edit_content_div.txt.html());
        layui.use(['layer', 'form'], function() {
            $('#tt').edatagrid({
                url: '/biz_client_follow_up_log/get_data/?crm_id=<?= $crm_id;?>',
                destroyUrl: '/biz_client_follow_up_log/delete_data',
                height: $(window).height(),
                onError: function (index, data) {
                    $.messager.alert('error', data.msg);
                },
                onDblClickRow: function (index, row) {
                    parent.layer.open({
                        type: 2,
                        title: '<?= lang('编辑日志');?>',
                        shade: false,
                        area: ['900px', '600px'],
                        maxmin: false,
                        shade: 0.5,
                        scrollbar: false,
                        shadeClose:true,
                        content: '/biz_client_follow_up_log/edit?id=' + row.id,
                        btn: ['<?= lang('保存');?>', '<?= lang('取消');?>'],
                        yes: function (e, layero) {
                            var l = parent.window["layui-layer-iframe" + e]
                                , r = "LAY-module-submit"
                                , n = layero.find("iframe").contents().find("#LAY-module-submit");
                            l.layui.form.on("submit(" + r + ")", function (f) {
                                var field = f.field;
                                $.ajax({
                                    type: 'post',
                                    url: '/biz_client_follow_up_log/update_data',
                                    data: field,
                                    success: function (data) {
                                        if (data.code == 1) {
                                            parent.layer.msg(data.msg, {
                                                icon: 1,
                                                shade: [0.5, '#393D49'],
                                            }, function () {
                                                $('#tt').datagrid('reload');
                                                parent.layer.close(e);
                                            });
                                        } else {
                                            parent.layer.alert(data.msg, {title: '<?= lang('系统信息');?>', icon: 5});
                                        }

                                    },
                                    error: function (data) {
                                        parent.layer.msg('<?= lang('请求失败，错误代码：');?>' + data.status);
                                    },
                                    dataType: "json"
                                });

                            });
                            n.trigger("click");
                        }
                    });
                }
            });
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height()
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });

    function type_for(value) {
        if (value == 1){
            return '<?= lang('询价跟进');?>';
        }else if (value == 2){
            return '<?= lang('线下跟进');?>';
        }else{
            return '<?= lang('电话跟进');?>';
        }
    }

</script>

<table id="tt" style="width:1100px;height:450px" rownumbers="false" pagination="true" idField="id" pagesize="1000" toolbar="#tb"  nowrap="false">
    <thead>
    <tr>
        <?php if (!empty($f)) {
            foreach ($f as $rs) {
                $field = $rs[0];
                $disp = $rs[1];
                $width = $rs[2];
                $attr = $rs[4];
                if ($width == "0") continue;
                echo "<th field=\"$field\" width=\"$width\" data-options='$attr'>" . lang($disp) . "</th>";
            }
        }
        ?>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('查询');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()"><?= lang('新增');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="del()"><?= lang('删除');?></a>
            </td>
            <td width='80'></td>
            <td style="color: #cf2d28"><?= lang('电话跟进占比：{num}%', array('num' => $type_1_proportion));?></td>
            <td width='10'></td>
            <td style="color: #cf2d28"><?= lang('线下跟进占比：{num}%', array('num' => $type_2_proportion));?></td>
            <td width='10'></td>
            <td style="color: #cf2d28"><?= lang('询价跟进占比：{num}%', array('num' => $type_0_proportion));?></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                <?= lang('查询');?> 1
            </td>
            <td align="right">
                <select name="f1"  id="f1"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>" . lang($item) . "</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1"  id="s1"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v1" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('查询');?> 2
            </td>
            <td align="right">
                <select name="f2"  id="f2"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>" . lang($item) . "</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2"  id="s2"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v2" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('查询');?> 3
            </td>
            <td align="right">
                <select name="f3"  id="f3"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>" . lang($item) . "</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s3"  id="s3"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v3" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;"><?= lang('查询');?></a>
</div>

<div id="_window" class="table" style="display: none; width: 100%">
    <div id="handsontable_box"></div>
</div>

<div id="add_div" class="easyui-window" title="<?= lang('新增');?>" closed="true" style="width:1100px;height:550px;padding:5px;">
    <form id="add_form" method="post">
        <table>
            <tr>
                <td><?= lang('content');?></td>
                <td>
                    <div id="add_content_div"></div>
                    <textarea name="content" id="add_content_textarea" style="display: none;"></textarea>
                </td>
            </tr>
            <tr>
                <td><?= lang('remind_time');?></td>
                <td>
                    <input class="easyui-datetimebox" name="remind_time" style="width: 255px;">
                </td>
            </tr>
            <tr>
                <td><?= lang('follow_date');?></td>
                <td>
                    <input class="easyui-datebox" name="follow_date" style="width: 255px;" required>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button type="button" onclick="add_save()"><?= lang('save');?></button>
                </td>
            </tr>
        </table>
    </form>
</div>

<div id="update_div" class="easyui-window" title="<?= lang('编辑');?>" closed="true" style="width:1100px;height:550px;padding:5px;">
    <form id="update_form" method="post">
        <input type="hidden" name="id">
        <table>
            <tr>
                <td><?= lang('content');?></td>
                <td>
                    <div id="edit_content_div"></div>
                    <textarea name="content" id="edit_content_textarea" style="display: none;"></textarea>
                </td>
            </tr>
            <tr>
                <td><?= lang('remind_time');?></td>
                <td>
                    <input class="easyui-datetimebox" name="remind_time" style="width: 255px;">
                </td>
            </tr>
            <tr>
                <td><?= lang('follow_date');?></td>
                <td>
                    <input class="easyui-datebox" name="follow_date" style="width: 255px;" required>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button type="button" onclick="update_save()"><?= lang('保存');?></button>
                </td>
            </tr>
        </table>
    </form>
</div>
