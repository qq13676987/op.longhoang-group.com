<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>导入CRM线索</title>
    <meta name="keywords" content="">
    <meta name="viewport" content="width=1180">
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css"/>
    <script type="text/javascript" src="/inc/third/layer/layer.js"></script>
    <link rel="stylesheet" href="/inc/third/handsontable/handsontable.full.min.css"/>
    <script type="text/javascript" src="/inc/third/handsontable/handsontable.full.min.js"></script>
    <style>
        .search_box {
            border: 1px solid #ddd;
            float: left;
            margin: 10px 0px 0px;
            padding: 15px;
            background-color: #edf2f3;
            width: 99%;
        }
        .selected-td{ background: #ff4c42 !important; }
        .layui-this{font-size: 18px !important; font-weight: bolder}
    </style>
</head>
<body>
<div class="layui-tab layui-tab-brief"  style="margin-left: 20px;">
    <ul class="layui-tab-title">
        <li class="layui-this">导入线索</li>
        <li >管理港口别名</li>
    </ul>
    <div class="layui-tab-content" style="padding-left: 0px;">
        <div class="layui-tab-item layui-show">
            <div style="margin-bottom: 80px;">
                <div id="handsontable_box" style="width: 99%"></div>
            </div>
            <div id="save_tools" style="display:; width: 100%; text-align: center; border-top: 1px solid #008a6b; padding: 20px 0px;position: fixed; z-index: 999999; bottom: 0px; left: 0px; background-color: white;">
                <button class="layui-btn layui-btn-primary" id="getCountryCode" style=" width: 150px;">补充国家代码</button>
                <button class="layui-btn" id="import_btn" style=" width: 300px;">确认导入线索</button>
            </div>
        </div>
        <!--港口别名列表-->
        <div class="layui-tab-item">
            <div class="layui-card-body">
                <div class="test-table-reload-btn" style="margin-bottom: 10px;">
                    搜索：
                    <div class="layui-inline">
                        <select name="field" id="field" class="easyui-combobox" style="height: 35px">
                            <option value="">请选择搜索列</option>
                            <option value="port_name">港口别名</option>
                            <option value="port_code">港口代码</option>
                        </select>
                    </div>
                    <div class="layui-inline">
                        <input class="easyui-textbox" id="value" style="width:200px;height: 35px;">
                    </div>
                    <button class="layui-btn" id="summit" data-type="reload">搜索</button>
                    <button type="button" id="port_reset" class="layui-btn layui-btn-primary">重置</button>
                </div>
                <table class="layui-hide" id="general-table-port" lay-filter="general-table-port"></table>

                <script type="text/html" id="row-toolbar-del">
                    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>
                </script>
            </div>
        </div>
    </div>
</div>
<!--创建港口别名-->
<div id="_window" class="table" style="display: none; width: 100%">
    <div id="port_box"></div>
</div>

<!--进度条-->
<div id="progress" style=" display: none;">
    <div style="color: red; height: 35px; line-height: 30px; padding: 10px 25px;">Tips：导入过程中请不要关闭或刷新窗口，否则导入进程会被打断！</div>
    <div class="layui-progress layui-progress-big" lay-filter="demo" lay-showPercent="true"
         style="margin:0px 20px;">
        <div class="layui-progress-bar layui-bg-green" lay-percent="0%"></div>
    </div>
</div>
</body>
<?php
if (isset($_POST['import_data']) && $_POST['import_data'] != ''){
    $default_data = base64_decode($_POST['import_data']);
}else{
    $default_data = '[{},{},{}]';
}
?>
<script src="/inc/third/layuiadmin/layui/layui.js"></script>
<script src="/inc/js/importExcel.js?time=<?php echo time();?>"></script>
<script type="text/javascript">
    var import_data = false;
    layui.config({
        base: '/inc/third/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index',
    }).use(['index', 'element', 'upload', 'form', 'table'], function () {
        var element = layui.element;
        var table = layui.table;
        //选项卡默认选中标签
        element.tabChange('test1', '111');

        //渲染港口别名表格
        var tableIns2 = table.render({
            elem: '#general-table-port'
            , url: '/biz_crm/get_port_data'
            //,toolbar: '#row-toolbar'
            , title: '用户数据表'
            , width: 880
            , defaultToolbar: []
            , cols: [[
                //{type:'checkbox'}
                {field: 'id', width: 100, title: 'ID', sort: true, align: 'center'}
                , {field: 'port_name', width: 200, title: '港口别名', align: 'center', edit: "text"}
                , {field: 'port_code', width: 200, title: '港口代码', sort: true, align: 'center', edit: "text"}
                , {field: 'name', title: '创建人', minWidth: 150, align: 'center', edit: "text"}
                , {title: '操作', toolbar: '#row-toolbar-del', width: 150, align: 'center'}
            ]]
            ,page: true
            ,limit:20
        });

        //搜索
        $('#summit').on('click', function(){
            var field = $('#field').combobox('getValue');
            var value = $('#value').val();
            if (field == '' && value == '') return;
            tableIns2.reload({
                where: { //设定异步数据接口的额外参数，任意设
                    field: field
                    ,val: value
                    //…
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
            });
        });

        //重置
        $('#port_reset').on('click', function(){
            tableIns2.reload({
                where: {field: ''}
                ,page: {curr: 1 }
            });
        });

        //快速编辑文本列
        table.on('edit(general-table-port)', function (obj) {
            var value = obj.value //得到修改后的值
                , data = obj.data //得到所在行所有键值
                , field = obj.field; //得到字段
            var loads = layer.load(1, {time: 10000});
            $.ajax({
                type: 'post',
                url: '/biz_quotation/port_alias_saveField',
                data: {
                    field: field,
                    value: value,
                    keyid: data.id
                },
                success: function (data) {
                    layer.close(loads);
                    if (data.code == 0) {
                        layer.msg(data.msg,{icon:7});
                    }
                },
                error:function(data){
                    layer.close(loads),
                        layer.msg('请求失败，错误代码：'+ data.status);
                },
                dataType: "json"
            });
        });
        //删除行
        table.on('tool(general-table-port)', function(obj){ //注：tool 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            var data = obj.data; //获得当前行数据
            var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var id = data.id;

            if(layEvent === 'delete'){ //删除
                layer.confirm("确定删除吗？此操作不可逆!",{icon:7,title:'你正在执行危险操作！'}, function (index) {
                    $.ajax({
                        type: 'post',
                        url: '/biz_quotation/del_port_alias',
                        data: {
                            id: id
                        },
                        success: function (data) {
                            if (data.code == 1) {
                                layer.msg('删除成功。', {icon: 1,shade: [0.5, '#606673'],time:1000}, function () {
                                    obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                                    layer.close(index);
                                    tableIns.reload();
                                });
                            } else {
                                layer.alert('删除失败。', {icon: 5});
                            }
                        },
                        error:function(data){
                            layer.msg('请求失败，错误代码：'+ data.status);
                        },
                        dataType: "json"
                    });
                });
            }
        });

        //保存表格里的数据
        $('#import_btn').on('click', function () {
            var data = [];
            var param = hot.getData();
            import_data = true;
            $.each(param, function (index, item) {
                if (item[0].trim() != '' && item[1].trim() == ''){
                    import_data = false;
                    layer.alert('请先补充完国家代码！', {icon: 5});
                    return false;
                }
            });
            if (import_data == true) {
                var columns = ['clue', 'country_code', 'customer', 'shipper', 'consignee', 'notify', 'pol', 'pod', 'etd', 'vessel', 'product_details', 'bill_no', 'remark'];
                //重组数据加入键名
                for (i = 0; i < param.length; i++) {
                    var temp= {}
                    $.each(columns, function (index, val) {
                        temp[val] = param[i][index];
                    })
                    data.push(temp);
                }

                //初始化进度条
                layui.element.progress('demo', '0%');
                //载入导入进程
                importExcel.init({
                    'data': data,
                    'importUrl': '/biz_crm/import_data',
                    'columns': columns
                })
            }else{
                layer.alert('请先补充完国家代码！', {icon: 5});
            }
        });
    });

    $(function () {
        var columns = [
            {data: 'clue', type: 'text', className: "htLeft", width: 300},
            {data: 'country_code', type: 'text', className: "htLeft", width: 120},
            {data: 'customer', type: 'text', className: "htLeft", width: 200},
            {data: 'shipper', type: 'text', className: "htLeft", width: 200},
            {data: 'consignee', type: 'text', className: "htLeft", width: 200},
            {data: 'notify', type: 'text', className: "htLeft", width: 200},
            {data: 'pol', type: 'text', className: "htCenter", width: 150},
            {data: 'pod', type: 'text', className: "htCenter", width: 150},
            {data: 'etd', type: 'text', className: "htLeft", width: 150},
            {data: 'vessel', type: 'text', className: "htLeft", width: 150},
            {data: 'product_details', type: 'text', className: "htCenter", width: 150},
            {data: 'bill_no', type: 'text', className: "htCenter", width: 180},
            {data: 'remark', type: 'text', className: "htCenter", width: 100},
        ];
        var colHeaders = ['clue', 'country_code', 'customer','Shipper','Consignee', 'Notify', 'POL', 'POD', 'ETD', 'vessel', 'product_description', 'bill_no', 'remark'];
        var settingForHandsontable = {
            columns: columns, //列配置参数
            data: <?=$default_data;?>,//为需要绑定数据集合
            licenseKey: 'non-commercial-and-evaluation',
            colHeaders: colHeaders, // 表头
            rowHeaders: true,
            dropdownMenu: true, //忘了
            fillHandle: true,
            contextMenu: true,//右键显示更多功能,
            filters: true,
            startRows: 0,
            startCols: 0,
            afterChange : function(change,source) {
                if (source === 'loadData') return;
                //获取行号
                var row = change[0][0];
                var this_col_name = change[0][1];
                if ('import_name' == this_col_name) {
                    //值发生改变后去除高亮
                    var className = hot.getCellMeta(row, 0).className;
                    if (className && className.lastIndexOf('selected-td') > 0) {
                        hot.setCellMeta(row, 0, 'className', 'htCenter htAutocomplete');
                        hot.render();
                    }
                }
            },
        };
        var container = document.getElementById('handsontable_box');
        hot = new Handsontable(container, settingForHandsontable);
        setTimeout(function () {
            $('#getCountryCode').click();
        }, 500);

        //填充国家代码
        $('#getCountryCode').on('click', function () {
            var loading = layer.load(1);
            var field = {};

            field.list_data = [];
            var param = hot.getData();
            var columns = ['clue', 'country_code', 'customer','shipper', 'consignee', 'notify', 'pol', 'pod', 'etd', 'vessel', 'product_details', 'bill_no', 'remark'];
            $.each(param, function (index, item) {
                field.list_data[index] = JSON.stringify(item);
            });
            field.columns = columns;
            $.ajax({
                type: 'post',
                url: '/biz_crm/get_country_code',
                data: field,
                success: function (data) {
                    layer.close(loading);
                    switch (data.code) {
                        case 1:
                            //载入新数据
                            hot.loadData(data.list_data);
                            import_data=true;
                            break;
                        case 2:
                            layer.alert('因有些港口匹配不到对应的五字代码，国家代码获取失败，请补齐五字代码后再尝试填充！', {icon: 5}, function (index) {
                                addPortCode(data.list_data);
                                layer.close(index);
                            });
                            break;
                        default:
                            layer.alert(data.msg, {icon: 5});
                    }
                },
                error: function (data) {
                    layer.close(loading);
                    layer.alert('请求失败，错误代码：' + data.status, {icon: 5});
                },
                dataType: "json"
            });
        });
    });

    /**
     * @title 打开添加港口窗口
     */
    function addPortCode(data) {
        layer.open({
            type: 1,
            title: '添加港口五字代码',
            shade: false,
            area: ['500px', '500px'],
            maxmin: false,
            shade: 0.5,
            scrollbar: false,
            content: $('#_window'),
            btn: ['保存', '取消'],
            success: function (layero) {
                var columns = [
                    {data: 'port_name', type: 'text', className: "htCenter", width: 200},
                    {data: 'port_code', type: 'text', className: "htCenter", width: 120},
                    {data: 'carrier_name', type: 'text', className: "htCenter", width: 130}
                ];
                var table_data = data;
                var colHeaders = ['港口名称', '五字代码', '创建人'];
                $("#port_box").handsontable({
                    columns: columns, //列配置参数
                    data: table_data,//为需要绑定数据集合
                    licenseKey: 'non-commercial-and-evaluation',
                    colHeaders: colHeaders, // 表头
                    rowHeaders: true,
                    dropdownMenu: true, //忘了
                    fillHandle: true,
                    //contextMenu: true,//右键显示更多功能,
                });
            },
            yes: function (index, layero) {
                savePortCode();
            }
        });
    }

    /**
     * 保存港口
     */
    function savePortCode() {
        var param = $("#port_box").handsontable('getData');
        var list_data = {};
        //表格内列表数据
        $.each(param, function (index, item) {
            list_data[index] = item;
        });
        $.ajax({
            type: 'post',
            url: '/Biz_crm/savePortCode',
            data: {data: list_data},
            success: function (data) {
                if (data.code == 1) {
                    layer.msg('保存成功。', {icon: 1}, function (e) {
                        layer.closeAll();
                    });
                } else {
                    layer.alert(data.msg, {icon: 5});
                }
            },
            error: function (data) {
                layer.alert('请求失败，错误代码：' + data.status, {icon: 5});
            },
            dataType: "json"
        });
    }
</script>