<style type="text/css">
    .f{
        width:115px;
    }
    .s{
        width:100px;
    }
    .v{
        width:120px;
    }    
    .del_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }

</style>
<script type="text/javascript">
	var data = {};
    function doSearch(){
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        $('#tt').datagrid('load', json);
        set_config();
		$('#chaxun').window('close');
    }
    
    function set_config() {
        var f_input = $('.f');
        var count = f_input.length;
        var config = [];
        $.each(f_input, function (index, item) {
            var c_array = [];
            c_array.push($('.f:eq(' + index + ')').combobox('getValue'));
            c_array.push($('.s:eq(' + index + ')').combobox('getValue'));
            config.push(c_array);
        });
        // var config_json = JSON.stringify(config);
        $.ajax({
            type: 'POST',
            url: '/sys_config/save_config_search',
            data:{
                table: 'bsc_message',
                count: count,
                config: config,
            },
            dataType:'json',
            success:function (res) {

            },
        });
    }
    
    function status_for(value, row, index) {
        var str;
        if(value == 0){
            str = '未提交';
        }else{
            str = '已提交';
        }
        return str;
    }
    function operation_for(value, row, index) {
        var str = "";
             
        str += '<a href="/upload/message/' + row.receiver + '_EDI/' + row.type + row.file_path + '" download="' + row.job_no + '.txt">下载报文</a> |';
        str += '<a href="/bsc_message/file_read/' + row.id + '" target="_blank">查看报文</a>';
        return str;
    }
    
    function ajax_get(url,name) {
        return $.ajax({
            type:'POST',
            url:url,
            dataType:'json',
            success:function (res) {
                data[name] = res;
            },
            error:function () {
                // $.messager.alert('获取数据错误');
            }
        });
    }
    
    var setting = {
         'trans_carrier':['trans_carrier','client_code','client_name'],
    };

    var datebox = [];

    var datetimebox = [];

    $(function () {
        $('#tt').edatagrid({
            url: '/bsc_message/get_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            },
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),

        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });

        var a1 = ajax_get('/biz_client/get_option/carrier', 'trans_carrier');//承运人
        $('#cx table').on('click' , '.add_tr', function () {
            var index = $('.f').length;
            var str = '<tr><td><?= lang('query');?></td><td align="right">' +
                    '<select name="field[f][]" class="f easyui-combobox"  required="true" data-options="\n' +
                    '                            onChange:function(newVal, oldVal){console.log(111);\n' +
                    '                                var index = $(\'.f\').index(this);\n' +
                    '                                if(setting.hasOwnProperty(newVal) === true){\n' +
                    '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                    '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\' >\');\n' +
                    '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                    '                                    $(\'.v:eq(\' + index + \')\').combobox({\n' +
                    '                                        data:data[setting[newVal][0]],\n' +
                    '                                        valueField:setting[newVal][1],\n' +
                    '                                        textField:setting[newVal][2],\n' +
                    '                                    });\n' +
                    '                                }else if ($.inArray(newVal, datebox) !== -1){\n' +
                    '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                    '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datebox\\\' >\');\n' +
                    '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                    '                                }else if ($.inArray(newVal, datetimebox) !== -1){\n' +
                    '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                    '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datetimebox\\\' >\');\n' +
                    '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                    '                                }else{\n' +
                    '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                    '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\' >\');\n' +
                    '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                    '                                }\n' +
                    '                            }\n' +
                    '                        ">' +
                    ' <option value="job_no"><?= lang('job_no');?></option>' +
                    '<option value="trans_carrier"><?= lang('trans_carrier');?></option>' + 
                    "<?php foreach($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>" . lang($item) . "</option>";
                    }?>" +
                    '</select> &nbsp; <select name="field[s][]" class="s easyui-combobox"  required="true"  data-options="\n' +
                    '">' +
                    '<option value="like">like</option> <option value="=">=</option> <option value=">=">>=</option> <option value="<="><=</option> <option value="!=">!=</option> </select> &nbsp; ' +
                    '<div class="this_v"><input name="field[v][]" class="v easyui-textbox"></div>' +
                    '<button class="del_tr" type="button">-</button> </td> </tr>';
            $('#cx table tbody').append(str);
            $.parser.parse($('#cx table tbody tr:last-child'));
            return false;
        });
        
        //删除按钮
        $('#cx table').on('click' , '.del_tr', function () {
            var index = $(this).parents('tr').index();

            $(this).parents('tr').remove();
            return false;
        });
         //初始判断count多少,自动调用该方法--start
        $.ajax({
            type: 'GET',
            url: '/sys_config/get_config_search/bsc_message',
            dataType: 'json',
            success:function (res) {
                // var config = $.parseJSON(res['data']);
                var config = res.data;
                if(config.length > 0){
                    var count = config[0];
                    $('.f:last').combobox('setValue', config[1][0][0]);
                    $('.s:last').combobox('setValue', config[1][0][1]);
                    for (var i = 1; i < count; i++){
                        $('.add_tr:eq(0)').trigger('click');
                        $('.f:last').combobox('setValue', config[1][i][0]);
                        $('.s:last').combobox('setValue', config[1][i][1]);
                    }
                }
            }
        });
        $.when(a1).done(function () {
            $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                var ec_value = $(ec_item).combobox('getValue');
                $(ec_item).combobox('clear').combobox('select', ec_value);
            });
        });
        //初始判断count多少,自动调用该方法--end
    });
</script>

<table id="tt" style="width:1100px;height:450px" rownumbers="false" pagination="true" idField="id" pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <?php if (!empty($f)) {
            foreach ($f as $rs) {

                $field = $rs[0];
                $disp = $rs[1];
                $width = $rs[2];
                $attr = isset($rs[4]) ? $rs[4] : '';

                if ($width == "0") continue;
                if($field == 'consol_id'){
                    echo "<th field=\"job_no\" width=\"$width\" $attr>" . lang('CONSOL') . "</th>";
                    echo "<th field=\"trans_carrier_name\" width=\"$width\" $attr>" . lang('trans_carrier') . "</th>";
                    continue;
                }
                if($field == 'created_by' || $field == 'updated_by'){
                    echo "<th field=\"{$field}_name\" width=\"$width\" $attr>" . lang($field) . "</th>";
                    continue;
                }
                echo "<th field=\"$field\" width=\"$width\" $attr>" . lang($disp) . "</th>";
            }
        }
        ?>
        <th data-options="formatter:operation_for,field:'operation',width:200"><?= lang('操作');?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>

            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <form id="cx">
        <table>
        <tr>
            <td>
                <?= lang('query');?>
            </td>
            <td align="right">
                <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="
                    onChange:function(newVal, oldVal){
                        var index = $('.f').index(this);
                        if(setting.hasOwnProperty(newVal) === true){
                            $('.v:eq(' + index + ')').textbox('destroy');
                            $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
                            $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                            $('.v:eq(' + index + ')').combobox({
                                data:data[setting[newVal][0]],
                                valueField:setting[newVal][1],
                                textField:setting[newVal][2],
                            });
                        }else if ($.inArray(newVal, datebox) !== -1){
                            $('.v:eq(' + index + ')').textbox('destroy');
                            $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datebox\' >');
                            $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                        }else if ($.inArray(newVal, datetimebox) !== -1){
                            $('.v:eq(' + index + ')').textbox('destroy');
                            $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datetimebox\' >');
                            $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                        }else{
                            $('.v:eq(' + index + ')').textbox('destroy');
                            $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
                            $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                        }
                    }
                ">
                    <option value="job_no"><?= lang('job_no');?></option>
                    <option value="trans_carrier"><?= lang('trans_carrier');?></option>
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>" . lang($item) . "</option>";
                    }
                    ?>
                    
                </select>
                &nbsp;
    
                <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="
                ">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                    <option value="!=">!=</option>
                </select>
                &nbsp;
                <div class="this_v">
                    <input name="field[v][]" class="v easyui-textbox" >
                </div>
                <button class="add_tr" type="button">+</button>
            </td>
        </tr>
    </table>
    </form>
    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;"><?= lang('search');?></a>
</div>