<div class="easyui-draggable" data-options="" style="position: absolute;right: <?= isset($right) ? $right . 'px' : '38px'; ?>;top: <?= isset($top) ? $top . 'px' : '10px'; ?>;z-index: 999;">
<div id="tags_tab" class="easyui-accordion" data-options="multiple:true" style="width:300px;">
    <div title="Tags" style="padding:10px;" >
        <style type="text/css">
            .topic-tag{
                display:inline-block
            }
            .topic-tag .text {
                display: inline-block;
                height: 16px;
                line-height: 16px;
                padding: 2px 5px;
                background-color: #99cfff;
                font-size: 12px;
                color: #fff;
                border-radius: 4px;
                cursor: pointer;
            }
            .topic-tag {
                margin: 0 5px 2px 0;
            }
            .topic-tag .text:hover, .topic-tag .close:hover{
                background-color: #339dff;
            }
            .this_tag .topic-tag .text, .role_tag .topic-tag .text{
                border-radius: 4px 0 0 4px;
                float: left;
            }
            .this_tag .topic-tag .close, .role_tag .topic-tag .close{
                float: left;
            }
            .topic-tag-apply .text{
                background-color: #cccccc;
            }
            .topic-tag .close {
                width: 20px;
                height: 20px;
                background-color: #66b7ff;
                text-align: center;
                line-height: 20px;
                color: #fff;
                font-size: 10px;
                opacity: 1;
                filter: alpha(opacity=100);
                border-radius: 0 4px 4px 0;
                display: inline-block;
                text-shadow: 0 1px 0 #fff;
                cursor: pointer;
            }
            .add_tags_body_input{
                display: inline-block;
                vertical-align: middle;
                width: 200px !important;
                margin: 0 5px 10px 0;
                padding: 6px;
                resize: none;
                box-shadow: none;
                height: 22px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
                background-color: #fff;
                background-image: none;
                border: 1px solid #ccc;
                border-radius: 4px;
                transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                float: left;
            }
            .add_tags_body_type{
                display: inline-block;
                vertical-align: middle;
                width: 100px !important;
                margin: 0 5px 10px 0;
                padding: 6px;
                resize: none;
                box-shadow: none;
                height: 36px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
                background-image: none;
                border: 1px solid #ccc;
                border-radius: 4px;
                transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                float: left;
            }
            .add_tags_body_input_btn:hover{
                background-color: rgb(22,155,213);
            }
            .add_tags_body_input_btn{
                margin: 0 10px 10px 0;
                border: none !important;
                background-color: rgb(94,180,214);
                color: #fff;
                min-width: 76px;
                height: 34px;
                padding: 0 10px;
                line-height: 34px;
                font-size: 14px;
                display: inline-block;
                font-weight: 400;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                background-image: none;
                border-radius: 4px;
                box-sizing: border-box;
            }
            .add_tags_body h3{
                margin-top: 20px;
                color: #333;
                font-size: 100%;
                line-height: 1.7;
                margin-bottom: 10px;
            }
            .add_tags_body{
                padding:10px;
            }
        </style>
        <script type="text/javascript">
            //添加标签
            function add_tags() {
                $('#add_tags_div').window('open');
            }
            //申请标签
            function apply_tag() {
                var tag_name = $('#tag_name').val().trim();
                if(tag_name == ''){
                    $.messager.alert('<?= lang('提示');?>', '标签内容不能为空');
                    return false;
                }
                var tag_class = $('#tag_class').val();
                if(tag_class == ''){
                    $.messager.alert('<?= lang('提示');?>', '请选择标签分类');
                    return false;
                }
                var post_data = {};
                post_data.tag_name = tag_name;
                post_data.tag_class = tag_class;
                post_data.id_type = $('#tag_id_type').val();
                post_data.id_no = $('#tag_id_no').val();
                submit_add_tag(post_data, function (res) {
                    $.messager.alert('<?= lang('提示');?>', res.msg);
                    if(res.code == 0){
                        $('#tag_name').val('');
                        var pass = res.data.is_pass;
                        add_tag_html(post_data.tag_name, tag_class, 'system_tag', pass);
                        add_tag_html(post_data.tag_name, tag_class, 'this_tag', pass);
                    }else{

                    }
                });
            }

            //点击添加标签
            function click_add_tag(e) {
                var tag_name = $(e).text();
                var tag_class = $(e).parents('.topic-bar').attr('tag_class');
                var post_data = {};
                post_data.tag_name = tag_name;
                post_data.tag_class = tag_class;
                post_data.id_type = $('#tag_id_type').val();
                post_data.id_no = $('#tag_id_no').val();
                submit_add_tag(post_data, function (res) {
                    if(res.code == 0){
                        var pass = res.data.is_pass;
                        add_tag_html(post_data.tag_name, tag_class, 'this_tag', pass);
                    }else{
                        $.messager.alert('<?= lang('提示');?>', res.msg);
                    }
                });
            }

            /**
             * 添加一个tag标签
             * @param tag_name 标签名
             * @param tag_class 标签类
             * @param tag_type 标签类型, 目前有this_tag 右上角的,system_tag 系统推荐标签
             * @param pass 是否通过,没通过的是灰色
             */
            function add_tag_html(tag_name, tag_class, tag_type, pass = 0, is_delete = true) {
                var span_class = "topic-tag";
                if(pass == 0) span_class += ' topic-tag-apply';

                if(tag_type === 'this_tag'){
                    //如果不存在,生成一个
                    if($(".this_tag[tag_class='" + tag_class + "']").length == 0){
                        var topic_bar_html = "<div class=\"topic-bar clearfix this_tag\" tag_class=\"" + tag_class + "\" style=\"margin: 0;\"><h3>" + tag_class + "</h3></div>";
                        $('.this_tag_box').append(topic_bar_html);
                    }

                    var tag_html = "<span class=\"" + span_class + "\">\n" +
                        "                    <a class=\"text\">" + tag_name + "</a>\n" ;
                    if(is_delete) tag_html += "<a class=\"close\" onclick=\"click_del_tag(this)\">X</a>" +
                        "                </span>";
                    $(".this_tag[tag_class='" + tag_class + "']").append(tag_html);
                }else if(tag_type === 'system_tag'){
                    //如果不存在,生成一个
                    if($(".system_tag[tag_class='" + tag_class + "']").length == 0){
                        var topic_bar_html = "<div class=\"topic-bar clearfix system_tag\" tag_class=\"" + tag_class + "\" style=\"margin: 0;\"><h3>" + tag_class + "</h3></div>";
                        $('.system_tag_box').append(topic_bar_html);
                    }

                    var tag_html = "<span class=\"" + span_class + "\">\n" +
                        "                    <a class=\"text\" onclick=\"click_add_tag(this)\">" + tag_name + "</a>\n" +
                        "                </span>";
                    $(".system_tag[tag_class='" + tag_class + "']").append(tag_html);
                }
            }

            //提交方法
            function submit_add_tag(post_data, fn) {
                ajaxLoading();
                $.ajax({
                    type: 'POST',
                    url: '/biz_tag/add_tag',
                    data: post_data,
                    success: function (res_json) {
                        ajaxLoadEnd();
                        try {
                            var res = $.parseJSON(res_json);
                            fn(res);
                        }catch (e) {
                            $.messager.alert('<?= lang('提示');?>', res_json);
                        }
                    },
                    error:function (e) {
                        ajaxLoadEnd();
                        $.messager.alert('<?= lang('提示');?>', e.responseText);
                    }
                });
            }

            //点击删除当前tag
            function click_del_tag(e) {
                var tag_name = $(e).prev().text();
                var tag_class = $(e).parents('.topic-bar').attr('tag_class');
                var post_data = {};
                post_data.tag_name = tag_name;
                post_data.tag_class = tag_class;
                post_data.id_type = $('#tag_id_type').val();
                post_data.id_no = $('#tag_id_no').val();
                ajaxLoading();
                $.ajax({
                    type: 'POST',
                    url: '/biz_tag/del_this_tag',
                    data: post_data,
                    success: function (res_json) {
                        ajaxLoadEnd();
                        try {
                            var res = $.parseJSON(res_json);
                            var topic_tag = $(e).parents('.topic-bar').children('.topic-tag');
                            if(topic_tag.length === 1) $(e).parents('.topic-bar').remove();
                            else $(e).parents('.topic-tag').remove();
                            //判断当前tag下是否还有其他标签, 如果一个都没了,tag_class整个删除
                        }catch (e) {
                            $.messager.alert('<?= lang('提示');?>', res_json);
                        }
                    },
                    error:function (e) {
                        ajaxLoadEnd();
                        $.messager.alert('<?= lang('提示');?>', e.responseText);
                    }
                });
            }

            //加载当前tag
            function load_tag() {
                var id_type = $('#tag_id_type').val();
                var id_no = $('#tag_id_no').val();
                //分为2部分,1是当前的tag,2是公共模板的tag
                $.ajax({
                    type: 'GET',
                    url: '/biz_tag/get_tag?id_type=' + id_type + '&id_no=' + id_no,
                    success: function (res_json) {
                        ajaxLoadEnd();
                        try {
                            var res = $.parseJSON(res_json);
                            if(res.code == 0){
                                //系统推荐的
                                $.each(res.data.system_tag, function (i, it) {
                                    $.each(it.data, function (i2, it2) {
                                        var is_delete = false;
                                        if(it2.lock === '0') is_delete = true;
                                        add_tag_html(it2.tag_name, it.tag_class, 'system_tag', it2.pass, is_delete);
                                    });
                                });

                                //当前的
                                $.each(res.data.this_tag, function (i, it) {
                                    $.each(it.data, function (i2, it2) {
                                        var is_delete = false;
                                        if(it2.lock === '0') is_delete = true;
                                        add_tag_html(it2.tag_name, it.tag_class, 'this_tag', it2.pass, is_delete);
                                    });
                                })
                            }else{
                                $.messager.alert('<?= lang('提示');?>', res_json);
                            }
                        }catch (e) {
                            $.messager.alert('<?= lang('提示');?>', res_json);
                        }
                    },
                    error:function (e) {
                        ajaxLoadEnd();
                        $.messager.alert('<?= lang('提示');?>', e.responseText);
                    }
                });
            }

            $(function () {
                load_tag();
                $('#tags_tab').accordion('select', 'Tags');
            });
        </script>
        <a href="javascript:void(0)" onclick="add_tags()">添加标签</a>
        <input type="hidden" name="id_type" id="tag_id_type" value="<?= $id_type;?>">
        <input type="hidden" name="id_no" id="tag_id_no" value="<?= $id_no;?>">
        <br>-------------------------------------
        <br><br>
        <div class="this_tag_box">
        </div>
    </div>
</div>
</div>
<!--添加标签-->
<div id="add_tags_div" class="easyui-window" style="width:450px;" data-options="title:'window',modal:true,closed:true,height:'auto',top:200">
    <div class="add_tags_body">
        <h3>自由添加标签</h3>
        <div class="add_tags_body_input_div" style="display: block;">
            <input type="text" class="add_tags_body_input" id="tag_name" autocomplete="off" placeholder="创建或搜索添加新话题...">
            <select class="add_tags_body_type" id="tag_class" style="background-color: #fff;">
                <option value="">选择分类</option>
                <option value="操作类">操作类</option>
                <option value="财务类">财务类</option>
                <option value="其他类">其他类</option>
            </select>
            <a href="javascript:void(0);" onclick="apply_tag()" class="add_tags_body_input_btn">申请添加</a>
        </div>
        <div class="system_tag_box">

        </div>
    </div>
</div>