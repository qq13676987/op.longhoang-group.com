<style>
    .signature_div {
        min-height: 50px;
    }
    #add_content_div {
        margin: 0 10px;
        border: 1px solid #ccc;
    }
    #toolbar-container {
        border-bottom: 1px solid #ccc;
    }
    #editor-container {
        height: 340px;
    }
</style>
<script>
    $(function(){
        var html = $("#mail_content_html").html()
        $("#mail_content_hidden").val(html)
    })

    function mail_template_input_oninput(id){
        if(!id) return;
        let ele = '#'+id;
        let ele2 = '#'+id+id;
        let input_val = $(ele).val();
        if(input_val == ''){
            $(ele2).text("[#"+id+"]");
        }else{
            $(ele2).text(input_val);
        }
        var html = $("#mail_content_html").html()
        $("#mail_content_hidden").val(html)
    }
</script>
<!--add_presend-->
<div style="padding:10px 40px 10px 40px;">
    <form id="send_mail" method="post">
        <input type="hidden" name="template_id" value="<?= $template_id; ?>">
        <input type="hidden" name="id_type" value="<?= $mail_type; ?>">
        <input type="hidden" name="id_no" value="<?= $id; ?>">
        <table cellpadding="5">
            <tr>
                <td><?= lang('发送人邮箱');?></td>
                <td>
                    <span style="display: flex;align-items: center">
                    <select class="easyui-combogrid" name="mail_sender_id" id="mail_sender_id" style="width:350px;" data-options="
                        panelWidth: '650px',
                        panelHeight: '300px',
                        separator:';',
                        editable: false,
                        url:'/mail_sender/get_user_sender',
                        value:'<?= $mail_sender_id;?>',
                        idField: 'id',              //ID字段
                        textField: 'smtp_username',    //显示的字段
                        striped: true,
                        pagination: false,           //是否分页
                        toolbar : '#mail_sender_id_div',
                        collapsible: true,         //是否可折叠的
                        method: 'post',
                        columns:[[
                            {field:'sender_name',title:'<?= lang('发送人名称');?>',width:300},
                            {field:'smtp_username',title:'<?= lang('发送人邮箱');?>',width:300},
                        ]],
                        emptyMsg : '<?= lang('未找到相应数据!');?>',
                        required:true,
                        mode: 'remote',
                        onShowPanel:function(){
                            var opt = $(this).combogrid('options');
                            var toolbar = opt.toolbar;
                            combogrid_search_focus(toolbar);
                        },
                    ">
                    </select>
                        &nbsp;&nbsp;&nbsp;
                    <!--<button onclick="open_url('/mail_sender/bind_sender')" type="button"><?= lang('配置我的邮箱');?></button>-->
                        &nbsp;&nbsp;&nbsp;
                    <a href="javascript:void(0)" style="padding: 0 16px 0 16px" class="easyui-linkbutton" onclick="send_mail()"><?= lang('确认发送');?></a>
                    </span>
                </td>
            </tr>
            <tr>
                <td><?= lang('收件人邮箱');?></td>
                <td>
                    <select class="easyui-combogrid" name="email[]" id="email" style="width:650px;" data-options="
                        panelWidth: '650px',
                        panelHeight: '300px',
                        multiple:true,
                        <?php if($template['content_is_edit'] == 0):?>
                        readonly:true,
                        <?php endif;?>
                        separator:';',
                        editable: false,
                        url:'/mail/get_email?id_type=<?= $mail_type;?>&id_no=<?= $id;?>',
                        value:'<?= $email;?>',
                        idField: 'email',              //ID字段
                        textField: 'email',    //显示的字段
                        striped: true,
                        pagination: false,           //是否分页
                        toolbar : '#email_div',
                        collapsible: true,         //是否可折叠的
                        method: 'post',
                        columns:[[
                            {field:'name',title:'<?= lang('name');?>',width:300},
                            {field:'email',title:'<?= lang('email');?>',width:300},
                        ]],
                        emptyMsg : '<?= lang('未找到相应数据!');?>',
                        required:true,
                        mode: 'remote',
                        onShowPanel:function(){
                            var opt = $(this).combogrid('options');
                            var toolbar = opt.toolbar;
                            combogrid_search_focus(toolbar);
                        },
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('抄送人邮箱');?>:</td>
                <td>
                    <select class="easyui-combogrid" name="ccs[]" id="ccs" style="width:650px;" data-options="
                        panelWidth: '650px',
                        panelHeight: '300px',
                        multiple:true,
                        separator:';',
                        editable: false,
                        url:'/mail/get_email?id_type=<?= $mail_type;?>&id_no=<?= $id;?>',
                        value:'<?= $ccs;?>',
                        idField: 'email',              //ID字段
                        textField: 'email',    //显示的字段
                        striped: true,
                        pagination: false,           //是否分页
                        toolbar : '#ccs_div',
                        collapsible: true,         //是否可折叠的
                        method: 'post',
                        columns:[[
                            {field:'name',title:'<?= lang('name');?>',width:300},
                            {field:'email',title:'<?= lang('ccs');?>',width:300},
                        ]],
                        emptyMsg : '<?= lang('未找到相应数据!');?>',
                        mode: 'remote',
                        onShowPanel:function(){
                            var opt = $(this).combogrid('options');
                            var toolbar = opt.toolbar;
                            combogrid_search_focus(toolbar);
                        },
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('邮件标题');?>:</td>
                <td><input class="easyui-textbox" type="text" name="mail_title" style="width:650px;" data-options="required:true" value="<?= $mail_title; ?>"/></td>
            </tr>
            <?php if(!empty($mail_template_input)):?>
                <?php foreach($mail_template_input as $item): ?>
                    <tr>
                        <td><?=$item['input_name_cn']?>:</td>
                        <td><input type="text" id="<?=$item['input_name_en']?>" oninput="mail_template_input_oninput('<?=$item['input_name_en']?>')" style="width:648px;border: 1px solid #95B8E7;border-radius: 5px"  /></td>
                    </tr>
                <?php endforeach;?>
            <?php endif; ?>
            <tr>
                <td><?= lang('邮件正文');?>:</td>
                <td>
                    <?php if($template['content_is_edit'] === '0'){ ?>
                        <div id="mail_content_html" style="width:800px;border: 1px solid #95B8E7;border-radius: 5px;padding-left:30px">
                            <?= $mail_content; ?>
                        </div>
                        <input type="hidden" id="mail_content_hidden" name="mail_content" value="">
                    <?php }else{ ?>
                        <div id="add_content_div" style="margin:0px">
                            <div id="toolbar-container"></div>
                            <div id="editor-container"></div>
                        </div>
                        <textarea name="mail_content" id="add_content_textarea" style="display: none;" data-options="required:true"><?= $mail_content; ?></textarea>
                    <?php } ?>
                </td>
            </tr>
            <?php if($template['id'] == 10):?>
                <tr>
                    <td></td>
                    <td>
                        <a href="javascript:;" class="easyui-linkbutton" onclick="open_upload()" style="margin-right: 15px"><?= lang('上传BC');?></a>
                        <a href="javascript:;" class="easyui-linkbutton" onclick="shipment_hbl_draft()" style="margin-right: 15px"><?= lang('生成提单');?></a>
                        <a href="javascript:;" class="easyui-linkbutton" onclick="select_att()" style="margin-right: 15px"><?= lang('附件选择');?></a>
                    </td>
                </tr>
            <?php endif;?>
            <tr>
                <td>&nbsp;</td>
                <td>

                </td>
            </tr>
        </table>

    </form>
</div>
<div id="mail_sender_id_div">
    <table>
        <tr>
            <td>
                <form id="mail_sender_id_form" method="post">
                    <div style="padding-left: 5px;display: inline-block;">
                        <label><?= lang('email');?>:</label><input name="email" class="easyui-textbox keydown_search" style="width:96px;" t_id="mail_sender_id_click"/>
                        <a href="javascript:void(0);" class="easyui-linkbutton" id="mail_sender_id_click" iconCls="icon-search" onclick="query_report('mail_sender_id_form', 'mail_sender_id')"><?= lang('search');?></a>
                    </div>
                </form>
            </td>
            <td>

            </td>
        </tr>
    </table>
</div>
<div id="email_div">
    <table>
        <tr>
            <td>
                <form id="email_form" method="post">
                    <div style="padding-left: 5px;display: inline-block;">
                        <label><?= lang('email');?>:</label><input name="email" class="easyui-textbox keydown_search" style="width:96px;" t_id="email_click"/>
                        <a href="javascript:void(0);" class="easyui-linkbutton" id="email_click" iconCls="icon-search" onclick="query_report('email_form', 'email')"><?= lang('search');?></a>
                    </div>
                </form>
            </td>
            <td>
                <button onclick="email_url('/mail/get_email?id_type=<?= $mail_type;?>&id_no=<?= $id;?>')" type="button"><?= lang('默认邮箱');?></button>
                <button onclick="email_url('/mail/inner_email')" type="button"><?= lang('查询同事邮箱');?></button>
                <!--                <button onclick="email_url('/mail/client_email')" type="button">--><?//= lang('查询客户邮箱');?><!--</button>-->
                <?php if(isset($shipment)):?>
                    <a href="javascript:;" class="easyui-linkbutton"  onclick="window.open('/biz_client_contact/index/<?=$shipment['client_code']?>/?role=factory,logistics_client')"><?= lang('1级委托方维护');?></a>
                    <a href="javascript:;" class="easyui-linkbutton" onclick="window.open('/biz_client_contact/index/<?=$shipment['client_code2']?>/?role=factory,logistics_client')"><?= lang('2级委托方维护');?></a>
                <?php endif;?>
            </td>
        </tr>
    </table>
</div>
<div id="ccs_div">
    <table>
        <tr>
            <td>
                <form id="ccs_form" method="post">
                    <div style="padding-left: 5px;display: inline-block;">
                        <label><?= lang('email');?>:</label><input name="email" class="easyui-textbox keydown_search" style="width:96px;" t_id="ccs_click"/>
                        <a href="javascript:void(0);" class="easyui-linkbutton" id="ccs_click" iconCls="icon-search" onclick="query_report('ccs_form', 'ccs')"><?= lang('search');?></a>
                    </div>
                </form>
            </td>
            <td>
                <button onclick="ccs_url('/mail/get_email?id_type=<?= $mail_type;?>&id_no=<?= $id;?>')" type="button"><?= lang('默认邮箱');?></button>
                <button onclick="ccs_url('/mail/inner_email')" type="button"><?= lang('查询同事邮箱');?></button>
                <!--                <button onclick="ccs_url('/mail/client_email')" type="button">--><?//= lang('查询客户邮箱');?><!--</button>-->
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript" src="/inc/layer3.1.1/layer.js"></script>
<!-- 引入 css -->
<link type="text/css" href="/inc/css/wangeditor/style.css" rel="stylesheet">
<!-- 引入 js -->
<script type="text/javascript" src="/inc/js/wangeditor/index.js" ></script>
<script type="text/javascript" src="/inc/js/wangeditor/dist/index.js" ></script>
<script>
    const E = window.wangEditor;
    var add_content_div, $add_content_textarea, signature_div, $signature_textarea;
    E.Boot.registerModule(window.WangEditorPluginUploadAttachment.default)

    $(function () {
        $add_content_textarea = $('#add_content_textarea');
        if($add_content_textarea.length > 0){
            // 编辑器配置
            const editorConfig = {
                // readOnly: true,
                placeholder: '<?= lang('请输入内容');?>',
                onChange(editor){
                    // 当编辑器选区、内容变化时，即触发
                    const content = editor.children;
                    const contentStr = JSON.stringify(content);
                    // document.getElementById('textarea-1').value = contentStr
                    // console.log(contentStr);


                    // const html = editor.getHtml();
                    // document.getElementById('textarea-2').value = html
                    $add_content_textarea.val(editor.getHtml());
                    // console.log('content', editor.children);
                    // console.log('html', editor.getHtml())
                },
                hoverbarKeys: {
                    attachment: {
                        menuKeys: ['downloadAttachment'],
                    },
                },
                MENU_CONF: {
                    uploadAttachment: {
                        server: '/bsc_upload/file_upload',
                        fieldName: 'file',
                        meta:{
                            is_save_data: 0,
                        },
                        timeout: 10 * 1000, // 5s
                        // metaWithUrl: true, // meta 拼接到 url 上

                        maxFileSize: 10 * 1024 * 1024, // 10M

                        onBeforeUpload(file) {
                            return file // 上传 file 文件
                            // return false // 会阻止上传
                        },
                        onProgress(progress) {
                            console.log('onProgress', progress)
                        },
                        onSuccess(file, res) {
                            console.log('onSuccess', file, res)
                        },
                        onFailed(file, res) {
                            alert(res.message)
                            console.log('onFailed', file, res)
                        },
                        onError(file, err, res) {
                            alert(err.message)
                            console.error('onError', file, err, res)
                        },
                        // 上传成功后，用户自定义插入文件
                        customInsert(res, file, insertFn) {
                            var url = "https://opchina.oss-cn-shanghai.aliyuncs.com" + res['data'][0]['url'] || {};

                            // 插入附件到编辑器
                            //手动插入
                            var file_name = `${file.name}`;
                            // var file_a = "<a href=\"" + url + "\" target='_blank'>" + file_name + "</a>";
                            // editor.setHtml(editor.getHtml() + file_a);
                            // var a_sign = "[#a]";

                            //2023-04-25 自动替换到[#a]的字段里
                            // var str = editor.getHtml().replace(a_sign, file_a + "<br/>" + a_sign);
                            editor.insertNode({type: 'link', url: url, target: '_blank', children: [{ text: file_name }]});
                            // editor.setHtml(str);
                        },
                    },
                    uploadImage: {
                        fieldName: 'file',
                        server:'/bsc_upload/file_upload',
                        meta:{
                            is_save_data: 0,
                        },
                        customInsert(res,insertFn){
                            // 图片上传并返回了结果，想要自己把图片插入到编辑器中
                            // 例如服务器端返回的不是 { errno: 0, data: [...] } 这种格式，可使用 customInsert
                            // result 即服务端返回的接口
                            // var url = document.location.protocol + '//' + window.location.host + "/download/read_img?file_path=" + result['data'][0]['url'];
                            var url = "https://opchina.oss-cn-shanghai.aliyuncs.com" + res['data'][0]['url'];

                            // insertImgFn 可把图片插入到编辑器，传入图片 src ，执行函数即可
                            insertFn(url);
                        },
                    }
                },
            };
// 工具栏配置
            const toolbarConfig = {
                insertKeys: {
                    index: 0,
                    keys: ['uploadAttachment'],
                },
            };

// 创建编辑器
            const editor = E.createEditor({
                html: $add_content_textarea.val(),
                selector: '#editor-container',
                config: editorConfig,
                mode: 'default' // 或 'simple' 参考下文
            });
            // editor.dangerouslyInsertHtml();
            // 创建工具栏
            const toolbar = E.createToolbar({
                editor,
                selector: '#toolbar-container',
                config: toolbarConfig,
                mode: 'default' // 或 'simple' 参考下文
            })
        }

        $.each($('.keydown_search'), function (i,it) {
            $(it).textbox('textbox').bind('keydown', function(e){
                //回车自动触发查询
                if(e.keyCode == 13){
                    console.log();
                    keydown_search(e, $(it).attr('t_id'));
                }
            });
        });
    });

</script>
<!--  一般没啥用,就easyui用到的相关事件  -->
<script>
    /**
     * 下拉选择表格展开时,自动让表单内的查询获取焦点
     */
    function combogrid_search_focus(form_id, focus_num = 0){
        // var form = $(form_id);
        setTimeout("easyui_focus($($('" + form_id + "').find('.keydown_search')[" + focus_num + "]));",200);
    }

    /**
     * easyui 获取焦点
     */
    function easyui_focus(jq){
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combogrid !== undefined){
            return $(jq).combogrid('textbox').focus();
        }else if(data.combobox !== undefined){
            return $(jq).combobox('textbox').focus();
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('textbox').focus();
        }else if(data.datebox !== undefined){
            return $(jq).datebox('textbox').focus();
        }else if(data.textbox !== undefined){
            return $(jq).textbox('textbox').focus();
        }
    }

    function query_report(form_id, combo_id){
        var where = {};
        var form_data = $('#' + form_id).serializeArray();
        $.each(form_data, function (index, item) {
            if(item.value == "") return true;
            if(where.hasOwnProperty(item.name) === true){
                if(typeof where[item.name] == 'string'){
                    where[item.name] =  where[item.name].split(',');
                    where[item.name].push(item.value);
                }else{
                    where[item.name].push(item.value);
                }
            }else{
                where[item.name] = item.value;
            }
        });
        if(where.length == 0){
            $.messager.alert('Tips', '请填写需要搜索的值');
            return;
        }
        //判断一下当前是 remote 还是local,如果是remode,走下面这个
        var inp = $('#' + combo_id);
        var opt = inp.combogrid('options');
        if(opt.mode == 'remote'){
            inp.combogrid('grid').datagrid('load',where);
        }else{
            //如果是本地的,那么先存一下
            if(opt.old_data == undefined) opt.old_data = inp.combogrid('grid').datagrid('getData'); // 如果没存过,这里存一下
            var rows = [];
            if(JSON.stringify(where) === '{}'){
                inp.combogrid('grid').datagrid('loadData', opt.old_data.rows);
                return;
            }
            $.each(opt.old_data.rows,function(i,obj){
                for(var p in where){
                    var q = where[p].toUpperCase();
                    var v = obj[p].toUpperCase();
                    if (!!v && v.toString().indexOf(q) >= 0){
                        rows.push(obj);
                        break;
                    }
                }
            });
            if(rows.length == 0) {
                inp.combogrid('grid').datagrid('loadData', []);
                return;
            }

            inp.combogrid('grid').datagrid('loadData',rows);
        }
    }

    //text添加输入值改变
    function keydown_search(e, click_id){
        $('#' + click_id).trigger('click');
    }
</script>
<script>
    function email_url(url) {
        $('#email').combogrid('grid').datagrid('reload', url);
        alert("切换成功!");
    }
    function ccs_url(url) {
        $('#ccs').combogrid('grid').datagrid('reload', url);
        alert("切换成功!");
    }

    function open_url(url) {
        window.open(url);
    }

    function send_mail(){
        $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认发送? 确认后邮件将进入发送队列');?>', function (r) {
            if(r){
                // ajaxLoading();
                $('#send_mail').form('submit',{
                    url: '/mail_box/send_mail',
                    onSubmit: function(param){
                        let mail_template_input = `<?=json_encode($mail_template_input)?>`;
                        let template_data = JSON.parse(mail_template_input);
                        let msg = '';
                        template_data && template_data.map(item=>{
                            let ele = "#"+item.input_name_en;
                            if($(ele).val() == ''){
                                msg = item.input_name_cn+'<?= lang('不能为空');?>'
                            }
                        })
                        if(msg != ''){
                            $.messager.alert('<?= lang('提示');?>', msg);
                            return false;
                        }
                        param.hide_str = $('#hide_str').val()
                        return $(this).form('validate');
                    },
                    success: function(res_json){
                        ajaxLoadEnd();
                        try {
                            var res = $.parseJSON(res_json);
                            $.messager.alert('<?= lang('提示');?>', res.msg);
                            if(res.code == 0){
                                //跳转到
                                location.href = "/mail_box/read/?id=" + res.data.mail_box_id;
                            }
                        }catch (e) {
                            $.messager.alert(res_json);
                        }
                    }
                });
            }
        });

    }
</script>
<div id="ddd" class="easyui-dialog" title="<?= lang('附件选择');?>" style="width:200px;height:300px;" closed="true" data-options="resizable:true,modal:true">
    <div style="display: none">
        <textarea  id="hide_list" ></textarea>
        <input type="hidden" id="hide_str">
    </div>
    <br><br>
    <div class="att_list" style="margin-left: 20px">

    </div>
</div>
<script>
    function open_upload(){
        layer.open({
            type: 2,
            area: ['800px', '500px'],
            content: '/bsc_upload/upload_files?biz_table=biz_consol&id_no=<?=isset($shipment)?$shipment['consol_id']:''?>&type=s/o',
        });
    }
    function shipment_hbl_draft(){
        $.getJSON("/bsc_upload/shipment_hbl_draft/<?=isset($shipment)?$shipment['id']:''?>", function(res){
            alert(res.msg)
        });
    }
    function select_att(){
        $.getJSON("/bsc_upload/get_att/<?=isset($shipment)?$shipment['id']:''?>", function(res){
            $('#hide_list').val(JSON.stringify(res))
            let str = '';
            for (let i = 0; i < res.length; i++) {
                str += `<div style="display: flex"><div style="width: 80px">${res[i].name}</div><div><input type="checkbox" onclick="att_check(this,${i})" value="${i}" class="att_check"></div></div>`
            }
            $('.att_list').html(str)
            $('#ddd').dialog('open')
        });
    }
    var that = '';
    var selected = []
    function att_check(e,i){
        let isChecked = $(e).is(':checked')
        // console.log($('#w-e-element-17'))
        if(isChecked){
            if($.inArray(i,selected) === -1){
                selected.push(i)
            }
        }else{
            if($.inArray(i,selected) != -1){
                let tmp = selected
                tmp.map((item,index)=>{
                    if(item == i){
                        selected.splice(index,1)
                    }
                })
            }
        }
        let data = JSON.parse($('#hide_list').val());
        let str = '<div class="appendstr">';
        if(selected.length > 0){
            for (let j = 0; j < selected.length; j++) {
                str += `<a contentEditable="false" href="${data[selected[j]].url}" style="margin-left:30px;userSelect:none">${data[selected[j]].name}</a><br>`
            }
        }
        str += '</div>';
        let pre = `<p><span style="color: rgb(225, 60, 57);"><strong><?= lang('附件');?> </strong></span>(<?= lang('以下附件可点击下载');?>)：</p>`;
        let newstr = pre+str
        $('#hide_str').val(newstr)
        if(that == ''){
            $('strong').each(function(i){
                if($(this).text() == '[a]'){
                    that = $(this);
                }
            })
        }
        that.html(str)
    }
</script>