<?php


class biz_shipment_truck extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_shipment_truck_model');
        $this->model = $this->biz_shipment_truck_model;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }


    public $field_edit = array();

    public $field_all = array(
        array('id', 'id', '80', '1', 'sortable:true'),
        array('truck_date', 'truck_date', '150', '3', "editor:'datetimebox'"),
        array('truck_supplier', 'truck_supplier', '150', '3', "editor:{type:'combobox',options:{valueField:'company_name',
								textField:'company_name',
								url:'/biz_client/get_option/truck',
								onHidePanel: function() {
									var valueField = $(this).combobox('options').valueField;
									var val = $(this).combobox('getValue');
									var allData = $(this).combobox('getData');
									var result = true;
									for (var i = 0; i < allData.length; i++) {
										if (val == allData[i][valueField]) {
											result = false;
										}
									}
									if (result) {
										$(this).combobox('clear');
									}
								},
								}},"),
		array('truck_client_code', 'truck_client_code', '0', '3', "hidden:true,editor:{type:'textbox',options:{value:'{client_code}'}}"),
        array('truck_company', 'truck_company', '100', '4', "editor:{type:'combobox',options:{
                                required:true,
                                valueField:'truck_company',
								textField:'truck_company',
								url:'/biz_truck_supplier/get_option/{client_code}',
								onSelect: function(res){
                                    var ed = $('#tt').datagrid('getEditor', {index:0,field:'truck_address'});
                                    $(ed.target).val(res.truck_address);
                                    var ed = $('#tt').datagrid('getEditor', {index:0,field:'truck_contact'});
                                    $(ed.target).val(res.truck_contact);
                                    var ed = $('#tt').datagrid('getEditor', {index:0,field:'truck_telephone'});
                                    $(ed.target).val(res.truck_telephone);
                                    var ed = $('#tt').datagrid('getEditor', {index:0,field:'truck_remark'});
                                    $(ed.target).val(res.truck_remark);
                                    var ed = $('#tt').datagrid('getEditor', {index:0,field:'GP20PRICE'});
                                    $(ed.target).numberbox('setValue', res.GP20PRICE);
                                    var ed = $('#tt').datagrid('getEditor', {index:0,field:'GP40PRICE'});
                                    $(ed.target).numberbox('setValue', res.GP40PRICE);
                                },
								}},"),
        array('truck_address', 'truck_address', '200', '5', "editor:'text'"),
        array('truck_contact', 'truck_contact', '100', '6', "editor:'text'"),
        array('truck_telephone', 'truck_telephone', '100', '6', "editor:'text'"),
        array('truck_remark', 'truck_remark', '150', '8', "editor:'text'"),
        array('feeder_date', 'feeder_date', '150', '3', "editor:'datetimebox'"),
        array('feeder_supplier', 'feeder_supplier', '150', '3', "editor:{type:'combobox',options:{valueField:'company_name',
								textField:'company_name',
								url:'/biz_client/get_option/feeder',
								onHidePanel: function() {
									var valueField = $(this).combobox('options').valueField;
									var val = $(this).combobox('getValue');
									var allData = $(this).combobox('getData');
									var result = true;
									for (var i = 0; i < allData.length; i++) {
										if (val == allData[i][valueField]) {
											result = false;
										}
									}
									if (result) {
										$(this).combobox('clear');
									}
								},
								}},"),
        array('feeder_vessel', 'feeder_vessel', '0', '10', ''),
        array('feeder_voyage', 'feeder_voyage', '0', '10', ''),
        array('GP20', 'GP20', '80', '8', "editor:{type:'numberbox',options:{min:0}}"),
        array('GP20PRICE', 'GP20PRICE', '80', '8', "editor:{type:'numberbox',options:{min:0,precision:2}}"),
        array('GP40', 'GP40', '80', '8', "editor:{type:'numberbox',options:{min:0}}"),
        array('GP40PRICE', 'GP40PRICE', '80', '8', "editor:{type:'numberbox',options:{min:0,precision:2}}"),
        array('created_time', 'created_time', '150', '10', 'sortable:true'),
    );

    public function index($shipment_id = 0)
    {
        $data = array();
        $this->load->model('biz_shipment_model');
        $shipment = $this->biz_shipment_model->get_duty_one("id = '{$shipment_id}'");
        $lock_lv = 0;
        $client_code = '';
        if(!empty($shipment)){
            $lock_lv = $shipment['lock_lv'];
            $client_code = $shipment['client_code'];
        }
        $data['lock_lv'] = $lock_lv;
        $data["shipment_id"] = $shipment_id;
        $data["client_code"] = $client_code;
        $data["truck_type"] = getValue('truck_type', 'truck');

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_shipment_truck_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            //将client_code 替换为参数
            $this->field_all[3][4] = str_replace("{client_code}",$client_code,$this->field_all[3][4]);
            $this->field_all[4][4] = str_replace("{client_code}",$client_code,$this->field_all[4][4]);
            $data["f"] = $this->field_all;
        }

        $this->load->view('head');
        $this->load->view('biz/shipment/truck/index_view', $data);
    }

    public function get_data($shipment_id = 0)
    {
        $result = array();
        $shipment_id = (int)$shipment_id;
        $truck_type = getValue('truck_type', 'truck');
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = array();
        $where[] = 'shipment_id = \'' . $shipment_id . '\'';
        $where[] = "truck_type = '{$truck_type}'";
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($v1 != "") $where[] = "$f1 $s1 '$v1'";
        if ($v2 != "") $where[] = "$f2 $s2 '$v2'";
        if ($v3 != "") $where[] = "$f3 $s3 '$v3'";
        //-----------------------------------------------------------------
        $where_str = join(' and ', $where);

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where_str);
        $this->db->limit($rows, $offset);
        $rs = $this->model->get($where_str, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data($shipment_id = 0)
    {
        $shipment_id = (int)$shipment_id;
        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            $data[$item] = $temp;
        }
        $data['truck_type'] = postValue('truck_type', 'truck');
        $data['shipment_id'] = $shipment_id;
        if(!isset($data['truck_company']) || (isset($data['truck_company']) && $data['truck_company'] == '')){
            echo json_encode(array('isError' => true, 'msg' => 'truck_company 必须填写'));
            return;
        }
        //获取shipment的client_code
        Model('biz_shipment_model');
        $shipment = $this->biz_shipment_model->get_duty_one("id = '{$shipment_id}'");
        //如果该门店公司为空,则添加一条作为记录
        if($data['truck_type'] == 'truck'){
            $this->load->model('biz_truck_supplier_model');
            $this->db->where('client_code', $shipment['client_code']);
            $truck_supplier = $this->biz_truck_supplier_model->get_one('truck_company', $data['truck_company']);
            if(empty($truck_supplier)){

                $supplier_data['client_code'] = $shipment['client_code'];
                $supplier_data['truck_company'] = $data['truck_company'];
                $supplier_data['truck_address'] = isset($data['truck_address']) ? $data['truck_address'] : null;
                $supplier_data['truck_contact'] = isset($data['truck_contact']) ? $data['truck_contact'] : null;
                $supplier_data['truck_telephone'] = isset($data['truck_telephone']) ? $data['truck_telephone'] : null;
                $supplier_data['truck_remark'] = isset($data['truck_remark']) ? $data['truck_remark'] : null;
                $supplier_data['GP20PRICE'] = isset($data['GP20PRICE']) ? $data['GP20PRICE'] : null;
                $supplier_data['GP40PRICE'] = isset($data['GP40PRICE']) ? $data['GP40PRICE'] : null;

                $supplier_id = $this->biz_truck_supplier_model->save($supplier_data);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_truck_supplier";
                $log_data["key"] = $supplier_id;
                $log_data["action"] = "insert";
                $log_data["value"] = json_encode($supplier_data);
                log_rcd($log_data);
            }else{//不为空进行更新
                isset($data['truck_address']) ? $supplier_data['truck_address'] = $data['truck_address'] : null;
                isset($data['truck_contact']) ? $supplier_data['truck_contact'] = $data['truck_contact'] : null;
                isset($data['truck_telephone']) ? $supplier_data['truck_telephone'] = $data['truck_telephone'] : null;
                isset($data['truck_remark']) ? $supplier_data['truck_remark'] = $data['truck_remark'] : null;
                isset($data['GP20PRICE']) ? $supplier_data['GP20PRICE'] = $data['GP20PRICE'] : null;
                isset($data['GP40PRICE']) ? $supplier_data['GP40PRICE'] = $data['GP40PRICE'] : null;
                $this->biz_truck_supplier_model->update($truck_supplier['id'], $supplier_data);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_truck_supplier";
                $log_data["key"] = $truck_supplier['id'];
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($supplier_data);
                log_rcd($log_data);
            }
        }

        $id = $this->model->save($data);
        $data['id'] = $id;
        echo json_encode($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment_truck";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function update_data()
    {
        $id = $_REQUEST["id"];
        $old_row = $this->model->get_one('id', $id);

        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($old_row[$item] != $temp)
                $data[$item] = $temp;
        }
        $id = $this->model->update($id, $data);
        $data['id'] = $id;
        echo json_encode($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment_truck";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->model->get_one('id', $id);
        $this->model->mdelete($id);
        echo json_encode(array('success' => true));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment_truck";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
    }

    public function get_option($shipment_id = "")
    {
        if ($shipment_id != '') {
            $where = array('shipment_id' => $shipment_id, 'truck_type' => getValue('truck_type', 'truck'));
            $data = $this->model->get_option($where);
            echo json_encode($data);
        }
    }
    
    public function truck_log($user_id = 0){
        $this->load->model('biz_truck_supplier_model');
        $data = $this->biz_truck_supplier_model->get();
        echo '<table>';
        foreach ($data as $dk => $row){
            echo '<tr>';
            foreach ($row as $key => $val){
                echo '<td>' . $val . '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';
    }
    
    public function truck_table($user_id = 0){
        $this->load->model('biz_shipment_truck_model');
        $data = $this->biz_shipment_truck_model->get();
        echo '<table>';
        foreach ($data as $dk => $row){
            $keys = array_keys($row);
            if($dk == 0){
                echo '<tr>';
                foreach ($keys as $kv){
                    echo '<td>' . lang($kv) . '</td>';
                }
                echo '</tr>';
            }
            echo '<tr>';
            foreach ($row as $key => $val){
                echo '<td>' . $val . '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';
    }

    public function validate(){
        $_post = $this->input->post();
        $noEmptyField = array(
            'truck_date'=>'拖车日期',
            'truck_supplier'=>'车队名称',
            'truck_company'=>'工厂名称',
            'truck_address'=>'工厂地址',
            'truck_contact'=>'工厂联系人',
            'truck_telephone'=>'联系电话',
        );

        $errmsg='';
        $data = array('msg'=>'', 'code'=>1);
        foreach ($noEmptyField as $key => $val){
            if (!isset($_post[$key]) || empty(trim($_post[$key]))){
                $errmsg = $val . '是必填项！';
                break;
            }
        }

        if (empty($errmsg)) {
            $_post['GP20'] = isset($_post['GP20']) ? (int)$_post['GP20'] : 0;
            $_post['GP20PRICE'] = isset($_post['GP20PRICE']) && is_numeric($_post['GP20PRICE']) ? $_post['GP20PRICE'] : 0;
            $_post['GP40'] = isset($_post['GP40']) ? (int)$_post['GP40'] : 0;
            $_post['GP40PRICE'] = isset($_post['GP40PRICE']) && is_numeric($_post['GP40PRICE']) ? $_post['GP40PRICE'] : 0;

            if (!$_post['GP20'] && !$_post['GP40']) {
                $errmsg = '大、小箱数量必须填写一个！';
            } elseif ($_post['GP20'] > 0 && $_post['GP20PRICE'] < 1) {
                $errmsg = '请输入小箱价格！';
            } elseif ($_post['GP20'] < 1 && $_post['GP20PRICE'] > 0) {
                $errmsg = '请输入小箱数量！';
            } elseif ($_post['GP40'] > 0 && $_post['GP40PRICE'] < 1) {
                $errmsg = '请输入大箱价格！';
            } elseif ($_post['GP40'] < 1 && $_post['GP40PRICE'] > 0) {
                $errmsg = '请输入大箱数量！';
            }
        }

        if (!empty($errmsg)){
            $data = array('msg'=>$errmsg, 'code'=>0);
            exit(json_encode($data));
        }else{
            exit(json_encode($data));
        }
    }
}