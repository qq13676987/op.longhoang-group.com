<script type="text/javascript">
    var is_submit = false;
    function save_container(index, is_auto = false){
        var data = $('#tt').datagrid('getData').rows;
        var row = data[index];
        if(is_auto){
            save_ajax(row, is_auto);
            return;
        }
        $.messager.confirm('确认对话框', '是否确认提交？', function(r) {
            if (r) {
                save_ajax(row);
            }
        });
    }

    function save_ajax(row, is_auto = false) {
        var url;
        if (is_submit) {
            return;
        }
        ajaxLoading();
        is_submit = true;
        var data = {};
        data.container_no = row.container_no;
        data.seal_no = row.seal_no;
        data.container_tare = row.container_tare;
        data.container_size = row.container_size;
        data.packs = row.packs;
        data.weight = row.weight;
        data.vgm = row.vgm;
        data.volume = row.volume;
        var msg = '';
        if(row.id == 0){
            url = '/biz_containershipment/add_data/<?= $shipment_id;?>/<?= $consol_id;?>';
        }else{
            if(row.container_tare == '' && row.container_no == '' && row.seal_no == '' && row.container_size == '' && row.packs == '' && row.weight == '' && row.volume == ''){
                url = '/biz_containershipment/delete_data?id=' + row.id;
            }else{
                url = '/biz_containershipment/update_data?id=' + row.id;
                //如果ID存在，其他值不能为空
                $.each(data, function (i, it) {
                    if(it == ''){
                        msg = '不能为空值';
                        return false;
                    }
                })
            }
        }
        if(msg !== ''){
            $.messager.alert('Tips', msg);
            is_submit = false;
            ajaxLoadEnd();
            return;
        }
        $.ajax({
            type:'POST',
            url:url,
            data:data,
            async:!is_auto,
            success:function (res_json) {
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误，请联系管理员');
                    return;
                }
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.reload();
                    });
                }else{
                    $.messager.alert('Tips', res.msg);
                }
                is_submit = false;
                ajaxLoadEnd();
            },
            error:function () {
                is_submit = false;
                ajaxLoadEnd();
                $.messager.alert('Tips', '发生错误');
            }
        });
    }

    function auto_submit(){
        var data = $('#tt').datagrid('getData').rows;
        $.messager.confirm('确认对话框', '是否确认一键提交？', function(r) {
            if(r){
                $.each(data, function (index, item) {
                    save_container(index, true);
                })
            }
        });
    }

    $(function () {
        $('#tt').datagrid({
            width:'auto',
            height: $(window).height() - 26,

        });
        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });
</script>
<button onclick="auto_submit()">一键提交</button>
<table id="tt" style="width:1100px;height:450px" rownumbers="false" idField="id" toolbar="#tb"  singleSelect="true" nowrap="false">
        <thead>
            <tr>
                <th data-options="field:'id', hidden:true">id</th>
				<th data-options="field:'container_no', width:'250', align:'center',formatter: function(value,row,index){
                    if (value != row.old_container_no){
                        return '<span style=\'color:red;\'>' +  value + '</span>';
                    } else {
                        return value;
                    }
                }">new_container_no</th>
                <th data-options="field:'seal_no', width:'111', align:'center',formatter: function(value,row,index){
                    if (value != row.old_seal_no){
                        return '<span style=\'color:red;\'>' +  value + '</span>';
                    } else {
                        return value;
                    }
                }">new_seal_no</th>
                <th data-options="field:'container_size', width:'111', align:'center',formatter: function(value,row,index){
                    if (value != row.old_container_size){
                        return '<span style=\'color:red;\'>' +  value + '</span>';
                    } else {
                        return value;
                    }
                }">new_container_size</th>
                <th data-options="field:'packs', width:'111',formatter: function(value,row,index){
                    if (value != row.old_packs){
                        return '<span style=\'color:red;\'>' +  value + '</span>';
                    } else {
                        return value;
                    }
                }">new_packs</th>
                <th data-options="field:'weight', width:'111', formatter: function(value,row,index){
                    if (value != row.old_weight){
                        return '<span style=\'color:red;\'>' +  value + '</span>';
                    } else {
                        return value;
                    }
                }">new_weight</th>
                <th data-options="field:'vgm', width:'111', formatter: function(value,row,index){
                    if (value != row.old_vgm){
                        return '<span style=\'color:red;\'>' +  value + '</span>';
                    } else {
                        return value;
                    }
                }">new_vgm</th>
                <th data-options="field:'container_tare', width:'111', formatter: function(value,row,index){
                    if (value != row.old_container_tare){
                        return '<span style=\'color:red;\'>' +  value + '</span>';
                    } else {
                        return value;
                    }
                }">new_container_tare</th>
                <th data-options="field:'volume', width:'111', formatter: function(value,row,index){
                    if (value != row.old_volume){
                        return '<span style=\'color:red;\'>' +  value + '</span>';
                    } else {
                        return value;
                    }
                }">new_volume</th>
                <th data-options="field:'operate', width:'111', formatter: function(value,row,index){
                    var str = '';
                    if(row.id == 0){
                        str += '<button class=\'op_button\' onclick=\'save_container(' + index + ')\'>新增=></button>';
                    }else{
                        if(row.container_no == '' && row.seal_no == '' && row.container_size == '' && row.packs == '' && row.weight == '' && row.volume == ''){
                            str += '<button class=\'op_button\' onclick=\'save_container(' + index + ')\'>删除=></button>';
                        }else{
                            str += '<button class=\'op_button\' onclick=\'save_container(' + index + ')\'>修改=></button>';
                        }
                    }
                    return str;
                }">operate</th>
                <th data-options="field:'old_container_no', width:'111'">old_container_no</th>
                <th data-options="field:'old_seal_no', width:'111'">old_seal_no</th>
                <th data-options="field:'old_container_size', width:'111'">old_container_size</th>
                <th data-options="field:'old_packs', width:'111'">old_packs</th>
                <th data-options="field:'old_weight', width:'111'">old_weight</th>
                <th data-options="field:'old_vgm', width:'111'">old_vgm</th>
                <th data-options="field:'old_container_tare', width:'111'">old_container_tare</th>
                <th data-options="field:'old_volume', width:'111'">old_volume</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($containers as $container){ ?>
                <tr>
                    <td><?= isset($container['id']) ? $container['id'] : 0; ?></td>
                    <td><?= $container['container_no']; ?></td>
                    <td><?= $container['seal_no']; ?></td>
                    <td><?= $container['container_size']; ?></td>
                    <td><?= $container['packs']; ?></td>
                    <td><?= $container['weight']; ?></td>
                    <td><?= $container['vgm']; ?></td>
                    <td><?= $container['container_tare']; ?></td>
                    <td><?= $container['volume']; ?></td>
                    <td></td>
                    <td><?= $container['old_container_no']; ?></td>
                    <td><?= $container['old_seal_no']; ?></td>
                    <td><?= $container['old_container_size']; ?></td>
                    <td><?= $container['old_packs']; ?></td>
                    <td><?= $container['old_weight']; ?></td>
                    <td><?= $container['old_vgm']; ?></td>
                    <td><?= $container['old_container_tare']; ?></td>
                    <td><?= $container['old_volume']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
</table>
