<?php
class Biz_tag extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->model = Model('biz_tag_model');
        $this->relation_model = Model('biz_tag_relation_model');
        $this->field_all = $this->model->field_all;
        $this->field_edit = array();
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }

    /**
     * tag审核页面
     */
    public function index(){
        if(!is_admin()) exit('不是管理员');
        $data = array();
        //获取表头配置
        $data["f"] = $this->field_all;

        $data['config_search'] = array(1, array(array('tag_name', 'like', '')));

        $this->load->view('head');
        $this->load->view('/biz/tag/index_view', $data);
    }

    public function get_data()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $fields = postValue('field', array());


        //-------这个查询条件想修改成通用的 -------------------------------
        $where = array();
        if(!empty($fields))foreach ($fields['f'] as $key => $field){
            $f = trim($fields['f'][$key]);
            $s = $fields['s'][$key];
            $v = trim($fields['v'][$key]);
            if($f == '') continue;
            $pre = 'biz_tag.';
            if($v != '') $where[] = search_like($pre . $f, $s, $v);
        }
        $where_str = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where_str);

        $this->db->limit($rows, $offset);
        $rs = $this->model->get($where_str, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data()
    {
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            //新增可以全部接收
            $temp = trim(postValue($item, ''));
            $data[$item] = $temp;
        }

        //新增的时候检测,系统里不能存在该 箱号
        $tag = $this->model->get_where_one("tag_name = '{$data['tag_name']}'");
        if(!empty($tag)) return jsonEcho(array('code' => 1, 'msg' => '标签名称已存在'));

        $id = $this->model->save($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_tag";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        return jsonEcho(array('code' => 0, 'msg' => '保存成功'));
    }

    public function update_data()
    {
        $id = postValue('id', 0);
        $old_row = $this->model->get_where_one("id = {$id}");
        if(empty($old_row)) return jsonEcho(array('code' => 1, 'msg' => '参数错误'));

        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            //新增可以全部接收
            if(isset($_POST[$item])){
                $temp = postValue($item, '');
                if($temp == $old_row[$item]) continue;
                $data[$item] = trim($temp);
            }

        }

        //新增的时候检测,系统里不能存在该 箱号
        if(isset($data['tag_name'])){
            $tag = $this->model->get_where_one("tag_name = '{$data['tag_name']}'");
            if(!empty($tag)) return jsonEcho(array('code' => 1, 'msg' => '标签名称已存在'));
        }

        
        $id = $this->model->update($id, $data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_tag";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        return jsonEcho(array('code' => 0, 'msg' => '更新成功'));
    }

    /**
     * 审核标签
     */
    public function approve_tag(){
        $id = postValue('id', '');
        if(empty($id)) return jsonEcho(array('code' => 1, 'msg' => '参数错误'));

        $tag = $this->model->get_where_one("id = '{$id}'");
        if(empty($tag)) return jsonEcho(array('code' => 1, 'msg' => '数据不存在'));

        if($tag['pass'] === '1') return jsonEcho(array('code' => 1, 'msg' => '已通过,请勿重复审核'));

        $data = array(
            'pass' => 1,
        );
        $this->model->update($id, $data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_tag";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        return jsonEcho(array('code' => 0, 'msg' => '审核成功'));
    }

    /**
     * 添加标签数据
     */
    public function add_tag(){
        $tag_name = trim(postValue('tag_name', ''));
        $tag_class = trim(postValue('tag_class', ''));
        $id_type = postValue('id_type', '');
        $id_no = postValue('id_no', '');

        if(empty($tag_class) || empty($id_type)) return jsonEcho(array('code' => 1, 'msg' => '参数错误'));
        if(empty($tag_name)) return jsonEcho(array('code' => 1, 'msg' => '标签名不能为空'));

        $data = array();

        //新增tag数据
        $tag = $this->model->get_where_one("tag_name = '{$tag_name}'");
        if(!empty($tag)){
            //如果存在直接获取
            $tag_id = $tag['id'];
            $data['is_pass'] = $tag['pass'];
        }else{
            //不存在新增一个待审核的
            $tag_data = array(
                'tag_name' => $tag_name,
                'tag_class' => $tag_class,
                'id_type' => $id_type,
            );

            $tag_id = $this->model->save($tag_data);

            if($tag_id){
                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_tag";
                $log_data["key"] = $tag_id;
                $log_data["action"] = "insert";
                $log_data["value"] = json_encode($tag_data);
                log_rcd($log_data);
            }
            $data['is_pass'] = 0;
        }

        if(!$tag_id) return jsonEcho(array('code' => 1, 'msg' => '新增失败'));

        //同时绑定一条relation
        //判断是否已存在,存在的话,不添加
        $relation = $this->relation_model->get_where_one("id_type = '{$id_type}' and id_no = '{$id_no}' and tag_id = '{$tag_id}'");

        if(!empty($relation)) return jsonEcho(array('code' => 1, 'msg' => '标签已存在,请勿重复添加'));

        $relation_data = array(
            'tag_id' => $tag_id,
            'id_type' => $id_type,
            'id_no' => $id_no,
        );

        $relation_id = $this->relation_model->save($relation_data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_tag_relation";
        $log_data["key"] = $relation_id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($relation_data);
        log_rcd($log_data);

        return jsonEcho(array('code' => 0, 'msg' => '添加成功', 'data' => $data));
    }

    /**
     * 删除当前关联tag
     */
    public function del_this_tag(){
        $tag_name = str_replace("'", "\'", postValue('tag_name', ''));
        $tag_class = postValue('tag_class', '');
        $id_type = postValue('id_type', '');
        $id_no = postValue('id_no', '');

        if(empty($id_no) || empty($id_type) || empty($tag_name)) return jsonEcho(array('code' => 1, 'msg' => '参数错误'));

        $this->db->select("biz_tag_relation.*");
        $this->db->join("biz_tag", "biz_tag.id = biz_tag_relation.tag_id", 'left');
        $relation = $this->relation_model->get_where_one("biz_tag.tag_name = '{$tag_name}' and biz_tag.tag_class = '{$tag_class}' and biz_tag_relation.id_type = '$id_type' and biz_tag_relation.id_no = '$id_no'");
        if(empty($relation)) return jsonEcho(array('code' => 0, 'msg' => '标签不存在'));//这种前端直接删就好了

        $this->relation_model->mdelete($relation['id']);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_tag_relation";
        $log_data["key"] = $relation['id'];
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($relation);
        log_rcd($log_data);

        return jsonEcho(array('code' => 0, 'msg' => '删除成功'));
    }

    /**
     * 获取当前相关的tag
     */
    public function get_tag(){
        $id_type = getValue('id_type', '');
        $id_no = getValue('id_no', '');
        $data = array();

        $rs = $this->model->get("id_type = '{$id_type}'");
        $system_tag = array();
        foreach ($rs as $row){
            !isset($system_tag[$row['tag_class']]) && $system_tag[$row['tag_class']] = array(
                'tag_class' => $row['tag_class'],
                'data' => array(),
            );
            $system_tag[$row['tag_class']]['data'][] = $row;
        }
        $data['system_tag'] = $system_tag;
        $this->db->select("biz_tag.tag_name,biz_tag.tag_class,biz_tag_relation.id_type,biz_tag_relation.id_no,biz_tag.pass,biz_tag_relation.lock");
        $this->db->join("biz_tag", "biz_tag.id = biz_tag_relation.tag_id", 'left');
        $rs = $this->relation_model->get("biz_tag_relation.id_type = '{$id_type}' and biz_tag_relation.id_no = '{$id_no}'", 'biz_tag.id', 'asc');
        $this_tag = array();
        foreach ($rs as $row){
            !isset($this_tag[$row['tag_class']]) && $this_tag[$row['tag_class']] = array(
                'tag_class' => $row['tag_class'],
                'data' => array(),
            );
            $this_tag[$row['tag_class']]['data'][] = $row;
        }
        $data['this_tag'] = $this_tag;

        return jsonEcho(array('code' => 0, 'msg' => 'success', 'data' => $data));
    }
}