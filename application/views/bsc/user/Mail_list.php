<meta charset="UTF-8">
<script>
    function parent_open_new_tab(title){
        // console.log(title)
        var url = "/bsc_user/list_details?title=" + title;
        var content = '<iframe scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:100%"></iframe>';
        $('#tabs').tabs('add', {
            title: title,
            content: content,
            closable: true
        });
    }
</script>
<body class="easyui-layout" data-options="fit: true" style="padding: 0px; ">

<div data-options="region: 'west', title: '<?= lang('导航栏');?>', iconCls: 'icon-redo', split: true, minWidth: 250, maxWidth: 500"
     style="width: 250px; padding: 1px;">
    <div class="easyui-accordion" data-options="fit: true,border:false">
        <div data-options="title:'<?= lang('菜单');?>',iconCls: 'icon-txl', selected:true" style=" padding: 10px;">

            <div class="easyui-panel" style="padding:5px">
                <ul id="tt"></ul>
            </div>

        </div>
        <div data-options="title:'<?= lang('关于');?>',iconCls: 'icon-help'" style="padding: 10px;">
            <h4>
                <?= lang('暂无信息');?>
            </h4>
        </div>
    </div>
</div>

<div data-options="region: 'center'" style="padding: 1px;">
    <div id="tabs" class="easyui-tabs" data-options="fit: true, border: true">
        <div data-options="title: '<?= lang('通讯录');?>', iconCls: 'icon-write_off'">
            <div style="text-align: left;padding-top: 11px;padding-left:10px;">
            </div>
            <div style="padding-top: 50px;">
                <div style="text-align: center;padding-bottom: 20px">
                    <!--<img src="https://client.leagueshipping.com/inc/image2/logo.png" style="height:50px;">-->
                    <!--|-->
                    <!--<a href="/bsc_user/xmind_leader" target="_blank" style="font-size:14px;color:red;"><?= lang('公司人员架构图');?></a>-->
                    <!--|-->
                    <!--<a href="/bsc_user/xmind_leader_group" target="_blank" style="font-size:14px;color:red;"><?= lang('公司部门架构图');?></a>-->
                </div>
                <div class="container">
                    <div class="input">
                    <input type="text"  placeholder="<?= lang('可查询姓名、电话,多个查询请用逗号隔开');?>" style="text-indent:15px;width:calc(100% - 90px);" id="query"
                           autocomplete="on"></div> 
                    <button onclick="query()"><?= lang('查一查');?></button>
                    <a class="clear" onclick="clear_input()"><?= lang('清空');?></a>
                    <!--公司-->
                    <div class="card_wrap" style="margin-top:10px;">
                        <?= lang('公司');?>：
                        <?php foreach ($company as $item): ?>
                            <div style="padding-right: 8px;margin-bottom: 8px;">
                                <a class="tag"
                                   onclick="company_name('<?php echo $item['company_code']; ?>','<?php echo $item['company_name']; ?>')"><?php echo $item['company_name']; ?> </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <div class="card_wrap" style="margin-top:10px;">
                        <?= lang('角色');?>：
                        <?php foreach ($user_role as $item): ?>
                            <div style="padding-right: 8px;margin-bottom: 10px;">
                                <a class="tag"
                                   onclick="job_label('<?php echo $item['value']; ?>')"><?php echo $item['value']; ?> </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!--job_label-->
                    <div class="card_wrap" style="margin-top:10px;">
                        <?= lang('标签');?>：
                        <?php foreach ($job_label as $items): ?>
                            <div style="padding-right: 8px;margin-bottom: 8px;">
                                <a class="tag"
                                   onclick="job_label('<?php echo $items; ?>')"><?php echo $items; ?></a>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>

            <style>
                .card_wrap {
                    width: 100%;
                    display: flex;
                    flex-wrap: wrap;
                    justify-content: flex-start;
                    padding-top: 10px;
                }

                .tag {
                    border-radius: 20px;
                    padding-top: 4px;
                    padding-bottom: 4px;
                    padding-left: 8px;
                    padding-right: 8px;
                    background: #e8eaec;
                    color: #2d8cf0;
                    cursor: pointer;
                }

                .tag:hover {
                    background: #2d8cf0;
                    color: white;
                }

                .container {
                    position: relative;
                    width: 50%;
                    margin: 0 auto;
                }
                .input {
                    width: 100%;
                    height: 38px;
                    border-radius: 20px ;
                    background-color: #dcdee2;
                    border: none;
                    font-size: 16px;
                    color: #17233d;
                }

                .container input {
                    width: 100%;
                    height: 38px;
                    border-radius: 20px 0px 0px 20px;
                    background-color: #dcdee2;
                    border: none;
                    font-size: 16px;
                    color: #17233d;
                    padding:0;
                    outline: none;
                }

               .input:hover{
                   outline: #2d8cf0 solid 2px;
                }

                .clear:hover {
                    outline: #2d8cf0 solid 1px;
                }

                .container button {
                    position: absolute;
                    top: 0px;
                    right: -5px;
                    width: 90px;
                    background-color: #2d8cf0;
                    border: none;
                    outline: none;
                    cursor: pointer;
                    border-radius: 20px;
                    height: 40px;
                    line-height: 40px;
                    color: #ffffff;
                    font-size: 18px;
                }

                .clear {
                    position: absolute;
                    top: 0px;
                    right: -85px;
                    width: 70px;
                    background-color: #dcdee2;
                    border: none;
                    outline: none;
                    cursor: pointer;
                    border-radius: 20px;
                    height: 40px;
                    line-height: 40px;
                    color: #2d8cf0;
                    font-size: 18px;
                    /*padding-left: 30px;*/
                    text-align: center;
                }

            </style>
</body>

<script>
    $('#tt').tree({
        url:'/bsc_user/getNewTree',
        method: 'get',
        animate: true,
        onClick: function (node) {
            if(node.url != ''){
                var content = '<iframe scrolling="auto" frameborder="0"  src="' + node.url + '" style="width:100%;height:99%"></iframe>';
                $('#tabs').tabs('add', {
                    title: node.text,
                    content: content,
                    closable: true
                });
            }
        }
    });
    function m_level_table(){
        var content = '<iframe scrolling="auto" frameborder="0"  src="/bsc_user/m_level_table" style="width:100%;height:99%"></iframe>';
        $('#tabs').tabs('add', {
            title: '<?= lang('M级别解释');?>',
            content: content,
            closable: true
        });
    }


    //点击公司名
    function company_name(company_code, company_name) {
        var query = $('#query').val();
        if(query==''){
            $('#query').val(company_name);
        }else{
            $('#query').val(query + ',' + company_name);
        }


    }



    //点击标签
    function job_label(job_label) {
        var query = $('#query').val();
        if(query==''){
            $('#query').val(job_label);
        }else{
            $('#query').val(query + ',' + job_label);
        }

    }

    //点击查一查
    function query() {
        var query = $('#query').val();
        query=query.replace(/，/ig,',');
        console.log(query);
        var url = "/bsc_user/mail_list?title=" + query + '&type=2';
        var content = '<iframe scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:100%"></iframe>';
        $('#tabs').tabs('add', {
            title: '<?= lang('查一查');?>',
            content: content,
            closable: true
        });
    }

    //点击清空
    function clear_input() {
        $('#query').val('');
    }

</script>
</html>
