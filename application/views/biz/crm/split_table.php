<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>拆分CRM线索</title>
    <meta name="keywords" content="">
    <meta name="viewport" content="width=1180">
    <link rel="stylesheet" href="/inc/third/handsontable/handsontable.full.min.css"/>
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css"/>
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="/inc/third/handsontable/handsontable.full.min.js"></script>
    <style>
        .search_box {
            border: 1px solid #ddd;
            float: left;
            margin: 10px 0px 0px;
            padding: 15px;
            background-color: #edf2f3;
            width: 99%;
        }
        .selected-td{ background: #ff4c42 !important; }
    </style>
</head>
<body>
<div class="tpl-body" style="padding:30px">
    <div class="layui-row">
        <span style="font-size: 20px;font-weight: bold;color:#1E9FFF;padding-left: 20px">拆分线索</span>
    </div>
    <div style="clear: both; height: 10px;"></div>
    <div style="width: 99%; margin-bottom: 80px;">
        <div id="handsontable_box" style="width: 99%"></div>
    </div>
</div>
<div id="save_tools" style="display:; width: 100%; text-align: center; border-top: 1px solid #008a6b; padding: 20px 0px;position: fixed; z-index: 999999; bottom: 0px; background-color: white;">
    <button class="layui-btn" id="split_btn" style=" width: 200px;">开始拆分</button>
</div>
<div style="display: ">
    <form id="myform" method="post" action="/biz_crm/import">
        <input type="hidden" name="import_data" id="import_data" value="">
    </form>
</div>
</body>
<script type="text/javascript" src="/inc/third/layui/layui.js"></script>
<script type="text/javascript">
    $(function () {
        layui.use(['element', 'form'], function () {
             form = layui.form;
        });
        var columns = [
            {data: 'customer', type: 'text', className: "htLeft", width: 200},
            {data: 'shipper', type: 'text', className: "htLeft", width: 300},
            {data: 'consignee', type: 'text', className: "htLeft", width: 300},
            {data: 'notify', type: 'text', className: "htLeft", width: 300},
            {data: 'pol', type: 'text', className: "htCenter", width: 150},
            {data: 'pod', type: 'text', className: "htCenter", width: 150},
            {data: 'etd', type: 'text', className: "htLeft", width: 150},
            {data: 'vessel', type: 'text', className: "htLeft", width: 150},
            {data: 'product_details', type: 'text', className: "htCenter", width: 250},
            {data: 'bill_no', type: 'text', className: "htCenter", width: 180},
            {data: 'remark', type: 'text', className: "htCenter", width: 100},
        ];
        var colHeaders = ['customer','Shipper','Consignee', 'Notify', 'POL', 'POD', 'ETD', 'vessel', 'product_description', 'bill_no', 'remark'];
        var settingForHandsontable = {
            columns: columns, //列配置参数
            data: [[],[],[]],//为需要绑定数据集合
            licenseKey: 'non-commercial-and-evaluation',
            colHeaders: colHeaders, // 表头
            rowHeaders: true,
            dropdownMenu: true, //忘了
            fillHandle: true,
            contextMenu: true,//右键显示更多功能,
            filters: true,
            startRows: 0,
            startCols: 0,
        };
        var container = document.getElementById('handsontable_box');
        hot = new Handsontable(container, settingForHandsontable);

        //拆分数据
        $('#split_btn').on('click', function () {
            var loading = layer.load(1);
            var field = {};
            field.list_data = [];
            var param = hot.getData();
            var columns = ['customer','shipper', 'consignee', 'notify', 'pol', 'pod', 'etd', 'vessel', 'product_details', 'bill_no', 'remark'];
            $.each(param, function (index, item) {
                field.list_data[index] = JSON.stringify(item);
            });
            field.columns = columns;
            $.ajax({
                type: 'post',
                url: '/biz_crm/split_table',
                data: field,
                success: function (data) {
                    layer.close(loading);
                    if (data.code == 1) {
                        $('#import_data').val(data.data);
                        layer.confirm('折分成功，是否进入导入界面？', {icon: 1, title:'询问'}, function(index){
                            $('#myform').submit();
                        });

                    }else{
                        layer.alert('拆分失败：'+data.msg, {icon: 5});
                    }
                },
                error: function (data) {
                    layer.close(loading);
                    layer.alert('请求失败，错误代码：' + data.status, {icon: 5});
                },
                dataType: "json"
            });
        });
    })
</script>