<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?= lang('我的运价'); ?></title>
    <meta name="keywords" content="">
    <meta name="viewport" content="width=1180">
    <!--<link rel="stylesheet" href="/inc/style.css" >-->
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/inc/css/tab.css">
</head>
<style>
    .layui-table-cell {
        height: auto;
    }
</style>
<body>
<div class="layui-row" style="margin:10px 0 0 10px;">
    <div class="layui-col-md12" style="margin: 20px 0px 0px 20px;">
        <div class="layui-form serchTable">
            <div class="layui-form-item">
                <div class="layui-inline " style="margin-right: 20px;">
                    <label class="layui-form-label"><?=lang('船公司')?>:</label>
                    <div class="layui-input-inline">
                        <select id="serch_carrier" name="carrier" lay-verify="required" lay-search="">
                            <option value=""><?= lang('请选择'); ?></option>
                            <?php if (isset($carr)) foreach ($carr as $v) { ?>
                                <option value="<?php echo $v['carrier']; ?>"><?php echo $v['carrier']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="layui-inline" style="margin-right: 20px;">
                    <label class="layui-form-label"><?=lang('起运港')?>:</label>
                    <div class="layui-input-inline">
                        <select id="serch_loading_code" name="loading_code" lay-verify="required" lay-search="">
                            <!--<option value="CNSHA">SHANGHAI,CHINA</option>-->
                            <?php if (isset($load)) foreach ($load as $v) { ?>
                                <option value="<?php echo $v['loading_code']; ?>" <?php if ($v['loading_code'] == "CNSHA") echo "selected"; ?> ><?php echo $v['loading_port']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="layui-inline" style="margin-right: 20px;">
                    <label class="layui-form-label"><?=lang('目的港')?>:</label>
                    <div class="layui-input-inline layui-form-select">

                        <input type="text" name="discharge_port" id="serch_discharge_port" autocomplete="off"
                               class="layui-input" placeholder="<?= lang('可输入英文或中文检索'); ?>"/>
                        <dl class="layui-anim layui-anim-upbit" id="discharge_port_dl">
                            <!--                            <dd lay-value="CNQDG" class="">QINGDAO,CHINA</dd>-->
                        </dl>
                        <div id="tab" class="tab layui-tab layui-tab-brief div-none">

                            <ul class="layui-tab-title">
                                <?php foreach ($sd_new as $k => $value): ?>
                                    <li <?php if ($k == '澳新'): ?>class="layui-this"<?php endif; ?>><?php echo $k ?></li>
                                <? endforeach; ?>


                            </ul>

                            <div class="layui-tab-content">
                                <?php echo $html_country ?>


                            </div>
                        </div>


                        <!--                        <select id="serch_discharge_port" name="discharge_port" lay-verify="required" lay-search="" >-->
                        <!--                            <option value="">请选择</option>-->
                        <!--                            --><?php //if(isset($disch))foreach($disch as $v){
                        //                                if($v['discharge_port']=="") continue;
                        //                                ?>
                        <!--                                <option  value="-->
                        <?php //echo $v['discharge_port'];?><!--" >-->
                        <?php //echo $v['discharge_port']." | ".$v['discharge_port_cn'];?><!--</option>-->
                        <!--                            --><?php //}?>
                        <!--                        </select>-->
                    </div>
                </div>

                <div class="layui-inline" style="margin-right: 20px;">
                    <label class="layui-form-label"><?=lang('目的港代码')?>:</label>
                    <div class="layui-input-inline layui-form-select">

                        <select id="serch_discharge_code" name="discharge_code" lay-verify="required" lay-search="">
                            <option value=""><?= lang('请选择'); ?></option>
                            <?php if (isset($discharge_code)) foreach ($discharge_code as $v) { ?>
                                <option value="<?php echo $v; ?>"><?php echo $v; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline" style="margin-right: 20px;">
                    <label class="layui-form-label"><?=lang('日期范围')?>:</label>
                    <div class="layui-input-inline">
                        <input type="text" id="serch_date" name="date" class="layui-input" id="test-laydate-range-date"
                               placeholder=" - " style="width: 190px" autocomplete="off" value="">
                    </div>
                </div>
                <div class="layui-inline" style="margin-right: 20px;">
                    <label class="layui-form-label"><?=lang('航线代码')?>:</label>
                    <div class="layui-input-inline">
                        <select id="serch_sailing_code" name="sailing_code" lay-verify="required" lay-search="">
                            <option value=""><?= lang('请选择'); ?></option>
                            <?php if (isset($saili)) foreach ($saili as $v) { ?>
                                <option value="<?php echo $v['sailing_code']; ?>"><?php echo $v['sailing_code']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="layui-inline" style="margin-right: 30px;">
                    <label class="layui-form-label"><?=lang('船期')?>:</label>
                    <div id="serch_sailing_day" style="width: 190px;margin-left: 110px" name="sailing_day"></div>
                </div>
            </div>
            <div class="layui-form-item">
                <div class="layui-inline" style="margin-left: 110px;">
                    <button class="layui-btn layui-btn-normal serch_btn" data-type="reload"><?=lang('查询')?></button>
                </div>
                <div class="layui-inline" style="margin-left: 50px;margin-right: 60px">
                </div>


                <!--<div class="layui-inline" style="margin-right: 20px;">
                    <label class="layui-form-label">运价类型:</label>
                    <div class="layui-input-inline">
                        <select id="serch_id_type" name="id_type" lay-verify="required" lay-search="">
                            <option value="0">全部</option>
                            <option value="1" selected>内部运价</option>
                            <option value="2">外部运价</option>
                        </select>
                    </div>
                </div>-->
                <div class="layui-inline">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-body">
                    <table id="table" lay-filter="table1"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="layui-hide" id="excel-upload">
    <div class="layui-form">
        <div class="layui-form-item">
            <div class="layui-upload" style="margin-left: 50px;">
                <button type="button" class="layui-btn layui-btn-normal" id="upload">选择文件</button>
                <button type="button" class="layui-btn" id="upload_start">开始上传</button>
            </div>
        </div>
    </div>
</div>

<div class="fullbg"></div>
<script src="/inc/third/layui/layui.all.js?v=1" charset="utf-8"></script>
<script type="text/javascript" src="/inc/third/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
    //加载组件
    layui.config({
        base: '/inc/third/layuiadmin/'
    }).extend({
        xmSelect: 'xm-select'
    }).use(['xmSelect'], function () {
        var xmSelect = layui.xmSelect;
        //渲染多选
        var sailing_day = xmSelect.render({
            el: '#serch_sailing_day',
            name: 'serch_sailing_day',
            tips:'<?= lang('请选择船期可多选');?>',
            searchTips:'<?= lang('请选择船期可多选');?>',
            empty:'<?= lang('暂无数据');?>',
            data: [
                {name: '<?= lang('周一');?>', value: 1},
                {name: '<?= lang('周二');?>', value: 2},
                {name: '<?= lang('周三');?>', value: 3},
                {name: '<?= lang('周四');?>', value: 4},
                {name: '<?= lang('周五');?>', value: 5},
                {name: '<?= lang('周六');?>', value: 6},
                {name: '<?= lang('周日');?>', value: 7},
            ]
        })
    })
</script>
<script type="text/html" id="bars">
    <div class="layui-btn-container layadmin-layer-demo">
        
    </div>
</script>
<!--<button class="layui-btn  layui-btn-xs" data-type="test35" id="LAY_layer_iframe_demo" lay-event="history"-->
<!--                style="height: 30px"><?= lang('历史');?>-->
<!--        </button>-->
<script type="text/html" id="40GP">
    {{#  if(d.GP40 === '****'){ }}
    <a href="javascript:;" title="登陆后即可查看">{{ d.GP40 }}</a>
    {{#  } else { }}
    {{ d.GP40 }}
    {{# if(d.GP20DT != '-' || d.HQ40DT != '-' || d.GP40DT != '-'){ }}
    <br/>
    {{ d.GP40DT }}(dt)
    {{#  } }}
    {{# if(d.GP20SPOT != '-' || d.GP40SPOT != '-' || d.HQ40SPOT != '-'){ }}
    <br/>
    {{ d.GP40SPOT }}(SPOT)
    {{#  } }}
    {{#  } }}
</script>
<script type="text/html" id="20GP">
    {{#  if(d.GP20 === '****'){ }}
    <a href="javascript:;" title="登陆后即可查看">{{ d.GP20 }}</a>
    {{#  } else { }}
    {{ d.GP20 }}
    {{# if(d.GP20DT != '-' || d.HQ40DT != '-' || d.GP40DT != '-'){ }}
    <br/>
    {{ d.GP20DT }}(dt)
    {{#  } }}
    {{# if(d.GP20SPOT != '-' || d.GP40SPOT != '-' || d.HQ40SPOT != '-'){ }}
    <br/>
    {{ d.GP20SPOT }}(SPOT)
    {{#  } }}
    {{#  } }}
</script>
<script type="text/html" id="40HQ">
    {{#  if(d.HQ40 === '****'){ }}
    <a href="javascript:;" title="登陆后即可查看">{{ d.HQ40 }}</a>
    {{#  } else { }}
    {{ d.HQ40 }}
    {{# if(d.GP20DT != '-' || d.HQ40DT != '-' || d.GP40DT != '-'){ }}
    <br/>
    {{ d.HQ40DT }}(dt)
    {{#  } }}
    {{# if(d.GP20SPOT != '-' || d.GP40SPOT != '-' || d.HQ40SPOT != '-'){ }}
    <br/>
    {{ d.HQ40SPOT }}(SPOT)
    {{#  } }}
    {{#  } }}
</script>
<script type="text/html" id="40NOR">
    {{#  if(d.NOR40 === '****'){ }}
    <a href="javascript:;" title="登陆后即可查看">{{ d.NOR40 }}</a>
    {{#  } else { }}
    {{ d.NOR40 }}
    {{#  } }}
</script>
<script type="text/html" id="special_size_text">
    {{#  if(d.special_size_text === '****'){ }}
    <a href="javascript:;" title="登陆后即可查看">{{ d.special_size_text }}</a>
    {{#  } else { }}
    {{ d.special_size_text }}
    {{#  } }}
</script>
<script type="text/html" id="GP40_change">
    {{# if(d.GP40_change > 0){ }}
    <span style="color:red;">↑</span>
    {{# }else if(d.GP40_change < 0){ }}
    <span style="color:green;">↓</span>
    {{# }else{ }}
    <span>-</span>
    {{# } }}
</script>
<script type="text/html" id="loading_port">
    {{ d.loading_port }}
</script>
<script type="text/html" id="20GPtemp">
    {{# if(d.promotion != ''){ }}
    <img src="/inc/icon.png" style="width: 20px;height: 20px;position:absolute;left:0px;" title="主推">
    {{# } }}
    {{ d.GP20 }}
</script>
<script type="text/html" id="min_add">
    {{ d.GP20_min_add }}
    /
    {{ d.GP40_min_add }}
    /
    {{ d.HQ40_min_add }}
</script>
<script type="text/html" id="dt_flagtemp">
    {{# if(d.dt_flag == 1){ }}
    DT价
    {{# } }}
</script>
<script type="text/html" id="collect">
    {{# if(d.collect == 1){ }}
    <img src="/inc/images/已收藏.png" onclick="collected(this, '已收藏',{{d.qss_id}})" style="height:18px;cursor: pointer;">
    {{# }else if(d.collect == 0){ }}
    <img src="/inc/images/未收藏.png" onclick="collected(this, '未收藏',{{d.qss_id}})" style="height:18px;cursor: pointer;">
    {{# } }}
</script>
<script type="text/html" id="source_channel">
    {{# if(d.source_channel == 'online'){ }}
    <span style="color: blue;font-weight:bold">电商</span>
    {{# }else if(d.source_channel == 'offline'){ }}
    <span>传统</span>
    {{# } }}
</script>
<script type="text/html" id="sailing_code">
    <a href="javascript:;" onclick="local('/biz_consol/sailing_code?sailing_code={{d.sailing_code}}&trans_origin={{d.loading_code}}&trans_carrier={{d.carrier_code}}')">{{ d.sailing_code }}</a>
</script>
<script type="text/html" id="collect1">
    <a class=" layui-btn-xs layui-icon layui-icon-rmb" lay-event="local"
       style="height: 40px;font-size: 16px;padding-right:12px;"></a>
</script>
<script>
    layui.use(['table', 'laydate', 'upload'], function () {
        var table = layui.table
            , laydate = layui.laydate
            , upload = layui.upload
            , layer = layui.layer;
        laydate.render({
            elem: '#serch_date'
            , range: true
        });
        table.render({
            elem: '#table'
            , url: '/biz_quotation/get_list'
            , toolbar: '#toolbar1' //开启头部工具栏，并为其绑定左侧模板
            , cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
            , autoSort: false
            , cols: [[
                {
                    field: 'btn',
                    width: 60,
                    align: 'center',
                    templet: function (d) {
                        return '<a style="width: 100%; height: 100%;cursor: pointer;font-size:24px;" lay-event="addRowTable">+</a>'
                    }
                }
                // , {field: 'collect', width: 80, title: '<?=lang("收藏")?>', align: 'center', templet: "#collect"}
                , {field: 'collect1', width: 80, title: '<?=lang("信息")?>', align: 'center', templet: "#collect1"}
                , {field: 'GP40_change', width: 40, title: '', templet: '#GP40_change'}
                , {field: 'source_channel', width: 50, title: '<?=lang("渠道")?>',templet: '#source_channel'}
                , {field: 'loading_port', width: 110, title: '<?=lang("起运港")?>', templet: '#loading_port'}
                , {field: 'loading_terminal', width: 60, title: '<?=lang("港区")?>'}
                , {field: 'discharge_code', width: 100, title: '<?=lang("目的港代码")?>'}
                , {field: 'discharge_port', width: 100, title: '<?=lang("目的港")?>'}
                , {field: 'discharge_terminal', width: 100, title: '<?=lang("目的港码头")?>'}
                , {field: 'carrier', width: 85, title: '<?=lang("船公司")?>'}
                , {field: 'sailing_day', width: 70, title: '<?=lang("船期")?>', sort: true}
                , {field: 'sailing_range', width: 60, title: '<?=lang("航程")?>'}
                , {field: 'GP20', width: 90, sort: true, title: '<?=lang("20GP")?>', templet: '#20GP'}
                , {field: 'GP40', width: 90, sort: true, title: '<?=lang("40GP")?>', templet: '#40GP'}
                , {field: 'HQ40', width: 90, sort: true, title: '<?=lang("40HQ")?>', templet: '#40HQ'}
                , {field: 'min_add', width: 90, title: '<?=lang("最低加价")?>', templet: '#min_add'}
                , {field: 'special_size_text', width: 130, sort: true, title: '<?=lang("特殊箱型")?>:<?=lang("价格")?>', templet: '#special_size_text'}
                // ,{field:'price_level', width:90, title: '价格类型'}
                , {field: 'transit_port', width: 90, title: '<?=lang("中转")?>/<?=lang("直达")?>'}
                , {field: 'start_date', width: 110, title: '<?=lang("有效船期")?>', sort: true}
                , {field: 'vessel', width: 100, title: '<?=lang("船名")?>'}
                , {field: 'voyage', width: 100, title: '<?=lang("航次")?>'}
                , {field: 'ETD', width: 100, title: '<?=lang("ETD")?>'}
                , {field: 'sailing_code', width: 100, title: '<?=lang("航线代码")?>',templet: '#sailing_code'}
                , {field: 'sea_clause', width: 100, title: '<?=lang("海运条款")?>'}
                , {
                    field: 'danger_g_remark',
                    minWidth: 100,
                    title: '<?=lang("危险品备注")?>',
                    templet: '<div><div title="{{d.danger_g_remark}}">{{d.danger_g_remark}}</div></div>'
                }
                , {
                    field: 'remark',
                    minWidth: 100,
                    title: '<?=lang("备注")?>',
                    templet: '<div><div title="{{d.remark}}">{{d.remark}}</div></div>'
                }
                , {field: 'market', minWidth: 90, title: '<?=lang("市场")?>'}
                , {field: 'qss_id', width: 80, title: '<?=lang("船期ID")?>'}
            ]]
            , emptyMsg : '无数据，请先收藏!'
            , page: true
            , id: 'table'
            , done: function (res, curr, count) {
                var that = this.elem.next();
                res.data.forEach(function (item, index) {
                    if (item.price_level === "卖价") {
                        var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']");
                        tr.css("background-color", "#00d5ff");
                    }
                });
            },
        });
        //监听工具条
        table.on('tool(table1)', function (obj) {
            var data = obj.data;
            var layEvent = obj.event;
            // 异常不要用它原来的这个作为tr的dom
            // var tr = obj.tr; //获得当前行 tr 的DOM对象
            var $this = $(this);
            var tr = $this.parents('tr');
            var trIndex = tr.data('index');
            if (layEvent === 'history') {
                // history_price('/biz_quotation/history/' + data.id);  //弹窗打开
                window.open('/biz_quotation/history/' + data.id, "_blank");  //在新窗口打开
            } else if (layEvent === 'local') {
                local('/biz_quotation/local_rmb?loading_port=' + data.loading_code + '&carrier=' + data.carrier_code + '&booking_agent=' + data.booking_agent_code + '&sheetname=' + data.sailing_area_code);
            } else if (layEvent === 'feedback') {
                window.open('/biz_quotation/feedback/' + data.id, "_blank");  //在新窗口打开
            } else if (layEvent === 'addRowTable') {
                // 外围的table的id + tableIn_ + 当前的tr的data-index
                $(this).attr('lay-event', 'fold').html('-');
                var tableId = 'tableOut_tableIn_' + trIndex;
                var _html = [
                    '<tr class="table-item">',
                    '<td colspan="' + tr.find('td').length + '" style="padding: 6px 12px;">',
                    '<table id="' + tableId + '"></table>',//可以嵌套表格也可以是其他内容，如是其他内容则无须渲染该表格
                    '</td>',
                    '</tr>'
                ];
                tr.after(_html.join('\n'));
                // 渲染table
                table.render({
                    elem: '#' + tableId,
                    url: '/biz_quotation/get_data_by_shipping_schedule?shipping_schedule_id=' + data.qss_id,
                    cols: [[
                        {field: 'GP40_change', width: 40, title: '', templet: '#GP40_change'},
                        {field: 'GP20', width: 90, sort: true, title: '20GP', templet: '#20GPtemp'},
                        {field: 'GP40', width: 90, sort: true, title: '40GP'},
                        {field: 'HQ40', width: 90, sort: true, title: '40HQ'},
                        {field: 'dt_flag', width: 90, sort: true, title: 'DT标记', templet: '#dt_flagtemp'},
                        {field: 'special_size_text', width: 130, templet: '#special_size_text', title: '特殊箱型:价格'},
                        {field: 'start_date', width: 110, sort: true, title: '有效日期'},
                        {
                            field: 'danger_g_remark',
                            width: 300,
                            templet: '<div><div title="{{d.danger_g_remark}}">{{d.danger_g_remark}}</div></div>',
                            title: '危险品备注'
                        },
                        {
                            field: 'remark',
                            width: 300,
                            templet: '<div><div title="{{d.remark}}">{{d.remark}}</div></div>',
                            title: '备注'
                        },
                        {field: 'vessel', width: 100, title: '船名'},
                        {field: 'voyage', width: 100, title: '航次'},
                        {fixed: 'right', minWidth: 125, width: 125, align: 'center', toolbar: '#bars'},
                    ]],
                    parseData: function (res) { //res 即为原始返回的数据
                        return {
                            "code": res.code, //解析接口状态
                            "msg": res.msg, //解析提示文本
                            "count": res.total, //解析数据长度
                            "data": res.rows //解析数据列表
                        };
                    },
                    done: function (res, curr, count) {
                        var that = this.elem.next();
                        res.data.forEach(function (item, index) {
                            if (item.price_level === "卖价") {
                                var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']");
                                tr.css("background-color", "#00d5ff");
                                // tr.find(".laytable-cell-1-0-9").css("backgroundColor","#00d5ff");
                            }
                        });
                        // for(var i = 0;i < res.data.length;i++){
                        //     // var remark = res.data[i].remark;
                        //     // var table = document.getElementsByClassName('layui-table-body');
                        //     // var trs = table[0].querySelectorAll("tr");
                        //     // if(remark.indexOf("最低卖价") != -1 ){
                        //     //     trs[i].style.backgroundColor = '#00d5ff';
                        //     // }else if(remark.indexOf("指导卖价") != -1 ){
                        //     //     trs[i].style.backgroundColor = '#ffab00';
                        //     // }
                        //     var price_level = res.data[i].price_level;
                        //     var table = $('#' + tableId);
                        //     console.log($(this));
                        //     var trs = table[0].querySelectorAll("tr");
                        //     if(price_level == '卖价' ){
                        //
                        //     }
                        // }
                    },
                });
                // $(window).resize();
            } else if (layEvent === 'fold') {
                $(this).attr('lay-event', 'addRowTable').html('+');
                tr.next().remove();
            }
        });
        var $ = layui.$, active = {
            reload: function () {
                var serch_carrier = $('#serch_carrier').val();
                var serch_loading_code = $('#serch_loading_code').val();
                var serch_discharge_port = $('#serch_discharge_port').val();
                var serch_discharge_code = $('#serch_discharge_code').val();
                var serch_date = $('#serch_date').val().split(' - ');
                var start_date = serch_date[0];
                var end_date = serch_date[1];
                var serch_sailing_code = $('#serch_sailing_code').val();
                var serch_discharge_terminal = $('#serch_discharge_terminal').val();
                var serch_sailing_day = $('input[name="serch_sailing_day"').val();
                var serch_id_type = $('#serch_id_type').val();
                //执行重载
                table.reload('table', {
                    page: {
                        curr: 1 //重新从第 1 页开始
                    }
                    , where: {
                        key: {
                            carrier: serch_carrier,
                            discharge_terminal: serch_discharge_terminal,
                            loading_code: serch_loading_code,
                            discharge_port: serch_discharge_port,
                            start_date: start_date,
                            end_date: end_date,
                            sailing_code: serch_sailing_code,
                            sailing_day: serch_sailing_day,
                            id_type: serch_id_type,
                            discharge_code: serch_discharge_code,
                        }
                    }
                }, 'data');
            }
        };
        table.on('sort(table1)', function (obj) { //注：sort 是工具条事件名，test 是 table 原始容器的属性 lay-filter="对应的值"
            //尽管我们的 table 自带排序功能，但并没有请求服务端。
            //有些时候，你可能需要根据当前排序的字段，重新向服务端发送请求，从而实现服务端排序，如：
            var field = obj.field;
            table.reload('table', {
                initSort: obj //记录初始排序，如果不设的话，将无法标记表头的排序状态。
                , where: { //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
                    field: field //排序字段
                    , order: obj.type //排序方式
                }
            });
        });
        $('.serchTable .layui-btn').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        $('.layui-input').keydown(function (e) {
            if (!$(this).parents('.layui-form-select').hasClass('layui-form-selected')) {//检测下拉框是否打开
                $(this).parents('.layui-form-select').addClass('layui-form-selected');
            }
        });
        //选完文件后不自动上传
        upload.render({
            elem: '#upload'
            , url: '/biz_quotation/upload' //改成您自己的上传接口
            , auto: false
            , exts: 'xlsx|xls'
            , field: 'file'
            , accept: 'file'
            //,multiple: true
            , bindAction: '#upload_start'
            , choose: function (obj) {
                //将每次选择的文件追加到文件队列
                // var files = obj.pushFile();
                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function (index, file, result) {
                    $('.file_name').remove();
                    str = '<div class="file_name">文件名：<span class="layui-upload-choose">' + file.name + '</span></div>';
                    $('.layui-upload').append(str);
                    //obj.upload(index, file); //对上传失败的单个文件重新上传，一般在某个事件中使用
                    //delete files[index]; //删除列表中对应的文件，一般在某个事件中使用
                });
            }
            , before: function (obj) { //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
                layer.load(); //上传loading
            }
            , done: function (res) {
                layer.closeAll();
                $('#excel-upload').addClass('layui-hide');
                if (res.code == 0) {
                    layer.msg(res.msg);
                } else {
                    layer.msg(res.msg);
                }
            }, error: function (index, upload) {
                //当上传失败时，你可以生成一个“重新上传”的按钮，点击该按钮时，执行 upload() 方法即可实现重新上传
                layer.closeAll('loading');
            }
        });
    });

    //历史运价
    function history_price(url) {
        var top = ($(window).height() - $('.layui-table-body').height()) / 2;
        var left = ($(window).width() - $('.layui-table-body').width()) / 2;
        layer.open({
            type: 2,
            title: '历史运价',
            area: ['1146px', '606px'],
            fixed: false,
            offset: [top, left],
            maxmin: true,
            content: url,
            // shade : 0
        });
    }

    //测试环境运价维护
    function ceshiyj() {
        // location.href = "http://testop.leagueshipping.com/biz_quotation_shipping_schedule?u_name=<?= get_session('name');?>";
        var json = {};
        json.u_name = '<?= get_session('name');?>';
        // GET方式示例，其它方式修改相关参数即可
        var form = $('<form></form>').attr('action', 'http://testop.leagueshipping.com/biz_quotation_shipping_schedule/').attr('method', 'POST');
        $.each(json, function (index, item) {
            if (typeof item === 'string') {
                form.append($('<input/>').attr('type', 'hidden').attr('name', index).attr('value', item));
            } else {
                $.each(item, function (index2, item2) {
                    form.append($('<input/>').attr('type', 'hidden').attr('name', index).attr('value', item2));
                })
            }
        });
        form.appendTo('body').submit().remove();
    }

    function local(url) {
        // var top = ($(window).height() - $('.layui-table-body').height()) / 2;
        layer.open({
            type: 2,
            area: ['70%', '70%'],
            fixed: false,
            offset: 'auto',
            maxmin: true,
            content: url,
        });
    }

    //上传文件
    function upload_price() {
        $('#excel-upload').removeClass('layui-hide');
        layer.open({
            type: 1,
            title: '文件上传',
            skin: 'layui-layer-rim',
            area: ['300px', '200px'],
            content: $('#excel-upload'),
            cancel: function (e) {
                $('#excel-upload').addClass('layui-hide');
            }
        });
        return false;
    }

    //收藏和取消
    function collected(e, type, qss_id) {
        console.log(type, qss_id);
        $.ajax({
            url: '/biz_quotation/collected/',
            dataType: 'json',
            data: {
                type: type,
                qss_id: qss_id,
            },
            type: 'post',
            success: function (res) {
               alert(res.msg);
               if(type=='已收藏'){
                   $(e).attr('src','/inc/images/未收藏.png');
               }else if(type=='未收藏'){
                   $(e).attr('src','/inc/images/已收藏.png');
               }
                layui.table.reload('table');

            }
        })

    }
</script>
<script>

    //tab
    $(function () {

        $("input[name='discharge_port']").focus(function () {

            //获取当前文本框的值
            $("#tab").removeClass("div-none");
            $("#tab").addClass("div-active");


            var bh = $(window).height();
            var bw = $(window).width();
            $(".fullbg").css({
                height: bh,
                width: bw,
                display: "block"
            });

        });

        $("input[name='discharge_port']").bind('input propertychange', function () {
            //console.log($(this).val());
            $(".fullbg").css({display: "none"});
            $("#tab").addClass("div-none");
            $("#tab").removeClass("div-active");

            $.getJSON("/biz_quotation/get_discharge_port?key=" + $(this).val(), function (json) {
                //console.log(json);
                var tml = "";
                if (json.code = 200) {
                    for (var o in json.list) {
                        tml += "<dd lay-value=\"" + json.list[o].discharge_port + "\">" + json.list[o].discharge_port + " | " + json.list[o].port_name_cn + "</dd>"
                    }
                    $("#discharge_port_dl").html(tml);
                    $("input[name='discharge_port']").parent().addClass("layui-form-selected")

                }
            });

        });

        $("#discharge_port_dl").on('click', "dd", function () {
            $("input[name='discharge_port']").val($(this).attr("lay-value"));
            var value = $(this).attr("lay-value");

            //2022-06-28 新增关联
            $.ajax({
                url: '/biz_quotation/discharge_terminal_get',
                dataType: 'json',
                type: 'post',
                data: {
                    value: value,
                },
                success: function (data) {
                    //清空当前行field_value的值
                    $('#serch_discharge_terminal').empty();

                    //赋值
                    $.each(data, function (index, value) {
                        $('#serch_discharge_terminal').append(new Option(value.discharge_terminal, value.discharge_terminal));// 下拉菜单里添加元素
                    });

                    layui.form.render("select");//重新渲染 固定写法

                }
            })
        });


        $(".fullbg").click(function () {
            $(".fullbg").css({display: "none"});
            $("#tab").addClass("div-none");
            $("#tab").removeClass("div-active");

        });

        $(".country").click(function () {

            $("input[name='discharge_port']").val($(this).html())
            var value = $(this).html();

            //2022-06-28 新增关联
            $.ajax({
                url: '/biz_quotation/discharge_terminal_get',
                dataType: 'json',
                type: 'post',
                data: {
                    value: value,
                },
                success: function (data) {
                    //清空当前行field_value的值
                    $('#serch_discharge_terminal').empty();

                    //赋值
                    $.each(data, function (index, value) {
                        $('#serch_discharge_terminal').append(new Option(value.discharge_terminal, value.discharge_terminal));// 下拉菜单里添加元素
                    });

                    layui.form.render("select");//重新渲染 固定写法

                }
            })

            $("#tab").addClass("div-none");
            $("#tab").removeClass("div-active");

        });


    });


</script>
</body>
</html>