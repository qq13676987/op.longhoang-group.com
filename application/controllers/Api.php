<?php
class api extends Common_Controller
{

    public function __construct(){
        parent::__construct();
        $this->load->model('m_model');
    }

    public function china_op($cus_no=0,$shipment_id=0){
        $result_json = curl_get("http://china.leagueshipping.com/api/get_shipment_node?cus_no='{$cus_no}'");
        $result = json_decode($result_json, true);
        $res = $result['data'];
        if(empty($res)) exit(json_encode(array("code"=>0,"msg"=>"no data","data"=>array())));

        $this->db = $this->load->database('default',true);
        if($res["dadanwancheng"]>0){
            $idata = array();
            $idata['id_type'] = 'biz_shipment';
            $idata['id_no'] = $shipment_id;
            $idata['node'] = 'booking';
            $idata['node_time'] = $res["dadanwancheng"];
            $idata['position'] = $res["trans_origin"];
            $insert_query = $this->db->insert_string('biz_tracking', $idata);
            $insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
            $this->db->query($insert_query);
        }
        if($res["xiangyijingang"]>0){
            $idata = array();
            $idata['id_type'] = 'biz_shipment';
            $idata['id_no'] = $shipment_id;
            $idata['node'] = 'gate in';
            $idata['node_time'] = $res["xiangyijingang"];
            $idata['position'] = $res["trans_origin"];
            $insert_query = $this->db->insert_string('biz_tracking', $idata);
            $insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
            $this->db->query($insert_query);
        }
        if($res["haiguanfangxing"]>0){
            $idata = array();
            $idata['id_type'] = 'biz_shipment';
            $idata['id_no'] = $shipment_id;
            $idata['node'] = 'customs release';
            $idata['node_time'] = $res["haiguanfangxing"];
            $idata['position'] = $res["trans_origin"];
            $insert_query = $this->db->insert_string('biz_tracking', $idata);
            $insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
            $this->db->query($insert_query);
        }
        if($res["chuanyiqihang"]>0){
            $idata = array();
            $idata['id_type'] = 'biz_shipment';
            $idata['id_no'] = $shipment_id;
            $idata['node'] = 'depature';
            $idata['node_time'] = $res["chuanyiqihang"];
            $idata['position'] = $res["trans_origin"];
            $insert_query = $this->db->insert_string('biz_tracking', $idata);
            $insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
            $this->db->query($insert_query);
        }
        exit('finished!');

        // $data["steps"][] = array("action"=>'custom release',"action_cn"=>'海关放行',"time"=>$res["haiguanfangxing"]);
        // $data["steps"][] = array("action"=>'terminal release',"action_cn"=>'码头放行',"time"=>$res["matoufangxing"]);
        // $data["steps"][] = array("action"=>'carrier release',"action_cn"=>'配载放行',"time"=>$res["peizaifangxing"]); 
        // $data["steps"][] = array("action"=>'Leave port',"action_cn"=>'已离港',"time"=>$res["chuanyiqihang"]); 

    }

    public function sync_user_commission_rate_get($user_id=0){
        // echo md5(date("Y-m-d H"));
        if(strlen($user_id)!=5) exit(json_encode(array("code"=>0,"msg"=>"not available!")));
        $rate = getValue("rate");
        $ymd = getValue("ymd");
        $key = getValue("key");
        if(md5(date("Y-m-d H")) != $key) exit(json_encode(array("code"=>0,"msg"=>"not available!")));
        $sql = "select id,commision_rate from bsc_user_commision_rate where user_id = $user_id and `datetime` = '$ymd'";
        $check = $this->db->query($sql)->row_array();
        if(empty($check)){
            exit(json_encode(array("code"=>1,"msg"=>"-")));
        }else{
            exit(json_encode(array("code"=>1,"msg"=>$check['commision_rate'])));
        }
    }
    public function sync_user_commission_rate_save($user_id=0){
        // echo md5(date("Y-m-d H"));
        if(strlen($user_id)!=5) exit(json_encode(array("code"=>0,"msg"=>"not available!")));
        $rate = getValue("rate");
        $ymd = getValue("ymd");
        $key = getValue("key");
        if(md5(date("Y-m-d H")) != $key) exit(json_encode(array("code"=>0,"msg"=>"not available!")));
        $sql = "select id from bsc_user_commision_rate where user_id = $user_id and `datetime` = '$ymd'";
        $check = $this->db->query($sql)->row_array();
        if(empty($check)){
            $sql = "INSERT INTO  `bsc_user_commision_rate`(`amount`, `user_role`, `user_id`, `commision_rate`, `datetime`) VALUES (9999999999.99, 'sales', $user_id, $rate, '$ymd')";
            $this->db->query($sql);
            exit(json_encode(array("code"=>1,"msg"=>"ok!")));
        }else{
            $sql = "update `bsc_user_commision_rate` set commision_rate = $rate where id = {$check['id']}";
            $this->db->query($sql);
            exit(json_encode(array("code"=>1,"msg"=>"ok!")));
        }
    }

    public function aa(){
        $sql = "select id,station from bsc_user";
        $rs = $this->db->query($sql)->result_array();

        foreach ($rs as $row){
            $station_arr = filter_unique_array(explode(',', $row['station']));

            foreach ($station_arr as $station){
                $this->db->insert("bsc_user_role_relation", array(
                    'user_id' => $row['id'],
                    'user_role_id' => $station,
                    'created_by' => 20233,
                    'created_time' => date('Y-m-d H:i:s')
                ));
            }
        }
    }

    public function biz_edi_ebooking_consol_insert(){
        $from_db = getValue("from_db");
        $id_no = getValue("id_no");
        if($from_db == '' || $id_no == ''){
            $this->save_edi_ebooking_consol_log(["code"=>0,"msg"=>"error!",'from_db'=>$from_db,'id_no'=>$id_no,'created_time'=>date('Y-m-d H:i:s')]);
            exit(json_encode(array("code"=>0,"msg"=>"error!")));
        }
        $key = getValue("key");
        if(md5(date("Y-m-d")) != $key) exit(json_encode(array("code"=>0,"msg"=>"not available!")));
        $db = $this->load->database($from_db,true);
        $consol = $db->where('id',$id_no)->get('biz_consol')->row_array();
        if(empty($consol)){
            $this->save_edi_ebooking_consol_log(["code"=>0,"msg"=>"no consol!",'from_db'=>$from_db,'id_no'=>$id_no,'created_time'=>date('Y-m-d H:i:s')]);
            exit(json_encode(array("code"=>0,"msg"=>"no consol!")));
        }
//		if($consol['agent_email'] == ''){
//			$this->save_edi_ebooking_consol_log(["code"=>0,"msg"=>"no agent email!",'from_db'=>$from_db,'id_no'=>$id_no,'created_time'=>date('Y-m-d H:i:s')]);
//			exit(json_encode(array("code"=>0,"msg"=>"no agent email!")));
//		}
        if($consol['status'] != 'normal'){
            $this->save_edi_ebooking_consol_log(["code"=>0,"msg"=>"not a normal consol!",'from_db'=>$from_db,'id_no'=>$id_no,'created_time'=>date('Y-m-d H:i:s')]);
            exit(json_encode(array("code"=>0,"msg"=>"not a normal consol!")));
        }
        if($consol['biz_type'] == 'export'){
            $user = $this->db->where("catalog = 'branch_office_edi_operator' and `name` = '{$consol['trans_destination_inner']}'")->get('bsc_dict')->row_array();
            if(empty($user)) exit(json_encode(array("code"=>0,"msg"=>"Set a branch_office_edi_operator for port {$consol['trans_destination_inner']}!")));
        }
        if($consol['biz_type'] == 'import'){
            $user = $this->db->where("catalog = 'branch_office_edi_operator' and `name` = '{$consol['trans_origin']}'")->get('bsc_dict')->row_array();
            if(empty($user)) exit(json_encode(array("code"=>0,"msg"=>"Set a branch_office_edi_operator for port {$consol['trans_origin']}!")));
        }
        $user_id = $user['value'];
        $add_data = [
            'from_db'		=> $from_db,
            'id_no'			=> $id_no,
            'job_no'		=> $consol['job_no'],
            'status'		=> 0,
            'ATD'			=> $consol['trans_ATD'],
            'ETD'			=> $consol['trans_ETD'],
            'ETA'			=> $consol['des_ETA'],
            'POL'			=> $consol['trans_origin'],
            'POD'			=> $consol['trans_destination'],
            'voyage'		=> $consol['voyage'],
            'vessel'		=> $consol['vessel'],
            'carrier'		=> $consol['trans_carrier'],
            'created_time'	=> date('Y-m-d H:i:s'),
            'updated_time'	=> date('Y-m-d H:i:s'),
            'created_by'	=> $user_id,
            'updated_by'	=> $user_id,
            'operator_id'	=> 0,
            'customer_service_id'	=> $user_id,
        ];
        if(!empty($this->db->where("id_no = $id_no and from_db = '$from_db' and status in (0,1)")->get('biz_edi_ebooking_consol')->row_array())){
            $this->save_edi_ebooking_consol_log(["code"=>0,"msg"=>"already exists!",'from_db'=>$from_db,'id_no'=>$id_no,'created_time'=>date('Y-m-d H:i:s')]);
            exit(json_encode(array("code"=>1,"msg"=>"no EDI sent this time, as pre-alert EDI had already been sent before.")));
        }
        $res = $this->db->insert('biz_edi_ebooking_consol',$add_data);
        $id = $this->db->insert_id();
        if($id && $user_id != 0){
            add_notice("new edi ebooking need to do ".$consol['job_no'], array("/biz_edi_ebooking_consol/edit/$id",'/biz_edi_ebooking_consol/index'),  $user_id, '',date('Y-m-d'), date('Y-m-d'), 'biz_edi_ebooking_consol', $id, 1, date('Y-m-d'), 'biz_edi_ebooking_consol',0,30);
        }
        $this->save_edi_ebooking_consol_log(["code"=>0,"msg"=>"ok!",'from_db'=>$from_db,'id_no'=>$id_no,'created_time'=>date('Y-m-d H:i:s')]);
        exit(json_encode(array("code"=>1,"msg"=>"ok!")));
    }

    private function save_edi_ebooking_consol_log($data=[]){
        if($data != []){
            $this->db->insert('biz_edi_ebooking_consol_log',$data);
        }
    }
}
