<?php

/**
 * biz_crm_email
 * 无效邮箱
 */
class Biz_crm_email extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('m_model');
    }

    public function index()
    {
        $data = [];
        $this->load->view('biz/crm/email/index.php', $data);
    }

    public function get_data()
    {
        $in=get_session("user_range");
        $in = join(",",$in);
        
        $ql = "select biz_client_contact_list.id,(select `name` from bsc_user where id = biz_client_contact_list.create_by) as create_by_name,(select `id` from bsc_user where id = biz_client_contact_list.create_by) as create_by, if(client_code=\"\",(select client_code from biz_client_crm where id = crm_id limit 1),client_code) as client_code, email from biz_client_contact_list 
left join crm_promote_mail_invalid 
on crm_promote_mail_invalid.invalid_email = biz_client_contact_list.email 
where crm_promote_mail_invalid.ok = -1 and biz_client_contact_list.email !='' and biz_client_contact_list.create_by in ($in) order by create_by_name limit 10000";
        

        // and createby in (get_session(\"user_range\"))



        $rows = $this->m_model->query_array($ql);

        foreach ($rows as $k=>$v){
            $sq_a="SELECT `company_name` FROM `biz_client` WHERE `client_code` = '{$v["client_code"]}'";
            $row_a = $this->m_model->query_one($sq_a);
            if(!empty($row_a)){
                $rows[$k]["company_name"]=$row_a["company_name"];
            }
            else{
                $rows[$k]["company_name"]="";
            }

            $n_email=trim($v["email"]);

            $rows[$k]["act"]="<a href='#' onclick='up_email(\"{$n_email}\")'>删除本条联系人</a> <a href='/other/force_pass_email?email={$n_email}' target='_blank'>强制通过</a>";
        }




        $j_data = [
            "code" => 0,
            "msg" => "success",
            "rows" => $rows,

        ];
        echo json_encode($j_data,JSON_UNESCAPED_UNICODE);


    }

    public function up_mail(){
        $email=isset($_GET["email"])?$_GET["email"]:"";
        $sql_u="update biz_client_contact_list set email = '' where email='$email'";
        $re=$this->m_model->query($sql_u);

        $j_data = [
            "code" => 200,
            "msg" => "更新成功",
            "de"=>$re

        ];
        echo json_encode($j_data,JSON_UNESCAPED_UNICODE);
    }
}