<title>League CRM</title>
<style>
    .tree li ul li{
        padding: 5px 0;
    }
    .tree li{
        padding: 5px 0;
    }
    .tree-title{
        font-size: 20px;
        display: inline-block;
        text-decoration: none;
        vertical-align: top;
        white-space: nowrap;
        padding: 0 10px;
        height: 26px;
        line-height: 21px;
    }
    .tree-node {
        height: 28px;
    }
    .tree-file{
        transform: scale(1.4);
    }
    .tree-collapsed{
        transform: scale(1.4);
    }
    .tree-folder{
        transform: scale(1.4);
    }
    .tree-folder-open{
        transform: scale(1.4);
    }
</style>
<script>
    function setiframeUrl(id, url){
        if(url === '') return false;
        if(url == $('#' + id).attr('src')) return false;
        $('#' + id).attr('src', url);
        return true;
    }

    $(function () {
        $('#tt').tree({
            onClick:function (node) {
                var text = node.text;
                var id = 'rightnav';
                var url = '';
                if(text === '我的简报') url = '/biz_crm/dashboard/';
                else if(text === '目标设置') url = '/biz_crm/crm_set_num/';
                else if(text === '目标完成度') url = '/biz_crm/crm_set_num_complete/';
                else if(text === '私海') url = '/biz_crm/customer_private/';
                else if(text === '公海') url = '/biz_crm/customer_public/';
                // else if(text === '非活跃客户') url = '/biz_crm/customer_inactive/';
                // else if(text === '我的指派') url = '/biz_crm/my_appoint/';
                else if(text === '我的客户') url = '/biz_crm/TotalNumber/';
                else if(text === '黑名单') url = '/biz_client/index/?level=-1';
                else if(text === '邮件群发') url = '/crm_promote_mail/index_new';
                else if(text === '群发历史') url = '/crm_promote_mail/index/';
                else if(text === '公共模板') url = '/crm_promote_mail/index/?group_id=1';
                else if(text === 'CRM客户') url = '/biz_crm/customer_apply/';
               /* else if(text === '申请中') url = '/biz_crm/customer_apply?apply_status=1';
                else if(text === '已通过') url = '/biz_crm/customer_apply?apply_status=2';
                else if(text === '已拒绝') url = '/biz_crm/customer_apply?apply_status=-1';
                else if(text === '转正申请中') url = '/biz_crm/customer_apply?apply_status=3';
                else if(text === '待跟进') url = '/biz_crm/customer_apply?apply_status=4';
                else if(text === '转正通过') url = '/biz_crm/customer_apply?apply_status=0'; */
                else if(text === '外部群发'){
                    window.open('/crm_company_out/search_send');
                    return;
                } 
                setiframeUrl(id, url);
            }
        });
                setiframeUrl('rightnav', '/biz_crm/dashboard/');
    });
</script>
<div id="cc" class="easyui-layout" style="width:100%;height:900px;">
    <div class="left_nav" data-options="region:'west',title:'<?= lang('');?>',split:true" style="width:230px;">
        <div class="easyui-panel" style="padding:5px">
            <ul id="tt">
                <li>
                    <span>我的简报</span>
                </li>
                <li>
                    <span>KPI目标</span>
                    <ul>
                        <li>
                            <span>目标设置</span>
                        </li>
                        <li>
                            <span>目标完成度</span>
                        </li>
                    </ul>
                </li>
                <li>
                    <span>非活跃客户</span>
                    <ul>
                        <li>
                            <span>私海</span>
                        </li>
                        <li>
                            <span>公海</span>
                        </li>
                    </ul>
                </li>
                <li>
                    <span>CRM客户</span>
                    <!--<ul>
                        <li>
                            <span>CRM客户</span>
                        </li>
                        <li>
                            <span>申请中</span>
                        </li>
                        <li>
                            <span>已通过</span>
                        </li>
                        <li>
                            <span>已拒绝</span>
                        </li>
                        <li>
                            <span>待跟进</span>
                        </li>
                        <li>
                            <span>转正申请中</span>
                        </li>
                        <li>
                            <span>转正通过</span>
                        </li>
                    </ul>-->
                </li>
               <!-- <li>
                    <span>我的指派</span>
                </li>-->
                <!--<li>-->
                    <!--<span>我的客户</span>-->
                <!--</li>-->
                <!--<li>-->
                <!--    <span>黑名单</span>-->
                <!--</li>-->
                <li>
                    <span>邮件群发</span>
                </li>
                <li>
                    <span>群发历史</span>
                </li>
                <li>
                    <span>公共模板</span>
                </li>
                <?php if(is_admin() || get_session("id") == 20142){ ?>
                <li>
                    <span>外部群发</span>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="right_nav" data-options="region:'center',title:'<?= lang('');?>'" style="padding:0px;">
        <iframe id="rightnav" scrolling="no" frameborder="0" style="width: 100%; height: 100%;"/>
    </div>
</div>