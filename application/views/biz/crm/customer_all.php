
<!doctype html> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/inc/css/normal.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/icon.css?v=3">
<script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.easyui.min.js?v=11"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.edatagrid.js?v=5"></script>
<script type="text/javascript" src="/inc/js/easyui/datagrid-detailview.js"></script> 
<script type="text/javascript" src="/inc/js/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/inc/js/other.js?v=1"></script>
<script type="text/javascript" src="/inc/js/set_search.js?v=5"></script>
<style type="text/css">
    .f{
        width:115px;
    }
    .s{
        width:100px;
    }
    .v{
        width:200px;
    }
    .del_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }
</style>
<script type="text/javascript">
    function doSearch() {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] = [json[item.name]];
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        $('#tt').datagrid('load', json);
        set_config();
        $('#chaxun').window('close');
    }
    //上传文件操作
    function UploadFile(file_ctrlname, guid_ctrlname, div_files) {
        var value = $("#" + file_ctrlname).filebox('getValue');
        var files = $("#" + file_ctrlname).next().find('input[type=file]')[0].files;

        var guid = $("#" + guid_ctrlname).val();
        if (value && files[0]) {
            //构建一个FormData存储复杂对象
            var formData = new FormData();
            formData.append('file', files[0]);

            $.ajax({
                url: '/excel/upload_excel/client1', //单文件上传
                type: 'POST',
                processData: false,
                contentType: false,
                data: formData,
                success: function (json) {
                    //转义JSON为对象
                    var data = $.parseJSON(json);
                    $.messager.alert("info", data.msg);
                    $('#tt').edatagrid('reload');
                },
                error: function (xhr, status, error) {
                    $.messager.alert("提示", "操作失败"); //xhr.responseText
                }
            });
        }
    }
    var ajax_data = {};
    var setting = {
        'operator_ids':['operator', 'id', 'name']
        ,'customer_service_ids':['customer_service', 'id', 'name']
        ,'sales_ids':['sales', 'id', 'name']
        ,'marketing_ids':['marketing', 'id', 'name']
        ,'oversea_cus_ids':['oversea_cus', 'id', 'name']
        ,'booking_ids':['booking', 'id', 'name']
        ,'document_ids':['document', 'id', 'name']
        ,'finance_ids':['finance', 'id', 'name']
    };

    var datebox = ['created_time', 'updated_time'];
 
    var datetimebox = [];
    $(function () {
        //添加对话框，上传控件初始化
        $('#file_upload').filebox({
            prompt:'Choose another file...',
            buttonText: 'choose file',  //按钮文本
            buttonAlign: 'right',   //按钮对齐
            //multiple: true,       //是否多文件方式
            //accept:'jpg|jpeg|gif|pdf|doc|docx|xlsx|xls|zip|rar'
            accept: ".gif,.jpeg,.jpg,.png,.pdf,.doc,.docx,.xlsx,.xls,.zip,.rar", //指定文件类型
        });

        $('#file_save').click(function () {
            UploadFile("file_upload", "AttachGUID", "div_files");//上传处理
        });
        var selectIndex = -1;
        $('#tt').edatagrid({
            url: '/biz_client/get_data/',
            destroyUrl: '/biz_client/delete_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            }
        });
        
        $('#cx').on('keydown', 'input', function (e) {
            if(e.keyCode == 13){
                doSearch();
            }
        });
        
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
            onDblClickRow: function (rowIndex) {
                $(this).datagrid('selectRow', rowIndex);
                var row = $('#tt').datagrid('getSelected');
                if (row) {
                    url = '/biz_client/edit/' + row.id;
                    window.open(url);
                }
            },
            onClickRow: function (index, data) {
                if (index == selectIndex) {
                    $(this).datagrid('unselectRow', index);
                    selectIndex = -1;
                } else {
                    selectIndex = index;
                }
            }
        });
        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
               //新增查询列
        $('#cx table').on('click' , '.add_tr', function () {
            var index = $('.f').length;
            var str = '<tr>' +
                '                <td>\n' +
                '                    查询\n'+
                '                </td>\n'+
                '                <td align="right">' +
            '                       <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="\n' +
                '                            onChange:function(newVal, oldVal){\n' +
                '                                var index = $(\'.f\').index(this);\n' +
                '                                if(setting.hasOwnProperty(newVal) === true){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                    $(\'.v:eq(\' + index + \')\').combobox({\n' +
                '                                        data:ajax_data[setting[newVal][0]],\n' +
                '                                        valueField:setting[newVal][1],\n' +
                '                                        textField:setting[newVal][2],\n' +
                '                                    });\n' +
                '                                }else if ($.inArray(newVal, datebox) !== -1){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datebox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }else if ($.inArray(newVal, datetimebox) !== -1){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datetimebox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }else{\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }\n' +
                '                            }\n' +
                '                        ">\n' +
                '<option value=\'id\'>id</option><option value=\'client_code\'>客户代码</option><option value=\'client_name\'>客户简称</option><option value=\'company_name\'>客户全称</option><option value=\'province\'>省</option><option value=\'city\'>市</option><option value=\'area\'>区</option><option value=\'sailing_area\'>航线区域</option><option value=\'company_address\'>客户地址</option><option value=\'contact_name\'>客户联系人</option><option value=\'contact_live_chat\'>客户QQ</option><option value=\'contact_telephone\'>手机号(网络账号)</option><option value=\'contact_telephone2\'>客户座机</option><option value=\'contact_email\'>客户邮箱</option><option value=\'contact_position\'>contact_position</option><option value=\'finance_name\'>财务联系人</option><option value=\'finance_live_chat\'>财务QQ</option><option value=\'finance_telephone\'>财务电话</option><option value=\'finance_email\'>财务邮箱</option><option value=\'finance_payment\'>付款方式</option><option value=\'finance_od_basis\'>超期依据</option><option value=\'finance_payment_month\'>finance_payment_month</option><option value=\'finance_payment_days\'>付款天数</option><option value=\'finance_payment_day_th\'>finance_payment_day_th</option><option value=\'credit_amount\'>授信额度</option><option value=\'finance_status\'>财务状态</option><option value=\'finance_remark\'>财务备注</option><option value=\'finance_sign_company_code\'>finance_sign_company_code</option><option value=\'bank_name_cny\'>银行(CNY)</option><option value=\'bank_account_cny\'>银行帐号(CNY)</option><option value=\'bank_address_cny\'>银行地址(CNY)</option><option value=\'bank_swift_code_cny\'>银行swift(CNY)</option><option value=\'bank_name_usd\'>银行(USD)</option><option value=\'bank_account_usd\'>银行帐号(USD)</option><option value=\'bank_address_usd\'>银行地址(USD)</option><option value=\'bank_swift_code_usd\'>银行swift(USD)</option><option value=\'taxpayer_name\'>纳税人名称</option><option value=\'taxpayer_id\'>纳税人识别号</option><option value=\'taxpayer_address\'>纳税人地址</option><option value=\'taxpayer_telephone\'>纳税人电话</option><option value=\'deliver_info\'>快递地址</option><option value=\'website_url\'>企业网址</option><option value=\'source_from\'>信息来源</option><option value=\'role\'>角色</option><option value=\'remark\'>备注</option><option value=\'sales_id\'>管理人</option><option value=\'read_user_group\'>read_user_group</option><option value=\'created_by\'>创建人</option><option value=\'created_time\'>创建时间</option><option value=\'updated_by\'>修改人</option><option value=\'updated_time\'>修改时间</option><option value=\'client_level\'>client_level</option><option value=\'tag\'>标签</option><option value=\'country_code\'>国家代码</option><option value=\'invoice_email\'>invoice_email</option><option value=\'stop_date\'>stop_date</option><option value=\'dn_accounts\'>dn_accounts</option><option value=\'operator_ids\'>operator_ids</option><option value=\'customer_service_ids\'>customer_service_ids</option><option value=\'sales_ids\'>sales_ids</option><option value=\'marketing_ids\'>marketing_ids</option><option value=\'oversea_cus_ids\'>oversea_cus_ids</option><option value=\'booking_ids\'>booking_ids</option><option value=\'document_ids\'>document_ids</option><option value=\'finance_ids\'>finance_ids</option>' +
                '                    </select>\n' +
                '                    &nbsp;\n' +
                '\n' +
                '                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="\n' +
                '                        ">\n' +
                '                        <option value="like">like</option>\n' +
                '                        <option value="=">=</option>\n' +
                '                        <option value=">=">>=</option>\n' +
                '                        <option value="<="><=</option>\n' +
                '                        <option value="!=">!=</option>\n' +
                '                    </select>\n' +
                '                    &nbsp;\n' +
                '                    <div class="this_v">\n' +
                '                        <input name="field[v][]" class="v easyui-textbox" >\n' +
                '                    </div>\n' +
                '                    <button class="del_tr" type="button">-</button>' +
                '   </td>' +
                '</tr>';
            $('#cx table tbody').append(str);
            $.parser.parse($('#cx table tbody tr:last-child'));
            // var this_sf_f = localStorage.getItem('search_f' + $('.f:last').parents('tr').index() + '_' + path);
            // var this_sf_s = localStorage.getItem('search_s' + $('.f:last').parents('tr').index() + '_' + path);
            // if(this_sf_f != null)$('.f:last').combobox('setValue', this_sf_f);
            // if(this_sf_s != null)$('.s:last').combobox('setValue', this_sf_s);
            // localStorage.setItem('search_count_' + path, index);
            return false;
        });
        //删除按钮
        $('#cx table').on('click' , '.del_tr', function () {
            var index = $(this).parents('tr').index();
            // localStorage.removeItem('search_f' + index + '_' + path);
            // localStorage.removeItem('search_s' + index + '_' + path);
            // localStorage.setItem('search_count_' + path, $('.f').length - 2);

            $(this).parents('tr').remove();
            return false;
        });
        var a1 = ajax_get('/bsc_user/get_data_role/operator', 'operator');
        var a2 = ajax_get('/bsc_user/get_data_role/customer_service', 'customer_service');
        var a3 = ajax_get('/bsc_user/get_data_role/sales', 'sales');
        var a4 = ajax_get('/bsc_user/get_data_role/marketing', 'marketing');
        var a5 = ajax_get('/bsc_user/get_data_role/oversea_cus', 'oversea_cus');
        var a6 = ajax_get('/bsc_user/get_data_role/booking', 'booking');
        var a7 = ajax_get('/bsc_user/get_data_role/document', 'document');
        var a8 = ajax_get('/bsc_user/get_data_role/finance', 'finance');
        $.ajax({
            type: 'GET',
            url: '/sys_config/get_config_search/biz_client',
            dataType: 'json',
            success:function (res) {
                // var config = $.parseJSON(res['data']);
                var config = res.data;
                if(config.length > 0){
                    var count = config[0];
                    $('.f:last').combobox('setValue', config[1][0][0]);
                    $('.s:last').combobox('setValue', config[1][0][1]);
                    for (var i = 1; i < count; i++){
                        $('.add_tr:eq(0)').trigger('click');
                        $('.f:last').combobox('setValue', config[1][i][0]);
                        $('.s:last').combobox('setValue', config[1][i][1]);
                    }
                }
            }
        });

        $.when(a1,a2,a3,a4,a5,a6,a7,a8).done(function () {
            $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                var ec_value = $(ec_item).combobox('getValue');
                $(ec_item).combobox('clear').combobox('select', ec_value);
            });
        });
    });
    
    function ajax_get(url,name) {
        return $.ajax({
            type:'POST',
            url:url,
            dataType:'json',
            success:function (res) {
                ajax_data[name] = res;
            },
            error:function () {
                // $.messager.alert('获取数据错误');
            }
        })
    }
    
    function set_config() {
        var f_input = $('.f');
        var count = f_input.length;
        var config = [];
        $.each(f_input, function (index, item) {
            var c_array = [];
            c_array.push($('.f:eq(' + index + ')').combobox('getValue'));
            c_array.push($('.s:eq(' + index + ')').combobox('getValue'));
            config.push(c_array);
        });
        // var config_json = JSON.stringify(config);
        $.ajax({
            type: 'POST',
            url: '/sys_config/save_config_search',
            data:{
                table: 'biz_client',
                count: count,
                config: config,
            },
            dataType:'json',
            success:function (res) {

            },
        });
    }

    function add() {
        var row = $('#tt').datagrid('getSelected');
        if (row) {
             window.open("/biz_client/add");
            // window.open("/biz_client/add/" + row.id);
        } else {
            window.open("/biz_client/add");
        }
    }
    
    
    function client_apply(){
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            window.open("/biz_client_apply/add");
            // window.open("/biz_client/add/" + row.id);
        } else {
            window.open("/biz_client_apply/add");
        }
    }

    function client_confirm(){
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            window.open("/biz_client_apply/index");
            // window.open("/biz_client/add/" + row.id);
        } else {
            window.open("/biz_client_apply/index");
        }
    }

    function config() {
        window.open("/biz_client/config_title/");
    }

    function del() {
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            //if(row.step!="未收款"){
            //	alert("不可以删除");
            //}else{
            //$('#tt').edatagrid('destroyRow');
            //}
            //增加一个手动输入的delete验证
            $.messager.prompt('Tips', '确认删除?请输入delete进行确认', function (r) {
                if(r === 'delete'){
                    $('#tt').edatagrid('destroyRow');
                }
            });
        } else {
            alert("No Item Selected");
        }
    }
    function import_excel(){
        $('#import').window('open');
    }
</script>

<table id="tt" style="width:1100px;height:450px" rownumbers="false" pagination="true" idField="id" pagesize="30" toolbar="#tb" singleSelect="true" nowrap="true">
    <thead>
    <tr><th field="lbl" width="111">客户标签</th>
        <th field="client_code" width="100">客户代码</th><th field="client_name" width="100">客户简称</th> <th field="province" width="100">省</th><th field="city" width="100">市</th>  <th field="company_address" width="120">客户地址</th><th field="contact_name" width="120">客户联系人</th> <th field="contact_telephone" width="120">手机号(网络账号)</th><th field="contact_telephone2" width="120">客户座机</th><th field="contact_email" width="120">客户邮箱</th> <th field="111" width="111">最近成交</th> <th field="111" width="111">当前销售</th>  <th field="111" width="111">转交操作</th>   </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
                   onclick="add();">新增</a>-->
                                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true"
                   onclick="javascript:$('#chaxun').window('open');">查询</a>
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm2',plain:true"
                   iconCls="icon-reload">设置</a> 
            </td>
        </tr>
    </table>
</div>

<div id="mm2" style="width:150px;">
    <div data-options="iconCls:'icon-ok'" onclick="javascript:config()">表头配置</div>
</div>
 

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <form id="cx">
        <table>
            <tr>
                <td>
                    角色                </td>
                <td align="right">
                    <select class="easyui-combobox" id="f_role" name="f_role" style="width: 475px"
                    data-options="
                        valueField:'value',
                        textField:'value',
                        url:'/bsc_dict/get_option/role',
                    ">
    
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    查询                </td>
                <td align="right">
                    <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="
                            onChange:function(newVal, oldVal){
                                var index = $('.f').index(this);
                                if(setting.hasOwnProperty(newVal) === true){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                    $('.v:eq(' + index + ')').combobox({
                                        data:ajax_data[setting[newVal][0]],
                                        valueField:setting[newVal][1],
                                        textField:setting[newVal][2],
                                    });
                                }else if ($.inArray(newVal, datebox) !== -1){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datebox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }else if ($.inArray(newVal, datetimebox) !== -1){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datetimebox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }else{
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }
                            }
                        ">
                        <option value='id'>id</option><option value='client_code'>客户代码</option><option value='client_name'>客户简称</option><option value='company_name'>客户全称</option><option value='province'>省</option><option value='city'>市</option><option value='area'>区</option><option value='sailing_area'>航线区域</option><option value='company_address'>客户地址</option><option value='contact_name'>客户联系人</option><option value='contact_live_chat'>客户QQ</option><option value='contact_telephone'>手机号(网络账号)</option><option value='contact_telephone2'>客户座机</option><option value='contact_email'>客户邮箱</option><option value='contact_position'>contact_position</option><option value='finance_name'>财务联系人</option><option value='finance_live_chat'>财务QQ</option><option value='finance_telephone'>财务电话</option><option value='finance_email'>财务邮箱</option><option value='finance_payment'>付款方式</option><option value='finance_od_basis'>超期依据</option><option value='finance_payment_month'>finance_payment_month</option><option value='finance_payment_days'>付款天数</option><option value='finance_payment_day_th'>finance_payment_day_th</option><option value='credit_amount'>授信额度</option><option value='finance_status'>财务状态</option><option value='finance_remark'>财务备注</option><option value='finance_sign_company_code'>finance_sign_company_code</option><option value='bank_name_cny'>银行(CNY)</option><option value='bank_account_cny'>银行帐号(CNY)</option><option value='bank_address_cny'>银行地址(CNY)</option><option value='bank_swift_code_cny'>银行swift(CNY)</option><option value='bank_name_usd'>银行(USD)</option><option value='bank_account_usd'>银行帐号(USD)</option><option value='bank_address_usd'>银行地址(USD)</option><option value='bank_swift_code_usd'>银行swift(USD)</option><option value='taxpayer_name'>纳税人名称</option><option value='taxpayer_id'>纳税人识别号</option><option value='taxpayer_address'>纳税人地址</option><option value='taxpayer_telephone'>纳税人电话</option><option value='deliver_info'>快递地址</option><option value='website_url'>企业网址</option><option value='source_from'>信息来源</option><option value='role'>角色</option><option value='remark'>备注</option><option value='sales_id'>管理人</option><option value='read_user_group'>read_user_group</option><option value='created_by'>创建人</option><option value='created_time'>创建时间</option><option value='updated_by'>修改人</option><option value='updated_time'>修改时间</option><option value='client_level'>client_level</option><option value='tag'>标签</option><option value='country_code'>国家代码</option><option value='invoice_email'>invoice_email</option><option value='stop_date'>stop_date</option><option value='dn_accounts'>dn_accounts</option><option value='operator_ids'>operator_ids</option><option value='customer_service_ids'>customer_service_ids</option><option value='sales_ids'>sales_ids</option><option value='marketing_ids'>marketing_ids</option><option value='oversea_cus_ids'>oversea_cus_ids</option><option value='booking_ids'>booking_ids</option><option value='document_ids'>document_ids</option><option value='finance_ids'>finance_ids</option>                    </select>
                    &nbsp;

                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="
                        ">
                        <option value="like">like</option>
                        <option value="=">=</option>
                        <option value=">=">>=</option>
                        <option value="<="><=</option>
                        <option value="!=">!=</option>
                    </select>
                    &nbsp;
                    <div class="this_v">
                        <input name="field[v][]" class="v easyui-textbox" >
                    </div>
                    <button class="add_tr" type="button">+</button>
                </td>
            </tr>
        </table>
        <br><br>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="doSearch();">查询</button>
        <a href="javascript:void;" class="easyui-tooltip" data-options="
            content: $('<div></div>'),
            onShow: function(){
                $(this).tooltip('arrow').css('left', 20);
                $(this).tooltip('tip').css('left', $(this).offset().left);
            },
            onUpdate: function(cc){
                cc.panel({
                    width: 500,
                    height: 'auto',
                    border: false,
                    href: '/bsc_help_content/help/query_condition'
                });
            }
        ">help</a>
    </form>
</div>
<div id="import" class="easyui-dialog" style="background-color: #F4F4F4;border:1px solid #95B8E7;margin-top:10px;"
     data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons',title:'导入表格'">
    <table>
        <tr>
            <td>
                <input class="easyui-filebox" name="file" id="file_upload"
                       data-options="" style="width:400px">
            </td>
        </tr>
    </table>
    <div id="dlg-buttons">
        <a class="easyui-linkbutton" id="file_save" iconCls="icon-ok" style="width:90px">保存</a>
    </div>
    <div id="div_files"></div>
</div>