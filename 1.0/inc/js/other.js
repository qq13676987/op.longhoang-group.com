//验证输入的是拼音
        function hanzi(v){             
            if(/.*[\u4e00-\u9fa5]+.*$/.test(v)) 
            { 
               // alert("请输入拼音！"); 
                return false; 
            } 
                return true; 
        } 
 
 //验证箱号              
function gf_trim(as_string)
{
   while(as_string.length > 0 && as_string.indexOf(" ")==0) as_string = as_string.substr(1);
   while(as_string.length > 0 && as_string.lastIndexOf(" ")==(as_string.length-1)) as_string = as_string.substr(0,as_string.length-1);
   return as_string;
}
//集装箱箱号验证
//功能：验证集装箱箱号：
//参数：
//   as_cntrno 是否符合国际标准，
//返回值：True 符合国际标准或强行通过(特殊箱号)
//举例：gf_chkcntrno( 'TEXU2982987', 0 )     
function chkcntrno(as_cntrno,ai_choice)
{
 var fi_ki;
 var fi_numsum;
 var fi_nummod;
 var fai_num = new Array(11);
 var fb_errcntrno=false;

 if (as_cntrno==null) return true; //null不进行验证
 if (gf_trim(as_cntrno)=="") return true; //空不进行验证
 
 if (as_cntrno.length == 11)   //国际标准为11位，最后一位是校验位，若不是11位肯定不是标准箱
 { for(fi_ki=1;fi_ki<=11;fi_ki++)
   fai_num[fi_ki] = 0;
  for(fi_ki=1;fi_ki<=4;fi_ki++)     //根据国际标准验证法则处理箱号前面的4个英文字母
  {
   fch_char=as_cntrno.charAt(fi_ki-1).toUpperCase();
   switch(true)
   { case (fch_char=="A"):{fai_num[fi_ki] = 10;break;}
    case (fch_char>="V" && fch_char<="Z"):{fai_num[fi_ki] = fch_char.charCodeAt() - 52;break;}
    case (fch_char>="L" && fch_char<="U"):{fai_num[fi_ki] = fch_char.charCodeAt() - 53;break;}
    default:{fai_num[fi_ki] = fch_char.charCodeAt() - 54;break;}
   }
  }
  for(fi_ki=5;fi_ki<=11;fi_ki++)
  {  fch_char=as_cntrno.charAt(fi_ki-1);
   fai_num[fi_ki] = parseInt(fch_char); //ctype((mid(as_cntrno, fi_ki, 1)), integer)
      }
  fi_numsum = 0
  
  for(fi_ki=1;fi_ki<=10;fi_ki++)
  { 
   fi_sqr = 1;
   for(i=1;i<fi_ki;i++){fi_sqr *=2;}
   fi_numsum += fai_num[fi_ki] * fi_sqr;
  }

  if (as_cntrno.substr(0,4) == "HLCU") fi_numsum = fi_numsum - 2; //hapaq lloyd的柜号与国际标准相差2
  fi_nummod = fi_numsum % 11;
  if (fi_nummod == 10) fi_nummod = 0;
  if (fi_nummod == fai_num[11]) fb_errcntrno = true;
  return fb_errcntrno;
 }else{
    return fb_errcntrno;
 }  
} 

var excel_datas = {};
var excel_param = {};

function export_Html(rows, caption, fields){
    rows = rows;
    var data = ['<table border="1" rull="all" style="border-collapse:collapse">'];
    var fields = fields;
    var trStyle = 'height:32px';
    var tdStyle0 = 'vertical-align:middle;padding:0 4px';
    if (caption){
        data.push('<caption>'+caption+'</caption>');
    }
    data.push('<tr style="'+trStyle+'">');
    for(var i=0; i<fields.length; i++){
        var tdStyle = tdStyle0 + ';width:200px;';
        tdStyle += ';text-align:center';
        data.push('<td style="'+tdStyle+'">'+fields[i]+'</td>');
    }
    data.push('</tr>');
    $.map(rows, function(row){
        data.push('<tr style="'+trStyle+'">');
        for(var i=0; i<fields.length; i++){
            var field = fields[i];
            var value = row[field];
            if (value == undefined){
                value = '';
            }
            var tdStyle = tdStyle0;
            tdStyle += ';text-align:center';
            data.push(
                '<td style="'+tdStyle+'">'+value+'</td>'
            );
        }
        data.push('</tr>');
    });
    data.push('</table>');
    return data.join('');
}

function export_Excel(){
    var filename = null;
    var caption = null;
    var rows = excel_datas;
    var param = excel_param;
    var worksheet = 'Worksheet';
    var fields = {};
    if (typeof param == 'string'){
        filename = param;
    } else {
        filename = param['filename'];
        caption = param['caption'];
        worksheet = param['worksheet'] || 'Worksheet';
        fields = param['fields'];
    }
    var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>{table}</body></html>'
    , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }

    var table = export_Html(rows, caption, fields);
    var ctx = { worksheet: worksheet, table: table };
    var data = base64(format(template, ctx));
    if (window.navigator.msSaveBlob){
        var blob = b64toBlob(data);
        window.navigator.msSaveBlob(blob, filename);
    } else {
        var alink = $('<a style="display:none"></a>').appendTo('body');
        alink[0].href = uri + data;
        alink[0].download = filename;
        alink[0].click();
        alink.remove();
    }
}

function copy (e, is_alert = true) {
    var text;
    if(e === null) e = '';
    if(e.innerText != undefined){
        text = e.innerText;
    }else{
        text = e;
    }
    copytext(text,function(){
        if(is_alert)alert("复制成功！");
    });
}
function copytext(txt,cd){
    var Url2 = txt;
    var oInput = document.createElement('textarea');
    oInput.value = Url2;
    document.body.appendChild(oInput);
    // oInput.select();
    selectText(oInput)
    document.execCommand("Copy");
    oInput.className = 'oInput';
    oInput.style.display = 'none';
    cd();
};
function selectText(oInput) {
    if(oInput.createTextRange) {//ie
        oInput.select();
    }else{//firefox/chrome
        oInput.setSelectionRange(0, oInput.value.length);
        oInput.focus();
    };
};

function getMonthDay(year, month) {
    let d = new Date(year, month + 1, 0);
    return d.getDate();
}

function urlencode (str) {  
    str = (str + '').toString();

    //.replace(/%20/g, '+') 这里去除了%20 替换为+的代码, 因为这里导致跳转详情时, 空格变为+号
    return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').  
    replace(/\)/g, '%29').replace(/\*/g, '%2A');
}

//获取get参数
var $_GET = (function(){
    var url = window.document.location.href.toString(); //获取的完整url
    var u = url.split("?");
    if(typeof(u[1]) == "string"){
        u = u[1].split("&");
        var get = {};
        for(var i in u){
            var j = u[i].split("=");
            if(j[0] == "") continue;
            get[decodeURIComponent(j[0])] = decodeURIComponent(j[1]);
        }
        return get;
    } else {
        return {};
    }
})();

//查询框相关代码--start
var ajax_data = {};//这个是存储已加载的数据使用的
var view_name = '';//视图名称

function get_function(fn_name){
    //判断函数是否存在
    if(typeof window[fn_name] === 'function'){
        return window[fn_name];
    }else{
        //不存在使用默认的
        return undefined;
    }
}

/**
 *  读取数据且存储到对应的格子上,
 */
function set_data_by_url(jq, url, data_name) {
    //如果不存在,加载
    if(ajax_data[data_name] === undefined){
        ajaxLoading();
        return get_ajax_data(url, data_name, function (res) {
            input_load_data(jq, res);
            ajaxLoadEnd();
        });
    }else{
        input_load_data(jq, ajax_data[data_name]);
        return true;
    }
}

/**
 * 加载数据
 */
function get_ajax_data(url, data_name, fn = function () {}) {
    ajaxLoading();
    //这里可以做点cookie什么的优化下
    return $.ajax({
        type: 'get',
        url: url,
        dataType:'json',
        success:function (res) {
            ajax_data[data_name] = res;
            fn(res);
        },error:function (e) {
            ajaxLoadEnd();
            $.messager.alert("<?= lang('提示');?>", e.responseText);
        }
    });
}

/**
 * 给控件加载数据
 */
function input_load_data(jq, loaddata) {
    var this_data = $.data(jq[0]);
    //这个页面里,只有4个
    //textbox,combobox,datebox,datetimebox
    if(this_data.combobox !== undefined){
        return $(jq).combobox('loadData', loaddata);
    }
}

/**
 * 字段框的下拉事件
 */
function select_f_change(newVal, oldVal){
    ajaxLoading();
    var index = $('.f').index(this);
    //明天这里改为读取option, 然后相关的格式化代码加在这里
    var this_option = query_option[newVal];
    $('.v:eq(' + index + ')').textbox('destroy');
    if(this_option !== undefined){
        var this_editor_config;
        //2022-11-10 已支持json串里的函数
        try {
            this_editor_config = JSON.parse(this_option.editor_config,function(k,v){
                if(v && typeof v === 'string') {
                    return v.indexOf('function') > -1 || v.indexOf('FUNCTION') > -1 ? new Function(`return ${v}`)() : v;
                }
                return v;
            });
        }catch (e) {
            this_editor_config = {};
        }
        $(this).siblings('.this_v').append('<input name="field[v][]" class="v" >');

        //如果有url, 那么这里会进行替换 这里因为改为了newVal,所以到时候字段不同,但是加载值相同的框,这里可能会多加载次
        var this_editor_config_url;
        if(this_editor_config.mode !== 'remote'){//如果是remote模式这里不进行提前加载
            this_editor_config_url = this_editor_config.url;
            delete this_editor_config.url;//移除对象,避免后面加载了
        }


        if(this_option.editor === 'combobox'){
            $('.v:eq(' + index + ')').combobox(this_editor_config);
        }else if(this_option.editor === 'datebox'){
            $('.v:eq(' + index + ')').datebox(this_editor_config);
        }else if(this_option.editor === 'datetimebox'){
            $('.v:eq(' + index + ')').datetimebox(this_editor_config);
        }else if(this_option.editor === 'numberbox'){
            $('.v:eq(' + index + ')').numberbox(this_editor_config);
        }else{
            $('.v:eq(' + index + ')').textbox(this_editor_config);
        }
        //判断是否开启了多行模式,开启的话,添加一个放大镜
        //开启多行配置时,生成一个放大镜按钮
        var this_loupe = $('.this_v:eq(' + index + ')').siblings('.loupe');
        if(this_editor_config.multiline === true && this_loupe.length === 0){
            var button_str = "<button class=\"loupe\" onclick=\"loupe(" + index + ")\" type=\"button\"></button>";
            $('.this_v:eq(' + index + ')').after(button_str);
            $('.loupe').linkbutton({
                iconCls:'icon-search',
            });
        }else{
            if(this_editor_config.multiline !== true && this_loupe.length > 0) this_loupe.remove();
        }
        //这里进行加载数据
        if(this_editor_config_url !== undefined) {
            set_data_by_url($('.v:eq(' + index + ')'), this_editor_config_url, newVal);
            this_editor_config.url = this_editor_config_url;
        }
    }else{
        //没有检测到的统一当成textbox处理
        $(this).siblings('.this_v').append('<input name="field[v][]" class="v easyui-textbox" >');
        $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
    }
    ajaxLoadEnd();
}

function set_config() {
    var f_input = $('.f');
    var count = f_input.length;
    var config = [];
    $.each(f_input, function (index, item) {
        var c_array = [];
        c_array.push($('.f:eq(' + index + ')').combobox('getValue'));
        c_array.push($('.s:eq(' + index + ')').combobox('getValue'));
        c_array.push($('.v:eq(' + index + ')').textbox('getValue'));
        config.push(c_array);
    });
    //如果有auto_click, 就不处理
    var auto_click = $_GET['auto_click'];
    if(auto_click === '1') return;
    // var config_json = JSON.stringify(config);
    $.ajax({
        type: 'POST',
        url: '/sys_config/save_config_search',
        data:{
            table: view_name,
            count: count,
            config: config,
        },
        dataType:'json',
        success:function (res) {

        },
    });
}

function loupe(index){
    var loupe_div = $("<div id='loupe' style=\"padding:5px;\"></div>");
    var val = $('.v:eq(' + index + ')').textbox('getValue');
    loupe_div.dialog({
        title:'loupe',
        closed:false,
        modal:true,
        border:'thin',
        content:"<textarea id=\"loupe_textarea\" onkeyup=\"loupe_textarea_keyup(this)\" v_index='" + index + "' style=\"width: 200px;height: 200px\">" + val + "</textarea>",
    });
}

function loupe_textarea_keyup(e){
    var index = $(e).attr('v_index');
    var val = $(e).val();
    $('.v:eq(' + index + ')').textbox('setValue', val);
}

function config(){
    window.open("/sys_config_title/index_user?table_name=" + table_name + "&view_name=" + view_name) ;
}

var load = false;

function ajaxLoading(content="加载中,请稍候。。。"){
    if(load) return false;
    load = true;
    $("<div class=\"datagrid-mask\" style=\"z-index:99999;\"></div>").css({display:"block",width:"100%",height:$(window).height()}).appendTo("body");
    $("<div class=\"datagrid-mask-msg\" style=\"z-index:99999;\"></div>").html(content).appendTo("body").css({display:"block",left:($(document.body).outerWidth(true) - 190) / 2,top:($(window).height() - 45) / 2});
}
function ajaxLoadEnd(){
    load = false;
    $(".datagrid-mask").remove();
    $(".datagrid-mask-msg").remove();
}

//放大镜功能
$('#cx table').on('click' , '.loupe', function () {
    var tr = $(this).parents('tr');

    $('#loupe').dialog('open');
    $('#loupe_textarea').attr('index', tr.index()).val(tr.find('.v').textbox('getValue'));
});
$('#loupe_textarea').keyup(function () {
    var index = $(this).attr('index');
    var val = $(this).val();
    // console.log($('#cx table tr:eq(' + index + ')').find('.v').textbox('getValue'));//, $(this).val
    $('#cx table tr:eq(' + index + ')').find('.v').textbox('setValue', val);
});
//查询框相关代码--end
