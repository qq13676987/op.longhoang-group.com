<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>Client-Add</title>
<script type="text/javascript" src="/inc/js/Convert_Pinyin.js"></script>
<script type="text/javascript">
    var is_search = false;
    var is_qcc_search = false;
    function checkvalue() {
        $('#form').form('submit', {
            url: "/biz_client/add_data/",
            onSubmit: function () {
                var vv = $('#country_code').combobox('getValue');
                if (vv == "") {
                    alert("country_code required！");
                    return false;
                }
                var vv = $('#client_name').textbox('getValue');
                if (vv == "") {
                    alert("client_name required！");
                    return false;
                }
                var vv = $('#company_name').textbox('getValue');
                if (vv == "") {
                    alert("company_name required！");
                    return false;
                }
                
                var vv = $('#company_name_en').textbox('getValue');
                if (vv == "") {
                    alert("company_name_en required！");
                    return false;
                }


                
                // var vv = $('#province').textbox('getValue');
                // var city = $('#city').textbox('getValue');
                // var area = $('#area').textbox('getValue');
                // if (vv == "" || city == "" || area == "") {
                //     alert("client area required！");
                //     return false;
                // }
                
                // var vv = $('#company_address').textbox('getValue');
                // if (vv == "") {
                //     alert("company_address required！");
                //     return false;
                // }

                 $('#role').val($('#role1').combobox('getValues'));
                var vv = $('#role1').combobox('getValues');
                if(vv == ''){
                    alert("role required！");
                    return false;
                }
                
                //country_code
                country =  $('#country_code').combobox('getValue');
                if(country != "CN"){
                    // var vv = $('#bank_name_usd').textbox('getValue');
                    // if (vv == "") {
                    //     alert("bank_name_usd required！");
                    //     return false;
                    // }
                    // var vv = $('#bank_account_usd').textbox('getValue');
                    // if (vv == "") {
                    //     alert("bank_account_usd required！");
                    //     return false;
                    // }
                }else{ 
                    // var vv = $('#bank_name_cny').textbox('getValue');
                    // if (vv == "") {
                    //     alert("bank_name_cny required！");
                    //     return false;
                    // }
                    // var vv = $('#bank_account_cny').textbox('getValue');
                    // if (vv == "") {
                    //     alert("bank_account_cny required！");
                    //     return false;
                    // }
                }
                
                // var vv = $('#taxpayer_name').textbox('getValue');
                // if (vv == "") {
                //     alert("taxpayer_name required！");
                //     return false;
                // }
                // var vv = $('#taxpayer_id').textbox('getValue');
                // if (vv == "") {
                //     alert("taxpayer_id required！");
                //     return false;
                // }
                // var vv = $('#taxpayer_address').textbox('getValue');
                // if (vv == "") {
                //     alert("taxpayer_address required！");
                //     return false;
                // }
                // var vv = $('#taxpayer_telephone').textbox('getValue');
                // if (vv == "") {
                //     alert("taxpayer_telephone required！");
                //     return false;
                // }
                var validate = $('#form').form('validate');
                if(!validate){
                    alert("<?= lang('有字段未填写,请检查是否有红色输入框');?>");
                    return false;
                }

                // let bank_name_cny = $('#bank_name_cny').textbox('getValue');
                // if(bank_name_cny.length < 5){
                //     $('#bank_name_cny').textbox('setValue','x')
                // }
                // let bank_account_cny = $('#bank_account_cny').textbox('getValue');
                // if(bank_account_cny.length < 5){
                //     $('#bank_account_cny').textbox('setValue','x')
                // }
                // let bank_name_usd = $('#bank_name_usd').textbox('getValue');
                // if(bank_name_usd.length < 5){
                //     $('#bank_name_usd').textbox('setValue','x')
                // }
                // let bank_account_usd = $('#bank_account_usd').textbox('getValue');
                // if(bank_account_usd.length < 5){
                //     $('#bank_account_usd').textbox('setValue','x')
                // }
                // let taxpayer_name = $('#taxpayer_name').textbox('getValue');
                // if(taxpayer_name.length < 5){
                //     $('#taxpayer_name').textbox('setValue','x')
                // }
                // let taxpayer_address = $('#taxpayer_address').textbox('getValue');
                // if(taxpayer_address.length < 5){
                //     $('#taxpayer_address').textbox('setValue','x')
                // }
                // let taxpayer_telephone = $('#taxpayer_telephone').textbox('getValue');
                // if(taxpayer_telephone.length < 5){
                //     $('#taxpayer_telephone').textbox('setValue','x')
                // }
                // let taxpayer_id = $('#taxpayer_id').textbox('getValue');
                // if(taxpayer_id.length < 5){
                //     $('#taxpayer_id').textbox('setValue','x')
                // }
                ajaxLoading();
            },
            success: function(res_json){
                var res;
                ajaxLoadEnd();
                try{
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res === undefined){
                    alert(res);
                }else{
                    if(res.code == 0){
                        alert(res.msg);
                        window.location.href='/biz_client/edit/' + res.data.id;
                    }else if(res.code == 2){
                        click_role_open(res.data.role);
                        alert(res.msg);
                    }else{
                        alert(res.msg);
                    }
                }
            }
        });
    }
    //获取综合描述
    function get_finance_describe(){
        return;
        var finance_payment = $('.finance_payment:checked').val();
        var finance_od_basis = $('.finance_od_basis:checked').val();

        var lang = '';
        if(finance_od_basis == 'monthly'){
            $('#finance_payment_month').textbox('enable');
            $('#finance_payment_day_th').textbox('enable');
            $('#finance_payment_days').textbox('disable');

            var finance_payment_month = $('#finance_payment_month').numberbox('getValue');
            var finance_payment_day_th = $('#finance_payment_day_th').numberbox('getValue');

            if(finance_payment == 'after work'){
                lang = '<?= lang('Payment on the (day)th day of (month) months after work');?>';
            }else if(finance_payment == 'after invoice') {
                lang = '<?= lang('Payment on the (day)th day of (month) months after invoice');?>';
            }

            lang = lang.replace('(month)', finance_payment_month).replace('(day)', finance_payment_day_th);

        }else if(finance_od_basis == 'daily'){
            $('#finance_payment_month').textbox('disable');
            $('#finance_payment_day_th').textbox('disable');
            $('#finance_payment_days').textbox('enable');

            var finance_payment_days = $('#finance_payment_days').numberbox('getValue');

            if(finance_payment == 'after work'){
                lang = '<?= lang('Payment (day) days after work');?>';
            }else if(finance_payment == 'after invoice') {
                lang = '<?= lang('Payment (day) days after invoice');?>';
            }
            lang = lang.replace('(day)', finance_payment_days);
        }
        // $('#finance_describe').textbox('setValue',lang);
    }
    

    function role_select() {
        if($('#role1').length > 0){
            $('#role').val($('#role1').combobox('getValues'));
        }

        return false;
        //这里根据当前选择了几个角色,然后在contact里生成对应的几个
        var role_length = $('#role1').combobox('getValues').length;

        var contact_list = $('.contact_list');
        var contact_list_length = contact_list.length;
        //1、角色数大于contact 数
        //2、角色数等于contact 数
        for (var i = 0; i < role_length; i++){
            if(contact_list_length > i) continue;
            var required = false;
            if(i == 0) required = true;
            var str = '<fieldset class="contact_list">\n' +
                '                        <legend>联系人' + (i + 1) + '</legend>\n' +
                '                        <table width="450" border="0" cellspacing="0">\n' +
                '                            <tr>\n' +
                '                                 <td><?= lang('contact role');?></td>\n' +
                '                                 <td>\n' +
                '                                    <select class="easyui-combobox" editable="false" style="width:255px;" data-options="required:' + required + ',\n' +
                '                                        multiple:false,\n' +
                '                                        valueField:\'value\', \n' +
                '                                        textField:\'value\', \n' +
                '                                        url:\'/bsc_dict/get_option/role\', \n' +
                '                                        onChange: function(newVal, oldVal){\n' +
                '                                            $(\'#this_role_' + i + '\').val(newVal)\n' +
                '                                        }\n' +
                '                                    ">\n' +
                '                                     </select>\n' +
                '                                     <input type="hidden" id="this_role_' + i + '" name="contact[' + i + '][role]" >\n' +
                '                                 </td>\n' +
                '                            </tr>\n' +
                '                            <tr>\n' +
                '                                <td><?= lang('contact_name'); ?> </td>\n'+
                '                                <td>\n'+
                '                                    <input class="easyui-textbox" name="contact[' + i + '][name]" data-options="required:' + required + '" style="width:255px;"/>\n'+
                '                                </td>\n'+
                '                            </tr>\n'+
                '                            <tr>\n'+
                '                                <td><?= lang('contact_live_chat'); ?> </td>\n'+
                '                                <td>\n'+
                '                                    <input class="easyui-textbox" name="contact[' + i + '][live_chat]" style="width:255px;"/>\n'+
                '                                </td>\n'+
                '                            </tr>\n'+
                '                            <tr>\n'+
                '                                <td><?= lang('contact_telephone'); ?> </td>\n'+
                '                                <td>\n'+
                '                                    <input class="easyui-textbox" name="contact[' + i + '][telephone]" data-options="required:' + required + '" style="width:255px;"/>\n'+
                '                                </td>\n'+
                '                            </tr>\n'+
                '                            <tr>\n'+
                '                                <td><?= lang('contact_telephone2'); ?> </td>\n'+
                '                                <td>\n'+
                '                                    <input class="easyui-textbox" name="contact[' + i + '][telephone2]" data-options="required:' + required + '" style="width:255px;"/>\n'+
                '                                </td>\n'+
                '                            </tr>\n'+
                '                            <tr>\n'+
                '                                <td><?= lang('contact_email'); ?> <a href="/other/force_pass_email" target="_blank">(<?= lang('手动验证邮箱'); ?>)</a></td>\n'+
                '                                <td>\n'+
                '                                    <input class="easyui-validatebox" name="contact[' + i + '][email]" data-options="validType:\'email\'" style="width:255px;"/>\n'+
                '                                </td>\n'+
                '                            </tr>\n'+
                '                            <tr>\n'+
                '                                <td><?= lang('position'); ?> </td>\n'+
                '                                <td>\n'+
                '                                    <select class="easyui-combobox" name="contact[' + i + '][position][]" data-options="required:' + required + ',multiple:true,\n' +
                '                editable:false,\n' +
                '                valueField:\'value\',\n' +
                '                textField:\'value\',\n' +
                '                url:\'/bsc_dict/get_option/contact_position\'" style="width:255px;"></select>\n'+
                '                                </td>\n'+
                '                            </tr>\n'+
                '                            <tr>\n'+
                '                                <td><?= lang('remark'); ?> </td>\n'+
                '                                <td>\n'+
                '                                    <input class="easyui-textbox" name="contact[' + i + '][remark]" style="width:255px;"/>\n'+
                '                                </td>\n'+
                '                            </tr>\n'+
                '                            <tr>\n'+
                '                        </table>\n'+
                '                    </fieldset>';
            $('#el').layout('panel', 'east').append(str);
            $.parser.parse('.contact_list:eq(' + i + ')');
        }
        //3、角色数小于contact 数 那么把多余的删除
        var now_contach_list_length = $('.contact_list').length;
        if(now_contach_list_length > role_length){
            for (var i = (now_contach_list_length - 1); i >= role_length; i--){
                $('.contact_list:eq(' + i + ')').remove();
            }
        }
    }

    //查找相似度
    function search_similar() {
        var company_name = $('#company_name').textbox('getValue');
        if(company_name === ''){
            $.messager.alert('Tips', '先填写全称');
            return;
        }
        ajaxLoading();
        is_search = true;
        $.ajax({
            type:'GET',
            url:'/biz_client/search?company_name=' + company_name,
            success:function (res_json) {
                ajaxLoadEnd();
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误,请联系管理员');
                }else{
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg + '<br />最接近的为' + res.data.company_name + ',相似度为' + res.data.percent + '%');
                    }else{
                        $.messager.alert('Tips', res.msg);
                    }
                    
                }
            },
        }); 
    }

    $(function () {
        $('.finance_od_basis').click(function(){
            var val = $(this).val();
            $('.finance_od_basis').not(this).attr('checked', false);
            if(val == 'monthly'){
                $('#finance_payment_month').textbox('enable');
                $('#finance_payment_day_th').textbox('enable');
                $('#finance_payment_days').textbox('disable');
            }else if(val == 'daily'){
                $('#finance_payment_month').textbox('disable');
                $('#finance_payment_day_th').textbox('disable');
                $('#finance_payment_days').textbox('enable');
            }else if(val == 'daily2'){
                $('#finance_payment_month').textbox('disable');
                $('#finance_payment_day_th').textbox('disable');
                $('#finance_payment_days').textbox('enable').textbox('setValue', 0);
                val = 'daily';
            }
            $('#finance_od_basis').val(val);
            get_finance_describe();
        });

        $('.finance_payment').click(function () {
            get_finance_describe();
        });

        $('.finance_od_basis:checked').trigger('click');
        $('#company_name').textbox('textbox').change(function(){
            is_search = false;
        });
        role_select();
        
    });
</script>
<style type="text/css">
    input:disabled, select:disabled, textarea:disabled{
        background-color: #d6d6d6;
        cursor: default;
    }
    .hide{
        display:none;
    }
</style>
<body style="margin:10px;">
<table>
    <tr>
        <td>
        <td></td>
    </tr>
</table>

<div class="easyui-tabs" style="width:100%;height:850px">
    <div title="Basic info" style="padding:1px">
        <form id="form" name="f" method="post" action="">
            <input type="hidden" name="temp_client_code" id="temp_client_code" value="<?= $temp_client_code;?>">
            <div class="easyui-layout" id="el" style="width:100%;height:780px;">
                <div data-options="region:'west',tools:'#tt'" title="Basic" style="width:450px;">
                    <table width="450" border="0" cellspacing="0" style="padding:10px">
                        <tr>
                            <td><?= lang('level');?></td>
                            <td>
                                <input type="radio" name="client_level" id="client_level0" value="1" <?php if($client_level == 1) echo 'checked';?>><?= lang('formal');?>
                                <input type="radio" name="client_level" id="client_level1" value="0" <?php if($client_level == 0) echo 'checked';?>><?= lang('potential');?>
                            </td>
                        </tr>
                        <?php if($client_code !== ''){ ?>
                        <tr>
                            <td><?= lang('client_code'); ?> </td>
                            <td>
                                <input class="easyui-textbox" readonly name="client_code" id="client_code" style="width:255px;"
                                       value="<?php echo $client_code; ?>"/>
                            </td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td><?= lang('country_code'); ?></td>
                            <td>
                                <select class="easyui-combobox" required name="country_code" id="country_code" style="width: 255px;" data-options="
                                    value:'<?= $country_code;?>',
                                    valueField:'value',
                                    textField:'namevalue',
                                    url:'/bsc_dict/get_option/country_code',
                                    onHidePanel: function() {
										var valueField = $(this).combobox('options').valueField;
										var val = $(this).combobox('getValue');
										var allData = $(this).combobox('getData');
										var result = true;
										for (var i = 0; i < allData.length; i++) {
											if (val == allData[i][valueField]) {
												result = false;
											}
										}
										if (result) {
											$(this).combobox('clear');
										}
								    },
								    onSelect: function(rec){
                                           console.log(rec.value);
								         if(rec.value=='CN'){
								            $('#company_name').textbox({ readonly: true });  
                                         }else{
								            $('#company_name').textbox({ readonly: false }); 
                                         }

                                    },
                                ">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('company_name'); ?> </td>
                            <td>
                                <input class="easyui-textbox" required name="company_name" id="company_name" style="width:255px;"
                                       value="<?php echo $company_name; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('company_name_en'); ?> </td>
                            <td>
                                <input class="easyui-textbox" <?= $company_name_en !== '' ? 'readonly' : '';?> required name="company_name_en" id="company_name_en" style="width:255px;"
                                       value="<?php echo $company_name_en; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('client_name'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="client_name" id="client_name" data-options="required:true" style="width:255px;"
                                       value="<?php echo $client_name; ?>"/>
                            </td>
                        </tr>
                        <!--<tr>
                            <td><?= lang('client area');?></td>
                            <td>
                                <select class="easyui-combobox" name="province" id="province"  data-options="
                                valueField:'cityname',
                                textField:'cityname',
                                url:'/city/get_province',
                                onSelect: function(rec){
                                    if(rec != undefined){
                                        $('#city').combobox('reload', '/city/get_city/' + rec.id);
                                    }
                                }," style="width: 80px;"></select>

                                <select class="easyui-combobox" name="city" id="city" style="width: 80px;" data-options="
                                    valueField:'cityname',
                                    textField:'cityname',
                                    onSelect: function(rec){
                                        if(rec != undefined){
                                            $('#area').combobox('reload', '/city/get_area/' + rec.id);
                                        }
                                    },
                                    onLoadSuccess:function(){
                                        var this_data = $(this).combobox('getData');
                                        var this_val = $(this).combobox('getValue');
                                        var rec = this_data.filter(el => el['cityname'] === this_val);
                                        if(rec.length > 0)rec = rec[0];
                                        $('#area').combobox('reload', '/city/get_area/' + rec.id);
                                    }
                                "></select>

                                <select class="easyui-combobox" name="area" id="area" style="width: 80px;" data-options="
                                    valueField:'cityname',
                                    textField:'cityname',
                                "></select>
                            </td>
                        </tr>-->
                        <tr>
                            <td><?= lang('company_address'); ?></td>
                            <td>

                                <textarea style="width: 255px;height: 40px" class="textarea b" name="company_address"
                                          id="company_address"><?= $company_address ?></textarea>
                            </td>
                        </tr>
                        <!--<tr>-->
                        <!--    <td><?= lang('sailing_area'); ?></td>-->
                        <!--    <td>-->
                        <!--        <select class="easyui-combobox" name="sailing_area" id="sailing_area" style="width:255px;" data-options="-->
                        <!--            valueField:'sailing_area',-->
                        <!--            textField:'sailing',-->
                        <!--            url:'/biz_port/get_sailing',-->
                        <!--            value:'<?php echo $sailing_area;?>',-->
                        <!--        ">-->
                        <!--        </select>-->
                        <!--    </td>-->
                        <!--</tr>-->
                      <!--  <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>----------------</td>
                            <td>
                               ------------------------------------------------------
                            </td>
                        </tr> -->
                        <!--<tr>
                            <td><?/*= lang('deliver_info'); */?> </td>
                            <td>
                                <input class="easyui-textbox" name="deliver_info" id="deliver_info" data-options="" style="width:255px;"
                                       value="<?php /*echo $deliver_info; */?>"/>
                            </td>
                        </tr>-->
                        <tr>
                            <td><?= lang('remark'); ?> </td>
                            <td>
                                <textarea style="width: 255px;height: 40px" class="textarea" name="remark" id="remark"><?= $remark ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td width="100px">----------------</td>
                            <td>
                               ------------------------------------------------------
                            </td>
                        </tr>
                        <!--<tr class="supplier_admin_ids1 hide">-->
                        <!--    <td>引进人<?php show_tip("用于审批延期申请");?></td>-->
                        <!--    <td>-->
                        <!--        <select class="easyui-combogrid default_partner" name="supplier_admin_ids"-->
                        <!--                editable="true" id="supplier_admin_ids1" ids="supplier_admin_ids" style="width: 255px"></select>-->
                        <!--    </td>-->
                        <!--</tr>-->
                        <tr>
                            <td><?= lang('role'); ?> </td>
                            <td>
                                <script>
                                    function check_client(val){
                                        var tmp_val=val;
                                        if (val == 'factory') tmp_val='factoryclient';
                                        var selected = $('#role1').combobox('getValues');
                                        selected.pop(); //删除刚刚选择的角色
                                        //把已选择的角色拼接成字符串
                                        var selected_str = selected.join(',');
                                        selected_str = selected_str.replace('factory', 'factoryclient');

                                        //已选择的角色是否已包含了client类型
                                        var selected_client = selected_str.search(/client/);
                                        //刚刚选中的角色是否包含client
                                        var now_select_client = tmp_val.search(/client/);
                                        if(selected_client >= 0 && now_select_client >= 0){
                                            alert('只能选择一个“client”类型的角色！')
                                            $('#role1').combobox('unselect', val);
                                        }
                                    }
                                </script>
                                <select id="role1" class="easyui-combobox" editable="false" name="role1" style="width:255px;" data-options="required:true,
                                    multiple:true,
    								valueField:'value', 
    								textField:'value', 
    								url:'/bsc_dict/get_option/role', 
    								value:'<?php echo $role; ?>',
    								onChange: function(newVal, oldVal){
    								    role_select();
                                        load_role_tag();
                                        
                                        //供应商显示引进人
                                        var supplier = ['booking_agent', 'truck', 'carrier', 'warehouse', 'declaration', 'other'];
                                        var is_supplier = false;
                                        $.each(newVal, function(i, it){
                                            if($.inArray(it, supplier) !== -1) is_supplier = true;
                                        });
                                        if(is_supplier) $('.supplier_admin_ids1').removeClass('hide');
                                        else $('.supplier_admin_ids1').addClass('hide');
    								},
    								onSelect:function(res){
    								    check_client(res.value);
    								}
								">
                                </select>
                                <input type="hidden" id="role" name="role" value="<?php echo $role; ?>">
                                <a href="javascript:void;" class="easyui-tooltip" data-options="
                                    content: $('<div></div>'),
                                    onShow: function(){
                                        $(this).tooltip('arrow').css('left', 20);
                                        $(this).tooltip('tip').css('left', $(this).offset().left);
                                    },
                                    onUpdate: function(cc){
                                        cc.panel({
                                            width: 500,
                                            height: 'auto',
                                            border: false,
                                            href: '/bsc_help_content/help/role'
                                        });
                                    }
                                "><?= lang('help');?></a>
                                <input type="hidden" name="read_user_group" id="read_user_group">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <!--<span style="color:red;">点击蓝色标签<br>设置操作等岗位</span>-->
                            </td>
                            <td>
                                <style type="text/css">
                                    .topic-tag{
                                        display:inline-block
                                    }
                                    .topic-tag .text {
                                        display: inline-block;
                                        height: 16px;
                                        line-height: 16px;
                                        padding: 2px 5px;
                                        background-color: #99cfff;
                                        font-size: 12px;
                                        color: #fff;
                                        border-radius: 4px;
                                        cursor: pointer;
                                    }
                                    .topic-tag {
                                        margin: 0 5px 2px 0;
                                    }
                                    .topic-tag .text:hover, .topic-tag .close:hover{
                                        background-color: #339dff;
                                    }
                                    .this_tag .topic-tag .text, .role_tag .topic-tag .text{
                                        border-radius: 4px 0 0 4px;
                                        float: left;
                                    }
                                    .this_tag .topic-tag .close, .role_tag .topic-tag .close{
                                        float: left;
                                    }
                                    .topic-tag-apply .text{
                                        background-color: #cccccc;
                                    }
                                    .topic-tag .close {
                                        width: 20px;
                                        height: 20px;
                                        background-color: #66b7ff;
                                        text-align: center;
                                        line-height: 20px;
                                        color: #fff;
                                        font-size: 10px;
                                        opacity: 1;
                                        filter: alpha(opacity=100);
                                        border-radius: 0 4px 4px 0;
                                        display: inline-block;
                                        text-shadow: 0 1px 0 #fff;
                                        cursor: pointer;
                                    }
                                    .add_tags_body_input{
                                        display: inline-block;
                                        vertical-align: middle;
                                        width: 200px !important;
                                        margin: 0 5px 10px 0;
                                        padding: 6px;
                                        resize: none;
                                        box-shadow: none;
                                        height: 22px;
                                        font-size: 14px;
                                        line-height: 1.42857143;
                                        color: #555;
                                        background-color: #fff;
                                        background-image: none;
                                        border: 1px solid #ccc;
                                        border-radius: 4px;
                                        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                                        float: left;
                                    }
                                    .add_tags_body_type{
                                        display: inline-block;
                                        vertical-align: middle;
                                        width: 100px !important;
                                        margin: 0 5px 10px 0;
                                        padding: 6px;
                                        resize: none;
                                        box-shadow: none;
                                        height: 36px;
                                        font-size: 14px;
                                        line-height: 1.42857143;
                                        color: #555;
                                        background-image: none;
                                        border: 1px solid #ccc;
                                        border-radius: 4px;
                                        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                                        float: left;
                                    }
                                    .add_tags_body_input_btn:hover{
                                        background-color: rgb(22,155,213);
                                    }
                                    .add_tags_body_input_btn{
                                        margin: 0 10px 10px 0;
                                        border: none !important;
                                        background-color: rgb(94,180,214);
                                        color: #fff;
                                        min-width: 76px;
                                        height: 34px;
                                        padding: 0 10px;
                                        line-height: 34px;
                                        font-size: 14px;
                                        display: inline-block;
                                        font-weight: 400;
                                        text-align: center;
                                        white-space: nowrap;
                                        vertical-align: middle;
                                        cursor: pointer;
                                        -webkit-user-select: none;
                                        -moz-user-select: none;
                                        -ms-user-select: none;
                                        user-select: none;
                                        background-image: none;
                                        border-radius: 4px;
                                        box-sizing: border-box;
                                    }
                                    .add_tags_body h3{
                                        margin-top: 20px;
                                        color: #333;
                                        font-size: 100%;
                                        line-height: 1.7;
                                        margin-bottom: 10px;
                                    }
                                    .add_tags_body{
                                        padding:10px;
                                    }
                                </style>
                                <script type="text/javascript">
                                    /**
                                     * 添加一个tag标签
                                     * @param tag_name 标签名
                                     * @param tag_class 标签类
                                     * @param tag_type 标签类型, 目前有this_tag 右上角的,system_tag 系统推荐标签
                                     * @param pass 是否通过,没通过的是灰色
                                     */
                                    function add_tag_html(tag_name, tag_class, tag_type, pass = 0) {
                                        var span_class = "topic-tag";
                                        if(pass == 0) span_class += ' topic-tag-apply';

                                        if(tag_type === 'role_tag'){
                                            //如果不存在,生成一个
                                            if($(".role_tag[tag_class='" + tag_class + "']").length == 0){
                                                var topic_bar_html = "<div class=\"topic-bar clearfix role_tag\" tag_class=\"" + tag_class + "\" style=\"margin: 0;\"></div>";
                                                $('.role_tag_box').append(topic_bar_html);
                                            }

                                            var tag_html = "<span class=\"" + span_class + "\">\n" +
                                                "                    <a class=\"text\" onclick=\"click_role_open('" + tag_name + "')\" role=\"" + tag_name + "\">" + tag_name + "</a>\n" +
                                                "<a class=\"close\" onclick=\"click_del_role('" + tag_name + "')\" role=\"" + tag_name + "\">X</a>" +
                                                "                </span>";
                                            $(".role_tag[tag_class='" + tag_class + "']").append(tag_html);
                                        }
                                    }

                                    //删除role角色
                                    function click_del_role(role) {
                                        //去除标签,且去除role里的值即可
                                        $('#role1').combobox('unselect', role);
                                        $(e).parents('.topic-tag').remove();
                                    }

                                    //打开client 角色维护权限窗口
                                    function click_role_open(role) {
                                        // var role = $(e).attr('role');
                                        var client_code = $('#temp_client_code').val();
                                        $('#window_iframe').css({width: 600,height:500,}).attr('src', '/biz_client_duty/index?is_config_sales_op=false&&client_code=' + client_code + '&client_role=' + role);
                                        $('#window').window({
                                            title:role + '<?= lang(' 角色权限');?>',
                                            width:'615',
                                            height:'539',
                                            top:200,
                                            modal:true
                                        });
                                    }

                                    function load_role_tag(){
                                        if($('#role').val() == '') return false;
                                        var role = $('#role').val().split(',');
                                        $('.role_tag_box').empty();
                                        $.each(role, function (i, it) {
                                            add_tag_html(it, '', 'role_tag', 1);
                                        });
                                        return true;
                                    }

                                    $(function () {
                                        load_role_tag();
                                    });
                                </script>
                                <div class="role_tag_box" style="width: 285px;">
                                </div>
                            </td>
                        </tr>

                    </table>
                    <br><br> <br><br> <br><br>
                    <div id="tt">
                        <a href="javascript:void(0)" class="icon-save" onclick="checkvalue();" title="save"
                           style="margin-right:15px;"></a>
                    </div>
                </div>
                <div data-options="region:'center',title:'Finance'">
                    <table width="500" border="0" cellspacing="0">
                        <input type="hidden" id="finance_payment" value="<?= $finance_payment;?>">
                        <input type="hidden" id="finance_od_basis" name="finance_od_basis" value="<?= $finance_od_basis;?>">
                        <!--<tr>
                            <td><?= lang('finance_payment'); ?> </td>
                            <td>
                                <input type="radio" readonly class="finance_payment" name="finance_payment" value="after work" checked><?= lang('after work');?>
                                <input type="radio" class="finance_payment" name="finance_payment" value="after invoice" ><?= lang('after invoice');?>
                            </td>
                        </tr>-->
                        <!--<tr>
                            <td><?= lang('finance_od_basis'); ?> </td>
                            <td>
                                <input type="radio" readonly class="finance_od_basis" value="monthly"><?= lang('monthly');?>
                                <input type="radio" readonly class="finance_od_basis" value="daily" ><?= lang('daily');?>
                                <input type="radio" readonly class="finance_od_basis fkmd" value="daily2" checked><?= lang('付款买单');?>
                            </td>
                        </tr>-->
                        <tr>
                            <td><?= lang('finance_payment_month_day'); ?> </td>
                            <td>
                                <input class="easyui-numberbox" readonly name="finance_payment_month" id="finance_payment_month" style="width: 117px;" data-options="
                                    min:1,
                                    value: '1',
                                    onChange:function(newValue,oldValue){
                                        get_finance_describe();
                                    }
                                ">
                                <?= lang('month');?>
                                <input class="easyui-numberbox" readonly name="finance_payment_day_th" id="finance_payment_day_th" style="width: 117px;" data-options="
                                    min:1,
                                    max:28,
                                    value: '20',
                                    onChange:function(newValue,oldValue){
                                        get_finance_describe();
                                    }
                                ">
                                <?= lang('th');?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('credit_amount');?></td>
                            <td>
                                <input class="easyui-numberbox" readonly name="credit_amount" id="credit_amount" style="width: 95px;" value="0" data-options="min:0,precision:2">
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('finance_payment_day'); ?> </td>
                            <td>
                                <input class="easyui-numberbox" readonly name="finance_payment_days" id="finance_payment_days" style="width: 117px;" data-options="
                                    min:0,
                                    value: '<?= $finance_payment_days;?>',
                                    onChange:function(newValue,oldValue){
                                        get_finance_describe();
                                    }
                                ">
                                <?= lang('days');?>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('finance_free_overdue_date');?></td>
                            <td>
                                <input class="easyui-datebox" name="finance_free_overdue_date" id="finance_free_overdue_date" style="width:255px" value="<?= $finance_free_overdue_date;?>">
                            </td>
                        </tr>
                        <!--<tr>
                            <td><?= lang('finance_describe'); ?> </td>
                            <td>
                                <input class="easyui-textbox" disabled id="finance_describe"
                                       style="width:255px;" />
                            </td>
                        </tr>-->
                        <tr>
                            <td><?= lang('finance_remark'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="finance_remark" id="finance_remark"
                                       style="width:255px;" value="<?php echo $finance_remark; ?>"/>
                            </td>
                        </tr>

                        <!--<tr>
                            <td>--------------------------------</td>
                            <td>
                               ------------------------------------------------------
                            </td>
                        </tr> -->
                        <!--<tr>
                            <td><?= lang('bank_name_cny'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_name_cny" data-options="required:true" 
                                       id="bank_name_cny"
                                       style="width:255px;" value="<?php echo $bank_name_cny; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('bank_account_cny'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_account_cny" data-options="required:true" 
                                       id="bank_account_cny"
                                       style="width:255px;" value="<?php echo $bank_account_cny; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('bank_address_cny'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_address_cny"
                                       id="bank_address_cny"
                                       style="width:255px;" value="<?php echo $bank_address_cny; ?>"/>
                            </td>
                        </tr> 
                        <tr>
                            <td><?= lang('bank_swift_code_cny'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_swift_code_cny"
                                       id="bank_swift_code_cny"
                                       style="width:255px;" value="<?php echo $bank_swift_code_cny; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>--------------------------------</td>
                            <td>
                               ------------------------------------------------------
                            </td>
                        </tr> -->
                        <!--<tr>
                            <td><?= lang('bank_name_usd'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_name_usd" data-options="required:true" id="bank_name_usd" style="width:255px;" value="<?php echo $bank_name_usd; ?>"/>
                            </td>
                        </tr>
                        <tr>

                            <td><?= lang('bank_account_usd'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_account_usd"
                                       id="bank_account_usd" data-options="required:true" 
                                       style="width:255px;" value="<?php echo $bank_account_usd; ?>"/>
                            </td>
                        </tr> 
                        <tr>
                            <td><?= lang('bank_address_usd'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_address_usd"
                                       id="bank_address_usd"
                                       style="width:255px;" value="<?php echo $bank_address_usd; ?>"/>
                            </td>
                        </tr> 
                        <tr>
                            <td><?= lang('bank_swift_code_usd'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="bank_swift_code_usd"
                                       id="bank_swift_code_usd"
                                       style="width:255px;" value="<?php echo $bank_swift_code_usd; ?>"/>
                            </td>
                        </tr>
                        <tr>-->
                            <td>----------------------------------</td>
                            <td>
                                --------------------------------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('taxpayer_name'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="taxpayer_name" data-options="required:true" 
                                       id="taxpayer_name" style="width:255px;" value="<?php echo $taxpayer_name; ?>"/>
                            </td>
                        </tr>
                        <tr>

                            <td><?= lang('taxpayer_id'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="taxpayer_id" data-options="required:true" 
                                       id="taxpayer_id" style="width:255px;" value="<?php echo $taxpayer_id; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('taxpayer_address'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="taxpayer_address" data-options="required:true" 
                                       id="taxpayer_address" style="width:255px;" value="<?php echo $taxpayer_address; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('taxpayer_telephone'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="taxpayer_telephone" data-options="required:true" 
                                       id="taxpayer_telephone" style="width:255px;" value="<?php echo $taxpayer_telephone; ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr> 
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                    <br>

                </div>
                <!--<div data-options="region:'east', title:'Contact'" style="width:450px;">-->

                <!--</div>-->
            </div>
        </form>
    </div>
</div>

<div id="window" style="display:none;">
    <iframe id="window_iframe" name="window_iframe" frameborder="0"></iframe>
</div>
</body>

<script>
    $(function () {
        // //实现表格下拉框的分页和筛选--start
        // var html = '<div id="supplier_admin_Tb">';
        // html += '<form id="supplier_admin_form" style="padding:5px">';
        // html += '<div style="padding-left: 5px;display: inline-block;"><label>姓名 : </label><input name="name" class="easyui-textbox user_search"style="width:96px"data-options="prompt:\'text\'"/></div>';
        // html += '<div style="padding-left: 5px;display: inline-block;"><label>部门 : </label><input name="group" class="easyui-textbox user_search"style="width:96px"data-options="prompt:\'text\'"/></div>';

        // html += '<div style="padding-left: 10px; display: inline-block;"><form>';
        // html += '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="supplier_admin_QueryReport">查询</a>';
        // html += '</div>';
        // html += '</div>';
        // $('body').append(html);
        // $.parser.parse('#supplier_admin_Tb');

        // $('#supplier_admin_ids1').combogrid({
        //     panelWidth: '600px',
        //     panelHeight: '400px',
        //     width: 255,
        //     multiple: false,
        //     idField: 'id',              //ID字段
        //     textField: 'name',    //显示的字段
        //     fitColumns: true,
        //     striped: true,
        //     editable: false,
        //     pagination: true,           //是否分页
        //     pageList: [30, 100, 500],
        //     pageSize: 500,
        //     toolbar: '#supplier_admin_Tb',
        //     rownumbers: true,           //序号
        //     collapsible: true,         //是否可折叠的
        //     method: 'post',
        //     columns: [[
        //         {field: 'name', title: '姓名', width: 120},
        //         {field: 'group_name', title: '部门', width: 120},
        //         {field: 'group', title: '部门代码', width: 120},

        //     ]],
        //     emptyMsg: '未找到相应数据!',
        //     onLoadSuccess: function (data) {

        //     },
        //     url: '/bsc_user/get_user_tree2/?more_ids=',
        // });
        // //点击搜索
        // $('#supplier_admin_QueryReport').click(function () {
        //     var where = {};
        //     var form_data = $('#supplier_admin_form').serializeArray();
        //     $.each(form_data, function (index, item) {
        //         if (where.hasOwnProperty(item.name) === true) {
        //             if (typeof where[item.name] == 'string') {
        //                 where[item.name] = where[item.name].split(',');
        //                 where[item.name].push(item.value);
        //             } else {
        //                 where[item.name].push(item.value);
        //             }
        //         } else {
        //             where[item.name] = item.value;
        //         }
        //     });
        //     // where['field'] = $('#to_search_field').combo('getValue');

        //     $('#supplier_admin_ids1').combogrid('grid').datagrid('load', where);
        // });
        // //text添加输入值改变
        // $('.user_search').textbox('textbox').keydown(function (e) {
        //     if (e.keyCode == 13) {
        //         $('#supplier_admin_QueryReport').trigger('click');
        //     }
        // });

    })
</script>