<?php
   class Biz_charge_model extends CI_Model{
       private $table = 'biz_charge';

       public function __construct()
       {
           parent::__construct();
           $this->load->database();
       }

       public function get_one($f="",$v=""){
           $this->db->where($f, $v);
           $query = $this->db->get($this->table);
           return $query->row_array();
       }
       
       public function get_where_one($where=""){
            if($where != '')$this->db->where($where);
            $query = $this->db->get($this->table);
            return $query->row_array();
        }
    
       public function get_option($where=''){
//           $this->db->where($where);
           $query = $this->db->get($this->table);
           return $query->result_array();
       }
       public function get($where="" ,$sort="id",$order="desc"){
           if($where !="")	$this->db->where($where);
           $this->db->order_by($sort,$order);
           $query = $this->db->get($this->table);
           return $query->result_array();
       }

       public function total($where=""){
           if($where !="")	$this->db->where($where);
           $this->db->from($this->table);
           return  $this->db->count_all_results('');
       }

       public function save($data=array()){
           $this->db->insert($this->table, $data);
           return $this->db->insert_id();
       }

       public function update($id='',$data=array()){
           $this->db->where('id', $id);
           $this->db->update($this->table, $data);
           return $id;
       }

       public function mdelete($id=''){
           // delete container
           $this->db->where('id', $id);
           $this->db->delete($this->table);
       }
       
       public function no_role_get($where="" ,$sort="id",$order="desc"){
           if($where !="")	$this->db->where($where);
           $this->db->order_by($sort,$order);
           $query = $this->db->get($this->table);
           return $query->result_array();
       }
   }