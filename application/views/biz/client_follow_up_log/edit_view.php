<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <link href="/inc/css/normal.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/default/easyui.css?v=1">
    <link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/icon.css?v=3">
    <link type="text/css" rel="stylesheet" href="/inc/third/layui/css/layui.css">
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="/inc/js/easyui/jquery.easyui.min.js?v=29"></script>
    <script type="text/javascript" src="/inc/js/easyui/datagrid-export.js"></script>
    <script type="text/javascript" src="/inc/js/easyui/jquery.edatagrid.js?v=5"></script>
    <script type="text/javascript" src="/inc/js/easyui/datagrid-detailview.js"></script>
    <script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="/inc/js/other.js?v=10"></script>
    <script type="text/javascript" src="/inc/js/set_search.js?v=5"></script>
    <script type="text/javascript" src="/inc/js/easyui/jquery.easyui.ext.js?v=9"></script>
    <script type="text/javascript" src="/inc/third/layer/layer.js"></script>
    <script type="text/javascript" src="/inc/third/layui/layui.all.js"></script>
    <style>
        .signature_div {
            min-height: 50px;
        }

        #add_content_div {
            margin: 0 10px;
            border: 1px solid #ccc;
        }

        #toolbar-container {
            border-bottom: 1px solid #ccc;
        }

        #editor-container {
            height: 300px;
        }

        #update_form table tr {
            height: 40px;
        }

        #update_form select {
            width: 250px;
        }
        ._right table tr td {text-align: center;}
        ._right table tr td button {width: 25px; font-weight: bolder;}
        ._right table tr th {text-align: center;}
        .type_0 {margin-top: 10px; margin:20px 0px 0px;}
        .type_0 tr td:first-child{text-align: center;}
    </style>
</head>
<body style="background: #f6f8f5;">
<div title="<?= lang('edit'); ?>" closed="true" style="width:900px;height:499px; border-bottom: 1px solid #588c98; overflow: hidden">
    <form id="update_form" class="layui-form" method="post">
        <input type="hidden" name="id" value="<?php echo $data['id']; ?>">
        <input type="hidden" name="type" value="<?php echo $data['type']; ?>">
        <?php
        if ($data['id'] > 0 && $data['type'] == 1):
            ?>
            <!--报价-->
            <div class="type_1" style="padding: 10px 10px 20px 25px; background: #f6f8f5; height: 100%; width: 350px; height: 450px; float: left; ">
                <!--港口船期信息-->
                <table style="">
                    <tr>
                        <td><?= lang('联系人'); ?></td>
                        <td>
                            <select class="easyui-combobox" id="contact" name="contact" label="State:" required
                                    labelPosition="top"
                                    data-options="value:'<?= isset($data['contact']) ? $data['contact'] : ''; ?>'"
                                    style="height: 30px;">
                                <?php
                                foreach ($contact as $item):
                                    ?>
                                    <option value="<?= $item['name']; ?>"><?= $item['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 80px;"><?= lang('起运港');?></td>
                        <td>
                            <select class="easyui-combobox" name="loading_port" id="loading_port" style="height: 30px;"
                                    data-options="required:true,value:'<?= isset($data['loading_port']) ? $data['loading_port'] : ''; ?>'">
                                <?php
                                foreach ($port as $item):
                                    if ($item['port_name_cn'] == '' || $item['port_code'] == '') continue;
                                    echo "<option value=\"{$item['port_name_cn']}\">{$item['port_name_cn']}</option>";
                                endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('目的港');?></td>
                        <td>
                            <select class="easyui-combobox" name="discharge_port" id="discharge_port"
                                    style="height: 30px;"
                                    data-options="required:true,value:'<?= isset($data['discharge_port']) ? $data['discharge_port'] : ''; ?>'">
                                <?php
                                foreach ($port as $item):
                                    if ($item['port_name_cn'] == '' || $item['port_code'] == '') continue;
                                    echo "<option value=\"{$item['port_name_cn']}\">{$item['port_name_cn']}</option>";
                                endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('船期');?></td>
                        <td>
                            <select class="easyui-combobox" id="sailing_day1" style="height: 30px;" data-options="
                                        required:true,
                                        multiple:true,
                                        multivalue:false,
                                        valueField:'value',
                                        textField:'name',
                                        value:'<?= isset($data['sailing_day']) ? $data['sailing_day'] : ''; ?>',
                                        onChange:function(newValue,oldValue){
                                            $('#sailing_day').val(newValue.join(','));
                                        }
                                    ">
                                <option value="周一"><?= lang('周一');?></option>
                                <option value="周二"><?= lang('周二');?></option>
                                <option value="周三"><?= lang('周三');?></option>
                                <option value="周四"><?= lang('周四');?></option>
                                <option value="周五"><?= lang('周五');?></option>
                                <option value="周六"><?= lang('周六');?></option>
                                <option value="周日"><?= lang('周日');?></option>
                            </select>
                            <input type="hidden" name="sailing_day" id="sailing_day"
                                   value="<?= isset($data['sailing_day']) ? $data['sailing_day'] : ''; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('品名大类');?></td>
                        <td>
                            <select class="easyui-combobox" id="product_type1" style="height: 30px;" data-options="
                                        required:true,
                                        multiple:true,
                                        multivalue:false,
                                        valueField:'value',
                                        textField:'name',
                                        value:'<?= isset($data['product_type']) ? $data['product_type'] : ''; ?>',
                                        url:'/bsc_dict/get_option/product_type',
                                        onChange:function(newValue,oldValue){
                                            $('#product_type').val(newValue.join(','));
                                        }
                                    ">
                            </select>
                            <input type="hidden" name="product_type" id="product_type"
                                   value="<?= isset($data['product_type']) ? $data['product_type'] : ''; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('要求');?></td>
                        <td>
                            <textarea name="remark" placeholder="<?= lang('请输入内容');?>" class="layui-textarea" style="width: 250px; height: 200px; border:1px solid rgba(27,165,214,0.78);"><?= isset($data['remark']) ? $data['remark'] : ''; ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="_right" style="height: 485px; width: 480px; padding: 15px 10px 0px 20px; background: white; float: right; overflow: auto">
                <!--箱型箱量信息-->
                <table class="layui-table" style="margin:0px;">
                    <colgroup>
                        <col width="70">
                        <col width="60">
                        <col width="60">
                        <col width="100">
                        <col width="50">
                        <col>
                    </colgroup>
                    <thead>
                    <tr>
                        <th><?= lang('箱型');?></th>
                        <th><?= lang('箱量');?></th>
                        <th><?= lang('价格(USD)');?></th>
                        <th><?= lang('承运人');?></th>
                        <th><?= lang('操作');?></th>
                    </tr>
                    </thead>
                    <tbody id="box_price">
                    <?php
                    foreach ($data['box_info'] as $index => $item):
                        ?>
                        <tr>
                            <td>
                                <select name="box_type[]" class="easyui-combobox" style="width:80px;"
                                        data-options="
                                    editable:false,
                                    valueField:'value',
                                    textField:'value',
                                    value:'<?=$item['box_type'];?>',
                                    url:'/bsc_dict/get_option/container_size',
                                    ">
                                </select>
                            </td>
                            <td>
                                <input name="box_number[]" class="easyui-numberbox" prompt="0" value="<?=$item['box_number'];?>" precision="0" style="width:50px;">
                            </td>
                            <td>
                                <input name="price[]" class="easyui-numberbox" prompt="$0.00" value="<?=$item['price'];?>" precision="2" style="width:80px;">
                            </td>
                            <td>
                                <select name="carrier[]" class="easyui-combobox" style="width:80px;"
                                        data-options="
                                    required:true,
                                    valueField:'client_name',
                                    textField:'client_name',
                                    value:'<?=$item['carrier'];?>',
                                    url:'/biz_client/get_option/carrier',
                                    onHidePanel: function() {
                                        var valueField = $(this).combobox('options').valueField;
                                        var val = $(this).combobox('getValue');
                                        var allData = $(this).combobox('getData');
                                        var result = true;
                                        for (var i = 0; i < allData.length; i++) {
                                            if (val == allData[i][valueField]) {
                                                result = false;
                                            }
                                        }
                                        if (result) {
                                            $(this).combobox('clear');
                                        }
                                    },
                                ">
                                </select>
                            </td>
                            <td align="center">
                                <?php
                                if ($index==0):
                                    ?>
                                    <button type="button" id="add_tr" class="layui-btn layui-btn-xs">
                                        <i class="layui-icon layui-icon-addition"></i>
                                    </button>
                                <?php else:?>
                                    <button type="button" class="del_tr layui-btn layui-btn-xs">
                                        <i class="layui-icon layui-icon-subtraction"></i>
                                    </button>
                                <?php endif;?>
                            </td>
                        </tr>
                    <?php
                    endforeach;
                    ?>
                    </tbody>
                </table>
            </div>
        <?php else: ?>
            <div id="update_div" style="width: 880px;">
                <table class="type_0">
                    <tr>
                        <td style="width: 100px;"><?= lang('content'); ?></td>
                        <td>
                            <div id="add_content_div" style="margin:0px">
                                <div id="toolbar-container"></div>
                                <div id="editor-container"></div>
                            </div>
                            <textarea name="content" id="add_content_textarea" style="display: none;" data-options="required:true">
                        <?php
                        //如果没有P标签编辑器渲染不出内容
                        if (strpos($data['content'], '<p>') === false) {
                            echo "<p>{$data['content']}</p>";
                        } else {
                            echo $data['content'];
                        }
                        ?>
                    </textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('联系人'); ?></td>
                        <td>
                            <select class="easyui-combobox" data-options="value:'<?= isset($data['contact']) ? $data['contact'] : ''; ?>'" id="contact" name="contact" label="State:" required
                                    labelPosition="top" style="width:255px; height: 30px;">
                                <option value=""><?= lang('请选择联系人');?></option>
                                <?php
                                foreach ($contact as $item):
                                    ?>
                                    <option value="<?= $item['name']; ?>"><?= $item['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        <?php endif; ?>
        <div class="layui-form-item layui-hide" style="display: none;">
            <input type="button" name="tags" lay-submit lay-filter="LAY-module-submit" id="LAY-module-submit" value="确认">
        </div>
    </form>
</div>
<?php
if ($data['type'] !=1):
    ?>
<link type="text/css" href="/inc/css/wangeditor/style.css" rel="stylesheet">
    <script type="text/javascript" src="/inc/js/wangeditor/index.js"></script>
    <script type="text/javascript" src="/inc/js/wangeditor/dist/index.js"></script>
    <script type="text/javascript">
        $(function () {
            const E = window.wangEditor;
            var $add_content_textarea;
            E.Boot.registerModule(window.WangEditorPluginUploadAttachment.default)

            $add_content_textarea = $('#add_content_textarea');
            // 编辑器配置
            const editorConfig = {
                placeholder: '<?= lang('请输入内容');?>',
                onChange(editor) {
                    // 当编辑器选区、内容变化时，即触发
                    const content = editor.children;
                    $add_content_textarea.val(editor.getHtml());
                },
                hoverbarKeys: {
                    attachment: {
                        menuKeys: ['downloadAttachment'],
                    },
                },
                MENU_CONF: {
                    uploadAttachment: {
                        server: '/bsc_upload/file_upload',
                        fieldName: 'file',
                        meta: {
                            is_save_data: 0,
                        },
                        timeout: 5 * 1000, // 5s
                        // metaWithUrl: true, // meta 拼接到 url 上

                        maxFileSize: 10 * 1024 * 1024, // 10M

                        onBeforeUpload(file) {
                            console.log('onBeforeUpload', file)
                            return file // 上传 file 文件
                            // return false // 会阻止上传
                        },
                        onProgress(progress) {
                            console.log('onProgress', progress)
                        },
                        onSuccess(file, res) {
                            console.log('onSuccess', file, res)
                        },
                        onFailed(file, res) {
                            alert(res.message)
                            console.log('onFailed', file, res)
                        },
                        onError(file, err, res) {
                            alert(err.message)
                            console.error('onError', file, err, res)
                        },
                        // 上传成功后，用户自定义插入文件
                        customInsert(res, file, insertFn) {
                            var url = "https://opchina.oss-cn-shanghai.aliyuncs.com" + res['data'][0]['url'] || {};

                            // 插入附件到编辑器
                            //手动插入
                            var file_name = `${file.name}`;
                            var file_a = "<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><a href=\"" + url + "\" target='_blank'>" + file_name + "</a><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>";
                            editor.setHtml(editor.getHtml() + file_a);
                            // editor.setHtml();
                            // insertFn(`${file.name}`, url);
                        },
                    },
                    uploadImage: {
                        fieldName: 'file',
                        server: '/bsc_upload/file_upload',
                        meta: {
                            is_save_data: 0,
                        },
                        customInsert(res, insertFn) {
                            // 图片上传并返回了结果，想要自己把图片插入到编辑器中
                            // 例如服务器端返回的不是 { errno: 0, data: [...] } 这种格式，可使用 customInsert
                            // result 即服务端返回的接口
                            // var url = document.location.protocol + '//' + window.location.host + "/download/read_img?file_path=" + result['data'][0]['url'];
                            var url = "https://opchina.oss-cn-shanghai.aliyuncs.com" + res['data'][0]['url'];

                            // insertImgFn 可把图片插入到编辑器，传入图片 src ，执行函数即可
                            insertFn(url);
                        },
                    }
                },
            };

            // 工具栏配置
            const toolbarConfig = {
                insertKeys: {
                    index: 0,
                    keys: ['uploadAttachment'],
                },
            };

            // 创建编辑器
            const editor = E.createEditor({
                html: $add_content_textarea.val(),
                selector: '#editor-container',
                config: editorConfig,
                mode: 'default' // 或 'simple' 参考下文
            });
            // 创建工具栏
            const toolbar = E.createToolbar({
                editor,
                selector: '#toolbar-container',
                config: toolbarConfig,
                mode: 'default' // 或 'simple' 参考下文
            });
            return;
        });
    </script>
<?php
else:
?>
    <script>
        $(function () {
            var tr_html = '';
            //加载tr碎片代码
            $.ajax({
                url: '/biz_client_follow_up_log/tr_fragment',
                type: 'get',
                dataType: 'text',
                success: function (data) {
                    tr_html = data;
                },
                error: function (xhr, status, error) {
                    layer.close(load);
                    layer.alert('<?= lang('获取tr碎片代码失败！');?>', {icon: 5});
                }
            });

            //添加tr节点
            $('#add_tr').click(function () {
                $('#box_price').append(tr_html);
                $.parser.parse($('#box_price').children(":last-child"));
            });
            //删除tr节点
            $('#box_price').on('click', '.del_tr', function () {
                $(this).parents('tr').remove();
            });
        })
    </script>
<?php endif;?>
</body>
</html>
