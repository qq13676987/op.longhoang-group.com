<?php


class bsc_group extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bsc_group_model');
        $this->model = $this->bsc_group_model;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }


    public $field_edit = array();

    public $field_all = array(
        array('id', 'id', '80', '1','sortable="true"'),
        array('company_code', 'company_code', '100', '3', ''),
        array('group_code', 'group_code', '250', '4', ''),
        array('group_name', 'group_name', '250', '4', ''),
        array('group_type', 'group_type', '80', '4', ''),
        array('status', 'status', '0', '4', ''),
        array('parent_id', 'parent_id', '0', '4', ''),
        array('created_time', 'create_time', '150', '8', 'sortable="true"'),
        array('updated_time', 'update_time', '150', '10', ''),
    );
    
    public function index()
    { 
        echo lang('请到 国内 系统维护');
        return;
        $data = array();

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('bsc_group_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }
        // page account
        $rs = $this->sys_config_model->get_one('bsc_group_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }

        $this->load->view('head');
        $this->load->view('bsc/group/index_view', $data);
    }

    public function get_data()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = array();
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if($s1=='like') $v1 = "%$v1%";
        if($s2=='like') $v2 = "%$v2%";
        if($s3=='like') $v3 = "%$v3%";
        if ($v1 != "") $where[] = "$f1 $s1 '$v1'";
        if ($v2 != "") $where[] = "$f2 $s2 '$v2'";
        if ($v3 != "") $where[] = "$f3 $s3 '$v3'";
        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $this->db->select("bsc_group.*, (select group_name from bsc_group bg where bg.group_code = bsc_group.company_code) as company_name");
        $rs = $this->model->get($where, $sort, $order);
        $result["sql"] = $this->db->last_query();

        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data()
    {
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($temp != "")
                $data[$item] = $temp;
        }
        
        $id = $this->model->save($data);
        
        if($id > 0 ){
            Model('m_model');
            $company_code = $data['company_code'];
            $sql = "select id,group_range from bsc_user where (group_range = '$company_code' or group_range like '$company_code,%' OR group_range like '%,$company_code' or group_range like '%,$company_code,%')";
            $users = $this->m_model->query_array($sql);
            Model('bsc_user_model');
            foreach ($users as $user){
                $this_data = array();
                $group_range = filter_unique_array(explode(',', $user['group_range']));
                $group_range[] = $data['group_code'];
                $this_data['group_range'] = join(',', $group_range);
                $this->bsc_user_model->update($user['id'], $this_data);
            }
        }
       
        $data['id'] = $id;

        echo json_encode($data);
        // record the log
        $log_data = array();
        $log_data["table_name"] = "bsc_group";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function update_data()
    {
        $id = $_REQUEST["id"];
        $old_row = $this->model->get_one('id', $id);

        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($old_row[$item] != $temp)
                $data[$item] = $temp;
        }
        if(isset($data['group_code']))unset($data['group_code']);
        $id = $this->model->update($id, $data);
        $data['id'] = $id;
        echo json_encode($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "bsc_group";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->model->get_one('id', $id);
        if(empty($old_row)) return jsonEcho(array('isError' => true, 'msg' => '数据不存在'));
        //必须没有下级部门
        $child_row = $this->model->get_where_one("parent_id = '{$id}'");
        if(!empty($child_row)) return jsonEcho(array('isError' => true, 'msg' => '请删除下级部门后再试', lastquery()));
        //未关联用户
        $group_user = Model('bsc_user_model')->get_where_one("group = '{$old_row['group_code']}'");
        if(!empty($group_user)) return jsonEcho(array('isError' => true, 'msg' => '该部门存在相关用户,无法删除'));

        //2022-08-25
        $this->db->limit(1);
        $is_use = Model("biz_duty_model")->get_new("user_group like '{$old_row['group_code']}%'");
        if(!empty($is_use)) return jsonEcho(array('isError' => true, 'msg' => '该部门已有订单使用,无法删除'));

        $this->model->mdelete($id);
        echo json_encode(array('success' => true));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "bsc_group";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
    }

    public function get_option($company_code='')
    {
        $type = isset($_GET['type']) ? $_GET['type'] : 0;
        $group_type = isset($_GET['group_type']) ? $_GET['group_type'] : '';
        $this->db->select('group_code, group_name');
        $this->db->order_by('company_code', 'desc');

        if($group_type !== '')$this->db->where("group_type = '$group_type'");
        $rs = $this->model->get_option($company_code);
        $rows = array();
        if($type == 1)$rows[] = array('code' => 'all', 'name' => 'all');
        foreach ($rs as $row){
            $temp = array('code' => $row['group_code'], 'name' => $row['group_name']);
            array_push($rows,$temp);
        }
        echo json_encode($rows);
    }

    /**
     * 获取树
     */
    public function get_tree(){
        $datatype = (int)getValue('datatype', 0);
        $group_type = getValue('group_type', '');

        //只显示没有被禁用的组
        $where = array();
        if($datatype == 0)$where[] = "status = 0";
        if($group_type != '')$where[] = "group_type = '$group_type'";

        $where_str = join(' and ', $where);

        $this->db->select("id,parent_id,orderby,company_code, (select group_name from bsc_group as bg where bg.group_code = bsc_group.company_code) as company_name,group_code,group_name,group_type,status,group_name as text");
        $rs = $this->model->get($where_str);
        $data = tree($rs, 0,'children', 'parent_id');
        return jsonEcho($data);
    }
}