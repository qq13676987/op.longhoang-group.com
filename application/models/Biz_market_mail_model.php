<?php

class biz_market_mail_model extends CI_Model
{
    public $table = 'biz_market_mail';

    public function __construct()
    {
        $this->load->database();
    }

    public function get_by_id($id = 0)
    {
        $this->db->where('id', $id);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_one($f = "", $v = "")
    {
        $this->db->where($f, $v);

        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_where_one($where = '')
    {
        if ($where != '') $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function no_role_get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function total($where = "")
    {
        if ($where != "") $this->db->where($where);

        $this->db->from($this->table);
        return $this->db->count_all_results('');

    }

    public function save($data = array())
    {
        $data["created_time"] = date('y-m-d H:i:s', time());
        $data["updated_time"] = date('y-m-d H:i:s', time());
        $data["created_by"] = $this->session->userdata('id');
        $data["updated_by"] = $this->session->userdata('id');

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function save_auto($data = array())
    {
        $data["created_time"] = date('y-m-d H:i:s', time());
        $data["updated_time"] = date('y-m-d H:i:s', time());
        $data["created_by"] = 0;
        $data["updated_by"] = 0;

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id = '', $data = array())
    {
        $this->db->where('id', $id);
        $data["updated_by"] = $this->session->userdata('id');
        $data["updated_time"] = date('y-m-d H:i:s', time());
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id = '')
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

}