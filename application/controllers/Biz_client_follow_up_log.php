<?php

class biz_client_follow_up_log extends Menu_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_client_follow_up_log_model');
        $this->model = $this->biz_client_follow_up_log_model;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }
    public $field_edit = array();
    public $field_all = array(
        array('id', 'id', '60', '1', 'sortable:true'),
        array('type', '日志类型', '100', '1', 'formatter:type_for'),
        array('crm_id', 'crm_id', '0', '1', 'sortable:true,formatter: function(value,row,index){ return row.client_code_name;}'),
        array('content', 'content', '500', '1', 'sortable:true'),
        array('contact', '联系人', '80', '1', 'sortable:true'),
        array('created_by', 'created_by', '80', '1', 'sortable:true,formatter: function(value,row,index){ return row.created_by_name;}'),
        array('created_time', 'created_time', '150', '1', 'sortable:true'),
        array('updated_by', 'updated_by', '80', '1', 'sortable:true,formatter: function(value,row,index){ return row.updated_by_name;}'),
        array('updated_time', 'updated_time', '150', '1', 'sortable:true'),
    );

    public function index()
    {
        $data = array();
        $crm_id = (int)getValue('crm_id', 0);
        //传参无效
        if ($crm_id == 0) return $this->load->view('errors/html/error_layui', array('msg'=>'参数无效！', 'icon'=>"layui-icon-face-cry"));
        //两类日志的分别占比
        $type_0_rows= $this->model->total(array('crm_id'=>$crm_id, 'type'=>0));
        $type_1_rows= $this->model->total(array('crm_id'=>$crm_id, 'type'=>1));
        $type_2_rows= $this->model->total(array('crm_id'=>$crm_id, 'type'=>2));
        $total = $type_0_rows + $type_1_rows + $type_2_rows;
        if ($total > 0) {
            $data['type_0_proportion'] = floor(strval(($type_0_rows / $total) * 10000)) / 10000 * 100;
            $data['type_1_proportion'] = floor(strval(($type_1_rows / $total) * 10000)) / 10000 * 100;
            $data['type_2_proportion'] = floor(strval(($type_2_rows / $total) * 10000)) / 10000 * 100;
        }else{
            $data['type_0_proportion'] = $data['type_1_proportion'] = $data['type_2_proportion'] = 0;
        }

        $data["f"] = $this->field_all;
        $this->load->vars('crm_id', $crm_id);
        $this->load->view('head');
        $this->load->view('biz/client_follow_up_log/index_view', $data);
    }

    /**
     * @title  编辑
     */
    public function edit(){
        $id = (int)$this->input->get('id');
        if ($id == 0) return $this->load->view('errors/html/error_layui', array('msg'=>'参数无效！', 'icon'=>"layui-icon-face-cry"));
        $row = $this->db->where('id', $id)->get('biz_client_follow_up_log')->row_array();
        if (empty($row)) return $this->load->view('errors/html/error_layui', array('msg'=>'参数无效！', 'icon'=>"layui-icon-face-cry"));
        //联系人
        $contact = $this->db->where('crm_id', $row['crm_id'])->get('biz_client_contact_list')->result_array();
        $port = $this->db->select('port_code, port_name, port_name_cn')->where('LENGTH(port_code) = 5')->distinct()->get('biz_port')->result_array();

        if ($row['type'] == 1){
            $data = json_decode($row['content'], true);
            if(!$data){
                $data['box_info'][] = array('box_type'=>'','box_number'=>'','price'=>'','carrier'=>'');
            }
            $data['id'] = $row['id'];
            $data['contact'] = $row['contact'];
            $data['type'] = $row['type'];
        }else{
            $data = $row;
        }

        $this->load->vars('port', $port);
        $this->load->vars('contact', $contact);
        $this->load->view('biz/client_follow_up_log/edit_view', array('data'=>$data));
    }

    /**
     * @title  添加
     */
    public function add(){
        $crm_id = (int)$this->input->get('crm_id');
        //传参无效
        if ($crm_id == 0) return $this->load->view('errors/html/error_layui', array('msg'=>'参数无效！', 'icon'=>"layui-icon-face-cry"));
        $userRange = get_session('user_range');
        //联系人
        $contact = $this->db->where('crm_id', $crm_id)->where_in('create_by', $userRange)->get('biz_client_contact_list')->result_array();
        $port = $this->db->select('port_code, port_name, port_name_cn')->where('LENGTH(port_code) = 5')->distinct()->get('biz_port')->result_array();

        $data['crm_id'] = $crm_id;
        $this->load->vars('contact', $contact);
        $this->load->vars('port', $port);
        $this->load->view('biz/client_follow_up_log/add_view', array('data'=>$data));
    }

    public function get_data($client_code = "")
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $crm_id = (int)$this->input->get('crm_id');
        //-------where -------------------------------
        $where = "crm_id = {$crm_id}";
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($v1 != "") $where .= " and $f1 $s1 '$v1'";
        if ($v2 != "") $where .= " and $f2 $s2 '$v2'";
        if ($v3 != "") $where .= " and $f3 $s3 '$v3'";
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $this->db->select('*,' .
            '(select name from bsc_user where bsc_user.id = biz_client_follow_up_log.created_by) as created_by_name,' .
            '(select name from bsc_user where bsc_user.id = biz_client_follow_up_log.updated_by) as updated_by_name');
        $rs = $this->model->get($where, $sort, $order);
        $rows = array();
        foreach ($rs as $row) {
            if ($row['type'] == 1){
                $json2arr = json_decode($row['content'], true);
                if (is_array($json2arr)){
                    $_content = array(
                        'loading_port' => $json2arr['loading_port'] ? "起运港：{$json2arr['loading_port']}" : '',
                        'discharge_port' => $json2arr['discharge_port'] ? "目的港：{$json2arr['discharge_port']}" : '',
                        'sailing_day' => $json2arr['sailing_day'] ? "船期：{$json2arr['sailing_day']}" : '',
                        'product_type' => $json2arr['product_type'] ? "品名大类：{$json2arr['product_type']}" : '',
                        'box_info' => "箱型箱量：gp20:"
                    );

                    $row['content'] = join('，', $_content);
                }
            }
            if(!leader_check('sales', $row['created_by'])) continue;

            $row['content'] = truncate(strip_tags($row['content']), 35, '...') . '<font style="color: #ff983d">【双击查看】</font>';
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data()
    {
        $result = array('code' => 0, 'msg' => '参数错误');
        $crm_id = (int)$this->input->get('crm_id');
        $type = isset($_POST['type']) ? (int)$_POST['type'] : 0;
        $field = $this->field_edit;
        $data = $box_type_price = array();
        if($crm_id < 1) return $this->output->set_content_type('application/json')->set_output(json_encode($result, 256));
        //如果是普通日志
        if ($type == 1){
            //报价日志
            $fields = array('loading_port' => '起运港', 'discharge_port' => '目的港', 'sailing_day'=>'船期', 'product_type' => '品名大类', 'contact' => '联系人');
            $json_arr = array();
            //校验必填项
            foreach ($fields as $key => $item) {
                if (!isset($_POST[$key]) || trim($_POST[$key]) == '') {
                    $result['msg'] = "“{$item}”必须填写！";
                    return $this->output->set_content_type('application/json')->set_output(json_encode($result, 256));
                }
                if ($key == 'contact') continue;
                //需要转成json的字段
                $json_arr[$key] = $_POST[$key];
            }
            //重组报价信息
            if (isset($_POST['box_type']) && is_array($_POST['box_type'])) {
                foreach ($_POST['box_type'] as $index => $item){
                    if ( (!isset($_POST['box_number'][$index]) || empty($_POST['box_number'][$index]))
                        || (!isset($_POST['price'][$index]) || empty($_POST['price'][$index]))
                        || (!isset($_POST['carrier'][$index]) || empty($_POST['carrier'][$index]))
                    ) continue;
                    $box_type_price[] = array(
                        'box_type' => $item,
                        'box_number' => $_POST['box_number'][$index],
                        'price' => $_POST['price'][$index],
                        'carrier' => $_POST['carrier'][$index],
                    );
                }
            }
            //校验报价
            if (count($box_type_price) < 1) {
                $result['msg'] = "“箱型报价”必须填写！";
                return $this->output->set_content_type('application/json')->set_output(json_encode($result, 256));
            }
            $json_arr['remark'] = isset($_POST['remark']) ? $_POST['remark'] : '';
            $json_arr['box_info'] = $box_type_price;
            //把报价日志信息放进content字段中
            $_POST['content'] = json_encode($json_arr, 256);
        }else{
            if(!isset($_POST['content']) || empty(strip_tags($_POST['content']))){
                $result['msg'] = '请填写日志内容！';
                return $this->output->set_content_type('application/json')->set_output(json_encode($result, 256));
            }
            if(!isset($_POST['contact1']) || empty($_POST['contact1'])){
                $result['msg'] = '请选择联系人！';
                return $this->output->set_content_type('application/json')->set_output(json_encode($result, 256));
            }
            $_POST['contact'] = $_POST['contact1'];
        }

        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($temp != "")
                $data[$item] = trim($temp);
        }
        $data['type'] = $type;
        $data['crm_id'] = $crm_id;
        $id = $this->model->save($data);
        if($id){
            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_client_follow_up_log";
            $log_data["key"] = $id;
            $log_data["action"] = "insert";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
            
            //给crm刷新一下跟进日期
            if($crm_id){
                Model('biz_client_crm_model');
                $this->biz_client_crm_model->update($crm_id, array('last_follow_time' => date('Y-m-d H:i:s'), 'updated_time' => date('Y-m-d H:i:s')));
            }

            $result['code'] = 1;
            $result['msg'] = 'success';
        }else{
            $result['msg'] = 'Query failed!';
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($result, 256));
    }

    public function update_data()
    {
        $id = postValue('id', 0);
        $field = $this->field_edit;
        $data = $box_type_price = array();
        $result = array('code' => 0, 'msg' => '参数错误');
        $old_row = $this->model->get_one('id', $id);
        if(empty($id) || empty($old_row)) return $this->output->set_content_type('application/json')->set_output(json_encode($result, 256));
        //如果是报价类型的日志
        if ($old_row['type'] == 1) {
            $fields = array('loading_port' => '起运港', 'discharge_port' => '目的港', 'sailing_day'=>'船期', 'product_type' => '品名大类', 'contact' => '联系人');
            $json_arr = array();
            //校验必填项
            foreach ($fields as $key => $item) {
                if (!isset($_POST[$key]) || trim($_POST[$key]) == '') {
                    $result['msg'] = "“{$item}”必须填写！";
                    return $this->output->set_content_type('application/json')->set_output(json_encode($result, 256));
                }
                if ($key == 'contact') continue;
                //需要转成json的字段
                $json_arr[$key] = $_POST[$key];
            }
            //重组报价信息
            if (isset($_POST['box_type']) && is_array($_POST['box_type'])) {
                foreach ($_POST['box_type'] as $index => $item){
                    if ( (!isset($_POST['box_number'][$index]) || empty($_POST['box_number'][$index]))
                        || (!isset($_POST['price'][$index]) || empty($_POST['price'][$index]))
                        || (!isset($_POST['carrier'][$index]) || empty($_POST['carrier'][$index]))
                    ) continue;
                    $box_type_price[] = array(
                        'box_type' => $item,
                        'box_number' => $_POST['box_number'][$index],
                        'price' => $_POST['price'][$index],
                        'carrier' => $_POST['carrier'][$index],
                    );
                }
            }
            //校验报价
            if (count($box_type_price) < 1) {
                $result['msg'] = "“箱型报价”必须填写！";
                return $this->output->set_content_type('application/json')->set_output(json_encode($result, 256));
            }

            $json_arr['remark'] = isset($_POST['remark']) ? $_POST['remark'] : '';
            $json_arr['box_info'] = $box_type_price;
            //把报价日志信息放进content字段中
            $_POST['content'] = json_encode($json_arr, 256);

        }

        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($temp != '' && $temp != $old_row[$item])
                $data[$item] = trim($temp);
        }

        if(!empty($data)){
            $this->model->update($id, $data);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_client_follow_up_log";
            $log_data["key"] = $id;
            $log_data["action"] = "insert";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        }
        $result['code'] = 1;
        $result['msg'] = 'success';
        return $this->output->set_content_type('application/json')->set_output(json_encode($result, 256));
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->model->get_one('id', $id);
        $this->model->mdelete($id);
        echo json_encode(array('success' => true));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_client_follow_up_log";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
    }

    /**
     * @title  tr碎片
     */
    public function tr_fragment(){
        $this->load->view('biz/client_follow_up_log/tr_fragment');
    }
}