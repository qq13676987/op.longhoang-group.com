<?php
   class Biz_shipment_model extends CI_Model{
        public $admin_field = array('sales', 'operator', 'customer_service');//, 'finance');
       //oversea_cus

       public $field_all = array(
           array("shipper_ref", "shipper_ref", "105", "9", '1', 'client info'),
           array("id", "ID", "0", "1", '1'),
           array("client_code", "Client Code", "0", "0", '1', 'client info'),
           array("client_code2", "Second Client", "0", "0", '1', 'client info'),
           array("client_company", "Client Company", "130", "3", '1', 'client info'),
           array("trans_origin", "trans_origin", "0", "9", '1', 'booking owner'),
           array("trans_origin_name", "trans_origin_name", "60", "9", '1', 'booking owner'),
           array("trans_discharge", "trans_discharge", "0", "9", '1', 'booking owner'),
           array("trans_discharge_name", "trans_discharge_name", "56", "9", '1', 'booking owner'),
           array("trans_destination", "trans_destination", "0", "9", '1', 'booking owner'),
           array("trans_destination_inner", "trans_destination_inner", "0", "9", '1', 'booking owner'),
           array("trans_destination_terminal", "trans_destination_terminal", "0", "9", '1', 'booking owner'),
           array("trans_destination_name", "trans_destination_name", "105", "9", '1', 'booking owner'),
           array("job_no", "Job No", "147", "2", '1'),
           array("booking_ETD", "booking_ETD", "103", "9", '1', 'booking owner'),
           array("trans_carrier", "trans_carrier", "149", "9", '1', 'booking owner'),
           array("biz_type", "biz_type", "47", "0", '1'),
           array("status", "status", "50", "0", '1'),
           array("client_address", "Client_Address", "0", "4", '1', 'client info'),
           array("client_contact", "client_contact", "0", "5", '1', 'client info'),
           array("client_telephone", "client_telephone", "0", "6", '1', 'client info'),
           array("shipper_company", "shipper_company", "120", "7", '1', 'booking info'),
           array("shipper_address", "shipper_address", "0", "8", '1', 'booking info'),
           array("shipper_contact", "shipper_contact", "0", "9", '1', 'booking info'),
           array("shipper_telephone", "shipper_telephone", "0", "9", '1', 'booking info'),
           array("shipper_email", "shipper_email", "0", "9", '1', 'booking info'),
           array("consignee_company", "consignee_company", "120", "9", '1', 'booking info'),
           array("consignee_address", "consignee_address", "0", "9", '1', 'booking info'),
           array("consignee_contact", "consignee_contact", "0", "9", '1', 'booking info'),
           array("consignee_telephone", "consignee_telephone", "0", "9", '1', 'booking info'),
           array("consignee_email", "consignee_email", "0", "9", '1', 'booking info'),
           array("notify_company", "notify_company", "0", "9", '1', 'booking info'),
           array("notify_address", "notify_address", "0", "9", '1', 'booking info'),
           array("notify_contact", "notify_contact", "0", "9", '1', 'booking info'),
           array("notify_telephone", "notify_telephone", "0", "9", '1', 'booking info'),
           array("notify_email", "notify_email", "0", "9", '1', 'booking info'),
		   array("client2_contact", "client2_contact", "0", "6", '1', 'client info'),
		   array("client2_telephone", "client2_telephone", "0", "6", '1', 'client info'),
		   array("client2_address", "client2_address", "0", "6", '1', 'client info'),
		   array("client2_email", "client2_email", "0", "6", '1', 'client info'),
           array("trans_mode", "trans_mode", "80", "9", '1'),
           array("trans_tool", "trans_tool", "80", "9", '1'),
           array("description", "description", "0", "9", '1'),
           array("description_cn", "description_cn", "0", "9", '1'),
           array("mark_nums", "mark_nums", "0", "9", '1'),
           array("release_type", "release_type", "60", "9", '3'),
           array("release_type_remark", "release_type_remark", "0", "9", '1'),
           array("sailing_code", "sailing_code", "60", "9", '1', 'booking owner'),//航线代码
           array("sailing_area", "sailing_area", "60", "9", '1', 'booking owner'),//航线区域
           array("good_outers", "good_outers", "60", "9", '1', 'booking cargo'),
           array("good_outers_unit", "good_outers_unit", "60", "9", '1', 'booking cargo'),
           array("good_weight", "good_weight", "60", "9", '1', 'booking cargo'),
           array("good_weight_unit", "good_weight_unit", "60", "9", '1', 'booking cargo'),
           array("good_volume", "good_volumn", "60", "9", '1', 'booking cargo'),
           array("good_volume_unit", "good_volumn_unit", "60", "9", '1', 'booking cargo'),

           array("good_commodity", "good_commodity", "0", "9", '1', 'booking cargo'),
           array("INCO_term", "INCO_term", "0", "9", '1', 'booking cargo'),
           array("hbl_no", "hbl_no", "60", "9", '1'),
           array("hbl_type", "hbl_type", "0", "9", '1'),
           array("consol_id", "consol_id", "0", "9", '1'),
           array("warehouse_no", "warehouse_code", "0", "0", '1'),//仓库编号
           array("warehouse_code", "warehouse_code", "0", "0", '1'),//仓库code
           array("warehouse_contact", "warehouse_contact", "0", "0", '1'),//仓库联系人
           array("warehouse_address", "warehouse_address", "0", "0", '1'),//仓库联系人地址
           array("warehouse_telephone", "warehouse_telephone", "0", "0", '1'),//仓库联系人电话
           array("warehouse_request", "warehouse_request", "0", "0", '1'),//进仓要求
           array("warehouse_charge", "warehouse_charge", "0", "0", '1'),//收费标准
           array("warehouse_in_date", "warehouse_in_date", "0", "0", '1'),//入库时间
           array("warehouse_in_num", "warehouse_in_num", "0", "0", '1'),//入库数量
           array("created_by", "created_by", "150", "10", '1'),
           array("created_time", "created_time", "150", "10", '1'),
           array("updated_by", "updated_by", "150", "10", '1'),
           array("updated_time", "updated_time", "150", "11", '1'),
           array("dadanwancheng", "dadanwancheng", "0", "0", '1', 'step info'),
           array("haiguancangdan", "haiguancangdan", "0", "0", '1', 'step info'),
           array("baoguanjieshu", "baoguanjieshu", "0", "0", '1', 'step info'),
           array("xiangyijingang", "xiangyijingang", "0", "0", '1', 'step info'),
           array("haiguanfangxing", "haiguanfangxing", "0", "0", '1', 'step info'),
           array("matoufangxing", "matoufangxing", "0", "0", '1', 'step info'),
           array("peizaifangxing", "peizaifangxing", "0", "0", '1', 'step info'),
           array("chuanyiqihang", "chuanyiqihang", "0", "0", '1', 'step info'),
           array("tidanqueren", "tidanqueren", "0", "0", '1', 'step info'),
           array("tidanqianfa", "tidanqianfa", "0", "0", '1', 'step info'),
           array("yizuofeiyong", "yizuofeiyong", "0", "0", '1', 'step info'),
           array("box_info", "box_info", "150", "0", '1', 'booking owner'),
           array("AGR_no", "AGR_no", "0", "0", '1', 'booking owner'),
           array("dangergoods_id", "dangergoods_id", "0", "0", '1', 'booking cargo'),
           array("hs_code", "hs_code", "0", "0", '1', 'booking cargo'),
           array("goods_type", "goods_type", "0", "0", '1', 'booking cargo'),
           array("requirements", "requirements", "0", "0", '1', 'booking cargo'),
           array("service_options", "service_options", "0", "0", '1'),
           array("INCO_option", "INCO_term_option", "0", "0", '1'),
           array("INCO_address", "INCO_term_sp_address", "0", "0", '1'),

           array("cus_no", "cus_no", "0", "2", '1'),
           array("client_email", "client_email", "0", "6", '1', 'client info'),
           array("trans_term", "trans_term", "0", "9", '1'),
           array("remark", "remark", "0", "9", '1'),
           array("consol_id_apply", "consol_id_apply", "0", "9", '1'),

           array("issue_date", "issue_date", "60", "9", '1'),
           array("pay_buy_flag", "pay_buy_flag", "0", "9", '1'),
           array("document_remark", "document_remarkr", "0", "9", '1'),
           array("pre_alert", "pre_alert", "0", "9", '1'),
       );

       public function __construct(){
            $this->load->database();
        }
        public function get_by_id($id=0){
            $this->db->where('id', $id);
            $query = $this->db->get('biz_shipment');
            return $query->row_array();
        }
        public function get_one_all($f="",$v=""){
            $this->db->where($f, $v);
            $query = $this->db->get('biz_shipment');
            return $query->row_array();
        }
        public function get_where_one_all($where=""){
            $this->db->where($where);
            $query = $this->db->get('biz_shipment');
            // file_put_contents('099.txt', lastquery());
            return $query->row_array();
        }
        public function get_where_one($where=""){
            $this->db->where($where);
            $query = $this->db->get('biz_shipment');
            // file_put_contents('099.txt', lastquery());
            return $query->row_array();
        }

       /**
        * 获取一条拼接了duty的数据
        */
        public function get_duty_one($where=""){
            $this->db->where($where);
            $query = $this->db->get('biz_shipment');
            $row = $query->row_array();

            if(empty($row)) return $row;

            //查询duty的数据进行整理
            return array_merge($query->row_array(), Model('biz_duty_model')->get_old_by_new('biz_shipment', $row['id']));
        }

       /**
        * 新版查询duty_new表使用的查询
        * @param string $where
        * @param string $sort
        * @param string $order
        * @param bool $unmerge
        * @return mixed
        */
       public function get_v3($where="" ,$sort="biz_shipment.id",$order="desc", $unmerge = true){
           if($where !="") $this->db->where($where, null, false);
           if($unmerge)$this->db->where('biz_shipment.parent_id = 0');
           $this->db->order_by($sort,$order);
           //限制只能看自己的权限
           $this->db->where(Model('bsc_user_role_model')->get_field_where('biz_shipment'));
           $query = $this->db->get('biz_shipment');
//           echo lastquery();
           return $query->result_array();
       }

       public function total_v3($where="", $unmerge = true){
           if($where !="") $this->db->where($where, null, false);
           if($unmerge)$this->db->where('biz_shipment.parent_id = 0');
           //限制只能看自己的权限
           $this->db->where(Model('bsc_user_role_model')->get_field_where('biz_shipment'));
           $this->db->from('biz_shipment');
           return  $this->db->count_all_results('');
       }

       public function no_role_get($where="" ,$sort="id",$order="desc"){
           if($where !="")	$this->db->where($where, null, false);
           $this->db->order_by($sort,$order);
           $query = $this->db->get('biz_shipment');
           return $query->result_array();
       }

       public function no_role_total($where=""){
           if($where !="")	$this->db->where($where);
//            $this->db->from('biz_shipment');
           $this->db->from('biz_shipment');
           return  $this->db->count_all_results('');
       }

        public function no_role_get_one($f="",$v=""){
            $this->db->where($f, $v);   
            $query = $this->db->get('biz_shipment');
            return $query->row_array();
        }

        public function save($data=array()){
            $data["created_time"] = date('y-m-d H:i:s',time());
            $data["updated_time"] = date('y-m-d H:i:s',time());
            $data["created_by"] =  $this->session->userdata('id');
            $data["updated_by"] =  $this->session->userdata('id');
            $data["created_group"] = implode(',', $this->session->userdata('group') );

            $this->db->insert('biz_shipment', $data);
            return $this->db->insert_id();
        } 
        
        public function update($id='',$data=array()){
            $this->db->where('id', $id);
            $data["updated_by"] =  $this->session->userdata('id');
            $data["updated_time"] = date('y-m-d H:i:s',time());
            $this->db->update('biz_shipment', $data);
            return $id;
        }   
        
        public function mdelete($id=''){
            // delete fee
            //$this->load->model('biz_fee_model');
            //$this->biz_fee_model->delete_by_id($id);
            // delete shipment
            $this->db->where('id', $id);
            $this->db->delete('biz_shipment');
        }

       public function get_tm_group($consol_id = 0){
           $sql = "select trans_mode from biz_shipment where consol_id = '$consol_id' GROUP BY trans_mode";
           $query = $this->db->query($sql);
           return $query->result_array();
       }

        public function get_shipment_by_consol($consol_id = 0){
            $this->db->where('consol_id', $consol_id);
            $query = $this->db->get('biz_shipment');
            return $query->result_array();
        }
        
        public function mbatch_update($data, $wfield = 'id')
        {
            $this->db->update_batch('biz_shipment', $data, $wfield);
            return $this->db->affected_rows();
        }
   }
