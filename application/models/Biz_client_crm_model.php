<?php
class biz_client_crm_model extends CI_Model{
    private $table = 'biz_client_crm';

    public $field_all = array(
        array("id", "id", "80", "1"),
        array("client_code", "client_code", "100", "2"),
        array("export_sailing", "export_sailing", "100", "2"),
        array("trans_mode", "trans_mode", "100", "2"),
        array("product_type", "product_type", "100", "2"),
        array("product_details", "product_details", "100", "2"),
        array("client_source", "client_source", "100", "2"),
        array("apply_remark", "apply_remark", "100", "2"),
        array("approve_user", "approve_user", "100", "2"),
        array("role", "role", "100", "2"),
        array("apply_type", "apply_type", "100", "2"),
        array("apply_status", "apply_status", "100", "2"),
        array("sales_id", "sales_id", "100", "2"),
        array("from_user_id", "from_user_id", "0", "2"),
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_one($f="",$v=""){
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_where_one($where = ""){
        if($where != '')$this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }
    
    //SELECT `biz_client_crm`.`id`, `biz_client_crm`.`client_code`, `biz_client`.`client_name`, `biz_client`.`company_name`, `biz_client`.`province`, `biz_client`.`city`, `biz_client`.`company_address`, `biz_client_crm`.`export_sailing`, `biz_client_crm`.`trans_mode`, `biz_client_crm`.`product_type`, `biz_client_crm`.`product_details`, `biz_client_crm`.`client_source`, `biz_client_crm`.`created_by`, (select name from bsc_user where bsc_user.id = biz_client_crm.created_by) as created_by_name, `biz_client_crm`.`created_time`, `biz_client_crm`.`apply_type`, `biz_client_crm`.`apply_status`, `biz_client_crm`.`apply_remark` FROM `biz_client_crm` LEFT JOIN `biz_client_follow_up` ON `biz_client_follow_up`.`client_code` = `biz_client_crm`.`client_code` LEFT JOIN `biz_client` ON `biz_client`.`client_code` = `biz_client_crm`.`client_code` WHERE `biz_client_crm`.`apply_type` = 1 and `biz_client_crm`.`apply_status` = 2 and `biz_client_crm`.`created_group` like 'SHJH-%' ORDER BY FIELD(biz_client_crm.apply_status ASC, '-1' ASC, '1' ASC, '2' ASC, '3' ASC, '4' ASC, '0') ASC LIMIT 30

    public function get($where="" ,$sort="id",$order="desc"){
        if($where !="")	$this->db->where($where, NULL, false);
        $this->db->order_by($sort,$order, false);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function total($where=""){
        if($where !="")	$this->db->where($where, NULL, false);
        $this->db->from($this->table);
        return  $this->db->count_all_results('');
    }
    
    public function no_role_get($where="" ,$sort="id",$order="desc"){
        if($where !="")	$this->db->where($where, null, false);
        $this->db->order_by($sort,$order, true);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function no_role_total($where=""){
        if($where !="")	$this->db->where($where, null, false);
        $this->db->from($this->table);
        return  $this->db->count_all_results('');
    }

    public function save($data=array()){
        $data["created_time"] = date('Y-m-d H:i:s', time());
        $data["created_group"] = join(',', get_session('group'));
        $data["created_by"] =  get_session('id');

        $data["updated_time"] = date('Y-m-d H:i:s', time());
        $data["updated_by"] =  get_session('id');
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function save_by_user($data=array(), $user_id = 0){
        if($user_id == 0) $user_id = get_session('id');
        $data["created_time"] = date('Y-m-d H:i:s', time());
        $data["updated_time"] = date('Y-m-d H:i:s', time());

        $data["created_group"] = getUserField($user_id, 'group');
        $data["created_by"] =  $user_id;
        $data["updated_by"] =  $user_id;
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id='',$data=array()){
        $data["updated_time"] = date('Y-m-d H:i:s', time());
        $data["updated_by"] =  get_session('id');
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id=''){
        // delete container
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function get_field_edit(){
        $field_edit = array();
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_group" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($field_edit, $v);
        }
        return $field_edit;
    }
}