<?php
class Op_model extends CI_Model{
   private $table = '';

   public function __construct()
   {
        parent::__construct();
       
        $this->op_db = $this->load->database('china', true);
   }
   
    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->op_db->where($where, NULL, false);
        $this->op_db->order_by($sort, $order);
        $query = $this->op_db->get($this->table);
        // exit(lastquery());
        return $query->result_array();
    }
   
   public function update($id='',$data=array()){
       $this->op_db->where('id', $id);
       $this->op_db->update($this->table, $data);
       return $id;
   }
   
   public function get_db(){
       return $this->op_db;
   }
   
   public function set_table($table){
       $this->table = $table;
   }
   
}
