<?php

class Sys_desktop extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function widget()
    {
        $data["u"] = lang('User') . ": ";
        $data["g"] = lang('Department') . ": ";
        if ($this->session->userdata('id') != '') {
            $data["u"] .= $this->session->userdata('name');
            $this->load->model("bsc_user_model");
            $user = $this->bsc_user_model->get_by_id($this->session->userdata('id'));
            // $data["m_level"] = $user["m_level"];
            $data["m_level"] = get_session("m_level");
        }
        if (get_session('group') != '') {
            Model('bsc_group_model');
            $userGroup = join(',', get_session('group'));
            $this->db->select('group_name');
            $group = $this->bsc_group_model->get_one('group_code', $userGroup);
            if (!empty($group)) $data["g"] .= $group['group_name'];
        }
//        $this->load->model("sys_update_log_model");
        $data['version'] = 'V.225004';
        $this->load->view('sys/desktop/widget', $data);
    }
    public function kongbai(){
        echo '<meta charset="utf-8"> <a href="/main/login/">超时，请重新登陆</a>';
    }
    public function count()
    {
        $id = get_session('id');
        if(empty($id))  exit(json_encode(array("code"=>9)));
        $userId = get_session('id');
        $time = time();
        $count = array();
        //加入client权限
//        $ebooking_where = array();
//        $ebooking_where[] = 'biz_shipment_ebooking.status = 0';
//
//        $role_where = array();
//        //这里我组有存的,如果到时候又要上级,可以修改
//        $user_range = join(",", get_session("user_range")) ;
//        if (in_array('sales', get_session('user_role'))) $role_where[] = "sales_id = " . get_session('id');
//        if (in_array('operator', get_session('user_role'))) $role_where[] = "operator_id in ($user_range)";
//        if (empty($role_where)) $role_where[] = "1 = 0";
//
//        $ebooking_where[] = "(" . join(' or ', $role_where) . ")";
//        // $ebooking_where[] = $biz_client_model->read_role();
//        $ebooking_where = join(' AND ', array_filter($ebooking_where));
//        $count['Ebooking'] = Model('biz_shipment_ebooking_model')->total($ebooking_where);

//        Model('biz_shipment_si_model');
//        $this->load->model('bsc_user_role_model');
//        $shipment_admin_field = array('operator', 'customer_service', 'finance', 'sales');
//        $rs = $this->bsc_user_role_model->get_field_where_new($shipment_admin_field, 'biz_shipment_si');
//        $role_where = $rs['role_where'];
//        $this->db->where($role_where);
//        $this->db->join('biz_duty_new', "biz_duty_new.id_no = biz_shipment_si.shipment_id and biz_duty_new.biz_table='biz_shipment'");
//        $this->db->join('(select max(id) as id from biz_shipment_si GROUP BY shipment_id) bss', 'bss.id = biz_shipment_si.id', 'RIGHT');
//        $count['Shipment SI'] = $this->biz_shipment_si_model->total("biz_shipment_si.status = 0");

        Model('m_model');
//
//        $sql = "select count(1) as count from (SELECT * FROM (`biz_market_mail`) WHERE recipient_by = $userId and is_delete = 0 and is_read = 0 GROUP BY `created_by`, `plan_send_time`, `subject`, `content`) a";
//        $rs = $this->m_model->query_one($sql);
//        $count['youjianrenwu'] = $rs['count'];
        
//        $sql = "select count(*) as count from bbs_master_at inner join bbs_master on bbs_master.id = bbs_master_at.master_id where bbs_master_at.user_id = '{$userId}' and bbs_master_at.read_time = '0000-00-00 00:00:00' and bbs_master.end_datetime >= '" . date('Y-m-d', time()) . "'";
//        $rs = $this->m_model->query_one($sql);
//        $count['bbs_master'] = (int)$rs['count'];
        //这个方法如果要加其他的值,那么键名为menu表的code, 且把menu表的is_count改为1即可

        //查询更新日期2天内的
        //协议文件的小红点
//        $two_days_ago = date('Y-m-d 00:00:00', strtotime("- 2 day"));
//        // $sql = "select count(*) as count from biz_download_file where updated_time >= '$two_days_ago' and catalog = '协议文件'";
//        $sql = "select count(*) as count from biz_download_file as node,biz_download_file as parent where node.lft between parent.lft AND parent.rgt AND parent.file_name = '协议文件' and node.updated_time >= '$two_days_ago'";
//        $rs = $this->m_model->query_one($sql);
//        $count['xieyiwenjian'] = (int)$rs['count'];
        
        //配舱通知的小红点
//        $now_start = date('Y-m-d 00:00:00', $time);
//        $now_end = date('Y-m-d 23:59:59', $time);
//        $sql = "SELECT count(*) as count FROM `biz_consol` LEFT JOIN `biz_duty_new` ON `biz_duty_new`.`id_no` = `biz_consol`.`id` and `biz_duty_new`.`biz_table` = 'biz_consol' WHERE biz_consol.publish_flag = 1 and biz_consol.status = 'pending' and biz_consol.trans_ATD = '0000-00-00 00:00:00' and pending_datetime >= '{$now_start}' and pending_datetime <= '{$now_end}'";
//        $rs = $this->m_model->query_one($sql);
//        $count['PENDING'] = (int)$rs['count'];

        echo json_encode($count);
    }

    /**
     * 根据当前角色获取初始打开
     */
    public function get_init_open()
    {
        $userRole = Model('bsc_user_role_model')->get_user_role(array('init_open'));

        //根据这个连接，从desktop表里获取
        $urls = filter_unique_array(explode(',', $userRole['init_open']));
        $init_open = array();

        if (!empty($urls)) $desktops = Model('bsc_menu_model')->get("url in ('" . join("','", $urls) . "')");
        else $desktops = array();
        foreach ($desktops as $desktop) {
            $row = array();
            $row['href'] = "/{$desktop['url']}";
            $row['text'] = 'window';
            $row['text'] = lang($desktop['name']);
            $init_open[] = $row;
        }

        echo json_encode($init_open);
    }
}
