<?php
if(!function_exists('setLang')){
    /**
     * 设置当前加载的类名
     * @param string $class 类名
     * @return bool
     */
    function setLang($class = ''){
        $CI =& get_instance();

        if(isset($CI->lang->this_class) && $CI->lang->this_class == $class) return true;

        $lang = isset($_COOKIE['lang']) ? $_COOKIE['lang'] : 'cn';
        //获取语言包
        $language_array = array(
            'en' => 'english',
            'cn' => 'chinese',
        );
        //不存在默认英文版
        if(!isset($language_array[$lang])){
            $lang = 'cn';
        }
        $language = $language_array[$lang];
        $CI->lang->is_loaded = array();
        $CI->lang->load($lang,$language);

        //公共部分默认加载 后续添加自带请直接[]添加数组，越靠后
        $lang_content = array();
        $lang_content[] = $CI->lang->line("public");
        $this_class_lang = $CI->lang->line($class);
        if($this_class_lang)$lang_content[] = $CI->lang->line($class);

        $lang_length = sizeof($lang_content);
        $lang_php = array();
        for ($i = 0; $i < $lang_length; $i++){
            if(!empty($lang_content[$i])){
                $lang_php = array_merge($lang_php, $lang_content[$i]);
            }
        }
        $CI->lang->this_class = $class;
        $CI->lang->setLang($lang_php);
    }
}

if(!function_exists('lang')) {
    /**
     * 获取语言
     * @param $line
     * @param string $class 类名
     * @param array $variable 变量数组 用于替换line里的一些变量参数
     * @return mixed
     */
    function lang($line, $variable = array(), $class = '')
    {
        $CI =& get_instance();
        if ($class != '') {
            setLang($class);
        }
        $lang = $CI->lang->line($line);

        //如果语言包没值,这里直接取传入的line
        $this_lang = $lang == false ? $line : $lang;

        //将变量替换掉
        preg_match_all('/\{(.*?)}/i', $this_lang, $matches);

        if(!empty($matches[0])){
            //将变量塞入规则中进行数据的获取
            foreach ($matches[0] as $key => $match){
                //如果传入的数组里没有该值,就不处理
                $this_match = $matches[1][$key];

                if(!isset($variable[$this_match])) continue;
                $variableData = $variable[$this_match];
                $this_lang = str_replace($match, $variableData, $this_lang);//将变量替换为值
            }
        }
        return $this_lang;
    }
}

if(!function_exists('get_session')) {
    /**
     * 获取CI的session
     * @param string $session_name session 名称
     * @return mixed
     */
    function get_session($session_name = "")
    {
        $CI =& get_instance();
        return $CI->session->userdata($session_name);
    }
}

if(!function_exists('Model')) {
    /**
     * 实例化model
     * @param $model_name
     * @return mixed
     */
    function Model($model_name)
    {
        $CI =& get_instance();
        $CI->load->model($model_name);
        return $CI->$model_name;
    }
}

if(!function_exists('is_ajax')) {
    /**
     * 判断是否是ajax请求
     * @return bool
     */
    function is_ajax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';//判断是否是ajax请求
    }
}

if(!function_exists('pricate_encrypt')) {
    /**
     * 非对称解密 私钥加密
     * @param $data
     * @return string|null
     */
    function pricate_encrypt($data)
    {
        if (!is_string($data)) {
            return null;
        }
        $abs_path = dirname(BASEPATH) . '/inc/pem/rsa_private_key.pem';
        $content = file_get_contents($abs_path);
        $privateKey = openssl_pkey_get_private($content);

        return openssl_private_encrypt($data, $encrypted, $privateKey) ? base64_encode($encrypted) : null;
    }
}

if(!function_exists('public_decrypt')) {
    /**
     * 非对称加密 公钥解密
     * @param $data
     * @return null
     */
    function public_decrypt($data)
    {
        if (!is_string($data)) {
            return null;
        }
        $abs_path = dirname(BASEPATH) . '/inc/pem/rsa_public_key.pub';
        $content = file_get_contents($abs_path);
        $publicKey = openssl_pkey_get_public($content);

        return openssl_public_decrypt(base64_decode($data), $decrypted, $publicKey) ? $decrypted : null;
    }
}

if(!function_exists('log_url_rcd')) {
    /**
     * 记录访问日志
     * @param $exec_time 记录请求执行时间 主要是后端的
     */
    function log_url_rcd($exec_time)
    {
        $CI =& get_instance();  //相当于$this->
        $class = $CI->router->fetch_class();
        $method = $CI->router->fetch_method();

        $log_url = array();
        //查询IP,如果无法获取,传0进去
        $ip = GetIP();
        $ip_long = $ip !== false ? ip2long($ip) : 0;
        $log_url['ip'] = $ip_long;
        $log_url['url'] = $_SERVER['REQUEST_URI'];
        $log_url['url2'] = $class . '/' . $method;
        $log_url['post_data'] = json_encode($_POST, JSON_UNESCAPED_UNICODE);
        if (get_session('id')) $log_url['user_id'] = get_session('id');
        $log_url['exec_time'] = $exec_time;
        $CI->load->model('sys_log_url_model');

        $CI->sys_log_url_model->save($log_url);
    }
}

if(!function_exists('GetIP')) {
    /**
     * 获取当前IP
     * @return bool|mixed
     */
    function GetIP()
    {
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $cip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } elseif (!empty($_SERVER["REMOTE_ADDR"])) {
            $cip = $_SERVER["REMOTE_ADDR"];
        } else {
            $cip = false;
        }
        return $cip;
    }
}

if(!function_exists('get_user_pan')) {
    /**
     * 获取目录,该方法支持递归获取子目录
     * @param int $user_id
     * @param array $data
     * @return array
     */
    function get_user_pan($user_id = 0, &$data = array()){
        $CI =& get_instance();
        //递归获取

        //获取初级目录
        $this_pan_id = getUserField($user_id, 'pan_id');
        $CI->db->select('id,name,pan_id');
        // $rs = Model('bsc_user_model')->get_by_id($user_id);
        //拼接当前的子账号,和自己,还有当前账号的父账号
        $data = Model('bsc_user_model')->get("pan_id = $user_id or id = $user_id or id = $this_pan_id");
        // $data[] = $rs;

        // get_user_pan($rs['pan_id'], $data);

        return $data;
    }
}

if(!function_exists('jsonEcho')) {
    /**
     * json输出代码
     * @param $data
     * @return bool
     */
    function jsonEcho($data)
    {
        echo json_encode($data);
        return true;
    }
}

if(!function_exists('jsonEcho')) {
    /**
     * json输出代码
     * @param $data
     * @return bool
     */
    function jsonEcho($data)
    {
        echo json_encode($data);
        return true;
    }
}

if(!function_exists('jsonEcho')) {
    /**
     * json输出代码
     * @param $data
     * @return bool
     */
    function resultEcho($data)
    {
        echo json_encode($data);
        return true;
    }
}

if(!function_exists('success')) {
    /**
     * json输出代码
     * @param $data
     * @return bool
     */
    function success($code, $mes, $data)
    {
        $return_data = array_merge(array('code' => $code, 'msg' => $mes), $data);
        return jsonEcho($return_data);
    }
}

if(!function_exists('error')) {
    /**
     * json输出代码
     * @param $data
     * @return bool
     */
    function error($code, $mes, $data)
    {
        $return_data = array_merge(array('code' => $code, 'msg' => $mes), $data);
        return jsonEcho($return_data);
    }
}
if(!function_exists('log_rcd')) {
    /**
     * 记录日志
     * @param $log_data
     * @param $type
     */
    function log_rcd($log_data, $type = 1)
    {

        $log_data["user_id"] = get_session('id');
        $computer_code = isset($_COOKIE['computer_code']) ? $_COOKIE['computer_code'] : '';
        $computer_code = json_decode(public_decrypt($computer_code), true);
        $log_data["system_pass_id"] = $computer_code['id'];
        $id =  Model('sys_log_model')->save($log_data);
        if (isset($log_data['key']) && $log_data['key'] === 0 && $type == 1) {
//            add_mail('mail', $log_data['action'] . '出错', "表名:{$log_data['table_name']},操作人:" . get_session('name') . ',日志ID为:' . $id);
        }
    }
}
if(!function_exists('is_admin')) {
    /**
     * @param int $type
     * @return bool
     */
    function is_admin($type = 0)
    {
        if ($type == 1) {
            if (get_session('name') == '金晶') {
                return false;
            }
        }

        if (get_session('level') == '9999') {
            return true;
        }

        return false;
    }
}
if(!function_exists('getValue')) {
    /**
     * 获取get参数, 如果不存在返回默认值
     * @param $name
     * @param string $spare
     * @return mixed|string
     */
    function getValue($name, $spare = '')
    {
        return issetGet($_GET, $name, $spare);
    }
}
if(!function_exists('postValue')) {
    /**
     * 获取post参数, 如果不存在返回默认值
     * @param $name
     * @param string $spare
     * @return mixed|string
     */
    function postValue($name, $spare = '')
    {
        return issetGet(check_param($_POST), $name, $spare);
    }
}
if(!function_exists('issetGet')) {
    /**
     * 获取数组中的指定值, 如果不存在返回默认值
     * @param $data
     * @param $name
     * @param string $spare
     * @return mixed|string
     */
    function issetGet($data, $name, $spare = '')
    {
        return isset($data[$name]) ? $data[$name] : $spare;
    }
}

if(!function_exists('filter_unique_array')) {
    /**
     * 数组去空且去重
     * @param $array
     * @return array
     */
    function filter_unique_array($array)
    {
        if (!is_array($array)) $array = array($array);
        return array_filter(array_unique($array));
    }
}
if(!function_exists('check_param')) {
    /**
     * 将值进行处理
     * @param null $value
     * @return array|string|null
     */
    function check_param($value = null)
    {
        if(is_array($value)){
            foreach ($value as $key => $val){
                $value[$key] = check_param($val);
            }
            return $value;
        }
        return htmlspecialchars(addslashes($value));
    }
}
if(!function_exists('lastquery')) {
    /**
     * 获取最后执行的sql语句
     */
    function lastquery(){
        $CI =& get_instance();
        return $CI->db->last_query();
    }
}