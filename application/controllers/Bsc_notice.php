<?php


class bsc_notice extends Menu_Controller
{
    protected $menu_name = 'notice';//菜单权限对应的名称
    protected $method_check = array(//需要设置权限的方法
        'add_data',
        'update_data',
        'delete_data',
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('bsc_notice_model');
        $this->model = $this->bsc_notice_model;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }

    //参数1 名称 参数2 颜色（待定）
    private $type = array(
        0 => array('普通', ''),
    );

    private $isread_color = array(
        0 => '',
        1 => 'background-color:#c8c8c8;color:#826e6e;',
    );

    private $status = array(
        0 => array('禁用'),
        1 => array('正在使用')
    );

    public $field_edit = array();

    public $field_all = array(
        array('id', 'id', '80', '1'),
        array('table_name', 'table_name', '100', '3'),
        array('id_no', 'id_no', '80', '4'),
        array('type', 'type', '100', '5'),
        array('start', 'start', '100', '6'),
        array('end', 'end', '100', '6'),
        array('rate', 'rate', '80', '8'),
        array('txt', 'txt', '150', '8'),
        array('href', 'href', '150', '8'),
        array('notice_user', 'notice_user', '150', '8'),
        array('notice_user_group', 'notice_user_group', '150', '8'),
        array('status', 'status', '80', '8'),
        array('delay_num', 'delay_num', '80', '8'),
        array('create_time', 'create_time', '150', '8'),
//        array('create_by','create_by','150','9'),
        array('update_time', 'update_time', '150', '10'),
//        array('update_by','update_by','150','11'),
    );

    //管理页面--start
    public function index()
    {
        if(!is_admin()){
            echo '不是管理员';
            return;
        }
        $data = array();
        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_auto_rate_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }
        // page account
        $rs = $this->sys_config_model->get_one('biz_auto_rate_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }
        $this->load->view('head');
        $this->load->view('bsc/notice/index_view', $data);
    }

    public function get_data()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = array();
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($v1 != "") $where[] = "$f1 $s1 '$v1'";
        if ($v2 != "") $where[] = "$f2 $s2 '$v2'";
        if ($v3 != "") $where[] = "$f3 $s3 '$v3'";
        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $rs = $this->model->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            $user = array_filter(explode(',',$row['notice_user']));
            $row['notice_user_name'] = '';
            if(!empty($user)) $row['notice_user_name'] = $this->db->select("group_concat(name) as notice_user_name")->where_in('id',$user)->get('bsc_user')->row_array()['notice_user_name'];
            $group = array_filter(explode(',',$row['notice_user_group']));
            $row['notice_user_group_name'] = '';
            if(!empty($user)) $row['notice_user_group_name'] = $this->db->select("group_concat(group_name) as notice_user_group_name")->where_in('group_code',$group)->get('bsc_group')->row_array()['notice_user_group_name'];
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }
    //管理页面--end

    //用户查看所有通知页面--start
    public function user_notice()
    {
        $data = array();

        $data['read_color'] = $this->isread_color;
        $this->load->view('head');
        $this->load->view('bsc/notice/user_notice', $data);
    }

    public function get_user_data()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = array();
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';

        $where[] = "notice_user is null";
        $where[] = "notice_user_group is null or notice_user like '%," . $this->session->userdata('id') . "%' or notice_user_group like '%," . implode(',', $this->session->userdata('group')) . "%'";
        if ($v1 != "") $where[] = "$f1 $s1 '$v1'";
        if ($v2 != "") $where[] = "$f2 $s2 '$v2'";
        if ($v3 != "") $where[] = "$f3 $s3 '$v3'";
        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $rs = $this->model->get_join_read($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {

            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }
    //用户查看通知页面--end

    //用户首页弹窗（查看可通知）--satrt
    public function main_user_notice()
    {
        $data = array();

        $data['read_color'] = $this->isread_color;
        $this->load->view('head');
        $this->load->view('bsc/notice/main_user_notice', $data);
    }

    /**
     * 获取用户的普通消息
     */
    public function get_message(){
        $this->load->view('bsc/notice/get_message');
    }

    public function get_user_noitce_data()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 9999;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $isread = isset($_POST['isread']) ? (int)$_POST['isread'] : 0;
        $type = isset($_POST['type']) ? (int)$_POST['type'] : 1;

        //-------where -------------------------------
        $where = array();
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        $u_id = $this->session->userdata('id');
        $u_group = implode(',', $this->session->userdata('group'));
        $u_group_arr = explode('-', $u_group);
        $company_code = $u_group_arr[0];
        
        
        $where[] = "notice_user = {$u_id} ";
        $where[] = "lock_screen = {$type}";
        $where[] = "start <= '" . date('Y-m-d H:i:s') . "'";
        if ($v1 != "") $where[] = "$f1 $s1 '$v1'";
        if ($v2 != "") $where[] = "$f2 $s2 '$v2'";
        if ($v3 != "") $where[] = "$f3 $s3 '$v3'";
        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->db->where($where)->count_all_results('bsc_notice');

        if($isread == 0){
            $where .= ' and status = 0';
        }else{
            $where .= ' and status = 1';
        }
        $rs = $this->db->select('*, (select name from bsc_user where bsc_user.id = bsc_notice.notice_user) as notice_user_name')->where($where)->order_by('lock_screen_datetime', 'asc')->get('bsc_notice')->result_array();
        $rows = array();
        $result["delay_status"] = true;
        foreach ($rs as $row) {
            $row['lock_screen_status'] = 1;
            if($row['lock_screen_datetime'] <= date('Y-m-d H:i:s',time())){
                $row['lock_screen_status'] = 2;
            }
            $row['delay_num_count'] = $row['delay_num'];
            if($row['delay_num_count'] == 0){
                $result["delay_status"] = false;
            }
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    //用户首页弹窗（查看可通知）--end
    
    public function add(){
        $data = array();
        
        // $this->load->view('head');
        $this->load->view('/bsc/notice/add_view', $data);
    }

    public function add_data()
    {
        $field = $this->field_edit;
        $data = array();
      
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($temp != "")
                $data[$item] = $temp;
        }
        
        $id = $this->model->save($data);
        if($id == 0){
            echo json_encode(array('code' => 1, 'msg' => 'error', 'data' => array()));
            return;
        }
        
        $data['id'] = $id;

        // record the log
        $log_data = array();
        $log_data["table_name"] = "bsc_notice";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        echo json_encode(array('code' => 0, 'msg' => 'success', 'data' => $data));
        
    }

    public function update_data()
    {
        $id = $_REQUEST["id"];
        $old_row = $this->model->get_one('id', $id);

        $field = array('id', 'start', 'end', 'txt','notice_user','notice_user_group','status','delay_num');
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($old_row[$item] != $temp)
                $data[$item] = $temp;
        }
        $id = $this->model->update($id, $data);
        $data['id'] = $id;
//        echo json_encode($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "bsc_notice";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        return jsonEcho(['code'=>1,'msg'=>'编辑成功']);
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->model->get_one('id', $id);
        $this->model->mdelete($id);
        echo json_encode(array('success' => true));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "bsc_notice";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
    }

    public function notice_read()
    {
        if ($this->session->userdata('id') == '') {
            redirect('main/login', 'refresh');
        }
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';

        $userid = $this->session->userdata('id');
        $notice = $this->db->where(array('id'=>$id, 'notice_user'=>$userid))->get('bsc_notice')->row_array();

        if (!empty($notice)){
            $data = array('status' => 1);
            $this->db->where('id', $id)->update('bsc_notice', $data);
        }

        echo json_encode(array('code' => 0, 'msg' => 'success'));
    }

    /**
     * 获取该用户当前通知数
     */
    public function now_notice_count()
    {
        $total = 0;
        $userId = get_session('id');
        $date = date('Y-m-d H:i:s');
        //锁屏消息
        $notice_lock_screen = Model('bsc_notice_model')->get("notice_user = '{$userId}' and status = 0 and lock_screen = 1 and lock_screen_datetime <= '{$date}'");
        //非锁屏(普通)消息
        $notice_unlock_screen = Model('bsc_notice_model')->get("notice_user = '{$userId}' and status = 0 and lock_screen = 0 and start < '{$date}'");
        //只有普通消息的时候弹出消息框
        //if(empty($notice_lock_screen) && !empty($notice_unlock_screen)){
            $total = count($notice_unlock_screen);
        //}
        echo json_encode(array('total' => $total));
    }

    /**
     * 查看当前锁屏的通知
     */
    public function lock_screen(){
        $data = array();
        $data['back_url'] = getValue('back_url', '');
        $this->load->view('head');
        $this->load->view('/bsc/notice/lock_screen_view', $data);
    }

    //一键增加4小时
    public function one_key(){
        $ids = isset($_POST['ids'])?trim($_POST['ids'],','):'';
        if(!$ids){
            return jsonEcho(['code'=>0,'msg'=>'参数错误']);
        }
        $time = date('Y-m-d H:i:s',time() + 14400);
        $add_data = ['lock_screen_datetime'=>$time];
        $data = $this->db->where("id in ({$ids})")->get('bsc_notice')->result_array();
        $remaining_num = $data[0]['delay_num'];
        foreach ($data as $v){
            if($v['lock_screen_datetime'] > date('Y-m-d H:i:s',time())) continue;
            $add_data['delay_num'] = ($v['delay_num'] - 1 <= 0 ? 0:($v['delay_num'] - 1)) ;
            if($add_data['delay_num'] < $remaining_num) $remaining_num = $add_data['delay_num'];
            $this->db->where('id',$v['id'])->update('bsc_notice',$add_data);
            $log_data["table_name"] = "bsc_notice";
            $log_data["key"] = $v['id'];
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($add_data);
            log_rcd($log_data);
        }
        return jsonEcho(['code'=>1,'msg'=>"您剩余的可用延迟次数为 {$remaining_num}，次数用完后需要邮件和boss说明情况再解锁。"]);
    }

    /**
     * 表格获取数据
     */
    public function get_lock_screen()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';


        //-------这个查询条件想修改成通用的 -------------------------------
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $user_id = get_session('id');
        $date = date('Y-m-d H:i:s');
        $where = array();

        //只能看自己的,且实际
        $where[] = "notice_user = '{$user_id}'";//通知人是自己
        $where[] = "status = 0";//未结束的
        $where[] = "lock_screen = 1";//锁屏通知
        $where[] = "lock_screen_datetime <= '{$date}'";//锁屏时间小于当前

        $field_edit = $this->field_edit;
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = trim($fields['s'][$key]);
                $v = trim($fields['v'][$key]);
                if(!in_array($f, $field_edit)) continue;
                if($v != '')$where[] = search_like('bsc_notice.' . $f, $s, $v);
            }
        }
        $where = join(' and ', $where);
        //-----------------------------------------------------------------


        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $this->db->select('id,href,notice_user,table_name,id_no,txt, create_time,lock_screen_datetime ,(select name from bsc_user where bsc_user.id = bsc_notice.notice_user) as notice_user_name');
        $rs = $this->model->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        return jsonEcho($result);
    }

    /**
     * 延迟锁屏时间
     */
    public function delay_notice(){
        $id = postValue('id', 0);
        $second = (int)postValue('second', 0);

        $row = Model('bsc_notice_model')->get_where_one("id = '{$id}'");
        if(empty($row)) return jsonEcho(array('code' => 1, 'msg' => '数据不存在'));

        if(empty($second)) return jsonEcho(array('code' => 0, 'msg' => '延迟成功'));

        $time = time();
        $last_date = date('Y-m-d H:i:s', $time + $second);

        $data = array(
            'lock_screen_datetime' => $last_date,
            'delay_num' => ($row['delay_num'] - 1 <= 0?0:($row['delay_num'] - 1)),
        );
        Model('bsc_notice_model')->update($id, $data);

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "bsc_notice";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        return jsonEcho(array('code' => 0, 'msg' => '延迟成功'));
    }

    /**
     * 主动关闭通知 会有根据表格限制的一系列逻辑
     */
    public function close_notice(){
        $id = postValue('id', 0);
        $old_row = Model('bsc_notice_model')->get_where_one("id = {$id}");
        if(empty($old_row)) return jsonEcho(array('code' => 1, 'msg' => '通知不存在'));

        //关闭了就直接结束
        if($old_row['status'] === '1') return jsonEcho(array('code' => 0, 'msg' => '关闭成功'));

        //不能关闭别人的消息
        $login_user_id = get_session('id');
        if($login_user_id != $old_row['notice_user']) return jsonEcho(array('code' => 0, 'msg' => ' 无权关闭别人的消息！'));

        $is_closed = false;//为true时 自动关闭
        $close_conditions = array(); // 存储关闭的相关条件
        // 相关的限制逻辑--start
        //consol相关
        if($old_row['table_name'] == 'biz_consol'){
            $consol = Model('biz_consol_model')->get_where_one("id = {$old_row['id_no']}");
            if($old_row['code'] == 'dcwc_gx'){
                $close_conditions = array(
                    "consol 已退关",
                    "consol 订舱接受已勾选",
                    "consol 订舱申请取消"
                );

                //不存在直接关闭
                if(empty($consol)) $is_closed = true;
                else{
                    //3、 consol 退关时， 可主动关闭任务
                    if($consol['status'] != 'normal') $is_closed = true;

                    //4、 consol 订舱接受已勾选时，可主动关闭任务(现在是自动) 这个只限制 dcwc_gx
                    if($consol['dingcangwancheng'] !== '0' && $consol['dingcangwancheng'] !== '0000-00-00 00:00:00') $is_closed = true;
                    if($consol['dingcangshenqing'] == '0') $is_closed = true;
                }
            }
            if($old_row['code'] == 'siwc_gx'){
                $close_conditions = array(
                    "consol 已退关",
                    "consol SI完成已勾选"
                );

                //不存在直接关闭
                if(empty($consol)) $is_closed = true;
                else{
                    //3、 consol 退关时， 可主动关闭任务
                    if($consol['status'] != 'normal') $is_closed = true;

                    //4、 consol Si勾选时,可主动关闭
                    if($consol['document_si'] !== '0' && $consol['document_si'] !== '0000-00-00 00:00:00') $is_closed = true;
                }
            }
            if($old_row['code'] == 'des_ETA'){
                $close_conditions = array(
                    "consol 非normal",
                    "consol 实际离泊时间未填写",
                    "consol 实际到目的港时间已填写"
                );
                //不存在直接关闭
                if(empty($consol)) $is_closed = true;
                else{
                    //3、 consol 退关时， 可主动关闭任务
                    if($consol['status'] != 'normal') $is_closed = true;
                    if($consol['trans_ATD'] == '0000-00-00 00:00:00') $is_closed = true;
                    if($consol['des_ATA'] != '0000-00-00 00:00:00') $is_closed = true;
                }
            }
            if($old_row['code'] == 'pre_alert_overdue'){
                $pre_alert = $this->db->where("consol_id={$old_row['id_no']} and pre_alert <> 0")->count_all_results('biz_shipment');
                $close_conditions = array(
                    "consol 非normal",
                    "海外预报已勾选"
                );
                //不存在直接关闭
                if(empty($consol)) $is_closed = true;
                else{
                    //3、 consol 退关时， 可主动关闭任务
                    if($consol['status'] != 'normal') $is_closed = true;
                    if ($pre_alert > 0) $is_closed = true;
                }
            }
            if(!$is_closed){
                $duty_new = $this->db->where("id_type = 'biz_consol' and id_no = {$old_row['id_no']}")->get('biz_duty_new')->result_array();
                if(!in_array($old_row['notice_user'],array_column($duty_new,'user_id'))){
                    $is_closed = true;
                }
            }
        }

		if($old_row['table_name'] == 'biz_shipment'){
			$shipment = $this->db->where("id = {$old_row['id_no']}")->get('biz_shipment')->row_array();
			if($old_row['code'] == 's1_lock_not_finish'){
				$close_conditions = array(
					"lock s1",
				);
				if($shipment['lock_lv'] >= 1) $is_closed = true;
			}
			if($old_row['code'] == 's2_lock_not_finish'){
				$close_conditions = array(
					"lock s2",
				);
				if($shipment['lock_lv'] >= 2) $is_closed = true;
			}
		}
        //ebooking相关
        if($old_row['table_name'] == 'biz_shipment_ebooking'){
            $ebooking = Model('biz_shipment_ebooking_model')->get_where_one("id = {$old_row['id_no']}");
            $close_conditions = array(
                "ebooking已拒绝",
                "consol ebooking转为shipment后(ebooking已通过)"
            );

            //不存在直接关闭
            if(empty($ebooking)) $is_closed = true;
            else{
                //1、 ebooking 拒绝时, 可以主动关闭
                if($ebooking['status'] === '-1') $is_closed = true;

                //2、ebooking转为shipment时, 可关闭(现在是自动) ebooking_wait_op
                if($old_row['code'] == 'ebooking_wait_op') {
                    if ($ebooking['status'] === '1') $is_closed = true;
                }
            }
        }

        //如果是线索消息或关联关系的消息
        if (in_array($old_row['code'], ['client_clue', 'crm_clue', 'relation']) === true){
            $is_closed = true;
        }
        if($old_row['code'] == 'user_data'){
            $user_id = get_session('id');
            $user = $this->db->where('id',$user_id)->get('bsc_user')->row_array();
            $photo = $this->db->where(['biz_table'=>'bsc_user','type_name'=>'入职登记表照片','id_no'=>$user_id])->get('bsc_upload')->row_array();
            if (!empty($user['country']) && !empty($user['name_en']) && !empty($user['email']) && !empty($user['telephone']) && !empty($photo)) {
                $is_closed = true;
            }else{
                $close_conditions[] = '补充个人资料中：国籍，中文名称，英文名称，邮箱，手机号，头像';
            }
        }
        // 相关的限制逻辑--end

        //将通知数组转为字符串
        $close_conditions_str = !empty($close_conditions) ? '<br/>' : '';
        foreach ($close_conditions as $key => $close_condition){
            $this_key = $key + 1;
            $close_conditions_str .= "{$this_key}. {$close_condition}</br>";
        }

        //如果不允许关闭这里进行提示
        if(!$is_closed) return jsonEcho(array('code' => 1, 'msg' => '满足以下任意可关闭条件时, 即可关闭通知.' . $close_conditions_str));

        //执行关闭
        $update_data = array('status' => 1);
        Model('bsc_notice_model')->update($id, $update_data);

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "bsc_notice";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($update_data);
        log_rcd($log_data);

        jsonEcho(array('code' => 0, 'msg' => '关闭成功'));
    }
}