<!--add_consignee-->
<div class="easyui-panel" title="<?= lang('增加收件人');?>">
    <div style="padding:10px 60px 20px 60px">
        <form id="ff" method="post">

            <input type="hidden" name="group_id" value="<?php echo $group_id ?>"/>
            <table cellpadding="5">
                <tr>
                    <td><?= lang('收件人组名');?>:</td>
                    <td>
                        <input class="easyui-textbox" type="text" name="group_name" data-options="required:true,"  value="<?php echo $group_name ?>" style="width: 555px;"/>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('批量收件人');?>
                        <br> <br>
                        <?= lang('【按照规则1行1个】');?>:<br>
                        <?= lang('【每行用"分号"分隔】');?>: </td>
                    <td>
                        <textarea style="width: 555px;height: 380px;" name="emails" id="emails"></textarea>
                        <br>
                        <?= lang('示例');?>: <br>
                        <?= lang('姓名;111@qq.com;13355663311');?><br>
                        <?= lang('姓名;111@qq.com;13355663311');?><br>
                        <?= lang('姓名;111@qq.com;13355663311');?><br>
                        <?= lang('姓名;111@qq.com;13355663311');?><br>
                    </td>
                </tr>
                <!--<tr>
                    <td>性别:</td>
                    <td>
                        <input type="radio" name="gender" checked="checked" value="男">男
                        <input type="radio" name="gender" value="女">女
                    </td>
                </tr> -->
                <tr>
                    <td></td>
                    <td>
                        <a href="javascript:void(0)" class="easyui-linkbutton" style="padding: 0px 16px 0px 16px;" onclick="subForm()"><?= lang('确定');?></a>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script type="text/javascript" src="/inc/layer3.1.1/layer.js"></script>
<script>

    function subForm() {
        ajaxLoading();
        var d = {};
        var formdata = $("#ff").serializeArray();

        $.each(formdata, function () {
            d[this.name] = this.value;
        });
        $.ajax({
            type: 'POST',
            url:'/crm_promote_mail/add_consignees_data',
            data:formdata,
            dataType:'json',
            success:function(data){
                ajaxLoadEnd();
                if (data.code == 200) {
                    layer.msg(data.msg);
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 2000);
                } else {
                    layer.msg(data.msg);
                }
            },
            error:function (e) {
                ajaxLoadEnd();
                layer.msg(e.responseText);
            }
        });
        // $.post("/crm_promote_mail/add_consignees_data", formdata, function (data) {
        //     if (data.code == 200) {
        //         ajaxLoadEnd();
        //         layer.msg(data.msg);
        //         setTimeout(function () {
        //             window.location.href = data.url;
        //         }, 2000);

        //     } else {
        //         ajaxLoadEnd();
        //         layer.msg(data.msg);

        //     }

        // }, "JSON");
    }
</script>
