<?php

/**
 * 航讯 报文相关
 */
class Bsc_message_hangxun extends Common_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    private function date_format($date){
        return strtoupper(date('d-M-Y', strtotime($date)));
    }

    private function text_format($text){
        $text = str_replace("\r\n", " ", $text);
        return $text;
    }

    /**
     * 迪拜的manifest
     */
    public function ae_manifest($id_no)
    {
        $consol = Model('biz_consol_model')->get_where_one("id = '{$id_no}'");
        if(empty($consol)) return array('code' => 1, 'msg' => lang('consol不存在'));
        $shipments = Model('biz_shipment_model')->get_shipment_by_consol($id_no);
        if(empty($shipments)) return array('code' => 1, 'msg' => lang('未绑定shipment'));

        $rotation_no = postValue('rotation_no');

        $ff_cus_code = 'LHM';
        $cus_agent_code = '56828L';
        //"VOY","FREIGHT FORWARDING CUSTOM CODE(ALWAYS SAME)","CUSTOM AGENT CODE(ALWAYS SAME)" ,"VESSEL DETAILS","VOYAGE","POD","ETA","ROTATION (FROM DUBAITRADE)","MFI (ALWAYS SAME)","1 (THIS IS INSTALLMENT NUMBER)","00106 (JOB NUMBER)"
        //"VOY","56828L","L0057","X-PRESS KAILASH","23031W","AEJEA","18-AUG-2023","904783","MFI","1","00106"
        $file_content = array();
        $row = array();
        $row[] = 'VOY';//固定
        $row[] = $ff_cus_code;//FREIGHT FORWARDING CUSTOM CODE(ALWAYS SAME)
        $row[] = $cus_agent_code;//CUSTOM AGENT CODE(ALWAYS SAME)
        $row[] = $consol['vessel'];//VESSEL DETAILS
        $row[] = $consol['voyage'];//VOYAGE
        $row[] = $consol['trans_destination'];//POD
        $row[] = $this->date_format($consol['trans_ETA']);//ETA   18-AUG-2023
        $row[] = $rotation_no;//ROTATION (FROM DUBAITRADE)  这个是每个港口不一样的, 简单配置用吧
        $row[] = 'MFI';//MFI (ALWAYS SAME)
        $row[] = '1';//1 (THIS IS INSTALLMENT NUMBER)
        $row[] = $consol['job_no'];//00106 (JOB NUMBER)
        $file_content[] = '"' . join("\",\"", $row) . '"';

        //"BOL","KMLSHA07310002","56828L","L0057","CNSHA","CNSHA","AEJEA","AEJEA","01-Jan-1900","","I","","","","F","","Y","FCL/FCL","CN","","","X-PRESS KAILASH","23031W","","","","CHANGZHOU PRO-TECH TRADE CO.,LTD","CHANGZHOU,CHINA","","D9999","LOGI COMPOSITES FZE","PO BOX 17134 JEBEL ALI FZEDUBAI UAE","","SAME AS CONSIGNEE","","","","","","","","LCF-IMP-2023-048LCF-IMP-2023-051","770000","1X40 HC CNTR STC26 PALLETS OF MULTI-AXIAL FABRICSCOMBINATION MATHS CODE: 7019150014 DAYS FREE DETENTION AT DESTINATION ","26","PALLETS","PLT","","","1","2","4.4","26060","26060","65","0","26.06","0","","",""
        $msg = array();
        foreach ($shipments as $shipment){
            $containers = Model('biz_shipment_container_model')->join_container_get_by_id($shipment['id'], $shipment['consol_id']);
            if(empty($containers)){
                $msg[] = lang("{job_no}未添加箱信息", array('job_no' => $shipment['job_no']));
                continue;
            }
            $packs = array_sum(array_column($containers, 'packs'));
            $weight = array_sum(array_column($containers, 'weight'));
            $volume = array_sum(array_column($containers, 'volume'));
            $goods_type_num_config = array('GC' => '770000');
            if(!isset($goods_type_num_config[$shipment['goods_type']])){
                $msg[] = lang("{job_no}:{goods_type}未设置代码, 请联系管理员添加", array('job_no' => $shipment['job_no'], 'goods_type' => $shipment['goods_type']));
                continue;
            }
            $goods_type_num = $goods_type_num_config[$shipment['goods_type']];

            //查询字典表获取该code
            $iso_code = Model('bsc_dict_model')->get_one('name', $shipment['good_outers_unit'], 'packing_unit');
            $good_outers_unit_code = $iso_code['ext2'];//这个应该是件数单位对应的code
            if(empty($good_outers_unit_code)){
                $msg[] = lang("{job_no}:{good_outers_unit}未设置对应CODE, 请提供对应code后联系管理员添加.", array('job_no' => $shipment['job_no'], 'good_outers_unit' => $shipment['good_outers_unit']));
                continue;
            }
            //如果特殊情况, 将起运港代码转换一下
// 			$trans_origin_arr = array('CNSHA' => 'CNSHA', 'CNTAO' => 'CNQIN','CNSHE'=>'CNSHK');
            $trans_origin_arr = array('CNSHA' => 'CNSHA', 'CNTAO' => 'CNTAO','CNSHE'=>'CNSHK', 'CNNGB' => 'CNNBG');
            $trans_origin = $trans_origin_arr[$shipment['trans_origin']] ? $trans_origin_arr[$shipment['trans_origin']] : $shipment['trans_origin'];
            $row = array();
            $row[] = "BOL";
            $row[] = $shipment['hbl_no'];//KMLSHA07310002
            $row[] = $ff_cus_code;//56828L
            $row[] = $cus_agent_code;//L0057
            $row[] = $trans_origin;//CNSHA
            $row[] = $trans_origin;//CNSHA
            $row[] = $shipment['trans_discharge'];//AEJEA
            $row[] = $shipment['trans_destination'];//AEJEA
            $row[] = '';//说是系统默认的日期,   暂时先使用当前日期吧 01-Jan-1900
            $row[] = '';//
            $row[] = 'I';//     I
            $row[] = '';//
            $row[] = '';//
            $row[] = '';//

            $trans_mode_code_arr = array('FCL' => 'F', 'LCL' => 'L');
            $trans_mode_code = $trans_mode_code_arr[$shipment['trans_mode']];
            $row[] = $trans_mode_code;//看着像运输模式     F
            $row[] = "";//
            $row[] = "Y";//          Y
            $row[] = "{$shipment['trans_mode']}/{$shipment['trans_mode']}";//          FCL/FCL
            $row[] = substr($consol['trans_origin'], 0, 2);//国家代码, 直接用起运港前两位吧          CN
            $row[] = "";//
            $row[] = "";//
            $row[] = $consol['vessel'];//VESSEL    X-PRESS KAILASH
            $row[] = $consol['voyage'];//VOYAGE    23031W
            $row[] = "";//
            $row[] = "";//
            $row[] = "";//
            $row[] = $shipment['shipper_company'];//看着像发货人名称 CHANGZHOU PRO-TECH TRADE CO.,LTD
            $row[] = $this->text_format($shipment['shipper_address']);//发货人地址?  CHANGZHOU,CHINA
            $row[] = "";//
            //"D9999" This is default importer code for each customer which can keep same, "26.06" This is weight in Tons
            $row[] = "D9999";//他们说暂时固定     D9999
            $row[] = $shipment['consignee_company'];//收货人名称     LOGI COMPOSITES FZE
            $row[] = $this->text_format($shipment['consignee_address']);//收货人地址     PO BOX 17134 JEBEL ALI FZEDUBAI UAE
            $row[] = "";//
            $row[] = $shipment['notify_company'];//通知人名称   SAME AS CONSIGNEE
            $row[] = $this->text_format($shipment['notify_address']);//通知人地址
            $row[] = "";//
            $row[] = "";//
            $row[] = "";//
            $row[] = "";//
            $row[] = "";//
            $row[] = "";//
            $row[] = $shipment['mark_nums'];//看例子里 好像是唛头? LCF-IMP-2023-048LCF-IMP-2023-051
            $row[] = $goods_type_num;//这个根据当前货物类型变得 普货是770000        770000
            $row[] = $this->text_format($shipment['description']);//好像是品名    770000
            //"26","PALLETS","PLT","","","1","2","4.4","26060","26060","65","0","26.06","0","","",""
            $row[] = $packs;//一眼件数       26
            $row[] = $shipment['good_outers_unit'];//件数单位       PALLETS
            $row[] = $good_outers_unit_code;//件数单位代码       PLT
            $row[] = '';//
            $row[] = '';//
            $row[] = '1';//
            $row[] = '2';//
            $row[] = '4.4';//
            $row[] = $weight;//重量       26060
            $row[] = $weight;//重量       26060
            $row[] = $volume;//体积       65
            $row[] = '0';//体积       65
            $row[] = $weight / 1000;//
            $row[] = '0';//
            $row[] = '';//
            $row[] = '';//
            $row[] = '';//
            $file_content[] = '"' . join("\",\"", $row) . '"';

            //"CTR","MAGU562033","3","4.4","TSH2161031"
            foreach ($containers as $container){
                $row = array();
                $row[] = 'CTR';
                $row[] = $container['container_no'];// 箱号
                $row[] = substr($container['container_no'], -1);//  这个数字是取箱号最后一位
                $row[] = '4.4';//
                $row[] = $container['seal_no'];//封号
                $file_content[] = '"' . join("\",\"", $row) . '"';

                //con是上面CTR的详细信息
                $row = array();
                $row[] = 'CON';
                $row[] = '1';//    13
                $row[] = $shipment['mark_nums'];//唛头?    LCF-IMP-2023-048LCF-IMP-2023-051
                $row[] = $this->text_format($shipment['description']);//品名
                $row[] = '';//
                $row[] = '770000';//        770000
                $row[] = $container['packs'];//件数
                $row[] = $shipment['good_outers_unit'];//件数单位
                $row[] = $good_outers_unit_code;//件数单位CODE
                $row[] = '0';//          0
                $row[] = $container['weight'];//重量        26060
                $row[] = $container['volume'];//体积        65
                $row[] = 'N';//        N
                $row[] = '';//
                $row[] = '';//
                $row[] = '';//
                $row[] = '';//
                $row[] = 'Y';//    Y
                $row[] = 'N';//    N
                $row[] = '0';//    0
                $row[] = '0';//    0
                $row[] = '';//
                $file_content[] = '"' . join("\",\"", $row) . '"';
            }
        }
        if(!empty($msg)) return array('code' => 1, 'msg' => join(";<br />", $msg));

        //"END","0","0","Version no: 6.0 Rls 1972,Product sr. no: Nautilus Freight Management"
        $row = array();
        $row[] = 'END';
        $row[] = '0';//  0
        $row[] = '0';//  0
        $row[] = 'Version no: 6.0 Rls 1972,Product sr. no: Nautilus Freight Management';//  Version no: 6.0 Rls 1972,Product sr. no: Nautilus Freight Management
        $file_content[] = '"' . join("\",\"", $row) . '"';

        $data['file_content'] = join("\r\n", $file_content);
        return array('code' => 0, 'msg' => lang('生成成功'), 'data' => $data);

        //"CON","13","LCF-IMP-2023-048LCF-IMP-2023-051",
        //"1X40 HC CNTR STC26 PALLETS OF MULTI-AXIAL FABRICSCOMBINATION MATHS CODE: 7019150014 DAYS FREE DETENTION AT DESTINATION "
        //,"","770000","26","PALLETS","PLT","0","26060","65","N","","","","","Y","N","0","0",""
        //"BOL","KMLSHA07310003","56828L","L0057","CNSHA","CNSHA","AEJEA","AEJEA","01-Jan-1900","","I","","","","F","","Y","FCL/FCL","CN","","","X-PRESS KAILASH","23031W","","","","QINGDAO LESON SPECIAL ROPE CO., LTD","BUILDING 18, 2888 BINHAI AVENUEWEST COAST NEW DISTRICT,QINGDAOCHINA","","D9999","EMPRISE SAFETY & MARINE SUPPLIES L.L.C","121120, DUBAI MARITIME CITY, DMCDUBAI, UNITED ARAB EMIRATES","","SAME AS CONSIGNEE","","","","","","","","N/M","770000","1X20 GP CNTR STC150 PKGS OF PP NETHS:5607909000 ","150","PACKAGES","PKG","","","1","1","2.2","4800","4800","22","0","4.8","0","","",""
        //"CTR","FCIU598204","5","2.2","TSH2170706"
        //"CON","2","N/M","1X20 GP CNTR STC150 PKGS OF PP NETHS:5607909000 ","","770000","150","PACKAGES","PKG","0","4800","22","N","","","","","Y","N","0","0",""
        //"BOL","KMLSHA07310004","56828L","L0057","CNSHA","CNSHA","AEJEA","AEJEA","01-Jan-1900","","I","","","","F","","Y","FCL/FCL","CN","","","X-PRESS KAILASH","23031W","","","","TOPEVER LOGISTICS (NINGBO) LTD","15/F.,JIAN ZHU BUILDING,202 JIEFANGSOUTH ROAD,NINGBO CITY,ZHEJIANG PROV","","D9999","LCL SHIPPING & FORWARDING LLC","P.O.BOX. NO 123567DUBAI,UAETel: 04-3947283  Fax: 04-3947286","","SAME AS CONSIGNEE","","","","","","","","ORDER NO-129744","770000","2X40 HC CNTR STC81 PKGS OF BRAKE CHAMBER HS CODE:84123100 AIR COIL , AIR BRAKE TUBE, 7 PIN CABLE HS CODE:39173900 COUPLING HEAD HS CODE:73079910 ","81","PACKAGES","PKG","","","2","4","8.8","29197","29197","117.3","0","29.197","0","","",""
        //"CTR","BSIU995776","8","4.4","TSH2161165"
        //"CON","6","ORDER NO-129744","2X40 HC CNTR STC81 PKGS OF BRAKE CHAMBER HS CODE:84123100 AIR COIL , AIR BRAKE TUBE, 7 PIN CABLE HS CODE:39173900 COUPLING HEAD HS CODE:73079910 ","","770000","41","PACKAGES","PKG","0","12570","59.3","N","","","","","Y","N","0","0",""
        //"CTR","TSSU504054","6","4.4","TSH2161100"
        //"CON","7","ORDER NO-129744","2X40 HC CNTR STC81 PKGS OF BRAKE CHAMBER HS CODE:84123100 AIR COIL , AIR BRAKE TUBE, 7 PIN CABLE HS CODE:39173900 COUPLING HEAD HS CODE:73079910 ","","770000","40","PACKAGES","PKG","0","16627","58","N","","","","","Y","N","0","0",""
        //"BOL","KMLSHA07310005","56828L","L0057","CNSHA","CNSHA","AEJEA","AEJEA","01-Jan-1900","","I","","","","F","","Y","FCL/FCL","CN","","","X-PRESS KAILASH","23031W","","","","TOPEVER LOGISTICS (NINGBO) LTD","15/F.,JIAN ZHU BUILDING,202 JIEFANGSOUTH ROAD,NINGBO CITY,ZHEJIANG PROV","","D9999","OVERSEAS DEVELOPMENT LLC","OFFICE 305, 48 BURJGATE, POBOX 26461SHEIKH ZAYED ROAD,DUBAI UAE","","SAME AS CONSIGNEE","","","","","","","","N/M","770000","1X20 GP CNTR STC33 PKGS OF AIR CONDITIONER COMPRESSOR WITH ACCESSORIES HS CODE:84143000 ","33","PACKAGES","PKG","","","1","1","2.2","19718","19718","20","0","19.718","0","","",""
        //"CTR","CAIU697046","2","2.2","TSH2086266"
        //"CON","1","N/M","1X20 GP CNTR STC33 PKGS OF AIR CONDITIONER COMPRESSOR WITH ACCESSORIES HS CODE:84143000 ","","770000","33","PACKAGES","PKG","0","19718","20","N","","","","","Y","N","0","0",""
        //"BOL","KMLSHA07310007","56828L","L0057","CNSHA","CNSHA","AEJEA","AEJEA","01-Jan-1900","","I","","","","F","","Y","FCL/FCL","CN","","","X-PRESS KAILASH","23031W","","","","HUBEI BINHE TRADE CO LTD","ROOM NO 504 BUILDING G13 QUANZHOUGUOJI JIAOTONG XIROAD,XIAOGAN,HUBEIHUBEI,CHINA","","D9999","AL SAHEB BUILDING MATERIAL TRADING LLC","PO BOX 80694 SHARJAH UAE","","SAME AS CONSIGNEE","","","","","","","","AZIZDUBAITEL 04-2670375","770000","1X40 HC CNTR STC1638 CARTONS OF ANGLE BRUSH,PAINT ROLLER,SANDING DISC, KNAPSACK SPRAYER, LEVEL, FLAP DISC, CUTTING DISC ","1638","CARTONS","CTN","","","1","2","4.4","9965","9965","63.24","0","9.965","0","","",""
        //"CTR","BSIU982500","0","4.4","TSH1819027"
        //"CON","5","AZIZDUBAITEL 04-2670375","1X40 HC CNTR STC1638 CARTONS OF ANGLE BRUSH,PAINT ROLLER,SANDING DISC, KNAPSACK SPRAYER, LEVEL, FLAP DISC, CUTTING DISC ","","770000","1638","CARTONS","CTN","0","9965","63.24","N","","","","","Y","N","0","0",""
        //"BOL","KMLSHA07310008","56828L","L0057","CNSHA","CNSHA","AEJEA","AEJEA","01-Jan-1900","","I","","","","F","","Y","FCL/FCL","CN","","","X-PRESS KAILASH","23031W","","","","HUBEI BINHE TRADE CO LTD","ROOM NO 504 BUILDING G13 QUANZHOUGUOJI JIAOTONG XIROAD,XIAOGAN,HUBEIHUBEI,CHINA","","D9999","TZZ BUILDING MATERIALS TRADING LLC","OFFICE NO 302 DNATA BUILDINGBANIYAS SQUARE DEIRA DUBAIUAE","","SAME AS CONSIGNEE","","","","","","","","T-DXB","770000","1X40 HC CNTR STC1084 CARTONS OF PAINT ROLLER,PAINT ROLLER REFILL,RADIATOR ROLLER,RADIATOR ROLLER REFILL,PAINT BRUSH,EXTENSION POLE,PAINT MIXER,TCT HOLE SAW,HAMMER,RUBBING BRICK,WELDING CHALK ","1084","CARTONS","CTN","","","1","2","4.4","16697","16697","63","0","16.697","0","","",""
        //"CTR","TLLU885694","1","4.4","TSH1819029"
        //"CON","4","T-DXB","1X40 HC CNTR STC1084 CARTONS OF PAINT ROLLER,PAINT ROLLER REFILL,RADIATOR ROLLER,RADIATOR ROLLER REFILL,PAINT BRUSH,EXTENSION POLE,PAINT MIXER,TCT HOLE SAW,HAMMER,RUBBING BRICK,WELDING CHALK ","","770000","1084","CARTONS","CTN","0","16697","63","N","","","","","Y","N","0","0",""
        //"BOL","KMLSHA07310009","56828L","L0057","CNSHA","CNSHA","AEJEA","AEJEA","01-Jan-1900","","I","","","","F","","Y","FCL/FCL","CN","","","X-PRESS KAILASH","23031W","","","","SHANGHAI FLOWINK INTL LOGISTICS LTD","12F, BLOCK A, 777 HUOSHAN ROAD,YANGPU DISTRICT,SHANGHAI,CHINA","","D9999","CONCORD PACIFIC SHIPPING LLC","P.O. BOX: 32783 OFFICE NO: 205,2ND FLOOR, AL KHALEEJ BUILDINGDUBAI - UNITED ARAB EMIR","","SAME AS CONSIGNEE","","","","","","","","N/M","770000","1X40 HC CNTR STC233 PKGS OF VALVEHS:8481804090 ","233","PACKAGES","PKG","","","1","2","4.4","26472.7","26472.7","56.844","0","26.473","0","","",""
        //"CTR","CAIU463747","3","4.4","TSH2161594"
        //"CON","3","N/M","1X40 HC CNTR STC233 PKGS OF VALVEHS:8481804090 ","","770000","233","PACKAGES","PKG","0","26472.7","56.844","N","","","","","Y","N","0","0",""
        //"BOL","KMLSHA07310010","56828L","L0057","CNSHA","CNSHA","AEJEA","AEJEA","01-Jan-1900","","I","","","","F","","Y","FCL/FCL","CN","","","X-PRESS KAILASH","23031W","","","","SUNGROW POWER SUPPLY CO., LTD","CHINA","","D9999","FOCUS FREIGHT INTERNATIONAL","JEBEL ALI FREE ZONE, P.O.BOX:263139UAETel: 04-2678011","","SAME AS CONSIGNEE","","","","","","","","N/M","770000","1X40 HC CNTR STC22 PKGS OF SUNGROW INVERTER SMART COMMUNICATION BOX METER SMART COMMUNICATION DEVICE INVERTER ACCESSORY WEATHER STATION ","22","PACKAGES","PKG","","","1","2","4.4","5442.9","5442.9","30.89","0","5.443","0","","",""
        //"CTR","TCKU675057","5","4.4","TSH2161549"
        //"CON","10","N/M","1X40 HC CNTR STC22 PKGS OF SUNGROW INVERTER SMART COMMUNICATION BOX METER SMART COMMUNICATION DEVICE INVERTER ACCESSORY WEATHER STATION ","","770000","22","PACKAGES","PKG","0","5442.9","30.89","N","","","","","Y","N","0","0",""
        //"BOL","KMLSHA07310012","56828L","L0057","CNSHA","CNSHA","AEJEA","AEJEA","01-Jan-1900","","I","","","","F","","Y","FCL/FCL","CN","","","X-PRESS KAILASH","23031W","","","","SUZHOU HLC PLASTICS INDUSTRY CO LTD","NO 103,FENGBEIDANG ROAD,BEIQIAO BLOCXIANGCHENG DISTRICT,SUZHOU,CHINASUZHOU,CHINA","","D9999","TO THE ORDER OF SUZHOU HLC PLASTICS INDUSTRY CO LT","CHINA","","WATEX PUMPS TRADING","D8 NEW INDUSTRIAL AREA AJMANAJMAN,UAE","","","","","","","N/M","770000","1X40 HC CNTR STC840 CARTONS OF HOSE REEL ASSEMBLY ","840","CARTONS","CTN","","","1","2","4.4","17656.8","17656.8","68.38","0","17.657","0","","",""
        //"CTR","FFAU439646","4","4.4","TSH2170626"
        //"CON","9","N/M","1X40 HC CNTR STC840 CARTONS OF HOSE REEL ASSEMBLY ","","770000","840","CARTONS","CTN","0","17656.8","68.38","N","","","","","Y","N","0","0",""
        //"BOL","KMLSHA07310013","56828L","L0057","CNSHA","CNSHA","AEJEA","AEJEA","01-Jan-1900","","I","","","","F","","Y","FCL/FCL","CN","","","X-PRESS KAILASH","23031W","","","","TOPEVER LOGISTICS (NINGBO) LTD","15/F.,JIAN ZHU BUILDING,202 JIEFANGSOUTH ROAD,NINGBO CITY,ZHEJIANG PROV","","D9999","VAM LOGISTICS LLC","PLOT NO : S40114JEBEL ALI FREEZONE, JEBEL ALI, DUBAI","","VAM LOGISTICS LLC","PLOT NO : S40114JEBEL ALI FREEZONE, JEBEL ALI, DUBAI","","","","","","","N/M","770000","2X40 HC CNTR STC39 PALLETS OF CHASSIS ASSEMBLY FRONT FACE UNIT FRONT PANEL UNIT CABLE UNIT MAIN BOARD SCREW SPARE PARTS SPARE PARTS ","39","PALLETS","PLT","","","2","4","8.8","9151.1","9151.1","83.68","0","9.151","0","","",""
        //"CTR","TCKU794504","1","4.4","TSH1817870"
        //"CON","11","N/M","2X40 HC CNTR STC39 PALLETS OF CHASSIS ASSEMBLY FRONT FACE UNIT FRONT PANEL UNIT CABLE UNIT MAIN BOARD SCREW SPARE PARTS SPARE PARTS ","","770000","19","PALLETS","PLT","0","3584.1","37.93","N","","","","","Y","N","0","0",""
        //"CTR","TSSU508012","7","4.4","TSH1817866"
        //"CON","12","N/M","2X40 HC CNTR STC39 PALLETS OF CHASSIS ASSEMBLY FRONT FACE UNIT FRONT PANEL UNIT CABLE UNIT MAIN BOARD SCREW SPARE PARTS SPARE PARTS ","","770000","20","PALLETS","PLT","0","5567","45.75","N","","","","","Y","N","0","0",""
        //"BOL","KMLSHA07310014","56828L","L0057","CNSHA","CNSHA","AEJEA","AEJEA","01-Jan-1900","","I","","","","F","","Y","FCL/FCL","CN","","","X-PRESS KAILASH","23031W","","","","HUBEI BINHE TRADE CO LTD","ROOM NO 504 BUILDING G13 QUANZHOUGUOJI JIAOTONG XIROAD,XIAOGAN,HUBEIHUBEI,CHINA","","D9999","MODERN VISION TRADING LLC","PO BOX 97105,DUBAI,UAE","","SAME AS CONSIGNEE","","","","","","","","N/M","770000","1X40 HC CNTR STC1526 CARTONS OF PAINT ROLLER, PAINT ROLLER FRAME,PAINT ROLLER REFILL,RADIATOR ROLLER REFILL, HAMMER,METAL CLIP,CUP GRINDING STONE, RUBBING BRICK, WELDING CHALK, RAIN BOOT, TILE SPACER ","1526","CARTONS","CTN","","","1","2","4.4","21873.5","21873.5","65.23","0","21.874","0","","",""
        //"CTR","TLLU796361","0","4.4","TSH1817904"
        //"CON","8","N/M","1X40 HC CNTR STC1526 CARTONS OF PAINT ROLLER, PAINT ROLLER FRAME,PAINT ROLLER REFILL,RADIATOR ROLLER REFILL, HAMMER,METAL CLIP,CUP GRINDING STONE, RUBBING BRICK, WELDING CHALK, RAIN BOOT, TILE SPACER ","","770000","1526","CARTONS","CTN","0","21873.5","65.23","N","","","","","Y","N","0","0",""
        //"END","0","0","Version no: 6.0 Rls 1972,Product sr. no: Nautilus Freight Management"
    }

    public function ae_manifest_form($param){
        $controller = $param['match']['controller'];
        $method = $param['match']['form_method'];
        $id_type = $param['match']['id_type'];
        $id_no = $param['id_no'];
        $receiver = $param['match']['message_receiver'];
        $type = $param['match']['message_type'];
        $action = $param['action'];


        $data = array();
        $data['match'] = $param['match'];
        $data['action'] = $action;
        $data['id_no'] = $id_no;

        $message = Model('bsc_message_model')->get_where_one("id_type = '{$id_type}' and id_no = '{$id_no}' and type = '{$type}' and receiver = '{$receiver}'");
        $data['message'] = $message;
        $data['edi_data'] = array('rotation_no' => '');
        if(!empty($message)){
            $message_data = Model('bsc_message_data_model')->get_where_one("message_id = {$message['id']}");
            if(!empty($message_data)){
                $data['edi_data'] = json_decode($message_data['send_data'], true);
            }
        }
        if(is_array($data['edi_data'])) $data['edi_data'] = array_replace_str($data['edi_data'], array('\r\n' => "\r\n"));
        $this->load->view('head');
        $this->load->view('/bsc/message/data/ae_manifest_form', $data);
    }
}
