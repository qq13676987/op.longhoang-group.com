<script type="text/javascript">
    //采用jquery easyui loading css效果
    function ajaxLoading(){
        $("<div class=\"datagrid-mask\"></div>").css({display:"block",width:"100%",height:$(window).height()}).appendTo("body");
        $("<div class=\"datagrid-mask-msg\"></div>").html("正在处理，请稍候。。。").appendTo("body").css({display:"block",left:($(document.body).outerWidth(true) - 190) / 2,top:($(window).height() - 45) / 2});
    }
    function ajaxLoadEnd(){
        $(".datagrid-mask").remove();
        $(".datagrid-mask-msg").remove();
    }
    $(function () {
        $('.reload_message').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/bsc_message/create_message/<?= $consol['id'];?>/ESL/SO/' + action,
                onBeforeLoad:function(){
                    ajaxLoading();
                },
                success: function (res) {
                    ajaxLoadEnd();
                    $('.reload_message').attr('disabled', false);
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg, 'info', function () {
                            location.reload();
                        });
                    }else{
                        $.messager.alert('Tips', res.msg, 'info');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $('.reload_message').attr('disabled', false);
                    $.messager.alert('Tips', 'error');
                }
            });
        });

        $('.reload_message2').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/bsc_message/create_message/<?= $consol['id'];?>/SINOTRANS/SO/' + action,
                onBeforeLoad:function(){
                    ajaxLoading();
                },
                success: function (res) {
                    ajaxLoadEnd();
                    $('.reload_message2').attr('disabled', false);
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg, 'info', function () {
                            location.reload();
                        });
                    }else{
                        $.messager.alert('Tips', res.msg, 'info');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $('.reload_message2').attr('disabled', false);
                    $.messager.alert('Tips', 'error');
                }
            });
        });
        
        $('.reload_message3').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');

            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/bsc_message/create_message/<?= $consol['id'];?>/yitong/MANIFEST/' + action,
                onBeforeLoad:function(){
                    ajaxLoading();
                },
                success: function (res) {
                    ajaxLoadEnd();
                    $('.reload_message2').attr('disabled', false);
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg, 'info', function () {
                            location.reload();
                        });
                    }else{
                        $.messager.alert('Tips', res.msg, 'info');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $('.reload_message2').attr('disabled', false);
                    $.messager.alert('Tips', 'error');
                }
            });
        });

        $('.send').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');
            var receiver = $(this).attr('receiver');
            var id = $(this).attr('mid');
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/bsc_message/send_message/' + id + '/' + action + '/' + receiver,
                onBeforeLoad:function(){
                    ajaxLoading();
                },
                success: function (res) {
                    ajaxLoadEnd();
                    $('.send').attr('disabled', false);
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg, 'info', function () {
                            location.reload();
                        });
                    }else{
                        $.messager.alert('Tips', res.msg, 'info');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $('.send').attr('disabled', false);
                    $.messager.alert('Tips', 'error');
                }

            });
        });
        //查看报文
        $('.read_message').click(function () {
            var id = $(this).attr('mid');
            window.open('/bsc_message/file_read/' + id);
        });
    });
</script>
  


<fieldset>
    <legend>亿通舱单发送</legend>
    <?php if(empty($yitong)){ ?>
        <button class="reload_message3" action="insert">点击生成报文</button>
    <?php }else{?>
        <?php if($yitong['receiver'] == 'yitong'){?>
            <!--<a href="http://china.leagueshipping.com/upload/message/yitong_EDI/MANIFEST<?= $yitong['file_path'];?>"  target="_blank">预览报文</a>-->
            <button class="reload_message3" action="insert">重新生成报文</button>
            <br />
            发送情况:
            <?php if($yitong['status'] == 0){?>
                未发送
            <?php }else if($yitong['status'] == 1){
               echo $yitong['send_time'];?>
                发送成功
            <?php }else if($yitong['status'] == 2){?>
                发送修改成功
            <?php }?>
            <br />
            <button class="read_message" mid="<?= $yitong['id'];?>" >查看报文</button>
            <button class="send" action="API" mid="<?= $yitong['id'];?>" receiver="<?= $yitong['receiver'];?>">发送报文</button>
        <?php }?>
    <?php }?>
</fieldset>
