<?php


class biz_shipment_ebooking extends Menu_Controller
{
    protected $menu_name = 'shipment';//菜单权限对应的名称
    protected $method_check = array(//需要设置权限的方法
        'index',
        'add',
        'edit',
    );

    protected $admin_field = array('operator', 'customer_service', 'sales');//, 'finance');
    
    protected $status_config = array(
        '-1' => '拒绝',
        '0' => '待操作审核',
        '1' => '通过',
    );
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_shipment_ebooking_model');
        // foreach ($this->field_all as $row) {
        //     $v = $row[0];
        //     if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
        //     array_push($this->field_edit, $v);
        // }
        $this->field_edit = $this->biz_shipment_ebooking_model->field_edit;
    }

    public $field_all = array(
        array("id", "id", "40", "1", 'sortable:true'),
        array("consol_id", "consol_id", "0", "1"),
        array("job_no", "job_no", "150", "1"),
        array("biz_type", "biz_type", "0", "1"),
        array("status", "status", "0", "1"),

        array("agent_code", "agent_code", "0", "1"),
        array("agent_company", "agent_company", "100", "1"),
        array("agent_address", "agent_address", "0", "1"),
        array("agent_contact", "agent_contact", "0", "1"),
        array("agent_telephone", "agent_telephone", "0", "1"),

        array("client_code", "client_code", "0", "1"),
        array("client_company", "client_company", "100", "1"),
        array("client_address", "client_address", "0", "1"),
        array("client_contact", "client_contact", "0", "1"),
        array("client_telephone", "client_telephone", "0", "1"),

        array("shipper_company", "shipper_company", "100", "1"),
        array("shipper_address", "shipper_address", "0", "1"),
        array("shipper_contact", "shipper_contact", "0", "1"),
        array("shipper_telephone", "shipper_telephone", "0", "1"),
        array("shipper_email", "shipper_email", "0", "1"),

        array("consignee_company", "consignee_company", "100", "1"),
        array("consignee_address", "consignee_address", "0", "1"),
        array("consignee_contact", "consignee_contact", "0", "1"),
        array("consignee_telephone", "consignee_telephone", "0", "1"),
        array("consignee_email", "consignee_email", "0", "1"),

        array("notify_company", "notify_company", "100", "1"),
        array("notify_address", "notify_address", "0", "1"),
        array("notify_contact", "notify_contact", "0", "1"),
        array("notify_telephone", "notify_telephone", "0", "1"),
        array("notify_email", "notify_email", "0", "1"),

        array("truck_company", "truck_company", "100", "1"),
        array("truck_date", "truck_date", "0", "1"),
        array("truck_address", "truck_address", "0", "1"),
        array("truck_contact", "truck_contact", "0", "1"),
        array("truck_telephone", "truck_telephone", "0", "1"),
        array("truck_remark", "truck_remark", "0", "1"),

        array("trans_mode", "trans_mode", "70", "1"),
        array("trans_term", "trans_term", "70", "1"),
        array("contract_no", "contract_no", "150", "1"),
        array("shipper_ref", "shipper_ref", "100", "1"),
        array("description", "description", "100", "1"),
        array("mark_nums", "mark_nums", "100", "1"),
        array("release_type", "release_type", "70", "1"),
        array("trans_origin", "trans_origin", "100", "1"),
        array("trans_discharge", "trans_discharge", "100", "1"),
        array("trans_destination", "trans_destination", "100", "1"),
        array("sailing_code", "sailing_code", "100", "1"),
        array("trans_carrier", "trans_carrier", "80", "1"),
        array("booking_ETD", "booking_ETD", "0", "100"),
        array("trans_ETD", "trans_ETD", "0", "100"),
        array("trans_ATD", "trans_ATD", "0", "100"),
        array("trans_ETA", "trans_ETA", "0", "100"),
        array("trans_ATA", "trans_ATA", "0", "100"),
        array("good_outers", "good_outers", "0", "1"),
        array("good_outers_unit", "good_outers_unit", "0", "1"),
        array("good_weight", "good_weight", "0", "1"),
        array("good_weight_unit", "good_weight_unit", "0", "1"),
        array("good_volume", "good_volume", "0", "1"),
        array("good_volume_unit", "good_volume_unit", "0", "1"),
        array("good_describe", "good_describe", "0", "1"),
        array("good_hscode", "good_hscode", "0", "1"),
        array("good_commodity", "good_commodity", "0", "1"),
        array("INCO_term", "INCO_term", "0", "1"),
//        array("issue_date", "issue_date", "0", "1"),
//        array("expiry_date", "expiry_date", "0", "1"),
        array("hbl_no", "hbl_no", "0", "1"),
        array("hbl_type", "hbl_type", "0", "1"),
        array("warehouse_code", "warehouse_code", "0", "1"),
        array("warehouse_request", "warehouse_request", "0", "1"),
        array("warehouse_charge", "warehouse_charge", "0", "1"),
        array("warehouse_in_date", "warehouse_in_date", "0", "1"),
        array("warehouse_in_num", "warehouse_in_num", "0", "1"),
        array("dadanwancheng", "dadanwancheng", "0", "1"),
        array("haiguancangdan", "haiguancangdan", "0", "1"),
        array("baoguanjieshu", "baoguanjieshu", "0", "1"),
        array("tidanqueren", "tidanqueren", "0", "1"),
        array("tidanqianfa", "tidanqianfa", "0", "1"),
        array("box_info", "box_info", "0", "1"),
        array("hs_code", "hs_code", "0", "1"),
        array("goods_type", "goods_type", "0", "1"),
        array("dangergoods_id", "dangergoods_id", "0", "1"),
        array("requirements", "requirements", "0", "1"),
        array("service_options", "service_options", "0", "1"),
        array("status", "status", "0", "1"),
    );

    public $field_edit = array();
	public function index(){
		$data = array();
		$data['edi_num'] = $this->db->where('status',0)->count_all_results('biz_edi_ebooking_consol');
		$this->load->view('head');
		$this->load->view('biz/shipment_ebooking/tabs', $data);
	}
    public function index2(){
        $data = array();
        // if($this->session->userdata('level') != 9999 && $this->session->userdata('id') != '20013'){
            // exit('正在维护中');
        // }
        // column defination
        $data['status'] = getValue('status', '');
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_shipment_ebooking_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $this->field_edit[] = "job_no";
            $data["f"] = array_map(function ($r){
                return array($r, $r, 0);
            }, $this->field_edit);
        }
        // page account
        $rs = $this->sys_config_model->get_one('biz_shipment_ebooking_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }
        $data['config_search'] = json_decode(getConfigSearch('biz_shipment_ebooking', array('job_no', 'like', ''))['config_text'], true);
        
        $this->load->view('head');
        $this->load->view('biz/shipment_ebooking/index_view', $data);
    }
    
    /**
     * 这里获取页面查询等需要用到的状态数据
     */
    public function get_status(){
        $status_config = $this->status_config;
        //这里转换为页面统一使用的方便格式
        $data = array();
        
        foreach ($status_config as $key => $val){
            $data[] = array('name' => lang($val), 'value' => $key);
        }
        
        return jsonEcho($data);
    }
    
    private function get_status_name($status){
        return lang($this->status_config[$status]);
    }

    public function edit($id = 0){
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $data[$item] = "";
        }
        $data['box_info'] = "{}";

        $data['id'] = $id;
        $shipment_ebooking = $this->biz_shipment_ebooking_model->get_one('id', $id);
        if(!empty($shipment_ebooking)){
            $data = $shipment_ebooking;
        }else{
            echo "参数错误";
            return;
        }
        $data['free_svr'] = json_decode($data['free_svr'], true);
        $data['box_info'] = json_decode($data['box_info'], true);
        
        //2022-04-22 由于 前台存入的数据,只有港口名称,所以这里需要重新保存一下
        Model('biz_port_model');
        if($data['trans_origin'] === '' && $data['trans_origin_name'] !== ''){
            $this->db->select('port_code');
            $port = $this->biz_port_model->get_one('port_name', $data['trans_origin_name']);
            $data['trans_origin'] = $port['port_code'];
        } 
        if($data['trans_discharge'] === '' && $data['trans_discharge_name'] !== ''){
            $this->db->select('port_code');
            $port = $this->biz_port_model->get_one('port_name', $data['trans_discharge_name']);
            $data['trans_discharge'] = $port['port_code'];
        }
        if($data['trans_destination'] === '' && $data['trans_destination_name'] !== ''){
            $this->db->select('port_code');
            $port = $this->biz_port_model->get_one('port_name', $data['trans_destination_name']);
            $data['trans_destination'] = $port['port_code'];
        }
        $this->load->view('head');
        if($data['trans_mode'] == 'FCL' && $data['trans_tool'] == 'SEA' && $data['biz_type'] == 'export'){
            $this->load->view('biz/shipment_ebooking/export_fcl_sea/edit_view', $data);
        }else{
            $this->load->view('biz/shipment_ebooking/other/edit_view', $data);
        }
    }

    public function get_data(){
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $fields = postValue('field', array());
        $status = getValue('status', '');
        $post_status = postValue('status', '');

        //-------这个查询条件想修改成通用的 -------------------------------
        $where = array();

        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_shipment_ebooking', 'biz_shipment_ebooking_index');//获取相关的配置信息
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];


                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') $v = '';
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) $title_data['sql_join'][$title_data['base_data'][$title_f]['sql_join']] = $this_join;

                    $where[] = search_like($f, $s, $v);
                }
            }
        }

        // $where[] = "biz_shipment_ebooking.status > -2";
        if($status !== '') $where[] = "biz_shipment_ebooking.status = $status";
        if($post_status !== '') $where[] = "biz_shipment_ebooking.status = $post_status";

        // //加入client权限
        // $biz_client_model = Model('biz_client_model');
        // $where[] = $biz_client_model->read_role();
        //旧版废除,新版只根据销售来
        $user_range = join(",", get_session("user_range")) ;
        if(!is_admin(1)){
            $role_where = array();
            //这里我组有存的,如果到时候又要上级,可以修改
            if(in_array('sales', get_session('user_role'))) $role_where[] = "biz_shipment_ebooking.sales_id = " . get_session('id');
            if(in_array('operator', get_session('user_role'))) $role_where[] = "biz_shipment_ebooking.operator_id in ($user_range)";
            if(in_array('customer_service', get_session('user_role'))) $role_where[] = "biz_shipment_ebooking.customer_service_id = " . get_session('id');
            if(empty($role_where)) $role_where[] = "1 = 0";
            
            $where[] = "(" . join(' or ', $role_where) . ")";
        }
        $where = join(' and ', $where);
        //-----------------------------------------------------------------
        $offset = ($page - 1) * $rows;
        $this->db->limit($rows, $offset);
//        $this->db->select('biz_shipment_ebooking.*, (select company_name from biz_client where biz_client.client_code = biz_shipment_ebooking.client_code2) as client_code2_name, biz_shipment.job_no');
//        $this->db->join('biz_shipment', 'biz_shipment.id = biz_shipment_ebooking.shipment_id', 'left');
        //获取需要的字段--start
        $selects = array(
            'biz_shipment_ebooking.id as id',
            'biz_shipment_ebooking.status as status',
        );
        foreach ($title_data['select_field'] as $key => $val){
            if(!in_array($val, $selects)) $selects[] = $val;
        }
        //获取需要的字段--end
        $this->db->select(join(',', $selects), false);
        //这里拼接join数据
        foreach ($title_data['sql_join'] as $j){
            $this->db->join($j[0], $j[1], $j[2]);
        }//将order字段替换为 对应的sql字段
        $sort = $title_data['sql_field']["biz_shipment_ebooking.{$sort}"];
        $this->db->is_reset_select(false);//不重置查询
        $rs = $this->biz_shipment_ebooking_model->get($where, $sort, $order);

        $this->db->clear_limit();//清理下limit
        $result["total"] = $this->db->count_all_results('');

        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }


    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->biz_shipment_ebooking_model->get_one('id', $id);

        $this->biz_shipment_ebooking_model->mdelete($id);
        echo json_encode(array('success' => true));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment_ebooking";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        $this->log_rcd($log_data);
    }
    
     /**
     * ebooking销售下单
     */
    public function add(){
        $data = array();
        // if(!in_array('sales', get_session('user_role')) && !in_array('customer_service', get_session('user_role'))){
        //     echo "请联系销售或者客服新增";
        //     return;
        // }
        $id = getValue('id', '');
        //新增时,生成一个随机ID,用于上传文件 ;  前面加个字母,不然如果ID真的到了这么多,会出现BUG
        $field_edit = $this->biz_shipment_ebooking_model->field_edit;
        foreach ($field_edit as $field){
            $data[$field] = "";
        }
        $data['box_info'] = "{}";
        

        $rand_id = 'T' . get_session('id') . date('YmdHis');
        $data['rand_id'] = $rand_id;
        //2022-04-06 由于客服也会新增ebooking,所以这里修改为如果是
        $data['sales_id'] = '';
        $data['sales_group'] = '';
        $data['biz_type'] =  getValue('biz_type', 'export');
        $data['trans_mode'] = getValue('trans_mode', 'FCL');
        $data['trans_tool'] = getValue('trans_tool', 'SEA');
        $data['goods_type'] = '';
        
        //复制的有传入页面
        $shipment_ebooking = $this->biz_shipment_ebooking_model->get_where_one("id = {$id}");
        if(!empty($shipment_ebooking)){
            //复制的需要将ID取出,不然会变成修改了
            //rand_id取新生成的

            $data = $shipment_ebooking;
            $data['id'] = '';
            $data['rand_id'] = $rand_id;
        }
        
        $data['box_info_arr'] = json_decode($data['box_info'], true);
        $data['free_svr'] = json_decode($data['free_svr'], true);
        
        $this->load->view('head');
        if($data['trans_mode'] == 'FCL' && $data['trans_tool'] == 'SEA' && $data['biz_type'] == 'export'){
            $this->load->view('/biz/shipment_ebooking/export_fcl_sea/add_view', $data);
        }else{
            $this->load->view('biz/shipment_ebooking/other/add_view', $data);
        }

    }

    /**
     * 新增一个ebooking
     * @return bool
     */
    public function add_data(){
        //这里销售直接关联为当前人
        $rand_id = $_POST['rand_id'];

        $data = $_POST;

        $data['created_id_type'] = 0;
        
        box_info_handle($data['box_info']);
        if(empty($data['service_options'])) $data['service_options'] = array();
        if(empty($data['service_options'])) return jsonEcho(array('code' => 1, 'msg' => lang('右上角服务选项必填')));
        // print_r($data);exit();
        $service_options = array();
        foreach($data["service_options"] as $k=>$v){
            $service_options[] = $v[0];
        }
        $INCO_term = isset($data['INCO_term']) ? $data['INCO_term'] : "";
        if(!in_array('booking', $service_options) && $INCO_term == 'CIF') return jsonEcho(array('code' => 1, 'msg' => lang("未勾选订舱服务不能选CIF")));
        //只有勾选目的港服务至少1个，才能选择DDU  DDP  DAP
        if(in_array($INCO_term, array('DDU', 'DDP', 'DAP'))){
            //取交集,如果没有目的港服务,那么不允许选
            // print_r($service_options);
            $mdgfw_arr = array_intersect(array('agent_handle', 'destination_warehouse', 'destination_trucking', 'destination_custom'),$service_options);
            if(sizeof($mdgfw_arr) == 0) return jsonEcho(array('code' => 1, 'msg' => lang("贸易方式为DDU  DDP  DAP时,必须勾选任意一个目的港服务")));
        }
        $client_code2 = $data['client_code2'];
        $this->db->select('id,role');
        $client = Model('biz_client_model')->get_where_one("client_code = '{$data['client_code']}'");
        if(in_array('oversea_client', explode(',', $client['role']))){
            if($data['client_code'] == $client_code2){
                // Mars 打电话取消
//                return jsonEcho(array('code' => 1, 'msg' => 'oversea client 客户必须选择不同的二级委托方，如果没二级委托方，请先到往来单位设置关联！'));
            }
        }
        

        // return;
        $data['service_options'] = json_encode($data['service_options']);

        free_svr_handle($data['free_svr']);
        $data['free_svr'] = json_encode($data['free_svr']);
        $data['box_info'] = json_encode($data['box_info']);

        //2022-03-31 如果是HBL 那么代理必填
        //以下为海运整箱时才会限制的
        if($data['trans_mode'] == 'FCL' && $data['trans_tool'] == 'SEA' && $data['biz_type'] == 'export'){
            if($data['hbl_type'] != 'MBL' && empty($data['agent_code'])) return jsonEcho(array('code' => 1, 'msg' => lang('检测到当前非MBL,请选择代理')));
        }

        //2024-03-29 新加入了 根据本地系统前缀 限制 出口起运港 和 进口目的港 必须为指定国家前缀
        $system_type = strtoupper(get_system_type());
		if(strlen($data['trans_origin']) ==5) {
			if ($data['biz_type'] == 'export') {
				//出口
				$trans_origin_country = substr($data['trans_origin'], 0, 2);
				$branch_office_port = Model('bsc_dict_model')->get_option("branch_office_port", "value = '{$trans_origin_country}'");
				$branch_office_port_name_arr = array_map(function ($r) {
					return strtoupper($r);
				}, array_column($branch_office_port, 'name'));
				if (!empty($branch_office_port) && !in_array($system_type, $branch_office_port_name_arr)) {
					return jsonEcho(array('code' => 1, 'msg' => lang('Please add order to {system} system', array('system' => $branch_office_port[0]['value']))));
				}
			} else {
				//进口
				$trans_destination_country = substr($data['trans_destination'], 0, 2);
				$branch_office_port = Model('bsc_dict_model')->get_option("branch_office_port", "value = '{$trans_destination_country}'");
				$branch_office_port_name_arr = array_map(function ($r) {
					return strtoupper($r);
				}, array_column($branch_office_port, 'name'));
				if (!empty($branch_office_port) && !in_array($system_type, $branch_office_port_name_arr)) {
					return jsonEcho(array('code' => 1, 'msg' => lang('Please add order to {system} system', array('system' => $branch_office_port[0]['value']))));
				}
			}
		}

        //如果存在ID值,那么这里改为修改
        $id = $data['id'];
        if($id > 0){
            $this->biz_shipment_ebooking_model->update($id, $data);
        }else{
            $id = $this->biz_shipment_ebooking_model->save($data);
        }

        

        if($id){
            //save the operation log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment_ebooking";
            $log_data["key"] = $id;
            $log_data["action"] = "insert";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);

            //保存成功后,将对应的文件,转移过来
            //if(isset($data['status']) && $data['status'] == -2) return jsonEcho(array('code' => 1, 'msg' => '暂存成功', 'data' => array('id' => $id)));
            Model('bsc_upload_model');
            $this->db->select('id');
            $uploads = $this->bsc_upload_model->get("biz_table='biz_shipment_ebooking' and id_no = '$rand_id'");

            foreach ($uploads as $upload){
                $upload_update_data = array();
                $upload_update_data['id_no'] = $id;

                $this->bsc_upload_model->update($upload['id'], $upload_update_data);

                //save the operation log
                $log_data = array();
                $log_data["table_name"] = "bsc_upload";
                $log_data["key"] = $upload['id'];
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($upload_update_data);
                log_rcd($log_data);
            }

            return jsonEcho(array('code' => 0, 'msg' => lang('新增成功'), 'data' => array('id' => $id)));
        }

        return jsonEcho(array('code' => 1, 'msg' => lang('新增失败'), lastquery()));

    }

    /**
     * 编辑保存
     * @param $id int
     */
    public function update_data($id = 0){
        $old_row = $this->biz_shipment_ebooking_model->get_one('id', $id);
        if(empty($old_row)) return jsonEcho(array('code' => 1, 'msg' => lang('ebooking不存在')));

        $data = $_POST;

        free_svr_handle($data['free_svr']);
        $data['free_svr'] = json_encode($data['free_svr']);
        if(!empty($data["box_info"])){
            box_info_handle($data['box_info']);
            $data['box_info'] = json_encode($data['box_info']);
        }

        if(empty($data['service_options'])) return jsonEcho(array('code' => 1, 'msg' => lang('右上角服务选项必填')));
        $data['service_options'] = json_encode($data['service_options']);


        //2022-03-31 如果是HBL 那么代理必填
        if($data['trans_mode'] == 'FCL' && $data['trans_tool'] == 'SEA' && $data['biz_type'] == 'export') {
            if ($data['hbl_type'] != 'MBL' && empty($data['agent_code'])) return jsonEcho(array('code' => 1, 'msg' => lang('检测到当前非MBL,请选择代理')));
        }else{
           
        }
        
        $client_code2 = $data['client_code2'];
        $this->db->select('id,role');
        $client = Model('biz_client_model')->get_where_one("client_code = '{$data['client_code']}'");
        if(in_array('oversea_client', explode(',', $client['role']))){
            if($data['client_code'] == $client_code2){
                // Mars 打电话取消
//                return jsonEcho(array('code' => 1, 'msg' => 'oversea client 客户必须选择不同的二级委托方，如果没二级委托方，请先到往来单位设置关联！'));
            }
        }

        foreach ($data as $key => $val){
            if(isset($old_row[$key]) && $old_row[$key] == $val) unset($data[$key]);
        }

        $this->biz_shipment_ebooking_model->update($id, $data);

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment_ebooking";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        return jsonEcho(array('code' => 0, 'msg' => lang('保存成功')));
    }
    
    /**
     * 拒绝掉该Ebooking
     */
    public function cancel_status(){
        $id = (int)getValue('id', 0);
        $status = (int)postValue('status', 0);

        $data = array();
        $data['status'] = $status;

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment_ebooking";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        $this->biz_shipment_ebooking_model->update($id, $data);
        
        return jsonEcho(array('code' => 0, 'msg' => lang('审核成功')));
    }

    /**
     * 修改状态
     */
    public function update_status(){
        $id = (int)postValue('id', 0);
        $status = (int)postValue('status', 0);
        $return = ['msg'=>'', 'code'=>0];

        if (is_ajax()) {
            $old_row = $this->biz_shipment_ebooking_model->get_one('id', $id);
            if(empty($old_row)) return jsonEcho(array('code' => 1, 'msg' => lang('ebooking不存在')));

            $data = array();
            $data['status'] = $status;
            $query = $this->biz_shipment_ebooking_model->update($id, $data);

            if ($query){
                $return['code'] = 1;
            }else{
                $return['msg'] = '状态更新失败！ error type:mysql';
            }
            //save the operation log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment_ebooking";
            $log_data["key"] = $id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        }else{
            $return['msg'] = '请求方式不允许！ error type:request type';
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }
}
