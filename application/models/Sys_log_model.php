<?php

class Sys_log_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get($where = '', $sort, $order)
    {
        if ($where != '') $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get('sys_log');
        return $query->result_array();
    }

    public function get_where_one($where = '')
    {
        if ($where == '') return array();
        $this->db->where($where);
        $query = $this->db->get('sys_log');
        return $query->row_array();
    }

    public function get_by_id($id = 0)
    {
        if ($id != 0) {
            $this->db->where('id', $id);
            $query = $this->db->get('sys_log');
            return $query->row_array();
        }
    }

    public function total($where = '')
    {
        if ($where != '') $this->db->where($where);
        $this->db->from('sys_log');
        return $this->db->count_all_results('');
    }

    public function save($data = array())
    {
        $time = time();
        $data["insert_time"] = date('Y-m-d H:i:s', $time);
        $data["insert_date"] = date('Y-m-d', $time);
        $data["insert_dtime"] = date('H:i:s', $time);
        $this->db->insert('sys_log', $data);
        return $this->db->insert_id();
    }

    public function update($id = 0, $data = array())
    {
        $this->db->where('id', $id);
        $this->db->update('sys_log', $data);
        return $id;
    }

    public function mdelete($id = '')
    {
        $this->db->where('id', $id);
        $this->db->delete('sys_log');
    }

    public function get_table_name($master_table_name = '', $master_key = '')
    {
        $this->db->select('table_name');
        $this->db->where('master_table_name', $master_table_name);
        $this->db->where('master_key', $master_key);
        $this->db->group_by('table_name');
        $query = $this->db->get('sys_log');
        return $query->result_array();
    }

    public function get_log_handle_by_field($field = "", $table_name = 'biz_consol', $key = 0)
    {
        $sql = "select sys_log.id,GROUP_CONCAT(field) as fields from sys_log LEFT JOIN sys_log_handle on sys_log_handle.sys_log_id = sys_log.id where action = 'update' and table_name = '$table_name' and `key` = $key and value like '%\"$field\"%' GROUP BY sys_log.id order by sys_log.id desc ";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    public function no_role_get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where, null, false);
        $this->db->order_by($sort, $order, false);
        $query = $this->db->get('sys_log');
        return $query->result_array();
    }
}