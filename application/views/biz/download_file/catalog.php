<style>
    td{
        font-size: 14px;
    }
    .hide{
        display: none;
    }
</style>
<link rel="stylesheet" href="/inc/third/layui/css/layui.css" media="all">
<body>
<br>
<a href='javascript:void(0);' onclick='javascript:history.go(-1);'> <?= lang('返回上一页');?> </a>
<?php if(is_admin()){ ?>
    <button type="button" class="easyui-linkbutton" data-options="iconCls:'icon-add'" onclick='addNode()'> <?= lang('添加节点');?> </button>
<?php } ?>
<span style="color:#79a1d7;font-weight: bold;font-size: 16px;line-height: 20px;vertical-align: middle;margin-left: 20px"><?= lang('搜索条件：');?></span>
<input id="search" class="easyui-textbox" style="vertical-align: middle;" value="<?=$search?>">
<a id="btn"  class="easyui-linkbutton" data-options="iconCls:'icon-search'" style="vertical-align: middle;"><?= lang('搜索');?></a>
<br><br>
<table  class="layui-table" border='0' cellpadding='4' cellspacing='1' bgcolor='#9999FF'>
    <thead>
    <tr bgcolor='#FFFFFF'>
        <th><?= lang('船公司');?></th>
        <th><?= lang('文件名');?></th>
        <th><?= lang('更新人');?></th>
        <th><?= lang('更新时间');?></th>
        <th><?= lang('下载');?></th>
        <th><?= lang('更新');?></th>
        <?php if(is_admin()){ ?>
            <th><?= lang('删除');?></th>
        <?php } ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($rows as $row):?>
        <tr bgcolor='#FFFFFF'>
            <td>
                <?php if(is_admin()){ ?>
                    <a href="/biz_download_file/catalog?pid=<?= $row["id"]; ?>">
                        <?= $parentNode["file_name"]; ?>
                    </a>
                <?php }else{ ?>
                    <?= $parentNode["file_name"]; ?>
                <?php } ?>
            </td>
            <td>
                <?= $row["file_name"]; ?>
                <?php if(strtotime('-2 day') <= strtotime($row['updated_time'])){ ?>
                    <span class="c-text c-text-new opr-toplist1-label_3Mevn" style="background-color: #FF455B;display: inline-block;padding: 0 2px;text-align: center;vertical-align: middle;font-style: normal;color: #fff;overflow: hidden;line-height: 16px;height: 16px;font-size: 12px;border-radius: 4px;font-weight: 200;margin-left: 6px;white-space: nowrap;"><?= lang('新');?></span>
                <?php } ?>
            </td>
            <td><?= $row["updated_by_name"]; ?></td>
            <td><?= $row["updated_time"]; ?></td>
            <td>
                <?php if($row['lft'] + 1 == $row['rgt']){ ?>
                    <a href='/download?file_path=/upload/download_file<?= $row["file_path"]; ?>' target='_blank'><img src="/inc/third/desktop/images/download.png?v=1" style="width:30px;height:30px;"></a>
                <?php }else{ ?>
                    <a href="/biz_download_file/catalog?pid=<?= $row["id"]; ?>">
                        <img src='/inc/third/desktop/images/folder.png?v=1' style='width:30px;height:30px;'>
                    </a>
                <?php } ?>
            </td>
            <td>
                <a href='javascript:void(0);' onclick="openwin(<?php echo $row["id"]?>)"><img src="/inc/third/desktop/images/upload.png" style="width:30px;height:30px;"></a>
            </td>
            <?php if(is_admin()){ ?>
                <td>
                    <a href='javascript:void(0);' onclick="del(<?php echo $row["id"]?>)"><?= lang('DEL');?></a>
                </td>
            <?php } ?>
        </tr>
    <?php endforeach;?>
    </tbody>

</table>
<div id="w" class="easyui-window" title="<?= lang('更新');?>" data-options="iconCls:'icon-save',closed:'true'" style="width:500px;height:200px;padding:10px;">
    <form id="uploadForm" method="post" enctype="multipart/form-data">
        <div style="margin-bottom:20px">
            <div><?= lang('更新');?>:</div>
            <input type="hidden" id="file_id" name="file_id" value="">
            <input id="file2" type="file" name="file" style="width:100%">
        </div>
    </form>
    <div>
        <button class="easyui-linkbutton" onclick="upload()" style="width:100%">Upload</button>
    </div>

</div>
<div id="addNodeDiv" class="easyui-window" title="添加节点" data-options="iconCls:'icon-save',closed:'true'" style="width:400px;height:250px;padding:10px;">
    <form id="addNodeForm" method="post" >
        <div style="margin-bottom:20px">
            <input type="hidden" name="parent_id" value="<?= $parentNode['id'];?>">
            <?= lang('节点名称');?>:
            <input class="easyui-textbox" name="file_name" id="addNodeFileName" required>
            <input type="hidden" name="file_path" id="addNodeFilePath">
            <select class="easyui-combobox" name="file_type" data-options="
                onSelect:add_file_type_select,
                editable:false,
            ">
                <option value="file"><?= lang('文件');?></option>
                <option value="dir"><?= lang('文件夹');?></option>
            </select>
            <script>
                function add_file_type_select(rec){
                    // if(rec.value == '文件'){
                    //     $('.fileDiv').removeClass('hide');
                    // }else{
                    //     $('.fileDiv').addClass('hide');
                    // }
                }
            </script>
        </div>
    </form>
    <div>
        <button class="easyui-linkbutton" onclick="addNodeSave()" style="width:100%"><?= lang('保存');?></button>
    </div>
</div>
<script>

    $(function(){
        $('#btn').bind('click', function(){
            var search=$('#search').textbox('getValue');
            window.location.href='/biz_download_file/catalog?pid=<?= $pid; ?>&search='+search;
        });
    });

    function upload(){
        var formData = new FormData($('#uploadForm')[0]);
        $.ajax({
            type: 'post',
            url: "/biz_download_file/upload", //上传文件的请求路径必须是绝对路
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            dataType:'json'
        }).success(function (data) {

            $('#w').window('close');
            $.messager.alert('info', data.msg);

        }).error(function () {
            $('#w').window('close');
            $.messager.alert('info', data.msg);
        });

    }

    function del(file_id){
        $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认删除该节点');?>', function (r) {
            if(r){
                $.ajax({
                    type: 'post',
                    url: "/biz_download_file/deleteNode", //上传文件的请求路径必须是绝对路
                    data: {
                        id:file_id
                    },
                    success:function (res_json) {
                        try {
                            var res = $.parseJSON(res_json);
                            $.messager.alert('<?= lang('提示');?>',res.msg);
                            if(res.code == 0){
                                location.reload();
                            }else{

                            }
                        }catch (e) {
                            $.messager.alert('<?= lang('提示');?>',res_json);
                        }
                    },
                });
            }
        })
    }

    function openwin(id){
        $("#file_id").val(id);

        $('#w').window('open');
    }

    //批量新增
    function addNode() {
        $('#addNodeDiv').window('open');
    }

    var is_submit = false;
    function addNodeSave() {
        if(is_submit){
            return;
        }
        $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认提交？');?>', function(r) {
            if (r) {
                is_submit = true;
                ajaxLoading();
                var this_window = $('#addNodeDiv');
                var form = $('#addNodeForm');
                this_window.window('close');

                form.form('submit', {
                    url: '/biz_download_file/addChildNode',
                    onSubmit: function (param) {


                        var validate = $(this).form('validate');
                        if (!validate) {
                            ajaxLoadEnd();
                            is_submit = false;
                            this_window.window('open');
                        }
                        return validate;
                    },
                    success: function (res_json) {
                        var res;
                        ajaxLoadEnd();
                        is_submit = false;
                        try {
                            res = $.parseJSON(res_json);
                            $.messager.alert('<?= lang('Tips')?>', res.msg);
                            if (res.code == 0) {
                                location.reload();
                            } else {

                            }
                        } catch (e) {
                            this_window.window('open');
                            $.messager.alert('Tips', res_json);
                        }
                    }
                });
            }
        });
    }
</script>
</body>
