<!doctype html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="/inc/css/normal.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/icon.css?v=3">
<link href="/inc/third/layui/css/layui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.easyui.min.js?v=11"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.edatagrid.js?v=5"></script>
<script type="text/javascript" src="/inc/js/easyui/datagrid-detailview.js"></script>
<script type="text/javascript" src="/inc/js/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/inc/js/other.js?v=1"></script>
<script type="text/javascript" src="/inc/js/set_search.js?v=5"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.easyui.ext.js?v=53"></script>
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<title>待分配</title>
<style type="text/css">
    .f {
        width: 115px;
    }

    .s {
        width: 100px;
    }

    .v {
        width: 200px;
    }

    .del_tr {
        width: 23.8px;
    }

    .this_v {
        display: inline;
    }

    .input {
        width: 200px;
    }

    .select {
        width: 210px;
    }

    .textarea {
        width: 200px;
        height: 60px;
    }

    .query_f {
        padding-left: 15px;
        color: #1E9FFF;
    }

    .query_s {
        padding-left: 5px;
        color: #5FB878;
    }

    .query_v {
        padding-left: 5px;
        color: #FF5722;
    }

    /**
    * 修改datagrid表格样式
    **/
    .datagrid-body td {height: 40px !important;}
    .datagrid-header, .datagrid-htable {height: 40px !important; background: #eceff0; border-bottom: none;}
    .datagrid-header-row td{border-width: 0px 1px 1px 0px; border-style: solid; border-color: #ddd;}
    .panel-body-noheader {border-top:0px !important;}
    .tabs {padding-left:0px;}
    .tabs-inner {height: 25px !important;}
    .tabs li.tabs-selected a.tabs-inner {border-bottom: 1px solid #F4F4F4;}
    .tabs-header {background: none;}
    .tabs li a.tabs-inner{padding: 0px 20px;}
    .tabs li.tabs-selected a.tabs-inner{background: #F4F4F4}
    .datagrid-row-over {background: #eceeeb;}
    .datagrid-header td.datagrid-header-over {background:#e5e7e8;color: #000000;cursor: default;}
    .layui-layer-lan .layui-layer-title {background: #325763 !important;}
    .datagrid .panel-body {border:none;}
    #cx table tr td {height: 30px; min-width: 50px;}
    #cx table tr td button {width: 23px;}
</style>
<!--这里是查询框相关的代码-->
<script>
    var query_option = {};//这个是存储已加载的数据使用的
    var table_name = 'biz_crm_public',//需要加载的表名称
        view_name = 'biz_crm_public_view';//视图名称
    var add_tr_str = "";//这个是用于存储 点击+号新增查询框的值, load_query_box会根据接口进行加载
    var url = '/biz_crm/unassigned';
    var ids_str = '';

    $(function () {
        load_query_box();
        $('#cx').on('keydown', 'input', function (e) {
            if (e.keyCode == 13) {
                doSearch();
            }
        });
    });
    /**
     * 加载查询框
     */
    function load_query_box(){
        ajaxLoading();
        $.ajax({
            type:'GET',
            url: '/sys_config_title/get_user_query_box?view_name=' + view_name + '&table_name=' + table_name,
            dataType:'json',
            success:function (res) {
                ajaxLoadEnd();
                if(res.code == 0){
                    //加载查询框
                    $('#cx table').append(res.data.box_html);
                    //需要调用下加载器才行
                    $.parser.parse($('#cx table tbody .query_box'));

                    add_tr_str = res.data.add_tr_str;
                    query_option = res.data.query_option;
                    var table_columns = [[
                        {field:'ck', checkbox:true},
                    ]];//表的表头

                    //填充表头信息
                    $.each(res.data.table_columns, function (i, it) {
                        //读取列配置,然后这里进行合并
                        var column_config;
                        try {
                            column_config = JSON.parse(it.column_config,function(k,v){
                                if(v && typeof v === 'string') {
                                    return v.indexOf('function') > -1 || v.indexOf('FUNCTION') > -1 ? new Function(`return ${v}`)() : v;
                                }
                                return v;
                            });
                        }catch (e) {
                            column_config = {};
                        }
                        var this_column_config = {field:it.table_field, title:it.title, width:it.width, sortable: it.sortable, formatter: get_function(it.table_field + '_for'), styler: get_function(it.table_field + '_styler')};
                        //合并一下
                        $.extend(this_column_config, column_config);

                        table_columns[0].push(this_column_config);
                    });


                    //渲染表头
                    $('#tt').datagrid({
                        columns:table_columns,
                        width: 'auto',
                        height: $(window).height(),
                        url: url,
                        pageSize:20,
                        onDblClickRow: function (index, row) {
                            url = '/biz_crm/edit/' + row.id;
                            window.open(url);
                        }
                    });
                    $(window).resize(function () {
                        $('#tt').datagrid('resize');
                    });

                    //为了避免初始加载时, 重复加载的问题,这里进行了初始加载ajax数据
                    var aj = [];
                    $.each(res.data.load_url, function (i,it) {
                        aj.push(get_ajax_data(it, i));
                    });
                    //加载完毕触发下面的下拉框渲染
                    //$_GET 在other.js里封装
                    var this_url_get = {};
                    $.each($_GET, function (i, it) {
                        this_url_get[i] = it;
                    });
                    var auto_click = this_url_get.auto_click;

                    $.when.apply($,aj).done(function () {
                        //这里进行字段对应输入框的变动
                        $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                            var ec_value = $(ec_item).combobox('getValue');
                            var v_inp = $('.v:eq(' + ec_index + ')');
                            var s_val = $('.s:eq(' + ec_index + ')').combobox('getValue');
                            var v_val = v_inp.textbox('getValue');
                            //如果自动查看,初始值为空
                            if(auto_click === '1') v_val = '';

                            $(ec_item).combobox('clear').combobox('select', ec_value);
                            //这里比对get数组里的值
                            $.each(this_url_get, function (trg_index, trg_item) {
                                //切割后,第一个是字段,第二个是符号
                                var trg_index_arr = trg_index.split('-');
                                //没有第二个参数时,默认用等于
                                if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';
                                //用一个删一个
                                //如果当前的选择框的值,等于get的字段值
                                if(ec_value === trg_index_arr[0] && s_val === trg_index_arr[1]){
                                    v_val = trg_item;//将v改为当前get的
                                    delete this_url_get[trg_index];
                                }
                            });
                            //没找到就正常处理
                            $('.v:eq(' + ec_index + ')').textbox('setValue', v_val);
                        });
                        //判断是否有自动查询参数
                        var no_query_get = ['auto_click'];//这里存不需要塞入查询的一些特殊变量
                        $.each(no_query_get, function (i,it) {
                            delete this_url_get[it];
                        });
                        //完全没找到的剩余值,在这里新增一个框进行选择
                        $.each(this_url_get, function (trg_index, trg_item) {
                            add_tr();//新增一个查询框
                            var trg_index_arr = trg_index.split('-');
                            //没有第二个参数时,默认用等于
                            if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';

                            $('#cx table tbody tr:last-child .f').combobox('setValue', trg_index_arr[0]);
                            $('#cx table tbody tr:last-child .s').combobox('setValue', trg_index_arr[1]);
                            $('#cx table tbody tr:last-child .v').textbox('setValue', trg_item);
                        });

                        //这里自动执行查询,显示明细
                        if(auto_click === '1'){
                            doSearch();
                        }
                    });
                }else{
                    $.messager.alert("<?= lang('提示');?>", res.msg);
                }
            },error:function (e) {
                ajaxLoadEnd();
                $.messager.alert("<?= lang('提示');?>", e.responseText);
            }
        });
    }

    /**
     * 新增一个查询框
     */
    function add_tr() {
        $('#cx table tbody').append(add_tr_str);
        $.parser.parse($('#cx table tbody tr:last-child'));
        var last_f = $('#cx table tbody tr:last-child .f');
        var last_f_v = last_f.combobox('getValue');
        last_f.combobox('clear').combobox('select', last_f_v);
        return false;
    }

    /**
     * 删除一个查询
     */
    function del_tr(e) {
        //删除按钮
        var index = $(e).parents('tr').index();
        $(e).parents('tr').remove();
        return false;
    }

    function setValue(jq, val) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combobox !== undefined){
            return $(jq).combobox('setValue',val);
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('setValue',val);
        }else if(data.datebox !== undefined){
            return $(jq).datebox('setValue',val);
        }else if(data.textbox !== undefined){
            return $(jq).textbox('setValue',val);
        }
    }

    function getValue(jq) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if (data.combobox !== undefined) {
            return $(jq).combobox('getValue');
        } else if (data.datetimebox !== undefined) {
            return $(jq).datetimebox('getValue');
        } else if (data.datebox !== undefined) {
            return $(jq).datebox('getValue');
        } else if (data.textbox !== undefined) {
            return $(jq).textbox('getValue');
        }
    }

    //模板相关--start
    function buttons_for(value, row, index) {
        var str = "";
        str += '<a href="javascript:void(0);" onclick="deliver(' + index + ')">转交至</a>';
        return str;
    }
    //模板相关--end

    var ids = []
    //selectAll和这个函数差不多,改的时候记得一起改
    function doSearch(url = '') {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if (json.hasOwnProperty(item.name) === true) {
                if (typeof json[item.name] == 'string') {
                    json[item.name] = [json[item.name]];
                    json[item.name].push(item.value);
                } else {
                    json[item.name].push(item.value);
                }
            } else {
                json[item.name] = item.value;
            }
        });

        //清空显示的查询条件
        $('#query').html('');

        var cx_data2 = cx_data
        $.each(cx_data2, function (index, item) {
            if (item.value == 'client.company_name') {
                cx_data2[index++] = {name: 'field[name][]', value: '客户全称'};
            }
            if (item.value == 'client.client_name') {
                cx_data2[index++] = {name: 'field[name][]', value: '客户简称'};
            }
            if (item.value == 'client.country_code') {
                cx_data2[index++] = {name: 'field[name][]', value: '国家代码'};
            }
            if (item.value == 'client.city') {
                cx_data2[index++] = {name: 'field[name][]', value: '市'};
            }
            if (item.value == 'client.company_address') {
                cx_data2[index++] = {name: 'field[name][]', value: '客户地址'};
            }
            if (item.value == 'crm.export_sailing') {
                cx_data2[index++] = {name: 'field[name][]', value: '出口航线'};
            }
            if (item.value == 'crm.trans_mode') {
                cx_data2[index++] = {name: 'field[name][]', value: '运输方式'};
            }
            if (item.value == 'crm.product_type') {
                cx_data2[index++] = {name: 'field[name][]', value: '品名大类'};
            }
            if (item.value == 'crm.product_details') {
                cx_data2[index++] = {name: 'field[name][]', value: '品名大类描述'};
            }
            if (item.value == 'crm.client_source') {
                cx_data2[index++] = {name: 'field[name][]', value: '客户来源'};
            }
            if (item.value == 'crm.created_by') {
                cx_data2[index++] = {name: 'field[name][]', value: '创建人'};
            }
            if (item.value == 'crm.close_plan_date') {
                cx_data2[index++] = {name: 'field[name][]', value: '结束时间'};
            }
            if (item.value == 'crm.created_time') {
                cx_data2[index++] = {name: 'field[name][]', value: '创建时间'};
            }
            if (item.name == 'field[v][]') {
                if (item.value == '') {
                    cx_data2[index] = {name: 'field[v][]', value: '无查询条件'};
                }
            }
        });
        $.each(cx_data2, function (index, item) {
            if (item.name == 'f_role') {
                if (item.value != '') {
                    $("#query").append('<span class="query_f">' + item.value + '</span>')
                }
            }
            //判断如果name存在,且为string类型
            if (item.name == 'field[name][]') {
                $("#query").append('<span class="query_f">' + item.value + '</span>')
            }
            if (item.name == 'field[s][]') {
                $("#query").append('<span class="query_s">' + item.value + '</span>')
            }
            if (item.name == 'field[v][]') {
                $("#query").append('<span class="query_v">' + item.value + '</span>')
            }

        });

        var opt = $('#tt').datagrid('options');
        if (url == '') {
            opt.url = '/biz_crm/unassigned';
        } else {
            opt.url = url;
        }

        $('#tt').datagrid('load', json).datagrid('clearSelections');
        set_config();
        $('#select_ids').val('');
        $('#chaxun').window('close');
    }

    function resetSearch(){
        $.each($('.v'), function(i,it){
            setValue($(it), '');
        });
    }

    function doSearch_all() {
        var url = '/biz_crm/unassigned';
        doSearch(url);
    }
</script>
<body>
<table id="tt" style="width:1100px;" rownumbers="false" pagination="true" autoRowHeight="false" idField="id" pagesize="30" toolbar="#tb" singleSelect="false" nowrap="true"></table>
<div id="tb" style="padding:5px;">
    <div style="display:flex;display:-webkit-flex;">
        <div style="flex: 1;">
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');">查询</a>
            <?php
            //只有田诗雯和销售经理可以转交客户
            if(in_array(get_session('id'), $leader_id)):
                ?>
                <a href="javascript:void(0);" class="easyui-linkbutton" plain="true" onclick="batch_deliver()">批量转交</a>
            <?php endif; ?>
            <div id="query" style="display:inline-block"></div>
        </div>
    </div>
</div>

<div id="title_tabs" style="hidden" value=""></div>
<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <form id="cx">
        <table></table>
        <br><br>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="doSearch_all();">查询</button>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="resetSearch();">重置</button>
        <a href="javascript:void;" class="easyui-tooltip" data-options="
            content: $('<div></div>'),
            onShow: function(){
                $(this).tooltip('arrow').css('left', 20);
                $(this).tooltip('tip').css('left', $(this).offset().left);
            },
            onUpdate: function(cc){
                cc.panel({
                    width: 500,
                    height: 'auto',
                    border: false,
                    href: '/bsc_help_content/help/query_condition'
                });
            }
        ">help</a>
    </form>
</div>

<div id="batch_deliver_div" style="display: none;">
    <form class="layui-form" lay-filter="myform" action="">
        <div class="layui-form-item" style="margin: 20px 30px 0px 0px;">
            <label class="layui-form-label">销售名单</label>
            <div class="layui-input-block">
                <select name="sale_id" id="sale_id" lay-search lay-verify="required" lay-reqText="请选择销售">
                    <option value="">请选择销售</option>
                    <?php
                    foreach ($sales as $user) {
                        $postion = $user['m_level'] == 6 ? '[销售经理]' : '';
                        echo "<option value=\"{$user['id']}\">{$user['name']}&nbsp;&nbsp;{$postion}</option>";
                        foreach ($user['child'] as $item) {
                            echo "<option value=\"{$item['id']}\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;┊┈┈&nbsp;&nbsp;{$item['name']}</option>";
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="layui-form-item" style="margin: 20px 30px 0px 0px;">
            <label class="layui-form-label">结束日期</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input" lay-verify="required" id="end_date" name="end_date" value="">
            </div>
        </div>
        <button id="batch_deliver_btn" style="display: none;" lay-submit lay-filter="*">提交</button>
    </form>
</div>
<script type="text/javascript"src="/inc/third/layui/layui.js"></script>
<script>
    layui.use(['form','laydate'], function(){
        var form = layui.form;
        var laydate = layui.laydate;
        //绑定日期控件
        laydate.render({
            elem: '#end_date'
        });

        //空格转换
        form.on('select', function(data){
            //layui的渲染方法可以做到无缝转换，但是不能解决右键时的BUG
            form.render();
            //解决右键BUG就用下面的代码强制转换为空格
            $('body').click(function () {
                $('.layui-form-select .layui-select-title>.layui-input').each(function () {
                    var _this = this;
                    setTimeout(function () {
                        var val = $(_this).val();
                        var new_val = val.replace(/\&nbsp\;/g, ' ')
                        if (new_val !== val) {
                            $(_this).val(new_val)
                        }
                    }, 100)
                })
            })
        });

        //提交表单
        form.on('submit(*)', function(data){
            var loading = layer.load(1);
            var sale_id = $('#sale_id').val();
            var end_date = $('#end_date').val();
            data.field.ids = ids_str;
            $.ajax({
                type: 'post',
                url: '/biz_crm/batch_deliver_client',
                data: {ids: ids_str, sale_id: sale_id, end_date: end_date},
                success: function (data) {
                    layer.close(loading);
                    if (data.code == 1) {
                        layer.close(batch_deliver_window);
                        //刷新父窗口（CRM客户）easyUI的datagrid控件
                        parent.$('#tt').datagrid('reload');
                        layer.alert('转交成功', {icon: 1, closeBtn:0}, function (index) {
                            //刷新easyUI的datagrid控件
                            $('#tt').datagrid('reload').datagrid('clearSelections');
                            //清空表单
                            form.val("myform", { "sale_id": "","end_date": ""});
                            layer.close(index);
                        });
                    } else {
                        layer.alert(data.msg, {icon: 5});
                    }
                },
                error: function (data) {
                    layer.close(loading);
                    layer.alert('请求失败，错误代码：' + data.status, {icon: 5});
                },
                dataType: "json"
            });
            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        });
    });

    //批量转交
    function batch_deliver() {
        var tt = $('#tt');
        var rows = tt.datagrid('getSelections');
        if (rows.length > 0) {
            var ids = [];
            $.each(rows, function (i, it) {
                ids.push(it.id);
            });
            ids_str = ids.join(',');
            batch_deliver_window = layer.open({
                id: 'to-sales',
                type: 1,
                title: '批量转交销售',
                area: '330',
                offset: '20%',
                shade: 0.5,
                content: $('#batch_deliver_div'),
                zIndex: 9999,
                btn: ['保存', '取消'],
                success: function (layero, index) {
                    $('#to-sales').css('overflow', 'unset');
                },
                yes: function (e, layero) {
                    $('#batch_deliver_btn').click();
                }
            });
        } else {
            layer.msg("No Item Selected", {icon:2});
        }
    }
</script>
</body>
<input type="hidden" id="select_ids">
