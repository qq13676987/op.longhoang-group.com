
<!doctype html> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="/inc/css/normal.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/icon.css?v=3">
<script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.easyui.min.js?v=11"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.edatagrid.js?v=5"></script>
<script type="text/javascript" src="/inc/js/easyui/datagrid-detailview.js"></script> 
<script type="text/javascript" src="/inc/js/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/inc/js/other.js?v=1"></script>
<script type="text/javascript" src="/inc/js/set_search.js?v=5"></script>
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<style type="text/css">
    .f{
        width:115px;
    }
    .s{
        width:100px;
    }
    .v{
        width:200px;
    }
    .del_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }
    .input{
        width: 200px;
    }
    .select{
        width: 210px;
    }
</style>
<script type="text/javascript">
    function doSearch() {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] = [json[item.name]];
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        //group的手动拼接进去 这里因为暂时没想到其他办法,先这样做了
        // var group_tree = $('#group').combotree('tree').tree('getChecked');
        // var group_val = [];
        // $.each(group_tree, function (i, it) {
        //     group_val.push(it.group_code);
        // });
        // json.group = group_val.join(',');
        
        $('#tt').datagrid('load', json).datagrid('clearSelections');
        set_config();
        $('#chaxun').window('close');
    }
    var ajax_data = {};
    var setting = {
        'operator_ids':['operator', 'id', 'name']
        ,'customer_service_ids':['customer_service', 'id', 'name']
        ,'sales_ids':['sales', 'id', 'name']
        ,'marketing_ids':['marketing', 'id', 'name']
        ,'oversea_cus_ids':['oversea_cus', 'id', 'name']
        ,'booking_ids':['booking', 'id', 'name']
        ,'document_ids':['document', 'id', 'name']
        ,'finance_ids':['finance', 'id', 'name']
    };

    var datebox = ['created_time', 'updated_time'];
 
    var datetimebox = [];
    $(function () {
        var selectIndex = -1;
        $('#tt').edatagrid({
            url: '/biz_crm/get_inactive_data/',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            }
        });
        
        $('#cx').on('keydown', 'input', function (e) {
            if(e.keyCode == 13){
                doSearch();
            }
        });
        
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height()-40,
            onDblClickRow: function (rowIndex) {
                $(this).datagrid('selectRow', rowIndex);
                var row = $('#tt').datagrid('getSelected');
                if (row) {
                    url = '/biz_client/edit_inactive/' + row.id;
                    window.open(url);
                }
            },
            onClickRow: function (index, data) {
                if (index == selectIndex) {
                    $(this).datagrid('unselectRow', index);
                    selectIndex = -1;
                } else {
                    selectIndex = index;
                }
            }
        });
        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
               //新增查询列
        $('#cx table').on('click' , '.add_tr', function () {
            var index = $('.f').length;
            var str = '<tr>' +
                '                <td>\n' +
                '                    查询\n'+
                '                </td>\n'+
                '                <td align="right">' +
            '                       <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="\n' +
                '                            onChange:function(newVal, oldVal){\n' +
                '                                var index = $(\'.f\').index(this);\n' +
                '                                if(setting.hasOwnProperty(newVal) === true){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                    $(\'.v:eq(\' + index + \')\').combobox({\n' +
                '                                        data:ajax_data[setting[newVal][0]],\n' +
                '                                        valueField:setting[newVal][1],\n' +
                '                                        textField:setting[newVal][2],\n' +
                '                                    });\n' +
                '                                }else if ($.inArray(newVal, datebox) !== -1){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datebox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }else if ($.inArray(newVal, datetimebox) !== -1){\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-datetimebox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }else{\n' +
                '                                    $(\'.v:eq(\' + index + \')\').textbox(\'destroy\');\n' +
                '                                    $(this).siblings(\'.this_v\').append(\'<input name=\\\'field[v][]\\\' class=\\\'v easyui-textbox\\\' >\');\n' +
                '                                    $.parser.parse($(\'.v:eq(\' + index + \')\').parents(\'.this_v\'));\n' +
                '                                }\n' +
                '                            }\n' +
                '                        ">\n' +
                '<option value=\'id\'>id</option><option value=\'client_code\'>客户代码</option><option value=\'client_name\'>客户简称</option><option value=\'company_name\'>客户全称</option><option value=\'province\'>省</option><option value=\'city\'>市</option><option value=\'area\'>区</option><option value=\'sailing_area\'>航线区域</option><option value=\'company_address\'>客户地址</option><option value=\'contact_name\'>客户联系人</option><option value=\'contact_live_chat\'>客户QQ</option><option value=\'contact_telephone\'>手机号(网络账号)</option><option value=\'contact_telephone2\'>客户座机</option><option value=\'contact_email\'>客户邮箱</option><option value=\'contact_position\'>contact_position</option><option value=\'finance_name\'>财务联系人</option><option value=\'finance_live_chat\'>财务QQ</option><option value=\'finance_telephone\'>财务电话</option><option value=\'finance_email\'>财务邮箱</option><option value=\'finance_payment\'>付款方式</option><option value=\'finance_od_basis\'>超期依据</option><option value=\'finance_payment_month\'>finance_payment_month</option><option value=\'finance_payment_days\'>付款天数</option><option value=\'finance_payment_day_th\'>finance_payment_day_th</option><option value=\'credit_amount\'>授信额度</option><option value=\'finance_status\'>财务状态</option><option value=\'finance_remark\'>财务备注</option><option value=\'finance_sign_company_code\'>finance_sign_company_code</option><option value=\'bank_name_cny\'>银行(CNY)</option><option value=\'bank_account_cny\'>银行帐号(CNY)</option><option value=\'bank_address_cny\'>银行地址(CNY)</option><option value=\'bank_swift_code_cny\'>银行swift(CNY)</option><option value=\'bank_name_usd\'>银行(USD)</option><option value=\'bank_account_usd\'>银行帐号(USD)</option><option value=\'bank_address_usd\'>银行地址(USD)</option><option value=\'bank_swift_code_usd\'>银行swift(USD)</option><option value=\'taxpayer_name\'>纳税人名称</option><option value=\'taxpayer_id\'>纳税人识别号</option><option value=\'taxpayer_address\'>纳税人地址</option><option value=\'taxpayer_telephone\'>纳税人电话</option><option value=\'deliver_info\'>快递地址</option><option value=\'website_url\'>企业网址</option><option value=\'source_from\'>信息来源</option><option value=\'role\'>角色</option><option value=\'remark\'>备注</option><option value=\'sales_id\'>管理人</option><option value=\'read_user_group\'>read_user_group</option><option value=\'created_by\'>创建人</option><option value=\'created_time\'>创建时间</option><option value=\'updated_by\'>修改人</option><option value=\'updated_time\'>修改时间</option><option value=\'client_level\'>client_level</option><option value=\'tag\'>标签</option><option value=\'country_code\'>国家代码</option><option value=\'invoice_email\'>invoice_email</option><option value=\'stop_date\'>stop_date</option><option value=\'dn_accounts\'>dn_accounts</option><option value=\'operator_ids\'>operator_ids</option><option value=\'customer_service_ids\'>customer_service_ids</option><option value=\'sales_ids\'>sales_ids</option><option value=\'marketing_ids\'>marketing_ids</option><option value=\'oversea_cus_ids\'>oversea_cus_ids</option><option value=\'booking_ids\'>booking_ids</option><option value=\'document_ids\'>document_ids</option><option value=\'finance_ids\'>finance_ids</option>' +
                '                    </select>\n' +
                '                    &nbsp;\n' +
                '\n' +
                '                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="\n' +
                '                        ">\n' +
                '                        <option value="like">like</option>\n' +
                '                        <option value="=">=</option>\n' +
                '                        <option value=">=">>=</option>\n' +
                '                        <option value="<="><=</option>\n' +
                '                        <option value="!=">!=</option>\n' +
                '                    </select>\n' +
                '                    &nbsp;\n' +
                '                    <div class="this_v">\n' +
                '                        <input name="field[v][]" class="v easyui-textbox" >\n' +
                '                    </div>\n' +
                '                    <button class="del_tr" type="button">-</button>' +
                '   </td>' +
                '</tr>';
            $('#cx table tbody').append(str);
            $.parser.parse($('#cx table tbody tr:last-child'));
            return false;
        });
        //删除按钮
        $('#cx table').on('click' , '.del_tr', function () {
            var index = $(this).parents('tr').index();
            $(this).parents('tr').remove();
            return false;
        });
        var a1 = ajax_get('/bsc_user/get_data_role/operator', 'operator');
        var a2 = ajax_get('/bsc_user/get_data_role/customer_service', 'customer_service');
        var a3 = ajax_get('/bsc_user/get_data_role/sales', 'sales');
        var a4 = ajax_get('/bsc_user/get_data_role/marketing', 'marketing');
        var a5 = ajax_get('/bsc_user/get_data_role/oversea_cus', 'oversea_cus');
        var a6 = ajax_get('/bsc_user/get_data_role/booking', 'booking');
        var a7 = ajax_get('/bsc_user/get_data_role/document', 'document');
        var a8 = ajax_get('/bsc_user/get_data_role/finance', 'finance');
        $.ajax({
            type: 'GET',
            url: '/sys_config/get_config_search/crm_customer_inactive',
            dataType: 'json',
            success:function (res) {
                // var config = $.parseJSON(res['data']);
                var config = res.data;
                if(config.length > 0){
                    var count = config[0];
                    $('.f:last').combobox('setValue', config[1][0][0]);
                    $('.s:last').combobox('setValue', config[1][0][1]);
                    for (var i = 1; i < count; i++){
                        $('.add_tr:eq(0)').trigger('click');
                        $('.f:last').combobox('setValue', config[1][i][0]);
                        $('.s:last').combobox('setValue', config[1][i][1]);
                    }
                }
            }
        });

        $.when(a1,a2,a3,a4,a5,a6,a7,a8).done(function () {
            $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                var ec_value = $(ec_item).combobox('getValue');
                $(ec_item).combobox('clear').combobox('select', ec_value);
            });
        });
    });
    
    function ajax_get(url,name) {
        return $.ajax({
            type:'POST',
            url:url,
            dataType:'json',
            success:function (res) {
                ajax_data[name] = res;
            },
            error:function () {
                // $.messager.alert('获取数据错误');
            }
        })
    }
    
    function set_config() {
        var f_input = $('.f');
        var count = f_input.length;
        var config = [];
        $.each(f_input, function (index, item) {
            var c_array = [];
            c_array.push($('.f:eq(' + index + ')').combobox('getValue'));
            c_array.push($('.s:eq(' + index + ')').combobox('getValue'));
            config.push(c_array);
        });
        // var config_json = JSON.stringify(config);
        $.ajax({
            type: 'POST',
            url: '/sys_config/save_config_search',
            data:{
                table: 'crm_customer_inactive',
                count: count,
                config: config,
            },
            dataType:'json',
            success:function (res) {

            },
        });
    }
    
    var is_submit = false;
    
    function deliver(index) {
        var tt = $('#tt');
        tt.datagrid('selectRow', index);
        var row = tt.datagrid('getSelected');
        if (row != null){
            $('#deliver_div').window('open');
    
            $('#deliver_fm').form('clear');
            var from_data = {};
            from_data.client_code = row.client_code;
            from_data.is_keep_sales = 0;
            $('#from_user_id').combobox('loadData', row.sales);
            if(row.sales.length == 1){
                $('#from_user_id').combobox('setValue', row.sales[0]['id']);
            }
            $('#deliver_fm').form('load', from_data);
        }else{
            $.messager.alert("Tips","No Item Selected");
        }
    }

    function deliver_save() {
        if(is_submit){
            return;
        }
        $.messager.confirm('确认对话框', '是否确认提交？', function(r) {
            if (r) {
                is_submit = true;
                ajaxLoading();
                var this_window = $('#deliver_div');
                var form = $('#deliver_fm');
                form.form('submit', {
                    url: '/biz_crm/deliver_client_inactve',
                    onSubmit: function (param) {
                        param.is_inactve = 'true';
                        var validate = $(this).form('validate');
                        if (!validate) {
                            ajaxLoadEnd();
                            is_submit = false;
                        }
                        return validate;
                    },
                    success: function (res_json) {
                        var res;
                        try {
                            res = $.parseJSON(res_json);
                        } catch (e) {
                        }
                        if (res == undefined) {
                            ajaxLoadEnd();
                            is_submit = false;
                            $.messager.alert('Tips', '发生错误');
                            return;
                        }
                        this_window.window('close');
                        ajaxLoadEnd();
                        is_submit = false;
                        if (res.code == 0) {
                            $.messager.alert('<?= lang('Tips')?>', res.msg);
                            $('#tt').datagrid('reload').datagrid('clearSelections');
                        } else {
                             $.messager.alert('<?= lang('Tips')?>', res.msg, 'info', function(r){
                                this_window.window('open');
                            });
                        }

                    }
                });
            }
        });
    }
    
    function batch_deliver() {
        var tt = $('#tt');
        var rows = tt.datagrid('getSelections');
        if (rows.length > 0){
            var client_codes = [];
            $.each(rows, function(i, it) {
                client_codes.push(it.client_code);
            });
            var client_codes_str = client_codes.join(',');

            $('#batch_deliver_div').window('open');
            $('#batch_deliver_fm').form('clear');
            var from_data = {};
            from_data.client_codes = client_codes_str;
            $('#batch_deliver_fm').form('load', from_data);
        }else{
            $.messager.alert("Tips","No Item Selected");
        }
    }

    function batch_deliver_save() {
        if(is_submit){
            return;
        }
        $.messager.confirm('确认对话框', '是否确认提交？', function(r) {
            if (r) {
                is_submit = true;
                ajaxLoading();
                var this_window = $('#batch_deliver_div');
                var form = $('#batch_deliver_fm');
                form.form('submit', {
                    url: '/biz_crm/batch_deliver_client_inactve',
                    onSubmit: function (param) {
                        param.is_inactve = 'true';
                        var validate = $(this).form('validate');
                        if (!validate) {
                            ajaxLoadEnd();
                            is_submit = false;
                        }
                        return validate;
                    },
                    success: function (res_json) {
                        var res;
                        try {
                            res = $.parseJSON(res_json);
                        } catch (e) {
                        }
                        if (res == undefined) {
                            ajaxLoadEnd();
                            is_submit = false;
                            $.messager.alert('Tips', res);
                            return;
                        }
                        this_window.window('close');
                        ajaxLoadEnd();
                        is_submit = false;
                        if (res.code == 0) {
                            $.messager.alert('<?= lang('Tips')?>', res.msg);
                            $('#tt').datagrid('reload').datagrid('clearSelections');
                        } else {
                            $.messager.alert('<?= lang('Tips')?>', res.msg, 'info', function(r){
                                this_window.window('open');
                            });
                        }

                    }
                });
            }
        });
    }

    function multiple() {
        var opts = $('#tt').datagrid('options');
        opts.singleSelect = !opts.singleSelect;
        $('#tt').datagrid('reload');
    }
    
    function buttons_for(value, row, index){
        var str = "";
        str += '<a href="javascript:void(0);" onclick="deliver(' + index + ')">转交至</a>'
        return str;
    }

    //格式化最新日报
    function new_log_date(value, row, index){
        var str = "";
        if (value != null) {
            str += '<a href="javascript:void(0);" onclick="open_log(' + index + ')"><img style="height: 12px; width: 12px;" src="/inc/menu/folder.png"> ' + value + '</a>';
        }
        return str;
    }

    //打开最新日报列表
    function open_log(index) {
        var data = $('#tt').datagrid('getData').rows[index];
        var url = '/biz_client_follow_up_log/index/'+data.client_code;
        layer.open({
            type: 2,
            title:'管理日报',
            area: ['1355px', '600px'],
            shadeClose:true,
            shade:0.5,
            content:url,
        });
    }

    /**
     * 扔进公海
     */
    function set_public() {
        var ids = [];
        var rows = $('#tt').datagrid('getSelections');
        if (rows.length == 0){
            layer.alert('请先选中数据！', {icon:5, title:'系统消息'});
            return;
        }else{
            $.each(rows, function (index, item) {
                ids.push(item.id);
            });
        }

        layer.confirm('“扔进公海”表示主动放弃客户归属权，此操作不可逆！', {icon: 3, title:'系统消息'}, function(index){
            loading = layer.load(1);
            $.ajax({
                url: '/biz_crm/set_public/',
                type: 'POST',
                data: {ids:ids},
                dataType: 'json',
                success: function (data) {
                    layer.close(loading);
                    if (data.code == 1) {
                        layer.msg('操作成功，已扔进公海。', {icon: 1, shade: [0.3, '#393D49']},function (e) {
                            $('#tt').datagrid('reload');
                            layer.close(e);//关闭窗口
                        });
                    } else {
                        layer.alert('操作失败：' + data.msg, {icon: 5}, function (e) {
                            layer.close(e);//关闭窗口
                        });
                    }
                },
                error: function (xhr, status, error) {
                    layer.close(loading);
                    layer.alert('请求失败！', {icon: 5, title: '系统消息'});
                }
            });
        });
    }
</script>
<div style="padding-bottom: 10px;color: red">最近3个月无成交记录的客户，需要领导转交给某个销售之后，才能进行群发重新开发！</div>
<table id="tt" style="width:1000px;height:450px" rownumbers="false" pagination="true" idField="id" pagesize="30" toolbar="#tb" singleSelect="true" nowrap="true">
    <thead>
        <tr>
            <th data-options="field:'ck',checkbox:true"></th>
            <th data-options="field:'id',width:50" sortable="true">ID</th>
            <th field="tag" width="111" sortable="true">客户标签</th>
            <th field="client_code" width="100" sortable="true">客户代码</th>
            <th field="company_name" width="200" sortable="true">客户全称</th>
            <th field="province" width="100" sortable="true">省</th>
            <th field="city" width="100" sortable="true">市</th>
            <th field="company_address" width="120" sortable="true">客户地址</th>
            <th field="export_sailing" width="80" sortable="true">出口航线</th>
            <th field="trans_mode" width="80" sortable="true">运输模式</th>
            <th field="product_type" width="80" sortable="true">品名大类</th>
            <th field="client_source" width="80" sortable="true">客户来源</th>
            <th field="last_shipment_time" width="120" sortable="true">最近成交</th>
            <th field="new_log_date" width="150" sortable="true" formatter="new_log_date">最新日报</th>
            <th field="log_num" width="70" align="center" sortable="true">跟进次数</th>
            <th field="sales_names" width="111">当前销售</th>
            <th field="buttons" width="80" formatter="buttons_for">转交操作</th>
        </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
                   onclick="add();">新增</a>-->
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');">查询</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="multiple()">多选</a>
                <a href="javascript:void(0);" class="easyui-linkbutton" plain="true" onclick="batch_deliver()">批量转交</a>
                <a href="javascript:void(0);" class="easyui-linkbutton" plain="true" onclick="set_public()">扔进公海</a>
            </td>
        </tr>
    </table>
</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <form id="cx">
        <table>
           <!-- <tr>
                <td>部门</td>
                <td>
                    <select class="easyui-combotree" id="group" style="width: 475px" data-options="
                        multiple:true,
                        url:'/bsc_user/getCompanyTree',
                    ">
                    </select>
                </td>
            </tr>-->
            <tr>
                <td>
                    角色
                </td>
                <td align="right">
                    <select class="easyui-combobox" id="f_role" name="f_role" style="width: 475px"
                    data-options="
                        valueField:'value',
                        textField:'value',
                        url:'/bsc_dict/get_option/role',
                    ">
    
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    查询
                </td>
                <td align="right">
                    <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="
                            onChange:function(newVal, oldVal){
                                var index = $('.f').index(this);
                                if(setting.hasOwnProperty(newVal) === true){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                    $('.v:eq(' + index + ')').combobox({
                                        data:ajax_data[setting[newVal][0]],
                                        valueField:setting[newVal][1],
                                        textField:setting[newVal][2],
                                    });
                                }else if ($.inArray(newVal, datebox) !== -1){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datebox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }else if ($.inArray(newVal, datetimebox) !== -1){
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datetimebox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }else{
                                    $('.v:eq(' + index + ')').textbox('destroy');
                                    $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
                                    $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
                                }
                            }
                        ">
                        <option value='id'>id</option><option value='client_code'>客户代码</option><option value='client_name'>客户简称</option><option value='company_name'>客户全称</option><option value='province'>省</option><option value='city'>市</option><option value='area'>区</option><option value='sailing_area'>航线区域</option><option value='company_address'>客户地址</option><option value='contact_name'>客户联系人</option><option value='contact_live_chat'>客户QQ</option><option value='contact_telephone'>手机号(网络账号)</option><option value='contact_telephone2'>客户座机</option><option value='contact_email'>客户邮箱</option><option value='contact_position'>contact_position</option><option value='finance_name'>财务联系人</option><option value='finance_live_chat'>财务QQ</option><option value='finance_telephone'>财务电话</option><option value='finance_email'>财务邮箱</option><option value='finance_payment'>付款方式</option><option value='finance_od_basis'>超期依据</option><option value='finance_payment_month'>finance_payment_month</option><option value='finance_payment_days'>付款天数</option><option value='finance_payment_day_th'>finance_payment_day_th</option><option value='credit_amount'>授信额度</option><option value='finance_status'>财务状态</option><option value='finance_remark'>财务备注</option><option value='finance_sign_company_code'>finance_sign_company_code</option><option value='bank_name_cny'>银行(CNY)</option><option value='bank_account_cny'>银行帐号(CNY)</option><option value='bank_address_cny'>银行地址(CNY)</option><option value='bank_swift_code_cny'>银行swift(CNY)</option><option value='bank_name_usd'>银行(USD)</option><option value='bank_account_usd'>银行帐号(USD)</option><option value='bank_address_usd'>银行地址(USD)</option><option value='bank_swift_code_usd'>银行swift(USD)</option><option value='taxpayer_name'>纳税人名称</option><option value='taxpayer_id'>纳税人识别号</option><option value='taxpayer_address'>纳税人地址</option><option value='taxpayer_telephone'>纳税人电话</option><option value='deliver_info'>快递地址</option><option value='website_url'>企业网址</option><option value='source_from'>信息来源</option> <option value='remark'>备注</option><option value='sales_id'>管理人</option><option value='read_user_group'>read_user_group</option><option value='created_by'>创建人</option><option value='created_time'>创建时间</option><option value='updated_by'>修改人</option><option value='updated_time'>修改时间</option><option value='client_level'>client_level</option><option value='tag'>标签</option><option value='country_code'>国家代码</option><option value='invoice_email'>invoice_email</option><option value='stop_date'>stop_date</option><option value='dn_accounts'>dn_accounts</option><option value='operator_ids'>operator_ids</option><option value='customer_service_ids'>customer_service_ids</option><option value='sales_ids'>sales_ids</option><option value='marketing_ids'>marketing_ids</option><option value='oversea_cus_ids'>oversea_cus_ids</option><option value='booking_ids'>booking_ids</option><option value='document_ids'>document_ids</option><option value='finance_ids'>finance_ids</option>                    </select>
                    &nbsp;

                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="
                        ">
                        <option value="like">like</option>
                        <option value="=">=</option>
                        <option value=">=">>=</option>
                        <option value="<="><=</option>
                        <option value="!=">!=</option>
                    </select>
                    &nbsp;
                    <div class="this_v">
                        <input name="field[v][]" class="v easyui-textbox" >
                    </div>
                    <button class="add_tr" type="button">+</button>
                </td>
            </tr>
        </table>
        <br><br>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="doSearch();">查询</button>
        <a href="javascript:void;" class="easyui-tooltip" data-options="
            content: $('<div></div>'),
            onShow: function(){
                $(this).tooltip('arrow').css('left', 20);
                $(this).tooltip('tip').css('left', $(this).offset().left);
            },
            onUpdate: function(cc){
                cc.panel({
                    width: 500,
                    height: 'auto',
                    border: false,
                    href: '/bsc_help_content/help/query_condition'
                });
            }
        ">help</a>
    </form>
</div>

<div id="deliver_div" class="easyui-window" title="转交销售" style="width:320px;height:187px;padding:5px;" data-options="closed:true,modal:true,border:'thin'">
    <form id="deliver_fm" method="post">
        <input type="hidden" name="client_code">
        <table style="text-align: right">
            <tr>
                <td>转交给</td>
                <td>
                    <select class="easyui-combobox select" name="to_user_id" id="to_user_id" required data-options="
                        valueField:'id',
						textField:'name',
						url:'/bsc_user/get_data_role/sales/?leader_users=1',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td>结束指派日期</td>
                <td>
                    <input class="easyui-datebox input" name="close_plan_date" id="close_plan_date" required>
                </td>
            </tr>
            <tr style="text-align: right">
                <td colspan="2"><button type="button" onclick="deliver_save()" style="width: 100px"><?= lang('submit');?></button></td>
            </tr>
        </table>
    </form>
</div>

<div id="batch_deliver_div" class="easyui-window" title="批量转交销售" style="width:320px;height:127px;padding:5px;" data-options="closed:true,modal:true,border:'thin'">
    <form id="batch_deliver_fm" method="post">
        <input type="hidden" name="client_codes">
        <table style="text-align: right">
            <tr>
                <td>转交给</td>
                <td>
                    <select class="easyui-combobox select" name="to_user_id" required data-options="
                        valueField:'id',
						textField:'name',
						url:'/bsc_user/get_data_role/sales/?leader_users=1',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td>结束指派日期</td>
                <td>
                    <input class="easyui-datebox input" name="close_plan_date" id="close_plan_date" required>
                </td>
            </tr>
            <tr style="text-align: right">
                <td colspan="2"><button type="button" onclick="batch_deliver_save()" style="width: 100px"><?= lang('submit');?></button></td>
            </tr>
        </table>
    </form>
</div>
<!--弹窗-->
<div id="_window" style="display: none; overflow:hidden;">
    <iframe scrolling="auto" id='openIframe' frameborder="0"  src="" style="width:100%;height:100%; overflow: hidden"></iframe>
</div>