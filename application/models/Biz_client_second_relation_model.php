<?php

class biz_client_second_relation_model extends CI_Model
{
    private $table = 'biz_client_second_relation';
    public function __construct()
    {
        $this->load->database();
    }

    /**
     * 获取当前客户相关联的开票客户
     * @param $client_code string 客户code
     */
    public function get_client_second_relation($client_code){
        $this->db->where($this->table . '.client_code', $client_code);

        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    /**
     * 获取当前客户和关联客户的关联数据
     * @param $client_code string 客户代码
     * @param $relation_client_code string 关联客户代码
     */
    public function get_one_client_second_relation($client_code, $relation_client_code){
        $this->db->where($this->table . '.client_code', $client_code);
        $this->db->where($this->table . '.relation_client_code', $relation_client_code);

        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);

        $this->db->order_by($sort, $order);

        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function total($where = "")
    {
        if ($where != "") $this->db->where($where);

        $this->db->from($this->table);
        return $this->db->count_all_results('');
    }

    public function get_one($field, $value){
        $this->db->where($field, $value);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function save($client_code, $relation_client_code)
    {
        $data = array();
        $data['client_code'] = $client_code;
        $data['relation_client_code'] = $relation_client_code;

        $this->db->insert($this->table, $data);
        $new_id = $this->db->insert_id();

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_client_second_relation";
        $log_data["key"] = $new_id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        return $new_id;
    }

    public function mdelete($id = '')
    {
        // delete containershipment
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
}