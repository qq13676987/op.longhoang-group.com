<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>CRM--export</title>
    <link type="text/css" rel="stylesheet" href="/inc/third/layui/css/layui.css">
    <style>
        .layui-table-view{margin: 0px; overflow: hidden}
    </style>
</head>
<body style="overflow: hidden">
<div class="layui-card-body" style="padding: 0px; overflow: auto">
    <script type="text/html" id="toolbarDemo">
        <div class="layui-btn-container">
            <div class="layui-inline" title="<?= lang('筛选列');?>">
                <i class="layui-icon" style="font-size: 12px; color: #d35400"><?= lang('Tips：点击右侧两个按钮进行筛选和导出');?></i>
            </div>
        </div>
    </script>

    <table class="layui-hide" id="test-table-toolbar" lay-filter="test-table-toolbar"></table>

</div>
<script src="/inc/third/layui/layui.all.js" charset="utf-8"></script>
<script>
    layui.use(['table', 'layer', 'jquery'], function () {
        var layer = layui.layer, table = layui.table, $ = layui.jquery;
        var params = {};
        var cx_data = parent.$('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if (params.hasOwnProperty(item.name) === true) {
                if (typeof params[item.name] == 'string') {
                    params[item.name] = [params[item.name]];
                    params[item.name].push(item.value);
                } else {
                    params[item.name].push(item.value);
                }
            } else {
                params[item.name] = item.value;
            }
        });

        table.render({
            elem: '#test-table-toolbar'
            , toolbar:'#toolbarDemo'
            , url: '/biz_crm/get_export_data'
            , title: '<?= lang('CRM抬头数据表');?>'
            , height: '760px'
            , method: 'post'
            , where: params
            , cols: [[
                <?php
                foreach ($table_col as $item):
                ?>
                {field: '<?=$item['table_field'];?>', <?php if ($item['table_field']=='company_name') echo "fixed: 'left',";?> title: '<?= lang($item['title']);?>', width: <?=$item['width'];?>},
                <?php endforeach;?>
            ]]
            , page: true
            , limit: 20
            , limits:[20,50,100,500,1000]
            , defaultToolbar: ['filter', 'exports']
        });
    });
</script>
</body>
</html>