<?php


class Biz_port extends Menu_Controller
{
    protected $menu_name = 'DictData';//菜单权限对应的名称
    protected $method_check = array(//需要设置权限的方法
        'index',
        'add_data',
        'update_data',
        'delete_data',
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_port_model');
        $this->model = $this->biz_port_model;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }


    public $field_edit = array();

    public $field_all = array(
        array('id', 'id', '80', '1', 'sortable="true"'),
        array('port_code', 'port_code', '100', '4', 'editor="textbox"'),
        array('port_name', 'port_name', '300', '3', 'editor="textbox"'),
        array('terminal', 'terminal', '100', '6', 'editor="textbox" sortable="true"'),
        array('port_name_cn', 'port_name_cn', '100', '5', 'editor="textbox"'),
        array('sailing', 'sailing', '200', '6', "data-options=\"editor:{type:'combobox',options:{valueField:'sailing',
			textField:'sailing',
			url:'/biz_port/get_sailing',
			onSelect: function(res){
                var ed = $('#tt').datagrid('getEditor', {index:0,field:'sailing_area'});
                $(ed.target).textbox('setValue', res.sailing_area);
            },
			}},\""),
        array('sailing_area', 'sailing_area', '100', '6', "editor=\"textbox\""),
        array('carrier', 'carrier', '150', '6', "data-options=\"editor:{type:'combobox',options:{valueField:'client_code',
			textField:'client_name',
			url:'/biz_client/get_option/carrier',
			onHidePanel: function() {
				var valueField = $(this).combobox('options').valueField;
				var val = $(this).combobox('getValue');
				var allData = $(this).combobox('getData');
				var result = true;
				for (var i = 0; i < allData.length; i++) {
					if (val == allData[i][valueField]) {
						result = false;
					}
				}
				if (result) {
					$(this).combobox('clear');
				}
			},
			}},\""),
		array('inner_code', 'inner_code', '150', '6', "data-options=\"editor:{type:'combobox',options:{valueField:'port_code',
			textField:'port_name',
			url:'/biz_port/get_option?carrier=inner_use',
			onHidePanel: function() {
				var valueField = $(this).combobox('options').valueField;
				var val = $(this).combobox('getValue');
				var allData = $(this).combobox('getData');
				var result = true;
				for (var i = 0; i < allData.length; i++) {
					if (val == allData[i][valueField]) {
						result = false;
					}
				}
				if (result) {
					$(this).combobox('clear');
				}
			},
			}},\""),
        array('created_time', 'created_time', '100', '6', 'sortable="true"'),
    );

    public function index($carrier = '')
    {
        echo lang('暂停维护');
        return;
        $data = array();

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_port_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }
        // page account
        $rs = $this->sys_config_model->get_one('biz_port_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }
        $data["carrier"] = $carrier;
        $this->load->view('head');
        $this->load->view('biz/port/index_view', $data);
    }

    public function get_data($carrier = '')
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = array();
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($v1 != "") $where[] = search_like($f1, $s1, $v1);
        if ($v2 != "") $where[] = search_like($f2, $s2, $v2);
        if ($v3 != "") $where[] = search_like($f3, $s3, $v3);
        if($carrier != "") $where[] = "carrier = '$carrier'";
        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $this->db->select('*, (select client_name from biz_client where client_code = carrier) as carrier_name');
        $rs = $this->model->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {

            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data($carrier = '')
    {
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? strtoupper(trim(ts_replace($_POST[$item]))) : '';
            $data[$item] = strtoupper($temp);
        }
        if(empty($data["port_code"])){
            $data['isError'] = true;
            $data['msg'] = '港口代码未填写';
            echo json_encode($data);
            return;
        }
        $res = check_banjiao($data["port_code"]); 
        if(!empty($res[0])){
            $data['isError'] = true;
            $data['msg'] = '港口代码有非法字符';
            echo json_encode($data);
            return; 
        }
        if(strlen($data["port_code"])!=5 && strlen($data["port_code"])!=3){
            $data['isError'] = true;
            $data['msg'] = '港口代码长度有误';
            echo json_encode($data);
            return;
        }
        $res = check_banjiao($data["port_name"]); 
        if(!empty($res[0])){
            $data['isError'] = true;
            $data['msg'] = '港口名称有非法字符';
            echo json_encode($data);
            return; 
        }
        if($carrier != '')$data['carrier'] = $carrier;
        if(empty(array_filter($data))){
            $data['isError'] = true;
            $data['msg'] = '未填写任何值';
            echo json_encode($data);
            return;
        }
        //2021-06-30 五字代码航线区域必填
        if(empty($data['carrier'])) $data['carrier'] = 'inner_use';
        
        $carrier = isset($data['carrier']) ? $data['carrier'] : 'inner_use';
        if(strlen($data['port_code']) == 5){
            if(empty($data['sailing_area'])){
                $data['isError'] = true;
                $data['msg'] = '五字代码航线区域必填';
                echo json_encode($data);
                return;
            }
            $this->db->where('carrier', $carrier);
        }
        $terminal = isset($data['terminal']) ? $data['terminal'] : '';
        $port_code = isset($data['port_code']) ? $data['port_code'] : '';
        $port_name = isset($data['port_name']) ? $data['port_name'] : '';
        $old_code_row = $this->biz_port_model->get_where_one("port_code = '$port_code' and terminal = '$terminal' and carrier = '$carrier'"); 
        if(!empty($old_code_row)){
            $data['isError'] = true;
            $data['msg'] = 'code重复';
            echo json_encode($data);
            return;
        }
        
        $old_code_row = $this->biz_port_model->get_where_one("port_name = '$port_name' and carrier = 'inner_use'"); 
        if(empty($old_code_row) && $carrier!="inner_use"){
            $data['isError'] = true;
            $data['msg'] = '缺少通用五字代码，请先添加1个不属于任何船公司的五字代码，再添加船公司专属的五字代码';
            echo json_encode($data);
            return;
        }
        
        //随便查一个港口代码一样的港口,名称必须一致
        if($data['carrier'] != 'XGALQC01'){//esl不限制,因为很多码头是港口全称的
            $old_row = $this->biz_port_model->get_where_one("port_code = '{$data['port_code']}'");
            if(!empty($old_row) && $old_row['port_name'] != $data['port_name']){
                $data['isError'] = true;
                $data['msg'] = '名称不能出现多个,请修改为:' . $old_row['port_name'];
                echo json_encode($data);
                return;
            }
            //如果非ESL的话,这里根据当前港口名称,查询
            //获取内部库是否有对应的港口
            if($data['carrier'] !== 'inner_use'){
                $inner_use_row = $this->biz_port_model->get_where_one("port_name = '{$data['port_name']}' and carrier = 'inner_use'");
                if(!empty($old_row)){
                    $data['inner_code'] = $inner_use_row['port_code'];
                }else{
                    $data['inner_code'] = '';
                }
                // if(empty($old_row)){
                //     //没有自动新增一个
                //     $inner_use_data = $data;
                //     $inner_use_data['carrier'] = 'inner_use';
                   
                //     $inner_use_id = $this->model->save($inner_use_data);
                   
                //   // record the log
                //     $log_data = array();
                //     $log_data["table_name"] = "biz_port";
                //     $log_data["key"] = $inner_use_id;
                //     $log_data["action"] = "insert";
                //     $log_data["value"] = json_encode($inner_use_data);
                //     log_rcd($log_data);
                    
                    // $data['inner_code'] = $inner_use_data['port_code'];
                // }else{
                    // $data['inner_code'] = $inner_use_row['port_code'];
                // }
            }
        }else{
            //如果是ESL的话,inner_code必填
            if(empty($data['inner_code'])){
                $data['isError'] = true;
                $data['msg'] = 'ESL 内部港口必选';
                echo json_encode($data);
                return;
            }
            if(strlen($data['inner_code']) != 5){
                $data['isError'] = true;
                $data['msg'] = '未正确选中港口,保存失败';
                echo json_encode($data);
                return;
            }
        }
        
        $id = $this->model->save($data);
        $data['id'] = $id;

        echo json_encode($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_port";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function update_data()
    {
        $id = $_REQUEST["id"];
        $old_row = $this->model->get_one('id', $id);

        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? strtoupper(trim(ts_replace($_POST[$item]))) : '';
            if ($old_row[$item] != $temp)
                $data[$item] = $temp;
        }
        if(!empty($data["port_name"])){
            $res = check_banjiao($data["port_name"]); 
            if(!empty($res[0])){
                $data['isError'] = true;
                $data['msg'] = '港口名称有非法字符';
                echo json_encode($data);
                return; 
            } 
        }   
        if(!empty($data["port_code"])){
            $res = check_banjiao($data["port_code"]); 
            if(!empty($res[0])){
                $data['isError'] = true;
                $data['msg'] = '港口代码有非法字符';
                echo json_encode($data);
                return; 
            }
            if(strlen($data["port_code"])!=5 && strlen($data["port_code"])!=3){
                $data['isError'] = true;
                $data['msg'] = '港口代码长度有误';
                echo json_encode($data);
                return;
            }
        }   
        if(isset($data['port_code']) || isset($data['terminal']) || isset($data['carrier']))
        {
            $carrier = isset($data['carrier']) ? $data['carrier'] : $old_row['carrier'];
            $terminal = isset($data['terminal']) ? $data['terminal'] : $old_row['terminal'];
            $port_code = isset($data['port_code']) ? $data['port_code'] : $old_row['port_code'];
            $old_code_row = $this->biz_port_model->get_where_one("port_code = '$port_code' and terminal = '$terminal' and carrier = '$carrier' and id != $id"); 
            if(!empty($old_code_row)){
                $data['isError'] = true;
                $data['msg'] = 'code重复';
                echo json_encode($data);
                return;
            }
        }
        //2021-06-30 五字代码航线区域必填
        $port_code = isset($data['port_code']) ? $data['port_code'] : $old_row['port_code'];
        if(strlen($port_code) == 5){
            $sailing = isset($data['sailing']) ? $data['sailing'] : $old_row['sailing'];
            $sailing_area = isset($data['sailing_area']) ? $data['sailing_area'] : $old_row['sailing_area'];
            if(empty($sailing) || empty($sailing_area)){
                $data['isError'] = true;
                $data['msg'] = '五字代码航线和航线区域必填';
                echo json_encode($data);
                return;
            }
        }
        if(isset($data['carrier']) && empty($data['carrier'])) $carrier = 'inner_use';
        $carrier = isset($data['carrier']) ? $data['carrier'] : $old_row['carrier'];
        if(isset($data['port_name']) && $carrier != 'XGALQC01'){//esl不限制
            $port_name = $data['port_name'];
            
            //随便查一个港口代码一样的港口,名称必须一致
            $old_row = $this->biz_port_model->get_where_one("port_code = '{$port_code}' and id != $id");
            if(!empty($old_row) && $old_row['port_name'] != $port_name){
                $data['isError'] = true;
                $data['msg'] = '名称不能出现多个,请修改为:' . $old_row['port_name'];
                echo json_encode($data);
                return;
            } 
        }
        $id = $this->model->update($id, $data);
        $data['id'] = $id;
        echo json_encode($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_port";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->model->get_one('id', $id);
        $this->model->mdelete($id);

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_port";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
        return jsonEcho(array('code' => 0, 'msg' => '删除成功'));
    }

    public function delete_datas(){
        $ids = $_REQUEST['ids'];
        $ids_array = array_filter(explode(',', $ids));
        if(empty($ids_array)) return jsonEcho(array('code' => 1, 'msg' => '请选择一行后再试'));

        $delete_datas = array();

        $rs = $this->model->get("id in ('" . join('\',\'', $ids_array) . "')");
        foreach ($rs as $row){
            if(empty($row)){
                continue;
            }

            $delete_datas[] = $row;
        }

        foreach ($delete_datas as $delete_data){
            $id = $delete_data['id'];
            $this->model->mdelete($id);

            //save the operation log
            $log_data = array();
            $log_data["table_name"] = "biz_port";
            $log_data["key"] = $id;
            $log_data["action"] = "delete";
            $log_data["value"] = json_encode($delete_data);
            log_rcd($log_data);
        }
        return jsonEcho(array('code' => 0, 'msg' => '删除成功,已删除' . sizeof($delete_datas) . '条数据'));
    }

    public function get_option()
    {
        $type = isset($_GET['type']) ? $_GET['type'] : 'SEA';
        $carrier = isset($_GET['carrier']) ? trim($_GET['carrier']) : '';
        $limit = isset($_GET['limit']) ? $_GET['limit'] : 'false';
        if(empty($carrier)) $carrier = 'inner_use';

        $port_code = trim(getValue('port_code', ''));
        $port_name = trim(getValue('port_name', ''));
        $terminal = trim(getValue('terminal', ''));
        $port_manager_area = trim(getValue('port_manager_area', ''));

        // $cache_key = "cache_biz_port_get_option_{$type}_{$carrier}";
        // $cache = cache_redis_get($cache_key);
        // if(!empty($cache)){
        //     return jsonEcho($cache);
        // }
        $where = array();

        if(!empty($port_code)) $where[] = "port_code like '%{$port_code}%'";
        if(!empty($port_name)) $where[] = "port_name like '%{$port_name}%'";
        if(!empty($terminal)) $where[] = "terminal like '%{$terminal}%'";
        if(!empty($port_manager_area)) $where[] = "port_manager_area like '%{$port_manager_area}%'";
        if($type == 'AIR'){
            $where[] = "carrier = 'inner_use'";
            $where[] = 'LENGTH(port_code) = 3';
        }else{
            $where[] = "carrier = '$carrier'";
            $where[] = 'LENGTH(port_code) = 5';
        }

        $where_str = join(' and ', $where);
        
        $this->db->select('port_name, port_code, sailing, sailing_area, carrier,terminal,port_manager_area');
        $this->db->where($where_str);
        if($limit === 'true')$this->db->limit(130);
        $rs = $this->model->get_option();
        $rows = array();

        foreach ($rs as $row){
            $row['port_name_long'] = $row['port_code'] . ' | ' . $row['port_name'];
            array_push($rows, $row);
        }

        //2022-07-14 新增判断老的是否存在
        $old = isset($_GET['old']) ? $_GET['old'] : '';
        if(!empty($old)){
            $old_data=$this->db->select('port_name, port_code, sailing, sailing_area, carrier,terminal')->where($where_str)->where('port_code',$old)->get('biz_port')->row_array();
            if(!empty($old_data)){
                //判断是否在原来数组里
                $port_code_arr=array_column($rows,'port_code');
                if(!in_array($old,$port_code_arr)){
                    $rows[] =$old_data;
                }
            }
        }
        
        if(empty($rows)){
            $new_row = array();
            $field = array('carrier','port_code','port_name','terminal','sailing', 'sailing_area','inner_port_code');
            foreach ($field as $val){
                $new_row[$val] = '';
            }
            $new_row['port_name'] = lang('no result');
            $rows[] = $new_row;
        }


        // if($limit === 'true') $rows[] = array(
        //     'port_name' => '当前只显示前30条,请自行搜索匹配',
        //     'port_code' => '',
        //     'sailing' => '',
        //     'sailing_area' => '',
        //     'carrier' => '',
        //     'terminal' => '',
        // );

        // cache_redis_save($cache_key, $rows, 600);
        echo json_encode($rows);
    }

    public function get_sailing(){
        $add_empty = getValue('add_empty' , 'false');
        $this->db->select('sailing, sailing_area');
        $rs = $this->model->get_sailing();
        $rows = array();
        if($add_empty === 'true') $rows[] = array('sailing' => '-', 'sailing_area' => '');

        foreach ($rs as $row){
            $row['sailing_long'] = $row['sailing_area'] . ' | ' . $row['sailing'];
            array_push($rows, $row);
        }
        echo json_encode($rows);
    }

    /**
     * 获取目的港选择项改版为
     * 原本的数据加上内部港口代码,选择时,自动选中内部港口代码
     */
    public function get_discharge(){
        $carrier = trim(getValue('carrier', 'inner_use'));
        $port_code = trim(getValue('port_code', ''));
        $port_name = trim(getValue('port_name', ''));
        $terminal = trim(getValue('terminal', ''));
        $type = isset($_GET['type']) ? $_GET['type'] : 'SEA';

        if(empty($carrier)) $carrier = 'inner_use';

        $where = array();

        if(!empty($port_code)) $where[] = "port_code like '%{$port_code}%'";
        if(!empty($port_name)) $where[] = "port_name like '%{$port_name}%'";
        if(!empty($terminal)) $where[] = "terminal like '%{$terminal}%'";
        $port_code_length = 5;
        if($type == 'AIR') {
            $port_code_length = 3;
            $carrier = 'inner_use';
        }
        $where[] = "carrier = '$carrier'";
        $where[] = 'LENGTH(port_code) = ' . $port_code_length;

        $where = join(' and ', $where);

        $this->db->select("carrier,port_code,port_name,terminal,sailing, sailing_area,inner_code as inner_port_code", false);
        $this->db->limit(30);
        $rows = $this->model->get($where);

        //2022-07-14 新增判断老的是否存在
        $old = isset($_GET['old']) ? $_GET['old'] : '';
        if(!empty($old)){
            if(empty($where)) {
                $where='1=1';
            }
            $old_data=$this->db->select("carrier,port_code,port_name,terminal,sailing, sailing_area,inner_code as inner_port_code", false)->where($where)->where('port_code',$old)->get('biz_port')->row_array();
            if(!empty($old_data)){
                //判断是否在原来数组里
                $port_code_arr=array_column($rows,'port_code');
                if(!in_array($old,$port_code_arr)){
                    $rows[] =$old_data;
                }
            }
        }
        if(empty($rows)){
            $new_row = array();
            $field = array('carrier','port_code','port_name','terminal','sailing', 'sailing_area','inner_port_code');
            foreach ($field as $val){
                $new_row[$val] = '';
            }
            $new_row['port_name'] = 'no result';
            $rows[] = $new_row;
        }

        
        // foreach ($rows as $row){
        //     $new_row = array();
        //     foreach ($row as $key => $val){
        //         $new_row[$key] = '';
        //     }
        //     $new_row['port_name'] = '当前只显示前30条,请自行搜索匹配';
        //     $rows[] = $new_row;
        //     break;
        // }
        //2022-05-13 如果是内部港口,这里将inner_code刷为自己的港口代码
        foreach ($rows as &$row){
            if($carrier == 'inner_use'){
                $row['inner_port_code'] = $row['port_code'];
            }
            //这里加一个saling_lang
            $row['sailing_lang'] = lang($row['sailing']);
        }

        return jsonEcho($rows);
    }
}
