<?php


class Other extends Common_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_model');
    }

    /**
     * 标出当前的特殊字段
     */
    public function ts_text(){
        $ts_str = isset($_POST['ts_str']) ? $_POST['ts_str'] : array();
        $text = isset($_POST['text']) ? $_POST['text'] : "";
        $msg = isset($_POST['msg']) ? $_POST['msg'] : "";

        echo $msg;
        echo '<br/>以下为内容<hr/>';

        foreach ($ts_str as $ts){
            $text = str_replace($ts, '<span style="border:1px solid red;">' . $ts . '</span>', $text);
        }
        echo $text;
    }
    
    
    public function sync_user_commission_rate(){
        echo "111";
    }
    
    /**
     * 强制通过邮箱使用,前提必须是邮箱格式
     */
    public function force_pass_email(){
        $data = array();
        $data['email'] = postValue('email');
        if($_POST){
            if(check_email($data['email'])){
                if(check_mail_valid($data['email'], true)){
                    echo "<script>alert('" . lang('邮箱已手动通过验证') . "'); window.close();</script>";
                    echo "<h3>" . lang('邮箱{email}已通过验证', array('email' => $data['email'])) . "</h3> ";
                }
            }else{
                echo "<script>alert('" . lang('请检测邮箱格式是否正确') . "');</script>";
                echo "<h3>" . lang('邮箱{email}格式不正确,请检查', array('email' => $data['email'])) . "</h3>";
            }
            
        }

        $this->load->view('head');
        $this->load->view('/other/force_pass_email', $data);
    }
}