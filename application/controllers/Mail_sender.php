<?php

/**
 * 邮件通道控制器
 * Class mail_sender
 */
class mail_sender extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->model = Model('mail_sender_model');
        $this->field_all = $this->model->field_all;//相关的ALL
        $this->field_edit = array(); //相关的可编辑字段
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }

        $this->field = [
            'sender_name'=>'通道',
            'smtp_username'=>'邮箱账号',
            'smtp_password'=>'邮箱密码',
            'smtp_host'=>'服务器地址',
            'smtp_port'=>'发送端口'
            ];
    }

    /**
     * @title 列表视图
     */
    public function index(){
        $data = array();
        $this->load->view('head');//引入easyui 头部JS文件等,如果这里完全不同,可以不用引入, 自己script引入即可
        $this->load->view("mail/sender/index_view", $data);//只是额外引入的js文件,直接加在这个对应页面即可
    }

    /**
     * @title 列表数据
     */
    public function get_data(){
        if (is_ajax())
        {
            $data = array('total'=>0, 'rows'=>[]);
            $page = isset($_POST['page']) && (int)$_POST['page'] > 0 ? (int)$_POST['page'] : 1;
            $rows = isset($_POST['rows']) && (int)$_POST['rows'] > 0 ? (int)$_POST['rows'] : 10;

            $count = $this->db->select('count(*) count')->get('mail_sender')->row_array()['count'];
            if ($count > 0)
            {
                $page_total = ceil($count / $rows);
                if ( $page > $page_total ) $page = $page_total;
                $pages = $rows * ($page-1);

                $this->db->select('id, sender_name, smtp_username, smtp_password, smtp_host, smtp_port, created_time, ssl_enable')->limit($rows, $pages);
                $data['total'] = $count;
                $data['rows'] = $this->db->get('mail_sender')->result_array();
            }

            return $this->output->set_content_type('application/json')->set_output(json_encode($data));
        }
    }

    public function get_crm_data(){
        $data = array('total'=>0, 'rows'=>[]);
        $page = isset($_POST['page']) && (int)$_POST['page'] > 0 ? (int)$_POST['page'] : 1;
        $rows = isset($_POST['rows']) && (int)$_POST['rows'] > 0 ? (int)$_POST['rows'] : 30;
        $date = date('Y-m-d');
        $sql = "SELECT
mail_sender.id,
	sender_name,
	smtp_username,
	( SELECT count( 1 ) AS ok FROM crm_promote_mail_log WHERE send_email = mail_sender.smtp_username AND crm_promote_mail_log.STATUS = 1 AND send_time > '{$date}' ) AS smtp_password,
	( SELECT count( 1 ) AS err FROM crm_promote_mail_log WHERE send_email = mail_sender.smtp_username AND crm_promote_mail_log.STATUS = -1 AND send_time > '{$date}' ) AS smtp_host ,
	( SELECT count( 1 ) AS err FROM crm_promote_mail_log WHERE send_email = mail_sender.smtp_username AND crm_promote_mail_log.STATUS = 1 AND send_time > '{$date}' ) * 100 /
	(( SELECT count( 1 ) AS err FROM crm_promote_mail_log WHERE send_email = mail_sender.smtp_username AND crm_promote_mail_log.STATUS in (-1,1) AND send_time > '{$date}' ))
   as smtp_port
FROM
	mail_sender 
WHERE
	sender_name LIKE 'crm%'
	order by smtp_port asc";
        $count = $this->db->query($sql)->num_rows();
        if ($count > 0)
        {
            $page_total = ceil($count / $rows);
            if ( $page > $page_total ) $page = $page_total;
            $pages = $rows * ($page-1);


            $data['total'] = $count;
            $data['rows'] = $this->db->query($sql." limit $pages,$rows")->result_array();
        }

        return jsonEcho($data);
    }

    public function get_crm_detail(){
        $sender_id = isset($_POST['sender_id'])?$_POST['sender_id']:0;
        $date = date('Y-m-d');
        $data = $this->db->query("SELECT id,send_msg,send_time,status FROM crm_promote_mail_log WHERE send_email = (select smtp_username from mail_sender where id = $sender_id) AND status in (-1,1) AND send_time > '{$date}'")->result_array();
        $data = array_map(function($v){
            $v['status'] = $v['status'] == 1?'成功':'失败';
            return $v;
        },$data);
        return jsonEcho(['code'=>1,'data'=>$data]);
    }

    /**
     * 新增方法
     * jsonEcho 封装在helper里, 就是echo 一下json_encode
     */
    public function add_data(){
        $return = ['msg'=>'', 'code'=>0];
        $_post = $this->input->post();
        $this->load->library('form_validation');

        //校验数据
        foreach ($this->field as $key => $val){
            switch($key){
                case 'smtp_username':
                    if ($this->form_validation->valid_email($_post['smtp_username']) === false){
                        $return['msg'] = "邮箱格式错误！";
                        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
                    }
                    break;
                default:
                    if ( array_key_exists($key, $_post) === false || trim($_post[$key]) == '' ){
                        $return['msg'] = "{$val}不能为空！";
                        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
                    }
            }
        }

        $data = [
            'sender_name'   => $_post['sender_name'],
            'smtp_username' => $_post['smtp_username'],
            'smtp_password' => $_post['smtp_password'],
            'smtp_host'     => $_post['smtp_host'],
            'smtp_port'     => $_post['smtp_port'],
            'ssl_enable'    => $_post['ssl_enable'],
            'created_by'    => get_session('id'),
            'created_time'  => date('Y-m-d H:i:s'),
        ];

        if ($this->db->insert('mail_sender', $data)){
            $id = $this->db->query('select MAX(id) as id from mail_sender')->row_array()['id'];
            $return['code'] = 1;
            $return['msg'] = '保存成功';
        }else{
            $return['msg'] = "数据保存失败！";
        }

        // 记录日志的例子
        $log_data = array();
        $log_data["table_name"] = "mail_sender"; //表名
        $log_data["key"] = (int)$id; //主键
        $log_data["action"] = "insert"; //操作
        $log_data["value"] = json_encode($data); //值
        log_rcd($log_data);

        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * 修改数据
     */
    public function update_data(){
        $return = ['msg'=>'', 'code'=>0];
        $_post = $this->input->post();
        $this->load->library('form_validation');

        $id = isset($_post['id']) ? (int)$_post['id'] : 0;
        if ($id == 0){
            $return['msg'] = "参数无效！";
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        foreach ($this->field as $key => $val){
            switch($key){
                case 'smtp_username':
                    if ($this->form_validation->valid_email($_post['smtp_username']) === false){
                        $return['msg'] = "邮箱格式错误！";
                        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
                    }
                    break;
                default:
                    if ( array_key_exists($key, $_post) === false || trim($_post[$key]) == '' ){
                        $return['msg'] = "{$val}不能为空！";
                        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
                    }
            }

        }

        $count = $this->db->select('count(*) as count')->where('id', $id)->get('mail_sender')->row_array()['count'];
        if ($count == 0){
            $return['msg'] = "参数无效！";
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        $data = [
            'id'   => $_post['id'],
            'sender_name'   => $_post['sender_name'],
            'smtp_username' => $_post['smtp_username'],
            'smtp_password' => $_post['smtp_password'],
            'smtp_host'     => $_post['smtp_host'],
            'smtp_port'     => $_post['smtp_port'],
            'ssl_enable'    => $_post['ssl_enable'],
            'updated_by'    => get_session('id'),
            'updated_time'  => date('Y-m-d H:i:s'),
        ];

        if ($this->db->where('id', $id)->update('mail_sender', $data)){
            $return['code'] = 1;
            $return['msg'] = '保存成功';
        }else{
            $return['msg'] = "数据保存失败！";
        }

        // 记录日志的例子
        $log_data = array();
        $log_data["table_name"] = "mail_sender";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data); // 修改的data为变动的数据,没变的不需要
        log_rcd($log_data);

        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * @title 测试通道
     */
    public function testEmail()
    {
        $return = array('msg' => '', 'code' => 0);
        $id = (int)$this->input->get('id');
        if ($id == 0) {
            paramError:
            $return['msg'] = '参数无效！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        $email_account = $this->db->select('*')->where('id', $id)->get('mail_sender')->row_array();
        if (empty($email_account)) {
            goto paramError;
        }

        $this->load->library('Mail');
        $mail = new Mail();
        $mail::$SMTP_host = $email_account['smtp_host'];
        $mail::$SMTP_username = $email_account['smtp_username'];
        $mail::$SMTP_password = $email_account['smtp_password'];
        $mail::$SMTP_port = $email_account['smtp_port'];
        $mail::$sender = array("email"=>$email_account['smtp_username']);
        $mail::$receivers = array("name"=>'leagueshipping',"email"=>'it_lin@leagueshipping.com');//it_lin@leagueshipping.com
        $mail::$reply_by = array("name"=>"leagueshipping","email"=>'it_lin@leagueshipping.com');
        $mail::$subject = '测试邮件发送通道';
        $mail::$body = '这是一封测试邮件，请不要回复！';
        $send_msg = $mail->send_mail();

        if($send_msg){
            $return['code'] = 1;
        }else{
            $return['msg'] = $this->email->print_debugger();
        }

        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     *
     */
    public function testConnect(){
        $smtp_host = postValue('smtp_host', '');
        $smtp_username = postValue('smtp_username', '');
        $smtp_password = postValue('smtp_password', '');
        $smtp_port = postValue('smtp_port', '');
        $ssl_enable = postValue('ssl_enable', '0');

        $this->load->library('Mail');
        $mail = new Mail();
        $mail::$SMTP_host = $smtp_host;
        $mail::$SMTP_username = $smtp_username;
        $mail::$SMTP_password = $smtp_password;
        $mail::$SMTP_port = $smtp_port;

        if($ssl_enable === '0') $is_ssl = false;
        else $is_ssl = true;
        $mail::$is_ssl = $is_ssl;
        try{
            $is_conect = $mail->testSmtpConnect();
            return jsonEcho(array('code' => 0, 'msg' => '连接成功!!!!'));
        }catch (\Exception $e){
            return jsonEcho(array('code' => 1, 'msg' => '信息填写不正确 或 连接超时'));
        }
    }

    /**
     * @title 删除一条
     */
    public function delete_data(){
        $id = isset($_POST['id']) && (int)$_POST['id'] > 0 ? (int)$_POST['id'] : 0;
        $return = array('msg'   => '', 'code'  => 0);
        $data = [];

        if ($id > 0) {
            $data = $this->db->select('*')->where('id', $id)->get('mail_sender')->row_array();
            if (empty($data)){
                $return['msg'] = '参数无效！';
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            $this->db->where('id', $id);
            $query = $this->db->delete('mail_sender');

            if ($query){
                $return['code'] = 1;
            }else{
                $return['msg'] = '删除失败！';
            }
        }else{
            $return['msg'] = '参数无效！';
        }

        // 记录日志的例子
        $log_data = array();
        $log_data["table_name"] = "mail_sender";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($data);//这里得查询下原本的完整数据
        log_rcd($log_data);

        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * 根据当前用户的邮箱,获取绑定的邮箱信息
     */
    public function bind_sender(){
        $email = getUserField(get_session('id'), 'email');
//        $email = "auto@leagueshipping.com";

        $data = array();
        $data['id'] = '';
        $data['sender_name'] = getUserField(get_session('id'), 'name');
        $data['smtp_username'] = $email;
        $data['smtp_password'] = '';
        $data['smtp_host'] = 'smtp.exmail.qq.com';
        $data['smtp_port'] = '465';
        $data['ssl_enable'] = '1';


        $sender = $this->model->get_where_one("smtp_username = '{$email}'");
        if(!empty($sender)){
            $data['id'] = $sender['id'];
            $data['sender_name'] = $sender['sender_name'];
            $data['smtp_username'] = $sender['smtp_username'];
            $data['smtp_password'] = $sender['smtp_password'];
            $data['smtp_host'] = $sender['smtp_host'];
            $data['smtp_port'] = $sender['smtp_port'];
            $data['ssl_enable'] = $sender['ssl_enable'];
        }

        $this->load->view('head');
        $this->load->view('/mail/sender/bind_sender', $data);
    }

    /**
     * 获取用户可选的通道
     */
    public function get_user_sender($type=1){
        // $this_email = getUserField(get_session('id'), 'email');
        $this_email = 'service01@vip.longhoang-group.com';
        if($type == 1){
            $where = array();
            $where[] = "smtp_username in ('{$this_email}')";//auto的暂不开放,再加上他们自己的

            $where = join(' and ' , $where);
        }
        if($type == 2){
            $email = isset($_POST['email'])?$_POST['email']:'';
            if(empty($email)){
                return jsonEcho([]);
            }
            $where = "smtp_username like '%{$email}%'";
        }

        $this->db->select('id,smtp_username,sender_name');
        $rs = $this->model->get($where);
        $rows = array();
        foreach ($rs as $row){
            $rows[] = $row;
        }

        return jsonEcho($rows);
    }
}