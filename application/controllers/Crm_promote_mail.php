<?php

/**
 * 邮件营销
 */
class crm_promote_mail extends Menu_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->this_db = $this->db;
		$this->db = Model('op_model')->get_db();
		$this->load->model('crm_promote_consignee_group_model');
		$this->load->model('crm_promote_consignee_model');
		$this->load->model('crm_promote_mail_model');
		$this->load->model('crm_promote_mail_log_model');
		$this->load->model('bsc_user_model');
		$this->load->model('bsc_group_model');
		$this->load->model('m_model');

	}

	public function index_new()
	{
		$data = array();
		$data['crm_user'] = getValue('crm_user', '');//get_session('id')
		$data['crm_user_type'] = getValue('crm_user_type', '');//user
		$data['crm_date'] = getValue('crm_date', date('Y-m-d'));

		if (empty($data['crm_user_type'])) {
			$data['crm_user_type'] = 'head';
			$data['crm_user'] = 'HEAD';
		}

		if ($data['crm_user_type'] == 'head') {

			$sql = "SELECT count(*) as n FROM `crm_promote_mail_log` where created_time > '" . date("Y-m-d") . "'";
			$total_send = $this->m_model->query_one($sql);
			$data["total_send"] = $total_send["n"];
			$start = date("Y-m-d", time() - 3600 * 24 * 8);
			$sql = "SELECT distinct date(created_time) as d,count(id) as total,count(case when (open_time is not null) then 1 else null end) as rd FROM `crm_promote_mail_log` where created_time > '$start' GROUP BY d";
			$data["rs"] = $this->m_model->query_array($sql);

		} else if ($data['crm_user_type'] == 'sub_company') {
			$total = $this->db->from('crm_promote_mail_log as a')->where('a.created_time >', date("Y-m-d"))->join('crm_promote_mail as b', 'b.id=a.mail_id')->join('bsc_user as c', 'c.id=b.create_by')->join('bsc_group as d', 'd.group_code=c.group')->where('d.company_code', $data['crm_user'])->count_all_results();
			$data["total_send"] = $total;
			$user_company = $data['crm_user'];

			$start = date("Y-m-d", time() - 3600 * 24 * 8);
			$sql = "SELECT distinct date(crm_promote_mail_log.created_time) as d,count(crm_promote_mail_log.id) as total,count(case when (crm_promote_mail_log.open_time is not null) then 1 else null end) as rd FROM `crm_promote_mail_log` JOIN `crm_promote_mail` on crm_promote_mail_log.mail_id = crm_promote_mail.id JOIN `bsc_user` on bsc_user.id = crm_promote_mail.create_by JOIN `bsc_group` on bsc_group.group_code = bsc_user.group where crm_promote_mail_log.created_time > '$start'and bsc_group.company_code ='$user_company' GROUP BY d";
			$data["rs"] = $this->m_model->query_array($sql);


		} else if ($data['crm_user_type'] == 'group') {
			$total = $this->db->from('crm_promote_mail_log as a')->where('a.created_time >', date("Y-m-d"))->join('crm_promote_mail as b', 'b.id=a.mail_id')->join('bsc_user as c', 'c.id=b.create_by')->where('c.group', $data['crm_user'])->count_all_results();
			$data["total_send"] = $total;
			$user_group = $data['crm_user'];

			$start = date("Y-m-d", time() - 3600 * 24 * 8);
			$sql = "SELECT distinct date(crm_promote_mail_log.created_time) as d,count(crm_promote_mail_log.id) as total,count(case when (crm_promote_mail_log.open_time is not null) then 1 else null end) as rd FROM `crm_promote_mail_log` JOIN `crm_promote_mail` on crm_promote_mail_log.mail_id = crm_promote_mail.id JOIN `bsc_user` on bsc_user.id = crm_promote_mail.create_by where crm_promote_mail_log.created_time > '$start'and bsc_user.group ='$user_group' GROUP BY d";
			$data["rs"] = $this->m_model->query_array($sql);


		} else if ($data['crm_user_type'] == 'user') {
			$total = $this->db->from('crm_promote_mail_log as a')->where('a.created_time >', date("Y-m-d"))->join('crm_promote_mail as b', 'b.id=a.mail_id')->where('b.create_by', $data['crm_user'])->count_all_results();
			$data["total_send"] = $total;
			$user_id = $data['crm_user'];

			$start = date("Y-m-d", time() - 3600 * 24 * 8);
			$sql = "SELECT distinct date(crm_promote_mail_log.created_time) as d,count(crm_promote_mail_log.id) as total,count(case when (crm_promote_mail_log.open_time is not null) then 1 else null end) as rd FROM `crm_promote_mail_log` JOIN `crm_promote_mail` on crm_promote_mail_log.mail_id = crm_promote_mail.id  where crm_promote_mail_log.created_time > '$start'and crm_promote_mail.create_by ='$user_id' GROUP BY d";
			$data["rs"] = $this->m_model->query_array($sql);
		}

		$data['user_role'] = $this->get_user_role_tree();
		$this->load->view('head');
		$this->load->view('biz/crm/crm_promote_mail/index_new', $data);
	}


	/**
	 * @return void
	 * 邮件列表
	 */
	public function index()
	{
		// $user = $this->m_model->query_one("select email_edm, signature from bsc_user where id = " . get_session("id"));
		// if (strlen($user["email_edm"]) < 5) exit("<script>alert('" . lang('请先配置推送邮箱,然后刷新') . "');location.href='/bsc_user/person/';</script>");
		// if (strlen($user["signature"]) < 5) exit("<script>alert('" . lang('请再配置邮箱签名,然后刷新') . "');location.href='/bsc_user/person/';</script>");

		//获取全年发送量和已读量
		$sql = "select count(1) as send_count, count(case when (crm_promote_mail_log.open_time != '0000-00-00 00:00:00') then 1 end) as read_count from crm_promote_mail_log left join crm_promote_mail on crm_promote_mail.id = crm_promote_mail_log.mail_id where created_time >= '" . date('Y-01-01 00:00:00') . "' and crm_promote_mail.create_by = " . get_session('id');
		$data['year_send'] = $this->m_model->query_one($sql);

		//获取本月发送量和已读量
		$sql = "select count(1) as send_count, count(case when (crm_promote_mail_log.open_time != '0000-00-00 00:00:00') then 1 end) as read_count from crm_promote_mail_log left join crm_promote_mail on crm_promote_mail.id = crm_promote_mail_log.mail_id where created_time >= '" . date('Y-m-01 00:00:00') . "' and crm_promote_mail.create_by = " . get_session('id');
		$data['month_send'] = $this->m_model->query_one($sql);


		//2022-05-06
		$data['crm_user'] = getValue('crm_user', '');//get_session('id')
		$data['crm_user_type'] = getValue('crm_user_type', '');//user

		$this->load->view('head');
		$group_id = getValue("group_id");
		if($group_id==1){
			$this->load->view('biz/crm/crm_promote_mail/index_public_moban', $data);
		}else{
			$this->load->view('biz/crm/crm_promote_mail/index', $data);
		}
	}

	public function invalid_email()
	{
		echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> <br><br>无效邮箱清单<hr>';
		//这里加入了链接到client里,获取对应的销售
		$sql = "select cpmi.created_time,cpmi.invalid_email,(select name from bsc_user where bsc_user.id = bcc.sales_id) as sales_name from crm_promote_mail_invalid cpmi left join biz_client_contact_list bccl on bccl.email = cpmi.invalid_email left join biz_client_crm bcc on bcc.id = bccl.crm_id limit 2000";
		$rs = $this->m_model->query_array($sql);
		foreach ($rs as $k => $v) {
			if ($v['sales_name'] == NULL) {
				unset($rs[$k]);
			} else {
				echo $v["created_time"] . " : <div style=\"width:48px;display: inline-block;\">" . $v['sales_name'] . "</div> : " . $v["invalid_email"] . "<br>";
			}
		}
	}

	public function mail_log($mail_id)
	{
		$data = array();

		$sql = "select * from crm_promote_mail where id = $mail_id";
		$row = $this->m_model->query_one($sql);
		$data["mail_c"] = $row;

		$sql = "select * from crm_promote_mail_log where mail_id = $mail_id";
		$rs = $this->m_model->query_array($sql);
		$read = 0;
		$send = 0;
		$total = 0;
		foreach ($rs as $k => $v) {
			$total++;
			if (!empty($v["open_time"])) {
				$read++;
			}
			if (!empty($v["status"]) && $v["status"] == 1) {
				$send++;
			}
			/*if (!empty($v["send_time"])&&$v["send_time"]==1) {
				$send++;
			}*/
		}
		$data["read"] = $read;
		$data["send"] = $send;
		$data["total"] = $total;

		$sql = "SELECT
                	crm_promote_mail_log.mail_id,
                	count( 1 ) AS num 
                FROM
                	`crm_promote_mail_log`
                	LEFT JOIN crm_promote_mail ON crm_promote_mail.id = crm_promote_mail_log.mail_id 
                WHERE
                	crm_promote_mail_log.`status` = 0 
                	AND crm_promote_mail.plan_send_time < '".date("Y-m-d H:i:s")."' 
                GROUP BY
                	mail_id 
                order by num asc";
		$rs3 = $this->m_model->query_array($sql);
		$n = 0;
		foreach($rs3 as $r3){
			if($r3['mail_id']==$mail_id) break;
			$n += $r3["num"];
		}
		$data["paidui"] = $n;

		$data["mail_l"] = $rs;
		$this->load->view('head');
		$this->load->view('biz/crm/crm_promote_mail/mail_log', $data);
	}


	/**
	 * 0628 公共模板收件人组改名
	 * @return void
	 */
	public function upGroupName(){
		$data["send_group_name"]=$_POST["send_group"];
		$id=$_POST["id"];
		$re=$this->crm_promote_mail_model->update($id,$data);

		if ($re > 0) {
			// record the log
			$log_data = array();
			$log_data["table_name"] = "crm_promote_mail";
			$log_data["key"] = $id;
			$log_data["action"] = "update";
			$log_data["value"] = json_encode($data);
			log_rcd($log_data);

			$reData['code'] = 200;
			$reData['msg'] = "已更新";
			$reData["send_group"]=$data["send_group_name"];
		}
		echo json_encode($reData, JSON_UNESCAPED_UNICODE);
	}

	public function delete_data()
	{
		$id = $_GET["id"];
		//2022-08-01 模板只能删除自己的
		$old_row = $this->crm_promote_mail_model->get_row("id = '$id'");
		if(empty($old_row)) return jsonEcho(array('code' => 400, 'msg' => '模板不存在'));
		if($old_row['create_by'] !== get_session('id') && !is_admin()) return jsonEcho(array('code' => 400, 'msg' => '创建人不是当前用户,删除失败'));

		$this->crm_promote_mail_log_model->del_by_parent($id);
		$re = $this->crm_promote_mail_model->del($id);

		// record the log
		$log_data = array();
		$log_data["table_name"] = "crm_promote_mail";
		$log_data["key"] = $id;
		$log_data["action"] = "delete";
		$log_data["value"] = json_encode($old_row);
		log_rcd($log_data);

		//2022-08-01 加入日志记录
		if ($re > 0) {
			$reData['code'] = 200;
			$reData['msg'] = "已删除";
		}
		echo json_encode($reData, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * 邮件列表
	 * @return void
	 */
	public function get_data()
	{
		$result = array();
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$offset = ($page - 1) * $rows;
		$where = array();
		//新增状态
		$status = isset($_POST['status']) ? intval($_POST['status']) : '';
		//新增组
		$crm_user = postValue('crm_user', get_session('id'));
		$crm_user_type = postValue('crm_user_type', '');
		$group_id = getValue('group_id');

		if ($crm_user_type == 'user') {
			//2022-01-10 新需求 CRM获取数据逻辑 原创建人 改为 创建人+销售
			//2022-02-23 改版后, crm获取数据,只自己获取自己的
			$where[] = " crm_promote_mail.create_by = '$crm_user' ";
		} else if ($crm_user_type == 'group') {
			$where[] = " ( select `group` from bsc_user where bsc_user.id = crm_promote_mail.create_by ) = '$crm_user' ";
		} else if ($crm_user_type == 'sub_company') {
			$this_crm_user = str_replace('company_', '', $crm_user);
			$where[] = " ( select `group` from bsc_user where bsc_user.id = crm_promote_mail.create_by ) like '$this_crm_user-%' ";
		} else if ($crm_user_type == 'head') {
			if ($crm_user == 'HEAD') {
				Model('bsc_group_model');
				$this->db->select('group_code');
				$companys = $this->bsc_group_model->get('parent_id = 1001');
				$sales_ids_or = array();
				foreach ($companys as $company) {
					$group_code = $company['group_code'];
					$sales_ids_or[] = "(select `group` from bsc_user where bsc_user.id = crm_promote_mail.create_by) like '%$group_code-%'";
				}
				if (empty($sales_ids_or)) $sales_ids_or[] = "1 = 0";
				$where[] = "(" . join(' or ', $sales_ids_or) . ")";
			}
		} else {
			// $where[] = '0 = 1';
		}

		//新增权限 2022-05-06
		$userRange = get_session('user_range');
		$userRange = implode(',', $userRange);
		//$where["create_by"]=$this->session->userdata('id');
		//2022-07-12 放开权限
		if($group_id != 1){
			if (!is_admin()) $where[] = "crm_promote_mail.create_by in($userRange)";
		}


		if (is_numeric($status)) {
			$where[] = "crm_promote_mail.status = '$status' ";
		}
		if($group_id == 1)
			$where[] = "crm_promote_mail.send_group_id = 1";
		else {
			$where[] = "crm_promote_mail.send_group_id > 1";
		}
		$where = join(' and ', $where);


		$result["total"] = $this->crm_promote_mail_model->total($where);
		$this->db->limit($rows, $offset);
		$this->db->select("crm_promote_mail.*, (select count(1) from crm_promote_mail_log where crm_promote_mail_log.mail_id = crm_promote_mail.id) as total_count, (select count(1) from crm_promote_mail_log where crm_promote_mail_log.mail_id = crm_promote_mail.id and crm_promote_mail_log.open_time > '0000-00-00 00:00:00') as read_count, (select count(1) from crm_promote_mail_log where crm_promote_mail_log.mail_id = crm_promote_mail.id and crm_promote_mail_log.status !=0) as send_count, (select count(1) from crm_promote_mail_log where crm_promote_mail_log.mail_id = crm_promote_mail.id and crm_promote_mail_log.status =1) as ok_count");

		$rs = $this->crm_promote_mail_model->get($where, $sort, $order);
		foreach ($rs as &$item) {
			$u = $this->m_model->query_one("select `name` from bsc_user where id = {$item["create_by"]}");
			$item["create_name"] = $u["name"];
			$item["send_group"] = $item["send_group_name"];
			$item["status_str"] = $this->crm_promote_mail_model->status_s($item["status"]);
			if ($item["status"] == 1) {
				$item["operate"] = "<span style='color: #e38809'>" . lang('待发送') . "</span>";
			}
			if ($item["status"] == 2) {
				$item["operate"] = "<span style='color: #1fe390'>" . lang('已发送') . "</span>";
			}
			if ($item["status"] == 0) {
				//2022-07-12 只有自己是创建人才能编辑
				if(get_session('id')==$item["create_by"]){
					$item["operate"] = "<span style='color: #e38809'>" . lang('待审核') . "</span> <a href='javascript:void(0);' onclick='edit({$item['id']})'>" . lang('编辑内容') . "</a>";
				}else{
					$item["operate"]="<span style='color: #e38809'>" . lang('待审核') . "</span>";
				}
			}
			if ($item["status"] == -1) {
				$item["operate"] = "<span style='color: #ccc'>{$item["reason"]}</span>";
			}
			$item["operate"] .= " <a href='/crm_promote_mail/mail_log/{$item["id"]}' target='_blank'>" . lang('查看详情') . "</a>";
			if($group_id==1) $item["operate"] = str_replace(lang('待审核'),"",$item["operate"]);
			if ($item["total_count"] == 0) {
				$item["send_count"] = 0;
			} else {
				$item["send_count"] = round(100 * $item["send_count"] / $item["total_count"], 0) . "%";
			}
		}
		//<a href='javascript:void(0)' style='color: #1E9FFF' onclick='sendMail({$item["id"]})'>发送</a>
		$result["rows"] = $rs;
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * 添加预发送mail
	 */
	public function add_presend()
	{
		$recipient_group = $this->crm_promote_consignee_group_model->get_all(get_session('id'));
		$signature = getUserField(get_session('id'), 'signature');
		$data = array(
			"mail_title" => "",
			"sender_name" => "",
			"mail_content" => "",
			"recipient_group" => $recipient_group,
			'signature' => $signature,
		);
		$c_id = isset($_GET["c_id"]) ? $_GET["c_id"] : "";

		if ($c_id != "") {
			$data = $this->m_model->query_one("select * from crm_promote_mail where id = $c_id");
			$data["recipient_group"] = $recipient_group;
			$data["signature"] = $signature;
		}


		$this->load->view('head');
		$this->load->view('biz/crm/crm_promote_mail/add_presend', $data);
	}

	/**
	 * 添加预发送保存
	 * @return void
	 */
	public function add_presend_data()
	{
		$reData = array('code' => 400, 'msg' => lang('失败'));
		$mail_title = postValue('mail_title');
		$sender_name = postValue('sender_name',"-");
		$mail_content = postValue('mail_content');
		$signature = trim(postValue('signature'));

		if (empty($mail_title)) {
			$reData['msg'] = lang('请输入邮件标题');
			echo json_encode($reData, JSON_UNESCAPED_UNICODE);
			exit;
		}
		if (empty($sender_name)) {
			$reData['msg'] = lang('请输入发送人名称');
			echo json_encode($reData, JSON_UNESCAPED_UNICODE);
			exit;
		}
		if (empty($mail_content)) {
			$reData['msg'] = lang('请输入内容');
			echo json_encode($reData, JSON_UNESCAPED_UNICODE);
			exit;
		}
		$row = $this->crm_promote_consignee_group_model->get_row(array("id" => $_POST['recipient_group']));
		$data["send_group_id"] = postValue('recipient_group');
		$data["send_group_name"] = $row["group_name"];
		$data["mail_title"] = postValue('mail_title');
		$data["sender_name"] = postValue('sender_name');
		$data["mail_content"] = postValue('mail_content');
		$data["status"] = 0;
		$data["plan_send_time"] = postValue("plan_send_time");

		//自动填充签名档
		if (!empty($signature)) $data['mail_content'] .= "<hr>" . str_replace("\n", '<br/>', $signature);
		Model('bsc_user_model');
		if (strlen($signature) > 5) {
			$this->bsc_user_model->update(get_session('id'), array('signature' => $signature));
		}
		$re = $this->crm_promote_mail_model->save($data);
		if ($re > 0) {
			// record the log
			$log_data = array();
			$log_data["table_name"] = "crm_promote_mail";
			$log_data["key"] = $re;
			$log_data["action"] = "insert";
			$log_data["value"] = json_encode($data);
			log_rcd($log_data);

			$reData['code'] = 200;
			$reData['msg'] = lang('添加成功');
			$reData['url'] = "/crm_promote_mail/mail_log/$re";
			if($data["send_group_id"]==1) $reData['url'] = "/crm_promote_mail/index/?group_id=1";  //返回公共模板
		}
		echo json_encode($reData, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * 收件人组列表
	 * @return void
	 */
	public function consignee_group_list()
	{
		$this->load->view('head');
		$this->load->view('biz/crm/crm_promote_mail/consignee_group_list');
	}

	/**
	 * 收件人组列表数据
	 * @return void
	 */
	public function consignee_group_list_get_data()
	{
		$result = array();
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
		$group_name = postValue('group_name', '');
		$offset = ($page - 1) * $rows;
		$where = array();
		$userRange = get_session('user_range');
		$where[] = "create_by in ('" . join('\',\'', $userRange) . "')";
		if ($group_name != '') $where[] = "group_name like '%{$group_name}%'";
		$where = join(' and ', $where);
		$result["total"] = $this->crm_promote_consignee_group_model->total($where);
		$this->db->limit($rows, $offset);
		$rs = $this->crm_promote_consignee_group_model->get($where, $sort, $order);
		foreach ($rs as &$item) {
			$num = $this->m_model->query_one("select count(*) as n from crm_promote_consignee where group_id = {$item["id"]}");
			$item["num"] = $num["n"];
			$user = $this->m_model->query_one("select name from bsc_user where id = {$item["create_by"]}");
			$item['create_by_name'] = $user["name"];
			$item["operate"] = "<a href='/crm_promote_mail/group_consignee_list?group_id={$item["id"]}' style='color: #1E9FFF' target='_blank'>" . lang('查看并开始群发') . "</a>";
		}
		$result["rows"] = $rs;
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * 新增收件人组
	 * @return void
	 */
	public function add_consignee_group($controller = '')
	{
		//获取group_id
		$session_id = get_session('id');
		$public_moban = $this->db->where('send_group_id', 1)->order_by('create_time', 'desc')->limit(10)->get('crm_promote_mail')->result_array();
		//获取最近十条数据
		$my_moban = $this->db->where('create_by', $session_id)->order_by('create_time', 'desc')->limit(10)->get('crm_promote_mail')->result_array();
		$data["list"] = array_merge($public_moban,$my_moban);
		$data['group_id'] = isset($_GET["group_id"]) ? $_GET["group_id"] : "";

		$data['controller'] = $controller;

		$this->load->view('head');
		$this->load->view('biz/crm/crm_promote_mail/add_consignee_group', $data);
	}

	public function delete_consignee_group_data()
	{
		$id = $_GET["id"];
		$this->crm_promote_consignee_model->del_by_parent($id);
		$re = $this->crm_promote_consignee_group_model->del($id);
		if ($re > 0) {
			$reData['code'] = 200;
			$reData['msg'] = "已删除";
		}
		echo json_encode($reData, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * 新增收件人组
	 */
	public function add_consignee_group_data()
	{
		$reData = array('code' => 400, 'msg' => '失败');
		$group_name = $_POST['group_name'];
		if (empty($group_name)) {
			$reData['msg'] = "请输入组名";
			echo json_encode($reData, JSON_UNESCAPED_UNICODE);
			exit;
		}
		$re = $this->crm_promote_consignee_group_model->add_group_name($group_name);
		if ($re > 0) {
			$reData['code'] = 200;
			$reData['msg'] = "添加成功";
			$reData['id'] = $re;
		}
		echo json_encode($reData, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * 组收件人列表
	 */
	function group_consignee_list()
	{
		$group_id = isset($_GET["group_id"]) ? $_GET["group_id"] : 0;
		$data = array(
			"group_id" => $group_id,
		);
		$sql = "select *,(select `name` from bsc_user where id = crm_promote_consignee_group.create_by) as created_name from crm_promote_consignee_group where id = $group_id ";
		$data["group_row"] = $this->m_model->query_one($sql);
		if(empty($data["group_row"]) || !in_array($data['group_row']['create_by'],get_session('user_range'))) die('no data autherization');
		$this->load->view('head');
		$this->load->view('biz/crm/crm_promote_mail/group_consignee_list', $data);
	}

	/**
	 * 组收件人列表 数据
	 */
	function group_consignee_list_get_data()
	{
		$result = array();
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$rows = isset($_GET['rows']) ? intval($_GET['rows']) : 30;
		$sort = isset($_GET['sort']) ? strval($_GET['sort']) : "id";
		$order = isset($_GET['order']) ? strval($_GET['order']) : 'desc';
		$offset = ($page - 1) * $rows;
		$where = array();

		$email = trim(getValue('email', ''));
		if($email !== '') $where[] = "crm_promote_consignee.email like '%{$email}%'";

		$where[] = "group_id = '{$_GET['group_id']}'";

		$where = join(' and ', $where);
		$result["total"] = $this->crm_promote_consignee_model->total($where);
		// $this->db->limit($rows, $offset);
		$this->load->model("m_model");
		$rs = $this->m_model->query_array("select crm_promote_consignee.*,(select company_name from biz_client where biz_client.client_code=crm_promote_consignee.client_code) as company_name,IFNULL(crm_promote_mail_invalid.ok, 0) AS ok from crm_promote_consignee LEFT JOIN crm_promote_mail_invalid on crm_promote_mail_invalid.invalid_email = crm_promote_consignee.email where {$where} order by $sort $order limit $offset,$rows");
		$result[] = $email;
		// $this->crm_promote_consignee_model->get($where,$sort,$order);
		if (!empty($rs)) {
			foreach ($rs as &$item) {
				$row = $this->crm_promote_consignee_group_model->get_row(array("id" => $item['group_id']));
				$item['group_name'] = isset($row['group_name']) ? $row["group_name"] : "-";
				$item["operate"] = "<a href='javasript:void(0)' onclick='delConsig({$item['id']})' style='color: #1E9FFF'>" . lang('删除') . "</a>";
				if ($item["ok"] < 0) $item["operate"] .= " <a href='/other/force_pass_email?email={$item['email']}'  style='color: #1E9FFF' target='_blank'> | " . lang('强制通过') . "</a>";
				if ($item['client_code']=='外部') $item['company_name'] = $item['name'];
			}
		}
		$result["rows"] = $rs;
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * 新增收件人
	 * @return void
	 */
	function add_consignee()
	{
		$row = $this->crm_promote_consignee_group_model->get_row(array("id" => $_GET["group_id"]));
		$data = array(
			"group_id" => $_GET["group_id"],
			"group_name" => $row["group_name"],
		);
		$this->load->view('head');
		$this->load->view('biz/crm/crm_promote_mail/add_consignee', $data);
	}

	/**
	 * 删除收件人
	 * @return void
	 */
	function del_consignee()
	{
		$reData = array('code' => 400, 'msg' => lang('失败'));
		$id = $_GET['id'];
		$re = $this->crm_promote_consignee_model->del($id);
		if ($re > 0) {
			$reData['code'] = 200;
			$reData['msg'] = lang('删除成功');
		}
		echo json_encode($reData, JSON_UNESCAPED_UNICODE);
	}

	function del_consignee_invalid()
	{
		$group_id = $_GET['group_id'];
		$sql = "SELECT crm_promote_consignee.id FROM `crm_promote_consignee` left join crm_promote_mail_invalid on crm_promote_mail_invalid.invalid_email = crm_promote_consignee.email where crm_promote_consignee.group_id = $group_id and ok = -1";
		$ids = $this->m_model->query_array($sql);
		$ids = array_column($ids, "id");
		$ids = join(",", $ids);
		$sql = "delete from crm_promote_consignee where id in ($ids);";
		$this->m_model->query($sql);
		$reData = array();
		$reData['code'] = 200;
		$reData['msg'] = lang('删除成功');
		echo json_encode($reData, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * 保存添加联系人
	 * @return void
	 */
	function add_consignee_data()
	{
		$data = array();
		$reData = array('code' => 400, 'msg' => '失败');
		if (empty($_POST['name'])) {
			$reData['msg'] = lang('姓名为必须项');
			echo json_encode($reData, JSON_UNESCAPED_UNICODE);
			exit;
		}
		if (empty($_POST['email'])) {
			$reData['msg'] = lang('邮件为必须项');
			echo json_encode($reData, JSON_UNESCAPED_UNICODE);
			exit;
		}
		if (checkEmail($_POST['email']) == 0) {
			$reData['msg'] = lang('错误的邮件地址');
			echo json_encode($reData, JSON_UNESCAPED_UNICODE);
			exit;
		}
		$data['name'] = $_POST['name'];
		$data['gender'] = $_POST['gender'];
		$data['phone'] = $_POST['phone'];
		$data['email'] = $_POST['email'];
		$data['group_id'] = $_POST['group_id'];
		$data['verify'] = "是";
		$re = $this->crm_promote_consignee_model->save($data);
		if ($re > 0) {
			$reData['code'] = 200;
			$reData['msg'] = lang('添加成功');
			$reData['url'] = "/crm_promote_mail/group_consignee_list?group_id=" . $_POST['group_id'];
		}
		echo json_encode($reData, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * crm 查询标签页面 用于后面发送筛选
	 */
	public function search_send()
	{
		Model('bsc_dict_model');
		Model('biz_client_model');
		$data = array();
		$data['export_sailing'] = array_column($this->bsc_dict_model->get_option('export_sailing'), 'value');
		$data['trans_mode'] = array_column($this->bsc_dict_model->get_option('client_trans_mode'), 'value');
		$data['product_type'] = array_column($this->bsc_dict_model->get_option('product_type'), 'value');
		$data['client_source'] = array_column($this->bsc_dict_model->get_option('client_source'), 'value');
		//国家代码首先需要去client里查出来,然后再去国家表里转化为中文
		$this->db->select('country_code');//(select name from bsc_dict where catalog = \'country_code\' and bsc_dict.value = biz_client.country_code) as country_name
		$this->db->group_by("country_code");
		$country_code = array_column($this->biz_client_model->no_role_get(""), 'country_code');
		$this->db->select('name');
		$data['country_code'] = array_column($this->bsc_dict_model->no_role_get("catalog = 'country_code' and value in ('" . join('\',\'', $country_code) . "')"), 'name');
		$this->load->view('head');
		$this->load->view('biz/crm/crm_promote_mail/search_send.php', $data);
	}

	/**
	 * CRM查询标签页面跳转到的 新增群发邮件
	 */
	public function add_presend_by_search_send()
	{
		$search_type = postValue('search_type', 0); // 传过来的title数据
		$userRange = get_session('user_range');
		// $crm_user = isset($_COOKIE['crm_user']) ? $_COOKIE['crm_user'] : get_session('id');
		// $crm_user_type = isset($_COOKIE['crm_user_type']) ? $_COOKIE['crm_user_type'] : 'user';
		//这里根据title传过来的数据 进行 查询,查询完成的同时,进行 组和 人员的数据新增
		Model('biz_client_crm_model');
		$title_where = array();
		$title_where[] = "biz_client_crm.sales_id in ('" . join('\',\'', $userRange) . "')";
		// if($crm_user_type == 'user'){
		//     //2022-01-10 新需求 CRM获取数据逻辑 原创建人 改为 创建人+销售
		//     //2022-02-23 改版后, crm获取数据,只自己获取自己的
		//     $title_where[] = "(biz_client_crm.created_by = '$crm_user')";
		// }else if($crm_user_type == 'group'){
		//     $title_where[] = "biz_client_crm.created_group like '%$crm_user%'";
		// }else if($crm_user_type == 'sub_company'){
		//     $this_crm_user = str_replace('company_', '', $crm_user);
		//     $title_where[] = "biz_client_crm.created_group like '$this_crm_user-%'";
		// }else {
		//     $title_where[] = '0 = 1';
		// }
		$title_where[] = 'biz_client_crm.apply_status = 2'; // 限制只有已通过
		$title_where[] = "length(biz_client_contact_list.email) > 5"; // 代替下面的 if (strlen($insert_data['email']) < 5) continue; 这个限制,因为计算总数需要用到
		if ($search_type == 0) {
			$title = postValue('title', ''); // 传过来的title数据
			$title_arr = filter_unique_array(explode(',', $title));

			if ($title != 'all') { // all的什么条件都不加
				$or_where = array();
				foreach ($title_arr as $title_str) {
					$or_where[] = "biz_client_crm.export_sailing like '%{$title_str}%'";
					$or_where[] = "biz_client_crm.trans_mode like '%{$title_str}%'";
					$or_where[] = "biz_client_crm.product_type like '%{$title_str}%'";
					$or_where[] = "(select name from bsc_dict where catalog = 'country_code' and bsc_dict.value = biz_client.country_code) like '%{$title_str}%'";
					// $or_where[] = "client_source like '%{$title_str}%'";
				}
				if (empty($or_where)) $or_where[] = '1 = 1';
				$title_where[] = '(' . join(' or ', $or_where) . ')';
			}
		} else if ($search_type == 1) {
			$fields = postValue('field', array());

			$title_arr = array();
			foreach ($fields as $key => $field_value) {
				$value = trim($field_value);
				if ($value == '') continue;
				$pre = 'biz_client_crm.';
				if (in_array($key, array('client_name', 'company_address', 'province', 'city', 'area'))) $pre = 'biz_client.';
				$title_where[] = "{$pre}{$key} like '%{$value}%'";
				$title_arr[] = $value;
			}
		} else {
			exit(lang('暂不支持'));
		}

		$title_where_str = join(' and ', $title_where);
		$this->db->select('biz_client_crm.id,biz_client_crm.client_code,biz_client_contact_list.telephone,biz_client_contact_list.email,biz_client_contact_list.name');
		$this->db->group_by('biz_client_contact_list.id');
		$this->db->join('biz_client_contact_list', "biz_client_contact_list.crm_id = biz_client_crm.id", 'right');
		$this->db->join('biz_client', "biz_client.client_code = biz_client_crm.client_code", 'left');

		//判断是否获取总数
		$get_total = getValue('get_total', 'false');
		if ($get_total === 'true') {
			$total = $this->biz_client_crm_model->no_role_total($title_where_str);
			return jsonEcho(array('code' => 0, 'msg' => lang('获取成功'), 'data' => array('total' => $total)));
		}

		$client_crms = $this->biz_client_crm_model->no_role_get($title_where_str, 'biz_client_crm.id');//获取 crm匹配的数据,然后将客户联系人信息进行整理
		if (empty($client_crms)) exit('<a href="javascript:history.go(-1);">' . lang('返回上一页') . '</a><br/>' . lang('不存在匹配CRM数据')); // 没数据就不下去了
		//将title的值排下序,然后判断是否存在该组,如果存在,直接绑定,不存在,进行新增
		$group_name = $title_arr;

		sort($group_name);//排下序,这样名字重复好查询
		//最后面加个时间戳,就不会重复了
		//. $crm_user_type . '_' . $crm_user
		$group_name = join(',', $group_name) . get_session('id');
		//首先查询组名
		$group = $this->crm_promote_consignee_group_model->get_one('group_name', $group_name);
		$insert_data = array();
		if (!empty($group)) {//存在直接取
			//2022-03-14 存在组的直接断掉,后续可以直接去除该步骤
			// exit('<a href="javascript:history.go(-1);">返回上一页</a><br/>已存在组,请联系管理员');
			// return;
			$insert_data['group_id'] = $group['id'];
		} else {//不存在新增再取
			$re = $this->crm_promote_consignee_group_model->add_group_name_rt_id($group_name);
			$insert_data['group_id'] = $re;
		}
		//开始循环新增该组成员信息
		//这里为了逻辑的简单化,就直接清空原组成员信息,重新进行新增
		$this->crm_promote_consignee_model->del_group($insert_data['group_id']);//清空组成员
		foreach ($client_crms as $client_crm) {
			$insert_data['name'] = $client_crm['name'];
			$insert_data['gender'] = '男';//现在的user没有性别字段的,所以我统一男了
			$insert_data['phone'] = $client_crm['telephone'];
			$insert_data['email'] = $client_crm['email'];
			$insert_data['client_code'] = $client_crm['client_code'];
			// $insert_data['verify']="是";
			if (strlen($insert_data['email']) < 5) continue;
			$this->crm_promote_consignee_model->save($insert_data);//新增组成员信息
		}
		$recipient_group = $this->crm_promote_consignee_group_model->get("id = '{$insert_data['group_id']}'");
		$signature = getUserField(get_session('id'), 'signature');
		$data = array(
			"recipient_group" => $recipient_group,
			"signature" => $signature,
		);
		$this->load->view('head');
		$this->load->view('biz/crm/crm_promote_mail/add_presend_by_search_send', $data);
	}

	/**
	 * CRM查询标签页面跳转到的 新增群发邮件
	 */
	public function add_presend_by_search_send_crm()
	{
		$group_ids = isset($_GET["group_ids"]) ? $_GET["group_ids"] : "";

		if (empty($group_ids)) {
			//变成数组
			$ids = explode(',', $_POST['ids']);
			$title2 = explode(',', $_POST['title']);
			if(empty(filter_unique_array($ids))) exit('<a href="javascript:window.close();">' . lang('关闭当前页面') . '</a><br/>' . lang('未传入有效的id')); //

//        $userRange = get_session('user_range');
			// $crm_user = isset($_COOKIE['crm_user']) ? $_COOKIE['crm_user'] : get_session('id');
			// $crm_user_type = isset($_COOKIE['crm_user_type']) ? $_COOKIE['crm_user_type'] : 'user';
			//这里根据title传过来的数据 进行 查询,查询完成的同时,进行 组和 人员的数据新增
			Model('biz_client_crm_model');
			$title_where = array();
//        $title_where[] = "biz_client_crm.id in ('" . join('\',\'', $ids) . "')";
//        $title_where[] = "biz_client_crm.sales_id in ('" . join('\',\'', $userRange) . "')";

			$title_where[] = 'biz_client_crm.apply_status = 2'; // 限制只有已通过
			$title_where[] = "length(biz_client_contact_list.email) > 5"; // 代替下面的 if (strlen($insert_data['email']) < 5) continue; 这个限制,因为计算总数需要用到


			/*if($search_type == 0){
				$title = postValue('title', ''); // 传过来的title数据

				$title_arr = filter_unique_array(explode(',', $title));

				if ($title != 'all') { // all的什么条件都不加
					$or_where = array();
					foreach ($title_arr as $title_str) {
						$or_where[] = "biz_client_crm.export_sailing like '%{$title_str}%'";
						$or_where[] = "biz_client_crm.trans_mode like '%{$title_str}%'";
						$or_where[] = "biz_client_crm.product_type like '%{$title_str}%'";
						$or_where[] = "(select name from bsc_dict where catalog = 'country_code' and bsc_dict.value = biz_client.country_code) like '%{$title_str}%'";
						// $or_where[] = "client_source like '%{$title_str}%'";
					}
					if (empty($or_where)) $or_where[] = '1 = 1';
					$title_where[] = '(' . join(' or ', $or_where) . ')';
				}
			}else if($search_type == 1){
				$fields = postValue('field', array());

				$title_arr = array();
				foreach ($fields as $key => $field_value){
					$value = trim($field_value);
					if($value == '') continue;
					$pre = 'biz_client_crm.';
					if(in_array($key, array('client_name', 'company_address', 'province', 'city', 'area'))) $pre = 'biz_client.';
					$title_where[] = "{$pre}{$key} like '%{$value}%'";
					$title_arr[] = $value;
				}
			}else{
				exit('暂不支持');
			}*/

			$title_where_str = join(' and ', $title_where);
			$this->this_db->select('biz_client_crm.id,biz_client_crm.client_code,biz_client_contact_list.telephone,biz_client_contact_list.email,biz_client_contact_list.name');
			$this->this_db->group_by('biz_client_contact_list.id');
			$this->this_db->join('biz_client_contact_list', "biz_client_contact_list.crm_id = biz_client_crm.id", 'right');
			$this->this_db->join('biz_client', "biz_client.client_code = biz_client_crm.client_code", 'left');

			/*  //判断是否获取总数
			  $get_total = getValue('get_total', 'false');
			  if($get_total === 'true'){
				  $total = $this->biz_client_crm_model->no_role_total($title_where_str);
				  return jsonEcho(array('code' => 0, 'msg' => '获取成功', 'data' => array('total' => $total)));
			  }*/

			if ($title_where_str != '') {
				$client_crms = $this->this_db->where_in('biz_client_crm.id', $ids, false)->where($title_where_str)->order_by('biz_client_crm.id', 'desc', true)->get('biz_client_crm')->result_array();
			} else {
				$client_crms = $this->this_db->where_in('biz_client_crm.id', $ids, false)->get('biz_client_crm')->result_array();
			}

			/*        $client_crms = $this->biz_client_crm_model->no_role_get($title_where_str, 'biz_client_crm.id');//获取 crm匹配的数据,然后将客户联系人信息进行整理

					public function no_role_get($where="" ,$sort="id",$order="desc"){
					if($where !="")	$this->db->where($where, null, false);
					$this->db->order_by($sort,$order, true);
					$query = $this->db->get($this->table);
					return $query->result_array();
				}*/


			if (empty($client_crms)) exit('<a href="javascript:window.close();">' . lang('关闭当前页面') . '</a><br/>' . lang('不存在匹配CRM数据')); // 没数据就不下去了
			//将title的值排下序,然后判断是否存在该组,如果存在,直接绑定,不存在,进行新增
			//2023-10-08 这里由于海外的原因, 还是用的china的库, 所以默认前面再拼接个国家前缀
			$group_name = $title2;

			sort($group_name);//排下序,这样名字重复好查询
			//最后面加个时间戳,就不会重复了
			//. $crm_user_type . '_' . $crm_user
			$group_name = get_system_type() . ',' . join(',', $group_name) . get_session('id');
			//首先查询组名
			$group = $this->crm_promote_consignee_group_model->get_one('group_name', $group_name);
			$insert_data = array();
			if (!empty($group)) {//存在直接取
				//2022-03-14 存在组的直接断掉,后续可以直接去除该步骤
				// exit('<a href="javascript:history.go(-1);">返回上一页</a><br/>已存在组,请联系管理员');
				// return;
				$insert_data['group_id'] = $group['id'];
			} else {//不存在新增再取
				$re = $this->crm_promote_consignee_group_model->add_group_name_rt_id($group_name);
				$insert_data['group_id'] = $re;
			}

			//开始循环新增该组成员信息
			//这里为了逻辑的简单化,就直接清空原组成员信息,重新进行新增
			$this->crm_promote_consignee_model->del_group($insert_data['group_id']);//清空组成员
			foreach ($client_crms as $client_crm) {
				$insert_data['name'] = $client_crm['name'];
				$insert_data['gender'] = '男';//现在的user没有性别字段的,所以我统一男了
				$insert_data['phone'] = $client_crm['telephone'];
				$insert_data['email'] = $client_crm['email'];
				$insert_data['client_code'] = $client_crm['client_code'];
				// $insert_data['verify']="是";
				if (strlen($insert_data['email']) < 5) continue;
				$this->crm_promote_consignee_model->save($insert_data);//新增组成员信息
			}
		} else {
			$insert_data['group_id'] = $group_ids;
		}
		$recipient_group = $this->crm_promote_consignee_group_model->get("id = '{$insert_data['group_id']}'");
		$signature = getUserField(get_session('id'), 'signature');
		$data = array(
			"recipient_group" => $recipient_group,
			"signature" => $signature,
		);

		//新增 2022-05-27
		$data['search'] = 'crm';

		$c_id = isset($_GET["c_id"]) ? $_GET["c_id"] : "";

		if (!empty($c_id)) {
			$c_data = $this->m_model->query_one("select * from crm_promote_mail where id = $c_id");
			$data['mail_title'] = $c_data['mail_title'];
			$data['sender_name'] = $c_data['sender_name'];
			$data['mail_content'] = $c_data['mail_content'];
			$data["signature"] = $signature;
		} else {
			$data['mail_title'] = '';
			$data['sender_name'] = '';
			$data['mail_content'] = '';
		}

		$this->load->view('head');
		$this->load->view('biz/crm/crm_promote_mail/add_presend_by_search_send', $data);
	}

	/**
	 * Client查询标签页面跳转到的 新增群发邮件
	 */
	public function add_presend_by_search_send_client()
	{

		$group_ids = isset($_GET["group_ids"]) ? $_GET["group_ids"] : "";

		if (empty($group_ids)) {

			//变成数组
			$ids = explode(',', $_POST['ids']);
			$title2 = explode(',', $_POST['title']);

//        $userRange = get_session('user_range');
			// $crm_user = isset($_COOKIE['crm_user']) ? $_COOKIE['crm_user'] : get_session('id');
			// $crm_user_type = isset($_COOKIE['crm_user_type']) ? $_COOKIE['crm_user_type'] : 'user';
			//这里根据title传过来的数据 进行 查询,查询完成的同时,进行 组和 人员的数据新增
			Model('biz_client_model');
			$title_where = array();
//        $title_where[] = "biz_client_crm.id in ('" . join('\',\'', $ids) . "')";
//        $title_where[] = "biz_client_crm.sales_id in ('" . join('\',\'', $userRange) . "')";

//        $title_where[] = 'biz_client_crm.apply_status = 2'; // 限制只有已通过
			$title_where[] = "length(biz_client_contact_list.email) > 5"; // 代替下面的 if (strlen($insert_data['email']) < 5) continue; 这个限制,因为计算总数需要用到


			/*if($search_type == 0){
				$title = postValue('title', ''); // 传过来的title数据

				$title_arr = filter_unique_array(explode(',', $title));

				if ($title != 'all') { // all的什么条件都不加
					$or_where = array();
					foreach ($title_arr as $title_str) {
						$or_where[] = "biz_client_crm.export_sailing like '%{$title_str}%'";
						$or_where[] = "biz_client_crm.trans_mode like '%{$title_str}%'";
						$or_where[] = "biz_client_crm.product_type like '%{$title_str}%'";
						$or_where[] = "(select name from bsc_dict where catalog = 'country_code' and bsc_dict.value = biz_client.country_code) like '%{$title_str}%'";
						// $or_where[] = "client_source like '%{$title_str}%'";
					}
					if (empty($or_where)) $or_where[] = '1 = 1';
					$title_where[] = '(' . join(' or ', $or_where) . ')';
				}
			}else if($search_type == 1){
				$fields = postValue('field', array());

				$title_arr = array();
				foreach ($fields as $key => $field_value){
					$value = trim($field_value);
					if($value == '') continue;
					$pre = 'biz_client_crm.';
					if(in_array($key, array('client_name', 'company_address', 'province', 'city', 'area'))) $pre = 'biz_client.';
					$title_where[] = "{$pre}{$key} like '%{$value}%'";
					$title_arr[] = $value;
				}
			}else{
				exit('暂不支持');
			}*/

			$title_where_str = join(' and ', $title_where);
			$this->db->select('biz_client.id,biz_client.client_code,biz_client_contact_list.telephone,biz_client_contact_list.email,biz_client_contact_list.name');
			$this->db->group_by('biz_client_contact_list.id');
			$this->db->join('biz_client_contact_list', "biz_client_contact_list.client_code = biz_client.client_code", 'right');

			/*  //判断是否获取总数
			  $get_total = getValue('get_total', 'false');
			  if($get_total === 'true'){
				  $total = $this->biz_client_crm_model->no_role_total($title_where_str);
				  return jsonEcho(array('code' => 0, 'msg' => '获取成功', 'data' => array('total' => $total)));
			  }*/

			if ($title_where_str != '') {
				$client_crms = $this->db->where_in('biz_client.id', $ids)->where($title_where_str)->order_by('biz_client.id', 'desc', true)->get('biz_client')->result_array();
			} else {
				$client_crms = $this->db->where_in('biz_client.id', $ids)->get('biz_client')->result_array();
			}

			/*        $client_crms = $this->biz_client_crm_model->no_role_get($title_where_str, 'biz_client_crm.id');//获取 crm匹配的数据,然后将客户联系人信息进行整理

					public function no_role_get($where="" ,$sort="id",$order="desc"){
					if($where !="")	$this->db->where($where, null, false);
					$this->db->order_by($sort,$order, true);
					$query = $this->db->get($this->table);
					return $query->result_array();
				}*/

			if (empty($client_crms)) exit('<a href="javascript:window.close();">' . lang('关闭当前页面') . '</a><br/>' . lang('不存在匹配Client数据')); // 没数据就不下去了
			//将title的值排下序,然后判断是否存在该组,如果存在,直接绑定,不存在,进行新增
			$group_name = $title2;

			sort($group_name);//排下序,这样名字重复好查询
			//最后面加个时间戳,就不会重复了
			//. $crm_user_type . '_' . $crm_user
			$group_name = join(',', $group_name) . get_session('id');
			//首先查询组名
			$group = $this->crm_promote_consignee_group_model->get_one('group_name', $group_name);
			$insert_data = array();


			if (!empty($group)) {//存在直接取
				//2022-03-14 存在组的直接断掉,后续可以直接去除该步骤
				// exit('<a href="javascript:history.go(-1);">返回上一页</a><br/>已存在组,请联系管理员');
				// return;
				$insert_data['group_id'] = $group['id'];
			} else {//不存在新增再取
				$re = $this->crm_promote_consignee_group_model->add_group_name_rt_id($group_name);
				$insert_data['group_id'] = $re;
			}

			//开始循环新增该组成员信息
			//这里为了逻辑的简单化,就直接清空原组成员信息,重新进行新增
			$this->crm_promote_consignee_model->del_group($insert_data['group_id']);//清空组成员
			foreach ($client_crms as $client_crm) {
				$insert_data['name'] = $client_crm['name'];
				$insert_data['gender'] = '男';//现在的user没有性别字段的,所以我统一男了
				$insert_data['phone'] = $client_crm['telephone'];
				$insert_data['email'] = $client_crm['email'];
				$insert_data['client_code'] = $client_crm['client_code'];
				// $insert_data['verify']="是";
				if (strlen($insert_data['email']) < 5) continue;
				$this->crm_promote_consignee_model->save($insert_data);//新增组成员信息
			}
		} else {
			$insert_data['group_id'] = $group_ids;
		}
		$recipient_group = $this->crm_promote_consignee_group_model->get("id = '{$insert_data['group_id']}'");
		$signature = getUserField(get_session('id'), 'signature');
		$data = array(
			"recipient_group" => $recipient_group,
			"signature" => $signature,
		);

		//新增 2022-05-27
		$data['search'] = 'client';

		$c_id = isset($_GET["c_id"]) ? $_GET["c_id"] : "";

		if (!empty($c_id)) {
			$c_data = $this->m_model->query_one("select * from crm_promote_mail where id = $c_id");
			$data['mail_title'] = $c_data['mail_title'];
			$data['sender_name'] = $c_data['sender_name'];
			$data['mail_content'] = $c_data['mail_content'];
			$data["signature"] = $signature;
		} else {
			$data['mail_title'] = '';
			$data['sender_name'] = '';
			$data['mail_content'] = '';
		}


		$this->load->view('head');
		$this->load->view('biz/crm/crm_promote_mail/add_presend_by_search_send', $data);
	}


	/**
	 * 根据传入的where条件,获取
	 */
	private function get_crm_contact()
	{

	}

	public function edit()
	{
		$id = getValue('id', '');
		$data = $this->crm_promote_mail_model->get_row(array("id" => $id));
		$this->load->view('head');
		$this->load->view('/biz/crm/crm_promote_mail/edit_view', $data);
	}

	public function update_mail()
	{
		$reData = array('code' => 400, 'msg' => lang('失败'));
		$id = postValue('id');
		//获取标题
		$mail_title = postValue('mail_title');
		if (empty($mail_title)) {
			$reData['msg'] = lang('请输入标题');
			echo json_encode($reData, JSON_UNESCAPED_UNICODE);
			exit;
		}
		$data["mail_title"] = postValue('mail_title');
		//获取内容
		$mail_content = postValue('mail_content');
		if (empty($mail_content)) {
			$reData['msg'] = lang('请输入内容');
			echo json_encode($reData, JSON_UNESCAPED_UNICODE);
			exit;
		}
		$data["mail_content"] = postValue('mail_content');
		$data["plan_send_time"] = postValue('plan_send_time');

		$re = $this->crm_promote_mail_model->update($id, $data);
		if ($re > 0) {
			// record the log
			$log_data = array();
			$log_data["table_name"] = "crm_promote_mail";
			$log_data["key"] = $id;
			$log_data["action"] = "update";
			$log_data["value"] = json_encode($data);
			log_rcd($log_data);

			$reData['code'] = 200;
			$reData['msg'] = lang('保存成功');
			$reData['url'] = "/crm_promote_mail/edit?id={$id}";
		}
		echo json_encode($reData, JSON_UNESCAPED_UNICODE);
	}

	/**
	 * 新增多条用户(批量)
	 */
	public function add_consignees()
	{
		$group_ids = isset($_GET["group_id"]) ? $_GET["group_id"] : '';
		$data['group_ids'] = $group_ids;
		if (!empty($group_ids)) {
			$row = $this->crm_promote_consignee_group_model->get_row(array("id" => $_GET["group_id"]));
			$data = array(
				"group_id" => $_GET["group_id"],
				"group_name" => $row["group_name"],
			);
		} else {
			$data = array(
				"group_id" => '',
				"group_name" => '',
			);
		}

		$this->load->view('head');
		$this->load->view('/biz/crm/crm_promote_mail/add_consignees', $data);
	}

	/**
	 * 保存添加联系人(批量)
	 * @return void
	 */
	function add_consignees_data()
	{
		//["group_id"]=> string(3) "420" ["group_name"]=> string(24) "ZJ,丽水市,参展20215" ["emails"]=> string(0) ""
		$data = array();
		$reData = array('code' => 400, 'msg' => '失败');
		$emails = postValue('emails', ''); //邮箱串
		$emails_arr = explode("\r\n", $emails);//首先将邮箱根据换行符进行拆分
		$add_datas = array();
		$group_id = $_POST['group_id'];

		if (empty($group_id)) {
			if (empty($_POST['group_name'])) {
				return jsonEcho(array('code' => 400, 'msg' => "请输入组名"));
			}
		}

		//获取往来单位和CRM黑名单
		$where = "email <> '' and ";
		//往来单位黑名单
		$where .= "client_code in(select client_code from biz_client where client_level = -1) or ";
		//CRM黑名单
		$where .= " crm_id in(select id from biz_client_crm where apply_status = -99)";
		//取关联黑名单的email
		$black_list = $this->db->select('email, client_code')->where($where)->get('biz_client_contact_list')->result_array();
		$black_list = array_column($black_list, 'email');

		$ignore = 0;
		$ignore_msg = array();
		foreach ($emails_arr as $key => $val) {
			//2022-04-21 由于外国人名字会带有空格,改为分号分割
			$val_arr = explode(';', $val);//将当前内容,按空格拆分,第一个值为姓名,第二个值为邮箱,邮箱必须验证有效性
			$num = $key + 1;
			if (sizeof($val_arr) < 2) { // 如果没有2个元素,那么结束
				$ignore_msg[] = "第{$num}行 请填写正确的格式: 姓名 + 分号 + 邮箱";
				$ignore++;
				continue;
			}
			$name = $val_arr[0];
			$email = $val_arr[1];
			$telephone = isset($val_arr[2]) ? $val_arr[2] : "";
			if (empty($name)) {
				$ignore_msg[] = "第{$num}行 请填写收件人姓名";
				$ignore++;
				continue;
			}
			if (empty($email)) {
				$ignore_msg[] = "第{$num}行 请填写邮件地址";
				$ignore++;
				continue;
			}
			//如果邮箱不满足格式,那么提示错误
			$email = trim($email);
			if (check_email($email) == 0) {
				$ignore_msg[] = "第{$num}行 错误的邮件地址 $email ";
				$ignore++;
				continue;
			}

			if (in_array($email, $black_list) === true){
				$ignore_msg[] = "第{$num}行 邮件地址 {$email} 在黑名单中！ ";
				$ignore++;
				continue;
			}

			//查询一下,如果存在的邮箱,报错
			$this->db->select('email');
			$isset_email = $this->crm_promote_consignee_model->get_row("group_id = '{$group_id}' and email = '{$email}'");
			if (!empty($isset_email)) {
				$ignore_msg[] = "第{$num}行 邮箱已存在: " . $email;
				$ignore++;
				continue;
			}

			$data = array();
			$data['name'] = $name;
			$data['gender'] = "-";
			$data['phone'] = $telephone;
			$data['email'] = $email;
			// $data['group_id'] = $group_id;
			$data['verify'] = "-";
			$add_datas[] = $data;
		}
		if($ignore > 0){
			$ignore_msg = join('<br/>', $ignore_msg);
//            return jsonEcho(array('code' => 400, 'msg' => "当前已忽略{$ignore}行, 原因如下:" . $ignore_msg));
		}
		//这里懒得改成事务,直接移动到后面,全部成功再进行新增组了
		//如果group_id为空,就新增组
		if (empty($group_id)) {

			if (empty($_POST['group_name'])) {
				$reData['msg'] = "请输入组名";
				echo json_encode($reData, JSON_UNESCAPED_UNICODE);
				exit;
			}

			//判断新增还是更新
			$bl = $this->db->where('group_name', $_POST['group_name'])->where('create_by', $this->session->userdata('id'))->get('crm_promote_consignee_group')->row_array();

			if (empty($bl)) {
				$re = $this->crm_promote_consignee_group_model->add_group_name($_POST['group_name']);
				if ($re < 0) {
					return jsonEcho(array('code' => 400, 'msg' => "新增组名失败"));
				} else {
					$group_id = $re;
				}
			} else {
				$group_id = $bl['id'];
			}
		}
		$success = 0;
		foreach ($add_datas as $data) {
			$data['group_id'] = $group_id;
			$re = $this->crm_promote_consignee_model->save($data);
			if ($re > 0) $success++;
		}
		$reData['code'] = 200;
		$reData['msg'] = "添加成功,已新增{$success}条. 失败： $ignore 条 ";//已忽略 $ignore 条
		$reData['url'] = "/crm_promote_mail/group_consignee_list?group_id=" . $group_id;
		echo json_encode($reData, JSON_UNESCAPED_UNICODE);
	}

	//导入email
	public function import_email(){
		$group_id = (int)$this->input->get('group_id');
		if ($group_id == 0) return $this->load->view('/errors/html/error_larryms', array('msg'=>lang('请求失败，参数无效！'),'icon'=>'larry-zhandianditu'));
		// if (!is_admin())  return $this->load->view('/errors/html/error_larryms', array('msg'=>lang('当前用户无权导入跨境搜！'),'icon'=>'larry-gangweishouquan'));
		$this->load->view('/biz/crm/crm_promote_mail/import_email');
	}

	//导入email
	public function import_data(){
		$return = array('code'=>0, 'msg'=>'');
		$group_id = (int)$this->input->get('group_id');
		$item['custom'] = $this->input->post('custom');
		$item['email'] = $this->input->post('email');
		$group = $this->db->where('id', $group_id)->get('crm_promote_consignee_group')->row_array();
		if (empty($item)){
			$return['msg'] = lang('没有需要导入的Email！');
		}elseif (empty($group)){
			$return['msg'] = lang('收件人分组不存在！');
		}else{
			$data = [];
			$this->load->library('form_validation');
			foreach (explode(';', $item['email']) as $val){
				$email = trim($val);
				if ($this->form_validation->valid_email($email) === false) continue;
				if ($this->db->where('email', $email)->count_all_results('crm_promote_consignee')>0) continue;
				$data[] = array(
					'name'        => truncate(str_replace(["'","\""], "", $item['custom']), 15, ''),
					'phone'       => '',
					'email'       => $email,
					'address'     => '',
					'gender'      => '-',
					'group_id'    => $group_id,
					'client_code' => '外部',
					'verify'      => '否',
					'create_by'   => get_session('id'),
					'create_time' => date('Y-m-d H:i:s'),
					'update_by'   => get_session('id'),
					'update_time' => date('Y-m-d H:i:s'),
				);
			}
			if (empty($data)){
				$return['msg'] = lang('重复数据，无法成功导入!');
			}elseif ($this->db->insert_batch('crm_promote_consignee', $data)){
				$return['code'] = 1;
			}else{
				$return['msg'] = lang('Query failed!');
			}
		}
		return $this->output->set_content_type('application/json')->set_output(json_encode($return));
	}

	/**
	 * 根据当前用户获取所能看到的所有权限
	 */
	private function get_user_role_tree(){
		$dataType = getValue('dataType', '0');
		$userId = get_session('id');
		$userRange = filter_unique_array(array_merge(get_session('user_range'), array($userId)));
		$groupRange = filter_unique_array(explode(',', get_session('group_range')));

		// 获取所有相关的user_range数据
		$where = "id in (" . join(',', $userRange) . ") and (FIND_IN_SET('sales', user_role) or FIND_IN_SET('customer_service', user_role))";
		$this->db->select("id as value,name, (select id from bsc_group where bsc_group.group_code = bsc_user.group) as parent_id");
		$users = $this->bsc_user_model->get($where);
		foreach ($users as &$user){
			$user['is_use'] = true;
			$user['disabled'] = false;
			$user['type'] = 'user';
		}

		//首先将所有组
		Model('bsc_group_model');
		$this->db->select("id as idk,parent_id,group_code as value,group_name as name,group_type");
		$group_where = "";
		$groups = $this->bsc_group_model->get($group_where);
		//不需要用户的版本

		foreach ($groups as $key => $group){
			if($group['group_type'] == '分公司') $groups[$key]['type'] = 'sub_company';
			else if($group['group_type'] == '总部') $groups[$key]['type'] = 'head';
			else $groups[$key]['type'] = 'group';
			$groups[$key]['is_use'] = false;
			$groups[$key]['disabled'] = true;
			if($group['group_type'] != '总部')$groups[$key]['state'] = 'closed';
			//如果当前用户有组权限,或是管理员,就能使用
			if(in_array($group['value'], $groupRange) || is_admin()) {
				$groups[$key]['is_use'] = true;
				$groups[$key]['disabled'] = false;
			}
			$groups[$key]['children'] = array();
		}
		if($dataType === '0'){
			$groups = array_merge($groups, $users);
		}

		//将相关的部门全部获取
		$data = $this->getGroupTree($groups);
		return $data;
	}

	private function getGroupTree($groups, $parent_id = 0){
		$data = array();
		foreach($groups as $key => $group){
			if($group['parent_id'] == $parent_id){
				//用户不处理
				if($group['type'] != 'user'){
					$group['children'] = $this->getGroupTree($groups,$group['idk']);

					if(empty($group['children'])) continue;
					// if(empty($group['children'])) unset($group['children']);
				}
				$data[] = $group;
			}
		}

		return $data;
	}

	public function skip_emails(){
		$this->load->view('head');
		$this->load->view('biz/crm/crm_promote_mail/skip_emails');
	}
	public function get_skip_data()
	{
		$op = $this->load->database('china',true);
		$result = array();
		$page = isset($_POST['page']) ? intval($_POST['page']) : 1;
		$rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
		$sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
		$order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

		$offset = ($page - 1) * $rows;
		$result["total"] = $op->count_all_results('crm_promote_mail_skip');
		$op->limit($rows, $offset);
		$rs = $op->order_by($sort,$order)->get('crm_promote_mail_skip')->result_array();
		$rows = array();
		foreach ($rs as $row) {
			$row['created_by_name'] = getUserField($row['created_by']);
			$row['updated_by_name'] = getUserField($row['updated_by']);
			array_push($rows, $row);
		}
		$result["rows"] = $rows;
		echo json_encode($result);
	}

	public function add_skip_data()
	{
		$op_write = $this->load->database('china',true);
		$field = ['email_domain'];
		$data = array();
		foreach ($field as $item) {
			$temp = isset($_POST[$item]) ? trim(ts_replace($_POST[$item])) : '';
			$data[$item] = $temp;
		}
		$data['updated_time'] = $data['created_time'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $data['created_by'] = get_session('id');
		$op_write->insert('crm_promote_mail_skip',$data);
		$data['id'] = $op_write->insert_id();
		$data['created_by_name'] = $data['updated_by_name'] = getUserField(get_session('id'));
		echo json_encode($data);
	}

	public function update_skip_data()
	{
		$op = $this->load->database('china',true);
		$op_write = $this->load->database('china',true);
		$id = $_REQUEST["id"];
		$old_row = $op->where('id', $id)->get('crm_promote_mail_skip')->row_array();

		$field = ['email_domain'];

		$data = array();
		foreach ($field as $item) {
			$temp = isset($_POST[$item]) ? trim(ts_replace($_POST[$item])) : '';
			if ($old_row[$item] != $temp)
				$data[$item] = $temp;
		}
		$data['updated_time'] = date('Y-m-d H:i:s');
		$data['updated_by'] = get_session('id');
		$op_write->where('id',$id)->update('crm_promote_mail_skip', $data);
		$data['id'] = $id;
		$data['updated_by_name'] = getUserField(get_session('id'));
		echo json_encode($data);
	}

	public function delete_skip_data()
	{
		$op = $this->load->database('china',true);
		$op_write = $this->load->database('china',true);
		$id = intval($_REQUEST['id']);
		$old_row = $op->where('id', $id)->get('crm_promote_mail_skip')->row_array();
		$op_write->where('id',$id)->delete('crm_promote_mail_skip');
		return jsonEcho(array('code' => 0, 'msg' => '删除成功'));
	}

}
