<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
    <meta name="keywords" content="">
    <meta name="viewport" content="width=1180">
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css"/>
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <style>
        .search_box {
            border: 1px solid #ddd;
            float: left;
            margin: 10px 0px 0px;
            padding: 15px;
            background-color: #edf2f3;
            width: 99%;
        }
        .selected-td{ background: #ff4c42 !important; }
        .layui-table td, .layui-table th {
            vertical-align: top;
        }
    </style>
</head>
<script>

</script>
<body>
<?php if(getValue('clue_pure_text')){ ?>
<button class="layui-btn layui-btn-sm layui-btn-normal" id="btn" onclick="get_report_clue()" style="margin: 10px;">点击获取线索</button>
<?php } ?>
<table class="layui-table" style="margin: 0px;">
    <tbody>
    <?php
    foreach($data as $item):
    ?>
    <tr>
        <td>
            <!--24小时内的新线索-->
            <?php if (time() - strtotime($item['created_time']) < 86400):?>
                <span style="color: #ff2b00; font-weight: bolder">[新线索]</span>
            <?php endif;?>
            <span style="color: #1a9ed6; font-weight: bolder">clue name：<?=$item['clue'];?></span>
            <br>
<!--            <b>customer：</b>--><?//=$item['customer'];?><!--<br>-->
<!--            <b>shipper：</b>--><?//=$item['shipper'];?><!--<br>-->
<!--            <b>consignee：</b>--><?//=$item['consignee'];?><!--<br>-->
<!--            <b>notify：</b>--><?//=$item['notify'];?>
        </td>
        <td style="min-width: 220px;">
            <b>POL：</b><?=$item['pol'];?><br>
            <b>POD：</b><?=$item['pod'];?><br>
            <span style="color: #d63f04; font-weight: bolder">ETD：<?=$item['etd'];?></span><br>
            <b>created_by：</b><?=$item['created_name'];?><br>
            <b>created_time：</b><?=$item['created_time'];?><br>
        </td>
        <td style="min-width: 200px;">
            <b>vessel：</b><?=$item['vessel'];?><br>
            <b>export_sailing：</b><?=$item['export_sailing'];?><br>
            <b>trans_mode：</b><?=$item['trans_mode'];?><br>
            <b>product_type：</b><?=$item['product_type'];?><br>
            <b>product_details：</b><?=$item['product_details'];?>
        </td>
        <td style="width: 200px;">
            <b>remark：</b><?=$item['remark'];?>
        </td>
    </tr>
    <?php endforeach;?>
    </tbody>
</table>
</body>

<script type="text/javascript" src="/inc/third/layui/layui.js"></script>
<script type="text/javascript">

    $(function () {
        $('#btn').click(function () {
            //获取report系统的 数据, 自动更新为线索
            // function get_report_clue(){
                ajaxLoading();
                $.ajax({
                    type:'post',
                    url:'/biz_crm/get_report_clue',
                    data:{
                        company_search_en:'<?= getValue('clue_pure_text'); ?>',
                    },
                    dataType:'json',
                    success:function (res) {
                        ajaxLoadEnd();
                        layer.alert(res.msg,{icon:5});
                        if(res.code == 0){

                        }
                    },error:function (e) {
                        ajaxLoadEnd();
                        layer.alert(e.responseText,{icon:5});
                    }
                })
            // }
        });

        layui.use(['element', 'form'], function () {
             form = layui.form;
        });
    })
</script>