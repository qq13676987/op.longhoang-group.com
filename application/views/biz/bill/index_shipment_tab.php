<script>
    $(function(){
        $('#tb').tabs({
            border:false,
            onSelect:function(title){
                if (title == "POD"){
                    if (document.getElementById("pod_sub").src==""){
                        <?php if($shipment['biz_type'] == 'export'):?>
                        document.getElementById("pod_sub").src="/biz_bill/index1/shipment/<?=$pod_id?>?site=<?=$pod_site?>&is_tab=1";
                        <?php endif;?>
                        <?php if($shipment['biz_type'] == 'import'):?>
                        document.getElementById("pod_sub").src="/biz_bill/index/shipment/<?=$pod_id?>?site=<?=$pod_site?>&is_tab=1";
                        <?php endif;?>
                    }
                }
                if (title == "POL"){
                    if (document.getElementById("pol_sub").src==""){
                        <?php if($shipment['biz_type'] == 'export'):?>
                        document.getElementById("pol_sub").src="/biz_bill/index/shipment/<?=$pol_id?>?site=<?=$pol_site?>&is_tab=1";
                        <?php endif;?>
                        <?php if($shipment['biz_type'] == 'import'):?>
                        document.getElementById("pol_sub").src="/biz_bill/index1/shipment/<?=$pol_id?>?site=<?=$pol_site?>&is_tab=1";
                        <?php endif;?>
                    }
                }
            }

        });
        <?php if($shipment['biz_type'] == 'export'):?>
        $('#pol_sub').attr('src','/biz_bill/index/shipment/<?=$pol_id?>?site=<?=$pol_site?>&is_tab=1')
        $('#tb').tabs('select','POL')
        <?php endif;?>
        <?php if($shipment['biz_type'] == 'import'):?>
        $('#pod_sub').attr('src','/biz_bill/index/shipment/<?=$pod_id?>?site=<?=$pod_site?>&is_tab=1')
        $('#tb').tabs('select','POD')
        <?php endif;?>
    })
</script>
<div  id="tb" class="easyui-tabs" style="width:100%;height:680px">
    <div title="POL" style="padding:10px">
        <iframe scrolling="auto" frameborder="0" id="pol_sub" style="width:100%;height:626px;"></iframe>
    </div>
    <div title="POD" style="padding:10px">
        <iframe scrolling="auto" frameborder="0" id="pod_sub" style="width:100%;height:626px;"></iframe>
    </div>
</div>