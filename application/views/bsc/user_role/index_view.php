<style>
    .box{
        width: 100%;
        height: 100vh;
    }
    .user_role_tree{
        width: 100%;
        height: 100%;
    }
    .menu_tree{
        width: 15%;
        height:100%;
    }
    .other_info{
        width: 40%;
        height: 100%;
    }
    .read_role_tree{
        width: 15%;
        height:100%;
    }
    .edit_role_tree{
        width: 15%;
        height:100%;
    }
    .user_role_info{
        width: 80%;
        height: 100%;
        display: flex;
        flex-direction: column;
    }
    .user_role_info_top{
        width: 100%;
        height: 32px;
    }
    .user_role_info_bottom{
        width: 100%;
        display: flex;
        flex-direction: row;
    }
    .user_role_info_bottom>div{
        border-right: 1px solid black;
        height: 100%;
    }
    .menu_tree{
        width: 30%;
    }
    .auth_tree{
        width: 15%;
    }
    .read_consol_tree{
        height: 100%;
    }
    .title{
        width:100%;
        height: 30px;
        background-color: #6699CC;
        text-align: center;
        line-height: 30px;
        font-weight: bold;
        font-size: 15px;
        border: 1px solid;
    }
    .hide{
        display: none;
    }
</style>
<script>

    var is_update = false;

    window.onbeforeunload = function (e) {
        // 兼容IE8和Firefox 4之前的版本
        //检测是否有字段变动
        e = e || window.event;
        var dialogText = '您有未保存的内容,是否确认退出';
        if(is_update){
            if (e) {
                e.returnValue = dialogText;
            }
            // Chrome, Safari, Firefox 4+, Opera 12+ , IE 9+
            return dialogText;
        }
    };
    /**
     * 载入表单的内容(由于不是表单控件,这里变成了纯树,所以数据那些需要重新封装)
     */
    function load_form(data){
        is_load = true;
        $('#id').val(data.id);
        $('#user_role').textbox('setValue', data.user_role);
        $('#pid').val(data.pid);
        $('#level').val(data.level);
        set_tree_data('read_consol_tt', data.consol_text_read);
        set_tree_data('edit_consol_tt', data.consol_text_edit);
        set_tree_data('read_client_tt', data.client_text_read);
        set_tree_data('edit_client_tt', data.client_text_edit);
        set_tree_data('read_bill_tt', data.bill_text_read);
        set_tree_data('edit_bill_tt', data.bill_text_edit);
        set_tree_data('read_shipment_tt', data.shipment_text_read);
        set_tree_data('edit_shipment_tt', data.shipment_text_edit);
        set_tree_data('template_download_tt', data.template_text);
        set_tree_data('menu_tt', data.menu);
        
        is_load = false;
    }

    function add(type = 0) {
        clear_form();
        $('#user_role').textbox('enable');
        //赋值PID
        if(type === 1){
            $('#pid').val(selected_node.id);
        }

    }

    function clear_form() {
        load_form({});
    }
    
    var is_load = false;
    
    /**
     * 保存树的数据
     */
    function set_tree_data(id, data) {
        if(typeof data == 'string'){
            data = data.split(',');
        }
        //首先获取树全部节点
        var inp = $('#' + id);
        var tree_data = inp.tree('getRoots');
        function each_check(array, values){
            //遍历后,如果当前节点的ID存在于data中,那么select
            $.each(array, function (i, it) {
                var node = inp.tree('find', it.id);
                if(it.children != undefined){
                    if($.inArray(it.id, values) !== -1) inp.tree('check', node.target);
                    else inp.tree('uncheck', node.target);
                    each_check(it.children, values);
                }else{
                    
                    if($.inArray(it.id, values) !== -1) {
                        
                        if(it.id == 9) console.log(node);
                        if(id == 'menu_tt')check_node_check_parent(id, node);
                        else inp.tree('check', node.target);
                    }
                    else inp.tree('uncheck', node.target);
                }
            });
        }
        //遍历后,如果当前节点的ID存在于data中,那么select
        each_check(tree_data, data);
    }
    
    /**
     * 选中节点的时候，把父节点一起选中
     */
    function check_node_check_parent(id, node){
        var inp = $('#' + id);
        if(node == null) return false;
        inp.tree('check', node.target);
        
        //获取父节点
        var parent = inp.tree('getParent', node.target);
        check_node_check_parent(id, parent);
    }
    
    /**
     * 选中节点时候把子节点全部选中
     */
     function check_node_check_chilrent(id, node){
        var inp = $('#' + id);
        if(node == null) return false;
        
        inp.tree('check', node.target);
        
        if(node.children != undefined){
            $.each(node.children, function(i, it){
                var this_node = inp.tree('find', it.id);
                check_node_check_chilrent(id, this_node);
            });
        }
     }
     
     /**
      * 如果有子节点，且当前节点取消，那么取消所有子节点
      */
     function uncheck_node_uncheck_chilren(id, node){
        var inp = $('#' + id);
        if(node == null) return false;
        inp.tree('uncheck', node.target);
        if(node.children == undefined) return false;
        
        $.each(node.children, function(i, it){
            var this_node = inp.tree('find', it.id);
            uncheck_node_uncheck_chilren(id, this_node);
        });
     }
     
     /**
     * 选中节点时,只选中父节点,不触发全选子节点
     */
    function check_node_only_check_parent(id, node) {
        var inp = $('#' + id);
        if(node == null) return false;
        inp.tree('check', node.target);

        var parent = inp.tree('getParent', node.target);
        //判断是否有父节点
        check_node_only_check_parent(id, parent);
    }

    var check_chilrent = true;//这个是用于不想批量选中子的时候使用
    /**
     * 目录表的选中事件
     */
    function checked_menu(node, checked) {
        if(is_load) return true;
        
        var inp = $('#menu_tt');
        is_update = true;
        if(checked){
            if(node.children != undefined && check_chilrent){
                check_node_check_chilrent('menu_tt', node);
                return true;
            }
            var parent = inp.tree('getParent', node.target);
            if(parent != null){
                check_chilrent = false;
                check_node_only_check_parent('menu_tt', parent);
                check_chilrent = true;
                return true;
            }
        }else{
            //如果有子节点，且当前节点取消，那么取消所有子节点
            if(node.children != undefined){
                uncheck_node_uncheck_chilren('menu_tt', node);
            }
        }
    }

    //return an array of values that match on a certain key
    function getValues(obj, key) {
        var objects = [];
        for (var i in obj) {
            if (!obj.hasOwnProperty(i)) continue;
            if (typeof obj[i] == 'object') {
                objects = objects.concat(getValues(obj[i], key));
            } else if (i == key) {
                objects.push(obj[i]);
            }
        }
        return objects;
    }


    /**
     * 提交表单的数据
     */
    function submit_form(){
        $('#save_form').form('submit', {
            url: '/bsc_user_role/save_data',
            onSubmit: function (param) {
                //需要特殊处理的值
                var text_array = ['consol_tt', 'shipment_tt', 'client_tt', 'bill_tt'];
                var text_values = new Array(4);
                $.each(text_array, function (t_index, val) {
                    var edit_val = getValues($('#edit_' + val).tree('getChecked'), 'id');
                    var read_val = getValues($('#read_' + val).tree('getChecked'), 'id');

                    text_values[t_index] = read_val.concat(edit_val);
                    var this_data = $('#read_' + val).tree('getRoots').concat($('#edit_' + val).tree('getRoots'));
                    $.each(this_data, function (index, item) {
                        $.each(item.children, function (i, it) {
                            //分组的标志,将该值切除后提交
                            if(it.not_add == 1){
                                var it_index = $.inArray(it.id,text_values[t_index]);
                                var is_all = false;
                                if(val === 'bill_text'){//是bill的话,如果该分支下面选择任意一个,就全选
                                    $.each(it.children, function (i2, it2) {
                                        if($.inArray(it2.id, text_values[t_index]) !== -1){
                                            is_all = true;
                                            return false;
                                        }
                                    });
                                    if(is_all){
                                        $.each(it.children, function (i2, it2) {
                                            if($.inArray(it2.id, text_values[t_index]) === -1){
                                                text_values[t_index].push(it2.id);
                                            }
                                        });
                                    }
                                }
                                if(it_index != -1){
                                    text_values[t_index].splice(it_index, 1);
                                }
                            }
                        })
                    });
                    text_values[t_index] = text_values[t_index].join(',');
                });
                param.consol_text = text_values[0];
                param.shipment_text = text_values[1];
                param.client_text = text_values[2];
                param.bill_text = text_values[3];
                var menu_val = $('#menu_tt').tree('getChecked');
                var menu = [];
                $.each(menu_val, function (i, it) {
                    menu.push(it.id);
                });
                param.menu = menu.join(',');
                
                var template_text_val = $('#template_download_tt').tree('getChecked');
                var template_text = [];
                $.each(template_text_val, function (i, it) {
                    template_text.push(it.id);
                });
                param.template_text = template_text.join(',');
                return $(this).form('validate');
            },
            success: function (res_json) {
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                is_update = false;
                if(res === undefined){
                    $.messager.alert('Tips',res_json);
                    return false;
                }
                // $('#tt').tree('reload');
                $.messager.alert('Tips', res.msg);
                if (res.code == 0) {

                } else {
                }
            }
        });
    }

    var selected_node = {};

    $(function () {
        $('#user_role_tt').tree({
            url:'/bsc_user_role/get_no_tree<?= $user_role === 0 ? '' : '?user_role=' . $user_role;?>',
            method: 'get',
            animate: true,
            onClick: function (node) {
                //判断是否有修改,有修改提示一下
                function init_form(id){
                    $('.user_role_info_top').removeClass('hide');
                    $('.user_role_info_bottom').removeClass('hide');
                    $('#save_from').form('clear');
                    $('#user_role').textbox('disable');
                    $.ajax({
                        type: 'POST',
                        url: '/bsc_user_role/get_role_one/' + id,
                        dataType: 'json',
                        success: function (res) {
                            load_form(res);
                        }
                    });
                }
                if(is_update){
                    $.messager.confirm('Tips', '有未保存的数据,是否确认切换?', function(r){
                        if(r){
                            is_update = false;
                            init_form(node.id);
                        }
                    });
                    return;
                }
                init_form(node.id);
            },
            onContextMenu:function(e, node){
                e.preventDefault();
                // 查找节点
                selected_node = node;
                $('#tt').tree('select', node.target);
                // 显示快捷菜单
                $('#mm').menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
            },
            formatter: function (node) {
                if (node.status == 1) {
                    return '<span style="color:red;">' + node.text + '</span>';
                }
                return node.text;
            }
        });

        $('#menu_tt').tree({
            url:'/bsc_menu/get_tree',
            dnd:true,
            checkbox:true,
            cascadeCheck: false,
            formatter:function(node){
                var str = '';
                if(node.display == 1){
                    str = node.name;
                }else{
                    str = '<span style="color: #666666;">' + node.name + '</span>';
                }
                if(node.is_menu == 1){
                    str += '<span style="color: red;">*</span>';
                }
                return str;
            },
            onCheck: function(node, checked){
                //勾选父的时候，子全部选中
                checked_menu(node, checked);
            },
        });

        $('#auth_tt').tree({
            data: [
                {id:'menu',text:'<?= lang('菜单')?>'},
                {id:'consol',text:'consol'},
                {id:'shipment',text:'shipment'},
                {id:'client',text:'client'},
                {id:'bill',text:'bill'},
                {id:'template_download',text:'模板下载'},
            ],
            onClick: function (node) {
                //双击后给右面的树赋值
                //先全部隐藏
                $('.tree_div').addClass('hide');
                //然后根据双击的某一个,进行加载
                if(node.id === 'consol'){
                    $('.read_consol_tree').removeClass('hide');
                    $('.edit_consol_tree').removeClass('hide');
                }else if(node.id === 'shipment'){
                    $('.read_shipment_tree').removeClass('hide');
                    $('.edit_shipment_tree').removeClass('hide');
                }else if(node.id === 'client'){
                    $('.read_client_tree').removeClass('hide');
                    $('.edit_client_tree').removeClass('hide');
                }else if(node.id === 'bill'){
                    $('.read_bill_tree').removeClass('hide');
                    $('.edit_bill_tree').removeClass('hide');
                }else if(node.id === 'bill'){
                    $('.read_bill_tree').removeClass('hide');
                    $('.edit_bill_tree').removeClass('hide');
                }else if(node.id === 'template_download'){
                    $('.template_download_tree').removeClass('hide');
                }else if(node.id === 'menu'){
                    $('.menu_tree').removeClass('hide');
                }
            }
        });
    });
</script>
<body>
    <div id="cc" class="easyui-layout box">
        <div data-options="region:'west',title:'<?= lang('user rule'); ?>',split:true,tools:'#add_btn'" style="width:230px;">
            <div id="add_btn">
                <?= $user_role === 0 ? '<a href="javascript:void(0)" class="icon-add" onclick="add()" title="save" style="margin-right:15px;"></a>' : '';?>
            </div>
            <!--总角色目录树-->
            <div class="user_role_tree">
                <ul id="user_role_tt"></ul>
            </div>
        </div>
        <div class="user_role_info" data-options="region:'center',title:'<?= lang('user role info'); ?>',tools:'#edit_btn'">
            <div class="user_role_info_top user_role_form hide">
                <form id="save_form" method="post">
                    <input type="hidden" name="id" id="id">
<!--                    <input type="hidden" name="level" id="level">-->
                    <input type="hidden" name="pid" id="pid">
                    <table>
                        <tr>
                            <td><?= lang('role');?>:</td>
                            <td>
                                <input name="user_role" id="user_role" disabled class="easyui-textbox" style="width: 90px;" data-options="required: true,">
                            </td>
                            <td>
                                <a class="easyui-linkbutton" onclick="submit_form()" style="width:90px"><?= lang('save');?></a>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="user_role_info_bottom hide">
                <!--这里再加个功能,显示为原本所有的树汇总,如点目录出现目录权限,点consol出现consol权限等-->
                <div class="auth_tree">
                    <div class="title">可配置权限</div>
                    <ul id="auth_tt">

                    </ul>
                </div>
                <!--目录树-->
                <div class="menu_tree tree_div hide">
                    <div class="title">角色对应的菜单权限</div>
                    <ul id="menu_tt"></ul>
                </div>
                <!--修改新增权限DIV-->
                <div class="read_consol_tree tree_div hide">
                    <div class="title">页面对应的数据框查看权限</div>
                    <ul id="read_consol_tt" class="easyui-tree" data-options="checkbox:true,url:'/biz_consol/table_tree/read',onCheck: function(node, checked){if(!is_load) is_update = true;}"></ul>
                </div>
                <div class="edit_consol_tree tree_div hide">
                    <div class="title">页面对应的数据框修改权限</div>
                    <ul id="edit_consol_tt" class="easyui-tree" data-options="checkbox:true,url:'/biz_consol/table_tree/edit',onCheck: function(node, checked){if(!is_load) is_update = true;}"></ul>
                </div>
                <div class="read_shipment_tree tree_div hide">
                    <div class="title">页面对应的数据框查看权限</div>
                    <ul id="read_shipment_tt" class="easyui-tree" data-options="checkbox:true,url:'/biz_shipment/table_tree/read',onCheck: function(node, checked){if(!is_load) is_update = true;}"></ul>
                </div>
                <div class="edit_shipment_tree tree_div hide">
                    <div class="title">页面对应的数据框修改权限</div>
                    <ul id="edit_shipment_tt" class="easyui-tree" data-options="checkbox:true,url:'/biz_shipment/table_tree/edit',onCheck: function(node, checked){if(!is_load) is_update = true;}"></ul>
                </div>
                <div class="read_client_tree tree_div hide">
                    <div class="title">页面对应的数据框查看权限</div>
                    <ul id="read_client_tt" class="easyui-tree" data-options="checkbox:true,url:'/biz_client/table_tree/read',onCheck: function(node, checked){if(!is_load) is_update = true;}"></ul>
                </div>
                <div class="edit_client_tree tree_div hide">
                    <div class="title">页面对应的数据框修改权限</div>
                    <ul id="edit_client_tt" class="easyui-tree" data-options="checkbox:true,url:'/biz_client/table_tree/edit',onCheck: function(node, checked){if(!is_load) is_update = true;}"></ul>
                </div>
                <div class="read_bill_tree tree_div hide">
                    <div class="title">页面对应的数据框查看权限</div>
                    <ul id="read_bill_tt" class="easyui-tree" data-options="checkbox:true,url:'/biz_bill/table_tree/read',onCheck: function(node, checked){if(!is_load) is_update = true;}"></ul>
                </div>
                <div class="edit_bill_tree tree_div hide">
                    <div class="title">页面对应的数据框修改权限</div>
                    <ul id="edit_bill_tt" class="easyui-tree" data-options="checkbox:true,url:'/biz_bill/table_tree/edit',onCheck: function(node, checked){if(!is_load) is_update = true;}"></ul>
                </div>
                <div class="template_download_tree tree_div hide">
                    <div class="title">页面对应的数据框修改权限</div>
                    <ul id="template_download_tt" class="easyui-tree" data-options="checkbox:true,url:'/bsc_template/get_template',onCheck: function(node, checked){if(!is_load) is_update = true;}"></ul>
                </div>
            </div>
        </div>
    </div>

    <!--    右键菜单定义如下：-->
    <div id="mm" class="easyui-menu" style="width:120px;">
        <div onclick="add(1)" data-options="iconCls:'icon-add'">新增节点</div>
    </div>
</body>