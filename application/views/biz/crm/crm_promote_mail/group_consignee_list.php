<div style="position: fixed; top: 10px; left: 30px; font-size: 13px; color: red;"><?= lang('收件人组名称');?>：<?=$group_row['group_name'];?></div>
<div align="center"><h3><?= lang('2、点击“{a}”开始创建群发内容', array('a' => '<a href="/crm_promote_mail/add_presend/?group_id='.$group_id.'">' . lang('下一步') . '</a>'));?></h3></div>
<!--组收件人列表-->
<table id="dg" class="easyui-datagrid" title="<?= lang('组收件人 组名');?>" data-options="
        singleSelect:true,
        url:'/crm_promote_mail/group_consignee_list_get_data?group_id=<?php echo $group_id?>',
        method:'get',
        pagination:true,
        toolbar: '#toolbar',
        pageSize:20,
        fitColumns: true,
        rowStyler: function(index,row){
            var str = '';
            if (row.ok == -1){
                str += 'color:red;';
            }else if(row.ok == 1){
                str += 'color:green;';
            }
            return str;
        }
   ">
	<thead>
	<tr>
		<th data-options="field:'id',width:20"><?= lang('ID');?></th>
		<th data-options="field:'company_name',width:80"><?= lang('客户名称');?></th>
		<th data-options="field:'name',width:50"><?= lang('Name');?></th>
		<th data-options="field:'phone',width:50"><?= lang('手机号');?></th>
		<th data-options="field:'email',width:100"><?= lang('email');?></th>
		<th data-options="field:'address',width:100"><?= lang('地址');?></th>
		<!--<th data-options="field:'verify',width:100"><?= lang('邮件是否验证');?></th>-->
		<th data-options="field:'operate',width:100"><?= lang('操作');?></th>

	</tr>
	</thead>
</table>
<div id="toolbar">
	<div>
		<!--<a href="/crm_promote_mail/add_consignee?group_id=<?php echo $group_id?>" class="easyui-linkbutton" iconCls="icon-add" plain="true">新增收件人</a>-->
		<a href="/crm_promote_mail/add_consignees?group_id=<?php echo $group_id?>" class="easyui-linkbutton" iconCls="icon-add" plain="true"><?= lang('新增收件人(批量)');?></a>
		<a href="/crm_promote_mail/import_email?group_id=<?php echo $group_id?>" target="_blank" style="margin-right: 20px;" class="easyui-linkbutton" iconCls="icon-add" plain="true"><?=lang('Import Excel');?></a>
		<?= lang('邮箱');?>:<input class="easyui-textbox" id="email">
		<button onclick="doSearch()" type="button"><?= lang('查询');?></button>
		<!--<a href="/other/force_pass_email" class="easyui-linkbutton" plain="true" target="_blank"><?= lang('手动验证邮箱'); ?></a>-->
		<span style="color:red;"><?= lang('!!!红色代表验证失败,绿色验证通过,黑色的正在等待验证.');?></span>
		<a href="javascript:del_consignee_invalid();" class="easyui-linkbutton" iconCls="icon-remove" plain="true"><?= lang('一键删除所有红色邮箱');?></a>
	</div>

</div>
<script type="text/javascript" src="/inc/layer3.1.1/layer.js"></script>
<script type="text/javascript" src="/inc/third/wangEditor/wangEditor.min.js"></script>
<script>
    function doSearch(){
        var json = {};
        json.email = $('#email').textbox('getValue');

        $('#dg').datagrid('load', json);
    }

    function delConsig(id){
        layer.confirm('<?= lang('您确定要删除收件人？');?>', {
            btn: ['<?= lang('确定');?>','<?= lang('取消');?>'] //按钮
        }, function()
        {
            layer.closeAll('dialog');
            $.getJSON("/crm_promote_mail/del_consignee?id="+id,function(data){

                if(data.code==200){
                    layer.msg(data.msg);
                    $('#dg').datagrid('reload');
                }

            });

        });


    }

    function del_consignee_invalid(){
        layer.confirm('<?= lang('您确定要一键删除所有红色email地址？');?>', {
            btn: ['<?= lang('确定');?>','<?= lang('取消');?>'] //按钮
        }, function()
        {
            layer.closeAll('dialog');
            $.getJSON("/crm_promote_mail/del_consignee_invalid?group_id=<?php echo $group_id;?>",function(data){
                if(data.code==200){
                    layer.msg(data.msg);
                    $('#dg').datagrid('reload');
                }
            });
        });
    }

</script>
