<?php

class biz_shipment extends Menu_Controller
{

    protected $admin_field = array('sales', 'operator', 'customer_service');//, 'finance');
	public $shipment_must_field = array('client_company','client_contact', 'client_email','shipper_company', 'consignee_company', 'notify_company', 'shipper_address', 'consignee_address', 'trans_mode', 'biz_type',
            'trans_tool', 'booking_ETD','trans_destination', 'status', 'hbl_type', 'release_type', 'trans_term',
            'INCO_term', 'description', 'mark_nums', 'good_outers_unit', 'goods_type', 'hs_code','trans_carrier','trans_origin','trans_discharge','sailing_area','description','good_outers','good_weight','good_volume');
    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_shipment_model');
        $this->load->model('bsc_user_model');
        $this->load->model('bsc_user_role_model');
        $this->load->helper('cz_consol');
        $this->load->helper('cz_container');
        $this->load->helper('cz_shipment');

        $this->field_all = $this->biz_shipment_model->field_all;
        $this->admin_field = $this->biz_shipment_model->admin_field;

        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            $this->lock_filed[$row[4]][] = $v;
            array_push($this->field_edit, $v);
        }
        foreach ($this->admin_field as $val){
            $this->lock_filed[1][] = $val;
        }
    }

    //原本的field_all到model里配置

    public $lock_filed = array();

    public $field_edit = array();

    public function index()
    {
        $data = array();
        $userRole = Model('bsc_user_role_model')->get_user_role(array('edit_text', 'delete_text'));

        $data['G_userBscEdit'] = explode(',', $userRole['edit_text']);

        $this->load->view('head');
        $this->load->view('biz/shipment/index_view', $data);
    }

    public function index_by_consol($consol_id = 0)
    {
        $data = array();
        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_shipment_by_consol_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            //提单号 件毛体（实际）收发通 品名  提单类型 销售 操作 客服
            $this->field_all = array(
                array("cus_no", "cus_no", "140", "9", '1'),
                array("job_no", "job_no", "120", "9", '1'),
                array('container_packs', 'container_packs', '60', '9', '1'),
                array('container_weight', 'container_weight', '60', '9', '1'),
                array('container_volume', 'container_volume', '60', '9', '1'),
                array('good_outers', 'good_outers', '60', '9', '1'),
                array('good_weight', 'good_weight', '60', '9', '1'),
                array('good_volume', 'good_volume', '60', '9', '1'),
                array('goods_type', 'goods_type', '80', '9', '1'),
                array('hs_code', 'hs_code', '80', '9', '1'),
                array('trans_term', 'trans_term', '80', '9', '1'),
                array("shipper_company", "shipper_company", "200", "7", '1', 'booking info'),
                array("consignee_company", "consignee_company", "200", "7", '1', 'booking info'),
                array("notify_company", "notify_company", "150", "7", '1', 'booking info'),
                array("description", "description", "180", "9", '1'),
                array("hbl_type", "hbl_type", "60", "9", '1'),

                array("sales", "sales", "60", "9", '1'),
                array("operator", "operator", "60", "9", '1'),
                array("customer_service", "customer_service", "60", "9", '1'),
            );
            $data["f"] = $this->field_all;
        }
        // page account
        $rs = $this->sys_config_model->get_one('biz_shipment_by_consol_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }

        $this->load->model('biz_consol_model');
        $data["consol"] = $this->biz_consol_model->get_by_id($consol_id);
        $data["consol_id"] = $consol_id;
        $this->load->view('head');
        $this->load->view('biz/shipment/index_by_consol_view', $data);
    }

    public function get_by_consol_data($consol_id = '', $parent_id = 0){
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 300;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------这个查询条件想修改成通用的 -------------------------------
        $where = array();

        //-----------------------------------------------------------------
        if ($consol_id != "" && $consol_id != 0 && empty($where)) {
            if ($where == "") {
                $where[] = "consol_id = $consol_id";
            } else {
                $where[] = "consol_id = $consol_id";
            }
        }
        $unmerge = false;
        if($consol_id == 0){
            if($parent_id == 0)$unmerge = true;
            $where[] = "consol_id = 0";
            //2021-06-07 汪庭彬 未绑定的直接锁 http://wenda.leagueshipping.com/?/question/165 bill里的批量锁
            $where[] = "biz_shipment.lock_lv = 0";
            $where[] = "biz_shipment.status = 'normal'";
        }
        $where[] = "biz_shipment.parent_id = $parent_id";
        $where = join(' and ', $where);

        $offset = ($page - 1) * $rows;
        if($consol_id != 0)$this->db->limit($rows, $offset);
        $this->db->select("biz_shipment.*," .
//            "biz_consol.job_no as consol_no, biz_consol.carrier_ref, biz_consol.vessel, biz_consol.voyage," .
            "(select group_concat(bsc_user.name) from bsc_user where bsc_user.id in (select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'sales')) as sales," .
            "(select group_concat(bsc_user.name) from bsc_user where bsc_user.id in (select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'operator')) as operator," .
            "(select group_concat(bsc_user.name) from bsc_user where bsc_user.id in (select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'customer_service')) as customer_service");
//        $this->db->join("biz_consol", "biz_consol.id = biz_shipment.consol_id");
        $rs = $this->biz_shipment_model->no_role_get($where, $sort, $order, $unmerge);

        //        $this->db->join("biz_consol", "biz_consol.id = biz_shipment.consol_id");
        $result["total"] = $this->biz_shipment_model->no_role_total($where, $unmerge);
        //$result['sq'] = $this->db->last_query();
        $rows = array();

        Model('biz_shipment_container_model');
        foreach ($rs as $row) {
            //获取箱数据
            $container_sum = $this->biz_shipment_container_model->get_container_sum($row['id'], array('packs', 'weight', 'volume'));
            $row['container_packs'] = $container_sum['packs'];
            $row['container_weight'] = $container_sum['weight'];
            $row['container_volume'] = $container_sum['volume'];

            //查询有几个子shipment
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add($id = 0)
    {
		$system_type = get_system_type();
		if(in_array($system_type,array("sg"))) exit("please use new system http://u$system_type.longmerchant-in.com/main/login");
        pass_role(array(), $this->admin_field);
        $consol_id = isset($_REQUEST["consol_id"]) ? $_REQUEST["consol_id"] : "";
        //将需要的duty字段加入----start
        foreach ($this->admin_field as $val){
            $this->field_edit[] = $val . '_id';
            $this->field_edit[] = $val . '_group';
        }
        //将需要的duty字段加入----end
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $data[$item] = "";
        }
        $data['id'] = $id;

        if ($id != 0) {
            $data = $this->biz_shipment_model->get_duty_one("id = '{$id}'");
            //duty数据，重新获取的话，取最新的组
            foreach ($this->admin_field as $val){
                $data[$val . '_group'] = getUserField($data[$val . '_id'], 'group');
            }
        }
        $data["cus_no"] = '';
        $ebooking_id = (int)getValue('ebooking_id', 0);
        //如果存在Ebooking时,自动带上数据过来
        $data['ebooking_id'] = $ebooking_id;
        if($ebooking_id){
            Model('biz_shipment_ebooking_model');
            $ebooking = $this->biz_shipment_ebooking_model->get_one('id', $ebooking_id);
            //这里状态得卸载掉,不然重名了
            if(!empty($ebooking)){
                $ebooking_status = $ebooking['status']; unset($ebooking['status']);
                if($ebooking_status == '0'){// 必须是待审核
                    $data['ebooking_id'] = $ebooking_id;
                    //目前只重置下面的字段
                    $ebooking_field = array('sales_id', 'sales_group', 'customer_service_id', 'customer_service_group', 'client_code', 'client_code2','client_company', 'operator_id', 'operator_group', 'shipper_ref',
                        'hbl_type', 'trans_origin' ,'trans_origin_name', 'trans_discharge' ,'trans_discharge_name', 'trans_destination_inner', 'trans_destination', 'trans_destination_name', 'trans_destination_terminal', 'trans_term', 'INCO_term', 'requirements',
                        'biz_type', 'trans_mode', 'trans_tool','box_info', 'goods_type', 'service_options','trans_carrier', 'booking_ETD', 'client_contact', 'client_telephone', 'client_email','cus_no', 'INCO_address'
                    );
                    foreach ($ebooking_field as $field){
                        //$data[$field], 
                        if(isset($ebooking[$field])){
                            $data[$field] = $ebooking[$field];
                        }
                    }
                    //根据当前目的港代码, 手动获取航线区域
                    if(isset($data['trans_destination_inner'])){
                        $dis_port = Model("biz_port_model")->get_where_one("carrier = 'inner_use' and port_code = '{$data['trans_destination_inner']}'");
                        $data['sailing_area'] = $dis_port['sailing_area'];
                        $data['sailing'] = $dis_port['sailing'];
                    }

                    //2022-03-17 由于Ebooking里没有字段的,所以这里读取client的地址等信息填充进去 联系人相关数据获取时,自动填充
                    $this->db->select('id,client_code,company_address,contact_name,contact_telephone2,contact_email');
                    $client = Model('biz_client_model')->get_one('client_code', $ebooking['client_code']);
                    if(!empty($client)){
                        $data['client_address'] = $client['company_address'];
                    }
                    //这里由于需要将航线区域预填写进去,不然新增时会空着,所以手动查询补上
                    $port = Model('biz_port_model')->get_where_one("port_code = '{$ebooking['trans_destination']}' and carrier = '{$data['trans_carrier']}'");
                    
                    if(!empty($port)){
                        $data['sailing_area'] = $port['sailing_area'];
                        $data['sailing'] = $port['sailing'];
                    }

                }else{
                    echo "ebooking状态必须为未审核";
                    return;
                }
            }
        }

        $data["status"] = 'normal';
        $data["consol_id"] = $consol_id;
        //设置页面需要的userList
        foreach ($this->admin_field as $val){
            $data['userList'][$val] = $data[$val . '_id'];
        }
        $data['INCO_option'] = explode(',', $data['INCO_option']);
        $data['box_info'] = json_decode($data['box_info'], true);

        $this->load->view('head');
        $this->load->view('biz/shipment/add_ajax_view', $data);
    }

    public function edit($id = 0)
    {
        $shipment = $this->biz_shipment_model->get_duty_one("id = '{$id}'");
        if(empty($shipment)){
            echo lang('SHIPMENT 不存在');
            return;
        }
        $data = $shipment;
        $consol = Model('biz_consol_model')->get_duty_one("id = '{$shipment['consol_id']}'");
        if(!empty($consol)){
            $this->db->select('client_code,company_name');
            $this_client_codes = array($consol['creditor']);
            $client = Model('biz_client_model')->no_role_get("client_code in ('" . join("','", $this_client_codes) . "')");
            $client_key_code = array_column($client, null, 'client_code');
            if(isset($client_key_code[$consol['creditor']]))$consol['creditor'] = $client_key_code[$consol['creditor']]['company_name'];
        }else{
            $consol['operator_si'] = '0';
            $consol['carrier_ref'] = '';
            $consol['document_si'] = '0';
            $consol['creditor'] = '';
            $consol['lock_lv'] = 0;
            $consol['closing_date'] = '';
        }

        pass_role($data, $this->admin_field);
        $data['issue_date'] = $data['issue_date'] == '0000-00-00' ? null : $data['issue_date'];

        $userId = get_session('id');
        $userBsc = Model('bsc_user_model')->get_by_id($userId);

        $userRole = Model('bsc_user_role_model')->get_data_role();
        $userBsc = array_merge($userBsc, $userRole);

        //2022-08-23 获取parent_id
        $parent_id=$data['parent_id'];

        $must_field = array('id', 'job_no', 'consol_no', 'created_time','created_by','updated_by','updated_time', 'show_fields', 'invoice_flag', 'payment_flag', 'loss_flag', 'document_si', 'lock_lv');
        $data1 = data_role($data, $must_field, explode(',', $userRole['read_text']),'shipment');

//        $data1["id"] = $id;
//        $data1["lock_lv"] = $data['lock_lv'];
        //获取client_id
        $data1['client_id'] = Model('biz_client_model')->get_where_one("client_code = '{$data['client_code']}'")['id'];

        //2022-07-05 马嬿雯反馈销售看不到consol区域信息后, 这里去除字段限制
        $consol_data1 = $consol;

        $data['lock_lv'] = isset($data['lock_lv']) ? $data['lock_lv'] : 0;
        $userBscEdit1 = explode(",", (isset($userBsc['edit_text']) ? $userBsc['edit_text'] : ''));
        $userBscEdit = array();
        $lock_field = array();
        //将锁后不能修改的字段进行填充
        for ($i = 1; $i <= $data['lock_lv']; $i++){
            if(isset($this->lock_filed[$i]))$lock_field = array_merge($lock_field, $this->lock_filed[$i]);
        }

        $g_edit_table = false;
        foreach ($userBscEdit1 as $row) {
            if (strpos($row, 'shipment.') !== false) {
                $arr_temp = explode(".", $row);

                //是否处于被锁的字段，锁住无法修改
                if(!in_array($arr_temp[1], $lock_field))array_push($userBscEdit, $arr_temp[1]);
            } else if ($row == 'shipment') {
                // 全表
                $g_edit_table = true;
                break;
            }
        }
        if ($g_edit_table) {
            $userBscEdit = array_diff(array_keys($data), $lock_field);
            //填充入当前相关的人员
            $userBscEdit = array_merge($userBscEdit, $this->admin_field);
        }
        //2023-03-13 默认关闭hbl的编辑权限
        // $userBscEdit = array_diff($userBscEdit, array('hbl_no'));
        // 
        $userBscDelete = explode(",", (isset($userBsc['delete_text']) ? $userBsc['delete_text'] : ''));
        $data1['lock_lv'] = $data['lock_lv'];
        $data1['r_e'] = false;
        $data1['G_userBscEdit'] = $userBscEdit;
        $data1['G_userBscDelete'] = $userBscDelete;
        if (isset($_POST['edit_text']) || isset($_POST['read_text'])) $data1['r_e'] = true;

        $data1['dangergoods'] = array();
        //有值传值
        if (!empty($data1['dangergoods_id'])) {
            $this->load->model('biz_shipment_dangergoods_model');
            $dangergoods = $this->biz_shipment_dangergoods_model->get_one('id', $data1['dangergoods_id']);
            $data1['dangergoods'] = $dangergoods;
        }
        $data1['INCO_option'] = explode(',', $data1['INCO_option']);
        $data1['box_info'] = json_decode($data1['box_info'], true);

        //设置页面需要的userList
        foreach ($this->admin_field as $val){
            $data1['userList'][$val] = $data1[$val . '_id'];
        }
        //
        $data1['consol'] = $consol_data1;
        //获取container真实数据汇总
        $this->load->model('biz_shipment_container_model');
        $container_sum = $this->biz_shipment_container_model->get_container_sum($id, array('packs', 'weight', 'volume'));
        $data1['container_sum'] = $container_sum;

        //判断子shipment数量, 如果有,那么开启Sub SHIPMENT 标签
        $this->db->select('id');
        $child_shipment = $this->biz_shipment_model->get_where_one("parent_id = '$id'");
        $data1['child_shipment'] = $child_shipment;

        //2021-06-29 http://wenda.leagueshipping.com/?/question/230
        $shipment_check = $this->shipment_check($data);
        //2021-07-05 当箱型箱量与container不匹配时，只能修改box_info
        if(isset($shipment_check['check']['s_c_box_info'])){
            $data1['G_userBscEdit'] = array('box_info', 'cus_no','status');
        }
        //2021-11-16 consol的操作SI勾选后,MBL无法修改shipper consignee notify 等相关数据.. 
        if($consol['operator_si'] !== '0' && $shipment['hbl_type']=='MBL' && $shipment['biz_type'] == 'export'){
            $data1['operator_si_check'] = 2;
            Model('biz_shipment_si_model');
            $si_field = $this->biz_shipment_si_model->getSiField();

            $data1['G_userBscEdit'] = array_diff($data1['G_userBscEdit'], $si_field);
        }else{
            $data1['operator_si_check'] = 1;
        }

        //2022-02-16 当放单方式是OBL且放单方式备注为待通知电放时,
        if($data['release_type'] == 'OBL' && $data['release_type_remark'] == '等通知电放'){
            $data1['G_userBscEdit'] = array_merge($data1['G_userBscEdit'], array('release_type'));
        }

        //2022-04-26 提单签发勾选时,查询一下是否有申请改单记录
        $data1['document_si'] = $consol['document_si'];

        //2021-06-03 显示绑定的consol
        if(isset($data['consol_id_apply']) && $data['consol_id_apply'] != 0){
            $this->db->select('id,job_no');
            $consol_apply = $this->biz_consol_model->get_by_id($data['consol_id_apply']);
            if(empty($consol_apply))$consol_apply['job_no'] = '';
            $data1['consol_apply'] = $consol_apply['job_no'];

            //2021-09-23 汪庭彬 http://wenda.leagueshipping.com/?/question/471 当前shipment已申请配舱后，无法修改所有内容
            $data1['G_userBscEdit'] = array();
        }

        $data1['shipment_check'] = $shipment_check;

        //获取当前锁等级的日志
        Model('sys_lock_log_model');
        $this->db->select('id,(select name from bsc_user where bsc_user.id = sys_lock_log.user_id) as user_name,max(op_time) as op_time_max,op_value');
        $this->db->order_by('op_value', 'asc');
        $this->db->group_by('id_type,id_no,op_value');
        $data1['lock_log'] = $this->sys_lock_log_model->get("id_type = 'biz_shipment' and id_no = '$id'");
        $consol_lock_log = array();
        if(isset($data['consol_id'])){
            $this->db->select('id,(select name from bsc_user where bsc_user.id = sys_lock_log.user_id) as user_name,max(op_time) as op_time_max,op_value');
            $this->db->order_by('op_value', 'asc');
            $this->db->group_by('id_type,id_no,op_value');
            $consol_lock_log = $this->sys_lock_log_model->get("id_type = 'biz_consol' and id_no = '{$data['consol_id']}'");
        }
        $data1['consol']['lock_lv'] = $consol['lock_lv'];
        $data1['consol']['lock_log'] = $consol_lock_log;
		$data1['client_company2'] = '';
		$data1['client2'] = [];
		if(!empty($data1['client_code2'])){
			$company2 = $this->db->where('client_code',$data1['client_code2'])->get('biz_client')->row_array();
			if(!empty($company2)){
				$data1['client_company2'] = $company2['company_name'];
				$data1['client2'] = $company2;
			}
		}
		$data1['verify_email1'] = false;
		$data1['verify_email2'] = false;
		$data1['contact_id1'] = 0;
		$data1['contact_id2'] = 0;
		if(!empty($data1['client_email'])){
			$contact = $this->db->where("client_code = '{$data1['client_code']}' and email = '{$data1['client_email']}'")->get('biz_client_contact_list')->row_array();
			if(!empty($contact)){
				if($contact['email_verify'] == 1) $data1['verify_email1'] = true;
				if(strlen($contact['email']) < 2) $data1['verify_email1'] = true;
				$data1['contact_id1'] = $contact['id'];
			}
		}
		if(!empty($data1['client2_email'])){
			if(!empty($data1['client_code2'])){
				$contact = $this->db->where("client_code = '{$data1['client_code2']}' and email = '{$data1['client2_email']}'")->get('biz_client_contact_list')->row_array();
				if(!empty($contact)){
					if($contact['email_verify'] == 1) $data1['verify_email2'] = true;
					if(strlen($contact['email']) < 2) $data1['verify_email2'] = true;
					$data1['contact_id2'] = $contact['id'];
				}
			}
		}
        //2022-06-28 新增tabs
        $data1['tab_title']  = isset($_GET['tab_title']) ? $_GET['tab_title'] : '';
        //2022-08-23 获取parent_id
        $data1['parent_id_read']=$parent_id;
		if ($data1['from_db'] != '' && $data1['from_id_no'] && $data1['biz_type'] == 'import') {
			$data1['G_userBscEdit'] = array_diff($data1['G_userBscEdit'], array('biz_type','box_info', 'trans_carrier', 'trans_origin', 'trans_discharge', 'trans_destination', 'trans_destination_terminal','booking_ETD','customer_booking','booking_agent','release_type','hbl_no','hbl_type','sailing_area','trans_term','INCO_term','shipper_company','shipper_address','shipper_contact','shipper_telephone','shipper_email','consignee_company','consignee_address','consignee_contact','consignee_telephone','consignee_email','notify_company','notify_address','notify_contact','notify_telephone','notify_email','cus_no','trans_destination_name','trans_destination_inner','trans_discharge_name','trans_origin_name','trans_tool','agent_code','agent_company','agent_address','agent_contact','agent_telephone','agent_email','creditor','shipper_ref'));
		}
        $this->load->view('head');
        $this->load->view('biz/shipment/edit_ajax_view', $data1);
    }

    public function sub_shipment($parent_id = 0){
        $data = array();

        $data['parent_id'] = $parent_id;
        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_shipment_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }

        $this->load->view('head');
        $this->load->view('biz/shipment/sub_shipment_view', $data);
    }

    public function get_data($consol_id = "")
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $parent_id = isset($_GET['sub']) ? strval($_GET['sub']) : '';

        //-------这个查询条件想修改成通用的 -------------------------------
        $carrier_ref = isset($_REQUEST['carrier_ref']) ? trim($_REQUEST['carrier_ref']) : '';
        $label = isset($_REQUEST['label']) ? $_REQUEST['label'] : array();
        $step_info = isset($_REQUEST['step_info']) ? $_REQUEST['step_info'] : array();
        $lock_lv = isset($_REQUEST['lock_lv']) ? $_REQUEST['lock_lv'] : array();
        $consol_lock_lv = isset($_REQUEST['consol_lock_lv']) ? $_REQUEST['consol_lock_lv'] : array();

        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $where = array();
        $unmerge = true;

        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_shipment', 'biz_shipment_index');//获取相关的配置信息
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }
                //服务选项的不等于用not like 处理
                $json_field = array('service_options', 'biz_shipment.service_options');
                if(in_array($f, $json_field) && $s == '!='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} not like '%$v%'";
                    }
                    continue;
                }
                if($f == 'biz_shipment.cus_no' || $f == 'cus_no'){
                    //2021-07-27 新增关单号可查询子shipment的
                    $this->db->select('parent_id');
                    $child_shipment = $this->biz_shipment_model->get_where_one_all(search_like($f, $s, $v) . ' and parent_id != 0');
                    $cus_no_where = search_like($f, $s, $v);
                    if(!empty($child_shipment)){
                        $cus_no_where = "(" . $cus_no_where . " or biz_shipment.id = " . $child_shipment['parent_id'] . ")";
                    }
                    if($v != '') $where[] = $cus_no_where;
                    continue;
                }
                // if($f == 'biz_shipment.carrier_ref'){
                //     Model('m_model');
                //     if(trim($v) === '') continue;
                //     //2022-06-14 如果是查的MBL,那么从另一张表里获取
                //     $mbl_mores = $this->m_model->query_array("select consol_id from biz_consol_mbl_more where " . search_like('mbl_no', 'like', $v));
                //     $where[] = "(biz_consol.id in ('" . join('\',\'', array_column($mbl_mores, 'consol_id')) . "') or " . search_like('biz_consol.carrier_ref', 'like', $v) . ")";
                //     continue;
                // }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];


                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') $v = '';
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) $title_data['sql_join'][$title_data['base_data'][$title_f]['sql_join']] = $this_join;

                    $where[] = search_like($f, $s, $v);
                }
            }
        }
        // if ($carrier_ref != ''){
        //     Model('m_model');
        //     //2022-06-14 如果是查的MBL,那么从另一张表里获取
        //     $mbl_mores = $this->m_model->query_array("select consol_id from biz_consol_mbl_more where " . search_like('mbl_no', 'like', $carrier_ref));

        //     $where[] = "(biz_consol.id in ('" . join('\',\'', array_column($mbl_mores, 'consol_id')) . "') or " . search_like('biz_consol.carrier_ref', 'like', $carrier_ref) . ")";
        // }
        if($label !== array()){
            if(in_array(1, $label)) $where[] = '(select over_credit from biz_client where biz_client.client_code = biz_shipment.client_code) = 1';//扣单
            if(in_array(2, $label)) $where[] = "invoice_flag = 0";//未开票
            if(in_array(3, $label)) $where[] = "loss_flag = 1";//亏损
            if(in_array(4, $label)) $where[] = "payment_flag = 0";//未核销
        } ;//标签 扣单 亏损 未开票
        if($step_info !== array()){
            if(in_array('dadanwancheng', $step_info)) $where[] = "biz_shipment.dadanwancheng != '0'";
            if(in_array('haiguancangdan', $step_info)) $where[] = "biz_shipment.haiguancangdan != '0'";
            if(in_array('baoguanjieshu', $step_info)) $where[] = "biz_shipment.baoguanjieshu != '0'";
            if(in_array('xiangyijingang', $step_info)) $where[] = "biz_shipment.xiangyijingang != '0'";
            if(in_array('haiguanfangxing', $step_info)) $where[] = "biz_shipment.haiguanfangxing != '0'";
            if(in_array('matoufangxing', $step_info)) $where[] = "biz_shipment.matoufangxing != '0'";
            if(in_array('peizaifangxing', $step_info)) $where[] = "biz_shipment.peizaifangxing != '0'";
            if(in_array('chuanyiqihang', $step_info)) $where[] = "biz_shipment.chuanyiqihang != '0'";
            if(in_array('tidanqueren', $step_info)) $where[] = "biz_shipment.tidanqueren != '0'";
            if(in_array('tidanqianfa', $step_info)) $where[] = "biz_shipment.tidanqianfa != '0'";
            if(in_array('yizuofeiyong', $step_info)) $where[] = "biz_shipment.yizuofeiyong != '0'";
        } ;
        $lock_lv_where = array();
        foreach ($lock_lv as $val){
            $lock_lv_where[] = "biz_shipment.lock_lv = $val";
        }
        if(!empty($lock_lv_where)) $where[] = '( ' . join(' or ', $lock_lv_where) . ' )';
        $consol_lock_lv_where = array();
        foreach ($consol_lock_lv as $val){
            $consol_lock_lv_where[] = "biz_consol.lock_lv = $val";
        }
        if(!empty($consol_lock_lv_where)) $where[] = '( ' . join(' or ', $consol_lock_lv_where) . ' )';

        if($parent_id != ''){
            $unmerge = false;
            $where[] = "biz_shipment.parent_id = '$parent_id'";
        }
        $where = join(' and ', $where);
        //-----------------------------------------------------------------
        if ($consol_id != "" && $where == "") {
            if ($where == "") {
                $where = "consol_id = $consol_id";
            } else {
                $where .= " and consol_id = $consol_id";
            }
        }
        $offset = ($page - 1) * $rows;
        //        $result["total"] = $this->biz_shipment_model->total_v3($where, $unmerge);
        //获取需要的字段--start
        $selects = array(
            'biz_shipment.id',
            'biz_shipment.job_no',
            'biz_shipment.cus_no',
            'biz_shipment.client_code',
            'biz_shipment.invoice_flag',
            'biz_shipment.payment_flag',
            'biz_shipment.pay_buy_flag',
            'biz_shipment.loss_flag',
            'biz_shipment.client_company',
            'biz_shipment.good_outers',
            'biz_shipment.good_outers_unit',
            'biz_shipment.good_weight',
            'biz_shipment.good_weight_unit',
            'biz_shipment.good_volume',
            'biz_shipment.good_volume_unit',
            'biz_shipment.box_info',
            'biz_shipment.status',
            'biz_shipment.lock_lv',
            'biz_shipment.dadanwancheng',
            'biz_shipment.haiguancangdan',
            'biz_shipment.baoguanjieshu',
            'biz_shipment.xiangyijingang',
            'biz_shipment.haiguanfangxing',
            'biz_shipment.matoufangxing',
            'biz_shipment.peizaifangxing',
            'biz_shipment.chuanyiqihang',
            'biz_shipment.yizuofeiyong',
        );
        foreach ($title_data['select_field'] as $key => $val){
            if($key == 'biz_shipment.trans_origin') isset($title_data['base_select_field']['biz_shipment.trans_origin_name']) && $selects[] = $title_data['base_select_field']['biz_shipment.trans_origin_name'];
            if($key == 'biz_shipment.trans_discharge') isset($title_data['base_select_field']['biz_shipment.trans_discharge_name']) && $selects[] = $title_data['base_select_field']['biz_shipment.trans_discharge_name'];
            if($key == 'biz_shipment.trans_destination') isset($title_data['base_select_field']['biz_shipment.trans_destination_name']) && $selects[] = $title_data['base_select_field']['biz_shipment.trans_destination_name'];
            if($key == 'biz_shipment.job_no'){
                isset($title_data['base_select_field']['biz_shipment.consol_no']) && $selects[] = $title_data['base_select_field']['biz_shipment.consol_no'];
                isset($title_data['base_select_field']['biz_shipment.carrier_ref']) && $selects[] = $title_data['base_select_field']['biz_shipment.carrier_ref'];
            }
            if($key == 'biz_shipment.trans_carrier_name') {
                isset($title_data['base_select_field']['biz_shipment.vessel']) && $selects[] = $title_data['base_select_field']['biz_shipment.vessel'];
                isset($title_data['base_select_field']['biz_shipment.voyage']) && $selects[] = $title_data['base_select_field']['biz_shipment.voyage'];
                isset($title_data['base_select_field']['biz_shipment.trans_carrier_name']) && $selects[] = $title_data['base_select_field']['biz_shipment.trans_carrier_name'];
            }
            if($key == 'biz_shipment.booking_ETD') isset($title_data['base_select_field']['biz_shipment.creditor_name']) && $selects[] = $title_data['base_select_field']['biz_shipment.creditor_name'];
            if($key == 'biz_shipment.created_by_name') isset($title_data['base_select_field']['biz_shipment.sales_name']) && $selects[] = $title_data['base_select_field']['biz_shipment.sales_name'];

            if(!in_array($val, $selects)) $selects[] = $val;
        }
        //获取需要的字段--end
        $this->db->select(join(',', $selects), false);
        if ($consol_id === "") {
            $this->db->limit($rows, $offset);
        }
        //这里拼接join数据
        foreach ($title_data['sql_join'] as $j){
            if($j[0] == 'biz_consol') continue;
            $this->db->join($j[0], $j[1], $j[2]);
        }
        //将order字段替换为 对应的sql字段
        $sort = $title_data['sql_field']["biz_shipment.{$sort}"];
        $this->db->is_reset_select(false);//不重置查询
        $this->db->join('biz_consol', 'biz_consol.id = biz_shipment.consol_id', 'LEFT');
        $rs = $this->biz_shipment_model->get_v3($where, $sort, $order, $unmerge);
        $this->db->clear_limit();//清理下limit
        $result["total"] = $this->db->count_all_results('');

        $rows = array();
        $must_field = array('id', 'label', 'node', 'vessel', 'voyage','job_no', 'trans_carrier', 'trans_carrier_name', 'creditor', 'creditor_name', 'consol_no', 'created_time','created_by','updated_by','updated_time', 'show_fields', 'carrier_ref', 'lock_lv');
        //获取每个操作的权限，根据当前用户，进行合并可查看字段
        $userRole = $this->bsc_user_role_model->get_data_role();
        $this->load->model('biz_client_model');
        $this->load->model('m_model');
        foreach ($rs as $row) {
            $row['label'] = array();
            //查询是否扣单
            $this->db->select('over_credit');
            $clients = $this->biz_client_model->get_one('client_code', $row['client_code']);
            if(!empty($clients) && $clients['over_credit'] == 1){
                $row['label'][] = '<a href="/biz_bill/overdue_info/' . $row['client_code'] . '" target="_blank" style="color: red;">扣单</a>';
            }
            //查询是否未开票
            if($row['invoice_flag'] == 0) $row['label'][] = '<span style="color: green;">未开票</span>';
            if($row['payment_flag'] == 0) $row['label'][] = '<span style="color: blue;">未核销</span>';
            if($row['pay_buy_flag'] != 0) $row['label'][] = '<span style="color: #CCCC00;">付买</span>';
            if($row['loss_flag'] == 1)$row['label'][] = '<span style="color: violet;">亏损</span>';
            $row['label'] = join('<br>', $row['label']);

            //如果存在服务选项时,进行转化
            if(isset($row['service_options'])){
                $service_options_arr = json_decode($row['service_options'], true);
                if($service_options_arr === false) $service_options_arr = array();
                $service_options = array_column($service_options_arr, 0);
                foreach ($service_options as &$service_option){
                    $service_option = lang($service_option, 'bsc_dict');
                }
//                $row['service_options'] = $service_options;
                $str = '';
                if(!empty($service_options)){
                    foreach ($service_options as $val){
                        $str .= $val.'<br>';
                    }
                }
                $row['service_options'] = $str;
                setLang('biz_shipment');
            }

            //节点
            //打单 dadanwancheng 舱单 haiguancangdan 报关 baoguanjieshu 进港 xiangyijingang 海放 haiguanfangxing 码放 matoufangxing 配载 peizaifangxing 船开 chuanyiqihang
            //操作锁 yizuofeiyong
            $row['node'] = array();
            if($row['dadanwancheng'] !== '0') $row['node'][] = '打单';
            if(!empty($row['yupeiyifang'])) $row['node'][] = '预配';
            if($row['haiguancangdan'] !== '0') $row['node'][] = '舱单';
            if($row['baoguanjieshu'] !== '0') $row['node'][] = '报关';
            if($row['xiangyijingang'] !== '0') $row['node'][] = '进港';
            if($row['haiguanfangxing'] !== '0') $row['node'][] = '海放';
            if($row['matoufangxing'] !== '0') $row['node'][] = '码放';
            if($row['peizaifangxing'] !== '0') $row['node'][] = '配载';
            if($row['chuanyiqihang'] !== '0') $row['node'][] = '船开';
            if($row['yizuofeiyong'] !== '0') $row['node'][] = '操作锁';

            $row['node'] = join("<br/>", $row['node']);


            $row = data_role($row, $must_field, explode(',', $userRole['read_text']), 'biz_shipment', 'biz_shipment_index');
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function get_data_statistics($consol_id = "")
    {
        $result = array();
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $parent_id = isset($_GET['sub']) ? strval($_GET['sub']) : '';

        //-------这个查询条件想修改成通用的 -------------------------------
        $carrier_ref = isset($_REQUEST['carrier_ref']) ? trim($_REQUEST['carrier_ref']) : '';
        $label = isset($_REQUEST['label']) ? $_REQUEST['label'] : array();
        $step_info = isset($_REQUEST['step_info']) ? $_REQUEST['step_info'] : array();
        $lock_lv = isset($_REQUEST['lock_lv']) ? $_REQUEST['lock_lv'] : array();
        $consol_lock_lv = isset($_REQUEST['consol_lock_lv']) ? $_REQUEST['consol_lock_lv'] : array();

        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $where = array();
        $unmerge = true;

        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_shipment', 'biz_shipment_index');//获取相关的配置信息
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }
                //服务选项的不等于用not like 处理
                $json_field = array('service_options', 'biz_shipment.service_options');
                if(in_array($f, $json_field) && $s == '!='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} not like '%$v%'";
                    }
                    continue;
                }
                if($f == 'biz_shipment.cus_no' || $f == 'cus_no'){
                    //2021-07-27 新增关单号可查询子shipment的
                    $this->db->select('parent_id');
                    $child_shipment = $this->biz_shipment_model->get_where_one_all(search_like($f, $s, $v) . ' and parent_id != 0');
                    $cus_no_where = search_like($f, $s, $v);
                    if(!empty($child_shipment)){
                        $cus_no_where = "(" . $cus_no_where . " or biz_shipment.id = " . $child_shipment['parent_id'] . ")";
                    }
                    if($v != '') $where[] = $cus_no_where;
                    continue;
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];


                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') $v = '';
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) $title_data['sql_join'][$title_data['base_data'][$title_f]['sql_join']] = $this_join;

                    $where[] = search_like($f, $s, $v);
                }
            }
        }
        // if ($carrier_ref != ''){
        //     Model('m_model');
        //     //2022-06-14 如果是查的MBL,那么从另一张表里获取
        //     $mbl_mores = $this->m_model->query_array("select consol_id from biz_consol_mbl_more where " . search_like('mbl_no', 'like', $carrier_ref));

        //     if(!empty($mbl_mores)) $where[] = "biz_consol.id in ('" . join('\',\'', array_column($mbl_mores, 'consol_id')) . "')";
        //     else $where[] = "0 = 1";
        //     // $where[] = "biz_consol.carrier_ref like '%$carrier_ref%'";
        // }
        if($label !== array()){
            if(in_array(1, $label)) $where[] = '(select over_credit from biz_client where biz_client.client_code = biz_shipment.client_code) = 1';//扣单
            if(in_array(2, $label)) $where[] = "invoice_flag = 0";//未开票
            if(in_array(3, $label)) $where[] = "loss_flag = 1";//亏损
            if(in_array(4, $label)) $where[] = "payment_flag = 0";//未核销
        } ;//标签 扣单 亏损 未开票
        if($step_info !== array()){
            if(in_array('dadanwancheng', $step_info)) $where[] = "biz_shipment.dadanwancheng != '0'";
            if(in_array('haiguancangdan', $step_info)) $where[] = "biz_shipment.haiguancangdan != '0'";
            if(in_array('baoguanjieshu', $step_info)) $where[] = "biz_shipment.baoguanjieshu != '0'";
            if(in_array('xiangyijingang', $step_info)) $where[] = "biz_shipment.xiangyijingang != '0'";
            if(in_array('haiguanfangxing', $step_info)) $where[] = "biz_shipment.haiguanfangxing != '0'";
            if(in_array('matoufangxing', $step_info)) $where[] = "biz_shipment.matoufangxing != '0'";
            if(in_array('peizaifangxing', $step_info)) $where[] = "biz_shipment.peizaifangxing != '0'";
            if(in_array('chuanyiqihang', $step_info)) $where[] = "biz_shipment.chuanyiqihang != '0'";
            if(in_array('tidanqueren', $step_info)) $where[] = "biz_shipment.tidanqueren != '0'";
            if(in_array('tidanqianfa', $step_info)) $where[] = "biz_shipment.tidanqianfa != '0'";
            if(in_array('yizuofeiyong', $step_info)) $where[] = "biz_shipment.yizuofeiyong != '0'";
        } ;
        $lock_lv_where = array();
        foreach ($lock_lv as $val){
            $lock_lv_where[] = "biz_shipment.lock_lv = $val";
        }
        if(!empty($lock_lv_where)) $where[] = '( ' . join(' or ', $lock_lv_where) . ' )';
        $consol_lock_lv_where = array();
        foreach ($consol_lock_lv as $val){
            $consol_lock_lv_where[] = "biz_consol.lock_lv = $val";
        }
        if(!empty($consol_lock_lv_where)) $where[] = '( ' . join(' or ', $consol_lock_lv_where) . ' )';

        if($parent_id != ''){
            $unmerge = false;
            $where[] = "biz_shipment.parent_id = '$parent_id'";
        }
        $where = join(' and ', $where);
        //-----------------------------------------------------------------
        if ($consol_id != "" && $where == "") {
            if ($where == "") {
                $where = "consol_id = $consol_id";
            } else {
                $where .= " and consol_id = $consol_id";
            }
        }

        $this->db->select("sum(biz_shipment.good_outers) as good_outers" .
            ", sum(biz_shipment.good_volume) as good_volume" .
            ", sum(biz_shipment.good_weight) as good_weight" .
            ", group_concat(biz_shipment.box_info SEPARATOR ';') as box_info" .
            ",SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_volume else 0 end)) as ex_LCL_good_volume" .
            ",SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_volume else 0 end)) as im_LCL_good_volume" .
            ",SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_weight else 0 end)) as ex_LCL_good_weight" .
            ",SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_weight else 0 end)) as im_LCL_good_weight" .
            ",SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_volume else 0 end)) as ex_AIR_good_volume" .
            ",SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_volume else 0 end)) as im_AIR_good_volume" .
            ",SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_weight else 0 end)) as ex_AIR_good_weight" .
            ",SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_weight else 0 end)) as im_AIR_good_weight" .
            "");
        //这里拼接join数据
        foreach ($title_data['sql_join'] as $j){
            if($j[0] == 'biz_consol') continue;
            $this->db->join($j[0], $j[1], $j[2]);
        }
        //将order字段替换为 对应的sql字段
        $sort = $title_data['sql_field']["biz_shipment.{$sort}"];
        $this->db->join('biz_consol', 'biz_consol.id = biz_shipment.consol_id', 'LEFT');
        $rs = $this->biz_shipment_model->get_v3($where, $sort, $order, $unmerge);
        $rows = array();
        $box_info_sum = array();
        foreach ($rs as $row) {
            //将箱型箱量全部加起来
            $this_box_info = explode(';', $row['box_info']);
            unset($row['box_info']);
            foreach ($this_box_info as $this_box){
                $this_box_arr = json_decode($this_box, true);
                if(is_array($this_box_arr)) $box_info_sum = merge_box_info($box_info_sum, $this_box_arr);
            }
            array_push($rows, $row);
        }
        $rows[0]['teu'] = 0;
        $rows[0]['box_info'] = array();
        foreach ($box_info_sum as $box){
            $rows[0]['box_info'][] = $box['size'] . "*" . $box['num'];
            if($box['size'][0] == '2'){
                $rows[0]['teu'] += 1 * $box['num'];
            }else if($box['size'][0] == '4'){
                $rows[0]['teu'] += 2 * $box['num'];
            }
        }
        $rows[0]['box_info'] = join(',', $rows[0]['box_info']);
        $result['data'] = $rows[0];
        echo json_encode($result);
    }

    /**
     * 更新shipment 的duty数据
     * 2022-06-20 将新的duty表数据进行存储
     * 这个别上线,正在改版中,准备先把数据存储进去,后面一点一点替换
     *
     */
    private function shipment_duty_update($shipment_id, $type = 0){
        //将consol相关的duty进行处理--start
        //这里有几个问题比较麻烦
        //1、页面上数据什么形式传过来好，
        // 第一种，按照现在这样， 直接传，好处， 页面代码可以少改一些，结构也不用进行太大的更改，只需要转换为新版的即可, 坏处, 数据得单独处理,可能会出现什么问题
        // 第二种, 按照新表格的 格式传输过来, 好处 数据格式可能清晰点,这里 需要考虑用什么样来传过来, 坏处, 处理麻烦,改动较大
        //这里还是依旧用第一种吧,因为海管家也是这种

        //2、原本的很多代码由于是横向查询的，这里是大量修改， 还是如何说呢

        //目前只做存储使用,后续可以把代码断掉, 一点一点改

        $duty = array();
        $duty['biz_table'] = 'biz_shipment';
        $duty['id_no'] = $shipment_id;
        $userRole = get_session('user_role');
        foreach ($this->admin_field as $val){
            $mr_id = 0;
            $mr_group = '';
            if(in_array($val, $userRole)){
                //不为空，且当前权限人员存在时
                $mr_id = get_session('id');
                $mr_group = join(',', get_session('group'));
            }

            if($type === 1){
                //新增时
                $duty["{$val}_id"] = postValue("{$val}_id", '0');
                $duty["{$val}_group"] = postValue("{$val}_group", '');

//                if($duty["{$val}_id"] === '0') $duty["{$val}_id"] = $mr_id;//如果什么都没选,且自己有这个角色的时候,会默认把他自己拼上去
//                if($duty["{$val}_group"] === '') $duty["{$val}_group"] = $mr_group;//如果什么都没选,且自己有这个角色的时候,会默认把他自己的组拼上去
            }else{
                //修改时
                if(isset($_POST["{$val}_id"])){
                    $duty["{$val}_id"] = $_POST["{$val}_id"];
                    $duty["{$val}_group"] = $_POST["{$val}_group"] ;
                }
            }

        }
        Model('biz_duty_model');
        if($type === 1){
            $this->biz_duty_model->save($duty);
        }else{
            $this->biz_duty_model->update_by_idno($shipment_id, $duty);
        }

        //常规的已经套过来了, 新版本的数据的话,需要考虑到无权限, 或者格子数据没传过来的情况
        //不能完全根据传过来的数据来处理,
        //将consol相关的duty进行处理--end
    }


    public function add_data_ajax()
    {
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if(is_string($temp))$temp = ts_replace(trim($temp));
            if ($temp != "") {
                $data[$item] = $temp;
            }
        }
		$consol_id = postValue('consol_id', '0');
		if($consol_id != 0){
			$data['dadanwancheng'] = date('Y-m-d H:i:s',time());
		}
        $data["job_no"] = shipment_job_no_rule($data['trans_origin'], $data['trans_mode']);
        //2022-03-28 如果是EXP FCL SEA 那么HBL不为FCL
        if(empty($data['hbl_no'])) $data['hbl_no'] = $data['job_no'];
        //如果没填写HBL,那么默认为JOB no
        if(!isset($data['hbl_no']) || (isset($data['hbl_no']) && $data['hbl_no'] == '')) $data['hbl_no'] = $data['job_no'];
        $data['warehouse_no'] = $data['job_no'];

        //box_info处理--start
        $data['box_info'] = postValue('box_info', array());
        box_info_handle($data['box_info']);
        $data['box_info'] = json_encode($data['box_info']);
        //box_info处理--end

        //新增危险品数据
        $goods_type = postValue('goods_type', 'GC');
        //服务选项--start
//            json_encode($data['service_options'])
        $data['service_options'] = json_encode(issetGet($data, 'service_options', array()));
        //服务选项--end
        $data['INCO_option'] = isset($data['INCO_option']) ? $data['INCO_option'] : array();
        $data['INCO_option'] = join(',', $data['INCO_option']);
        $this->must_field_check($data);

        //2024-03-29 新加入了 根据本地系统前缀 限制 出口起运港 和 进口目的港 必须为指定国家前缀
        $system_type = strtoupper(get_system_type());
		if(strlen($data['trans_origin']) ==5) {
			if ($data['biz_type'] == 'export') {
				//出口
				$trans_origin_country = substr($data['trans_origin'], 0, 2);
				$branch_office_port = Model('bsc_dict_model')->get_option("branch_office_port", "value = '{$trans_origin_country}'");
				$branch_office_port_name_arr = array_map(function ($r) {
					return strtoupper($r);
				}, array_column($branch_office_port, 'name'));
				if (!empty($branch_office_port) && !in_array($system_type, $branch_office_port_name_arr)) {
					return jsonEcho(array('code' => 1, 'msg' => lang('Please add order to {system} system', array('system' => $branch_office_port[0]['value']))));
				}
			} else {
				//进口
				$trans_destination_country = substr($data['trans_destination'], 0, 2);
				$branch_office_port = Model('bsc_dict_model')->get_option("branch_office_port", "value = '{$trans_destination_country}'");
				$branch_office_port_name_arr = array_map(function ($r) {
					return strtoupper($r);
				}, array_column($branch_office_port, 'name'));
				if (!empty($branch_office_port) && !in_array($system_type, $branch_office_port_name_arr)) {
					return jsonEcho(array('code' => 1, 'msg' => lang('Please add order to {system} system', array('system' => $branch_office_port[0]['value']))));
				}
			}
		}


        if(!checkToken('shipment_add')){
            echo json_encode(array('code' => 1, 'msg' => '请勿重复提交,请刷新页面后再试'));
            return;
        }
        
        $this->update_company($data);
        //不等于普通货物时，进行危险品的新增
        if ($goods_type != 'GC' && $goods_type != 'RF' && $goods_type != '') {
            $this->load->model('biz_shipment_dangergoods_model');
            $danger_goods = array(
                'shipment_id' => 0,
                'goods_type' => $goods_type,
                'isdangerous' => $_POST['isdangerous'],
                'ism_spec' => $_POST['ism_spec'],
                'UN_no' => $_POST['UN_no'],
                'IMDG_page' => $_POST['IMDG_page'],
                'packing_type' => $_POST['packing_type'],
                'flash_point' => $_POST['flash_point'],
                'IMDG_code' => $_POST['IMDG_code'],
                'is_pollution_sea' => $_POST['is_pollution_sea'],
                'ems_no' => $_POST['ems_no'],
                'MFAG_NO' => $_POST['MFAG_NO'],
                'e_contact_name' => $_POST['e_contact_name'],
                'e_contact_tel' => $_POST['e_contact_tel'],
                'long' => $_POST['long'],
                'wide' => $_POST['wide'],
                'high' => $_POST['high'],
            );
            $dangergoods_id = $this->biz_shipment_dangergoods_model->save($danger_goods);
            $data['dangergoods_id'] = $dangergoods_id;
        }
        $id = $this->biz_shipment_model->save($data);
        if(!$id){
            if (isset($dangergoods_id)) $this->biz_shipment_dangergoods_model->mdelete($dangergoods_id);
            echo json_encode(array('code' => 1, 'msg' => '新增失败,请联系管理员', 'data' => $data));
            return;
        }

        //同步shipper consignee notify--start
        if(isset($data['consol_id']) && $data['consol_id'] > 0){
            $this->tb_consol($data['consol_id'], $data);
        }
        //同步shipper consignee notify--end
        //将consol相关的duty进行处理--start
        $this->shipment_duty_update($id,1);
        if(isset($data['consol_id'])){
            $this->consol_duty_update($data['consol_id']);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment";
            $log_data["key"] = $id;
            $log_data['master_table_name'] = "biz_consol";
            $log_data['master_key'] = $data['consol_id'];
            $log_data["action"] = "bind_update";

            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        }
        //将consol相关的duty进行处理--end
        $data['id'] = $id;
        if(isset($_POST['ebooking_id']) && !empty($_POST['ebooking_id'])) $this->ebooking_pass($_POST['ebooking_id'], $id);
        //关联危险品关联表的shipment_id
        if (isset($dangergoods_id)) $this->biz_shipment_dangergoods_model->update($dangergoods_id, array('shipment_id' => $id));

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);//

        echo json_encode(array('code' => 0, 'msg' => lang('新增成功'), 'data' => $data));
    }
    
        /**
     * ebooking通过
     */
    private function ebooking_pass($ebooking_id, $shipment_id){
        Model('biz_shipment_ebooking_model');
        Model('bsc_upload_model');
        if(!empty($ebooking_id)){
            $update_data = array(
                'shipment_id' => $shipment_id,
                'status' => 1
            );
            $this->biz_shipment_ebooking_model->update($ebooking_id, $update_data);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment_ebooking";
            $log_data["key"] = $ebooking_id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($update_data);
            log_rcd($log_data);
            $bsc_upload = $this->bsc_upload_model->get_by_id_no('biz_shipment_ebooking',$ebooking_id);
            if(!empty($bsc_upload)){
                foreach ($bsc_upload as $v){
                    $add = [];
                    $add['biz_table'] = 'biz_shipment';
                    $add['type_name'] = $v['type_name'];
                    $add['file_name'] = $v['file_name'];
                    $add['id_no'] = $shipment_id;
                    $add_id = $this->bsc_upload_model->save($add);
                    $log_data = array();
                    $log_data["table_name"] = "bsc_upload";
                    $log_data["key"] = $add_id;
                    $log_data["action"] = "add";
                    $log_data["value"] = json_encode($add);
                    log_rcd($log_data);
                }
            }
        }
    }

    public function update_data_ajax($id = 0)
    {
        if ($id == 0) $id = $_REQUEST["id"];
        $old_row = $this->biz_shipment_model->get_by_id($id);
        $consol = Model('biz_consol_model')->get_by_id($old_row['consol_id']);
        $field = $this->field_edit;
        if($old_row['lock_lv'] > 0){
            if(strlen($old_row['release_type_remark'])<5){   // 假如等电放通知，允许修改放单方式，这里开个口子允许保存
                echo json_encode(array('code' => 1, 'msg' => '已锁无法修改'));
                return;
            }
        }
        $data = array();

        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($temp != "") {
                if (array_key_exists($item, $old_row) && $old_row[$item] != $temp)
                    $data[$item] = is_array($temp) ? $temp : ts_replace(trim($temp));
            }
        }
        //这里加入时间相关字段的空值处理方式
        $empty_date_fields = array('issue_date');
        foreach ($empty_date_fields as $empty_date_field){
            if(isset($data[$empty_date_field]) && empty($data[$empty_date_field])) $data[$empty_date_field] = '0000-00-00';
        }

        $data['show_fields'] = isset($_POST['show_fields']) ? join(',', $_POST['show_fields']) : '';
        if($data['show_fields'] == $old_row['show_fields']) unset($data['show_fields']);
        //box_info处理--start
        $trans_mode = postValue('trans_mode', $old_row['trans_mode']);
        if($trans_mode == 'FCL'){
            if(isset($_POST['box_info'])){
                $data['box_info'] = $_POST['box_info'];
                box_info_handle($data['box_info']);
                $data['box_info'] = json_encode($data['box_info']);
                //如果箱型箱量没变，那么不保存
                if($data['box_info'] == $old_row['box_info']) unset($data['box_info']);
            }
        }else{
            $data['box_info'] = json_encode(array());
            //如果箱型箱量没变，那么不保存
            if($data['box_info'] == $old_row['box_info']) unset($data['box_info']);
        }
        //box_info处理--end
        
        //服务选项--start
        if(isset($data['service_options'])){
            $data['service_options'] = json_encode($data['service_options']);
            if($data['service_options'] == $old_row['service_options']) unset($data['service_options']);
        }
        //服务选项--end
        $data['INCO_option'] = isset($data['INCO_option']) ? $data['INCO_option'] : array();
        $data['INCO_option'] = join(',', $data['INCO_option']);
        if($data['INCO_option'] == $old_row['INCO_option']) unset($data['INCO_option']);
        if(isset($_POST['release_type']) && $_POST['release_type'] !='TER'){
            $data['release_type_remark'] = '';
        }

        //危险品数据--start
        $goods_type = isset($_POST['goods_type']) ? $_POST['goods_type'] : 'GC';

        if ($goods_type != 'GC' && $goods_type != 'RF') {
            $danger_goods = array(
                'goods_type' => $goods_type,
                'isdangerous' => $_POST['isdangerous'],
                'ism_spec' => $_POST['ism_spec'],
                'UN_no' => $_POST['UN_no'],
                'IMDG_page' => $_POST['IMDG_page'],
                'packing_type' => $_POST['packing_type'],
                'flash_point' => $_POST['flash_point'],
                'IMDG_code' => $_POST['IMDG_code'],
                'is_pollution_sea' => $_POST['is_pollution_sea'],
                'ems_no' => $_POST['ems_no'],
                'MFAG_NO' => $_POST['MFAG_NO'],
                'e_contact_name' => $_POST['e_contact_name'],
                'e_contact_tel' => $_POST['e_contact_tel'],
                'long' => $_POST['long'],
                'wide' => $_POST['wide'],
                'high' => $_POST['high'],
            );
        } else {
            $danger_goods = array();
        }
        $danger_goods_id = $this->dangergoods_handle($old_row, $goods_type, $danger_goods);

        if ($danger_goods_id !== false) {
            $data['dangergoods_id'] = $danger_goods_id;
        }
        //危险品数据--end
        $this->update_company($data, $old_row);

        //同步shipper consignee notify--start
        $this->tb_consol($old_row['consol_id'], $data);
        //同步shipper consignee notify--end
        $id = $this->biz_shipment_model->update($id, $data);
        if($old_row['consol_id'] != 0)consol_status($old_row['consol_id']);
        //更新账单
        update_bill_ETD('shipment', $id);
        $this->shipment_duty_update($id);
        $this->consol_duty_update($old_row['consol_id']);
        //将consol相关的duty进行处理--end
        if(!empty($data)){
            $data['id'] = $id;

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment";
            $log_data["key"] = $id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        }else{
            $data['id'] = $id;
        }
        echo json_encode(array('code' => 0, 'msg' => lang('修改成功'), 'data' => $data));
    }

    public function update_checkbox($id=0){
        if ($id == 0) $id = $_REQUEST["id"];
        $old_row = $this->biz_shipment_model->get_duty_one("id = '{$id}'");
        if(empty($old_row)){
            echo json_encode(array('code' => 1, 'msg' => lang("SHIPMENT不存在")));
            return;
        }
        if($old_row['lock_lv'] > 0){
            echo json_encode(array('code' => 1, 'msg' => lang("SHIPMENT已锁,无法修改")));
            return;
        }
		$dadanwancheng = postValue("dadanwancheng");
		if($dadanwancheng!=""){
			if(strlen($dadanwancheng)>2){
				foreach ($this->shipment_must_field as $val){
					if(empty($old_row[$val])){
						if($val == 'good_outers' && $old_row[$val] == 0) continue;
						return jsonEcho(['code'=>5]);
					}
					if($val == 'trans_mode' && $old_row['trans_mode'] == 'FCL'){
						$box_info = json_decode($old_row['box_info'], true);
						if(empty($box_info)){
							return jsonEcho(['code'=>5]);
						}
					}
				}
				$duty_field = array('sales', 'operator');
				Model('biz_duty_model');
				$duty = $this->biz_duty_model->get_new("id_type = 'biz_shipment' and user_id!=0 and id_no = $id");
				$duty_role = array_column($duty,"user_role");
				foreach($duty_field as $val){
					if(!in_array($val,$duty_role)){
						return jsonEcho(['code'=>5]);
					}
				}
			}else{
				if(strlen($old_row['consol_id'])>2) exit(json_encode(array('code' => 1, 'msg' => lang('先解绑consol'))));
			}
		}
        $steps_field = postValue('steps_field','');
        $data[$steps_field] = postValue($steps_field,'0');
		$this->biz_shipment_model->update($id, $data);
		$log_data = array();
		$log_data["table_name"] = "biz_shipment";
		$log_data["key"] = $id;
		$log_data["action"] = "update";
		$log_data["value"] = json_encode($data);
		log_rcd($log_data);


		echo json_encode(array('code' => 0, 'msg' => 'ok'));die;
        $service_options_json =  $old_row['service_options'];
        $service_options = array_column(json_decode($service_options_json, true), 0);

        $data = array();
        $dadanwancheng = postValue("dadanwancheng");
        $haiguancangdan = postValue("haiguancangdan");
        $baoguanjieshu = postValue("baoguanjieshu");
        $xiangyijingang = postValue("xiangyijingang");
        $haiguanfangxing = postValue("haiguanfangxing");
        $matoufangxing = postValue("matoufangxing");
        $peizaifangxing = postValue("peizaifangxing");
        $chuanyiqihang = postValue("chuanyiqihang");
        $pre_alert = postValue("pre_alert");
        $tidanqianfa = postValue("tidanqianfa");
        $yizuofeiyong = postValue("yizuofeiyong");
        $pay_buy_flag = postValue("pay_buy_flag");
        // 监听各个节点并设置逻辑 
        if($dadanwancheng!=""){
            if(strlen($dadanwancheng)>2){
                $data['dadanwancheng'] = date('Y-m-d H:i:s');
                // $this->update_checkbox_dadanwancheng($old_row);
            }else{
                if(strlen($old_row['haiguancangdan'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【舱单】')));
                if(strlen($old_row['baoguanjieshu'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【报关】')));
                if(strlen($old_row['xiangyijingang'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【进港】')));
                if(strlen($old_row['consol_id'])>2) exit(json_encode(array('code' => 1, 'msg' => '先解绑consol')));
                $data['dadanwancheng'] = '0';
            }
        }
        if($haiguancangdan!=""){
            if(strlen($haiguancangdan)>2){
                if(empty($old_row['consol_id'])) exit(json_encode(array('code' => 1, 'msg' => 'need a consol')));
                if(empty($old_row['dadanwancheng'])) exit(json_encode(array('code' => 1, 'msg' => '先勾选【打单】')));
                $data['haiguancangdan'] = date('Y-m-d H:i:s');
            }else{
                $data['haiguancangdan'] = '0';
            }
        }
        if($baoguanjieshu!=""){
            if(strlen($baoguanjieshu)>2){
                // if(!in_array("custom_declaration",$service_options)) exit(json_encode(array('code' => 1, 'msg' => 'service option 未勾选报关服务')));
                if(empty($old_row['consol_id'])) exit(json_encode(array('code' => 1, 'msg' => 'need a consol')));
                if(empty($old_row['dadanwancheng'])) exit(json_encode(array('code' => 1, 'msg' => '先勾选【打单】')));
                $data['baoguanjieshu'] = date('Y-m-d H:i:s');
            }else{
                if(strlen($old_row['xiangyijingang'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【进港】')));
                $data['baoguanjieshu'] = '0';
            }
        }
        if($xiangyijingang!=""){
            if(strlen($xiangyijingang)>2){
                if(empty($old_row['consol_id'])) exit(json_encode(array('code' => 1, 'msg' => 'need a consol')));
                if(empty($old_row['dadanwancheng'])) exit(json_encode(array('code' => 1, 'msg' => '先勾选【打单】')));
                $data['xiangyijingang'] = date('Y-m-d H:i:s');
            }else{
                if(strlen($old_row['haiguanfangxing'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【海放】')));
                if(strlen($old_row['pre_alert'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【预报】')));
                $data['xiangyijingang'] = '0';
            }
        }
        if($haiguanfangxing!=""){
            if(strlen($haiguanfangxing)>2){
                if(empty($old_row['consol_id'])) exit(json_encode(array('code' => 1, 'msg' => 'need a consol')));
                if(empty($old_row['baoguanjieshu']) && in_array("custom_declaration",$service_options)) exit(json_encode(array('code' => 1, 'msg' => '先勾选【报关】')));
                if(empty($old_row['xiangyijingang'])) exit(json_encode(array('code' => 1, 'msg' => '先勾选【进港】')));
                $data['haiguanfangxing'] = date('Y-m-d H:i:s');
            }else{
                if(strlen($old_row['matoufangxing'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【码放】')));
                $data['haiguanfangxing'] = '0';
            }
        }
        if($matoufangxing!=""){
            if(strlen($matoufangxing)>2){
                if(empty($old_row['consol_id'])) exit(json_encode(array('code' => 1, 'msg' => 'need a consol')));
                if(empty($old_row['haiguanfangxing'])) exit(json_encode(array('code' => 1, 'msg' => '先勾选【海放】')));
                $data['matoufangxing'] = date('Y-m-d H:i:s');
            }else{
                if(strlen($old_row['peizaifangxing'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【配载】')));
                $data['matoufangxing'] = '0';
            }
        }
        if($peizaifangxing!=""){
            if(strlen($peizaifangxing)>2){
                if(empty($old_row['consol_id'])) exit(json_encode(array('code' => 1, 'msg' => 'need a consol')));
                if(empty($old_row['matoufangxing'])) exit(json_encode(array('code' => 1, 'msg' => '先勾选【码放】')));
                $data['peizaifangxing'] = date('Y-m-d H:i:s');
            }else{
                if(strlen($old_row['chuanyiqihang'])>2) exit(json_encode(array('code' => 1, 'msg' => '先取消【船开】')));
                $data['peizaifangxing'] = '0';
            }
        }
        if($chuanyiqihang!=""){
            Model('biz_consol_model');
            $this->db->select('id,trans_ATD');
            $consol = $this->biz_consol_model->get_by_id($old_row['consol_id']);

            if(strlen($chuanyiqihang)>2){
                if(empty($old_row['consol_id'])) exit(json_encode(array('code' => 1, 'msg' => 'need a consol')));
                // if(empty($old_row['peizaifangxing'])) exit(json_encode(array('code' => 1, 'msg' => '先勾选【配载】')));
                if(!empty($consol) && $consol['trans_ATD'] == '0000-00-00 00:00:00'){
                    exit(json_encode(array('code' => 1, 'msg' => 'Please input ATD in consol first!')));
                }
                $data['chuanyiqihang'] = date('Y-m-d H:i:s');
            }else{
                if(!empty($consol) && $consol['trans_ATD'] != '0000-00-00 00:00:00'){
                    exit(json_encode(array('code' => 1, 'msg' => 'Please clear ATD in consol !')));
                }
                $data['chuanyiqihang'] = '0';
            }
        }
        if($pre_alert!=""){
            if(strlen($pre_alert)>2){
                if(empty($old_row['consol_id'])) exit(json_encode(array('code' => 1, 'msg' => 'need a consol')));
                if(empty($old_row['matoufangxing'])) exit(json_encode(array('code' => 1, 'msg' => '先勾选【码放】')));
                //勾选预报勾时,自动把其他shipment一起勾上
                Model('biz_shipment_model');
                $this->db->select('id');
                $shipments = $this->biz_shipment_model->get_shipment_by_consol($old_row['consol_id']);
                foreach ($shipments as $shipment){
                    //把自己排除
                    if($shipment['id'] == $id) continue;
                    $shipment_update_data = array(
                        'pre_alert' => date('Y-m-d H:i:s'),
                    );
                    $this->biz_shipment_model->update($shipment['id'], $shipment_update_data);

                    // record the log
                    $log_data = array();
                    $log_data["table_name"] = "biz_shipment";
                    $log_data["key"] = $shipment['id'];
                    $log_data["action"] = "update";

                    $log_data["value"] = json_encode($shipment_update_data);
                    log_rcd($log_data);
                }
                $data['pre_alert'] = date('Y-m-d H:i:s');
            }else{
                $data['pre_alert'] = '0';
            }
        }
        if($tidanqianfa!=""){
            if(strlen($tidanqianfa)>2){
                exit(json_encode(array('code' => 1, 'msg' => 'System auto_click when download B/L !')));
            }else{
                //2022-04-19 如果取消勾选提单签发的话, 只能有特殊权限的人来 
                Model('m_model');
                $operator = $this->m_model->query_one("select user_id from biz_duty_new where id_type = 'biz_shipment' and user_role='operator' and id_no = '$id'");
                if(!leader_check('operator', $operator['user_id'])){
                    exit(json_encode(array('code' => 1, 'msg' => 'Permission denied! operator leader can do it!')));
                }
                $data['tidanqianfa'] = '0';
            }
        }
        if($yizuofeiyong!=""){
            if(strlen($yizuofeiyong)>2){
                if(empty($old_row['consol_id'])) exit(json_encode(array('code' => 1, 'msg' => 'need a consol')));
                //2021-11-02 14:17  http://wenda.leagueshipping.com/?/question/561 新增一个判断实际离泊 
                Model('biz_consol_model');
                $this->db->select('id,status,trans_ATD');
                $consol = $this->biz_consol_model->get_by_id($old_row['consol_id']);
                if(!empty($consol) && $consol['status'] != 'cancel' && $consol['trans_ATD'] == '0000-00-00 00:00:00'){
                    exit(json_encode(array('code' => 1, 'msg' => 'Please input ATD in consol first! ')));
                }
                $data['yizuofeiyong'] = date('Y-m-d H:i:s');
            }else{
                $data['yizuofeiyong'] = '0';
            }
        }
        if($pay_buy_flag!=""){
            if(strlen($pay_buy_flag)>2){
                if(empty($old_row['consol_id'])) exit(json_encode(array('code' => 1, 'msg' => 'need a consol')));
                $data['pay_buy_flag'] = date('Y-m-d H:i:s');
            }else{
                //2021-11-02  shipment的付买标记相关事件：点击时，如果client是付买 那么直接提示不能撤销付买；如果client不是付买,那么向jj和阎明提交邮件通知，
                if(shipment_client_is_pay_buy($id)){
                    echo json_encode(array('code' => 1, 'msg' => 'client是付买，所以不能撤销付买'));
                    return;
                }else{
                    if(shipment_consol_is_pay_buy($id)){
                        echo json_encode(array('code' => 1, 'msg' => 'consol 有付买标记，如果需要撤销请联系【市场】撤销consol的付买标记！' ));
                        return;
                    }
                }
                $data['pay_buy_flag'] = '0';
            }
        }

        $this->biz_shipment_model->update($id, $data);
        $log_data = array();
        $log_data["table_name"] = "biz_shipment";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);


        echo json_encode(array('code' => 0, 'msg' => '提交成功'));
    }
    
    /**
     * 必填字段校验
     */
    private function must_field_check($data, $row = array()){
        $must_field = array('booking_ETD', 'sales_id');
        $error_msg = array();//错误提示信息,如果这个数组有值时,就提示相关的
        $jump_field = array();
        $int_field = array('sales_id');
        foreach ($must_field as $val) {
            if (!isset($data[$val])) {
                if (!empty($row)) {
                    $data[$val] = $row[$val];
                } else {
                    $data[$val] = '';
                }
            }
            if (in_array($val, $jump_field)) continue;

            if (isset($data[$val])) {
                if (in_array($val, $int_field)) {
                    if ($_POST[$val] == 0) {
                        $error_msg[] = lang("{field}不能为空", array('field' => lang($val)));
                        continue;
                    }
                } else {
                    if ($data[$val] == '') {
                        $error_msg[] = lang("{field}不能为空", array('field' => lang($val)));
                        continue;
                    }
                }

            }
        }
        if(!empty($error_msg)){
            error(1, join(",\r\n", $error_msg));
            exit();
        }
    }

    public function update_checkbox_dadanwancheng($data=array()){
        $must_field = array('client_company', 'client_email','shipper_company', 'consignee_company', 'notify_company', 'service_options', 'trans_mode', 'biz_type',
            'trans_tool', 'booking_ETD','trans_destination_inner', 'status', 'hbl_type', 'release_type', 'trans_term',
            'INCO_term', 'description', 'mark_nums', 'good_outers_unit', 'goods_type', 'hs_code');
        $error_msg = array();//错误提示信息,如果这个数组有值时,就提示相关的
        foreach ($must_field as $val){
            if(empty($data[$val])){
                $error_msg[] = lang($val) . '不能为空';
                continue;
//                echo json_encode(array('code' => 1, 'msg' => lang($val) . '不能为空'));
//                exit();
            }
            if($val == 'trans_mode' && $data['trans_mode'] == 'FCL'){
                $box_info = json_decode($data['box_info'], true);
                if(empty($box_info)){
                    $error_msg[] = '箱型箱量不能为空';
                    continue;
//                    echo json_encode(array('code' => 1, 'msg' => '箱型箱量不能为空'));
//                    exit();
                }
            }
        }
        $duty_field = array('sales', 'operator');
        Model('biz_duty_model');
        $duty = $this->biz_duty_model->get_new("id_type = 'biz_shipment' and user_id!=0 and id_no = '{$data['id']}'");
        $duty_role = array_column($duty,"user_role");
        foreach($duty_field as $val){
            if(!in_array($val,$duty_role)){
                $error_msg[] = lang($val) . '不能为空';
                continue;
//                echo json_encode(array('code' => 1, 'msg' => lang($val) . '不能为空'));
//                exit();
            }
        }
        //2021-07-26 货物描述和唛头加入半角后端验证 http://wenda.leagueshipping.com/?/question/295
        $banjiao_fields = array('description', 'mark_nums', 'client_telephone', 'client_email', 'client_address', 'shipper_email', 'shipper_telephone', 'shipper_contact', 'shipper_address', 'consignee_email', 'consignee_telephone', 'consignee_contact', 'consignee_address', 'notify_email', 'notify_telephone', 'notify_contact', 'notify_address');
        foreach ($banjiao_fields as $val){
            if(isset($data[$val])){
                if(in_array($val, array("client_address"))){
                    $mach = check_banjiao_cn($data[$val]);
                }else{
                    $mach = check_banjiao($data[$val]);
                }
                if(sizeof($mach[0]) > 0){
                    //特殊字符验证这里打算新增修改先加一份, 放在勾这里有点难受
                    $error_msg[] = lang($val) . '存在特殊字符: 【' . join('', $mach[0]) . '】,需要修改';
//                    echo json_encode(array('code' => 444, 'msg' =>  lang($val) . '存在特殊字符: 【' . join('', $mach[0]) . '】,需要修改', 'ts_str' => $mach[0], 'text' => $data[$val]));
//                    exit();
                }
            }
        }
        //检查目的港、运输方式等
        $port_length = 5;
        $trans_mode = $data['trans_mode'];
        $trans_tool = $data['trans_tool'];
        $biz_type = $data['biz_type'];
        if($trans_tool == 'AIR' && $trans_mode != 'AIR'){
            $error_msg[] = '检测到当前运输工具为AIR, 请将业务类型改为 ' . $biz_type . ' AIR';
//            echo json_encode(array('code' => 1, 'msg' => '检测到当前运输工具为AIR, 请将业务类型改为 ' . $biz_type . ' AIR'));
//            exit();
        }
        if($trans_mode == 'AIR') $port_length = 3;
        if(isset($data['trans_origin'])){
            if(strlen($data['trans_origin']) != $port_length){
                $error_msg[] = '起运港代码不能超出' . $port_length . '位';
//                echo json_encode(array('code' => 1, 'msg' => '起运港代码不能超出' . $port_length . '位'));
//                exit();
            }
            preg_match("/[A-Z]+/u", $data['trans_origin'] , $matches);
            if($matches[0] != $data['trans_origin']){
                $error_msg[] = '起运港代码只能存在英文';
//                echo json_encode(array('code' => 1, 'msg' => '起运港代码只能存在英文'));
//                exit();
            }
        }
        if(isset($data['trans_discharge'])){
            if(strlen($data['trans_discharge']) != $port_length){
                $error_msg[] = '中转港代码不能超出' . $port_length . '位';
//                echo json_encode(array('code' => 1, 'msg' => '中转港代码不能超出' . $port_length . '位'));
//                exit();
            }
            preg_match("/[A-Z]+/u", $data['trans_discharge'] , $matches);
            if($matches[0] != $data['trans_discharge']){
                $error_msg[] = '中转港代码只能存在英文';
//                echo json_encode(array('code' => 1, 'msg' => '中转港代码只能存在英文'));
//                exit();
            }
        }
        if(isset($data['trans_destination'])){
            if(strlen($data['trans_destination']) != $port_length){
                $error_msg[] = '目的港代码不能超出' . $port_length . '位';
//                echo json_encode(array('code' => 1, 'msg' => '目的港代码不能超出' . $port_length . '位'));
//                exit();
            }
            preg_match("/[A-Z]+/u", $data['trans_destination'] , $matches);
            if($matches[0] != $data['trans_destination']){
                $error_msg[] = '目的港代码只能存在英文';
//                echo json_encode(array('code' => 1, 'msg' => '目的港代码只能存在英文'));
//                exit();
            }
        }
        if(!empty($error_msg)){
            error(1, join(',<br/>', $error_msg));
            exit();
        }
        //检测client code
        $client_code2 =  $data['client_code2'];
        $this->db->select('id,role');
        $client = Model('biz_client_model')->get_where_one("client_code = '{$data['client_code']}'");
        if(empty($client)){
            echo json_encode(array('code' => 1, 'msg' => '当前客户不存在系统中,请重新选择'));
            exit();
        }
        if(in_array('oversea_client', explode(',', $client['role']))){
            if($data['client_code'] == $client_code2){
                // Mars 打电话取消
//                echo json_encode(array('code' => 1, 'msg' => 'oversea client 客户必须选择不同的二级委托方，如果没二级委托方，请先到往来单位设置关联！'));
//                exit();
            }
        }
        //2022-04-08 新增, 订舱服务勾选时,如果贸易方式为FOB,提示,有订舱服务不应该选FOB
        $service_options_json =  $data['service_options'];
        $service_options = array_column(json_decode($service_options_json, true), 0);

        $INCO_term =  $data['INCO_term'];
        if(!in_array('booking', $service_options) && in_array($INCO_term, array('DDU', 'DDP', 'DAP','CIF'))){
            echo json_encode(array('code' => 2, 'msg' => '未勾选订舱服务不能选CIF'));
            exit();
        }
        //目的港服务 ：目的港清关/目的港送货/目的港换单/目的港仓储服务
        //勾选目的港送货，联动出来送货地址，， 而且加上必填
        //只有勾选目的港服务至少1个，才能选择DDU  DDP  DAP
        if(in_array($INCO_term, array('DDU', 'DDP', 'DAP'))){
            //取交集,如果没有目的港服务,那么不允许选
            $mdgfw_arr = array_intersect(array('agent_handle', 'destination_warehouse', 'destination_trucking', 'destination_custom'), $service_options);
            if(sizeof($mdgfw_arr) == 0){
                echo json_encode(array('code' => 2, 'msg' => '贸易方式为DDU  DDP  DAP时,必须勾选任意一个目的港服务'));
                exit();
            }
            //目的港地址必填  
            if(empty($data['INCO_address'])){
                echo json_encode(array('code' => 2, 'msg' => '贸易方式为DDU  DDP  DAP时,目的港送货地址必填'));
                exit();
            }
        }

    }

    public function update_data_consol_id($id = 0)
    {
        if ($id == 0) $id = $_REQUEST["id"];
        $old_row = $this->biz_shipment_model->get_duty_one("id = '{$id}'");
        if(empty($old_row)){
            echo json_encode(array('code' => 1, 'msg' => 'no shipment exist'));
            return;
        }
		if(strlen($old_row['dadanwancheng']) < 2){
			echo json_encode(array('code' => 1, 'msg' => 'please tick booking order in top of shipment page'));
			return;
		}
        if($old_row['lock_lv'] > 0){
            echo json_encode(array('code' => 1, 'msg' => 'locked, no editable! '));
            return;
        }
        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($temp != "") {
                $data[$item] = $temp;
            }
        }
        if(isset($data['consol_id']) && $data['consol_id'] > 0){
            if($old_row['lock_lv'] > 0){//锁了不能操作
                echo json_encode(array('code' => 1, 'msg' => 'shipment locked. Failure'));
                return;
            }
            Model('biz_consol_model');
            $consol = $this->biz_consol_model->get_by_id($data['consol_id']);
            if($consol['lock_lv'] > 0){//锁了不能操作
                echo json_encode(array('code' => 1, 'msg' => 'consol locked, Failure'));
                return;
            }
            //2022-03-18 由于没有shipment的consol也允许做账单,这里加入判断,consol有账单时,不能绑定上去
            Model('biz_bill_model');
            $this->db->select('id');
            $bill = $this->biz_bill_model->get_where_one("id_type = 'consol' and id_no = '{$data['consol_id']}'");;
            if(!empty($bill)){
                return jsonEcho(array('code' => 1, 'msg' => 'CONSOL have billing,please delete billing'));
            }
            if($old_row['trans_origin']!=$consol["trans_origin"]){
                echo json_encode(array('code' => 1, 'msg' => 'POL not the same'));
                return;
            }
            if($old_row['trans_destination']!=$consol["trans_destination"]){
                echo json_encode(array('code' => 1, 'msg' => 'POD not the same'));
                return;
            }
        }
        if (isset($data['consol_id'])) {
            if($old_row['status'] != 'normal'){
                echo json_encode(array('code' => 1, 'msg' => 'SHIPMENT not normal status'));
                return;
            }
            //2021-06-11 汪庭彬 2、shipment退关，需要先解除consol绑定，如果没解除，不给退关，如果consol解绑后没有shipment了，进入error状态
            //2021-07-22 汪庭彬 解绑shipment时 张路遥提出 订舱预配勾选优化 http://wenda.leagueshipping.com/?/question/288
            if($data['consol_id'] == 0){
                if(strlen($old_row['xiangyijingang'])>2) exit(json_encode(array('code' => 1, 'msg' => 'shipment cfs in, please uncheck it')));

                $this->db->select('id');
                $shipements = $this->biz_shipment_model->get_shipment_by_consol($old_row['consol_id']);
                $ids = array_column($shipements, 'id');
                if(sizeof($ids) == 1 && in_array($id, $ids)){
                    //前置条件：   consol必须没有费用输入的情况下才能解绑最后1个shipment， 否则不让解绑。先删除费用
                    Model('biz_bill_model');
                    $bill = $this->biz_bill_model->get_where_one("id_type = 'consol' and id_no = {$old_row['consol_id']}");
                    if(!empty($bill)){
                        echo json_encode(array('code' => 1, 'msg' => 'CONSOL has billing , failure'));
                        return;
                    }
                    Model('biz_consol_model');
                    $this->db->select('id,yupeiyifang,carrier_ref,status');
                    $consol = $this->biz_consol_model->get_by_id($old_row['consol_id']);

                    //以下关单号为consol里的关单号，即MBL
                    //2021-08-25 加入cancel时，不用判定
                    if($consol['status'] != 'cancel'){
                        if($consol['carrier_ref'] != ''){
                            //2，有关单号 无预配确认，无法解绑，跳提示：consol存在预配，请确认是否有效，若有效，请标注预配确认后解绑。
                            if(empty($consol['yupeiyifang'])){
                                echo json_encode(array('code' => 1, 'msg' => 'consol存在预配，请确认是否有效，若有效，请标注预配确认后解绑。 若无预配，请先退关'));
                                return;
                            }
                        }else{
                            if(!empty($consol['yupeiyifang'])){
                                //4，无关单号，有预配确认，无法解绑，跳提示：预配确认必须得有MBL。
                                echo json_encode(array('code' => 1, 'msg' => '预配确认必须得有MBL，解绑失败'));
                                return;
                            }
                        }
                    }
                }
                //解绑时自动清空shipment关单号
                $data['cus_no'] = '';
                $consol_id = $old_row['consol_id'];
            }else{
                $consol_id = $data['consol_id'];
            }
        }

        $this->biz_shipment_model->update($id, $data);
        if(isset($data['consol_id'])) consol_status($consol_id);
        //同步shipper consignee notify--start
        if(isset($data['consol_id']) && $data['consol_id'] > 0){
            $this->tb_consol($data['consol_id'], $old_row);
        }
        //同步shipper consignee notify--end
        //更新账单
        update_bill_ETD('shipment', $id);
        $data['id'] = $id;

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment";
        $log_data["key"] = $id;
        if(isset($data['consol_id']) && $data['consol_id'] > 0){
            $log_data['master_table_name'] = "biz_consol";
            $log_data['master_key'] = $data['consol_id'];
            $log_data["action"] = "bind_update";
        }else{
            $log_data["action"] = "update";
        }

        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        //2021-11-03 再加个判断当前未开票就不能勾选 http://wenda.leagueshipping.com/?/question/565
        //2021-11-03 17:17改为提示,不强制
        if(!isset($data['consol_id']) && isset($data['yizuofeiyong']) && $data['yizuofeiyong'] != '0' && $old_row['invoice_flag'] == 0){
            echo json_encode(array('code' => 2, 'msg' => 'billing not invoiced'));
            return;
        }

        echo json_encode(array('code' => 0, 'msg' => 'success'));
    }

    public function table_tree($type = 'read')
    {
        $data = array();
        $this->field_edit = array();
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            $this->lock_filed[$row[4]][] = $v;
            if(isset($row[5]) && !empty($row[5])){
                $this->field_edit[$row[5]][] = $v;
            }else{
                array_push($this->field_edit, $v);
            }
        }
        //将需要的duty字段加入----start
        foreach ($this->admin_field as $val){
            $this->field_edit['staff info'][] = $val;
        }
        //将需要的duty字段加入----end
        $temp = array();
        foreach ($this->field_edit as $key => $val) {
            if(is_array($val)){
                $cd_temp = array();
                foreach ($val as $v){
                    array_push($cd_temp, array("id" => "1$type." . $v, "text" => lang($v)));
                }
                array_push($temp, array('id' => "1$type." . $key, 'text' => lang($key), 'children' => $cd_temp, 'not_add' => 1));
            }else{
                array_push($temp, array("id" => "1$type." . $val, "text" => lang($val)));
            }
        }
        array_push($data, array("id" => "1$type", "text" => lang("$type"), "children" => $temp));
        echo json_encode($data);
    }

    /**
     * 危险品数据的处理（不包括新增）
     * @param $old_row shipment单条数据
     * @param $goods_type 货物类型
     * @param $goods  危险品数据
     * @return bool|int
     */
    public function dangergoods_handle($old_row, $goods_type, $goods)
    {
        //存在危险品关联时，进行删除
        $this->load->model('biz_shipment_dangergoods_model');
        if (!empty($old_row['dangergoods_id'])) {
            //不等于普通货物，进行修改，等于则删除
            if ($goods_type != 'GC' && $goods_type != 'RF') {
                $this->biz_shipment_dangergoods_model->update($old_row['dangergoods_id'], $goods);
                return false;
            } else {
                $this->biz_shipment_dangergoods_model->mdelete($old_row['dangergoods_id']);
                return 0;
            }
        } else {
            //不等于普通货物时，进行危险品的新增
            if ($goods_type != 'GC' && $goods_type != 'RF' && $goods_type != '') {
                $goods['shipment_id'] = $old_row['id'];
                $dangergoods_id = $this->biz_shipment_dangergoods_model->save($goods);
                return $dangergoods_id;
            } else {
                return false;
            }
        }
    }

    public function lock(){
        $id = postValue('id', 0);
        $level = postValue('level', 0);
        if($id != 0){
            $data = array();
            //判断consol是否锁过,锁了不给锁
			$this->db->select('id,consol_id,lock_lv,chuanyiqihang,status,invoice_flag,trans_carrier,cus_no,from_db,from_id_no');
            $shipment = $this->biz_shipment_model->get_duty_one("id = '{$id}'");
            $lock_role = lock_role('biz_shipment', $id, $shipment['lock_lv'], $level);
            if($lock_role){
                echo json_encode(array('code' => 1, 'msg' => $lock_role));
                return;
            }
			if($level==0){
				// 解锁判断下 from_db是不是s2
				if(!empty($shipment['from_db'])){
					$from_db = $this->load->database($shipment['from_db'],true);
					$check_from_db_s2 = $from_db->query("select lock_lv,job_no from biz_shipment where id = {$shipment['from_id_no']}")->row_array();
					if(!empty($check_from_db_s2) && $check_from_db_s2['lock_lv']>=2){
						echo json_encode(array('code' => 1, 'msg' => "pls let 【{$shipment['from_db']}】 system unlock S2 for job no 【{$check_from_db_s2['job_no']}】"));
						return;
					}
				}
			}

			if(isset($shipment['consol_id']) && $shipment['consol_id'] != 0){
                Model('biz_consol_model');
                $this->db->select("id,lock_lv,customer_booking,trans_carrier, status,report_date," .
                    "(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_consol' and biz_duty_new.id_no = biz_consol.id and biz_duty_new.user_role = 'marketing') as marketing_id");
                $consol = $this->biz_consol_model->get_one('id', $shipment['consol_id']);

                //2021-10-18 http://wenda.leagueshipping.com/?/question/533 锁C1 和 S1的时候，检查 consol的实际开航日，如果为空不让锁
                if($shipment['lock_lv'] < $level && $consol['status'] != 'cancel' && $consol['report_date'] == '0000-00-00'){
                    return jsonEcho(array('code' => 1, 'msg' => lang('请先到 consol 里填写 report_date')));
                }

//                if($level == 1 && $consol['trans_carrier'] == '') return jsonEcho(array('code' => 1,'msg' => 'S1锁时,consol承运人必填'));

                //2021-08-12 汪庭彬 徐婕提出客户自订舱 无需C3锁 可以直接锁S1 市场为空 无需C3锁 可以直接锁S1 http://wenda.leagueshipping.com/?/question/347
                $consol_update = array();
                if($level  == 0){
                    $consol_update['lock_lv'] = 0;
                }else{
                    $consol_update['lock_lv'] = 3;
                }
                $this->biz_consol_model->update($shipment['consol_id'], $consol_update);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_consol";
                $log_data["key"] = $shipment['consol_id'];
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($consol_update);
                log_rcd($log_data);
            }
            // else{
            //     echo json_encode(array('code' => 1, 'msg' => 'consol不存在,锁失败'));
            //     return;
            // }
            //2021-09-06 汪庭彬 新加锁S2时，如果船开未勾，那么自动勾上 http://wenda.leagueshipping.com/?/question/431
            if($level == 2 && $shipment['status'] == 'normal' && empty($shipment['chuanyiqihang'])){
                $data['chuanyiqihang'] = date('Y-m-d H:i:s');
            }

            if($level == 0 && $shipment['lock_lv'] == 1) $data['lock_s1'] = 0;
            if($level == 0 && $shipment['lock_lv'] == 2) $data['lock_s2'] = 0;
            if($level == 0 && $shipment['lock_lv'] == 3) $data['lock_s3'] = 0;
            if($level == 1) $data['lock_s1'] = 1;
            if($level == 2) $data['lock_s2'] = 1;
            if($level == 3) $data['lock_s3'] = 1;
            $this->tax_diff($id, $level);

            $data['lock_lv'] = $level;
            $data["bujiti_operator"] = 0;
            $data["bujiti_sales"] = 0;
            $data["bujiti_customer_service"] = 0;
            $this->biz_shipment_model->update($id, $data);
            lock_log('biz_shipment', $id, $level);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment";
            $log_data["key"] = $id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
            $data['id'] = $id;
            echo json_encode(array('code' => 0, 'msg' => 'success'));
        }
    }

    //补税差
    private function tax_diff($shipment_id, $lock){
        //2022-06-15 加入了S1锁时,会自动比较系统里的金额税率差,如果最终税率差 不等于 0 那么生成一笔对应的账单
        if($lock == 1){
            Model('biz_bill_model');

            //这里先查一下是否有指定费用名称的费用 FWFS
            $this->db->select('id,amount');
            $charge_bill = $this->biz_bill_model->get_where_one("charge_code = 'FWFS' and id_type = 'shipment' and id_no = '$shipment_id' and parent_id = 0");

            $this->db->select('vat,type, local_amount');
            //专项排除
            $bills = $this->biz_bill_model->get("id_type = 'shipment' and id_no = '$shipment_id'",'id','desc',0, '');

            $amount = 0;
            foreach ($bills as $bill){
                $vat = str_replace('%', '', $bill['vat']);
                if($vat == '无票') $vat = 0;//无票的暂时按照0处理,后续看是否不处理
                $local_amount = $bill['type'] == 'cost' ? -$bill['local_amount'] : $bill['local_amount'];

                $amount += round($local_amount * ($vat / 100), 2);
            }

            //判断上面获取到的指定费用,是否和当前的金额相同,不相同,删除后新增, 如果为空,那么直接新增
            if(!empty($charge_bill)){
                //2022-06-29 金额一样的没有跳过,已修复BUG
                if($charge_bill['amount'] != $amount) $this->biz_bill_model->mdelete($charge_bill['id']);
                else return true;
            }
            //如果amount小于0,那么需要生成
            if($amount > 0){
                $data = array(
                    'charge_code' => 'FWFS',
                    'unit_price' => $amount,
                    'number' => 1,
                    'type' => 'cost',
                    'client_code' => 'SHJHCW02', //统一付给聚翰
                    'remark' => '',
                    'currency' => 'CNY',
                    'vat' => '0%',
                    'is_special' => 0,
                    'split_mode' => 0,
                    'rate_id' => 0,
                    'id_type' => 'shipment',
                    'id_no' => $shipment_id,
                );
                add_bill($data);
            }

        }
    }

    public function get_role_id_group(&$duty){
        $userRole = $this->session->userdata('user_role');
//        $user = $this->bsc_user_model->get_one('id', $this->session->userdata('id'));

        //获取伙伴的id，group数据后，以id为键名
//        $partners = array_column($this->bsc_user_model->get_user_relation_ids($default_partner_ids), null, 'id');
        foreach ($this->admin_field as $val){
            $mr_id = 0;
            $mr_group = '';
            if(in_array($val, $userRole)){
                //不为空，且当前权限人员存在时
                $mr_id = $this->session->userdata('id');
                $mr_group = join(',', $this->session->userdata('group'));
            }

            $duty[$val . '_id'] = isset($_POST[$val . '_id']) && !empty($_POST[$val . '_id']) ? $_POST[$val . '_id'] : $mr_id;
            $duty[$val . '_group'] = isset($_POST[$val . '_group']) && !empty($_POST[$val . '_group']) ? $_POST[$val . '_group'] : $mr_group;
        }
    }

    /**
     * 修改表格数据用
     */
    public function update_data_table(){
        $id = $_REQUEST["id"];
        $old_row = $this->biz_shipment_model->get_duty_one("id = '{$id}'");
        $data = array();

        if(empty($old_row)) return jsonEcho(array('code' => 1, 'msg' => 'SHIPMENT 不存在', 'isError' => true));

        if($old_row['lock_lv'] > 0) return jsonEcho(array('code' => 1, 'msg' => '已锁无法修改', 'isError' => true));

        $field = array('cus_no', 'consol_id_apply');


        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($old_row[$item] != $temp)
                $data[$item] = is_array($temp) ? $temp : trim($temp);
        }


        if(isset($data['consol_id_apply']) && $data['consol_id_apply'] != 0){
            //2022-03-18 由于没有shipment的consol也允许做账单,这里加入判断,consol有账单时,不能绑定上去
            Model('biz_bill_model');
            $bill = $this->biz_bill_model->get_where_one("id_type = 'consol' and id_no = '{$data['consol_id_apply']}'");;
            if(!empty($bill)){
                return jsonEcho(array('code' => 1, 'msg' => '该CONSOL已有费用,不能申请,请删除consol费用后再试', 'isError' => true));
            }
            //2022-10-19 服务选项权限从页面移动到这里, 避免由于服务选项没权限导致的页面报错
            $service_options_arr = json_decode($old_row['service_options'], true);
            if (!$service_options_arr) $services = array();
            else $services = array_column($service_options_arr, 0);

            if (!in_array("booking", $services)) {
                return jsonEcho(array('code' => 1, 'msg' => '没勾选订舱服务', 'isError' => true));
            }
        }

        // if(empty($old_row['dadanwancheng'])){
        //     return jsonEcho(array('code' => 1, 'msg' => 'Please tick 打单勾 first.', 'isError' => true));
        // }

        $this->biz_shipment_model->update($id, $data);
        $data['id'] = $id;


        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        $data['code'] = 0;
        $data['msg'] = '保存成功';
        echo json_encode($data);
    }

    /**
     * 根据当前shipment的duty进行更新consol
     * @param $consol_id int
     */
    public function consol_duty_update($consol_id){
        if(empty($consol_id)) return false;
        Model('biz_duty_model');
        $this->db->select('id,user_id,user_group');
        $shipments_duty = $this->biz_duty_model->get("id_type = 'biz_shipment' and id_no in (select id from biz_shipment where consol_id = $consol_id)");
        $consol_duty = array();
        $consol_duty['biz_table'] = 'biz_consol';

        $sales_ids = array();
        $sales_groups = array();
        foreach ($shipments_duty as $shipment_duty){
            if(!in_array($shipment_duty['user_id'], $sales_ids))$sales_ids[] = $shipment_duty['user_id'];
            if(!in_array($shipment_duty['user_group'], $sales_groups))$sales_groups[] = $shipment_duty['user_group'];
        }

        $consol_duty['sales_id'] = join(',', $sales_ids);
        $consol_duty['sales_group'] = join(',', $sales_groups);
        $this->biz_duty_model->update_by_idno($consol_id, $consol_duty);
        //将consol的销售进行更新--end

        //TODO 增加一个 如果consol的当前agent,不存在当前的销售,则直接清空agent
    }

    public function update_company($data,$shipment = array()){

        //shipper consignee notify 同步--start
        $biz_company_model = Model('biz_company_model');
        //如果consol和shipment都没有client_code的话,这里不进行任何处理
        if(!isset($data['client_code'], $shipment['client_code'])){
            return false;
        }

        //不存在查询下shipment
        if(!isset($data['client_code'])){
            $data['client_code'] = $shipment['client_code'];
        }
        if(!isset($data['shipper_company'])){
            $data['shipper_company'] = $shipment['shipper_company'];
        }
        if(!isset($data['consignee_company'])){
            $data['consignee_company'] = $shipment['consignee_company'];
        }
        if(!isset($data['notify_company'])){
            $data['notify_company'] = $shipment['notify_company'];
        }
        $shipper = $biz_company_model->get_where_one("client_code = '{$data['client_code']}' and company_type = 'shipper' and company_name = '" . es_encode($data['shipper_company'], "'", false) . "'");
        if(!empty($shipper)){
            //如果有值更新,直接进行同步
            $update_shipper = array();
            if(isset($data['shipper_address']) && $data['shipper_address'] !== $shipper['company_address']){
                $update_shipper['company_address'] = $data['shipper_address'];
            }
            if(isset($data['shipper_contact']) && $data['shipper_contact'] !== $shipper['company_contact']){
                $update_shipper['company_contact'] = $data['shipper_contact'];
            }
            if(isset($data['shipper_telephone']) && $data['shipper_telephone'] !== $shipper['company_telephone']){
                $update_shipper['company_telephone'] = $data['shipper_telephone'];
            }
            if(isset($data['shipper_email']) && $data['shipper_email'] !== $shipper['company_email']){
                $update_shipper['company_email'] = $data['shipper_email'];
            }
            if(!empty($update_shipper)){
                $biz_company_model->update($shipper['id'], $update_shipper);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_company";
                $log_data["key"] = $shipper['id'];
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($update_shipper);
                log_rcd($log_data);
            }
        }
        //consignee
        $consignee = $biz_company_model->get_where_one("client_code = '{$data['client_code']}' and company_type = 'consignee' and company_name = '" . es_encode($data['consignee_company'], "'", false) . "'");
        if(!empty($consignee)){
            //如果有值更新,直接进行同步
            $update_consignee = array();
            if(isset($data['consignee_address']) && $data['consignee_address'] !== $consignee['company_address']){
                $update_consignee['company_address'] = $data['consignee_address'];
            }
            if(isset($data['consignee_contact']) && $data['consignee_contact'] !== $consignee['company_contact']){
                $update_consignee['company_contact'] = $data['consignee_contact'];
            }
            if(isset($data['consignee_telephone']) && $data['consignee_telephone'] !== $consignee['company_telephone']){
                $update_consignee['company_telephone'] = $data['consignee_telephone'];
            }
            if(isset($data['consignee_email']) && $data['consignee_email'] !== $consignee['company_email']){
                $update_consignee['company_email'] = $data['consignee_email'];
            }
            if(!empty($update_consignee)){
                $biz_company_model->update($consignee['id'], $update_consignee);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_company";
                $log_data["key"] = $consignee['id'];
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($update_consignee);
                log_rcd($log_data);
            }
        }
        //notify
        $notify = $biz_company_model->get_where_one("client_code = '{$data['client_code']}' and company_type = 'notify' and company_name = '" . es_encode($data['notify_company'], "'", false) . "'");
        if(!empty($notify)){
            //如果有值更新,直接进行同步
            $update_notify = array();
            if(isset($data['notify_address']) && $data['notify_address'] !== $notify['company_address']){
                $update_notify['company_address'] = $data['notify_address'];
            }
            if(isset($data['notify_contact']) && $data['notify_contact'] !== $notify['company_contact']){
                $update_notify['company_contact'] = $data['notify_contact'];
            }
            if(isset($data['notify_telephone']) && $data['notify_telephone'] !== $notify['company_telephone']){
                $update_notify['company_telephone'] = $data['notify_telephone'];
            }
            if(isset($data['notify_email']) && $data['notify_email'] !== $notify['company_email']){
                $update_notify['company_email'] = $data['notify_email'];
            }
            if(!empty($update_notify)){
                $biz_company_model->update($notify['id'], $update_notify);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_company";
                $log_data["key"] = $notify['id'];
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($update_notify);
                log_rcd($log_data);
            }
        }
        //shipper consignee notify 同步--end
    }

    /**
     * 同步consol数据
     * @param $consol_id
     * @param $data
     */
    private function tb_consol($consol_id = 0, $data = array()){
        $tb_fields = array('shipper_company','shipper_address','shipper_contact','shipper_telephone','shipper_email',
            'consignee_company','consignee_address', 'consignee_contact', 'consignee_telephone',  'consignee_email',
            'notify_company', 'notify_address', 'notify_contact', 'notify_telephone', 'notify_email', 'description', 'mark_nums');
        //没有consol不同处理
        if(empty($consol_id)){
            return false;
        }
        $shipments = $this->biz_shipment_model->get_shipment_by_consol($consol_id);
        //全同步
        if(sizeof($shipments) > 0){
            //不是mbl不同步
            if($shipments[0]['hbl_type'] !== 'MBL'){
                return false;
            }
            $consol_update = array();
            foreach ($tb_fields as $tb_field){
                if(isset($data[$tb_field]))$consol_update[$tb_field] = $data[$tb_field];
            }
            if(!empty($consol_update)){
                Model('biz_consol_model');
                $this->biz_consol_model->update($consol_id, $consol_update);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_consol";
                $log_data["key"] = $consol_id;
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($consol_update);
                log_rcd($log_data);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function batch_lock(){
        $shipment_ids = isset($_POST['shipment_ids']) ? $_POST['shipment_ids'] : '';
        $lock_lv = isset($_POST['lock_lv']) ? (int)$_POST['lock_lv'] : 3;
        $special_test = 'shipment_batch_lock' . $lock_lv;


        if(!menu_role($special_test)){
            echo json_encode(array('code' => 1, 'msg' => '没有权限'));
            return;
        }

        //批量检测shipment
        $shipment_ids_array = array_filter(explode(',', $shipment_ids));

        $no_consol_shipment = array();
        $no_consol_shipment_str = '';
        $lock_shipments = array();
        $lock_consols = array();
        $consol_no_lock = array();
        $shipment_lock_ne = array();//锁等级不满足
        $consol_atd_null = array();//consolATD没值
        $cus_no_null = array();//cus_no没值

        $result_data = array();
        $result_data[0] = array('key_name' => '不是当前销售', 'data' => array());
        $result_data[1] = array('key_name' => 'CONSOL专项金额之和不等于0', 'data' => array());
        $result_data[2] = array('key_name' => '不是当前操作的上级', 'data' => array());
        $result_data[3] = array('key_name' => '锁S1时consol承运人必填', 'data' => array());
        // $result_data[1] = array('key_name' => '未开票状态', 'data' => array());

        Model('biz_shipment_model');
        Model('biz_duty_model');
        Model('biz_consol_model');
        $success = 0;
        $fail = 0;
        if(!empty($shipment_ids_array)){
            $this->db->select('biz_shipment.id,biz_shipment.cus_no,biz_shipment.trans_carrier,biz_shipment.job_no,biz_shipment.lock_lv,biz_shipment.consol_id,biz_shipment.chuanyiqihang,biz_shipment.status,biz_shipment.invoice_flag,' .
                '(select lock_lv from biz_consol where biz_consol.id = biz_shipment.consol_id) as consol_lock_lv, ' .
                '(select customer_booking from biz_consol where biz_consol.id = biz_shipment.consol_id) as customer_booking,' .
                "(select group_concat(bsc_user.name) from bsc_user where bsc_user.id in (select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'operator')) as operator_name," .
                "(select group_concat(bsc_user.name) from bsc_user where bsc_user.id in (select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'sales')) as sales_name" .
                '');
            $shipments = $this->biz_shipment_model->no_role_get("id in (" . join(',', $shipment_ids_array) . ") and lock_lv < 3");
            if(empty($shipments)){
                echo json_encode(array('code' => 1, 'msg' => '已全部S3锁'));
                return;
            }
            $consol_ids = array_column($shipments, 'consol_id');
            $this->db->select('id,trans_carrier,trans_ATD,' .
                "(select group_concat(bsc_user.name) from bsc_user where bsc_user.id in (select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_consol' and biz_duty_new.id_no = biz_consol.id and biz_duty_new.user_role = 'marketing')) as marketing_name");
            $consol_duty = Model('biz_consol_model')->get("id in (" . join(',', $consol_ids) . ")");
            $consol_duty_users = array_column($consol_duty, null, 'id');
            foreach ($shipments as $shipment){
                //未绑定consol的跳过
                //2021-06-07 汪庭彬 未绑定的直接锁 http://wenda.leagueshipping.com/?/question/165 shipment里也修改了一个只能查看未锁的shipment
                // if($shipment['consol_id'] == 0){
                //     $no_consol_shipment[] = $shipment['job_no'];
                //     continue;
                // }
                //2021-05-26 加入业务锁的顺序限制 C3 < S1 < S2 < S3 强制顺序
                //锁等级未到2的shipment
                if($shipment['lock_lv'] < ($lock_lv - 1)){
                    $shipment_lock_ne[] = $shipment['job_no']. '-' . $shipment['operator_name'] . '-' . $shipment['sales_name'];
                    $fail++;
                    continue;
                }
                if($shipment['lock_lv'] == 1 && $lock_lv == 2){
                    $this_lock_role = lock_role('biz_shipment', $shipment['id'], $shipment['lock_lv'], $lock_lv);
                    if($this_lock_role){
                        $result_data[0]['data'][] = $shipment['job_no']. '-' . $shipment['operator_name'] . '-' . $shipment['sales_name'];
                        $fail++;
                        continue;
                    }
                }
                if($shipment['lock_lv'] == 0 && $lock_lv == 1){
                    $this_lock_role = lock_role('biz_shipment', $shipment['id'], $shipment['lock_lv'], $lock_lv);
                    if($this_lock_role){
                        $result_data[2]['data'][] = $shipment['job_no']. '-' . $shipment['operator_name'] . '-' . $shipment['sales_name'];
                        $fail++;
                        continue;
                    }
                }
                // //2021-11-03 再加个判断当前未开票就不能S1 http://wenda.leagueshipping.com/?/question/565
                // if($lock_lv == 1 && $shipment['invoice_flag'] == 0){
                //     $result_data[1]['data'][] = $shipment['job_no']. '-' . $shipment['operator_name'] . '-' . $shipment['sales_name'];
                //     $fail++;
                //     continue;
                // }
                //2021-10-22日 修复了相同锁状态,会锁上的bug
                if($shipment['lock_lv'] == $lock_lv || $shipment['lock_lv'] > $lock_lv){
                    $success++;
                    continue;
                }
                // cus_no不能为空
                if($shipment['consol_id'] != 0 && $shipment['lock_lv'] < $lock_lv && empty($shipment['cus_no']) && $shipment['status']=='normal'){
                    $cus_no_null[] = $shipment['job_no']. '-' . $shipment['operator_name'] . '-' . $shipment['sales_name'];
                    $fail++;
                    continue;
                }
                //2021-10-18 http://wenda.leagueshipping.com/?/question/533 锁C1 和 S1的时候，检查 consol的实际开航日，如果为空不让锁 
                if($shipment['consol_id'] != 0 && $shipment['lock_lv'] < $lock_lv && $consol_duty_users[$shipment['consol_id']]['trans_ATD'] == '0000-00-00 00:00:00'){
                    $consol_atd_null[] = $shipment['job_no']. '-' . $shipment['operator_name'] . '-' . $shipment['sales_name'];
                    $fail++;
                    continue;
                }


                //锁S1时候,加一个承运人必填检测
                //2022-06-20 承运人必填检测改到做账那里去
//                if($shipment['consol_id'] != 0 && $consol_duty_users[$shipment['consol_id']]['trans_carrier'] == ''){
//                    $result_data[3]['data'][] = $shipment['job_no']. '-' . $shipment['operator_name'] . '-' . $shipment['sales_name'];
//                    $fail++;
//                    continue;
//                }
                //consol未锁的跳过
                if($shipment['consol_id'] != 0 && $shipment['consol_lock_lv'] <= 0){
                    //2021-08-12 汪庭彬 徐婕提出客户自订舱 无需C3锁 可以直接锁S1 市场为空 无需C3锁 可以直接锁S1 http://wenda.leagueshipping.com/?/question/347
                    if($shipment['customer_booking'] == 1 || $consol_duty_users[$shipment['consol_id']]['marketing_name'] == ''){
                        //只限制锁的时候
                        //2021-12-14 金晶要求consol锁的时候,专项金额之和必须等于0 http://wenda.leagueshipping.com/?/question/642
                        //查询consol的专项账单
                        Model('biz_bill_model');
                        $this->db->select('(amount * exrate) as local_amount');
                        $special_bills = $this->biz_bill_model->get("id_type = 'consol' and id_no = '{$shipment['consol_id']}'", 'id', 'desc', 1);
                        if(array_sum(array_column($special_bills, 'local_amount')) != 0){
                            $result_data[1]['data'][] = $shipment['job_no']. '-' . $shipment['operator_name'] . '-' . $shipment['sales_name'] . '-' . $consol_duty_users[$shipment['consol_id']]['marketing_name'];
                            $fail++;
                            continue;
                        }
                        $lock_consols[] = array('id' => $shipment['consol_id'], 'lock_lv' => 3);
                    }else{
                        $consol_no_lock[] = $shipment['job_no']. '-' . $shipment['operator_name'] . '-' . $shipment['sales_name'] . '-' . $consol_duty_users[$shipment['consol_id']]['marketing_name'];//
                        $fail++;
                        continue;
                    }

                }

                $lock_shipment = array('id' => $shipment['id'], 'lock_lv' => $lock_lv);
                //2021-09-06 汪庭彬 新加锁S2时，如果船开未勾，那么自动勾上 http://wenda.leagueshipping.com/?/question/431
                if($lock_lv == 2 && $shipment['status'] == 'normal' && empty($shipment['chuanyiqihang'])){
                    $lock_shipment['chuanyiqihang'] = date('Y-m-d H:i:s');
                }
                if($lock_lv == 0 && $shipment['lock_lv'] == 1) $lock_shipment['lock_s1'] = 0;
                if($lock_lv == 0 && $shipment['lock_lv'] == 2) $lock_shipment['lock_s2'] = 0;
                if($lock_lv == 0 && $shipment['lock_lv'] == 3) $lock_shipment['lock_s3'] = 0;
                if($lock_lv == 1) $lock_shipment['lock_s1'] = 1;
                if($lock_lv == 2) $lock_shipment['lock_s2'] = 1;
                if($lock_lv == 3) $lock_shipment['lock_s3'] = 1;
                $lock_shipments[] = $lock_shipment;
                $success++;
            }
        }

        $result_data[] = array('key_name' => 'CONSOL未锁的SHIPMENT', 'data' => $consol_no_lock);
        $result_data[] = array('key_name' => 'SHIPMENT锁等级未达到S' . ($lock_lv - 1), 'data' => $shipment_lock_ne);
        $result_data[] = array('key_name' => '实际开航日未填写的', 'data' => $consol_atd_null);
        $result_data[] = array('key_name' => 'shipment关单号为空', 'data' => $cus_no_null);

        if(empty($lock_shipments)){
            echo json_encode(array('code' => 0, 'msg' => '没有可修改的值:<br />成功:' . $success . '票<br/ > 失败:' . $fail . '票', 'data' => $result_data));
            return;
        }

        foreach ($lock_consols as $lock_consol){
            $update_data = $lock_consol;
            $this_id = $update_data['id'];
            unset($update_data['id']);

            $this->biz_consol_model->update($this_id, $update_data);

            lock_log('biz_consol', $this_id, $update_data['lock_lv']);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_consol";
            $log_data["key"] = $this_id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($update_data);
            log_rcd($log_data);
        }

        //批量修改数据
        $this->biz_shipment_model->mbatch_update($lock_shipments);
        
        foreach ($lock_shipments as $lock_shipment){
            $update_data = $lock_shipment;
            $this_id = $update_data['id'];
            unset($update_data['id']);
            unset($update_data['old_lock_lv']);

            // $this->tax_diff($this_id, $update_data['lock_lv']);
            lock_log('biz_shipment', $this_id, $update_data['lock_lv']);


            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment";
            $log_data["key"] = $this_id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($update_data);
            log_rcd($log_data);
        }
        // $result_data[] = array('key_name' => 'SHIPMENT已锁', 'data' => $lock_shipments);

        echo json_encode(array('code' => 0, 'msg' => '修改成功<br />成功:' . $success . '票<br/ > 失败:' . $fail . '票', 'data' => $result_data));
    }

    public function batch_lock3(){
        // $shipment_ids = isset($_POST['shipment_ids']) ? $_POST['shipment_ids'] : '';
        return error('1', lang('功能已关闭'));
        $userId = get_session('id');
        if($userId != 20061 && !is_admin()){
            echo json_encode(array('code' => 1, 'msg' => '马嬿雯的特殊权限'));
            return;
        }

        $this_lock = 3;

        // if(!special_test('batch_lock3')){
        //     echo json_encode(array('code' => 1, 'msg' => '没有权限'));
        //     return;
        // }
        $parent_id = isset($_GET['sub']) ? strval($_GET['sub']) : '';

        //-------这个查询条件想修改成通用的 -------------------------------
        $carrier_ref = isset($_REQUEST['carrier_ref']) ? trim($_REQUEST['carrier_ref']) : '';
        $label = isset($_REQUEST['label']) ? $_REQUEST['label'] : array();
        $step_info = isset($_REQUEST['step_info']) ? $_REQUEST['step_info'] : array();
        $lock_lv = isset($_REQUEST['lock_lv']) ? $_REQUEST['lock_lv'] : array();
        $consol_lock_lv = isset($_REQUEST['consol_lock_lv']) ? $_REQUEST['consol_lock_lv'] : array();

        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $where = array();
        $unmerge = true;

        $trans_ATD_start = '';
        $trans_ATD_end = "";
        $title_data = get_user_title_sql_data('biz_shipment', 'biz_shipment_index');//获取相关的配置信息
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if($f == 'biz_shipment.trans_ATD'){
                    if($s == '<='){
                        $trans_ATD_end = $v;
                    }
                    if($s == '>='){
                        $trans_ATD_start = $v;
                    }
                    if($s == '='){
                        $trans_ATD_start = $v;
                        $trans_ATD_end = $v;
                    }
                }

                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];

                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];

                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }
                //服务选项的不等于用not like 处理
                $json_field = array('service_options', 'biz_shipment.service_options');
                if(in_array($f, $json_field) && $s == '!='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} not like '%$v%'";
                    }
                    continue;
                }
                if($f == 'biz_shipment.cus_no' || $f == 'cus_no'){
                    //2021-07-27 新增关单号可查询子shipment的
                    $this->db->select('parent_id');
                    $child_shipment = $this->biz_shipment_model->get_where_one_all(search_like($f, $s, $v) . ' and parent_id != 0');
                    $cus_no_where = search_like($f, $s, $v);
                    if(!empty($child_shipment)){
                        $cus_no_where = "(" . $cus_no_where . " or biz_shipment.id = " . $child_shipment['parent_id'] . ")";
                    }
                    if($v != '') $where[] = $cus_no_where;
                    continue;
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];


                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') $v = '';
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) $title_data['sql_join'][$title_data['base_data'][$title_f]['sql_join']] = $this_join;

                    $where[] = search_like($f, $s, $v);
                }
            }
        }

        //超过2个月不能让锁，防止批量误操作
        if(($trans_ATD_end=='')){
            echo json_encode(array('code' => 1, 'msg' => '实际离泊必填'));
            return;
        }
        if((strtotime($trans_ATD_end) - strtotime($trans_ATD_start))> 3600*24*60){
            echo json_encode(array('code' => 1, 'msg' => '实际离泊范围不能超过2个月'));
            return;
        }
        // if ($carrier_ref != ''){
        //     Model('m_model');
        //     //2022-06-14 如果是查的MBL,那么从另一张表里获取
        //     $mbl_mores = $this->m_model->query_array("select consol_id from biz_consol_mbl_more where " . search_like('mbl_no', 'like', $carrier_ref));

        //     if(!empty($mbl_mores)) $where[] = "biz_consol.id in ('" . join('\',\'', array_column($mbl_mores, 'consol_id')) . "')";
        //     else $where[] = "0 = 1";
        //     // $where[] = "biz_consol.carrier_ref like '%$carrier_ref%'";
        // }
        if($label !== array()){
            if(in_array(1, $label)) $where[] = '(select over_credit from biz_client where biz_client.client_code = biz_shipment.client_code) = 1';//扣单
            if(in_array(2, $label)) $where[] = "invoice_flag = 0";//未开票
            if(in_array(3, $label)) $where[] = "loss_flag = 1";//亏损
            if(in_array(4, $label)) $where[] = "payment_flag = 0";//未核销
        } ;//标签 扣单 亏损 未开票
        if($step_info !== array()){
            if(in_array('dadanwancheng', $step_info)) $where[] = "biz_shipment.dadanwancheng != '0'";
            if(in_array('haiguancangdan', $step_info)) $where[] = "biz_shipment.haiguancangdan != '0'";
            if(in_array('baoguanjieshu', $step_info)) $where[] = "biz_shipment.baoguanjieshu != '0'";
            if(in_array('xiangyijingang', $step_info)) $where[] = "biz_shipment.xiangyijingang != '0'";
            if(in_array('haiguanfangxing', $step_info)) $where[] = "biz_shipment.haiguanfangxing != '0'";
            if(in_array('matoufangxing', $step_info)) $where[] = "biz_shipment.matoufangxing != '0'";
            if(in_array('peizaifangxing', $step_info)) $where[] = "biz_shipment.peizaifangxing != '0'";
            if(in_array('chuanyiqihang', $step_info)) $where[] = "biz_shipment.chuanyiqihang != '0'";
            if(in_array('tidanqueren', $step_info)) $where[] = "biz_shipment.tidanqueren != '0'";
            if(in_array('tidanqianfa', $step_info)) $where[] = "biz_shipment.tidanqianfa != '0'";
            if(in_array('yizuofeiyong', $step_info)) $where[] = "biz_shipment.yizuofeiyong != '0'";
        } ;
        $lock_lv_where = array();
        foreach ($lock_lv as $val){
            $lock_lv_where[] = "biz_shipment.lock_lv = $val";
        }
        if(!empty($lock_lv_where)) $where[] = '( ' . join(' or ', $lock_lv_where) . ' )';
        $consol_lock_lv_where = array();
        foreach ($consol_lock_lv as $val){
            $consol_lock_lv_where[] = "biz_consol.lock_lv = $val";
        }
        if(!empty($consol_lock_lv_where)) $where[] = '( ' . join(' or ', $consol_lock_lv_where) . ' )';

        if($parent_id != ''){
            $unmerge = false;
            $where[] = "biz_shipment.parent_id = '$parent_id'";
        }
        $where[] = "biz_shipment.lock_lv < 3";
        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $this->db->select('biz_shipment.id,biz_shipment.job_no,biz_shipment.lock_lv,biz_shipment.consol_id,biz_consol.lock_lv as consol_lock_lv,' .
            '(select group_concat(bsc_user.name) from bsc_user where bsc_user.id in (select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = \'biz_shipment\' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = \'operator\')) as operator_name,' .
            '(select group_concat(bsc_user.name) from bsc_user where bsc_user.id in (select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = \'biz_shipment\' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = \'sales\')) as sales_name');
        //将order字段替换为 对应的sql字段
        foreach ($title_data['sql_join'] as $j){
            if($j[0] == 'biz_consol') continue;
            $this->db->join($j[0], $j[1], $j[2]);
        }
        $this->db->join("biz_consol", "biz_consol.id = biz_shipment.consol_id", "left");
        $shipments = $this->biz_shipment_model->get_v3($where, 'biz_shipment.id', 'desc', $unmerge);

        $lock_shipments = array();
        // $shipment_ext_datas = array();
        $lock_consols = array();
        $result_data = array();
        $success = 0;
        $fail = 0;

        //批量检测shipment
        // $shipment_ids_array = array_filter(explode(',', $shipment_ids));

        Model('biz_shipment_model');
        Model('biz_consol_model');
        Model('biz_duty_model');
        Model('biz_shipment_ext_model');
        Model('m_model');

        if(empty($shipments)){
            echo json_encode(array('code' => 1, 'msg' => '已全部S3锁'));
            return;
        }
        $consol_ids = array_column($shipments, 'consol_id');
        $this->db->select('id,trans_carrier,trans_ATD,' .
            "(select group_concat(bsc_user.name) from bsc_user where bsc_user.id in (select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_consol' and biz_duty_new.id_no = biz_consol.id and biz_duty_new.user_role = 'marketing')) as marketing_name");
        $consol_duty = Model('biz_consol_model')->get("id in (" . join(',', $consol_ids) . ")");
        $consol_duty_users = array_column($consol_duty, null, 'id_no');

        //获取账单数据
        $bill_profits = array_column($this->m_model->query_array("select sum(if(`type`='cost',-local_amount,local_amount)) as profit,id_no from biz_bill where id_type='shipment' and id_no in ('" . join('\',\'', array_column($shipments, 'id')) . "') group by id_no;"), null, 'id_no');

        $result_data[0] = array('key_name' => '实际开航日未填写的', 'data' => array());
        foreach ($shipments as $shipment){
            //未绑定consol的跳过
            //2021-06-07 汪庭彬 未绑定的直接锁 http://wenda.leagueshipping.com/?/question/165 shipment里也修改了一个只能查看未锁的shipment
            // if($shipment['consol_id'] == 0){
            //     $no_consol_shipment[] = $shipment['job_no'];
            //     continue;
            // }
            //2021-10-18 http://wenda.leagueshipping.com/?/question/533 锁C1 和 S1的时候，检查 consol的实际开航日，如果为空不让锁
            if($shipment['consol_id'] != 0 && $shipment['lock_lv'] < $lock_lv && $consol_duty_users[$shipment['consol_id']]['trans_ATD'] == '0000-00-00 00:00:00'){
                $result_data[0]['data'][] = $shipment['job_no']. '-' . $shipment['operator_name'] . '-' . $shipment['sales_name'];
                $fail++;
                continue;
            }
            //consol未锁的跳过
            if($shipment['consol_id'] != 0 && $shipment['consol_lock_lv'] <= 0){
                $lock_consol = array('id' => $shipment['consol_id'], 'lock_lv' => 3);// 
                $lock_consols[] = $lock_consol;
                // continue;
            }
            if($shipment['lock_lv'] == 3){
                continue;
            }
//            $lock_shipment = array('id' => $shipment['id'], 'lock_lv' => $this_lock, 'old_lock_lv' =>$shipment['lock_lv']);
            $lock_shipment = array('id' => $shipment['id'], 'lock_lv' => $this_lock);

            //存储需要修改的ext的数据
            $bill_profit = isset($bill_profits[$shipment['id']]) ? $bill_profits[$shipment['id']]['profit'] : 0;
            if($bill_profit>=0){
                if($shipment["lock_lv"]<1){
                    $lock_shipment["bujiti_operator"] = 1;

                    $this->biz_shipment_ext_model->save(array("shipment_id"=>$shipment['id'],"ext_type"=>"bujiti","bujiti_operator_reason"=>"未及时锁S1"));
                    // $shipment_ext_datas[] = array("shipment_id"=>$shipment['id'],"ext_type"=>"bujiti","bujiti_operator_reason"=>"未及时锁S1");
                }else{
                    $lock_shipment["bujiti_operator"] = 0;
                }
                if($shipment["lock_lv"]==1){
                    $lock_shipment["bujiti_sales"] = 1;
                    $lock_shipment["bujiti_customer_service"] = 1;

                    $this->biz_shipment_ext_model->save(array("shipment_id"=>$shipment['id'],"ext_type"=>"bujiti","bujiti_sales_reason"=>"未及时锁S2","bujiti_customer_service_reason"=>"未及时锁S2"));
                }else{
                    $lock_shipment["bujiti_sales"] = 0;
                }
            }

            if($this_lock == 3) $lock_shipment['lock_s3'] = 1;
            $lock_shipments[] = $lock_shipment;
            $success++;
        }

        //批量修改数据
        foreach ($lock_consols as $lock_consol){
            $update_data = $lock_consol;
            $this_id = $update_data['id'];
            unset($update_data['id']);
            $this->biz_consol_model->update($this_id, $update_data);

            lock_log('biz_consol', $this_id, $update_data['lock_lv']);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_consol";
            $log_data["key"] = $this_id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($update_data);
            log_rcd($log_data);
        }

        //批量修改数据
        $this->biz_shipment_model->mbatch_update($lock_shipments);
        // $this->biz_shipment_ext_model->mbatch_update($shipment_ext_datas);

        //下面的插入日志丢到队列里执行
        //由于text 只能存一定量,这里切成500个一组执行
        //后面如果变量多了,可能这里的切割值得再缩小,或者把数据库字段扩大
        //可能因为这里导致的报错,先改为100一批
        $limit = 100;
        for($i = 0; $i <= sizeof($lock_shipments) / $limit; $i++){
            save_queue('批量锁S3的生成日志', 'biz_shipment', -1, array('user_id' => get_session('id'), 'lock_shipments' => array_slice($lock_shipments, $i * $limit,($i + 1) * $limit)), '', 'batch_lock3_log');
        }

        echo json_encode(array('code' => 0, 'msg' => '修改成功<br />成功:' . $success . '票<br/ > 失败:' . $fail . '票', 'data' => $result_data));
    }

    private function shipment_check($shipment)
    {
        $check = array();

        $shipment_id = isset($shipment['id']) ? $shipment['id'] : 0;
        $consol_id = isset($shipment['consol_id']) ? $shipment['consol_id'] : 0;
        //2022-02-09 如果是子shipment,consol_id去父那里取
        if($shipment['parent_id'] != 0){
            $this->db->select('consol_id');
            $consol_id = $this->biz_shipment_model->get_by_id($shipment['parent_id'])['consol_id'];
        }

        if ($shipment == 0) return $check;
        //2021-07-29 箱型箱量6月1号前的限制，在一个月后去除
        //2021-09-13 由于改配会出现在海放后的原因，这里改为船开后才验证箱型箱量
        if ($shipment['trans_mode'] == 'FCL' && $shipment['chuanyiqihang'] != 0 && $shipment['chuanyiqihang'] != '0000-00-00 00:00:00' && $shipment['chuanyiqihang'] != '0000-00-00' && $shipment['status'] != 'cancel' ) { //  && strtotime($shipment['booking_ETD']) >= strtotime('2021-06-01') 2021-08-31 汪庭彬 已经取消了订舱船期在6月1号之前不验证的代码
            Model('biz_shipment_container_model');
            $s_box_info = json_decode($shipment['box_info'], true);
            //2021-08-05 修复了如果为0时，箱型箱量也会不同的BUG
            foreach($s_box_info as $key => $row){
                if($row['num'] == 0) unset($s_box_info[$key]);
            }

            $shipment_containers = array();
            if ($consol_id) {
                $this->db->select('(select container_size from biz_container where container_no = biz_shipment_container.container_no AND biz_container.consol_id = \'' . $consol_id . '\' limit 1) as container_size,(select seal_no from biz_container where container_no = biz_shipment_container.container_no AND biz_container.consol_id = \'' . $consol_id . '\' limit 1) as seal_no');
                $shipment_containers = $this->biz_shipment_container_model->get("shipment_id = $shipment_id");
            }
            $sc_box_info = array();
            $container_size = array_column($shipment_containers, 'container_size');
            $rows = array_count_values(array_filter($container_size));
            foreach ($rows as $key => $row){
                $sc_box_info[] = array('size' => $key, 'num' => $row);
            }
            //排序--start
            $s = array_column($sc_box_info, 'size');
            array_multisort($s, SORT_DESC, $sc_box_info);
            $s = array_column($s_box_info, 'size');
            array_multisort($s, SORT_DESC, $s_box_info);
            if ($sc_box_info != $s_box_info) {
                $check['s_c_box_info'] = 'Quantiy in Container page is not the same what you booked.';
            }
        }
        Model('biz_consol_model');
        // $this->db->select('biz_type,trans_mode,trans_tool,customer_booking');
        // $consol = $this->biz_consol_model->get_by_id($consol_id);

        //免箱期验证-----end

        //组合页面文字描述
        $result = array('check' => $check);
        $result['shipment_msg'] = '';
        $not_result_field = array();
        if(!empty($check)){
            $result['shipment_msg'] .= '<span style="color:red"> ' . lang('warn') . '</span>:' . join(',', $check);
        }
        return $result;
    }

    /**
     * 查询SHIPMENT没有consol且未申请
     **/
    public function get_not_consol(){
        $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
        $limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 20 ;
        $orderfield = isset($_POST['orderfield']) ? $_POST['orderfield'] : 'id' ;
        $order = isset($_POST['order']) ? $_POST['order'] : 'DESC' ;
        $is_apply = isset($_GET['is_apply']) ? (int)$_GET['is_apply'] : 0;
        $consol_id = isset($_GET['consol_id']) ? (int)$_GET['consol_id'] : 0;

        $this->db->select('trans_origin, trans_destination');
        $consol = Model('biz_consol_model')->get_by_id($consol_id);
        if(empty($consol)){
            echo json_encode(array());
            return;
        }

        $result = array();
        $where = array();

        if($is_apply == 0) $where[] = "biz_shipment.consol_id_apply = 0";
        else $where[] = "biz_shipment.consol_id_apply != 0";

        $where[] = "biz_shipment.consol_id = 0";
        $where[] = "biz_shipment.status = 'normal'";

        //2021-07-19 下面条件同get_error_consol
        $where[] = "biz_shipment.trans_origin = '{$consol['trans_origin']}'";
        $where[] = "biz_shipment.trans_destination = '{$consol['trans_destination']}'";
        //2021-06-11 只查看pending
        //3、申请errorlist改为pendinglist，只有	pending的consol

        $where = join(' and ', $where);


        $result['total'] = $this->biz_shipment_model->total($where);
        $offset = ($page - 1) * $limit;
        $this->db->limit($limit, $offset);
        $this->db->select("biz_shipment.id,biz_shipment.job_no,biz_shipment.consol_id_apply,biz_shipment.trans_carrier, (select client_name from biz_client where biz_client.client_code = biz_shipment.trans_carrier) as trans_carrier_name, biz_shipment.booking_ETD");
        $rs = $this->biz_shipment_model->get($where);

        $rows = array();

        foreach ($rs as $row){
            $row['box_info_json'] = $row['box_info'];

            $box_info = json_decode($row['box_info'], true);
            $box_info = array_map(function ($val){
                return $val['size'] . '*' . $val['num'];
            }, $box_info);

            $row['box_info'] = join(',', $box_info);

            $rows[] = $row;
        }
        $result['rows'] = $rows;
        echo json_encode($result);
    }

    /**
     * 根据传入的角色 将shipment的同步到consol里 http://wenda.leagueshipping.com/?/question/316
     **/
    public function tb_duty_user(){
        $role = isset($_GET['role']) ? $_GET['role'] : '';
        $shipment_id = isset($_GET['id']) ? (int)$_GET['id'] : 0;
        $result = array('code' => 1, 'msg' => '参数错误');
        if(!in_array($role, array('operator', 'customer_service'))){
            echo json_encode($result);
            return;
        }
        if($role == ''){
            echo json_encode($result);
            return;
        }
        //根据当前传入的角色同步consol的操作为shipment的
        $this->db->select('id,consol_id');
        $shipment = $this->biz_shipment_model->get_by_id($shipment_id);
        if(empty($shipment)){
            $result['msg'] = 'SHIPMENT不存在';
            echo json_encode($result);
            return;
        }
        $consol_id = $shipment['consol_id'];
        if($consol_id == 0){
            $result['msg'] = '请先绑定CONSOL';
            echo json_encode($result);
            return;
        }

        //将consol的角色更新到shipment对应的角色--start
        $role_id = "{$role}_id";
        $role_group = "{$role}_group";

        Model('biz_duty_model');
        $shipment_duty = $this->biz_duty_model->get_old_by_new('biz_shipment', $shipment_id);
        if(empty($shipment_duty)){
            $result['msg'] = '发生错误';
            echo json_encode($result);
            return;
        }

        $duty_update = array();
        $duty_update['biz_table'] = 'biz_consol';
        $duty_update[$role_id] = $shipment_duty[$role_id];
        $duty_update[$role_group] = $shipment_duty[$role_group];
        $this->biz_duty_model->update_by_idno($consol_id, $duty_update);
        //将consol的角色更新到shipment对应的角色--start
        $this->consol_duty_update($consol_id);
        $result['code'] = 0;
        $result['msg'] = '同步成功';
        echo json_encode($result);
    }

    public function cancel_apply(){
        $id = postValue('id', 0);
        $result = array('code' => 1, 'msg' => '参数错误');
        if(empty($id)){
            echo json_encode($result);
            return;
        }

        $this->db->select('id, consol_id_apply');
        $old_row = $this->biz_shipment_model->get_by_id($id);
        if(empty($old_row)){
            $result['msg'] = 'shipment已不存在';
            echo json_encode($result);
            return;
        }
        if($old_row['consol_id_apply'] == 0){
            $result['msg'] = '该sihpment未处于待申请状态';
            echo json_encode($result);
            return;
        }
        $update_data = array('consol_id_apply' => 0);
        $this->biz_shipment_model->update($id, $update_data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($update_data);
        log_rcd($log_data);

        $result['code'] = 0;
        $result['msg'] = '撤销成功';
        echo json_encode($result);
    }

    /**
     * 通过shipment获取子shipment
     */
    public function shipment_children_get(){
        $parent_id=isset($_GET['parent_id'])?$_GET['parent_id']:'';
        if(!empty($parent_id)){
            $shipment_children=$this->db->where('parent_id',$parent_id)->get('biz_shipment')->result_array();
            echo json_encode($shipment_children);
        }
    }

    /**
     * 用于各种地方执行前获取提示使用
     */
    public function shipment_check_before(){
        $id = getValue('id', 0);
        $check_type = getValue('check_type', '');
        if($check_type == 'pre_alert'){//用于shipment勾选预报时
            //预报只需要获取有多少个shipment即可
            $shipment_count = $this->biz_shipment_model->no_role_total("consol_id != 0 and consol_id = (select consol_id from biz_shipment bs1 where bs1.id = '{$id}')");

            return jsonEcho(array('code' => 0, 'msg' => '同1个consol下属于你的shipment共有' . $shipment_count . '个,将一起勾上,是否确认', 'data' => array('shipment_count' => $shipment_count)));
        }else{
            return jsonEcho(array('code' => 1, 'msg' => '暂不支持'));
        }
    }

    /**
     * 获取未绑定,但是待绑定的shipment
     */
    public function get_binding_shipments(){
        //该方法只能看到自己有权限看到的未绑定的shipment
        $job_no = trim(postValue('job_no', ''));

        $where = array();

        $where[] = "biz_shipment.consol_id = 0";
        $where[] = "biz_shipment.status = 'normal'";
        $where[] = "biz_shipment.parent_id = 0";

        if($job_no != '') $where[] = "biz_shipment.job_no like '%{$job_no}%'";

        $where_str = join(' and ', $where);

        $this->db->select('biz_shipment.id,biz_shipment.job_no,biz_shipment.client_company');
        $this->db->limit(10);
        $rs = $this->biz_shipment_model->no_role_get($where_str, 'biz_shipment.id', 'desc');

        $result = array('rows' => array(), 'total' => sizeof($rs));

        foreach ($rs as $row){
            $result['rows'][] = $row;
        }

        return jsonEcho($result);
    }

    /**
     * 拆分生成子shipment
     * @param int $id
     * @return bool|string
     *  2021-06-29  http://wenda.leagueshipping.com/?/question/237 原：第一个继承父，其余空白，改为空白类型完全继承填入的值
     */
    public function new_split_shipment(){
        //1、传入参数
        // 参数1 :父shipment id
        // 参数2 :拆分后后缀类型
        //         数字 类型1 拆分后 以job_no-1、2、3、4,
        //         字母 类型2 拆分后 以job_no-A、B、C、D
        //         空白 类型3 拆分后 生成一个和原本一样的, 之后 报关单号空白
        // 参数3 :拆分个数
        //2、
        $id = isset($_GET['shipment_id']) ? $_GET['shipment_id'] : 0;
        $type = isset($_GET['type']) ? $_GET['type'] : 0;
        $count = isset($_GET['count']) ? $_GET['count'] : 0;

        $this_shipment = $this->biz_shipment_model->get_duty_one("id = '{$id}'");
        if(empty($this_shipment)){
            echo json_encode(array('code' => 1, 'msg' => 'shipment不存在'));
            return;
        }
        //该shipment不能是子shipment
        if($this_shipment['parent_id'] != 0){
            echo json_encode(array('code' => 1, 'msg' => '该shipment不支持拆分'));
            return;
        }
        $cellName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        , );
        //'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ'

        //查询当前有几个子shipment
        $child_shipment = $this->biz_shipment_model->no_role_total("parent_id = '$id'");
        $type_arr = array('1','2','3');
        if(!in_array($type, $type_arr)){
            echo json_encode(array('code' => 1, 'msg' => '暂不支持拆分类型'));
            return;
        }

        //需要继承的值
        $inherit_field = array(
            'job_no',
            'client_code', 'client_code2','client_company', 'client_address', 'client_contact', 'client_telephone', 'client_email', 'shipper_company', 'shipper_address',
            'shipper_contact', 'shipper_telephone', 'shipper_email', 'consignee_company', 'consignee_address', 'consignee_contact', 'consignee_telephone', 'consignee_email',
            'notify_company', 'notify_address', 'notify_contact', 'notify_telephone', 'notify_email', 'trans_mode', 'biz_type', 'booking_ETD', 'status', 'hbl_no', 'hbl_type',
            'AGR_no', 'shipper_ref', 'release_type', 'trans_origin', 'trans_origin_name', 'trans_discharge', 'trans_discharge_name', 'trans_destination', 'trans_destination_name', 'sailing_code', 'trans_carrier', 'cus_no', 'trans_term',
            'INCO_term', 'requirements', 'description', 'mark_nums', 'goods_type', 'hs_code', 'dadanwancheng', 'haiguancangdan', 'baoguanjieshu',
            'tidanqueren', 'tidanqianfa','service_options','dangergoods_id','good_outers_unit','good_weight_unit','good_volume_unit','INCO_option','cus_no',
            'box_info', 'trans_destination_inner'
        );
        //继承后 job_no 后面-数字   客户单号后面加后缀
        //继承duty
        $data = array();
        foreach ($inherit_field as $item) {
            $data[$item] = $this_shipment[$item];
        }
        $data['box_info'] = isset($data['box_info']) ? $data['box_info'] : '[]';
        if(empty($data['cus_no'])){
            echo json_encode(array('code' => 1,'msg' => '报关单号不能为空'));
            return;
        }
        //2022-07-18 新增如果没有子shipment时, 不能只拆1个
        if($child_shipment == 0 && $count == 1) return jsonEcho(array('code' => 1, 'msg' => '当前shipment未存在子shipment, 不能单独拆1票'));

        $cus_nos = isset($_POST['cus_nos']) ? $_POST['cus_nos'] : '';
        $cus_nos_array = explode("\n", $cus_nos);
        $cus_nos_array = array_filter(array_trim($cus_nos_array));
        //当子shipment为0时,后面加一个@
        if(substr($this_shipment['cus_no'], -1, 1) !== '@'){
            if($child_shipment == 0){
                $this_shipment_cus_no = $this_shipment['cus_no'] . '@';
                $this->biz_shipment_model->update($this_shipment['id'], array('cus_no' => $this_shipment_cus_no));
            }
        }else{
            $data['cus_no'] = substr($this_shipment['cus_no'], 0, strlen($this_shipment['cus_no']) - 1);
        }
        for ($i = 0; $i < $count; $i++){
            $add_data = $data;
            $add_data['job_no'] = $add_data['job_no'] . '-' . ($child_shipment + 1);
            //如果没有子shipment 且类型为3 则直接继承
            $cus_no1 = substr($add_data['cus_no'], 0, strlen($add_data['cus_no']) - 4);
            $cus_no2 = substr($add_data['cus_no'], -4, 4);
            $cus_no = $add_data['cus_no'];
            if($type == 1){
                //数字 类型1 拆分后 以job_no-1、2、3、4,
                if($child_shipment > 0) {
                    $suffix = $child_shipment;
                    $cus_no = $cus_no1 . str_pad((int)$cus_no2 + $suffix, 4, '0', STR_PAD_LEFT);
                }
            }else if($type == 2){
                //字母 类型2 拆分后 以job_no-A、B、C、D
                if($child_shipment > 0) {
                    $suffix = $cellName[$child_shipment - 1];
                    $cus_no = $cus_no . $suffix;
                }
            }else if($type == 3){
                //空白 类型3 拆分后 生成一个和原本一样的, 之后 根据A B C D
                $cus_no = '';
                if(isset($cus_nos_array[$i])) $cus_no = $cus_nos_array[$i];
            }
            $add_data['cus_no'] = $cus_no;
            $add_data['parent_id'] = $this_shipment['id'];
            $shipment_id = $this->biz_shipment_model->save($add_data);
            $child_shipment++;

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment";
            $log_data["key"] = $shipment_id;
            $log_data["action"] = "insert";
            $log_data["value"] = json_encode($add_data);
            log_rcd($log_data);

            //继承duty
            $this->load->model('biz_duty_model');
            $this_duty = $this->biz_duty_model->get_old_by_new('biz_shipment', $this_shipment['id']);
            if(!empty($this_duty)){
                unset($this_duty['id']);
                $this_duty['biz_table'] = 'biz_shipment';
                $this_duty['id_no'] = $shipment_id;
                $duty_id = $this->biz_duty_model->save($this_duty);
            }
            //继承危险品数据
            if($this_shipment['dangergoods_id'] != 0){
                $this->load->model('biz_shipment_dangergoods_model');
                $this_dangergoods = $this->biz_shipment_dangergoods_model->get_one('id', $this_shipment['dangergoods_id']);
                if(!empty($this_dangergoods)){
                    unset($this_dangergoods['id']);
                    $this_dangergoods['shipment_id'] = $shipment_id;
                    $danger_goods_id = $this->biz_shipment_dangergoods_model->save($this_dangergoods);
                    $this->biz_shipment_model->update($shipment_id, array('danger_goods_id', $danger_goods_id));

                    // record the log
                    $log_data = array();
                    $log_data["table_name"] = "biz_shipment_dangergoods";
                    $log_data["key"] = $danger_goods_id;
                    $log_data["action"] = "insert";
                    $log_data["value"] = json_encode($this_dangergoods);
                    log_rcd($log_data);
                }
            }

        }

        echo json_encode(array('code' => 0, 'msg' => '拆分成功'));
    }

    /**
     * 删除shipment, 子shipment那里会用到
     */
    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $data = $this->biz_shipment_model->get_duty_one("id = '{$id}'");
        //关联删除--start
        //child_shipment--start
        $old_rows = $this->biz_shipment_model->get_one_all("parent_id = '$id'");
        if(!empty($old_rows)){
            echo json_encode(array('isError' => true, 'msg' => '子shipment数据未删除'));
            return;
        }
        //child_shipment--end
        //shipment_container--start
        $this->load->model('biz_shipment_container_model');
        $old_rows = $this->biz_shipment_container_model->get("shipment_id = '$id'");
        if(!empty($old_rows)){
            echo json_encode(array('isError' => true, 'msg' => 'container数据未删除'));
            return;
        }
        //shipment_container--end
        //bill--start
        $this->load->model('biz_bill_model');
        $old_rows = $this->biz_bill_model->not_role_get("id_type = 'shipment' and id_no = '$id'");
        if(!empty($old_rows)){
            echo json_encode(array('isError' => true, 'msg' => 'bill数据未删除'));
            return;
        }
        //bill--end
        //truck--start
        $this->load->model('biz_shipment_truck_model');
        $old_rows = $this->biz_shipment_truck_model->no_role_get("shipment_id = '$id'");
        if(!empty($old_rows)){
            echo json_encode(array('isError' => true, 'msg' => 'truck数据未删除'));
            return;
        }
        //truck--end
        //2021-11-29 不能是参与过shipment合并的
        Model('biz_shipment_combine_model');
        $combine_log = $this->biz_shipment_combine_model->get_where_one("status = 0 and (shipmentA_id = $id or shipmentB_id = $id or shipmentB_copy_id = $id)");
        if(!empty($combine_log)){
            echo json_encode(array('isError' => true, 'msg' => '参与过合并,无法删除'));
            return;
        }

        //shipment_dangergoods--start
        if ($data['dangergoods_id'] != 0) {
            $this->load->model('biz_shipment_dangergoods_model');
            $old_row = $this->biz_shipment_dangergoods_model->get_one('id', $data['dangergoods_id']);
            if(!empty($old_row)){
                $this->biz_shipment_dangergoods_model->mdelete($old_row['id']);
                //save the operation log
                $log_data = array();
                $log_data["table_name"] = "biz_shipment_dangergoods";
                $log_data["key"] = $id;
                $log_data["action"] = "delete";
                $log_data["value"] = json_encode($old_row);
                log_rcd($log_data);
            }
        }
        //shipment_dangergoods--end
        //duty--start
        $this->load->model('biz_duty_model');
        $this->biz_duty_model->mdelete($id, 'biz_shipment');
        //duty--end
        //关联删除--end

        $this->biz_shipment_model->mdelete($id);
        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        echo json_encode(array('success' => true));
    }
    
    /**
     * 根据当前切换的数据库类型, 来获取
     */
    public function global_shipment_crm(){
        $database_config = array('CN' => 'china', 'TH' => 'th', 'SG' => 'sg', 'AE' => 'ae', 'VN' => 'vn');
        unset($database_config[strtoupper(get_system_type())]);
        $url_pre = get_system_type();
        //默认排除本地数据库
        if(is_ajax()){
            $page = postValue('page', 1);
            $rows = postValue('rows', 30);
            //进来默认是CN库
            $db = postValue('db', 'CN');
            if(!isset($database_config[$db])){
                $db = 'CN';
            }
            $connet_db = $database_config[$db];
            $this->connet_db = $this->load->database($connet_db, true);
            
            $offset = ($page - 1) * $rows;
            $sql = "select {field} from 
                (select 
        		biz_shipment.client_company
        		,biz_shipment.client_contact
        		,biz_shipment.client_telephone
        		,biz_shipment.client_email
        		,(select role from biz_client where biz_client.client_code = biz_shipment.client_code) as client_role
        		,biz_shipment.consignee_company
        		,biz_shipment.consignee_address
        		,biz_shipment.consignee_contact
        		,biz_shipment.consignee_telephone
        		,biz_shipment.consignee_email
        		,biz_shipment.booking_ETD
        		,concat(vessel, ' ', voyage) as vessel_voyage
        		,biz_consol.vessel
        		,biz_consol.voyage
        		,biz_shipment.trans_origin_name
        		,biz_shipment.trans_destination_name
        		,biz_shipment.INCO_term
        		,biz_shipment.trans_term
        		,(select client_name from biz_client where biz_client.client_code = biz_shipment.trans_carrier) as trans_carrier
        		from (select * from biz_shipment HAVING 1 order by booking_ETD desc) biz_shipment
        		LEFT JOIN biz_consol on biz_consol.id = biz_shipment.consol_id
        		where 
        		biz_shipment.parent_id = 0   
        		and biz_shipment.status = 'normal'
        		and biz_shipment.hbl_type = 'MBL'
        		and biz_shipment.biz_type = 'export'
        		and biz_shipment.trans_destination like '$url_pre%'
        		GROUP BY biz_shipment.consignee_company) global_shipment_crm {limit}
            ";
        
            $rs = $this->connet_db->query(str_replace(array('{field}', '{limit}'), array('*', "limit {$offset},{$rows}"), $sql))->result_array();
            $total = $this->connet_db->query(str_replace(array('{field}', '{limit}'), array('count(*) as c', ''), $sql))->row_array()['c'];
            $rows = array();
            
            //获取提取次数
            $clue_name_arr = array_map(function($val){
                return match_chinese($val);
            },array_column($rs, 'consignee_company'));
            $sql = "select clue_name, count(*) as c from biz_client_crm where clue_name in ('" . join('\',\'', $clue_name_arr) . "') group by clue_name";
            $clue_count = array_column($this->db->query($sql)->result_array(), null, 'clue_name');
            
            foreach ($rs as $row){
                $row['client_info'] = "";
                //委托方字段，还要判断是factory 还是 logistics client， 如果factory，直接不用显示。 如果是logistic client，则显示名称，回车，联系方式 
                $client_role = explode(',', $row['client_role']);
                if(in_array('logistics_client', $client_role)){
                    $row['client_info'] .= $row['client_company'];
                    $row['client_info'] .= "<br/>" . $row['client_contact'];
                    $row['client_info'] .= "<br/>TEL: " . $row['client_telephone'];
                    $row['client_info'] .= "<br/>EMAIL: " . $row['client_email'];
                }
                $clue_name = match_chinese($row['consignee_company']);
                $row['clue_count'] = isset($clue_count[$clue_name]) ? $clue_count[$clue_name]['c'] : 0;
                $rows[] = $row;
            }
            
            return jsonEcho(array('rows' => $rows, 'total' => $total, $clue_count, $sql));
        }
        $data = array();
        $data['database_config'] = $database_config;
        $this->load->view('head');
        $this->load->view('/biz/shipment/global_crm_view', $data);
    }

	public function basic_steps($id=0){
		$field = $this->field_edit;
		$data = array();
		foreach ($field as $item) {
			$data[$item] = "";
		}
		$consol = array();
		if ($id != 0) {
			$shipment = $this->biz_shipment_model->get_duty_one('id ='.$id);
			if(!empty($shipment)){
				$data = $shipment;
				$this->load->model('biz_consol_model');
				$consol = $this->biz_consol_model->get_duty_one('id='.$shipment['consol_id']);
				if(empty($consol)){
					$consol['operator_si'] = '0';
					$consol['carrier_ref'] = '';
					$consol['document_si'] = '0';
					$consol['creditor'] = '';
					$consol['lock_lv'] = 0;
				}
			}else{
				echo 'shipment不存在';
				return;
			}
		}
		pass_role($data, $this->admin_field);

		$userId = $this->session->userdata('id');
		$userBsc = $this->bsc_user_model->get_by_id($userId);
		$this->load->model('bsc_user_role_model');
		$userRole = $this->bsc_user_role_model->get_data_role();
		$userBsc = array_merge($userBsc, $userRole);
		//页面传假值
		$userBsc['edit_text'] = isset($_POST['edit_text']) ? $_POST['edit_text'] : $userBsc['edit_text'];
		$userBsc['read_text'] = isset($_POST['read_text']) ? $_POST['read_text'] : $userBsc['read_text'];
		$data1 = array();
		foreach ($data as $key => $value) {
			$data1[$key] = '***';
		}
		$userBscRead = explode(",", (isset($userBsc['read_text']) ? $userBsc['read_text'] : ''));
		$g_read_table = false; //全表读
		$g_edit_table = false; //全表可写
		foreach ($userBscRead as $row) {
			if (strpos($row, 'shipment.') !== false) {
				$arr_temp = explode(".", $row);
				$key = $arr_temp[1];
				$data1[$key] = array_key_exists($key,$data) ? $data[$key] : '';
			} else if ($row == 'shipment') {
				// 全表
				$g_read_table = true;
				break;
			}
		}
		if ($g_read_table) {
			$data1 = $data;
		}
		$data1["id"] = $id;
		$userBscEdit1 = explode(",", (isset($userBsc['edit_text']) ? $userBsc['edit_text'] : ''));
		$userBscEdit = array();
		$lock_field = array();
		for ($i = 1; $i <= $data['lock_lv']; $i++){
			if(isset($this->lock_filed[$i]))$lock_field = array_merge($lock_field, $this->lock_filed[$i]);
		}
		foreach ($userBscEdit1 as $row) {
			if (strpos($row, 'shipment.') !== false) {
				$arr_temp = explode(".", $row);
				//是否处于被锁的字段，锁住无法修改
				if(!in_array($arr_temp[1], $lock_field))array_push($userBscEdit, $arr_temp[1]);
			} else if ($row == 'shipment') {
				// 全表
				$g_edit_table = true;
				break;
			}
		}
		if ($g_edit_table) {
			$userBscEdit = array_diff(array_keys($data), $lock_field);
		}
		$data1['G_userBscEdit'] = $userBscEdit;
		if(!empty($consol)){
			if($consol['operator_si'] !== '0' && $shipment['hbl_type']=='MBL' || $shipment['tidanqianfa'] !=='0'){

				Model('biz_shipment_si_model');
				$si_field = $this->biz_shipment_si_model->getSiField();

				$data1['G_userBscEdit'] = array_diff($data1['G_userBscEdit'], $si_field);
			}
			if($consol['document_si'] != '0'){;
				$data1['G_userBscEdit'] = array_diff($data1['G_userBscEdit'], array('client_code', 'client_company', 'hbl_type'));
			}
		}
		if(isset($data['consol_id_apply']) && $data['consol_id_apply'] != 0){
			$data1['G_userBscEdit'] = array();
		}
		$this->load->view('head');
		$this->load->view('biz/shipment/basic_steps_view', $data1);
	}

	public function basic_sft($id = 0){
		$field = $this->field_edit;
		$data = array();
		foreach ($field as $item) {
			$data[$item] = "";
		}
		$consol = array();
		if ($id != 0) {
			$shipment = $this->biz_shipment_model->get_duty_one("id = '{$id}'");
			if(!empty($shipment)){
				$data = $shipment;
				$this->load->model('biz_consol_model');
				$consol = $this->biz_consol_model->get_duty_one("id = '{$shipment['consol_id']}'");
			}else{
				echo 'shipment不存在';
				return;
			}
		}
		pass_role($data, $this->admin_field);

		$userId = $this->session->userdata('id');
		$userBsc = $this->bsc_user_model->get_by_id($userId);
		$this->load->model('bsc_user_role_model');
		$userRole = $this->bsc_user_role_model->get_data_role();
		$userBsc = array_merge($userBsc, $userRole);
		//页面传假值
		$userBsc['edit_text'] = isset($_POST['edit_text']) ? $_POST['edit_text'] : $userBsc['edit_text'];
		$userBsc['read_text'] = isset($_POST['read_text']) ? $_POST['read_text'] : $userBsc['read_text'];
		$data1 = array();
		foreach ($data as $key => $value) {
			$data1[$key] = '***';
		}
		$userBscRead = explode(",", (isset($userBsc['read_text']) ? $userBsc['read_text'] : ''));
		$g_read_table = false; //全表读
		$g_edit_table = false; //全表可写
		foreach ($userBscRead as $row) {
			if (strpos($row, 'shipment.') !== false) {
				$arr_temp = explode(".", $row);
				$key = $arr_temp[1];
				$data1[$key] = array_key_exists($key,$data) ? $data[$key] : '';
			} else if ($row == 'shipment') {
				// 全表
				$g_read_table = true;
				break;
			}
		}
		if ($g_read_table) {
			$data1 = $data;
		}
		$data1["id"] = $id;
		$userBscEdit1 = explode(",", (isset($userBsc['edit_text']) ? $userBsc['edit_text'] : ''));
		$userBscEdit = array();
		$lock_field = array();
		for ($i = 1; $i <= $data['lock_lv']; $i++){
			if(isset($this->lock_filed[$i]))$lock_field = array_merge($lock_field, $this->lock_filed[$i]);
		}
		foreach ($userBscEdit1 as $row) {
			if (strpos($row, 'shipment.') !== false) {
				$arr_temp = explode(".", $row);
				//是否处于被锁的字段，锁住无法修改
				if(!in_array($arr_temp[1], $lock_field))array_push($userBscEdit, $arr_temp[1]);
			} else if ($row == 'shipment') {
				// 全表
				$g_edit_table = true;
				break;
			}
		}
		if ($g_edit_table) {
			$userBscEdit = array_diff(array_keys($data), $lock_field);
		}
		$data1['G_userBscEdit'] = $userBscEdit;
		$this->load->model('biz_client_model');
		$data1['client'] = $this->biz_client_model->get_one('client_code', $shipment['client_code']);

		if(!empty($consol) && $shipment['biz_type'] == 'export'){
			if(($consol['operator_si'] !== '0' && $shipment['hbl_type']=='MBL') || $shipment['tidanqianfa'] !=='0'){
				Model('biz_shipment_si_model');
				$si_field = $this->biz_shipment_si_model->getSiField();

				$data1['G_userBscEdit'] = array_diff($data1['G_userBscEdit'], $si_field);
			}
			if($consol['document_si'] != '0'){
				$data1['G_userBscEdit'] = array_diff($data1['G_userBscEdit'], array('client_code', 'client_company', 'hbl_type'));
			}
		}
		if(isset($data['consol_id_apply']) && $data['consol_id_apply'] != 0){
			$data1['G_userBscEdit'] = array();
		}
		$data1['r_e'] = false;
		if (isset($_POST['edit_text']) || isset($_POST['read_text'])) $data1['r_e'] = true;

		$data1['is_use_billing'] = false;
		if(!empty($data1['client_code2']) && !empty(Model('biz_client_model')->get_where_one("client_code = '{$data1['client_code2']}'"))){
			$billing = $this->db->where("id_type = 'shipment' and id_no = {$id} and client_code = '{$data1['client_code2']}'")->get('biz_bill')->row_array();
			if(!empty($billing)){
				$data1['is_use_billing'] = true;
			}
		}
		if ($data1['from_db'] != '' && $data1['from_id_no'] && $data1['biz_type'] == 'import') {
			$data1['G_userBscEdit'] = array_diff($data1['G_userBscEdit'], array('shipper_company','shipper_address','shipper_contact','shipper_telephone','shipper_email','consignee_company','consignee_address','consignee_contact','consignee_telephone','consignee_email','notify_company','notify_address','notify_contact','notify_telephone','notify_email',));
		}
		$this->load->view('head');
		$this->load->view('biz/shipment/basic_sft_view', $data1);

	}

	public function shipment_must_field($shipment_id=0){
		$old_row = $this->biz_shipment_model->get_duty_one("id = $shipment_id");
    	$rows = [];
		foreach ($this->shipment_must_field as $val){
			$rows[$val] = 1;
			if(empty($old_row[$val])){
				$rows[$val] = 0;
			}
			if($val == 'trans_mode' && $old_row['trans_mode'] == 'FCL'){
				$box_info = json_decode($old_row['box_info'], true);
				if(empty($box_info)){
					$rows['box_info'] = 0;
				}
			}
		}
		$duty_field = array('sales', 'operator');
		Model('biz_duty_model');
		$duty = $this->biz_duty_model->get_new("id_type = 'biz_shipment' and user_id!=0 and id_no = $shipment_id");
		$duty_role = array_column($duty,"user_role");
		foreach($duty_field as $val){
			if(!in_array($val,$duty_role)){
				$rows[$val] = 0;
			}else{
				$rows[$val] = 1;
			}
		}
		$this->load->view('head');
		$this->load->view('biz/shipment/must_field',['data'=>$rows]);
	}
}
