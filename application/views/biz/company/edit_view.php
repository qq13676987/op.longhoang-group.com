<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>Company-Edit</title>
<style>
    .input{
        width: 350px;
    }
    .select{
        width: 357px;
    }
    .textarea{
        width: 350px;
        height: 60px;
    }
    input:read-only,select:read-only,textarea:read-only
    {
        background-color: #eaeaea;
        cursor: default;
    }
</style>
<script>
    var form_submit = false;
    $(function () {
        $('#f').form({
            url: "/biz_company/update_data",
            onSubmit: function(){
                var isValid = $(this).form('validate');
                if(form_submit){
                    return false;
                }
                form_submit = true;
                if (!isValid){
                    form_submit = false;
                }
                return isValid;
            },
            success:function(res_json){
                try{
                    var res = $.parseJSON(res_json);
                    if(res.code !== 444)alert(res.msg);
                    form_submit = false;
                    if(res.code == 0){
                        window.opener.$('#tt').datagrid('reload')
                        window.close();
                    }else if(res.code == 444){
                         // GET方式示例，其它方式修改相关参数即可
                        var form = $('<form></form>').attr('action','/other/ts_text').attr('method','POST').attr('target', '_blank');
                        var json_data = {};
                        json_data['text'] = res.text;
                        json_data['ts_str[]'] = res.ts_str;
                        json_data['msg'] = res.msg;
                        $.each(json_data, function (index, item) {
                            if(typeof item === 'string'){
                                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                            }else{
                                $.each(item, function(index2, item2){
                                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                                })
                            }
                        });
                
                        form.appendTo('body').submit().remove();
                    }
                }catch (e) {
                    form_submit = false;
                    $.messager.alert("<?= lang('提示');?>", res_json);
                }
            }
        });
    });
</script>
<body style="margin-left:10px;">
<table width="100%">
    <tr>
        <td width="80px;">
            <h2> Edit </h2>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td width="380px;">

        </td>
        <td width="30px;"> &nbsp;&nbsp;&nbsp;&nbsp; </td>
        <td  style="background-color:#0099cc;">

        </td>
    </tr>
</table>
<div  id="tb" class="easyui-tabs" style="width:100%;height:850px">
    <div title="Basic Registration" style="padding:1px">
        <div class="easyui-layout" style="width:100%;height:780px;">
            <div data-options="region:'center',tools:'#tt'" title="Basic" style="width:100%;">
                <form id="f" name="f" method="post">
                    <input type="hidden" name="id" value="<?= $id;?>">
                    <input type="hidden" name="client_code" value="<?= $client_code;?>">
                    <input type="hidden" name="company_type" value="<?= $company_type;?>">
                    <table>
                        <tr>
                            <td><?= lang('company_name');?></td>
                            <td>
                                <input class="easyui-textbox input" required name="company_name" value="<?= $company_name;?>">
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('company_address');?></td>
                            <td>
                                <textarea class="textarea" name="company_address" ><?= $company_address;?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('company_contact');?></td>
                            <td>
                                <textarea class="textarea" name="company_contact" ><?= $company_contact;?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('company_telephone');?></td>
                            <td>
                                <textarea class="textarea" name="company_telephone" ><?= $company_telephone;?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('company_email');?></td>
                            <td>
                                <textarea class="textarea" name="company_email" ><?= $company_email;?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('country');?> <?= $country_name;?></td>
                            <td>
                                <select class="easyui-combobox select" name="country_name" data-options="
                                        valueField:'cityname_en',
                                        textField:'namecode_en',
                                        value:'<?= $country_name;?>',
                                        url:'/city/get_country',
                                        onSelect: function(rec){
                                            if(rec == undefined)return;
                                            $('#region').combobox('reload', '/city/get_province?country=' + rec.cityname).combobox('clear');
                                            $('#country').val(rec.code);
                                        },
                                ">
                                </select>
                                <input type="hidden" name="country" id="country" value="<?= $country;?>">
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('省/州');?></td>
                            <td>
                                <select class="easyui-combobox select" id="region" name="region" data-options="
                                    valueField:'cityname_en',
                                    textField:'cityname_en',
                                    value: '<?= $region;?>',
                                    onSelect:function(rec){
                                        $('#region_code').val(rec.code);
                                    },
                                    <?php if(!empty($country)) echo 'url:\'/city/get_province/0/code?country=' . $country . '\',';?>
                                ">
                                </select>
                                <input type="hidden" id="region_code" name="region_code" value="<?= $region_code;?>">
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('city');?></td>
                            <td>
                                <input class="easyui-textbox input" name="city" value="<?= $city;?>">
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('postalcode');?></td>
                            <td>
                                <input class="easyui-textbox input" name="postalcode" value="<?= $postalcode;?>">
                            </td>
                        </tr>
                    </table>
                </form>
                <div id="tt">
                    <a href="javascript:void(0)" class="icon-save" onclick="$('#f').submit();" title="save"
                       style="margin-right:15px;"></a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>