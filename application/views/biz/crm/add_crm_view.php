<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title><?= lang('CRM-Add');?></title>
<link type="text/css" rel="stylesheet" href="/inc/third/layui/css/layui.css">
<script type="text/javascript" src="/inc/js/Convert_Pinyin.js"></script>
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<script type="text/javascript">
    // var is_search = false;
    var is_qcc_search = false;
    function checkvalue() {
        // if (!is_search) {
        //     layer.alert("先查找相似！", {icon:5});
        //     return false;
        // }
        // if (!is_search) {
        //     layer.alert("请企查查查找并选择！", {icon:5});
        //     return false;
        // }
        var vv = $('#country_code').textbox('getValue');
        if (vv == "") {
            layer.alert("country_code required！", {icon:5});
            return false;
        }
        var vv = $('#company_name').textbox('getValue');
        if (vv == "") {
            layer.alert("company_name required！", {icon:5});
            return false;
        }

        var vv = $('#client_name').textbox('getValue');
        // if (vv == "") {
        //     layer.alert("client_name required！", {icon:5});
        //     return false;
        // }

        var web_url = $.trim($('#web_url').textbox('getValue'));
        // if (web_url == '') {layer.alert('<?= lang('请填写公司网址！');?>',{icon:5}); return;}

        var vv = $('#approve_user').combobox('getValue');
        if (vv == "") {
            layer.alert("审批人 required！", {icon:5});
            return false;
        }

        $('#role').val($('#role1').combobox('getValues'));
        var vv = $('#role1').combobox('getValues');
        if (vv == '') {
            layer.alert("role required！", {icon:5});
            return false;
        }

        var vv = $('#company_address').val();
        // if (vv == "") {
        //     layer.alert("company_address required！", {icon:5});
        //     return false;
        // }
        var default_partner_tips = false;
        $.each($('.default_partner'), function (index, item) {
            var id = $(this).attr('ids');
            var val = $(this).combotree('getValues');
            $('#' + id).val(val);
            if ($.inArray(id, default_partner_request) !== -1 && val == '' && id != 'customer_service_ids') {
                default_partner_tips = id;
            }
        });
        if (default_partner_tips) {
            layer.alert(default_partner_tips + " required！", {icon:5});
            return false;
        }
        return true;
    }

    //获取综合描述
    function get_finance_describe() {
        return;
        var finance_payment = $('.finance_payment:checked').val();
        var finance_od_basis = $('.finance_od_basis:checked').val();

        var lang = '';
        if (finance_od_basis == 'monthly') {
            $('#finance_payment_month').textbox('enable');
            $('#finance_payment_day_th').textbox('enable');
            $('#finance_payment_days').textbox('disable');

            var finance_payment_month = $('#finance_payment_month').numberbox('getValue');
            var finance_payment_day_th = $('#finance_payment_day_th').numberbox('getValue');

            if (finance_payment == 'after work') {
                lang = '<?= lang('Payment on the (day)th day of (month) months after work');?>';
            } else if (finance_payment == 'after invoice') {
                lang = '<?= lang('Payment on the (day)th day of (month) months after invoice');?>';
            }

            lang = lang.replace('(month)', finance_payment_month).replace('(day)', finance_payment_day_th);

        } else if (finance_od_basis == 'daily') {
            $('#finance_payment_month').textbox('disable');
            $('#finance_payment_day_th').textbox('disable');
            $('#finance_payment_days').textbox('enable');

            var finance_payment_days = $('#finance_payment_days').numberbox('getValue');

            if (finance_payment == 'after work') {
                lang = '<?= lang('Payment (day) days after work');?>';
            } else if (finance_payment == 'after invoice') {
                lang = '<?= lang('Payment (day) days after invoice');?>';
            }
            lang = lang.replace('(day)', finance_payment_days);
        }
        // $('#finance_describe').textbox('setValue',lang);
    }

    var default_partner_request = [];

    //查找相似度
    function search_similar() {
        var company_name = $('#company_name').textbox('getValue');
        if (company_name === '') {
            $.messager.alert('<?= lang('提示');?>', '<?= lang('先填写全称');?>');
            return;
        }

        ajaxLoading();
        is_search = true;
        $.ajax({
            type: 'GET',
            url: '/biz_client/search?company_name=' + company_name,
            dataType:'json',
            success: function (res) {
                ajaxLoadEnd();
                if (res.code == 0) {
                    var status = res.data.client_level==0?'<?= lang('停用');?>':'<?= lang('正常');?>';
                    var msg = res.msg + '<br />' + '<?= lang('最接近的为“{company_name}”,相似度为{percent}%，状态：{status}', array('company_name' => "' + res.data.company_name + '", 'percent' => "' + res.data.percent + '", 'status' => "'+status + '"));?>';
                    layer.alert(msg);
                } else {
                    layer.alert(res.msg);
                }
            },error:function (e) {
                $.messager.alert('<?= lang('提示');?>', e.responseText);
            }
        });
    }

    function conuntry_event(newValue, oldValue){
        if (newValue != '') {
            if (newValue == 'CN') {
                // $('#company_name').textbox('readonly', true)
            } else {
                // $('#company_name').textbox('readonly', false)
            }
        }
    }

    $(function () {
        $('.finance_od_basis').click(function () {
            var val = $(this).val();
            $('.finance_od_basis').not(this).attr('checked', false);
            if (val == 'monthly') {
                $('#finance_payment_month').textbox('enable');
                $('#finance_payment_day_th').textbox('enable');
                $('#finance_payment_days').textbox('disable');
            } else if (val == 'daily') {
                $('#finance_payment_month').textbox('disable');
                $('#finance_payment_day_th').textbox('disable');
                $('#finance_payment_days').textbox('enable');
            } else if (val == 'daily2') {
                $('#finance_payment_month').textbox('disable');
                $('#finance_payment_day_th').textbox('disable');
                $('#finance_payment_days').textbox('enable').textbox('setValue', 0);
                val = 'daily';
            }
            $('#finance_od_basis').val(val);
            get_finance_describe();
        });

        $('.finance_payment').click(function () {
            get_finance_describe();
        });

        $('.finance_od_basis:checked').trigger('click');
        $('#company_name').textbox('textbox').change(function () {
            is_search = false;
        });

        //实现表格下拉框的分页和筛选--start
        // var html = '<div id="qcc_searchTb">';
        // html += '<form id="qcc_searchform">';
        // html += '<div style="padding-left: 5px;display: inline-block;"><label><?= lang('公司名称');?>:</label><input name="keyword" id="qcc_keyword" class="easyui-textbox to_search"style="width:200px;"data-options="prompt:\'text\'"/></div>';

        // html += '<div style="padding-left: 1px; display: inline-block;"><form>';
        // html += '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="qcc_query"><?= lang('search');?></a>';
        // html += '</div>';
        // html += '</div>';
        // $('body').append(html);
        // $.parser.parse('#qcc_searchTb');

        // $('#qcc_search').combogrid("reset");
        // $('#qcc_search').combogrid({
        //     panelWidth: '700px',
        //     panelHeight: '300px',
        //     prompt: '<?= lang('企查查更精确');?>',
        //     // multiple:true,
        //     idField: 'name',              //ID字段
        //     textField: 'name',    //显示的字段
        //     fitColumns: true,
        //     striped: true,
        //     editable: false,
        //     pagination: true,           //是否分页
        //     pageList: [20, 50, 100],
        //     pageSize: 20,
        //     toolbar: '#qcc_searchTb',
        //     rownumbers: true,           //序号
        //     collapsible: true,         //是否可折叠的
        //     method: 'get',
        //     columns: [[
        //         {field: 'Name', title: '<?= lang('Name');?>', width: 250},
        //         {field: 'CreditCode', title: '<?= lang('CreditCode');?>', width: 150},
        //         //{field: 'OperName', title: '<?= lang('OperName');?>', width: 150},
        //         //{field: 'No', title: '<?= lang('No');?>', width: 150},
        //     ]],
        //     emptyMsg: '未找到相应数据!',
        //     onBeforeLoad: function (param) {
        //         if (Object.keys(param).length < 3) {
        //             return false;
        //         }
        //     },
        //     onSelect: function (index, row) {
        //         var load = layer.load(1);
        //         is_qcc_search = true;
        //         $.ajax({
        //             type: 'post',
        //             url: '/biz_client/get_last_shipment/',
        //             data: {company_name:row.Name},
        //             dataType: 'json',
        //             async: true,
        //             success:function (data) {
        //                 layer.close(load);
        //                 if (data.code == 0){
        //                     var msg = row.Name + data.msg + '记录，当前处于保护状态，不能使用该抬头！';
        //                     layer.alert(msg, {icon:5});
        //                 }else{
        //                     var this_val = row.Name;
        //                     //选中时给下面的赋值
        //                     var input = $('#company_name');
        //                     input.textbox('setValue', this_val);
        //                     //自动查询选择的抬头
        //                     $('#search_client').attr('src', '/biz_client/client_sales/?company_name=' + this_val);
        //                     //选中时给下面的赋值
        //                     var input = $('#taxpayer_id');
        //                     // var this_val = row.No;
        //                     var this_val = row.CreditCode;
        //                     input.textbox('setValue', this_val);
        //                     //选中时给下面的赋值
        //                     var input = $('#taxpayer_name');
        //                     input.textbox('setValue', this_val);
        //                     //自动选择中国
        //                     var input = $('#country_code');
        //                     input.combobox('setValue', 'CN');
        //                     if($('#country_code').combobox('getValue') == 'CN'){
        //                         get_taxpayer()
        //                     }
        //                 }
        //             }
        //         });
        //     },
        //     onLoadSuccess: function (data) {
        //         var opts = $(this).combogrid('grid').datagrid('options');
        //         var vc = $(this).combogrid('grid').datagrid('getPanel').children('div.datagrid-view');
        //         vc.children('div.datagrid-empty').remove();
        //         if (!$(this).combogrid('grid').datagrid('getRows').length) {
        //             var d = $('<div class="datagrid-empty"></div>').html(opts.emptyMsg || 'no records').appendTo(vc);
        //             d.css({
        //                 position: 'absolute',
        //                 left: 0,
        //                 top: 50,
        //                 width: '100%',
        //                 fontSize: '14px',
        //                 textAlign: 'center'
        //             });
        //         }
        //     },
        //     onShowPanel: function () {
        //         //展开面板时
        //         var company_name = $('#company_name').textbox('getValue');
        //         var form_data = {
        //             'keyword': company_name,
        //         };
        //         $('#qcc_searchform').form('load', form_data);
        //         $('#qcc_query').trigger('click');
        //     },
        //     url: '/api_qcc/GetList_1027',
        // });
        // //点击搜索
        // $('#qcc_query').click(function () {
        //     var where = {};
        //     var form_data = $('#qcc_searchform').serializeArray();
        //     $.each(form_data, function (index, item) {
        //         if (where.hasOwnProperty(item.name) === true) {
        //             if (typeof where[item.name] == 'string') {
        //                 where[item.name] = where[item.name].split(',');
        //                 where[item.name].push(item.value);
        //             } else {
        //                 where[item.name].push(item.value);
        //             }
        //         } else {
        //             where[item.name] = item.value;
        //         }
        //     });
        //     $('#qcc_search').combogrid('grid').datagrid('load', where);
        // });
        // //text添加输入值改变
        // $('.to_search').textbox('textbox').keydown(function (e) {
        //     if (e.keyCode == 13) {
        //         $('#qcc_query').trigger('click');
        //     }
        // });

        //异步写入数据
        $('#save_data').click(function () {
            if(checkvalue() == true) {
                var load = layer.load(1);
                var field = {};
                $.each($('#myform').serializeArray(), function (index, item) {
                    field[item.name] = item.value;
                });

                $.ajax({
                    url: '/biz_crm/add_data',
                    type: 'POST',
                    data: field,
                    dataType: 'json',
                    success: function (data) {
                        layer.close(load);
                        if (data.code == 1) {
                            //刷新父页面的列表控件
                            try{
                                parent.$('#tt').edatagrid('reload');
                                layer.alert('<?= lang('保存成功。');?>', {icon:1,closeBtn:0}, function () {
                                    parent.layer.closeAll();
                                });
                            }catch(e){
                                window.close();
                            }
                        } else {
                            layer.alert('<?= lang('保存失败：');?>' + data.msg, {icon:5});
                        }
                    },
                    error: function (xhr, status, error) {
                        layer.close(load);
                        layer.alert('<?= lang('网络请求失败！');?>', {icon:5});
                    }
                });
            }
        });
    });
    function get_taxpayer(){
        var company_name = $('#company_name').textbox('getValue');
        $.ajax({
            type: 'POST',
            url: '/api/bw_search_company?company_name='+company_name,
            data: '',
            dataType: 'json',
            success: function (res) {
                ddd = res.response.result[0];
                $('#company_address').val(ddd.addressAndPhone);
            },
            error: function () {
                $.messager.alert('Tips', 'error');
            }
        });
    }
</script>
<style type="text/css">
    input:disabled, select:disabled, textarea:disabled {
        background-color: #d6d6d6;
        cursor: default;
    }
    .form_table {float: left;width: 400px;}
    .form_table tr td:first-child {padding-left: 20px; color: #023e62}
</style>
<body style="max-width:1215px;max-height:665px;">
<div style="height:600px; margin: 0px auto;" >
    <form id="myform" name="f" method="post" onsubmit="return false;">
        <div style="padding: 20px 20px 20px 30px; background: #f6f8f5; height: 530px; overflow: auto; width: 400px; float: left; border-right:1px solid #eee;">
            <table class="form_table" border="0" cellspacing="0">
                <input type="hidden" name="client_level" value="0">
                <input type="hidden" name="credit_amount" value="0">
                <input type="hidden" name="event" value="<?= $event;?>">
                <input type="hidden" name="clue_name" value="<?= $clue_name;?>">
                <tr style="height: 35px;">
                    <td style="width: 90px;"><?= lang('国家代码'); ?></td>
                    <td>
                        <select class="easyui-combobox" required name="country_code"
                                id="country_code" style="width: 255px; height: 30px;" data-options="
                                value:'<?= $country_code;?>',
                                valueField:'value',
                                textField:'namevalue',
                                url:'/bsc_dict/get_option/country_code',
                                onHidePanel: function() {
                                    var valueField = $(this).combobox('options').valueField;
                                    var val = $(this).combobox('getValue');
                                    var allData = $(this).combobox('getData');
                                    var result = true;
                                    for (var i = 0; i < allData.length; i++) {
                                        if (val == allData[i][valueField]) {
                                            result = false;
                                        }
                                    }
                                    if (result) {
                                        $(this).combobox('clear');
                                    }
                                },
                                onChange: function(newValue, oldValue){
                                    conuntry_event(newValue, oldValue);
                                }
                            ">
                        </select>
                    </td>
                </tr>
                <!--<tr id="qcc_tr" style="height: 50px; display: ;">-->
                <!--    <td><?= lang('企查查'); ?> </td>-->
                <!--    <td>-->
                <!--        <input id="qcc_search" class="easyui-combogrid" style="width:180px; height: 30px;">-->
                <!--        <button type="button" class="layui-btn layui-btn-sm layui-btn-primary layui-border-green" onclick="search_similar()">查找相似</button>-->
                <!--        <br><span style="color: #cd8d2e">Tips:中国的客户全称强制通过企查查来选择</span>-->
                <!--    </td>-->
                <!--</tr>-->
                <tr style="height: 35px;">
                    <td><?= lang('客户全称'); ?> </td>
                    <td>
                        <input class="easyui-textbox" required name="company_name" id="company_name"
                               style="width:255px;height: 30px;" value=""
                               data-options="
                                onChange: function(newValue, oldValue){
                                    var country_code = $('#country_code').combobox('getValue');
                                    if(newValue != oldValue && country_code != 'CN'){
                                        $('#company_name_en').textbox('setValue', $('#company_name').val());
                                    }
                                }
                           "/>
                        <script>
                            $(function(){
                                $('#company_name').textbox('setValue', '<?= es_encode($company_name);?>');
                            });
                        </script>
                    </td>
                </tr>
                <tr style="height: 35px;" id="company_name_en_tr">
                    <td><?= lang('英文全称'); ?> </td>
                    <td>
                        <input class="easyui-textbox" name="company_name_en" id="company_name_en"
                               style="width:255px;height: 30px;" value=""
                               data-options="
                                onChange: function(newValue, oldValue){
                                    $('#company_name_en').textbox('setValue', newValue.replace(/[^\w|\d|' '|'.']/ig,''));
                                }
                           "/>
                    </td>
                </tr>
                <tr style="height: 35px;">
                    <td><?= lang('客户简称'); ?> </td>
                    <td>
                        <input class="easyui-textbox" name="client_name" id="client_name"
                               data-options="" style="width:255px;height: 30px;"
                               value=""/>
                               <!--required:true-->
                    </td>
                </tr>
                <tr style="height: 35px;">
                    <td><?= lang('客户网址'); ?> </td>
                    <td>
                        <input class="easyui-textbox" name="web_url" id="web_url"
                               data-options="" style="width:255px;height: 30px;"
                               value=""/>
                    </td>
                </tr>
                <!--<tr style="height: 35px;">-->
                <!--    <td><?= lang('客户地区'); ?></td>-->
                <!--    <td>-->
                <!--        <select class="easyui-combobox" name="province" id="province" data-options="-->
                <!--            valueField:'cityname',-->
                <!--            textField:'cityname',-->
                <!--            url:'/city/get_province',-->
                <!--            onSelect: function(rec){-->
                <!--                if(rec != undefined){-->
                <!--                    $('#city').combobox('reload', '/city/get_city/' + rec.id);-->
                <!--                }-->
                <!--            }," style="width: 80px;height: 30px;"></select>-->

                <!--        <select class="easyui-combobox" name="city" id="city" style="width: 80px;height: 30px;"-->
                <!--                data-options="-->
                <!--                valueField:'cityname',-->
                <!--                textField:'cityname',-->
                <!--                onSelect: function(rec){-->
                <!--                    if(rec != undefined){-->
                <!--                        $('#area').combobox('reload', '/city/get_area/' + rec.id);-->
                <!--                    }-->
                <!--                },-->
                <!--                onLoadSuccess:function(){-->
                <!--                    var this_data = $(this).combobox('getData');-->
                <!--                    var this_val = $(this).combobox('getValue');-->
                <!--                    var rec = this_data.filter(el => el['cityname'] === this_val);-->
                <!--                    if(rec.length > 0)rec = rec[0];-->
                <!--                    $('#area').combobox('reload', '/city/get_area/' + rec.id);-->
                <!--                }-->
                <!--            "></select>-->

                <!--        <select class="easyui-combobox" name="area" id="area" style="width: 80px;height: 30px;"-->
                <!--                data-options="-->
                <!--                valueField:'cityname',-->
                <!--                textField:'cityname',-->
                <!--            "></select>-->
                <!--    </td>-->
                <!--</tr>-->
                <tr style="height: 35px;">
                    <td><?= lang('客户地址'); ?></td>
                    <td>
                            <textarea style="width: 255px;height: 40px" class="textarea b" name="company_address"
                                      id="company_address"><?= $company_address;?></textarea>
                    </td>
                </tr>
                <!--<tr style="height: 35px;">-->
                <!--    <td><?= lang('出口航线'); ?></td>-->
                <!--    <td>-->
                <!--        <select class="easyui-combobox" id="export_sailing1" style="width: 255px;height: 30px;"-->
                <!--                data-options="-->
                <!--                editable:false,-->
                <!--                multiple:true,-->
                <!--                multivalue:false,-->
                <!--                valueField:'value',-->
                <!--                required:true,-->
                <!--                textField:'name',-->
                <!--                url:'/bsc_dict/get_option/export_sailing',-->
                <!--                onChange:function(newValue,oldValue){-->
                <!--                    $('#export_sailing').val(newValue.join(','));-->
                <!--                }-->
                <!--            ">-->
                <!--        </select>-->
                <!--        <input type="hidden" name="export_sailing" id="export_sailing">-->
                <!--    </td>-->
                <!--</tr>-->
                <!--<tr style="height: 35px;">-->
                <!--    <td><?= lang('运输模式'); ?></td>-->
                <!--    <td>-->
                <!--        <select class="easyui-combobox" id="trans_mode1" style="width: 255px;height: 30px;"-->
                <!--                data-options="-->
                <!--                editable:false,-->
                <!--                multiple:true,-->
                <!--                multivalue:false,-->
                <!--                valueField:'value',-->
                <!--                required:true,-->
                <!--                textField:'name',-->
                <!--                url:'/bsc_dict/get_option/client_trans_mode',-->
                <!--                onChange:function(newValue,oldValue){-->
                <!--                    $('#trans_mode').val(newValue.join(','));-->
                <!--                }-->
                <!--            ">-->
                <!--        </select>-->
                <!--        <input type="hidden" name="trans_mode" id="trans_mode">-->
                <!--    </td>-->
                <!--</tr>-->
                <!--<tr style="height: 35px;">-->
                <!--    <td><?= lang('品名大类'); ?></td>-->
                <!--    <td>-->
                <!--        <select class="easyui-combobox" id="product_type1" style="width: 255px;height: 30px;"-->
                <!--                data-options="-->
                <!--                editable:false,-->
                <!--                multiple:true,-->
                <!--                multivalue:false,-->
                <!--                valueField:'value',-->
                <!--                textField:'name',-->
                <!--                url:'/bsc_dict/get_option/product_type',-->
                <!--                onChange:function(newValue,oldValue){-->
                <!--                    $('#product_type').val(newValue.join(','));-->
                <!--                }-->
                <!--            ">-->
                <!--        </select>-->
                <!--        <input type="hidden" name="product_type" id="product_type">-->
                <!--    </td>-->
                <!--</tr>-->
                <tr style="height: 35px;">
                    <td><?= lang('品名详情'); ?></td>
                    <td>
                                        <textarea name="product_details" id="product_details"
                                                  style="height:60px;width:248px;"></textarea>
                    </td>
                </tr>
                <!--<tr style="height: 35px;">-->
                <!--    <td><?= lang('客户来源'); ?></td>-->
                <!--    <td>-->
                <!--        <select class="easyui-combobox" id="client_source" name="client_source" style="width: 255px;height: 30px;"-->
                <!--                data-options="-->
                <!--                editable:false,-->
                <!--                multivalue:false,-->
                <!--                valueField:'value',-->
                <!--                textField:'name',-->
                <!--                url:'/bsc_dict/get_option/client_source'-->
                <!--            ">-->
                <!--        </select>-->
                <!--    </td>-->
                <!--</tr>-->
                <tr style="height: 35px;">
                    <td><?= lang('角色'); ?> </td>
                    <td>
                        <select id="role1" class="easyui-combobox" editable="false" name="role1"
                                style="width:255px;height: 30px;" data-options="required:true,
                                valueField:'value',
                                textField:'name',
                                url:'/bsc_dict/get_option/role?ext1=1',
                                value:'',
                                onSelect: function(rec){
                                    rec = rec.value;
                                    if(rec != 'factory' && rec !='client' && rec!='logistics_client' && rec != 'oversea_client'){
                                        alert('角色必须是客户类的');
                                        $(this).combobox('clear');
                                    }
                                },
                            ">
                        </select>
                        <input type="hidden" id="role" name="role" value="">
                        <a href="javascript:void;" class="easyui-tooltip" data-options="
                                content: $('<div></div>'),
                                onShow: function(){
                                    $(this).tooltip('arrow').css('left', 20);
                                    $(this).tooltip('tip').css('left', $(this).offset().left);
                                },
                                onUpdate: function(cc){
                                    cc.panel({
                                        width: 500,
                                        height: 'auto',
                                        border: false,
                                        href: '/bsc_help_content/help/role'
                                    });
                                }
                            "><?= lang('help'); ?></a>
                    </td>
                </tr>
                <tr style="height: 35px;">
                    <td><?= lang('审批人'); ?></td>
                    <td>
                        <select class="easyui-combobox" name="approve_user" readonly="true"
                                id="approve_user" style="width: 255px;height: 30px;" data-options="
                                                required:true,
                                                editable:false,
                                                valueField:'id',
                                                textField:'name',
                                                url:'/bsc_user/get_leader_user',
                                                value:'<?=$user['leader_id']; ?>'
                                            ">
                        </select>

                    </td>
                </tr>
                <tr style="height: 35px;">
                    <td><?= lang('销售'); ?></td>
                    <td>
                        <select class="easyui-combobox" name="sales_id" id="sales_id"
                                style="width: 255px;height: 30px;" data-options="
                                required:true,
                                editable:false,
                                valueField:'id',
                                textField:'otherName',
                                url:'/bsc_user/get_data_role/sales/?status=0',
                                value:'<?= $sales_id; ?>',
                            ">
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 10px 30px 10px 0px; width: 690px; float: right; overflow: hidden">
            <iframe src="/biz_client/client_sales" id="search_client" style="width:700px;height:600px; border:none;"></iframe>
            <div id="tt">
                <a href="javascript:void(0)" class="icon-save" onclick="checkvalue();" title="save"
                   style="margin-right:15px;"></a>
            </div>
        </div>
        <div id="save_tools"
             style="width: 100%; text-align: center; padding: 20px 0px; border-top:1px solid #588c98; position: fixed; z-index: 999999; bottom: 0px; background-color:#f6f8f5;">
            <button class="layui-btn" id="save_data" style=" width: 200px; "><?= lang('提交保存');?></button>
            <button class="layui-btn layui-btn-primary" onclick="$('#myform').form('reset');" style=" width: 100px;"><?= lang('重置');?>
            </button>
        </div>
    </form>
</div>
</body>