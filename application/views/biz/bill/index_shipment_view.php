<style type="text/css">
    .hide{
        display: none;
    }
    .bill_tips{
        margin-left: 10px;
    }
    .cost{
        color: red;
    }
    .cost a:link{
        color: red;
    }
    .sell{
        color: blue;
    }
    .sell a:link{
        color: blue;
    }
    tr.sell{
    }
    .cp_table1{
        width: 100%;
    }
    .cp_table2{
        width: 100%;
        text-align: left;
        border-collapse:collapse;
        border:none;
    }
    .cp_table2 th{
        width: 50%;
        border: 1px solid black;
    }
    .cp_table2 td{
        border: 1px solid black;
    }
    .hidden_invoice_input{
        display: none;
    }
    .lock{
        display: inline-block;
        width: 16px;
        height: 16px;
        opacity: 0.6;
        filter: alpha(opacity=60);
        margin: 0 0 0 2px;
        vertical-align: top;
    }
</style>
<script type="text/javascript">
    $.fn.combobox.defaults.keyHandler.enter=function (e) {
        keydown($(this), e);

        $(this).combobox('hidePanel');
        return false;
    };

    var singleSelect = true;


    var url = '/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>';
    var filter = {};
    var ajax_data = {};

    function click_row (is_click=true, rowIndex){
        if(is_click){
            $('#tt').datagrid('selectRow', rowIndex);
            var row = $('#tt').datagrid('getSelected');
            $('#dlg').removeClass('hide');
            $.each(row, function (index,item) {
                if(item == '***') row[index] = '';
            })
            if(row.sell_id == null && row.cost_id == null){
                return false;
            }
            var sell_is_edit = 'enable';
            if (row && row.sell_ban_handle) {
                sell_is_edit = 'disable';
            }

            $('#sell').textbox(sell_is_edit);
            $('#sell_unit_price').textbox(sell_is_edit);
            $('#sell_number').textbox(sell_is_edit);
            $('#sell_amount').textbox(sell_is_edit);
            $('#sell_vat').textbox(sell_is_edit);
            $('#sell_vat_withhold').textbox(sell_is_edit);
            $('#debitor').textbox(sell_is_edit);
            $('#sell_remark').textbox(sell_is_edit);

            var cost_is_edit = 'enable';
            if(row && row.cost_ban_handle){
                cost_is_edit = 'disable';
            }
            $('#cost').textbox(cost_is_edit);
            $('#cost_unit_price').textbox(cost_is_edit);
            $('#cost_number').textbox(cost_is_edit);
            $('#cost_amount').textbox(cost_is_edit);
            $('#cost_vat').textbox(cost_is_edit);
            $('#cost_vat_withhold').textbox(cost_is_edit);
            $('#creditor').textbox(cost_is_edit);
            $('#cost_remark').textbox(cost_is_edit);
			if(row.debitor) $('#sell_payment_bank').combobox('reload','/bsc_dict/get_client_account1/'+row.debitor)
			if(row.creditor) $('#cost_payment_bank').combobox('reload','/bsc_dict/get_client_account1/'+row.creditor)
			$('#sell_payment_bank').combobox('setValue',row.sell_bank_account_id)
			$('#cost_payment_bank').combobox('setValue',row.cost_bank_account_id)
            $('#fm').form('load', row);
            url = '/biz_bill/update_data';
            $('#save').linkbutton('settext', '<?= lang('修改');?>');
        }
    }

    $.fn.linkbutton.methods.settext = function(jq,text){
        var btn = $(jq);
        var left = btn.find('.l-btn-left');
        if(text == '<?= lang('修改');?>'){
            $('#add').css('display', 'inline-block');
        }
        if(text == '<?= lang('保存');?>'){
            $('#add').css('display', 'none');
        }
        var text_target = btn.find('.l-btn-text');
        text_target.text(text);
    }

    $(function () {
        //2022-08-25 窗口关闭
        $('#history').window('close');

        $('#tt').edatagrid({
            saveUrl: '/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>',
            updateUrl: '/biz_bill/update_data/',
        });
        $('#tt').datagrid({
            url: '/biz_bill/get_data/<?php echo $id_type;?>/<?php echo $id_no;?><?php if($is_special == 1) echo '?is_special=1'?>',
            width: 'auto',
            height: $(window).height() - 390,
            onClickRow: function (rowIndex) {
                click_row(true,rowIndex);
            },
            onLoadSuccess:function(res){
            	let row = {}
            	res.rows && res.rows.map(item=>{
            		if(item.sell_id > 0 && item.sell_is_local == 0 && item.sell_account_no != 'third party lock' && item.charge != '***' && item.sell_bank_account_id != 0) row = item
				})
				if(row.sell_id){
					open_third_party(row)
				}
                $('.currency_total').empty();
                $.each(res.profit, function(i, it){
                    if(!it.is_text){
                        var str = '<div class="currency_total_column">\n' +
                            '                        <div class="currency_total_column_row sell">\n' +
                            '                            <div class="currency_total_column_row_left">\n' +
                            '                                ' + it.sell_title + ': \n' +
                            '                            </div>\n' +
                            '                            <div class="currency_total_column_row_right">\n' +
                            '                                ' + it.sell_amount + '\n' +
                            '                            </div>\n' +
                            '                        </div>\n' +
                            '                        <div class="currency_total_column_row cost">\n' +
                            '                            <div class="currency_total_column_row_left">\n' +
                            '                                ' + it.cost_title + ': \n' +
                            '                            </div>\n' +
                            '                            <div class="currency_total_column_row_right">\n' +
                            '                                ' + it.cost_amount + '\n' +
                            '                            </div>\n' +
                            '                        </div>\n' +
                            '                        <div class="currency_total_column_row sell">\n' +
                            '                            <div class="currency_total_column_row_left">\n' +
                            '                                ' + it.profit_title + ': \n' +
                            '                            </div>\n' +
                            '                            <div class="currency_total_column_row_right">\n' +
                            '                                ' + it.profit_amount + '\n' +
                            '                            </div>\n' +
                            '                        </div>\n' +
                            '                    </div>';
                    }else{
                        var str = '<div class="currency_total_column">\n' +
                            '                        <div class="sell">\n' +
                            '                                ' + it.text + ' \n' +
                            '                        </div>\n' +
                            '                    </div>';
                    }
                    $('.currency_total').append(str);
                });
            }
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });

        $('#fm').on('keydown', '.textbox-text', function (event) {
            var input = $(this).parent('span').siblings();
            keydown(input, event);
        });
        $('#fm').on('blur', '.textbox-text', function (event) {
            var input = $(this).parent('span').siblings();
            if(input.hasClass('easyui-combobox')){
                var valueField = input.combobox('options').valueField;
                var val = input.combobox('getValue');
                var allData = input.combobox('getData');
                var result = true;
                if(Object.keys(filter).length != 0 && filter.hasOwnProperty(val) && filter[val].length >= 1){
                    input.combobox('select', filter[val][0]);
                }else{
                    for (var i = 0; i < allData.length; i++) {
                        if (val == allData[i][valueField]) {
                            result = false;
                        }
                    }
                    if (result) {
                        input.combobox('clear');
                    }
                }
                filter = {};
            }
        })

        var a1 = ajax_get('/biz_client/get_option/0/1', 'client');
        var a2 = ajax_get('/biz_client/get_option/wanglaikuan/1', 'wanglaikuan_client');
        $.when(a1,a2).done(function () {
            $('#creditor').combobox('loadData', ajax_data['client']);
            $('#debitor').combobox('loadData', ajax_data['client']);
        });
    });
    function ajax_get(url,name) {
        return $.ajax({
            type:'POST',
            url:url,
            dataType:'json',
            success:function (res) {
                ajax_data[name] = res;
            },
            error:function () {
                // $.messager.alert('获取数据错误');
            }
        });
    }

    function keydown(this_input, event) {
        if(event.keyCode == 9 || event.keyCode == 13){
            var input = this_input;

            if(input.hasClass('easyui-combobox')){
                var valueField = input.combobox('options').valueField;
                var val = input.combobox('getValue');
                var allData = input.combobox('getData');
                var result = true;
                if(Object.keys(filter).length != 0 && filter.hasOwnProperty(val) && filter[val].length >= 1){
                    input.combobox('select', filter[val][0]);
                }else{
                    for (var i = 0; i < allData.length; i++) {
                        if (val == allData[i][valueField]) {
                            result = false;
                        }
                    }
                    if (result) {
                        input.combobox('clear');
                    }
                }
                filter = {};
            }
            if(input.hasClass('vat')){
                save();
                return false;
            }
            if(event.keyCode == 13){
                let formValue = $('#fm').serializeArray();
                $.each(formValue, function(index, item) {
                    if(item.name == input.prop('id')){
                        var inp = $('#' + formValue[(index+1)].name);
                        if(inp.hasClass('easyui-combobox')){
                            inp.combobox('textbox').focus();
                        }else if(inp.hasClass('easyui-textbox')){
                            inp.textbox('textbox').focus();
                        }else if(inp.hasClass('easyui-numberbox')){
                            inp.numberbox('textbox').focus(function () {
                                this.select();
                            }).focus();
                        }
                    }
                });
            }
        }
    }
    function add() {
        // window.location.reload();
        //return 1; // 直接刷新来得清爽
        // $('#dlg').removeClass('hide');
        $('#charge').combobox('initValue');
        $('#charge_code').val('');
        $('#sell_unit_price').numberbox('initValue');
        $('#sell_number').numberbox('initValue');
        $('#sell_amount').numberbox('initValue');
        $('#cost_unit_price').numberbox('initValue');
        $('#cost_number').numberbox('initValue');
        $('#cost_amount').numberbox('initValue');
        $('#sell_remark').textbox('initValue');
        $('#cost_remark').textbox('initValue');


        cost_is_edit = 'enable';
        sell_is_edit = 'enable';
        $('#sell').textbox(sell_is_edit);
        $('#sell_unit_price').textbox(sell_is_edit);
        $('#sell_number').textbox(sell_is_edit);
        $('#sell_amount').textbox(sell_is_edit);
        $('#sell_vat').textbox(sell_is_edit);
        $('#sell_vat_withhold').textbox(sell_is_edit);
        $('#debitor').textbox(sell_is_edit);
        $('#sell_remark').textbox(sell_is_edit);

        $('#cost').textbox(cost_is_edit);
        $('#cost_unit_price').textbox(cost_is_edit);
        $('#cost_number').textbox(cost_is_edit);
        $('#cost_amount').textbox(cost_is_edit);
        $('#cost_vat').textbox(cost_is_edit);
        $('#cost_vat_withhold').textbox(cost_is_edit);
        $('#creditor').textbox(cost_is_edit);
        $('#cost_remark').textbox(cost_is_edit);
        url = '/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>';
        $('#save').linkbutton('settext', '<?= lang('新增');?>');
        $('#add').css('display', 'none');
    }

    var is_save = false;
    function save() {
        if(url == ''){
            url = '/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>';
        }
        <?php if($lock_lv > 0){?>
        return false;
        <?php } ?>
        if(is_save){
            return;
        }
        ajaxLoading();
        is_save = true;
        $('#fm').form('submit', {
            url: url,
            onSubmit: function () {
                var debitor = $('#debitor').combobox('getValue');
                var creditor = $('#creditor').combobox('getValue');
                var validate = true;
                if($('#charge').combobox('getValue') === '' || $('#charge_code').val() == ''){
                    $.messager.alert('Tips', '<?= lang('charge');?>');
                    validate = false;
                }
                if($('#creditor').combobox('getValue') === '' && $('#debitor').combobox('getValue') === ''){
                    $.messager.alert('Tips', '<?= lang('creditor');?> or <?= lang('debitor');?>');
                    validate = false;
                }
                if(creditor != '' && $('#cost').combobox('getValue') != ''){
                    if($('#creditor').combobox('getValue') === ''){
                        $.messager.alert('Tips', '<?= lang('creditor');?>');
                        validate = false;
                    }
                    if($('#cost').combobox('getValue') === ''){
                        $.messager.alert('Tips', '<?= lang('cost');?>');
                        validate = false;
                    }
                    if($('#cost_unit_price').numberbox('getValue') === '' || $('#cost_number').numberbox('getValue') === '' || $('#cost_amount').numberbox('getValue') === ''){
                        $.messager.alert('Tips', '<?= lang('cost_number');?>');
                        validate = false;
                    }
                    if($('#cost_vat').numberbox('getValue') === ''){
                        $.messager.alert('Tips', '<?= lang('cost_vat');?>');
                        validate = false;
                    }
                    if($('#cost_vat_withhold').numberbox('getValue') === ''){
                        $.messager.alert('Tips', '<?= lang('cost_vat_withhold');?>');
                        validate = false;
                    }
                    if($('#cost_payment_bank').combobox('getValue') === ''){
                        $.messager.alert('Tips', '<?= lang('cost_account');?>');
                        validate = false;
                    }

				}
                if(debitor != '' && $('#sell').combobox('getValue') != ''){
                    if($('#debitor').combobox('getValue') === ''){
                        $.messager.alert('Tips', '<?= lang('debitor');?>');
                        validate = false;
                    }
                    if($('#sell').combobox('getValue') === ''){
                        $.messager.alert('Tips', '<?= lang('sell');?>');
                        validate = false;
                    }
                    if($('#sell_unit_price').numberbox('getValue') === '' || $('#sell_number').numberbox('getValue') === '' || $('#sell_amount').numberbox('getValue') === ''){
                        $.messager.alert('Tips', '<?= lang('amount');?>');
                        validate = false;
                    }
                    if($('#sell_vat').numberbox('getValue') === ''){
                        $.messager.alert('Tips', '<?= lang('sell_vat');?>');
                        validate = false;
                    }
                    if($('#sell_vat_withhold').numberbox('getValue') === ''){
                        $.messager.alert('Tips', '<?= lang('sell_vat_withhold');?>');
                        validate = false;
                    }
					if($('#sell_payment_bank').combobox('getValue') === ''){
						$.messager.alert('Tips', '<?= lang('sell_account');?>');
						validate = false;
					}
                }

                if(!validate){
                    ajaxLoadEnd();
                    is_save = false;
                }
                return validate;
                // return $(this).form('validate');
            },
            success: function (result) {
                // $('#dlg').addClass('hide');
                var res = $.parseJSON(result);
                if(res.code == 0){
                    url = '/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>';
                    add();
                    $('#charge').combobox('textbox').focus();
                    $.messager.alert('Tips', res.msg);
                    $('#tt').datagrid('reload');
                    // $.messager.alert('Tips', res.msg);
                }else{
                    $.messager.alert('Tips', res.msg);
                }
                is_save = false;
                ajaxLoadEnd();
            }
        });

    }

    function del(type) {
        var rows = $('#tt').datagrid('getSelections');
        var id = 0;
        if(rows.length > 0){
            if(rows.length <= 1 && type != 'all'){
                var row = rows[0];
                if(type == 'cost'){
                    id = row.cost_id;
                }else if(type == 'sell'){
                    id = row.sell_id;
                }else{
                    return false;
                }
                if(id == null){
                    $.messager.alert('<?= lang('提示')?>','<?= lang('No billings');?>');
                    return false;
                }
                $.messager.confirm('<?= lang('提示');?>', '<?= lang('confirm to delete？');?>', function(r){
                    if (r){
                        // 退出操作;
                        $.ajax({
                            type: 'POST',
                            url: '/biz_bill/delete_data<?php if($is_special == 1) echo '?is_special=1'?>',
                            data:{
                                id:id,
                            },
                            success: function (res_json) {
                                try{
                                    var res = $.parseJSON(res_json);
                                    if(res.code == 0){
                                        $.messager.alert('info：', res.msg);
                                        $('#tt').datagrid('reload');
                                        url = '/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>';
                                        $('#save').linkbutton('settext', '<?= lang('保存');?>');
                                    }else{
                                        $.messager.alert('info：', res.msg);
                                    }
                                }catch(e){
                                    $.messager.alert('info：', res_json);
                                }

                            }
                        });
                    }
                });
            }else{
                var ids = [];
                var result = 0;
                var msg = '<?= lang('发生错误');?>';
                $.each(rows, function (index, item) {
                    if(type == 'cost' || type == 'all'){
                        if(item.cost_invoice_id != 0){
                            result = 1;
                            msg = '<?= lang("费用名称为{charge}的成本已开票", array('charge' => "' + item.charge + '"));?>';
                            return false;
                        }
                        ids.push(item.cost_id);
                    }
                    if(type == 'sell' || type == 'all'){
                        if(item.sell_invoice_id != 0){
                            result = 1;
                            msg = '<?= lang("费用名称为{charge}的卖价已开票", array('charge' => "' + item.charge + '"));?>';
                            return false;
                        }
                        ids.push(item.sell_id);
                    }
                });
                if(result == 1){
                    $.messager.alert('<?= lang('提示');?>', msg);
                    return ;
                }
                ids = ids.join(',');
                $.messager.prompt('<?= lang('提示');?>', '<?= lang('请输入delete确认删除？');?>', function(r){
                    if (r === 'delete'){
                        // 退出操作;
                        $.ajax({
                            type: 'POST',
                            url: '/biz_bill/delete_datas',
                            data:{
                                ids:ids,
                            },
                            success: function (res_json) {
                                try{
                                    var res = $.parseJSON(res_json);
                                    if(res.code == 0){
                                        $.messager.alert('info：', res.msg);
                                        $('#tt').datagrid('reload').datagrid('clearSelections');
                                        url = '/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>';
                                        $('#save').linkbutton('settext', '<?= lang('保存');?>');
                                    }else{
                                        $.messager.alert('info：', res.msg);
                                    }
                                }catch(e){
                                    $.messager.alert('info：', res_json);
                                }
                            }
                        });
                    }
                });
            }

        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }
    }

    function bill_apply() {
        $('#bill_apply').window({
            title: 'window',
            width: 900,
            height: 450,
            cache: false,
            modal: true,
            onResize:function (width, height) {
                $('#bill_apply_iframe').width(width - 30);
                $('#bill_apply_iframe').height(height - 40);
            }
        });
        $('#bill_apply_iframe').attr('src', '/biz_bill_apply/index/<?= $id_type;?>/<?= $id_no;?><?php if($is_special == 1) echo '?is_special=1'?>');
    }
    //费用确认
    function batch_confirm_date(type,index = null){
        if(index !== null){
            var datas = $('#tt').datagrid('getData');
            var data = [datas['rows'][index]];
        }else{
            var data = $('#tt').datagrid('getSelections');
        }
        if(data.length > 0){
            var ids = [];
            //检测付款方是否为同一个,如果不是,检测币种,税率是否相同
            var client_codes = [];
            var currencys = [];
            var vats = [];
            //已开票,已核销 已对账不能弹出窗口
            var not_window = '';
            $.each(data, function(index, item){
                if(type == 'cost'){
                    if(item.cost_invoice_id != 0){
                        not_window = '<?= lang('已开票');?>';
                        return false;
                    }
                    if(item.cost_confirm_date !== '0000-00-00'){
                        not_window = '<?= lang('已确认');?>';
                        return false;
                    }
                    ids.push(item.cost_id);
                    if($.inArray(item.creditor, client_codes) === -1)client_codes.push(item.creditor);
                    if($.inArray(item.cost, currencys) === -1)currencys.push(item.cost);
                    if($.inArray(item.cost_vat, vats) === -1)vats.push(item.cost_vat);
                }else if(type == 'sell'){
                    if(item.sell_invoice_id != 0){
                        not_window = '<?= lang('已开票,无法确认');?>';
                        return false;
                    }
                    if(item.sell_confirm_date !== '0000-00-00'){
                        not_window = '<?= lang('已确认,无法确认');?>';
                        return false;
                    }
                    ids.push(item.sell_id);
                    if($.inArray(item.debitor, client_codes) === -1)client_codes.push(item.debitor);
                    if($.inArray(item.sell, currencys) === -1)currencys.push(item.sell);
                    if($.inArray(item.sell_vat, vats) === -1)vats.push(item.sell_vat);
                }
            });
            if(not_window !== ''){
                $.messager.alert('<?= lang('提示');?>', not_window);
                return;
            }
            //客户是否相同
            if(client_codes.length > 1){
                //判断币种税率是否相同
                if(currencys.length > 1 || vats.length > 1){
                    $.messager.alert('<?= lang('提示')?>','<?= lang('币种或税率不同,无法确认')?>');
                    return;
                }
            }

            ids = ids.join(',');
            $('#batch_confirm_date input[name="ids"]').val(ids);
            // 获取当前日期时间
            var myDate = new Date();
            $('#confirm_date').datebox('clear').datebox('setValue', myDate.getFullYear() + '-' + (myDate.getMonth() + 1) + '-' + myDate.getDate());
            // $('#account_no').textbox('clear');
            $('#batch_confirm_date').dialog('open').dialog('setTitle', type);
        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('No columns selected')?>');
        }
    }

    function save_batch_confirm() {
        var ids = $('#batch_confirm_date input[name="ids"]').val();
        var confirm_date = $('#confirm_date').datebox('getValue');
        // var account_no = $('#account_no').textbox('getValue');
        var isValid = $('#batch_confirm_date').form('validate');
        if(isValid){
            $.ajax({
                type: 'POST',
                url: '/biz_bill/batch_confirm_date',
                data: {
                    ids:ids,
                    confirm_date:confirm_date,
                    // account_no:account_no,
                    is_special : '<?= $is_special;?>',
                },
                success:function (res_json) {
                    try {
                        var res = $.parseJSON(res_json);
                        $.messager.alert('<?= lang('提示');?>', res.msg);
                        if(res.code == 0){
                            $('#tt').datagrid('reload');
                        }
                    }catch (e) {
                        $.messager.alert('<?= lang('提示');?>', res_json);
                    }
                },
            })
            $('#batch_confirm_date').dialog('close');
        }

    }

    //批量确认自己的--start
    function batch_confirm_self(type) {
        // 获取当前日期时间
        var myDate = new Date();
        $('#confirm_date').datebox('clear').datebox('setValue', myDate.getFullYear() + '-' + (myDate.getMonth() + 1) + '-' + myDate.getDate());
        // $('#account_no').textbox('clear');
        $('#batch_confirm_date').dialog('open').dialog('setTitle', type);
        var bcd_a = $('#bcd a');
        bcd_a.attr('onclick2', bcd_a.attr('onclick'));
        bcd_a.attr('onclick', "save_batch_confirm_self(\'" + type +"')");
    }

    function save_batch_confirm_self(type) {
        var confirm_date = $('#confirm_date').datebox('getValue');
        // var account_no = $('#account_no').textbox('getValue');
        var isValid = $('#batch_confirm_date').form('validate');
        if(isValid){
            $.ajax({
                type: 'POST',
                url: '/biz_bill/batch_confirm_self_bill',
                data: {
                    type : type,
                    id_type : '<?= $id_type;?>',
                    id_no : '<?= $id_no;?>',
                    is_special : '<?= $is_special;?>',
                    confirm_date : confirm_date,
                },
                success:function (res_json) {
                    try {
                        var res = $.parseJSON(res_json);
                        $.messager.alert('<?= lang('提示');?>', res.msg);
                        if(res.code == 0){
                            $('#tt').datagrid('reload');
                        }
                    }catch (e) {
                        $.messager.alert('<?= lang('提示');?>', res_json);
                    }
                },
            });
            var bcd_a = $('#bcd a');
            bcd_a.attr('onclick', bcd_a.attr('onclick2'));
            bcd_a.attr('onclick2', '');
            $('#batch_confirm_date').dialog('close');
        }
    }
    //批量确认自己的--end

    //费用取消确认
    function batch_date_cancel(type,index) {
        if(index !== null){
            var datas = $('#tt').datagrid('getData');
            var data = [datas['rows'][index]];
        }else{
            var data = $('#tt').datagrid('getSelections');
        }
        if(data.length > 0){
            var row = data[0];
            var ids = [];
            $.each(data, function(index, item){
                if(type == 'cost'){
                    ids.push(item.cost_id);
                }else if(type == 'sell'){
                    ids.push(item.sell_id);
                }
            });
            var confirm_date = '';
            // var account_no = '';
            ids = ids.join(',');
            $.messager.confirm('<?= lang('提示');?>', '<?= lang('确认取消');?>?', function(r) {
                if (r) {
                    $.ajax({
                        type: 'POST',
                        url: '/biz_bill/batch_confirm_date',
                        data: {
                            ids:ids,
                            confirm_date:confirm_date,
                            // account_no:account_no,
                        },
                        success:function (res_json) {
                            try {
                                var res = $.parseJSON(res_json);
                                $.messager.alert('<?= lang('提示');?>', res.msg);
                                if(res.code == 0){
                                    $('#tt').datagrid('reload');
                                }
                            }catch (e) {
                                $.messager.alert('<?= lang('提示');?>', res_json);
                            }
                        },
                    });
                }
            });
        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('未选中行')?>');
        }
    }

    function set_item_value(item){
        if(item){
            $("#buyer_company1").val(item.buyer_company)
            $("#currency1").val(item.currency)
            $("#vat1").val(item.vat)
            // $("#invoice_type1").val(item.invoice_type)
            $("#invoice_amount1").val(item.invoice_amount)
            $("#invoice_company1").val(item.invoice_company)
            $("#remark1").text(item.remark)
            $('#invoice_type1').combobox({
                valueField:'value',
                textField:'name',
                value: item.invoice_type,
                url:'/bsc_dict/get_option/invoice_type',
            });
        }
    }

    //开票申请--start
    function invoice_apply(type){
        var data = $('#tt').datagrid('getSelections');
        if(data.length > 0){
            var ids = [],client_codes = [],currencys = [],vats = [],sum_amount = 0,not_window = '',item;
            //客户,税率,币种必须一致
            $.each(data, function(index, item2){
                item = item2;
                if(type == 'cost'){
                    if(item.cost_invoice_id != 0){
                        not_window = '<?= lang('已开票,无法申请');?>';
                        return false;
                    }
                    if(item.cost_confirm_date === '0000-00-00'){
                        not_window = '<?= lang('未确认,无法申请');?>';
                        return false;
                    }
                    var amount = - parseFloat(item.cost_local_amount);
                    sum_amount += amount != null ? amount : 0;

                    ids.push(item.cost_id);
                    if($.inArray(item.creditor, client_codes) === -1)client_codes.push(item.creditor);
                    if($.inArray(item.cost, currencys) === -1)currencys.push(item.cost);
                    if($.inArray(item.cost_vat, vats) === -1)vats.push(item.cost_vat);
                }else if(type == 'sell'){
                    if(item.sell_id == null){
                        not_window = '<?= lang('账单不存在,无法申请');?>';
                        return false;
                    }
                    if(item.sell_invoice_id != 0){
                        not_window = '<?= lang('已开票,无法申请');?>';
                        set_item_value(item.sell_invoice_data)
                        // return false;
                    }
                    if(item.sell_confirm_date === '0000-00-00'){
                        not_window = '<?= lang('未确认,无法申请');?>';
                        return false;
                    }
                    var amount = item.sell_amount * 100;
                    sum_amount += amount != null ? amount : 0;

                    ids.push(item.sell_id);
                    if($.inArray(item.debitor, client_codes) === -1)client_codes.push(item.debitor);
                    if($.inArray(item.sell, currencys) === -1)currencys.push(item.sell);
                    if($.inArray(item.sell_vat, vats) === -1)vats.push(item.sell_vat);
                }
            });
            if(currencys.length > 1){
                not_window = '<?= lang('币种必须一致,无法申请');?>';
            }
            if(not_window == '<?= lang('已开票,无法申请');?>'){
                $('#buyer_company').combobox('setValue', client_codes[0]).combobox('reload');
                get_biz_client_contract_data(client_codes[0],1)
                $('#invoice_apply1').dialog('open').dialog('setTitle', '<?= lang('已开票');?>');
                return;
            }
            if(not_window !== ''){
                $.messager.alert('Tips', not_window);
                return;
            }
            sum_amount = sum_amount / 100;
            get_remark(currencys[0], sum_amount, 'remark','invoice_apply_from');
            //客户是否相同
            if(client_codes.length > 1){
                $.messager.alert('<?= lang('提示')?>', '<?= lang('只能有一个抬头');?>');
                return;
            }

            //判断币种税率是否相同
            if(vats.length > 1){
                $.messager.alert('<?= lang('提示')?>','<?= lang('税率不同')?>');
                return;
            }
            ids = ids.join(',');

            $('#buyer_company').combobox('setValue', client_codes[0]).combobox('reload');
            var from_data = {'ids' : ids,'buyer_company' : client_codes[0],'currency' : 'CNY', 'vat' : vats[0], 'invoice_amount' : sum_amount * data[0]['sell_exrate']};
            get_biz_client_contract_data(client_codes[0],2)
            $('#invoice_apply_from').form('load', from_data);
            $('#invoice_apply').dialog('open').dialog('setTitle', type);
        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('未选中行')?>');
        }
    }
    function get_biz_client_contract_data(client_code='',type=''){
        if(!client_code) return;
        let ele = "#table"+type;
        $.getJSON("/biz_client_contract/get_data/"+client_code, function(json){
            if(json.rows.length > 0){
                let str = ''
                for (let i = 0; i < json.rows.length; i++) {
                    str += `<tr>
                                <td>${json.rows[i].party_b}<?= lang('合同有效期');?></td>
                                <td><div style="padding-right:10px;"><input type="text" style="width:80px;" disabled value="${json.rows[i].contract_start}"> - <input type="text" style="width:80px;" disabled value="${json.rows[i].contract_expiry}" ></div></td>
                            </tr>`
                }
                $(ele).prepend(str)
            }
        });
    }
    var form_submit = false;
    function upload_pay_confirm(){
        let id_no = "upload_pay_confirm"+"<?=$id_no?>"
        $('#upload_pay_confirm_iframe').attr('src','/bsc_upload/upload_files?biz_table=biz_bill_invoice&id_no='+id_no+'&upload_pay_confirm=upload_pay_confirm')
        $('#upload_pay_confirm_dialog').dialog('open').dialog('setTitle', '<?= lang('上传费用确认单');?>');
    }
    function upload_pay_confirm1(){
        var data = $('#tt').datagrid('getSelections');
        let id_no = data[0].sell_invoice_data.id;
        $('#upload_pay_confirm_iframe').attr('src','/bsc_upload/upload_files?biz_table=biz_bill_invoice&id_no='+id_no+'&upload_pay_confirm_file=upload_pay_confirm_file')
        $('#upload_pay_confirm_dialog').dialog('open').dialog('setTitle', '<?= lang('上传费用确认单');?>');
    }
    function save_invoice_apply() {
        if(form_submit){
            return;
        }
        var form_data = $('#invoice_apply_from').serializeArray();
        var isValid = $('#invoice_apply_from').form('validate');
        var json = {};
        $.each(form_data, function (index, item) {
            json[item.name] = item.value;
        });
        let id_no = "upload_pay_confirm"+"<?=$id_no?>"
        if(isValid){
            ajaxLoading();
            form_submit = true;
            $.ajax({
                type: 'POST',
                url: '/biz_bill/batch_invoice?id_no='+id_no,
                data: json,
                success:function (res_json) {
                    form_submit = false;
                    ajaxLoadEnd();
                    try{
                        var res = $.parseJSON(res_json);
                        $.messager.alert('Tips', res.msg);
                        if(res.code == 0){
                            $('#tt').datagrid('reload');
                            $('#invoice_apply').dialog('close')
                        }
                    }catch(e){
                        $.messager.alert('<?= lang('提示');?>', res_json);
                    }
                },
            })
            $('#batch_confirm_date').dialog('close');
        }

    }


    function dn_invoice_apply(type){
        var data = $('#tt').datagrid('getSelections');
        if(data.length > 0){
            var ids = [];
            //客户,税率,币种必须一致
            var client_codes = [];
            var currencys = [];
            var vats = [];
            var sum_amount = 0;

            var not_window = '';
            var item;
            $.each(data, function(index, item2){
                item = item2;
                if(type == 'cost'){
                    if(item.cost_invoice_id != 0){
                        not_window = '<?= lang('已开票,无法申请');?>';
                        return false;
                    }
                    if(item.cost_confirm_date === '0000-00-00'){
                        not_window = '<?= lang('未确认,无法申请');?>';
                        return false;
                    }
                    var amount = - parseFloat(item.cost_local_amount);
                    sum_amount += amount != null ? amount : 0;

                    ids.push(item.cost_id);
                    if($.inArray(item.creditor, client_codes) === -1)client_codes.push(item.creditor);
                    if($.inArray(item.cost, currencys) === -1)currencys.push(item.cost);
                    if($.inArray(item.cost_vat, vats) === -1)vats.push(item.cost_vat);
                }else if(type == 'sell'){
                    if(item.sell_id == null){
                        not_window = '<?= lang('账单不存在,无法申请');?>';
                        return false;
                    }
                    if(item.sell_invoice_id != 0){
                        not_window = '<?= lang('已开票,无法申请');?>';
                        return false;
                    }
                    if(item.sell_confirm_date === '0000-00-00'){
                        not_window = '<?= lang('未确认,无法申请');?>';
                        return false;
                    }
                    var amount = item.sell_amount * 100;
                    sum_amount += amount != null ? amount : 0;

                    ids.push(item.sell_id);
                    if($.inArray(item.debitor, client_codes) === -1)client_codes.push(item.debitor);
                    if($.inArray(item.sell, currencys) === -1)currencys.push(item.sell);
                    if($.inArray(item.sell_vat, vats) === -1)vats.push(item.sell_vat);
                }
            });
            if(currencys.length > 1){
                not_window = '<?= lang('币种必须一致,无法申请');?>';
            }
            if(not_window !== ''){
                $.messager.alert('<?= lang('提示');?>', not_window);
                return;
            }
            sum_amount = sum_amount / 100;
            get_remark(currencys[0], sum_amount, 'dn_ramark', 'dn_invoice_apply_from');
            //客户是否相同
            if(client_codes.length > 1){
                $.messager.alert('<?= lang('提示')?>', '<?= lang('只能有一个抬头');?>');
                return;
            }

            //判断币种税率是否相同
            if(vats.length > 1){
                $.messager.alert('<?= lang('提示')?>','<?= lang('税率不同')?>');
                return;
            }
            ids = ids.join(',');

            var client_code_name;
            if(type == 'sell'){
                client_code_name = item.debitor_name;
            }else{
                client_code_name = item.creditor_name;
            }
            $('#de_account_id').combobox('reload',  '/bsc_dict/by_s_get_client_account/' + client_codes[0]);
            var from_data = {'ids' : ids,'buyer_company' : client_code_name,'currency' : 'CNY', 'vat' : vats[0], 'invoice_amount' : sum_amount * data[0]['sell_exrate']};
            $('#dn_invoice_apply_from').form('load', from_data);
            $('#dn_invoice_apply_div').dialog('open').dialog('setTitle', type);
        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('未选中行')?>');
        }
    }

    function save_dn_invoice_apply() {
        if(form_submit){
            return;
        }
        var form_data = $('#dn_invoice_apply_from').serializeArray();
        var isValid = $('#dn_invoice_apply_from').form('validate');
        var json = {};
        $.each(form_data, function (index, item) {
            json[item.name] = item.value;
        });
        if(isValid){
            ajaxLoading();
            form_submit = true;
            $.ajax({
                type: 'POST',
                url: '/biz_bill/batch_invoice',
                data: json,
                success:function (res_json) {
                    form_submit = false;
                    ajaxLoadEnd();
                    try{
                        var res = $.parseJSON(res_json);
                        $.messager.alert('Tips', res.msg);
                        if(res.code == 0){
                            $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否下载该DN');?>：', function(r){
                                if (r){
                                    if(res.pdf_path != '' && res.pdf_path != undefined)window.open(res.pdf_path);
                                    $('#tt').datagrid('reload');
                                    $('#dn_invoice_apply_div').dialog('close')
                                }
                            });
                        }
                    }catch(e){
                        $.messager.alert('<?= lang('提示');?>', res_json);
                    }
                },
            })
        }

    }

    function add_history(){
        iframe_window({title:'<?= lang('历史费用');?>',  height : $(window).height()-200} , "/biz_bill/history_bill?id_no=<?=$id_no?>&client_code=<?=$debitor_default?>&client_code2=<?=$shipment['client_code2']?>");
    }

    function reload_client(type = 0){
        // if(type == 0){
        // alert("逐步关闭该功能");return;
        //2022-04-07 应付改为不显示纯供应商相关的抬头
        $('#creditor').combobox('reload', '/biz_client/get_option/0/1');
        // $('#debitor').combobox('reload', '/biz_client/get_option/0/1?bill_type=sell');
        $('#debitor').combobox('reload', '/biz_client/get_option/0/1');
        // }else{
        //2022-04-07 应付改为不显示纯供应商相关的抬头
        // $('#creditor').combobox('reload', '/biz_client/get_bill_client/<?= $id_type;?>/<?= $id_no?>');
        // $('#debitor').combobox('reload', '/biz_client/get_bill_client/<?= $id_type;?>/<?= $id_no?>?type=sell');
        // $('#debitor').combobox('reload', '/biz_client/get_bill_client/<?= $id_type;?>/<?= $id_no?>');
        // }
    }

    function reload_edit_client_code(combobox_id = ''){
        var type = $('#update_div_client_code').combobox('options').bill_type;
        if(combobox_id == 'debitor' && type == 'sell'){
            $('#update_div_client_code').combobox('loadData', $('#debitor').combobox('getData'));
        }else if(combobox_id == 'creditor' && type == 'cost'){
            $('#update_div_client_code').combobox('loadData', $('#creditor').combobox('getData'));
        }
    }

    function invoice_type_select(rec) {
        if(rec.value == '999'){//形式发票
            $('.hidden_invoice_input.invoice_type999').css('display', 'table-row');
            $('#payment_bank').combobox({'required' : true});
            $('.hidden_invoice_input.invoice_type1001').css('display', 'none');
            $('#use_type').combobox({'required' : false});
        }else if(rec.value == '1001'){//形式发票
            $('.hidden_invoice_input.invoice_type1001').css('display', 'table-row');
            $('#use_type').combobox({'required' : true});

            $('.hidden_invoice_input.invoice_type999').css('display', 'none');
            $('#payment_bank').combobox({'required' : false});
        }else{
            $('.hidden_invoice_input.invoice_type999').css('display', 'none');
            $('#payment_bank').combobox({'required' : false});

            $('.hidden_invoice_input.invoice_type1001').css('display', 'none');
            $('#use_type').combobox({'required' : false});
        }
    }

    function lock(lock){
        if(lock >= 0){
            $.ajax({
                url: '/biz_shipment/lock',
                type: 'POST',
                data:{
                    id:'<?= $id_no;?>',
                    level:lock,
                },
                success: function (res_json) {
                    var res;
                    try{
                        res = $.parseJSON(res_json);
                        if(res.code == 0){
                            parent.location.reload();
                        }else{
                            $.messager.alert('<?= lang('提示');?>', res.msg);
                        }
                    }catch(e){
                        $.messager.alert('<?= lang('提示');?>', res_json);
                    }
                }
            });
        }
    }

    function reload_bill_local_save(type = 'sell') {
        var data = $('#tt').datagrid('getData').rows;
        if(data.length == 0){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('至少存在一条数据');?>');
            return;
        }
        var row = data[0];
        var start_time = row['sell_bill_ETD'];
        if(start_time == null) start_time = row['cost_bill_ETD'];
        $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认刷新指定账单的汇率');?>', function(r){
            if(r){
                ajax_reload_bill_local(start_time);
            }
        });
    }

    function ajax_reload_bill_local(start_time){
        ajaxLoading();
        $.ajax({
            type:'POST',
            url: '/biz_bill/reload_bill_local',
            data:{
                start_time: start_time,
                end_time: start_time,
            },
            success:function(res){
                ajaxLoadEnd();
                try{
                    var res = $.parseJSON(res_json);
                    if(res.code == 0){
                        $.messager.alert('info', res.msg);
                    }
                }catch(e){
                    $.messager.alert('<?= lang('提示');?>', res_json);
                }
            },error:function(e){

            }
        });
    }

    //字段--start
    function charge_for(value, row, index) {
        var charge_name_en = row.charge_name_en;
        if(charge_name_en == null){
            return "";
        }
        var str = charge_name_en + " / " + value;
        return str;
    }


    function cost_all_style(value, row, index) {
        var style = '';
        if(row.cost_ban_handle){
            style = 'background-color:#C0C0C0;';
        }
        return style;
    }

    function sell_all_style(value, row, index) {
        var style = '';
        if(row.sell_ban_handle){
            style = 'background-color:#C0C0C0;';
        }
        return style;
    }
    function cost_for(value,row,index){
        var str = '';
        var cost = row.cost;
        if(cost == null){
            cost = '';
        }
        str += '<span class="cost">' + cost + '</span>';
        return str;
    }
    function sell_for(value,row,index){
        var str = '';
        var sell = row.sell;
        if(sell == null){
            sell = '';
        }
        str += '<span class="sell">' + sell + '</span>';
        return str;
    }
    function cost_unit_price_for(value,row,index){
        var str = '';
        var cost_unit_price = row.cost_unit_price;
        if(cost_unit_price == null){
            cost_unit_price = '';
        }
        str += '<span class="cost">' + cost_unit_price + '</span>';
        return str;
    }
    function sell_unit_price_for(value,row,index){
        var str = '';
        var sell_unit_price = row.sell_unit_price;
        if(sell_unit_price == null){
            sell_unit_price = '';
        }
        str += '<span class="sell">' + sell_unit_price + '</span>';
        return str;
    }

    function cost_number_for(value,row,index){
        var str = '';
        var cost_number = row.cost_number;
        if(cost_number == null){
            cost_number = '';
        }
        str += '<span class="cost">' + cost_number + '</span>';
        return str;
    }
    function sell_number_for(value,row,index){
        var str = '';
        var sell_number = row.sell_number;
        if(sell_number == null){
            sell_number = '';
        }
        str += '<span class="sell">' + sell_number + '</span>';
        return str;
    }
    function cost_amount_for(value,row,index){
        var str = '';
        var cost_amount = row.cost_amount;
        if(cost_amount == null){
            cost_amount = '';
        }
        var cost_actual_flag = row.cost_actual_flag;
        if(cost_actual_flag == '' || cost_actual_flag == undefined){
            cost_actual_flag = '';
        }else{
            cost_actual_flag = ' (' + cost_actual_flag + ')';
        }
        var cost_invoice_flag = row.cost_invoice_flag;
        if(cost_invoice_flag == '' || cost_invoice_flag == undefined){
            cost_invoice_flag = '';
        }else{
            cost_invoice_flag = ' (' + cost_invoice_flag + ')';
        }
        var cost_chongping = row.cost_chongping;
        if(cost_chongping !== '' && cost_chongping !== undefined) cost_chongping = ' (' + cost_chongping + ')';
        else cost_chongping = '';
        str += '<span class="cost">' + cost_amount + cost_invoice_flag + cost_actual_flag + cost_chongping + '</span>';
        return str;
    }
    function sell_amount_for(value,row,index){
        var str = '';
        var sell_amount = row.sell_amount;
        if(sell_amount == null){
            sell_amount = '';
        }
        var sell_actual_flag = row.sell_actual_flag;
        if(sell_actual_flag == '' || sell_actual_flag == undefined){
            sell_actual_flag = '';
        }else{
            sell_actual_flag = ' (' + sell_actual_flag + ')';
        }
        var sell_invoice_flag = row.sell_invoice_flag;
        if(sell_invoice_flag == '' || sell_invoice_flag == undefined){
            sell_invoice_flag = '';
        }else{
            sell_invoice_flag = ' (' + sell_invoice_flag + ')';
        }
        var sell_chongping = row.sell_chongping;
        if(sell_chongping !== '' && sell_chongping !== undefined) sell_chongping = ' (' + sell_chongping + ')';
        else sell_chongping = '';
        str += '<span class="sell">' + sell_amount + sell_invoice_flag + sell_actual_flag + sell_chongping + '</span>';
        return str;
    }
    function cost_vat_for(value,row,index){
        var str = '';
        if(row['id'] == 'Sum:'){
            return str;
        }
        var cost_vat = row.cost_vat;
        if(cost_vat == null){
            cost_vat = '';
        }
        var cost_vat_withhold = row.cost_vat_withhold;
        if(cost_vat_withhold == null){
            cost_vat_withhold = '';
        }
        // str += '<span class="cost" onclick="vat_update(' + index + ', \'cost\')" style="cursor: pointer;">' + cost_vat + '</span>';
        str += '<span class="cost">' + cost_vat + '</span>';
        str += '<br/><span class="color: #a59797;">' + cost_vat_withhold + '</span>';
        return str;
    }
    function sell_vat_for(value,row,index){
        var str = '';
        if(row['id'] == 'Sum:'){
            return str;
        }
        var sell_vat = row.sell_vat;
        if(sell_vat == null){
            sell_vat = '';
        }
        var sell_vat_withhold = row.sell_vat_withhold;
        if(sell_vat_withhold == null){
            sell_vat_withhold = '';
        }
        // str += '<span class="sell" onclick="vat_update(' + index + ', \'sell\')" style="cursor: pointer;">' + sell_vat + '</span>';
        str += '<span class="sell" >' + sell_vat + '</span>';
        str += '<br/><span class="color: #a59797;">' + sell_vat_withhold + '</span>';
        return str;
    }
    function cost_confirm_date_for(value,row,index){
        var str = '';
        if(row['id'] == 'Sum:'){
            return str;
        }
        var cost_confirm_date = row.cost_confirm_date;
        if(row.cost_id != null){
            if(cost_confirm_date == '0000-00-00'){
                cost_confirm_date = '<span onclick="batch_confirm_date(\'cost\', ' + index + ')"><?= lang('未确认');?></span>';
            }else{
                cost_confirm_date = '<span onclick="batch_date_cancel(\'cost\', ' + index + ')">' + cost_confirm_date + '<?= lang('确认');?></span>';
            }
            str += '<span class="cost">' + cost_confirm_date + '</span>';
        }
        return str;
    }
    function sell_confirm_date_for(value,row,index){
        var str = '';
        if(row['id'] == 'Sum:'){
            return str;
        }
        var sell_confirm_date = row.sell_confirm_date;
        if(row.sell_id != null){
            if(sell_confirm_date == '0000-00-00'){
                sell_confirm_date = '<span onclick="batch_confirm_date(\'sell\', ' + index + ')"><?= lang('未确认');?></span>';
            }else{
                sell_confirm_date = '<span onclick="batch_date_cancel(\'sell\', ' + index + ')">' + sell_confirm_date + '<?= lang('确认');?></span>';
            }
            str += '<span class="sell">' + sell_confirm_date + '</span>';
        }
        return str;
    }
    function cost_invoice_no_for(value,row,index){
        var str = '';
        var cost_invoice_no = row.cost_invoice_no;
        if(cost_invoice_no == null){
            cost_invoice_no = '';
        }
        str += '<span class="cost">' + cost_invoice_no + '</span>';
        return str;
    }
    function sell_invoice_no_for(value,row,index){
        var str = '';
        var sell_invoice_no = row.sell_invoice_no;
        if(sell_invoice_no == null){
            sell_invoice_no = '';
        }
        str += '<span class="sell">' + sell_invoice_no + '</span>';
        return str;
    }
    function cost_close_date_for(value,row,index){
        var str = '';
        var cost_close_date = row.cost_close_date;
        if(cost_close_date == null){
            cost_close_date = '';
        }
        str += '<span class="cost">' + cost_close_date + '</span>';
        return str;
    }
    function sell_close_date_for(value,row,index){
        var str = '';
        var sell_close_date = row.sell_close_date;
        if(sell_close_date == null){
            sell_close_date = '';
        }
        str += '<span class="sell">' + sell_close_date + '</span>';
        return str;
    }

    function client_code_update(index, type = 'cost') {
        var tt = $('#tt');
        tt.datagrid('selectRow', index);
        var row = tt.datagrid('getSelected');

        if(row != null){
            var form_data = {};
            if(type == 'sell'){
                form_data.client_code = row.debitor;
                $('#update_div_client_code').combobox('options').bill_type = 'sell';
                reload_edit_client_code('debitor');
            }else{
                form_data.client_code = row.creditor;
                $('#update_div_client_code').combobox('options').bill_type = 'cost';
                reload_edit_client_code('creditor');
            }

            form_data.id = row[type + '_id'];
            //已确认无法修改
            var Tips = '';
            if(row[type + '_parent_id'] != 0){
                Tips = '<?= lang('不能修改分摊账单');?>';
            }
            if(row[type + '_confirm_date'] != '0000-00-00'){
                Tips = '<?= lang('已确认无法修改');?>';
            }
            if(row[type + '_account_no'] != ''){
                Tips = '<?= lang('已填写对账号无法修改');?>';
            }
            if(row[type + '_invoice_id'] != 0){
                Tips = '<?= lang('已开票无法修改');?>';
            }
            if(row[type + '_payment_id'] != 0){
                Tips = '<?= lang('已核销无法修改');?>';
            }
            if(row[type + '_split_id'] != 0){
                Tips = '<?= lang('已拆分无法修改');?>';
            }
            if(Tips != ''){
                $.messager.alert('<?= lang('提示');?>', Tips);
                return false;
            }

            $('#client_code_div').window('open');
            $('#client_code_form').form('load', form_data);
        }else{
            $.messager.alert('<?= lang('提示');?>', '<?= lang('发生错误');?>');
        }
    }

    function client_code_save() {
        $.messager.confirm('<?= lang('提示');?>','<?= lang('是否确认提交？');?>',function(r){
            if (r){
                $('#client_code_form').form('submit', {
                    url:'/biz_bill/update_field_data',
                    onSubmit:function () {
                        return $(this).form('validate');    // 返回false终止表单提交
                    },
                    success: function(res_json){
                        var res;
                        try {
                            res = $.parseJSON(res_json);
                        }catch (e) {
                            $.messager.alert('<?= lang('提示');?>', res_json);
                        }
                        if(res === undefined){
                            return false;
                        }
                        $.messager.alert('<?= lang('提示');?>', res.msg);
                        if(res.code == 0){
                            $('#tt').datagrid('reload');
                            $('#client_code_div').window('close');
                        }else{

                        }
                    },
                });
            }
        });
    }

    //汇率日期修改 按钮
    function ex_rate_date() {
        $('#ex_rate_date_div').window('open');
    }

    //汇率日期修改 保存
    function ex_rate_date_save() {
        $.messager.confirm('<?= lang('提示');?>','<?= lang('是否确认提交？');?>',function(r){
            if (r){
                ajaxLoading();
                $('#ex_rate_date_form').form('submit', {
                    url:'/biz_consol/save_ex_rate_date',
                    onSubmit:function () {
                        var validate = $(this).form('validate'); // 返回false终止表单提交
                        if(!validate) ajaxLoadEnd();
                        return validate;
                    },
                    success: function(res_json){
                        ajaxLoadEnd();
                        try {
                            var res = $.parseJSON(res_json);

                            $.messager.alert('<?= lang('提示');?>', res.msg);

                            if(res.code == 0){
                                $('#tt').datagrid('reload');
                                $('#ex_rate_date_div').window('close');
                                ajax_reload_bill_local(res.data.bill_ETD);
                            }else{

                            }
                        }catch (e) {
                            $.messager.alert('<?= lang('提示');?>', res_json);
                            return false;
                        }


                    },
                });
            }
        });
    }

    function apply_payment_wlk(index, type = 'cost', is_wlk_yw = 0) {
        var tt = $('#tt');
        tt.datagrid('selectRow', index);
        var row = tt.datagrid('getSelected');
        if(row != null){
            $('#apply_payment_wlk').window({
                title: 'window',
                width: 700,
                height: 500,
                cache: false,
                modal: true,
                onResize:function (width, height) {
                    $('#apply_payment_wlk_iframe').width(width - 30);
                    $('#apply_payment_wlk_iframe').height(height - 50);
                }
            });
            $('#apply_payment_wlk_iframe').attr('src', '/biz_bill_payment_wlk_apply/add?bill_id=' + row[type + '_id'] + '&is_wlk_yw=' + is_wlk_yw);
        }else{
            $.messager.alert('<?= lang('提示');?>', '<?= lang('发生错误');?>');
        }
    }

    function creditor_for(value,row,index){
        var str = '';
        var creditor_name = row.creditor_name;
        if(creditor_name == null){
            creditor_name = '';
        }
        str += '<span class="cost" onclick="client_code_update(' + index + ', \'cost\')" style="cursor: pointer;">' + creditor_name + '</span>';
		let bank_account = row.cost_bank_account == null ? '': row.cost_bank_account;
		if(row.cost_is_local == 0) str += "<br><span style='color: #ccc'>account:"+bank_account+"</span>"
        return str;
    }
    function debitor_for(value,row,index){
        var str = '';
        var debitor_name = row.debitor_name;
        if(debitor_name == null){
            debitor_name = '';
        }
        str += '<span class="sell" onclick="client_code_update(' + index + ', \'sell\')" style="cursor: pointer;">' + debitor_name + '</span>';
        let bank_account = row.sell_bank_account == null ? '': row.sell_bank_account;
        if(row.sell_is_local == 0) str += "<br><span style='color: #ccc'>account:"+bank_account+"</span>"
        return str;
    }
    //字段--end
</script>
<script>
    function query_report(form_id, combo_id){
        var where = {};
        var form_data = $('#' + form_id).serializeArray();
        $.each(form_data, function (index, item) {
            if(item.value == "") return true;
            if(where.hasOwnProperty(item.name) === true){
                if(typeof where[item.name] == 'string'){
                    where[item.name] =  where[item.name].split(',');
                    where[item.name].push(item.value);
                }else{
                    where[item.name].push(item.value);
                }
            }else{
                where[item.name] = item.value;
            }
        });
        if(where.length == 0){
            $.messager.alert('Tips', '请填写需要搜索的值');
            return;
        }
        if(combo_id == 'client_code'){
            if(where['client_code'] == '' && where['company_name'] == '' ){
                alert('请填写需要搜索的值');
                return;
            }
        }
        //判断一下当前是 remote 还是local,如果是remode,走下面这个
        var inp = $('#' + combo_id);
        var opt = inp.combogrid('options');
        if(opt.mode == 'remote'){
            inp.combogrid('grid').datagrid('load',where);
        }else{
            //如果是本地的,那么先存一下
            if(opt.old_data == undefined) opt.old_data = inp.combogrid('grid').datagrid('getData'); // 如果没存过,这里存一下
            var rows = [];
            if(JSON.stringify(where) === '{}'){
                inp.combogrid('grid').datagrid('loadData', opt.old_data.rows);
                return;
            }
            $.each(opt.old_data.rows,function(i,obj){
                for(var p in where){
                    var q = where[p].toUpperCase();
                    var v = obj[p].toUpperCase();
                    if (!!v && v.toString().indexOf(q) >= 0){
                        rows.push(obj);
                        break;
                    }
                }
            });
            if(rows.length == 0) {
                inp.combogrid('grid').datagrid('loadData', []);
                return;
            }

            inp.combogrid('grid').datagrid('loadData',rows);
        }
    }
    function combogrid_search_focus(form_id, focus_num = 0){
        // var form = $(form_id);
        setTimeout("easyui_focus($($('" + form_id + "').find('.keydown_search')[" + focus_num + "]));",200);
    }
    function easyui_focus(jq){
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combogrid !== undefined){
            return $(jq).combogrid('textbox').focus();
        }else if(data.combobox !== undefined){
            return $(jq).combobox('textbox').focus();
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('textbox').focus();
        }else if(data.datebox !== undefined){
            return $(jq).datebox('textbox').focus();
        }else if(data.textbox !== undefined){
            return $(jq).textbox('textbox').focus();
        }
    }
</script>
<?php
$userRole = get_session('user_role');
if($lock_lv == 0){
    //操作主管锁
    if(menu_role('shipment_lock1')){
        // if(in_array('operator', $this->session->userdata('user_role'))){
        echo '<a href="javascript:void(0)" title="' . lang('操作锁') . '" onclick="lock(1)">S1锁</a>';
    }
}
else if($lock_lv == 1){
    //销售或市场才可以锁和解锁
    if(menu_role('shipment_lock1')){
        echo '<a href="javascript:void(0)" title="' . lang('解锁') . '" onclick="lock(0)" style="margin-right:15px;">' . lang('解锁') . '</a>';
    }
    if(menu_role('shipment_lock2')){
        echo '<a href="javascript:void(0)" title="' . lang('销售锁') . '" onclick="lock(2)" style="margin-right:15px;">' . lang('S2锁') . '</a>';
    }
}
else if($lock_lv == 2){
    if(menu_role('shipment_lock2')){
        echo '<a href="javascript:void(0)" title="' . lang('解锁') . '" onclick="lock(0)" style="margin-right:15px;">' . lang('解锁') . '</a>';
    }
    if(menu_role('shipment_lock3')){
        echo '<a href="javascript:void(0)" title="' . lang('财务锁') . '" onclick="lock(3)" style="margin-right:15px;">' . lang('S3锁') . '</a>';
    }
}else{
    if($lock_lv > 0 && (menu_role('shipment_lock3') || in_array('finance', $userRole))){
        echo '<a href="javascript:void(0)" title="' . lang('解锁') . '解锁" onclick="lock(0)" style="margin-right:15px;">' . lang('解锁') . '</a>';
    }
}
?>
<?php if($lock_lv < 1){?>
    <div id="dlg" class="" style="width:100%;background-color: #F4F4F4;border:1px solid #95B8E7;margin-top:10px;"
         data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons'">
        <form id="fm" method="post" novalidate style="margin:0;padding:10px 10px">
            <input type="hidden" name="sell_id_type" id="sell_id_type">
            <input type="hidden" name="cost_id_type" id="cost_id_type">
            <input type="hidden" name="sell_id_no" id="sell_id_no">
            <input type="hidden" name="cost_id_no" id="cost_id_no">
            <input type="hidden" name="sell_id" id="sell_id">
            <input type="hidden" name="cost_id" id="cost_id">
            <input type="hidden" name="is_special" value="<?= $is_special;?>">
            <table>
                <tr>
                    <input name="charge_code" id="charge_code" type="hidden">
                    <td><?= lang('charge');?>:</td>
                    <td>
                        <input name="charge" id="charge" class="easyui-combobox" style="width:150px;" data-options="
                        required:true,
                        valueField:'charge_name_cn',
                        textField:'charge_name_long',
                        url:'/biz_charge/get_option',
                        onSelect: function(rec){
                            $('#charge_code').val(rec.charge_code);
                        },
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">
                    </td>
					<td colspan="14"><span class="account_bank_show" style="display: none"><a href="#" style="color: blue" target="_blank" id="write_off_href"></a></span></td>
                </tr>
                <tr class="sell">
                    <td><a href="javascript:void(0);" onclick="reload_client(0)" class="easyui-tooltip" ><?= lang('debitor');?></a></td>
                    <td>
                        <input name="debitor" id="debitor" class="easyui-combobox" style="width:150px;" data-options="
                        required:true,
                        valueField:'client_code',
                        textField:'company_name_en',
                        value: '<?php echo $debitor_default;?>',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                        onLoadSuccess:function(){
                            reload_edit_client_code('debitor');
                            let value = $(this).combobox('getValue')
                            if(value != ''){
                            	$('#sell_payment_bank').combobox('reload','/bsc_dict/get_client_account1/'+value)
								$('#write_off_href').attr('href','/biz_client/edit/'+value)
								$('#write_off_href').text('Click here to add Bank Aceount for the '+$(this).combobox('getText'))
                            }
                        },
                        onSelect:function(rec){
							$('#sell_payment_bank').combobox('reload','/bsc_dict/get_client_account1/'+rec.client_code)
            				$('#write_off_href').attr('href','/biz_client/edit/'+rec.client_code)
            				$('#write_off_href').text('Click here to add Bank Aceount for the '+rec.company_name)
                        }
                    ">
                    </td>
                    <td><?= lang('remark');?>:</td>
                    <td><input id="sell_remark" name="sell_remark" class="easyui-textbox" style="width:100px;"></td>
                    <td><?= lang('sell');?></td>
                    <td>
                        <input name="sell" id="sell" class="easyui-combobox" style="width:60px;" data-options="
                        required:true,
                        valueField:'name',
                        textField:'name',
                        url:'/biz_bill_currency/get_currency/sell',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        }
                    ">
                    </td>
                    <td><?= lang('amount');?></td>
                    <td>
                        <input name="sell_unit_price" id="sell_unit_price" class="easyui-numberbox" required="true" precision="2" style="width:70px;" data-options="
                        onChange:function(newVal, oldVal){
                            if($('#sell_number').numberbox('getValue') == ''){
                                $('#sell_number').numberbox('setValue', '1');
                            }
                            $('#sell_amount').numberbox('setValue', newVal * $('#sell_number').numberbox('getValue'));
                        },
                    ">
                    <td>X</td>
                    <td>
                        <input name="sell_number" id="sell_number" class="easyui-numberbox" required="true" precision="4" style="width:60px;" data-options="
                        onChange:function(newVal, oldVal){
                            if($('#sell_unit_price').numberbox('getValue') == ''){
                                $('#sell_unit_price').numberbox('setValue', '1');
                            }
                            $('#sell_amount').numberbox('setValue', $('#sell_unit_price').numberbox('getValue') * newVal);
                        },
                    ">
                    </td>
                    <td>=</td>
                    <td>
                        <input name="sell_amount" id="sell_amount" class="easyui-numberbox" readonly="readonly" required="true" precision="4" style="width:90px;">
                    </td>
                    <td><?= lang('vat');?></td>
                    <td>
                        <select class="easyui-combobox" name="sell_vat" id="sell_vat" style="width: 58px;" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        value: '0%',
                        url:'/bsc_dict/get_option/vat',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    "></select>
                    </td>
                    <td><?= lang('vat_withhold');?></td>
                    <td>
                        <select class="easyui-combobox" name="sell_vat_withhold" id="sell_vat_withhold" style="width: 58px;" data-options="
                            required: true,
                            valueField:'value',
                            textField:'name',
                            value: '0%',
                            url:'/bsc_dict/get_option/vat_withhold',
                            filter:function(q, row){
                                var opts = $(this).combobox('options');
                                if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                    if(q !== '' && filter[q] == undefined) filter[q] = [];
                                    //添加筛选值
                                    if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                    return true;
                                }else{
                                    return false;
                                }
                            },
                        "></select>
                    </td>
                    <td><?= lang('account');?></td>
                    <td>
						<select class="easyui-combobox"
								id="sell_payment_bank" style="width: 80px" data-options="
                                    valueField : 'id',
                                    textField : 'title_cn_currency',
                                    editable:false,
                                    required:true,
                                    onSelect:function(rec){
                                        $('#sell_payment_bank_id').val(rec.id)
                                    },
                                    onLoadSuccess:function(data){
                                    	if(data.length > 0){
                                    		$('.account_bank_show').hide()
                                    	}else{
                                    		$.messager.alert('Tips','Add bank account for the company in client page.')
                                    		$('.account_bank_show').show()
                                    	}
                                    }
                                ">
						</select>
						<input type="hidden" name="sell_bank_account_id" id="sell_payment_bank_id">
                    </td>
                    <td>
                        <a href="javascript:void(0)" title="<?= lang('创建人');?>" class="easyui-tooltip">
                            <input class="easyui-textbox" name='sell_created_by' id="sell_created_by" disabled="disabled" style="width:60px">
                        </a>
                        <a href="javascript:void(0)" title="<?= lang('发票号');?>" class="easyui-tooltip">
                            <input class="easyui-textbox" name='sell_invoice_no' id="sell_invoice_no" disabled="disabled" style="width:90px">
                        </a>
                        <a href="javascript:void(0)" title="<?= lang('开票日期');?>" class="easyui-tooltip">
                            <input class="easyui-textbox" name='sell_invoice_date' id="sell_invoice_date" disabled="disabled" style="width:90px">
                        </a>
                        <a href="javascript:void(0)" title="<?= lang('创建日期');?>" class="easyui-tooltip">
                            <input class="easyui-textbox" name='sell_created_time' id="sell_created_time" disabled="disabled" style="width:130px">
                        </a>
                    </td>
                </tr>
                <tr class="cost">
                    <td><a href="javascript:void(0);" onclick="reload_client(1)"><?= lang('creditor');?></a></td>
                    <td>
                        <input name="creditor" id="creditor" class="easyui-combobox" style="width:150px;" data-options="
                        required:true,
                        valueField:'client_code',
                        textField:'company_name_en',
                        value: '<?php echo $creditor_default;?>',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                        onLoadSuccess:function(){
                            reload_edit_client_code('creditor');
                            let value = $(this).combobox('getValue')
                            if(value != ''){
                            	$('#cost_payment_bank').combobox('reload','/bsc_dict/get_client_account1/'+value)
								$('#write_off_href').attr('href','/biz_client/edit/'+value)
								$('#write_off_href').text('Click here to add Bank Aceount for the '+$(this).combobox('getText'))
                            }
                        },
                        onSelect:function(rec){
							$('#cost_payment_bank').combobox('reload','/bsc_dict/get_client_account1/'+rec.client_code)
            				$('#write_off_href').attr('href','/biz_client/edit/'+rec.client_code)
            				$('#write_off_href').text('Click here to add Bank Aceount for the '+rec.company_name)
                        }
                        ">
                    </td>
                    <td><?= lang('remark');?>:</td>
                    <td><input id="cost_remark" name="cost_remark" class="easyui-textbox" style="width:100px;"></td>
                    <td><?= lang('cost');?></td>
                    <td>
                        <input name="cost" id="cost" class="easyui-combobox" style="width:60px;" data-options="
                        required:true,
                        valueField:'name',
                        textField:'name',
                        url:'/biz_bill_currency/get_currency/cost',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        }
                    ">
                    </td>
                    <td><?= lang('amount');?></td>
                    <td>
                        <input name="cost_unit_price" id="cost_unit_price" class="easyui-numberbox" required="true" precision="2" style="width:70px;" data-options="
                        onChange:function(newVal, oldVal){
                            if($('#cost_number').numberbox('getValue') == ''){
                                $('#cost_number').numberbox('setValue', '1');
                            }
                            $('#cost_amount').numberbox('setValue', newVal * $('#cost_number').numberbox('getValue'));
                        },
                    ">
                    </td>
                    <td>X</td>
                    <td>
                        <input name="cost_number" id="cost_number" class="easyui-numberbox" precision="4" required="true" style="width:60px;" data-options="
                    onChange:function(newVal, oldVal){
                        if($('#cost_unit_price').numberbox('getValue') == ''){
                            $('#cost_unit_price').numberbox('setValue', '1');
                        }
                        $('#cost_amount').numberbox('setValue', $('#cost_unit_price').numberbox('getValue') * newVal);
                    },">
                    </td>
                    <td>=</td>
                    <td>
                        <input name="cost_amount" id="cost_amount" class="easyui-numberbox" readonly="readonly" required="true" precision="4" style="width:90px;">
                    </td>
                    <td><?= lang('vat');?></td>
                    <td>
                        <select class="vat easyui-combobox" name="cost_vat" id="cost_vat" style="width: 58px;" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        value: '0%',
                        url:'/bsc_dict/get_option/vat',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    "></select>
                    </td>
                    <td><?= lang('vat_withhold');?></td>
                    <td>
                        <select class="easyui-combobox" name="cost_vat_withhold" id="cost_vat_withhold" style="width: 58px;" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        value: '0%',
                        url:'/bsc_dict/get_option/vat_withhold',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    "></select>
                    </td>
					<td><?= lang('account');?></td>
					<td>
						<select class="easyui-combobox"
								id="cost_payment_bank" style="width: 80px" data-options="
                                    valueField : 'id',
                                    textField : 'title_cn_currency',
                                    editable:false,
                                    required:true,
                                    onSelect:function(rec){
                                        $('#cost_payment_bank_id').val(rec.id)
                                    },
                                    onLoadSuccess:function(data){
                                    	if(data.length > 0){
                                    		$('.account_bank_show').hide()
                                    	}else{
                                    		$.messager.alert('Tips','Add bank account for the company in client page.')
                                    		$('.account_bank_show').show()
                                    	}
                                    }
                                ">
						</select>
						<input type="hidden" name="cost_bank_account_id" id="cost_payment_bank_id">
					</td>
                    <td>
                        <a href="javascript:void(0)" title="<?= lang('创建人');?>" class="easyui-tooltip">
                            <input class="easyui-textbox" name='cost_created_by' id="cost_created_by" disabled="disabled" style="width:60px">
                        </a>
                        <a href="javascript:void(0)" title="<?= lang('发票号');?>" class="easyui-tooltip">
                            <input class="easyui-textbox" name='cost_invoice_no' id="cost_invoice_no" disabled="disabled" style="width:90px">
                        </a>
                        <a href="javascript:void(0)" title="<?= lang('创建日期');?>" class="easyui-tooltip">
                            <input class="easyui-textbox" name='cost_created_time' id="cost_created_time" disabled="disabled" style="width:90px">
                        </a>
                    </td>
                </tr>
            </table>
        </form>
        <div id="dlg-buttons">
            <a href="javascript:save()" class="easyui-linkbutton" iconCls="icon-ok" id="save"><?= lang('新增');?></a>
            <a href="javascript:add();$('#debitor').combobox('select', '<?= $debitor_default;?>');" class="easyui-linkbutton" id="add" iconCls="icon-add" ><?= lang('add');?></a>
            <?php if($hasDelete){?>
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm2',plain:false" iconCls="icon-remove"><?= lang('delete');?></a>
            <?php } ?>
            <a href="javascript:void(0);" class="easyui-linkbutton" onclick="javascript:singleSelect = !singleSelect;$('#tt').datagrid('clearSelections').datagrid({singleSelect: singleSelect,onClickRow:function(rowIndex) {click_row(singleSelect,rowIndex)}});"><?= lang('多选');?></a>
            <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm3',plain:false" iconCls="icon-ok"><?= lang('批量确认');?></a>
            <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm6',plain:false" iconCls="icon-ok"><?= lang('日志');?></a>
            <a href="javascript:void(0)"  class="easyui-linkbutton" iconCls="icon-add" onclick="add_history()"><?= lang('复制历史费用');?></a>
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="reload_bill_local_save()" iconCls="icon-ok"><?= lang('刷新汇率');?></a>
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="ex_rate_date()"><?= lang('汇率日期');?></a>
            <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm7',plain:false" iconCls="icon-ok"><?= lang('third party');?></a>
        </div>
    </div>
<?php }else{?>
    <form id="fm" method="post" novalidate style="margin:0;padding:10px 10px">
        <input type="hidden" name="sell_id_type" id="sell_id_type">
        <input type="hidden" name="cost_id_type" id="cost_id_type">
        <input type="hidden" name="sell_id_no" id="sell_id_no">
        <input type="hidden" name="cost_id_no" id="cost_id_no">
        <input type="hidden" name="sell_id" id="sell_id">
        <input type="hidden" name="cost_id" id="cost_id">
        <input type="hidden" name="is_special" value="<?= $is_special;?>">
        <table>
            <tr>
                <input name="charge_code" id="charge_code" type="hidden">
                <td><?= lang('charge');?>:</td>
                <td>
                    <input name="charge" id="charge" class="easyui-combobox" style="width:150px;" data-options="
                        required:true,
                        valueField:'charge_name_cn',
                        textField:'charge_name_long',
                        url:'/biz_charge/get_option',
                        onSelect: function(rec){
                            $('#charge_code').val(rec.charge_code);
                        },
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">
                </td>
            </tr>
            <tr class="sell">
                <td><a href="javascript:void(0);" onclick="reload_client(0)"><?= lang('debitor');?></a></td>
                <td>
                    <input name="debitor" id="debitor" class="easyui-combobox" style="width:150px;" data-options="
                        required:true,
                        valueField:'client_code',
                        textField:'company_name',
                        value: '<?php echo $debitor_default;?>',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                        onLoadSuccess:function(){
                            reload_edit_client_code('debitor');
                        },
                    ">
                </td>
                <td><?= lang('remark');?>:</td>
                <td><input id="sell_remark" name="sell_remark" class="easyui-textbox" style="width:100px;"></td>
                <td><?= lang('sell');?></td>
                <td>
                    <input name="sell" id="sell" class="easyui-combobox" style="width:60px;" data-options="
                        required:true,
                        valueField:'name',
                        textField:'name',
                        url:'/biz_bill_currency/get_currency/sell',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">
                </td>
                <td><?= lang('amount');?></td>
                <td>
                    <input name="sell_unit_price" id="sell_unit_price" class="easyui-numberbox" required="true" precision="2" style="width:70px;" data-options="
                        onChange:function(newVal, oldVal){
                            if($('#sell_number').numberbox('getValue') == ''){
                                $('#sell_number').numberbox('setValue', '1');
                            }
                            $('#sell_amount').numberbox('setValue', newVal * $('#sell_number').numberbox('getValue'));
                        },
                    ">
                <td>X</td>
                <td>
                    <input name="sell_number" id="sell_number" class="easyui-numberbox" required="true" precision="4" style="width:60px;" data-options="
                        onChange:function(newVal, oldVal){
                            if($('#sell_unit_price').numberbox('getValue') == ''){
                                $('#sell_unit_price').numberbox('setValue', '1');
                            }
                            $('#sell_amount').numberbox('setValue', $('#sell_unit_price').numberbox('getValue') * newVal);
                        },
                    ">
                </td>
                <td>=</td>
                <td>
                    <input name="sell_amount" id="sell_amount" class="easyui-numberbox" readonly="readonly" required="true" precision="4" style="width:90px;">
                </td>
                <td><?= lang('vat');?></td>
                <td>
                    <select class="easyui-combobox" name="sell_vat" id="sell_vat" style="width: 58px;" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        value: '0%',
                        url:'/bsc_dict/get_option/vat',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    "></select>
                </td>
                <td><?= lang('vat_withhold');?></td>
                <td>
                    <select class="easyui-combobox" name="sell_vat_withhold" id="sell_vat_withhold" style="width: 58px;" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        value: '0%',
                        url:'/bsc_dict/get_option/vat_withhold',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    "></select>
                </td>
                <td>
                    <a href="javascript:void(0)" title="<?= lang('创建人');?>" class="easyui-tooltip">
                        <input class="easyui-textbox" name='sell_created_by' id="sell_created_by" disabled="disabled" style="width:60px">
                    </a>
                    <a href="javascript:void(0)" title="<?= lang('发票号');?>" class="easyui-tooltip">
                        <input class="easyui-textbox" name='sell_invoice_no' id="sell_invoice_no" disabled="disabled" style="width:90px">
                    </a>
                    <a href="javascript:void(0)" title="<?= lang('开票日期');?>" class="easyui-tooltip">
                        <input class="easyui-textbox" name='sell_invoice_date' id="sell_invoice_date" disabled="disabled" style="width:90px">
                    </a>
                    <a href="javascript:void(0)" title="<?= lang('创建日期');?>" class="easyui-tooltip">
                        <input class="easyui-textbox" name='sell_created_time' id="sell_created_time" disabled="disabled" style="width:130px">
                    </a>
                </td>
            </tr>
            <tr class="cost">
                <td><a href="javascript:void(0);" onclick="reload_client(1)"><?= lang('creditor');?></a></td>
                <td>
                    <input name="creditor" id="creditor" class="easyui-combobox" style="width:150px;" data-options="
                        required:true,
                        valueField:'client_code',
                        textField:'company_name',
                        value: '<?php echo $creditor_default;?>',
                        onSelect:function(rec){
                        },
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                        onLoadSuccess:function(){
                            reload_edit_client_code('creditor');
                        },
                        ">
                </td>
                <td><?= lang('remark');?>:</td>
                <td><input id="cost_remark" name="cost_remark" class="easyui-textbox" style="width:100px;"></td>
                <td><?= lang('cost');?></td>
                <td>
                    <input name="cost" id="cost" class="easyui-combobox" style="width:60px;" data-options="
                        required:true,
                        valueField:'name',
                        textField:'name',
                        url:'/biz_bill_currency/get_currency/cost',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">
                </td>
                <td><?= lang('amount');?></td>
                <td>
                    <input name="cost_unit_price" id="cost_unit_price" class="easyui-numberbox" required="true" precision="2" style="width:70px;" data-options="
                        onChange:function(newVal, oldVal){
                            if($('#cost_number').numberbox('getValue') == ''){
                                $('#cost_number').numberbox('setValue', '1');
                            }
                            $('#cost_amount').numberbox('setValue', newVal * $('#cost_number').numberbox('getValue'));
                        },
                    ">
                </td>
                <td>X</td>
                <td>
                    <input name="cost_number" id="cost_number" class="easyui-numberbox" precision="4" required="true" style="width:60px;" data-options="
                    onChange:function(newVal, oldVal){
                        if($('#cost_unit_price').numberbox('getValue') == ''){
                            $('#cost_unit_price').numberbox('setValue', '1');
                        }
                        $('#cost_amount').numberbox('setValue', $('#cost_unit_price').numberbox('getValue') * newVal);
                    },">
                </td>
                <td>=</td>
                <td>
                    <input name="cost_amount" id="cost_amount" class="easyui-numberbox" readonly="readonly" required="true" precision="4" style="width:90px;">
                </td>
                <td><?= lang('vat');?></td>
                <td>
                    <select class="vat easyui-combobox" name="cost_vat" id="cost_vat" style="width: 58px;" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        value: '0%',
                        url:'/bsc_dict/get_option/vat',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    "></select>
                </td>
                <td><?= lang('vat_withhold');?></td>
                <td>
                    <select class="easyui-combobox" name="cost_vat_withhold" id="cost_vat_withhold" style="width: 58px;" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        value: '0%',
                        url:'/bsc_dict/get_option/vat_withhold',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    "></select>
                </td>
                <td>
                    <a href="javascript:void(0)" title="<?= lang('创建人');?>" class="easyui-tooltip">
                        <input class="easyui-textbox" name='cost_created_by' id="cost_created_by" disabled="disabled" style="width:60px">
                    </a>
                    <a href="javascript:void(0)" title="<?= lang('发票号');?>" class="easyui-tooltip">
                        <input class="easyui-textbox" name='cost_invoice_no' id="cost_invoice_no" disabled="disabled" style="width:90px">
                    </a>
                    <a href="javascript:void(0)" title="<?= lang('创建日期');?>" class="easyui-tooltip">
                        <input class="easyui-textbox" name='cost_created_time' id="cost_created_time" disabled="disabled" style="width:90px">
                    </a>
                </td>
            </tr>
        </table>
    </form>

    <div id="dlg-buttons">
        <?php if($lock_lv == 3){ ?>
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="bill_apply()"><?= lang('bill apply');?></a>
            <!--<a href="javascript:save()" class="easyui-linkbutton" iconCls="icon-ok" id="save22">新增</a>-->
        <?php } ?>
        <?php
        // if($is_special != 1){
        //     if(special_test('special_bill')){
        //         echo '<a href="javascript:void(0)" class="easyui-linkbutton"  onclick="javascript:window.location.href=\'?is_special=1\';">' . lang('special') . '</a>';
        //     }
        // }else{
        //     echo '<a href="javascript:void(0)" class="easyui-linkbutton"  onclick="javascript:window.location.href=\'/biz_bill/index/' . $id_type . '/' . $id_no. '\';">' . lang('non-specialty') . '</a>';
        // }
        ?>
        <a href="javascript:void(0);" class="easyui-linkbutton" onclick="javascript:singleSelect = !singleSelect;$('#tt').datagrid('clearSelections').datagrid({singleSelect: singleSelect,onClickRow:function(rowIndex) {click_row(singleSelect,rowIndex)}});">多选</a>
        <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm3',plain:false" iconCls="icon-ok"><?= lang('批量确认');?></a>
        <a href="javascript:void(0)"  class="easyui-linkbutton" iconCls="icon-add" onclick="add_history()"><?= lang('复制历史费用');?></a>
        <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm6',plain:false" iconCls="icon-ok"><?= lang('日志');?></a>
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="reload_bill_local_save()" iconCls="icon-ok"><?= lang('刷新汇率');?></a>
		<a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm7',plain:false" iconCls="icon-ok"><?= lang('third party');?></a>
    </div>
<?php }?>
<div id="mm2" style="width:150px;display:none;">
    <div data-options="iconCls:'icon-remove'" onclick="javascript:del('sell');"><?= lang('debitor');?><?= lang('delete');?></div>
    <div data-options="iconCls:'icon-remove'" onclick="javascript:del('cost');"><?= lang('creditor');?><?= lang('delete');?></div>
    <div data-options="iconCls:'icon-remove'" onclick="javascript:del('all');"><?= lang('整行');?><?= lang('delete');?></div>
</div>
<div id="mm3" style="width:150px;display:none;">
    <div data-options="iconCls:'icon-ok'" onclick="javascript:batch_confirm_date('sell');"><?= lang('debitor');?><?= lang('confirm');?></div>
    <div data-options="iconCls:'icon-ok'" onclick="javascript:batch_confirm_date('cost');"><?= lang('creditor');?><?= lang('confirm');?></div>
    <div data-options="iconCls:'icon-ok'" onclick="javascript:batch_confirm_self('sell');"><?= lang('debitor');?><?= lang('confirm');?>(<?= lang('editor');?>)</div>
    <div data-options="iconCls:'icon-ok'" onclick="javascript:batch_confirm_self('cost');"><?= lang('creditor');?><?= lang('confirm');?>(<?= lang('editor');?>)</div>
    <div data-options="iconCls:'icon-ok'" onclick="javascript:batch_confirm_self('');"><?= lang('all');?><?= lang('confirm');?>(<?= lang('editor');?>)</div>
</div>
<div id="mm6" style="width:150px;display:none;">
    <div data-options="iconCls:'icon-ok'" onclick="javascript:sys_log('sell');"><?= lang('应收日志');?></div>
    <div data-options="iconCls:'icon-ok'" onclick="javascript:sys_log('cost');"><?= lang('应付日志');?></div>
</div>
<div id="mm7" style="width:150px;display:none;">
    <div data-options="iconCls:'icon-ok'" onclick="javascript:third_party1();"><?= lang('collect on behalf');?></div>
    <div data-options="iconCls:'icon-ok'" onclick="javascript:third_party2();"><?= lang('pay on behalf');?></div>
</div>
<div id="third_party_window" class="easyui-window" style="width:620px;height:325px;padding: 15px" data-options="title:'third party',modal:true,closed:true,minimizable:false,collapsible:false,maximizable:false">
    <form id="fm_party">
        <input type="hidden" name="bill_ids" id="bill_ids" value="">
        <input type="hidden" name="receive_party" id="receive_party" value="">
        <input type="hidden" name="payer_party" id="payer_party" value="">
        <input type="hidden" name="job_no" value="<?=$shipment['job_no']?>">
        <table>
            <tr class="third_party_show">
                <td>charge:</td>
                <td>
					<span class="third_party_charge"></span>
                </td>
            </tr>
            <tr class="third_party_show">
                <td>amount:</td>
                <td>
					<span class="third_party_amount"></span>
                </td>
            </tr>
            <tr>
                <td>payer party(client):</td>
                <td>
					<span class="payer_party"></span>
                </td>
            </tr>
            <tr>
                <td>job no:</td>
                <td><span class="job_no"><?=$shipment['job_no']?></span></td>
            </tr>
            <tr>
                <td>agent party:</td>
                <td>
					<div class="not_third_party_div">
                        <select name="agent_party" class="easyui-combobox" style="width: 350px" data-options="
							required:true,
							editable:false,
							valueField:'value',
							textField:'company_name',
							url:'/bsc_dict/get_branch_office',
							">

                        </select>
                    </div>
                    <div class="third_party_div" style="display: none;">
                        <span class="third_party_div_agent_party"></span>
                    </div>
				</td>
            </tr>
            <tr>
                <td>apply party:</td>
                <td><span class="receive_party"></span></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <div style="display: flex">
                        <div class="save_party1">
                            <a href="javascript:;" class="easyui-linkbutton" id="save_party_button" disabled onclick="save_party()">save</a>
                        </div>
                        <div class="save_party2" style="display: none">
                            <span style="color: red">not confirmed</span>
                        </div>
                        <div class="third_party_template_div third_party_div" style="display: none;margin-left: 10px;line-height: 26px">
                            <?= template_link('shipment', $id_no, get_system_config('third_party_template_id'),'download receipt');?>
                        </div>
                    </div>

                </td>
            </tr>
        </table>
    </form>

</div>

<script>
	var third_party_type = 1;
	function open_third_party(row){
		third_party_type = 1;
		if(row.sell_id){
			$('.third_party_show').show()
			$('.third_party_charge').text(row.charge_name_en)
			$('.third_party_amount').text(row.sell+' '+row.sell_amount)
			$('.payer_party').text(row.debitor_name)
			$('#payer_party').val(row.debitor)
			$('#bill_ids').val(row.sell_id)
			$('.save_party1').show()
			$('.save_party2').hide()
			if(row.sell_confirm_date == '0000-00-00'){
				$('.save_party1').hide()
				$('.save_party2').show()
			}
			$.post("/biz_bill/third_party/"+row.debitor, {},
					function(res){
						if(res.code == 0){
							$.messager.alert('Tips',res.msg)
						}
						if(res.code == 1){
							$('.receive_party').text(res.data.receive_party)
							$('#receive_party').val(res.data.receive_party_code)
							$('#third_party_window').window('open')
						}
					}, "json");
		}
	}
    function third_party1(){
		third_party_type = 1;
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length == 0){
            $.messager.alert('Tips', '请选中一条数据');
            return;
        }else{
			$('.save_party1').show()
			$('.save_party2').hide()
			$('.third_party_show').hide()
            let msg = '';
            let client_name = rows[0].debitor_name
            let ids_arr = [];
            rows.map(item=>{
                if(item.charge == '***') msg='所选数据无权限';
                if(item.sell_id == null) msg='请选择应收';
                if(item.debitor_name != client_name) msg = 'Client must be the same.';
                if(item.sell_is_local > 0) msg = 'account是本地账号';
				if(item.sell_confirm_date == '0000-00-00') msg = 'not confirmed';
                ids_arr.push(item.sell_id)
            })
            if(msg != ''){
                $.messager.alert('Tips', msg);
                return;
            }
            $('.payer_party').text(client_name)
            $('#payer_party').val(rows[0].debitor)
            let ids = ids_arr.join(',')
            $('#bill_ids').val(ids)
            $.post("/biz_bill/third_party/"+rows[0].debitor, {},
                function(res){
                    if(res.code == 0){
                        $.messager.alert('Tips',res.msg)
                    }
                    if(res.code == 1){
                        $('.receive_party').text(res.data.receive_party)
                        $('#receive_party').val(res.data.receive_party_code)
                        agent_party_change(1);
                        $('#third_party_window').window('open');
                    }
                }, "json");

        }
    }
    function third_party2(){
		third_party_type = 2;
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length == 0){
            $.messager.alert('Tips', '请选中一条数据');
            return;
        }else{
			$('.save_party1').show()
			$('.save_party2').hide()
			$('.third_party_show').hide()
            let msg = '';
            let client_name = rows[0].creditor_name
            let ids_arr = [];
            rows.map(item=>{
                if(item.charge == '***') msg='所选数据无权限';
                if(item.cost_id == null) msg='请选择应收';
                if(item.creditor_name != client_name) msg = 'Client must be the same.';
                if(item.cost_is_local > 0) msg = 'account是本地账号';
				if(item.cost_confirm_date == '0000-00-00') msg = 'not confirmed';
                ids_arr.push(item.cost_id)
            })
            if(msg != ''){
                $.messager.alert('Tips', msg);
                return;
            }
            $('.payer_party').text(client_name)
            $('#payer_party').val(rows[0].creditor)
            let ids = ids_arr.join(',')
            $('#bill_ids').val(ids)
            $.post("/biz_bill/third_party/"+rows[0].creditor, {},
                function(res){
                    if(res.code == 0){
                        $.messager.alert('Tips',res.msg)
                    }
                    if(res.code == 1){
                        $('.receive_party').text(res.data.receive_party)
                        $('#receive_party').val(res.data.receive_party_code)
                        agent_party_change(2);
                        $('#third_party_window').window('open');
                    }
                }, "json");

        }
    }
	function agent_party_change(type=1) {
		var data = $('#fm_party').serializeArray();
		let obj = {};
		data.map(item=>{
			obj[item.name] = item.value
		});
		//选择完后, 检测当前数据是否存在china库中, 不存在允许使用保存按钮, 存在出现一个download receipt按钮, 点击后下载
		ajaxLoading();
		$.ajax({
			type:'POST',
			url:'/biz_bill/get_third_party_data/'+type,
			data:obj,
			dataType:'json',
			success:function(res){
				ajaxLoadEnd();
				if(res.code == 0){
					//如果获取到值了, 这里直接生成一个download按钮
					if(res.data.rows.length > 0){
						var this_row = res.data.rows[0];
						$('.third_party_div_agent_party').text(this_row.agent_party_name);
						$('.third_party_div').css('display', 'block');
						$('.not_third_party_div').css('display', 'none');
						$('#save_party_button').linkbutton('disable');
					}else{
						//反之让按钮可以用
						$('.third_party_div').css('display', 'none');
						$('.not_third_party_div').css('display', 'block');
						$('#save_party_button').linkbutton('enable');
					}
				}
			},
			error:function (e) {
				ajaxLoadEnd();
				$.messager.alert('<?= lang('提示');?>',e.responseText);
			},
		});
	}
	function save_party(){
		if(!$('#fm_party').form('validate')){
			return false;
		}
		var data = $('#fm_party').serializeArray()
		let obj = {};
		data.map(item=>{
			obj[item.name] = item.value
		})
		$.post("/biz_bill/save_third_party/"+third_party_type, obj,
				function(res){
					$.messager.alert('Tips',res.msg)
					if(res.code == 1){
						$('#third_party_window').window('close')
						$('#tt').datagrid('reload')
					}
				}, "json");
	}
    function sys_log(type){
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length == 0){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请选中一条数据');?>');
            return;
        }else{
            if(type=='sell'){
                window.open("/sys_log/index?table_name=biz_bill&key_no=" + rows[0]["sell_id"]);
            }else{
                window.open("/sys_log/index?table_name=biz_bill&key_no=" + rows[0]["cost_id"]);
            }
        }
    }
</script>
<table id="tt"
       rownumbers="false" pagination="true" idField="id"
       pagesize="150" toolbar="#tb" singleSelect="true" nowrap="false" style="height: 500px">
    <!--showFooter="true"-->
    <thead>
    <tr>
        <!--        <th data-options="field:'id',width:44,sortable:true">--><?//= lang('id');?><!--</th>-->
        <th data-options="field:'charge',width:300, formatter:charge_for"><?= lang('charge');?></th>
        <th data-options="field:'charge_code',width:60"><?= lang('charge_code');?></th>
        <th data-options="field:'sell',width:65,formatter:sell_for,styler:sell_all_style"><?= lang('sell');?></th>
        <th data-options="field:'sell_unit_price',width:90,sortable:true,formatter:sell_unit_price_for,styler:sell_all_style"><?= lang('sell_unit_price');?></th>
        <th data-options="field:'sell_number',width:70,formatter:sell_number_for,styler:sell_all_style"><?= lang('sell_number');?></th>
        <th data-options="field:'sell_amount',width:150,sortable:true,formatter:sell_amount_for,styler:sell_all_style"><?= lang('sell_amount');?></th>
        <th data-options="field:'sell_vat',width:55,formatter:sell_vat_for,styler:sell_all_style"><?= lang('sell_vat');?></th>
        <th data-options="field:'sell_confirm_date',width:85,formatter:sell_confirm_date_for,styler:sell_all_style" >
            <?= lang('confirm_date');?><br><span title='<?= lang('点击未确认可以确认费用。 点击已确认可以取消确认');?>' style='color:red;'><?= lang('(确认/取消)');?></span></th>
        <th data-options="field:'debitor',width:230,formatter:debitor_for,styler:sell_all_style"><?= lang('debitor');?></th>
        <th data-options="field:'cost',width:55,formatter:cost_for,styler:cost_all_style"><?= lang('cost');?></th>
        <th data-options="field:'cost_unit_price',width:90,sortable:true,formatter:cost_unit_price_for,styler:cost_all_style"><?= lang('cost_unit_price');?></th>
        <th data-options="field:'cost_number',width:60,formatter:cost_number_for,styler:cost_all_style"><?= lang('cost_number');?></th>
        <th data-options="field:'cost_amount',width:120,sortable:true,formatter:cost_amount_for,styler:cost_all_style"><?= lang('cost_amount');?></th>
        <th data-options="field:'cost_vat',width:55,formatter:cost_vat_for,styler:cost_all_style"><?= lang('cost_vat');?></th>
        <th data-options="field:'cost_confirm_date',width:85,formatter:cost_confirm_date_for,styler:cost_all_style"><?= lang('confirm_date');?><br><span style='color:red;'><?= lang('(确认/取消)');?></span></th>
        <th data-options="field:'creditor',width:230,formatter:creditor_for,styler:cost_all_style"><?= lang('creditor');?></th>
    </tr>
    </thead>
</table>
<!-- 显示币种相关的利润统计 -->
<style>
    .currency_total{
        display: flex;
        display:-webkit-flex;
        font-weight: bold;
    }
    .currency_total_column{
        display: flex;
        display:-webkit-flex;
        flex-direction: column;
    }
    .currency_total_column_row{
        display: flex;
        display:-webkit-flex;
        width: 220px;
        padding: 3px 0;
    }
    .currency_total_column_row_left{
        width: 120px;
        text-align: right;
        padding-right: 10px;
    }
    .currency_total_column_row_right{
        width: 100px;
    }
</style>
<div class="currency_total">

</div>
<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()">add</a>-->

                <!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true"-->
                <!--                   onclick="javascript:$('#tt').edatagrid('saveRow')">save</a>-->
                <!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true"-->
                <!--                   onclick="javascript:$('#tt').edatagrid('cancelRow')">cancel</a>-->
            </td>
        </tr>
    </table>

</div>

<!--批量费用确认-start-->
<div id="batch_confirm_date" class="easyui-dialog" style="width:300px;height:150px" data-options="title:'window',buttons:'#bcd',modal:true,closed:true">
    <input type="hidden" name="ids">
    <table>
        <!--<tr>
            <td><?= lang('account_no');?>:</td>
            <td>
                <input class="easyui-textbox" id="account_no">
            </td>
        </tr>-->
        <tr>
            <td><?= lang('confirm_date');?>:</td>
            <td>

                <input class="easyui-datebox" required id="confirm_date">
            </td>
        </tr>
    </table>
</div>
<div id="bcd">
    <a href="javascript:void(0);" onclick="save_batch_confirm()" class="easyui-linkbutton"><?= lang('save');?></a>
</div>
<!--批量费用确认-end-->

<!--开票申请-->
<div id="invoice_apply" class="easyui-dialog" style="width:480px;height:350px" data-options="title:'window',buttons:'#iab',modal:true,closed:true">
    <br>
    <form id="invoice_apply_from">
        <input type="hidden" name="ids">
        <table id="table2">

            <tr>
                <td><?= lang('buyer_company');?></td>
                <td>
                    <select id="buyer_company" class="easyui-combobox" style="width:250px;" readonly data-options="
                        valueField:'client_code',
                        textField:'company_name',
                        url:'/biz_client/get_client/?q_field=client_code',
                        mode: 'remote',
                        onBeforeLoad:function(param){
                            if($(this).combobox('getValue') == '')return false;
                            if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                        },
                        onLoadSuccess:function(){
                            var val = $(this).combobox('getValue');
                            $(this).combobox('clear').combobox('select', val);
                        },
                        onSelect: function(res){
                            $('#invoice_apply input[name=\'buyer_client_code\']').val(res.client_code);
                            $('#invoice_apply input[name=\'buyer_company\']').val(res.taxpayer_name);
                            $('#invoice_apply input[name=\'buyer_taxpayer_id\']').val(res.taxpayer_id);
                            $('#invoice_company').combobox('loadData', res.finance_sign_company_data);
                        },
                    ">
                    </select>
                    <input type="hidden" name="buyer_client_code">
                    <input type="hidden" name="buyer_company">
                    <input type="hidden" name="buyer_taxpayer_id">
                </td>
                <td>
                    <a style="color:blue;cursor: pointer" onclick="buyer_company_herf()" target="_blank"><?= lang('查看信息详情');?></a>
                    <script>
                        function buyer_company_herf(){
                            var val = $('#buyer_company').combobox('getValue');
                            window.open('/biz_bill/buyer_company_herf/'+val);
                        }
                    </script>
                </td>
            </tr>
            <tr>
                <td><?= lang('currency');?></td>
                <td>
                    <input name="currency" class="easyui-textbox" readonly style="width:250px;">
                </td>
            </tr>
            <tr>
                <td><?= lang('vat');?></td>
                <td>
                    <input type="text" name="vat" readonly class="easyui-textbox" style="width:250px;">
                </td>
            </tr>
            <tr>
                <td><?= lang('invoice_type');?></td>
                <td>
                    <select name="invoice_type" editable="false" class="easyui-combobox" style="width:250px;"  data-options="
                        required:true,
                        valueField:'value',
                        textField:'name',
                        url:'/bsc_dict/get_option/invoice_type',
                        onSelect: function(rec){
                            invoice_type_select(rec);
                        }
                    ">
                    </select>
                </td>
            </tr>
            <tr class="hidden_invoice_input invoice_type999">
                <td><?= lang('payment_bank');?></td>
                <td>
                    <select name="payment_bank" id="payment_bank" class="easyui-combobox" style="width: 250px" data-options="
                        valueField:'name',
                        textField:'name',
                        url:'/bsc_dict/get_option/payment_bank',
                    ">
                    </select>
                </td>
            </tr>
            <tr class="hidden_invoice_input invoice_type1001">
                <td><?= lang('use_type');?></td>
                <td>
                    <select name="use_type" id="use_type" class="easyui-combobox" style="width: 250px" data-options="
                        valueField:'name',
                        textField:'name',
                        url:'/bsc_dict/get_option/use_type',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('invoice_amount');?></td>
                <td>
                    <input name="invoice_amount" class="easyui-numberbox" readonly precision="4" style="width:250px;">
                </td>
            </tr>
            <tr>
                <td><?= lang('invoice_company');?>
                    <a href="javascript:void;" class="easyui-tooltip" data-options="
                        content: $('<div></div>'),
                        onShow: function(){
                            $(this).tooltip('arrow').css('left', 20);
                            $(this).tooltip('tip').css('left', $(this).offset().left);
                        },
                        onUpdate: function(cc){
                            cc.panel({
                                width: 300,
                                height: 'auto',
                                border: false,
                                href: '/bsc_help_content/help/invoice_company_20220617'
                            });
                        }
                    "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                    <a href="javascript:void(0);" onclick="edit_invoice_company()">E</a>
                    |
                    <a href="javascript:void(0);" onclick="reload_buyer_company()">R</a>
                </td>
                <td>
                    <select name="invoice_company" id="invoice_company" class="easyui-combobox" style="width:250px;" data-options="
                        required: true,
                        editable: false,
                        valueField:'company_name',
                        textField:'company_title',
                    ">
                    </select>
                    <br/>
                    <script>
                        function edit_invoice_company() {
                            var buyer_company = $('#buyer_company').combobox('getValue');
                            window.open('/biz_client/edit_by_client/' + buyer_company);
                        }
                        function reload_buyer_company() {
                            $('#buyer_company').combobox('reload');
                        }
                    </script>

                </td>
                <td>
                    <a href="javascript:;" onclick="upload_pay_confirm()"><?= lang('上传费用确认单');?></a>
                </td>
            </tr>
            <tr>
                <td><?= lang('remark');?></td>
                <td>
                    <textarea name="remark" id="remark" style="width:250px;"></textarea>
                </td>
            </tr>
        </table>
    </form>
</div>

<div id="invoice_apply1" class="easyui-dialog" style="width:480px;height:350px" data-options="title:'window',modal:true,closed:true">
    <br>

    <table id="table1">
        <tr>
            <td><?= lang('buyer_company');?></td>
            <td>
                <input readonly id="buyer_company1" style="width:248px;border-radius: 5px;border: 1px solid #95B8E7;">
            </td>
            <td>
                <a style="color:blue;cursor: pointer" onclick="buyer_company_herf()" target="_blank"><?= lang('上传费用确认单');?></a>
                <script>
                    function buyer_company_herf(){
                        var val = $('#buyer_company').combobox('getValue');
                        window.open('/biz_bill/buyer_company_herf/'+val);
                    }
                </script>
            </td>
        </tr>
        <tr>
            <td><?= lang('currency');?></td>
            <td>
                <input name="currency" id="currency1" readonly style="width:248px;border-radius: 5px;border: 1px solid #95B8E7;">
            </td>
        </tr>
        <tr>
            <td><?= lang('vat');?></td>
            <td>
                <input type="text" name="vat" id="vat1" readonly  style="width:248px;border-radius: 5px;border: 1px solid #95B8E7;">
            </td>
        </tr>
        <tr>
            <td><?= lang('invoice_type');?></td>
            <td>
                <select  id="invoice_type1" editable="false" class="easyui-combobox" style="width:250px;"  >
                </select>
            </td>
        </tr>
        <tr>
            <td><?= lang('invoice_amount');?></td>
            <td>
                <input id="invoice_amount1"  readonly precision="4" style="width:248px;border-radius: 5px;border: 1px solid #95B8E7;">
            </td>
        </tr>
        <tr>
            <td><?= lang('invoice_company');?>

                <a href="javascript:void(0);" onclick="edit_invoice_company()">E</a>
                |
                <a href="javascript:void(0);" onclick="reload_buyer_company()">R</a>
            </td>
            <td>
                <input id="invoice_company1"  readonly precision="4" style="width:248px;border-radius: 5px;border: 1px solid #95B8E7;">
                <br/>
                <script>
                    function edit_invoice_company() {
                        var buyer_company = $('#buyer_company').combobox('getValue');
                        window.open('/biz_client/edit_by_client/' + buyer_company);
                    }
                    function reload_buyer_company() {
                        $('#buyer_company').combobox('reload');
                    }
                </script>

            </td>
            <td>
                <a href="javascript:;" onclick="upload_pay_confirm1()"><?= lang('上传费用确认单');?></a>
            </td>
        </tr>
        <tr>
            <td><?= lang('remark');?></td>
            <td>
                <textarea name="remark" id="remark1" style="width:250px;"></textarea>
            </td>
        </tr>
    </table>
</div>
<div id="iab">
    <a href="javascript:void(0);" onclick="save_invoice_apply()" class="easyui-linkbutton"><?= lang('save');?></a>
</div>
<div id="upload_pay_confirm_dialog" class="easyui-dialog" style="width:720px;height:525px" data-options="title:'window',modal:true,closed:true">

    <iframe  id="upload_pay_confirm_iframe" frameborder="0" style="width: 100%;height:100%"></iframe>


</div>

<div id="bill_apply" style="display:none;">
    <iframe id="bill_apply_iframe" style="width:810px;height:375px;padding:5px;border:0px;"></iframe>
</div>

<div id="apply_payment_wlk" style="display:none;">
    <iframe id="apply_payment_wlk_iframe" style="width:1110px;height:675px;padding:5px;border:0px;"></iframe>
</div>

<!--DN开票申请-->
<div id="dn_invoice_apply_div" class="easyui-dialog" style="width:400px;height:300px" data-options="title:'window',modal:true,closed:true">
    <form id="dn_invoice_apply_from">
        <input type="hidden" name="ids">
        <input type="hidden" name="invoice_type" value="1001">
        <input type="hidden" name="payment_bank" value="DN">
        <input type="hidden" name="currency">
        <input type="hidden" name="vat">
        <input type="hidden" name="use_type" value="DN">
        <table>
            <tr>
                <td><?= lang('buyer_company');?></td>
                <td>
                    <select name="buyer_company" class="easyui-combobox" style="width:250px;" readonly data-options="
                        valueField:'company_name',
                        textField:'company_name',
                        url:'/biz_client/get_client/?q_field=company_name',
                        mode: 'remote',
                        onBeforeLoad:function(param){
                            if($(this).combobox('getValue') == '')return false;
                            if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                        },
                        onLoadSuccess:function(){
                            var val = $(this).combobox('getValue');
                            $(this).combobox('clear').combobox('select', val);
                        },
                        onSelect: function(res){
                            $('#invoice_apply input[name=\'buyer_client_code\']').val(res.client_code);
                            $('#invoice_apply input[name=\'buyer_company\']').val(res.taxpayer_name);
                            $('#invoice_apply input[name=\'buyer_taxpayer_id\']').val(res.taxpayer_id);
                        },
                    ">
                    </select>
                    <input type="hidden" name="buyer_client_code">
                    <input type="hidden" name="buyer_taxpayer_id">
                </td>
            </tr>
            <tr>
                <td><?= lang('invoice_amount');?></td>
                <td>
                    <input name="invoice_amount" class="easyui-numberbox" readonly precision="4" style="width:250px;">
                </td>
            </tr>
            <tr>
                <td><span style="color:blue;" title="<?= lang('该选项取值于当票客户的debit note account');?>"><?= lang('银行账户信息');?></span></td>
                <td>
                    <select name="account_id" id="de_account_id" class="easyui-combobox" style="width:230px;" data-options="
                        editable:false,
                        valueField:'id',
                        textField:'title_cn',
                    ">
                    </select>
                    <a href="http://wenda.leagueshipping.com/?/question/395" target="_blank"><?= lang('提示');?></a>
                </td>
            </tr>
            <tr>
                <td><?= lang('remark');?></td>
                <td>
                    <textarea name="remark" id="dn_ramark" style="width:250px;"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right;">
                    <a href="javascript:void(0);" onclick="save_dn_invoice_apply()" class="easyui-linkbutton"><?= lang('save');?></a>
                </td>
            </tr>
        </table>
    </form>
</div>
<script>
    $('#add').css('display', 'none');
</script>

<!--发票明细-->
<div id="invoice_read_list" class="easyui-window" title="<?= lang('发票明细');?>" closed="true" style="width:840px;height:475px;padding:5px;" data-options="onResize:function(width,height){
        var this_css = {width: width - 40,height: height - 70};
        $('#invoice_read_list_iframe').css(this_css);
    }">
    <iframe id="invoice_read_list_iframe" style="width:800px;height:405px;padding:5px;border:0px;">

    </iframe>
</div>

<!--client_code修改-->
<div id="client_code_div" class="easyui-window" style="width:400px;height:100px" data-options="title:'window',modal:true,closed:true">
    <form id="client_code_form">
        <input type="hidden" name="id">
        <table>
            <tr>
                <td><a href="javascript:void(0);" onclick="reload_client(1)"><?= lang('修改');?></a></td>
                <td>
                    <input name="client_code" id="update_div_client_code" class="easyui-combobox" style="width:150px;" data-options="
                        required:true,
                        valueField:'client_code',
                        textField:'company_name',
                        value: '<?php echo $creditor_default;?>',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">

                </td>
                <td colspan="4"> <?= lang('添加其他抬头请点击');?>"<a href="javascript:void(0);" onclick="reload_client(0)"><?= lang('这里');?></a>"<?= lang('刷新');?></td>
            </tr>
            <tr>
                <td colspan="6"> </td>
            </tr>
            <tr>
                <td colspan="6">
                    <button onclick="client_code_save()" type="button" class="easyui-linkbutton" style="width: 180px;"><?= lang('save');?></button>
                </td>
            </tr>
        </table>
    </form>
</div>

<!--vat修改-->
<div id="vat_div" class="easyui-window" style="width:400px;height:100px" data-options="title:'window',modal:true,closed:true">
    <form id="vat_form">
        <input type="hidden" name="id">
        <table>
            <tr>
                <td><a href="javascript:void(0);" onclick="reload_client(1)"><?= lang('修改');?></a></td>
                <td>
                    <select class="easyui-combobox" name="vat" id="update_div_vat" style="width: 58px;" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        value: '0%',
                        url:'/bsc_dict/get_option/vat',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    "></select>

                </td>
            </tr>

            <tr>
                <td colspan="6">
                    <button onclick="vat_save()" type="button" class="easyui-linkbutton" style="width: 180px;"><?= lang('save');?></button>
                </td>
            </tr>
        </table>
    </form>
</div>

<!--2022-08-25 新增历史费用窗口-->
<div id="history" class="easyui-window" title="<?= lang('历史费用');?>" style="width:1000px;height:600px" data-options="modal:true">
    <iframe id="iframe" src="" width="100%" height="99%" frameborder="0" scrolling="auto"></iframe>
</div>

<!--汇率日期修改-->
<div id="ex_rate_date_div" class="easyui-window" style="width:570px;height:370px" data-options="title:'window',modal:true,closed:true">
    <form id="ex_rate_date_form">
        <input type="hidden" name="id" value="<?= $consol_id;?>">
        <table style="margin-left: 10px;">
            <tr>
                <td><?= lang("汇率日期");?></td>
                <td>
                    <input class="easyui-datebox" name="ex_rate_date" value="<?= $ex_rate_date;?>" style="width:150px" data-options="onSelect: function(date){
                        var date_str = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
                        var json = {f1:'start_time',s1:'=',v1:date_str};
                        $('#ex_rate_date_table').datagrid({
                            queryParams: json
                        }).datagrid('clearSelections');
                    }">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table id="ex_rate_date_table" class="easyui-datagrid" style="width:520px;height:250px" data-options="url:'/biz_bill_currency/get_data/',queryParams:{f1:'start_time',s1:'=',v1:'<?= $ex_rate_date;?>'}">
                        <thead>
                        <tr>
                            <th field="currency_from" width="100" align="center"><?= lang('currency_from');?></th>
                            <th field="currency_to" width="100" align="center"><?= lang('currency_to');?></th>
                            <th field="sell_value" width="100"><?= lang('sell_value');?></th>
                            <th field="cost_value" width="100"><?= lang('cost_value');?></th>
                            <th field="start_time" width="100"><?= lang('start_time');?></th>
                        </tr>
                        </thead>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <button onclick="ex_rate_date_save()" type="button" class="easyui-linkbutton" style="width: 180px;"><?= lang('save');?></button>
                </td>
            </tr>
        </table>
    </form>
</div>
