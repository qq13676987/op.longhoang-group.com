<?php


class City_model extends CI_Model
{
    private $table = 'bsc_city';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function get_where_one($where = ""){
        if($where != "")$this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_option($where=array()){
        if($where != array()) $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
}