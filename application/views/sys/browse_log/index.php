<script type="text/javascript"src="/inc/third/layer/layer.js"></script>
<link rel="stylesheet" href="/inc/third/layui/css/layui.css" media="all">
<style>
    /**
    * 修改datagrid表格样式
    **/
    .datagrid-body td {height: 40px !important;}
    .datagrid-header, .datagrid-htable {height: 40px !important; background: #eceff0; border-bottom: none;}
    .datagrid-header-row td{border-width: 0px 1px 1px 0px; border-style: solid; border-color: #ddd;}
    .panel-body-noheader {border-top:0px !important;}
    .tabs {padding-left:0px;}
    .tabs-inner {height: 25px !important;}
    .tabs li.tabs-selected a.tabs-inner {border-bottom: 1px solid #F4F4F4;}
    .tabs-header {background: none;}
    .tabs li a.tabs-inner{padding: 0px 20px;}
    .tabs li.tabs-selected a.tabs-inner{background: #F4F4F4}
    .datagrid-row-over {background: #eceeeb;}
    .datagrid-header td.datagrid-header-over {background:#e5e7e8;color: #000000;cursor: default;}
    .layui-layer-lan .layui-layer-title {background: #325763 !important;}
    #cx table tr td {height: 30px; min-width: 50px;}
    #cx table tr td button {width: 23px;}
</style>
<script type="text/javascript">
    function doSearch() {
        $('#tt').datagrid('load', {
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val(),
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val(),
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val()
        });
        $('#chaxun').window('close');
    }

    function buttons_for(value, row, index) {
        var str = '';
        if (row.is_default == false) {
            str += '<button onclick="set_default(' + row.id + ', \'' + row.client_code + '\')"><?= lang('设置默认');?></button>';
        } else {
            str += '<?= lang('当前默认');?>';
        }
        return str;
    }

    function telephone_for(value, row, index) {
        var str = value;

        if(row.is_bind_wx) str += " <span style='color:blue;'><?= lang('已绑微信');?></span>";

        return str;
    }

    $(function () {
        $('#tt').edatagrid({
            width: 'auto',
            height: $(window).height(),
            url: '/sys_browse_log/get_data/?type=<?=$type?>',
            //destroyUrl: '/biz_clue/del_clue_log/',
            onDblClickRow: function (index, row) {}
        });
    });

</script>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th field="id" width="80" sortable="true" align="center"><?= lang('ID');?></th>
        <th field="id_type" width="120" align="center"><?= lang('id_type');?>
        <th field="id_no" width="100" ><?= lang('id_no');?></th>
        <th field="id_name" width="350" ><?= lang('id_name');?></th>
        <th field="user_id" width="150" align="center" formatter="telephone_for"><?= lang('访问用户');?></th>
        <th field="create_time" width="150" align="center" ><?= lang('访问时间');?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
                   onclick="javascript:$('#tt').edatagrid('destroyRow')"><?= lang('生成'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true"
                   onclick="javascript:$('#chaxun').window('open');"><?= lang('查询'); ?></a>
            </td>
            <td width='80'></td>
        </tr>
    </table>
</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                <?= lang('查询'); ?> 1
            </td>
            <td align="right">
                <select name="f1" id="f1" class="easyui-combobox" required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs) {
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1" id="s1" class="easyui-combobox" required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v1" class="easyui-textbox" style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('查询'); ?> 2
            </td>
            <td align="right">
                <select name="f2" id="f2" class="easyui-combobox" required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs) {
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2" id="s2" class="easyui-combobox" required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v2" class="easyui-textbox" style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('查询'); ?> 3
            </td>
            <td align="right">
                <select name="f3" id="f3" class="easyui-combobox" required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs) {
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s3" id="s3" class="easyui-combobox" required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v3" class="easyui-textbox" style="width:120px;">
            </td>
        </tr>
    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()"
       style="width:200px;height:30px;"><?= lang('查询'); ?></a>
</div>
