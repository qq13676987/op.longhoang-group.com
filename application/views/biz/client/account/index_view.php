<script type="text/javascript">
    function doSearch(){
        $('#tt').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val() ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val() ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val()
        });
        $('#chaxun').window('close');
    }

    $(function () {
        $('#tt').edatagrid({
            url: '/biz_client_account/get_data/<?php echo $client_code;?>',
            saveUrl: '/biz_client_account/add_data/<?php echo $client_code;?>',
            updateUrl: '/biz_client_account/update_data/',
            destroyUrl: '/biz_client_account/delete_data'
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
            <?php if(!menu_role('client_account_edit')){ ?>
            onDblClickCell:function(index,field) {
            },
            <?}?>
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });

</script>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th field="id" width="60" sortable="true"><?= lang('id')?></th>
        <!--<th field="bank_title" width="110" align="center" editor="text"><?= lang('bank_title')?></th>-->
        <th field="bank_name" width="250" align="center" editor="text"><?= lang('bank_name')?></th>
        <th field="bank_account" width="300" align="center" editor="text"><?= lang('bank_account')?></th>
        <th field="bank_address" width="500" editor="text"><?= lang('bank_address')?></th>
        <th field="bank_swift_code" width="100" editor="text"><?= lang('bank_swift_code')?></th>
        <th field="biz_type" width="100" editor="{
                            type:'combobox',
                            options:{
                                valueField:'value',
                                textField:'name',
                                url:'/bsc_dict/get_option/client_account_type',
                                required:true
                            }
                }"><?= lang('biz_type')?></th>
        <th field="create_time" width="150" sortable="true"><?= lang('create_time')?></th>
        <th field="update_time" width="150" sortable="true"><?= lang('update_time')?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                  <a href="javascript:void(0)" class="easyui-linkbutton <?= menu_role('client_account_add') ? '' : 'hide';?>" iconCls="icon-add" plain="true" onclick="javascript:$('#tt').edatagrid('addRow',0)"><?= lang('add');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton <?= menu_role('client_account_delete') ? '' : 'hide';?>" iconCls="icon-remove" plain="true" onclick="javascript:$('#tt').edatagrid('destroyRow')"><?= lang('delete');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#tt').edatagrid('saveRow')"><?= lang('save');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#tt').edatagrid('cancelRow')"><?= lang('cancel');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>
            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                <?= lang('query');?> 1
            </td>
            <td align="right">
                <select name="f1"  id="f1"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1"  id="s1"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v1" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('query');?> 2
            </td>
            <td align="right">
                <select name="f2"  id="f2"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2"  id="s2"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v2" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('query');?> 3
            </td>
            <td align="right">
                <select name="f3"  id="f3"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s3"  id="s3"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v3" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;"><?= lang('search');?></a>
</div>
