<style>
    .f{
        width:115px;
    }
    .s{
        width:100px;
    }
    .v{
        width:120px;
    }    
    .del_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }
    .all_t{
        color: blue;
        font-weight: 800;
    }
</style>
<!--这里是查询框相关的代码-->
<script>
    var query_option = {};//这个是存储已加载的数据使用的

    var table_name = 'biz_edi_ebooking_consol',//需要加载的表名称
        view_name = 'biz_edi_ebooking_consol_index';//视图名称
    var add_tr_str = "";//这个是用于存储 点击+号新增查询框的值, load_query_box会根据接口进行加载


    /**
     * 加载查询框
     */
    function load_query_box(){
        ajaxLoading();
        $.ajax({
            type:'GET',
            url: '/sys_config_title/get_user_query_box?view_name=' + view_name + '&table_name=' + table_name,
            dataType:'json',
            success:function (res) {
                ajaxLoadEnd();
                if(res.code == 0){
                    //加载查询框
                    $('#cx table').append(res.data.box_html);
                    //需要调用下加载器才行
                    $.parser.parse($('#cx table tbody .query_box'));

                    add_tr_str = res.data.add_tr_str;
                    query_option = res.data.query_option;
                    var table_columns = [[
                    ]];//表的表头

                    //填充表头信息
                    $.each(res.data.table_columns, function (i, it) {
                        //读取列配置,然后这里进行合并
                        var column_config;
                        try {
                            column_config = JSON.parse(it.column_config,function(k,v){
                                if(v && typeof v === 'string') {
                                    return v.indexOf('function') > -1 || v.indexOf('FUNCTION') > -1 ? new Function(`return ${v}`)() : v;
                                }
                                return v;
                            });
                        }catch (e) {
                            column_config = {};
                        }
                        var this_column_config = {field:it.table_field, title:it.title, width:it.width, sortable: it.sortable, formatter: get_function(it.table_field + '_for'), styler: get_function(it.table_field + '_styler')};
                        //合并一下
                        $.extend(this_column_config, column_config);

                        table_columns[0].push(this_column_config);
                    });

                    //渲染表头
                    $('#tt').datagrid({
                        columns:table_columns,
                        width:'auto',
                        height: $(window).height(),
                        rowStyler:function(index,row){
                            var style = "";
                            switch(row.status){
                                case '-1':
                                    style += 'color:red;';
                                    break;
                                case '0':
                                    style += 'color:blue;';
                                    break;
                            }
                            return style;
                        },
                        onDblClickRow: function(index, row) {
                            url = '/biz_edi_ebooking_consol/edit/'+row.id;
                            window.open(url);
                        },
                    });
                    $(window).resize(function () {
                        $('#tt').datagrid('resize');
                    });

                    //为了避免初始加载时, 重复加载的问题,这里进行了初始加载ajax数据
                    var aj = [];
                    $.each(res.data.load_url, function (i,it) {
                        aj.push(get_ajax_data(it, i));
                    });
                    //加载完毕触发下面的下拉框渲染
                    //$_GET 在other.js里封装
                    var this_url_get = {};
                    $.each($_GET, function (i, it) {
                        this_url_get[i] = it;
                    });
                    var auto_click = this_url_get.auto_click;

                    $.when.apply($,aj).done(function () {
                        //这里进行字段对应输入框的变动
                        $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                            var ec_value = $(ec_item).combobox('getValue');
                            var v_inp = $('.v:eq(' + ec_index + ')');
                            var s_val = $('.s:eq(' + ec_index + ')').combobox('getValue');
                            var v_val = v_inp.textbox('getValue');
                            //如果自动查看,初始值为空
                            if(auto_click === '1') v_val = '';

                            $(ec_item).combobox('clear').combobox('select', ec_value);
                            //这里比对get数组里的值
                            $.each(this_url_get, function (trg_index, trg_item) {
                                //切割后,第一个是字段,第二个是符号
                                var trg_index_arr = trg_index.split('-');
                                //没有第二个参数时,默认用等于
                                if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';
                                //用一个删一个
                                //如果当前的选择框的值,等于get的字段值
                                if(ec_value === trg_index_arr[0] && s_val === trg_index_arr[1]){
                                    v_val = trg_item;//将v改为当前get的
                                    delete this_url_get[trg_index];
                                }
                            });
                            //没找到就正常处理
                            $('.v:eq(' + ec_index + ')').textbox('setValue', v_val);
                        });
                        //判断是否有自动查询参数
                        var no_query_get = ['auto_click'];//这里存不需要塞入查询的一些特殊变量
                        $.each(no_query_get, function (i,it) {
                            delete this_url_get[it];
                        });
                        //完全没找到的剩余值,在这里新增一个框进行选择
                        $.each(this_url_get, function (trg_index, trg_item) {
                            add_tr();//新增一个查询框
                            var trg_index_arr = trg_index.split('-');
                            //没有第二个参数时,默认用等于
                            if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';

                            $('#cx table tbody tr:last-child .f').combobox('setValue', trg_index_arr[0]);
                            $('#cx table tbody tr:last-child .s').combobox('setValue', trg_index_arr[1]);
                            $('#cx table tbody tr:last-child .v').textbox('setValue', trg_item);
                        });

                        //这里自动执行查询,显示明细
                        if(auto_click === '1'){
                            doSearch();
                        }
                    });
                }else{
                    $.messager.alert("<?= lang('提示');?>", res.msg);
                }
            },error:function (e) {
                ajaxLoadEnd();
                $.messager.alert("<?= lang('提示');?>", e.responseText);
            }
        });
    }

    /**
     * 新增一个查询框
     */
    function add_tr() {
        $('#cx table tbody').append(add_tr_str);
        $.parser.parse($('#cx table tbody tr:last-child'));
        var last_f = $('#cx table tbody tr:last-child .f');
        var last_f_v = last_f.combobox('getValue');
        last_f.combobox('clear').combobox('select', last_f_v);
        return false;
    }

    /**
     * 删除一个查询
     */
    function del_tr(e) {
        //删除按钮
        var index = $(e).parents('tr').index();
        $(e).parents('tr').remove();
        return false;
    }

    //执行加载函数
    $(function () {
        load_query_box();
    });
</script>
<script>
    function getValue(jq) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combobox !== undefined){
            return $(jq).combobox('getValue');
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('getValue');
        }else if(data.datebox !== undefined){
            return $(jq).datebox('getValue');
        }else if(data.textbox !== undefined){
            return $(jq).textbox('getValue');
        }
    }

    /**
     * 简略封装的给easyui赋值的方法
     **/
    function setValue(jq, val) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combobox !== undefined){
            return $(jq).combobox('setValue',val);
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('setValue',val);
        }else if(data.datebox !== undefined){
            return $(jq).datebox('setValue',val);
        }else if(data.textbox !== undefined){
            return $(jq).textbox('setValue',val);
        }
    }
    function status_for(value,row,index){
        if(value == 0) return '<?=lang('to do')?>';
        if(value == 1) return '<?=lang('passed')?>';
        if(value == -1) return '<?=lang('reject')?>';
	}
</script>
<script type="text/javascript">
    function doSearch(){
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        $('#tt').datagrid({
            url: '/biz_edi_ebooking_consol/get_data?status=<?= $status;?>',
            clearSelections:true,
            queryParams: json
        }).datagrid('clearSelections');
        set_config();
        $('#chaxun').window('close');
    }

    function resetSearch(){
        $.each($('.v'), function(i,it){
            setValue($(it), '');
        });
    }

    $(function(){

    });

	function del(){
		var row = $('#tt').datagrid('getSelected'); 
		if (row){ 
            $('#tt').edatagrid('destroyRow');
		}else{
			alert("No Item Selected");
		}
	}
	/**
	 * 设置状态,进行查询
	 */
	function status_search(status){
	    $('#status').val(status);
	    doSearch();
	}
</script>
    
<table id="tt" rownumbers="false" pagination="true"  idField="id" pagesize="30" toolbar="#tb"  singleSelect="true" nowrap="false">
</table>

   <div id="tb" style="padding:3px;">
        <table>
		<tr>
        <td>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>
			<a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#status_mm'" iconCls="icon-search"><?= lang('状态查询');?></a>
			<a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm2',plain:true" iconCls="icon-reload"><?= lang('config');?></a>
			&nbsp;&nbsp;&nbsp;&nbsp;
        </td> 
        </tr>
		</table>
         
    </div>
		<div id="mm2" style="width:150px;">
			<div data-options="iconCls:'icon-ok'" onclick="javascript:config()"><?= lang('表头配置');?></div>
		</div>
		<div id="status_mm" style="width:150px;">
			<div data-options="iconCls:'icon-ok'" onclick="status_search('');"><?= lang('all');?></div>
			<div data-options="iconCls:'icon-ok'" onclick="status_search(0);"><?= lang('to do');?></div>
			<div data-options="iconCls:'icon-ok'" onclick="status_search(1);"><?= lang('passed');?></div>
			<div data-options="iconCls:'icon-ok'" onclick="status_search(-1);"><?= lang('reject');?></div>
		</div>
	
	<div id="chaxun" class="easyui-window" title="<?= lang('查询');?>" closed="false" style="width:650px;height:380px;padding:5px;">
		<br><br>
        <form id="cx">
            <input type="hidden" name="status" id="status" value="">
            <table>

            </table>
        </form>
		<br><br>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;"><?= lang('查询');?></a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:resetSearch()" style="width:200px;height:30px;"><?= lang('重置');?></a>
	</div>
	
