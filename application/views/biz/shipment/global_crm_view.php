<link href="/inc/third/layui/css/layui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<style>
    .db_btn{
        width: 34px;
        height: 23px;
        margin: 2px;
    }    
</style>
<script>
    var this_db = 'CN';
    function doSearch(db){
        json = {
            db : db
        };
        this_db = db;
        $('.db_btn').removeAttr('disabled', '');
        $('.db_btn[db_type="' + db + '"]').attr('disabled', 'true');

        $('#tt').datagrid({
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            },
            queryParams: json
        }).datagrid('clearSelections');
    }
    $(function(){
        $('#tt').datagrid({
            url: '/biz_shipment/global_shipment_crm',
            width:'auto',
            height: $(window).height() - 26,
            columns: [[
                {field: 'consignee_company', title: '<?= lang('收货人公司');?>', width: 120},
                {field: 'button', title: '<?= lang('按钮');?>', formatter:button_for, width: 80},
                {field: 'clue_count', title: '<?= lang('提取次数');?>', width: 80},
                {field: 'consignee_address', title: '<?= lang('收货人地址');?>', width: 250},
                {field: 'consignee_telephone', title: '<?= lang('收货人电话');?>', width: 120},
                {field: 'consignee_contact', title: '<?= lang('收货人联系人');?>', width: 120},
                {field: 'consignee_email', title: '<?= lang('收货人邮箱');?>', width: 120},
                {field: 'booking_ETD', title: '<?= lang('订舱船期');?>', width: 80},
                {field: 'vessel_voyage', title: '<?= lang('船名/航次');?>'},
                {field: 'trans_origin_name', title: '<?= lang('POL');?>'},
                {field: 'trans_destination_name', title: '<?= lang('POD');?>'},
                {field: 'trans_carrier', title: '<?= lang('船公司');?>'},
                {field: 'trans_term', title: '<?= lang('运输条款');?>'},
                {field: 'INCO_term', title: '<?= lang('贸易方式');?>'},
                {field: 'client_info', title: '<?= lang('委托方');?>'},
            ]],
        });
        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });
    
    function button_for(value,row,index){
        var str = "";
        
        //加一个提取线索的按钮
        str += '<a href="javascript:void(0);" style="color: #077320;" onclick="gaining(' + index + ')"><?= lang('可提取');?></a>';
        
        return str;
    }
    
    /**
     * 提取的方法, 这里 走简单版的, 根据当前索引选中当前行后, 自动进入新增页面
     */
    function gaining(index){
        var tt = $('#tt');
        tt.datagrid('selectRow', index);
        var rows = tt.datagrid('getSelections');
        if (rows.length == 0){
            return $.messager.alert('<?= lang('提示')?>', '<?= lang('请选择任意一行后再试')?>');
        }
        
        var row = rows[0];
        
        // $.ajax({
        // 	type: "POST",
        // 	url: "/biz_crm/add",
        // 	data: {
        // 	    clue_name:row.consignee_company,
        // 	    company_name:row.consignee_company,
        // 	    company_address:row.consignee_address,
        // 	},
        // 	success: function(res) {
        //         var html = res.data;
        // 		layer.open({
        // 			type: 2,
        // 			shade:0.5,
        //             area: ['1270px', '700px'],
        // 			title: '<?= lang('我要提取');?>',
        // 			content: html,
        // 			btn: ['确定', '关闭'],
        // 			yes: function (index, layero) {
        //                 // 调用子页面（弹出框）的doSubmit方法并获取其返回值
        // 				var context = layero.find('page').context;
        // 				var data = context.defaultView.doSubmit();
        // 			},
        // 			cancel: function (layer_window) {
        //                 // 关闭弹出框页面
        // 				layer.close(layer_window);
        // 			}
        // 		});
        // 	}
        // });
        
        // $.ajax({
        //     url: '/biz_crm/add',
        //     type: 'POST',
        //     data: {
        //         clue_name:row.consignee_company,
        // 	    company_name:row.consignee_company,
        // 	    company_address:row.consignee_address,
        // 	    country_code:this_db,
        //     },
        //     dataType : "html",
        //     success: function (result) {
        //         layer.open({
        //             type: 1,
        //             anim: 0,
        //             title: '<?= lang('我要提取');?>',
        //             shade:0.5,
        //             area: ['1270px', '700px'],
        //             content: result
        //         });
        //     }
        // });
        
        // layer.open({
        //     type: 2,
        //     title: '<?= lang('我要提取');?>',
        //     shade:0.5,
        //     area: ['1270px', '700px'],
        //     content: '/biz_crm/add',
        //     success: function (layero, index) {
                // var iframe = $(layero).find('iframe');
                //先给iframe加一个id属性
                // iframe.attr('id', 'layer_id').attr('name', 'layer_id');
                //然后模拟form表单提交到该iframe
                
                var form = $('<form></form>').attr('target','layer_id').attr('action', '/biz_crm/add?event=clue_insert').attr('method','POST').attr('encoding',  'multipart/form-data');
                var json = {
              	    clue_name:row.consignee_company,
            	    company_name:row.consignee_company,
            	    company_address:row.consignee_address,
            	    country_code:this_db,
                };
        
                $.each(json, function (index, item) {
                    if(typeof item === 'string'){
                        form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                    }else{
                        $.each(item, function(index2, item2){
                            form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                        })
                    }
                });
                form.appendTo('body').submit().remove();
            // }
        // });
    }
</script>
<body>
    <div>
        
    </div>
    <table id="tt" rownumbers="false" pagination="true"  idField="id" pagesize="30" toolbar="#tb"  singleSelect="true" nowrap="false">
    </table>
    
    <div id="tb" style="padding:3px;">
        <table>
            <tr>
                <?php foreach($database_config as $k => $this_db){ ?>
                <td><button class="db_btn" db_type="<?= $k;?>" onClick="doSearch('<?= $k;?>');"><?= lang($k);?></button></td>
                <?php } ?>
            </tr>
        </table>
    </div>
</body>