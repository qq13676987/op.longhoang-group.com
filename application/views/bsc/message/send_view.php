<script type="text/javascript">
        var is_submit = false;
    function create_hbl_files() {
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/api/shipments_hbl_file?consol_id=<?= $consol['id'];?>',
            success: function (res) {
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.reload();
                    });
                }else{
                    $.messager.alert('Tips', res.msg, 'info');
                }
                ajaxLoadEnd();
                is_submit = false;
            },
            error: function () {
                ajaxLoadEnd();
                is_submit = false;
                $.messager.alert('Tips', 'error');
            }
        });
    }

    function per_alert() {
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/api/check_mbl?consol_id=<?= $consol['id'];?>',
            success: function (res) {
                if(res.code == 0){
                    //获取成功后，打开邮件发送页面
                    $('#subject').textbox('setValue', res.mail.subject);
                    $('#body').val(res.mail.body);
                    $('#send_email').window('open');
                }else{
                    $.messager.alert('Tips', res.msg);
                }
                ajaxLoadEnd();
                is_submit = false;
            },
            error: function () {
                ajaxLoadEnd();
                is_submit = false;
                $.messager.alert('Tips', 'error');
            }
        });
    }

    function save_send_email(){
        if(is_submit){
            return;
        }
        ajaxLoading();
        is_submit = true;
        $('#send_email_form').form('submit', {
            url: '/email/send_email',
            onSubmit: function () {
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                    is_submit = false;
                }
                return validate;
            },
            success: function (res_json) {
                var res;
                try{
                    res = $.parseJSON(res_json);
                }catch(e){
                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误');
                }else{
                    $.messager.alert('Tips', res.msg);
                    if(res.code == 0){
                        $('#send_email').dialog('close');
                    }else{

                    }
                }
                ajaxLoadEnd();
                is_submit = false;
            }
        });
    }
    
    function logwing_si_message(action){
        ajaxLoading();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/bsc_message/create_message/<?= $consol['id'];?>/LOGWING/SI/' + action,
            success: function (res) {
                ajaxLoadEnd();
                $('.reload_message_logwing_si').attr('disabled', false);
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.reload();
                    });
                }else{
                    $.messager.alert('Tips', res.msg, 'info');
                }
            },
            error: function () {
                ajaxLoadEnd();
                $('.reload_message_logwing_si').attr('disabled', false);
                $.messager.alert('Tips', 'error');
            }
        });
    }
    
    function ams_message(action){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/bsc_message/create_message/<?= $consol['id'];?>/EFREIGHT/AMS/' + action,
            onBeforeLoad:function(){
                ajaxLoading();
            },
            success: function (res) {
                ajaxLoadEnd();
                $('.reload_message_ams').attr('disabled', false);
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.reload();
                    });
                }else{
                    $.messager.alert('Tips', res.msg, 'info');
                }
            },
            error: function () {
                ajaxLoadEnd();
                $('.reload_message_ams').attr('disabled', false);
                $.messager.alert('Tips', 'error');
            }
        });
    }
    
    function emf_message(action){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/bsc_message/create_message/<?= $consol['id'];?>/EFREIGHT/E-MANIFEST/' + action,
            onBeforeLoad:function(){
                ajaxLoading();
            },
            success: function (res) {
                ajaxLoadEnd();
                $('.reload_message_emf').attr('disabled', false);
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.reload();
                    });
                }else{
                    $.messager.alert('Tips', res.msg, 'info');
                }
            },
            error: function () {
                ajaxLoadEnd();
                $('.reload_message_emf').attr('disabled', false);
                $.messager.alert('Tips', 'error');
            }
        });
    }
    
    function aci_message(action){
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/bsc_message/create_message/<?= $consol['id'];?>/EFREIGHT/ACI/' + action,
            onBeforeLoad:function(){
                ajaxLoading();
            },
            success: function (res) {
                ajaxLoadEnd();
                $('.reload_message_ams').attr('disabled', false);
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.reload();
                    });
                }else{
                    $.messager.alert('Tips', res.msg, 'info');
                }
            },
            error: function () {
                ajaxLoadEnd();
                $('.reload_message_ams').attr('disabled', false);
                $.messager.alert('Tips', 'error');
            }
        });
    }
    $(function () {
        $('.reload_message').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/bsc_message/create_message/<?= $consol['id'];?>/ESL/SO/' + action,
                onBeforeLoad:function(){
                    ajaxLoading();
                },
                success: function (res) {
                    ajaxLoadEnd();
                    $('.reload_message').attr('disabled', false);
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg, 'info', function () {
                            location.reload();
                        });
                    }else{
                        $.messager.alert('Tips', res.msg, 'info');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $('.reload_message').attr('disabled', false);
                    $.messager.alert('Tips', 'error');
                }
            });
        });
        
        $('.reload_message_kmtc').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/bsc_message/create_message/<?= $consol['id'];?>/KMTC/SO/' + action,
                onBeforeLoad:function(){
                    ajaxLoading();
                },
                success: function (res) {
                    ajaxLoadEnd();
                    $('.reload_message_kmtc').attr('disabled', false);
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg, 'info', function () {
                            location.reload();
                        });
                    }else{
                        $.messager.alert('Tips', res.msg, 'info');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $('.reload_message_kmtc').attr('disabled', false);
                    $.messager.alert('Tips', 'error');
                }
            });
        });

        $('.reload_message2').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/bsc_message/create_message/<?= $consol['id'];?>/SINOTRANS/SO/' + action,
                onBeforeLoad:function(){
                    ajaxLoading();
                },
                success: function (res) {
                    ajaxLoadEnd();
                    $('.reload_message2').attr('disabled', false);
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg, 'info', function () {
                            location.reload();
                        });
                    }else{
                        $.messager.alert('Tips', res.msg, 'info');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $('.reload_message2').attr('disabled', false);
                    $.messager.alert('Tips', 'error');
                }
            });
        });
        
        $('.reload_message_logwing').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/bsc_message/create_message/<?= $consol['id'];?>/LOGWING/SO/' + action,
                onBeforeLoad:function(){
                    ajaxLoading();
                },
                success: function (res) {
                    ajaxLoadEnd();
                    $('.reload_message_logwing').attr('disabled', false);
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg, 'info', function () {
                            location.reload();
                        });
                    }else{
                        $.messager.alert('Tips', res.msg, 'info');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $('.reload_message_logwing').attr('disabled', false);
                    $.messager.alert('Tips', 'error');
                }
            });
        });
        
        $('.reload_message_logwing_si').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');
            //生成前加一个填写参数的页面
            $.messager.confirm('确认对话框', '是否填写额外参数后提交？', function(r){
                if (r){
                    $('#ams_data').window('open');
                    $("#ams_data_iframe").attr('src', '/biz_consol_ext?id=<?= $consol['id'];?>&ext_type=SI&type=consol');
                }else{
                    logwing_si_message(action);
                }
            });
        });
        
        $('.reload_message3').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');

            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/bsc_message/create_message/<?= $consol['id'];?>/yitong/MANIFEST/' + action,
                onBeforeLoad:function(){
                    ajaxLoading();
                },
                success: function (res) {
                    ajaxLoadEnd();
                    $('.reload_message3').attr('disabled', false);
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg, 'info', function () {
                            window.open('/bsc_message/file_read/' + res.data.id);
                            location.reload();
                        });
                    }else if(res.code == 444){
                        // GET方式示例，其它方式修改相关参数即可
                        var form = $('<form></form>').attr('action','/other/ts_text').attr('method','POST').attr('target', '_blank');
                        var json_data = {};
                        json_data['text'] = res.text;
                        json_data['ts_str[]'] = res.ts_str;
                        json_data['msg'] = res.msg;
                        $.each(json_data, function (index, item) {
                            if(typeof item === 'string'){
                                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                            }else{
                                $.each(item, function(index2, item2){
                                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                                })
                            }
                        });
                
                        form.appendTo('body').submit().remove();
                    }else{
                        $.messager.alert('Tips', res.msg, 'info');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $('.reload_message3').attr('disabled', false);
                    $.messager.alert('Tips', 'error');
                }
            });
        });

        $('.reload_message_yitong_iftcps').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');

            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/bsc_message/create_message/<?= $consol['id'];?>/yitong/IFTCPS/' + action,
                onBeforeLoad:function(){
                    ajaxLoading();
                },
                success: function (res) {
                    ajaxLoadEnd();
                    $('.reload_message_yitong_iftcps').attr('disabled', false);
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg, 'info', function () {
                            // window.open('/bsc_message/file_read/' + res.data.id);
                            location.reload();
                        });
                    }else if(res.code == 444){
                        // GET方式示例，其它方式修改相关参数即可
                        var form = $('<form></form>').attr('action','/other/ts_text').attr('method','POST').attr('target', '_blank');
                        var json_data = {};
                        json_data['text'] = res.text;
                        json_data['ts_str[]'] = res.ts_str;
                        json_data['msg'] = res.msg;
                        $.each(json_data, function (index, item) {
                            if(typeof item === 'string'){
                                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                            }else{
                                $.each(item, function(index2, item2){
                                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                                })
                            }
                        });

                        form.appendTo('body').submit().remove();
                    }else{
                        $.messager.alert('Tips', res.msg, 'info');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $('.reload_message_yitong_iftcps').attr('disabled', false);
                    $.messager.alert('Tips', 'error');
                }
            });
        });
        
        $('.reload_message_ams').click(function () {
            // // $(this).attr('disabled', true);
            var action = $(this).attr('action');
            // //ams 生成前加一个填写参数的页面
            // $.messager.confirm('确认对话框', '是否填写额外参数后提交？', function(r){
            //     if (r){
            //         $('#ams_data').window('open');
            //         $("#ams_data_iframe").attr('src', '/biz_consol_ext?id=<?= $consol['id'];?>&ext_type=AMS&type=consol');
            //     }else{
            //         ams_message(action);
            //     }
            // });
            $.messager.confirm('确认对话框', '是否填写额外参数后提交？', function(r){
                if (r){
                    window.open('/bsc_message/message_form/consol/<?= $consol['id'];?>/AMS/EFREIGHT');
                }else{
                    ams_message(action);
                }
            });
           
        });
        
        $('.reload_message_emf').click(function () {
            // // $(this).attr('disabled', true);
            var action = $(this).attr('action');
            $.messager.confirm('确认对话框', '是否填写额外参数后提交？', function(r){
                if (r){
                    window.open('/bsc_message/message_form/consol/<?= $consol['id'];?>/E-MANIFEST/EFREIGHT/' + action);
                }else{
                    emf_message(action);
                }
            });
           
        });
        
        $('.reload_message_aci').click(function () {
            // $(this).attr('disabled', true);
            var action = $(this).attr('action');
            //ams 生成前加一个填写参数的页面
            $.messager.confirm('确认对话框', '是否填写额外参数后提交？', function(r){
                if (r){
                    window.open('/bsc_message/message_form/consol/<?= $consol['id'];?>/ACI/EFREIGHT/' + action);
                    // $('#ams_data').window('open');
                    // $("#ams_data_iframe").attr('src', '/biz_consol_ext?id=<?= $consol['id'];?>&ext_type=ACI&type=consol');
                }else{
                    aci_message(action);
                }
            });
            
        });

        $('.send').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');
            var receiver = $(this).attr('receiver');
            var id = $(this).attr('mid');
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/bsc_message/send_message/' + id + '/' + action + '/' + receiver,
                onBeforeLoad:function(){
                    ajaxLoading();
                },
                success: function (res) {
                    ajaxLoadEnd();
                    $('.send').attr('disabled', false);
                    if(res.code == 0){
                        $.messager.alert('Tips', res.msg, 'info', function () {
                            location.reload();
                        });
                    }else{
                        $.messager.alert('Tips', res.msg, 'info');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $('.send').attr('disabled', false);
                    $.messager.alert('Tips', 'error');
                }

            });
        });
        
        $('.get_out').click(function () {
            $(this).attr('disabled', true);
            var action = $(this).attr('action');
            var receiver = $(this).attr('receiver');
            var id = $(this).attr('mid');
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/bsc_message/logwing_ftp_read_out/' + id,
                onBeforeLoad:function(){
                    ajaxLoading();
                },
                success: function (res) {
                    ajaxLoadEnd();
                    $('.get_out').attr('disabled', false);
                    if(res.code == 0){
                        $.messager.alert('回执内容', res.data);
                    }else{
                        $.messager.alert('Tips', res.msg, 'info');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $('.get_out').attr('disabled', false);
                    $.messager.alert('Tips', 'error');
                }

            });
        });
        
        //查看报文
        $('.read_message').click(function () {
            var id = $(this).attr('mid');
            window.open('/bsc_message/file_read/' + id);
        });

    });
</script>

<!--2021-05-26 17:30:00 市场群 张星莹因为操作发EDI全是错的，所以要求限制为前段（订舱）和市场可发送-->
<!--2022-07-20 由于高丽要发esl 这里加了操作角色 -->
<?php if(is_admin() || in_array('marketing', get_session('user_role')) || in_array('booking', get_session('user_role')) || in_array('operator', get_session('user_role'))){?>
<fieldset>
    <legend>ESL SO发送</legend>
    <?php if(empty($ESL)){ ?>
        <button class="reload_message" action="insert">点击生成报文</button>
        <span style="color:red;"> 有了提单号以后 任何更改请邮件发送更正单 不要重新发送EDI</span>
    <?php }else{?>
        <?php if($ESL['receiver'] == 'ESL'){?>
            <a href="/upload/message/ESL_EDI/SO<?= $ESL['file_path'];?>" download="<?= $consol['job_no'];?>.txt">下载报文</a> |
             <a class="read_message" mid="<?= $ESL['id'];?>" href='javascript:void(0)'>查看报文</a>
            <button class="reload_message" action="insert">重新生成报文</button>
            <button class="reload_message" action="update">重新生成修改报文</button>
            <span style="color:red;"> 有了提单号以后 任何更改请邮件发送更正单 不要重新发送EDI</span>
            <?php /*if($ESL['status'] < 1){*/?>
            
            发送情况:
            <?php if($ESL['status'] == 0){?>
                未发送
            <?php }else if($ESL['status'] == 1){
                echo $ESL['send_time'];
            ?>
                发送成功
            <?php }else if($ESL['status'] == 2){?>
                发送修改成功
            <?php }?>
            <?php /*}*/?>
            <br />
           
            <button class="send" action="FTP" mid="<?= $ESL['id'];?>" receiver="<?= $ESL['receiver'];?>">发送报文</button>
            
        <?php }?>
    <?php }?>
</fieldset>
<br><br>
<?php } ?>

<fieldset>
    <legend>KMTC SO发送</legend>
    <?php if(empty($KMTC)){ ?>
        <button class="reload_message_kmtc" action="insert">点击生成报文</button>
        <span style="color:red;"> 有了提单号以后 任何更改请邮件发送更正单 不要重新发送EDI</span>
    <?php }else{?>
        <?php if($KMTC['receiver'] == 'KMTC'){?>
            <!--<a href="/upload/message/KMTC_EDI/SO<?= $KMTC['file_path'];?>" download="<?= $consol['job_no'];?>.txt">点击下载报文</a>-->
            <button class="reload_message_kmtc" action="insert">重新生成报文</button>
            <button class="reload_message_kmtc" action="update">重新生成修改报文</button>
            <span style="color:red;"> 有了提单号以后 任何更改请邮件发送更正单 不要重新发送EDI</span>
            <?php /*if($KMTC['status'] < 1){*/?>
            
            发送情况:
            <?php if($KMTC['status'] == 0){?>
                未发送
            <?php }else if($KMTC['status'] == 1){
                echo $KMTC['send_time'];
            ?>
                发送成功
            <?php }else if($KMTC['status'] == 2){?>
                发送修改成功
            <?php }?>
            <?php /*}*/?>
            <br />
            <button class="read_message" mid="<?= $KMTC['id'];?>">查看报文</button>
            <button class="send" action="FTP" mid="<?= $KMTC['id'];?>" receiver="<?= $KMTC['receiver'];?>">发送报文</button>
        <?php }?>
    <?php }?>
</fieldset>
<br><br>

<fieldset>
    <legend>外运 SO发送</legend>
    <?php if(empty($SINOTRANS)){ ?>
        <button class="reload_message2" action="insert">点击生成报文</button>
    <?php }else{?>
        <?php if($SINOTRANS['receiver'] == 'SINOTRANS'){?>
            <!--<a href="/upload/message/SINOTRANS_EDI/SO<?= $SINOTRANS['file_path'];?>" download="<?= $consol['job_no'];?>.xml">点击下载报文</a>-->
            <button class="reload_message2" action="insert">重新生成报文</button>
            <button class="reload_message2" action="update">重新生成修改报文</button>
            <br />
            发送情况:
            <?php if($SINOTRANS['status'] == 0){?>
                未发送
            <?php }else if($SINOTRANS['status'] == 1){
                echo $SINOTRANS['send_time'];
            ?>
                发送成功
            <?php }else if($SINOTRANS['status'] == 2){?>
                发送修改成功
            <?php }?>
            <br />
            <button class="read_message" mid="<?= $SINOTRANS['id'];?>">查看报文</button>
            <button class="send" action="API" mid="<?= $SINOTRANS['id'];?>" receiver="<?= $SINOTRANS['receiver'];?>">发送报文</button>
        <?php }?>
    <?php }?>
</fieldset>

<br><br>

<fieldset>
    <legend>乐域订舱 SO发送</legend>
    <?php if(empty($LOGWING)){ ?>
        <button class="reload_message_logwing" action="insert">点击生成报文</button>
    <?php }else{?>
        <?php if($LOGWING['receiver'] == 'LOGWING'){?>
            <!--<a href="/upload/message/LOGWING_EDI/SO<?= $LOGWING['file_path'];?>" download="<?= $consol['job_no'];?>.txt">点击下载报文</a>-->
            <button class="reload_message_logwing" action="insert">重新生成报文</button> 
            <br />
            发送情况:
            <?php if($LOGWING['status'] == 0){?>
                未发送
            <?php }else if($LOGWING['status'] == 1){
                echo $LOGWING['send_time'];
            ?>
                发送成功
            <?php }else if($LOGWING['status'] == 2){?>
                发送修改成功
            <?php }?>
            <br />
            <button class="read_message" mid="<?= $LOGWING['id'];?>">查看报文</button>
            <button class="send" action="FTP" mid="<?= $LOGWING['id'];?>" receiver="<?= $LOGWING['receiver'];?>">发送报文</button>
            <button class="get_out" action="FTP" receiver="<?= $LOGWING['receiver'];?>" mid="<?= $LOGWING['id'];?>">获取回执</button>
        <?php }?>
    <?php }?>
</fieldset>
<br><br>
<fieldset>
    <legend>乐域订舱 SI发送</legend>
    <?php if(empty($LOGWING_SI)){ ?>
        <button class="reload_message_logwing_si" action="insert">点击生成报文</button>
    <?php }else{?>
        <?php if($LOGWING_SI['receiver'] == 'LOGWING'){?>
            <!--<a href="/upload/message/LOGWING_EDI/SO<?= $LOGWING_SI['file_path'];?>" download="<?= $consol['job_no'];?>.txt">点击下载报文</a>-->
            <button class="reload_message_logwing_si" action="insert">重新生成报文</button> 
            <br />
            发送情况:
            <?php if($LOGWING_SI['status'] == 0){?>
                未发送
            <?php }else if($LOGWING_SI['status'] == 1){
                echo $LOGWING_SI['send_time'];
            ?>
                发送成功
            <?php }else if($LOGWING_SI['status'] == 2){?>
                发送修改成功
            <?php }?>
            <br />
            <button class="read_message" mid="<?= $LOGWING_SI['id'];?>">查看报文</button>
            <button class="send" action="FTP" mid="<?= $LOGWING_SI['id'];?>" receiver="<?= $LOGWING_SI['receiver'];?>">发送报文</button>
            <button class="get_out" action="FTP" receiver="<?= $LOGWING_SI['receiver'];?>" mid="<?= $LOGWING_SI['id'];?>">获取回执</button>
        <?php }?>
    <?php }?>
</fieldset>
<br><br>

<fieldset style="display:flex;">
    <legend>亿通舱单发送</legend>
    <div>
        <?php if(empty($yitong)){ ?>
            <button class="reload_message3" action="insert">点击生成报文</button>
        <?php }else{?>
            <?php if($yitong['receiver'] == 'yitong'){?>
                <!--<a href="http://china.leagueshipping.com/upload/message/yitong_EDI/MANIFEST<?= $yitong['file_path'];?>"  target="_blank">预览报文</a>-->
                <button class="reload_message3" action="insert">重新生成报文</button>
                <br />
                发送情况:
                <?php if($yitong['status'] == 0){?>
                    未发送
                <?php }else if($yitong['status'] == 1){
                    echo $yitong['send_time'];?>
                    发送成功
                <?php }else if($yitong['status'] == 2){?>
                    发送修改成功
                <?php }?>
                <br />
                <button class="read_message" mid="<?= $yitong['id'];?>" >查看报文</button>
                <button class="send" action="API" mid="<?= $yitong['id'];?>" receiver="<?= $yitong['receiver'];?>">发送报文</button>
            <?php }?>
        <?php }?>
    </div>
    <div style="margin-left: 30px;">
        IFTCPS
        <?php if(empty($yitong_iftcps)){ ?>
            <button class="reload_message_yitong_iftcps" action="insert">点击生成报文</button>
        <?php }else{?>
            <?php if($yitong_iftcps['receiver'] == 'yitong'){?>
                <a href="/download?file_path=/upload/message/yitong_EDI/IFTCPS<?= $yitong_iftcps['file_path'];?>">下载报文</a> |
                <button class="reload_message_yitong_iftcps" action="insert">重新生成报文</button>
                <br />
                发送情况:
                <?php if($yitong_iftcps['status'] == 0){?>
                    未发送
                <?php }else if($yitong_iftcps['status'] == 1){
                    echo $yitong_iftcps['send_time'];?>
                    发送成功
                <?php }else if($yitong_iftcps['status'] == 2){?>
                    发送修改成功
                <?php }?>
                <br />
                <button class="read_message" mid="<?= $yitong_iftcps['id'];?>">查看报文</button>
            <?php }?>
        <?php }?>
    </div>
</fieldset>

<br><br>

<fieldset>
    <legend>AMS发送</legend>
    <?php if(empty($ams)){ ?>
        <button class="reload_message_ams" action="insert">点击生成报文</button>
    <?php }else{?>
        <?php if($ams['receiver'] == 'EFREIGHT'){?>
            <!--<a href="http://china.leagueshipping.com/upload/message/yitong_EDI/MANIFEST<?= $ams['file_path'];?>"  target="_blank">预览报文</a>-->
            <button class="reload_message_ams" action="insert">再编辑报文</button>
            <br />
            发送情况:
            <?php if($ams['status'] == 0){?>
                未发送
            <?php }else if($ams['status'] == 1){
               echo $ams['send_time'];?>
                发送成功
            <?php }else if($ams['status'] == 2){?>
                发送修改成功
            <?php }?>
            <br />
            <button class="read_message" mid="<?= $ams['id'];?>" >查看报文</button>
            <button class="send" action="API" mid="<?= $ams['id'];?>" receiver="<?= $ams['receiver'];?>">发送报文</button>
        <?php }?>
    <?php }?>
</fieldset>

<br><br>

<fieldset>
    <legend>ACI发送</legend>
    <?php if(empty($aci)){ ?>
        <button class="reload_message_aci" action="insert">点击生成报文</button>
    <?php }else{?>
        <?php if($aci['receiver'] == 'EFREIGHT'){?>
            <!--<a href="http://china.leagueshipping.com/upload/message/yitong_EDI/MANIFEST<?= $ams['file_path'];?>"  target="_blank">预览报文</a>-->
            <button class="reload_message_aci" action="insert">再编辑报文</button>
            <!--<button class="reload_message_aci" action="update">重新生成修改报文</button>-->
            <br />
            发送情况:
            <?php if($aci['status'] == 0){?>
                未发送
            <?php }else if($aci['status'] == 1){
               echo $aci['send_time'];?>
                发送成功
            <?php }else if($aci['status'] == 2){?>
                发送修改成功
            <?php }?>
            <br />
            <button class="read_message" mid="<?= $aci['id'];?>" >查看报文</button>
            <button class="send" action="API" mid="<?= $aci['id'];?>" receiver="<?= $aci['receiver'];?>">发送报文</button>
        <?php }?>
    <?php }?>
</fieldset>

<br><br>

<fieldset>
    <legend>E-MANIFEST发送</legend>
    <?php if(empty($emf)){ ?>
        <button class="reload_message_emf" action="insert">点击生成报文</button>
    <?php }else{?>
        <?php if($emf['receiver'] == 'EFREIGHT'){?>
            <button class="reload_message_emf" action="insert">再编辑报文</button>
            <!--<button class="reload_message_emf" action="update">重新生成修改报文</button>-->
            <br />
            发送情况:
            <?php if($emf['status'] == 0){?>
                未发送
            <?php }else if($emf['status'] == 1){
               echo $emf['send_time'];?>
                发送成功
            <?php }else if($emf['status'] == 2){?>
                发送修改成功
            <?php }?>
            <br />
            <button class="read_message" mid="<?= $emf['id'];?>" >查看报文</button>
            <button class="send" action="API" mid="<?= $emf['id'];?>" receiver="<?= $emf['receiver'];?>">发送报文</button>
        <?php }?>
    <?php }?>
</fieldset>

<br><br>

<div id="ams_data" class="easyui-window" title="<?= lang('ams_data');?>" closed="true" style="width:850px;height:455px;padding:5px;" data-options="onResize:function(width,height){
        var this_css = {width: width - 40,height: height - 70};
        $('#ams_data_iframe').css(this_css);
    }">
    <iframe id="ams_data_iframe" style="width:810px;height:375px;padding:5px;border:0px;">
    
    </iframe>
</div>
<div id="send_email" class="easyui-window" title="<?= lang('email');?>推送" closed="true" style="width:550px;height:455px;padding:5px;">
    提单号:<span id="send_mail_carrier_ref"><?= $consol['carrier_ref'];?></span>
    <form id="send_email_form" method="POST">
        <table>
            <tr>
                <td>收件人:</td>
                <td>
                    <input name="receivers" id="receivers" class="easyui-textbox" style="width: 350px;">
                </td>
            </tr>
            <tr>
                <td>主题:</td>
                <td>
                    <input name="subject" id="subject" class="easyui-textbox" style="width:350px;" readonly>
                </td>
            </tr>
            <tr>
                <td>内容:</td>
                <td>
                    <textarea name="body" id="body" style="width:350px;height:250px"></textarea>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <a href="javascript:void(0);" id="cpb" onclick="save_send_email()" class="easyui-linkbutton"><?= lang('发送');?></a><a target="_blank" href="http://wenda.leagueshipping.com/?/question/264">help</a>
                </td>
                <td></td>
            </tr>
        </table>
    </form>
</div>