<script type="text/javascript">
    function doSearch(){
        $('#tt').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val() ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val() ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val()
        });
        $('#chaxun').window('close');
    }

    function add() {
        $('#dlg').dialog('open');
        $('#form').form('clear');
        $('#fild_id').css('display');
        $('#upload_id').combogrid('grid').datagrid('reload', '/bsc_upload/get_files/template');
        url = '/bsc_template/add_data';
    }

    function set_template() {
        var rows = $('#tt').datagrid('getSelections');
        if (rows.length < 1){
            $.messager.alert("<?= lang('提示');?>", '<?= lang('请选择配置对象！');?>');
            return;
        }
        document.getElementById("tanchuang").src='/Bsc_template_match/index/?template_id='+rows[0].id;
        $('#gcenter').window('open');
    }

    function update() {
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                id: $('#id').val(),
                file_name: $('#file_name').textbox('getValue'),
                data_table: $('#data_table').combobox('getValue'),
                upload_id: $('#upload_id').combobox('getValue'),
                file_template: $('#file_template').val(),
                users: $('#users').combobox('getValues').join(','),
                groups: $('#groups').combobox('getValues').join(','),
                parameter: $('#parameter').textbox('getValue'),
            },
            dataType: 'json',
            success: function (data) {
                //转义JSON为对象
                $.messager.alert("<?= lang('提示');?>", '<?= lang('保存成功');?>');
                $('#tt').edatagrid('reload');
            },
            error: function (xhr, status, error) {
                $.messager.alert("<?= lang('提示');?>", xhr.responseText); //xhr.responseText
            }
        });
    }

    //上传文件操作
    function UploadFile(file_ctrlname, guid_ctrlname, div_files) {
        var value = $("#" + file_ctrlname).filebox('getValue');
        var files = $("#" + file_ctrlname).next().find('input[type=file]')[0].files;

        var guid = $("#" + guid_ctrlname).val();
        if (value && files[0]) {
            //构建一个FormData存储复杂对象
            var formData = new FormData();
            formData.append("id_no", $('#id').val());
            formData.append('biz_table', 'bsc_template');
            formData.append('type_name', 'template');
            formData.append('file', files[0]);

            $.ajax({
                url: '/bsc_upload/file_upload?up_location=local', //单文件上传
                type: 'POST',
                processData: false,
                contentType: false,
                data: formData,
                success: function (json) {
                    //转义JSON为对象
                    var res = $.parseJSON(json);
                    if (res.code == 0) {
                        $('#upload_id').combogrid('grid').datagrid('reload');
                        $.messager.alert("<?= lang('提示');?>", "<?= lang('上传成功');?>");
                    } else {
                        $.messager.alert("<?= lang('提示');?>", "<?= lang('上传失败');?>"); //xhr.responseText
                    }
                },
                error: function (xhr, status, error) {
                    $.messager.alert("<?= lang('提示');?>", xhr.responseText); //xhr.responseText
                }
            });
        }
    }

    function file_template_for(val,row,index){
        return '<a href="javascript:window.open(\'/upload'+ row.file_template_download +'\');">'+val+'</a>';
    }

    var url = '';
    $(function () {
        $('#tt').edatagrid({
            url: '/bsc_template/get_data/',
            destroyUrl: '/bsc_template/delete_data'
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
            onDblClickRow: function (index, row) {
                $('#dlg').dialog('open');
                $('#form').form('load', row);
                url = '/bsc_template/update_data';
                $('#upload_id').combogrid('grid').datagrid('reload','/bsc_upload/get_files/template/bsc_template/' + row.id);
            },
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });

        $('#up_file_bt').click(function () {
            UploadFile("file_upload", "AttachGUID", "div_files");
        });
    });

</script>
<div id="gcenter" class="easyui-window" title="<?= lang('弹窗');?>"  closed="true" style="width:1150px;height:600px;" data-options="right:'1px',top:'110px'">
    <iframe scrolling="auto" frameborder="0" id="tanchuang" style="width:100%;height:100%;"> </iframe>
</div>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th field="id" width="80" sortable="true"><?= lang('ID');?></th>
        <th field="order" width="80" sortable="true"><?= lang('排序');?></th>
        <th field="upload_id" width="60" ><?= lang('文件编号');?></th>
        <th field="file_template" width="550" data-options="formatter:file_template_for"><?= lang('文件路由');?></th>
        <th field="file_name" width="190" ><?= lang('文件名称');?></th>
        <th field="data_table" width="100" ><?= lang('表名');?></th>
        <th field="parameter" width="100" ><?= lang('参数');?></th>
        <th field="groups" width="300" data-options="formatter:function(value, row, index){
            if(value.length > 30){
                return value.substring(0, 30) + '...';
            }else{
                return value;
            }
        }"><?= lang('可查看部门');?></th>
        <th field="users" width="300" data-options="formatter:function(value, row, index){
            if(value.length > 30){
                return value.substring(0, 30) + '...';
            }else{
                return value;
            }
        }"><?= lang('可查看用户');?></th>
        <th field="create_time" width="150" sortable="true"><?= lang('创建时间');?></th>
    </tr>
    </thead>
</table>
<div id="set_template"></div>
<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
                   onclick="javascript:add();"><?= lang('新增'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
                   onclick="javascript:$('#tt').edatagrid('destroyRow')"><?= lang('删除'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true"
                   onclick="javascript:$('#tt').edatagrid('saveRow')"><?= lang('保存'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true"
                   onclick="javascript:set_template();"><?= lang('配置模板');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true"
                   onclick="javascript:$('#chaxun').window('open');"><?= lang('查询'); ?></a>
            </td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="<?= lang('查询');?>" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                <?= lang('查询');?> 1
            </td>
            <td align="right">
                <select name="f1"  id="f1"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>" . lang($item) . "</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1"  id="s1"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v1" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('查询');?> 2
            </td>
            <td align="right">
                <select name="f2"  id="f2"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>" . lang($item) . "</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2"  id="s2"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v2" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('查询');?> 3
            </td>
            <td align="right">
                <select name="f3"  id="f3"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>" . lang($item) . "</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s3"  id="s3"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v3" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;"><?= lang('查询');?></a>
</div>

<div id="dlg" class="easyui-dialog" style="width: auto;height: 500px;"
     data-options="title:'edit', resizable: true, closed:true, modal:true, border:'thin', tools:'#dlg-buttons'">
    <form id="form" method="post">
        <table>
            <input type="hidden" name="id" id="id">
            <tr>
                <td><?= lang('file_name'); ?></td>
                <td>
                    <input id="file_name" name="file_name" class="easyui-textbox" data-options="required:true,"
                           style="width: 175px;">
                </td>
                <td><?= lang('data_table'); ?></td>
                <td>
                    <select name="data_table" id="data_table" class="easyui-combobox" style="width: 175px;"
                            data-options="
                        valueField:'value',
                        textField:'name',
                        url:'/bsc_dict/get_option/template_data_table',
                        required:true,
                        ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('Binding'); ?></td>
                <td>
                    <select name="upload_id" id="upload_id" class="easyui-combogrid" style="width: 175px;"
                            data-options="

                        panelWidth:450,
                        idField:'id',
                        textField:'file_name',
                        url:'/bsc_upload/get_files/template',
                        columns:[[
                            {field:'id',title:'id',width:60},
                            {field:'file_name',title:'file_name',width:300},
                        ]],
                        onSelect: function(index,row){
                            $('#file_template').val(row.file_name);
                        },
                        ">
                    </select>
                    <br>
                    <input id="file_template" name="file_template" type="hidden">
                </td>
                <td><?= lang('Upload'); ?></td>
                <td>
                    <input class="easyui-filebox" name="file" id="file_upload"
                           data-options="" style="width:175px">
                    <a href="javascript:;" id="up_file_bt"><?= lang('Upload'); ?></a>
                </td>
            </tr>
            <tr>
                <td>
                    <?= lang('groups');?>
                </td>
                <td>
                    <select class="easyui-combobox" name="groups" id="groups" style="width: 175px;" data-options="
                        multiple:true,
                        valueField:'code',
                        textField:'name',
                        url:'/bsc_group/get_option',
                    ">
                    </select>
                </td>
                <td>
                    <?= lang('users');?>
                </td>
                <td>
                    <select class="easyui-combobox" name="users" id="users" style="width: 175px;" data-options="
                        multiple:true,
                    	valueField:'id',
						textField:'name',
						url:'/bsc_user/get_data_role',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <?= lang('参数');?>
                </td>
                <td>
                    <input id="parameter" name="parameter" class="easyui-textbox" data-options="required:true,"
                           style="width: 175px;">
                </td>
            </tr>
        </table>
    </form>
</div>

<div id="dlg-buttons">
    <a href="javascript:update();" class="save" iconCls="icon-save" plain="true"></a>
</div>
