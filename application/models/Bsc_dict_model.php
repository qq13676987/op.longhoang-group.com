<?php

class bsc_dict_model extends CI_Model{

    public function __construct(){

        $this->load->database();
        $this->url = get_system_type();
    }

    public function get($name='',$catalog='',$sort='id',$order='desc'){
        $this->db->like('name', $name);
        $this->db->where('catalog', $catalog);
        $this->db->order_by($sort,$order);
        $query = $this->db->get('bsc_dict');
        return $query->result_array();
    }

    public function get_one($field = '', $value = '', $catalog = '')
    {
        $where = array();
//        $this->db->where($field, $value);
        $where[] = "{$field} = '{$value}'";
//        $this->db->where('catalog', $catalog);
        $where[] = "catalog = '{$catalog}'";
        $where = join(' and ', $where);
        $this->db->where($where . " and url = '{$this->url}'");
        $query = $this->db->get('bsc_dict');
        $row = $query->row_array();
        if(empty($row)){
            $this->db->where($where . " and url = ''");
            $query = $this->db->get('bsc_dict');
            $row = $query->row_array();
        }
        return $row;
    }
    
    public function get_one_eq($field = '', $value = '', $catalog = '')
    {
        $where = array();
        $where[] = "{$field} = '{$value}'";
        $where[] = "catalog = '{$catalog}'";
        $where = join(' and ',  $where);
//        $this->db->where($field, $value);
//        $this->db->where('catalog', $catalog);
        $this->db->where($where . " and url = '{$this->url}'");
        $query = $this->db->get('bsc_dict');
        $row =  $query->row_array();
        if(empty($row)){
            $this->db->where($where . " and url = ''");
            $query = $this->db->get('bsc_dict');
            $row =  $query->row_array();
        }
        return $row;
    }
    
    public function get_by_ext1_one($field = '', $value = '', $catalog = '', $ext1 = '')
    {
        $where = array();
//        $this->db->like($field, $value);
        $where[] = "{$field} like '%$value%'";
        if($ext1 != ''){
//            $this->db->where('ext1', $ext1);
            $where[] = "ext1 = '$ext1'";
        }else{
//            $this->db->where('ext1 is null');
            $where[] = "ext1 is null";
        }
//        $this->db->where('catalog', $catalog);
        $where[] = "catalog = '{$catalog}'";
        $where = join(' and ', $where);
        $this->db->where($where . " and url = '{$this->url}'");
        $query = $this->db->get('bsc_dict');
        $row = $query->row_array();
        if(empty($row)){
            $this->db->where($where . " and url = ''");
            $query = $this->db->get('bsc_dict');
            $row = $query->row_array();
        }

        return $row;
    }
    
    public function get_by_ext1_eq_one($field = '', $value = '', $catalog = '', $ext1 = '')
    {
        $where = array();
        $where[] = "{$field} = '{$value}'";
//        $this->db->where($field, $value);
        if($ext1 != ''){
//            $this->db->where('ext1', $ext1);
            $where[] = "ext1 = '{$ext1}'";
        }else{
            $where[] = "ext1 is null";
//            $this->db->where('ext1 is null');
        }
        $where[] = "catalog = '{$catalog}'";
        $where = join(' and ', $where);
        $this->db->where($where . " and url = '{$catalog}'");
        $query = $this->db->get('bsc_dict');
        $row =  $query->row_array();
        if(empty($row)){
            $this->db->where($where . " and url = ''");
            $query = $this->db->get('bsc_dict');
            $row =  $query->row_array();
        }

        return $row;
    }
    
    public function no_role_get($where="" ,$sort="id",$order="desc"){
        if($where !="")	$this->db->where($where);
        $this->db->order_by($sort,$order);
        $query = $this->db->get('bsc_dict');
        return $query->result_array();
    }

	public function get_option($catalog, $where = ""){
        $this_where = $where;
        if(!empty($this_where)) $this_where .= " and ";
        $this_where .= "catalog = '{$catalog}'";

        $this->db->where($this_where . " and url = '{$this->url}'");
        $this->db->select('name,value,orderby,ext1,ext2');
        $this->db->order_by('orderby','asc');
        $query = $this->db->get('bsc_dict');
        $rows = $query->result_array();
        //如果没查到, 返回常规的
        if(empty($rows)){
            $this->db->where($this_where . " and url = ''");
            $this->db->select('name,value,orderby,ext1,ext2');
            $this->db->order_by('orderby','asc');
            $query = $this->db->get('bsc_dict');

            $rows = $query->result_array();
        }
        return $rows;
    } 
    
    public function get_option_where($catalog, $where = '')
    {
        if ($where != '') $this->db->where($where);
        $this->db->where('catalog', $catalog);
        $this->db->select('name,value,ext1');
        $this->db->order_by('orderby', 'asc');
        $query = $this->db->get('bsc_dict');
        return $query->result_array();
    }

    public function total_where($catalog = '',$where='')
    {
        if ($where != '') $this->db->where($where);
        $this->db->where('catalog', $catalog);
        $this->db->from('bsc_dict');
        return $this->db->count_all_results('');
    }
    
	public function total($name='',$catalog=''){ 
        $this->db->like('name', $name); 
        $this->db->where('catalog', $catalog); 
        $this->db->from('bsc_dict'); 
        return  $this->db->count_all_results(''); 
    }

	public function save($data=array()){
		$this->db->insert('bsc_dict', $data);
		return $this->db->insert_id();     
    }

	public function update($id='',$data=array()){
		$this->db->where('id', $id);
		$this->db->update('bsc_dict', $data); 
		return $id;     
    }   

    public function mdelete($id=''){
		$this->db->where('id', $id);
		$this->db->delete('bsc_dict'); 
    }
}