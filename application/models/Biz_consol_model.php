<?php
   class biz_consol_model extends CI_Model{
       public $admin_field = array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'sales');
    //   public $admin_field = array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'finance');
       public $field_all = array(
           array("job_no", "Job No", "94", "2", '1'),
           array("trans_carrier", "trans_carrier", "77", "9", '1', 'port info'),    //第0是字段，第1是字段name，第2是宽度，第3待确定，第4是锁到第几级disable掉，第5是用户权限管理的树节点
           array("trans_carrier_second", "trans_carrier_second", "77", "9", '1', 'port info'),
           array("carrier_ref", "carrier_ref", "140", "9", '1', 'port info'),
           array("vessel", "vessel", "140", "9", '1', 'port info'),
           array("voyage", "voyage", "60", "9", '1', 'port info'),
           array("voyage_inner", "voyage_inner", "0", "9", '1', 'port info'),
           array("booking_ETD", "booking_ETD", "87", "9", '1', 'ship info'),
           array("trans_origin", "trans_origin", "60", "9", '1', 'port info'),
           array("trans_origin_name", "trans_origin_name", "0", "9", '1', 'port info'),
           array("trans_discharge", "trans_discharge", "60", "9", '1', 'port info'),
           array("trans_discharge_name", "trans_discharge_name", "0", "9", '1', 'port info'),
           array("trans_destination_inner", "trans_destination_inner", "60", "9", '1', 'port info'),
           array("trans_destination", "trans_destination", "60", "9", '1', 'port info'),
           array("trans_destination_terminal", "trans_destination_terminal", "60", "9", '1', 'port info'),
           array("trans_destination_name", "trans_destination_name", "0", "9", '1', 'port info'),
           array("trans_receipt", "trans_receipt", "0", "9", '1', 'port info'),
           array("trans_receipt_name", "trans_receipt_name", "0", "9", '1', 'port info'),
           array("creditor", "creditor", "122", "9", '1'),
           array("carrier_agent", "carrier_agent", "0", "9", '1'),
           array("agent_company", "agent Company", "116", "3", '1', 'agent info'),
           array("status", "status", "50", "0", '1'),
           array("biz_type", "biz_type", "60", "0", '1'),//业务类型
           array("created_by", "created_by", "72", "10", '1'),
           array("closing_date", "closing_date", "101", "9", '1'),
           array("close_ens_date", "close_ens_date", "0", "9", '1'),
           array("carrier_cutoff", "carrier_cutoff", "101", "9", '1'),
           array("carrier_cutoff_text", "carrier_cutoff_text", "101", "9", '1'),
           array("cy_open", "cy_open", "0", "9", '1', 'ship info'),
           array("cy_close", "cy_close", "0", "9", '1', 'ship info'),
           array("trans_ETD", "trans_ETD", "60", "9", '1', 'ship info'),
           array("trans_ATD", "trans_ATD", "60", "9", '1', 'ship info'),
           array("trans_ETA", "trans_ETA", "60", "9", '1', 'ship info'),
           array("trans_ATA", "trans_ATA", "60", "9", '1', 'ship info'),
           array("report_date", "report_date", "60", "9", '1', 'ship info'),
           array("feeder_voyage", "feeder_voyage", "60", "9", '1', 'port info'),
           array("feeder", "feeder", "60", "9", '1', 'port info'),
           array("trans_type", "trans_type", "60", "9", '1', 'port info'),
           array("sailing_code", "sailing_code", "60", "9", '1', 'port info'),//航线代码
           array("sailing_area", "sailing_area", "60", "9", '1', 'port info'),//航线区域
           array("id", "ID", "0", "1", '1'),
           array("agent_address", "agent_Address", "0", "4", '1', 'agent info'),
           array("agent_contact", "agent_contact", "0", "5", '1', 'agent info'),
           array("agent_telephone", "agent_telephone", "0", "6", '1', 'agent info'),
           array("agent_email", "agent_email", "0", "0", '1', 'agent info'),
           array("agent_code", "agent Code", "88", "0", '1', 'agent info'),
           array("shipper_company", "shipper_company", "0", "7", '1', 'booking info'),
           array("shipper_address", "shipper_address", "0", "8", '1', 'booking info'),
           array("shipper_contact", "shipper_contact", "0", "9", '1', 'booking info'),
           array("shipper_telephone", "shipper_telephone", "0", "9", '1', 'booking info'),
           array("shipper_email", "shipper_email", "0", "9", '1', 'booking info'),
           array("consignee_company", "consignee_company", "120", "9", '1', 'booking info'),
           array("consignee_address", "consignee_address", "0", "9", '1', 'booking info'),
           array("consignee_contact", "consignee_contact", "0", "9", '1', 'booking info'),
           array("consignee_telephone", "consignee_contact", "0", "9", '1', 'booking info'),
           array("consignee_email", "consignee_email", "0", "9", '1', 'booking info'),
           array("notify_company", "notify_company", "0", "9", '1', 'booking info'),
           array("notify_address", "notify_address", "0", "9", '1', 'booking info'),
           array("notify_contact", "notify_contact", "0", "9", '1', 'booking info'),
           array("notify_telephone", "notify_contact", "0", "9", '1', 'booking info'),
           array("notify_email", "notify_email", "0", "9", '1', 'booking info'),
           array("container_owner", "container_owner", "80", "9", '1'),
           array("payment", "payment", "80", "9", '1'),
           array("payment_third", "payment_third", "0", "9", '1'),
           array("agent_ref", "agent_ref", "80", "9", '1'),
           array("description", "description", "0", "9", '1'),
           array("description_cn", "description_cn", "0", "9", '1'),
           array("mark_nums", "mark_nums", "0", "9", '1'),
           array("created_time", "created_time", "150", "10", '1'),
           array("updated_by", "updated_by", "150", "10", '1'),
           array("updated_time", "updated_time", "150", "11", '1'),
           array("warehouse_code", "warehouse_code", "0", "0", '1'),
           array("hbl_type", "hbl_type", "0", "0", '1'),
           array("dingcangshenqing", "dingcangshenqing", "0", "0", '1', 'step info'),
           array("dingcangwancheng", "dingcangwancheng", "0", "0", '1', 'step info'),
           array("xiangyijingang", "xiangyijingang", "0", "0", '1', 'step info'),
           array("yifangdan", "yifangdan", "0", "0", '1', 'step info'),
           array("matoufangxing", "matoufangxing", "0", "0", '1', 'step info'),
           array("chuanyiqihang", "chuanyiqihang", "0", "0", '1', 'step info'),
           array("trans_mode", "trans_mode", "80", "9", '1', 'booking info2'),
           array("LCL_type", "LCL_type", "80", "9", '1', 'booking info2'),
           array("trans_tool", "trans_tool", "80", "9", '1'),
           array("hs_code", "hs_code", "0", "0", '1', 'booking info2'),
           array("box_info", "box_info", "150", "0", '1', 'booking info2'),
           array("free_svr", "free_svr", "0", "0", '1', 'freight rates'),
           array("AGR_no", "AGR_no", "0", "0", '1', 'freight rates'),
           array("requirements", "requirements", "0", "0", '1', 'booking info2'),
           array("floor_price", "floor_price", "0", "0", '1', 'freight rates'),
           array("freight_rate", "freight_rate", "0", "0", '1', 'freight rates'),
           array("freight_affirmed", "freight_affirmed", "0", "0", '1', 'freight rates'),
           array("booking_confirmation", "booking_confirmation", "0", "0", '1', 'freight rates'),
           array("AP_code", "AP_code", "0", "0", '1'),
           array("AP_remark", "AP_remark", "0", "0", '1'),//11
           array("customer_booking", "customer_booking", "0", "0", '1'),
           array("trans_discharge_code", "trans_discharge_code", "0", "0", '1'),
           array("goods_type", "goods_type", "60", "9", '1', 'booking cargo'),
           array("good_outers", "good_outers", "60", "9", '1', 'booking cargo'),
           array("good_outers_unit", "good_outers_unit", "60", "9", '1', 'booking cargo'),
           array("good_weight", "good_weight", "60", "9", '1', 'booking cargo'),
           array("good_weight_unit", "good_weight_unit", "60", "9", '1', 'booking cargo'),
           array("good_volume", "good_volume", "60", "9", '1', 'booking cargo'),
           array("good_volume_unit", "good_volume_unit", "60", "9", '1', 'booking cargo'),
           array("sign", "sign", "0", "0", '1'),
           array("remark", "remark", "0", "0", '1'),
           array("document_remark", "document_remark", "0", "0", '1'),
           array("quotation_id", "quotation_id", "0", "0", '1'),
           array("master_consol", "master_consol", "0", "0", '1'),
           array("price_flag", "price_flag", "0", "0", '1'),
           array("price_flag_msg", "price_flag_msg", "0", "0", '1'),
           array("louzhuang", "louzhuang", "0", "0", '1'),
           array("imo_number", "imo_number", "0", "0", '1'),
           array("des_ETA", "des_ETA", "0", "0", '1'),
           array("pay_buy_flag", "pay_buy_flag", "0", "0", '1'),
           array("yupeiyifang", "yupeiyifang", "0", "0", '1'),
           array("trans_term", "trans_term", "0", "0", '1'),
           array("publish_flag", "publish_flag", "0", "0", '1'),
           array("hide_flag", "hide_flag", "0", "0", '1'),
           array("cancel_msg", "cancel_msg", "0", "0", '1'),
           array("release_type", "release_type", "0", "0", '1'),
           array("operator_si", "operator_si", "0", "0", '1'),
           array("document_si", "document_si", "0", "0", '1'),
        //   array("pa_send_time", "pa_send_time", "0", "0", '1'),
        //   array("pa_file_is_update", "pa_file_is_update", "0", "0", '1'),
        //   array("pa_mbl", "pa_mbl", "0", "0", '1'),
        //   array("pa_hbl", "pa_hbl", "0", "0", '1'),
        //   array("pa_bill_sell", "pa_bill_sell", "0", "0", '1'),
        //   array("pa_bill_cost", "pa_bill_cost", "0", "0", '1'),
           array("vgm_confirmation", "vgm_confirmation", "0", "0", '1'),
           array("gaipei_old_id", "gaipei_old_id", "0", "0", '1'),
           array("late_operator_si", "late_operator_si", "0", "0", '1'), //晚截操作SI
        //   array("late_document_si", "late_document_si", "0", "0", '1'), // 晚截截单SI

           array("gs_soc_tixianghao", "gs_soc_tixianghao", "0", "0", '1'), // 提箱号
           array("gs_soc_tixiangdian", "gs_soc_tixiangdian", "0", "0", '1'), // 提箱点
           array("ex_rate_date", "ex_rate_date", "0", "0", '1'), // 汇率日期
//        array("truck_date", "truck_date", "111", "6", '2'),
//        array("truck_company", "truck_company", "0", "6", '2'),
//        array("truck_address", "truck_address", "0", "6", '2'),
//        array("truck_contact", "truck_contact", "0", "6", '2'),
//        array("truck_telephone", "truck_telephone", "0", "6", '2'),
//        array("truck_remark", "truck_remark", "0", "6", '2'),
       );
    
       public function __construct(){
            $this->load->database();
        }
       public function set_admin_field($array){
           return $this->admin_field = $array;
       }
        public function get_by_id($id=0){
            $this->db->where('id', $id);
            $query = $this->db->get('biz_consol');
            return $query->row_array();
        } 
        public function get_one($f="",$v=""){
            $this->db->where($f, $v);
            $query = $this->db->get('biz_consol');
            return $query->row_array();
        }
       /**
        * 获取一条拼接了duty的数据
        */
       public function get_duty_one($where=""){
           $this->db->where($where);
           $query = $this->db->get('biz_consol');
           $row = $query->row_array();

           if(empty($row)) return $row;

           //查询duty的数据进行整理
           return array_merge($query->row_array(), Model('biz_duty_model')->get_old_by_new('biz_consol', $row['id']));
       }
        public function get_where_one($where = ''){
           if($where != '')$this->db->where($where);
           $query = $this->db->get('biz_consol');
           return $query->row_array();
        }
        public function get($where="" ,$sort="id",$order="desc"){ 
			if($where !="")	$this->db->where($where, null, false);	
            $this->db->order_by($sort,$order);
            $query = $this->db->get('biz_consol');
            return $query->result_array();
        }

       /**
        * 新版查询duty_new表使用的查询
        * @param string $where
        * @param string $sort
        * @param string $order
        * @param bool $unmerge
        * @return mixed
        */
       public function get_v3($where="" ,$sort="biz_consol.id",$order="desc"){
           if($where !="") $this->db->where($where, null, false);
           $this->db->order_by($sort,$order);
           //限制只能看自己的权限
           $this->db->where(Model('bsc_user_role_model')->get_field_where('biz_consol'));
           $query = $this->db->get('biz_consol');
           return $query->result_array();
       }
        
        public function export_get($where="" ,$sort="id",$order="desc"){ 
			if($where !="")	$this->db->where($where);	
            $this->db->order_by($sort,$order);

            $field = array();
            $this->load->model('bsc_user_role_model');
            $rs = $this->bsc_user_role_model->get_field_where($this->admin_field, 'biz_consol', $field);
            $role_where = $rs['role_where'];
            
            $this->db->where($role_where);
            $this->db->join('biz_duty_new', "biz_duty_new.id_no = biz_consol.id and biz_duty_new.biz_table='biz_consol'", 'left');
            $query = $this->db->get('biz_consol');
            return $query->result_array();
        }

        public function no_role_get($where="" ,$sort="id",$order="desc"){
            if($where !="")	$this->db->where($where, null, false);
            $this->db->order_by($sort,$order, false);
            $query = $this->db->get('biz_consol');
            return $query->result_array();
        }
        
        public function no_role_group_get($where = "", $group_field = '', $select_field = ""){
            if($group_field != ''){
                if($select_field == "") $this->db->select($group_field);
                $this->db->group_by($group_field);
            }
            return $this->no_role_get($where);
        }
		
		public function no_role_total($where=""){  
            if($where !="")	$this->db->where($where, null, false);
            $this->db->from('biz_consol'); 
            
            return  $this->db->count_all_results('');
        }  	
        
		public function total($where=""){  
            if($where !="")	$this->db->where($where, null, false);

            $this->load->model('bsc_user_role_model');
            $rs = $this->bsc_user_role_model->get_field_where($this->admin_field, 'biz_consol');
            $role_where = $rs['role_where'];

            $this->db->where($role_where);
            $this->db->join('biz_duty_new', "biz_duty_new.id_no = biz_consol.id and biz_duty_new.biz_table='biz_consol'", 'left');
            $this->db->from('biz_consol');
            return  $this->db->count_all_results('');
            
        }  	           
        
        public function save($data=array()){
            $data["created_time"] = date('y-m-d H:i:s',time());
            $data["updated_time"] = date('y-m-d H:i:s',time());
            $data["created_by"] =  $this->session->userdata('id');
            $data["updated_by"] =  $this->session->userdata('id');
            $data["created_group"] = implode(',', $this->session->userdata('group') );

            $this->db->insert('biz_consol', $data);
            return $this->db->insert_id();
        } 
        
        public function update($id='',$data=array()){
                $this->db->where('id', $id);
				$data["updated_by"] =  $this->session->userdata('id');
				$data["updated_time"] = date('Y-m-d H:i:s',time());
                $this->db->update('biz_consol', $data);
				return $id;
        }   
        
        public function mdelete($id=''){
				// delete fee
				//$this->load->model('biz_fee_model'); 
				//$this->biz_fee_model->delete_by_id($id); 
				// delete consol
                $this->db->where('id', $id);
                $this->db->delete('biz_consol');
        }
         
        /**
         * 获取市场模块主视图的统计
         */
        public function get_vessel_view_count($sailing_code,$trans_origin, $vessel, $voyage, $trans_carrier, $creditor){
            $where = array();
            if($vessel != 'null') $where[] = "vessel = '$vessel'";  
            else $where[] = "vessel is null";
            
            if($voyage != 'null') $where[] = "voyage = '$voyage'";      
            else $where[] = "voyage is null";
            
            if($trans_carrier != 'null') $where[] = "trans_carrier = '$trans_carrier'";
            else $where[] = "trans_carrier is null";
            
            if($creditor != 'null') $where[] = "creditor = '$creditor'";
            else $where[] = "creditor is null";
            
            if($trans_origin != 'null') $where[] = "trans_origin = '$trans_origin'";
            else $where[] = "trans_origin is null";

            if($sailing_code != 'null') $where[] = "sailing_code = '$sailing_code'";
            else $where[] = "sailing_code is null";
            
            $where = join(' and ' , $where);
            
            $this->db->where($where);
            $this->db->select("(count(case when (biz_consol.lock_lv=3 and biz_consol.status='normal') then 1 else null end) / count(case when (biz_consol.status='normal') then 1 else null end)) as normal_lock_c3" . //normal锁C3的总数
            ",count(case when (biz_consol.status='cancel') then 1 else null end) / count(1) as cancel_rate" . //normal总数
            '');
            $this->db->group_by("trans_carrier,vessel,voyage,creditor");
            $query = $this->db->get('biz_consol');
            
            return $query->row_array();
        }

       /**
        * 获取箱东模块主视图的统计
        * @param $vessel
        * @param $voyage
        * @param $trans_carrier
        * @param $creditor
        * @return mixed
        */
       public function get_consol_kml_vessel_view_count($vessel, $voyage, $trans_carrier, $creditor, $trans_destination_inner){
       	exit("0312");
           $where = array();

           if($vessel != 'null') $where[] = "vessel = '$vessel'";
           else $where[] = "vessel is null";

           if($voyage != 'null') $where[] = "voyage = '$voyage'";
           else $where[] = "voyage is null";

           if($trans_carrier != 'null') $where[] = "trans_carrier = '$trans_carrier'";
           else $where[] = "trans_carrier is null";

           if($creditor != 'null') $where[] = "creditor = '$creditor'";
           else $where[] = "creditor is null";

           if($trans_destination_inner != 'null') $where[] = "trans_destination = '$trans_destination_inner'";
           else $where[] = "trans_destination = ''";

           $where = join(' and ' , $where);
           $this->db->where($where, null, false);
           $this->db->select("count(*) as count" . //normal总数
               '');
           $this->db->group_by("trans_carrier,vessel,voyage,creditor");
           $query = $this->db->get('biz_consol');

           return $query->row_array();
       }

        /**
         * 获取截单模块主视图的统计
         */
        public function get_consol_si_vessel_view_count($vessel, $voyage, $trans_carrier, $creditor){
            $where = array();
            if($vessel != 'null') $where[] = "vessel = '$vessel'";  
            else $where[] = "vessel is null";
            
            if($voyage != 'null') $where[] = "voyage = '$voyage'";      
            else $where[] = "voyage is null";
            
            if($trans_carrier != 'null') $where[] = "trans_carrier = '$trans_carrier'";
            else $where[] = "trans_carrier is null";
            
            if($creditor != 'null') $where[] = "creditor = '$creditor'";
            else $where[] = "creditor is null";
            
            $where = join(' and ' , $where);
            
            $this->db->where($where);
            $this->db->select("(count(case when (biz_consol.operator_si!='0' and biz_consol.status='normal') then 1 else null end) / count(case when (biz_consol.status='normal') then 1 else null end)) as operator_si_rate" . //normal操作SI完成的总数
            ",(count(case when (biz_consol.document_si!='0' and biz_consol.status='normal') then 1 else null end) / count(case when (biz_consol.status='normal') then 1 else null end)) as document_si_rate" . //截单SI完成率
            ",(count(case when (biz_consol.vgm_confirmation!='0' and biz_consol.status='normal') then 1 else null end) / count(case when (biz_consol.status='normal') then 1 else null end)) as vgm_rate" . //vgm
            '');
            $this->db->group_by("trans_carrier,vessel,voyage,creditor");
            $query = $this->db->get('biz_consol');
            
            return $query->row_array();
        }

       /**
        * 获取操作模块主视图的统计
        */
       public function get_consol_operator_vessel_view_count($vessel, $voyage, $trans_carrier, $creditor){
           $where = array();
           if($vessel != 'null') $where[] = "vessel = '$vessel'";
           else $where[] = "vessel is null";

           if($voyage != 'null') $where[] = "voyage = '$voyage'";
           else $where[] = "voyage is null";

           if($trans_carrier != 'null') $where[] = "trans_carrier = '$trans_carrier'";
           else $where[] = "trans_carrier is null";

           if($creditor != 'null') $where[] = "creditor = '$creditor'";
           else $where[] = "creditor is null";

           $where = join(' and ' , $where);

           $this->db->where($where);
           $this->db->select(
               "(count(case when (biz_consol.status='cancel') then 1 else null end) / count(case when (biz_consol.status='normal' or biz_consol.status='cancel') then 1 else null end)) as cancel_rate" . //退关率
               ",(count(case when (biz_consol.yupeiyifang!='0' and biz_consol.status='normal') then 1 else null end) / count(case when (biz_consol.status='normal') then 1 else null end)) as yupei_rate" . //预配
               ",(count(case when (biz_consol.vgm_confirmation!='0' and biz_consol.status='normal') then 1 else null end) / count(case when (biz_consol.status='normal') then 1 else null end)) as vgm_rate" . //vgm
               ""
           );
           $this->db->group_by("trans_carrier,vessel,voyage,creditor");
           $query = $this->db->get('biz_consol');

           return $query->row_array();
       }

        /**
        * 获取市场模块的默认表头配置, 因为很多地方要用,所以我这里直接搞个默认值了
        */
       public function getMarketTableField(){

           $field_all = $this->field_all;
           foreach ($this->admin_field as $val){
               $field_all[] = array($val, $val, "0");
           }

           $market_default = array(
               array('field' => 'job_no', 'width' => 80),
               array('field' => 'carrier_ref', 'width' => 100),
               array('field' => 'box_info', 'width' => 100),
               array('field' => 'AP_code', 'width' => 100),
               array('field' => 'trans_origin_name', 'width' => 150),
               array('field' => 'trans_destination_name', 'width' => 150),
               array('field' => 'trans_discharge_code', 'width' => 60),
               array('field' => 'closing_date', 'width' => 120),
               array('field' => 'carrier_cutoff', 'width' => 120),
               array('field' => 'sales', 'width' => 60),
               array('field' => 'operator', 'width' => 60),
               array('field' => 'marketing', 'width' => 60),
               array('field' => 'booking', 'width' => 60),
               array('field' => 'louzhuang', 'width' => 60),
               array('field' => 'yupeiyifang', 'width' => 60),
               array('field' => 'yifangdan', 'width' => 60),
               array('field' => 'document_si', 'width' => 60),
            //   array('field' => 'haiaguanfangxing', 'width' => 80),
               array('field' => 'matoufangxing', 'width' => 80),
               array('field' => 'chuanyiqihang', 'width' => 80),
               array('field' => 'status', 'width' => 80),
               array('field' => 'remark', 'width' => 110),
               array('field' => 'usd_bill_info', 'width' => 400),
               array('field' => 'cny_bill_info', 'width' => 400),
           );

           $result = array();
           //首先将上面的填充,然后再将剩余的填充
           foreach ($market_default as $field){
               $result[] = array($field['field'], $field['field'], $field['width']);
           }

           $isset_field = array_column($market_default, 'field');
           foreach ($field_all as $field){
               //上面没有的才再次添加
               $field[2] = 0;
               if(!in_array($field[0], $isset_field)) $result[] = $field;
           }

           return $result;
       }

       public function mbatch_update($data, $wfield = 'id')
       {
           $this->db->update_batch('biz_consol', $data, $wfield);
           return $this->db->affected_rows();
       }
   }
