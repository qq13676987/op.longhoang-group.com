<title><?= lang('编辑联系人');?></title>
<body style="padding: 10px 20px;">
<div style="width: 100%">
    <form id="myform" name="myform" method="post">
        <input type="hidden" name="id" id="id" value="<?=isset($_GET['id'])?(int)$_GET['id']:'0';?>">
        <table>
            <tr style="height: 30px;">
                <td style="width: 60px;"><?= lang('角色');?>：</td>
                <td>
                    <select id="role" name="role" readonly="true" class="easyui-combobox" style="width: 280px; height: 25px;"
                            data-options="
								">
                    </select>
                </td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('姓名');?>：</td>
                <td><input class="easyui-textbox" type="text" required="required" name="name" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('手机号');?>：</td>
                <td><input class="easyui-textbox" type="text" name="telephone" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('座机号');?>：</td>
                <td><input class="easyui-textbox" type="text" name="telephone2" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('邮箱');?>：</td>
                <td><input class="easyui-textbox" type="text" required="required" name="email" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('地址');?>：</td>
                <td><input class="easyui-textbox" type="text" name="address" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('QQ');?>：</td>
                <td><input class="easyui-textbox" type="text" name="live_chat" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('职位');?>：</td>
                <td>
                    <input class="easyui-combobox" type="text" name="position" value="" style="width: 280px; height: 25px;"
                           data-options="
                           valueField: 'value',
                           textField: 'name',
                           url:'/bsc_dict/get_option/contact_position',"
                    />
                </td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('前台权限');?>：</td>
                <td>
                    <input class="easyui-combobox menu" type="text" id="menu" name="menu[]" value="" style="width: 280px; height: 25px;"
                           data-options="
                           multiple:true,
                           valueField: 'value',
                           textField: 'value',
                           value:<?php echo str_replace('"', "'", json_encode($data['menu'], JSON_UNESCAPED_UNICODE));?>,
                           url:'/biz_client_contact/get_menu',
                           "/>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('前台密码');?>：</td>
                <td><input class="easyui-textbox" type="text" name="password" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('航线');?>：</td>
                <td>
                    <input class="easyui-combobox" type="text" id="sailing" name="sailing[]" value="" style="width: 280px; height: 25px;"
                           data-options="
                           multiple:true,
                           valueField: 'value',
                           textField: 'name',
                           value:<?php echo str_replace('"', "'", json_encode(array_values($data['sailing']), JSON_UNESCAPED_UNICODE));?>,
                           url:'/bsc_dict/get_option/export_sailing',"
                    />
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('备注');?>：</td>
                <td><input class="easyui-textbox" type="text" name="remark" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td></td>
                <td align="center">
                    <input type="hidden" name="client_code" value="">
                    <input type="hidden" name="is_submit" value="1">
                    <a href="javascript:void(0);" id="save_data" lay-submit lay-filter="*" class="easyui-linkbutton" style="width: 280px;height:30px;margin-top: 10px;"><?= lang('保存联系人');?></a>
                </td>
            </tr>
        </table>
    </form>
</div>
<link type="text/css" href="/inc/css/wangeditor/style.css" rel="stylesheet">
<script type="text/javascript" src="/inc/js/wangeditor/index.js" ></script>
<script type="text/javascript" src="/inc/js/wangeditor/dist/index.js" ></script>
<script>
    $(function () {
        $('#role').combobox({
            required:true,
            multiple:false,
            editable:false,
            valueField:'value',
            textField:'value',
            url:'/bsc_dict/get_option/role',
            value:'',
        })

        //初始化默认值
        $('#myform').form('load', <?=json_encode($data);?>);
        //异步写入数据
        $('#save_data').click(function () {
            var field = {};
            $.each($('#myform').serializeArray(), function (index, item) {
                field[item.name] = item.value;
            });
            field.menu = $('#menu').combobox('getValues').join(',');
            field.sailing = $('#sailing').combobox('getValues').join(',');
            $.ajax({
                url: '/biz_client_contact/edit_crm/?id=<?=$data['id'];?>',
                type: 'POST',
                data: field,
                dataType: 'json',
                success: function (data) {
                    if (data.code == 1){
                        //刷新父页面的列表控件
                        parent.$('#tt').edatagrid('reload');

                        $.messager.show({
                            title:'<?= lang('系统消息');?>',
                            msg:'<?= lang('保存成功。');?>',
                            timeout:2000,
                            showType:null,
                            style:{
                                right:'',
                                bottom:''
                            }
                        });

                        setTimeout(function () {
                            //关闭当前弹窗
                            parent.$('#_window').window('close');
                        }, 2000);

                    }else{
                        $.messager.alert("<?= lang('提示');?>", '<?= lang('保存失败：');?>'+data.msg, 'error');
                    }
                },
                error: function (xhr, status, error) {
                    $.messager.alert("<?= lang('提示');?>", "<?= lang('操作失败');?>"); //xhr.responseText
                }
            });
        });
    })
</script>
</body>
</html>