<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= lang('查找往来单位');?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css" media="all">
</head>
<div style="position: fixed; z-index: 999; top: 0px; padding-top: 10px; background: white; height: 70px; width: 100%">
    <form action="/biz_client/client_sales" method="get">
        <table >
            <tr>
                <td>
                    <div class="layui-form-item" style="margin-bottom: 5px;">
                        <label class="layui-form-label"><?= lang('公司名称');?></label>
                        <div class="layui-input-block">
                            <input type="text" name="company_name" id="company_name" value="<?= $company_name;?>" placeholder="<?= lang('请输入关键词');?>" style="width: 350px;" class="layui-input">
                        </div>
                    </div>
                </td>
                <td>
                    <div class="layui-form-item" style="margin-bottom: 5px;">
                        <div class="layui-input-block" style="margin-left: 20px;">
                            <button type="submit" class="layui-btn" style="width: 100px;"><?= lang('查询');?></button>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <span style="color:#ff7e0c; margin-left: 110px;"><?= lang('Tips:可关联查询,如"上海 国际" 等于同时查上海和国际');?></span>
    </form>
</div>
<table class="layui-table" width="600px" style="margin-top: 80px;">
    <colgroup>
        <col width="">
        <col width="">
        <col width="">
        <col width="">
    </colgroup>
    <thead>
    <tr>
        <th><?= lang('company_name'); ?></th>
        <th><?= lang('当前销售'); ?></th>
        <th><?= lang('shipment未合作天数'); ?></th>
        <th><?= lang('标签'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    if ($total > 0):
        foreach ($rows as $row) { ?>
            <tr>
                <td>
                    <?= $row['company_name']; ?>
                    <?php
                    if ($row['client_level'] == -1){
                        echo "<br><font color='red'>【" . lang('黑名单') . "】</font><font style='color: #fd810b'>{$row['client_level_remark']}</font>";
                    }
                    ?>
                </td>
                <td><?= $row['sales_ids_names']; ?></td>
                <td><?= $row['not_traded_days']; ?></td>
                <td><?= $row['tag']; ?></td>
            </tr>
            <?php
        }
    else:
        ?>
        <tr bgcolor="#FFFFFF">
            <td colspan="4" style="color: red; height: 35px; line-height: 35px; text-align: center"><?= lang('按当前关键词未找到任何符合的结果！');?></td>
        </tr>
    <?php
    endif;
    if ($total > 10):
        ?>
        <tr bgcolor="#FFFFFF">
            <td colspan="4" style="color: red; height: 35px; line-height: 30px; text-align: center"><?= lang('找到 {total} 条符合条件的记录，当前列出最新 10 条，请缩小查询范围', array('total' => $total));?></td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>