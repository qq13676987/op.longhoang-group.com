<?php
// check the doc whether closed
$disable = "";
?>
<style type="text/css">
    .hide{
        display: none;
    }
    .cost{
        color: red;
    }
    .cost a:link{
        color: red;
    }
    .sell{
        color: blue;
    }
    .sell a:link{
        color: blue;
    }
    tr.sell{
    <?php if($id_type == 'consol'){?>
        display: none;
    <?php }?>
    }
    .cp_table1{
        width: 100%;
    }
    .cp_table2{
        width: 100%;
        text-align: left;
        border-collapse:collapse;
        border:none;
    }
    .cp_table2 th{
        width: 50%;
        border: 1px solid black;
    }
    .cp_table2 td{
        border: 1px solid black;
    }
    .lock{
        display: inline-block;
        width: 16px;
        height: 16px;
        opacity: 0.6;
        filter: alpha(opacity=60);
        margin: 0 0 0 2px;
        vertical-align: top;
    }
</style>
<script type="text/javascript">
    $.fn.combobox.defaults.keyHandler.enter=function (e) {
        keydown($(this), e);

        $(this).combobox('hidePanel');
        return false;
    };

    var singleSelect = true;


    var url = '/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>';
    var filter = {};
    var ajax_data = {};
    function doSearch() {
        $('#tt').datagrid('load', {
            container_no: $('#container_no').val()
        });
    }
    function click_row (is_click=true, rowIndex){
        if(is_click){
            $('#tt').datagrid('selectRow', rowIndex);
            var row = $('#tt').datagrid('getSelected');
            $('#dlg').removeClass('hide');
            $.each(row, function (index,item) {
                if(item == '***') row[index] = '';
            })
            if(row.cost_id == null){
                return false;
            }
            var cost_is_edit = 'enable';
            if(row && row.cost_ban_handle){
                cost_is_edit = 'disable';
            }
            $('#cost').textbox(cost_is_edit);
            $('#cost_unit_price').textbox(cost_is_edit);
            $('#cost_number').textbox(cost_is_edit);
            $('#cost_amount').textbox(cost_is_edit);
            $('#cost_vat').textbox(cost_is_edit);
            $('#cost_vat_withhold').textbox(cost_is_edit);
            $('#creditor').textbox(cost_is_edit);
            $('#cost_remark').textbox(cost_is_edit);
            $('#cost_split_mode').textbox('readonly', true);

            $('#fm').form('load', row);
            url = '/biz_bill/update_data';

            $('#save').linkbutton('settext', '<?= lang('修改');?>');
        }
    }

    $.fn.linkbutton.methods.settext = function(jq,text){
        var btn = $(jq);
        var left = btn.find('.l-btn-left');
        if(text == '<?= lang('修改');?>'){
            $('#add').css('display', 'inline-block');
        }
        if(text == '<?= lang('保存');?>'){
            $('#add').css('display', 'none');
        }
        var text_target = btn.find('.l-btn-text');
        text_target.text(text);
    }

    $(function () {
        $('#add').css('display', 'none');

        $('#tt').edatagrid({
            url: '/biz_bill/get_data/<?php echo $id_type;?>/<?php echo $id_no;?><?php if($is_special == 1) echo '?is_special=1'?>',
            saveUrl: '/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>',
            updateUrl: '/biz_bill/update_data/',
            onClickRow: function (rowIndex) {
                click_row(true,rowIndex);
            },
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height() - 200,
            rowStyler: function(index,row){
            }
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });

        $('#fm').on('keydown', '.textbox-text', function (event) {
            var input = $(this).parent('span').siblings();
            keydown(input, event);
        });
        $('#fm').on('blur', '.textbox-text', function (event) {
            var input = $(this).parent('span').siblings();
            if(input.hasClass('easyui-combobox')){
                var valueField = input.combobox('options').valueField;
                var val = input.combobox('getValue');
                var allData = input.combobox('getData');
                var result = true;
                if(Object.keys(filter).length != 0 && filter.hasOwnProperty(val) && filter[val].length >= 1){
                    input.combobox('select', filter[val][0]);
                }else{
                    for (var i = 0; i < allData.length; i++) {
                        if (val == allData[i][valueField]) {
                            result = false;
                        }
                    }
                    if (result) {
                        input.combobox('clear');
                    }
                }
                filter = {};
            }
        })

        // var a1 = ajax_get('/biz_client/get_bill_client/<?= $id_type;?>/<?= $id_no;?>', 'client');
        var a1 = ajax_get('/biz_client/get_option/0/1', 'client');
        var a2 = ajax_get('/biz_client/get_option/wanglaikuan/1', 'wanglaikuan_client');
        $.when(a1,a2).done(function () {
            $('#creditor').combobox('loadData', ajax_data['client']);
        });

        $('.split_mode').click(function () {
            var val = $('.split_mode:checked').val();
            var row = $('#tt').datagrid('getSelected');
            var box_size = $('#consol_percent_box_size').combobox('getValue');
            $('.split_mode_tr').addClass('hide');
            if(val === '4'){
                $('.split_mode_4_tr').removeClass('hide');
                //当是per container时, 先填写当前分配的箱型, 再进行获取
                //如果有值直接分配
                if(box_size !== ''){
                    get_shipments_bill_percent(val,row.cost_id, box_size);
                }else{
                    $.messager.alert('<?= lang('提示');?>', '<?= lang('填写下方箱型后即可自动分配');?>')
                }
            }else{
                //除了情况4以外, 直接获取分成数据
                get_shipments_bill_percent(val,row.cost_id);
            }
        });
    });

    function get_shipments_bill_percent(split_mode,bill_id, box_size = ''){
        $.ajax({
            url: '/biz_bill/get_shipments_bill_percent/<?= $id_no;?>/' + split_mode + '/' + bill_id + '?box_size=' + box_size,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                var str = '';
                if(res.data.length !== 0){
                    $.each(res.data, function (index, item) {
                        var amount_str;
                        if(split_mode == 3){//自定义
                            amount_str = '<input type="text" class="diy_datas_amount" name="diy_datas[' + index + '][amount]" value="' + item.amount + '"/>';
                        }else{
                            amount_str = item.amount;
                        }
                        str += '<tr>';
                        str += '<td>' + item.job_no + '</td>';
                        str += '<td>' + amount_str + '</td>';
                        str += '</tr>';
                    });
                    $('#cpb').attr('onclick', 'save_consol_percent()');
                }else{
                    $('#cpb').attr('onclick', "alert('<?= lang('无法保存');?>')");
                    $.messager.alert('<?= lang('提示');?>', res.msg);
                }
                $('#shipment_consol_percent_info tbody').html(str);
            },error:function (e) {
                $.messager.alert('<?= lang('提示');?>', e.responseText);
            }
        });
    }

    function ajax_get(url,name) {
        return $.ajax({
            type:'POST',
            url:url,
            dataType:'json',
            success:function (res) {
                ajax_data[name] = res;
            },
            error:function () {
                // $.messager.alert('获取数据错误');
            }
        });
    }

    function keydown(this_input, event) {
        if(event.keyCode == 9 || event.keyCode == 13){
            var input = this_input;

            if(input.hasClass('easyui-combobox')){
                var valueField = input.combobox('options').valueField;
                var val = input.combobox('getValue');
                var allData = input.combobox('getData');
                var result = true;
                if(Object.keys(filter).length != 0 && filter.hasOwnProperty(val) && filter[val].length >= 1){
                    input.combobox('select', filter[val][0]);
                }else{
                    for (var i = 0; i < allData.length; i++) {
                        if (val == allData[i][valueField]) {
                            result = false;
                        }
                    }
                    if (result) {
                        input.combobox('clear');
                    }
                }
                filter = {};
            }
            if(input.hasClass('vat')){
                save();
                return false;
            }
            if(event.keyCode == 13){
                let formValue = $('#fm').serializeArray();
                $.each(formValue, function(index, item) {
                    if(item.name == input.prop('id')){
                        var inp = $('#' + formValue[(index+1)].name);
                        if(inp.hasClass('easyui-combobox')){
                            inp.combobox('textbox').focus();
                        }else if(inp.hasClass('easyui-textbox')){
                            inp.textbox('textbox').focus();
                        }else if(inp.hasClass('easyui-numberbox')){
                            inp.numberbox('textbox').focus(function () {
                                this.select();
                            }).focus();
                        }
                    }
                });
            }
        }
    }
    function add() {
        // $('#dlg').removeClass('hide');
        $('#charge').combobox('initValue');
        $('#charge_code').val('');
        $('#cost_unit_price').numberbox('initValue');
        $('#cost_number').numberbox('initValue');
        $('#cost_amount').numberbox('initValue');
        $('#cost_remark').textbox('initValue');


        cost_is_edit = 'enable';
        $('#cost').textbox(cost_is_edit);
        $('#cost_unit_price').textbox(cost_is_edit);
        $('#cost_number').textbox(cost_is_edit);
        $('#cost_amount').textbox(cost_is_edit);
        $('#cost_vat').textbox(cost_is_edit);
        $('#cost_vat_withhold').textbox(cost_is_edit);
        $('#creditor').textbox(cost_is_edit);
        $('#cost_remark').textbox(cost_is_edit);
        $('#cost_split_mode').textbox('readonly', false);
        url = '/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>';
        $('#save').linkbutton('settext', '<?= lang('新增');?>');
        $('#add').css('display', 'none');
    }

    var is_save = false;
    function save() {
        <?php if($lock_lv == 3 ){ ?>
        url = '/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>';
        <?php }else if($lock_lv > 0){?>
        return false;
        <?php } ?>
        if(url == ''){
            url = '/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>';
        }
        if(is_save){
            return;
        }
        ajaxLoading();
        is_save = true;
        $('#fm').form('submit', {
            url: url,
            onSubmit: function () {
                var creditor = $('#creditor').combobox('getValue');
                var validate = true;
                if($('#charge').combobox('getValue') === '' || $('#charge_code').val() == ''){
                    $.messager.alert('<?= lang('提示');?>', '<?= lang('charge');?>');
                    validate = false;
                }
                if($('#creditor').combobox('getValue') === ''){
                    $.messager.alert('<?= lang('提示');?>', '<?= lang('creditor');?>');
                    validate = false;
                }
                if(creditor != '' && $('#cost').combobox('getValue') != ''){
                    if($('#creditor').combobox('getValue') === ''){
                        $.messager.alert('<?= lang('提示');?>', '<?= lang('creditor');?>');
                        validate = false;
                    }
                    if($('#cost').combobox('getValue') === ''){
                        $.messager.alert('<?= lang('提示');?>', '<?= lang('cost');?>');
                        validate = false;
                    }
                    if($('#cost_unit_price').numberbox('getValue') === '' || $('#cost_number').numberbox('getValue') === '' || $('#cost_amount').numberbox('getValue') === ''){
                        $.messager.alert('<?= lang('提示');?>', '<?= lang('cost_number');?>');
                        validate = false;
                    }
                    if($('#cost_vat').numberbox('getValue') === ''){
                        $.messager.alert('<?= lang('提示');?>', '<?= lang('cost_vat');?>');
                        validate = false;
                    }
                    if($('#cost_vat_withhold').numberbox('getValue') === ''){
                        $.messager.alert('<?= lang('提示');?>', '<?= lang('cost_vat_withhold');?>');
                        validate = false;
                    }
					if($('#cost_payment_bank').combobox('getValue') === ''){
						$.messager.alert('<?= lang('提示');?>', '<?= lang('cost_account');?>');
                        validate = false;
                    }
                }
                if(!validate){
                    ajaxLoadEnd();
                    is_save = false;
                }
                return validate;
                // return $(this).form('validate');
            },
            success: function (result) {
                // $('#dlg').addClass('hide');
                var res = $.parseJSON(result);
                if(res.code == 0){
                    url = '/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>';
                    add();
                    $('#charge').combobox('textbox').focus();
                    $.messager.alert('<?= lang('提示');?>', res.msg);
                    $('#tt').datagrid('reload');
                }else{
                    $.messager.alert('<?= lang('提示');?>', res.msg);
                }
                is_save = false;
                ajaxLoadEnd();
            }
        });

    }

    function del(type) {
        var rows = $('#tt').datagrid('getSelections');
        var id = 0;
        if(rows.length > 0){
            if(rows.length <= 1){
                var row = rows[0];
                if(type == 'cost'){
                    id = row.cost_id;
                }else if(type == 'sell'){
                    id = row.sell_id;
                }else{
                    return false;
                }
                if(id == null){
                    $.messager.alert('<?= lang('提示')?>','<?= lang('不存在,无法删除');?>');
                    return false;
                }
                $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认删除？');?>', function(r){
                    if (r){
                        // 退出操作;
                        $.ajax({
                            type: 'POST',
                            url: '/biz_bill/delete_data<?php if($is_special == 1) echo '?is_special=1'?>',
                            data:{
                                id:id,
                            },
                            dataType: 'json',
                            success: function (res) {
                                if(res.code == 0){
                                    $.messager.alert('info：', res.msg);
                                    $('#tt').datagrid('reload');
                                    url='/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>';
                                    $('#save').linkbutton('settext', '<?= lang('保存');?>');
                                }else{
                                    $.messager.alert('info：', res.msg);
                                }
                            }
                        });
                    }
                });
            }else{
                var ids = [];
                var result = 0;
                var msg = '<?= lang('发生错误');?>';
                $.each(rows, function (index, item) {
                    if(type == 'cost'){
                        if(item.cost_invoice_id != 0){
                            result = 1;
                            msg = '<?= lang("费用名称为{charge}的成本已开票", array('charge' => "' + item.charge + '"));?>';
                            return false;
                        }
                        ids.push(item.cost_id);
                    }else if(type == 'sell'){
                        if(item.sell_invoice_id != 0){
                            result = 1;
                            msg = '<?= lang("费用名称为{charge}的卖价已开票", array('charge' => "' + item.charge + '"));?>';
                            return false;
                        }
                        ids.push(item.sell_id);
                    }
                });
                if(result == 1){
                    $.messager.alert('Tips', msg + ',无法删除');
                    return ;
                }
                ids = ids.join(',');
                $.messager.prompt('<?= lang('提示');?>', '<?= lang('请输入delete确认删除？');?>', function(r){
                    if (r === 'delete'){
                        // 退出操作;
                        $.ajax({
                            type: 'POST',
                            url: '/biz_bill/delete_datas',
                            data:{
                                ids:ids,
                            },
                            dataType: 'json',
                            success: function (res) {
                                if(res.code == 0){
                                    $.messager.alert('info：', res.msg);
                                    $('#tt').datagrid('reload').datagrid('clearSelections');
                                    url='/biz_bill/add_data/<?php echo $id_type;?>/<?php echo $id_no;?>';
                                }else{
                                    $.messager.alert('info：', res.msg);
                                }
                            }
                        });
                    }
                });
            }

        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }
    }

    function child_bill() {
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            $('#child_bill').window({
                title: 'window',
                width: 900,
                height: 450,
                cache: false,
                modal: true,
                onResize:function (width, height) {
                    $('#child_bill_iframe').width(width - 30);
                    $('#child_bill_iframe').height(height - 50);
                }
            });
            $('#child_bill_iframe').attr('src', '/biz_bill/child_bill?id=' + rows[0]['cost_id']);
        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }

    }
    function sys_log() {
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            $('#sys_log').window({
                title: 'window',
                width: 1100,
                height: 650,
                cache: false,
                modal: true,
                onResize:function (width, height) {
                    $('#sys_log_iframe').width(width - 30);
                    $('#sys_log_iframe').height(height - 50);
                }
            });
            $('#sys_log_iframe').attr('src', '/sys_log/index?table_name=biz_bill&key_no=' + rows[0]['cost_id']);
        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }

    }

    /**
     * 批量申请支付的页面
     */
    function plsq(){
        window.open('/biz_bill_user/wlk_index');
    }

    function bill_apply() {
        $('#bill_apply').window({
            title: 'window',
            width: 900,
            height: 450,
            cache: false,
            modal: true,
            onResize:function (width, height) {
                $('#bill_apply_iframe').width(width - 30);
                $('#bill_apply_iframe').height(height - 40);
            }
        });
        $('#bill_apply_iframe').attr('src', '/biz_bill_apply/index/<?= $id_type;?>/<?= $id_no;?><?php if($is_special == 1) echo '?is_special=1'?>');
    }

    //费用确认
    function batch_confirm_date(type,index = null){
        if(index !== null){
            var datas = $('#tt').datagrid('getData');
            var data = [datas['rows'][index]];
        }else{
            var data = $('#tt').datagrid('getSelections');
        }
        if(data.length > 0){
            var ids = [];
            //检测付款方是否为同一个,如果不是,检测币种,税率是否相同
            var client_codes = [];
            var currencys = [];
            var vats = [];
            //已开票,已核销 已对账不能弹出窗口
            var not_window = '';
            $.each(data, function(index, item){
                if(type == 'cost'){
                    if(item.cost_invoice_id != 0){
                        not_window = '<?= lang('已开票,无法确认');?>';
                        return false;
                    }
                    if(item.cost_confirm_date !== '0000-00-00'){
                        not_window = '<?= lang('已确认,无法确认');?>';
                        return false;
                    }
                    ids.push(item.cost_id);
                    if($.inArray(item.creditor, client_codes) === -1)client_codes.push(item.creditor);
                    if($.inArray(item.cost, currencys) === -1)currencys.push(item.cost);
                    if($.inArray(item.cost_vat, vats) === -1)vats.push(item.cost_vat);
                }else if(type == 'sell'){
                    if(item.sell_invoice_id != 0){
                        not_window = '<?= lang('已开票,无法确认');?>';
                        return false;
                    }
                    if(item.sell_confirm_date !== '0000-00-00'){
                        not_window = '<?= lang('已确认,无法确认');?>';
                        return false;
                    }
                    ids.push(item.sell_id);
                    if($.inArray(item.debitor, client_codes) === -1)client_codes.push(item.debitor);
                    if($.inArray(item.sell, currencys) === -1)currencys.push(item.sell);
                    if($.inArray(item.sell_vat, vats) === -1)vats.push(item.sell_vat);
                }
            });
            if(not_window !== ''){
                $.messager.alert('<?= lang('提示');?>', not_window);
                return;
            }
            //客户是否相同
            if(client_codes.length > 1){
                //判断币种税率是否相同
                if(currencys.length > 1 || vats.length > 1){
                    $.messager.alert('<?= lang('提示')?>','<?= lang('币种或税率不同,无法确认')?>');
                    return;
                }
            }

            ids = ids.join(',');
            $('#batch_confirm_date input[name="ids"]').val(ids);
            // 获取当前日期时间
            var myDate = new Date();
            $('#confirm_date').datebox('clear').datebox('setValue', myDate.getFullYear() + '-' + (myDate.getMonth() + 1) + '-' + myDate.getDate());
            // $('#account_no').textbox('clear');
            $('#batch_confirm_date').dialog('open').dialog('setTitle', type);
        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }
    }

    function save_batch_confirm() {
        var ids = $('#batch_confirm_date input[name="ids"]').val();
        var confirm_date = $('#confirm_date').datebox('getValue');
        // var account_no = $('#account_no').textbox('getValue');
        var isValid = $('#batch_confirm_date').form('validate');
        if(isValid){
            $.ajax({
                type: 'POST',
                url: '/biz_bill/batch_confirm_date',
                data: {
                    ids:ids,
                    confirm_date:confirm_date,
                    // account_no:account_no,
                    is_special : '<?= $is_special;?>',
                },
                dataType: 'json',
                success:function (res) {
                    $.messager.alert('<?= lang('提示');?>', res.msg);
                    if(res.code == 0){
                        $('#tt').datagrid('reload');
                    }
                },
                error: function () {
                    $.messager.alert('提示', '<?= lang('发生错误');?>');
                }
            })
            $('#batch_confirm_date').dialog('close');
        }

    }

    //批量确认自己的--start
    function batch_confirm_self(type) {
        // 获取当前日期时间
        var myDate = new Date();
        $('#confirm_date').datebox('clear').datebox('setValue', myDate.getFullYear() + '-' + (myDate.getMonth() + 1) + '-' + myDate.getDate());
        // $('#account_no').textbox('clear');
        $('#batch_confirm_date').dialog('open').dialog('setTitle', type);
        var bcd_a = $('#bcd a');
        bcd_a.attr('onclick2', bcd_a.attr('onclick'));
        bcd_a.attr('onclick', "save_batch_confirm_self(\'" + type +"')");
    }

    function save_batch_confirm_self(type) {
        var confirm_date = $('#confirm_date').datebox('getValue');
        // var account_no = $('#account_no').textbox('getValue');
        var isValid = $('#batch_confirm_date').form('validate');
        if(isValid){
            $.ajax({
                type: 'POST',
                url: '/biz_bill/batch_confirm_self_bill',
                data: {
                    type : type,
                    id_type : '<?= $id_type;?>',
                    id_no : '<?= $id_no;?>',
                    is_special : '<?= $is_special;?>',
                    confirm_date : confirm_date,
                },
                dataType: 'json',
                success: function (res) {
                    $.messager.alert('<?= lang('提示');?>', res.msg);
                    if (res.code == 0) {
                        $('#tt').datagrid('reload');
                    }
                },
                error: function () {
                    $.messager.alert('<?= lang('提示');?>', '<?= lang('发生错误');?>');
                }
            });
            var bcd_a = $('#bcd a');
            bcd_a.attr('onclick', bcd_a.attr('onclick2'));
            bcd_a.attr('onclick2', '');
            $('#batch_confirm_date').dialog('close');
        }
    }
    //批量确认自己的--end

    //费用取消确认
    function batch_date_cancel(type,index) {
        if(index !== null){
            var datas = $('#tt').datagrid('getData');
            var data = [datas['rows'][index]];
        }else{
            var data = $('#tt').datagrid('getSelections');
        }
        if(data.length > 0){
            var row = data[0];
            var ids = [];
            $.each(data, function(index, item){
                if(type == 'cost'){
                    ids.push(item.cost_id);
                }else if(type == 'sell'){
                    ids.push(item.sell_id);
                }
            });
            var confirm_date = '0000-00-00';
            // var account_no = '';
            ids = ids.join(',');
            $.messager.confirm('<?= lang('提示');?>', '<?= lang('确认取消?');?>', function(r) {
                if (r) {
                    $.ajax({
                        type: 'POST',
                        url: '/biz_bill/batch_confirm_date',
                        data: {
                            ids:ids,
                            confirm_date:confirm_date,
                            // account_no:account_no,
                        },
                        dataType: 'json',
                        success:function (res) {
                            $.messager.alert('<?= lang('提示');?>', res.msg);
                            if(res.code == 0){
                                $('#tt').datagrid('reload');
                            }
                        },
                        error: function () {
                            $.messager.alert('<?= lang('提示');?>', '<?= lang('发生错误');?>');
                        }
                    });
                }
            });
        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }
    }

    //开票申请--start
    function invoice_apply(type){
        var data = $('#tt').datagrid('getSelections');
        if(data.length > 0){
            var ids = [];
            //客户,税率,币种必须一致
            var client_codes = [];
            var currencys = [];
            var vats = [];
            var sum_amount = 0;

            var not_window = '';
            var item;
            $.each(data, function(index, item2){
                item = item2;
                if(type == 'cost'){
                    if(item.cost_invoice_id != 0){
                        not_window = '<?= lang('已开票,无法申请');?>';
                        return false;
                    }
                    if(item.cost_confirm_date === '0000-00-00'){
                        not_window = '未确认,无法申请';
                        return false;
                    }
                    var amount = - parseFloat(item.cost_local_amount);
                    sum_amount += amount != null ? amount : 0;

                    ids.push(item.cost_id);
                    if($.inArray(item.creditor, client_codes) === -1)client_codes.push(item.creditor);
                    if($.inArray(item.cost, currencys) === -1)currencys.push(item.cost);
                    if($.inArray(item.cost_vat, vats) === -1)vats.push(item.cost_vat);
                }else if(type == 'sell'){
                    if(item.sell_id == null){
                        not_window = '<?= lang('账单不存在,无法申请');?>';
                        return false;
                    }
                    if(item.sell_invoice_id != 0){
                        not_window = '<?= lang('已开票,无法申请');?>';
                        return false;
                    }
                    if(item.sell_confirm_date === '0000-00-00'){
                        not_window = '<?= lang('未确认,无法申请');?>';
                        return false;
                    }
                    var amount = parseFloat(item.sell_local_amount);
                    sum_amount += amount != null ? amount : 0;

                    ids.push(item.sell_id);
                    if($.inArray(item.debitor, client_codes) === -1)client_codes.push(item.debitor);
                    if($.inArray(item.sell, currencys) === -1)currencys.push(item.sell);
                    if($.inArray(item.sell_vat, vats) === -1)vats.push(item.sell_vat);
                }
            });
            if(not_window !== ''){
                $.messager.alert('<?= lang('提示');?>', not_window);
                return;
            }
            //客户是否相同

            if(client_codes.length > 1){
                $.messager.alert('<?= lang('提示')?>', '<?= lang('只能有一个抬头');?>');
                return;
            }

            //判断币种税率是否相同
            if(vats.length > 1){
                $.messager.alert('<?= lang('提示')?>','<?= lang('税率不同')?>');
                return;
            }
            ids = ids.join(',');

            var client_code_name;
            if(type == 'sell'){
                client_code_name = item.debitor_name;
            }else{
                client_code_name = item.creditor_name;
            }
            $('#buyer_company').combobox('setValue', client_code_name).combobox('reload');
            console.log(currencys);
            var from_data = {'ids' : ids,'buyer_company' : client_codes[0],'currency' : 'CNY', 'vat' : vats[0], 'invoice_amount' : sum_amount};
            $('#invoice_apply_from').form('load', from_data);
            $('#invoice_apply').dialog('open').dialog('setTitle', type);
        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }
    }

    function save_invoice_apply() {
        var form_data = $('#invoice_apply_from').serializeArray();
        var isValid = $('#invoice_apply_from').form('validate');
        var json = {};
        $.each(form_data, function (index, item) {
            json[item.name] = item.value;
        });
        if(isValid){
            $.ajax({
                type: 'POST',
                url: '/biz_bill/batch_invoice',
                data: json,
                dataType: 'json',
                success:function (res) {
                    $.messager.alert('<?= lang('提示');?>', res.msg);
                    if(res.code == 0){
                        $('#tt').datagrid('reload');
                        $('#invoice_apply').dialog('close')
                    }
                },
                error: function () {
                    $.messager.alert('<?= lang('提示');?>', '<?= lang('发生错误');?>');
                }
            })
            $('#batch_confirm_date').dialog('close');
        }

    }

    //分摊比例--start
    function consol_percent(index){
        if(index !== undefined) $('#tt').datagrid('selectRow', index);
        var row = $('#tt').datagrid('getSelected');
        if(row != null){
            if(row.cost_confirm_date != '0000-00-00' || row.cost_invoice_id != 0){
                $.messager.alert('<?= lang('提示')?>','<?= lang('已确认或已开票,无法再次分摊')?>');
                return;
            }
            if(row.cost_split_mode == '-1'){
                $.messager.alert('<?= lang('提示')?>','<?= lang('该分摊模式不支持重新分摊,请删除后新增')?>');
                return;
            }
            $('#consol_percent_form').form('load', {'id':row.cost_id, 'split_mode' : row.cost_split_mode});
            $('.split_mode:checked').trigger('click');
            $('#consol_percent').dialog('open').dialog('setTitle', '分摊');
        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }
    }

    function save_consol_percent() {
        var form_data = $('#consol_percent_form').serializeArray();
        var isValid = $('#consol_percent_form').form('validate');
        var json = {};
        $.each(form_data, function (index, item) {
            json[item.name] = item.value;
        });
        //2021-11-03 18:13 新增一个如果总金额不等于账单总金额,那么不给提交
        if(json.split_mode == 3){
            var all_amount = 0;
            $.each($('.diy_datas_amount'), function(i, it){

                all_amount += parseInt(Math.round(Number($(it).val()) * 100));
            });
            var row = $('#tt').datagrid('getSelected');
            if(row != null){
                if(all_amount != parseInt(Math.round(Number(row.cost_amount) * 100))){
                    $.messager.alert('<?= lang('提示');?>', '<?= lang('总金额不等于{cost_amount},提交失败', array('cost_amount' => "' + row.cost_amount + '"));?>');
                    return;
                }
            }
        }
        if(json.split_mode == 4){
            if(json.box_size === ''){
                return $.messager.alert("<?= lang('提示');?>", "<?= lang('请选择箱型后再试');?>");
            }
        }


        ajaxLoading();
        if(isValid){
            $.ajax({
                type: 'POST',
                url: '/biz_bill/reload_child_bill',
                data: json,
                dataType: 'json',
                success:function (res) {
                    ajaxLoadEnd();
                    alert(res.msg);
                    if(res.code == 0){
                        $('#consol_percent').dialog('close');
                        $('#tt').datagrid('reload');
                    }
                },
                error: function () {
                    ajaxLoadEnd();
                    $.messager.alert('<?= lang('提示');?>', '发生错误');
                }
            })
        }
    }
    //分摊比例--end

    function reload_client(type = 0){
        // if(type == 0){
        $('#creditor').combobox('reload', '/biz_client/get_option/0/1');
        // }else{
        // $('#creditor').combobox('reload', '/biz_client/get_bill_client/<?= $id_type;?>/<?= $id_no?>');
        // }
    }

    function reload_edit_client_code(combobox_id = ''){
        var type = $('#update_div_client_code').combobox('options').bill_type;
        if(combobox_id == 'creditor' && type == 'cost'){
            $('#update_div_client_code').combobox('loadData', $('#creditor').combobox('getData'));
        }
    }

    function lock(lock){
        if(lock >= 0){
            $.ajax({
                url: '/biz_consol/lock',
                type: 'POST',
                data:{
                    id:'<?= $id_no;?>',
                    level:lock,
                },
                success: function (res_json) {
                    var res;
                    try{
                        res = $.parseJSON(res_json);
                    }catch(e){

                    }
                    if(res == undefined){
                        $.messager.alert('<?= lang('提示');?>',res_json);
                    }else{
                        if(res.code == 0){
                            parent.location.reload();
                        }else{
                            $.messager.alert('<?= lang('提示');?>', res.msg);
                        }
                    }
                }
            });
        }
    }

    //字段--start
    function charge_for(value, row, index) {
        var charge_name_en = row.charge_name_en;
        if(charge_name_en == null){
            return "";
        }
        var str = charge_name_en + " / " + value;
        return str;
    }

    function cost_all_style(value, row, index) {
        var style = '';
        if(row.cost_ban_handle){
            style = 'background-color:#C0C0C0;';
        }
        return style;
    }
    function sell_all_style(value, row, index) {
        var style = '';
        if(row.sell_ban_handle){
            style = 'background-color:#C0C0C0;';
        }
        return style;
    }
    function cost_for(value,row,index){
        var str = '';
        var cost = row.cost;
        if(cost == null){
            cost = '';
        }
        str += '<span class="cost">' + cost + '</span>';
        return str;
    }
    function sell_for(value,row,index){
        var str = '';
        var sell = row.sell;
        if(sell == null){
            sell = '';
        }
        str += '<span class="sell">' + sell + '</span>';
        return str;
    }
    function cost_unit_price_for(value,row,index){
        var str = '';
        var cost_unit_price = row.cost_unit_price;
        if(cost_unit_price == null){
            cost_unit_price = '';
        }
        str += '<span class="cost">' + cost_unit_price + '</span>';
        return str;
    }
    function sell_unit_price_for(value,row,index){
        var str = '';
        var sell_unit_price = row.sell_unit_price;
        if(sell_unit_price == null){
            sell_unit_price = '';
        }
        str += '<span class="sell">' + sell_unit_price + '</span>';
        return str;
    }

    function cost_number_for(value,row,index){
        var str = '';
        var cost_number = row.cost_number;
        if(cost_number == null){
            cost_number = '';
        }
        str += '<span class="cost">' + cost_number + '</span>';
        return str;
    }
    function sell_number_for(value,row,index){
        var str = '';
        var sell_number = row.sell_number;
        if(sell_number == null){
            sell_number = '';
        }
        str += '<span class="sell">' + sell_number + '</span>';
        return str;
    }
    function cost_amount_for(value,row,index){
        var str = '';
        var cost_amount = row.cost_amount;
        if(cost_amount == null){
            cost_amount = '';
        }
        var cost_actual_flag = row.cost_actual_flag;
        if(cost_actual_flag == '' || cost_actual_flag == undefined){
            cost_actual_flag = '';
        }else{
            cost_actual_flag = ' (' + cost_actual_flag + ')';
        }
        var cost_invoice_flag = row.cost_invoice_flag;
        if(cost_invoice_flag == '' || cost_invoice_flag == undefined){
            cost_invoice_flag = '';
        }else{
            cost_invoice_flag = ' (' + cost_invoice_flag + ')';
        }
        var cost_chongping = row.cost_chongping;
        if(cost_chongping !== '' && cost_chongping !== undefined) cost_chongping = ' (' + cost_chongping + ')';
        else cost_chongping = '';
        str += '<span class="cost">' + cost_amount + cost_invoice_flag + cost_actual_flag + cost_chongping + '</span>';
        return str;
    }
    function sell_amount_for(value,row,index){
        var str = '';
        var sell_amount = row.sell_amount;
        if(sell_amount == null){
            sell_amount = '';
        }
        var sell_actual_flag = row.sell_actual_flag;
        if(sell_actual_flag == '' || sell_actual_flag == undefined){
            sell_actual_flag = '';
        }else{
            sell_actual_flag = ' (' + sell_actual_flag + ')';
        }
        var sell_chongping = row.sell_chongping;
        if(sell_chongping !== '' && sell_chongping !== undefined) sell_chongping = ' (' + sell_chongping + ')';
        else sell_chongping = '';
        str += '<span class="sell">' + sell_amount + sell_actual_flag + sell_chongping + '</span>';
        return str;
    }
    function cost_vat_for(value,row,index){
        var str = '';
        if(row['id'] == 'Sum:'){
            return str;
        }
        var cost_vat = row.cost_vat;
        if(cost_vat == null){
            cost_vat = '';
        }
        var cost_vat_withhold = row.cost_vat_withhold;
        if(cost_vat_withhold == null){
            cost_vat_withhold = '';
        }
        str += '<span class="cost">' + cost_vat + '</span>';
        str += '<br/><span class="color: #a59797;">' + cost_vat_withhold + '</span>';
        return str;
    }
    function sell_vat_for(value,row,index){
        var str = '';
        if(row['id'] == 'Sum:'){
            return str;
        }
        var sell_vat = row.sell_vat;
        if(sell_vat == null){
            sell_vat = '';
        }
        str += '<span class="sell">' + sell_vat + '</span>';
        return str;
    }
    function cost_confirm_date_for(value,row,index){
        var str = '';
        if(row['id'] == 'Sum:'){
            return str;
        }
        var cost_confirm_date = row.cost_confirm_date;
        if(row.cost_id != null){
            if(cost_confirm_date == '0000-00-00'){
                cost_confirm_date = '<span onclick="batch_confirm_date(\'cost\', ' + index + ')" style="cursor:pointer"> <?= lang('未确认');?></span>';
            }else{
                cost_confirm_date = '<span onclick="batch_date_cancel(\'cost\', ' + index + ')" style="cursor:pointer">' + cost_confirm_date + ' <?= lang('确认');?></span>';
            }
            str += '<span class="cost">' + cost_confirm_date + '</span>';
        }

        return str;
    }
    function sell_confirm_date_for(value,row,index){
        var str = '';
        if(row['id'] == 'Sum:'){
            return str;
        }
        var sell_confirm_date = row.sell_confirm_date;
        if(row.sell_id != null){
            if(sell_confirm_date == '0000-00-00'){
                sell_confirm_date = '<span onclick="batch_confirm_date(\'sell\', ' + index + ')" style="cursor:pointer"><?= lang('未确认');?></span>';
            }else{
                sell_confirm_date = '<span onclick="batch_date_cancel(\'sell\', ' + index + ')" style="cursor:pointer">' + sell_confirm_date + '<?= lang('确认');?></span>';
            }
            str += '<span class="sell">' + sell_confirm_date + '</span>';
        }
        return str;
    }
    function cost_invoice_no_for(value,row,index){
        var str = '';
        var cost_invoice_no = row.cost_invoice_no;
        if(cost_invoice_no == null){
            cost_invoice_no = '';
        }
        str += '<span class="cost">' + cost_invoice_no + '</span>';
        return str;
    }
    function sell_invoice_no_for(value,row,index){
        var str = '';
        var sell_invoice_no = row.sell_invoice_no;
        if(sell_invoice_no == null){
            sell_invoice_no = '';
        }
        str += '<span class="sell">' + sell_invoice_no + '</span>';
        return str;
    }
    function cost_close_date_for(value,row,index){
        var str = '';
        var cost_close_date = row.cost_close_date;
        if(cost_close_date == null){
            cost_close_date = '';
        }
        str += '<span class="cost">' + cost_close_date + '</span>';
        return str;
    }
    function sell_close_date_for(value,row,index){
        var str = '';
        var sell_close_date = row.sell_close_date;
        if(sell_close_date == null){
            sell_close_date = '';
        }
        str += '<span class="sell">' + sell_close_date + '</span>';
        return str;
    }

    function client_code_update(index, type = 'cost') {
        var tt = $('#tt');
        tt.datagrid('selectRow', index);
        var row = tt.datagrid('getSelected');

        if(row != null){
            var form_data = {};
            if(type == 'sell'){
                form_data.client_code = row.debitor;
                $('#update_div_client_code').combobox('options').bill_type = 'sell';
                reload_edit_client_code('debitor');
            }else{
                form_data.client_code = row.creditor;
                $('#update_div_client_code').combobox('options').bill_type = 'cost';
                reload_edit_client_code('creditor');
            }
            form_data.id = row[type + '_id'];

            //已确认无法修改
            var Tips = '';
            if(row[type + '_parent_id'] != 0){
                Tips = '<?= lang('不能修改分摊账单');?>';
            }
            if(row[type + '_confirm_date'] != '0000-00-00'){
                Tips = '<?= lang('已确认无法修改');?>';
            }
            if(row[type + '_account_no'] != ''){
                Tips = '<?= lang('已填写对账号无法修改');?>';
            }
            if(row[type + '_invoice_id'] != 0){
                Tips = '<?= lang('已开票无法修改');?>';
            }
            if(row[type + '_payment_id'] != 0){
                Tips = '<?= lang('已核销无法修改');?>';
            }
            if(row[type + '_split_id'] != 0){
                Tips = '<?= lang('已拆分无法修改');?>';
            }
            if(Tips != ''){
                $.messager.alert('<?= lang('提示');?>', Tips);
                return false;
            }

            $('#client_code_div').window('open');
            $('#client_code_form').form('load', form_data);
        }else{
            $.messager.alert('<?= lang('提示');?>', '<?= lang('发生错误');?>');
        }
    }

    function client_code_save() {
        $.messager.confirm('<?= lang('提示');?>','<?= lang('是否确认提交？');?>',function(r){
            if (r){
                $('#client_code_form').form('submit', {
                    url:'/biz_bill/update_field_data',
                    onSubmit:function () {
                        return $(this).form('validate');    // 返回false终止表单提交
                    },
                    success: function(res_json){
                        var res;
                        try {
                            res = $.parseJSON(res_json);
                        }catch (e) {

                        }
                        if(res === undefined){
                            $.messager.alert('<?= lang('提示');?>', res_json);
                            return false;
                        }
                        $.messager.alert('<?= lang('提示');?>', res.msg);
                        if(res.code == 0){
                            $('#tt').datagrid('reload');
                            $('#client_code_div').window('close');
                        }else{

                        }
                    },
                });
            }
        });
    }

    function apply_payment_wlk(index, type = 'cost', is_wlk_yw = 0) {
        var tt = $('#tt');
        tt.datagrid('selectRow', index);
        var row = tt.datagrid('getSelected');
        if(row != null){
            $('#apply_payment_wlk').window({
                title: '<?= lang('window');?>',
                width: 700,
                height: 500,
                cache: false,
                modal: true,
                onResize:function (width, height) {
                    $('#apply_payment_wlk_iframe').width(width - 30);
                    $('#apply_payment_wlk_iframe').height(height - 50);
                }
            });
            $('#apply_payment_wlk_iframe').attr('src', '/biz_bill_payment_wlk_apply/add?bill_id=' + row[type + '_id'] + '&is_wlk_yw=' + is_wlk_yw);
        }else{
            $.messager.alert('<?= lang('提示');?>', '<?= lang('发生错误');?>');
        }
    }

    function creditor_for(value,row,index){
        var str = '';
        var creditor_name = row.creditor_name;
        if(creditor_name == null){
            creditor_name = '';
        }
        str += '<span class="cost" onclick="client_code_update(' + index + ', \'cost\')" style="cursor: pointer;">' + creditor_name + '</span>';
        return str;
    }
    function debitor_for(value,row,index){
        var str = '';
        var debitor_name = row.debitor_name;
        if(debitor_name == null){
            debitor_name = '';
        }
        str += '<span class="sell">' + debitor_name + '</span>';
        return str;
    }
    function cost_split_mode_for(value,row,index){
        var str = '';
        if(row['id'] == 'Sum:'){
            return str;
        }
        var cost_split_mode = row.cost_split_mode;
        if(row.cost_id != null){
            if(cost_split_mode == 0){
                cost_split_mode = '<?= lang('按票');?>';
            }else if(cost_split_mode == 1){
                cost_split_mode = '<?= lang('按体积');?>';
            }else if(cost_split_mode == 2){
                cost_split_mode = '<?= lang('按重量');?>';
            }else if(cost_split_mode == 3){
                cost_split_mode = '<?= lang('自定义');?>';
            }else if(cost_split_mode == 4){
                cost_split_mode = '<?= lang('按箱');?>';
            }
            str += '<span class="cost" onclick="consol_percent(' + index + ')" style="cursor:pointer">' + cost_split_mode + '</span>';
        }
        return str;
    }
    //字段--end

    function ex_rate_date() {
        $('#ex_rate_date_div').window('open');
    }

    function ex_rate_date_save() {
        $.messager.confirm('<?= lang('提示');?>','<?= lang('是否确认提交？');?>',function(r){
            if (r){
                ajaxLoading();
                $('#ex_rate_date_form').form('submit', {
                    url:'/biz_consol/save_ex_rate_date',
                    onSubmit:function () {
                        var validate = $(this).form('validate'); // 返回false终止表单提交
                        if(!validate) ajaxLoadEnd();
                        return validate;
                    },
                    success: function(res_json){
                        ajaxLoadEnd();
                        try {
                            var res = $.parseJSON(res_json);

                            $.messager.alert('<?= lang('提示');?>', res.msg);

                            if(res.code == 0){
                                $('#tt').datagrid('reload');
                                $('#ex_rate_date_div').window('close');
                            }else{

                            }
                        }catch (e) {
                            $.messager.alert('<?= lang('提示');?>', res_json);
                            return false;
                        }


                    },
                });
            }
        });
    }
</script>
<?php
$userRole = get_session('user_role');
if($lock_lv < 3){
    if(menu_role('consol_lock3')){
        echo '<a href="javascript:void(0)" onclick="lock(3)" style="margin-right:15px;" title="' . lang('carrier cost锁') . '">' . lang('C3锁') . '</a>';
    }
}else{
    if($lock_lv > 0 && menu_role('consol_lock3')){
        echo '<a href="javascript:void(0)" title="' . lang('解锁') . '" onclick="lock(0)" style="margin-right:15px;">' . lang('解锁') . '</a>';
    }
}
?>
<?php if($lock_lv < 1){?>
    <div id="dlg" class="" style="width:100%;background-color: #F4F4F4;border:1px solid #95B8E7;margin-top:10px;"
         data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons'">
        <form id="fm" method="post" novalidate style="margin:0;padding:0px 10px 10px">
            <input type="hidden" name="cost_id_type" id="cost_id_type">
            <input type="hidden" name="cost_id_no" id="cost_id_no">
            <input type="hidden" name="cost_id" id="cost_id">
            <input type="hidden" name="is_special" value="<?= $is_special;?>">
            <table>
                <tr >
                    <input name="charge_code" id="charge_code" type="hidden">
                    <td><?= lang('charge');?>:</td>
                    <td>
                        <input name="charge" id="charge" class="easyui-combobox" style="width:150px;" data-options="
                        required:true,
                        valueField:'charge_name_cn',
                        textField:'charge_name_long',
                        url:'/biz_charge/get_option',
                        onSelect: function(rec){
                            $('#charge_code').val(rec.charge_code);
                            if(rec.charge_code == 'YJ'){
                                $('#creditor').combobox('loadData', ajax_data.wanglaikuan_client);
                                $('#debitor').combobox('loadData', ajax_data.wanglaikuan_client);
                            }
                        },
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">
                    </td>
                    <td><?= lang('汇率日期');?></td>
                    <td></td>
					<td colspan="12"><span class="account_bank_show" style="display: none"><a href="#" style="color: blue" target="_blank" id="write_off_href"></a></span></td>
                </tr>
                <tr class="cost">
                    <td><a href="javascript:void(0);" onclick="reload_client(1)"><?= lang('creditor');?></a></td>
                    <td>
                        <input name="creditor" id="creditor" class="easyui-combobox" style="width:150px;" data-options="
                        required:true,
                        valueField:'client_code',
                        textField:'company_name',
                        value: '<?php echo $creditor_default;?>',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                        onLoadSuccess:function(){
                            reload_edit_client_code('creditor');
                            let value = $(this).combobox('getValue')
                            let currency = $('#cost').combobox('getValue')
                            if(value != ''){
                            	$('#cost_payment_bank').combobox('reload','/bsc_dict/get_client_account1/'+value+'/'+currency)
								$('#write_off_href').attr('href','/biz_client/edit/'+value)
								$('#write_off_href').text('Click here to add Bank Aceount for the '+$(this).combobox('getText'))
                            }
                        },
                        onSelect:function(rec){
                        	let currency = $('#cost').combobox('getValue')
							$('#cost_payment_bank').combobox('reload','/bsc_dict/get_client_account1/'+rec.client_code+'/'+currency)
            				$('#write_off_href').attr('href','/biz_client/edit/'+rec.client_code)
            				$('#write_off_href').text('Click here to add Bank Aceount for the '+rec.company_name)
                        }
                        ">
                    </td>
                    <td><?= lang('remark');?>:</td>
                    <td><input id="cost_remark" name="cost_remark" class="easyui-textbox" style="width:100px;"></td>
                    <td><?= lang('cost');?></td>
                    <td>
                        <input name="cost" id="cost" class="easyui-combobox" style="width:60px;" data-options="
                        required:true,
                        valueField:'name',
                        textField:'name',
                        url:'/biz_bill_currency/get_currency/cost',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                        onSelect:function(rec){
							if(rec.name != ' ' && rec.name != ''){
								let client_code = $('#creditor').combobox('getValue')
								$('#cost_payment_bank').combobox('reload','/bsc_dict/get_client_account1/'+client_code+'/'+rec.name)
							}

                        }
                    ">
                    </td>
                    <td><?= lang('amount');?></td>
                    <td>
                        <input name="cost_unit_price" id="cost_unit_price" class="easyui-numberbox" required="true" precision="2" style="width:70px;" data-options="
                        onChange:function(newVal, oldVal){
                            if($('#cost_number').numberbox('getValue') == ''){
                                $('#cost_number').numberbox('setValue', '1');
                            }
                            $('#cost_amount').numberbox('setValue', newVal * $('#cost_number').numberbox('getValue'));
                        },
                    ">
                    </td>
                    <td>X</td>
                    <td>
                        <input name="cost_number" id="cost_number" class="easyui-numberbox" precision="4" required="true" style="width:60px;" data-options="
                    onChange:function(newVal, oldVal){
                        if($('#cost_unit_price').numberbox('getValue') == ''){
                            $('#cost_unit_price').numberbox('setValue', '1');
                        }
                        $('#cost_amount').numberbox('setValue', $('#cost_unit_price').numberbox('getValue') * newVal);
                    },">
                    </td>
                    <td>=</td>
                    <td>
                        <input name="cost_amount" id="cost_amount" class="easyui-numberbox" readonly="readonly" required="true" precision="4" style="width:90px;">
                    </td>
                    <td><?= lang('vat');?></td>
                    <td>
                        <select class="vat easyui-combobox" name="cost_vat" id="cost_vat" style="width: 58px;" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        value: '0%',
                        url:'/bsc_dict/get_option/vat',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    "></select>
                    </td>
                    <td><?= lang('vat_withhold');?></td>
                    <td>
                        <select class="easyui-combobox" name="cost_vat_withhold" id="cost_vat_withhold" style="width: 58px;" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        value: '0%',
                        url:'/bsc_dict/get_option/vat_withhold',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    "></select>
                    </td>
					<td><?= lang('account');?></td>
					<td>
						<select class="easyui-combobox"
								id="cost_payment_bank" style="width: 80px" data-options="
                                    valueField : 'title_cn_currency',
                                    textField : 'title_cn_currency',
                                    editable:false,
                                    required:true,
                                    onSelect:function(rec){
                                        $('#cost_payment_bank_id').val(rec.id)
                                    },
                                    onLoadSuccess:function(data){
                                    	if(data.length > 0){
                                    		$('.account_bank_show').hide()
                                    	}else{
                                    		$.messager.alert('Tips','Add bank account for the company in client page.')
                                    		$('.account_bank_show').show()
                                    	}
                                    }
                                ">
						</select>
						<input type="hidden" name="cost_bank_account_id" id="cost_payment_bank_id">
					</td>
                    <td>
                        <select class="easyui-combobox" editable="false" id="cost_split_mode" name="cost_split_mode">
                            <option value="0"><?= lang('按票');?></option>
                            <option value="1"><?= lang('按体积');?></option>
                            <option value="2"><?= lang('按立方');?></option>
                        </select>
                    </td>
                    <td><?= lang('created_by');?></td>
                    <td>
                        <a href="javascript:void(0)" title="<?= lang('创建人');?>" class="easyui-tooltip">
                            <input class="easyui-textbox" name='cost_created_by' id="cost_created_by" disabled="disabled" style="width:60px">
                        </a>
                        <a href="javascript:void(0)" title="<?= lang('发票号');?>" class="easyui-tooltip">
                            <input class="easyui-textbox" name='cost_invoice_no' id="cost_invoice_no" disabled="disabled" style="width:90px">
                        </a>
                        <a href="javascript:void(0)" title="<?= lang('创建日期');?>" class="easyui-tooltip">
                            <input class="easyui-textbox" name='cost_created_time' id="cost_created_time" disabled="disabled" style="width:90px">
                        </a>
                    </td>
                </tr>
            </table>
        </form>
        <div id="dlg-buttons">
            <a href="javascript:save()" id="save" class="easyui-linkbutton" iconCls="icon-ok"><?= lang('新增');?></a>
            <a href="javascript:add();$('#debitor').combobox('select', '<?= $debitor_default;?>');" id="add" class="easyui-linkbutton" iconCls="icon-add" ><?= lang('add');?></a>
            <?php if($hasDelete){?>
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm2',plain:false" iconCls="icon-remove"><?= lang('delete');?></a>


            <?php } ?>
            <?php
            if($is_special != 1){
                if(menu_role('billing_special_bill')){
                    echo '<a href="javascript:void(0)" class="easyui-linkbutton"  onclick="javascript:window.location.href=\'/biz_bill/index?id_type=' . $id_type . '&id_no=' . $id_no. '&is_special=1\';">' . lang('special') . '</a>';
                }
            }else{
                echo '<a href="javascript:void(0)" class="easyui-linkbutton"  onclick="javascript:window.location.href=\'/biz_bill/index?id_type=' . $id_type . '&id_no=' . $id_no. '\';">' . lang('non-specialty') . '</a>';
            }
            ?>

            <a href="javascript:void(0);" class="easyui-linkbutton" onclick="javascript:singleSelect = !singleSelect;$('#tt').datagrid('clearSelections').datagrid({singleSelect: singleSelect,onClickRow:function(rowIndex) {click_row(singleSelect,rowIndex)}});"><?= lang('多选');?></a>

            <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm3',plain:false" iconCls="icon-ok"><?= lang('批量确认');?></a>
            <!--        <a href="javascript:$('#dlg').addClass('hide')" class="easyui-linkbutton" iconCls="icon-cancel"-->
            <!--           style="width:90px">取消</a>-->
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="child_bill()"><?= lang('分摊详情');?></a>
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="sys_log()"><?= lang('日志');?></a>
            <a href="javascript:void(0)" class="easyui-linkbutton" onclick="ex_rate_date()"><?= lang('汇率日期');?></a>
        </div>
    </div>
<?php }else { ?>
    <form id="fm" method="post" novalidate style="margin:0;padding:10px 10px">
        <input type="hidden" name="cost_id_type" id="cost_id_type">
        <input type="hidden" name="cost_id_no" id="cost_id_no">
        <input type="hidden" name="cost_id" id="cost_id">
        <input type="hidden" name="is_special" value="<?= $is_special;?>">
        <table>
            <tr>
                <input name="charge_code" id="charge_code" type="hidden">
                <td><?= lang('charge');?>:</td>
                <td>
                    <input name="charge" id="charge" class="easyui-combobox" style="width:150px;" data-options="
                        required:true,
                        valueField:'charge_name_cn',
                        textField:'charge_name_long',
                        url:'/biz_charge/get_option',
                        onSelect: function(rec){
                            $('#charge_code').val(rec.charge_code);
                            if(rec.charge_code == 'YJ'){
                                $('#creditor').combobox('loadData', ajax_data.wanglaikuan_client);
                                $('#debitor').combobox('loadData', ajax_data.wanglaikuan_client);
                            }
                        },
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">
                </td>
            </tr>
            <tr class="cost">
                <td><a href="javascript:void(0);" onclick="reload_client(1)"><?= lang('creditor');?></a></td>
                <td>
                    <input name="creditor" id="creditor" class="easyui-combobox" style="width:150px;" data-options="
                        required:true,
                        valueField:'client_code',
                        textField:'company_name',
                        value: '<?php echo $creditor_default;?>',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                        onLoadSuccess:function(){
                            reload_edit_client_code('creditor');
                        },
                        ">
                </td>
                <td><?= lang('remark');?>:</td>
                <td><input id="cost_remark" name="cost_remark" class="easyui-textbox" style="width:100px;"></td>
                <td><?= lang('cost');?></td>
                <td>
                    <input name="cost" id="cost" class="easyui-combobox" style="width:60px;" data-options="
                        required:true,
                        valueField:'name',
                        textField:'name',
                        url:'/biz_bill_currency/get_currency/cost',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">
                </td>
                <td><?= lang('amount');?></td>
                <td>
                    <input name="cost_unit_price" id="cost_unit_price" class="easyui-numberbox" required="true" precision="2" style="width:70px;" data-options="
                        onChange:function(newVal, oldVal){
                            if($('#cost_number').numberbox('getValue') == ''){
                                $('#cost_number').numberbox('setValue', '1');
                            }
                            $('#cost_amount').numberbox('setValue', newVal * $('#cost_number').numberbox('getValue'));
                        },
                    ">
                </td>
                <td>X</td>
                <td>
                    <input name="cost_number" id="cost_number" class="easyui-numberbox" precision="4" required="true" style="width:60px;" data-options="
                    onChange:function(newVal, oldVal){
                        if($('#cost_unit_price').numberbox('getValue') == ''){
                            $('#cost_unit_price').numberbox('setValue', '1');
                        }
                        $('#cost_amount').numberbox('setValue', $('#cost_unit_price').numberbox('getValue') * newVal);
                    },">
                </td>
                <td>=</td>
                <td>
                    <input name="cost_amount" id="cost_amount" class="easyui-numberbox" readonly="readonly" required="true" precision="4" style="width:90px;">
                </td>
                <td><?= lang('vat');?></td>
                <td>
                    <select class="vat easyui-combobox" name="cost_vat" id="cost_vat" style="width: 58px;" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        value: '0%',
                        url:'/bsc_dict/get_option/vat',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    "></select>
                </td>
                <td><?= lang('vat_withhold');?></td>
                <td>
                    <select class="easyui-combobox" name="cost_vat_withhold" id="cost_vat_withhold" style="width: 58px;" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        value: '0%',
                        url:'/bsc_dict/get_option/vat_withhold',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    "></select>
                </td>
                <td>
                    <select class="easyui-combobox" editable="false" id="cost_split_mode" name="cost_split_mode">
                        <option value="0"><?= lang("按票");?></option>
                        <option value="1"><?= lang("按体积");?></option>
                        <option value="2"><?= lang("按立方");?></option>
                    </select>
                </td>
                <td><?= lang('created_by');?></td>
                <td>
                    <a href="javascript:void(0)" title="<?= lang("创建人");?>" class="easyui-tooltip">
                        <input class="easyui-textbox" name='cost_created_by' id="cost_created_by" disabled="disabled" style="width:60px">
                    </a>
                    <a href="javascript:void(0)" title="<?= lang("发票号");?>" class="easyui-tooltip">
                        <input class="easyui-textbox" name='cost_invoice_no' id="cost_invoice_no" disabled="disabled" style="width:90px">
                    </a>
                    <a href="javascript:void(0)" title="<?= lang("创建日期");?>" class="easyui-tooltip">
                        <input class="easyui-textbox" name='cost_created_time' id="cost_created_time" disabled="disabled" style="width:90px">
                    </a>
                </td>
            </tr>
        </table>
    </form>
    <div id="dlg-buttons">
        <?php if($lock_lv == 3){ ?>
            <!--<a href="javascript:save()" class="easyui-linkbutton" iconCls="icon-ok" ><?= lang('save');?></a>-->
            <?php if($shipment_lock_lv == 3){ ?>
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="bill_apply()"><?= lang('bill apply');?></a>
            <?php } ?>
        <?php } ?>
        <?php
        if($is_special != 1){
            if(menu_role('billing_special_bill')){
                echo '<a href="javascript:void(0)" class="easyui-linkbutton"  onclick="javascript:window.location.href=\'?is_special=1\';">' . lang('special') . '</a>';
            }
        }else{
            echo '<a href="javascript:void(0)" class="easyui-linkbutton"  onclick="javascript:window.location.href=\'/biz_bill/index/' . $id_type . '/' . $id_no. '\';">' . lang('non-specialty') . '</a>';
        }
        ?>
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="child_bill()"><?= lang('分摊详情');?></a>
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="sys_log()"><?= lang('日志');?></a>
        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="plsq()"><?= lang('批量申请支付');?></a>
    </div>
<?php } ?>
<div id="mm2" style="width:150px;display:none;">
    <div data-options="iconCls:'icon-remove'" onclick="javascript:del('cost');"><?= lang('creditor');?><?= lang('delete');?></div>
</div>
<div id="mm3" style="width:150px;display:none;">
    <div data-options="iconCls:'icon-ok'" onclick="javascript:batch_confirm_date('cost');"><?= lang('creditor');?><?= lang('confirm');?></div>
    <div data-options="iconCls:'icon-ok'" onclick="javascript:batch_confirm_self('cost');"><?= lang('creditor');?><?= lang('confirm');?>(<?= lang('editor');?>)</div>
</div>

<table id="tt"
       rownumbers="false" pagination="true" idField="id"
       pagesize="300" toolbar="#tb" singleSelect="true" nowrap="false" showFooter="true" style="height: 500px">
    <thead>
    <tr>
        <!--        <th data-options="field:'id',width:44,sortable:true">--><?//= lang('id');?><!--</th>-->
        <th data-options="field:'charge',width:300, formatter:charge_for"><?= lang('charge');?></th>
        <th data-options="field:'charge_code',width:60"><?= lang('charge_code');?></th>
        <th data-options="field:'cost',width:55,formatter:cost_for,styler:cost_all_style"><?= lang('cost');?></th>
        <th data-options="field:'cost_unit_price',width:90,sortable:true,formatter:cost_unit_price_for,styler:cost_all_style"><?= lang('cost_unit_price');?></th>
        <th data-options="field:'cost_number',width:60,formatter:cost_number_for,styler:cost_all_style"><?= lang('cost_number');?></th>
        <th data-options="field:'cost_amount',width:150,sortable:true,formatter:cost_amount_for,styler:cost_all_style"><?= lang('cost_amount');?></th>
        <th data-options="field:'cost_vat',width:55,formatter:cost_vat_for,styler:cost_all_style"><?= lang('cost_vat');?></th>
        <th data-options="field:'cost_confirm_date',width:85,formatter:cost_confirm_date_for,styler:cost_all_style">
            <?= lang('confirm_date');?><br>
            <span title='<?= lang('点击未确认可以确认费用。 点击已确认可以取消确认');?>' style='color:red;'>
            (<?= lang('确认');?>/<?= lang('取消');?>)
            </span></th>
        <th data-options="field:'creditor',width:230,formatter:creditor_for,styler:cost_all_style"><?= lang('creditor');?></th>
        <th data-options="field:'cost_split_mode',width:60,formatter:cost_split_mode_for,styler:cost_all_style"><?= lang('split_mode');?></th>
        <th data-options="field:'sell',width:65,formatter:sell_for,styler:sell_all_style"><?= lang('sell');?></th>
        <th data-options="field:'sell_unit_price',width:90,sortable:true,formatter:sell_unit_price_for,styler:sell_all_style"><?= lang('sell_unit_price');?></th>
        <th data-options="field:'sell_number',width:60,formatter:sell_number_for,styler:sell_all_style"><?= lang('sell_number');?></th>
        <th data-options="field:'sell_amount',width:120,sortable:true,formatter:sell_amount_for,styler:sell_all_style"><?= lang('sell_amount');?></th>
        <th data-options="field:'sell_vat',width:55,formatter:sell_vat_for,styler:sell_all_style"><?= lang('sell_vat');?></th>
        <th data-options="field:'sell_confirm_date',width:85,formatter:sell_confirm_date_for,styler:sell_all_style">
             <span title='<?= lang('点击未确认可以确认费用。 点击已确认可以取消确认');?>'>
            <?= lang('sell_confirm_date');?>
        </th>
        <th data-options="field:'debitor',width:230,formatter:debitor_for,styler:sell_all_style"><?= lang('debitor');?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()">add</a>-->

                <!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true"-->
                <!--                   onclick="javascript:$('#tt').edatagrid('saveRow')">save</a>-->
                <!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true"-->
                <!--                   onclick="javascript:$('#tt').edatagrid('cancelRow')">cancel</a>-->
            </td>
        </tr>
    </table>

</div>

<!--批量费用确认-start-->
<div id="batch_confirm_date" class="easyui-dialog" style="width:300px;height:150px" data-options="title:'window',buttons:'#bcd',modal:true,closed:true">
    <input type="hidden" name="ids">
    <table>
        <!--<tr>
            <td><?= lang('account_no');?>:</td>
            <td>
                <input class="easyui-textbox" id="account_no">
            </td>
        </tr>-->
        <tr>
            <td><?= lang('confirm_date');?>:</td>
            <td>

                <input class="easyui-datebox" required id="confirm_date">
            </td>
        </tr>
    </table>
</div>
<div id="bcd">
    <a href="javascript:void(0);" onclick="save_batch_confirm()" class="easyui-linkbutton"><?= lang('save');?></a>
</div>
<!--批量费用确认-end-->

<!--开票申请-->
<div id="invoice_apply" class="easyui-dialog" style="width:400px;height:300px" data-options="title:'window',buttons:'#iab',modal:true,closed:true">
    <form id="invoice_apply_from">
        <input type="hidden" name="ids">
        <table>
            <tr>
                <td><?= lang('buyer_company');?></td>
                <td>
                    <select id="buyer_company" class="easyui-combobox" style="width:250px;" readonly data-options="
                        valueField:'company_name',
                        textField:'company_name',
                        url:'/biz_client/get_option/0?q_field=company_name',
                        mode: 'remote',
                        onBeforeLoad:function(param){
                            if($(this).combobox('getValue') == '')return false;
                            if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                        },
                        onLoadSuccess:function(){
                            var val = $(this).combobox('getValue');
                            $(this).combobox('clear').combobox('select', val);
                        },
                        onSelect: function(res){
                            $('#invoice_apply input[name=\'buyer_client_code\']').val(res.client_code);
                            $('#invoice_apply input[name=\'buyer_company\']').val(res.taxpayer_name);
                            $('#invoice_apply input[name=\'buyer_taxpayer_id\']').val(res.taxpayer_id);
                        },
                    ">
                    </select>
                    <input type="hidden" name="buyer_client_code">
                    <input type="hidden" name="buyer_company">
                    <input type="hidden" name="buyer_taxpayer_id">
                </td>
            </tr>
            <tr>
                <td><?= lang('currency');?></td>
                <td>
                    <input name="currency" class="easyui-textbox" readonly style="width:250px;">
                </td>
            </tr>
            <tr>
                <td><?= lang('vat');?></td>
                <td>
                    <input type="text" name="vat" readonly class="easyui-textbox" style="width:250px;">
                </td>
            </tr>
            <tr>
                <td><?= lang('invoice_type');?></td>
                <td>
                    <select name="invoice_type" editable="false" class="easyui-combobox" style="width:250px;"  data-options="
                        required:true,
                        valueField:'value',
                        textField:'name',
                        url:'/bsc_dict/get_option/invoice_type',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('invoice_amount');?></td>
                <td>
                    <input name="invoice_amount" class="easyui-numberbox" readonly precision="4" style="width:250px;">
                </td>
            </tr>
            <tr>
                <td><?= lang('invoice_company');?></td>
                <td>
                    <select name="invoice_company" id="invoice_company" class="easyui-combobox" style="width:250px;" data-options="
                        required: true,
                        valueField:'value',
                        textField:'name',
                        url:'/bsc_dict/get_option/invoice_title',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('remark');?></td>
                <td>
                    <textarea name="remark" id="remark" style="width:250px;"></textarea>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="iab">
    <a href="javascript:void(0);" onclick="save_invoice_apply()" class="easyui-linkbutton"><?= lang('save');?></a>
</div>

<div id="child_bill" style="display:none;">
    <iframe id="child_bill_iframe" style="width:750px;height:455px;padding:5px;border:0px;"></iframe>
</div>

<div id="bill_apply" style="display:none;">
    <iframe id="bill_apply_iframe" style="width:810px;height:375px;padding:5px;border:0px;"></iframe>
</div>

<div id="sys_log" style="display:none;">
    <iframe id="sys_log_iframe" style="width:1110px;height:675px;padding:5px;border:0px;"></iframe>
</div>

<div id="apply_payment_wlk" style="display:none;">
    <iframe id="apply_payment_wlk_iframe" style="width:1110px;height:675px;padding:5px;border:0px;"></iframe>
</div>

<div class="tips_bill">
    <table>
        <?php
        $sum_packs = 0;
        $sum_weight = 0;
        $sum_volume = 0;
        foreach($containers as $container){
            $sum_packs += $container['packs'];
            $sum_weight += $container['weight'];
            $sum_volume += $container['volume'];
            ?>
        <?php } ?>
        <tr>
            <td><?= lang('箱型箱量');?></td>
            <td>
                <?php
                $container_info = array();
                foreach($container_sizes as $c_key => $container_size){?>
                    <?php $container_info[] = $c_key . 'X' . $container_size;?>
                <?php } ?>
                <input class="easyui-textbox" value="<?= join(',', $container_info);?>" readonly="readonly">

            </td>
            <td><?= lang('订舱箱型箱量');?></td>
            <td>
                <?php
                $box_info_str = array();
                foreach($box_info as $row){
                    $box_info_str[] = $row['size'] . 'X' . $row['num'];
                }
                ?>
                <input class="easyui-textbox" value="<?= join(',', $box_info_str);?>" readonly="readonly">

            </td>
            <td><?= lang('trans_destination');?></td>
            <td>
                <input class="easyui-textbox" value="<?= $trans_destination;?>" readonly="readonly">
            </td>
            <td><?= lang('packs');?></td>
            <td>
                <input class="easyui-textbox" value="<?= $sum_packs;?>" readonly="readonly">
            </td>
            <td><?= lang('weight');?></td>
            <td>
                <input class="easyui-textbox" value="<?= $sum_weight;?>" readonly="readonly">
            </td>
            <td><?= lang('volume');?></td>
            <td>
                <input class="easyui-textbox" value="<?= $sum_volume;?>" readonly="readonly">
            </td>
        </tr>
    </table>
</div>

<!--consol分成-->
<div id="consol_percent" class="easyui-dialog" style="width:500px;height:400px" data-options="title:'window',modal:true,closed:true">
    <form id="consol_percent_form">
        <input type="hidden" name="id">
        <table class="cp_table1">
            <tr>
                <td>
                    <input type="radio" class="split_mode" name="split_mode" value="0"><?= lang('按票');?>
                </td>
                <td>
                    <input type="radio" class="split_mode" name="split_mode" value="1"><?= lang('体积');?>
                </td>
                <td>
                    <input type="radio" class="split_mode" name="split_mode" value="2"><?= lang('重量');?>
                </td>
                <td>
                    <input type="radio" class="split_mode" name="split_mode" value="3"><?= lang('自定义');?>
                </td>
                <td>
                    <input type="radio" class="split_mode" name="split_mode" value="4"><?= lang('按箱');?>
                </td>
            </tr>
            <tr class="split_mode_tr split_mode_4_tr hide">
                <td colspan="4">
                    <span><?= lang('请输入指定分配的箱型');?>: </span>
                    <select class="easyui-combobox" name="box_size" id="consol_percent_box_size" style="width: 100px;" data-options="
                        editable:false,
                        onSelect:function(rec){
                            var val = $('.split_mode:checked').val();
                            var row = $('#tt').datagrid('getSelected');
                            var box_size = $('#consol_percent_box_size').combobox('getValue');
                            //当是per container时, 先填写当前分配的箱型, 再进行获取
                            get_shipments_bill_percent(val,row.cost_id, box_size);
                        }
                    ">
                        <?php foreach ($box_info as $val){ ?>
                            <option value="<?= $val['size'];?>"><?= lang($val['size']);?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table class="cp_table2" id="shipment_consol_percent_info"  border="0" cellpadding="4" cellspacing="1">
                        <thead>
                        <tr>
                            <th><?= lang('job_no');?></th>
                            <th><?= lang('分摊');?></th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <a href="javascript:void(0);" id="cpb" onclick="save_consol_percent()" class="easyui-linkbutton"><?= lang('save');?></a>
                </td>
                <td></td>
            </tr>
        </table>
    </form>
</div>

<!--client_code修改-->
<div id="client_code_div" class="easyui-window" style="width:400px;height:100px" data-options="title:'window',modal:true,closed:true">
    <form id="client_code_form">
        <input type="hidden" name="id">
        <table>
            <tr>
                <td><a href="javascript:void(0);" onclick="reload_client(1)"><?= lang('修改');?></a></td>
                <td>
                    <input name="client_code" id="update_div_client_code" class="easyui-combobox" style="width:150px;" data-options="
                        required:true,
                        valueField:'client_code',
                        textField:'company_name',
                        value: '<?php echo $creditor_default;?>',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">

                </td>
                <td colspan="4"><?= lang("添加其他抬头请点击\"{这里}\"刷新", array('这里' => '<a href="javascript:void(0);" onclick="reload_client(0)">这里</a>'));?></td>
            </tr>
            <tr>
                <td colspan="6"> </td>
            </tr>
            <tr>
                <td colspan="6">
                    <button onclick="client_code_save()" type="button" class="easyui-linkbutton" style="width: 180px;"><?= lang('save');?></button>
                </td>
            </tr>
        </table>
    </form>
</div>

<!--汇率日期修改-->
<div id="ex_rate_date_div" class="easyui-window" style="width:570px;height:370px" data-options="title:'window',modal:true,closed:true">
    <form id="ex_rate_date_form">
        <input type="hidden" name="id" value="<?= $consol_id;?>">
        <table style="margin-left: 10px;">
            <tr>
                <td><?= lang("汇率日期");?></td>
                <td>
                    <input class="easyui-datebox" name="ex_rate_date" value="<?= $ex_rate_date;?>" style="width:150px" data-options="onSelect: function(date){
                        var date_str = date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
                        var json = {f1:'start_time',s1:'=',v1:date_str};
                        $('#ex_rate_date_table').datagrid({
                            queryParams: json
                        }).datagrid('clearSelections');
                    }">
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table id="ex_rate_date_table" class="easyui-datagrid" style="width:520px;height:250px" data-options="url:'/biz_bill_currency/get_data/',queryParams:{f1:'start_time',s1:'=',v1:'<?= $ex_rate_date;?>'}">
                        <thead>
                        <tr>
                            <th field="currency_from" width="100" align="center"><?= lang('currency_from');?></th>
                            <th field="currency_to" width="100" align="center"><?= lang('currency_to');?></th>
                            <th field="sell_value" width="100"><?= lang('sell_value');?></th>
                            <th field="cost_value" width="100"><?= lang('cost_value');?></th>
                            <th field="start_time" width="100"><?= lang('start_time');?></th>
                        </tr>
                        </thead>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <button onclick="ex_rate_date_save()" type="button" class="easyui-linkbutton" style="width: 180px;"><?= lang('save');?></button>
                </td>
            </tr>
        </table>
    </form>
</div>
