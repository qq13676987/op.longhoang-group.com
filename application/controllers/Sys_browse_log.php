<?php


class Sys_browse_log extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_client_model');
        $this->load->model('bsc_user_model');
        $this->load->model('m_model');
        $this->db = Model('op_model')->get_db();
    }

    public function index(){
        $type = getValue('type', '');
        $this->load->view('head');
        $this->load->view('/sys/browse_log/index', array('type'=>$type));
    }

    public function get_data(){
        $type = getValue('type', '');
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

        $where = array();
        if (!empty($type)) $where[] = "id_type = '{$type}'";
        if (!empty($where)) $where = join(' and ', $where);


        $offset = ($page - 1) * $rows;
        $this->db->limit($rows, $offset);
        $query = $this->db->where($where, null, false)->order_by($sort, $order)->get('sys_browse_log');
        //echo $this->db->last_query();exit;
        $rs = $query->result_array();
        $this->db->clear_limit();//清理下limit
        $result["total"] = $this->db->count_all_results('');
        $result['rows'] = $rs;

        return $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function get_detail(){
        $id_type = getValue('id_type', '');
        $id_no = getValue('id_no', 0);
        if ($id_type == '' || (int)$id_no == 0){
            $this->load->view('/errors/html/error_layui', array('msg'=>'请求内容不存在！', 'icon'=>'layui-icon-404'));
        }
        $where = array('id_type'=>$id_type, 'id_no'=>$id_no);
        $this->db->select('id, count(user_id) as num, create_time, user_id');
        $data = $this->db->where($where)->order_by('id', 'desc')->group_by('user_id')->get('sys_browse_log')->result_array();
        foreach ($data as $index => $row){
            $row['user_name'] = getUserName($row['user_id']);
            $row['user_group'] = $this->db->where('group_code', getUserField($row['user_id'], 'group'))->get('bsc_group')->row_array()['group_name'];
            $data[$index] = $row;
        }
        $this->load->view('/sys/browse_log/log_detail', array('data'=>$data));
    }
}