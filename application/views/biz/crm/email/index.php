<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>无效邮箱</title>
    <link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/icon.css?v=3">
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="/inc/js/easyui/jquery.easyui.min.js?v=11"></script>
    <script type="text/javascript" src="/inc/js/easyui/jquery.edatagrid.js?v=5"></script>
    <script type="text/javascript" src="/inc/js/easyui/datagrid-detailview.js"></script>
    <script type="text/javascript" src="/inc/js/easyui/locale/easyui-lang-zh_CN.js"></script>



</head>
<body> 
<table id="dg" class="easyui-datagrid" title="无效邮箱
" style="width:100%;height:700px"
       data-options="singleSelect:true,url:'/biz_crm_email/get_data',method:'get'"

       rownumbers="false"
        >
    <thead>
    <tr>
        <th data-options="field:'id',width:80">ID</th>
        <th data-options="field:'client_code',width:100">客户码</th>
        <th data-options="field:'company_name',width:300">公司名</th>
        <th data-options="field:'email',width:300">邮箱</th>
        <th data-options="field:'create_by_name',width:300">销售人员</th>
        <th data-options="field:'act',width:300">操作</th>

    </tr>
    </thead>
</table>
<script>
    $('#dg').datagrid({
        width: 'auto',
        height: $(window).height() - 29,
    });
    function up_email(email){
        $.getJSON("/biz_crm_email/up_mail?email="+email, function(json){
            if(json.code==200){
                $.messager.alert('提示', json.msg, 'info', function() {
                    $('#dg').datagrid('reload');
                });
            }
        });
    }

</script>
</body>
</html>