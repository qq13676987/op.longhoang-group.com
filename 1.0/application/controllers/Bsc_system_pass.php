<?php
class Bsc_system_pass extends Common_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bsc_system_pass_model');
        $this->model = $this->bsc_system_pass_model;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }

    public $field_edit = array();

    public $field_all = array(
        array("id", "id", "60", "2", '1'),
        array("MAC", "MAC", "150", "2", '1'),
        array("name", "name", "94", "2", '1'),
        array("telephone", "telephone", "94", "2", '1'),
        array("status", "status", "94", "2", '1'),
        array("remark", "remark", "200", "2", '1'),
        array("created_time", "created_time", "94", "2", '1'),
    );

    public function index(){
        if (!is_admin()){
            exit ("你不是管理员");
        }

        $data = array();

        $data["f"] = $this->field_all;
        $this->load->view('head');
        $this->load->view('bsc/system_pass/index_view',$data);
    }

    public function get_data(){

        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $where = array();
        $id = isset($_REQUEST['id']) ? ($_REQUEST['id']) : '';
        $name = isset($_REQUEST['name']) ? ($_REQUEST['name']) : '';
        if($id !== '')$where[] = "id = '$id'";
        if($name !== '')$where[] = "name like '%$name%'";
        $where = join(' and ', $where);

        $offset = ($page-1)*$rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $rs = $this->model->get($where,$sort,$order);
        $rows = array();
        foreach ($rs as $row){
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        return jsonEcho($result);
    }

    public function update_data($id=0){
        $field = $this->field_edit;
        $data= array();
        foreach ($field as $item){
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if($temp != '')$data[$item] = $temp;
        }
        $this->model->update($id, $data);
        $data["id"]  = $id;

        // record the log
        $log_data = array();
        $log_data["table_name"] = "bsc_system_pass";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        return jsonEcho($data);
    }

    //1 进入页面,根据当前设备MAC进行查询,
    //1.1 存在值,
    //1.1.1 允许进入的,放行
    //1.1.2 不允许进入和禁止的, 提示当前机器不允许通行,等待放行或联系XX人
    //1.2 不存在值, 进入2
    //2 获取设备信息, 填写姓名手机号备注提交,进入3
    //3 提交后,进入1.1.2
    public function pass_apply(){
        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            if (isset($_POST[$item])) {
                $temp = $_POST[$item];
                $data[$item] = $temp;
                if (isset($data[$item]) && is_string($data[$item])) $data[$item] = trim($data[$item]);
            }
        }
        $system_pass = $this->model->get_one('MAC', $data['MAC']);

        $array = array('MAC' => $data['MAC'],'name' => $data['name'], 'telephone' => $data['telephone'], 'end_time' => time());
        if(!empty($system_pass)){
            $array['id'] = $system_pass['id'];
            if($system_pass['status'] == 1){
                $array['end_time'] = time() + 365 * 24 * 3600;
            }
            $ea = pricate_encrypt(json_encode($array));
            setcookie('computer_code',$ea,time() + 365 * 24 * 3600,'/');

            echo json_encode(array('code' => 1, 'msg' => lang('已存在,无法再次申请')));
            return;
        }

        $id = $this->model->save($data);

        $array['id'] = $id;
        $ea = pricate_encrypt(json_encode($array));
        setcookie('computer_code',$ea,time() + 365 * 24 * 3600,'/');

        // record the log
        $log_data = array();
        $log_data["table_name"] = "bsc_system_pass";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        echo json_encode(array('code' => 0, 'msg' => lang('申请成功')));
    }

    public function ts_lg(){
        //http://oversea.leagueshipping.com/bsc_system_pass/ts_lg?de=klin
        $de = isset($_GET['de']) ? $_GET['de'] : '';
        // if($de == 'mlf') $_GET['MAC'] = 'A8-A7-95-7D-9A-DF';
        if(!isset($_GET['MAC'])){
            echo '该通道已关闭';
            return;
        }
        $this->get_pass();
        //将数组中后续需要的信息保存缓存，供后续使用
        if(isset($_GET['MAC'])){
            echo '<script language="javascript">';
            echo 'localStorage.setItem("MAC", \'' . $_GET['MAC'] . '\');';
            echo '</script>';
            redirect('desktop', 'refresh');
        }
    }

    public function add(){
        $data['MAC'] = "B0-BE-83-60-C9-F4";
        $data['telephone'] = '17621353418';
        $data['name'] = "张园园";
        $data['remark'] = "家庭电脑";
        $data['status'] = 0;

        $id = $this->model->save($data);
    }

    public function get_pass(){
        $MAC = isset($_GET['MAC']) ? $_GET['MAC'] : '';
        if($MAC == ''){
            echo json_encode(array('code' => 1, 'msg' => lang('未获取到设备配置')));
            return;
        }
        $pass = $this->model->get_one('MAC', $MAC);
        if(empty($pass)){
            echo json_encode(array('code' => 2, 'msg' => lang('未申请')));
            return;
        }
        $array = array('MAC' => $pass['MAC'],'name' => '', 'telephone' => $pass['telephone'], 'end_time' => time());
        $array['id'] = $pass['id'];
        if($pass['status'] == 1) $array['end_time'] = time() + 365 * 24 * 3600;
        $json = json_encode($array);
        // $json = '{"MAC":"50-2B-73-D5-07-43","name":"\u4e0a\u5b98\u9038\u6f9c","telephone":"13776283050","end_time":1652863779,"id":"222"}';
        $ea = pricate_encrypt($json);
        //echo $ea;
        setcookie('computer_code',$ea,time() + 365 * 24 * 3600,'/');
        //echo $_COOKIE["computer_code"];

        if($pass['status'] == 1){
            echo json_encode(array('code' => 0, 'msg' => lang('获取成功')));
        }else{
            echo json_encode(array('code' => 3, 'msg' => lang('未通过')));
        }
    }

    /**
     * 获取当前设备通行信息
     */
    public function check_pass(){
        $computer_code = json_decode(public_decrypt($_COOKIE['computer_code']), true);
        $MAC = isset($computer_code['MAC']) ? $computer_code['MAC'] : '';
        if ($MAC == '') {
            if (isset($_COOKIE['computer_code'])) {
                setcookie('computer_code', '', time() - 1, '/');
            }
            //这个要重新进入验证页面 申请验证
            redirect('bsc_system_pass/auth', 'refresh');
        }
        $this->db->select("id,MAC,name,telephone,status ");
        $pass_row = Model('bsc_system_pass_model')->get_where_one("MAC = '{$MAC}'");
        //如果不存在申请信息,这里直接进入申请页面重新申请
        if (empty($pass_row)) {
            if (isset($_COOKIE['computer_code'])) {
                setcookie('computer_code', '', time() - 1, '/');
            }
            redirect('bsc_system_pass/auth', 'refresh');
        }
        $array = array('MAC' => $pass_row['MAC'], 'name' => $pass_row['name'], 'telephone' => $pass_row['telephone'], 'end_time' => time());
        $array['id'] = $pass_row['id'];
        if ($pass_row['status'] == 1) {
            $array['end_time'] = time() + 24 * 3600;
        }
        $ea = pricate_encrypt(json_encode($array));
        setcookie('computer_code', $ea, time() + 360000, '/');
        if ($pass_row['status'] == 1) {
            echo "<script> location.href='/'; </script>";
        } else if ($pass_row['status'] == 0) {
            echo lang("设备编号: {computer_id} 正在申请中", array('computer_id' => $pass_row['id']));
        } else if ($pass_row['status'] == 2) {
            echo lang("设备编号: {computer_id} 已被拒绝", array('computer_id' => $pass_row['id']));
        }
    }

    /**
     * 申请验证页面
     */
    public function auth(){
        //公共加入设备验证
        header("Access-Control-Allow-Origin: * ");
        if(isset($_COOKIE['computer_code'])){
            redirect('bsc_system_pass/check_pass', 'refresh');
        }

        $data = array();

        $this->load->view("head");
        $this->load->view("/bsc/system_pass/auth_view", $data);
    }
}