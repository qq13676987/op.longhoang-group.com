<?php
class Common_Controller extends CI_Controller{
    protected $start_exec;
    protected $end_exec;
    protected $is_log_url = false;
    protected $is_load_session = true;

    public function __construct()
    {
        parent::__construct();
        $this->benchmark->mark('code_start');
        //获取当前控制器名
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();

        $ts_action_arr = array(
            '/main/wx_login_handle',
            '/main/auto_login',
            '/bsc_upload/get_xinhai_ftp',
            '/api/auto_update_consol_port',
            '/export/export_word',
            '/api/pdf',
            '/bsc_upload/wb_file_upload',
            '/api/wlk_apply_one',
            '/biz_quotation/get_list',
            '/biz_quotation/get_data_by_shipping_schedule',
            // '/bsc_system_pass/index',
            // '/bsc_system_pass/get_data',
            '/bsc_system_pass/check_pass',
            '/bsc_system_pass/auth',
            '/bsc_system_pass/get_pass',
            '/bsc_system_pass/pass_apply',
            '/main/get_user_range_api',
            '/api_weixin/login_wechat_check',
        );
        //这个可以在控制器开启个变量, 需要跳过的手动设置下, 不过这里就先这样吧
        $ts_class = array(
            'api',
            'other',
            'api_weixin',
            'api_china',
            'api_yunlsp',
            'sys_queue',
            'bsc_message',
            'download',
        );

        //不是放行的函数,或控制器时, 会进入相关的判断
        $is_pass = in_array("/{$class}/{$method}", $ts_action_arr) || in_array($class, $ts_class);

        //未存在验证信息时
        if(!isset($_COOKIE['computer_code'])){
            //如果不是默认放行的方法, 这里 进入验证页面
            if (!$is_pass) {
                if(!is_ajax()) {
                    redirect('bsc_system_pass/auth', 'refresh');
                }else {
                    echo json_encode(array('code' => 403, 'msg' => lang("设备验证未通过, 请刷新页面后再试")));
                }
                exit;
            }
        }else{
            //存在验证信息时
            $computer_code = json_decode(public_decrypt($_COOKIE['computer_code']), true);
            //判断当前验证是否过期,过期了 重新进入验证页面
            if(!$is_pass && $computer_code['end_time'] <= time()){
                if(!is_ajax()) {
                    redirect('bsc_system_pass/check_pass', 'refresh');
                }else {
                    echo json_encode(array('code' => 403, 'msg' => lang("设备验证已过期, 请刷新页面后再试")));
                }
                exit;
            }
        }

        //这里需要设置一些经常调用的方法进行过滤,不然记录会太多了
        $no_log_url_method = array();//需要跳过记录的方法
        $no_log_url_class = array('sys_desktop');//需要跳过记录的方法
        $no_log_url_class_and_method = array('main/index', 'bsc_help_content/help','sys_desktop/count', 'biz_shipment/index_by_consol', 'bsc_help_content/help_carrier', 'api/bw_search_company', 'api_weixin/login_wechat_check', 'api_yunlsp/e_tuoche_voyApi', 'biz_consol/sel_carrier_ref', 'biz_bill/auto_remark', 'biz_consol/sel_agent_ref', 'biz_client/search', 'bsc_dict/by_s_get_client_account', 'api_qcc/GetList');//需要跳过记录的控制器/方法
        $must_log_url_class_and_method = array('biz_containershipment/get_container_by_consol', 'biz_containershipment/get_container');
        //get前缀的都排除掉
        if(!in_array($class, $no_log_url_class) && !in_array($method, $no_log_url_method) && !in_array($class . '/' . $method, $no_log_url_class_and_method) && substr($method, 0, 3) !== 'get'){
            $this->is_log_url = true;
        }
        if(in_array($class . '/' . $method, $must_log_url_class_and_method)) $this->is_log_url = true;
        $this->end_exce = microtime(true);
        if($this->is_load_session) {
            $this->load->library('session');
        }
        //记录日志加在这里,顺便记录执行时间
        //这里加入新的访问流程记录
//        if($this->is_load_session && $this->is_log_url)log_url_rcd(0);

        setLang($class);
        
        if(!defined('UPLOAD_FILE')) define('UPLOAD_FILE',		            './upload/' . get_system_type());//CONSOL的编号前缀
    }
    
    public function __destruct() {
        //
        $this->benchmark->mark('code_end');
        if($this->is_load_session && $this->is_log_url) log_url_rcd($this->benchmark->elapsed_time('code_start', 'code_end') * 10000);
    }
}

class Menu_Controller extends Common_Controller{
    public function __construct()
    {
        parent::__construct();

        //菜单权限的设置
        $userId = get_session('id');
        $admin_login = get_session('admin_login');
        $computer_code = json_decode(public_decrypt($_COOKIE['computer_code']), true); // 获取设备code
        Model('bsc_system_pass_model');
        $this_computer = $this->bsc_system_pass_model->get_where_one("id = '{$computer_code['id']}'"); // 获取设备所有人
        //获取当前控制器名
        $class = $this->router->fetch_class();
        $method = $this->router->fetch_method();

        if(empty($this_computer)){
            if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"){
                // ajax 请求的处理方式
                echo json_encode(array('code' => 403, 'msg' => lang("当前设备不存在,已拒绝当前请求"), 'data' => array('url' => '/bsc_system_pass/check_pass')));
                exit;
            }else{
                echo "<script>alert('" . lang('当前设备不存在,已拒绝当前请求。') . "');</script>";
                redirect('bsc_system_pass/check_pass', 'refresh');
            }
        }else{
            if($this_computer['status'] == 0){
                if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"){
                    // ajax 请求的处理方式
                    echo json_encode(array('code' => 403, 'msg' => lang("设备未通过审核,已拒绝当前请求。当前设备编号为({computer_id})", array('computer_id' => $this_computer['id'])), 'data' => array('url' => '/bsc_system_pass/check_pass')));
                    exit;
                }else{
                    echo "<script>alert('" . lang("设备未通过审核,已拒绝当前请求。当前设备编号为({computer_id})", array('computer_id' => $this_computer['id'])) . "');</script>";
                    redirect('bsc_system_pass/check_pass', 'refresh');
                }
            }else if($this_computer['status'] == 2){
                if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"){
                    // ajax 请求的处理方式
                    echo json_encode(array('code' => 403, 'msg' => lang("设备已被禁用,已拒绝当前请求。当前设备编号为({computer_id}), 如果需要继续使用,那么请联系 {contact_name} 提供当前使用人的: 姓名 手机号", array('computer_id' => $this_computer['id'], 'contact_name' => '田诗雯')), 'data' => array('url' => '/bsc_system_pass/check_pass')));
                    exit;
                }else{
                    echo "<script>alert('" . lang("设备已被禁用,已拒绝当前请求。当前设备编号为({computer_id}), 如果需要继续使用,那么请联系 {contact_name} 提供当前使用人的: 姓名 手机号", array('computer_id' => $this_computer['id'], 'contact_name' => '田诗雯')) . "');</script>";
                    redirect('bsc_system_pass/check_pass', 'refresh');
                }
            }
        }
        if($userId){ // 判断是否登录 如果没有cookie,值为false,代表未登录
            //用于管理员登录其他人账号使用的,方便排查问题
			$no_lock_url = array('/Bsc_notice/report', '/Bsc_notice/index2','/bsc_notice/lock_screen', '/bsc_notice/get_message', '/bsc_notice/get_lock_message', '/bsc_user/anniversary');
			if(!is_ajax() && !in_array('/' . $class . '/' . $method, $no_lock_url)){
				$more_href_arr = array();
				$is_lock_screen = false;
				$all_notice = Model('bsc_notice_model')->get("notice_user = '{$userId}' and status = 0");
				foreach ($all_notice as $item){
					if ($item['lock_screen'] == 1 && strtotime($item['lock_screen_datetime']) < time()){
						$is_lock_screen = true;
					}
					if (!empty($item['more_href'])){
						array_push($more_href_arr, ...json_decode($item['more_href'], true));
					}

					array_push($more_href_arr, $item['href']);
				}
				$length_status = false;
				$uri = urldecode($_SERVER['REQUEST_URI']);
				foreach ($more_href_arr as $u){
					if(substr($uri,0,strlen($u)) == $u) $length_status = true;
				}
				if($is_lock_screen && !$length_status){
					echo "<script>alert('to-do list need to be handled, system is jumping to the page');</script>";
					redirect('/bsc_notice/lock_screen?back_url=' . $_SERVER['REQUEST_URI'], 'refresh');
					exit;
				}
			}
            if($admin_login != 'admin'){
                Model('bsc_user_model');
                $this->db->select('login_system_id');
                $is_login_this_system = $this->bsc_user_model->get_by_id($userId);//查询当前用户登录的ID是谁
                $pass_user = array(20057,20060,20061); //放行用户ID
                //如果不是当前账号,或,不是设置的默认放行用户,那么提示账号已被登录
                if(!is_admin() && $is_login_this_system['login_system_id'] != $computer_code['id'] && !in_array($userId, $pass_user)) {
                    //获取设备名称
                    $this->db->select('id,name');
                    $computer = $this->bsc_system_pass_model->get_one('id', $is_login_this_system['login_system_id']); // 获取设备所有人
                    if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"){
                        //2022-09-28 已修复 如果这里查不到时,会报错的问题
                        if(empty($computer)){
                            $computer['name'] = '';
                            $computer['id'] = $is_login_this_system['login_system_id'];
                        }
                        $this->session->sess_destroy();
                        // ajax 请求的处理方式
                        echo json_encode(array('code' => 403, 'msg' => lang('您的账号已被{computer_name}登录,设备ID为{computer_id},请重新登录后再试', array('computer_name' => $computer['name'], 'computer_id' => $computer['id'])), 'data' => array('url' => '/main/')));
                        exit;
                    }else{
                        $this->session->sess_destroy();
                        echo "<script>alert('" . lang('您的账号已被{computer_name}登录,设备ID为{computer_id}', array('computer_id' => $computer['id'], 'computer_name' => $computer['name'])) . "');</script>";
                        redirect('main/login?back_url=' . $_SERVER['REQUEST_URI'], 'refresh');
                    }
                }
            }
        }else{
            if(isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest"){
                // ajax 请求的处理方式
                echo json_encode(array('code' => 403, 'msg' => lang('登录已过期，请重新登录后再试'), 'data' => array('url' => '/main/')));
                exit;
            }else{
                echo "<script>alert('" . lang('登录已过期，请重新登录后再试') . "');</script>";
                redirect('main/login?back_url=' . $_SERVER['REQUEST_URI'], 'refresh');
            }
        }
        // $this->session->set_userdata('station', getUserField(get_session('id'), 'station'));
        //如果登录成功,没问题,那么这里进行权限的判定
        // if(is_admin()){
        //     Model('bsc_menu_relation_model');
        //     $this_menu = $this->bsc_menu_relation_model->getInfo();
        //     //如果不存在权限里,那么直接不给访问
        //     if($this_menu){
        //         if (!$this->bsc_menu_relation_model->checkAuth($this_menu['id']) &&
        //             $this_menu['url'] != 'desktop') {
        //             exit(lang("访问权限不足"));
        //         }
        //     }else{
        //         //如果不是ajax请求,那么提示
        //         if(!is_ajax()) exit(lang('请联系管理员添加节点'));
        //     }
        // }
    }
}
