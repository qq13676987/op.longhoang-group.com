<link href="/inc/third/layui/css/layui.css" rel="stylesheet" type="text/css">
<style>
	textarea,input[type="text"]{
		margin:5px 0;
	}
	.textbox{
		margin:5px 0;
		height: 26px !important;
	}
</style>
<script>
	function create_company_tool(div_id, id){
		var html = '<div id="' + div_id + '_div">' +
			'<form id="' + div_id + '_form" method="post">' +
			'<div style="padding-left: 5px;display: inline-block;">' +
			'<label><?= lang('company_name');?>:</label><input name="company_name" class="easyui-textbox keydown_search" t_id="' + id + '" style="width:96px;"/>' +
			'</div>' +
			'<form>' +
			'<div style="padding-left: 1px; display: inline-block;">' +
			'<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="' + id + '_click" onclick="query_report(\'' + div_id + '\', \'' + id + '\')"><?= lang('search');?></a>' +
			'</div>' +
			'</div>';
		$('body').append(html);
		$.parser.parse('#' + div_id + '_div');
		return div_id;
	}

	function create_client_tool(div_id, id){
		var html = '<div id="' + div_id + '_div">' +
			'<form id="' + div_id + '_form" method="post">' +
			'<div style="padding-left: 5px;display: inline-block;">' +
			'<label><?= lang('client_code');?>:</label><input name="client_code" class="easyui-textbox keydown_search" t_id="' + id + '" style="width:96px;"/>' +
			'<label><?= lang('company_name');?>:</label><input name="company_name_or" class="easyui-textbox keydown_search" t_id="' + id + '" style="width:96px;"/>' +
			'</div>' +
			'<form>' +
			'<div style="padding-left: 1px; display: inline-block;">' +
			'<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="' + id + '_click" onclick="query_report(\'' + div_id + '\', \'' + id + '\')"><?= lang('search');?></a>' +
			'<span class="cliet_code2_label"></span>' +
			'</div>' +
			'</div>';
		$('body').append(html);
		$.parser.parse('#' + div_id + '_div');
		//if(div_id == 'client_code2'){
		//	$('.cliet_code2_label').html(`<a href="javascript:void(0);" style='margin-left:20px;color:#000' onclick="window.open('/biz_client_apply/add_client_base')"><?//= lang('新增纯开票抬头');?>//</a>`)
		//}
		return div_id;
	}

	function query_report(div_id, load_id){
		var where = {};
		var form_data = $('#' + div_id + '_form').serializeArray();
		$.each(form_data, function (index, item) {
			if(item.value == "") return true;
			if(where.hasOwnProperty(item.name) === true){
				if(typeof where[item.name] == 'string'){
					where[item.name] =  where[item.name].split(',');
					where[item.name].push(item.value);
				}else{
					where[item.name].push(item.value);
				}
			}else{
				where[item.name] = item.value;
			}
		});
		if(div_id == 'client_code2'){
			if(where['client_code'] == '' && where['company_name'] == '' ){
				alert('Search value');
				return;
			}
			if(where['company_name'] && where['company_name'].length < 4 ){
                alert('Input at lease 4 charactors');
				return;
			}
		}
		if(where.length == 0){
			$.messager.alert('Tips', 'Search value');
			return;
		}
		$('#' + load_id).combogrid('grid').datagrid('load',where);
	}

	//text添加输入值改变
	function keydown_search(e, click_id){
		if(e.keyCode == 13){
			$('#' + click_id + '_click').trigger('click');
		}
	}
	function change_sft_client(type = 1){
		var client_code = '';
		if(type == 1){
			client_code = $('#client_code').textbox('getValue');
			if(!client_code){
                alert('Please select client 1')
				return;
			}
		}
		if(type == 2){
			client_code = $('#client_code2').combogrid('getValue');
			if(!client_code){
                alert('Please select client B')
				return;
			}
		}
		changeClient2({client_code})
	}
	function changeClient(data, is_load = false){
		// $.each($('.userList'), function (index,item) {
		//     var id = $(this).attr('id').replace('_id','');
		//     if(id !== 'finance'){
		//         var key = id + '_ids';
		//         if(data[key] === undefined || data[key] == '')data[key] = 0;
		//         $(this).combobox('reload', '/bsc_user/get_data_role/' + id + '?userIds=' + data[key]);
		//     }
		// });
		$('#sales_id').combobox('reload', '/bsc_user/get_client_user?client_code=' + data.client_code + '&role=sales&sales_id=');
		$('#client_A').attr('href', '/biz_client/edit/' + data.id);
		$('#contact_A').attr('href', '/biz_client_contact/index/' + data.client_code);
		$('#client_contact').combobox('reload', '/biz_client_contact/get_option/' + data.client_code+'/2');
		//这里同时得重新加载二级客户的数据
		// $('#client_code2').combogrid('grid').datagrid('load', {client_code:data.client_code});

		// changeClient2(data, is_load);

		//清空操作
		if(!is_load){
			$('#sales_id').combobox('select', '0');
			$('#operator_id').combobox('select', '0');
			$('#customer_service_id').combobox('select', '0');
			$('#client_contact').combobox('clear');
		}
	}

	/**
	 * 修改二级客户
	 */
	function changeClient2(data) {
		$('#shipper_company2').combogrid('grid').datagrid('reload', '/biz_company/get_option/shipper/'+data.client_code+'?country=<?=substr($trans_origin,0,2)?>');
		$('#consignee_company2').combogrid('grid').datagrid('reload', '/biz_company/get_option/consignee/'+data.client_code+'?country=<?=substr($trans_destination,0,2)?>');
		$('#notify_company2').combogrid('grid').datagrid('reload', '/biz_company/get_option/notify/'+data.client_code+'?country=<?=substr($trans_destination,0,2)?>');
		$('#client_B').attr('href', '/biz_client/edit/' + data.client_code);
		$('#contact_B').attr('href', '/biz_client_contact/index/' + data.client_code);
	}
	$(function(){
		changeClient2({client_code:'<?=$client_code2?>'})
	})


	function reload_select(id = '', type='combobox'){
		if(type == 'combobox'){
			var client_code = $('#client_code').textbox('getValue');
			if(id == 'client'){
				$('#client_contact').combobox('reload', '/biz_client_contact/get_option/' + client_code+'/2');
				// var client_datagrid = $('#client_company').combogrid('grid');
				// client_datagrid.datagrid('selectRow', client_datagrid.datagrid('getRowIndex', client_code));
			}else if(id = 'client2'){
				var client_code2 = $('#client_code2').combogrid('getValue')
				$('#client2_contact').combobox('reload', '/biz_client_contact/get_option/' + client_code2+'/2');
			}else{
				$('#trans_carrier').combobox('reload');
				$('#client_code').textbox('getValue');
				$('#shipper_company2').combogrid('grid').datagrid('reload');
				$('#consignee_company2').combogrid('grid').datagrid('reload');
				$('#notify_company2').combogrid('grid').datagrid('reload');
				$('#client_company').combogrid('grid').datagrid('reload');
			}
		}
	}


	function combogrid_onBeforeLoad(param, id){
		if($('#' + id).combogrid('getValue') != '' && Object.keys(param).length == 0)param.q = $('#' + id).combogrid('getValue');
		if($('#' + id).combogrid('getValue') == '' && Object.keys(param).length == 0) return false;
		else return true;
	}
	var no_selcet = 0;
	function del_company(id=0,company_type='shipper'){
		no_selcet = 1;
		$.messager.confirm({
            title:'Confirm',
            closable:false,
            msg: 'Confirm to Delete？',
			fn:function(r){
				no_selcet = 0;
				if (r){
					$.post("/biz_company/delete_data", { "id": id },
						function(data){
							if(data.success == true){
                                $.messager.alert('Tips','Delete success')
								$('#'+company_type+'_company2').combogrid('grid').datagrid('reload');
							}
							if(data.code == 1){
								$.messager.alert('Tips',data.msg)
							}
						}, "json");
				}
			}
		});
	}

	function select_client(client_code,ele){
		if(client_code.length < 2) return;
		$.post("/biz_shipment/select_client/"+client_code, { },
				function(res){
					if(res.code == 0){
						$.messager.alert('Tips','Please add client contact person for the client. ')
						$('#'+ele).combogrid('clear')
						if(ele == 'client_company'){
							$('#client_code').textbox('clear')
						}
					}
				}, "json")
	}
	$(function(){
		//初始值
		$('#client_contact').combobox('setValue', '<?= es_encode($client_contact);?>');
		$('#shipper_company').val('<?= es_encode($shipper_company);?>');
		$('#consignee_company').val('<?= es_encode($consignee_company);?>');
		$('#notify_company').val('<?= es_encode($notify_company);?>');

		var client_company_config = {
			panelWidth: '1000px',
			panelHeight: '300px',
			idField: 'company_name',              //ID字段
			textField: 'company_name',    //显示的字段
			// multiple:true,
			fitColumns : true,
			striped: true,
			editable: false,
			nowrap:false,
			pagination: false,           //是否分页
			// pageList : [30,50,100],
			// pageSize : 50,
			// toolbar : '#' + create_client_tool('client_company', 'client_company') + '_div',
			// rownumbers: true,           //序号
			collapsible: true,         //是否可折叠的
			method: 'post',
			columns:[[
				{field:'client_code',title:'<?= lang('client_code');?>',width:100},
                {field: 'company_name_en', title: '<?= lang('company_name_en');?>', width: 300},
                {field: 'company_name', title: '<?= lang('company_name');?>', width: 300},
				{field: 'tag', title: '<?= lang('tag');?>', width: 580, formatter: function(value,row,index){
						if (value){
							return value;
						} else {
                            return "<a href='/biz_client/edit/" + row.client_code + "' target='_blank'>Tags</a>";
                        }
                    }},
            ]],
            emptyMsg : 'No data!',
			required:true,
			mode: 'remote',
			onBeforeLoad:function(param){
				combogrid_onBeforeLoad(param, 'client_company');
			},
			onBeforeSelect:function(index, row){
				// if(row.stop_bool==false){
				//     alert('代理已过期请重新设置');
				//     return false;
				// }
			},
			onSelect: function(index, row){
				select_client(row.client_code,'client_company')
				// if (row.company_name_en == '') {
				// 	alert('请先到往来单位维护英文全称再选择');
				// 	$('#client_company').combogrid('clear')
				// 	$('#client_code').textbox('clear')
				// 	return false;
				// }
				// if($('#client_code').textbox('getValue') == row.client_code){
				// 	$('#client_company').combogrid('clear')
				// 	$('#client_code').textbox('clear')
				// 	return;
				// }
				//检测重复联系人
				// let res = check_repeat_contact(row.client_code);
				// if(res == 1){
				// 	$('#client_company').combogrid('clear')
				// 	$('#client_code').textbox('clear')
				// 	return false;
				// }
				if(row.company_address == '') row.company_address = ' ';
				if(row.contact_email == '') row.contact_email = ' ';
				if(row.contact_telephone2 == '') row.contact_telephone2 = ' ';
				$('#client_address').val(row.company_address);
				$('#client_contact').combobox('setValue', row.client_contact);
				$('#client_telephone').textbox('setValue', row.contact_telephone2);
				$('#client_email').textbox('setValue', row.contact_email);
				$('#client_code').textbox('setValue',row.client_code);
				changeClient(row);
			},
		};
		//2022-09-01 由于第一个表格下拉会出现表头宽度错误的问题,这里用一个隐藏,没什么用的作为替代,保证原本的没问题
		$('#client_company2').combogrid("reset");
		$('#client_company2').combogrid(client_company_config);
		client_company_config.url = '/biz_client/get_option/client?other_role=factory,logistics_client&is_tag=true';
		client_company_config.toolbar = '#' + create_client_tool('client_company', 'client_company') + '_div';
		client_company_config.value = '<?= es_encode($client_company);?>';
		$('#client_company').combogrid("reset");
		$('#client_company').combogrid(client_company_config);

		//二级委托方--start
		client_company_config.idField = 'client_code';
		client_company_config.textField = 'company_name';
		client_company_config.required = false;
		client_company_config.value = '<?= $client_code2;?>';
		client_company_config.url = '/biz_client_second_relation/serachClientData/2';
		client_company_config.toolbar = '#' + create_client_tool('client_code2', 'client_code2') + '_div';
		client_company_config.columns = [[
			{field: 'client_code', title: '<?= lang('client_code');?>', width: 100},
			{field: 'company_name', title: '<?= lang('company_name');?>', width: 600},
		]];
		// client_company_config.onBeforeSelect = function(index,row){
		// 	if(row.client_code == $('#client_code').textbox('getValue')){
		// 		alert('POD CLIENT can not be the same as POL CLIENT')
		// 		return false;
		// 	}
		// }
		client_company_config.onSelect = function(index, row){
			select_client(row.client_code,'client_code2')
			$('#client2_contact').combobox('setValue', row.client_contact);
			$('#client2_contact').combobox('reload', '/biz_client_contact/get_option/' + row.client_code+'/2');
			$('#client2_address').val(row.company_address);
			$('#client2_telephone').textbox('setValue', row.contact_telephone);
			$('#client2_email').textbox('setValue', row.contact_email);
			//现在shipper等根据这个字段来了,所以统一
			// changeClient2(row);
			$('#client_B').attr('href', '/biz_client/edit/' + row.client_code);
			$('#contact_B').attr('href', '/biz_client_contact/index/' + row.client_code);
		};
		client_company_config.onLoadSuccess = function(data){
			var client_code = $(this).combogrid('getValue');
			// changeClient2({client_code:client_code});
		};
		$('#client_code2').combogrid("reset");
		$('#client_code2').combogrid(client_company_config);
		$('#client_code2').combogrid('grid').datagrid('load',{client_code:'<?= $client_code2;?>'});
		//二级委托方--end

		var company_config = {
			panelWidth: '1000px',
			panelHeight: '300px',
			idField: 'company_name',              //ID字段
			textField: 'company_name',    //显示的字段
			// multiple:true,
			// fitColumns : true,
			striped: true,
			editable: true,
			pagination: false,           //是否分页
			// pageList : [30,50,100],
			// pageSize : 50,
			toolbar : '#' + create_company_tool('shipper_company2', 'shipper_company2') + '_div',
			// rownumbers: true,           //序号
			collapsible: true,         //是否可折叠的
            prompt:'input...write or select..',
			method: 'post',
			url:'/biz_company/get_option/shipper/<?=$client_code2?>?country=<?=substr($trans_origin,0,2)?>',
			columns:[[
				{field:'company_name',title:'<?= lang('company_name');?>',width:200},
				{field:'company_address',title:'<?= lang('company_address');?>',width:400},
				{field:'company_contact',title:'<?= lang('company_contact');?>',width:100},
				{field:'company_telephone',title:'<?= lang('company_telephone');?>',width:100},
				{field:'company_email',title:'<?= lang('company_email');?>',width:100},
			]],
            emptyMsg : 'No data!',
			required:true,
			mode: 'remote',
			onBeforeSelect:function(){
				if(no_selcet === 1){
					return false;
				}
				return true;
			},
			onBeforeCheck :function(index, row){
				if(no_selcet === 1){
					return false;
				}
				return true;
			},
			onSelect: function(index, row){
				if(row.company_address == '') row.company_address = ' ';
				if(row.company_contact == '') row.company_contact = ' ';
				if(row.company_telephone == '') row.company_telephone = ' ';
				if(row.company_email == '') row.company_email = ' ';
				$('#shipper_company').val(row.company_name);
				$('#shipper_address').val(row.company_address);
				$('#shipper_contact').val(row.company_contact);
				$('#shipper_telephone').val(row.company_telephone);
				$('#shipper_email').val(row.company_email);
			},
			value:'<?= es_encode($shipper_company);?>',
		};
		$('#shipper_company2').combogrid("reset");
		$('#shipper_company2').combogrid(company_config);

		company_config.toolbar = '#' + create_company_tool('consignee_company2', 'consignee_company2') + '_div';
		company_config.onSelect = function(index, row){
			if(row.company_address == '') row.company_address = ' ';
			if(row.company_contact == '') row.company_contact = ' ';
			if(row.company_telephone == '') row.company_telephone = ' ';
			if(row.company_email == '') row.company_email = ' ';
			$('#consignee_company').val(row.company_name);
			$('#consignee_address').val(row.company_address);
			$('#consignee_contact').val(row.company_contact);
			$('#consignee_telephone').val(row.company_telephone);
			$('#consignee_email').val(row.company_email);
		};
		company_config.value = '<?= es_encode($consignee_company);?>';
		company_config.url = '/biz_company/get_option/consignee/<?=$client_code2?>?country=<?=substr($trans_destination,0,2)?>';
		$('#consignee_company2').combogrid("reset");
		$('#consignee_company2').combogrid(company_config);

		company_config.toolbar = '#' + create_company_tool('notify_company2', 'notify_company2') + '_div';
		company_config.onSelect = function(index, row){
			if(row.company_address == '') row.company_address = ' ';
			if(row.company_contact == '') row.company_contact = ' ';
			if(row.company_telephone == '') row.company_telephone = ' ';
			if(row.company_email == '') row.company_email = ' ';
			$('#notify_company').val(row.company_name);
			$('#notify_address').val(row.company_address);
			$('#notify_contact').val(row.company_contact);
			$('#notify_telephone').val(row.company_telephone);
			$('#notify_email').val(row.company_email);
		};
		company_config.value = '<?= es_encode($notify_company);?>';
		company_config.url = '/biz_company/get_option/notify/<?=$client_code2?>?country=<?=substr($trans_destination,0,2)?>';
		$('#notify_company2').combogrid("reset");
		$('#notify_company2').combogrid(company_config);



	})
</script>
<form  id="fm">
	<div style="width: 1000px;display: flex;justify-content: space-between;margin-bottom:20px">
		<div>
			<?=lang('Client')?>:<br>
			<input name="client_code" <?php echo in_array("client_code",$G_userBscEdit) ? '' : 'disabled'; ?> id="client_code" class="easyui-textbox" readonly value="<?= $client_code;?>" style="width:130px;" >
			<a href="javascript:void(0);" class="easyui-tooltip" data-options="
                                content: $('<div></div>'),
                                onShow: function(){
                                    $(this).tooltip('arrow').css('left', 20);
                                    $(this).tooltip('tip').css('left', $(this).offset().left);
                                    $(this).tooltip('update', $('#client_company').combogrid('getValue'));
                                },
                                hideDelay:1000,
                            ">
				<div style="display: none;">
					<select id="client_company2" class="easyui-combogrid" editable="true" style="width:275px;"></select>
				</div>
				<select id="client_company" class="easyui-combogrid" <?php echo in_array("client_company",$G_userBscEdit) ? '' : 'disabled'; ?>  editable="true" name="client_company" style="width:275px;"></select>
			</a>
			<a id="client_A" target="_blank" href="/biz_client/edit/<?= $client_code;?>"><?= lang('A');?></a>
			|
			<a href="javascript: reload_select('client');"><?= lang('R');?></a>
			<br>
			<?=lang('Client Contact')?>:<a id="contact_A" target="_blank" href="/biz_client_contact/index/<?= $client_code;?>" style="color: blue"">【edit】</a><br>
			<select <?php echo in_array("client_contact",$G_userBscEdit) ? '' : 'disabled'; ?> name="client_contact" id="client_contact" class="easyui-combobox" style="width:440px;" data-options="
                                valueField:'name',
								textField:'name',
								editable:false,
								url:'<?= !empty($client_code)?"/biz_client_contact/get_option/{$client_code}/2":''?>',
								onSelect: function(rec){
									$('#client_telephone').textbox('setValue',rec.telephone);
									$('#client_email').textbox('setValue',rec.email);
								},
                            "
			>

			</select>
			<br><?= lang('Client Telephone');?>:
			<br><input type="text" class="easyui-textbox" style="width:440px;" name="client_telephone" id="client_telephone" readonly value="<?= $client_telephone;?>" >
			<br><?= lang('Client Email'); ?>:
			<script>
                function push_set(){
                    document.getElementById("tanchuang").src="/biz_client_push_config/?id_type=biz_shipment&client_code=<?php echo $id;?>";
                    $('#gcenter').window('open');
                }
			</script>
			<br><input type="text" class="easyui-textbox" style="width:440px;" name="client_email" id="client_email" readonly value="<?= $client_email;?>" >
			<br><?= lang('Client Address');?>:<br>
			<textarea <?php echo in_array("client_address",$G_userBscEdit) ? '' : 'disabled'; ?> name="client_address" id="client_address" style="height:60px;width:440px;"><?php echo $client_address;?></textarea><br>
		</div>
		<div>
			<div class="client_code2_div">
				<?= lang('Client B');?><br/>
				<select name="client_code2" id="client_code2" <?php echo in_array("client_code2",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-combogrid" style="width:400px;" <?//= $is_use_billing?"readonly":''?>></select>
				<a id="client_B" target="_blank" href="/biz_client/edit/<?= $client_code2;?>"><?= lang('A');?></a>
				|
				<a href="javascript: reload_select('client2');"><?= lang('R');?></a>
<!--				<a href="javascript:;" class="easyui-linkbutton" iconcls="icon-add" onclick="add_bill_factory()"></a>-->
				<br>
				<?= lang('Client B contact');?>:<a id="contact_B" target="_blank" href="/biz_client_contact/index/<?= $client_code2;?>" style="color: blue"">【edit】</a><br>
				<select <?php echo in_array("client2_contact",$G_userBscEdit) ? '' : 'disabled'; ?> name="client2_contact" id="client2_contact" class="easyui-combobox" style="width:440px;" data-options="
								editable:false,
                                valueField:'name',
								textField:'name',
								url:'<?= !empty($client_code2)?"/biz_client_contact/get_option/{$client_code2}/2":''?>',
								value: '<?=$client2_contact?>',
								onSelect: function(rec){
									$('#client2_telephone').textbox('setValue',rec.telephone);
									$('#client2_email').textbox('setValue',rec.email);
								},
                            "
				>

				</select>
				<br><?= lang('Client B Telephone');?>:
				<br><input type="text" class="easyui-textbox" style="width:440px;" name="client2_telephone" id="client2_telephone" readonly value="<?= $client2_telephone;?>" >
				<br><?= lang('Client B Email'); ?>: &emsp;
				<br><input type="text" class="easyui-textbox" style="width:440px;" name="client2_email" id="client2_email" readonly value="<?= $client2_email;?>" >
				<br><?= lang('Client B Address');?>:<br>
				<textarea <?php echo in_array("client2_address",$G_userBscEdit) ? '' : 'disabled'; ?> name="client2_address" id="client2_address" style="height:60px;width:435px;"><?php echo $client2_address;?></textarea><br>
			</div>
		</div>
	</div>

	<div style="width: 1000px;display: flex;justify-content: space-between">
		<div style="width: 30%">
			Shipper<br>
			<select id="shipper_company2" <?php echo in_array("shipper_company",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-combogrid" style="width:265px;"></select>
            <a href="javascript: goEditUrl('/biz_company/index/shipper');">&nbsp;<?= lang('E'); ?></a>
			|
			<a href="javascript: reload_select('shipper');"><?= lang('R');?></a>
			</a>

			<input type="hidden" <?php echo in_array("shipper_company",$G_userBscEdit) ? '' : 'disabled'; ?> id="shipper_company" name="shipper_company">
			<!--            <a href="javascript: goEditUrl('/biz_company/index/shipper');">--><?//= lang('E');?><!--</a>-->
			<!--            |-->
			<!--            <a href="javascript: reload_select('shipper');">--><?//= lang('R');?><!--</a>-->
			<br>
			<?= lang('update_shipper_address');?>:<br>
			<div style="display: flex">
				<textarea   <?php echo in_array("shipper_address",$G_userBscEdit) ? '' : 'disabled'; ?>  name="shipper_address" id="shipper_address" style="height:120px;width:300px;"><?php echo $shipper_address;?></textarea>
			</div>
<!--			<span style="color:red;">tips: 以下信息不显示在提单上</span>-->
			<br><?= lang('update_shipper_contact');?>: <input type="checkbox" <?php echo in_array("shipper_address",$G_userBscEdit) ? '' : 'disabled'; ?> class="check1" value="shipper_contact">
			<br><input type="text" <?php echo in_array("shipper_contact",$G_userBscEdit) || empty($shipper_contact)  ? '' : 'disabled'; ?>  name="shipper_contact" id="shipper_contact" style="height:20px;width:300px;" value="<?php echo $shipper_contact;?>">
			<br><?= lang('update_shipper_telephone');?>: <input type="checkbox" <?php echo in_array("shipper_address",$G_userBscEdit) ? '' : 'disabled'; ?> class="check1" value="shipper_telephone">
			<br><input type="text" <?php echo in_array("shipper_telephone",$G_userBscEdit) || empty($shipper_telephone) ? '' : 'disabled'; ?>  name="shipper_telephone" id="shipper_telephone" style="height:20px;width:300px;" value="<?php echo $shipper_telephone;?>">
			<br><?= lang('update_shipper_email');?>: <input type="checkbox" <?php echo in_array("shipper_address",$G_userBscEdit) ? '' : 'disabled'; ?> class="check1" value="shipper_email">
			<input type="text" <?php echo in_array("shipper_email",$G_userBscEdit) || empty($shipper_email) ? '' : 'disabled'; ?>  name="shipper_email" id="shipper_email" style="height:20px;width:300px;" value="<?php echo $shipper_email;?>">
		</div>
		<div style="width: 30%">
			Consignee &emsp;<br>
                <select <?php echo in_array("consignee_company",$G_userBscEdit) ? '' : 'disabled'; ?> id="consignee_company2" class="easyui-combogrid" style="width:265px;"></select><a href="javascript: goEditUrl('/biz_company/index/consignee');">&nbsp;<?= lang('E'); ?></a>
							|
							<a href="javascript: reload_select('consignee');"><?= lang('R');?></a>
			<input type="hidden" <?php echo in_array("consignee_company",$G_userBscEdit) ? '' : 'disabled'; ?> id="consignee_company" name="consignee_company">
			<!--            <a href="javascript: goEditUrl('/biz_company/index/consignee');">--><?//= lang('E');?>
			<!--                |-->
			<!--                <a href="javascript: reload_select('consignee');">--><?//= lang('R');?><!--</a>-->
			<br><?= lang('update_consignee_address');?>:
			<br>
			<div style="display: flex">
				<textarea   <?php echo in_array("consignee_address",$G_userBscEdit) ? '' : 'disabled'; ?>  name="consignee_address" id="consignee_address" style="height:120px;width:300px;"><?php echo $consignee_address;?></textarea>
			</div>
			<br><?= lang('update_consignee_contact');?>: <input type="checkbox" <?php echo in_array("consignee_address",$G_userBscEdit) ? '' : 'disabled'; ?>  class="check1" value="consignee_contact">
			<br><input type="text" <?php echo in_array("consignee_contact",$G_userBscEdit) || empty($consignee_contact)  ? '' : 'disabled'; ?>  name="consignee_contact" id="consignee_contact" style="height:20px;width:300px;" value="<?php echo $consignee_contact;?>">
			<br><?= lang('update_consignee_telephone');?>: <input type="checkbox" <?php echo in_array("consignee_address",$G_userBscEdit) ? '' : 'disabled'; ?>  class="check1" value="consignee_telephone">
			<br><input type="text" <?php echo in_array("consignee_telephone",$G_userBscEdit) || empty($consignee_telephone) ? '' : 'disabled'; ?>  name="consignee_telephone" id="consignee_telephone" style="height:20px;width:300px;" value="<?php echo $consignee_telephone;?>">
			<br><?= lang('update_consignee_email');?>: <input type="checkbox" <?php echo in_array("consignee_address",$G_userBscEdit) ? '' : 'disabled'; ?>  class="check1" value="consignee_email">
			<br><input type="text" <?php echo in_array("consignee_email",$G_userBscEdit)  || empty($consignee_email) ? '' : 'disabled'; ?>  name="consignee_email" id="consignee_email" style="height:20px;width:300px;" value="<?php echo $consignee_email;?>">
		</div>
		<div style="width: 30%">
			Notify Party &emsp;<br>
			<select id="notify_company2" <?php echo in_array("notify_company",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-combogrid" style="width:265px;"></select><a href="javascript: goEditUrl('/biz_company/index/notify');">&nbsp;<?= lang('E'); ?></a>
			|
			<a href="javascript: reload_select('notify');"><?= lang('R');?></a>
			<input type="hidden" <?php echo in_array("notify_company",$G_userBscEdit) ? '' : 'disabled'; ?> id="notify_company" name="notify_company">
			<!--            <a href="javascript: goEditUrl('/biz_company/index/notify');">--><?//= lang('E');?><!--</a>-->
			<!--            |-->
			<!--            <a href="javascript: reload_select('notify');">--><?//= lang('R');?><!--</a>-->
			<br>
			<?= lang('update_notify_address');?>:<br>
			<div style="display: flex">
				<textarea  <?php echo in_array("notify_address",$G_userBscEdit) ? '' : 'disabled'; ?>  name="notify_address" id="notify_address" style="height:120px;width:300px;"><?php echo $notify_address;?></textarea>
			</div>
			<br><?= lang('update_notify_contact');?>: <input type="checkbox" <?php echo in_array("notify_address",$G_userBscEdit) ? '' : 'disabled'; ?> class="check1" value="notify_contact">
			<br><input type="text" <?php echo in_array("notify_contact",$G_userBscEdit)  || empty($notify_contact) ? '' : 'disabled'; ?>  name="notify_contact" id="notify_contact" style="height:20px;width:300px;" value="<?php echo $notify_contact;?>">
			<br><?= lang('update_notify_telephone');?>: <input type="checkbox" <?php echo in_array("notify_address",$G_userBscEdit) ? '' : 'disabled'; ?> class="check1" value="notify_telephone">
			<br><input type="text" <?php echo in_array("notify_telephone",$G_userBscEdit) || empty($notify_telephone)  ? '' : 'disabled'; ?>  name="notify_telephone" id="notify_telephone" style="height:20px;width:300px;" value="<?php echo $notify_telephone;?>">
			<br><?= lang('update_notify_email');?>: <input type="checkbox" <?php echo in_array("notify_address",$G_userBscEdit) ? '' : 'disabled'; ?> class="check1" value="notify_email">
			<br><input type="text" <?php echo in_array("notify_email",$G_userBscEdit) || empty($notify_email)  ? '' : 'disabled'; ?>  name="notify_email" id="notify_email" style="height:20px;width:300px;" value="<?php echo $notify_email;?>">
		</div>
	</div>
	<a href="javascript:void(0)" class="easyui-linkbutton" style="width:40px;float: right;margin-right: 70px;" onclick="$('#fm').form('submit');"><?= lang('保存');?></a>
</form>

<div id="gcenter" class="easyui-window" title="弹窗"  closed="true" style="width:1020px;height:530px;padding:5px;" data-options="
				iconCls:'icon-sum',right:'1px',top:'110px'">
	<iframe scrolling="auto" frameborder="0" id="tanchuang" style="width:100%;height:100%;"> </iframe>
</div>
<div id="option_company_window" class="easyui-window" title="其他抬头" style="width:680px;height:480px"  closed="true">
	<iframe id="option_option_iframe" src="" width="100%" height="99%" frameborder="0" scrolling="auto"></iframe>
</div>
<script>
    function goEditUrl(url){
        var clientCode = $('#client_code2').combogrid('getValue').trim();
        if(clientCode === ''){
            clientCode = $('#client_code').textbox('getValue').trim();
        }
        if(clientCode === ''){
            alert('clientCode is empty');
        }else{
            window.open(url+'/'+clientCode)
        }
    }
	function add_bill_factory(){
		if('<?= $client2_email;?>' == ''){
			$.messager.alert('Tips','请先填写二级委托方邮箱');
			return;
		}
		$('#option_option_iframe').attr('src','/biz_bill/option_company/shipment/<?=$id?>?sft=1')
		$('#option_company_window').window('open')
	}
	function qzb(str){
		str = str.replace(new RegExp('·', "g"), '`');
		str = str.replace(new RegExp('！', "g"), '!');
		str = str.replace(new RegExp('￥', "g"), '$');
		str = str.replace(new RegExp('【', "g"), '[');
		str = str.replace(new RegExp('】', "g"), ']');
		str = str.replace(new RegExp('】', "g"), ']');
		str = str.replace(new RegExp('；', "g"), ';');
		str = str.replace(new RegExp('：', "g"), ':');
		str = str.replace(new RegExp('“', "g"), '"');
		str = str.replace(new RegExp('”', "g"), '"');
		str = str.replace(new RegExp('’', "g"), '\'');
		str = str.replace(new RegExp('‘', "g"), '\'');
		str = str.replace(new RegExp('、', "g"), '');
		str = str.replace(new RegExp('？', "g"), '?');
		str = str.replace(new RegExp('《', "g"), '<');
		str = str.replace(new RegExp('》', "g"), '>');
		str = str.replace(new RegExp('，', "g"), ',');
		str = str.replace(new RegExp('。', "g"), '.');
		str = str.replace(new RegExp(' ', "g"), ' ');
		str = str.replace(new RegExp('　', "g"), ' ');
		str = str.replace(new RegExp('（', "g"), '(');
		str = str.replace(new RegExp('）', "g"), ')');
		return str;
	}
	function checkName(val, type = 0){
		// var reg = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");\
		if(type == 0){
			var reg = new RegExp(/[^\x00-\x7f]/g);
		}else{
			var reg = new RegExp(/[^\x00-\x7f|\u4e00-\u9fa5]/g);
		}
		var rs = "";
		for (var i = 0, l = val.length; i < val.length; i++) {
			if(val.substr(i, 1).match(reg)) rs += val.substr(i, 1);
		}
		return rs;
	}
	var is_update = false;
	$(function (){
		var consignee_address_global = '';
		$.extend($.fn.validatebox.defaults.rules, {
			to_order: {
				validator: function(value, param){
					return value.substr(0,8) == 'TO ORDER';
				},
				message: '请以TO ORDER开头'
			}
		});
		$('#to_order').change(function(){
			let checked = $(this).prop('checked');
			if(checked){
				$('#consignee_to_order1').hide()
				$('#consignee_to_order2').show()
				$('#consignee_company').val($('#to_order_input').textbox('getValue'))
				consignee_address_global = $('#consignee_address').text()
				$('#consignee_address').val(' ')
			}else{
				$('#consignee_company').val($('#consignee_company2').combogrid('getValue'))
				$('#consignee_to_order1').show()
				$('#consignee_to_order2').hide()
				$('#consignee_address').val(consignee_address_global)
			}

		})
		$('#asconsignee').change(function(){
			let checked = $(this).prop('checked');
			if(checked){
				$('#notify_company2').combogrid('setValue',$('#consignee_company2').combogrid('getValue'))
				if($('#consignee_to_order1').css('display') == 'none'){
					$('#notify_company2').combogrid('setValue',$('#to_order_input').textbox('getValue'))
				}
				$('#notify_address').val($('#consignee_address').val()==''?' ':$('#consignee_address').val())
				$('#notify_contact').val($('#consignee_contact').val()==''?' ':$('#consignee_contact').val())
				$('#notify_telephone').val($('#consignee_telephone').val()==''?' ':$('#consignee_telephone').val())
				$('#notify_email').val($('#consignee_email').val()==''?' ':$('#consignee_email').val())
				$('#notify_company').val($('#consignee_company').val())
			}
		})
		$('.check1').change(function(){
			let checked = $(this).prop('checked');
			if(checked){
				let val = $(this).val()
				let ele = val.split('_')[0] + '_address'
				let address_val = $('#'+ele).val()
				let cur_val = $('#'+val).val()
				let str = '';
				if(val.split('_')[1] == 'contact'){
					str = 'ATTN:'
				}
				if(val.split('_')[1] == 'telephone'){
					str = 'TEL:'
				}
				if(val.split('_')[1] == 'email'){
					str = 'EMAIL:'
				}
				if(cur_val != ''){
					$('#'+ele).val(address_val+'\r'+str+cur_val)
				}
			}
		})

		$('#fm').form({
			url: '/biz_shipment/update_data_ajax/<?php echo $id;?>',
			onChange: function (target) {
				is_update = true;
			},
			onSubmit: function(param){
				is_update = false;
				if(!$('#client_code').val()){
					alert("请先选择一级委托方");
					return false;
				}
				var vv = $('#shipper_company').val();
				var vv2 = $('#shipper_company2').combogrid('getValue')
				if (vv == "") {
					if(vv2 == ''){
						alert("<?= lang('shipper_company');?> required！");
						return false;
					}
					$('#shipper_company').val(vv2)
					param.shipper_company = vv2
				}
				if(vv != vv2){
					$('#shipper_company').val(vv2)
					param.shipper_company = vv2
				}
				var vv = $('#consignee_company').val();
				var vv2 = $('#consignee_company2').combogrid('getValue')
				if($('#consignee_to_order1').css('display') == 'none'){
					vv2 = $('#to_order_input').textbox('getValue')
				}
				if (vv == "") {
					if(vv2 == ''){
						alert("<?= lang('consignee_company');?> required！");
						return false;
					}
					$('#consignee_company').val(vv2)
					param.consignee_company = vv2
				}
				if(vv != vv2){
					$('#consignee_company').val(vv2)
					param.consignee_company = vv2
				}
				var vv = $('#notify_company').val();
				var vv2 = $('#notify_company2').combogrid('getValue')
				if (vv == "") {
					if(vv2 == ''){
						alert("<?= lang('notify_company');?> required！");
						return false;
					}
					$('#notify_company').val(vv2)
					param.notify_company = vv2
				}
				if(vv != vv2){
					$('#notify_company').val(vv2)
					param.notify_company = vv2
				}
				$.each($('textarea'), function(index, item){
					var str = $(this).val();
					var id = $(this).prop('id');
					var ids = ['shipper_address', 'consignee_address', 'notify_address'];
					str = qzb(str);
					$(this).val(str);
					var newstr = $(this).val();
					if($.inArray(id, ids) !== -1){
						var special_char = checkName(newstr);
					}else{
						var special_char = checkName(newstr, 1);
					}
					if(special_char !== ''){
						// $.messager.alert('error', id + '检测到有以下特殊字符:<br/>' + special_char);
						// is_special_char = true;
						// return false;
					}else{
						is_special_char = false;
						$(this).val($(this).val().toUpperCase());
					}
				});
				if(is_special_char){
					// alert(is_special_char + '存在特殊字符');
					// return false;
				}
				var client_code2 = $('#client_code2').combogrid('getValue');
				var v = $('#client2_email').textbox('getValue')
				if(client_code2.length > 1 && v.length < 2){
					alert("Second Client email required");
					return false;
				}
				if(v != ''){
					if(v.indexOf("league") !== -1 || v.indexOf("kimxin") !== -1){
						alert("二级委托方不允许输入包含league或者kimxin的邮箱");
						return false;
					}
				}
				return true;
			},
			success:function(res_json){
				var res;
				try{
					res = $.parseJSON(res_json);
				}catch (e) {

				}
				if(res === undefined){
					alert(res_json);

				}else{
					if(res.code !== 444)alert(res.msg);
					if(res.code == 0){
						parent.location.reload()
					}else if(res.code == 444){
						// GET方式示例，其它方式修改相关参数即可
						var form = $('<form></form>').attr('action','/other/ts_text').attr('method','POST').attr('target', '_blank');
						var json_data = {};
						json_data['text'] = res.text;
						json_data['ts_str[]'] = res.ts_str;
						json_data['msg'] = res.msg;
						$.each(json_data, function (index, item) {
							if(typeof item === 'string'){
								form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
							}else{
								$.each(item, function(index2, item2){
									form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
								})
							}
						});

						form.appendTo('body').submit().remove();
					}
				}
			}
		});
	})

</script>

