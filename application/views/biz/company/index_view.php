    <script type="text/javascript">
        function doSearch(){
            $('#tt').datagrid('load',{
                company_name: $('#company_name').val()
            });
        }
        $(function(){ 
            $('#tt').edatagrid({
                url: '/biz_company/get_data/<?php echo $company_type;?>/<?php echo $client_code;?>',
                // saveUrl: '/biz_company/add_data/<?php echo $company_type;?>/<?php echo $client_code;?>' ,
                // updateUrl: '/biz_company/update_data/' ,
                destroyUrl: '/biz_company/delete_data',
                onError: function (index, data) {
                    $.messager.alert('error', data.msg);
                    return false;
                }
            });
			$('#tt').datagrid({
				width:'auto',
				height: $(window).height(),
				onDblClickRow:function(index, row){
				    window.open("/biz_company/edit/" + row.id);
				    return false;
				}
            }); 
			
			$(window).resize(function () {
				$('#tt').datagrid('resize');
			});                 
        });
        function add(){
            window.open("/biz_company/add/<?php echo $company_type;?>/<?php echo $client_code;?>");
        }
        function save(){
			$('#tt').edatagrid('saveRow');
			setTimeout(function(){window.opener.$('#<?php echo $company_type;?>_company2').combobox('reload')},1000);
		}		
		function created_by_for(value,row,index){
            return row.created_by_name;
        }
		function updated_by_for(value,row,index){
            return row.updated_by_name;
        }
    </script>
    
<table id="tt" style="width:1100px;height:450px"
            rownumbers="false" pagination="true"  idField="id"
            pagesize="30" toolbar="#tb"  singleSelect="true" nowrap="false">
        <thead>
            <tr>
                <th field="id" width="60" sortable="true">ID</th>      
                <!--th field="company_type" width="150" align="center" sortable="true">company_type</th-->           
				<th field="client_code" width="100"><?= lang('client_code');?></th>
                <th field="company_type" width="200" align="center"><?= lang('company_type');?></th>
                <th field="company_name" width="200" align="center"  ><?= lang('company_name');?></th>
				<th field="company_address" width="500"><?= lang('company_address');?></th>
				<th field="company_contact" width="200"><?= lang('company_contact');?></th>
				<th field="company_telephone" width="150" ><?= lang('company_telephone');?></th>
				<th field="company_email" width="150"><?= lang('company_email');?></th>
				<th field="country" width="120"><?= lang('country');?></th>
				<th field="region" width="60"><?= lang('省');?></th>
				<th field="city" width="60"><?= lang('city');?></th>
				<th field="postalcode" width="80"><?= lang('postalcode');?></th>
				<th field="created_by" width="80" formatter="created_by_for"><?= lang('created_by');?></th>
				<th field="updated_by" width="80" formatter="updated_by_for"><?= lang('updated_by');?></th>
            </tr>
        </thead>
</table>

   <div id="tb" style="padding:3px;">
        <table><tr>
        <td>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()"><?= lang('add');?></a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#tt').edatagrid('destroyRow')"><?= lang('delete');?></a>
        </td>
        <td width='80'> </td>
        <td>
        <span style="font-size:12px;"><?= lang('company_name');?></span>
        <input id="company_name" style="border:1px solid #ccc">
        </td><td>
        <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="doSearch()"><?= lang('search');?></a>
        </td>
        </tr></table>
         
    </div>
