<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
    <meta name="keywords" content="">
    <meta name="viewport" content="width=1180">
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css"/>
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <style>
        .search_box {
            border: 1px solid #ddd;
            float: left;
            margin: 10px 0px 0px;
            padding: 15px;
            background-color: #edf2f3;
            width: 99%;
        }
        .selected-td{ background: #ff4c42 !important; }
        .layui-table td, .layui-table th {
            vertical-align: top;
        }
    </style>
</head>
<body>
<?php if ($repeat_num < 3):?>
<table class="layui-table" style="margin: 0px 0px 20px;">
    <colgroup>
        <col width="200">
        <col width="">
        <col width="">
        <col width="">
        <col width="">
        <col>
    </colgroup>
    <thead>
    <tr>
        <th>联系人</th>
        <th>职位</th>
        <th>角色</th>
        <th>电话1</th>
        <th>电话2</th>
        <th>邮箱</th>
    </tr>
    </thead>
    <tbody>
    <?php
    if ($contact):
    foreach($contact as $item):
        ?>
        <tr>
            <td>
                <span style="color: #ff6000; font-weight: bolder"><?=$item['name'];?></span>
            </td>
            <td>
                <?=$item['position'];?>
            </td>
            <td>
                <?=$item['role'];?>
            </td>
            <td>
                <?=$item['telephone'];?><br>
            </td>
            <td>
                <?=$item['telephone2'];?><br>
            </td>
            <td>
                <?=$item['email'];?>
            </td>
        </tr>
    <?php
    endforeach;
    else:
    ?>
    <tr><td colspan="6" style="color: red; text-align: center">暂无联系人！</td></tr>
    <?php endif;?>
    </tbody>
</table>
<?php endif;?>

<table class="layui-table" style="margin: 0px;">
    <colgroup>
        <col width="150">
        <col width="250">
        <col width="200">
        <col>
    </colgroup>
    <thead>
    <tr>
        <th>booking ETD</th>
        <th>港口</th>
        <th>箱型箱量</th>
        <th>货物描述</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($data as $item):
    ?>
    <tr>
        <td>
            <span style="color: #1a9ed6; font-weight: bolder"><?=$item['booking_ETD'];?></span>
        </td>
        <td>
            <b>起始港：</b><?=$item['trans_origin_name'];?><br>
            <b>目的港：</b><?=$item['trans_destination_name'];?>
        </td>
        <td>
            <?php
            $box_info = json_decode($item['box_info'], true);
            foreach($box_info as $v){
                echo "<b>箱型：</b>{$v['size']}，<b>箱量：</b>{$v['num']}<br>";
            }
            ?>
        </td>
        <td>
            <?=$item['description'];?>
        </td>
    </tr>
    <?php endforeach;?>
    </tbody>
</table>
</body>

<script type="text/javascript" src="/inc/third/layui/layui.js"></script>
<script type="text/javascript">
    $(function () {
        layui.use(['element', 'form'], function () {
             form = layui.form;
        });
    })
</script>