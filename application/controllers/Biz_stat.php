<?php


class biz_stat extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->driver('cache');

        $this->load->model('bsc_user_model');
        $this->load->model('bsc_group_model');
        $this->load->model('m_model');
        $this->cache_key = 'stat1:'.get_session('id');
        $this->group = $this->get_group_data();
    }

    public function index(){
        if ($this->cache->redis->get($this->cache_key) == false){
            $data = array();
        }else{
            $data = json_decode($this->cache->redis->get($this->cache_key), true);
        }

        $title = [lang('一个月内'),lang('两个月内'),lang('三个月内'),lang('六个月内'),lang('十二个月内')];
        foreach ([30,60,90,180,365] as $index => $val){
            list($start_date, $end_date) = array_map(function ($v){return date('Y-m-d', $v);}, $this->getXday_SdateAndEdate(time(), $val));
            $data['date_option'][] = array(
                'title'         => $title[$index],
                'data_range'    => join(' ~ ', [$start_date, $end_date]),
            );
        }
        $data['group'] = $this->db->select('group_name, group_code')->get('bsc_group')->result_array();
        $data['user_role'] = $this->get_user_role_tree();
        $this->load->view('/biz/stat/index', $data);
    }

    public function get_data(){
        $post = $this->input->post();
        $data['client_line_chart'] = $this->client_line_chart(); //近30天往来单位新增客户走势
        $data['active'] = $this->active($post); //住来单位活跃客户总数
        $data['no_active'] = $this->no_active($post); //住来单位活跃客户总数
        $data['email_send_count'] = $this->email_send_count();  //邮件群发数量
        $data['client_black_list'] = $this->client_black_list($post); //往来单位黑名单
        $data['client_add'] = $this->client_add($post); //往来单位新增
        $data['crm_public'] = $this->crm_public($post); //CRM公海数量
        $data['crm_add_clue'] = $this->crm_add_clue($post); //CRM线索1数量
        $data['crm_clue_2'] = $this->crm_clue_2($post); //CRM线索2数量
        $data['crm_clue_extraction'] = $this->crm_clue_extraction($post); //CRM线索提取数量
        $data['crm_add_by_hand'] = $this->crm_add_by_hand($post); //CRM手工新增
        $data['crm_status_1'] = $this->crm_status(1); //申请中
        $data['crm_status_2'] = $this->crm_status(2); //跟进中
        $data['crm_status_3'] = $this->crm_status(3); //转正中
        $data['crm_status_0'] = $this->crm_status(0); //已转正
        $data['crm_add_line_chart'] = $this->crm_add_line_chart(); //近30天CRM新增走势
        $data['crm_to_client_line_chart'] = $this->crm_to_client_line_chart(); //CRM转正走势
        $data['crm_conversion_rate'] = $this->crm_conversion_rate();

        $data['where'] = $post;
        $this->cache->redis->save($this->cache_key, json_encode($data), 43200);//12小时
        return $this->output->set_content_type('application/json')->set_output(json_encode(array('code'=>1), 256));
    }

    /**
     * @return mixed
     */
    public function email_send_count(){
        $params = $this->input->get();
        $default_date = array_map(function ($v){return date('Y-m-d H:i:s', $v);}, $this->getXday_SdateAndEdate(time()));
        $date_range = isset($params['date_range']) ? $params['date_range'] : join(' ~ ', $default_date);
        $where = [];
        if ($date_range != '') {
            list($start_date, $end_date) = explode(' ~ ', $date_range);
            $where[] = "send_time > '{$start_date} 00:00:00'";
            $where[] = "send_time < '{$end_date} 00:00:00'";
        }

        if (!empty($this->get_group($_POST))) $where[] = "crm_promote_mail.create_by in(".join(',', $this->get_group($_POST)).")";
        $where = join(' and ', $where);
        $this->db->where($where)->join('crm_promote_mail', 'crm_promote_mail.id = crm_promote_mail_log.mail_id', 'LEFT');
        return $this->db->count_all_results('crm_promote_mail_log');
    }

    /**
     * 往来单新增客户图表
     */
    public function client_add_list(){
        $params = $this->input->get();
        $data = array('title'=>"往来单位新增客户统计图（{$params['date_range']}）");
        $group = $this->get_childen_group($params['group']);

        if (!empty($group)) {
            $data['name'] = array_column($group, 'name');
            foreach ($group as $item) {
                if (empty($item['user'])){
                    $data['value'][] = 0;
                    continue;
                }
                $where = array();
                $where[] = "created_by in(" . join(',', $item['user']) . ")";
                if (!empty($this->create_where($params))) array_push($where, ...$this->create_where($params));
                if (!empty($where)) {
                    $where = join(' and ', $where);
                    $this->db->where($where, null, false);
                }

                //获取统计结果按用户组分组
                if (count($group) > 1) {
                    $data['value'][] = $this->db->count_all_results('biz_client');
                }else{
                    //获取统计结果按添加人分组
                    $this->db->select('count(*) as `value`');
                    $result = $this->db->group_by('created_by')->order_by('created_by', 'asc')->get('biz_client')->result_array();
                    $data['value'] = array_column($result, 'value');

                    //获取用户名称
                    $user = $this->db->where_in("id", $item['user'])->order_by('created_by', 'asc')->get('bsc_user')->result_array();
                    $data['name'] = array_column($user, 'name');
                }
            }
        }

        foreach ($data['name'] as $key => $item){
            $data['data'][] = array(
                'name' => $item,
                'value' => isset($data['value'][$key])?$data['value'][$key]:0
            );
        }

        $this->load->vars('data', $data);
        $this->load->view('/biz/stat/stat_detail');
    }

    /**
     * @CRM转化率
     */
    private function crm_conversion_rate(){
        //日期范围
        list($start_date, $end_date) = array_map(function ($v) {
            return str_replace('created_time', 'create_time', $v);
        }, $this->create_where($_POST));

        $where[] = "id_type = 2";
        $where[] = "status = 1";
        array_push($where, ...[$start_date, $end_date]);
        $where[] = "status_text LIKE '%通过%'";

        if (!empty($this->get_group($_POST))) $where[] = "create_id in(".join(',', $this->get_group($_POST)).")";
        $where = join(' and ', $where);
        return $this->db->where($where)->count_all_results('biz_workflow');
    }

    /**
     * @近30天CRM抬头转正走势
     */
    private function crm_to_client_line_chart(){
        $data = $where = [];
        list($start_date, $end_date) = $this->getXday_SdateAndEdate(time());
        $_start_date = date('Y-m-d H:i:s',$start_date);
        $_end_date = date('Y-m-d H:i:s',$end_date);

        $where[] = "id_type = 2";
        $where[] = "status = 1";
        $where[] = "status_text LIKE '%通过%'";
        $where[] = "create_time > '{$_start_date}' AND create_time < '{$_end_date}'";
        if (!empty($this->get_group($_POST))) $where[] = "create_id in(".join(',', $this->get_group($_POST)).")";
        $where = join(' and ', $where);

        $result = $this->db->select("count(*) as num, DATE_FORMAT(create_time, '%m.%d') as month_day")
            ->where($where)
            ->group_by("DATE_FORMAT( create_time, '%m.%d' )")
            ->get('biz_workflow')
            ->result_array();

        $result = array_combine(array_column($result, 'month_day'), array_column($result, 'num'));
        for($i=0; $i<30; $i++){
            $date = date('m.d', strtotime("+{$i} day", $start_date));
            $data[$date] = array_key_exists($date, $result) === true ? $result[$date] : 0;
        }

        return $data;
    }

    /**
     * @近30天CRM新增抬头走势
     */
    private function crm_add_line_chart(){
        $group = postValue('group', '');
        $data = [];
        list($start_date, $end_date) = $this->getXday_SdateAndEdate(time());
        $_start_date = date('Y-m-d H:i:s',$start_date);
        $_end_date = date('Y-m-d H:i:s',$end_date);

        $where[] = "apply_status != -2";
        $where[] = "created_time > '{$_start_date}' AND created_time < '{$_end_date}'";
        if (!empty($this->get_group($_POST))) $where[] = "created_by in(".join(',', $this->get_group($_POST)).")";
        $where = join(' and ', $where);
        $result = $this->db->select("count(*) as num, DATE_FORMAT(created_time, '%m.%d') as month_day")
            ->where($where)
            ->group_by("DATE_FORMAT( created_time, '%m.%d' )")
            ->get('biz_client_crm')
            ->result_array();

        $result = array_combine(array_column($result, 'month_day'), array_column($result, 'num'));
        for($i=0; $i<30; $i++){
            $date = date('m.d', strtotime("+{$i} day", $start_date));
            $data[$date] = array_key_exists($date, $result) === true ? $result[$date] : 0;
        }

        return $data;
    }

    /**
     * CRM手工新增
     */
    private function crm_add_by_hand($post){
        $where[] = "add_way = 'hand'";
        if (!empty($this->create_where($post))) array_push($where, ...$this->create_where($post));
        if (!empty($this->get_group($_POST))) $where[] = "sales_id in(".join(',', $this->get_group($_POST)).")";
        $where = join(' and ', $where);
        return $this->db->where($where, null, false)->count_all_results('biz_client_crm');
    }


    /**
     * CRM线索提取数量
     */
    private function crm_clue_extraction($post){
        $where[] = "add_way in('clue1', 'clue2')";
        if (!empty($this->create_where($post))) array_push($where, ...$this->create_where($post));
        if (!empty($this->get_group($_POST))) $where[] = "sales_id in(".join(',', $this->get_group($_POST)).")";
        $where = join(' and ', $where);
        return $this->db->where($where, null, false)->count_all_results('biz_client_crm');
    }

    private function crm_clue_2($post){
        $query_date = date('Y-m-d', strtotime("-3 month"));
        $query_week = date('Y-m-d', strtotime("-2 week"));
        $where = array();
        $where[] = "biz_shipment.created_time > '{$query_week}'";
        $where[] = "biz_consol.trans_ATD > '1999-12-12'";
        $where[] = 'biz_shipment.client_code <> biz_shipment.client_code2';
        $where[] = 'biz_shipment.client_code2 not in(select client_code from biz_shipment where booking_ETD > \''.$query_date.'\')';
        $where[] = 'biz_client.client_level != -1';
        $where = join(' and ', $where);
        $this->db->select('count(*)');
        $this->db->where($where, null, false);
        $this->db->join('biz_client','biz_client.client_code = biz_shipment.client_code2', 'LEFT');
        $this->db->join('biz_consol','biz_consol.id = biz_shipment.consol_id', 'LEFT');
        $this->db->group_by('biz_shipment.client_code2');
        $sql = $this->db->get_compiled_select('biz_shipment');
        return $this->db->count_all_results("({$sql}) as tt");
    }

    /**
     * CRM线索数量
     */
    private function crm_add_clue($post){
        if (!empty($this->create_where($post))) $where = $this->create_where($post);
        if (!empty($where)){
            $where = join(' and ', $where);
            $this->db->where($where, null, false);
        }
        return $this->db->count_all_results('biz_client_crm_clue');
    }

    /**
     * CRM各种状态的数量
     */
    private function crm_status($type=1){
        $where[] = 'apply_status = ' . $type;
        if (!empty($this->get_group($_POST))) $where[] = "sales_id in(".join(',', $this->get_group($_POST)).")";
        $where = join(' and ', $where);
        return $this->db->where($where, null, false)->count_all_results('biz_client_crm');
    }

    /**
     * CRM公海数量
     */
    private function crm_public($post){
        $where[] = 'sales_id = 0';
        $where[] = '(apply_status between 1 and 3)';
        $where = join(' and ', $where);
        return $this->db->where($where, null, false)->count_all_results('biz_client_crm');
    }

    /**
     * @近30天往来单位新增客户走势
     */
    private function client_line_chart(){
        $data = $where = [];
        list($start_date, $end_date) = $this->getXday_SdateAndEdate(time());
        $_start_date = date('Y-m-d H:i:s',$start_date);
        $_end_date = date('Y-m-d H:i:s',$end_date);
        if (!empty($this->get_group($_POST))) $where[] = "created_by in(".join(',', $this->get_group($_POST)).")";
        $where[] = "created_time > '{$_start_date}' AND created_time < '{$_end_date}'";
        $where = join(' and ', $where);
        $result = $this->db->select("count(*) as num, DATE_FORMAT(created_time, '%m.%d') as month_day")
            ->where($where)
            ->group_by("DATE_FORMAT( created_time, '%m.%d' )")
            ->get('biz_client')
            ->result_array();

        $result = array_combine(array_column($result, 'month_day'), array_column($result, 'num'));
        for($i=0; $i<30; $i++){
            $date = date('m.d', strtotime("+{$i} day", $start_date));
            $data[$date] = array_key_exists($date, $result) === true ? $result[$date] : 0;
        }

        return $data;
    }

    /**
     * 住来单位黑名单客户
     */
    private function client_black_list(){
        $where = 'client_level = -1';
        return $this->db->where($where, null, false)->count_all_results('biz_client');
    }

    /**
     * 住来单位新增客户
     */
    private function client_add($post){
        $where = [];
        if (!empty($this->get_group($_POST))) $where[] = "created_by in(".join(',', $this->get_group($_POST)).")";
        if (!empty($this->create_where($post))) array_push($where, ...$this->create_where($post));
        if (!empty($where)){
            $where = join(' and ', $where);
            $this->db->where($where, null, false);
        }
        return $this->db->count_all_results('biz_client');
    }

    /**
     * 住来单位活跃客户
     */
    private function active($post){
        $ThreeMonthsAgo = date('Y-m-d', strtotime('-3 month'));
        $where[] = "biz_client.is_client = 1";
        if (!empty($this->get_group($post))) $where[] = "client_code IN (SELECT client_code FROM biz_client_duty WHERE user_role = 'sales' AND user_id IN ( ".join(',', $this->get_group($post))." ))";
        $where[] = "(select booking_ETD from biz_shipment where biz_shipment.client_code = biz_client.client_code order by booking_ETD desc limit 1) >= '{$ThreeMonthsAgo}'";
        $where = join(' and ', $where);
        return $this->db->where($where, null, false)->count_all_results('biz_client');
    }

    /**
     * 住来单位活跃客户
     */
    private function no_active($post){
        $ThreeMonthsAgo = date('Y-m-d', strtotime('-3 month'));
        $where[] = "biz_client.is_client = 1";
        if (!empty($this->get_group($post))) $where[] = "client_code IN (SELECT client_code FROM biz_client_duty WHERE user_role = 'sales' AND user_id IN ( ".join(',', $this->get_group($post))." ))";
        $where[] = "((select booking_ETD from biz_shipment where biz_shipment.client_code = biz_client.client_code order by booking_ETD desc limit 1) <= '{$ThreeMonthsAgo}' or (select booking_ETD from biz_shipment where biz_shipment.client_code = biz_client.client_code order by booking_ETD desc limit 1) is null)";
        $where = join(' and ', $where);

        return $this->db->where($where, null, false)->count_all_results('biz_client');
    }

    /**
     * 创建查询条件
     * @param $post 查询参数，主要来自表单提交
     * @param $type 查询类型，主要分为两个，client表示往来单位，crm表示CRM抬头
     * @return array
     */
    private function create_where($post){
        $default_date = array_map(function ($v){return date('Y-m-d H:i:s', $v);}, $this->getXday_SdateAndEdate(time()));
        $date_range = isset($post['date_range']) ? $post['date_range'] : join(' ~ ', $default_date);

        $where = [];
        if ($date_range != '') {
            list($start_date, $end_date) = explode(' ~ ', $date_range);
            $where[] = "created_time > '{$start_date} 00:00:00'";
            $where[] = "created_time < '{$end_date} 00:00:00'";
        }

        return $where;
    }

    /**
     * 获取用户组
     */
    private function get_group($post){
        $type = isset($post['type']) ? $post['type'] : 'user';
        $group = isset($post['group']) ? $post['group'] : '';
        $user = '';
        if ($type == 'head'){
        }elseif ($type == 'user' && $group != ''){
            $user = [$group];
        }else {
            $groups = Model('bsc_group_model')->get_child_group($group);
            if ($groups) {
                $group_list = array_column($groups, 'group_code');
                $user = $this->db->select('id, name')->where_in('group', $group_list)->get('bsc_user')->result_array();
                if (!empty($user)) {
                    $user = array_column($user, 'id');
                }else{
                    $user = [-99];
                }
            }
        }

        return $user;
    }

    /**
     * 获取某月的开始时间和结束时间
     * $current_time 时段内的任意时间戳
     */
    private function getMonth_SdateAndEdate($current_time)
    {
        $start_time = mktime(0,0, 0, date('m', $current_time),1,date('Y', $current_time));
        $end_time = mktime(0, 0, 0, date('m', $current_time) + 1, 1, date('Y', $current_time))-1;
        return [$start_time, $end_time];
    }

    /**
     * 获取最近X天内的开始时间和结束时间
     * $xDay 当前时间已过去的天数，默认30天前
     * $current_time 当前时间戳
     */
    private function getXday_SdateAndEdate($current_time=0, $xDay=30)
    {
        if (!$current_time) return false;
        $start_time = strtotime("-{$xDay} days", mktime(0,0, 0, date('m',$current_time),date('d',$current_time),date('Y',$current_time)));
        $end_time = mktime(0,0,0, date('m',$current_time),date('d',$current_time),date('Y',$current_time))-1;
        return [$start_time, $end_time];
    }


    /**
     * 根据当前用户获取所能看到的所有权限
     */
    private function get_user_role_tree(){
        $dataType = getValue('dataType', '0');
        $userId = get_session('id');
        $userRange = filter_unique_array(array_merge(get_session('user_range'), array($userId)));
        $groupRange = filter_unique_array(explode(',', get_session('group_range')));

        // 获取所有相关的user_range数据
        $where = "id in (" . join(',', $userRange) . ") and (FIND_IN_SET('sales', user_role) or FIND_IN_SET('customer_service', user_role))";
        $this->db->select("id as value,name, (select id from bsc_group where bsc_group.group_code = bsc_user.group) as parent_id");
        $users = $this->bsc_user_model->get($where);
        foreach ($users as &$user){
            $user['is_use'] = true;
            $user['disabled'] = false;
            $user['type'] = 'user';
        }

        //首先将所有组
        Model('bsc_group_model');
        $this->db->select("id as idk,parent_id,group_code as value,group_name as name,group_type");
        $group_where = "";
        $groups = $this->bsc_group_model->get($group_where);
        //不需要用户的版本

        foreach ($groups as $key => $group){
            if($group['group_type'] == '分公司') $groups[$key]['type'] = 'sub_company';
            else if($group['group_type'] == '总部') $groups[$key]['type'] = 'head';
            else $groups[$key]['type'] = 'group';
            $groups[$key]['is_use'] = false;
            $groups[$key]['disabled'] = true;
            if($group['group_type'] != '总部')$groups[$key]['state'] = 'closed';
            //如果当前用户有组权限,或是管理员,就能使用
            if(in_array($group['value'], $groupRange) || is_admin()) {
                $groups[$key]['is_use'] = true;
                $groups[$key]['disabled'] = false;
            }
            $groups[$key]['children'] = array();
        }
        if($dataType === '0'){
            $groups = array_merge($groups, $users);
        }

        //将相关的部门全部获取
        $data = $this->getGroupTree($groups);
        return $data;
    }

    private function getGroupTree($groups, $parent_id = 0){
        $data = array();
        foreach($groups as $key => $group){
            if($group['parent_id'] == $parent_id){
                //用户不处理
                if($group['type'] != 'user'){
                    $group['children'] = $this->getGroupTree($groups,$group['idk']);

                    if(empty($group['children'])) continue;
                    // if(empty($group['children'])) unset($group['children']);
                }
                $data[] = $group;
            }
        }

        return $data;
    }

    public function get_group_data(){
        //首先将所有组
        Model('bsc_group_model');
        $this->db->select("id,parent_id,group_code as value,group_name as name,group_type");
        $group_where = "";
        foreach($this->bsc_group_model->get($group_where, 'level', 'desc') as $row){
            $has_child = $this->db->where('parent_id', $row['id'])->count_all_results('bsc_group');
            $row['has_child'] = $has_child > 0 ? true : false;
            $groups[$row['id']] = $row;
        }

        foreach ($groups as $k => $v) {
            if (isset($groups[$v['parent_id']])) {
                $groups[$v['parent_id']]['childen'][] = $v;
            }
        }

        foreach ($groups as $k => $v) {
            $groups[$v['value']] = $v;
            unset($groups[$k]);
        }

        return $groups;
    }

    private function get_childen_group($group_str = 'HEAD'){
        $group = [];
        if (array_key_exists($group_str, $this->group) === true){
            if ($this->group[$group_str]['has_child'] == 1) {
                foreach ($this->group[$group_str]['childen'] as $index => $item) {
                    $result = Model('bsc_group_model')->get_child_group($item['value']);
                    //if (in_array($item['group_type'], ['操作部','销售部','市场部','OPERATOR','SALES','分公司','大区维护','MARKETING']) === false) continue;
                    $group[$index] = $item;
                    $group_name_list = array_column($result, 'group_code');
                    $group_name_str = array_map(function ($v) {
                        return "'{$v}'";
                    }, $group_name_list);
                    $user = $this->db->where("group in(".join(',', $group_name_str).")")->get('bsc_user')->result_array();
                    $group[$index]['user'] = array_column($user, 'id');
                }
            }else{
                $group[0] = $this->group[$group_str];
                $user = $this->db->where("group", $group_str)->get('bsc_user')->result_array();
                $group[0]['user'] = array_column($user, 'id');
            }
        }else{
            $group[0] = array('has_child'=>false, 'user'=>[(int)$group_str]);
        }

        return $group;
    }
}