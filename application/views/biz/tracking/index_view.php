<table id="tt2" style="width:1100px;min-height:650px"
       rownumbers="false"  idField="id"
       toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th field="id" width="60" >id</th>
        <th field="node" width="220"  >node</th>
        <th field="position" width="220" >position</th>
        <th field="node_time" width="150"  >node_time</th>
        <th field="remark" width="200">remark</th>
        <th field="created_time" width="150" >创建时间</th>
        <th field="updated_time" width="150">修改时间</th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()"><?= lang('add');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="del()"><?= lang('delete');?></a>
            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="add_div" class="easyui-window" style="width:450px;height:345px;padding:5px;" shadow="false" data-options="closed:true,border:'thin',onClose:function(){ $('#add_fm').form('clear')}">
    <form id="add_fm" method="post">
        <input type="hidden" name="id" id="tracking_id">
        <table>

            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td width="80"><?= lang('node');?></td>
                <td>
                    <input class="easyui-combobox" required name="node" id="node" style="width: 280px; height: 25px;"
                           data-options="
                           valueField: 'value',
                           textField: 'value',
                           url:'/bsc_dict/get_option/tracking_node',"
                    />
                </td>
            </tr>
            <tr>
                <td width="80"><?= lang('position');?></td>
                <td>
                    <input class="easyui-combobox" required name="position" id="position" style="width: 280px; height: 25px;"
                           data-options="
                           valueField: 'port_code',
                           textField: 'port_name',
                           url:'/biz_tracking/port/<?=$id_type?>/<?=$id_no?>',"
                    />
                </td>
            </tr>
            <tr>
                <td width="80"><?= lang('node_time');?></td>
                <td>
                    <input class="easyui-datetimebox input" required name="node_time" style="width: 280px; height: 25px;" id="node_time" value="">
                </td>
            </tr>
            <tr>
                <td width="80"><?= lang('remark');?></td>
                <td>
                    <textarea name="remark" id="remark" style="width: 275px; height: 30px;"></textarea>
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3"><button type="button" onclick="tracking_save()" style="width: 100px"><?= lang('save');?></button></td>
            </tr>
        </table>
    </form>
</div>
<div id="position_div">
    <form id="position_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="position_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="position_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="position_click" iconCls="icon-search" onclick="query_report('position', 'position')"><?= lang('search');?></a>
        </div>
    </form>
</div>
<script type="text/javascript">
    var type = 'add';

    function edit(row){
        type = 'edit';
        if(row){
            $("#node").combobox('setValue',row.node)
            $("#position").combobox('setValue',row.position)
            $("#node_time").datetimebox('setValue',row.node_time)
            $("#remark").val(row.remark)
            $("#tracking_id").val(row.id)
            $("#add_div").window({title: '编辑'}).window("open")
        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('请选中任意行')?>');
        }
    }

    function tracking_save(){
        let formData = $('#add_fm').serializeArray();
        let obj = {}
        let msg = ''
        formData.map(item=>{
            if(item.name == 'node' || item.name == 'position' || item.name == 'node_time'){
                if(item.value == ''){
                    msg = '请填写必填项'
                }
            }
            obj[item.name] = item.value

        })
        if(msg != ''){
            $.messager.alert('<?= lang('Tips')?>', msg)
            return
        }
        if(type == 'add'){
            obj['id_type'] = "<?=$id_type?>";
            obj['id_no'] = "<?=$id_no?>";
            $.post("/biz_tracking/add_data", obj,
                function(res){
                    if(res.code == 1){
                        $.messager.alert('<?= lang('Tips')?>', res.msg)
                        $('#tt2').datagrid('reload');
                    }else{
                        $.messager.alert('<?= lang('Tips')?>', res.msg)
                    }
                    $("#add_div").window("close")
                }, "json");
        }else if(type == 'edit'){
            $.post("/biz_tracking/edit_data", obj,
                function(res){
                    if(res.code == 1){
                        $.messager.alert('<?= lang('Tips')?>', res.msg)
                        $('#tt2').datagrid('reload');
                    }else{
                        $.messager.alert('<?= lang('Tips')?>', res.msg)
                    }
                    $("#add_div").window("close")
                }, "json");
        }

    }

    function add(){
        type = 'add'
        $("#add_div").window({title: '新增'}).window("open")
    }

    function del() {
        var row = $('#tt2').datagrid('getSelected');
        if(row){
            $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认删除？');?>', function(r){
                if (r){
                    $.post("/biz_tracking/del_data", {id: row.id},
                        function(res){
                            if(res.code == 1){
                                $.messager.alert('<?= lang('Tips')?>', res.msg)
                                $('#tt2').datagrid('reload');
                            }else{
                                $.messager.alert('<?= lang('Tips')?>', res.msg)
                            }
                            $("#add_div").window("close")
                        }, "json");
                }
            });


        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('请选中任意行')?>');
        }
    }


    $(function () {

        $('#tt2').datagrid({
            width: 'auto',
            height: $(window).height(),
            url: '/biz_tracking/get_data/<?=$id_type?>/<?=$id_no?>',
            onDblClickRow:function(index,row){
                edit(row)
            }
        });

        $(window).resize(function () {
            $('#tt2').datagrid('resize');
        });


    });



</script>

