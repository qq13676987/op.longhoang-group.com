<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= lang('往来单位统计');?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link href="/inc/third/layui/css/layui.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
</head>
<body>
<div style="display: ; border-bottom: 1px solid #999; padding: 10px 0px;">
    <div class="layui-inline" id="" style="line-height: 20px; padding-left: 30px;"><?= lang('筛选条件');?>：</div>
    <div class="layui-inline" id="history_group" style="line-height: 20px; color: #2e9869"></div>
    <div class="layui-inline" style="line-height: 20px; padding-left: 30px;">
        <span style="color: #ff6300; cursor: pointer; margin-left: 30px;" onclick="open_window()">[<?= lang('更换统计维度');?>]</span>
    </div>
    <div class="layui-inline" style="line-height: 20px;">
        <span style="color: dodgerblue; cursor: pointer; margin-left: 10px;" class="go_back">[<?= lang('返回上一级');?>]</span>
    </div>

    <!--<div class="layui-inline" style="line-height: 30px;">
            <label class="layui-form-label" style="width: auto">筛选维度：</label>
                <input type="radio" name="group_by" value="sales" <?php /*if($group_by=='sales')echo 'checked';*/ ?> title="销售">
                <input type="radio" name="group_by" value="sales_group" <?php /*if($group_by=='sales_group')echo 'checked';*/ ?> title="销售部">
                <input type="radio" name="group_by" value="sub_company" <?php /*if($group_by=='sub_company')echo 'checked';*/ ?> title="分公司">
        </div>
        <div class="layui-inline" style="text-align: center">
            <div class="layui-input-block" style="margin: 0px 18px">
                <input type="hidden" id="type" name="type" value="<? /*=$type;*/ ?>">
                <input type="hidden" id="date_range" name="date_range" value="<? /*=isset($_GET['date_range'])?$_GET['date_range']:'';*/ ?>">
                <button class="layui-btn" type="submit" id="submit_query" lay-submit lay-filter="*">
                    <i class="layui-icon layui-icon-search layui-font-12"></i>
                    查询
                </button>
                <span style="color: dodgerblue; cursor: pointer; margin-left: 30px;" onclick="return history.back(-1);">返回上一级</span>
            </div>
        </div>-->
</div>

<div id="query_table" style="display: none">
    <form class="layui-form" method="get">
        <div class="layui-inline" style="width: 100%; margin-top: 10px;">
            <fieldset class="layui-elem-field" style=" margin: 10px 18px">
                <legend style="font-size: 12px;"><?= lang('选择统计维度');?></legend>
                <div class="layui-field-box" style="color:#b0b5b6">
                    <div class="layui-form-item" style="text-align: left">
                        <input type="radio" name="group_by" lay-filter="group_by" value="sales" <?php if ($group_by == 'sales') echo 'checked'; ?> title="<?= lang('销售');?>">
                        <input type="radio" name="group_by" lay-filter="group_by" value="sales_group" <?php if ($group_by == 'sales_group') echo 'checked'; ?> title="<?= lang('销售部');?>">
                        <input type="radio" name="group_by" lay-filter="group_by" value="sub_company" <?php if ($group_by == 'sub_company') echo 'checked'; ?> title="<?= lang('分公司');?>">
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="layui-inline" style="width: 100%; text-align: center">
            <div class="layui-input-block" style="margin: 0px 18px; display: block;">
                <input type="hidden" id="group" name="group" value="<?=$group;?>">
                <input type="hidden" id="type" name="type" value="<?=$type;?>">
                <input type="hidden" id="date_range" name="date_range" value="<?=$date_range;?>">
                <input type="hidden" id="event" name="event" value="next">
                <button class="layui-btn" type="submit" id="submit_query" lay-submit lay-filter="*" style="width: 300px;">
                    <i class="layui-icon layui-icon-search layui-font-12"></i>
                    <?= lang('查询');?>
                </button>
            </div>
        </div>
    </form>
</div>

<div id="content" class="" style="height: 600px; width: 100%; overflow: auto"></div>
<script id="content_tpl" type="text/html">
    <table class="layui-table" style="margin: 0px;">
        <colgroup>
            <col width="200">
            <col>
            <col width="120">
            <col width="150">
        </colgroup>
        <thead>
        <th style="text-align: center; font-weight: bolder;"><?= lang('统计维度');?></th>
        <th style="text-align: center; font-weight: bolder;"><?= lang('百分比');?></th>
        <th style="text-align: center; font-weight: bolder;"><?= lang('数量');?></th>
        <th style="text-align: center; font-weight: bolder;"><?= lang('下钻');?></th>
        </thead>
        <tbody>
        {{#  layui.each(d.data, function(index, item){ }}
        <tr>
            <td style="text-align: right">{{item.group_name}}</td>
            <td style="text-align: center;">
                <div class="layui-progress layui-progress-big" lay-showPercent="yes">
                    <div class="layui-progress-bar layui-bg-red" lay-percent="{{item.proportion}}%"></div>
                </div>
            </td>
            <td style="text-align: center;">{{item.total}}</td>
            <td style="text-align: center;">
                <div style="cursor: pointer; color: dodgerblue" class="next_serarch" data-value="{{item.group_code}}"><?= lang('详细分析');?></div>
            </td>
        </tr>
        {{#  }); }}
        {{#  if(d.data.length === 0){ }}
        <tr>
            <td colspan="4" style="text-align: center; color: #ff8100; font-size: 14px;"><?= lang('没有统计结果，你可以尝试改变条件或维度！');?></td>
        </tr>
        {{#  } }}
        </tbody>
    </table>
</script>
<script id="query_tpl" type="text/html">
    <div style="margin-bottom: 10px;" class="query_row">
        <div class="layui-inline" style="width: 150px;">
            <select name="field[f][]" lay-filter="query_field" id="query_field"
                    style="height: 25px !important;">
                <option value=""><?= lang('选择查询条件');?></option>
                <?php foreach ($search_field as $item): ?>
                    <option value="<?= $item['table_field']; ?>"><?= $item['title']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="layui-inline" style="width: 80px;">
            <select name="field[s][]" class="query_operation">
                <option value="like" selected="">like</option>
                <option value="like前"><?= lang('like前');?></option>
                <option value="=">=</option>
                <option value=">=">&gt;=</option>
                <option value="<=">&lt;=</option>
                <option value="!=">!=</option>
            </select>
        </div>
        <div class="layui-inline">
            <input type="text" name="field[v][]" placeholder="" autocomplete="off"  class="layui-input query_value">
        </div>
        <div class="layui-inline" style="padding-top: 2px">
            <div class="layui-icon layui-icon-add-circle"
                 style="font-size: 25px; color: #1E9FFF; margin-left: 10px; cursor: pointer"></div>
        </div>
    </div>
</script>
<script src="/inc/third/layui/layui.js"></script>
<script>
    var action = '<?=$action;?>';       //获取统计结果的方法
    var laytpl,element,layer,form;       //全局化layui组件
    var param_group = ['<?=$group;?>'];     //统计参数，查询条件
    var param_group_by = '<?=$group_by;?>'; //统计参数，统计维度
    var param_type = '<?=$type;?>';     //统计参数，类型
    var add_group = '';     //临时变量，param_group的新增元素

    layui.extend({
    }).use(['layer', 'form','laytpl', 'laydate', 'element'], function () {
        form = layui.form;
        laytpl = layui.laytpl;
        element = layui.element;
        layer = layui.layer;

        //缺少重要参数
        if (action == '') {
            layer.alert('<?= lang('无效的请求！');?>', {icon:2});
            return false;
        }

        //打开查询窗口
        open_window();

        /*
        //加载表头查询框
        $('.query_table_tr').html(laytpl($('#query_tpl').html()).render(['']));
        $('.query_table_tr').children().first().find('.layui-icon').addClass('layui-icon-add-circle').attr({id:'add_btn'});
        form.render();
        form.on('select(query_field)', function(data){
            var selected = data.value; //被选中的值
            var parent_dom = $(this).parents('.query_row');
            var index = parent_dom.index();
            parent_dom.find('.query_value').attr({id:'query_value_'+index});
            $('#query_value_'+index).val('');
            laydate.render({
                elem: '#query_value_'+index //指定元素
            });
            form.render();
        });
        //添加节点
        /*$('#add_btn').on('click', function () {
            if ($('.query_table_tr').children().length > 3){
                layer.alert('最多允许组合4个条件！', {icon: 2});
                return;
            }
            laytpl($('#query_tpl').html()).render([''], function (html) {
                $('.query_table_tr').append(html);
            });
            $('.query_table_tr').children().last().find('.layui-icon')
            .removeClass('layui-icon-add-circle')
            .addClass('layui-icon-reduce-circle reduce_btn')
            .removeAttr('id')
            .css({color:'#999'});
            form.render();

        });
        //删除节点
        $('div').on('click', '.reduce_btn', function () {
            $(this).parents('.query_row').remove();
        });
        */

        //提交表单，异步发送请求
        form.on('submit(*)', function(data){
            if (add_group != '') {
                param_group.push(add_group);
                add_group = '';
            }
            var group = Object.assign({}, param_group);
            data.field.group = group;
            data.field.group_by = param_group_by;
            loadData(data.field);
            return false;
        });

        //改变统计维度
        form.on('radio(group_by)', function(data){
            param_group_by = data.value;
        });

        //向下查询
        $('div').on('click', '.next_serarch',function () {
            add_group = $(this).attr('data-value');
            open_window();
        });

        //返回上一级
        $('.go_back').bind('click', function () {
            if (param_group.length > 1) {
                param_group.length = param_group.length - 1;
            }else{
                layer.msg('<?= lang('当前已是初始状态！');?>', {icon: 5});
                return false;
            }
            console.log(param_group);
            $('#submit_query').click();
        });
    });

    function open_window() {
        layer_box = layer.open({
            id: 'query_box',
            type: 1,
            title: '<?= lang('筛选统计');?>',
            area: ['470px', '230px'],
            offset: '25%',
            shade: false,
            shadeClose:true,
            content: $('#query_table')
        });
        //$("input:radio[value='" + param_group_by +"']").prop("checked", true);
        form.render();
    }

    function loadData(data) {
        layer.close(layer_box);
        var loading = layer.msg('<?= lang('计算中，请耐心等待...');?>', {icon:16, time:0, shade: [0.2, '#393D49']});
        $.ajax({
            type: 'POST',
            url: '/biz_stat_client/'+action,
            data: data,
            dataType: 'json',
            success: function (res) {
                layer.close(loading);
                if (res.code == 1) {
                    laytpl($('#content_tpl').html()).render({data:res.data}, function (html) {
                        $('#content').html(html);
                        element.render();
                    });
                    $('#history_group').text(res.history_group);
                } else {
                    layer.alert(res.msg, {icon: 5});
                }
            },
            error:function (res) {
                layer.close(loading);
                layer.alert('<?= lang('请求失败！');?>', {icon: 5});
            }
        });
    }
</script>
</body>
</html>