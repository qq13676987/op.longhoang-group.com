<?php


class bsc_menu_model extends CI_Model
{
    private $table = 'bsc_menu';

    static public $field_edit = array('code', 'name', 'url', 'parent_id', 'm_type', 'display', 'param', 'icon', 'order', 'is_delete', 'created_by', 'created_time', 'updated_by'
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_field(){
        return self::$field_edit;
    }

    public function get_one($f = "", $v = "")
    {
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_where_one($where='', $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function save($data = array())
    {
        $data["created_time"] = date('Y-m-d H:i:s', time());
        $data["updated_time"] = date('Y-m-d H:i:s', time());
        $data["created_by"] = $this->session->userdata('id');
        $data["updated_by"] = $this->session->userdata('id');
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id = '', $data = array())
    {
        $data["updated_time"] = date('Y-m-d H:i:s', time());
        $data["updated_by"] = $this->session->userdata('id');
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id = '')
    {
        // delete container
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

}