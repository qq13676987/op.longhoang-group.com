<?php

/**
 * Class download
 * 该类用于下载等各种函数的相关处理
 */
class download extends Common_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 下载入口
     * 2022-02-16 新增下载日志记录功能
     */
    public function index(){
        $file_path = getValue('file_path'); // 文件路径
        $user_id = getValue('user_id', '');
        $id_type = getValue('id_type', '');
        $id_no = getValue('id_no', 0);

        if($user_id === '') $user_id = get_session('id'); //如果没有传,那么取系统里的
        
        //如果还是没有,不给下载
        if($user_id === '' || $user_id === false) exit($user_id . '无权登录');
        //这里后续可以加入,如果是系统里的,检测是否有查询权限那些
        $file_path_arr = explode('/', $file_path);
        //根据文件夹路径,判断是否是2月18号之前的,如果是,那么用远程获取,不是的话,本地获取
        $type = 'local';
        //2023-07-11 海外的 这个时间之前使用通用文件夹
        if($file_path_arr[3] <= '20230617'){
            unset($file_path_arr[2]);
            $file_path = join('/', $file_path_arr);
        }
//        if($file_path_arr[2] >= '20220218'){
//            $type = 'ali';//ali local
//        }else{
//            $type = 'local';//ali local
//        }
//        if($id_type == '' && $id_no == 0){
//            $type = 'local';
//        }
        if($type == 'ali')$download_file_url = $this->url_handle($file_path);
        else $download_file_url = $file_path;
        $file_name = end($file_path_arr);

        // if(is_admin()){
        //     var_dump($download_file_url);
        //     return;
        // }
        //判断文件是否存在 首先检测云端,没有再到本地检测
        if(!$this->file_exit($type, $download_file_url)){
            if($type == 'ali'){
                $type = 'local';
                if(!$this->file_exit($type, $download_file_url)){
                    echo '文件不存在';
                    return;
                }
            }
        }

        //记录下载日志
        $this->set_download_log($file_path, $file_name, $id_type, $id_no, $user_id);

        //处理下载链接
        $file_url = $this->get_url($type, $download_file_url);
        if($type == 'local' && is_dir($file_url)) exit("文件夹无法下载");
        //4.从浏览器下载
        $fp = fopen($file_url,"r");
        //获取文件大小
        $file_size = $this->getFileSize($type, $file_url);

        Header("Content-type: application/octet-stream");
        Header("Accept-Ranges: bytes");
        Header("Accept-Length: ".$file_size);
        //2022-08-01 SXHCC20049233并单保函%C2%A0(彩色发货人章,扫描件).docx 由于某些字符的原因,这里urlencode一下
        Header("Content-Disposition: attachment; filename=" . urlencode($file_name));

        $v_content = '';
        //循环是因为fread有8192的限制,不循环下载的文件不完整
        while (strlen($v_content) < $file_size) {
            $v_content .= fread($fp, $file_size - strlen($v_content));
        }
        echo $v_content;
        fclose($fp);
    }
    
    /**
     * 进行url处理等的地方
     */
    private function url_handle($url){
        $url = str_replace('+', '%2B', $url);
        $url = str_replace(' ', '%20', $url);
        $url = str_replace('#', '%23', $url);
        return $url;
    }
    
    /**
     * 查看文件,只限制图片
     */
    public function read_img(){
        $file_path = getValue('file_path'); // 文件路径
        $user_id = getValue('user_id', '');
        $data_type = getValue('data_type', 'img');

        $type = 'ali';//ali local
        if($user_id == '') $user_id = get_session('id'); //如果没有传,那么取系统里的

        //如果还是没有,不给下载
        if(!$user_id) exit('无权登录');

        //判断文件是否存在
        if(!$this->file_exit($type, $file_path)){
            echo '文件不存在';
            return;
        }

        //处理下载链接
        $file_url = $this->get_url($type, $file_path);
        //4.从浏览器下载
        $fp = fopen($file_url,"r");
       // 检测文件格式，检测文件是否存在
        if(strtolower(substr(strrchr($file_path,'.'),1)) == 'pdf'){
            header('Content-type: application/pdf');
            
            fpassthru($fp); // 读取pdf中的内容
            
            fclose($fp);
            return;
        }
        
        
        //获取文件大小
        $file_size = $this->getFileSize($type, $file_url);
        $v_content = '';
        //循环是因为fread有8192的限制,不循环下载的文件不完整
        while (strlen($v_content) < $file_size) {
            $v_content .= fread($fp, $file_size - strlen($v_content));
        }
        $header = get_headers($file_url, true);
        $mime_type = $header['Content-Type'];
        $base64_data = base64_encode($v_content);
        $base64_file = 'data:'.$mime_type.';base64,'.$base64_data;
        if($data_type == 'base64'){
            echo $base64_file;
        }else{
            echo "<img src='$base64_file'>";
        }
        fclose($fp);
    }

    /**
     * 记录下载日志
     * @param $file_path
     * @param $file_name
     * @param $id_type
     * @param $id_no
     * @param $user_id
     * @return bool
     */
    private function set_download_log($file_path, $file_name, $id_type, $id_no, $user_id){
        $download_model = Model('m_model');
        $download_model::$table = 'sys_download_log';

        $add_data = array();
        $add_data['file_path'] = $file_path;
        $add_data['file_name'] = $file_name;
        $add_data['id_type'] = $id_type;
        $add_data['id_no'] = $id_no;
        $add_data['user_id'] = $user_id;
        $add_data['insert_time'] = date('Y-m-d H:i:s');


        $download_model->save($add_data);
        return true;
    }

    /**
     * 阿里云OSS判断文件是否存在
     */
    private function aliOssFileExist($object){
        $accessKeyId = "LTAIcNVHORoWCXf7";
        $accessKeySecret = "I59vWVf5jhkhgtPGGSs5WAatS7DG9r";
        $endpoint = "http://oss-cn-shanghai.aliyuncs.com";
        $bucket= "opchina";

        require_once  APPPATH . '/libraries/oss_sdk/autoload.php';

        // 文件名称
        try{
            $ossClient = new OSS\OssClient($accessKeyId, $accessKeySecret, $endpoint);
            return $ossClient->doesObjectExist($bucket, $object);
        } catch(OSS\OssException $e) {
            return false;
        }
    }

    /**
     * 检测文件是否存在
     */
    private function file_exit($type = 'local', $file_path){
        if($type == 'local'){//如果是本地
            //首先 实现下载功能
            if($file_path[0] !== '.'){
                $file_path_arr = explode('/', $file_path);

                if(sizeof($file_path_arr) > 1) $file_path = '.' . $file_path;
            }

            //检测文件是否存在
            if(!file_exists($file_path)){
                return false;
            }

            return true;
        }else if($type == 'ali'){
            //这个是阿里的文件
            //如果最前面有/,这里去掉
            if($file_path[0] == '/') $file_path = substr($file_path, 1);
            return $this->aliOssFileExist($file_path);
        }else{
            return false;
        }
    }

    /**
     * 获取下载链接
     */
    public function get_url($type= 'local', $path = ''){
        if ($type == 'local') {//如果是本地
            if($path[0] !== '.') $path = '.' . $path;
            return $path;
        }else if($type == 'ali'){
            return "https://opchina.oss-cn-shanghai.aliyuncs.com{$path}";
        }else{
            return $path;
        }
    }

    /**
     * 获取文件大小
     * @param $type
     * @param $url
     * @param string $user
     * @param string $pw
     * @return mixed|string
     */
    private function getFileSize($type = 'local', $url,$user='',$pw='')
    {
        if($type == 'local'){//如果是本地用filesize
            return filesize($url);
        }else if($type == 'ali'){
//            $file = file_get_contents($url);
//            $size = strlen($file);
            $header_array = get_headers($url, true);
            $size = $header_array['Content-Length'];
            return $size;
        }
    }
    
    /**
     * 显示下载日志
     */
    public function download_log(){
        $data = array();
        $data['id_type'] = getValue('id_type', '');
        $data['id_no'] = getValue('id_no', '');

        $this->load->view('head');
        $this->load->view('/sys/download/index_view', $data);
    }

    /**
     * 获取AJAX下载日志数据
     */
    public function get_data(){
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $id_type = postValue('id_type', '');
        $id_no = postValue('id_no', '');
        $file_name = trim(postValue('file_name', ''));
        $fields = postValue('fields', array());

        //-------这个查询条件想修改成通用的 -------------------------------
        $where = array();
        
        if($id_type == '' || $id_no == '') $where[] = "1 = 0";//如果没参数就不显示
        else $where[] = "id_type = '{$id_type}' and id_no = '{$id_no}'";
        
        if($file_name != '') $where[] = "file_name = '{$file_name}'";

        if(!empty($fields)) foreach ($fields['f'] as  $key => $field){
            $symbol = $fields['s'][$key]; // 获取符号
            $value = trim($fields['v'][$key]);

            if(!empty($value)) $where[] = search_like($field, $symbol, $value);
        }

        $where_str = join(' and ', $where);
        //-----------------------------------------------------------------
        $download_model = Model('m_model');
        $download_model::$table = 'sys_download_log';

        $result["total"] = $download_model->total($where_str);

        $offset = ($page - 1) * $rows;
        $this->db->limit($rows, $offset);
        $rs = $download_model->get($where_str, $sort, $order);
        $result[] = lastquery();
        $rows = array();
        foreach ($rs as $row) {
            $row['user_name'] = getUserName($row['user_id']);
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }
}