<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>error</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css" media="all">
    <link href="/inc/third/layui/css/font-icon.css" rel="stylesheet" type="text/css">
    <style>
        .box{
            width: 100%;
            position: absolute;
            top: 30%;
            text-align: center;
        }
    </style>
</head>
<body>
<?php
if (!isset($icon) || $icon==''){
    $icon = "larry-kongshuju";
}
?>
<div class="box">
    <i class="icon larry-icon <?=$icon;?>" style="font-size: 80px; color: #38bb80;"></i>
    <div style="font-size: 16px; color: #ef3502; line-height: 25px;">
        <i class="icon larry-icon larry-msg" style="font-size: 16px; color: #ff6000;"></i>
        <?php echo $msg;?>
    </div>
</div>
</body>
</html>