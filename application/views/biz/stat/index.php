<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link href="/inc/third/layui/css/layui.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <style type="text/css">
        html, body { overflow: visible;}
        .nc-row { font-size: 0!important; *word-spacing:-1px/*IE6、7*/;}
        .nc-row li {cursor: pointer; font-size: 12px; vertical-align: top; letter-spacing: normal; display: inline-block!important; *display: inline/*IE7*/; *zoom:1/*IE7*/;}
        .nc-row li h6 {overflow: hidden; text-overflow: ellipsis; white-space: nowrap; width: 96%;}
        .clear { font-size: 0; line-height: 0; height: 0; clear: both; float: none; padding: 0; margin: 0; border: 0; zoom: 1; }

        .ncap-form-default { padding: 10px 0; overflow: hidden; }
        .ncap-form-default .title { padding: 10px 0; border-bottom: solid 1px #C8C8C8; }
        .ncap-form-default .title h3 { font-size: 16px; line-height: 20px; color: #333; font-weight: normal; }
        .ncap-form-default dl.row,
        .ncap-form-all dd.opt { font-size: 0; color: #777; background-color: #FFF; *word-spacing:-1px/*IE6、7*/;
            padding: 12px 0; margin-top: -1px; border-style: solid; border-width: 1px 0; border-color: #F0F0F0; position: relative; z-index: 1; }
        .ncap-form-default dl.row:first-child,
        .ncap-form-all dd.opt:first-child { border-top-color: #FFF; }
        .ncap-form-default dl.row:nth-child(even),
        .ncap-form-all dd.opt:nth-child(even) { background-color: #FDFDFD; }
        .ncap-form-default dl.row:hover,
        .ncap-form-all dd.opt:hover { color: #000; background-color: #FFF; border-style: dotted; border-color: #D7D7D7; z-index: 2; box-shadow: 0 0 4px rgba(0,0,0,0.05); }
        .ncap-form-default dt.tit,
        .ncap-form-default dd.opt { font-size: 12px; line-height: 24px; vertical-align: top; letter-spacing: normal; display: inline-block; *display: inline/*IE7*/;
            *zoom:1/*IE7*/;}
        /*整列表单*/
        .ncap-form-all { padding: 10px 0; }
        .ncap-form-all .title { padding-bottom: 10px; border-bottom: solid 1px #C8C8C8; }
        .ncap-form-all .title h3 { font-size: 16px; line-height: 20px; color: #333; font-weight: normal; display: inline-block; min-width: 190px; vertical-align: middle; }
        .ncap-form-all .title .tab-base { vertical-align: middle; }



        /*注释说明帮助*/
        .explanation { color: #16594a; background-color: #EDFBF8; display: block; width: 99%; height: 70px; padding: 6px 9px; border-radius: 5px; position: relative; overflow: ; }
        .explanation .title { white-space: nowrap; margin-bottom: 8px; position: relative; cursor: pointer; }
        .explanation .title h4 { font-size: 14px; font-weight: normal; line-height: 20px; height: 20px; display: inline-block; }
        .explanation .title i { font-size: 18px; vertical-align: middle; margin-right: 6px; }
        .explanation .title span {width: 20px; height: 20px; position: absolute; z-index: 1; top: -6px; right: -9px; }
        .explanation ul { color: #748A8F; margin-left: 10px; }
        .explanation li { line-height: 20px; padding-left: 10px; margin-bottom: 4px; text-shadow: 1px 1px 0 rgba(255,255,255,0.5); }
        /* 宽度\高度\尺寸
        ------------------------------------------------------------------- */


        .page { background-color: #FFF; min-width: 1000px; padding: 5px 1% 0 1%; text-align: left; overflow: hidden; }
        .fixed-bar { background-color: #FFF; width: 100%; padding-bottom: 4px; position: fixed; z-index: 99; top: 0; left: 0; }
        .item-title { line-height: 20px; white-space: nowrap; width: 98%; padding-top: 3px; margin: 0 1%; border-bottom: solid 1px #E6E6E6; }
        .item-title .subject { vertical-align: bottom; display: inline-block; *display: inline; *zoom: 1; min-width: 190px; height: 38px; padding: 6px 0; margin-right: 10px; }
        .item-title h3 { font-size: 16px; font-weight: normal; line-height: 20px; color: #333; }
        .item-title h5 { font-size: 12px; font-weight: normal; line-height: 18px; color: #999; }
        .tab-base { vertical-align: bottom; display: inline-block; *display: inline;
            *zoom: 1;
        }


        /*统计情报表*/
        .ncap-stat-general { }
        .ncap-stat-general .row { border: none !important; }
        .ncap-stat-general ul { overflow: hidden; }
        .ncap-stat-general li { width: 20%; height: 60px; position: relative; z-index: 1; border-top: dashed 1px #E7E7E7; border-left: dashed 1px #E7E7E7; margin: -1px 0 0 -1px; }
        .ncap-stat-general li h4 { font-size: 14px; line-height: 20px; color: #555; position: absolute; z-index: 1; top: 10px; left: 3%; }
        .ncap-stat-general li h6 { font-size: 12px; line-height: 20px; color: #999; font-weight: normal; position: absolute; z-index: 1; bottom: 10px; left: 3%; }
        .ncap-stat-general li h2 { font-size: 20px; line-height: 40px; font-weight: normal; color: #2cbca3; position: absolute; z-index: 1; top: 10px; right: 5%; }
        /*统计情报表-单行*/
        .ncap-stat-general-single { }
        .ncap-stat-general-single .row { border: none !important; }
        .ncap-stat-general-single .opt { background-color: #FBFBFB!important; border: none !important; }
        .ncap-stat-general-single ul { overflow: hidden; }
        .ncap-stat-general-single li { width: 16.5%; height: 20px; position: relative; z-index: 1; border-left: dashed 1px #D7D7D7; margin-left: -1px; }
        .ncap-stat-general-single li h4 { font-size: 14px; line-height: 20px; color: #555; position: absolute; z-index: 1; top: 0; left: 5%;}
        .ncap-stat-general-single li h6 { font-size: 12px; line-height: 20px; color: #999; font-weight: normal; position: absolute; z-index: 1; top: 0; right: 5%; }
        .ncap-stat-general-single li h2 { font-size: 20px; line-height: 20px; font-weight: normal; color: #2cbca3; position: absolute; z-index: 1; top: 0; right: 12%; }
        /*统计类型图标*/
        .ncap-stat-chart { padding: 10px 0; }
        .ncap-stat-chart .title { font-size: 0; padding-bottom: 10px; border-bottom: solid 1px #E6E6E6; }
        .ncap-stat-chart .title h3 { font-size: 16px; line-height: 20px; color: #333; font-weight: normal; vertical-align: middle; display: inline-block; }
        .ncap-stat-chart .title h5 { vertical-align: middle; display: inline-block; margin-left: 20px; }
        .ncap-stat-chart .no-date { font-size: 14px; color: #999; text-align: center; width: 100%; height: 24px; padding: 100px 0; }
        .ncap-stat-chart .no-date i { font-size: 18px; color: #fc0; margin-right: 6px; }
        .layui-card-body{padding: 10px 0px !important;}
        .layui-layer-lan .layui-layer-title {background: #325763 !important;}
    </style>
</head>
<body style=" overflow: auto;">

<div class="page">
    <!--筛选表单-->
    <div class="explanation" id="explanation" style="border: 1px solid #86e0a7;">
        <form class="layui-form" lay-filter="search">
            <div class="layui-form-item" style="margin:10px;">
                <div class="layui-inline" style="width: 370px;">
                    <label class="layui-form-label"><b><?= lang('选择部门');?></b></label>
                    <div class="layui-input-inline" id="group" style="width: 250px;">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label"><b><?= lang('日期范围');?></b></label>
                    <div class="layui-input-inline" style="width: 100px;">
                        <input type="text" name="date_range" class="layui-input" style="width: 200px; height: 40px;"
                               id="date_range" placeholder=" ~ ">
                    </div>
                </div>
                <div class="layui-inline" style="margin-left: 100px;">
                    <select name="select_date" lay-filter="date_str" >
                        <?php foreach ($date_option as $item): ?>
                            <option value="<?=$item['data_range'];?>"><?=$item['title'];?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="layui-inline" style="margin-left: 10px;">
                    <input type="hidden" name="type" id="type" value="">
                    <button class="layui-btn" id="formDemo" lay-submit lay-filter="formDemo"><?= lang('立即提交');?></button>
                    <button type="reset" id="reset" class="layui-btn layui-btn-primary"><?= lang('重置');?></button>
                    <span style="color: #ff6000; margin-left: 10px;"><?= lang('Tips:不选日期范围默认统计最近30天内的数据');?></span>
                </div>
            </div>
        </form>
    </div>

    <!--统计概览-->
    <div class="ncap-form-all ncap-stat-general">
        <dl class="row">
            <dd class="opt">
                <ul class="nc-row">
                    <li onclick="open_detail('stat_no_active', 'Client非活跃客户')">
                        <h4><b><?= lang('Client非活跃客户');?></b></h4>
                        <h6 onmousemove="removeClassType(this)"><?= lang('往来单位中有过交易但三个月内未下单的客户总数');?></h6>
                        <h2 class="timer" id="count-number"  data-to="<?=isset($no_active) ? $no_active : 0;?>" data-speed="1500"></h2>
                    </li>
                    <li onclick="open_detail('stat_active', 'Client活跃客户')">
                        <h4><b><?= lang('Client活跃客户');?></b></h4>
                        <h6 onmousemove="removeClassType(this)"><?= lang('往来单位中近三个月内有过交易记录的客户总数');?></h6>
                        <h2 class="timer" id="count-number"  data-to="<?=isset($active) ? $active : 0;?>" data-speed="1000"></h2>
                    </li>
                    <li onclick="open_detail('stat_add_client', 'Client新增客户')">
                        <h4><b><?= lang('Client新增客户');?></b></h4>
                        <h6 onmousemove="removeClassType(this)"><?= lang('往来单位来源于CRM转正和客户申请的总数');?></h6>
                        <h2 class="timer" id="count-number"  data-to="<?=isset($client_add) ? $client_add: 0;?>" data-speed="1000"></h2>
                    </li>
                    <li onclick="open_detail('crm_status_2', 'CRM跟进中')">
                        <h4><b><?= lang('CRM跟进中');?></b></h4>
                        <h6 onmousemove="removeClassType(this)"><?= lang('CRM中处于跟进状态的抬头数量');?></h6>
                        <h2 class="timer" id="count-number"  data-to="<?=isset($crm_status_2) ? $crm_status_2 : 0;?>" data-speed="1000"></h2>
                    </li>
                    <li id="crm_public">
                        <h4><b><?= lang('CRM公海数量');?></b></h4>
                        <h6 onmousemove="removeClassType(this)"><?= lang('CRM中未指派销售人员的抬头');?></h6>
                        <h2 class="timer" id="count-number"  data-to="<?=isset($crm_public) ? $crm_public : 0;?>" data-speed="1000"></h2>
                    </li>
                    <li onclick="open_detail('crm_add_by_hand', 'CRM手工新增')">
                        <h4><b><?= lang('CRM手工新增');?></b></h4>
                        <h6 onmousemove="removeClassType(this)"><?= lang('CRM中由销售人员手工添加的抬头总数');?></h6>
                        <h2 class="timer" id="count-number"  data-to="<?=isset($crm_add_by_hand) ? $crm_add_by_hand : 0;?>" data-speed="1000"></h2>
                    </li>
                    <li id="clue1">
                        <h4><b><?= lang('CRM线索1');?></b></h4>
                        <h6 onmousemove="removeClassType(this)"><?= lang('CRM中线索抬头的录入总数');?></h6>
                        <h2 class="timer" id="count-number"  data-to="<?=isset($crm_add_clue) ? $crm_add_clue : 0;?>" data-speed="1000"></h2>
                    </li>
                    <li id="clue2">
                        <h4><b><?= lang('CRM线索2');?></b></h4>
                        <h6 onmousemove="removeClassType(this)"><?= lang('往来单位3个月未交易的二级委托方总数');?></h6>
                        <h2 class="timer" id="count-number"  data-to="<?=isset($crm_clue_2) ? $crm_clue_2 : 0;?>" data-speed="1000"></h2>
                    </li>
                    <li onclick="open_detail('crm_clue_extraction', 'CRM线索提取')">
                        <h4><b><?= lang('CRM线索提取');?></b></h4>
                        <h6 onmousemove="removeClassType(this)"><?= lang('CRM来源线索1和线索2的抬头总数');?></h6>
                        <h2 class="timer" id="count-number"  data-to="<?=isset($crm_clue_extraction) ? $crm_clue_extraction : 0;?>" data-speed="1000"></h2>
                    </li>
                    <li onclick="open_detail('crm_conversion_rate', 'CRM转正数量')">
                        <h4><b><?= lang('CRM转正数量');?></b></h4>
                        <h6 onmousemove="removeClassType(this)"><?= lang('CRM抬头成功转为正式客户的总数');?></h6>
                        <h2 class="timer" id="crm_conversion_rate" data-to="<?=isset($crm_conversion_rate)?$crm_conversion_rate:0;?>" data-speed="1000"></h2>
                    </li>
                    <li onclick="open_detail('email_send_count', '邮件群发数量')">
                        <h4><b><?= lang('邮件群发数量');?></b></h4>
                        <h6 onmousemove="removeClassType(this)"><?= lang('邮件群发数量');?></h6>
                        <h2 class="timer" id="count-number" data-to="<?=isset($email_send_count)?$email_send_count:0;?>" data-speed="1000"></h2>
                    </li>
                </ul>
            </dd>
        </dl>
    </div>

    <!--往来单位统计图表-->
    <div class="layui-col-sm12">
        <div class="layui-card-header" style="background: #ebebeb; height: 10px;"></div>
        <div class="layui-card-body" style="padding-top: 0px !important;">
            <div class="layui-card-header" style="margin-bottom: 5px;"><?= lang('往来单位统计图表');?></div>
            <div class="layui-col-sm8">
                <div class=" layadmin-dataview" data-anim="fade" lay-filter="LAY-index-pagetwo">
                    <div id="LAY-index-pagetwo" style="width: 100%; height: 350px;">
                        <i class="layui-icon layui-icon-loading1 layadmin-loading"></i>
                    </div>
                </div>
            </div>
            <div class="layui-col-sm4" style="overflow: hidden; height: 350px;">
                <div class="layuiadmin-card-list" id="browse" style="width: 100%; height: 390px;"></div>
            </div>
        </div>
    </div>
    <div class="layui-col-sm12" style="background: #ebebeb; height: 10px;"></div>

    <!--CRM统计图表-->
    <div class="layui-col-sm12">
        <div class="layui-card-body" style="padding-top: 0px !important;">
            <div class="layui-card-header" style="margin-bottom: 5px;"><?= lang('CRM统计图表');?></div>
            <div class="layui-row">
                <div class="layui-col-lg3" id="crm_type" style="height: 300px; padding-left: 10px;"></div>
                <div class="layui-col-lg9">
                    <div class="layui-col-lg6" id="crm_add" style="height: 300px;"></div>
                    <div class="layui-col-lg6" id="crm_to_client" style="height: 300px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/inc/js/jquery.numberAnimation.js"></script>
<script type="text/javascript" src="/inc/third/layui/xm-select.js"></script>
<script src="/inc/third/layui/layui.js"></script>
<script>
    function removeClassType($this) {
        $($this).attr("title",$this.innerText);
    }

    var group;
    $(function () {
        group = xmSelect.render({
            el: '#group',
            name: 'group',
            model: { label: { type: 'text' } },
            radio: true,
            clickClose: true,
            tree: {
                show: true,
                strict: false,
                expandedKeys: <?php echo isset($where) ? "['{$where['group']}']" : "true";?>,
                clickExpand: true,
                //点击节点是否选中
                clickCheck: true,
            },
            height: '500px',
            data:<?=json_encode($user_role);?>,
            on: function(data){
                var type = data.change[0].type;
                $('#type').val(type);
            },
        });

        <?php if (!isset($where) || $where['group'] == ''):?>
        //默认选中
        $('.xm-option').each(function (index, item) {
            var value = $(item).attr('value');
            if ($(item).is('.disabled') == false) {
                group.setValue([value]);
                if (value == 'HEAD') {
                    $('#type').val('HEAD');
                } else if (typeof value == 'string') {
                    $('#type').val('group');
                } else {
                    $('#type').val('user');
                }
                return false;
            }
        })
        <?php else:?>
        group.setValue(['<?=$where['group'];?>']);
        <?php endif;?>

        //重置
        $('#reset').click(function () {
            $('.xm-option').each(function (index, item) {
                var value = $(item).attr('value');
                if ($(item).is('.disabled') == false){
                    group.setValue([value]);
                    if (value == 'HEAD'){
                        $('#type').val('HEAD');
                    }else if(typeof value  == 'string'){
                        $('#type').val('group');
                    }else{
                        $('#type').val('user');
                    }
                    return false;
                }
            })
        })

        $('#clue1').click(function () {
            layer.open({
                type: 2,
                title: '<?= lang('线索');?>',
                area: ['1590px', '90%'],
                skin: 'layui-layer-lan',
                shade: 0.5,
                content: '/biz_crm/clue'
            });
        });

        $('#clue2').click(function () {
            layer.open({
                type: 2,
                title: '<?= lang('线索2');?>',
                area: ['1590px', '90%'],
                skin: 'layui-layer-lan',
                shade: 0.5,
                content: '/biz_clue/index'
            });
        });

        $('#crm_public').click(function () {
            layer.open({
                type: 2,
                title:'<?= lang('公海客户');?>',
                skin: 'layui-layer-lan',
                area: ['1590px','90%'],
                shade:0.5,
                content:'/biz_crm/public_client'
            });
        });
    });


    layui.extend({
        echarts: '/inc/third/layui/lay/modules/echarts',
        echartsTheme: '/inc/third/layui/lay/modules/echartsTheme',
    }).use(['laydate', 'echarts', 'form'], function () {
        var $ = layui.$
            ,form = layui.form
            ,echarts = layui.echarts
            ,laydate = layui.laydate;

        laydate.render({
            elem: '#date_range'
            , range: '~'
            ,value: '<?php echo date('Y-m-d', strtotime('-30 day')) .' ~ '. date('Y-m-d');?>'
            ,max: '<?=date('Y-m-d');?>'
        });

        form.on('select(date_str)', function(data){
            $('#date_range').val(data.value)
        });

        //监听提交
        form.on('submit(formDemo)', function(data){
            var loading = layer.msg('<?= lang('计算中，请耐心等待...');?>', {icon:16, time:0, shade: [0.2, '#393D49']});
            data.field.group = group.getValue('valueStr');
            data.field.date_range = $('#date_range').val();
            data.field.group_by = 'sales_group';
            $.ajax({
                type: 'POST',
                url: '/biz_stat/get_data',
                data: data.field,
                dataType: 'json',
                success: function (res) {
                    layer.close(loading);
                    if (res.code == 1) {
                        location.reload();
                    } else {
                        layer.alert(res.msg, {icon: 5});
                    }
                },
                error:function (res) {
                    layer.close(loading);
                    layer.alert('<?= lang('请求失败！');?>', {icon: 5});
                }
            })

            return false;
        });

        <?php if (isset($where)):?>
        form.val("search", <?=json_encode($where);?>);
        var echartsApp3 = [], options = [
            {
                title: {
                    text: '<?= lang('近30天CRM抬头转正走势');?>',
                    x: 'center',
                    textStyle: {
                        fontSize: 14
                    }
                },
                tooltip : {
                    trigger: 'axis'
                },
                calculable : true,
                xAxis : [
                    {
                        type : 'category',
                        boundaryGap : false,
                        data : <?=json_encode(array_keys($crm_to_client_line_chart));?>
                    }
                ],
                yAxis : [
                    {
                        type : 'value',
                        axisLabel : {
                            formatter: '{value}'
                        }
                    },
                ],
                series : [
                    {
                        name:'<?= lang('CRM转正');?>',
                        type:'line',
                        itemStyle: {
                            normal: {
                                color: "#FF5252",
                                lineStyle: {
                                    color: "#FF5252"
                                }
                            }
                        },
                        data:<?=json_encode(array_values($crm_to_client_line_chart));?>
                    }
                ]
            }
        ]
            ,elemDataView3 = $('#crm_to_client')
            , renderDataView3 = function (index) {
            echartsApp3[index] = echarts.init(elemDataView3[index], layui.echartsTheme);
            echartsApp3[index].setOption(options[index]);
            window.onresize = echartsApp3[index].resize;
        };
        //没找到DOM，终止执行
        if(!elemDataView3[0]) return;
        renderDataView3(0);

        var echartsApp4 = [], options = [
            {
                title: {
                    text: '<?= lang('近30天CRM新增抬头走势');?>',
                    x: 'center',
                    textStyle: {
                        fontSize: 14
                    }
                },
                tooltip : {
                    trigger: 'axis'
                },
                calculable : true,
                xAxis : [
                    {
                        type : 'category',
                        boundaryGap : false,
                        data : <?=json_encode(array_keys($crm_add_line_chart));?>
                    }
                ],
                yAxis : [
                    {
                        type : 'value',
                        axisLabel : {
                            formatter: '{value}'
                        }
                    },
                ],
                series : [
                    {
                        name:'<?= lang('新增CRM抬头');?>',
                        type:'line',
                        itemStyle: {
                            normal: {
                                color: "#FF5252",
                                lineStyle: {
                                    color: "#FF5252"
                                }
                            }
                        },
                        data:<?=json_encode(array_values($crm_add_line_chart));?>
                    }
                ]
            }
        ]
            ,elemDataView4 = $('#crm_add')
            , renderDataView4 = function (index) {
            echartsApp4[index] = echarts.init(elemDataView4[index], layui.echartsTheme);
            echartsApp4[index].setOption(options[index]);
            window.onresize = echartsApp4[index].resize;
        };
        //没找到DOM，终止执行
        if(!elemDataView4[0]) return;
        renderDataView4(0);

        var echartsApp5 = [], options5 = [
            //访客浏览器分布
            {
                title : {
                    text: '<?= lang('当前CRM抬头开发进度占比');?>',
                    x: 'center',
                    textStyle: {
                        fontSize: 14
                    }
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient : 'vertical',
                    x : 'left',
                    data:['<?= lang('申请中');?>','<?= lang('跟进中');?>','<?= lang('转正中');?>','<?= lang('已转正');?>']
                },
                series : [{
                    name:'<?= lang('CRM分类占比');?>',
                    type:'pie',
                    radius: [50, 100],
                    center: ['60%', '50%'],
                    roseType: 'area',
                    itemStyle: {
                        borderRadius: 4
                    },
                    data:[
                        {value:<?=$crm_status_1;?>, name:'<?= lang('申请中');?>'},
                        {value:<?=$crm_status_2;?>, name:'<?= lang('跟进中');?>'},
                        {value:<?=$crm_status_3;?>, name:'<?= lang('转正中');?>'},
                        {value:<?=$crm_status_0;?>, name:'<?= lang('已转正');?>'},
                    ]
                }]
            }
        ]
            ,elemDataView5 = $('#crm_type')
            ,renderDataView5 = function (index) {
            echartsApp5[index] = echarts.init(elemDataView5[index], layui.echartsTheme);
            echartsApp5[index].setOption(options5[index]);
            window.onresize = echartsApp5[index].resize;
        };
        //没找到DOM，终止执行
        if(!elemDataView5[0]) return;
        renderDataView5(0);

        var echartsApp2 = [], options2 = [
            //访客浏览器分布
            {
                title : {
                    text: '<?= lang('当前往来单位客户活跃度占比');?>',
                    x: 'center',
                    textStyle: {
                        fontSize: 14
                    }
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient : 'vertical',
                    x : 'left',
                    data:['<?= lang('活跃客户');?>','<?= lang('非活跃客户');?>','<?= lang('黑名单');?>']
                },
                series : [{
                    name:'<?= lang('客户类型');?>',
                    type:'pie',
                    radius : '60%',
                    center: ['50%', '50%'],
                    data:[
                        {value:<?=isset($active)?$active:0;?>, name:'<?= lang('活跃客户');?>'},
                        {value:<?=isset($no_active)?$no_active:0;?>, name:'<?= lang('非活跃客户');?>'},
                        {value:<?=isset($client_black_list)?$client_black_list:0;?>, name:'<?= lang('黑名单');?>'},
                    ]
                }]
            }
        ]
            ,elemDataView2 = $('#browse')
            , renderDataView2 = function (index) {
            echartsApp2[index] = echarts.init(elemDataView2[index], layui.echartsTheme);
            echartsApp2[index].setOption(options2[index]);
            window.onresize = echartsApp2[index].resize;
        };
        //没找到DOM，终止执行
        if(!elemDataView2[0]) return;
        renderDataView2(0);


        var echartsApp = [], options = [
            {
                title: {
                    text: '<?= lang('近30天往来单位新增客户走势');?>',
                    x: 'center',
                    textStyle: {
                        fontSize: 14
                    }
                },
                tooltip : {
                    trigger: 'axis'
                },
                calculable : true,
                xAxis : [
                    {
                        type : 'category',
                        boundaryGap : false,
                        data : <?=json_encode(array_keys($client_line_chart));?>
                    }
                ],
                yAxis : [
                    {
                        type : 'value',
                        axisLabel : {
                            formatter: '{value}'
                        }
                    },
                ],
                series : [
                    {
                        name:'<?= lang('新增客户');?>',
                        type:'line',
                        data:<?=json_encode(array_values($client_line_chart));?>
                    }
                ]
            }
        ]
            ,elemDataView = $('#LAY-index-pagetwo')
            , renderDataView = function (index) {
            echartsApp[index] = echarts.init(elemDataView[index], layui.echartsTheme);
            echartsApp[index].setOption(options[index]);
            window.onresize = echartsApp[index].resize;
        };
        //没找到DOM，终止执行
        if(!elemDataView[0]) return;
        renderDataView(0);
        <?php else: ?>
        $('#formDemo').click();
        <?php endif;?>
    });

    function open_detail(action, title) {
        var where = "<?php echo isset($where) ? http_build_query($where) : '';?>";
        var url = '/biz_stat_client/index/?action='+action+'&'+where;
        layer.open({
            type: 2,
            title: '<?= lang('数据统计');?> —— ' + title,
            area: ['1100px', '710px'],
            content: [url, 'no'],
            offset: '100px',
            scrollbar: false,
            skin: 'layui-layer-lan',
        })
    }

</script>
</body>
</html>