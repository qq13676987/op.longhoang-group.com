<?php

class bsc_user_role extends Menu_Controller
{
    public function __construct()
    {

        parent::__construct();
        $this->load->model('bsc_user_role_model');

        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "create_by" || $v == "create_time" || $v == "update_by" || $v == "update_time") continue;
            array_push($this->field_edit, $v);
        }
    }

    public $field_all = array(
        array('id', 'id', '80', '1'),
        array('user_role', 'user_role', '100', '3'),
        array('read_text', 'read_text', '100', '4'),
        array('edit_text', 'edit_text', '100', '5'),
        array('delete_text', 'delete_text', '100', '5'),
        array('template_text', 'template_text', '100', '5'),
        array('menu', 'menu', '100', '5'),
        array('special_text', 'special_text', '100', '5'),
        array('pid', 'pid', '0', '5'),
        array('level', 'level', '0', '5'),
        array('create_time', 'create_time', '100', '6'),
//        array('create_by', 'create_by', '100', '6'),
        array('update_time', 'update_time', '100', '6'),
//        array('update_by', 'update_by', '100', '6'),
    );
    public $field_edit = array();

    public function index()
    {
        $data = array();
        
        if (is_admin()) {
            $data['user_role'] = getValue('user_role', 0);

            $this->load->view('head');
            $this->load->view('bsc/user_role/index_view', $data);
        } else {
            echo "Admin Only";
        }
    }

    public function get_data()
    {
        $result = array();
        $page = postValue('page', 1);
        $rows = intval(postValue('rows', 30));
        $sort = strval(postValue('sort', 'id'));
        $order = strval(postValue('order', 'desc'));

        //-------where -------------------------------
        $where = array();
        $f1 = postValue('f1', '');
        $s1 = postValue('s1', '');
        $v1 = postValue('v1', '');
        $f2 = postValue('f2', '');
        $s2 = postValue('s2', '');
        $v2 = postValue('v2', '');
        $f3 = postValue('f3', '');
        $s3 = postValue('s3', '');
        $v3 = postValue('v3', '');
        if ($v1 != "") $where[] = "$f1 $s1 '$v1'";
        if ($v2 != "") $where[] = "$f2 $s2 '$v2'";
        if ($v3 != "") $where[] = "$f3 $s3 '$v3'";
        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->bsc_user_role_model->total($where);
        $this->db->limit($rows, $offset);
        $rs = $this->bsc_user_role_model->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {

            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        return success(0, 'success', $result);
    }

    public function add_data()
    {
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_REQUEST[$item]) ? $_REQUEST[$item] : '';
            $data[$item] = $temp;
        }

        //处理3个权限数据
        $array['consol'] = explode(',', isset($_POST['consol_text']) ? $_POST['consol_text'] : '');
        $array['shipment'] = explode(',', isset($_POST['shipment_text']) ? $_POST['shipment_text'] : '');
        $array['client'] = explode(',', isset($_POST['client_text']) ? $_POST['client_text'] : '');
        $array['bill'] = explode(',', isset($_POST['bill_text']) ? $_POST['bill_text'] : '');
        $array = $this->r_e_handle($array, 1);
        $data = array_merge($data, $array);

        $id = $this->bsc_user_role_model->save($data);
        
        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "bsc_user_role";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        $data['id'] = $id;

        echo json_encode(array($data));
    }
    
    /**
     * 改版后只保留这个,add_data和update
     */
    public function save_data(){
        $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : 0;
        $action = 'insert';
        if ($id != 0) {
            $old_row = $this->bsc_user_role_model->get_one('id', $id);
            $action = 'update';
        }

        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            if(isset($_POST[$item])){
                $temp = $_REQUEST[$item];
                if (!empty($old_row) && $old_row[$item] == $temp) continue;
                $data[$item] = $temp;
            }
        }
        if($action == 'insert') $data['level'] = 2;

        //处理3个权限数据
        $array['consol'] = explode(',', isset($_POST['consol_text']) ? $_POST['consol_text'] : '');
        $array['shipment'] = explode(',', isset($_POST['shipment_text']) ? $_POST['shipment_text'] : '');
        $array['client'] = explode(',', isset($_POST['client_text']) ? $_POST['client_text'] : '');
        $array['bill'] = explode(',', isset($_POST['bill_text']) ? $_POST['bill_text'] : '');
        $array = $this->r_e_handle($array, 1);
        $data = array_merge($data, $array);

        if(isset($data['menu'])) unset($data['menu']);
        // echo json_encode($data);
        // return;
        if(empty($old_row)) $id = $this->bsc_user_role_model->save($data);
        else $this->bsc_user_role_model->update($id, $data);

        $menu = isset($_POST['menu']) ? $_POST['menu'] : '';

        Model('bsc_menu_relation_model');
        $this->bsc_menu_relation_model->batch_save('bsc_user_role', $id, $menu);

        $data["id"] = $id;

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "bsc_user_role";
        $log_data["key"] = $id;
        $log_data["action"] = $action;
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        $data['msg'] = '保存成功';
        echo json_encode($data);
    }

    public function update_data()
    {
        $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : 0;
        if ($id == 0) {
            echo "id should not be 0";
            exit;
        }

        $old_row = $this->bsc_user_role_model->get_one('id', $id);
        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_REQUEST[$item]) ? $_REQUEST[$item] : '';
            if ($old_row[$item] != $temp)
                $data[$item] = $temp;

        }
        if (isset($data['password']) && strlen($data["password"]) < 12) {
            $data["password"] = md5($data["password"]);
        }

        //处理3个权限数据
        $array['consol'] = explode(',', isset($_POST['consol_text']) ? $_POST['consol_text'] : '');
        $array['shipment'] = explode(',', isset($_POST['shipment_text']) ? $_POST['shipment_text'] : '');
        $array['client'] = explode(',', isset($_POST['client_text']) ? $_POST['client_text'] : '');
        $array['bill'] = explode(',', isset($_POST['bill_text']) ? $_POST['bill_text'] : '');
        $array = $this->r_e_handle($array, 1);
        $data = array_merge($data, $array);

        $this->bsc_user_role_model->update($id, $data);

        $data["id"] = $id;
        
        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "bsc_user_role";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        echo json_encode(array($data));

    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->bsc_user_role_model->get_one('id', $id);
        $this->bsc_user_role_model->mdelete($id);
        echo json_encode(array('success' => true));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "bsc_user_role";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
    }

    public function get_role_one($value = '', $field = 'id')
    {
        $data = $this->bsc_user_role_model->get_one($field, $value);

        if (!empty($data)) {
            $newArray = array(
                'read_text' => explode(',', $data['read_text']),
                'edit_text' => explode(',', $data['edit_text']),
            );
            $newArray = $this->r_e_cut($newArray, 1);

            //目录的信息得关联查询
            Model('bsc_menu_relation_model');
            $menu = array_column($this->bsc_menu_relation_model->get_menu_relation('bsc_user_role', $data['id']), 'menu_id');
            $data['menu'] = join(',', $menu);
            $data = array_merge($data, $newArray);
        }

        echo json_encode($data);
    }

    /**
     * 处理传过来的数组的值,数组键名为需要替换的字段,整理成read和edit返还回去
     * @param $data
     * @param int $return_type
     * @return array
     */
    public function r_e_handle($data, $return_type = 0)
    {
        //需要匹配的字段
        $newArray = array(
            'read' => '1read',
            'edit' => '1edit',
        );

        $array = array(
            'read_text' => array(),
            'edit_text' => array(),
        );
        //匹配数组中的值，
        foreach ($data as $key => $val) {
            //匹配成功的值，替换
            foreach ($val as $vtkey => $vtval) {
                if ($vtval != '') {
                    if (strpos($vtval, $newArray['read']) !== false) {
                        $this_text = substr_replace($vtval, $key, strpos($vtval, $newArray['read']), strlen($newArray['read']));
                        !in_array($this_text, $array['read_text']) ? $array['read_text'][] = $this_text : false;
                    } else if (strpos($vtval, $newArray['edit']) !== false) {
                        $this_text = substr_replace($vtval, $key, strpos($vtval, $newArray['edit']), strlen($newArray['edit']));
                        //是否存在于read中，不存在添加
                        if(!in_array($this_text, $array['read_text'])){
                            $array['read_text'][] = $this_text;
                        }
                        $array['edit_text'][] = $this_text;
                    }
                }
            }
        }

        if ($return_type == 1) {
            foreach ($array as $key => $val) {
                $array[$key] = join(',', $val);
            }
        }

        return $array;
    }

    /**
     * 分割read和edit数组的值
     * @param $data
     * @param int $return_type
     * @return array
     */
    public function r_e_cut($data, $return_type = 0)
    {
        //需要匹配的字段
        $newArray = array(
            'read' => '1read',
            'edit' => '1edit',
        );

        $data_corr = array(
            'edit_text' => 'edit',
            'read_text' => 'read',
        );

        $array = array(
        );

        //匹配数组中的值，
        //$data为数据库传过来的类似array('edit_text' => array(....),'read_text' => array(...))
        foreach ($data as $key => $val) {
            //匹配成功的值，替换
            foreach ($val as $vtkey => $vtval) {
                //$data_corr[$key] 等于 edit 或 read
                if (explode('.', $vtval)[0] == 'consol') {
                    $this_text = 'consol';
                }else if (explode('.', $vtval)[0] == 'shipment') {
                    $this_text = 'shipment';
                }else if (explode('.', $vtval)[0] == 'client') {
                    $this_text = 'client';
                }else if (explode('.', $vtval)[0] == 'bill') {
                    $this_text = 'bill';
                }else{
                    continue;
                }
                $prefix = $newArray[$data_corr[$key]];
                if($prefix == '1read'){
                    $array[$this_text . '_text_read'][] = substr_replace($vtval, $prefix, strpos($vtval, $this_text), strlen($this_text));
                }else{
                    $array[$this_text . '_text_edit'][] = substr_replace($vtval, $prefix, strpos($vtval, $this_text), strlen($this_text));
                }
            }
        }

        if ($return_type == 1) {
            foreach ($array as $key => $val) {
                $array[$key] = join(',', $val);
            }
        }

        return $array;
    }

    public function get_role_tree(){
        $where = array();
        $where[] = 'isdelete = 0';
        $where = join(' and ', $where);

        $rows = $this->bsc_user_role_model->get($where);
        $tree = tree($rows, 0, 'children');
        $data = array();
        foreach ($tree as $row){
            $row['id'] = $row['user_role'];
            $row['text'] = $row['user_role'];

            if(isset($row['children']))foreach ($row['children'] as $c_key => $c_row){
                $c_row['id'] = $c_row['user_role'];
                $c_row['text'] = $c_row['user_role'];
                $row['children'][$c_key] = $c_row;
            }
            $data[] = $row;
        }
        echo json_encode($data);
    }

    public function get_role(){
        $where = array();
        $where[] = 'isdelete = 0';
        $where[] = 'level = 1';
        $where[] = 'pid = 0';
        $where = join(' and ', $where);

        $this->db->select('id,user_role');
        $data = $this->bsc_user_role_model->get($where);
//        array_unshift($data, array('id' => '', 'user_role' => ''));
        echo json_encode($data);
    }
    
    public function get_tree()
    {
        $user_role = getValue('user_role',0);
        $where = array();
        if($user_role !== 0)$where[] = "id in ('" . join('\',\'', filter_unique_array(explode(',', $user_role))) . "')";
        $where_str = join(' and ', $where);

        $this->db->select('id,user_role as text,pid,level');
        $user_roles = $this->bsc_user_role_model->get($where_str, 'user_role', 'asc');
        $data = getTree($user_roles);
//        $data = $user_roles;

        echo json_encode($data);
    }

    public function get_no_tree()
    {
        $user_role = getValue('user_role',0);
        $where = array();
        if($user_role !== 0)$where[] = "id in ('" . join('\',\'', filter_unique_array(explode(',', $user_role))) . "')";
        $where_str = join(' and ', $where);

        $this->db->select('id,user_role as text,pid,level');
        $user_roles = $this->bsc_user_role_model->get($where_str, 'user_role', 'asc');
//        $data = getTree($user_roles);
        $data = $user_roles;

        echo json_encode($data);
    }
}