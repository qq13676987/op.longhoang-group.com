<?php

/**
 * 邮件审核
 */
class crm_promote_mail_auditing extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->db = Model('op_model')->get_db();
        $this->load->library('Mail');
        $this->load->model('crm_promote_mail_model');
        $this->load->model('crm_promote_consignee_model');
        $this->load->model('m_model');
        $this->load->model('bsc_user_model');
        $is_loaded =& is_loaded();
        unset($is_loaded['session']);//清理掉session的自动加载
    }

    public function index()
    {
        $this->load->view('head');
        $this->load->view('biz/crm/crm_promote_mail_auditing/index');
    }

    /**
     * 审核邮件列表
     * @return void
     */
    public function get_data()
    {
        $result = array();
        $page = isset($_REQUEST['page']) ? intval($_REQUEST['page']) : 1;
        $rows = isset($_REQUEST['rows']) ? intval($_REQUEST['rows']) : 10;
        $sort = isset($_REQUEST['sort']) ? strval($_REQUEST['sort']) : "id";
        $order = isset($_REQUEST['order']) ? strval($_REQUEST['order']) : 'desc';
        $offset = ($page - 1) * $rows;
        $where = array();
        // $where['status']=0;
        $result["total"] = $this->crm_promote_mail_model->total($where);
        $this->db->select('id,send_group_id,create_by,status,send_group_name,reason,mail_title,sender_name,create_time');
        $this->db->limit($rows, $offset);
        $rs = $this->crm_promote_mail_model->get($where, $sort, $order);
        foreach ($rs as &$item) {
            $u = $this->m_model->query_one("select `name` from bsc_user where id = {$item["create_by"]}");
            $item["create_name"] = $u["name"];
            $item["send_group"] = "<a href='/crm_promote_mail/group_consignee_list?group_id={$item["send_group_id"]}' target='_blank'>" . $item["send_group_name"] . "</a>";
            $item["status_str"] = "<span style='color: #e38809'>" . $this->crm_promote_mail_model->status_s($item["status"]) . "</span>";
            if ($item["status"] == 0) {
                $item["operate"] = "<a href='javascript:void(0)' style='color: #1E9FFF' onclick='view_details({$item["id"]})'>审核并发送</a> <a href='javascript:void(0);' onclick='edit({$item['id']})'>编辑</a>";
            } else if ($item["status"] == 2) {
                $item["operate"] = "已发送成功";
            } else if ($item["status"] == -1) {
                $item["operate"] = $item["reason"];
            } else {
                $item["operate"] = $item["status"];
            }
            $item["operate"] .= " <a href='/crm_promote_mail/mail_log/{$item["id"]}' target='_blank'>查看详情</a>";
        }
        $result["rows"] = $rs;
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * 审核邮件
     * @return void
     */
    function auditing()
    {
        $data = array();
        $reData = array('code' => 400, 'msg' => '失败');
        $id = $_GET['id'];
        $data['status'] = 1;
        $re = $this->crm_promote_mail_model->update($id, $data);
        if ($re > 0) {
            $reData['code'] = 200;
            $reData['msg'] = "审核成功";
        }
        echo json_encode($reData, JSON_UNESCAPED_UNICODE);
    }

    function view_details()
    {
        $data = array();
        $result = array('code' => 400, 'msg' => '失败');
        $id = $_GET['id'];
        $data = $this->crm_promote_mail_model->get_row(array("id" => $id));
        $html = $this->load->view('biz/crm/crm_promote_mail_auditing/view_details', $data, true);
        $result["ht"] = $html;
        $result["code"] = 200;
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    /**
     * 审核 拒绝 或发送
     * @return void
     */
    function do_it()
    {
        $id = $_REQUEST['id'];
        $reData = array('code' => 400, 'msg' => lang('失败'));
        $row = $this->crm_promote_mail_model->get_row(array('id' => $id));
        if ($row['status'] != 0) {
            $reData["msg"] = lang('邮件已处理完毕');
            $reData["code"] = 201;
            echo json_encode($reData, JSON_UNESCAPED_UNICODE);
            exit;
        }
        $re_total = $this->crm_promote_consignee_model->total(array("group_id" => $row['send_group_id']));
        if ($re_total == 0) {
            $reData["msg"] = lang('该组无任何邮件联系人,请添加该组联系人');
            $reData["code"] = 201;
            echo json_encode($reData, JSON_UNESCAPED_UNICODE);
            exit;
        }

        //添加自己
        $this->addself($_GET['group_id']);

        //re 发送组
        $re = $this->crm_promote_consignee_model->get_result(array("group_id" => $row['send_group_id']));

        //倒序将自己排在第一位
        $re = array_reverse($re);


        //print_r($re);exit();
        //2022-04-20 新加入,如果不是全部验证过的话,就不给发送

        /*2024-01-05 老徐注释，因校验前就得把Email写入到待发送表
        $ok_arr = array_column($re, 'ok');
        $ok_arr_unique = array_unique($ok_arr);
        if (in_array(0, $ok_arr_unique)) { // 只要存在null或 0 的 就不给发送
            $no_valid_email_count = array_count_values($ok_arr);
            $wait_count = isset($no_valid_email_count['0']) ? $no_valid_email_count['0'] : 0;

            $reData["msg"] = lang('该组剩余 {wait_count} 个邮箱正在有效性检测,请耐心等。', array('wait_count' => $wait_count));
            $reData["code"] = 201;
            echo json_encode($reData, JSON_UNESCAPED_UNICODE);
            exit;
        }elseif (in_array(-1, $ok_arr_unique)) {
            $reData["msg"] = lang('请返回到发送组，删除全部红色的邮箱后再确认发送！');
            $reData["code"] = 201;
            echo json_encode($reData, JSON_UNESCAPED_UNICODE);
            exit;
        }

        $wait_count = 0;
        if (in_array(-1, $ok_arr_unique)) { // 只要存在-1的 就不给发送
            $no_valid_email_count = array_count_values($ok_arr);
            $wait_count = isset($no_valid_email_count['-1']) ? $no_valid_email_count['-1'] : 0;
        }*/
        $log_id = 0;
        $date_s = date('Y-m-d H:i:s');
        $data = array();
        foreach ($re as $v) {
            //if ($v["ok"] != 1) continue;
            //if (!check_mail_valid($v["email"])) continue;
            $data[] = array(
                'mail_id'       => $row["id"],
                'rcv_name'      => $v["name"],
                'rcv_email'     => $v["email"],
                'open_time'     => '',
                'send_time'     => '',
                'created_time'  => $date_s,
				'valid_flag'     => time(),
            );
        }

        if (!$this->db->insert_batch('crm_promote_mail_log', $data)) {
            $reData["msg"] = lang('无可发送有效邮箱');
            $reData["code"] = 201;
            $this->crm_promote_mail_model->update($id, array('status' => -1, 'reason' => lang('无有效邮箱')));
            echo json_encode($reData, JSON_UNESCAPED_UNICODE);
            exit;
        }

        $this->crm_promote_mail_model->update($id, array('status' => 2, 'reason' => ""));
        $reData['code'] = 200;
        $reData['msg'] = lang('邮件已进入发送队列。');
        echo json_encode($reData, JSON_UNESCAPED_UNICODE);
    }


    //以下函数废弃  2022-05-15
    private function preConfAndSend()
    {

        $userBsc = $this->bsc_user_model->get_by_id($this->create_by);
        $SMTP_password = "SMtp123456";
        $SMTP_username = "";
        if (!empty($userBsc["email_edm"])) $SMTP_username = $userBsc["email_edm"];
        $reply_email = "it_lin@leagueshipping.com";
        if (!empty($userBsc["email"])) $reply_email = $userBsc["email"];

        $rms = array();
        //$rcv = $userBsc["email"];
        foreach ($this->receivers as $r) {
            //$arr[] = $r;
            $date_s = date('Y-m-d H:i:s');
            $id = $this->m_model->query_id("INSERT INTO `crm_promote_mail_log`(`mail_id`, `rcv_name`, `rcv_email`, `open_time`, `created_time`) VALUES ({$r["mail_id"]}, '{$r["name"]}', '{$r["email"]}', NULL, '{$date_s}');");

            $img = "https://client.leagueshipping.com/crm_promote_mail_handle/img/$id";
            $htm = "<img src=\"" . $img . "\" style=\"display:none;\"></img>";

            // $mail = new Mail();
            // $mail::$SMTP_host = "smtpdm.aliyun.com";
            // $mail::$SMTP_username = $SMTP_username;
            // $mail::$SMTP_password = $SMTP_password;
            // $mail::$sender = array("email"=>$SMTP_username);
            // $mail::$receivers = array("name"=>$r["name"],"email"=>$r["email"]);
            // $mail::$reply_by = array("name"=>"leagueshipping","email"=>$reply_email);
            // $mail::$subject = $r["name"].": ".$this->subject;
            // $mail::$body = $r["name"]."<br>".$this->body.$htm;
            // $send_msg = $mail->send_mail();
            // $status = -1;
            // if($send_msg=="Message has been sent") $status = 1;
            // $this->m_model->query("update crm_promote_mail_log set status = $status where id = '{$id}';");
            $send_msg = "Message has been sent";
            $rms[] = $send_msg;

        }

        return $rms;
    }

    //发送邮件前，添加自己
    public function addself($group_id = '')
    {
        if (!empty($group_id)) {

            //获取自己的id
            $session_id = get_session('id');
            $user = $this->db->where('id', $session_id)->get('bsc_user')->row_array();

            $bl = $this->db->where('group_id', $group_id)->where('name', $user['name'])->get('crm_promote_consignee')->row_array();
            if (!empty($bl)) {
                //不管有没有都删除
                $this->db->where('id', $bl['id'])->delete('crm_promote_consignee');
            }
            //新增
            $add_datas = array(
                'name' => $user['name'],
                'phone' => $user['telephone'],
                'email' => $user['email'],
                'group_id' => $group_id,
                'create_by' => $session_id,
                'update_by' => $session_id,
            );
            $this->db->insert('crm_promote_consignee', $add_datas);
        }

    }
}
