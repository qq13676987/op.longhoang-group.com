<?php
if(!function_exists('setLang')){
    /**
     * 设置当前加载的类名
     * @param string $class 类名
     * @return bool
     */
   function setLang($class = ''){
        $CI =& get_instance();

        if(isset($CI->lang->this_class) && $CI->lang->this_class == $class) return true;

        $lang = isset($_COOKIE['lang']) ? $_COOKIE['lang'] : 'en';
        //获取语言包b
        $language_array = array(
            'en' => 'english',
            'cn' => 'chinese',
        );
        //不存在默认英文版
        if(!isset($language_array[$lang])){
            $lang = 'cn';
        }
        $language = $language_array[$lang];
        $CI->lang->is_loaded = array();
        $CI->lang->load($lang,$language);

        //公共部分默认加载 后续添加自带请直接[]添加数组，越靠后
        $lang_content = array();
        $lang_content[] = $CI->lang->line("public");
        $this_class_lang = $CI->lang->line($class);
        if($this_class_lang)$lang_content[] = $CI->lang->line($class);

        $lang_length = sizeof($lang_content);
        $lang_php = array();
        for ($i = 0; $i < $lang_length; $i++){
            if(!empty($lang_content[$i])){
                $lang_php = array_merge($lang_php, $lang_content[$i]);
            }
        }
        $CI->lang->this_class = $class;
        $CI->lang->setLang($lang_php);
    }
}

if(!function_exists('lang')) {
    /**
     * 获取语言
     * @param $line
     * @param string $class 类名
     * @param array $variable 变量数组 用于替换line里的一些变量参数
     * @return mixed
     */
    function lang($line, $variable = array(), $class = '')
    {
        $CI =& get_instance();
        if ($class != '') {
            setLang($class);
        }
        $lang = $CI->lang->line($line);

        //如果语言包没值,这里直接取传入的line
//        $this_lang = $lang == false ? $line : $lang;
        //2024-02-22 如果语言包没值, 这里直接插入一条原内容的数据到lang里面
        if($lang == false){
            $this_lang = $line;
            //这里做一个缓存,如果查过了, 就1小时内不处理, 不然会卡, lang使用的地方很多
//            $lang_cache_key = "lang_set_{$line}";
//            $lang_cache = cache_redis_get($lang_cache_key);
//            if($lang_cache === false){
//                $china_db = $CI->load->database('ae', true);
//                $lang_row = $china_db->where("page_view = 'public' and cn = '{$this_lang}'")->get('sys_lang')->row_array();
//                if(empty($lang_row)){
//                    $china_db->insert('sys_lang', array('page_view' => 'public', 'cn' => $this_lang, 'en' => $this_lang));
//                }
//                cache_redis_save($lang_cache_key, 999, 3600);
//            }
        }else{
            $this_lang = $lang;
        }

        //将变量替换掉
        preg_match_all('/\{(.*?)}/i', $this_lang, $matches);

        if(!empty($matches[0])){
            //将变量塞入规则中进行数据的获取
            foreach ($matches[0] as $key => $match){
                //如果传入的数组里没有该值,就不处理
                $this_match = $matches[1][$key];

                if(!isset($variable[$this_match])) continue;
                $variableData = $variable[$this_match];
                $this_lang = str_replace($match, $variableData, $this_lang);//将变量替换为值
            }
        }
        return $this_lang;
    }
}

if(!function_exists('get_session')) {
    /**
     * 获取CI的session
     * @param string $session_name session 名称
     * @return mixed
     */
    function get_session($session_name = "")
    {
        $CI =& get_instance();
        return $CI->session->userdata($session_name);
    }
}

if(!function_exists('Model')) {
    /**
     * 实例化model
     * @param $model_name
     * @return mixed
     */
    function Model($model_name)
    {
        $CI =& get_instance();
        $CI->load->model($model_name);
        return $CI->$model_name;
    }
}
if(!function_exists('Controller')) {
    function Controller($controller_name){
        $CI =& get_instance();
        if(isset($CI->$controller_name)){
            return $CI->$controller_name;
        }else{
            $CI->load->controller($controller_name);
    
            if (($last_slash = strrpos($controller_name, '/')) !== FALSE)
            {
                // And the controller name behind it
                $controller_name = substr($controller_name, $last_slash + 1);
            }
            $CI->lang->is_loaded = array(); // 清空语言包加载
            return $CI->$controller_name;
        }
    }
}

if(!function_exists('is_ajax')) {
    /**
     * 判断是否是ajax请求
     * @return bool
     */
    function is_ajax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';//判断是否是ajax请求
    }
}

if(!function_exists('pricate_encrypt')) {
    /**
     * 非对称解密 私钥加密
     * @param $data
     * @return string|null
     */
    function pricate_encrypt($data)
    {
        if (!is_string($data)) {
            return null;
        }
        $abs_path = dirname(BASEPATH) . '/inc/pem/rsa_private_key.pem';
        $content = file_get_contents($abs_path);
        $privateKey = openssl_pkey_get_private($content);

        return openssl_private_encrypt($data, $encrypted, $privateKey) ? base64_encode($encrypted) : null;
    }
}

if(!function_exists('public_decrypt')) {
    /**
     * 非对称加密 公钥解密
     * @param $data
     * @return null
     */
    function public_decrypt($data)
    {
        if (!is_string($data)) {
            return null;
        }
        $abs_path = dirname(BASEPATH) . '/inc/pem/rsa_public_key.pub';
        $content = file_get_contents($abs_path);
        $publicKey = openssl_pkey_get_public($content);

        return openssl_public_decrypt(base64_decode($data), $decrypted, $publicKey) ? $decrypted : null;
    }
}

if(!function_exists('log_url_rcd')) {
    /**
     * 记录访问日志
     * @param $exec_time 记录请求执行时间 主要是后端的
     */
    function log_url_rcd($exec_time)
    {
        $CI =& get_instance();  //相当于$this->
        $class = $CI->router->fetch_class();
        $method = $CI->router->fetch_method();

        $log_url = array();
        //查询IP,如果无法获取,传0进去
        $ip = GetIP();
        $ip_long = $ip !== false ? ip2long($ip) : 0;
        $log_url['ip'] = $ip_long;
        $log_url['url'] = $_SERVER['REQUEST_URI'];
        $log_url['url2'] = $class . '/' . $method;
        $log_url['post_data'] = json_encode($_POST, JSON_UNESCAPED_UNICODE);
        if (get_session('id')) $log_url['user_id'] = get_session('id');
        $log_url['exec_time'] = $exec_time;
        $CI->load->model('sys_log_url_model');

        $CI->sys_log_url_model->save($log_url);
    }
}

if(!function_exists('GetIP')) {
    /**
     * 获取当前IP
     * @return bool|mixed
     */
    function GetIP()
    {
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $cip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } elseif (!empty($_SERVER["REMOTE_ADDR"])) {
            $cip = $_SERVER["REMOTE_ADDR"];
        } else {
            $cip = false;
        }
        return $cip;
    }
}

if(!function_exists('get_user_pan')) {
    /**
     * 获取目录,该方法支持递归获取子目录
     * @param int $user_id
     * @param array $data
     * @return array
     */
    function get_user_pan($user_id = 0, &$data = array()){
        $CI =& get_instance();
        //递归获取

        //获取初级目录
        $this_pan_id = getUserField($user_id, 'pan_id');
        $CI->db->select('id,name,pan_id');
        // $rs = Model('bsc_user_model')->get_by_id($user_id);
        //拼接当前的子账号,和自己,还有当前账号的父账号
        $data = Model('bsc_user_model')->get("pan_id = $user_id or id = $user_id or id = $this_pan_id");
        // $data[] = $rs;

        // get_user_pan($rs['pan_id'], $data);

        return $data;
    }
}

if(!function_exists('jsonEcho')) {
    /**
     * json输出代码
     * @param $data
     * @return bool
     */
    function jsonEcho($data)
    {
        echo json_encode($data);
        return true;
    }
}

if(!function_exists('jsonEcho')) {
    /**
     * json输出代码
     * @param $data
     * @return bool
     */
    function jsonEcho($data)
    {
        echo json_encode($data);
        return true;
    }
}

if(!function_exists('jsonEcho')) {
    /**
     * json输出代码
     * @param $data
     * @return bool
     */
    function resultEcho($data)
    {
        echo json_encode($data);
        return true;
    }
}

if(!function_exists('success')) {
    /**
     * json输出代码
     * @param $code
     * @param $mes
     * @param array $data
     * @return bool
     */
    function success($code, $mes, $data = array())
    {
        $return_data = array_merge(array('code' => $code, 'msg' => $mes), $data);
        return jsonEcho($return_data);
    }
}

if(!function_exists('error')) {
    /**
     * json输出代码
     * @param $code
     * @param $mes
     * @param array $data
     * @return bool
     */
    function error($code, $mes, $data = array())
    {
        $return_data = array_merge(array('code' => $code, 'msg' => $mes), $data);
        return jsonEcho($return_data);
    }
}
if(!function_exists('log_rcd')) {
    /**
     * 记录日志
     * @param $log_data
     * @param $type
     */
    function log_rcd($log_data, $type = 1)
    {

        $log_data["user_id"] = get_session('id');
        $computer_code = isset($_COOKIE['computer_code']) ? $_COOKIE['computer_code'] : '';
        $computer_code = json_decode(public_decrypt($computer_code), true);
        $log_data["system_pass_id"] = $computer_code['id'];
        $id =  Model('sys_log_model')->save($log_data);
    }
}
if(!function_exists('log_rec')){
    /**
     * 2023-04-11 汪庭彬 记录日志的函数, 改版, 懒得写数组了
     * @param string $table_name 表名
     * @param int $key 主键
     * @param string $action 方法名
     * @param array $value 值
     * @param string $master_table_name 主表名
     * @param string $master_key 主表主键
     * @param int $type
     */
    function log_rec($table_name = 'no_table', $key = 0, $action = '', $value = array(), $master_table_name = '', $master_key = '', $type = 1){
        $log_data = array();
        $log_data["table_name"] = $table_name;
        $log_data["key"] = $key;
        $log_data["action"] = $action;
        $log_data["master_table_name"] = $master_table_name;
        $log_data["master_key"] = $master_key;
        $log_data["value"] = json_encode($value);
        log_rcd($log_data, $type);
    }
}
if(!function_exists('is_admin')) {
    /**
     * @param int $type
     * @return bool
     */
    function is_admin($type = 0)
    {
        if ($type == 1) {
            if (get_session('name') == '金晶') {
                return false;
            }
        }

        if (get_session('level') == '9999') {
            return true;
        }

        return false;
    }
}
if(!function_exists('getValue')) {
    /**
     * 获取get参数, 如果不存在返回默认值
     * @param $name
     * @param string $spare
     * @return mixed|string
     */
    function getValue($name, $spare = '')
    {
        return issetGet($_GET, $name, $spare);
    }
}
if(!function_exists('postValue')) {
    /**
     * 获取post参数, 如果不存在返回默认值
     * @param $name
     * @param string $spare
     * @return mixed|string
     */
    function postValue($name, $spare = '')
    {
        return issetGet(($_POST), $name, $spare);
    }
}
if(!function_exists('issetGet')) {
    /**
     * 获取数组中的指定值, 如果不存在返回默认值
     * @param $data
     * @param $name
     * @param string $spare
     * @return mixed|string
     */
    function issetGet($data, $name, $spare = '')
    {
        return isset($data[$name]) ? $data[$name] : $spare;
    }
}

if(!function_exists('filter_unique_array')) {
    /**
     * 数组去空且去重
     * @param $array
     * @return array
     */
    function filter_unique_array($array)
    {
        if (!is_array($array)) $array = array($array);
        return array_filter(array_unique($array));
    }
}
if(!function_exists('check_param')) {
    /**
     * 将值进行处理
     * @param null $value
     * @return array|string|null
     */
    function check_param($value = null)
    {
        if(is_array($value)){
            foreach ($value as $key => $val){
                $value[$key] = check_param($val);
            }
            return $value;
        }
        return htmlspecialchars(addslashes($value));
    }
}

if(!function_exists('get_menu')) {
    /**
     * 获取目录,该方法支持递归获取子目录
     * @param $pid int
     * @param $is_for 是否遍历,不遍历就只获取到1级
     * @param $data array
     */
    function get_menu($pid = 0, $is_for = false, $click_type = 'iframe')
    {
        $CI =& get_instance();
        $data = array();
        //递归获取

        //获取初级目录
        $where = array();
        $where[] = 'display = 1';//显示
        $where[] = 'is_delete = 0';//未删除
        $where[] = 'parent_id = ' . $pid;//左下角
        $where[] = "m_type = '导航'";//未删除
        $where = join(' and ', $where);

        $CI->db->select('id,code,name as text,parent_id as pid,url as href,icon,param,is_count');
        $rs = $CI->model->get($where, 'order', 'asc');

        //2、循环父目录,然后递归获取子目录的集合
        if ($is_for) {
            foreach ($rs as $row) {
                $row['text'] = lang($row['text']);
                $row['href'] = '/' . $row['href'] . '?' . $row['param'];
                $row['icon'] = '/inc/menu/' . $row['icon'];//icon全部固定在inc menu目录里
                //这里可以在menu里加字段,但是为了省事,直接单独判断吧
                if($click_type === 'iframe') $row['iframe'] = true;
                if($click_type === 'no_window') $row['window'] = 'no';
                if ($row['pid'] == $pid) {
                    $children = get_menu($row['id'], true, $click_type);
                    if (!empty($children)) $row['children'] = $children;
                }
                $data[] = $row;
            }
        } else {
            foreach ($rs as $row) {
                $row['text'] = lang($row['text']);
                if($click_type === 'iframe') $row['iframe'] = true;
                if($click_type === 'no_window') $row['window'] = 'no';
                $row['href'] = '/' . $row['href'] . '?' . $row['param'];
                $row['icon'] = '/inc/menu/' . $row['icon'];//icon全部固定在inc menu目录里
                $data[] = $row;
            }
        }

        return $data;
    }
}

if(!function_exists('getUserName')) {
    /**
     * 获取用户的名称
     * @param int $id
     * @return mixed
     */
    function getUserName($id = 0)
    {
        return getUserField($id, 'name_en');
    }
}

if(!function_exists('getUserField')) {
    /**
     * 根据用户ID获取对应的字段数据
     * @param int $id
     * @param string $field
     * @return mixed
     */
    function getUserField($id = 0, $field = 'name')
    {
        $CI =& get_instance();
        $bsc_user_model = Model('bsc_user_model');
        $CI->db->select($field);
        $user = $bsc_user_model->get_by_id($id);
        if (empty($user)) $user[$field] = '';
        return $user[$field];
    }
}
if(!function_exists('tree')) {
    /**
     * 整理为树的方法
     * @param $array
     * @param $pid
     * @param string $child_key
     * @param string $pid_key_name
     * @return array
     */
    function tree($array, $pid, $child_key = 'child', $pid_key_name = 'pid')
    {
        $tree = array();
        foreach ($array as $key => $value) {
            if ($value[$pid_key_name] == $pid) {
                $value[$child_key] = tree($array, $value['id'], $child_key, $pid_key_name);
                if (!$value[$child_key]) {
                    unset($value[$child_key]);
                }
                $tree[] = $value;
            }
        }
        return $tree;
    }
}
if(!function_exists('get_user_title')){
    /**
     * 获取当前用户该视图的相关参数
     */
    function get_user_title($table_name, $view_name){
        $CI = & get_instance();
        $CI->db->select("sys_config_title_base.title,sys_config_title_base.sql_field,sys_config_title_base.sql_join, sys_config_title_base.table_name, sys_config_title_base.table_field,sys_config_title_base.editor,sys_config_title_user.width,sys_config_title_base.editor_config,sys_config_title_base.column_config,sys_config_title_base.sortable");
        //查询允许查询的
//        $CI->db->where("sys_config_title_base.is_query = 1");
        $CI->db->order_by('sys_config_title_user.order', 'asc');
        $rs = Model("sys_config_title_model")->get_user_title($table_name, $view_name);

        //如果没有任何配置, 先做成读取title的全部相关
        if(empty($rs)){
            //和上面不同的地方在于宽度取默认的了
            $CI->db->select("sys_config_title_base.title, sys_config_title_base.table_name,sys_config_title_base.sql_field,sys_config_title_base.sql_join,sys_config_title_base.table_field,sys_config_title_base.editor,sys_config_title_base.width,sys_config_title_base.editor_config,sys_config_title_base.column_config,sys_config_title_base.sortable");
            $CI->db->order_by('sys_config_title_view.order', 'asc');
            $rs = Model('sys_config_title_model')->get_default_title($view_name);
        }
        $data = $rs;

        return $data;
    }
}

if(!function_exists('get_user_title_sql_data')){
    /**
     * 获取当前用户该视图的sql相关参数
     * @param $table_name
     * @param $view_name
     * @return array
     */
    function get_user_title_sql_data($table_name, $view_name){
        $cache_key = "user_title_sql_data_{$table_name}_{$view_name}_" . get_session('id');

        $data = cache_redis_get($cache_key);
        if(!empty($data)) return json_decode($data, true);

        $title_data = get_user_title($table_name, $view_name);
        $base_data = get_base_query_title($table_name, $view_name);

        $data = array();
        //整理sql_field出来
        $data['data'] = array();//这个是用于 查询那里使用
        $data['editor'] = array();
        $data['select_field'] = array();//查询用的数据
        $data['sql_join'] = array();//join用的数据
        foreach ($title_data as $row){
            $this_key = "{$row['table_name']}.{$row['table_field']}";
            $data['data'][$this_key] = $row;
            // $data['sql_field'][$this_key] = "{$row['sql_field']}";

            if($row['width'] > 0) {//宽度大于0时才使用的一些
                if(!empty($row['sql_join'])) {
                    //用来当键名防止重复的
                    //这里要切割一下, 这里和ci保持一致, 0是表, 1是on,2 是连接条件\
                    $this_sql_join = $row['sql_join'];

                    $data['sql_join'][$row['sql_join']] = join_sql_change_join_ci($this_sql_join);
                }
                $data['select_field'][$this_key] = "{$row['sql_field']} as {$row['table_field']}";
            }
        }

        $data['sql_field'] = array();//sql查询等用的数据
        $data['base_select_field'] = array();//sql查询等用的数据
        //查询的使用base表的
        foreach ($base_data as $row){
            $this_key = "{$row['table_name']}.{$row['table_field']}";
            $data['sql_field'][$this_key] = "{$row['sql_field']}";
            !isset($data['editor'][$row['editor']]) && $data['editor'][$row['editor']] = array();
            $data['editor'][$row['editor']][] = $this_key;

            $data['base_select_field'][$this_key] = "{$row['sql_field']} as {$row['table_field']}";
            $data['base_data'][$this_key] = $row;
        }
        //存30秒缓存
        cache_redis_save($cache_key, json_encode($data), 30);

        return $data;
    }
}

if(!function_exists('join_sql_change_join_arr')){
    /**
     * 把字符串切割为 数组 CI里使用
     * @param string $join_sql
     * @return array
     */
    function join_sql_change_join_ci($join_sql = ''){
        if(empty(trim($join_sql))) return array();
        //这里要切割一下, 这里和ci保持一致, 0是表, 1是on,2 是连接条件\
        $join_split_sql = explode(' JOIN ', $join_sql);//根据JOIN切割, 前面的是连接类型, 后面的是剩余的
        $type = $join_split_sql[0];//连接类型 LEFT等
        $this_sql_join = $join_split_sql[1];//剩余

        $cond_split_sql = explode(' ON ', $this_sql_join);
        if(sizeof($cond_split_sql) !== 2) return array();//如果宽度不等于于2就不处理
        $cond = $cond_split_sql[1];
        $table = $cond_split_sql[0];

        return array($table, $cond, $type);
    }
}

if(!function_exists('get_base_query_title')){
    /**
     * 获取指定表格的所有相关可查询字段
     * @param $table_name
     * @param $is_query 是否获取可查询的
     * @return mixed
     */
    function get_base_query_title($table_name, $view_name, $is_query = true){
        $CI = & get_instance();
//        $CI->db->select('table_name,table_field,title,editor,editor_config,sql_field,sql_join');
//        if(!$is_query) $CI->db->where("sys_config_title_base.is_query = 1");
//        return Model("sys_config_title_model")->get_base_title($table_name);
        //改为了从view取值
        $CI->db->select('sys_config_title_base.table_name,sys_config_title_base.table_field,sys_config_title_base.edit_field,sys_config_title_base.title,sys_config_title_base.editor,sys_config_title_base.editor_config,sys_config_title_base.sql_field,sys_config_title_base.sql_join');
        if(!$is_query) $CI->db->where("sys_config_title_base.is_query = 1");
        return Model("sys_config_title_model")->get_view_title($table_name, $view_name);
    }
}

if(!function_exists('data_role')) {
    /**
     * TODO 根据当前的select 值 和 base_title的配置字段,
     * @param $data 当前查询条件的
     * @param $field_config 这个是新版的传过来的title_base数据
     * @param $userRole 用户当前角色的权限
     * @return mixed
     */
    function data_role($data = array(), $must_field = array(), $userRole = array(), $table_name = "no_table", $view_name = "no_view"){
        $userRole = filter_unique_array($userRole);//去重去空值,避免后面出现bug

        //table_field为键名, edit_field为值
        if($view_name == 'no_view'){
            $this_edit_fields = array();
        }else{
            $title_data = get_user_title_sql_data($table_name, $view_name);//获取相关的配置信息
            $this_edit_fields = array_column($title_data['base_data'], 'edit_field', 'table_field');
        }

        foreach ($data as $key => $val) {
            //如果是必须跳过的,这里 不处理
            if(in_array($key, $must_field)) continue;

            //如果edit配置为空,这里不处理
            if(empty($this_edit_fields)){
                $this_edit_field = "{$table_name}.{$key}";
                //懒得加参数了,这里直接判断shipment或者consol 时 _id那种判断
                if(in_array($table_name, array('biz_shipment', 'biz_consol'))){
                    //如果是sales_id等,直接当sales处理
                    $admin_fields = array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'sales');
                    foreach ($admin_fields as $admin_field){
                        if($key === "{$admin_field}_id" || $key === "{$admin_field}_group"){
                            $this_edit_field = "{$table_name}.{$admin_field}";
                            continue;
                        }
                    }
                }

                //如果是没视图这种的话, 都不需要处理了
                if(in_array($table_name, $userRole)) break;
            }else{
                if(isset($this_edit_fields[$key])){
                    $this_edit_field = $this_edit_fields[$key];
                }else{
                    $this_edit_field = false;
                }
            }

            //如果edit_field为 空字符串的话,也默认通过
            if($this_edit_field === '') continue;

            //判断当前是否有完整权限 . 前面的表名是否有值 如 shipment  那么shipment. 的全部跳过
            //这里如果后续改为 直接字段的话,没有.前面的时, 可能会出现全开放的问题
            if(in_array(explode('.', $this_edit_field)[0], $userRole)) continue;

            //如果没有完整权限,这里判断是否有单个字段的权限
            if(in_array($this_edit_field, $userRole)) continue;

            //其他情况统一加密
            $data[$key] = '***';
        }
        return $data;
    }
}

if(!function_exists('getConfigSearch')){
    /**
     * 获取查询条件信息
     * @param $table 表面
     * @param array $default 如果空着,默认给的数据
     * @return array
     */
    function getConfigSearch($table, $default = array('field', 'like', 'value')){
        $CI =& get_instance();
        if(!isset($CI->sys_config_model))Model('sys_config_model');
        $result = $CI->sys_config_model->get_one($table . '_search');
        if(empty($result)) $result = array('config_name' => $table . '_search', 'config_text' => '["1",[' . json_encode($default) . ']]');
        //2022-06-13 已修复 空值时 导致报错的问题
        if($result['config_text'] == '["0",""]') $result = array('config_name' => $table . '_search', 'config_text' => '["1",[' . json_encode($default) . ']]');

        return $result;
    }
}

if(!function_exists('cache_redis_get')) {
    /**
     * 获取cache信息
     * @param $cache_key
     * @return mixed
     */
    function cache_redis_get($cache_key)
    {
        $CI =& get_instance();

        $CI->load->driver('cache');
        //2023-05-15 这里的key 统一加前后缀
        $cache_key .= get_system_type();

//        return $CI->cache->redis->get($cache_key);
        return $CI->cache->get($cache_key);
    }
}
if(!function_exists('cache_redis_save')) {
    /**
     * 存储cache数据
     * @param $cache_key
     * @param $data
     * @param int $ttl
     * @param bool $raw
     * @return mixed
     */
    function cache_redis_save($cache_key, $data, $ttl = 60, $raw = false)
    {
        $CI =& get_instance();

        $CI->load->driver('cache');
        //2023-05-15 这里的key 统一加前后缀
        $cache_key .= get_system_type();

//        return $CI->cache->redis->save($cache_key, $data, $ttl, $raw);
        return $CI->cache->save($cache_key, $data, $ttl, $raw);
    }
}
if(!function_exists('cache_redis_delete')) {
    /**
     * 删除cache数据
     * @param $cache_key
     * @return mixed
     */
    function cache_redis_delete($cache_key)
    {
        $CI =& get_instance();

        $CI->load->driver('cache');
        //2023-05-15 这里的key 统一加前后缀
        $cache_key .= get_system_type();

//        return $CI->cache->redis->delete($cache_key);
        return $CI->cache->delete($cache_key);
    }
}

if(!function_exists('client_read_role')) {
    /**
     * 查看当前用户是否有client的查看权限
     * @param array $data
     * @return bool
     */
    function client_read_role($data = array())
    {
        //管理员直接通过
        if (is_admin()) return true;

        $userid = get_session('id');
        $rangeGroup = explode(',', get_session('group_range'));
        $rangeUser = get_session('user_range');

        $is_read = false;

        //全开放
        if (isset($data['read_user_group']) && in_array('all', explode(',', $data['read_user_group']))) return true;

        //创建人
        if (isset($data['created_by']) && $data['created_by'] == $userid) return true;

        //后面的全开放改为了is_all
        if (isset($data['is_all']) && $data['is_all']) return true;

        foreach ($rangeUser as $ru) {
            //是当前操作
            if (isset($data['operator_ids']) && in_array($ru, explode(',', $data['operator_ids']))) return true;

            if (isset($data['customer_service_ids']) && in_array($ru, explode(',', $data['customer_service_ids']))) return true;

            if (isset($data['sales_ids']) && in_array($ru, explode(',', $data['sales_ids']))) return true;

            if (isset($data['marketing_ids']) && in_array($ru, explode(',', $data['marketing_ids']))) return true;

            if (isset($data['oversea_cus_ids']) && in_array($ru, explode(',', $data['oversea_cus_ids']))) return true;

            if (isset($data['booking_ids']) && in_array($ru, explode(',', $data['booking_ids']))) return true;

            if (isset($data['document_ids']) && in_array($ru, explode(',', $data['document_ids']))) return true;

            if (isset($data['finance_ids']) && in_array($ru, explode(',', $data['finance_ids']))) return true;
        }

        foreach ($rangeGroup as $rg) {
            if (isset($data['read_user_group']) && in_array($rg, explode(',', $data['read_user_group']))) return true;
        }

        if (!$is_read) {
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
            echo "没有权限！";
            exit;
        }
    }
}

if(!function_exists('es_encode')) {
    /**
     * 替换一下斜线等值,前端如果赋予值时需要用上
     * @param $str
     * @param string $replace
     * @param bool $is_html
     * @return mixed|string
     */
    function es_encode($str, $replace = "'", $is_html = true)
    {
        if ($replace == "'") $str = str_replace("'", "\\'", $str);
        if ($replace == "\"") $str = str_replace('"', '\\"', $str);
        if ($is_html) {
            //检测最后一个是不是 / 如果是那么替换为双斜线
            $str_length = strlen($str);
            $str_length != 0 && $str[$str_length - 1] == "\\" && $str = substr($str, 0, $str_length - 1) . "\\\\";
        }
        return $str;
    }
}

/**
 * 判断
 */
if(!function_exists('show_tip')) {
    /**
     * 生成easyui的感叹号提示框+悬浮提示信息
     * @param $title
     * @return bool
     */
    function show_tip($title)
    {
        echo "<a href=\"javascript:void(0);\" class=\"easyui-tooltip\" title=\"$title\"><img src=\"../../../inc/image/tipicon.png?v=1\" style=\"width:15px;height:15px;\"></a>";
        //false代表通过, true相关的值都代表不通过原因
        return false;
    }
}

if(!function_exists('array_replace_str')) {
    /**
     * 将数组里的一些值进行替换
     * @param $array
     * @param string $str 需要替换的值,如果是数组,就替换多个, 键名替换为值, 如果传入字符串,默认为去除
     * @param bool $upper 转大写
     * @return mixed
     */
    function array_replace_str($array, $str = '', $upper = false)
    {
        if (is_array($str)) {
            $str_replace = $str;
        } else {
            $str_replace = array();
            $str_replace[$str] = '';
        }

        foreach ($array as $key => $val) {
            if (is_array($val)) {
                $array[$key] = array_replace_str($val, $str_replace);
            } else {
                if ($upper) $array[$key] = strtoupper($array[$key]);
                $array[$key] = ts_replace($array[$key]);
                foreach ($str_replace as $k => $v) {
                    $array[$key] = str_replace($k, $v, $array[$key]);
                }
            }
        }
        return $array;
    }
}

if(!function_exists('ts_replace')) {
    /**
     * 将一些特殊字符替换掉
     * @param $str
     * @return mixed
     */
    function ts_replace($str)
    {
        $str = str_replace('·', '`', $str);
        $str = str_replace('！', '!', $str);
        $str = str_replace('￥', '$', $str);
        $str = str_replace('【', '[', $str);
        $str = str_replace('】', ']', $str);
        $str = str_replace('；', ';', $str);
        $str = str_replace('：', ':', $str);
        $str = str_replace('“', '"', $str);
        $str = str_replace('”', '"', $str);
        $str = str_replace('’', '\'', $str);
        $str = str_replace('‘', '\'', $str);
        // $str = str_replace('、', '', $str);
        $str = str_replace('？', '?', $str);
        $str = str_replace('《', '<', $str);
        $str = str_replace('》', '>', $str);
        $str = str_replace('，', ',', $str);
        $str = str_replace('。', '.', $str);
        $str = str_replace(' ', ' ', $str);
        $str = str_replace('     ', ' ', $str);
        $str = str_replace('　', ' ', $str);
        $str = str_replace('（', '(', $str);
        $str = str_replace('）', ')', $str);
        $str = str_replace('–', '-', $str);
        $str = preg_replace('/[ ]+/', ' ', $str);
        return $str;
    }
}

if ( ! function_exists('menu_role')) {
    /**
     * 判断是否有当前目录权限
     * @param $menu_code 目录code
     * @return bool
     */
    function menu_role($menu_code){
        //本来是想用ID对比的,但是思考了下
        //根据目录code获取到当前的ID,然后与当前用户的权限进行对比,确认是否有ID更好
        $CI =& get_instance();
        $bsc_menu_relation_model = Model('bsc_menu_relation_model');
        $bsc_menu_model = Model('bsc_menu_model');

        $CI->db->select('id');
        $menu = $bsc_menu_model->get_where_one("code = '$menu_code'");
        if(empty($menu)) return false; // 没找到目录不给通过

        return $bsc_menu_relation_model->checkAuth($menu['id']);
    }
}

if (!function_exists('check_field_role')) {
    /**
     * 判断字段是否有权限
     * @param $field string 字段名
     * @param $table string 表名
     * @param $role array 权限字段的数组
     * @param $type string 类型 read的话返回值为 ***和值
     * @param $value $type为read时会用上
     * @return bool|string
     */
    function check_field_role($field, $table, $role = array(), $type = 'edit', $value = "")
    {
        $this_field = $table . '.' . $field;
        if ($type == 'edit') {
            if (in_array($table, $role)) return true;
            return in_array($this_field, $role);
        } else if ($type == 'read') {
            if (in_array($this_field, $role) || in_array($table, $role)) {
                return $value;
            } else {
                return '***';
            }
        } else {
            return false;
        }
    }
}

if (!function_exists('pass_role')) {
    /**
     * 判断是否有权限进入该页面
     * @param array $data
     * @param $admin_field
     */
    function pass_role($data = array(), $admin_field)
    {
        $userId = get_session('id');
        $userGroup = array_filter(explode(',', get_session('group_range')));  //过滤掉null 空值
        $user_range = get_session('user_range');
        if (is_admin()) return true;
        if (empty($data)) return true;

        //根据当前的角色ID等数据,判断当前用户是否有权限进入
        foreach ($admin_field as $val) {
            $id_field = $val . '_id';
            $group_field = $val . '_group';
            if(!isset($data[$id_field])) continue;

            if ($data[$id_field] == $userId || in_array($data[$group_field], $userGroup, true) || in_array($data[$id_field], $user_range)) return true;
        }
        if (get_session('admin_login') == 'admin') {
            // dump($user_range);
        }

        echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
        echo "没有权限！<br/>";
        //2021-12-27 没有权限时,将对应人员贴上去
        foreach ($admin_field as $val) {
            $id_field = $val . '_id';
            $id_name = getUserName($data[$id_field]);
            echo lang($val) . ": " . $id_name . '<br/>';
        }
        exit;
    }
}

if (!function_exists('search_like')) {
    /**
     * 手动处理下like语句
     * @param $f 字段名
     * @param $s 符号
     * @param $v 值
     * @return string
     */
    function search_like($f, $s, $v){
        $v = trim($v);
        if($s == 'like'){
            return "$f $s '%$v%'";
        }else if($s == 'not null'){
            return "$f is not null";
        }else if($s == 'not empty'){
            return "$f != ''";
        }else if($s == 'is empty'){
            return "$f = ''";
        }else if($s == 'in'){
            return "$f $s $v";
        }else{
            return "$f $s '$v'";
        }
    }
}

if(!function_exists('add_bill')) {
    /**
     * 新增一笔账单
     * @param array $data
     * @return array
     */
    function add_bill($data = array(), $is_auto = false)
    {
        $CI =& get_instance();
        //有问题的都是返回的不为空的字符串
        //没问题的返回false或者空字符串?
        $required_field = array('id_type', 'id_no', 'type', 'charge_code', 'currency', 'unit_price', 'number', 'vat', 'client_code');//这里设置必填字段,如果没有不给新增

        //新增数据的数组, 防止有人偷懒导致新增数据继承了
        $add_bill_data = array();

        //判断必填字段是否有值
        foreach ($required_field as $field) {
            if (!isset($data[$field]) || (isset($data[$field]) && $data[$field] === '')) return array('code' => 1, 'msg' => "必填字段:" . lang($field) . " 不能为空", $data);
            $add_bill_data[$field] = $data[$field];
        }
        $id_type = $data['id_type'];
        $id_no = $data['id_no'];
        $type = $data['type'];
        Model('biz_shipment_model');
        Model('biz_consol_model');
        Model('biz_charge_model');
        Model('biz_client_model');
        Model('biz_bill_currency_model');
        Model('bsc_user_role_model');
        Model('biz_bill_model');

        //获取修改权限
        $user_role = $CI->bsc_user_role_model->get_user_role(array('edit_text'));
        $userRoleEdit = explode(',', $user_role['edit_text']);//bill.cost,bill.sell

        //不存在权限弹出
        if (!$is_auto && !in_array('bill.' . $type, $userRoleEdit)) return array('code' => 1, 'msg' => lang('无权新增'));

        //根据当前id_type获取
        $consol_trans_carrier = '';
        if ($id_type == 'shipment') {
            //这个booking_ETD现在好像没啥用了 都是用的bill_etd
            $CI->db->select('booking_ETD,consol_id');
            $type_row = $CI->biz_shipment_model->get_by_id($id_no);
            if (empty($type_row)) return array('code' => 1, 'msg' => lang('SHIPMENT 不存在'));

            $CI->db->select('trans_carrier');
            $consol = $CI->biz_consol_model->get_by_id($type_row['consol_id']);

            if(!empty($consol)) $consol_trans_carrier = $consol['trans_carrier'];
            else $consol_trans_carrier = '-';
        } else if ($id_type == 'consol') {
            $CI->db->select('booking_ETD,trans_carrier');
            $type_row = $CI->biz_consol_model->get_by_id($id_no);
            if (empty($type_row)) return array('code' => 1, 'msg' => lang('CONSOL 不存在'));

            $CI->load->model('biz_shipment_model');
            $CI->db->select('id');
            $shipments = $CI->biz_shipment_model->get_shipment_by_consol($id_no);
            if (empty($shipments)) {
                // $data['split_mode'] = -1;
                    return array('code' => 1, 'msg' => '未绑定SHIPMENT,无法做账');
            }
            $consol_trans_carrier = $type_row['trans_carrier'];

            //2022-03-17 原consol不存在shipment 不允许做账,改为 允许做账,但是分摊模式默认为-1  思考了下,如果他们添加了这个账单后,又某种原因,重新启用后,又关联了shipment,那么这里就会出现问题了
            //2022-03-18 已恢复,但是绑定shipment和申请绑定shipment那里,不能申请有账单的consol
        } else {
            return array('code' => 1, 'msg' => lang('id_type 错误'));
        }
        //2022-06-20 承运人限制改到做账这里
        // if(empty($consol_trans_carrier)) return array('code' => 1, 'msg' => lang('CONSOL承运人未填写'));

        //获取汇率等
        $bill_ETD = get_bill_ETD($id_type, $id_no); //调用函数获取bill_etd
        $add_bill_data['bill_ETD'] = $bill_ETD;

        //默认费用名称字段
        //获取charge名称--start
        $charge_code = $add_bill_data['charge_code'];
        $charge = $CI->biz_charge_model->get_one('charge_code', $charge_code);
        if (empty($charge)) {
            return array('code' => 1, 'msg' => '费用名称不存在');
        }
        $charge_name_key = 'charge_name_cn';

        //查询当前客户是否有agent和海外客户的角色,如果有, 名称使用英文的
        $client = $CI->biz_client_model->get_one('client_code', $add_bill_data['client_code']);
        if (!empty($client)) {
            $client_role = explode(',', $client['role']);
            if (in_array('agent', $client_role)) $charge_name_key = 'charge_name';
            if (in_array('oversea_client', $client_role)) $charge_name_key = 'charge_name';
        } else {
            return array('code' => 1, 'msg' => '客户不存在,请确认');
        }
        if ($charge_name_key == 'charge_name' && empty($charge[$charge_name_key])) return array('code' => 1, 'msg' => '费用名称英文名未设置');

        $add_bill_data['charge'] = $charge[$charge_name_key];
        //获取charge名称--end

        //获取过期时间,这里考虑的是如果后续加入开票后等,
        if ($client['finance_payment'] == 'after work') $add_bill_data['overdue_date'] = get_overdue_date($client, $type_row['booking_ETD']);
        else $add_bill_data['overdue_date'] = '2999-12-31';
        //其他情况统一不过期

        //其他额外数据的添加
		$add_bill_data['bank_account_id'] = isset($data['bank_account_id']) ? $data['bank_account_id'] : 0;
        $add_bill_data['remark'] = trim(isset($data['remark']) ? $data['remark'] : ''); // 备注
        $add_bill_data['amount'] = round($data['unit_price'] * $data['number'], 2); //这里因为前端计算不靠谱,所以由后端计算
        $add_bill_data['is_special'] = isset($data['is_special']) ? $data['is_special'] : 0; // 是否专项
        $add_bill_data['split_mode'] = isset($data['split_mode']) ? $data['split_mode'] : 0; // 分成模式, 用于consol分摊,shipment未使用
        $add_bill_data['rate_id'] = isset($data['rate_id']) ? $data['rate_id'] : 0; // 这个字段是用于自动做账新增用的,如果是自动做账新增的,这里会记录自动做账的ID
        $add_bill_data['vat_withhold'] = isset($data['vat_withhold']) ? $data['vat_withhold'] : "0%";
        $add_bill_data['client_inner_department'] = isset($data['client_inner_department']) ? $data['client_inner_department'] : ''; // 这个字段是用于自动做账新增用的,如果是自动做账新增的,这里会记录自动做账的ID

        //服务费S跳过检测
        if(!$is_auto){

        }

        //如果单价 数量或者总价为0 则不新增
        if ($add_bill_data['unit_price'] == 0 || $add_bill_data['number'] == 0 || $add_bill_data['amount'] == 0) {
            return array('code' => 1, 'msg' => '总金额不能为0');
        }

        //币种系统里目前是只有这2种使用的, 后续如果要修改,就改为查询charge数据
         //2023-02-02  币种改到从字典表里获取了, 原本的表拿来维护两种币种的汇率使用
        //币种系统里目前是只有这2种使用的, 后续如果要修改,就改为查询charge数据
        $system_type = get_system_type();
        $currency_row = Model('bsc_dict_model')->no_role_get("catalog = 'currency_{$system_type}' and name = '{$add_bill_data['currency']}'");
        if(empty($currency_row)){
            $currency_row = Model('bsc_dict_model')->no_role_get("catalog = 'currency' and name = '{$add_bill_data['currency']}'");
            if(empty($currency_row)) return array('code' => 1, 'msg' => lang('请维护当前币种数据后再试'));
        }

        //这里开始就是查询是否有匹配账单,如果有匹配的,那么进行合并
        $where = array();
        $where[] = "id_type = '$id_type'";
        $where[] = "id_no = '$id_no'";
        $where[] = "client_code = '{$add_bill_data['client_code']}'";
        $where[] = "charge_code = '{$add_bill_data['charge_code']}'";
        $where[] = "invoice_id = 0";
        $where[] = "payment_id = 0";
        $where[] = "split_id = 0";
        $where[] = "currency = '{$add_bill_data['currency']}'";
        $where[] = "vat = '{$add_bill_data['vat']}'";
        $where[] = "unit_price = '{$add_bill_data['unit_price']}'";
        $where[] = "confirm_date = '0000-00-00'";
        $where[] = "account_no = ''";
        $where[] = "parent_id = 0";
        $where[] = "commision_flag = 0";
        $where[] = "is_special = '{$add_bill_data['is_special']}'";
        $where[] = "remark = '{$add_bill_data['remark']}'";
        $where[] = "type = '{$type}'";
        $where = join(' and ', $where);
        $bill = $CI->biz_bill_model->get_where_one($where);

        if (!empty($bill)) {
            //获取到数据后匹配
            //判断单价是否相同,不相同返回,
            $bill['unit_price'] = (float)$bill['unit_price'];
            $bill['number'] = (float)$bill['number'];
            $bill['amount'] = (float)$bill['amount'];

            $update_data = array();

            //查出符合当月
            $this_currency = $add_bill_data['currency'];

            if (isset($add_bill_data['rate_id'])) $update_data['rate_id'] = $add_bill_data['rate_id']; // 这里保持原样,如果自动做账ID存在,旧的会被改掉

            if (isset($add_bill_data['split_mode'])) $update_data['split_mode'] = $add_bill_data['split_mode']; // 待讨论 这里如果合并的话,会出现冲突

            //将金额和数量进行合并
            $update_data['number'] = $bill['number'] + $add_bill_data['number'];
            $update_data['amount'] = $bill['amount'] + $add_bill_data['amount']; // 总金额这里可能有点疑问,是直接+ 还是重新计算, 直接+的话,原本如果计算出现问题的话,这里是无法再次修复的 目前先使用直接计算

            //汇率是否等于本地汇率
            //合并这里因为汇率不会变,所以不需要修改
            // if ($this_currency == LOCAL_CURRENCY) {
            //     $amount = $update_data['amount'];
            // } else {
            //     //这里是将所有数值转为整数后进行运算 避免精度出现问题
            //     $local_amount = strval($update_data['amount'] * 100) * strval($bill['exrate'] * 1000000);
            //     $amount = $local_amount / 100000000;
            // }
            $amount = amount_currency_change($update_data['amount'], $bill['bill_ETD'], $bill['currency'], $bill['type']);
            $update_data['local_amount'] = $amount;

            $CI->biz_bill_model->update($bill['id'], $update_data); // 执行合并操作
            bill_child($bill['id'], $update_data['split_mode']); // 执行分配代码,如果是shipment不会分配的

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_bill";
            $log_data["key"] = $bill['id'];
            $log_data["action"] = "update";
            $log_data["master_table_name"] = "biz_" . $id_type;
            $log_data["master_key"] = $id_no;
            $log_data["value"] = json_encode($update_data);
            log_rcd($log_data);

            return array('code' => 0, 'msg' => '合并成功', 'data' => array('id' => $bill['id']));
        } else {
            //如果没有找到可以合并的账单,那么这里是执行正常的新增操作
            $booking_ETD_end = date('Y-m-t', strtotime($bill_ETD));
            $add_bill_data['exrate'] = get_ex_rate($booking_ETD_end, $add_bill_data['currency'], $add_bill_data['type']);
            //如果汇率为0, 不让新增
            $CURRENT_CURRENCY = get_system_config("CURRENT_CURRENCY");
            if($add_bill_data['exrate'] == 0) return array('code' => 1, 'msg' => lang('当前汇率为0, 请维护{currency_form}转{currency_to}汇率后再试', array('currency_form' => $add_bill_data['currency'], 'currency_to' => $CURRENT_CURRENCY)));
            $add_bill_data['local_amount'] = amount_currency_change($add_bill_data['amount'], $booking_ETD_end, $add_bill_data['currency'], $add_bill_data['type']);
            $id = $CI->biz_bill_model->save($add_bill_data);
            if ($id == 0) return array('code' => 1, 'msg' => '新增失败');

            bill_child($id, $add_bill_data['split_mode']);// 执行分配代码,如果是shipment不会分配的

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_bill";
            $log_data["key"] = $id;
            $log_data["action"] = "insert";
            $log_data["master_table_name"] = "biz_" . $id_type;
            $log_data["master_key"] = $id_no;
            $log_data["value"] = json_encode($add_bill_data);
            log_rcd($log_data);

            return array('code' => 0, 'msg' => '新增成功', 'data' => array('id' => $id));
        }
    }
}

if(!function_exists('get_bill_ETD')) {
    /**
     * 获取bill_ETD
     * @param string $id_type
     * @param int $id_no
     * @return bool|mixed|string
     */
    function get_bill_ETD($id_type = 'shipment', $id_no = 0)
    {
        $CI =& get_instance();  //相当于$this->
        if ($id_type == 'shipment') {
            $biz_shipment_model = Model('biz_shipment_model');
            $CI->db->select('id,consol_id,booking_ETD');
            $shipment = $biz_shipment_model->get_by_id($id_no);
            $biz_consol_model = Model('biz_consol_model');
            $CI->db->select('id,trans_ATD,ex_rate_date');
            $consol = $biz_consol_model->get_by_id($shipment['consol_id']);
            if(!empty($consol) && $consol['ex_rate_date'] !== '0000-00-00') return $consol['ex_rate_date'];
            
            if (empty($consol)) $consol['trans_ATD'] = '0000-00-00 00:00:00';
            //ATD存在时,取值ATD
            if ($consol['trans_ATD'] > '1999-01-01 00:00:00') {
                return $consol['trans_ATD'];
            } else {
                return $shipment['booking_ETD'];
            }
        } else if ($id_type == 'consol' || $id_type == 'kml') {
            $biz_consol_model = Model('biz_consol_model');
            $CI->db->select('id,booking_ETD,trans_ATD,ex_rate_date');
            $consol = $biz_consol_model->get_by_id($id_no);
            if(!empty($consol) && $consol['ex_rate_date'] !== '0000-00-00') return $consol['ex_rate_date'];
            //ATD存在时,取值ATD
            if ($consol['trans_ATD'] > '1999-01-01 00:00:00') {
                return $consol['trans_ATD'];
            } else {
                return $consol['booking_ETD'];
            }
        } else {
            return false;
        }
    }
}

if(!function_exists('bill_child')) {
    /**
     * 同步bill子账单
     * @param int $bill_id
     * @param string $split_mode
     * @param array $diy_datas
     * @return int
     */
    function bill_child($bill_id = 0, $split_mode = '', $diy_datas = array())
    {
        $CI =& get_instance();
        //consol的bill新增时,同步
        Model('biz_bill_model');
       // $CI->db->force_master();
        $this_bill = $CI->biz_bill_model->get_one('id', $bill_id);
        if (empty($this_bill) || $this_bill['id_type'] !== 'consol') return 1;
        if ($split_mode === '') $split_mode = $this_bill['split_mode'];
        if ($this_bill['invoice_id'] !== '0' || $this_bill['confirm_date'] !== '0000-00-00') return 2;
        if ($this_bill['split_id'] !== '0') return 4;//已拆分
        if ($this_bill['payment_id'] !== '0') return 5;//已销账

        $CI->db->select('id, id_no');
       // $CI->db->force_master();
        $child_bills_now = $CI->biz_bill_model->get("parent_id = {$this_bill['id']} and id_type = 'shipment'", 'id', 'desc', $this_bill['is_special'], false);
        $child_bills = array_column($child_bills_now, null, 'id_no');

        //根据子bill的分成方式进行切割
        $split_mode = (int)$split_mode;
        if ($split_mode === 0) {//按票
            $new_bills = get_shipment_consol_percent($this_bill['id_no'], 0, $this_bill);
        } else if ($split_mode === 1) {//按体积
            $new_bills = get_shipment_consol_percent($this_bill['id_no'], 1, $this_bill);
        } else if ($split_mode === 2) {//按重量
            $new_bills = get_shipment_consol_percent($this_bill['id_no'], 2, $this_bill);
        } else if ($split_mode === 3) {//自定义
            $new_bills = get_shipment_consol_percent($this_bill['id_no'], 3, $this_bill, $diy_datas);
        } else if($split_mode === 4){//自定义
            $new_bills = get_shipment_consol_percent($this_bill['id_no'], 4, $this_bill);
        } else {
            $new_bills = array();
        }
        if (empty($new_bills)) return 3;

        //根据比例将值填入
        foreach ($new_bills as $key => $new_bill){
            $add_data = $this_bill;
            $add_data['parent_id'] = $add_data['id'];
            if (isset($add_data['id'])) unset($add_data['id']);
            $add_data['id_type'] = 'shipment';
            $add_data['id_no'] = $new_bill['id'];
            $add_data['number'] = $new_bill['number'];
            $add_data['amount'] = $new_bill['amount'];
            // $add_data['amount_no_tax'] = round($add_data['amount'] / (1 + (int)$add_data['vat'] / 100), 2);
            $add_data['local_amount'] = $new_bill['local_amount'];
            $add_data['split_mode'] = $split_mode;

            //2024-03-08 如果金额为0的 这里直接忽略掉
            if($add_data['amount'] == 0){
                unset($new_bills[$key]);
                continue;
            }
            //存在3种,1、包含在新增里
            if (isset($child_bills[$add_data['id_no']])) {
                $CI->biz_bill_model->update($child_bills[$add_data['id_no']]['id'], $add_data);
                $id = $child_bills[$add_data['id_no']]['id'];
                $action = 'child_update';
            } else {
                $id = $CI->biz_bill_model->save($add_data);
                $action = 'child_insert';
            }
        }
        //获取全部
        //与$new_bills比较差集
        $all_ids = array_column($child_bills, 'id_no');
        $need_ids = array_column($new_bills, 'id');
        $delete_ids = array_diff($all_ids, $need_ids);

        foreach ($delete_ids as $delete_id) {
            $id = $child_bills[$delete_id]['id'];
            $add_data = $child_bills[$delete_id];
            $CI->biz_bill_model->mdelete($id);
        }

        //子bill里,每个shipment_id只允许存在1条,因网络等异常原因导致重复新增的shipment_id需要删除. 将重复新增的去重删除
       // $CI->db->force_master();
        $child_bills_now = $CI->biz_bill_model->get("parent_id = {$this_bill['id']} and id_type = 'shipment'", 'id', 'desc', $this_bill['is_special'], false);
        $child_bills = array_column($child_bills_now, null, 'id_no');
        $all_now_ids = array_diff(array_column($child_bills_now, 'id'), array_column($child_bills, 'id'));//删除多余的
        foreach ($all_now_ids as $all_now_id) {
            $CI->biz_bill_model->mdelete($all_now_id);
        }

        return 0;
    }
}

if(!function_exists('get_overdue_date')) {
    /**
     * 刷新授信值
     * @param $client
     * @param $date
     * @return false|string
     */
    function get_overdue_date($client, $date)
    {
        //根据授信相关参数,填入过期日期
        $overdue_date = '2999-12-31';
        if (!empty($client)) {
            //after work  开航
            $date = strtotime($date);
            if ($client['finance_od_basis'] == 'monthly') {
                //按月
                //1、获取年月日
                $year = date('Y', $date);

                //2、月加上设置的月份
                //2.1 情况1 当前月加上设置的月份后,超过12, 将超出的折算为年加到年上
                if (empty($client['finance_payment_month'])) $client['finance_payment_month'] = 1;
                if (empty($client['finance_payment_day_th'])) $client['finance_payment_day_th'] = 10;
                $month = date('m', $date) + (int)$client['finance_payment_month'];
                if ($month > 12) {
                    $month = $month - 12;
                    $year++;
                }

                //2.2 情况2 没超过, 直接取值当前月

                //3、日超过当月最大日则直接转换为最大
                $month_max_day = date('t', strtotime($year . '-' . $month . '-' . '01'));
                $day = $client['finance_payment_day_th'];
                if ($day > $month_max_day) {
                    $day = $month_max_day;
                }
                $overdue_date = $year . '-' . $month . '-' . $day;
            }
            if ($client['finance_od_basis'] == 'daily') {
                $overdue_date = date('Y-m-d', $date + $client['finance_payment_days'] * 24 * 3600);
            }
        }
        return $overdue_date;
    }
}

if(!function_exists('get_ex_rate')) {
    /**
     * 获取汇率
     * @param string $date 获取汇率的日期
     * @param string $currency 币种
     * @param string $type 类型
     * @param string $local_currency 转化的币种
     * @return float|int
     */
    function get_ex_rate($date, $currency = "", $type = 'sell', $local_currency = "")
    {
        if($currency == "") $currency = get_system_config("CURRENT_CURRENCY");
        if($local_currency == "") $local_currency = get_system_config("CURRENT_CURRENCY");
        $type_arr = array('sell', 'cost', 'invoice');

        //其他的直接
        $cache_key = "ex_rate_{$type}_{$date}_{$currency}_{$local_currency}";
        if(!in_array($type, $type_arr)) return 0;
        if($currency == $local_currency) return 1;
        //2023-02-02 由于表结构的更新 改为直接获取了
        //获取缓存
        $row = cache_redis_get($cache_key);
        if(empty($row)){
            $row = Model('biz_bill_currency_model')->get_where_one("((currency_from = '{$currency}' and currency_to = '{$local_currency}') or (currency_to = '{$currency}' and currency_from = '{$local_currency}')) and start_time <= '{$date}'");

            //第一次查询查一遍, 直接取最新的
            if(empty($row)){
                $row = Model('biz_bill_currency_model')->get_where_one("((currency_from = '{$currency}' and currency_to = '{$local_currency}') or (currency_to = '{$currency}' and currency_from = '{$local_currency}'))");
            }

            //如果没查到,汇率为0
            if(empty($row)) return 0;
            cache_redis_save($cache_key, json_encode($row));
        }else{
            $row = json_decode($row, true);
        }


        //2023-04-10 currency_from 是$currency时, 直接返回, to 是 $currency 时, 得用1 / 汇率
        $this_rate = $row["{$type}_value"];
        if($row['currency_to'] == $currency){
            $this_rate = round(1 / $this_rate, 6);
        }
        return $this_rate;
    }
}


if(!function_exists('amount_currency_change')) {
    /**
     * 金额币种转换
     */
    function amount_currency_change($amount, $date, $currency, $type = 'sell', $local_currency = ""){
        if($local_currency == "") $local_currency = get_system_config("CURRENT_CURRENCY");

        $type_arr = array('sell', 'cost', 'invoice');
        //其他的直接
        $cache_key = "ex_rate_{$date}_{$currency}_{$local_currency}";
        if(!in_array($type, $type_arr)) return 0;
        if($currency == $local_currency){
            return $amount;
        }else{
            //2023-02-02 由于表结构的更新 改为直接获取了
            //获取缓存
            $row = cache_redis_get($cache_key);
            if(empty($row)){
                $row = Model('biz_bill_currency_model')->get_where_one("((currency_from = '{$currency}' and currency_to = '{$local_currency}') or (currency_to = '{$currency}' and currency_from = '{$local_currency}')) and start_time <= '{$date}'");

                //第一次查询查一遍, 直接取最新的
                if(empty($row)){
                    $row = Model('biz_bill_currency_model')->get_where_one("((currency_from = '{$currency}' and currency_to = '{$local_currency}') or (currency_to = '{$currency}' and currency_from = '{$local_currency}'))");
                }

                //如果没查到,汇率为0
                if(empty($row)) return 0;
                cache_redis_save($cache_key, json_encode($row));
            }else{
                $row = json_decode($row, true);
            }


            //2023-04-10 currency_from 是$currency时, 直接返回, to 是 $currency 时, 得用1 / 汇率
            $this_rate = $row["{$type}_value"];
            if($row['currency_to'] == $currency){
                return round($amount / $this_rate, 2);
            }else{
                return round($amount * $this_rate, 2);
            }
        }
    }
}

if(!function_exists('get_system_config')){
    /**
     * 获取系统的配置
     */
    function get_system_config($config_name){
        $cache_key = "system_config_{$config_name}";
        $config = cache_redis_get($cache_key);
        if(!empty($config)) return json_decode($config, true)['config_text'];
        
        $config = Model('sys_config_model')->get_one_zero($config_name);
        //这个可以考虑做缓存,先这样吧
        cache_redis_save($cache_key, json_encode($config));
        return $config['config_text'];
    }
}

if(!function_exists('get_shipment_consol_percent')) {
    /**
     * 获取consol对应shipment的分摊值
     * @param int $consol_id
     * @param string $split_mode
     * @param $bill
     * @return array
     */
    function get_shipment_consol_percent($consol_id = 0, $split_mode = '', $bill = array(), $diy_datas = array())
    {
        //获取分成方式
        $CI =& get_instance();
        if ($split_mode === '') {
            $biz_consol_model = Model('biz_consol_model');
            //获取consol
            $consol = $biz_consol_model->get_by_id($consol_id);
            $split_mode = $consol['split_mode'];
        }
        //获取该票consol的所有shipment
        $biz_shiment_model = Model('biz_shipment_model');
        $update_datas = array();

        //consol拥有哪些shipment
        $CI->db->select('id,box_info');
        $shipments = $biz_shiment_model->get_shipment_by_consol($consol_id);
        if ($split_mode == 0) {
            //按票数： 平均摊
            //获取
            $all_percent = 1;
            $all_amount = $bill['amount'];
            $all_local_amount = $bill['local_amount'];
            $all_number = $bill['number'];
            foreach ($shipments as $key => $shipment) {
                $update_data = array();
                $update_data['id'] = $shipment['id'];
                if ($key == sizeof($shipments) - 1) {
                    $this_consol_percent = round($all_percent, 6);
                    $update_data['consol_percent'] = $this_consol_percent;
                    $update_data['amount'] = $all_amount;
                    $update_data['local_amount'] = $all_local_amount;
                    $update_data['number'] = $all_number;
                } else {
                    $this_consol_percent = round(1 / sizeof($shipments), 6);
                    $update_data['amount'] = round($bill['amount'] / sizeof($shipments), 2);
                    $update_data['local_amount'] = round($bill['local_amount'] / sizeof($shipments), 2);
                    $update_data['number'] = round($bill['number'] / sizeof($shipments), 4);
                }
                $update_data['consol_percent'] = round($this_consol_percent, 2);

                $all_percent = round($all_percent - $this_consol_percent, 2);
                $all_amount = round($all_amount - $update_data['amount'], 2);
                $all_local_amount = round($all_local_amount - $update_data['local_amount'], 2);
                $all_number = round($all_number - $update_data['number'], 2);
                $update_datas[] = $update_data;
            }
        } else if ($split_mode == 3) {
            //自定义,根据传入的金额进行分配
            $all_percent = 1;
            $all_amount = $bill['amount'];
            $all_local_amount = $bill['local_amount'];
            $all_number = $bill['number'];
            foreach ($shipments as $key => $shipment) {
                $update_data = array();
                $update_data['id'] = $shipment['id'];
                if ($key == sizeof($shipments) - 1) {
                    $this_consol_percent = round($all_percent, 6);
                    $update_data['consol_percent'] = $this_consol_percent;
                    $update_data['amount'] = $all_amount;
                    $update_data['local_amount'] = $all_local_amount;
                    $update_data['number'] = $all_number;
                } else {
                    if ($diy_datas[$shipment['id']]['amount'] != 0) {
                        $this_consol_percent = round(1 * $diy_datas[$shipment['id']]['amount'] / $all_amount, 6);
                        $update_data['amount'] = round($diy_datas[$shipment['id']]['amount'], 2);
                        $update_data['local_amount'] = round($bill['local_amount'] * $diy_datas[$shipment['id']]['amount'] / $all_amount, 2);
                        $update_data['number'] = round($bill['number'] * $diy_datas[$shipment['id']]['amount'] / $all_amount, 4);
                    } else {
                        $this_consol_percent = 0;
                        $update_data['amount'] = 0;
                        $update_data['local_amount'] = 0;
                        $update_data['number'] = 0;
                    }
                }
                $update_data['consol_percent'] = round($this_consol_percent, 2);

                $all_percent = round($all_percent - $this_consol_percent, 2);
                $all_amount = round($all_amount - $update_data['amount'], 2);
                $all_local_amount = round($all_local_amount - $update_data['local_amount'], 2);
                $all_number = round($all_number - $update_data['number'], 2);
                $update_datas[] = $update_data;
            }
        } else if($split_mode == 4){
            //per container 按输入的箱子分配
            //按票数： 平均摊
            //获取
            $all_percent = 1;
            $all_amount = $bill['amount'];
            $all_local_amount = $bill['local_amount'];
            $all_number = $bill['number'];
            //将箱数据汇总下, 后面根据具体的箱型进行分摊
            $all_box_info = array();
            foreach ($shipments as $shipment){
                $all_box_info = merge_box_info($all_box_info, json_decode($shipment['box_info'], true),'+');
            }
            $all_box_info = array_column($all_box_info, 'num', 'size');

            foreach ($shipments as $key => $shipment){
                $update_data = array();
                $update_data['id'] = $shipment['id'];

                $box_info = array_column(json_decode($shipment['box_info'], true), 'num', 'size');
                $f_value = isset($box_info[$bill['box_size']]) ? $box_info[$bill['box_size']] : 0;
                //按输入的账单箱型进行分配
                if(isset($all_box_info[$bill['box_size']])){
                    $all_size = $all_box_info[$bill['box_size']];

                    $this_consol_percent = round($f_value / $all_size, 6);
                    $update_data['amount'] = round($bill['amount']  * ($f_value / $all_size), 2);
                    $update_data['local_amount'] = round($bill['local_amount']  * ($f_value / $all_size), 2);
                    $update_data['number'] = round($bill['number']  * ($f_value / $all_size), 4);
                }else{
                    $this_consol_percent = 0;
                    $update_data['amount'] = 0;
                    $update_data['local_amount'] = 0;
                    $update_data['number'] = 0;
                }

                //2024-03-08 当是最后一个时, 如果值还有剩余, 且当前分摊金额为0, 那么加到前面除0外任意一个值上
                if(($key == sizeof($shipments) - 1) && $update_data['amount'] == 0){
                    foreach ($update_datas as $update_data_key => $this_update_data){
                        if($this_update_data['amount'] != 0){
                            $update_datas[$update_data_key]['consol_percent'] += $this_consol_percent;
                            $update_datas[$update_data_key]['amount'] += $all_amount;
                            $update_datas[$update_data_key]['local_amount'] += $all_local_amount;
                            $update_datas[$update_data_key]['number'] += $all_number;
                            break;
                        }
                    }

                }
                $update_data['consol_percent'] = round($this_consol_percent, 2);

                $all_percent = round($all_percent - $this_consol_percent, 2);
                $all_amount = round($all_amount - $update_data['amount'], 2);
                $all_local_amount = round($all_local_amount - $update_data['local_amount'], 2);
                $all_number = round($all_number - $update_data['number'], 2);
                $update_datas[] = $update_data;
            }
        } else {

            //获取container数据
            $biz_shipment_container_model = Model('biz_shipment_container_model');
            $CI->db->select('shipment_id,sum(packs) as packs, sum(volume) as volume, sum(weight) as weight');
            $shipment_containers = array_column($biz_shipment_container_model->get_containers_by_consol_id_group_shipment($consol_id), null, 'shipment_id');

            //对不存在的进行填充
            foreach ($shipments as $shipment) {
                if (!isset($shipment_containers[$shipment['id']])) $shipment_containers[] = array('shipment_id' => $shipment['id'], 'packs' => 0, 'volume' => 0, 'weight' => 0);
            }

            $all_percent = 1;
            $all_amount = $bill['amount'];
            $all_number = $bill['number'];
            $all_local_amount = $bill['local_amount'];
            $all_size = 0;
            $field = '';
            if ($split_mode == 1) {
                //立方数： container体积
                $field = 'volume';

                //获取总立方
                $all_size = array_sum(array_column($shipment_containers, $field));
            } else if ($split_mode == 2) {
                //吨数： container重量
                $field = 'weight';

                //获取总重量
                $all_size = array_sum(array_column($shipment_containers, $field));
            }
            //对不存在的shipment箱信息进行0值填充
            $k = 0;

            foreach ($shipment_containers as $shipment_container) {
                $update_data = array();
                $update_data['id'] = $shipment_container['shipment_id'];

                //如果不存在该type,返回失败
                if ($field === '') {
                    return $update_datas;
                }
                $f_value = $shipment_container[$field];
                if ($k == (sizeof($shipment_containers) - 1)) {
                    //最后一个为剩下的
                    // $this_consol_percent = round($all_percent, 6);
                    // $update_data['amount'] = round($bill['amount']  * $all_percent, 2);
                    // $update_data['local_amount'] = round($bill['local_amount']  * $all_percent, 2);
                    // $update_data['number'] = round($bill['number']  * $all_percent, 4);

                    //由于上面会导致值出现偏差,所以这里修复为了取最后一份
                    //最后一个为剩下的
                    $this_consol_percent = round($all_percent, 6);
                    $update_data['amount'] = round($all_amount, 2);
                    $update_data['local_amount'] = round($all_local_amount, 2);
                    $update_data['number'] = round($all_number, 4);
                } else {
                    //指定重量或立方 除以 总的
                    if ($f_value != 0) {
                        $this_consol_percent = round($f_value / $all_size, 6);
                        $update_data['amount'] = round($bill['amount'] * ($f_value / $all_size), 2);
                        $update_data['local_amount'] = round($bill['local_amount'] * ($f_value / $all_size), 2);
                        $update_data['number'] = round($bill['number'] * ($f_value / $all_size), 4);
                    } else {
                        $this_consol_percent = 0;
                        $update_data['amount'] = 0;
                        $update_data['local_amount'] = 0;
                        $update_data['number'] = 0;
                    }
                }
                $update_data['consol_percent'] = round($this_consol_percent, 2);

                $all_percent = round($all_percent - $this_consol_percent, 2);
                $all_amount = round($all_amount - $update_data['amount'], 2);
                $all_local_amount = round($all_local_amount - $update_data['local_amount'], 2);
                $all_number = round($all_number - $update_data['number'], 2);

                $update_datas[] = $update_data;
                $k++;
            }
        }
        return $update_datas;
    }
}

if(!function_exists('unsetToken')) {
    function unsetToken($name = 'token')
    {
        @session_start();
        unset($_SESSION[$name]);
    }
}

if(!function_exists('getToken')) {
    function getToken($name = 'token')
    {
        @session_start();
        $return = null;
        if (isset($_SESSION[$name])) $return = $_SESSION[$name];
        return $return;
    }
}

if(!function_exists('setToken')) {
    function setToken($name = 'token')
    {
        @session_start();
        if (isset($_SESSION[$name])) return $_SESSION[$name];
        $_SESSION[$name] = md5(microtime(true) * 10000);
        return $_SESSION[$name];
    }
}

if(!function_exists('checkToken')) {
    function checkToken($name = 'token')
    {
        @session_start();
        if (getToken($name) != $_POST['__token__']) {
            return false;
        } else {
            unsetToken($name);
            return true;
        }
    }
}
if(!function_exists('check_banjiao')) {
    function check_banjiao($str = '')
    {
        if (empty($str)) $str = '';
        preg_match_all('/[^\w\r\n \'=[\]~!@#$%^&*()_\-+<>?:;"{},.\/]+/', $str, $mach);

        return $mach;
    }
}
if(!function_exists('check_banjiao_cn')) {
    function check_banjiao_cn($str = '')
    {
        if (empty($str)) $str = '';
        preg_match_all('/[^A-Za-z0-9 =\x{4e00}-\x{9fa5}\r\n~!@#$%^&*()_\-+<>?:"{},.\/;\'[\]]+/u', $str, $mach);

        return $mach;
    }
}

if(!function_exists('shipment_job_no_rule')) {
    /**
     * 生成job_no
     * @param $trans_origin
     * @param $trans_mode
     * @return string
     */
    function shipment_job_no_rule($trans_origin, $trans_mode)
    {
        $CI =& get_instance();
        $CI->load->model('bsc_user_model');
        $CI->load->model('biz_shipment_model');

        $letter = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
        $time = time();
        //公司简写前缀 查询当前登录用户的所属公司--start
        $user = $CI->bsc_user_model->get_one('id', $CI->session->userdata('id'));
        $company_no = Model('sys_config_model')->get_one_zero("SHIPMENT_JOB_NO_PREFIX")['config_text'];
        //公司编号改为
        //公司简写前缀--end

        //起运港 起运港以5字代码后3位为编号--start
        $trans_origin_start = strlen($trans_origin) - 3 < 0 ? 0 : strlen($trans_origin) - 3;
        $trans_origin_no = substr($trans_origin, $trans_origin_start, 3);
        //起运港--end

        //trans_mode规则, 取首字母--start
        $trans_mode_no = strtoupper(substr($trans_mode, 0, 1));
        //trans_mode规则--end

        //年规则  2020为A,2021为B,以此类推--start
        $setting_year = 2020;
        $year = date('Y', $time);
        $year_num = ($year - $setting_year) % sizeof($letter);
        $year_no = $letter[$year_num];
        //年规则--end

        //月规则  1月为A,2月为B,以此类推--start
        $month = date('m', $time);
        $month_no = $letter[$month - 1];
        //月规则  1月为A,2月为B,以此类推--end

        //最后4位数字,以同前缀最后一条+1
        $prefix = $company_no . $trans_origin_no . $trans_mode_no . $year_no . $month_no;
        $CI->db->order_by('job_no', 'desc');
        //$CI->db->force_master();
        //这里筛选后四位为数字的原因是 系统里有子单, -1 -A这种结尾的
        $last_shipment = $CI->biz_shipment_model->get_where_one_all("job_no like '$prefix%' and SUBSTR(job_no, -4, 4) REGEXP '^[0-9]+$'");
        if (empty($last_shipment)) {
            $last_shipment['job_no'] = 0;
        }
        $num = str_pad((int)substr($last_shipment['job_no'], -4, 4) + 1, 4, '0', STR_PAD_LEFT);

        $job_no = $prefix . $num;
        return $job_no;
    }
}
if(!function_exists('free_svr_handle')) {
    function free_svr_handle(&$free_svr)
    {
        foreach ($free_svr as $key => $row) {
            if (sizeof(array_filter($row)) < sizeof($row)) {
                unset($free_svr[$key]);
            }
        }
        sort($free_svr);
    }
}
if(!function_exists('box_info_handle')) {
    function box_info_handle(&$box_info)
    {
        $array = array();
        if (empty($box_info)) $box_info = array();
        //获取到所有箱信息后，将相同的箱型相加
        foreach ($box_info as $key => $row) {
            foreach ($row as $rkey => $rs) {
                $array[$rkey][$key] = $rs;
            }
        }

        $new_array = array();
        //获取到所有箱信息后，将相同的箱型相加
        foreach ($array as $key => $row) {
            if ($row['size'] != '' || $row['num'] != '') {
                if (!isset($new_array[$row['size']])) {
                    $new_array[$row['size']] = 0;
                }
                $new_array[$row['size']] += $row['num'];
            }
        }

        $box_info = array();
        foreach ($new_array as $key => $val) {
            $box_info[] = array(
                'size' => $key,
                'num' => $val,
            );
        }
    }
}


if(!function_exists('dump')) {
    /**
     * 浏览器友好的变量输出
     * @param mixed $var 变量
     * @param boolean $echo 是否输出 默认为True 如果为false 则返回输出字符串
     * @param string $label 标签 默认为空
     * @param boolean $strict 是否严谨 默认为true
     * @return void|string
     */
    function dump($var, $echo = true, $label = null, $strict = true)
    {
        $label = ($label === null) ? '' : rtrim($label) . ' ';
        if (!$strict) {
            if (ini_get('html_errors')) {
                $output = print_r($var, true);
                $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
            } else {
                $output = $label . print_r($var, true);
            }
        } else {
            ob_start();
            var_dump($var);
            $output = ob_get_clean();
            if (!extension_loaded('xdebug')) {
                $output = preg_replace('/\]\=\>\n(\s+)/m', '] => ', $output);
                $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
            }
        }
        if ($echo) {
            echo($output);
            return null;
        } else
            return $output;
    }
}

if(!function_exists('lastquery')) {
    function lastquery()
    {
        $CI =& get_instance();
        return $CI->db->last_query();
    }
}

if(!function_exists('update_bill_ETD')) {
    /**
     * 批量更新账单的ETD
     * @param string $id_type
     * @param int $id_no
     */
    function update_bill_ETD($id_type = 'shipment', $id_no = 0)
    {
        $CI =& get_instance();  //相当于$this->
        $bill_ETD = get_bill_ETD($id_type, $id_no);
        $m_model = Model('m_model');

        if ($id_type == 'shipment') {//shipment
            $sql = "UPDATE biz_bill SET bill_ETD = '$bill_ETD' where id_type = '$id_type' and id_no = '$id_no'";
            $m_model->sqlquery($sql);
            // record the log
            if ($CI->db->affected_rows() > 0) {
                $log_data = array();
                $log_data["table_name"] = "biz_bill_shipment";
                $log_data["key"] = $id_no;
                $log_data["action"] = "update";
                $log_data["value"] = json_encode(array('bill_ETD' => $bill_ETD));
                log_rcd($log_data);
            }

        } else if ($id_type == 'consol') {//consol
            $sql = "UPDATE biz_bill SET bill_ETD = '$bill_ETD' where id_type = '$id_type' and id_no = '$id_no'";
            $m_model->sqlquery($sql);
            // record the log
            if ($CI->db->affected_rows() > 0) {
                $log_data = array();
                $log_data["table_name"] = "biz_bill_consol";
                $log_data["key"] = $id_no;
                $log_data["action"] = "update";
                $log_data["value"] = json_encode(array('bill_ETD' => $bill_ETD));
                log_rcd($log_data);
            }

            //更新和CONSOL相关联的shipment的bill
            $sql = "select id from biz_shipment where consol_id = $id_no";
            $rs = $m_model->query_array($sql);
            foreach ($rs as $row) {
                update_bill_ETD('shipment', $row["id"]);
            }
        } else {
            return false;
        }

    }
}

if(!function_exists('lock_log')) {
    /**
     * 添加锁日志
     * @param $id_type
     * @param $id_no
     * @param $op_value
     */
    function lock_log($id_type, $id_no, $op_value)
    {
        $CI =& get_instance();

        Model('sys_lock_log_model');
        $lock_log = array();
        $lock_log['id_type'] = $id_type;
        $lock_log['id_no'] = $id_no;
        $lock_log['user_id'] = get_session('id');
        $lock_log['op_time'] = date('Y-m-d H:i:s');
        $lock_log['op_value'] = $op_value;
        $CI->sys_lock_log_model->save($lock_log);
    }
}

if(!function_exists('lock_role')) {
    /**
     * 判断是否有当前锁权限
     * @param string $id_type
     * @param int $id_no
     * @param int $lock_lv
     * @param int $lock
     */
    function lock_role($id_type = '', $id_no = 0, $lock_lv = 0, $lock = 0)
    {
        //0代表通过
        //1代表不是当前销售
        //-1 代表不相干,也是拒绝
        if (is_admin()) {
            return 0;
        }
        $CI =& get_instance();

        $CI->load->model('biz_duty_model');
        $duty = $CI->biz_duty_model->get_old_by_new($id_type, $id_no);

        $userId = get_session('id');
        //shipment判断S2是否是当前销售,
        if ($id_type == 'biz_shipment') {
            //S0 -> S1 不用判定
            if ($lock_lv == 0 && $lock == 1) {
                //2022-05-24 由于系统里 存在操作主管 作为 客服的情况, 所以这里加入限制,必须当前 操作主管是 当前操作的上级时,才能锁S1或解锁
                // if (!leader_check('operator', $duty['operator_id'], true)) {
                //     return '不是当前操作的上级';
                // } else {
                //     return 0;
                // }
                if(menu_role('shipment_lock1')){
                    return 0;
                }else{
                    return lang("没有S1锁权限");
                }
            }
            //S1 -> S0
            if ($lock_lv == 1 && $lock == 0) {
                return 0;
            }
            //S1->S2时候判断是否为当前销售
            if (($lock_lv == 1 && $lock == 2)) {
                // if ($userId == $duty['sales_id']) {
                //     return 0;
                // } else {
                //     return '不是当前销售';
                // }
                if(menu_role('shipment_lock2')){
                    return 0;
                }else{
                    return lang("没有S2锁权限");
                }
            }
            //S2 -> S0
            if ($lock_lv == 2 && $lock == 0) {
                if(menu_role('shipment_lock2')){
                    return 0;
                }else{
                    return lang("没有S2锁权限");
                }
                // if ($userId == $duty['sales_id']) {
                //     return 0;
                // } else {
                //     return '不是当前销售';
                // }
            }
            //S2->S3 不用判定
            if ($lock_lv == 2 && $lock == 3) {
                return 0;
            }
            //S3 -> S0 不用判定
            if ($lock_lv == 3 && $lock = 0) {
                return 0;
            }
        }

        return 0;
    }
}

if(!function_exists('save_queue')) {
    /**
     * 保存任务
     * @param int $name_id 任务id,详情打开sys_queue_model查看,找不到直接将ID作为name使用
     * @param string $id_type 表来源
     * @param int $id_no 表主键
     * @param array $request_data 传入参数, 不填写默认将id_no作为id传入
     * @param string $exec_time 执行时间 不填写默认当前
     * @param string $action 方法名称 后续拼接上请求域名
     * @return bool
     */
    function save_queue($name_id = '', $id_type = '', $id_no = 0, $request_data = array(), $exec_time = "", $action = "")
    {
        if (empty($exec_time)) $exec_time = date('Y-m-d H:i:s', time());
        if (empty($request_data)) $request_data['id'] = $id_no;
        $sys_queue_model = Model('sys_queue_model');
        //获取预配置的任务方法名和名称
        if (empty($action)) $action = $sys_queue_model->get_queue_url($name_id);
        $name = $sys_queue_model->get_queue_name($name_id);

        if (empty($name) || empty($id_no) || empty($id_type) || empty($action)) {
            return false;
        }
        //如果只传了方法过来,那么默认为sys_queue控制器
        $action_array = explode('/', $action);
        if (sizeof($action_array) == 1) {
            $action = '/sys_queue/' . $action;
        }
        $url = 'http://china2.leagueshipping.com' . $action;

        $queue_save = array();
        $queue_save['name'] = $name;
        $queue_save['id_type'] = $id_type;
        $queue_save['id_no'] = $id_no;
        $queue_save['url'] = $url;
        $queue_save['exec_time'] = $exec_time;
        $queue_save['request_data'] = json_encode($request_data);
        $sys_queue_model->save($queue_save);
    }
}

if(!function_exists('getTree')) {
    function getTree($item = array(), $pid = 0, $sub = 'children', $level = 1)
    {
        $data = array();
        foreach ($item as $key => $val) {
            if ($val['pid'] == $pid) {
                $val['level'] = $level;
                $val[$sub] = getTree($item, $val['id'], 'sub', $level + 1);
                $data[] = $val;
            }
        }
        return $data;
    }
}

if(!function_exists('consol_job_no_rule')) {
    /**
     * 生成job_no 根据ID生产C编号
     */
    function consol_job_no_rule($id)
    {
        //前缀--start
        // $company_no = CONSOL_NUMBER_PREFIX;
        $company_no = Model('sys_config_model')->get_one_zero("CONSOL_NUMBER_PREFIX")['config_text'];
        //前缀--end

        //检测$company_no 是几位,如果是1 那么数字为8,最多
        $num_length = 9 - strlen($company_no);

        //最后8位数字,以同前缀最后一条+1
        $prefix = $company_no;
        $num = str_pad($id, $num_length, '0', STR_PAD_LEFT);

        $job_no = $prefix . $num;
        return $job_no;
    }
}

if(!function_exists('merge_box_info')) {
    /**
     * 合并箱型箱量
     * @param array $box_info1
     * @param array $box_info2
     * @param string $c
     * @return array
     */
    function merge_box_info($box_info1 = array(), $box_info2 = array(), $c = '+')
    {
        $box_info2 = array_column($box_info2, null, 'size');
        foreach ($box_info1 as $box) {
            //判断箱型是否存在,存在+num,不存在后面添加
            if (isset($box_info2[$box['size']])) {
                if ($c == '-') $box_info2[$box['size']]['num'] -= $box['num'];
                else $box_info2[$box['size']]['num'] += $box['num'];
            } else {
                $box_info2[$box['size']] = $box;
            }
        }

        $result_box_info = array();
        foreach ($box_info2 as $box) {
            $result_box_info[] = $box;
        }
        return $result_box_info;
    }
}

/**
 * 去除不可编辑的字段的代码
 * @param array $data 数据
 * @param array $userBscEdit 可修改的字段
 * @param array $diff 不可修改的字段
 * @param string $cause 原因
 * @param array $mustDisabled 必须禁用的字段
 * @return array
 */
if(!function_exists('checkboxDisabledField')) {
    function checkboxDisabledField($data = array(), $userBscEdit = array(), $diff = array(), $cause = '', $mustDisabled = array())
    {

        $disabled = array();
        $errorTips = array();

        //从diff里, 有值的禁用,无值的提示必填
        foreach ($diff as $val) {
//            if (isset($data[$val])) {
            if (array_key_exists($val, $data)) {
                if (!in_array($val, $mustDisabled) && (empty($data[$val]) || $data[$val] == '0000-00-00 00:00:00' || $data[$val] == '0000-00-00' || $data[$val] == '0000-00-00 00:00')) $errorTips[] = $val;
                else $disabled[] = $val;
            }
        }

        $CI =& get_instance();
        $CI->load->view("tip_view", array('errorTips' => $errorTips, 'disabledTips' => $disabled, 'cause' => lang($cause)));

        //且让对应的diff字段标红
        return array_diff($userBscEdit, $disabled);
    }
}
if(!function_exists('match_chinese')) {
    /**
     * 将字符串去除文字等
     * @param $chars
     * @param string $encoding
     * @return string
     */
    function match_chinese($chars, $encoding = 'utf8')
    {
        $pattern = ($encoding == 'utf8') ? '/[\x{4e00}-\x{9fa5}a-zA-Z0-9]/u' : '/[\x80-\xFF]/';
        preg_match_all($pattern, $chars, $result);
        return join('', $result[0]);
    }
}

if(!function_exists('leader_check')) {
    /**
     * 领导权限判断,该方法用于S2 销售锁,操作SI,截单SI勾选等地方
     */
    function leader_check($role, $check_user_id = 0, $is_me = true){
        //如果未传入user_id,那么默认为当前人员
        if($check_user_id == 0) $check_user_id = get_session('id');
        $groupRange = get_session('group_range');//带,的字符串
        $userRange = get_session('user_range');//数组
        $userId = get_session('id');//字符串
        $userRole = get_session('user_role');//数组

        //如果是管理员,通过
        if(is_admin()) return true;
        //如果当前人员,和传入的角色不相同,那么不给通过
        if(!is_array($userRole) || !in_array($role, $userRole)) return false;

        if($is_me && $userId == $check_user_id) return true;//如果判断的人等于当前用户,那么直接通过

        //不等于当前人时,如果该人员存在于当前用户的下属员工里,通过
        if(in_array($check_user_id, $userRange) && $userId != $check_user_id) return true;

        $checkUserGroup = getUserField($check_user_id, 'group');//getUserField 为传入 ID,和想要获取的字段,就可以单独获取该用户的某个字段了
        //如果返回为空,不给通过
        if($checkUserGroup == '') return false;
        //如果该人员,所在组存在于当前人员下属部门里,那么通过
        if(in_array($checkUserGroup, explode(',', $groupRange))) return true;

        return false;
    }
}

if(!function_exists('array_trim')) {
    /**
     * 数组批量trim
     * @param $array
     * @param string $trim_str
     * @return mixed
     */
    function array_trim($array, $trim_str = ' ')
    {
        foreach ($array as $key => $val) {
            if (is_array($val)) {
                $array[$key] = array_trim($val);
            } else {
                $array[$key] = trim($array[$key], $trim_str);
            }
        }
        return $array;
    }
}

/**
 * 判断
 * @param $company_name
 * @param $role
 */
if(!function_exists('check_company_role')) {
    function check_company_role($company_name, $role)
    {
        if (!is_array($role)) $role = explode(',', $role);

        if (in_array('client', $role) || in_array('factory', $role)) {
            if (strstr($company_name, '物流') !== false) return "往来单位公司名称带有“物流/供应链/货运/船务”，不允许使用client factory这2个角色,建议选择：logistic client";
            if (strstr($company_name, '供应链') !== false) return "往来单位公司名称带有“物流/供应链/货运/船务”，不允许使用client factory这2个角色,建议选择：logistic client";
            if (strstr($company_name, '货运') !== false) return "往来单位公司名称带有“物流/供应链/货运/船务”，不允许使用client factory这2个角色,建议选择：logistic client";
            if (strstr($company_name, '船务') !== false) return "往来单位公司名称带有“物流/供应链/货运/船务”，不允许使用client factory这2个角色,建议选择：logistic client";
        }

        //false代表通过, true相关的值都代表不通过原因
        return false;
    }
}

/**
 * 根据当前client的代码,和所有角色,进行权限表数据的处理
 */
if(!function_exists('client_role_duty')) {
    function client_role_duty($client_code = '', $role = '')
    {
        $CI =& get_instance();
        if(is_string($role)) $role = explode(',', $role); //字符串转为数组
        $role[] = '-1';
        if(!is_array($role)) return false;//不是数组不处理
        //这里先查再删, 如果后面确定不需要日志,也可以改成直接mdelete where的
        // $CI->db->force_master();
        $rs = Model('biz_client_duty_model')->get("client_code = '{$client_code}'");
        $all_role_duty = array();
        foreach ($rs as $row){
            if(!in_array($row['client_role'], $role)) {
                Model('biz_client_duty_model')->mdelete($row['id']);
                //删除时,日志是否记录, 目前暂时不记录 新增有日志了,
                continue;
            }

            if($row['user_id'] == -1){
                //全开放的都塞入这里 后面可以避免重复插入数据
                $all_role_duty[$row['client_role']] = $row;
            }
        }
        //如果是供应商等角色,这里直接加一条-1的数据
        $client_role_config = Model('biz_client_duty_model')->client_role_config;

        foreach ($role as $r){
            //检测当前供应商角色是否添加了全开权限 没有的话新增
            if(!isset($client_role_config[$r])) continue;//没有角色配置
            if($client_role_config[$r][1] != 'all') continue; //不是全开放
            if(isset($all_role_duty[$r])) continue; //已有全开放数据

            $data = array();
            $data['client_code'] = $client_code;
            $data['client_role'] = $r;
            $data['user_role'] = '';
            $data['user_id'] = -1;
            $data['user_group'] = '-1';

            $id = Model('biz_client_duty_model')->save($data);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_client_duty";
            $log_data["key"] = $id;
            $log_data["master_table_name"] = 'biz_client';
            $log_data["master_key"] = $client_code;
            $log_data["action"] = "insert";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        }

        return true;
    }
}

if(!function_exists('curl_get')) {
    function curl_get($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json;charset='utf-8'",
            )
        );
        return curl_exec($ch);
    }
}


if(!function_exists('update_message')) {
    /**
     * 更新文件信息
     * @param $consol_id
     * @param $new_file_path
     * @param $type
     */
    function update_message($consol_id, $data, $message_type){
        $CI =& get_instance();
        Model('bsc_message_model');
        //保存成功后,进行数据的新增或更新
        $message_info = $CI->bsc_message_model->get_one_where("id_type = 'consol' and id_no = '$consol_id' and type = '$message_type'");
        if(!empty($message_info)){
            $id = $message_info['id'];
            $CI->bsc_message_model->update($message_info['id'], $data);
            $action = 'update';
        }else{
            if(!isset($data['type'])) $data['type'] = $message_type;
            $data['id_type'] = 'consol';
            $data['id_no'] = $consol_id;
            $data['consol_id'] = $consol_id;
            $id = $CI->model->save($data);
    
            $action = 'insert';
        }
    
        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "bsc_message";
        $log_data["key"] = $id;
        $log_data["action"] = $action;
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        return $id;
    }
}

if(!function_exists('update_message_by_shipment')) {
    /**
     * 更新文件信息
     * @param $consol_id
     * @param $new_file_path
     * @param $type
     */
    function update_message_by_shipment($shipment_id, $data, $message_type){
        $CI =& get_instance();
        //保存成功后,进行数据的新增或更新
        $message_info = $CI->bsc_message_model->get_one_where("id_type = 'shipment' and id_no = '$shipment_id' and type = '$message_type'");
        if(!empty($message_info)){
            $id = $message_info['id'];
            $CI->bsc_message_model->update($message_info['id'], $data);
            $action = 'update';
        }else{
            if(!isset($data['type'])) $data['type'] = $message_type;
            $data['id_type'] = 'shipment';
            $data['id_no'] = $shipment_id;
            $id = $CI->bsc_message_model->save($data);
    
            $action = 'insert';
        }
    
        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "bsc_message";
        $log_data["key"] = $id;
        $log_data["action"] = $action;
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        
        return $id;
    }
}

if(!function_exists('curl_post_body')) {
    function curl_post_body($url, $header, $body, $cookie = array())
    {
        $ch = curl_init($url);
        //声明使用POST方式来进行发送
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        if(!empty($cookie)){
            curl_setopt($ch, CURLOPT_COOKIE, $cookie);
        }
        return curl_exec($ch);
    }
}

if(!function_exists('number_to_word')) {
    /**
     * 数字转英文
     */
    function number_to_word($num = '')
    {
        $num = ( string )(( int )$num);
        if (( int )($num) && ctype_digit($num)) {
            $words = array();
            $num = str_replace(array(',', ' '), '', trim($num));
             $list1 = array('', 'ONE', 'TWO', 'THREE', 'FOUR', 'FIVE', 'SIX', 'SEVEN',
                'EIGHT', 'NINE', 'TEN', 'ELEVEN', 'TWELVE', 'THIRTEEN', 'FOURTEEN',
                'FIFTEEN', 'SIXTEEN', 'SEVENTEEN', 'EIGHTEEN', 'NINETEEN');
            $list2 = array('', 'TEN', 'TWENTY', 'THIRTY', 'FORTY', 'FIFTY', 'SIXTY',
                'SEVENTY', 'EIGHTY', 'NINETY', 'HUNDRED');
            $list3 = array('', 'THOUSAND', 'MILLION', 'BILLION', 'TRILLION',
                'QUADRILLION', 'QUINTILLION', 'SEXTILLION', 'SEPTILLION',
                'OCTILLION', 'NONILLION', 'DECILLION', 'UNDECILLION',
                'DUODECILLION', 'TREDECILLION', 'QUATTUORDECILLION',
                'QUINDECILLION', 'SEXDECILLION', 'SEPTENDECILLION',
                'OCTODECILLION', 'NOVEMDECILLION', 'VIGINTILLION');
            $num_length = strlen($num);
            $levels = ( int )(($num_length + 2) / 3);
            $max_length = $levels * 3;
            $num = substr('00' . $num, -$max_length);
            $num_levels = str_split($num, 3);
            foreach ($num_levels as $num_part) {
                $levels--;
                $hundreds = ( int )($num_part / 100);
                $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' HUNDRED' . ($hundreds == 1 ? '' : '') . ' ' : '');
                $tens = ( int )($num_part % 100);
                $singles = '';
                if ($tens < 20) {
                    $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '');
                } else {
                    $tens = ( int )($tens / 10);
                    $tens = ' ' . $list2[$tens] . ' ';
                    $singles = ( int )($num_part % 10);
                    $singles = ' ' . $list1[$singles] . ' ';
                }
                $words[] = $hundreds . $tens . $singles . (($levels && ( int )($num_part)) ? ' ' . $list3[$levels] . ' ' : '');
            }
    
            $commas = count($words);
    
            if ($commas > 1) {
                $commas = $commas - 1;
            }
    
            $words = implode(', ', $words);
    
            //Some Finishing Touch
            //Replacing multiples of spaces with one space
            $words = trim(str_replace(' ,', ',', trim_all(ucwords($words))), ', ');
            if ($commas) {
                $words = str_replace_last(',', ' AND', $words);
            }
            return $words;
        } else if (!(( int )$num)) {
            return 'ZERO';
        }
        return '';
    }
}

if(!function_exists('trim_all')) {
    function trim_all($str, $what = NULL, $with = ' ')
    {
        if ($what === NULL) {
            //  Character      Decimal      Use
            //  "\0"            0           Null Character
            //  "\t"            9           Tab
            //  "\n"           10           New line
            //  "\x0B"         11           Vertical Tab
            //  "\r"           13           New Line in Mac
            //  " "            32           Space
    
            $what = "\\x00-\\x20";    //all white-spaces and control chars
        }
    
        return trim(preg_replace("/[" . $what . "]+/", $with, $str), $what);
    }
}

if(!function_exists('str_replace_last')) {
    function str_replace_last($search, $replace, $str)
    {
        if (($pos = strrpos($str, $search)) !== false) {
            $search_length = strlen($search);
            $str = substr_replace($str, $replace, $pos, $search_length);
        }
        return $str;
    }
}

if(!function_exists('get_system_type')){
    /**
     * 获取当前的系统类型
     * 版本1 根据域名的前缀返回
     */
    function get_system_type(){
        $host = explode('.', $_SERVER['HTTP_HOST']);
        
        return $host[0];
    }
}
if(!function_exists('float_number')){
    /**
     * 格式化数字
     */
    function float_number($number){
        $left_number = round($number);
        $left_length = strlen($left_number);
        if($left_length > 4){
            $number = round($number / 10000, 2);
            return $number . "万";
        }else{
            return $number;
        }
    }
}
if(!function_exists('number_to_word_vn')){
    function number_to_word_vn($num)
    {
        $to_19 = array('không', 'một', 'hai', 'ba', 'bốn', 'năm', 'sáu',
            'bảy', 'tám', 'chín', 'mười', 'mười một', 'mười hai',
            'mười ba', 'mười bốn', 'mười lăm', 'mười sáu', 'mười bảy',
            'mười tám', 'mười chín');
        $tens = array('hai mươi', 'ba mươi', 'bốn mươi', 'năm mươi',
            'sáu mươi', 'bảy mươi', 'tám mươi', 'chín mươi');
        $denom = array('',
            'nghìn', 'triệu', 'tỷ', 'nghìn tỷ', 'trăm nghìn tỷ',
            'Quintillion', 'Sextillion', 'Septillion', 'Octillion', 'Nonillion',
            'Decillion', 'Undecillion', 'Duodecillion', 'Tredecillion',
            'Quattuordecillion', 'Sexdecillion', 'Septendecillion',
            'Octodecillion', 'Novemdecillion', 'Vigintillion');
    
        function _convert_nn($val, $to_19, $tens, $denom)
        {
            if ($val < 20) {
                return $to_19[$val];
            } else {
                foreach ($tens as $v => $k) {
                    foreach (array($k => 20 + (10 * $v)) as $dcap => $dval) {
                        if ($dval + 10 > $val) {
                            var_dump($dcap);
                            if ($val % 10) {
                                $a = 'lăm';
                                if ($to_19[$val % 10] == 'một') {
                                    $a = 'mốt';
                                } else {
                                    $a = $to_19[$val % 10];
                                }
                                if ($to_19[$val % 10] == 'năm') {
                                    $a = 'lăm';
                                }
                                return $dcap . ' ' . $a;
                            }
                            return $dcap;
                        }
                    }
                }
            }
        }
        function _convert_nnn($val, $to_19, $tens, $denom)
        {
            $word = '';
            list($mod, $rem) = array($val % 100, floor($val / 100));
            if ($rem > 0) {
                $word = $to_19[$rem] . ' trăm';
                if($mod > 0){
                    $word = $word . ' ';
                }
            }
            if($mod > 0 && $mod < 10){
                if($mod == 5){
                    $word = $word != '' ? $word . 'lẻ năm' : $word . 'năm';
                }else{
                    $word = $word != '' ? $word . 'lẻ ' . _convert_nn($mod, $to_19, $tens, $denom) : $word . _convert_nn($mod, $to_19, $tens, $denom);
                }
            }
            if($mod >= 10){
                $word = $word . _convert_nn($mod, $to_19, $tens, $denom);
            }
            return $word;
        }
    
        function vietnam_number($val, $to_19, $tens, $denom){
            if($val < 100){
                return _convert_nn($val, $to_19, $tens, $denom);
            }
            if($val < 1000) {
                return _convert_nnn($val, $to_19, $tens, $denom);
            }
            foreach ($denom as $v => $k){
                foreach (array($v => pow(1000, $v + 1)) as $didx => $dval){
                    if($dval > $val){
                        $mod = pow(1000, $didx);
                        $lval = floor($val / $mod);
                        $r = $val - ($lval * $mod);
    
                        $ret = _convert_nnn($lval, $to_19, $tens, $denom) . ' ' . $denom[$didx];
                        if (99 >= $r && $r > 0){
                            $ret = _convert_nnn($lval, $to_19, $tens, $denom) . ' ' . $denom[$didx] . ' lẻ';
                        }
                        if ($r > 0){
                            $ret = $ret . ' ' . vietnam_number($r, $to_19, $tens, $denom);
                        }
                        return $ret;
                    }
                }
            }
        }
    
        function number_to_text($number, $to_19, $tens, $denom){
            $number = round($number, 2);
            $the_list = explode('.', $number);
            $start_word = vietnam_number((int)$the_list[0], $to_19, $tens, $denom);
            $final_result = $start_word;
            if (sizeof($the_list) > 1 && (int)$the_list[1] > 0){
                $end_word = vietnam_number((int)$the_list[1], $to_19, $tens, $denom);
                $final_result = $final_result . ' phẩy ' . $end_word;
            }
            return $final_result;
        }
        return number_to_text($num, $to_19, $tens, $denom);
    }
}

if(!function_exists('check_mail_valid')){
    /**
     * 验证邮箱有效性
     * @param $email 邮箱
     * @param $force_pass 是否强制通过,true表示通过
     */
    function check_mail_valid($email, $force_pass = false){
        $time = time();
        $db = Model('op_model')->get_db();
        $email = trim($email);

        //查询是否曾经验证过, 限制必须一年内
        $sql = "select id,invalid_email,ok,valid_time from crm_promote_mail_invalid where invalid_email = '{$email}'";
        $invalid_row = $db->query($sql)->row_array();
        if(!empty($invalid_row)){
            //如果有值,且ok = 1 那么返回true
            if($invalid_row['ok'] == 1){
                //如果验证时间为一年之前,那么重新验证
                $time_volid = strtotime($invalid_row['valid_time']);
                $time_year = strtotime('-1 year', $time);
                //当前2022-04-20
                //2021-04-20 <= 2021-04-21时,代表验证还没过期
                if($time_year <= $time_volid) return true;
            }
            if($invalid_row['ok'] == -1 && !$force_pass) return false;
            $id = $invalid_row['id'];
        }else{
            //如果没值,那么新增一条
            $sql = "INSERT INTO `crm_promote_mail_invalid`(`invalid_email`, `created_time`, `valid_time`) VALUES ('{$email}', '" . date('Y-m-d H:i:s', $time) . "', '" . date('Y-m-d', $time) . "');";
            $id = $db->query($sql)->insert_id();
        }

        //首先验证邮箱规则是否满足
        if(!check_email($email)){
            //不满足直接false
            //验证不通过
            $sql = "UPDATE `crm_promote_mail_invalid` SET `ok` = -1 WHERE `id` = {$id}";
            $db->query($sql);
            return false;
        }

        if($force_pass){
            //验证通过
            $sql = "UPDATE `crm_promote_mail_invalid` SET `ok` = 1,`valid_time` = '" . date('Y-m-d', $time) . "' WHERE `id` = {$id}";
            $db->query($sql);
            return true;
        }

            // 重新验证
            $sql = "UPDATE `crm_promote_mail_invalid` SET `ok` = 0 WHERE `id` = {$id}";
            $db->query($sql);
            return false;
    }
}

if(!function_exists('check_email')) {
    function check_email($email)
    {
        $result = trim($email);
        if (filter_var($result, FILTER_VALIDATE_EMAIL)){
            return true;
        }
        else{
            return false;
        }
    }
}

/**
 * 创建CRM客户流转日志
 */
if(!function_exists('create_crm_log')) {
    function create_crm_log($id_no=0, $sales_id=0, $operation_id=0, $remark='')
    {
        $CI = & get_instance();
        $CI->db = Model('op_model')->get_db();
        if ((int)$id_no==0 || (int)$operation_id==0 || $remark=='') return false;
        $logData = array('id_no'=>$id_no, 'sales_id'=>$sales_id, 'operation_id'=>$operation_id, 'remark'=>$remark, 'create_time'=>time());
        return $CI->db->insert('biz_client_crm_log', $logData);
    }
}


/**
 * 清除特殊字符
 */
if(!function_exists('clear_special_str')) {
    function clear_special_str($str){
        if (trim($str) == '') return '';
    
        $arr = array_map(
            function ($v) {
                //if(不是中文字符 && ASCII代码为非常用特殊符号)
                if (!preg_match("/^[\x7f-\xff]+$/", $v) && (ord($v) < 32 || ord($v) > 126)) {
                    return '';
                } else {
                    return $v;
                }
            }, str_split($str)
        );
        return join('', $arr);
    }
}

/**
 * 创建浏览日志
 */
 if(!function_exists('create_browse_log')) {
    function create_browse_log($id_type='', $id_no=0, $id_name = ''){
        if ($id_type == '' || (int)$id_no == 0){
            return false;
        }else {
            $CI = &get_instance();
            $data = array(
                'user_id' => get_session('id'),
                'id_type' => $id_type,
                'id_no' => $id_no,
                'id_name' => $id_name,
                'create_time' => date('Y-m-d H:i:s')
            );
            return Model('op_model')->get_db()->insert('sys_browse_log', $data);
        }
    }
}

if (!function_exists('truncate')) {
    /**
     * 截取字符串，已经去除所有html标签
     * @param string $string 待截取的字符串
     * @param integer $length 截取的长度
     * @param string $etc 结束字符串
     * @return string truncated string
     */
    function truncate($string, $length = 80, $dot = '...', $break_words = true, $middle = false)
    {
        if ($length == 0)
            return '';
        $string = str_replace("\r\n", '', $string);
        $string = str_replace("\n", '', $string);
        $string = str_replace("\t", '', $string);

        if (is_callable('mb_strlen')) {
            if (mb_strlen($string) > $length) {
                $length -= min($length, mb_strlen($dot));
                if (!$break_words && !$middle) {
                    $string = preg_replace('/\s+?(\S+)?$/u', '', mb_substr($string, 0, $length + 1));
                }
                if (!$middle) {
                    return mb_substr($string, 0, $length) . $dot;
                } else {
                    return mb_substr($string, 0, $length / 2) . $dot . mb_substr($string, -$length / 2);
                }
            } else {
                return $string;
            }
        } else {
            if (strlen($string) > $length) {
                $length -= min($length, strlen($dot));
                if (!$break_words && !$middle) {
                    $string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length + 1));
                }
                if (!$middle) {
                    return substr($string, 0, $length) . $dot;
                } else {
                    return substr($string, 0, $length / 2) . $dot . substr($string, -$length / 2);
                }
            } else {
                return $string;
            }
        }
    }
}

if(!function_exists('add_mail')){
    /**
     * 发送邮件的事件 version 2.0
     * 这里后续box和receiver是否会存在,一个box对应多个receiver的情况,如果有,这里的可能不能满足了
     * @param string $send_to 发送人
     * @param string $subject 标题
     * @param string $content 内容
     * @param string $ccs 抄送人
     * @param int $template_id 模板ID
     * @param int $mail_status 邮件状态, -1为待审核(预留,暂时用不上), 0 为草稿,1为正式, 默认为1
     * @param int $mail_sender_id 模板通道,没有的话,发送时,会自动使用一个通道
     * @param string $exec_time 发送时间
     * @param string $id_type
     * @param string $id_no 到时候可能会用于统计,查看发送的相关
     * @return bool|void
     */
    function add_mail($send_to = '', $subject = '', $content = '', $ccs = '', $template_id = 0, $exec_time = "", $mail_sender_id = 0, $mail_status = 1, $id_type = '', $id_no = 0){
        if($send_to == '') return false;
        if($subject == '') $subject = '标题';
        if($exec_time == '') $exec_time = date('Y-m-d H:i:s');

        //新增数据到
        $mail_box_data = array(
            'template_id' => $template_id,
            'mail_title' => $subject,
            'mail_content' => $content,
            'plan_send_time' => $exec_time,
            'mail_status' => $mail_status,
            'id_type' => $id_type,
            'id_no' => $id_no,
        );

        $mail_box_id = Model('mail_box_model')->save($mail_box_data);
        if(!$mail_box_id) return false;
        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "mail_box";
        $log_data["key"] = $mail_box_id;
        $log_data["action"] = 'insert';
        $log_data["value"] = json_encode($mail_box_data);
        log_rcd($log_data);

        $mail_receiver_data = array(
            'mail_box_id' => $mail_box_id,
            'email' => trim($send_to),
//            'name' => '',
            'ccs' => trim($ccs),
            'mail_sender_id' => $mail_sender_id,
        );

        $mail_receiver_id = Model('mail_receiver_model')->save($mail_receiver_data);

        if(!$mail_receiver_id) return false;
        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "mail_receiver";
        $log_data["key"] = $mail_receiver_id;
        $log_data["action"] = 'insert';
        $log_data["value"] = json_encode($mail_receiver_data);
        log_rcd($log_data);
        
        if($mail_sender_id == 0) $mail_sender_id = 1;
        
        $mail_sender = Model('mail_sender_model')->get_where_one("id = {$mail_sender_id}");
        send_mail($mail_receiver_id, $mail_box_id, $send_to, $subject, $content, $ccs, '', $mail_sender['smtp_host'], $mail_sender['smtp_username'], $mail_sender['smtp_password'], $mail_sender['smtp_port'], $mail_sender['ssl_enable']);

        return $mail_box_id;
    }
}

if(!function_exists('send_mail')){
    function send_mail($mail_receiver_id, $mail_box_id, $send_to, $title, $content, $ccs, $reply_by, $SMTP_host, $SMTP_username, $SMTP_password, $SMTP_port, $is_ssl){
        $CI = & get_instance();
        $Mail_receiver = Model('mail_receiver_model');
        $CI->load->library('Mail');

        $mail = new Mail();
        
        //2022-10-27 收件人支持多个
        $receivers = array();
        $receivers_array = filter_unique_array(explode(',', $send_to));
        foreach ($receivers_array as $receiver){
            $receivers[] = array('email' => $receiver);
        }
        
        //创建人和销售
        $this_CCs = array();
        $ccs_array = filter_unique_array(explode(',', $ccs));
        foreach ($ccs_array as $cc){
            $this_CCs[] = array('email' => $cc);
        }
        unset($ccs_array);
        $this_reply_by = array();
        $reply_by_array = filter_unique_array(explode(',', $reply_by));
        foreach ($reply_by_array as $val){
            //和smtphost一样时,不处理
            if($val == $SMTP_host) continue;
            $this_reply_by[] = array('email' => $val);
        }
        unset($reply_by_array);
        
        //默认后面插入一个隐藏的图片标签 这里有个只读的问题就是 由于数据只有1条,多个人时候,就不知道是谁查看的了 后面可能还是得每个人发一封
        $img = "http://client.longhoang-group.com/crm_promote_mail_handle/mail_receiver_img/{$mail_receiver_id}?source=" . get_system_type();
        $content .= "<img src=\"" . $img . "\" style=\"display:none;\"></img>";
        $content = "<a href='https://client.leagueshipping.com/api/mail_true/?key={$mail_receiver_id}&b={$mail_box_id}' style='color:red;font-size:12px;text-decoration:none;' target='_blank'>This email is sent by system. Click to verify truth.</a><hr>" . $content;
        
        // Log::info('正在载入邮件参数:');
        $mail::$receivers = $receivers;
        // Log::info("收件人: {$send_to}");
        $mail::$CCs = $this_CCs;
        // Log::info("抄送人: {$ccs}");
        //加入了如果没有回复人,那么这里不进行处理
        if(!empty($this_reply_by)) {
            $mail::$reply_by = $this_reply_by;
            // Log::info("回复人: {$reply_by}");
        }
        unset($this_reply_by);
        //日志输出太多如果日志文件过大可能到时候会导致超时, 超时时间就得适量的增加一点
        $mail::$subject = $title;
        // Log::info("标题: {$title}");
        $mail::$body = $content;
        // Log::info("内容: {$content}");
        $mail::$SMTP_host = $SMTP_host;
        // Log::info("SMTP_host: {$SMTP_host}");
        $mail::$SMTP_username = $SMTP_username;
        // Log::info("SMTP_username: {$SMTP_username}");
        $mail::$SMTP_password = $SMTP_password;
        // Log::info("SMTP_password: {$SMTP_password}");
        $mail::$SMTP_port = $SMTP_port;
        // Log::info("SMTP_port: {$SMTP_port}");
        $mail::$is_ssl = $is_ssl;
        // Log::info("is_ssl: {$is_ssl}");
        //TODO 这里到时候可能会加个发送附件的功能,需要增加一张表,
        // Log::info("当前内存占用: " . memory_get_usage());
        // echo "{$SMTP_host} | {$SMTP_username} | {$SMTP_password} | {$SMTP_port}";
        // Log::info("开始发送邮件 {$title} 邮件ID: {$mail_receiver_id}");
        $result = $mail->send_mail();
        // $result = "Message has been sent";
        $update_data = array();
        $update_data['send_time'] = date('Y-m-d H:i:s');
        //发送成功  
        if($result == 'Message has been sent'){
            // echo "发送成功 {$title} 邮件ID: {$mail_receiver_id}\n";
            // Log::info("发送成功 {$title} 邮件ID: {$mail_receiver_id}");
            $update_data['send_status'] = 1;
        }else{
            // echo "发送失败: 原因 {$result}\n";
            // Log::info("发送失败: 原因 {$result}");
            $update_data['send_status'] = -1;
        }
        $update_data['send_result'] = $result;
        // save方法第二个参数为更新条件
        $Mail_receiver->update($mail_receiver_id, $update_data);
        if($result == 'Message has been sent'){
            return lang("发送成功 {title} 邮件ID: {mail_receiver_id}", array('title' => $title, 'mail_receiver_id' => $mail_receiver_id));
        }else{
            return lang("发送失败: 原因 {result}", array('result' => $result));
        }
    }
}

if(!function_exists('add_template_mail')){
    /**
     * 根据模板 这里发送的全是正式的
     * @param int $template_id 模板ID
     * @param array $data 变量
     * @param string $send_to 发送人
     * @param string $ccs 抄送人
     * @param string $exec_time 发送时间
     * @param string $id_type 类型
     * @param int $id_no id
     * @return bool|void
     */
    function add_template_mail($template_id, $data = array(),$send_to = '', $ccs = '', $exec_time = "", $id_type = '', $id_no = 0){
        //add_mail这里不用处理, 直接使用文本就行
        //查询模板
        $template = Model("mail_template_model")->get_where_one("id = '{$template_id}'");
        if(empty($template)) return false;
        if(empty($data)) $data = array();

        $subject = $template['mail_title'];
        $content = $template['mail_content'];
        //获取所有变量
        $CI =& get_instance();
        $CI->load->library('Variable_content');
        $CI->Variable_content = new Variable_content($id_no, $data);
        $CI->Variable_content->setVariableRule("/\{(.*?)}/i");
        $CI->Variable_content->setCleanRule("/[{|}]/");
        $subject = $CI->Variable_content->get($subject);
        $content = $CI->Variable_content->get($content);

        return add_mail($send_to, $subject, $content, $ccs, $template['id'], $exec_time, $template['default_mail_sender_id'], 1, $id_type, $id_no);
    }
}

if(!function_exists('convert_encoding')){
    /**
     * 兼容性转码
     *
     * 系统转换编码调用此函数, 会自动根据当前环境采用 iconv 或 MB String 处理
     *
     * @param  string
     * @param  string
     * @param  string
     * @return string
     */
    function convert_encoding($string, $from_encoding = 'GBK', $target_encoding = 'UTF-8')
    {
    	if (function_exists('mb_convert_encoding'))
    	{
    		return mb_convert_encoding($string, str_replace('//IGNORE', '', strtoupper($target_encoding)), $from_encoding);
    	}
    	else
    	{
    		if (strtoupper($from_encoding) == 'UTF-16')
    		{
    			$from_encoding = 'UTF-16BE';
    		}
    
    		if (strtoupper($target_encoding) == 'UTF-16')
    		{
    			$target_encoding = 'UTF-16BE';
    		}
    
    		if (strtoupper($target_encoding) == 'GB2312' or strtoupper($target_encoding) == 'GBK')
    		{
    			$target_encoding .= '//IGNORE';
    		}
    
    		return iconv($from_encoding, $target_encoding, $string);
    	}
    }
}

if(!function_exists('update_message_by_id_type')){
    /**
     * 更新文件信息
     * @param $consol_id
     * @param $new_file_path
     * @param $type
     */
    function update_message_by_id_type($data, $id_type = '', $id_no = '', $message_type = '', $message_receiver = ''){
        $CI =& get_instance();
        if(!isset($data['id_type'])) $data['id_type'] = $id_type;
        else $id_type = $data['id_type'];
        if(!isset($data['id_no'])) $data['id_no'] = $id_no;
        else $id_no = $data['id_no'];
        if(!isset($data['receiver'])) $data['receiver'] = $message_receiver;
        else $message_receiver = $data['receiver'];
        if(!isset($data['type'])) $data['type'] = $message_type;
        else $message_type = $data['type'];
        //保存成功后,进行数据的新增或更新
        $message_info = Model('bsc_message_model')->get_one_where("id_type = '{$id_type}' and id_no = '{$id_no}' and type = '{$message_type}'");
        if(!empty($message_info)){
            $id = $message_info['id'];
            Model('bsc_message_model')->update($message_info['id'], $data);
            $action = 'update';
        }else{
            $id = Model('bsc_message_model')->save($data);
    
            $action = 'insert';
        }
    
        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "bsc_message";
        $log_data["key"] = $id;
        $log_data["action"] = $action;
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        return $id;
    }
	function special_test($action=''){
		$CI =& get_instance();
		$special_text = explode(',', $CI->session->userdata('special_text'));
		$result = false;
		if(in_array($action, $special_text) || is_admin()){
			$result = true;
		}
		return $result;
	}
}

if(!function_exists('show_steps')){
	/**
	 *
	 */
	function show_steps($step_field='',$table='biz_shipment',$id=0,$able=true,$year=0)
	{
		$CI = &get_instance();
		$field = explode('.',$step_field);
		$html = '';
		$row = $CI->db->where(['id_type'=>$field[0],'step_field_id'=>$field[1]])->get('biz_steps_config')->row_array();
		if (!empty($row)) {
			$value = 0;
			$checked = '';
			$title = '';
			$disabled = '';
			$pass = true;
			if($able !== true){
				$title = lang('无权限填写');
				$disabled = 'disabled';
			}

			$shipment_id = 0;
			$consol_id = 0;
			if($table=="biz_shipment"){
				$shipment_id = $id;
				if($year != 0){
					$shipment = $CI->db->where('id',$id)->get('biz_shipment_'.$year)->row_array();
				}else{
					$shipment = $CI->db->where('id',$id)->get('biz_shipment')->row_array();
				}
				if(!empty($shipment)) $consol_id = $shipment['consol_id'];
			}
			if($table=="biz_consol"){
				$consol_id = $id;
				if($year != 0){
					$shipment = $CI->db->where('consol_id',$id)->get('biz_shipment_'.$year)->row_array();
				}else{
					$shipment = $CI->db->where('consol_id',$id)->get('biz_shipment')->row_array();
				}
				if(!empty($shipment)) $shipment_id = $shipment['id'];
				if(empty($shipment) && !empty($row['shipment_where'])) $pass = false;
			}

			if($row['id_type'] == 'biz_shipment' && !empty($shipment)){
				$value = $shipment[$field[1]];
			}
			if($row['id_type'] == 'biz_consol'){
				if($year != 0){
					$consol = $CI->db->where('id',$consol_id)->get('biz_consol_'.$year)->row_array();
				}else{
					$consol = $CI->db->where('id',$consol_id)->get('biz_consol')->row_array();
				}
				if(!empty($consol)){
					$value = $consol[$field[1]];
				}
			}
			if($value=='0') $disabled = "";
			// 在shipment页面，调取consol的节点，则仅显示，不能勾选
			if($table == 'biz_shipment' && $row['id_type']=='biz_consol'){
				$title = lang('在consol填写');
				$disabled = 'disabled';
			}
			if($table == 'biz_consol' && $row['id_type']=='biz_shipment' && $row['step_field_id']!='pre_alert'){
				$title = lang('在shipment填写');
				$disabled = 'disabled';
			}
//			if($table == 'biz_shipment' && $row['step_field_id']=='flag_ata'){
//				$title = lang('在consol填写');
//				$disabled = 'disabled';
//			}
//			if($table == 'biz_shipment' && $row['step_field_id']=='chuanyiqihang'){
//				$title = lang('在consol填写');
//				$disabled = 'disabled';
//			}
			if($table == 'biz_shipment' && $row['step_field_id']=='pre_alert'){
				$title = lang('在consol填写');
				$disabled = 'disabled';
			}

			if(strlen($value) >2){
				$checked = 'checked';
			}
			$shipment = [1];
//			if(!empty($row['shipment_where']) && $pass){
//				if($year != 0){
//					$shipment = $CI->db->query("select id from biz_shipment_{$year} where {$row['shipment_where']} and id = {$shipment_id}")->row_array();
//				}else{
//					$shipment = $CI->db->query("select id from biz_shipment where {$row['shipment_where']} and id = {$shipment_id}")->row_array();
//				}
//			}

			$html .= "<span style='display: flex;flex-direction: column;' title='{$value} {$title}'><span>".lang($row['step_name']).":";
			if (empty($shipment) || !$pass) {
				$html .= "<span style='color:black;background-color:#ccc;font-weight:bold;font-size:16px;border: 1px solid gray;margin:3px;'>&nbsp;X&nbsp;</span>";
			} else {
				$html .= "<input $disabled type='checkbox' class='status' id='{$field[1]}' $checked value='1' style='width:18px;height:18px;'>";
			}
			$html .= "</span>";
			$html .= "<span>";
			if ($value != '0' && strlen($value) >2) {
				$html .= date('m-d H', strtotime($value)) . "h&emsp;";
			} else {
				$html .= "&emsp;&emsp;&emsp;&emsp;";
			}
			$html .= "</span>";
			$html .= "</span>";
		}
		return $html;
	}
}

/**
 * 生成一条最新的通知
 * @param string $txt 模板内容
 * @param string $href 链接
 * @param string $notice_user 通知用户 ,为ID串 不填写默认当前用户
 * @param string $notice_group 通知组 为组code串
 * @param string $start 开始时间 默认当天
 * @param string $end 结束时间 默认当天 + 1 天
 * @param string $table_name 表名
 * @param string $id_no 对应表的ID
 * @param int $status 默认正常
 */
function add_notice($txt = '', $href = '', $notice_user = '', $notice_group = '', $start = "", $end = "", $table_name = '', $id_no = '', $lock_screen = 0, $lock_screen_datetime = '', $code = '', $status = 0, $delay_num = 5){
	$CI = & get_instance();
	if (empty($notice_user) && empty($notice_group)) return false;
	if($start == '') $start = date('Y-m-d H:i:s');
	if($end == '') $end = date('Y-m-d H:i:s', time() + 24 + 3600);
	if(is_array($href)){
		$href_arr = $href;
	}else{
		$href_arr = array($href);
	}

	//提交了用户就创建单条通知，否则为部门下的每个用户都创建一条通知
	if(!empty($notice_user)) {
		$notice_user = (array)$notice_user;
	}else{
		$groups = Model('bsc_group_model')->get_child_group($notice_group);
		$groups = !empty($groups) ? array_column($groups, 'group_code') : [$notice_group];
		$user = $CI->db->where_in('group', $groups)->get('bsc_user')->result_array();
		$notice_user = array_column($user, 'id');
	}

	//第一个为当前通知的跳转页面
	$href = $href_arr[0];
	$more_href = json_encode($href_arr);

	$bsc_notice_model = Model('bsc_notice_model');
	$notice = $bsc_notice_model->get_where_one("txt = '{$txt}' and status = 0 and table_name = '{$table_name}' and id_no = '{$id_no}'");
	if(!empty($notice)) return;
	if(empty($lock_screen_datetime)) $lock_screen_datetime = $end;

	//循环为创建一条消息通知
	foreach ($notice_user as $user_id) {
		$notice_data = array(
			'txt' => $txt,
			'href' => $href,
			'more_href' => $more_href,
			'notice_user' => $user_id,
			'notice_user_group' => getUserField($user_id, 'group'),
			'start' => $start,
			'end' => $end,
			'table_name' => $table_name,
			'id_no' => $id_no,
			'code' => $code,
			'status' => $status,
			'lock_screen' => $lock_screen,
			'lock_screen_datetime' => $lock_screen_datetime,
			'delay_num' => $delay_num,
		);
		$id = $bsc_notice_model->save($notice_data);

		// record the log
		$log_data = array();
		$log_data["table_name"] = "bsc_notice";
		$log_data["key"] = $id;
		$log_data["action"] = "insert";
		$log_data["value"] = json_encode($notice_data);
		log_rcd($log_data);
	}
}

if(!function_exists("stop_notice")){
	function stop_notice($table_name, $id_no, $code = ""){
		$where = array();

		$where[] = "table_name = '{$table_name}'";
		$where[] = "id_no = '{$id_no}'";
		$where[] = "code = '{$code}'";

		$where_str = join(' and ', $where);
		$data = array(
			'status' => 1,
			'lock_screen' => 0,
		);

		Model('bsc_notice_model')->update_where($where_str, $data);
	}
}

if(!function_exists('template_link')){
    /**
     * 根据参数, 生成对应的模板链接
     * @param $data_table
     * @param $id
     * @param int $template_id
     * @param string $text
     * @return string
     */
    function template_link($data_table, $id, $template_id = 0, $text = "", $export_url = ''){
        $CI = &get_instance();

        //这里如果有晚截标记，但是没输入晚截费，则不让下载。
        $m_model = Model('m_model');
        //当shipment时, 如果 晚截后未输入 晚截费, 那么不让看到下载
        if($data_table == "shipment"){
            $sql = "select id,late_operator_si from biz_consol where id = (select consol_id from biz_shipment where id={$id}) and biz_type='export' and trans_mode ='FCL' and trans_origin = 'CNSHA' and trans_ATD >= '2022-09-05' and customer_booking != 1";
            $cons = $m_model->query_one($sql);
            if(!empty($cons) && $cons["late_operator_si"]>'0000-00-00 00:00:00'){
                $sql ="select count(1) as n from biz_bill where id_type='consol' and id_no = '{$cons['id']}' and charge_code = 'WJF2' and `type`='cost'";
                $billnum = $m_model->query_one($sql);
                if($billnum['n']==0) return "<span style='color:red;'>该票consol有晚截标记，但是未输入‘晚截费’，请先联系操作、截单，在consol补充输入晚截费！</span>";
            }
        }
        if($data_table == "consol"){
            $sql = "select id,late_operator_si from biz_consol where id = {$id} and trans_origin = 'CNSHA' and trans_ATD >= '2022-09-05' and customer_booking != 1";
            $cons = $m_model->query_one($sql);
            if(!empty($cons) && $cons["late_operator_si"]>'0000-00-00 00:00:00'){
                $sql ="select count(1) as n from biz_bill where id_type='consol' and id_no = '{$cons['id']}' and charge_code = 'WJF2' and `type`='cost'";
                $billnum = $m_model->query_one($sql);
                if($billnum['n']==0) return "<span style='color:red;'>该票consol有晚截标记，但是未输入‘晚截费’，请先联系操作、截单，在consol补充输入晚截费！</span>";
            }
        }

        $special = false;
        if($data_table == "shipment"){
            $shipment = Model('biz_shipment_model')->get_by_id($id);
            $bsc_dict = $CI->db->where(['catalog'=>'consignee_special_flag'])->get('bsc_dict')->result_array();
            if(in_array(substr($shipment['trans_destination'],0,2),array_column($bsc_dict,'value'))){
                $special = true;
            }
        }
        $userId = get_session('id');
        $userBsc = Model('bsc_user_model')->get_by_id($userId);

        $userRole = Model('bsc_user_role_model')->get_user_role(array('template_text'));
        $userBsc = array_merge($userBsc, $userRole);

        $limit = '';
        $where = array();
        $temp_ids = array(0);
        $userBsc_template = explode(",", (isset($userBsc['template_text']) ? $userBsc['template_text'] : '') );
        foreach ($userBsc_template as $ukey => $uval){
            //判断当前data_table是否存在，存在
            if($uval == $data_table){
                $limit = 'all';
                break;
            }else if( strpos($uval, $data_table . '.') !== false  ){
                $arr_temp =  explode(".", $uval);
                $temp_id = $arr_temp[1];
                $temp_ids[] = $temp_id;
            }
        }
        //2022-04-19 这里改为了只显示没设置类型的模板, 目前只有个B/L 提单不会显示
        //2022-04-21 HOST 聚翰 和 精心的全开放普通版
        $where[] = "(bsc_template.type = '')";
        $where[] = "is_delete = 0";
        $where[] = "data_table = '{$data_table}'";
        if(ctype_digit($template_id)){
            $where[] = "id = '{$template_id}'";
        }else{
            $where[] = "code = '{$template_id}'";
        }
        if(!is_admin()){
            if($limit != 'all'){
                $where[] = 'id in (' . join(',', $temp_ids) . ')';
            }
            $where[] = "(FIND_IN_SET('{$userBsc['group']}', groups) or groups = '')";
        }

        $where = join(' and ', $where);
        $template = Model('bsc_template_model')->get_where_one($where);
        if(empty($template)) return "";

        if($text === '') $text = $template['file_name'];

        if(substr($template['file_name'],0,4)=='HOST' && $special){
            $template['file_name'] .= "<span style='color: red'>【特殊提单国家】</span>";
        }

        //获取模板是否设置匹配条件
//        $sql = "select * from bsc_template_match where template_id = {$template['id']}";
//        $match = $m_model->query_one($sql);
//        if(sizeof($match) > 0) {
//            //存在匹配条件时
//            if(empty($match["sql"])){
//                $where2=$match['id_field']."='".$match['id_value']."'";
//            }else{
//                $where2=$match['sql'];
//            }
//
//            $sql = "select * from {$match['id_type']} where id={$id} and {$where2}";
//            $check_row = $m_model->query_one($sql);
//            if(empty($check_row)) return "";
//        }

        //如果空的, 这里 直接跳转到 下载, 有参数先跳转到参数
        $file_template = explode('.', $template['file_template']);
        if(empty($export_url)){
            $export_url = '/export/export_word';
            if(end($file_template) !== 'docx'){
                $export_url = '/export/export_excel';
            }
        }
        if (empty($template['parameter'])) {
            $url = "{$export_url}?template_id={$template['id']}&data_table={$template['data_table']}&id={$id}";
        } else {
            $url = "/bsc_template/set_parameter?url={$export_url}&template_id={$template['id']}&data_table={$template['data_table']}&id={$id}&parameter={$template['parameter']}";
        }
        return "<script>function template_a_click(e) {
            var url = $(e).prop('href');
            if(url.indexOf('/bsc_template/set_parameter') != -1){
                var this_window = $('<div/>').appendTo('body'); 
                this_window.window({
                    title:'" . lang('窗口') . "',
                    width:500,
                    height:300,
                    href:url,
                    modal:true
                });
                return false;
            }else{
                return true;
            }
}</script><a href=\"{$url}\" target=\"_blank\" onclick=\"return template_a_click(this);\">{$text}</a>";
    }
}

/**
 * 生成临时文件
 */
if(!function_exists('temp_upload_file')){
    function temp_upload_file($file_name, $file_content){
        $path = date('Ymd');
        $new_dir = dirname(BASEPATH) . '/upload/temp/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $new_path = $new_dir . $file_name;
        $new_path = str_replace('\\', DIRECTORY_SEPARATOR, $new_path);
        $new_path = str_replace('/', DIRECTORY_SEPARATOR, $new_path);
        file_put_contents($new_path, $file_content);

        return $new_path;
    }
}

if(!function_exists('get_table_name')){
	/**
	 * @param string $table_name
	 * @param string $year
	 * @return string
	 */
	function get_table_name($table_name = ''){
		$this_table_name = $table_name;

		//检测当前的年份参数
		$year = get_system_year();

		//如果没传入, 默认为今年
		$time = time();
		//如果是今年, 那么 直接返回表名
		if($year == date('Y', $time)) return $this_table_name;
		//不是的话 ,获取旧表的
		return $this_table_name . '_' . $year;
	}
}
if(!function_exists('get_system_year')){
	/**
	 * @param string $table_name
	 * @param string $year
	 * @return string
	 */
	function get_system_year(){
		//检测当前的年份参数
		$year = get_session('system_year');

		//如果没传入, 默认为今年
		$time = time();
		if(empty($year)) $year = date('Y', $time);
		return $year;
	}
}

