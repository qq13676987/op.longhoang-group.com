<?php

/**
 * ESL 报文相关
 */
class bsc_message_sinotrans extends Common_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bsc_message_model');
        $this->model = $this->bsc_message_model;
    }

    private function so_checking($consol, $shipments){
        $consol_check = array('job_no', 'payment', 'booking_ETD', 'trans_carrier', 'trans_origin', 'trans_discharge', 'trans_destination', 'shipper_company',
            'consignee_company', 'notify_company', 'box_info', 'trans_mode', 'container_owner',
            //shipment的consol现在都有了
            'trans_term', 'release_type','goods_type', 'good_outers_unit', 'good_outers', 'good_volume', 'good_weight'
        );
        $shipment_check = array('trans_term', 'release_type','goods_type', 'good_outers_unit', 'good_outers', 'good_volume', 'good_weight');

        foreach ($consol_check as $val){
            if(empty($consol[$val])){
                return $val;
            }
        }

//        foreach ($shipment_check as $val){
//            if(empty($shipments[0][$val])){
//                return $val;
//            }
//        }
        if($consol['dingcangwancheng'] == '0' || $consol['dingcangwancheng'] == '0000-00-00 00:00:00') return 'SO必须勾选订舱勾后才能生成';
        //如果是TSL 和RCL 中文品名必填
        if(in_array($consol['trans_carrier'], array('DXHYYX02', 'HHXYYX01'))){
            if(empty($consol['description_cn'])){
                return 'description_cn';
            }
        }

        return true;
    }
    
    private function message_so_by_carrier($carrier, &$all_data){
        if($carrier == 'BEN'){
            if(strlen($all_data['consol']['hs_code']) != 6){
                return 'hs_code 只能填写6位';
            }
        }
        if($carrier !== 'SMLINE'){
            if(empty($all_data['consol']['hs_code'])){
                return 'hs_code';
            }
        }else{
            $all_data['consol']['hs_code'] = '';
        }
        return false;
    }
    
    /**
     * 生成报文(外运) SO
     * @param $consol_id
     * @param $file_function
     */
    public function message_so($consol_id, $file_function = 9){

        $this->load->model('biz_consol_model');
        $this->load->model('biz_shipment_model');
        $this->load->model('biz_port_model');
        $this->load->model('biz_container_model');
        $this->load->model('bsc_dict_model');
        $this->load->model('bsc_user_model');
        $this->load->model('m_model');

        $message = array(
            'XML' => array(
                'HEAD' => array(
                    'SENDER_CODE' => '',
                    'SENDER_ORG_ID' => '',
                    'MESSAGE_TYPE' => '',
                    'RECIVER_CODE' => '',
                    'RECIVER_ORG_ID' => '',
                    'FILE_FUNCTION' => '',
                    'FILE_CREATE_TIME' => '',
                    'MESSAGE_ID' => '',
                    'VERSION' => '',
                ),
                'DECLARATIONS' => array(
                    'DECLARATION' => array(
                        'BOOKING' => array(
                            'BOOKING_NO' => '',
                            'BOOKING_NO_EXT1' => '',
                            'BL_NO' => '',
                            'FREIGHT_CLAUSE_CODE' => '',
                            'FREIGHT_CLAUSE' => '',
                            'ISSUE_PARTY_CODE' => '',
                            'ISSUE_PARTY' => '',
                            'APPLICANT' => '',
                            'FOB_BK_PARTY' => '',
                            'BL_TRANSHIP_ID' => '',
                            'BATCH_ID' => '',
                            'SHIPMENT_DATE' => '',
                            'EXPIRY_DATE' => '',
                            'CHARGE_TYPE' => '',
                            'SC_NO' => '',
                            'BOND_NO' => '',
                            'SLOT_CHARTER_ID' => '',
                            'AMS_CODE' => '',
                            'MASTER_BL_NO' => '',
                            'SVC_REQ' => '',
                            'GOODS_OWNER_CODE' => '',
                            'GOODS_OWNER' => '',
                            'CANVASSER' => '',
                            'BIZ_LEVEL' => '',
                            'DUTY_FREE_FLAG' => '',
                            'LM_ID' => '',
                            'PO_NO' => '',
                            'SO_NO' => '',
                            'REAL_NAME' => '',
                            'USER_EMAIL' => '',
                            'USER_FAX' => '',
                            'USER_MOBILE' => '',
                            'USER_TEL' => '',
                        ),
                        'BL' => array(
                            'BL_VISE_TYPE' => '',
                            'BL_ISSUE_MODE_CODE' => '',
                            'BL_ISSUE_MODE_NAME' => '',
                            'BL_ISSUE_PLACE_CODE' => '',
                            'BL_ISSUE_PLACE' => '',
                            'ISSUE_DATE' => '',
                            'BL_COPY_NUM' => '',
                            'PREPAID_OR_COLLECT' => '',
                            'PAY_PLACE_CODE' => '',
                            'PAY_PLACE_NAME' => '',
                            'TOTAL_CARGO_NUM' => '',
                            'TOTAL_CARGO_WEIGHT' => '',
                            'TOTAL_CARGO_VOL' => '',
                            'TOTAL_CNT_NUM' => '',
                            'HBL_NO' => '',
                            'SCAC_CODE' => '',
                            'SURCHARGE_REMARK' => '',
                            'FREIGHT_CHARGE' => '',
                            'CARBON_COUNT' => '',
                            'SHOW_DIS_AGENT' => '',
                            'SHOW_OCEAN_FREIGHT' => '',
                            'HBL_FLAG' => '',
                            'SPLIT_TYPE' => '',
                            'SPLIT_DESC' => '',
                            'AGREEMENT_TYPE' => '',
                            'DESTINATION_PROPERTY' => '',
                            'DESTINATION_TRAFFIC' => '',
                            'RECEIPT_TRAFFIC' => '',
                            'IS_AMS' => '',
                            'DEST_ZIP_CODE' => '',
                            'CNT_PORT' => '',
                            'CNT_CARGO_STATUS' => '',
                            'CUSTOM_STATUS' => '',
                        ),
                        'BL_REFERENCE' => array(
                            'REFERENCE_TYPE' => '',
                            'REFERENCE_CODE' => '',
                            'CTN_NO' => '',
                        ),
                        'REMARKS' => array(
                            'TYPE' => '',
                            'TEXT' => '',
                        ),
                        'VESSEL' => array(
                            'VESSEL_CODE' => '',
                            'VESSEL_NAME' => '',
                            'VOYAGE' => '',
                            'VESSEL_CALL' => '',
                            'VESSEL_IMO' => '',
                            'SHIPPING_LINE_CODE' => '',
                            'SHIPPING_LINE' => '',
                            'BL_CARRY_CODE' => '',
                            'BL_CARRY' => '',
                            'REQUESTED_SHIPMENT_DATE' => '',
                            'TRADE_CODE' => '',
                            'TRADE' => '',
                            'PRE_VESSEL_CODE' => '',
                            'PRE_VESSEL' => '',
                            'PRE_VOYAGE' => '',
                        ),
                        'PORTS' => array(
                            'RECEIPT_PLACE_CODE' => '',
                            'RECEIPT_PLACE' => '',
                            'LOAD_PORT_CODE' => '',
                            'LOAD_PORT' => '',
                            'DISCHARGE_PORT_CODE' => '',
                            'DISCHARGE_PORT' => '',
                            'TRANSFER_PORT_CODE' => '',
                            'TRANSFER_PORT' => '',
                            'DELIVERY_PLACE_CODE' => '',
                            'DELIVERY_PLACE' => '',
                            'FINAL_DESTINATION_CODE' => '',
                            'FINAL_DESTINATION' => '',
                            'TRANSIT_PLACE_CODE' => '',
                            'TRANSIT_PLACE' => '',
                            'TRANSIT_DESTI_CODE' => '',
                            'TRANSIT_DESTI' => '',
                        ),
                        'COMPANY' => array(
                            'TYPE' => '',
                            'COMPANY_CODE' => '',
                            'COMPANY_NAME' => '',
                            'STREET_AND_NUMBER' => '',
                            'CITY_CODE' => '',
                            'CITY_NAME' => '',
                            'COUNTRY_SUB_ENTITY' => '',
                            'COUNTRY_SUB_ENTITY_NAME' => '',
                            'POSTCODE' => '',
                            'COUNTRY_CODE' => '',
                            'TEL' => '',
                            'EMAIL' => '',
                            'FAX' => '',
                            'SPECIFIC_CONTACT_NAME' => '',
                            'SPECIFIC_CONTACT_TEL' => '',
                            'SPECIFIC_CONTACT_EMAIL' => '',
                            'SPECIFIC_CONTACT_FAX' => '',
                            'COMPANY_DESCRIPTION' => '',
                            'COM_CARRIER_CODE' => '',
                            'AEO_CODE' => '',
                        ),
                        'PRECONTAINER' => array(
                            'CTN_SIZE_TYPE' => '',
                            'CTN_NUMBERS' => '',
                            'CTN_STATUS' => '',
                            'MASTER_LCL_NO' => '',
                            'MODE_OF_STUFFING' => '',
                            'VANNING_DEPOT_CODE' => '',
                            'VANNING_DEPOT' => '',
                            'CONTAINER_SOC' => '',
                            'CONTAINER_SUPPLIER_TYPE_CODE' => '',
                            'CONSOLIDATOR_CODE' => '',
                            'CONSOLIDATOR' => '',
                        ),
                        'CARGO' => array(
                            'CARGO_SEQUENCE_NO' => '',
                            'TARIFF_CODE_NUMBER_CUSTOMS' => '',
                            'CARGO_KIND' => '',
                            'CARGO_PKGS_KIND_CODE' => '',
                            'CARGO_PKGS_KIND_NAME' => '',
                            'CARGO_PKGS_NUMBER' => '',
                            'CARGO_MEASUREMENT' => '',
                            'CARGO_GROSS_WEIGHT' => '',
                            'CARGO_NET_WEIGHT' => '',
                            'QUARANTINE_CODING_CODE' => '',
                            'QUARANTINE_CODING' => '',
                            'CONSIGUMENT_VALUE' => '',
                            'CUSTOMS_PROCEDURE_CODE' => '',
                            'ORIGIN_COUNTRY_CODE' => '',
                            'UNI_CONSIGNMENT_REF_NO' => '',
                            'IS_NORMAL_FREEZE' => '',
                            'DANGEROUS_REEFER_OOG' => array(
                                'CLASS' => '',
                                'DANGEROUS_GRADE' => '',
                                'PAGE' => '',
                                'UNDG_NO' => '',
                                'LABEL' => '',
                                'FLASH_POINT' => '',
                                'EMS_NO' => '',
                                'MFAG_NO' => '',
                                'MPT_MARINE_POLLUTANT' => '',
                                'REEFER_VENTILATION_FLUX' => '',
                                'TEMPERATURE_SETTING' => '',
                                'MIN_TEMPERATURE' => '',
                                'MAX_TEMPERATURE' => '',
                                'OVER_LENGTH_FRONT' => '',
                                'OVER_LENGTH_BACK' => '',
                                'OVER_WIDTH_LEFT' => '',
                                'OVER_WIDTH_RIGHT' => '',
                                'OVER_HEIGHT' => '',
                                'DANGEROUS_GOODS_CONTACT' => '',
                                'COMMUMICATION_NUMBER_TE' => '',
                                'COMMUMICATION_NUMBER_EM' => '',
                                'COMMUMICATION_NUMBER_FX' => '',
                                'HUMIDITY' => '',
                                'TECH_NAME' => '',
                                'DANGER_PKGS_TYPE' => '',
                                'PROPER_SHIPPING_NAME' => '',
                            ),
                            'MARKS' => '',
                            'CARGO_DESCRIPTION' => '',
                            'CARGO_DESCRIPTION_CN' => '',
                            'MAIN_NAME' => '',
                            'EMERGENCY_CONTACT' => '',
                        ),

                    ),
                ),
            )
        );

        //                        'OPTION_PORTS' => array(
        //                            'OPT_DISCH_PORT_CODE' => '',
        //                            'OPT_DISCH_PORT' => '',
        //                            'OPT_DELIVERY_PLACE_CODE' => '',
        //                            'OPT_DELIVERY_PLACE' => '',
        //                        ),
        //'CONTAINER' => array(
        //                            'CTN_NO' => '',
        //                            'CTN_TYPE_SIZE_CODE' => '',
        //                            'CTN_TYPE_SIZE' => '',
        //                            'CTN_STATUS_CODE' => '',
        //                            'CTN_STATUS' => '',
        //                            'CTN_PKGS_NUM' => '',
        //                            'CARGO_NET_WEIGHT' => '',
        //                            'CARGO_TARE_WEIGHT' => '',
        //                            'CTN_CARGO_MEASUREMENT' => '',
        //                            'CONTAINER_SUPPLIER_TYPE_CODE' => '',
        //                            'NCL_NO' => '',
        //                            'CTN_REMARK' => '',
        //                            'CTN_E_GOODS' => '',
        //                            'CTN_C_GOODS' => '',
        //                            'PKG_KIND_CODE' => '',
        //                            'PKG_KIND' => '',
        //                            'CONTAINER_SOC' => '',
        //                            'VGM' => array(
        //                                'VGM_WEIGHT' => '',
        //                                'VGM_VERIFY_TYPE' => '',
        //                                'VGM_VERIFY_DATE' => '',
        //                                'VGM_SIGNATURE' => '',
        //                                'VGM_CERTIFICATE_NO' => '',
        //                            ),
        //                            'COMPANY' => array(
        //                                'TYPE' => '',
        //                                'COMPANY_CODE' => '',
        //                                'COMPANY_NAME' => '',
        //                                'STREET_AND_NUMBER' => '',
        //                                'SPECIFIC_CONTACT_NAME' => '',
        //                                'SPECIFIC_CONTACT_TEL' => '',
        //                                'SPECIFIC_CONTACT_EMAIL' => '',
        //                                'VGM_PERSON' => '',
        //                                'VGM_PERSON_SIG' => '',
        //                            ),
        //                            'SEAL' => array(
        //                                'SEAL_KIND_CODE' => '',
        //                                'SEAL_AGENCY_CODE' => '',
        //                                'SEAL_AGENCY' => '',
        //                                'SEAL_NO' => '',
        //                            ),
        //                        ),
        //                        'CTN_CARGO_RELATION' => array(
        //                            'CTN_NO' => '',
        //                            'CARGO_SEQUENCE_NO' => '',
        //                            'QUANTITY' => '',
        //                            'MEASUREMENT' => '',
        //                            'NET_WEIGHT' => '',
        //                            'GROSS_WEIGHT' => '',
        //                            'MEMO' => '',
        //                        ),
        $result = array('code' => 1, 'msg' => 'error');
        //获取或转换对应的数据--start
        $consol = $this->biz_consol_model->get_one('id', $consol_id);
        $shipments = $this->biz_shipment_model->no_role_get("consol_id =  '" . $consol['id'] . "'");
        $checking = $this->so_checking($consol, $shipments);
        if($checking !== true){
            $result['msg'] = lang($checking);
            echo json_encode($result);
            return;
        }
        $consol_admin_field = array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document');
        $users = array();
        foreach ($consol_admin_field as $val){
            $users[$val] = $this->bsc_user_model->get_by_id($consol[$val . '_id']);
        }

        $this_dict = $this->bsc_dict_model->get_by_ext1_one('name', $consol['payment'], 'payment_edi','sinotrans');
        if(empty($this_dict)){
            $result['msg'] = lang('payment');
            echo json_encode($result);
            return;
        }
        $payment = $this_dict['value'];

        $this_dict = $this->bsc_dict_model->get_by_ext1_one('name', $consol['trans_carrier'], 'trans_carrier_edi','sinotrans');
        if(empty($this_dict)){
            $result['msg'] = lang('trans_carrier');
            echo json_encode($result);
            return;
        }
        $trans_carrier = $this_dict['value'];

        $this_dict = $this->bsc_dict_model->get_by_ext1_one('name', $consol['trans_mode'], 'trans_mode_edi','sinotrans');
        if(empty($this_dict)){
            $result['msg'] = lang('trans_mode');
            echo json_encode($result);
            return;
        }
        $trans_mode = $this_dict['value'];

        $this_dict = $this->bsc_dict_model->get_by_ext1_one('name', $consol['container_owner'], 'container_owner_edi','sinotrans');
        if(empty($this_dict)){
            $result['msg'] = lang('trans_mode');
            echo json_encode($result);
            return;
        }
        $container_owner = $this_dict['value'];

        //2022-10-13 改为取值consol
//        $this_dict = $this->bsc_dict_model->get_by_ext1_one('name', $shipments[0]['goods_type'], 'goods_type_edi','sinotrans');
        $this_dict = $this->bsc_dict_model->get_by_ext1_one('name', $consol['goods_type'], 'goods_type_edi','sinotrans');
        if(empty($this_dict)){
            $result['msg'] = lang('goods_type');
            echo json_encode($result);
            return;
        }
        $goods_type = $this_dict['value'];

        //2022-10-13 改为取值consol
//        $this_dict = $this->bsc_dict_model->get_by_ext1_one('name', $shipments[0]['good_outers_unit'], 'packing_unit_edi','sinotrans');
        $this_dict = $this->bsc_dict_model->get_by_ext1_one('name', $consol['good_outers_unit'], 'packing_unit_edi','sinotrans');
        if(empty($this_dict)){
            $result['msg'] = lang('good_outers_unit');
            echo json_encode($result);
            return;
        }
        $good_outers_unit_code = $this_dict['value'];

        //1: 正本
        //2: 电放
        //3: SEAWAYBILL
        //4: 其他"
//        if($shipments[0]['release_type'] == 'OBL'){//ORI
//            $release_type = 1;
//        }else if($shipments[0]['release_type'] == 'TER'){//SUR
//            $release_type = 2;
//        }else if($shipments[0]['release_type'] == 'SWB'){//
//            $release_type = 3;
//        }else{
//            $release_type = 4;
//        }
        //2022-10-13 改为取值consol
        if($consol['release_type'] == 'OBL'){//ORI
            $release_type = 1;
        }else if($consol['release_type'] == 'TER'){//SUR
            $release_type = 2;
        }else if($consol['release_type'] == 'SWB'){//
            $release_type = 3;
        }else{
            $release_type = 4;
        }
        $user_email = join(';', array_filter(array_column($users, 'email')));
        //获取或转换对应的数据--end
        //2022-10-13 改为取值consol
//        $trans_term = $shipments[0]['trans_term'];
//        $good_outers = $shipments[0]['good_outers'];//包装件数
//        $good_volume = $shipments[0]['good_volume'];//货物体积，单位:立方米，小数点后三位
//        $good_weight = $shipments[0]['good_weight'];//毛重，单位:千克；小数点后三位

        $trans_term = $consol['trans_term'];
        $good_outers = $consol['good_outers'];//包装件数
        $good_weight = $consol['good_weight'];//毛重，单位:千克；小数点后三位
        $good_volume = $consol['good_volume'];//货物体积，单位:立方米，小数点后三位

        //规则验证,根据船公司
        $all_data = array('consol' => $consol, 'shipment' => $shipments);
        $carrier_rule_checking = $this->message_so_by_carrier($trans_carrier, $all_data);
        if($carrier_rule_checking){
            $result['msg'] = lang($carrier_rule_checking);
            echo json_encode($result);
            return;
        }

        //开始填入数据
        $message['XML']['HEAD']['SENDER_CODE'] = 'JXGYLAPI';//发送方代码，请向外运索取
        $message['XML']['HEAD']['SENDER_ORG_ID'] = '83';//请固定为83
        $message['XML']['HEAD']['MESSAGE_TYPE'] = 'IFTMBF';//订舱：IFTMBF，SI确认/补料：IFTMIN，舱单：IFTCPS，VGM：IFTVGM
        $message['XML']['HEAD']['FILE_FUNCTION'] = $file_function;//文件功能, 9自动，1删除，5修改(目前仅用于舱单)
        $message['XML']['HEAD']['FILE_CREATE_TIME'] = date('YmdHis');//创建文件日期 格式YYYYMMDDHHMMSS
        $message['XML']['HEAD']['MESSAGE_ID'] = '';//唯一标记 自定义,不重复就行
        $message['XML']['HEAD']['VERSION'] = '1.0';//版本 固定为1.0

        $message['XML']['DECLARATIONS']['DECLARATION']['BOOKING']['BOOKING_NO'] = 'JXGYL' . $consol['job_no'];// 公司内部编号
        $message['XML']['DECLARATIONS']['DECLARATION']['BOOKING']['BL_NO'] = $consol['carrier_ref'];//提单号  船公司单号
        $message['XML']['DECLARATIONS']['DECLARATION']['BOOKING']['FREIGHT_CLAUSE_CODE'] = $trans_term;//运输条款代码 (见代码表)
        $message['XML']['DECLARATIONS']['DECLARATION']['BOOKING']['SC_NO'] = $consol['AP_code'];//服务合同号(约号)
        $message['XML']['DECLARATIONS']['DECLARATION']['BOOKING']['REAL_NAME'] = $users['operator']['name'];//报文发送方真实联系人姓名
        $message['XML']['DECLARATIONS']['DECLARATION']['BOOKING']['USER_EMAIL'] = $user_email;//报文发送方真实联系人邮件(多个邮箱地址使用英文分号进行分隔)
        $message['XML']['DECLARATIONS']['DECLARATION']['BOOKING']['USER_FAX'] = '';//报文发送方真实联系人传真
        $message['XML']['DECLARATIONS']['DECLARATION']['BOOKING']['USER_MOBILE'] = $users['operator']['telephone'];//报文发送方真实联系人手机
        $message['XML']['DECLARATIONS']['DECLARATION']['BOOKING']['USER_TEL'] = $users['operator']['telephone'];//报文发送方真实联系人电话

        $message['XML']['DECLARATIONS']['DECLARATION']['BL']['BL_ISSUE_MODE_CODE'] = $release_type;//放单方式
        $message['XML']['DECLARATIONS']['DECLARATION']['BL']['BL_COPY_NUM'] = '3';//提单份数 1 3
        $message['XML']['DECLARATIONS']['DECLARATION']['BL']['PREPAID_OR_COLLECT'] = $payment;//付款(支付)方式代码 (见代码表)

        $message['XML']['DECLARATIONS']['DECLARATION']['REMARKS']['TYPE'] = 'BK';//备注类型(若TEXT有值，则该值非空) BK: 订舱备注 SI: 提单备注 SO: 退关备注
        $message['XML']['DECLARATIONS']['DECLARATION']['REMARKS']['TEXT'] = $consol['requirements'];//备注内容 可取订舱要求

        if(empty($consol['vessel']) || $consol['vessel'] == '-' || empty($consol['voyage'] || $consol['voyage'] == '-')){
            $consol['vessel'] = '';
            $consol['voyage'] = '';
        }
        $message['XML']['DECLARATIONS']['DECLARATION']['VESSEL']['VESSEL_NAME'] = $consol['vessel'];//船名
        $message['XML']['DECLARATIONS']['DECLARATION']['VESSEL']['VOYAGE'] = $consol['voyage'];//航次
        $message['XML']['DECLARATIONS']['DECLARATION']['VESSEL']['BL_CARRY_CODE'] = $trans_carrier;//船公司或承运人代码 在client那里加一个edi code 自编辑 或加入转换code里
        $message['XML']['DECLARATIONS']['DECLARATION']['VESSEL']['REQUESTED_SHIPMENT_DATE'] = date('Ymd', strtotime($consol['booking_ETD']));
        $message['XML']['DECLARATIONS']['DECLARATION']['VESSEL']['TRADE_CODE'] = $consol['sailing_code'];

        $message['XML']['DECLARATIONS']['DECLARATION']['PORTS']['RECEIPT_PLACE_CODE'] = $consol['trans_origin'];//收货地代码，五字码
        $message['XML']['DECLARATIONS']['DECLARATION']['PORTS']['LOAD_PORT_CODE'] = $consol['trans_origin'];//装货港代码,五字码
        $message['XML']['DECLARATIONS']['DECLARATION']['PORTS']['DISCHARGE_PORT_CODE'] = $consol['trans_discharge'];//卸货港代码,五字码
        $message['XML']['DECLARATIONS']['DECLARATION']['PORTS']['DELIVERY_PLACE_CODE'] = $consol['trans_destination'];//交货地代码,五字码

        //公司信息 可循环
        $COMPANY_message = $message['XML']['DECLARATIONS']['DECLARATION']['COMPANY'];
        $message['XML']['DECLARATIONS']['DECLARATION']['COMPANY'] = array();

        //发货人
        $this_array = $COMPANY_message;
        $this_array['TYPE'] = 'SH';//公司类型
        $this_array['COMPANY_NAME'] = $consol['shipper_company'];//公司名称
        $this_array['STREET_AND_NUMBER'] = $consol['shipper_address'];//公司地址
        $message['XML']['DECLARATIONS']['DECLARATION']['COMPANY'][] = $this_array;

        //收货人
        $this_array = $COMPANY_message;
        $this_array['TYPE'] = 'CO';//公司类型
        $this_array['COMPANY_NAME'] = $consol['consignee_company'];//公司名称
        $this_array['STREET_AND_NUMBER'] = $consol['consignee_address'];//公司地址
        $message['XML']['DECLARATIONS']['DECLARATION']['COMPANY'][] = $this_array;

        //第一通知人
        $this_array = $COMPANY_message;
        $this_array['TYPE'] = 'NO1';//公司类型
        $this_array['COMPANY_NAME'] = $consol['notify_company'];//公司名称
        $this_array['STREET_AND_NUMBER'] = $consol['notify_address'];//公司地址
        $message['XML']['DECLARATIONS']['DECLARATION']['COMPANY'][] = $this_array;

        //集装箱数据,可循环
        $box_info = json_decode($consol['box_info'], true);
        $PRECONTAINER_message = $message['XML']['DECLARATIONS']['DECLARATION']['PRECONTAINER'];
        $message['XML']['DECLARATIONS']['DECLARATION']['PRECONTAINER'] = array();
        foreach ($box_info as $row){
            $this_array = $PRECONTAINER_message;
            $this_array['CTN_SIZE_TYPE'] = $row['size'];//集装箱尺寸类型
            $this_array['CTN_NUMBERS'] = $row['num'];//集装箱箱数
            $this_array['CTN_STATUS'] = $trans_mode;//运输模式 trans_mode 集装箱状态,F: 整箱 L: 拼箱 E: 空箱

            $this_array['CONTAINER_SOC'] = $container_owner;//Y=SOC N=COC
            $message['XML']['DECLARATIONS']['DECLARATION']['PRECONTAINER'][] = $this_array;
        }

        //货物信息
        $PRECONTAINER_message = $message['XML']['DECLARATIONS']['DECLARATION']['CARGO'];
        $message['XML']['DECLARATIONS']['DECLARATION']['CARGO'] = array();

        $this_array = $PRECONTAINER_message;
        $this_array['CARGO_SEQUENCE_NO'] = '1';//货物序号
        $this_array['TARIFF_CODE_NUMBER_CUSTOMS'] = $consol['hs_code'];//海关税则编号, HS Code
        $this_array['CARGO_KIND'] = $goods_type;//货类 S: 普通 R: 冷冻 D: 危险 O: 非标 既是危险品又是冷藏品，请填写""R""；冷代平，请填写""S""并<IS_NORMAL_FREEZE> 填写 ""Y""
        $this_array['CARGO_PKGS_KIND_CODE'] = $good_outers_unit_code;//包装类型代码
        $this_array['CARGO_PKGS_KIND_NAME'] = '';//包装类型名称
        $this_array['CARGO_PKGS_NUMBER'] = $good_outers;//包装件数
        $this_array['CARGO_MEASUREMENT'] = $good_volume;//货物体积，单位:立方米，小数点后三位
        $this_array['CARGO_GROSS_WEIGHT'] = $good_weight;//毛重，单位:千克；小数点后三位
        $this_array['MARKS'] = $consol['mark_nums'];//唛头
        $this_array['CARGO_DESCRIPTION'] = $consol['description'];//货物描述/品名
        $this_array['CARGO_DESCRIPTION_CN'] = $consol['description_cn'];//中文货物描述/品名
        $message['XML']['DECLARATIONS']['DECLARATION']['CARGO'] = $this_array;
        $file_content = '<?xml version="1.0" encoding="utf-8"?>' . XML_encode($message);

        //保存文件xml
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        //ESL_EDI/日期
        $new_dir = $root_path . '/upload/message/SINOTRANS_EDI/SO/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $consol['id'] . '.xml';
        $file_path = $new_dir . $file_name;
        // if(preg_match('/[\x{4e00}-\x{9fa5}]+/u', $file_content, $matches1) === 1){// || preg_match('/[！￥……（）——·，。、；’‘、【】《》？：“”]+/', $file_content, $matches2) === 1

        //     echo json_encode(array('code' => 1, 'msg' => '存在中文或中文标点符号,生成失败'));
        //     return;
        // }
        file_put_contents($file_path, $file_content);
        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => 'SO',
            'receiver' => 'SINOTRANS',
        );
        update_message($consol_id, $message_data, 'SO');
        echo json_encode(array('code' => 0, 'msg' => '文件生成成功'));
    }
}