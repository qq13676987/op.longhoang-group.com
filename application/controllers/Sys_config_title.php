<?php

class Sys_config_title extends Common_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('sys_config_title_model');

    }

    public function index($table_name = "")
    {
        $this->load->model('sys_config_model');
        $data = array();
        $data["table_name"] = $table_name;
        $rs = $this->sys_config_model->get_one($table_name . "_table");
        // echo lastquery();
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $this->load->model('sys_config_title_model');
            $data["f"] = $this->sys_config_title_model->get_one($table_name);
            // print_r($data);
        }
        $this->load->view('head');
        $this->load->view('sys/config_title/index_view', $data);
    }

    //2022-08-01 �������
    public function table($table_name = "")
    {
        if (!is_admin()) {
            exit('不是管理员');
        }
        $data["table_name"] = $table_name;
        $this->load->view('head');
        $this->load->view('sys/config_title/table', $data);
    }

    public function get_data($table_name = ""){
        $result = array();

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 10;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : 'id';
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';


        $offset = ($page - 1) * $rows;
        $result["total"] = $this->db->where('table_name',$table_name)->count_all_results('sys_config_title');

        $rs=$this->db->where('table_name',$table_name)->limit($rows, $offset)->order_by($sort,$order)->get('sys_config_title')->result_array();


        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data($table_name = ""){
        $bl=$this->db->where('table_name',$table_name)->where('table_field',$_POST['table_field'])->get('sys_config_title')->row_array();
        if(empty($bl)){
            $data=array(
                'table_name'=>$table_name,
                'table_field'=>$_POST['table_field'],
                'field_name'=>$_POST['field_name'],
                'width'=>$_POST['width'],
                'order_by'=>$_POST['order_by'],
                'sortable'=>$_POST['sortable'],
            );
            $bl2=$this->db->insert('sys_config_title',$data);
            if($bl2){

                $id=$this->db->insert_id();
                $data["id"] = $id;
                $log_data = array();
                $log_data["table_name"] = "sys_config_title";
                $log_data["key"] = $id;
                $log_data["action"] = "insert";
                $log_data["value"] = json_encode($data);
                log_rcd($log_data);
                
                echo json_encode($data);
                return;
            }else{
                $data['isError'] = true;
                $data['msg'] = '新增失败';
                echo json_encode($data);
                return;
            }
        }else{
            $data['isError'] = true;
            $data['msg'] = '未找到table_field';
            echo json_encode($data);
            return;
        }
    }
    public function update_data($table_name = ""){
       $old=$this->db->where('id',$_POST['id'])->get('sys_config_title')->row_array();

       $bl=$this->db->where('table_name',$table_name)->where('table_field',$_POST['table_field'])->where('table_field !=',$old['table_field'])->get('sys_config_title')->row_array();

        if(empty($bl)){
            $data=array(
                'table_field'=>$_POST['table_field'],
                'field_name'=>$_POST['field_name'],
                'width'=>$_POST['width'],
                'order_by'=>$_POST['order_by'],
                'sortable'=>$_POST['sortable'],
            );
            $bl2=$this->db->where('id',$_POST['id'])->update('sys_config_title',$data);
            if($bl2){

                $data["id"] = $_POST['id'];
                $log_data = array();
                $log_data["table_name"] = "sys_config_title";
                $log_data["key"] = $_POST['id'];
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($data);
                log_rcd($log_data);

                echo json_encode($data);
                return;
            }else{
                $data['isError'] = true;
                $data['msg'] = '修改失败';
                echo json_encode($data);
                return;
            }
        }else{
            $data['isError'] = true;
            $data['msg'] = '未找到table_field';
            echo json_encode($data);
            return;
        }

    }
    public function delete_data(){
        $id = intval($_REQUEST['id']);
        $bl=$this->db->where('id', $id)->delete('sys_config_title');
        if($bl){
            echo json_encode(array('success' => true));
        }else{
            $data['isError'] = true;
            $data['msg'] = '删除失败';
            echo json_encode($data);
            return;
        }

    }

    /**
     * 用户配置
     * 2022-10-27
     */
    public function index_user(){
        $table_name = getValue('table_name', '');
        $view_name = getValue('view_name', '');
        $data = array();
        $data['table_name'] = $table_name;
        $data['view_name'] = $view_name;

        $this->load->view("head");
        $this->load->view("/sys/config_title/user/index_user_view", $data);
    }

    /**
     * 获取基础表数据
     */
    public function get_base_data(){
        $table_name = getValue('table_name', 'no_table');
        $view_name = getValue('view_name', 'no_view');
        $ext = array();

        $where = array();

        $where[] = "table_name = '{$table_name}'";
        $where[] = "view_name = '{$view_name}'";

        $where_str = join(' and ', $where);

        $this->db->select("sys_config_title_base.id as base_id,sys_config_title_base.width,sys_config_title_base.table_name,sys_config_title_base.title,sys_config_title_view.id as title_id");
        $this->db->join("sys_config_title_view", "sys_config_title_view.title_base_id = sys_config_title_base.id");
        $rs = Model('sys_config_title_base_model')->get($where_str, 'sys_config_title_base.order', 'asc');
        $rows = array();

        $this->db->select('sys_config_title_user.id,sys_config_title_user.base_id');
        $config_title_user = Model("sys_config_title_model")->get_user_title($table_name, $view_name);
        $config_title_user_base_ids = array_column($config_title_user, 'base_id');
        $ext[] = $config_title_user_base_ids;

        if(empty($config_title_user_base_ids)){
            $base_id = $this->db->select('title_base_id as base_id')->where(array('view_name'=>$view_name, 'is_default'=>1))->get('sys_config_title_view')->result_array();
            $config_title_user_base_ids = array_column($base_id, 'base_id');
        }

        foreach ($rs as $row){
            //title_user的要去除
            if(in_array($row['base_id'], $config_title_user_base_ids)) continue;

            $row['title'] = lang($row['title'], array(), $row['table_name']);
            $rows[] = $row;
        }

        return jsonEcho(array('rows' => $rows, $ext));
    }

    /**
     * 获取用户的设置数据
     */
    public function get_user_data(){
        $table_name = getValue('table_name', 'no_table');
        $view_name = getValue('view_name', 'no_view');

        $ext = array();
        $this->db->select("sys_config_title_user.id,sys_config_title_base.table_name,sys_config_title_user.title_id,sys_config_title_user.base_id,sys_config_title_user.width,sys_config_title_base.title");
        $this->db->order_by('sys_config_title_user.order', 'asc');
        $rs = Model("sys_config_title_model")->get_user_title($table_name, $view_name);

        //没有自定义表头读取默认表头填充
        if (empty($rs)) {
            $where = array(
                'sys_config_title_base.table_name' => $table_name,
                'sys_config_title_view.view_name' => $view_name,
                'sys_config_title_view.is_default' => 1,
            );
            $this->db->select('sys_config_title_base.width,sys_config_title_view.id as title_id,sys_config_title_base.table_name,sys_config_title_view.title_base_id as base_id,sys_config_title_base.title ');
            $this->db->where($where)->order_by('sys_config_title_view.order', 'asc');
            $this->db->join('sys_config_title_base', 'sys_config_title_base.id = sys_config_title_view.title_base_id');
            $query = $this->db->get('sys_config_title_view');
            $rs = $query->result_array();
        }

        $rows = array();
        foreach ($rs as $row){
            $row['title'] = lang($row['title'], array(), $row['table_name']);

            $rows[] = $row;
        }

        return jsonEcho(array('rows' => $rows, $ext));
    }

    /**
     * 存储用户的抬头数据
     */
    public function save_user_config_title(){
        $rows_json = postValue('rows', '{}');
        $view_name = postValue('view_name', 'no_view');
        $table_name = postValue('table_name', 'no_table');
        $rows = json_decode($rows_json, true);
        $user_id = get_session('id');

        if(empty($rows)){
            //直接全删掉,再进行保存
            //理论上不会出现视图名重复的,如果重复的话,这里会出现多删的问题
            $this->db->query("DELETE from sys_config_title_user where user_id = {$user_id} and (select view_name from sys_config_title_view where sys_config_title_view.id = sys_config_title_user.title_id) = '{$view_name}'");

            return jsonEcho(array('code' => 0, 'msg' => '清空成功,已恢复默认配置'));
        }

        //获取旧的表配置
        $this->db->select("sys_config_title_user.title_id,sys_config_title_user.base_id,sys_config_title_user.width");
        $this->db->order_by("sys_config_title_user.order", 'asc');
        $old_rows = Model("sys_config_title_model")->get_user_title($table_name, $view_name);

        //会变动的字段
        $update_fields = array('title_id', 'base_id', 'width');

        //只有2个数组长度一样时候才进行比较
        $is_update = sizeof($old_rows) !== sizeof($rows) ? true : false;
        $a = array();
        if(!$is_update){
            foreach ($old_rows as $key => $old_row){
                //如果没有任何变动,这里直接断掉吧
                //这个不需要了,因为2个数组长度相等才会走这里
//            if(!isset($rows[$key])){
//                $is_update = true;
//                break;
//            }
                $row = $rows[$key];
                foreach ($update_fields as $update_field){
                    if(isset($old_row[$update_field]) && isset($row[$update_field])){
                        //当值不相等时,就
                        $a[] = array($old_row[$update_field], $row[$update_field], $old_row[$update_field] !== $row[$update_field]);
                        if($old_row[$update_field] !== $row[$update_field]) {
                            $is_update = true;
                            break;
                        }
                    }else{
                        //如果有个不存在就代表有变动,重新保存
                        $is_update = true;
                        break;
                    }
                }
            }
        }
        //如果什么都没变动,这里直接保存成功
        if(!$is_update) return jsonEcho(array('code' => 1, 'msg' => '保存成功'));

        //直接全删掉,再进行保存
        //理论上不会出现视图名重复的,如果重复的话,这里会出现多删的问题
        //直接过期,懒得再加删除这里直接加这个了
        cache_redis_delete("user_title_sql_data_{$table_name}_{$view_name}_" . get_session('id'));
        $this->db->query("DELETE from sys_config_title_user where user_id = {$user_id} and (select view_name from sys_config_title_view where sys_config_title_view.id = sys_config_title_user.title_id) = '{$view_name}'");

        //与新的进行比较,相同的不做处理
        $order = 1;
        $add_datas = array();
        foreach ($rows as $row){
            //胡提出, 为0的不保存
            if(empty($row['width'])) continue;
            $add_data = array(
                'user_id' => $user_id,
                'title_id' => $row['title_id'],
                'base_id' => $row['base_id'],
                'width' => $row['width'],
                'order' => $order++,
            );
            Model('sys_config_title_user_model')->save($add_data);
            $add_datas[] = $add_data;
        }
        $log_data = array();
        $log_data["table_name"] = "sys_config_title_user";
        $log_data["key"] = $view_name;//用视图作为key
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($add_datas);
        log_rcd($log_data);

        //然后根据当前内容,进行批量的
        return jsonEcho(array('code' => 0, 'msg' => '保存成功'));
    }

    /**
     * 获取当前用户的查询框相关信息
     */
    public function get_user_query_box(){
        $table_name = getValue('table_name', 'no_table');
        $view_name = getValue('view_name', 'no_view');
        $echo_type = getValue('echo_type', 'tr');
        $more_options = postValue('more_options', array());

        $result_data = array();
        $rs = get_user_title($table_name, $view_name);
        //再为空就不获取了
        if(empty($rs)) return jsonEcho(array('code' => 1, 'msg' => '未获取到相关的查询框信息'));

        $result_data['table_columns'] = array();//表头相关的变量
        foreach ($rs as $row){
            //array($row['editor'], $row['editor_config'])
            //等于0的不加入表头
            if($row['width'] > 0)$result_data['table_columns'][] = array('table_field' => $row['table_field'], 'title' => lang($row['title'], array(),  $row['table_name']), 'width' => $row['width'], 'sortable' => (int)$row['sortable'], 'column_config' => $row['column_config']);
        }
        $base_data = get_base_query_title($table_name, $view_name, false);

        $result_data['load_url'] = array(); //初始需要加载的url, 这里可以去掉,但是需要在全部相关页面上手动填充url信息
        $result_data['query_option'] = array(); //控件相关的参数,用于判断变化下拉框,输入框等使用
        //获取当前的查询配置
        $config_search = getConfigSearch($view_name,array("{$rs[0]['table_name']}.{$rs[0]['table_field']}", 'like', ''))['config_text'];
        $config_text = json_decode($config_search, true);
        $box_html = '';//页面渲染使用的查询框html代码

        if($echo_type == 'tr'){
            foreach ($base_data as $row){
                $this_key = "{$row['table_name']}.{$row['table_field']}";
                //直接整个存储了,
//                $row['editor_config'] = json_decode($row['editor_config'], true);
//                if(empty($row['editor_config'])) $row['editor_config'] = array();

                $result_data['query_option'][$this_key] = $row;
            }

            foreach ($config_text[1] as $key => $config_field){
                //这里的TR尽量别复制换行进来
                $tr_str = "<tr class='query_box'>";
                $tr_str .= "<td>" . lang('查询') . "</td>";
                $tr_str .= "<td align=\"left\"><select name=\"field[f][]\" class=\"f easyui-combobox\"  required=\"true\" data-options=\"onChange:select_f_change,\">";
                //输出相关的字段选项
                $is_find = false;
                foreach ($base_data as $row){
                    $row['editor_config'] = json_decode($row['editor_config'], true);
                    if(empty($row['editor_config'])) $row['editor_config'] = array();

                    $selected = '';
                    $opt_val = "{$row['table_name']}.{$row['table_field']}";
                    if($opt_val == $config_field[0]) {
                        $selected = 'selected';
                        $is_find = true;
                        //存在的加入默认加载数据里
                        if(isset($row['editor_config']['url'])) $result_data['load_url'][$opt_val] = $row['editor_config']['url'];
                    }

                    $tr_str .= "<option value='{$opt_val}' {$selected}>" . lang($row['title'], $row['table_name']) . "</option>";
                }
                foreach ($more_options as $more_option){
                    $selected = "";
                    $opt_val = $more_option['field'];
                    if($opt_val == $config_field[0]) {
                        $is_find = true;
                        $selected = 'selected';
                    }
                    $tr_str .= "<option value='{$opt_val}' {$selected}>" . lang($more_option['title']) . "</option>";
                }
                //如果没找到填充一个
                if(!$is_find){
                    $tr_str .= "<option value='{$config_field[0]}' selected>" . lang($config_field[0]) . "</option>";
                }
//                $tr_str .= $options;
                $tr_str .= "</select>";
                $tr_str .= "&nbsp;<select name=\"field[s][]\" class=\"s easyui-combobox\"  required=\"true\" data-options=\"value:'{$config_field[1]}'\">" .
                    "<option value=\"like\" >like</option>" .
                    "<option value=\"=\" >=</option>" .
                    "<option value=\">=\" >>=</option>" .
                    "<option value=\"<=\" ><=</option>" .
                    "<option value=\"!=\" >!=</option>" .
                    "</select>";
                !isset($config_field[2]) && $config_field[2] = '';
                $tr_str .= "&nbsp;<div class=\"this_v\">" .
                    "<input name=\"field[v][]\" class=\"v easyui-textbox\" value=\"{$config_field[2]}\">" .
                    "</div>";

                $add_button = "&nbsp;<button class=\"add_tr\" type=\"button\" onclick=\"add_tr()\">+</button>";
                $del_button = "&nbsp;<button class=\"del_tr\" type=\"button\" onclick=\"del_tr(this)\">-</button>";

                if($key == 0){
                    $this_button = $add_button;
                }else{
                    $this_button = $del_button;
                }
                $tr_str .= $this_button;

                $result_data['add_tr_str'] = str_replace($add_button, $del_button, $tr_str);
                $tr_str .= "</tr>";

                $box_html .= $tr_str;

                //用于新增方法使用
            }


        }
        $result_data['box_html'] = $box_html;//页面上加载的html

        return jsonEcho(array('code' => 0, 'msg' => '获取成功', 'data' => $result_data));
    }

    /**
     * 船卡配置版本
     */
    public function chuanka_config(){
        $table_name = 'biz_consol';
        $view_name = 'biz_consol_index';
        $data = array();
        $data['table_name'] = $table_name;
        $data['view_name'] = $view_name;

        $this->load->view("head");
        $this->load->view("/sys/config_title/chuanka_view", $data);
    }

    /**
     * 获取船卡的base数据
     * 与get_base_data 大致一致,但是对比的数据是取自的
     */
    public function get_chuanka_base_data(){
        $table_name = getValue('table_name', 'no_table');
        $view_name = getValue('view_name', 'no_view');
        $template_name = getValue('template_name', '');
        $user_id = get_session('id');
        $ext = array();

        $where = array();

        $where[] = "sys_config_title_base.table_name = '{$table_name}'";
        $where[] = "sys_config_title_view.view_name = '{$view_name}'";
        $where[] = "sys_config_title_base.sql_field != \"''\"";//只显示空字符串需要填充的这里就不获取了

        $where_str = join(' and ', $where);

        $this->db->select("sys_config_title_base.id as base_id,sys_config_title_base.width,sys_config_title_base.table_name,sys_config_title_base.title");
        $this->db->join("sys_config_title_view", "sys_config_title_view.title_base_id = sys_config_title_base.id");
        $rs = Model('sys_config_title_base_model')->get($where_str, 'sys_config_title_base.order', 'asc');
        $rows = array();

        $config_data = Model("sys_config_model")->get_one("chuanka_config_{$user_id}_{$template_name}");
        if(empty($config_data)) $config_data['config_text'] = '{}';
        $config = json_decode($config_data['config_text'], true);
        $config_title_user_base_ids = array_column($config, 'base_id');

        foreach ($rs as $row){
            //title_user的要去除
            if(in_array($row['base_id'], $config_title_user_base_ids)) continue;

            $row['title'] = lang($row['title'], $row['table_name']);
            $rows[] = $row;
        }

        return jsonEcho(array('rows' => $rows, $ext));
    }

    public function get_chuanka_user_data(){
        $table_name = getValue('table_name', 'no_table');
        $view_name = getValue('view_name', 'no_view');
        $template_name = getValue('template_name', '');
        $user_id = get_session('id');

        $config_data = Model("sys_config_model")->get_one("chuanka_config_{$user_id}_{$template_name}");
        if(empty($config_data)) $config_data['config_text'] = '{}';
        $rs = json_decode($config_data['config_text'], true);

        $rows = array();
        foreach ($rs as $row){
            $row['title'] = lang($row['title'], $row['table_name']);

            $rows[] = $row;
        }

        return jsonEcho(array('rows' => $rows));
    }
}
