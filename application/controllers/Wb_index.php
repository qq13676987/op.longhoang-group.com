<?php
//该方法用于一些外部地址的跳转
class wb_index extends Common_Controller
{
    static $wb_url = 'http://china.leagueshipping.com';

    public function __construct()
    {
        parent::__construct();
    }

    //上海的运价查询页面
    public function quotation_index(){
//        echo ('暂未开放');
//        return;
        $this->jump_china('/biz_quotation/index');
    }

    //上海的CRM模块
    public function crm_index(){
//        echo ('暂未开放');
//        return;
        $this->jump_china('/biz_crm/index');
    }

    private function jump_china($url){
        $telephone = getUserField(get_session('id'),'telephone');
        $time = time();
        $token = md5('CZ' . $url . $telephone . date('Ymd', $time));

        //http://china-ningbo.leagueshipping.com/biz_quotation/shanghaiyunjia
        //http://china.leagueshipping.com/biz_quotation_shipping_schedule/?is_display=1
        redirect(self::$wb_url . "/main/wb_login?token={$token}&telephone={$telephone}&url={$url}&time={$time}");
    }
}
