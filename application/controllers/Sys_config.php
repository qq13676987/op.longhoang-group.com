<?php

class sys_config extends Common_Controller
{

    public function __construct()
    {

        parent::__construct();

        // if ($this->session->userdata('id')==''){  
        //     redirect('main/login', 'refresh');
        // }   

        $this->load->model('sys_config_model');

    }

    /**
     * 保存查询数据
     */
    public function save_config_search()
    {
        $table = postValue('table', '');
        $count = postValue('count', '');
        $config = postValue('config', '');

        $configs = array($count, $config);
        $config_name = $table . '_search';
        $config_json = json_encode($configs);

        $this->sys_config_model->save($config_name, $config_json);

        echo json_encode(array('code' => 0, 'msg' => '保存成功'));
    }

    /**
     * 获取船卡的config数据
     */
    public function get_chuanka_config(){
        $add_empty = getValue('add_empty', 'false');

        $config_prefix = 'chuanka_config_' . get_session('id') . '_';

        $rs = $this->sys_config_model->get("config_name like '{$config_prefix}%'");

        $rows = array();
        if($add_empty === 'true'){
            $rows[] = array(
                'template_name' => '空模板',
                'template_name_value' => ''
            );
        }

        foreach ($rs as $row){
            //把前缀去掉就是模板名
            $row['template_name'] = str_replace($config_prefix, '', $row['config_name']);
            $row['template_name_value'] = $row['template_name'];
            $rows[] = $row;
        }
        return jsonEcho($rows);
    }

    public function index()
    {
        $data = array();

        $this->load->view('head');
        $this->load->view('sys/config/index_view', $data);
    }

    public function get_data()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = array();
        $where[] = 'user_id = 0';
        $where[] = "config_name not like 'DEBIT_NOTE%'";
        $where[] = "config_name not like 'USE_TYPE_NOTE%'";
        $f1 = isset($_REQUEST['f1']) ? ($_REQUEST['f1']) : '';
        $s1 = isset($_REQUEST['s1']) ? ($_REQUEST['s1']) : '';
        $v1 = isset($_REQUEST['v1']) ? ($_REQUEST['v1']) : '';
        $f2 = isset($_REQUEST['f2']) ? ($_REQUEST['f2']) : '';
        $s2 = isset($_REQUEST['s2']) ? ($_REQUEST['s2']) : '';
        $v2 = isset($_REQUEST['v2']) ? ($_REQUEST['v2']) : '';
        $f3 = isset($_REQUEST['f3']) ? ($_REQUEST['f3']) : '';
        $s3 = isset($_REQUEST['s3']) ? ($_REQUEST['s3']) : '';
        $v3 = isset($_REQUEST['v3']) ? ($_REQUEST['v3']) : '';
        if ($v1 != "") $where[] = "$f1 $s1 '$v1'";
        if ($v2 != "") $where[] = "$f2 $s2 '$v2'";
        if ($v3 != "") $where[] = "$f3 $s3 '$v3'";
        $where = join(' AND ', $where);
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->sys_config_model->total($where);
        $this->db->limit($rows, $offset);
        $this->db->select("*");
        $rs = $this->sys_config_model->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data()
    {
        $field = array('config_name', 'config_text');
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($temp != "")
                $data[$item] = $temp;
        }
        if (empty($data)) {
            $data['isError'] = true;
            $data['msg'] = '参数错误';
            echo json_encode($data);
            return;
        }
        $id = $this->sys_config_model->insert_zero($data['config_name'], $data['config_text']);
        if (!$id) {
            $data['isError'] = true;
            $data['msg'] = '新增失败';
            echo json_encode($data);
            return;
        }
        $data['id'] = $id;

        // record the log
        $log_data = array();
        $log_data["table_name"] = "sys_config";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        echo json_encode($data);
    }

    public function update_data()
    {
        $id = $_REQUEST["id"];
        $old_row = $this->sys_config_model->get_one('id', $id);

        $field = array('config_name', 'config_text');

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            $data[$item] = $temp;
        }
        $this->sys_config_model->update_zero($data['config_name'], $data['config_text']);
        $data['id'] = $id;

        // record the log
        $log_data = array();
        $log_data["table_name"] = "sys_config";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        echo json_encode($data);
    }

    public function save_config()
    {
        $text = isset($_POST['value']) ? $_POST['value'] : '';
        $config = isset($_POST['config']) ? $_POST['config'] : '';
        $user_id = get_session('id');
        $result = array('code' => 1, 'msg' => '参数错误');
        if ($config == '') {
            echo json_encode($result);
            return;
        }
        $config_row = $this->sys_config_model->get_one($config);
        $this->sys_config_model->user_id = $user_id;
        if (empty($config_row)) {
            $this->sys_config_model->insert($config, $text);
        } else {
            $this->sys_config_model->update($config, $text);
        }
        $result['code'] = 0;
        $result['msg'] = '保存成功';
        echo json_encode($result);
    }

    /**
     * 删除配置信息
     */
    public function delete_config()
    {
        $config = postValue('config', '');
        $user_id = get_session('id');
        if ($config == '') {
            return jsonEcho(array('code' => 1, 'msg' => '参数错误'));
        }
        $config_row = $this->sys_config_model->get_one($config);
        $this->sys_config_model->user_id = $user_id;
        $this->sys_config_model->mdelete($config);

        $log_data = array();
        $log_data["table_name"] = "sys_config";
        $log_data["key"] = $config_row['id'];
        $log_data["master_table_name"] = 'sys_config';
        $log_data["master_key"] = $config;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($config_row);
        log_rcd($log_data);

        return jsonEcho(array('code' => 0, 'msg' => '保存成功'));
    }
    
     /**
     * 切换系统
     */
    public function change_system(){
        $data = array();
        $sub_system = getValue('sub_system', '');
        $sub_system_arr = filter_unique_array(explode(',', $sub_system));
        $where = array();
        foreach ($sub_system_arr as $val){
            $where[] = "FIND_IN_SET(bsc_dict.value, '{$val}')";
        }
        $where = join(' and ', $where);

        $subsystem = Model("bsc_dict_model")->get_option("subsystem", $where);
        $data['rows'] = array();
        $telephone = getUserField(get_session('id'), 'telephone');
        $time = date('Ymd');
        $token = md5("HZ{$telephone}{$time}CZ");
        foreach ($subsystem as $row){
            //如果是当前网址, 就不处理
            if($row['ext1'] == base_url()){
                continue;
            }

            //name 是描述
            //value是值 没啥用
            //ext1 是 指定的分系统网址
            $row['url'] = "{$row['ext1']}{$row['ext2']}?telephone={$telephone}&token={$token}";
            //ext2 是 指定的自动登录接口
            $data['rows'][] = $row;
        }
        $this->load->view("head");
        $this->load->view("/sys/config/change_system_view", $data);
    }
}
