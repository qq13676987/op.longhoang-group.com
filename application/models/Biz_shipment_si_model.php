<?php


class biz_shipment_si_model extends CI_Model
{
    protected $table = 'biz_shipment_si';
    
    public $si_field = array('shipper_company', 'shipper_address', 'shipper_contact', 'shipper_telephone', 'shipper_email', 'consignee_company', 'consignee_address', 'consignee_contact', 'consignee_telephone', 'consignee_email', 'notify_company', 'notify_address', 'notify_contact', 'notify_telephone', 'notify_email',  'release_type', 'trans_origin', 'trans_origin_name', 'trans_discharge', 'trans_discharge_name', 'trans_destination', 'trans_destination_name', 'trans_term', 'mark_nums', 'good_outers_unit', 'document_remark', 'release_type_remark', 'description', 'hs_code', 'vgm_send');
    //2022-01-28 requirements 替换为document_remark， 2022-03-07删除'shipper_ref'这个字段
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    
    public function getSiField(){
        return $this->si_field;
    }

    public function get_one($f = "", $v = "")
    {
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_where_one($where='', $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function total($where = "")
    {
        if ($where != "") $this->db->where($where);
        $this->db->from($this->table);

        return $this->db->count_all_results('');
    }

    public function save($data = array())
    {
        $data["created_time"] = date('Y-m-d H:i:s', time());
        $data["created_by"] = $this->session->userdata('id');
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id = '', $data = array())
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id = '')
    {
        // delete container
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
}