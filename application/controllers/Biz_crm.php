<?php

class biz_crm extends Menu_Controller
{
    protected $menu_name = 'client';//菜单权限对应的名称
    protected $method_check = array(//需要设置权限的方法
        'index',
        'add',
        'edit',
        'add_data',
        'update_data',
        'delete_data',
    );

    //申请类型改为了申请状态 下面的表示数字对应的信息
    protected $apply_status_values = array('0' => '正式', '-1' => '已拒绝', '1' => '申请中', '2' => '已通过', '3' => '待审核', '-2' => '线索');

    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_client_model');
        $this->load->model('bsc_user_model');
        $this->load->model('m_model');
        $this->load->library('Client');
    }

    public function create_apprel(){
        $work = $this->db->select('id_no')->where(array('id_type' => 1, 'status'=>0))->get('biz_workflow')->result_array();
        $id_no = array_column($work, 'id_no');
        $data = $this->db->select('id, company_name, sales_id, approve_user')
            ->where(array('apply_status'=>1))
            ->where_not_in('id', $id_no)
            ->get('biz_client_crm')
            ->result_array();
        foreach ($data as $item){
            //创建新事务
            $data = array(
                'id_no'         => $item['id'],
                'id_type'       => 1,
                'current_step'  => 1,
                'operation_user'=> getUserField($item['approve_user'], 'name'),
                'operation_id'  => $item['approve_user'],
                'complete_time' => '',
                'remark'        => $item['company_name'],
                'create_id'     => $item['sales_id'],
                'create_time'   => date('Y-m-d H:i:s')
            );

            $this->db->insert('biz_workflow', $data);
        }
    }

    public function main(){
        $login_user_id = get_session('id');
        if ($login_user_id == 20001){
            $where['sales_id'] = "";
        }elseif(getUserField($login_user_id, 'm_level') == 8){
            $where['sales_id'] = get_session('id');
        }else{
            $where['is_new'] = 1;
            $where['sales_id'] = get_session('id');
        }
        $new = $this->db->where($where)->count_all_results('biz_client_crm');

        $this->load->vars('new', $new);
        $this->load->view('biz/crm/main_view');
    }

    public function index()
    {
        if (!in_array('sales', get_session('user_role')) && !in_array('customer_service', get_session('user_role')) && !in_array('oversea_cus', get_session('user_role'))) {
            echo '无销售身份';
            return;
        }
        $data = array();
        $this->load->view('head');
        $this->load->view('biz/crm/index_view', $data);
    }

    public function index_left()
    {
        $this->load->view('biz/crm/index_left');
    }

    public function dashboard()
    {
        $data = array();
        $userId = get_session('id');

        $group = join(',', $this->session->userdata('group'));
        $rangeGroup = array_filter(explode(',', $this->session->userdata('group_range')));
        $rangeGroup[] = $group;
        $rangeUser = filter_unique_array($this->session->userdata('user_range'));
        $rangeUser[] = $userId;

        // $where[] = 'created_group in (\'' . join('\',\'', $rangeGroup) . '\')';
        // $where[] = 'read_user_group is null';
        $where[] = "user_id in (" . join(',', $rangeUser) . ")";
        $where[] = "user_group in ('" . join('\',\'', $rangeGroup) . "')";

        $data['crm_user'] = getValue('crm_user', '');//$userId
        $data['crm_user_type'] = getValue('crm_user_type', '');//user
        $data['crm_date_first'] = getValue('crm_date_first', date('Y-m-d'));
        $data['crm_date_end'] = getValue('crm_date_end', date('Y-m-d'));

        //2023-03-08 由于组加入了左右值的相关代码,这里的查询进行了相关优化
        if ($data['crm_user_type'] == 'user') {
            $sales_ids_where = "biz_client_duty.user_id = '{$data['crm_user']}'";
        } else if ($data['crm_user_type'] == 'group') {
            $sales_ids_where = "biz_client_duty.user_group = '{$data['crm_user']}' and biz_client_duty.user_role = 'sales'";
        } else if ($data['crm_user_type'] == 'sub_company') {
            $crm_user = str_replace('company_', '', $data['crm_user']);
            //2023-03-08 这里改为用方法获取子组
            $groups = Model('bsc_group_model')->get_child_group($crm_user);
            $groups_code = array_column($groups, 'group_code');
            $sales_ids_where = "biz_client_duty.user_group in ('" . join('\',\'', $groups_code) . "') and biz_client_duty.user_role = 'sales'";

        } else if ($data['crm_user_type'] == 'head') {
            if ($data['crm_user'] == 'HEAD') {
                //2023-03-08 这里改为用方法获取子组
                $groups = Model('bsc_group_model')->get_child_group($data['crm_user']);
                $groups_code = array_column($groups, 'group_code');
                $sales_ids_where = "biz_client_duty.user_group in ('" . join('\',\'', $groups_code) . "') and biz_client_duty.user_role = 'sales'";
            }
        } else {
            $sales_ids_where = '0 = 1';
        }
        $sales_ids_where = ' and ' . Model('biz_client_model')->get_read_role_where($sales_ids_where);

        //当前总客户数
        $month_first_day = $data['crm_date_first'];
        $month_end_day = $data['crm_date_end'];
        // $month = date("Y-m", $time);
        $sql = "SELECT count(*) as n FROM `biz_client` where 1 = 1 $sales_ids_where";
        $rs = $this->m_model->query_one($sql);
        $data["totalNumber"] = $rs['n'];

        //新增客户数统计图--start
        // $sql = "SELECT DATE_FORMAT(created_time,'%Y-%m-%d') as d, count(*) as n FROM `biz_client` where created_time >= '$month_first_day 00:00:00' and  created_time <= '$month_end_day 23:59:59' $sales_ids_where GROUP BY DATE_FORMAT(created_time,'%Y-%m-%d')";
        // $rs = array_column($this->m_model->query_array($sql), null, 'd');

        //apply_type 为1代表CRM申请的

        $data['NewAddList'] = array();
        // //如果不存在，根据月份直接填充全部天数的
        // for($i = 1; $i <= 31; $i++){
        //     $this_day = str_pad($i,2,'0',STR_PAD_LEFT);
        //     $this_d_date = $month . '-' . $this_day;
        //     if(isset($rs[$this_d_date])) $data['NewAddList'][] = $rs[$this_d_date];
        //     else $data['NewAddList'][] = array('d' => $this_d_date, 'n' => 0);
        // }
        //新增客户数统计图--end

        //本月新增客户数
        // $sql = "SELECT count(*) as n FROM `biz_client` where created_time > '$month_first_day' $sales_ids_where";
        // $row = $this->m_model->query_one($sql);
        // $row = array_sum(array_column($data['NewAddList'], 'n'));


        //首次成交客户数
        //2021-11-04 改为本月首次成交
        $sql = "select count(*) as n from (select client_code,count(*) as n, biz_shipment.booking_ETD from biz_shipment GROUP BY client_code HAVING n = 1) a INNER JOIN biz_client on biz_client.client_code = a.client_code where a.booking_ETD >= '$month_first_day 00:00:00' and a.booking_ETD <= '$month_end_day 23:59:59' and biz_client.apply_type = 1 $sales_ids_where";
        $row = $this->m_model->query_one($sql);
        $data["CrmDealOne"] = $row["n"];

        //成交客户数
        // $sql = "select count(*) as n from biz_client RIGHT JOIN (select client_code,count(*) as n from biz_shipment GROUP BY client_code HAVING n >= 1) a on a.client_code = biz_client.client_code where 1 = 1 and created_time > '$month_first_day' $sales_ids_where";
        $sql = "select count(*) as n from (select client_code,count(*) as n, biz_shipment.booking_ETD from biz_shipment where `status`='normal' GROUP BY client_code HAVING n >= 1) a INNER JOIN biz_client on biz_client.client_code = a.client_code where 1 = 1 $sales_ids_where";
        $row = $this->m_model->query_one($sql);
        $data["DealMore"] = $row["n"];

        //本月成交客户数统计图--start
        // $sql = "select DATE_FORMAT(biz_client.created_time,'%Y-%m-%d') as d,count(*) as n from biz_client RIGHT JOIN (select client_code,count(*) as n from biz_shipment where created_time >= '$month_first_day 00:00:00' and created_time <= '$month_end_day 23:59:59' GROUP BY client_code HAVING n > 1) a on a.client_code = biz_client.client_code where 1 = 1 $sales_ids_where GROUP BY DATE_FORMAT(biz_client.created_time,'%Y-%m-%d')";
        // $sql = "select client_code,count(*) as n from biz_shipment where created_time >= '$month_first_day 00:00:00' and created_time <= '$month_end_day 23:59:59' GROUP BY client_code HAVING n > 1";
        $sql = "select biz_shipment.client_code,count(*) as n  from biz_shipment inner join biz_client on biz_client.client_code = biz_shipment.client_code where biz_shipment.booking_ETD >= '$month_first_day 00:00:00' and biz_shipment.booking_ETD <= '$month_end_day 23:59:59' $sales_ids_where GROUP BY biz_shipment.client_code";
        $rs = $this->m_model->query_array($sql);
        $row = sizeof($rs);
        $data["DealNumber"] = $row;

        //统计图待定,目前只显示总数
        $data['DealNumberList'] = array();

        //客户活跃度
        if ($data["totalNumber"] == 0) $data['ClientActivityDegree'] = 0;
        else $data['ClientActivityDegree'] = round($data["DealNumber"] / $data["totalNumber"], 4) * 100;

        //跟进次数 待做

        //客户TEU数
        //原逻辑,根据销售找到客户,根据客户找到相关的shipment,client_code in (客户代码)
        //2021-11-11 这里因为需要到shipment进行查看,所以改为shipment销售=当前
        if ($data['crm_user_type'] == 'user') {
            $shipment_ids_where = " and user_role = 'sales' and biz_duty_new.user_id = '{$data['crm_user']}'";
        } else if ($data['crm_user_type'] == 'group') {
            $shipment_ids_where = " and user_role = 'sales' and biz_duty_new.user_group = '{$data['crm_user']}'";
        } else if ($data['crm_user_type'] == 'sub_company') {
            $crm_user = str_replace('company_', '', $data['crm_user']);
            //2023-03-08 这里改为用方法获取子组
            $groups = Model('bsc_group_model')->get_child_group($crm_user);
            $groups_code = array_column($groups, 'group_code');
            $shipment_ids_where = " and user_role = 'sales' and biz_duty_new.user_group in ('" . join('\',\'', $groups_code) . "')";
//            $shipment_ids_where = " and biz_duty_new.sales_group like '{$crm_user}-%'";
        } else if ($data['crm_user_type'] == 'head') {
            $shipment_ids_where = " and 1 = 0";
            if ($data['crm_user'] == 'HEAD') {
//                Model('bsc_group_model');
//                $this->db->select('group_code');
//                $companys = $this->bsc_group_model->get('parent_id = 1001');
//                $sales_ids_or = array();
//                foreach ($companys as $company) {
//                    $sales_ids_or[] = "biz_duty_new.sales_group like '%{$company['group_code']}-%'";
//                }
//                if (empty($sales_ids_or)) $sales_ids_or[] = "1 = 0";
//                $shipment_ids_where = " and (" . join(' or ', $sales_ids_or) . ")";
                //2023-03-08 这里改为用方法获取子组
                $groups = Model('bsc_group_model')->get_child_group($data['crm_user']);
                $groups_code = array_column($groups, 'group_code');
                $shipment_ids_where = " and user_role = 'sales' and biz_duty_new.user_group in ('" . join('\',\'', $groups_code) . "')";
            }
        } else {
            $shipment_ids_where = ' and 0 = 1';
        }

        $sql = "select box_info from biz_shipment INNER JOIN biz_duty_new on biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id where booking_ETD >= '$month_first_day 00:00:00' and booking_ETD <= '$month_end_day 23:59:59' and biz_shipment.parent_id = 0 and biz_shipment.status = 'normal' $shipment_ids_where";
        $rs = $this->m_model->query_array($sql);
        $data['ClientTEU'] = 0;
        foreach ($rs as $r) {
            $r_arr = json_decode($r['box_info'], true);
            foreach ($r_arr as $rit) {
                if ($rit['size'][0] == '2') {
                    $data['ClientTEU'] += 1 * $rit['num'];
                } else if ($rit['size'][0] == '4') {
                    $data['ClientTEU'] += 2 * $rit['num'];
                }
            }
        }

        //CrmShipmentCount CRM总票数
        $sql = "select count(*) as n from biz_shipment INNER JOIN biz_client on biz_client.client_code = biz_shipment.client_code INNER JOIN biz_duty_new on biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id where biz_shipment.booking_ETD >= '$month_first_day 00:00:00' and biz_shipment.booking_ETD <= '$month_end_day 23:59:59' and biz_shipment.parent_id = 0 and biz_client.apply_type = 1 $shipment_ids_where";
        $row = $this->m_model->query_one($sql);
        $data['CrmShipmentCount'] = $row['n'];

        //CrmClientTeu  Crm TEU总数
        $sql = "select box_info from biz_shipment INNER JOIN biz_client on biz_client.client_code = biz_shipment.client_code  INNER JOIN biz_duty_new on biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id where biz_shipment.booking_ETD >= '$month_first_day 00:00:00' and biz_shipment.booking_ETD <= '$month_end_day 23:59:59' and biz_shipment.parent_id = 0 and biz_shipment.status = 'normal' and biz_client.apply_type = 1 $shipment_ids_where";
        $rs = $this->m_model->query_array($sql);
        $data['CrmClientTeu'] = 0;
        foreach ($rs as $r) {
            $r_arr = json_decode($r['box_info'], true);
            foreach ($r_arr as $rit) {
                if ($rit['size'][0] == '2') {
                    $data['CrmClientTeu'] += 1 * $rit['num'];
                } else if ($rit['size'][0] == '4') {
                    $data['CrmClientTeu'] += 2 * $rit['num'];
                }
            }
        }

        if ($data['crm_user_type'] == 'user') {
            $crm_where = " and biz_client_crm.sales_id = '{$data['crm_user']}'";
        } else if ($data['crm_user_type'] == 'group') {
            $crm_where = " and (select `group` from bsc_user where biz_client_crm.sales_id = bsc_user.id) = '{$data['crm_user']}'";
        } else if ($data['crm_user_type'] == 'sub_company') {
            $crm_user = str_replace('company_', '', $data['crm_user']);
            //2023-03-08 这里改为用方法获取子组
            $groups = Model('bsc_group_model')->get_child_group($crm_user);
            $groups_code = array_column($groups, 'group_code');
//            $crm_where = " and (select `group` from bsc_user where biz_client_crm.sales_id = bsc_user.id) like '%{$data['crm_user']}-%'";
            $crm_where = " and (select `group` from bsc_user where biz_client_crm.sales_id = bsc_user.id) in ('" . join('\',\'', $groups_code) . "')";
        } else if ($data['crm_user_type'] == 'head') {
            $crm_where = " and 1 = 0";
            if ($data['crm_user'] == 'HEAD') {
//                Model('bsc_group_model');
//                $this->db->select('group_code');
//                $companys = $this->bsc_group_model->get('parent_id = 1001');
//                $sales_ids_or = array();
//                foreach ($companys as $company) {
//                    $sales_ids_or[] = "(select `group` from bsc_user where biz_client_crm.sales_id = bsc_user.id) like '%{$company['group_code']}-%'";
//                }
//                if (empty($sales_ids_or)) $sales_ids_or[] = "1 = 0";
//                $crm_where = " and (" . join(' or ', $sales_ids_or) . ")";
                //2023-03-08 这里改为用方法获取子组
                $groups = Model('bsc_group_model')->get_child_group($data['crm_user']);
                $groups_code = array_column($groups, 'group_code');
                $crm_where = " and (select `group` from bsc_user where biz_client_crm.sales_id = bsc_user.id) in ('" . join('\',\'', $groups_code) . "')";
            }
        } else {
            $crm_where = ' and 0 = 1';
        }

        //获取CRM 转化率 原理是 通过的 / 总CRM数据
        $sql = "select ((select count(*) from biz_client_crm where apply_status = 0 {$crm_where}) / (select count(*) from biz_client_crm where apply_status != 0 {$crm_where})) as rate";
        $rs = $this->m_model->query_one($sql);
        $data['CrmClientRate'] = empty($rs['rate']) ? '0.00' : round($rs['rate'] * 100, 2);

        $sql = "SELECT count(*) as n FROM `biz_client_crm` where created_time >= '$month_first_day 00:00:00' and  created_time <= '$month_end_day 23:59:59' and apply_type = 1 $crm_where";
        $row = $this->m_model->query_one($sql);
        $data["CrmClientNewAdd"] = $row['n'];

        //跟进客户数统计图--start
        // $sql = "SELECT DATE_FORMAT(biz_client_follow_up.created_time,'%Y-%m-%d') as d,count(*) as n FROM `biz_client` RIGHT JOIN biz_client_follow_up ON biz_client_follow_up.client_code = biz_client.client_code where biz_client_follow_up.status = 0 and biz_client.client_level >= 0 $sales_ids_where GROUP BY DATE_FORMAT(biz_client_follow_up.created_time,'%Y-%m-%d')";
        $sql = "select count(*) as n from biz_client_crm where apply_type = 1 and apply_status = 2 {$crm_where}";

        $rs = array_column($this->m_model->query_array($sql), null, 'n');

        $data['FollowUpList'] = array();
        // //如果不存在，根据月份直接填充全部天数的
        // for($i = 1; $i <= 31; $i++){
        //     $this_day = str_pad($i,2,'0',STR_PAD_LEFT);
        //     $this_d_date = $month . '-' . $this_day;
        //     if(isset($rs[$this_d_date])) $data['FollowUpList'][] = $rs[$this_d_date];
        //     else $data['FollowUpList'][] = array('d' => $this_d_date, 'n' => 0);
        // }
        //跟进客户数
        // $sql = "SELECT count(*) as n FROM `biz_client` RIGHT JOIN biz_client_follow_up ON biz_client_follow_up.client_code = biz_client.client_code where  1 = 1 $sales_ids_where";
        // $row = $this->m_model->query_one($sql);
        $row = array_sum(array_column($rs, 'n'));
        $data["FollowUp"] = $row;

        //跟进客户数统计图--start

        //最后一次群发时间--start
        $crm_promote_mail_where = array();
        if ($data['crm_user_type'] == 'user') {
            //2022-01-10 新需求 CRM获取数据逻辑 原创建人 改为 创建人+销售
            //2022-02-23 改版后, crm获取数据,只自己获取自己的
            $crm_promote_mail_where[] = " crm_promote_mail.create_by = '{$data['crm_user']}' ";
        } else if ($data['crm_user_type'] == 'group') {
            $crm_promote_mail_where[] = " ( select `group` from bsc_user where bsc_user.id = crm_promote_mail.create_by ) = '{$data['crm_user']}' ";
        } else if ($data['crm_user_type'] == 'sub_company') {
            $this_crm_user = str_replace('company_', '', $data['crm_user']);

            //2023-03-08 这里改为用方法获取子组
            $groups = Model('bsc_group_model')->get_child_group($this_crm_user);
            $groups_code = array_column($groups, 'group_code');
//            $crm_promote_mail_where[] = " ( select `group` from bsc_user where bsc_user.id = crm_promote_mail.create_by ) like '$this_crm_user-%' ";
            $crm_promote_mail_where[] = " ( select `group` from bsc_user where bsc_user.id = crm_promote_mail.create_by ) in ('" . join('\',\'', $groups_code) . "')";
        } else if ($data['crm_user_type'] == 'head') {
            if ($data['crm_user'] == 'HEAD') {
//                Model('bsc_group_model');
//                $this->db->select('group_code');
//                $companys = $this->bsc_group_model->get('parent_id = 1001');
//                $sales_ids_or = array();
//                foreach ($companys as $company) {
//                    $group_code = $company['group_code'];
//                    $sales_ids_or[] = "(select `group` from bsc_user where bsc_user.id = crm_promote_mail.create_by) like '%$group_code-%'";
//                }
//                if (empty($sales_ids_or)) $sales_ids_or[] = "1 = 0";
//                $crm_promote_mail_where[] = "(" . join(' or ', $sales_ids_or) . ")";
                //2023-03-08 这里改为用方法获取子组
                $groups = Model('bsc_group_model')->get_child_group($data['crm_user']);
                $groups_code = array_column($groups, 'group_code');

                $crm_promote_mail_where[] = "(select `group` from bsc_user where bsc_user.id = crm_promote_mail.create_by) in ('" . join('\',\'', $groups_code) . "')";
            }
        } else {
            // $crm_promote_mail_where[] = '0 = 1';
        }
        $crm_promote_mail_where[] = "crm_promote_mail.status = '2' ";
        $crm_promote_mail_where_str = join(' and ', $crm_promote_mail_where);
        $this->db->limit(1);
        $this->db->select("crm_promote_mail.create_time");
        $rs = Model('crm_promote_mail_model')->get($crm_promote_mail_where_str, 'create_time', 'desc');
        $data['LastSendMailTime'] = !empty($rs) ? date('m-d H:i', strtotime($rs[0]['create_time'])) : '暂无';
        //status
        //最后一次群发时间--end
        
        //邮件群发数量--start
        $data['SendMailCount'] = Model('crm_promote_mail_model')->total($crm_promote_mail_where_str . " and crm_promote_mail.create_time >= '{$month_first_day} 00:00:00' and crm_promote_mail.create_time <= '{$month_end_day} 23:59:59'");
        //邮件群发数量--end

        $this->load->view('biz/crm/dashboard', $data);
    }

    /**
     * 待跟进客户
     */
    public function customer_followup()
    {
        $this->load->view('biz/crm/customer_followup');
    }

    /**
     * 待跟进客户数据
     */
    public function get_followup_data()
    {
        $result = array('code' => 0, 'msg' => 'success', 'rows' => array(), 'total' => 0);

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        $f_role = isset($_REQUEST['f_role']) ? $_REQUEST['f_role'] : '';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $crm_user = isset($_COOKIE['crm_user']) ? $_COOKIE['crm_user'] : get_session('id');
        $crm_user_type = isset($_COOKIE['crm_user_type']) ? $_COOKIE['crm_user_type'] : 'user';
        $crm_date = isset($_COOKIE['crm_date']) ? $_COOKIE['crm_date'] : date('Y-m');


        //where---start
        $ThreeMonthsAgo = date('Y-m-d', strtotime('-3 month'));
        $where = array();
        $where[] = "client_level >= 0";
        $where[] = "biz_client_follow_up.status = 0";
        if ($crm_user_type == 'user') {
            $where[] = "biz_client_follow_up.to_user_id = $crm_user";
        } else if ($crm_user_type == 'group') {
            $where[] = "(select `group` from bsc_user where biz_client_follow_up.to_user_id = bsc_user.id) = '$crm_user'";
        } else if ($crm_user_type == 'sub_company') {
            $this_crm_user = str_replace('company_', '', $crm_user);

            $groups = Model('bsc_group_model')->get_child_group($this_crm_user);
            $groups_code = array_column($groups, 'group_code');
            $where[] = "(select `group` from bsc_user where biz_client_follow_up.to_user_id = bsc_user.id) in ('" . join('\',\'', $groups_code) . "')";
        } else if ($crm_user_type == 'head') {
            if ($crm_user == 'HEAD') {
//                Model('bsc_group_model');
//                $this->db->select('group_code');
//                $companys = $this->bsc_group_model->get('parent_id = 1001');
//                $sales_ids_or = array();
//                foreach ($companys as $company) {
//                    $sales_ids_or[] = "(select `group` from bsc_user where biz_client_follow_up.to_user_id = bsc_user.id) like '%{$company['group_code']}-%'";
//                }
//                if (empty($sales_ids_or)) $sales_ids_or[] = "1 = 0";
//                $where[] = "(" . join(' or ', $sales_ids_or) . ")";
                $groups = Model('bsc_group_model')->get_child_group($crm_user);
                $groups_code = array_column($groups, 'group_code');
                $where[] = "(select `group` from bsc_user where biz_client_follow_up.to_user_id = bsc_user.id) in ('" . join('\',\'', $groups_code) . "')";
            }
        } else {
            $where[] = '0 = 1';
        }
        //用户限制
        if (!empty($fields)) {
            foreach ($fields['f'] as $key => $field) {
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if ($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=  
                $date_field = array('created_time', 'updated_time');
                if (in_array($f, $date_field) && $s == '=') {
                    if ($v != '') {
                        $where[] = "{$pre}{$f} >= '$v 00:00:00' and {$pre}{$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                if ($f == 'company_name') {
                    $f = 'company_search';
                    $v = match_chinese($v);
                }
                if ($f == 'sales_id') {
                    $f = '(select name from bsc_user where bsc_user.id = sales_id)';
                    if ($v != '') $where[] = search_like($f, $s, $v);
                } else {
                    if ($v != '') $where[] = search_like('biz_client.' . $f, $s, $v);
                }

            }
        }
        if ($f_role != '') $where[] = "role like '%$f_role%'";
        $where = join(' and ', $where);
        //where---end

        $offset = ($page - 1) * $rows;
        $this->db->join("biz_client_follow_up", 'biz_client_follow_up.client_code = biz_client.client_code', 'RIGHT');
        $result["total"] = $this->biz_client_model->total($where);
        $this->db->select('biz_client.id,tag,biz_client.client_code,client_name,province,city,company_address,contact_name,contact_telephone,contact_telephone2,contact_email,DATEDIFF(NOW(), biz_client_follow_up.created_time) as follow_up_day', false);
        $this->db->join("biz_client_follow_up", 'biz_client_follow_up.client_code = biz_client.client_code ', 'RIGHT');
        $this->db->limit($rows, $offset);
        $rs = $this->biz_client_model->get($where, 'biz_client.' . $sort, $order);

        foreach ($rs as $row) {
            $result['rows'][] = $row;
        }

        echo json_encode($result);
    }

    public function customer_inactive_main()
    {
        $this->load->view('biz/crm/customer_inactive_main');
    }

    public function customer_private()
    {
        $this->load->view('biz/crm/customer_private');
    }
    public function customer_public()
    {
        $this->load->view('biz/crm/customer_public');
    }

    public function get_inactive_data()
    {
        $result = array('code' => 0, 'msg' => 'success', 'rows' => array(), 'total' => 0);
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        $f_role = isset($_REQUEST['f_role']) ? $_REQUEST['f_role'] : '';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

        //交易时间或日志时间的排序
        if($sort == 'last_deal_time'){
            $sort = '(select booking_ETD from biz_shipment where status="normal" and  biz_shipment.client_code = biz_client.client_code order by booking_ETD desc limit 1)';
        }elseif($sort == 'new_log_date'){
            $sort = '(select created_time from biz_client_follow_up_log where biz_client_follow_up_log.client_code = biz_client.client_code order by created_time desc limit 1)';
        }elseif($sort == 'log_num'){
            $sort = '(SELECT count(client_code) as num FROM `biz_client_follow_up_log` where biz_client_follow_up_log.client_code = biz_client.client_code GROUP BY client_code order by num desc)';
        }

        //where---start
        $ThreeMonthsAgo = date('Y-m-d', strtotime('-3 month'));
        $where = array();
        $where[] = 'is_open = 0';
        $where[] = 'client_level >= 0';
        //用户限制
        if (!empty($fields)) {
            foreach ($fields['f'] as $key => $field) {
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if ($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=  
                $date_field = array('created_time', 'updated_time');
                if (in_array($f, $date_field) && $s == '=') {
                    if ($v != '') {
                        $where[] = "{$pre}{$f} >= '$v 00:00:00' and {$pre}{$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                if ($f == 'company_name') {
                    $f = 'company_search';
                    $v = match_chinese($v);
                }
                if ($f == 'sales_id') {
                    $f = '(select name from bsc_user where bsc_user.id = sales_id)';
                    if ($v != '') $where[] = search_like($f, $s, $v);
                } else {
                    if ($v != '') $where[] = search_like('biz_client.' . $f, $s, $v);
                }

            }
        }
        if ($f_role != '') $where[] = "(biz_client.role like '%,{$f_role}' or biz_client.role like '{$f_role},%' or biz_client.role like '%,{$f_role},%' or biz_client.role = '{$f_role}')";

        $userRange = get_session('user_range');
        $join_more = "biz_client_duty.user_role = 'sales' and biz_client_duty.user_id in(".join(',', $userRange).")";
        $where[] = "client_code in (SELECT biz_client_duty.client_code FROM biz_client_duty as biz_client_duty LEFT JOIN biz_client_inactve as biz_client_inactve ON biz_client_inactve.client_code=biz_client_duty.client_code WHERE 1=1 and biz_client_inactve.is_inactve=0 and {$join_more} GROUP BY biz_client_duty.client_code)";
        $where = join(' and ', $where);

        $result["total"] = $this->biz_client_model->no_role_total($where);
        $field = 'biz_client.id,tag,client_code,client_name,company_name,province,city,company_address,contact_name,contact_telephone,contact_telephone2,contact_email,sales_ids';
        $field .=',(select booking_ETD from biz_shipment where status="normal" and biz_shipment.client_code = biz_client.client_code order by booking_ETD desc limit 1) as last_shipment_time';
        $field .=',(select created_time from biz_client_follow_up_log where biz_client_follow_up_log.client_code = biz_client.client_code order by created_time desc limit 1) as new_log_date';
        $field .=',(SELECT count(client_code) FROM `biz_client_follow_up_log` where biz_client_follow_up_log.client_code = biz_client.client_code GROUP BY client_code) as log_num';
        $offset = ($page - 1) * $rows;
        $this->db->select($field)->limit($rows, $offset)->where($where);
        $this->db->order_by($sort,$order, true);
        $query = $this->db->get('biz_client');
        //echo $this->db->last_query();exit;
        $rs = $query->result_array();

        //从biz_client_duty表中取得销售人员ID
        $client_duty = Model('biz_client_duty_model')->get_client_data(array_column($rs, 'client_code'));
        foreach ($rs as $key => $row){
            isset($client_duty[$row['client_code']]) && $rs[$key] = array_merge($rs[$key], $client_duty[$row['client_code']]);
        }

        //获取销售名称--start
        Model('bsc_user_model');
        $sales_ids = filter_unique_array(explode(',', join(',', array_column($rs, 'sales_ids'))));
        if (empty($sales_ids)) $sales_ids[] = 0;
        $this->db->select('id,name');
        $sales_users = array_column($this->bsc_user_model->get("id in (" . join(',', $sales_ids) . ")"), null, 'id');
        //获取销售名称--end

        //2021-09-13 由于同时拥有client和booking_agent等，组权限为ALL的角色，导致其他没有client组查看权限的客户单位，出现在了不活跃里，这里需要将不满足条件的进行手动删除
        foreach ($rs as $row) {
            $this_sales_ids = filter_unique_array(explode(',', $row['sales_ids']));
            $row['sales_names'] = array();
            $row['sales'] = array();
            foreach ($this_sales_ids as $this_sales_id) {
                if (isset($sales_users[$this_sales_id])) {
                    $row['sales_names'][] = $sales_users[$this_sales_id]['name'];
                    $row['sales'][] = $sales_users[$this_sales_id];
                }
            }
            $row['sales_names'] = join(',', $row['sales_names']);
            $result['rows'][] = $row;
        }

        echo json_encode($result);
    }

    public function get_public_data()
    {
        $result = array('code' => 0, 'msg' => 'success', 'rows' => array(), 'total' => 0);
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        $f_role = isset($_REQUEST['f_role']) ? $_REQUEST['f_role'] : '';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

        //交易时间或日志时间的排序
        if($sort == 'last_deal_time'){
            $sort = '(select booking_ETD from biz_shipment where biz_shipment.client_code = biz_client.client_code order by booking_ETD desc limit 1)';
        }elseif($sort == 'new_log_date'){
            $sort = '(select created_time from biz_client_follow_up_log where biz_client_follow_up_log.client_code = biz_client.client_code order by created_time desc limit 1)';
        }

        //where---start
        $ThreeMonthsAgo = date('Y-m-d', strtotime('-3 month'));
        $where = array();
        $where[] = 'is_open = 1';
        //用户限制
        if (!empty($fields)) {
            foreach ($fields['f'] as $key => $field) {
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if ($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = array('created_time', 'updated_time');
                if (in_array($f, $date_field) && $s == '=') {
                    if ($v != '') {
                        $where[] = "{$pre}{$f} >= '$v 00:00:00' and {$pre}{$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                if ($f == 'company_name') {
                    $f = 'company_search';
                    $v = match_chinese($v);
                }
                if ($f == 'sales_id') {
                    $f = '(select name from bsc_user where bsc_user.id = sales_id)';
                    if ($v != '') $where[] = search_like($f, $s, $v);
                } else {
                    if ($v != '') $where[] = search_like('biz_client.' . $f, $s, $v);
                }

            }
        }
        if ($f_role != '') $where[] = "(biz_client.role like '%,{$f_role}' or biz_client.role like '{$f_role},%' or biz_client.role like '%,{$f_role},%' or biz_client.role = '{$f_role}')";

        $where = join(' and ', $where);
        $result["total"] = $this->biz_client_model->no_role_total($where);

        $field = 'id,biz_client.id,tag,biz_client.client_code,client_name,company_name,province,city,company_address,contact_name,contact_telephone,contact_telephone2,contact_email,sales_ids';
        $field .=',(select booking_ETD from biz_shipment where biz_shipment.client_code = biz_client.client_code order by booking_ETD desc limit 1) as last_shipment_time';
        $field .=',(select created_time from biz_client_follow_up_log where biz_client_follow_up_log.client_code = biz_client.client_code order by created_time desc limit 1) as new_log_date';
        $offset = ($page - 1) * $rows;
        $this->db->select($field);
        $this->db->limit($rows, $offset);
        $rs = $this->biz_client_model->no_role_get($where, $sort, $order);
        //echo $this->db->last_query();exit;

        //从biz_client_duty表中取得销售人员ID
        $client_duty = Model('biz_client_duty_model')->get_client_data(array_column($rs, 'client_code'));
        foreach ($rs as $key => $row){
            isset($client_duty[$row['client_code']]) && $rs[$key] = array_merge($rs[$key], $client_duty[$row['client_code']]);
        }

        //获取销售名称--start
        Model('bsc_user_model');
        $sales_ids = filter_unique_array(explode(',', join(',', array_column($rs, 'sales_ids'))));
        if (empty($sales_ids)) $sales_ids[] = 0;
        $this->db->select('id,name');
        $sales_users = array_column($this->bsc_user_model->get("id in (" . join(',', $sales_ids) . ")"), null, 'id');
        //获取销售名称--end

        //2021-09-13 由于同时拥有client和booking_agent等，组权限为ALL的角色，导致其他没有client组查看权限的客户单位，出现在了不活跃里，这里需要将不满足条件的进行手动删除
        foreach ($rs as $row) {
            $this_sales_ids = filter_unique_array(explode(',', $row['sales_ids']));
            $row['sales_names'] = array();
            $row['sales'] = array();
            foreach ($this_sales_ids as $this_sales_id) {
                if (isset($sales_users[$this_sales_id])) {
                    $row['sales_names'][] = $sales_users[$this_sales_id]['name'];
                    $row['sales'][] = $sales_users[$this_sales_id];
                }
            }
            $row['sales_names'] = join(',', $row['sales_names']);
            $result['rows'][] = $row;
        }

        echo json_encode($result);
    }

    public function customer_all()
    {
        $this->load->view('biz/crm/customer_all');
    }

    /**
     * 进入CRM客户页面前的选择状态页面
     */
    public function customer_apply_status()
    {
        $data = array();

        $this->load->view('biz/crm/customer_apply_status', $data);
    }

    /**
     * CRM客户页面
     */
    public function customer_apply($import = '')
    {
        // column defination
        $table_name = "biz_crm_index";
        $data = array();
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one($table_name . '_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $this->load->model('sys_config_title_model');
            $data["f"] = $this->sys_config_title_model->get_one($table_name);
        }

        $data['apply_status'] = getValue('apply_status', '');
        $data['crm_user'] = getValue('crm_user', get_session('id'));//get_session('id')
        $data['crm_user_type'] = getValue('crm_user_type', 'user');//user
        $data['crm_date_first'] = getValue('crm_date_first', date('Y-m-d'));
        $data['crm_date_end'] = getValue('crm_date_end', date('Y-m-d'));
        $this->yz_token();

        //新增导入依据
        $data['import'] = $import;

        //获取全部销售和销售领导
        $sales = $this->db->select('id, leader_id, m_level, name,(select bsc_group.group_name from bsc_group where bsc_group.group_code = bsc_user.group) as group_name')
            ->where_in('id', get_session('user_range'))
            ->where("status = 0 and (FIND_IN_SET('sales', user_role) or FIND_IN_SET('sales_assistant', user_role))")->get('bsc_user')
            ->result_array();
        $this->load->vars('sales', $sales);
        $this->load->view('biz/crm/customer_apply', $data);
    }

    /**
     * 删除管理员
     * @return void
     */
    public function del_customer()
    {
        $ids = $this->input->post('ids');
        $this->load->model('biz_client_crm_model');

        //提前查询一遍
        $this->db->select('id,apply_status');
        $rows = $this->biz_client_crm_model->get("id in ('" . join('\',\'', $ids) . "')");

        $status_arr = filter_unique_array(array_column($rows, 'apply_status'));
        foreach ($status_arr as $v) {
            if (!in_array($v, array(1, -1))) {
//                return jsonEcho(array("code" => 0, "msg" => "只能删除申请中或已拒绝的CRM客户"));
            }
        }

        Model('biz_client_contact_model');
        foreach ($rows as $row) {
            $id = $row['id'];
            $this->biz_client_crm_model->mdelete($id);
            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_client_crm";
            $log_data["key"] = $id;
            $log_data["action"] = "delete";
            $log_data["value"] = json_encode($row);
            log_rcd($log_data);

            //2022-07-04加入了联系人的删除
            $contacts = $this->biz_client_contact_model->get("crm_id = '{$id}'");
            //同步删除日志
            $this->db->where('id_no', $id)->delete('biz_client_crm_log');
            //删除相关线索
            $this->db->where('crm_id', $id)->delete('biz_client_crm_clue');
            //删除流转日志
            $this->db->where('id_no', $id)->delete('biz_client_crm_log');
            //删除跟踪日志
            $this->db->where('crm_id', $id)->delete('biz_client_follow_up_log');
            //删除进行中的审批事务
            $this->db->where(array('id_no'=>$id, 'id_type'=>1, 'status'=>0))->delete('biz_workflow');

            foreach ($contacts as $contact) {
                $this->biz_client_contact_model->mdelete($contact['id']);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_client_contact_list";
                $log_data["key"] = $contact['id'];
                $log_data["action"] = "delete";
                $log_data["value"] = json_encode($contact);
                log_rcd($log_data);
            }
        }

        $result = [
            "code" => 1,
            "msg" => ""
        ];
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;

    }


    /**
     * 这里做个简单token验证,如果他们传过来的token对不上,那么这里不给过
     * 后面可以改成直接验证是否当前用户有权限
     */
    private function yz_token()
    {
        $token = getValue('token', '');
        $crm_user = getValue('crm_user', '');
        $crm_user_type = getValue('crm_user_type', '');
        $crm_date_first = getValue('crm_date_first', '');
        $crm_date_end = getValue('crm_date_end', '');

        $check_token = md5($crm_user_type . $crm_user . $crm_date_first . $crm_date_end);
        if ($token != '' && $token != $check_token) exit('参数失效,请关闭窗口后重新点击');
    }

    /**
     * 获取CRM客户数据
     */
    public function get_apply_data()
    {
        $result = array('code' => 0, 'msg' => 'success', 'rows' => array(), 'total' => 0);

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "biz_client_crm.id";//FIELD(biz_client_crm.apply_status,'1','-1','2','3','-2','0')
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $apply_status = getValue('apply_status', '');
        $whzts = postValue('whzts', '');//未合作天数
        $getType = (int)postValue('getType', 0);
        $workflow = (int)getValue('workflow', 1);

        if ($sort == 'apply_status') {
            $sort = "FIELD(biz_client_crm.apply_status,'1','-1','-2','2','3','0')";
        }

        $f_role = isset($_REQUEST['f_role']) ? $_REQUEST['f_role'] : '';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $crm_user = postValue('crm_user', get_session('id'));
        $crm_user_type = postValue('crm_user_type', 'user');

        $where = array();
        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_client_crm', 'biz_client_crm_index');//获取相关的配置信息
        
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];

                if($v !== '') {
                     //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) $title_data['sql_join'][$title_data['base_data'][$title_f]['sql_join']] = $this_join;

                    //如果条件是”来源“，1：领导指派或2：个人新增
                    if ($f == 'from_user_id'){
                        $where[] = $v==1 ? 'from_user_id > 0' : 'from_user_id = 0';
                    }else{
                        $where[] = search_like($f, $s, $v);
                    }
                }
            }
        }
        // if(is_admin()){
        //         var_dump($title_data['sql_join']);
        //         return;
        //     }
        $where[] = "biz_client_crm.apply_type = 1";
        if ($apply_status !== '') {
            $where[] = "biz_client_crm.apply_status = $apply_status";
        }else{
            $where[] = 'biz_client_crm.apply_status > 0';
        }

        //申请审核中
        if ($apply_status == 1) {
            if ($workflow == 1) {
                $where[] = 'biz_client_crm.id in(select id_no from biz_workflow where `status`=0 and id_type=1)';
            } else {//未申请审核
                $where[] = 'biz_client_crm.id not in(select id_no from biz_workflow where `status`=0 and id_type=1)';
                $where[] = 'biz_client_crm.sales_id = ' . get_session('id');
            }
        }

        if ($f_role != '') $where[] = "FIND_IN_SET('{$f_role}', biz_client_crm.role)";
        // if(is_admin()){
        if ($crm_user_type == 'user') {
            //2022-01-10 新需求 CRM获取数据逻辑 原创建人 改为 创建人+销售
            //2022-02-23 改版后, crm获取数据,只自己获取自己的
            $where[] = "(biz_client_crm.sales_id = '$crm_user' or biz_client_crm.created_by = '$crm_user')";
        } else if ($crm_user_type == 'group') {
            $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) = '$crm_user'";
        } else if ($crm_user_type == 'sub_company') {
            $this_crm_user = str_replace('company_', '', $crm_user);
            $groups = Model('bsc_group_model')->get_child_group($this_crm_user);
            $groups_code = array_column($groups, 'group_code');
//            $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) like '$this_crm_user-%'";
            $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) in ('" . join('\',\'', $groups_code) . "')";
        } else if ($crm_user_type == 'head') {
            if ($crm_user == 'HEAD') {
//                Model('bsc_group_model');
//                $this->db->select('group_code');
//                $companys = $this->bsc_group_model->get('parent_id = 1001');
//                $sales_ids_or = array();
//                foreach ($companys as $company) {
//                    $sales_ids_or[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) like '%{$company['group_code']}-%'";
//                }
//                if (empty($sales_ids_or)) $sales_ids_or[] = "1 = 0";
//                $where[] = "(" . join(' or ', $sales_ids_or) . ")";
                $groups = Model('bsc_group_model')->get_child_group($crm_user);
                $groups_code = array_column($groups, 'group_code');
                $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) in ('" . join('\',\'', $groups_code) . "')";
            }
        } else {
            $where[] = '0 = 1';
        }

        $where = join(' and ', $where);
        // $where .= " or (biz_client.client_level >= 0 and biz_client_follow_up.status = 0 and $crm_where)";
        //where---end
        Model('biz_client_crm_model');
        if($getType === 0){
            $offset = ($page - 1) * $rows;
            $this->db->limit($rows, $offset);
            //获取需要的字段--start
            $selects = array(
                'biz_client_crm.id',
                'biz_client_crm.created_by',
                'biz_client_crm.apply_status',
                'biz_client_crm.is_new',
                'biz_client_crm.company_search',
            );
            foreach ($title_data['select_field'] as $key => $val){
                if(!in_array($val, $selects)) $selects[] = $val;
            }

            //获取需要的字段--end
            $this->db->select(join(',', $selects), false);
            //这里拼接join数据
            foreach ($title_data['sql_join'] as $j){
                $this->db->join($j[0], $j[1], $j[2]);
            }//将order字段替换为 对应的sql字段
            if(isset($title_data['sql_field']["biz_client_crm.{$sort}"])){
                $sort = $title_data['sql_field']["biz_client_crm.{$sort}"];
            }

            $this->db->is_reset_select(false);//不重置查询
            $rs = $this->db->where($where,null, false)->order_by($sort, $order, false)->get('biz_client_crm')->result_array();
            //exit($this->db->where($where,null, false)->order_by($sort, $order, false)->get_compiled_select('biz_client_crm'));
            $this->db->clear_limit();//清理下limit
            $result["total"] = $this->db->count_all_results('');

            $apply_status_values = $this->apply_status_values;
            foreach ($rs as $row) {
                $row['apply_status_name'] = isset($apply_status_values[$row['apply_status']]) ? $apply_status_values[$row['apply_status']] : '';

                //2022-04-26新增一个完整度统计 计算规则为 联系方式权重2 其他权重1 加起来刚好10
                $row['score'] = 0;
                if (!empty($row['email'])) $row['score'] += 20;
                if (!empty($row['telephone'])) $row['score'] += 20;
                if (!empty($row['live_chat'])) $row['score'] += 20;
                if (!empty($row['company_address'])) $row['score'] += 10;
                if (!empty($row['export_sailing'])) $row['score'] += 10;
                if (!empty($row['trans_mode'])) $row['score'] += 10;
                if (!empty($row['product_type'])) $row['score'] += 10;
                $row['score'] .= "%";

                /*$row['progress'] = 0;
                if ($row['close_plan_date'] && $row['assign_date']){
                    $total_days = floor((strtotime($row['close_plan_date']) - strtotime($row['assign_date'])) / 86400);
                    $row['progress'] = $total_days > 0 ? floor(strval(($row['passed'] / $total_days)*10000))/10000*100 : 100;
                }*/
                $result['rows'][] = $row;
            }
        }else if($getType === 1){   //导入邮箱
            $this->db->select('biz_client_crm.id');
            //这里拼接join数据
            foreach ($title_data['sql_join'] as $j){
                if($j[0] == 'biz_client') continue;
                $this->db->join($j[0], $j[1], $j[2]);
            }//将order字段替换为 对应的sql字段
            $rs = $this->biz_client_crm_model->get($where, $sort, $order);

            $result['data'] = array('ids' => join(',', array_column($rs, 'id')));
            $result['code'] = 0;
            $result['msg'] = '获取成功';
        }

        echo json_encode($result);
    }

    /**
     * CRM 修改数据,目前只支持修改状态和备注
     */
    public function crm_update_data()
    {
        $ids = postValue('ids', '');
        $fields = array('apply_status', 'apply_remark');
        $result = array('code' => 1, 'msg' => '参数错误');
        Model('biz_client_crm_model');
        $ids_arr = filter_unique_array(explode(',', $ids));
        if (empty($ids_arr)) {
            echo json_encode($result);
            return;
        }

        $this->db->select('id,apply_status,apply_remark');
//        $old_row = $this->biz_client_model->get_by_id($id); // 原本的因为是新增在client里,新版的改为修改client_crm表了
//        $old_row = $this->biz_client_crm_model->get_one('id', $id);
        $old_rows = array_column($this->biz_client_crm_model->get("id in ('" . join('\',\'', $ids_arr) . "')"), null, 'id');
        if (empty($old_rows)) {
            echo json_encode($result);
            return;
        }

        // $apply_type_values = $this->apply_type_values;

        $data = array();
        foreach ($fields as $field) {
            if (isset($_POST[$field]) && trim($_POST[$field]) !== '') {
                $item = trim($_POST[$field]);
                $data[$field] = $item;
            }
        }

        //2022-01-20 修改CRM状态时,直接将当前客户等级改为潜在
        //2022-02-23 由于CRM数据独立新表了,这里就不需要修改为潜在了,只有通过申请的时候(/biz_client/approved_client),原本的客户信息才会变为正式的,除非正式后有人单独修改为潜在
//        $data['client_level'] = 0;

        if (empty($data)) {
            $result['code'] = 0;
            $result['msg'] = '保存成功';
            echo json_encode($result);
            return;
        }

        foreach ($old_rows as $old_row) {
            $this->biz_client_crm_model->update($old_row['id'], $data);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_client_crm";
            $log_data["key"] = $old_row['id'];
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        }

        $result['code'] = 0;
        $result['msg'] = '保存成功';
        echo json_encode($result);
    }

    /**
     * CRM审核页面,用于审核CRM相关的数据
     */
    public function index_apply()
    {
        $data = array();
        setLang('biz_client');
        Model('biz_client_crm_model');

        // column defination
        $table_name = "biz_crm_index_apply";
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one($table_name . '_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $this->load->model('sys_config_title_model');
            $data["f"] = $this->sys_config_title_model->get_one($table_name);
        }

//        $data["f"] = $this->biz_client_crm_model->field_all;

        $this->load->view('head');
        $this->load->view('biz/crm/index_apply_view', $data);
    }

    /**
     * 获取处于CRM待审核状态中的数据
     */
    public function get_index_apply_data()
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------这个查询条件想修改成通用的 -------------------------------
        $f_role = isset($_REQUEST['role']) ? $_REQUEST['role'] : '';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

        $where = array();
        $where[] = 'biz_client_crm.apply_status = 3';//只显示待审核的
        //用户限制
        if (!empty($fields)) {
            foreach ($fields['f'] as $key => $field) {
                $f = $fields['f'][$key];
                $s = $fields['s'][$key];
                $v = $fields['v'][$key];
                if ($v != '') $where[] = search_like('biz_client_crm.' . $f, $s, $v);
            }
        }
        if ($f_role != '') $where[] = "biz_client_crm.role like '%$f_role%'";
        $where = join(' and ', $where);
        //-----------------------------------------------------------------

        Model('biz_client_crm_model');
        $offset = ($page - 1) * $rows;
        $this->db->join('biz_client', 'biz_client.client_code = biz_client_crm.client_code', 'LEFT');
        $result["total"] = $this->biz_client_crm_model->total($where);
        $this->db->limit($rows, $offset);
        $this->db->select('biz_client_crm.*, (select name from bsc_user where bsc_user.id = biz_client_crm.created_by) as created_by_name,(select name from bsc_user where bsc_user.id = biz_client_crm.updated_by) as updated_by_name');
        $this->db->join('biz_client', 'biz_client.client_code = biz_client_crm.client_code', 'LEFT');
        $rs = $this->biz_client_crm_model->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {
            $row['created_by'] = $row['created_by_name'];
            $row['updated_by'] = $row['updated_by_name'];

            //approve_user sales_id apply_status中文
            $user = $this->db->where('id', $row['approve_user'])->get('bsc_user')->row_array();
            $row['approve_user'] = $user['name'];
            $user = $this->db->where('id', $row['sales_id'])->get('bsc_user')->row_array();
            $row['sales_id'] = $user['name'];

            switch ($row['apply_status']) {
                case '0':
                    $row['apply_status'] = '正式';
                    break;
                case '-1':
                    $row['apply_status'] = '已拒绝';
                    break;
                case '1':
                    $row['apply_status'] = '申请中';
                    break;
                case '2':
                    $row['apply_status'] = '已通过';
                    break;
                case '3':
                    $row['apply_status'] = '转正申请中';
                    break;
                case '-2':
                    $row['apply_status'] = '线索';
                    break;
            }

            //缺抬头
            $client = $this->db->where('client_code', $row['client_code'])->get('biz_client')->row_array();
            $row['company_name'] = $client['company_name'];

            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add()
    {
        $userId = $this->session->userdata('id');
        $userBsc = $this->bsc_user_model->get_by_id($userId);

        //判断只是销售
        $data['event'] = getValue('event', 'insert');
        $data['company_name'] = postValue('company_name');
        $data['clue_name'] = match_chinese(postValue('clue_name'));
        $data['company_address'] = postValue('company_address');
        $data['country_code'] = postValue('country_code', 'CN');
        $data['is_sales'] = false;
        if ($this->session->userdata('level') == '9999') $data['is_sales'] = true;
        $this->load->view('head');
        $data['sales_id'] = in_array('sales',get_session('user_role'))?get_session('id'):0;
        $data['temp_client_code'] = "A" . $userId . date('YmdHis');
        $data['country_code'] = 'CN';
        $data['user'] = $userBsc;
        $this->load->view('biz/crm/add_crm_view', $data);
    }

    public function add_data(){
        $return = array('code'=>0, 'msg'=>'');
        $_post = $this->input->post();
        $event = postValue('event', 'insert');
        //校验提交
        $validate = $this->validate($_post, $event);
        if ($validate !== true){
            $return['msg'] = $validate;
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        //创建写入字段
        $data = $this->create_date($_post);
        $data['clue_name'] = match_chinese(strtoupper($data['clue_name']));
        $data['client_name'] = strtoupper($data['client_name']);
        $data['company_name'] = strtoupper($data['company_name']);
        $data['company_name_en'] = strtoupper($data['company_name_en']);

        $data['company_search'] = match_chinese($data['company_name']);
        $data["approve_user"] = getUserField($data['sales_id'], 'leader_id');
        $data["created_group"] = implode(',', $this->session->userdata('group') );
        $data["apply_type"] = 1;
        $data["apply_status"] = 2;//默认1 改为了2
        $data["created_by"] = get_session('id');
        $data["created_time"] = date('Y-m-d H:i:s');
        $data["last_follow_time"] = '0000-00-00 00:00:00';
        $data["last_add_contact_time"] = '0000-00-00 00:00:00';
        $data["add_way"] = 'hand';

        //新增CRM数据
        if ($this->db->insert('biz_client_crm', $data)) {
            $return['code'] = 1;
            $id = $this->db->insert_id();
            // 记录日志
            $this->write_log($id, 'insert', $data);
        } else {
            $return['msg'] = "Query failed！";
        }

        //返回操作结果
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * 把指定抬头设为黑名单，抬头不存在则创建黑名单
     */
    public function create_black_list(){
        $return = array('code'=>0, 'msg'=>'');
        $_post = $this->input->post();
        $company_name = $this->input->post('company_name');
        $black_list_cause = '从工具箱批量导入黑名单。';

        if (get_session('id') != 20001){
            $return['code'] = 403;
            $return['msg'] = '当前账号无权导入黑名单！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        if (!empty(trim($company_name))){
            $company_search = match_chinese($company_name);
            $crm = $this->db->select('id')->where('company_search', $company_search)->get('biz_client_crm')->result_array();
            //如果抬头存在则状态改为黑名单，否则添加一条状态为黑名单的抬头
            if (!empty($crm)) {
                $data = array('apply_status' => -99, 'black_list_cause' => $black_list_cause, 'black_list_time' => date('Y-m-d H:i:s'));
                if ($this->db->where('company_search', $company_search)->update('biz_client_crm', $data)) {
                    $return['code'] = 1;

                    //创建日志
                    foreach ($crm as $item){
                        $this->write_log($item['id'], 'update', $data);
                        create_crm_log($item['id'], 0, get_session('id'), $black_list_cause);
                    }
                }else{
                    $return['msg'] = '操作失败：Query failed！';
                }
            }else{
                //创建写入字段
                $data = $this->create_date($_post);
                $data['client_name'] = strtoupper($data['client_name']);
                $data['company_name'] = strtoupper($data['company_name']);
                $data['company_name_en'] = strtoupper($data['company_name_en']);
                $data['company_search'] = $company_search;
                $data["approve_user"] = 0;
                $data["created_group"] = implode(',', $this->session->userdata('group') );
                $data["apply_type"] = 1;
                $data["apply_status"] = -99;
                $data['black_list_cause'] = $black_list_cause;
                $data['black_list_time'] = date('Y-m-d H:i:s');
                $data["created_by"] = get_session('id');
                $data["created_time"] = date('Y-m-d H:i:s');
                $data["last_follow_time"] = '0000-00-00 00:00:00';
                $data["last_add_contact_time"] = '0000-00-00 00:00:00';
                $data["add_way"] = 'import';
                //新增CRM数据
                if ($this->db->insert('biz_client_crm', $data)) {
                    $return['code'] = 1;
                    $id = $this->db->insert_id();
                    // 记录日志
                    $this->write_log($id, 'insert', $data);
                } else {
                    $return['msg'] = "Query failed！";
                }
            }
        }else{
            $return['msg'] = '抬头不能为空！';
        }

        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * 该页面是销售们的crm编辑页面
     * @param int $crm_id
     */
    public function edit($crm_id = 0)
    {
        //请求类型参数，不同的类型使用不同的视图，分为编辑(edit)和审批,默认为编辑，
        $request_type = isset($_GET['type']) ? $_GET['type'] : 'edit';
        setLang('biz_client');//设置使用client的语言包
        Model('biz_client_crm_model');
        $data = $this->biz_client_crm_model->get_where_one("id = $crm_id"); // 获取crm相关数据
        if (empty($data)) exit('参数错误');

        //获取审批事务
        $where = array('id_no' => $crm_id, 'status'=>0);
        $workflow = $this->db->where($where)->where_in('id_type', array(1,2))->get('biz_workflow')->row_array();

        //if(目的是编辑)
        if ($request_type == 'edit') {
            //if(不是管理员 && 不是当前销售或不是销售的领导) 不允许编辑
            if (!is_admin() && !in_array($data['sales_id'], get_session('user_range')) && $data['created_by']!=get_session('id')) {
                return $this->load->view('errors/html/error_layui', array('msg' => '无权编辑此信息！'));
            }
        }else{ //如果是审批请求 if(没有正在审批中的事务 || 不是审批人)
            $login_user_id = get_session('id');
            $user_range = get_session('user_range');

            //审批人由指定审批人和指定审批人的上级组成
            $operation_id = explode(',', $workflow['operation_id']);
            $exists = array_intersect($user_range, $operation_id);
            //如果当前账号是指定审批人的上级，则当前账号也拥有审批权
            if (!empty($exists)){
                array_push($operation_id, $login_user_id);
            }

            if (empty($workflow) || !in_array($login_user_id, $operation_id)){
                return $this->load->view('errors/html/error_layui', array('msg' => '无权审批此信息！'));
            }
        }
        //crm 目前先全开放,不限制权限
        $G_userBscEdit = $this->biz_client_crm_model->get_field_edit();
        $data['G_userBscEdit'] = $G_userBscEdit;

        //查询该公司是否3个月内有过交易
        $where = array('biz_client.company_search'=>$data['company_search']);
        $this->db->select('biz_client.client_code,biz_client.company_search,biz_shipment.booking_ETD');
        $this->db->where($where)->or_where('biz_client.company_name', $data['company_name']);
        $this->db->join('biz_shipment', 'biz_shipment.client_code = biz_client.client_code and status="normal"', 'LEFT');
        $this->db->order_by('biz_shipment.booking_ETD', 'desc')->limit(1);
        $client = $this->db->get('biz_client')->row_array();
        $data['is_allow'] = true;
        if ($client){
            //如果3个月内有过交易,不允许操作
            if (strtotime('+3 month', strtotime($client['booking_ETD'])) > time()){
                $data['is_allow'] = false;
            }
        }

        //print_r($client);
        $this->load->vars('workflow', $workflow);
        $this->load->view('head');

        if ($request_type == 'edit') {
            $this->load->view('biz/crm/edit_crm_view', $data);
        }else{
            $this->load->view('biz/crm/review_view', $data);
        }
    }

    public function save_data(){
        $return = array('code'=>0, 'msg'=>'');
        $_post = $this->input->post();
        $id = (int)$this->input->get('id');
        $crm = $this->db->select('apply_status')->where('id', $id)->get('biz_client_crm')->row_array();
        if (!$crm){
            $return['msg'] = '参数错误！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        //校验提交
        $validate = $this->validate($_post, 'edit');
        if ($validate !== true){
            $return['msg'] = $validate;
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        //创建写入字段
        $data = $this->create_date($_post);
        $data['client_name'] = strtoupper($data['client_name']);
        $data['company_name'] = strtoupper($data['company_name']);
        $data['company_name_en'] = strtoupper($data['company_name_en']);
        $data['company_search'] = match_chinese($data['company_name']);
        $data["approve_user"] = getUserField($data['sales_id'], 'leader_id');
        $data["created_group"] = implode(',', $this->session->userdata('group') );
        $data["updated_by"] = get_session('id');
        $data["updated_time"] = date('Y-m-d H:i:s');
        $data["is_new"] = 0;
        if ($crm["apply_status"]==-1) $data["apply_status"] = 1;

        //新增CRM数据
        if ($this->db->where('id', $id)->update('biz_client_crm', $data)) {
            $return['code'] = 1;
            // 记录日志
            $this->write_log($id, 'update', $data);
        } else {
            $return['msg'] = "Query failed！";
        }

        //返回操作结果
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * crm客户申请转正页面
     * 2022-02-24 原本为client填写数据后,自动将原本的客户apply_status改为3(待审核)
     */
    public function apply_formal()
    {
        Model('biz_client_crm_model');
        setLang('biz_client');
        $crm_id = getValue('crm_id');

        $client_crm = $this->biz_client_crm_model->get_one('id', $crm_id);
        if (empty($client_crm)) exit(lang('CRM申请不存在'));

        //申请状态感觉应该不用判定是否有权限修改的了
        $data = $client_crm;
        $data['crm_id'] = $crm_id;
        $field = $this->biz_client_crm_model->get_field_edit();
        foreach ($field as $item) {
            if (!isset($data[$item])) $data[$item] = "";
            if (isset($data[$item]) && ($data[$item] == '0000-00-00' || $data[$item] == '0000-00-00 00:00:00')) $data[$item] = null;
        }

        $data['G_userBscEdit'] = $field;
        $data = array_merge($data, Model('biz_client_duty_model')->get_client_data_one($data['client_code']));
        $data['sales_ids'] = $client_crm['sales_id'];
        $data['leader_id'] = getUserField($client_crm['sales_id'], 'leader_id');
        $data['crm_apply_status'] = $client_crm['apply_status'];

        $this->load->view('head');
        $this->load->view('/biz/crm/apply_formal_view', $data);
    }

    /**
     * 创建转正申请
     */
    public function create_apply_data()
    {
        $return = array('code'=>0, 'msg'=>'');
        $_post = $this->input->post();
        $crm_id = (int)$this->input->post('crm_id');
        if ($crm_id < 1) {
            $return['msg'] = '参数错误！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }else {
            $row = $this->db->where('id', $crm_id)->get('biz_client_crm')->row_array();
            if (empty($row)) {
                $return['msg'] = '参数错误！';
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }
        }

        if ($row['apply_status'] != 2) {
            $return['msg'] = '当前客户不满足可申请条件！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        //校验
        $validate = function($_post, $row){
            if (!isset($_post['operator_id']) || (int)$_post['operator_id'] < 1) {
                return "必须选择操作人员！";
            }

            $validate_field = [
//                'bank_name_cny'=>'银行名称',   //考虑到国外公司没那么多信息可以填
//                'bank_account_cny'=>'银行帐号',
//                'taxpayer_name'=>'纳税人名称 ',
//                'taxpayer_id'=>'纳税人识别号',
//                'taxpayer_address'=>'纳税人地址',
//                'taxpayer_telephone'=>'纳税人电话',
            ];
            foreach ($validate_field as $key => $val) {
                if (!isset($_post[$key]) || $_post[$key] == '') {
                    return "{$val}必须填写！";
                }
            }
            if ($row['sales_id'] == '' || $row['sales_id'] != get_session('id')){
                return "当前账号无权执行转正申请！";
            }

            $contact = $this->db->select('crm_id, role')->where('crm_id', $row['id'])->get('biz_client_contact_list')->result_array();
            if (empty($contact)){
                return "请到Contact list设置联系人！";
            }

            $client_contact_role = array_column($contact, 'role');
            $unique_contact_role = filter_unique_array(explode(',', join(',', $client_contact_role)));
            $this_role_arr = explode(',', $row['role']);
            //这里将差集获取到,然后提示应该添加这些角色的联系人
            $diff_role = array_diff($this_role_arr, $unique_contact_role);
            if(sizeof($diff_role) > 0){
                return "请添加角色" . join(',', $diff_role) . "等的相关联系人后再试";
            }
            return true;
        };

        $validate = $validate($_post, $row);
        if ($validate !== true){
            $return['msg'] = $validate;
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        //查询该抬头在往来单位中是否已经存在
        $company_search = match_chinese($row['company_name']);
        $client = $this->db->where('company_search', $company_search)->get('biz_client')->row_array();
        $old_sales = $old_sales_leader = array();
        //如果已经存在
        if (!empty($client)){
            //如果3个月内有过交易则不允许转正
            if ($this->client->is_active($client['client_code']) === true){
                $return['msg'] = "“{$row['company_name']}”在3个月内有过交易记录！";
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            //如果关联公司中有活跃客户则不允许转正
            if ($this->client->is_active($this->client->get_relation_company($client['client_code'])) === true){
                $return['msg'] = "该抬头在往来单的关联公司中有活跃客户！";
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            //获取往来单位中相同抬头的销售
            $old_sales = $this->client->get_client_sales($client['client_code']);
            if (count($old_sales) > 3){
                $return['msg'] = "销售人员不能超过三个！";
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            //获取多个销售领导中级别最高的一个
            $old_sales_leader = $this->client->get_user_level_max(array_column($old_sales, 'leader_id'));
        }

        $data = array(
            'operator_id'   => $_post['operator_id'],
            'service_id'   => $_post['service_id'],
            'bank_name_cny'   => $_post['bank_name_cny'],
            'bank_account_cny'   => $_post['bank_account_cny'],
            'bank_address_cny'   => $_post['bank_address_cny'],
            'bank_name_usd'   => $_post['bank_name_usd'],
            'bank_account_usd'   => $_post['bank_account_usd'],
            'bank_address_usd'   => $_post['bank_address_usd'],
            'bank_swift_code_usd'   => $_post['bank_swift_code_usd'],
            'taxpayer_name'   => $_post['taxpayer_name'],
            'taxpayer_id'   => $_post['taxpayer_id'],
            'taxpayer_address'   => $_post['taxpayer_address'],
            'taxpayer_telephone'   => $_post['taxpayer_telephone'],
            'invoice_email'   => $_post['invoice_email'],
            'dn_accounts'   => $_post['dn_accounts'],
            'dn_title'   => $_post['dn_title'],
        );
        $query = $this->db->where('id', $crm_id)->update('biz_client_crm', $data);
        if ($query){
            //获取当前审批人
            $operation_id = get_leader_row($row['sales_id'])['id'];
            //todo 创建转正审批事务
            //if (有原销售领导 && 当前销售的领导 ！= 原销售的领导) 创建二级审批并生成审批事务的私有配置
            if (!empty($old_sales_leader) && $operation_id != $old_sales_leader['id']) {
                //获得当前事务审批的默认配置
                $this->load->config('approval');
                $config = $this->config->item('approval')[2];

                $config['step2'] = $config['step1'];
                $config['step1']['approval_user'] = array($operation_id => getUserName($operation_id));
                $config['step2']['approval_user'] = array($old_sales_leader['id'] => $old_sales_leader['name']);
                $config['step1']['callback'] = '';
            }

            $remark = $row['company_name'];
            if (!empty($old_sales)){
                $remark .= '<br><font color="red">原销售：' . join('，', array_column($old_sales, 'name')) . '</font>';
            }
            $contact = $this->db->select('telephone, telephone2, email')->where("crm_id = {$crm_id} and (email <> '' or telephone <> '')")->get('biz_client_contact_list')->row_array();
            if(!empty($contact)){
                $remark .= '<br><font color="#1e90ff"> 手机号：' . $contact['telephone'] . '</font><br><font color="#1e90ff">座机号：' . $contact['telephone2'] . '</font><br><font color="#d2691e">邮箱：' . $contact['email'] . '</font>';
            }
            $params = array(
                'id_no' => $crm_id,
                'id_type' => 2,
                'operation_id'=>$operation_id,
                'callback_param'=>'',
                'config' => isset($config) ? json_encode($config, 256) : '',
                'remark' => $remark
            );
            $this->load->library('approval', $params);
            if ($this->approval->get_runStatus() === false){
                $return['msg'] = $this->approval->error_msg;
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }else{
                Model('biz_client_crm_model');
                $client_crm_update = array('apply_status' => 3);
                $this->biz_client_crm_model->update($crm_id, $client_crm_update);
            }

            $return['code'] = 1;
        }else{
            $return['msg'] = 'Query failed';
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    //所有转交功能都有个前提的权限限制，只能转交下级的客户给另外1个下级
    public function deliver_auth_check($uid)
    {
        if (get_session("id") == $uid) return 1;  //自己是自己的下属
        $user_range = get_session("user_range");
        if (in_array($uid, $user_range)) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * 转交销售，
     */
    public function deliver_client()
    {
        $id = postValue('id', '');
//        $from_user_id = intval(postValue('from_user_id', 0));
        //新版来源改成自己
        $to_user_id = intval(postValue('to_user_id', 0));
        $close_plan_date = postValue('close_plan_date', '');
        $is_inactve = postValue('is_inactve', 'false');

        $to_user_id = intval(postValue('to_user_id', 0));
        $close_plan_date = postValue('close_plan_date', '');
        Model('biz_client_crm_model');

        if (!$this->deliver_auth_check($to_user_id)) return jsonEcho(array('code' => 1, 'msg' => '转交人不是你的下属'));

        $this->db->select('id, sales_id,client_code');
        $client_crm = $this->biz_client_crm_model->get_one('id', $id);
        if (empty($client_crm)) return jsonEcho(array('code' => 1, 'msg' => 'CRM不存在'));

        if (!$this->deliver_auth_check($client_crm['sales_id'])) return jsonEcho(array('code' => 1, 'msg' => '这条数据不属于你的权限范围'));

        //修改销售为指定销售
        $client_crm_update = array();
        $client_crm_update['sales_id'] = $to_user_id;
        $client_crm_update['client_source'] = get_session('name');
        $client_crm_update['from_user_id'] = get_session('id');
        $client_crm_update['close_plan_date'] = $close_plan_date;
        $this->biz_client_crm_model->update($client_crm['id'], $client_crm_update);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_client_crm";
        $log_data["key"] = $client_crm['id'];
        $log_data["action"] = "deliver_update";
        $log_data["value"] = json_encode($client_crm_update);
        log_rcd($log_data);

        // $add_crm = $this->add_crm($client_crm['client_code'], $to_user_id, $close_plan_date, true);
        // if($add_crm['code'] == '1'){
        //     return jsonEcho($add_crm);
        // }

        return jsonEcho(array('code' => 0, 'msg' => '转交成功'));
    }

    /**
     * 新增crm数据
     * @param $client_code
     * @param $to_user_id
     * @param $close_plan_date
     * @param bool $is_send_mail
     * @param string $is_inactve
     * @param bool $is_add_crm
     * @return array
     */
    private function add_crm($client_code, $to_user_id, $close_plan_date, $is_send_mail = false, $is_inactve = 'false', $is_add_crm = true)
    {
//        $from_user_id = intval(postValue('from_user_id', 0));
        //新版来源改成自己
        if (!$this->deliver_auth_check($to_user_id)) return array('code' => 1, 'msg' => '转交人不是你的下属');

        $from_user_id = get_session('id');

        if (!$client_code || !$from_user_id || !$to_user_id || !$close_plan_date) return array('code' => 1, 'msg' => '参数错误');

        if ($from_user_id == $to_user_id) return array('code' => 1, 'msg' => '转交人不能是同一个');
        if (empty($close_plan_date) || $close_plan_date == '0000-00-00') return array('code' => 1, 'msg' => '指派结束时间不能为空');

        //将当前的client指定销售给修改为新的销售
        Model('biz_client_model');
        $this->db->select('id,client_code,sales_ids,company_name,export_sailing,trans_mode,product_type,product_details,client_source,apply_remark,approve_user,role,apply_type');
        $client = $this->biz_client_model->get_one('client_code', $client_code);
        if (empty($client)) return array('code' => 1, 'msg' => '客户不存在');

        // 销售必须是当前用户的下属才行
        $is_user_range = 0;
        //获得该客户的所有销售人员
        $client_duty = Model('biz_client_duty_model')->get_client_data(array_column(array($client), 'client_code'));
        $sales_id = array_column($client_duty, 'sales_ids');
        //销售可能不是一个人，需要拆分
        if (!empty($sales_id)) {
            $sales_id_array = explode(',', $sales_id[0]);
            foreach ($sales_id_array as $sid) {
                if ($this->deliver_auth_check($sid)) {
                    $is_user_range = 1;
                }
            }
        }
        if (!$is_user_range) return array('code' => 1, 'msg' => '这条数据不属于你的权限范围');


        $client_update = array();
        Model('biz_client_crm_model');
        Model('biz_client_follow_up_model');

        //2021-12-13 申请状态置为4
        //2022-02-25 这里申请状态就不修改了 改为新增一条client_crm 数据 给对应的销售,如果

        if ($is_add_crm) {
            $add_crm_data = array(
                'client_code' => $client['client_code'],
                'export_sailing' => $client['export_sailing'],
                'trans_mode' => $client['trans_mode'],
                'product_type' => $client['product_type'],
                'product_details' => $client['product_details'],
                'close_plan_date' => $close_plan_date,
                'client_source' => get_session('name'),
                'apply_remark' => $client['apply_remark'],
                'from_user_id' => get_session('id'),
                'approve_user' => get_session('id'),//审核人改为当前点击的
                'role' => $client['role'],
                'apply_type' => 1,
                'apply_status' => 2, // 这里状态进入哪里待讨论,目前直接到2
                'sales_id' => $to_user_id,
            );

            //开启事务
            $this->db->trans_begin();

            $crm_id = $this->biz_client_crm_model->save($add_crm_data);
            $where = array('client_code'=>$client['client_code'], 'user_role'=>'sales');
            //踢出正式（转交到CRM）客户后，原来跟进的销售人员全部删除
            $this->db->where($where)->delete('biz_client_duty');
            //提交事务||回滚
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                return array('code' => 1, 'msg' => '转交失败，存储错误!');
            }else{
                $this->db->trans_commit();
            }

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_client_crm";
            $log_data["key"] = $crm_id;
            $log_data["action"] = "insert";
            $log_data["value"] = json_encode($add_crm_data);
            log_rcd($log_data);
        }

        //$client_update['apply_status'] = 4;
        //如果是活跃,那么将当前客户改为潜在
        if ($is_inactve === 'true') {
            $client_update['client_level'] = 0;

            //将非活跃的客户,状态置为1
            $sql = "update biz_client_inactve set is_inactve = 1 where client_code = '{$client['client_code']}'";
            Model('m_model');
            $this->m_model->sqlquery($sql);

            //2022-06-02 转交非活跃客户时,联系人一起移动到CRM里
            if ($is_add_crm) {
                Model('Biz_client_contact_model');
                $this->db->select('id');
                $contact_list = $this->Biz_client_contact_model->get("client_code = '{$client['client_code']}'");

                foreach ($contact_list as $contact) {
                    $contact_update = array();
                    $contact_update['crm_id'] = $crm_id;
                    $contact_update['client_code'] = '';
                    $contact_update["create_by"] = $to_user_id;

                    $this->Biz_client_contact_model->update($contact['id'], $contact_update);

                    // record the log
                    $log_data = array();
                    $log_data["table_name"] = "biz_client_contact_list";
                    $log_data["key"] = $contact['id'];
                    $log_data["action"] = "deliver_update";
                    $log_data["value"] = json_encode($contact_update);
                    log_rcd($log_data);
                }
            }
        }
        if (!empty($client_update)) {
            $this->biz_client_model->update($client['id'], $client_update);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_client";
            $log_data["key"] = $client['id'];
            $log_data["action"] = "deliver_update";
            $log_data["value"] = json_encode($client_update);
            log_rcd($log_data);
        }

        // $follow_up_data = array();

        // $follow_up_data['client_code'] = $client['client_code'];
        // $follow_up_data['from_user_id'] = $from_user_id;
        // $follow_up_data['to_user_id'] = $to_user_id;
        // $follow_up_data['close_plan_date'] = $close_plan_date;

        // $follow_up_id = $this->biz_client_follow_up_model->save($follow_up_data);

        // // record the log
        // $log_data = array();
        // $log_data["table_name"] = "biz_client_follow_up";
        // $log_data["key"] = $follow_up_id;
        // $log_data["action"] = "insert";
        // $log_data["value"] = json_encode($follow_up_data);
        // log_rcd($log_data);

        //生成邮件任务给指派人和接收人
        if ($is_send_mail) {
            $send_users = array($to_user_id, get_session('id'));
            Model('bsc_user_model');
            Model('biz_market_mail_model');
            $this->db->select('id,name');
            $users = array_column($this->bsc_user_model->get("id in ({$from_user_id}," . join(',', $send_users) . ")"), null, 'id');
            $to_user_name = $users[$to_user_id]['name'];
            $from_user_name = $users[$from_user_id]['name'];
            foreach ($send_users as $send_user) {
                $add_data = array();
                $add_data['recipient_by'] = $send_user;
                $add_data['plan_send_time'] = date('Y-m-d H:i:s');
                $add_data['actual_send_time'] = '0000-00-00 00:00:00';
                $add_data['category'] = '客户跟踪';
                $add_data['subject'] = $client['company_name'] . "已由" . $from_user_name . "指派给" . $to_user_name;
                $add_data['content'] = $add_data['subject'];
                $add_data['auto_content'] = '';

                Model('biz_market_mail_model');
                $this_id = $this->biz_market_mail_model->save_auto($add_data);
            }
        }

        return array('code' => 0, 'msg' => '转交成功');
    }

    /**
     * 批量转交销售
     */
    public function batch_deliver_client()
    {
        $return = array('code'=>0, 'msg'=>'');
        $ids = postValue('ids', '');
        $sale_id = intval(postValue('sale_id', 0));
        $end_date = postValue('end_date', '');

        $ids_arr = filter_unique_array(explode(',', $ids));
        if (empty($ids_arr)) {
            $return['msg'] = '参数错误！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        };

        if (get_session('id') != 20001) {
            if (!$this->deliver_auth_check($sale_id)) {
                $return['msg'] = '转交人不是你的下属！';
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }
        }

        $msg_ext = array();
        $success = 0;
        $fail = 0;
        $client_crms = $this->db->select('id, sales_id, company_name, client_source, role')->where_in('id', explode(',', $ids))->get('biz_client_crm')->result_array();
        if (!empty($client_crms)) {
            foreach ($client_crms as $client_crm) {
                //如果（不是田诗雯 && 当前已经有销售人员 && 当前销售人员不是登录账号的下级）
                if (get_session('id') != 20001 && $client_crm['sales_id'] > 0 && !$this->deliver_auth_check($client_crm['sales_id'])) {
                    $msg_ext[] = "失败理由:超权限范围";
                    $fail++;
                    continue;
                }

                //修改销售为指定销售
                $client_crm_update = array();
                if ($client_crm['client_source'] =='') $client_crm_update['client_source'] = '其它';
                $client_crm_update['sales_id'] = $sale_id;
                $client_crm_update['is_new'] = 1;
                $client_crm_update['from_user_id'] = get_session('id');
                //$client_crm_update['close_plan_date'] = $end_date;
                $client_crm_update['assign_date'] = date('Y-m-d');
//                $client_crm_update['apply_status'] = 1;
//                $client_crm_update['apply_type'] = 1;
                $client_crm_update['approve_user'] = getUserField($sale_id, 'leader_id');
                $query = $this->db->where('id', $client_crm['id'])->update('biz_client_crm', $client_crm_update);
                //echo $this->db->last_query();
                if ($query) {
                    //同时把联系人转给销售
                    $this->db->where('crm_id', $client_crm['id'])->update('biz_client_contact_list', array('create_by'=>$sale_id, 'role'=>$client_crm['role']));

                    //创建客户流转日志
                    $remark = '指派销售为 “' . getUserName($sale_id) . '“';
                    create_crm_log($client_crm['id'], $client_crm['sales_id'], get_session('id'), $remark);

                    // record the log
                    $log_data = array();
                    $log_data["table_name"] = "biz_client_crm";
                    $log_data["key"] = $client_crm['id'];
                    $log_data["action"] = "deliver_update";
                    $log_data["value"] = json_encode($client_crm_update);
                    log_rcd($log_data);
                    $success++;
                }
            }
            $msg_ext_str = join('<br/>', $msg_ext);
            //$return['msg'] = "转交成功, 成功{$success}, 失败{$fail}, " . $msg_ext_str;
            if ($success > 0) {
                $return['code'] = 1;
            }else{
                $return['msg'] = $msg_ext_str;
            }
        }else{
            $return['msg'] = "转交客户不存在!";
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * 我的指派
     */
    public function my_appoint()
    {
        $data = array();

        $this->load->view('/biz/crm/my_appoint', $data);
    }

    /**
     * 我的指派数据
     */
    public function get_appoint_data()
    {
        $result = array('code' => 0, 'msg' => 'success', 'rows' => array(), 'total' => 0);

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        $f_role = isset($_REQUEST['f_role']) ? $_REQUEST['f_role'] : '';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $crm_user = isset($_COOKIE['crm_user']) ? $_COOKIE['crm_user'] : get_session('id');
        $crm_user_type = isset($_COOKIE['crm_user_type']) ? $_COOKIE['crm_user_type'] : 'user';


        //where---start
        $ThreeMonthsAgo = date('Y-m-d', strtotime('-3 month'));
        $where = array();
        $where[] = "client_level >= 0";
        // if($crm_user_type == 'user'){
        $where[] = "biz_client_crm.from_user_id = " . get_session('id');
        // }else if($crm_user_type == 'group'){
        //     $where[] = "(select `group` from bsc_user where biz_client_crm.from_user_id = bsc_user.id) = '$crm_user'";
        // }else if($crm_user_type == 'sub_company'){
        //     $this_crm_user = str_replace('company_', '', $crm_user);
        //     $where[] = "(select `group` from bsc_user where biz_client_crm.from_user_id = bsc_user.id) like '{$this_crm_user}-%'";
        // }else {
        //     $where[] = '0 = 1';
        // }
        //用户限制
        if (!empty($fields)) {
            foreach ($fields['f'] as $key => $field) {
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if ($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=  
                $date_field = array('created_time', 'updated_time');
                if (in_array($f, $date_field) && $s == '=') {
                    if ($v != '') {
                        $where[] = "{$pre}{$f} >= '$v 00:00:00' and {$pre}{$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                if ($f == 'company_name') {
                    $f = 'company_search';
                    $v = match_chinese($v);
                }
                if ($f == 'sales_id') {
                    $f = '(select name from bsc_user where bsc_user.id = sales_id)';
                    if ($v != '') $where[] = search_like($f, $s, $v);
                } else {
                    if ($v != '') $where[] = search_like('biz_client.' . $f, $s, $v);
                }

            }
        }
        if ($f_role != '') $where[] = "role like '%$f_role%'";
        $where = join(' and ', $where);
        //where---end

        Model('biz_client_crm_model');

        $offset = ($page - 1) * $rows;
        // $this->db->join("biz_client_follow_up", 'biz_client_follow_up.client_code = biz_client.client_code and status = 0', 'RIGHT');
        $this->db->join("biz_client", 'biz_client.client_code = biz_client_crm.client_code', 'LEFT');
        $result["total"] = $this->biz_client_crm_model->total($where);
        // $this->db->select('biz_client.id, tag, biz_client.client_code, client_name, province, city, company_address, contact_name, contact_telephone, contact_telephone2, contact_email, DATEDIFF(NOW(), biz_client_follow_up.created_time) as follow_up_day, biz_client_follow_up.id as biz_client_follow_up_id', false);
        // $this->db->join("biz_client_follow_up", 'biz_client_follow_up.client_code = biz_client.client_code and status = 0', 'RIGHT');
        $this->db->join("biz_client", 'biz_client.client_code = biz_client_crm.client_code', 'LEFT');
        $this->db->limit($rows, $offset);
        $rs = $this->biz_client_crm_model->get($where, 'biz_client.' . $sort, $order);

        foreach ($rs as $row) {
            $result['rows'][] = $row;
        }

        echo json_encode($result);
    }

    /**
     * 撤销指派
     */
    public function cancel_follow()
    {
        return jsonEcho(array('code' => 1, 'msg' => '该功能已禁用'));

        $id = postValue('id', '');
        $result = array('code' => 1, 'msg' => '参数错误');

        if (empty($id)) {
            echo json_encode($result);
            return;
        }

        Model('biz_client_follow_up_model');
        $this->db->select('id,client_code,from_user_id, to_user_id,status');
        $old_row = $this->biz_client_follow_up_model->get_where_one("id = $id");
        if (empty($old_row)) {
            echo json_encode($result);
            return;
        }
        $from_user_id = $old_row['from_user_id'];
        $to_user_id = $old_row['to_user_id'];

        if ($old_row['status'] != 0) {
            echo json_encode(array('code' => 1, 'msg' => '该指派已结束，撤销失败'));
            return;
        }
        //将当前的结束，然后将to_user替换回from_user

        $this->db->select('id,sales_ids');
        $client = $this->biz_client_model->get_where_one("client_code = '{$old_row['client_code']}'");

        $client_update = array();

        //由于新加了个保持原本销售的,所以这里去重一下 join(',', filter_unique_array(explode(',',  是为了转数组去重,要改为直接字符串也可以

        $client_update['sales_ids'] = join(',', filter_unique_array(explode(',', str_replace($to_user_id, $from_user_id, $client['sales_ids']))));
        //2021-12-13 申请状态置为4
        $client_update['apply_status'] = 0;
        if ($client_update['sales_ids'] != $client['sales_ids']) {
            $this->biz_client_model->update($client['id'], $client_update);

            // record the log
            $log_data = array();
            $log_data["table_name"] = "biz_client";
            $log_data["key"] = $client['id'];
            $log_data["action"] = "deliver_update";
            $log_data["value"] = json_encode($client_update);
            log_rcd($log_data);
        }
        $follow_up_update = array();
        $follow_up_update['status'] = 1;
        $this->biz_client_follow_up_model->update($old_row['id'], $follow_up_update);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_client_follow_up";
        $log_data["key"] = $old_row['id'];
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($client_update);
        log_rcd($log_data);
        $result['code'] = 0;
        $result['msg'] = '撤销指派完成';
        echo json_encode($result);
    }


    public function DealNumber()
    {
        $data = array();

        $data['crm_user'] = getValue('crm_user', get_session('id'));
        $data['crm_user_type'] = getValue('crm_user_type', 'user');
        $data['crm_date_first'] = getValue('crm_date_first', date('Y-m-d'));
        $data['crm_date_end'] = getValue('crm_date_end', date('Y-m-d'));
        $this->yz_token();

        $this->load->view('/biz/crm/list/DealNumber', $data);
    }

    public function DealOne()
    {
        $data = array();

        $data['crm_user'] = getValue('crm_user', get_session('id'));
        $data['crm_user_type'] = getValue('crm_user_type', 'user');
        $data['crm_date_first'] = getValue('crm_date_first', date('Y-m-d'));
        $data['crm_date_end'] = getValue('crm_date_end', date('Y-m-d'));
        $this->yz_token();

        $this->load->view('/biz/crm/list/DealOne', $data);
    }

    public function DealMore()
    {
        $data = array();

        $data['crm_user'] = getValue('crm_user', '');
        $data['crm_user_type'] = getValue('crm_user_type', '');
        $data['crm_date_first'] = getValue('crm_date_first', '');
        $data['crm_date_end'] = getValue('crm_date_end', '');
        $this->yz_token();

        $this->load->view('/biz/crm/list/DealMore', $data);
    }

    public function TotalNumber()
    {
        $data = array();

        $data['crm_user'] = getValue('crm_user', get_session('id'));
        $data['crm_user_type'] = getValue('crm_user_type', 'user');
        $data['crm_date_first'] = getValue('crm_date_first', date('Y-m-d'));
        $data['crm_date_end'] = getValue('crm_date_end', date('Y-m-d'));
        $this->yz_token();

        $this->load->view('/biz/crm/list/TotalNumber', $data);
    }

    public function CrmClientNewAdd()
    {
        $data = array();

        $data['crm_user'] = getValue('crm_user', get_session('id'));
        $data['crm_user_type'] = getValue('crm_user_type', 'user');
        $data['crm_date_first'] = getValue('crm_date_first', date('Y-m-d'));
        $data['crm_date_end'] = getValue('crm_date_end', date('Y-m-d'));
        $this->yz_token();

        $this->load->view('/biz/crm/list/CrmClientNewAdd', $data);
    }

    public function get_client_list()
    {
        $result = array('code' => 0, 'msg' => 'success', 'rows' => array(), 'total' => 0);

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        $f_role = isset($_REQUEST['f_role']) ? $_REQUEST['f_role'] : '';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        // $crm_user = isset($_COOKIE['crm_user']) ? $_COOKIE['crm_user'] : get_session('id');
        // $crm_user_type = isset($_COOKIE['crm_user_type']) ? $_COOKIE['crm_user_type'] : 'user';
        // $crm_date_first = isset($_COOKIE['crm_date_first']) ? $_COOKIE['crm_date_first'] : date('Y-m-d');
        // $crm_date_end = isset($_COOKIE['crm_date_end']) ? $_COOKIE['crm_date_end'] : date('Y-m-d');
        $crm_user = postValue('crm_user', get_session('id'));
        $crm_user_type = postValue('crm_user_type', 'user');
        $crm_date_first = postValue('crm_date_first', date('Y-m-d'));
        $crm_date_end = postValue('crm_date_end', date('Y-m-d'));
        $type = isset($_GET['type']) ? $_GET['type'] : '';

        //当前总客户数
        $month_first_day = $crm_date_first;
        $month_end_day = $crm_date_end;

        //where---start
        $where = array();

        if ($crm_user_type == 'user') {
            $sales_ids_where = "biz_client_duty.user_id = '{$crm_user}'";
        } else if ($crm_user_type == 'group') {
            $sales_ids_where = "biz_client_duty.user_group = '{$crm_user}' and biz_client_duty.user_role = 'sales'";
        } else if ($crm_user_type == 'sub_company') {
            $crm_user = str_replace('company_', '', $crm_user);
            $groups = Model('bsc_group_model')->get_child_group($crm_user);
            $groups_code = array_column($groups, 'group_code');
            $sales_ids_where = "biz_client_duty.user_group in ('" . join('\',\'', $groups_code) . "') and biz_client_duty.user_role = 'sales'";

        } else if ($crm_user_type == 'head') {
            if ($crm_user == 'HEAD') {
                $groups = Model('bsc_group_model')->get_child_group($crm_user);
                $groups_code = array_column($groups, 'group_code');
                $sales_ids_where = "biz_client_duty.user_group in ('" . join('\',\'', $groups_code) . "') and biz_client_duty.user_role = 'sales'";
            }
        } else {
            $sales_ids_where = '0 = 1';
        }

        $where[] = Model('biz_client_model')->get_read_role_where($sales_ids_where);

        Model('m_model');
        if ($type == 'DealNumber') {
            $sql = "select biz_shipment.client_code from biz_shipment where biz_shipment.booking_ETD >= '$month_first_day 00:00:00' and biz_shipment.booking_ETD <= '$month_end_day 23:59:59' GROUP BY biz_shipment.client_code";
            $client_codes = array_column($this->m_model->query_array($sql), 'client_code');
            if (empty($client_codes)) $client_codes[] = 0;
            $where[] = "client_code in ('" . join('\',\'', $client_codes) . "')";
        } else if ($type == 'DealOne') {
            $sql = "select client_code from (select client_code,count(*) as n, biz_shipment.created_time from biz_shipment GROUP BY client_code HAVING n = 1) a where a.created_time >= '$month_first_day 00:00:00' and a.created_time <= '$month_end_day 23:59:59'";
            // $sql = "select client_code,count(*) as n from biz_shipment GROUP BY client_code HAVING n = 1";
            $client_codes = array_column($this->m_model->query_array($sql), 'client_code');
            if (empty($client_codes)) $client_codes[] = 0;
            $where[] = "client_code in ('" . join('\',\'', $client_codes) . "')";
        } else if ($type == 'CrmClientNewAdd') {
            $where[] = "created_time >= '$month_first_day 00:00:00' and created_time <= '$month_end_day 23:59:59' and apply_type = 1";
        } else if ($type == 'TotalNumber') {
            //这种就不用管
        } else if ($type == 'DealMore') {
            $sql = "select client_code from biz_shipment where `status`='normal' GROUP BY client_code HAVING count(*) >= 1";
            $client_codes = array_column($this->m_model->query_array($sql), 'client_code');
            if (empty($client_codes)) $client_codes[] = 0;
            $where[] = "client_code in ('" . join('\',\'', $client_codes) . "')";
        } else {
            $where[] = "0 = 1";
        }

        //用户限制
        if (!empty($fields)) {
            foreach ($fields['f'] as $key => $field) {
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if ($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=  
                $date_field = array('created_time', 'updated_time');
                if (in_array($f, $date_field) && $s == '=') {
                    if ($v != '') {
                        $where[] = "{$pre}{$f} >= '$v 00:00:00' and {$pre}{$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                if ($f == 'company_name') {
                    $f = 'company_search';
                    $v = match_chinese($v);
                }
                if ($f == 'sales_id') {
                    $f = '(select name from bsc_user where bsc_user.id = sales_id)';
                    if ($v != '') $where[] = search_like($f, $s, $v);
                } else {
                    if ($v != '') $where[] = search_like('biz_client.' . $f, $s, $v);
                }

            }
        }
        if ($f_role != '') $where[] = "role like '%$f_role%'";
        $where = join(' and ', $where);
        //where---end

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->biz_client_model->no_role_total($where);
        $this->db->select('biz_client.id, tag, biz_client.client_code, client_name, company_name, province, city, company_address, contact_name, contact_telephone, contact_telephone2, contact_email,sales_ids', false);
        $this->db->limit($rows, $offset);
        $rs = $this->biz_client_model->no_role_get($where, 'biz_client.' . $sort, $order);

        //获取销售名称--start
        Model('bsc_user_model');
        $sales_ids = filter_unique_array(explode(',', join(',', array_column($rs, 'sales_ids'))));
        if (empty($sales_ids)) $sales_ids[] = 0;
        $this->db->select('id,name');
        $sales_users = array_column($this->bsc_user_model->get("id in (" . join(',', $sales_ids) . ")"), null, 'id');
        //获取销售名称--end

        foreach ($rs as $row) {
            if ($type == 'TotalNumber') {
                $this_sales_ids = filter_unique_array(explode(',', $row['sales_ids']));
                $row['sales_names'] = array();
                $row['sales'] = array();
                foreach ($this_sales_ids as $this_sales_id) {
                    if (isset($sales_users[$this_sales_id])) {
                        $row['sales_names'][] = $sales_users[$this_sales_id]['name'];
                        $row['sales'][] = $sales_users[$this_sales_id];
                    }
                }
                $row['sales_names'] = join(',', $row['sales_names']);
            }

            $result['rows'][] = $row;
        }

        echo json_encode($result);
    }

    // /**
    //  * 批量转交销售
    //  */
    // public function batch_deliver_client(){
    //     $client_codes = postValue('client_codes', '');
    //     $to_user_id = intval(postValue('to_user_id', 0));
    //     $client_codes_arr = filter_unique_array(explode(',', $client_codes));
    //     if(empty($client_codes_arr)) return jsonEcho(array('code' => 1, 'msg' => '参数错误'));

    //     Model('biz_client_model');
    //     Model('biz_client_crm_model');
    //     //获取客户相关数据
    //     $this->db->select('id,client_code,export_sailing,company_name,trans_mode,product_type,product_details,apply_remark,role,apply_type,operator_ids,customer_service_ids,sales_ids,marketing_ids,oversea_cus_ids,booking_ids,document_ids,finance_ids,read_user_group');
    //     $clients = $this->biz_client_model->get("client_code in ('" . join('\',\'', $client_codes_arr) . "')");
    //     if(empty($clients)) return jsonEcho(array('code' => 1, 'msg' => '不存在客户'));
    //     $userId = get_session('id');
    //     //这里进行循环处理
    //     foreach ($clients as $client){
    //         //1、清空客户的销售
    //         $client_update = array();
    //         $client_update['sales_ids'] = "";

    //         //这里将原本销售相关的组清除一下?
    //         $user_groups = $this->bsc_user_model->getUsersByDataVer2($client, array('operator_ids', 'customer_service_ids','sales_ids','marketing_ids','oversea_cus_ids','booking_ids','document_ids','finance_ids'), array('id', 'group'), 'group');
    //         //这里将组卸载
    //         if(in_array('all', explode(',', $client['read_user_group']))) $client_update['read_user_group'] = "all" . ',' . join(',', array_column($user_groups, 'group'));
    //         else $client_update['read_user_group'] = join(',', array_column($user_groups, 'group'));


    //         $this->biz_client_model->update($client['id'], $client_update);

    //         // record the log
    //         $log_data = array();
    //         $log_data["table_name"] = "biz_client_follow_up";
    //         $log_data["key"] = $client['id'];
    //         $log_data["action"] = "deliver_update";
    //         $log_data["value"] = json_encode($client_update);
    //         log_rcd($log_data);

    //         //2、新增一条CRM记录， 原本的不处理
    //         $add_crm_data = array(
    //             'client_code' => $client['client_code'],
    //             'export_sailing' => $client['export_sailing'],
    //             'trans_mode' => $client['trans_mode'],
    //             'product_type' => $client['product_type'],
    //             'product_details' => $client['product_details'],
    //             'client_source' => get_session('name'),//$client['client_source'] 这里改为取当前用户名称
    //             'apply_remark' => $client['apply_remark'],
    //             'approve_user' => get_session('id'),//审核人改为当前点击的
    //             'role' => $client['role'],
    //             'apply_type' => 1,
    //             'apply_status' => 1, // 这里状态进入哪里待讨论,目前直接到1
    //         );
    //         $crm_id = $this->biz_client_crm_model->save_by_user($add_crm_data, $to_user_id);

    //         // record the log
    //         $log_data = array();
    //         $log_data["table_name"] = "biz_client_crm";
    //         $log_data["key"] = $crm_id;
    //         $log_data["action"] = "insert";
    //         $log_data["value"] = json_encode($add_crm_data);
    //         log_rcd($log_data);

    //         //3、批量发送邮件任务
    //         //生成邮件任务给指派人和接收人
    //         $send_users = array($to_user_id, $userId);
    //         Model('bsc_user_model');
    //         Model('biz_market_mail_model');
    //         $this->db->select('id,name');
    //         $users = array_column($this->bsc_user_model->get("id in (" . join(',', $send_users) . ")"), null, 'id');
    //         $to_user_name = $users[$to_user_id]['name'];
    //         foreach ($send_users as $send_user){
    //             $add_data = array();
    //             $add_data['recipient_by'] = $send_user;
    //             $add_data['plan_send_time'] = date('Y-m-d H:i:s');
    //             $add_data['actual_send_time'] = '0000-00-00 00:00:00';
    //             $add_data['category'] = '客户跟踪';
    //             $add_data['subject'] = $client['company_name'] . "已指派给" . $to_user_name;
    //             $add_data['content'] = $add_data['subject'];
    //             $add_data['auto_content'] = '';

    //             Model('biz_market_mail_model');
    //             $this_id = $this->biz_market_mail_model->save_auto($add_data);
    //         }

    //         //将非活跃的客户,状态置为1
    //         $sql = "update biz_client_inactve set is_inactve = 1 where client_code = '{$client['client_code']}'";
    //         Model('m_model');
    //         $this->m_model->sqlquery($sql);
    //     }

    //     return jsonEcho(array('code' => 0, 'msg' => '转交成功'));
    // }

    /**
     * CRM新增客户一级表格获取统计数据
     */
    public function getCrmNewAdd()
    {
        $result = array('code' => 0, 'msg' => 'success', 'rows' => array(), 'total' => 0);

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        $f_role = isset($_REQUEST['f_role']) ? $_REQUEST['f_role'] : '';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $crm_user = postValue('crm_user', get_session('id'));
        $crm_user_type = postValue('crm_user_type', 'user');
        $crm_date_first = postValue('crm_date_first', date('Y-m-d'));
        $crm_date_end = postValue('crm_date_end', date('Y-m-d'));
        $sales_id = postValue('sales_id', '');

        //当前总客户数
        $month_first_day = $crm_date_first;
        $month_end_day = $crm_date_end;

        //where---start
        $where = array();
        if ($crm_user_type == 'user') {
            //2022-01-10 新需求 CRM获取数据逻辑 原创建人 改为 创建人+销售
            //2022-02-23 改版后, crm获取数据,只自己获取自己的
            $where[] = "(biz_client_crm.sales_id = '$crm_user')";
        } else if ($crm_user_type == 'group') {
            $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) = '$crm_user'";
        } else if ($crm_user_type == 'sub_company') {
            $this_crm_user = str_replace('company_', '', $crm_user);
            $groups = Model('bsc_group_model')->get_child_group($this_crm_user);
            $groups_code = array_column($groups, 'group_code');
//            $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) like '$this_crm_user-%'";
            $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) in ('" . join('\',\'', $groups_code) . "')";
        } else if ($crm_user_type == 'head') {
            if ($crm_user == 'HEAD') {
//                Model('bsc_group_model');
//                $this->db->select('group_code');
//                $companys = $this->bsc_group_model->get('parent_id = 1001');
//                $sales_ids_or = array();
//                foreach ($companys as $company) {
//                    $sales_ids_or[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) like '%{$company['group_code']}-%'";
//                }
//                if (empty($sales_ids_or)) $sales_ids_or[] = "1 = 0";
//                $where[] = "(" . join(' or ', $sales_ids_or) . ")";
                $groups = Model('bsc_group_model')->get_child_group($crm_user);
                $groups_code = array_column($groups, 'group_code');
                $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) in ('" . join('\',\'', $groups_code) . "')";
            }
        } else {
            $where[] = '0 = 1';
        }

        //
        $where[] = "biz_client_crm.created_time >= '$month_first_day 00:00:00' and biz_client_crm.created_time <= '$month_end_day 23:59:59' and biz_client_crm.apply_type = 1";


        //用户限制
        if (!empty($fields)) {
            foreach ($fields['f'] as $key => $field) {
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if ($f == '') continue;
                $pre = 'biz_client_crm.';
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = array('created_time', 'updated_time');
                if (in_array($f, $date_field) && $s == '=') {
                    if ($v != '') {
                        $where[] = "{$pre}{$f} >= '$v 00:00:00' and {$pre}{$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                if ($f == 'company_name') {
                    $f = 'biz_client.company_search';
                    $v = match_chinese($v);
                }
                if ($f == 'sales_id') {
                    $f = '(select name from bsc_user where bsc_user.id = biz_client_crm.sales_id)';
                    if ($v != '') $where[] = search_like($f, $s, $v);
                } else {
                    if ($v != '') $where[] = search_like('biz_client.' . $f, $s, $v);
                }

            }
        }
        if ($f_role != '') $where[] = "biz_client_crm.role like '%$f_role%'";
        $where = join(' and ', $where);
        //where---end

        Model('biz_client_crm_model');

//        $offset = ($page - 1) * $rows;
//        $this->db->group_by("sales_id");
//        $result["total"] = $this->biz_client_crm_model->no_role_total($where);
        $this->db->select('sales_id, count(1) as add_crm_count');
//        $this->db->limit($rows, $offset);
        $this->db->group_by("sales_id");
        $rs = $this->biz_client_crm_model->no_role_get($where, $sort, $order);

        //获取销售名称--start
        Model('bsc_user_model');
        $sales_ids = filter_unique_array(array_column($rs, 'sales_id'));
        if (empty($sales_ids)) $sales_ids[] = 0;
        $this->db->select('bsc_user.id,bsc_user.name,bsc_user.group,bsc_group.group_name, (select group_name from bsc_group bg where bg.group_code = bsc_group.company_code) as company_name');
        $this->db->join('bsc_group', "bsc_group.group_code = bsc_user.group", "left");
        $sales_users = array_column($this->bsc_user_model->get("bsc_user.id in (" . join(',', $sales_ids) . ")", "bsc_user.id"), null, 'id');
        //获取销售名称--end

        //这里根据组,将原本的人上面再套一个组上去

//        $group =

        foreach ($rs as $row) {
            //将销售的组作为键名整理到一起
            $sales = $sales_users[$row['sales_id']];

            if (!isset($result['rows'][$sales['group']])) {
                $result['rows'][$sales['group']] = array(
                    'group' => $sales['group'],
                    'group_name' => $sales['group_name'],
                    'company_name' => $sales['company_name'],
                    'add_crm_count' => 0,
                    'users' => array()
                );
            }

            $row['sales_name'] = $sales['name'];
            $result['rows'][$sales['group']]['add_crm_count'] += $row['add_crm_count'];
            $result['rows'][$sales['group']]['users'][] = $row;
        }
        $result['rows'] = array_values($result['rows']);
        echo json_encode($result);
    }

    /**
     * 根据当前销售获取相关数据,
     */
    public function getCrmNewAddBySales()
    {
        $result = array('code' => 0, 'msg' => 'success', 'rows' => array(), 'total' => 0);

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        $f_role = isset($_REQUEST['f_role']) ? $_REQUEST['f_role'] : '';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $crm_user = postValue('crm_user', get_session('id'));
        $crm_user_type = postValue('crm_user_type', 'user');
        $crm_date_first = postValue('crm_date_first', date('Y-m-d'));
        $crm_date_end = postValue('crm_date_end', date('Y-m-d'));
        $sales_id = getValue('sales_id', '');

        //当前总客户数
        $month_first_day = $crm_date_first;
        $month_end_day = $crm_date_end;

        //where---start
        $where = array();
        if ($crm_user_type == 'user') {
            //2022-01-10 新需求 CRM获取数据逻辑 原创建人 改为 创建人+销售
            //2022-02-23 改版后, crm获取数据,只自己获取自己的
            $where[] = "(biz_client_crm.sales_id = '$crm_user')";
        } else if ($crm_user_type == 'group') {
            $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) = '$crm_user'";
        } else if ($crm_user_type == 'sub_company') {
            $this_crm_user = str_replace('company_', '', $crm_user);
            $groups = Model('bsc_group_model')->get_child_group($this_crm_user);
            $groups_code = array_column($groups, 'group_code');
//            $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) like '$this_crm_user-%'";
            $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) in ('" . join('\',\'', $groups_code) . "')";
        } else if ($crm_user_type == 'head') {
            if ($crm_user == 'HEAD') {
//                Model('bsc_group_model');
//                $this->db->select('group_code');
//                $companys = $this->bsc_group_model->get('parent_id = 1001');
//                $sales_ids_or = array();
//                foreach ($companys as $company) {
//                    $sales_ids_or[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) like '%{$company['group_code']}-%'";
//                }
//                if (empty($sales_ids_or)) $sales_ids_or[] = "1 = 0";
//                $where[] = "(" . join(' or ', $sales_ids_or) . ")";
                $groups = Model('bsc_group_model')->get_child_group($crm_user);
                $groups_code = array_column($groups, 'group_code');
                $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) in ('" . join('\',\'', $groups_code) . "')";
            }
        } else {
            $where[] = '0 = 1';
        }
        if ($sales_id != '') $where[] = "biz_client_crm.sales_id = $sales_id";

        //
        $where[] = "biz_client_crm.created_time >= '$month_first_day 00:00:00' and biz_client_crm.created_time <= '$month_end_day 23:59:59' and biz_client_crm.apply_type = 1";


        //用户限制
        if (!empty($fields)) {
            foreach ($fields['f'] as $key => $field) {
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if ($f == '') continue;
                $pre = 'biz_client_crm.';
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = array('created_time', 'updated_time');
                if (in_array($f, $date_field) && $s == '=') {
                    if ($v != '') {
                        $where[] = "{$pre}{$f} >= '$v 00:00:00' and {$pre}{$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                if ($f == 'company_name') {
                    $f = 'biz_client.company_search';
                    $v = match_chinese($v);
                }
                if ($f == 'sales_id') {
                    $f = '(select name from bsc_user where bsc_user.id = biz_client_crm.sales_id)';
                    if ($v != '') $where[] = search_like($f, $s, $v);
                } else {
                    if ($v != '') $where[] = search_like('biz_client.' . $f, $s, $v);
                }

            }
        }
        if ($f_role != '') $where[] = "biz_client_crm.role like '%$f_role%'";
        $where = join(' and ', $where);
        //where---end

        Model('biz_client_crm_model');

        // $offset = ($page - 1) * $rows;
        $result["total"] = $this->biz_client_crm_model->no_role_total($where);
        $this->db->select('biz_client_crm.id, biz_client_crm.client_code, biz_client_crm.client_name, biz_client_crm.company_name' .
            ', biz_client_crm.province, biz_client_crm.city, biz_client_crm.company_address,biz_client_crm.sales_id' .
            ', (select biz_client_contact_list.name from biz_client_contact_list where biz_client_contact_list.crm_id = biz_client_crm.id order by id desc limit 1) as contact_name' .
            ', (select biz_client_contact_list.telephone from biz_client_contact_list where biz_client_contact_list.crm_id = biz_client_crm.id order by id desc limit 1) as contact_telephone' .
            ', (select biz_client_contact_list.telephone2 from biz_client_contact_list where biz_client_contact_list.crm_id = biz_client_crm.id order by id desc limit 1) as contact_telephone2' .
            ', (select biz_client_contact_list.email from biz_client_contact_list where biz_client_contact_list.crm_id = biz_client_crm.id order by id desc limit 1) as contact_email' .
            '', false);

        // $this->db->limit($rows, $offset);
        $rs = $this->biz_client_crm_model->no_role_get($where, $sort, $order);

        //获取销售名称--start
        Model('bsc_user_model');
        $sales_ids = filter_unique_array(array_column($rs, 'sales_id'));
        if (empty($sales_ids)) $sales_ids[] = 0;
        $this->db->select('id,name');
        $sales_users = array_column($this->bsc_user_model->get("id in (" . join(',', $sales_ids) . ")"), null, 'id');
        //获取销售名称--end

        foreach ($rs as $row) {
            $row["contact_email"] = "";
            $row["contact_telephone"] = "";
            $row["live_chat"] = "";
            $sql = "select * from biz_client_contact_list where crm_id = {$row["id"]}";
            $contact_row = $this->m_model->query_one($sql);
            if (!empty($contact_row)) {
                $row["contact_email"] = $contact_row["email"];
                $row["contact_telephone"] = $contact_row["telephone"];
                $row["live_chat"] = $contact_row["live_chat"];
            }
            $row['sales_name'] = $sales_users[$row['sales_id']]['name'];
            $result['rows'][] = $row;
        }

        echo json_encode($result);
    }

    //展示新增发送数量
    public function Crmsend()
    {
        $data = array();

        $data['crm_user'] = getValue('crm_user', get_session('id'));
        $data['crm_user_type'] = getValue('crm_user_type', 'user');
        $data['crm_date'] = getValue('crm_date', '');

        $this->load->view('/biz/crm/list/Crmsend', $data);
    }

    public function getCrmsend()
    {
        $result = array('code' => 0, 'msg' => 'success', 'rows' => array(), 'total' => 0);


        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "crm_promote_mail_log.id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        /*$f_role = isset($_REQUEST['f_role']) ? $_REQUEST['f_role'] : '';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();*/

        $crm_user = postValue('crm_user', '');
        $crm_user_type = postValue('crm_user_type', '');
        $crm_date = postValue('crm_date', '');

        //where---start
        $where = array();


        if ($crm_user_type == 'user') {
            //2022-01-10 新需求 CRM获取数据逻辑 原创建人 改为 创建人+销售
            //2022-02-23 改版后, crm获取数据,只自己获取自己的
            $where[] = "(crm_promote_mail.create_by = '$crm_user')";
        } else if ($crm_user_type == 'group') {
            $where[] = "(select `group` from bsc_user where bsc_user.id = crm_promote_mail.create_by) = '$crm_user'";
        } else if ($crm_user_type == 'sub_company') {
            $this_crm_user = str_replace('company_', '', $crm_user);
            $where[] = "(select `group` from bsc_user where bsc_user.id = crm_promote_mail.create_by) like '$this_crm_user-%'";
        } else if ($crm_user_type == 'head') {
            if ($crm_user == 'HEAD') {
                Model('bsc_group_model');
                $this->db->select('group_code');
                $companys = $this->bsc_group_model->get('parent_id = 1001');
                $sales_ids_or = array();
                foreach ($companys as $company) {
                    $sales_ids_or[] = "(select `group` from bsc_user where bsc_user.id = crm_promote_mail.create_by) like '%{$company['group_code']}-%'";
                }
                if (empty($sales_ids_or)) $sales_ids_or[] = "1 = 0";
                $where[] = "(" . join(' or ', $sales_ids_or) . ")";
            }
        } else {
            $where[] = '0 = 1';
        }


        //
        if (!empty($crm_date)) {
            $where[] = "crm_promote_mail_log.created_time >= '$crm_date 00:00:00' and crm_promote_mail_log.created_time <= '$crm_date 23:59:59'";
        }


        //用户限制
        /*if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                $pre = 'crm_promote_mail.';
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = array('created_time', 'updated_time');
                if(in_array($f, $date_field) && $s == '='){
                    if($v != ''){
                        $where[] = "{$pre}{$f} >= '$v 00:00:00' and {$pre}{$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                if($f == 'company_name'){
                    $f = 'biz_client.company_search';
                    $v = match_chinese($v);
                }
                if($f == 'sales_id'){
                    $f = '(select name from bsc_user where bsc_user.id = crm_promote_mail.create_by)';
                    if($v != '')$where[] = search_like($f, $s, $v);
                }else{
                    if($v != '')$where[] = search_like('biz_client.' . $f, $s, $v);
                }

            }
        }
        if($f_role != '') $where[] = "biz_client_crm.role like '%$f_role%'";*/
        $where = join(' and ', $where);
        //where---end

        /*        Model('biz_client_crm_model');

        //        $offset = ($page - 1) * $rows;
        //        $this->db->group_by("sales_id");
        //        $result["total"] = $this->biz_client_crm_model->no_role_total($where);
                $this->db->select('sales_id, count(1) as add_crm_count');
        //        $this->db->limit($rows, $offset);
                $this->db->group_by("sales_id");
                $rs = $this->biz_client_crm_model->no_role_get($where, $sort, $order);*/


        $arr = $this->db->select('crm_promote_mail.create_by')->from('crm_promote_mail_log')->join('crm_promote_mail', 'crm_promote_mail_log.mail_id=crm_promote_mail.id')->where($where, null, false)->order_by($sort, $order, true)->get()->result_array();
        $arr = array_column($arr, 'create_by');
        $arr = array_count_values($arr);
        $rs = array();
        foreach ($arr as $k => $v) {
            $rs[] = array(
                'create_by' => $k,
                'total' => $v,
            );
        }

        //获取销售名称--start
        Model('bsc_user_model');
        $create_by_ids = filter_unique_array(array_column($rs, 'create_by'));
        if (empty($create_by_ids)) $create_by_ids[] = 0;
        $this->db->select('bsc_user.id,bsc_user.name,bsc_user.group,bsc_group.group_name, (select group_name from bsc_group bg where bg.group_code = bsc_group.company_code) as company_name');
        $this->db->join('bsc_group', "bsc_group.group_code = bsc_user.group", "left");
        $create_by_users = array_column($this->bsc_user_model->get("bsc_user.id in (" . join(',', $create_by_ids) . ")", "bsc_user.id"), null, 'id');
        //获取销售名称--end

        //这里根据组,将原本的人上面再套一个组上去

//        $group =

        foreach ($rs as $row) {
            //将销售的组作为键名整理到一起
            $sales = $create_by_users[$row['create_by']];

            if (!isset($result['rows'][$sales['group']])) {
                $result['rows'][$sales['group']] = array(
                    'group' => $sales['group'],
                    'group_name' => $sales['group_name'],
                    'company_name' => $sales['company_name'],
                    'total' => 0,
                    'users' => array()
                );
            }

            $row['create_by_name'] = $sales['name'];
            $result['rows'][$sales['group']]['total'] += $row['total'];
            $result['rows'][$sales['group']]['users'][] = $row;
        }
        $result['rows'] = array_values($result['rows']);

        echo json_encode($result);
    }


    public function crm_set_num()
    {
        $data = array();
        $date = getValue('date', '');
        $arr = array();

        if (!empty($date)) {
            //保存按钮
            $button = false;

            //设置权限
            $id = get_session('id');
            $user = $this->db->where('id', $id)->get('bsc_user')->row_array();

            if (empty($user['group_range'])) {
                //只能查看自己的部门
                //获取销售部门
                $group = $this->db->select('group_name,group_code')->where('group_type', '销售部')->where('group_code', $user['group'])->get('bsc_group')->result_array();

            } else {
                $group_range = explode(',', $user['group_range']);
                //获取销售部门
                $group = $this->db->select('group_name,group_code')->where('group_type', '销售部')->where_in('group_code', $group_range)->get('bsc_group')->result_array();
            }

            //循环获取数据
            foreach ($group as $k => $v) {

                //获取年值
                $year = substr($date, 0, 4);
                //判断是否存在该年
                $year_data = $this->db->where('target_type', '1')->where('target_name', $v['group_code'])->where('date_value', $year)->where('date_type', 'y')->get('biz_client_crm_target')->row_array();

                if (empty($year_data)) {
                    $arr[$k] = array(
                        'crm_num_year' => '0',
                        'client_num_year' => '0',
                        'shipment_num_year' => '0',
                        'target_name' => $v['group_code'],
                        'target_name_cn' => $v['group_name'],
                    );
                } else {
                    //获取月份
                    $arr[$k] = array(
                        'crm_num_year' => $year_data['crm_num'],
                        'client_num_year' => $year_data['client_num'],
                        'shipment_num_year' => $year_data['shipment_num'],
                        'target_name' => $v['group_code'],
                        'target_name_cn' => $v['group_name'],
                    );
                }

                //判断是否存在该月
                $month_data = $this->db->where('target_type', '1')->where('target_name', $v['group_code'])->where('date_value', $date)->where('date_type', 'm')->get('biz_client_crm_target')->row_array();

                if (empty($month_data)) {
                    $arr[$k]['crm_num_month'] = '0';
                    $arr[$k]['client_num_month'] = '0';
                    $arr[$k]['shipment_num_month'] = '0';
                    $arr[$k]['start_date'] = $date;
                } else {
                    $arr[$k]['crm_num_month'] = $month_data['crm_num'];
                    $arr[$k]['client_num_month'] = $month_data['client_num'];
                    $arr[$k]['shipment_num_month'] = $month_data['shipment_num'];
                    $arr[$k]['start_date'] = $date;
                }

                //保存按钮是否可以按
                if (!empty($user['group_range'])) {
                    $group_range = explode(',', $user['group_range']);
                    if (in_array($v['group_code'], $group_range)) {
                        $button = true;
                    }
                }

                $arr[$k]['button'] = $button;
            }

        }

        $data['arr'] = $arr;
        $data['date'] = $date;

        $this->load->view('head');
        $this->load->view('/biz/crm/set/crm_set_num', $data);
    }

    public function crm_set_num_user()
    {

        $data = array();
        $group_code = getValue('group_code', '');
        $data['group_code'] = $group_code;
        $date = getValue('date', '');
        $arr = array();

        if (!empty($date)) {

            //保存按钮
            $button = false;

            //设置权限
            $id = get_session('id');
            $user = $this->db->where('id', $id)->get('bsc_user')->row_array();

            $user_range = get_session('user_range');

            if (empty($user_range)) {
                //只能查看自己
                //获取该部门销售
                $group = $this->db->select('b.id,b.name')->from('bsc_group as a')->where('a.group_code', $group_code)->join('bsc_user as b', 'b.group=a.group_code', 'right')->where('b.id', $user['id'])->get()->result_array();

            } else {

                //获取该部门销售
                $group = $this->db->select('b.id,b.name')->from('bsc_group as a')->where('a.group_code', $group_code)->join('bsc_user as b', 'b.group=a.group_code', 'right')->where_in('b.id', $user_range)->get()->result_array();

            }


            //循环获取数据
            foreach ($group as $k => $v) {

                //获取年值
                $year = substr($date, 0, 4);
                //判断是否存在该年
                $year_data = $this->db->where('target_type', '2')->where('target_name', $v['id'])->where('date_value', $year)->where('date_type', 'y')->get('biz_client_crm_target')->row_array();

                if (empty($year_data)) {
                    $arr[$k] = array(
                        'crm_num_year' => '0',
                        'client_num_year' => '0',
                        'shipment_num_year' => '0',
                        'target_name' => $v['id'],
                        'target_name_cn' => $v['name'],
                    );
                } else {
                    //获取月份
                    $arr[$k] = array(
                        'crm_num_year' => $year_data['crm_num'],
                        'client_num_year' => $year_data['client_num'],
                        'shipment_num_year' => $year_data['shipment_num'],
                        'target_name' => $v['id'],
                        'target_name_cn' => $v['name'],
                    );
                }

                //判断是否存在该月
                $month_data = $this->db->where('target_type', '2')->where('target_name', $v['id'])->where('date_value', $date)->where('date_type', 'm')->get('biz_client_crm_target')->row_array();

                if (empty($month_data)) {
                    $arr[$k]['crm_num_month'] = '0';
                    $arr[$k]['client_num_month'] = '0';
                    $arr[$k]['shipment_num_month'] = '0';
                    $arr[$k]['start_date'] = $date;
                } else {
                    $arr[$k]['crm_num_month'] = $month_data['crm_num'];
                    $arr[$k]['client_num_month'] = $month_data['client_num'];
                    $arr[$k]['shipment_num_month'] = $month_data['shipment_num'];
                    $arr[$k]['start_date'] = $date;
                }


                //保存按钮是否可以按
                if (!empty($user['group_range'])) {
                    $group_range = explode(',', $user['group_range']);
                    if (in_array($group_code, $group_range)) {
                        $button = true;
                    }
                }

                //如果是自己的不能设置
                if ($v['id'] == $user['id']) {
                    $button = false;
                }

                $arr[$k]['button'] = $button;
            }

        }

        $data['arr'] = $arr;
        $data['date'] = $date;

        $this->load->view('head');
        $this->load->view('/biz/crm/set/crm_set_num_user', $data);


    }


    public function crm_set_num_complete()
    {
        $data = array();
        $date_s = getValue('date_s', '');
        $date_e = getValue('date_e', '');

        if (!empty($date_s) && !empty($date_e)) {
            //设置权限
            $id = get_session('id');
            $user = $this->db->where('id', $id)->get('bsc_user')->row_array();

            if (empty($user['group_range'])) {
                //只能查看自己的部门
                //获取销售部门
                $group = $this->db->select('group_name,group_code')->where('group_type', '销售部')->where('group_code', $user['group'])->get('bsc_group')->result_array();

            } else {
                $group_range = explode(',', $user['group_range']);
                //获取销售部门
                $group = $this->db->select('group_name,group_code')->where('group_type', '销售部')->where_in('group_code', $group_range)->get('bsc_group')->result_array();
            }

            //获取能看的销售部门
            $group_str = array_column($group, "group_code");
            $group_str = implode("','", $group_str);
            $group_str = "'" . $group_str . "'";

            //设置月开始结束时间
            $month_first_day = $date_s . '-01';
            $day = date('t', strtotime($date_e));
            $month_end_day = $date_e . '-' . $day;
            $month_end_day = $month_end_day . ' 23:59:59';

            // 获取crm数量 
            $sql = "select created_group, count(1) as num from biz_client_crm where apply_status = 2 and created_time>='$month_first_day' and created_time<='$month_end_day' and created_group in ($group_str) GROUP BY created_group";
            $rs_crm_month = $this->m_model->query_array($sql);
            $rs_crm_month = array_column($rs_crm_month, "num", "created_group");

            // 获取新增合作客户数
            $sql = "select d.user_group, count(distinct s.client_code) as num from biz_shipment as s inner join biz_consol as c on s.consol_id = c.id join biz_duty_new as d on d.user_role ='sales' and d.id_type='biz_shipment' and d.id_no = s.id where c.trans_ATD >= '$month_first_day'  and c.trans_ATD <='$month_end_day' and s.status = 'normal' and d.user_group in ($group_str) and s.client_code not in (select distinct biz_shipment.client_code from biz_shipment  inner join biz_consol on biz_shipment.consol_id = biz_consol.id where biz_consol.trans_ATD < '$month_first_day'  and biz_shipment.status = 'normal') GROUP BY d.user_group";
            $rs_new_customer_month = $this->m_model->query_array($sql);
            $rs_new_customer_month = array_column($rs_new_customer_month, "num", "user_group");

            //获取订单数
            $sql = "select d.user_group, count(1) as num from biz_shipment as s inner join biz_consol as c on s.consol_id = c.id join biz_duty_new as d on d.user_role ='sales' and d.id_type='biz_shipment' and d.id_no = s.id where c.trans_ATD >= '$month_first_day' and c.trans_ATD <='$month_end_day' and d.user_group in ($group_str) GROUP BY d.user_group";
            $rs_order_num_month = $this->m_model->query_array($sql);
            $rs_order_num_month = array_column($rs_order_num_month, "num", "user_group");

            foreach ($group as $g) {
                if (empty($rs_crm_month[$g['group_code']])) $rs_crm_month[$g['group_code']] = 0;
                if (empty($rs_new_customer_month[$g['group_code']])) $rs_new_customer_month[$g['group_code']] = 0;
                if (empty($rs_order_num_month[$g['group_code']])) $rs_order_num_month[$g['group_code']] = 0;
            }

            $data['rs_crm_month'] = $rs_crm_month;
            $data['rs_new_customer_month'] = $rs_new_customer_month;
            $data['rs_order_num_month'] = $rs_order_num_month;

            //获取目标数据
            foreach ($group as $k => $v) {
                //处理月问题
                $month_data = $this->db->where('target_type', '1')->where('target_name', $v['group_code'])->where('date_type', 'm')->where('date_value >=', $date_s)->where('date_value <=', $date_e)->get('biz_client_crm_target')->result_array();

                if (empty($month_data)) {
                    $group[$k]['crm_num_month'] = '0';
                    $group[$k]['client_num_month'] = '0';
                    $group[$k]['shipment_num_month'] = '0';
                } else {

                    $group[$k]['crm_num_month'] = array_sum(array_column($month_data, 'crm_num'));
                    $group[$k]['client_num_month'] = array_sum(array_column($month_data, 'client_num'));
                    $group[$k]['shipment_num_month'] = array_sum(array_column($month_data, 'shipment_num'));
                }


                if ($rs_crm_month[$v['group_code']] != '0' && $group[$k]['crm_num_month'] != '0') {
                    $group[$k]['crm_month'] = round($rs_crm_month[$v['group_code']] / $group[$k]['crm_num_month'] * 100, 1) . "％";
                } else {
                    $group[$k]['crm_month'] = '0%';
                }

                if ($rs_new_customer_month[$v['group_code']] != '0' && $group[$k]['client_num_month'] != '0') {
                    $group[$k]['client_month'] = round($rs_new_customer_month[$v['group_code']] / $group[$k]['client_num_month'] * 100, 1) . "％";
                } else {
                    $group[$k]['client_month'] = '0%';
                }

                if ($rs_order_num_month[$v['group_code']] != '0' && $group[$k]['shipment_num_month'] != '0') {
                    $group[$k]['shipment_month'] = round($rs_order_num_month[$v['group_code']] / $group[$k]['shipment_num_month'] * 100, 1) . "％";
                } else {
                    $group[$k]['shipment_month'] = '0%';
                }

            }

            $data['group'] = $group;
            // print_r($group);exit();
        }

        $data['date_e'] = $date_e;
        $data['date_s'] = $date_s;

        $this->load->view('head');
        $this->load->view('/biz/crm/set/crm_set_num_complete', $data);
    }

    public function crm_set_num_user_complete()
    {
        $data = array();
        //区别
        $group_code = getValue('group_code', '');
        $data['group_code'] = $group_code;

        $date_s = getValue('date_s', '');
        $date_e = getValue('date_e', '');


        if (!empty($date_s) && !empty($date_e)) {

            //设置权限
            $id = get_session('id');
            $user = $this->db->where('id', $id)->get('bsc_user')->row_array();

            $user_range = get_session('user_range');

            if (empty($user_range)) {
                //只能查看自己
                //获取该部门销售
                $group = $this->db->select('b.id,b.name')->from('bsc_group as a')->where('a.group_code', $group_code)->join('bsc_user as b', 'b.group=a.group_code', 'right')->where('b.id', $user['id'])->get()->result_array();

            } else {
                //获取该部门销售
                $group = $this->db->select('b.id,b.name')->from('bsc_group as a')->where('a.group_code', $group_code)->join('bsc_user as b', 'b.group=a.group_code', 'right')->where_in('b.id', $user_range)->get()->result_array();

            }

            //获取能看的销售
            $group_str = array_column($group, "id");
            $group_str = implode("','", $group_str);
            $group_str = "'" . $group_str . "'";

            //设置月开始结束时间
            $month_first_day = $date_s . '-01';
            $day = date('t', strtotime($date_e));
            $month_end_day = $date_e . '-' . $day;
            $month_end_day = $month_end_day . ' 23:59:59';


            // 获取crm数量
            $sql = "select sales_id, count(1) as num from biz_client_crm where apply_status = 2 and created_time>='$month_first_day' and created_time<='$month_end_day' and sales_id in ($group_str) GROUP BY sales_id";
            $rs_crm_month = $this->m_model->query_array($sql);
            $rs_crm_month = array_column($rs_crm_month, "num", "sales_id");


            // 获取新增合作客户数
            $sql = "select d.user_id, count(distinct s.client_code) as num from biz_shipment as s inner join biz_consol as c on s.consol_id = c.id join biz_duty_new as d on d.user_role ='sales' and d.id_type='biz_shipment' and d.id_no = s.id where c.trans_ATD >= '$month_first_day'  and c.trans_ATD <='$month_end_day' and s.status = 'normal' and d.user_id in ($group_str) and s.client_code not in (select distinct biz_shipment.client_code from biz_shipment  inner join biz_consol on biz_shipment.consol_id = biz_consol.id where biz_consol.trans_ATD < '$month_first_day'  and biz_shipment.status = 'normal') GROUP BY d.user_id";
            $rs_new_customer_month = $this->m_model->query_array($sql);
            $rs_new_customer_month = array_column($rs_new_customer_month, "num", "user_id");

            //获取订单数
            $sql = "select d.user_id, count(1) as num from biz_shipment as s inner join biz_consol as c on s.consol_id = c.id join biz_duty_new as d on d.user_role ='sales' and d.id_type='biz_shipment' and d.id_no = s.id where c.trans_ATD >= '$month_first_day' and c.trans_ATD <='$month_end_day' and d.user_id in ($group_str) GROUP BY d.user_id";
            $rs_order_num_month = $this->m_model->query_array($sql);
            $rs_order_num_month = array_column($rs_order_num_month, "num", "user_id");

            foreach ($group as $g) {
                if (empty($rs_crm_month[$g['id']])) $rs_crm_month[$g['id']] = 0;
                if (empty($rs_new_customer_month[$g['id']])) $rs_new_customer_month[$g['id']] = 0;
                if (empty($rs_order_num_month[$g['id']])) $rs_order_num_month[$g['id']] = 0;
            }

            $data['rs_crm_month'] = $rs_crm_month;
            $data['rs_new_customer_month'] = $rs_new_customer_month;
            $data['rs_order_num_month'] = $rs_order_num_month;

            //获取目标数据
            foreach ($group as $k => $v) {
                //处理月问题
                $month_data = $this->db->where('target_type', '2')->where('target_name', $v['id'])->where('date_type', 'm')->where('date_value >=', $date_s)->where('date_value <=', $date_e)->get('biz_client_crm_target')->result_array();

                if (empty($month_data)) {
                    $group[$k]['crm_num_month'] = '0';
                    $group[$k]['client_num_month'] = '0';
                    $group[$k]['shipment_num_month'] = '0';
                } else {

                    $group[$k]['crm_num_month'] = array_sum(array_column($month_data, 'crm_num'));
                    $group[$k]['client_num_month'] = array_sum(array_column($month_data, 'client_num'));
                    $group[$k]['shipment_num_month'] = array_sum(array_column($month_data, 'shipment_num'));
                }


                if ($rs_crm_month[$v['id']] != '0' && $group[$k]['crm_num_month'] != '0') {
                    $group[$k]['crm_month'] = round($rs_crm_month[$v['id']] / $group[$k]['crm_num_month'] * 100, 1) . "％";
                } else {
                    $group[$k]['crm_month'] = '0%';
                }

                if ($rs_new_customer_month[$v['id']] != '0' && $group[$k]['client_num_month'] != '0') {
                    $group[$k]['client_month'] = round($rs_new_customer_month[$v['id']] / $group[$k]['client_num_month'] * 100, 1) . "％";
                } else {
                    $group[$k]['client_month'] = '0%';
                }

                if ($rs_order_num_month[$v['id']] != '0' && $group[$k]['shipment_num_month'] != '0') {
                    $group[$k]['shipment_month'] = round($rs_order_num_month[$v['id']] / $group[$k]['shipment_num_month'] * 100, 1) . "％";
                } else {
                    $group[$k]['shipment_month'] = '0%';
                }

            }

            $data['group'] = $group;
            // print_r($group);exit();
        }


        $data['date_e'] = $date_e;
        $data['date_s'] = $date_s;

        $this->load->view('head');
        $this->load->view('/biz/crm/set/crm_set_num_user_complete', $data);
    }


    function checkStr($str, $target)
    {
        $tmpArr = explode($str, $target);
        if (count($tmpArr) > 1) return true;
        else return false;
    }

    public function crm_set_num_save($type = '')
    {

        $post = $_POST;
        if (!empty($post)) {
            /* //获取年值
             $year = substr($post['start_date'], 0, 4);

             //判断是否存在年目标
             $year_data = $this->db->where('target_type', $type)->where('target_name', $post['target_name'])->where('date_value', $year)->where('date_type', 'y')->get('biz_client_crm_target')->row_array();

             if (empty($year_data)) {
                 //不存在新增
                 $data_y = array(
                     'created_by' => get_session('id'),
                     'created_time' => date('Y-m-d H:i:s', time()),
                     'target_type' => $type,
                     'target_name' => $post['target_name'],
                     'date_type' => 'y',
                     'date_value' => $year,
                     'crm_num' => $post['crm_num_year'],
                     'client_num' => $post['client_num_year'],
                     'shipment_num' => $post['shipment_num_year'],
                 );
                 $bl = $this->db->insert('biz_client_crm_target', $data_y);
             } else {
                 $data_y = array(
                     'crm_num' => $post['crm_num_year'],
                     'client_num' => $post['client_num_year'],
                     'shipment_num' => $post['shipment_num_year'],
                 );
                 $bl = $this->db->where('id', $year_data['id'])->update('biz_client_crm_target', $data_y);
             }

             if ($bl) {
                 $msg[] = $year . '年目标保存成功';
             } else {
                 echo json_encode(array('code' => '1', 'msg' => '保存失败'));
                 return;
             }*/


            $q = $post['start_date'];
            //判断是否存在月目标
            $month_data = $this->db->where('target_type', $type)->where('target_name', $post['target_name'])->where('date_value', $q)->where('date_type', 'm')->get('biz_client_crm_target')->row_array();

            if (empty($month_data)) {
                //不存在新增
                $data_m = array(
                    'created_by' => get_session('id'),
                    'created_time' => date('Y-m-d H:i:s', time()),
                    'target_type' => $type,
                    'target_name' => $post['target_name'],
                    'date_type' => 'm',
                    'date_value' => $post['start_date'],
                    'crm_num' => $post['crm_num_month'],
                    'client_num' => $post['client_num_month'],
                    'shipment_num' => $post['shipment_num_month'],
                );
                $bls = $this->db->insert('biz_client_crm_target', $data_m);
            } else {
                $data_m = array(
                    'crm_num' => $post['crm_num_month'],
                    'client_num' => $post['client_num_month'],
                    'shipment_num' => $post['shipment_num_month'],
                );
                $bls = $this->db->where('id', $month_data['id'])->update('biz_client_crm_target', $data_m);
            }

            if ($bls) {
                $msg[] = $post['start_date'] . '月目标保存成功';
            } else {
                echo json_encode(array('code' => '1', 'msg' => '保存失败'));
                return;
            }


        } else {
            echo json_encode(array('code' => '1', 'msg' => '参数错误'));
            return;
        }

        $msg = implode(',', $msg);
        echo json_encode(array('msg' => $post['target_name'] . '：' . $msg, 'code' => 0));

    }

    //获取详情
    public function crm_set_details($group = '', $type = '',$case='')
    {
        $data['group'] = $group;
        $data['type'] = $type;

        $date_s = getValue('date_s', '');
        $date_e = getValue('date_e', '');

        $data['date_s'] = $date_s . '-01';
        $day = date('t', strtotime($date_e));
        $data['date_e'] = $date_e . '-' . $day;

        $this->load->view('head');

        if($case=='crm'){
            $this->load->view('/biz/crm/set/crm_set_details_crm', $data);
        }else if($case=='client'){
            $this->load->view('/biz/crm/set/crm_set_details_client', $data);
        }else if($case=='shipment'){
            $this->load->view('/biz/crm/set/crm_set_details_shipment', $data);
        }

    }

    //client详情数据获取
    public function crm_set_details_client_get_data($group = '', $type = '')
    {
        $date_s = getValue('date_s', '');
        $date_e = getValue('date_e', '');

        if ($type == '0') {
            $title = 'user_group';
        } else if ($type == '1') {
            $title = 'user_id';
        }


        $result = array();

        $sql = "select s.client_company,s.client_code,(select group_name from bsc_group where group_code=d.user_group) as user_group,s.id,s.job_no,(select `name` from bsc_user where id = d.user_id) as user_name,c.trans_ATD from biz_shipment as s inner join biz_consol as c on s.consol_id = c.id join biz_duty_new as d on d.user_role ='sales' and d.id_type='biz_shipment' and d.id_no = s.id where c.trans_ATD >= '$date_s'  and c.trans_ATD <='$date_e 23:59:59' and s.status = 'normal' and d.$title = '$group' and s.client_code not in (select distinct biz_shipment.client_code from biz_shipment  inner join biz_consol on biz_shipment.consol_id = biz_consol.id where biz_consol.trans_ATD < '$date_s'  and biz_shipment.status = 'normal') order by s.client_company";
        // echo $sql;exit();
        $data = $this->m_model->query_array($sql);


        $result['total'] = count($data);


        $result['rows'] = $data;

        echo json_encode($result);


    }


    //crm客户数详情获取
    public function crm_set_details_crm_get_data($group = '', $type = '')
    {
        $date_s = getValue('date_s', '');
        $date_e = getValue('date_e', '');

        if ($type == '0') {
            $title = 'created_group';
        } else if ($type == '1') {
            $title = 'sales_id';
        }


        $result = array();

        // 获取crm数量
        $sql = "select created_time,(select `name` from bsc_user where id = biz_client_crm.sales_id) as user_name ,(select `company_name` from biz_client where client_code = biz_client_crm.client_code) as client_company ,export_sailing,trans_mode from biz_client_crm where apply_status = 2 and created_time>='$date_s' and created_time<='$date_e 23:59:59' and $title = '$group'order by client_company ";

        $data = $this->m_model->query_array($sql);


        $result['total'] = count($data);


        $result['rows'] = $data;

        echo json_encode($result);


    }

    //shipment客户数详情获取
    public function crm_set_details_shipment_get_data($group = '', $type = '')
    {
        $date_s = getValue('date_s', '');
        $date_e = getValue('date_e', '');

        if ($type == '0') {
            $title = 'user_group';
        } else if ($type == '1') {
            $title = 'user_id';
        }
        
        $result = array();

        $sql = "select s.client_company,(select `name` from bsc_user where id = d.user_id) as user_name, (select group_name from bsc_group where group_code=d.user_group) as user_group,s.id,s.job_no, c.trans_ATD from biz_shipment as s inner join biz_consol as c on s.consol_id = c.id join biz_duty_new as d on d.user_role ='sales' and d.id_type='biz_shipment' and d.id_no = s.id where c.trans_ATD >= '$date_s' and c.trans_ATD <= '$date_e 23:59:59' and d.$title = '$group'order by s.client_company ";

        $data = $this->m_model->query_array($sql);


        $result['total'] = count($data);


        $result['rows'] = $data;

        echo json_encode($result);


    }

    /**
     * 私海客户扔进公海
     */
    public function set_public(){
        $return = array('msg'=>'', 'code'=>0);
        $_id = (array)$this->input->post('ids');
        $ids = array();
        foreach ($_id as $id){
            if ((int)$id == 0) continue;

            //只有负责客户的唯一销售人员才能对该客户设置公海
            $where = array('biz_client.id'=>$id,'biz_client_duty.user_role'=>'sales');
            $this->db->select('biz_client_duty.user_id')->where($where);
            $this->db->join('biz_client_duty', 'biz_client_duty.client_code = biz_client.client_code');
            $result = array_column($this->db->get('biz_client')->result_array(), 'user_id');

            //只有一个销售人员 && 销售人员账号=当前登录账号
            if (count($result) == 1 && $result[0] == get_session('id')){
                $ids[] = (int)$id;
            }
        }

        if (count($ids) > 0){
            $data = array('is_open'=>1);
            $query = $this->db->where_in('id', $ids)->update('biz_client', $data);
            if($query){
                $return['code'] = 1;

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_client";
                $log_data["key"] = join(',', $ids);
                $log_data["action"] = "update";
                $log_data["value"] = json_encode($data);
                $this->log_rcd($log_data);
            }else{
                $return['msg'] = '数据更新失败！';
            }
        }else{
            $return['msg'] = '权限不足，只能把完全属于自己的客户“扔进公海”！';
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * 导入客户
     */
    public function import(){
        $user = $this->db->select('id, name')->where('status', 0)->get('bsc_user')->result_array();
        $this->load->vars('user', $user);
        $this->load->view('biz/crm/import');
    }

    //拆分数据
    public function split_table(){
        if (is_ajax()){
            $return = array('code'=>0, 'msg'=>'');
            $columns = $this->input->post('columns');
            $list_data = $this->input->post('list_data');
            if(empty($list_data) || empty($columns)){
                $return['msg'] = '提交格式错误！';
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }
            $data = array();
            foreach ($list_data as $index => $item) {
                $row = json_decode($item, true);
                if (count($columns) != count($row)) {
                    $return['msg'] = '数据的键值不匹配！';
                    return $this->output->set_content_type('application/json')->set_output(json_encode($return));
                }
                $row = array_combine($columns, $row);
                foreach ($row as $key => $val){
                    //如果notify中出现CONSIGNEE，不作为线索使用
                    if ($key == 'notify' && strpos(strtoupper($row['notify']), 'CONSIGNEE')) continue;
                    if (in_array($key, array('customer','shipper', 'consignee', 'notify')) && trim($val) != ''){
                        $temp_key = str_replace(' ','', trim($val));
                        $data[] = array(
                            'clue'   => $val,
                            'country_code'   => '',
                            'customer'   => $row['customer'],
                            'shipper'   => $row['shipper'],
                            'consignee'   => $row['consignee'],
                            'notify'   => $row['notify'],
                            'pol'   => $row['pol'],
                            'pod'   => $row['pod'],
                            'etd'   => $row['etd'],
                            'vessel'   => $row['vessel'],
                            'product_details'   => $row['product_details'],
                            'bill_no'   => $row['bill_no'],
                            'remark'    => $row['remark'],
                        );
                    }
                }
            }

            $return['code'] = 1;
            $return['data'] = base64_encode(json_encode(array_values($data)));
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }
        else
        {
            $this->load->view('biz/crm/split_table');
        }
    }

    //批量导入（没有使用）
    public function __import_data(){
        $return = array('code'=>0, 'msg'=>'');
        $columns = $this->input->post('columns');
        $list_data = $this->input->post('list_data');
        if(empty($list_data) || empty($columns)){
            $return['msg'] = '提交内容无效！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        $errinfo = $clue_data = $crm_data = array();
        $success = $clue_data['crm_id'] = 0;
        foreach ($list_data as $index => $item){
            //转回数组
            $item = json_decode($item, true);
            //清除特殊字符
            $row = $item = array_map(function($v){
                return clear_special_str($v);
            }, $item);

            $row_no = $index + 1;
            if (count($columns) != count($row)){
                $return['msg'] = '数据的键值不匹配！';
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }else{
                $clue_data = array_combine($columns, $row);
                $clue_data['clue'] = clear_special_str($clue_data['clue']);
                $crm_data = array(
                    'clue_name'=>$clue_data['clue'],
                    'country_code'=>$clue_data['country_code'],
                    'company_search' => match_chinese($clue_data['clue']),
                    'product_details'=>$clue_data['product_details'],
                    'clue_status'=>1,   //1表示为分析中
                    'apply_status'=>-2, //-2表示为线索
                    'remark'=>$clue_data['remark'],
                );
                unset($clue_data['country_code']);

                //线索在收发通中的位置
                $item = array_combine($item, $columns);
                $clue_data['clue_type'] = $item[$clue_data['clue']];

                //如果有相同的线索直接跳过
                $where = array_filter($clue_data);
                unset($where['export_sailing'],$where['trans_mode'],$where['product_type'],$where['remark'],$where['clue_type']);
                $clue_data['key'] = $key = md5(join('', $where));
                $is_exists = $this->db->where('key', $key)->count_all_results('biz_client_crm_clue');
                if ($is_exists) {
                    $success++;
                    continue;
                }

                //必填字段
                if (!isset($clue_data['clue']) || trim($clue_data['clue']) == '') {
                    $errinfo[] = array('row'=>$row_no, 'col'=>0);
                }else{
                    $clue_data["created_by"] = $crm_data["created_by"] = get_session('id');
                    $clue_data["created_time"] = $crm_data["created_time"] = date('Y-m-d H:i:s');

                    $crm = $this->db->select('id')->where('company_search', $crm_data['company_search'])->get('biz_client_crm')->row_array();
                    //如果线索公司已存在则CRM里不插入新抬头，只存新线索
                    if (!empty($crm)){
                        $clue_data['crm_id'] = $crm['id'];
                    }else{
                        $this->db->insert('biz_client_crm', array_filter($crm_data));
                        $clue_data['crm_id'] = $this->db->insert_id();
                    }

                    //开始导入线索
                    if ($clue_data['crm_id'] > 0){
                        $this->db->insert('biz_client_crm_clue', array_filter($clue_data));
                        $success++;
                    }
                }
            }
        }

        if(!empty($errinfo)) {
            $return['code'] = 2;
            $return['errinfo'] = $errinfo;
        }else{
            if ($success > 0){
                $return['code'] = 1;
            }else{
                $return['msg'] = 'Query failed!';
            }
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    //轮询单条导入
    public function import_data(){
        $return = array('code'=>1, 'msg'=>'');
        $clue_data = $this->input->post();
        //清除特殊字符
        $clue_data = array_map(function($v){
            return clear_special_str($v);
        }, $clue_data);

        $crm_data = array(
            'clue_name'=>$clue_data['clue'],
            'company_name'=>$clue_data['clue'],
            'country_code'=>$clue_data['country_code'],
            'company_search' => match_chinese($clue_data['clue']),
            'product_details'=>$clue_data['product_details'],
            'clue_status'=>1,   //1表示为分析中
            'apply_status'=>-2, //-2表示为线索
            'remark'=>$clue_data['remark'],
        );
        unset($clue_data['country_code']);

        //如果有相同的线索直接跳过
        $where = array_filter($clue_data);
        unset($where['export_sailing'],$where['trans_mode'],$where['product_type'],$where['remark'],$where['clue_type']);
        $clue_data['key'] = $key = md5(join('', $where));
        //如果相同的线索已经存在则不用再次保存
        $is_exists = $this->db->where('key', $key)->count_all_results('biz_client_crm_clue');
        if ($is_exists) {
            $return['code'] = 1;
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        if (!isset($clue_data['clue']) || trim($clue_data['clue']) == '') {
            //必填字段
            $return['code'] = 1;
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }elseif($this->db->where(array('company_search'=>$crm_data['company_search'], 'client_level'=>-1))->count_all_results('biz_client') > 0){
            //往来单位黑名单
            $return['code'] = 1;
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }else{
            $clue_data["created_by"] = $crm_data["created_by"] = get_session('id');
            $clue_data["created_time"] = $crm_data["created_time"] = date('Y-m-d H:i:s');

            $crm = $this->db->select('id, apply_status')->where('company_search', $crm_data['company_search'])->get('biz_client_crm')->row_array();
            //如果线索公司已存在则CRM里不插入新抬头，只存新线索
            if (!empty($crm)) {
                $clue_data['crm_id'] = $crm['id'];
                if ($crm['apply_status'] == -99) {
                    $return['code'] = 1;
                    return $this->output->set_content_type('application/json')->set_output(json_encode($return));
                }
            } else {
                $this->db->insert('biz_client_crm', array_filter($crm_data));
                $clue_data['crm_id'] = $this->db->insert_id();
            }

            //开始导入线索
            if ($clue_data['crm_id'] > 0){
                $clue_data['clue_pure_text'] = match_chinese($clue_data['clue']);
                if($this->db->insert('biz_client_crm_clue', array_filter($clue_data))){
                    $return['code'] = 1;
                    //发送站内消息
                    $this->load->library('Disktop_notice');
                    $this->disktop_notice->import_clue($crm_data['company_search']);
                }
            }
        }

        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * @title 填充国家代码
     */
    public function get_country_code(){
        $return = array('code'=>0, 'msg'=>'');
        $columns = $this->input->post('columns');
        $list_data = $this->input->post('list_data');
        if(empty($list_data) || empty($columns)){
            $return['msg'] = '提交内容无效！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        $clue_data = $crm_data = $UndefinedPortCode = $submit_port = array();
        $clue_data['crm_id'] = 0;

        //港口代码
        $get_port = $this->get_port();
        foreach ($list_data as $index => $item){
            $row_no = $index + 1;
            $row = $item = json_decode($item, true);

            if (count($columns) != count($row)){
                $return['msg'] = '数据的键值不匹配！';
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }else{
                $row = array_combine($columns, $row);
                //线索在收发通中的位置
                $item = array_combine($item, $columns);
                $clue_type = $item[$row['clue']];

                if (empty($row['pol']) || empty($row['pod'])){
                    $return['msg'] = "第{$row_no}行POL或POD没有填写，无法补齐国家代码！";
                    return $this->output->set_content_type('application/json')->set_output(json_encode($return));
                }

                if ($clue_type == 'customer' || $clue_type == 'shipper'){
                    $port_name = $row['pol'];
                }else{
                    $port_name = $row['pod'];
                }

                $strtoupper_after = strtoupper(trim($port_name));
                if (array_key_exists($strtoupper_after, $get_port) === false) {
                    $UndefinedPortCode[$strtoupper_after] = array(
                        'port_name' => $port_name,
                        'port_code' => '',
                        'carrier_name' => getUserName(get_session('id'))
                    );
                } else {
                    $row['country_code'] = substr($get_port[$strtoupper_after], 0, 2);
                }
            }
            $data[] = $row;
        }

        //如果有找不到五字代码的港口则返回全部需要补齐五字代码的港口
        if (!empty($UndefinedPortCode)) {
            //2表示有些港口找不到五字代码
            $return['code'] = 2;
            $return['msg'] = '请补齐五字代码';
            $return['list_data'] = array_values($UndefinedPortCode);

        }else{
            $return['code'] = 1;
            $return['list_data'] = $data;
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * @title 获取港口名称和五字代码，以港品名称为键值的格式返回
     */
    private function get_port(){
        //获取港口代码
        $data = array();
        $result = $this->db->where('carrier', 20001)->get('biz_port_alias')->result_array();
        //把五字代码作为键值
        foreach ($result as $val) {
            $port_name = strtoupper(trim($val['port_name']));
            $data[$port_name] = $val['port_code'];
        }
        return $data;
    }

    /**
     * @title 保存港口别名
     */
    public function savePortCode(){
        $return = array('msg'   => '', 'code'  => 0);
        //提交来的数据，由两部分组成，base：基础数据和list_data：列表数据
        $_post = $this->input->post('data');
        $keys = array('port_name', 'port_code', 'carrier');
        $data = array();
        foreach($_post as $index => $val){
            $row = $index +1;
            //跳过空行
            if (count(array_filter($val)) < 1) continue;
            $val = array_combine($keys, $val);
            $val['carrier'] = 20001;

            //如果数据表中已经存在就跳过
            $where = array('port_name'=>$val['port_name'], 'carrier'=>20001);
            $is_exists = $this->db->select('count(*) as count')->where($where)->get('biz_port_alias')->row_array()['count'];
            if ($is_exists > 0) {
                continue;
            }
            //必填
            if ($val['port_name']=='' || $val['port_code']==''){
                $return['msg'] = "第{$row}行：港口信息不完整，请补齐后再提交！";
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            $data[] = $val;
        }

        if (count($data) < 1){
            $return['code'] = 1;
        }else{
            $query = $this->db->insert_batch('biz_port_alias', $data);
            if ($query > 0){
                $return['code'] = 1;
            }else{
                $return['msg'] = "保存失败！";
            }
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    //公海
    public function public_client(){
        if (is_ajax()){
            $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
            $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
            $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
            $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
            $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

            $where = array();
            $where[] = 'sales_id = 0';
            $where[] = 'apply_status between 1 and 3';

            //这里是新版的查询代码
            $title_data = get_user_title_sql_data('biz_crm_public', 'biz_crm_public_view');//获取相关的配置信息

            if(!empty($fields)){
                foreach ($fields['f'] as $key => $field){
                    $f = trim($fields['f'][$key]);
                    $s = $fields['s'][$key];
                    $v = trim($fields['v'][$key]);
                    if($f == '') continue;
                    //TODO 如果是datetime字段,=默认>=且<=
                    $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                    //datebox的用等于时代表搜索当天的
                    if(in_array($f, $date_field) && $s == '='){
                        if(!isset($title_data['sql_field'][$f])) continue;
                        $f = $title_data['sql_field'][$f];
                        if($v != ''){
                            $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                        }
                        continue;
                    }
                    //datebox的用等于时代表搜索小于等于当天最晚的时间
                    if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                        if(!isset($title_data['sql_field'][$f])) continue;
                        $f = $title_data['sql_field'][$f];
                        if($v != ''){
                            $where[] = "{$f} $s '$v 23:59:59'";
                        }
                        continue;
                    }

                    //把f转化为对应的sql字段
                    //不是查询字段直接排除
                    $title_f = $f;
                    if(!isset($title_data['sql_field'][$title_f])) continue;
                    $f = $title_data['sql_field'][$title_f];


                    if($v !== '') {
                        //这里缺了2个
                        if($v == '-') continue;
                        //如果进行了查询拼接,这里将join条件填充好
                        $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                        if(!empty($this_join)) {
                        $this_join0 = explode(' ', $this_join[0]);
                        $title_data['sql_join'][end($this_join0)] = $this_join;
                    }

                        $where[] = search_like($f, $s, $v);
                    }
                }
            }

            $where = join(' and ', $where);
            //where---end
            Model('biz_client_crm_model');
            $offset = ($page - 1) * $rows;
            $this->db->limit($rows, $offset);
            //获取需要的字段--start
            $selects = array(
                'biz_client_crm.id',
                'biz_client_crm.created_by',
                'biz_client_crm.apply_status',
            );
            foreach ($title_data['select_field'] as $key => $val) {
                if (!in_array($val, $selects)) $selects[] = $val;
            }

            //获取需要的字段--end
            $this->db->select(join(',', $selects), false);
            //这里拼接join数据
            foreach ($title_data['sql_join'] as $j) {
                $this->db->join($j[0], $j[1], $j[2]);
            }//将order字段替换为 对应的sql字段
            if (isset($title_data['sql_field']["biz_client_crm.{$sort}"])) {
                $sort = $title_data['sql_field']["biz_client_crm.{$sort}"];
            }
            $this->db->is_reset_select(false);//不重置查询
            $rs = $this->biz_client_crm_model->get($where, $sort, $order);
            $this->db->clear_limit();//清理下limit
            $result["total"] = $this->db->count_all_results('');
            $result['rows'] = $rs;
            return $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
        else
        {
            //获取全部销售和销售领导
            $sales = $this->db->select('id, leader_id, m_level, name')
                ->where_in('id', get_session('user_range'))
                ->where("status = 0 and FIND_IN_SET('sales', user_role)")->get('bsc_user')
                ->result_array();
            $this->load->view('biz/crm/public_client', array('sales'=>$sales));
        }
    }

    //待分配
    public function unassigned(){
        if (is_ajax()){
            $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
            $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
            $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "assign_date";
            $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
            $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

            //当前登录的用户ID
            $login_user_id = get_session('id');
            //获取销售经理
            $user = $this->db->select('id, leader_id, name')->where(array('m_level'=>8, 'status'=>0))->get('bsc_user')->result_array();
            $sales_manager = array_column($user, 'id');
            $where = array();
            //如果是销售经理则只读取属于销售经理的CRM客户
            if(in_array($login_user_id, $sales_manager) === true){
                $where[] = 'sales_id = '.$login_user_id;
            }else{
                $where[] = 'sales_id = ""';
            }

            //这里是新版的查询代码
            $title_data = get_user_title_sql_data('biz_crm_public', 'biz_crm_public_view');//获取相关的配置信息

            if(!empty($fields)){
                foreach ($fields['f'] as $key => $field){
                    $f = trim($fields['f'][$key]);
                    $s = $fields['s'][$key];
                    $v = trim($fields['v'][$key]);
                    if($f == '') continue;
                    //TODO 如果是datetime字段,=默认>=且<=
                    $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                    //datebox的用等于时代表搜索当天的
                    if(in_array($f, $date_field) && $s == '='){
                        if(!isset($title_data['sql_field'][$f])) continue;
                        $f = $title_data['sql_field'][$f];
                        if($v != ''){
                            $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                        }
                        continue;
                    }
                    //datebox的用等于时代表搜索小于等于当天最晚的时间
                    if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                        if(!isset($title_data['sql_field'][$f])) continue;
                        $f = $title_data['sql_field'][$f];
                        if($v != ''){
                            $where[] = "{$f} $s '$v 23:59:59'";
                        }
                        continue;
                    }

                    //把f转化为对应的sql字段
                    //不是查询字段直接排除
                    $title_f = $f;
                    if(!isset($title_data['sql_field'][$title_f])) continue;
                    $f = $title_data['sql_field'][$title_f];


                    if($v !== '') {
                        //这里缺了2个
                        if($v == '-') continue;
                        //如果进行了查询拼接,这里将join条件填充好
                        $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                        if(!empty($this_join)) {
                        $this_join0 = explode(' ', $this_join[0]);
                        $title_data['sql_join'][end($this_join0)] = $this_join;
                    }

                        $where[] = search_like($f, $s, $v);
                    }
                }
            }

            $where = join(' and ', $where);
            //where---end
            Model('biz_client_crm_model');
            $offset = ($page - 1) * $rows;
            $this->db->limit($rows, $offset);
            //获取需要的字段--start
            $selects = array(
                'biz_client_crm.id',
                'biz_client_crm.created_by',
                'biz_client_crm.apply_status',
            );
            foreach ($title_data['select_field'] as $key => $val) {
                if (!in_array($val, $selects)) $selects[] = $val;
            }

            //获取需要的字段--end
            $this->db->select(join(',', $selects), false);
            //这里拼接join数据
            foreach ($title_data['sql_join'] as $j) {
                $this->db->join($j[0], $j[1], $j[2]);
            }//将order字段替换为 对应的sql字段
            if (isset($title_data['sql_field']["biz_client_crm.{$sort}"])) {
                $sort = $title_data['sql_field']["biz_client_crm.{$sort}"];
            }
            $this->db->is_reset_select(false);//不重置查询
            $rs = $this->biz_client_crm_model->get($where, $sort, $order);

            $this->db->clear_limit();//清理下limit
            $result["total"] = $this->db->count_all_results('');
            $result['rows'] = $rs;
            return $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
        else
        {
            $sales = array();
            //获取全部销售和销售领导
            $user = $this->db->select('id, leader_id, m_level, name')->where(array('m_level'=>8, 'status'=>0))->or_where("FIND_IN_SET('sales', user_role)")->get('bsc_user')->result_array();
            //建立键值对型态
            foreach($user as $item){
                if ($item['m_level'] == 8) {
                    $item['child'] = array();
                    $leader[$item['id']] = $item;
                    $leader_id[] = $item['id'];
                }
            }
            //建立上下级关系型态
            foreach ($user as $val){
                if ($val['m_level'] != 8 && isset($leader[$val['leader_id']])){
                    array_push($leader[$val['leader_id']]['child'], $val);
                }
            }
            //田诗雯指派客户给销售经理或任意一个销售人员，销售经理指派给下级销售人员
            if (get_session('id') == 20001){
                array_push($leader_id, 20001);
                $sales = $leader;
            }else {
                //是否有指派权限
                if (in_array(get_session('id'), $leader_id)) {
                    $sales = $leader[get_session('id')]['child'];
                }
            }
            $this->load->vars('sales', $sales);
            $this->load->vars('leader_id', $leader_id);
            $this->load->view('biz/crm/unassigned', array('sales'=>$sales));
        }
    }

    //待分配数量
    public function get_unassigned_num(){
        $result = array('code' => 200, 'unassigned_num' => 0);
        $login_user_id = get_session('id');
        //只有田诗雯才可以强制转交客户
        if ($login_user_id == 20001){
            $where['sales_id'] = "";
        }elseif(getUserField($login_user_id, 'm_level') == 8){
            $where['sales_id'] = get_session('id');
        }else{
            $where['is_new'] = 1;
            $where['sales_id'] = get_session('id');
        }
        //待分配提醒
        $result['unassigned_num'] = $this->db->where($where)->count_all_results('biz_client_crm');
        return $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    //已分配
    public function assigned(){
        if (is_ajax()){
            $result = array('code' => 0, 'msg' => 'success', 'rows' => array(), 'total' => 0);
            $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
            $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
            $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
            $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
            $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

            //当前登录的用户ID
            $login_user_id = get_session('id');
            $where = array();
            if ($login_user_id == 20001){
                $where[] = 'from_user_id > 0';
            }else{
                $where[] = 'from_user_id = '.$login_user_id;
            }
            $where[] = 'sales_id > 0';

            //这里是新版的查询代码
            $title_data = get_user_title_sql_data('biz_crm_assigned', 'biz_crm_assigned_view');//获取相关的配置信息

            if(!empty($fields)){
                foreach ($fields['f'] as $key => $field){
                    $f = trim($fields['f'][$key]);
                    $s = $fields['s'][$key];
                    $v = trim($fields['v'][$key]);
                    if($f == '') continue;
                    //TODO 如果是datetime字段,=默认>=且<=
                    $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                    //datebox的用等于时代表搜索当天的
                    if(in_array($f, $date_field) && $s == '='){
                        if(!isset($title_data['sql_field'][$f])) continue;
                        $f = $title_data['sql_field'][$f];
                        if($v != ''){
                            $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                        }
                        continue;
                    }
                    //datebox的用等于时代表搜索小于等于当天最晚的时间
                    if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                        if(!isset($title_data['sql_field'][$f])) continue;
                        $f = $title_data['sql_field'][$f];
                        if($v != ''){
                            $where[] = "{$f} $s '$v 23:59:59'";
                        }
                        continue;
                    }

                    //把f转化为对应的sql字段
                    //不是查询字段直接排除
                    $title_f = $f;
                    if(!isset($title_data['sql_field'][$title_f])) continue;
                    $f = $title_data['sql_field'][$title_f];


                    if($v !== '') {
                        //这里缺了2个
                        if($v == '-') continue;
                        //如果进行了查询拼接,这里将join条件填充好
                        $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                        if(!empty($this_join)) {
                        $this_join0 = explode(' ', $this_join[0]);
                        $title_data['sql_join'][end($this_join0)] = $this_join;
                    }

                        $where[] = search_like($f, $s, $v);
                    }
                }
            }

            $where = join(' and ', $where);
            //where---end
            Model('biz_client_crm_model');
            $offset = ($page - 1) * $rows;
            $this->db->limit($rows, $offset);
            //获取需要的字段--start
            $selects = array(
                'biz_client_crm.assign_date as assign_date',
                'biz_client_crm.close_plan_date as close_plan_date',
                'DATEDIFF(now(), biz_client_crm.assign_date) as passed',
                'biz_client_crm.id',
                'biz_client_crm.created_by',
                'biz_client_crm.apply_status',
            );
            foreach ($title_data['select_field'] as $key => $val) {
                if (!in_array($val, $selects)) $selects[] = $val;
            }

            //获取需要的字段--end
            $this->db->select(join(',', $selects), false);
            //这里拼接join数据
            foreach ($title_data['sql_join'] as $j) {
                $this->db->join($j[0], $j[1], $j[2]);
            }//将order字段替换为 对应的sql字段
            if (isset($title_data['sql_field']["biz_client_crm.{$sort}"])) {
                $sort = $title_data['sql_field']["biz_client_crm.{$sort}"];
            }
            $this->db->is_reset_select(false);//不重置查询
            $rs = $this->biz_client_crm_model->get($where, $sort, $order);
            //echo $this->db->last_query();
            $this->db->clear_limit();//清理下limit
            $result["total"] = $this->db->count_all_results('');
            foreach ($rs as $row) {
                $row['apply_status_name'] = isset($apply_status_values[$row['apply_status']]) ? $apply_status_values[$row['apply_status']] : '';

                //2022-04-26新增一个完整度统计 计算规则为 联系方式权重2 其他权重1 加起来刚好10
                $row['score'] = 0;
                if (!empty($row['email'])) $row['score'] += 20;
                if (!empty($row['telephone'])) $row['score'] += 20;
                if (!empty($row['live_chat'])) $row['score'] += 20;
                if (!empty($row['company_address'])) $row['score'] += 10;
                if (!empty($row['export_sailing'])) $row['score'] += 10;
                if (!empty($row['trans_mode'])) $row['score'] += 10;
                if (!empty($row['product_type'])) $row['score'] += 10;
                $row['score'] .= "%";

                $row['progress'] = 0;
                if ($row['close_plan_date'] && $row['assign_date']){
                    if ($row['close_plan_date'] && $row['assign_date']){
                        $total_days = floor((strtotime($row['close_plan_date']) - strtotime($row['assign_date'])) / 86400);
                        $row['progress'] = $total_days > 0 ? floor(strval(($row['passed'] / $total_days)*10000))/10000*100 : 100;
                    }
                }
                $result['rows'][] = $row;
            }
            return $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
        else
        {

            $this->load->view('biz/crm/assigned');
        }
    }

    /**
     * CRM日志
     */
    public function crm_log()
    {
        $crm_id = (int)$this->input->get('crm_id');
        $field = "*,";
        $field .="(select name from bsc_user where id=sales_id limit 1) as sales_name,";
        $field .="(select name from bsc_user where id=operation_id limit 1) as operation_name,";
        $query = $this->db->select($field)->where('id_no', $crm_id)->order_by('id', 'desc')->get('biz_client_crm_log');
        $data = $query->result_array();

        $this->load->vars('data', $data);
        $this->load->view('biz/crm/crm_log');
    }

    /**
     * @title CRM申请
     */
    public function create_crm_approval(){
        $return = array('code' => 0, 'msg' => '参数错误');
        $crm_id = (int)$this->input->get('crm_id');
        if ($crm_id < 1) return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        $query = $this->db->where('id', $crm_id)->get('biz_client_crm');
        $row = $query->row_array();
        if (empty($row)) return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        //只有销售人员才能申请审批
        $login_user_id = get_session('id');
        if ($row['sales_id'] != $login_user_id && $row['created_by'] != $login_user_id){
            $return['msg'] = '角色错误，当前账号不是该客户的销售！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        if($this->db->where('crm_id', $crm_id)->count_all_results('biz_client_contact_list') == 0){
            $return['msg'] = '未创建联系人，不能提交审核！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        //如果不是手动添加的抬头，提交审核时至少要有一条跟踪日志
        if($row['add_way'] != 'hand' && $this->db->where('crm_id', $crm_id)->count_all_results('biz_client_follow_up_log') == 0){
            $return['msg'] = '未创建跟进记录，不能提交审核！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        //审批人
        $operation_id = getUserField($login_user_id, 'leader_id');
        //todo 创建审批事务
        $remark = $row['company_name'];
        $contact = $this->db->select('telephone, telephone2, email')->where("crm_id = {$crm_id} and (email <> '' or telephone <> '')")->get('biz_client_contact_list')->row_array();
        if(!empty($contact)){
            $remark .= '<br><font color="#1e90ff"> 手机号：' . $contact['telephone'] . '</font><br><font color="#1e90ff">座机号：' . $contact['telephone2'] . '</font><br><font color="#d2691e">邮箱：' . $contact['email'] . '</font>';
        }
        $params = array('id_no' => $crm_id,'id_type' => 1,'operation_id'=>$operation_id,'remark' => $remark);
        $this->load->library('approval', $params);
        if ($this->approval->get_runStatus() === false){
            $return['msg'] = $this->approval->error_msg;
        }else{
            $return = array('code' => 1, 'msg' => 'success');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * @title CRM导入
     */
    public function import_crm(){
        if (is_ajax()){
            $return = array('code'=>0, 'msg'=>'');
            $_post = $this->input->post();
            $_post['sales_id'] = get_session('id');

            //校验必填项
            $validate_field = [
                'country_code'=>lang('国家代码'),
                'company_name'=>lang('客户全称'),
                'client_name'=>lang('客户简称'),
                'company_address'=>lang('客户地址'),
                'export_sailing'=>lang('出口航线'),
                'trans_mode'=>lang('运输方式'),
                'web_url'=>lang('网址'),
                'role'=>lang('客户角色'),
                'contact'=>lang('联系人'),
                'email'=>lang('邮箱'),
            ];
            foreach ($validate_field as $key => $val)
            {
                if (!isset($_post[$key]) || $_post[$key] == ''){
                    $return['msg'] = lang('{field}必须填写！', array("field" => $val));
                    return $this->output->set_content_type('application/json')->set_output(json_encode($return));
                }
            }

            //校验提交
            $validate = $this->validate($_post, 'edit');
            if ($validate !== true){
                $return['msg'] = $validate;
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            //创建写入字段
            $data = $this->create_date($_post);
            $data['client_name'] = strtoupper($data['client_name']);
            $data['company_name'] = strtoupper($data['company_name']);
            $data['company_name_en'] = strtoupper($data['company_name_en']);

            $data['company_search'] = match_chinese($data['company_name']);
            $data["approve_user"] = getUserField($data['sales_id'], 'leader_id');
            $data["created_group"] = implode(',', $this->session->userdata('group') );
            $data["apply_type"] = 1;
            $data["apply_status"] = 2;//默认1 改为了2
            $data["created_by"] = get_session('id');
            $data["created_time"] = date('Y-m-d H:i:s');
            $data["last_follow_time"] = '0000-00-00 00:00:00';
            $data["last_add_contact_time"] = '0000-00-00 00:00:00';
            $data["add_way"] = 'import';

            //相同销售相同抬头已经存在
            $where = array('company_search'=>$data['company_search'], 'sales_id'=>get_session('id'));
            if ($this->db->where($where)->count_all_results('biz_client_crm') > 0){
                $return['code'] = 1;
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            //新增CRM数据
            if ($this->db->insert('biz_client_crm', $data)) {
                $return['code'] = 1;
                $id = $this->db->insert_id();
                $contact_data = array('crm_id'=>$id, 'name'=>$_post['contact'], 'telephone'=>$_post['telephone'],'email'=>$_post['email'], 'create_by'=>get_session('id'));
                $this->db->insert('biz_client_contact_list', $contact_data);
                // 记录日志
                $this->write_log($id, 'insert', $data);
            } else {
                $return['msg'] = "Query failed！";
            }
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }
        else
        {
            $country_code = $this->db->where('catalog', 'country_code')->order_by('value', 'asc')->get('bsc_dict')->result_array();
            $export_sailing = $this->db->where('catalog', 'export_sailing')->get('bsc_dict')->result_array();
            $trans_mode = $this->db->where('catalog', 'client_trans_mode')->get('bsc_dict')->result_array();
            $client_role = $this->db->where("catalog='role' and ext1=1")->get('bsc_dict')->result_array();
            $product_type = $this->db->where("catalog='product_type'")->get('bsc_dict')->result_array();
            $contact_position = $this->db->where("catalog='contact_position'")->get('bsc_dict')->result_array();
            $data = array(
                'country_code' => array_column($country_code, 'value'),
                'export_sailing' => array_column($export_sailing, 'value'),
                'trans_mode' => array_column($trans_mode, 'value'),
                'client_role' => array_column($client_role, 'value'),
                'product_type' => array_column($product_type, 'value'),
                'contact_position' => [''] + array_column($contact_position, 'value'),
            );
            $this->load->view('biz/crm/import_crm', $data);
        }
    }

    /**
     * @title CRM导出
     */
    public function export_crm(){
        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_client_export', 'biz_client_export_view');//获取相关的配置信息

        $this->load->vars('table_col', $title_data['data']);
        $this->load->view('biz/crm/export_crm');
    }

    public function get_export_data(){
        $result = array('code' => 0, 'rows' => array(), 'total' => 0);

        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $limit = isset($_POST['limit']) ? intval($_POST['limit']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $f_role = isset($_REQUEST['f_role']) ? $_REQUEST['f_role'] : '';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $crm_user = postValue('crm_user', get_session('id'));
        $crm_user_type = postValue('crm_user_type', 'user');

        $where = array();
        $where[] = 'biz_client_crm.company_name <> ""';
        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_client_export', 'biz_client_export_view');//获取相关的配置信息
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $f = str_replace('biz_client_crm', 'biz_client_export', $f);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];

                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') continue;
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) {
                        $this_join0 = explode(' ', $this_join[0]);
                        $title_data['sql_join'][end($this_join0)] = $this_join;
                    }

                    //如果条件是”来源“，1：领导指派或2：个人新增
                    if ($f == 'from_user_id'){
                        $where[] = $v==1 ? 'from_user_id > 0' : 'from_user_id = 0';
                    }else{
                        $where[] = search_like($f, $s, $v);
                    }
                }
            }
        }

        $where[] = "biz_client_crm.apply_type = 1";
        if ($f_role != '') $where[] = "FIND_IN_SET('{$f_role}', biz_client_crm.role)";
        // if(is_admin()){
        if ($crm_user_type == 'user') {
            //2022-01-10 新需求 CRM获取数据逻辑 原创建人 改为 创建人+销售
            //2022-02-23 改版后, crm获取数据,只自己获取自己的
            $where[] = "(biz_client_crm.sales_id = '$crm_user')";
        } else if ($crm_user_type == 'group') {
            $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) = '$crm_user'";
        } else if ($crm_user_type == 'sub_company') {
            $this_crm_user = str_replace('company_', '', $crm_user);

            $groups = Model('bsc_group_model')->get_child_group($this_crm_user);
            $groups_code = array_column($groups, 'group_code');
//            $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) like '$this_crm_user-%'";
            $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) in ('" . join('\',\'', $groups_code) . "')";
        } else if ($crm_user_type == 'head') {
            if ($crm_user == 'HEAD') {
//                Model('bsc_group_model');
//                $this->db->select('group_code');
//                $companys = $this->bsc_group_model->get('parent_id = 1001');
//                $sales_ids_or = array();
//                foreach ($companys as $company) {
//                    $sales_ids_or[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) like '%{$company['group_code']}-%'";
//                }
//                if (empty($sales_ids_or)) $sales_ids_or[] = "1 = 0";
//                $where[] = "(" . join(' or ', $sales_ids_or) . ")";
                $groups = Model('bsc_group_model')->get_child_group($crm_user);
                $groups_code = array_column($groups, 'group_code');
                $where[] = "(select `group` from bsc_user where bsc_user.id = biz_client_crm.sales_id) in ('" . join('\',\'', $groups_code) . "')";
            }
        } else {
            $where[] = '0 = 1';
        }

        $where = join(' and ', $where);
        // $where .= " or (biz_client.client_level >= 0 and biz_client_follow_up.status = 0 and $crm_where)";
        //where---end
        Model('biz_client_crm_model');
        $offset = ($page - 1) * $limit;
        $this->db->limit($limit, $offset);
        //获取需要的字段--start
        $selects = array(
            'biz_client_crm.assign_date as assign_date',
            'biz_client_crm.close_plan_date as close_plan_date',
            'DATEDIFF(now(), biz_client_crm.assign_date) as passed',
            'biz_client_crm.id',
            'biz_client_crm.created_by',
            'biz_client_crm.apply_status',
            'biz_client_crm.is_new',
        );
        foreach ($title_data['select_field'] as $key => $val) {
            if (!in_array($val, $selects)) $selects[] = $val;
        }

        //获取需要的字段--end
        $this->db->select(join(',', $selects), false);
        //这里拼接join数据
        foreach ($title_data['sql_join'] as $j) {
            $this->db->join($j[0], $j[1], $j[2]);
        }//将order字段替换为 对应的sql字段
        if (isset($title_data['sql_field']["biz_client_crm.{$sort}"])) {
            $sort = $title_data['sql_field']["biz_client_crm.{$sort}"];
        }
        $this->db->is_reset_select(false);//不重置查询
        $rs = $this->biz_client_crm_model->get($where, $sort, $order);
        //echo $this->db->last_query();exit;
        $this->db->clear_limit();//清理下limit
        $result["count"] = $this->db->count_all_results('');
        $result['data'] = $rs;


        return $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
     * @title 线索
     */
    public function clue(){
        $this->db = Model('op_model')->get_db();
        if (is_ajax()){
            $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
            $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
            $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "clue_num";
            $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
            $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

            $where = array();
            $where[] = 'apply_status > -99';    //排除黑名单
            $where[] = 'clue_status BETWEEN 1 and 2'; //只要分析中和可提取的
            $where[] = 'clue_name <> \'\'';     //CRM线索有两个类型，1类是导入的线索，有线索名称，2类是手动添加的线索，没有线索名称，当前要获取的是第一类
            //2023-10-08 这里改为使用国内的库, 但是国家代码统一获取当前国家的
            $where[] = "country_code = '" . get_system_type() . "'";


            //这里是新版的查询代码
            $title_data = get_user_title_sql_data('biz_crm_clue', 'biz_crm_clue_view');//获取相关的配置信息

            if(!empty($fields)){
                foreach ($fields['f'] as $key => $field){
                    $f = trim($fields['f'][$key]);
                    $s = $fields['s'][$key];
                    $v = trim($fields['v'][$key]);
                    if($f == '') continue;
                    //TODO 如果是datetime字段,=默认>=且<=
                    $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                    //datebox的用等于时代表搜索当天的
                    if(in_array($f, $date_field) && $s == '='){
                        if(!isset($title_data['sql_field'][$f])) continue;
                        $f = $title_data['sql_field'][$f];
                        if($v != ''){
                            $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                        }
                        continue;
                    }
                    //datebox的用等于时代表搜索小于等于当天最晚的时间
                    if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                        if(!isset($title_data['sql_field'][$f])) continue;
                        $f = $title_data['sql_field'][$f];
                        if($v != ''){
                            $where[] = "{$f} $s '$v 23:59:59'";
                        }
                        continue;
                    }

                    //把f转化为对应的sql字段
                    //不是查询字段直接排除
                    $title_f = $f;
                    if(!isset($title_data['sql_field'][$title_f])) continue;
                    $f = $title_data['sql_field'][$title_f];


                    if($v !== '') {
                        //这里缺了2个
                        if($v == '-') continue;
                        //如果进行了查询拼接,这里将join条件填充好
                        $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                        if(!empty($this_join)) {
                        $this_join0 = explode(' ', $this_join[0]);
                        $title_data['sql_join'][end($this_join0)] = $this_join;
                    }

                        $where[] = search_like($f, $s, $v);
                    }
                }
            }

            $where = join(' and ', $where);
            //where---end
            Model('biz_client_crm_model');
            $offset = ($page - 1) * $rows;
            $this->db->limit($rows, $offset);
            //获取需要的字段--start
            $selects = array(
                'biz_client_crm.id',
                'biz_client_crm.country_code',
                'biz_client_crm.company_search',
                'biz_client_crm.company_search_en',
                'biz_client_crm.created_by',
                'biz_client_crm.apply_status',
                '( SELECT count( * ) FROM biz_client_crm_clue WHERE biz_client_crm_clue.crm_id = biz_client_crm.id ) AS clue_num'
            );
            foreach ($title_data['select_field'] as $key => $val) {
                if (!in_array($val, $selects)) $selects[] = $val;
            }

            //获取需要的字段--end
            $this->db->select(join(',', $selects), false);
            //这里拼接join数据
            foreach ($title_data['sql_join'] as $j) {
                $this->db->join($j[0], $j[1], $j[2]);
            }//将order字段替换为 对应的sql字段
            if (isset($title_data['sql_field']["biz_client_crm.{$sort}"])) {
                $sort = $title_data['sql_field']["biz_client_crm.{$sort}"];
            }
            $this->db->is_reset_select(false);//不重置查询
            $query = $this->db->where($where, null, false)->order_by($sort, $order)->get('biz_client_crm');
            //echo $this->db->last_query();exit;
            $rs = $query->result_array();
            $this->db->clear_limit();//清理下limit
            $result["total"] = $this->db->count_all_results('');
            $result['rows'] = $rs;
            return $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
        else
        {
            //总数量
            $total = $this->db->where('clue_status between 1 and 3')->count_all_results('biz_client_crm');
            //分析中数量
            $loading = $this->db->where('clue_status = 1')->count_all_results('biz_client_crm');
            //可提取数量
            $gain = $this->db->where('clue_status = 2')->count_all_results('biz_client_crm');
            //不可提取数量
            $not_gain = $this->db->where('clue_status = 3')->count_all_results('biz_client_crm');
            $data['load_proportion'] = $data['gain_proportion'] = $data['not_gain'] = 0;
            $data['total'] = $total;
            if ($total > 0) {
                $data['load_proportion'] = floor(strval(($loading / $total) * 10000)) / 10000 * 100;
                $data['gain_proportion'] = floor(strval(($gain / $total) * 10000)) / 10000 * 100;
                $data['not_gain'] = floor(strval(($not_gain / $total) * 10000)) / 10000 * 100;
            }

            $this->load->view('biz/crm/clue', $data);
        }
    }

    /**
     * 全部线索
     */
    public function clue_detail(){
        $company_search = $this->input->get('company_search');
        $clue_pure_text = $this->input->get('clue_pure_text');
        $id = (int)getValue('id', 0);
        if (empty(trim($company_search))){
            return $this->load->view('/errors/html/error_layui', array('msg'=>'参数错误！', 'icon'=>'layui-icon-404'));
        }
        $this->db = Model('op_model')->get_db();

        //创建浏览日志
        $row = $this->db->select('id, company_search, company_name_en, clue_name')->where("id", $id)->get('biz_client_crm')->row_array();
        if (!empty($row)) create_browse_log('clue1', $row['id'], $row['clue_name']);

        $create_by = '(select `name` from bsc_user where id = biz_client_crm_clue.created_by) as created_name';
        //模糊匹配线索名称
        $where = "clue_pure_text = '{$company_search}'";
        if ($clue_pure_text != '') $where .= " or clue_pure_text = '{$clue_pure_text}'";
        /*foreach ($clue_name_en as $val){
            $where .= " or clue_pure_text like '%{$val}%'";
        }*/

        $result = $this->db->select("biz_client_crm_clue.*, {$create_by}")
            ->where($where)
            ->order_by('id', 'desc')
            ->get('biz_client_crm_clue')
            ->result_array();
        if (empty($result)) {
            return $this->load->view('/errors/html/error_layui', array('msg'=> lang('暂无相关线索！'), 'icon'=>'layui-icon-face-cry'));
        }else{
            foreach ($result as $index => $item){
                if (!empty($item['bill_no'])){
                    $is_consol = $this->db->where("carrier_ref='{$item['bill_no']}' and customer_booking != 1")->count_all_results('biz_consol');
                    if ($is_consol){
                        //$item['bill_no'] = "<span style='color: #ff0000'>(公司内)</span>{$item['bill_no']}";
                        $result[$index]['remark'] = $item['remark'] . "<br><span style='color: #ff0000'>" . lang('公司货') . "</span>";
                    }
                }
            }
        }
        $this->load->vars('data', $result);
        $this->load->view('head');
        $this->load->view('biz/crm/clue_detail');
    }

    /**
     * @title 提取线索——视图
     */
    public function gaining(){+
        $clue_id = (int)$this->input->get('clue_id');
        if ($clue_id == 0){
            return $this->load->view('/errors/html/error_layui', array('msg'=>'参数错误！', 'icon'=>'layui-icon-404'));
        }
        $this->db = Model('op_model')->get_db();

        $userId = $this->session->userdata('id');
        $userBsc = $this->bsc_user_model->get_by_id($userId);
        $where = array('id' => $clue_id, 'clue_status' => 2);
        $subQuery = '(select clue_type from biz_client_crm_clue where crm_id=biz_client_crm.id limit 1) as clue_type';
        $data = $this->db->select('biz_client_crm.*, '.$subQuery)->where($where)->get('biz_client_crm')->row_array();
        if (empty($data)){
            return $this->load->view('/errors/html/error_layui', array('msg'=>'线索不存在或不可提取！', 'icon'=>'layui-icon-404'));
        }

        $this->load->vars('user', $userBsc);
        $this->load->view('head');
        $this->load->view('biz/crm/gaining_view', $data);
    }

    /**
     * @提取线索——写入
     */
    public function gaining_data(){
        $return = array('code'=>0, 'msg'=>'');
        $this->opchina_db = Model('op_model')->get_db();
        $_post = $this->input->post();
        $clue_id = (int)$this->input->get('clue_id');
        $crm_clue = $this->opchina_db->select('apply_status, clue_name')->where('id', $clue_id)->get('biz_client_crm')->row_array();
        if (!$crm_clue){
            $return['msg'] = '参数错误！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        //校验提交
        $validate = $this->validate($_post, 'clue_insert');
        if ($validate !== true){
            $return['msg'] = $validate;
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }

        //创建写入字段
        $data = $this->create_date($_post);
        $data["clue_name"] = $crm_clue["clue_name"];
        $data['sales_id'] = (int)$data['sales_id'] == 0 ? get_session('id') : $data['sales_id'];
        $data['company_search'] = match_chinese($data['company_name']);
        $data["approve_user"] = getUserField($data['sales_id'], 'leader_id');
        $data["created_group"] = implode(',', $this->session->userdata('group') );
        $data["created_by"] = $data["updated_by"] = get_session('id');
        $data["created_time"] = $data["updated_time"] = date('Y-m-d H:i:s');
        $data["apply_type"] = 1;
        $data["apply_status"] = 2;  //1表示为申请中 //默认1 改为了2
        $data["add_way"] = 'clue1';

        //新增CRM数据
        if ($this->db->insert('biz_client_crm', $data)) {
            $insert_id = $this->db->insert_id();
            $this->db->where('id', $clue_id)->set('gained_num', 'gained_num+1', false)->update('biz_client_crm');
            $return['code'] = 1;
            // 记录日志
            $this->write_log($clue_id, 'insert', $data);

            //创建客户流转日志
            $remark = '从“线索1”中提取， 指派销售为：' . getUserName($data['sales_id']);
            create_crm_log($insert_id, 0, get_session('id'), $remark);
        } else {
            $return['msg'] = "Query failed！";
        }

        //返回操作结果
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * 线索分析
     */
    public function clue_analysis(){
        $company_name = isset($_GET['company_name']) ? $_GET['company_name'] : '';
        $crm_id = isset($_GET['crm_id']) ? $_GET['crm_id'] : 0;
        $data = $where = $client = $crm = array();
        if (!empty($company_name)) {
            $company_name = match_chinese($company_name);
            $where['company_search'] = $company_name;

            //分析往来单位
            $client = $this->db->select('id, client_code,company_name, sales_ids, client_level,client_level_remark')->where($where)->get('biz_client')->row_array();
            //如果抬头已经存在
            if ($client) {
                $rs_ids = array($client['id']);
                $this->db->select("group_concat(biz_tag.tag_name) as tag_names,biz_tag_relation.id_no");
                $this->db->join("biz_tag", "biz_tag.id = biz_tag_relation.tag_id");
                $this->db->group_by("biz_tag_relation.id_no");
                //显示操作类标签
                $tag_arr = array_column(Model('biz_tag_relation_model')->get("biz_tag.tag_class= '操作类' and biz_tag_relation.id_type = 'biz_client' and biz_tag_relation.id_no in ('" . join("','", $rs_ids) . "')", 'biz_tag_relation.id'), null, 'id_no');
                Model('m_model');
                $client['sales_ids_names'] = '';
                $sql = "select id,booking_ETD from biz_shipment where client_code = '{$client['client_code']}' and status = 'normal' order by booking_ETD desc";
                $last_shipment = $this->m_model->query_one($sql);
                //新增未合作天数 查询最后一笔normal的单子
                if (empty($last_shipment)) {
                    $client['not_traded_days'] = lang('未进行过交易');
                } else {
                    $booking_ETD = $last_shipment['booking_ETD'];
                    $diff_day = round((time() - strtotime($booking_ETD)) / (3600 * 24));//相差天数
                    if ($diff_day <= 10) {
                        $client['not_traded_days'] = lang('10天内有交易');
                    } else {
                        $client['not_traded_days'] = lang('{day}天前有交易', array('day' => $diff_day));
                    }
                }

                //从duty表中取出当前客户的所有销售人员，拼接成字符串输出
                $this->db->select('bsc_user.name')->where(array('biz_client_duty.client_code' => $client['client_code'], 'biz_client_duty.user_role' => 'sales'));
                $this->db->join('bsc_user', 'bsc_user.id = biz_client_duty.user_id', 'LEFT');
                $users = $this->db->get('biz_client_duty')->result_array();
                if ($users) $client['sales_ids_names'] = join(',', array_column($users, 'name'));
                $client['tag'] = isset($tag_arr[$client['id']]) ? $tag_arr[$client['id']]['tag_names'] : '';
                $this->db->where('id', $crm_id)->update('biz_client_crm', array('company_search'=>$company_name));
            }

            //分析CRm
            $where['sales_id <> '] = '';
            $where['id <> '] = $crm_id;
            $field = '*,';
            $field .= '(select email from biz_client_contact_list where crm_id = biz_client_crm.id limit 1) as email,';
            $field .= '(select telephone from biz_client_contact_list where crm_id = biz_client_crm.id limit 1) as telephone,';
            $field .= '(select live_chat from biz_client_contact_list where crm_id = biz_client_crm.id limit 1) as live_chat,';
            $rows = $this->db->select($field)->where($where)->get('biz_client_crm')->result_array();
            foreach ($rows as $k => $row) {
                $row['score'] = 0;
                if (!empty($row['email'])) $row['score'] += 20;
                if (!empty($row['telephone'])) $row['score'] += 20;
                if (!empty($row['live_chat'])) $row['score'] += 20;
                if (!empty($row['company_address'])) $row['score'] += 10;
                if (!empty($row['export_sailing'])) $row['score'] += 10;
                if (!empty($row['trans_mode'])) $row['score'] += 10;
                if (!empty($row['product_type'])) $row['score'] += 10;
                $row['score'] .= "%";

                //获取多个销售名
                $sales = $this->db->where('id', $row['sales_id'])->get('bsc_user')->row_array();
                $row['sales_name'] = $sales['name'];
                $row['sales_email'] = $sales['email'];
                $crm[] = $row;
            }
        }

        $data['crm'] = $crm;
        $data['client'] = $client;
        $data['company_name'] = $company_name;
        $this->load->view("/biz/crm/clue_analysis", $data);
    }

    /**
     * @title 获取港口别名
     */
    public function get_port_data(){
        $data = array('code'=>0,'total'=>0, 'rows'=>[]);
        $page = isset($_GET['page']) && (int)$_GET['page'] > 0 ? (int)$_GET['page'] : 1;
        $limit = isset($_GET['limit']) && (int)$_GET['limit'] > 0 ? (int)$_GET['limit'] : 10;
        $field = isset($_GET['field']) && $_GET['field'] ? $_GET['field'] : '';
        $value = isset($_GET['val']) && $_GET['val'] ? $_GET['val'] : '';
        $where = array();
        $where['carrier'] = 20001;
        if ($field!='' && $value!=''){
            $where[$field] = $value;
        }

        $count = $this->db->select('count(*) count')->where($where)->get('biz_port_alias')->row_array()['count'];
        if ($count > 0)
        {
            $page_total = ceil($count / $limit);
            if ( $page > $page_total ) $page = $page_total;
            $pages = $limit * ($page-1);

            $this->db->select('* , (SELECT name 	FROM bsc_user WHERE	id = carrier) as name')->limit($limit, $pages);
            //echo $this->db->last_query();
            $data['count'] = $count;
            $data['data'] = $this->db->where($where)->get('biz_port_alias')->result_array();
        }

        //echo $this->db->last_query();

        return $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    /**
     * title CRM黑名单
     */
    public function black_list(){
        if (is_ajax()){
            $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
            $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
            $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
            $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
            $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

            $where = array();
            $where[] = 'apply_status = -99';

            //这里是新版的查询代码
            $title_data = get_user_title_sql_data('biz_crm_black_list', 'biz_crm_black_list_view');//获取相关的配置信息

            if(!empty($fields)){
                foreach ($fields['f'] as $key => $field){
                    $f = trim($fields['f'][$key]);
                    $s = $fields['s'][$key];
                    $v = trim($fields['v'][$key]);
                    if($f == '') continue;
                    //TODO 如果是datetime字段,=默认>=且<=
                    $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                    //datebox的用等于时代表搜索当天的
                    if(in_array($f, $date_field) && $s == '='){
                        if(!isset($title_data['sql_field'][$f])) continue;
                        $f = $title_data['sql_field'][$f];
                        if($v != ''){
                            $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                        }
                        continue;
                    }
                    //datebox的用等于时代表搜索小于等于当天最晚的时间
                    if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                        if(!isset($title_data['sql_field'][$f])) continue;
                        $f = $title_data['sql_field'][$f];
                        if($v != ''){
                            $where[] = "{$f} $s '$v 23:59:59'";
                        }
                        continue;
                    }

                    //把f转化为对应的sql字段
                    //不是查询字段直接排除
                    $title_f = $f;
                    if(!isset($title_data['sql_field'][$title_f])) continue;
                    $f = $title_data['sql_field'][$title_f];


                    if($v !== '') {
                        //这里缺了2个
                        if($v == '-') continue;
                        //如果进行了查询拼接,这里将join条件填充好
                        $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                        if(!empty($this_join)) {
                        $this_join0 = explode(' ', $this_join[0]);
                        $title_data['sql_join'][end($this_join0)] = $this_join;
                    }

                        $where[] = search_like($f, $s, $v);
                    }
                }
            }

            $where = join(' and ', $where);
            //where---end
            Model('biz_client_crm_model');
            $offset = ($page - 1) * $rows;
            $this->db->limit($rows, $offset);
            //获取需要的字段--start
            $selects = array();
            foreach ($title_data['select_field'] as $key => $val) {
                if (!in_array($val, $selects)) $selects[] = $val;
            }

            //获取需要的字段--end
            $this->db->select(join(',', $selects), false);
            //这里拼接join数据
            foreach ($title_data['sql_join'] as $j) {
                $this->db->join($j[0], $j[1], $j[2]);
            }//将order字段替换为 对应的sql字段
            if (isset($title_data['sql_field']["biz_client_crm.{$sort}"])) {
                $sort = $title_data['sql_field']["biz_client_crm.{$sort}"];
            }
            $this->db->is_reset_select(false);//不重置查询
            $rs = $this->biz_client_crm_model->get($where, $sort, $order);
            $this->db->clear_limit();//清理下limit
            $result["total"] = $this->db->count_all_results('');
            $result['rows'] = $rs;
            return $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
        else
        {
            $this->load->view('biz/crm/black_list');
        }
    }
    //加入黑名单
    public function add_black_list(){
        $return = array('code'=>0, 'msg'=>'');
        $ids = (array)$this->input->post('ids');
        $black_list_cause = $this->input->post('black_list_cause');
        $result = false;

        if (empty($ids)){
            $return['msg'] = '参数错误！';
        }else{
            $data = array('apply_status'=> -99, 'black_list_cause'=>$black_list_cause, 'black_list_time'=>date('Y-m-d H:i:s'));
            foreach($ids as $id) {
                $crm = $this->db->select('id, sales_id')->where('id', $id)->get('biz_client_crm')->row_array();
                if (empty($crm)) continue;
                if ($this->db->where('id', $id)->update('biz_client_crm', $data)){
                    $return['code'] = 1;
                    $this->write_log($id, 'update', $data);
                    create_crm_log($id, $crm['sales_id'], get_session('id'), '加入黑名单，原因：'.$black_list_cause);
                    $result = true;
                }
            }
        }

        if ($result == false){
            $return['msg'] = '操作失败：Query failed！';
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    //移出黑名单
    public function remove_black_list(){
        $return = array('code'=>0, 'msg'=>'');
        $ids = (array)$this->input->post('ids');
        $result = false;

        if (empty($ids)){
            $return['msg'] = '参数错误！';
        }else{
            $data = array('apply_status'=> 1);
            foreach($ids as $id) {
                $crm = $this->db->select('id, sales_id, clue_status')->where('id', $id)->get('biz_client_crm')->row_array();
                if (empty($crm)) continue;
                if ($crm['clue_status'] > 0) $data['apply_status'] = -2;
                if ($this->db->where('id', $id)->update('biz_client_crm', $data)){
                    $return['code'] = 1;
                    $this->write_log($id, 'update', $data);
                    create_crm_log($id, $crm['sales_id'], get_session('id'), '移出黑名单');
                    $result = true;
                }
            }
        }

        if ($result == false){
            $return['msg'] = '操作失败：Query failed！';
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    //跟进超180天
    public function ovewstep(){
        $r = $this->db->query('SELECT `id`, `sales_id`, DATEDIFF(now(), biz_client_crm.created_time) as total_day FROM `biz_client_crm` WHERE `sales_id` >0 and DATEDIFF(now(),biz_client_crm.created_time) > 180 ORDER BY `total_day` DESC LIMIT 5');
        $data = $r->result_array();

        foreach($data as $item){
            $query = $this->db->where('id', $item['id'])->update('biz_client_crm', array('sales_id' => 0, 'apply_status'=>1));
            if ($query){
                create_crm_log($item['id'], $item['sales_id'], '20280', '跟进超过180天自动返回公海！');
            }
        }
    }

    /**
     * @title 过滤post提交
     * @param array $post
     * @return array
     */
    private function create_date($post = []){
        $field = ['clue_name', 'country_code', 'client_name', 'company_name_en', 'company_search', 'company_name', 'company_address', 'created_group',
            'export_sailing', 'trans_mode', 'product_type', 'product_details', 'client_source', 'approve_user', 'role',
            'sales_id','province','city','area','web_url'];
        $data = [];
        foreach ($field as $val){
            $data[$val] = isset($post[$val])?$post[$val]:'';
        }
        return $data;
    }

    /**
     * @title 校验post提交
     * @param array $data
     * @return bool|string
     */
    private function validate($data = [], $event='insert'){
        //校验必填项
        $validate_field = [
            'company_name'=>'客户全称',
            'company_address'=>'客户地址',
            'country_code'=>'国家代码',
            // 'export_sailing'=>'出口航线',
            // 'trans_mode'=>'运输方式',
            'sales_id'=>'销售'
        ];
        if($event === 'clue_insert'){
             $validate_field = [
                'company_name'=>'客户全称',
                // 'company_address'=>'客户地址',
                'country_code'=>'国家代码',
                // 'export_sailing'=>'出口航线',
                // 'trans_mode'=>'运输方式',
                'sales_id'=>'销售'
            ];
        }
        foreach ($validate_field as $key => $val)
        {
            if (!isset($data[$key]) || $data[$key] == ''){
                return "{$val}必须填写！";
            }
        }
        //校验角色类型
        if ($check_company_role = check_company_role($data['company_name'], $data['role'])) {
            return "{$check_company_role}！";
        }

        //查询该公司是否3个月内有过交易，如果有则不允许添加或编辑
        $company_search = match_chinese($data['company_name']);
        $this->db->select('biz_client.client_code,biz_client.company_search,biz_client.client_level,biz_shipment.booking_ETD');
        $this->db->where('biz_client.company_search', $company_search);
        $this->db->join('biz_shipment', 'biz_shipment.client_code = biz_client.client_code and status="normal"', 'LEFT');
        $this->db->order_by('biz_shipment.booking_ETD', 'desc')->limit(1);
        $client = $this->db->get('biz_client')->row_array();

        //校验CRM黑名单
        $crm = $this->db->where('company_search', $company_search)->where('apply_status', -99)->get('biz_client_crm')->row_array();
        if ($crm) return "“{$data['company_name']}”已被加入CRM黑名单中，不能使用！";

        if ($client){
            //校验往来单位黑名单
            if ($client['client_level'] == -1){
                return "“{$data['company_name']}”已被加入往来单位黑名单中，不能使用！";
            }
            //如果3个月内有过交易
            if (strtotime('+3 month', strtotime($client['booking_ETD'])) > time()){
                return "“{$data['company_name']}”在3个月内有过交易记录！";
            }
        }
        return true;
    }

    /***
     * @title 写入操作日志
     * @param int $id
     * @param string $event
     * @param array $data
     */
    private function write_log($id=0, $event='insert', $data=[]){
        $log_data = array();
        $log_data["table_name"] = "biz_client_crm"; //表名
        $log_data["key"] = $id; //主键
        $log_data["action"] = $event; //操作
        $log_data["value"] = json_encode($data); //值
        log_rcd($log_data);
    }

    /**
     * 线索名称去除特殊符号
     */
    public function clue_remove_special_symbols(){
        foreach($this->db->where('clue_pure_text', '')->get('biz_client_crm_clue')->result_array() as $row){
            $this->db->where('id', $row['id'])->update('biz_client_crm_clue', array('clue_pure_text'=>match_chinese($row['clue'])));
        }
    }

    /**
     * CRM英文名称去除特殊符号
     */
    public function crm_remove_special_symbols(){
        foreach($this->db->where("company_search_en = '' and company_name_en <> ''")->get('biz_client_crm')->result_array() as $row){
            $this->db->where('id', $row['id'])->update('biz_client_crm', array('company_search_en'=>match_chinese($row['company_name_en'])));
        }
    }

    /**
     * 线索分析
     */
    public function ClueAnalysis()
    {
        //载入线索分析类
        $this->load->library('ClueAnalysis', array('clue_id' => 67990));
        //更新线索名称
        $this->clueanalysis->update_clue_name();
        //执行分析
        $this->clueanalysis->start_analysis();
    }

    /**
     * 获取report的 线索分析
     */
    public function get_report_clue(){
        $company_search_en = postValue('company_search_en', '');

        //开始在report库里获取
        $shipments = Model('report_model')->get_shipment_info($company_search_en);

        $field_config = array(
            'customer' => 'company_name_en',
            'shipper' => 'shipper_company',
            'consignee' => 'consignee_company',
            'notify' => 'notify_company',
            'pol' => 'trans_origin',
            'pod' => 'trans_destination_inner',
            'etd' => 'report_date',
            'vessel' => 'vessel',
            'product_details' => 'description',
            'country_code' => 'country_code',
//            'remark'
        );
        $data = array();
        foreach ($shipments as $shipment){
            //开始填充数据
            $row = array();
            $row['remark'] = '';

            foreach ($field_config as $field => $value_field){
                if(empty($shipment[$value_field])) $shipment[$value_field] = '';
                $row[$field] = $shipment[$value_field];
            }
            //需要填充4次  client_company1次
            $row['clue'] = $row['customer'];
            $data[] = $row;

            //s
            $row['clue'] = $row['shipper'];
            $data[] = $row;

            //c
            $row['clue'] = $row['consignee'];
            $data[] = $row;

            //n
            $row['clue'] = $row['notify'];
            $data[] = $row;
        }

        //开始调用import 接口, 生成对应的数据
        $result = array();
        $header = array(
        );
        $cookie = array();
        foreach ($_COOKIE as $key => $val){
            $cookie[] = "{$key}=" . urlencode($val);
        }
        $cookie = join(';', $cookie);
        session_write_close();

        foreach ($data as $row){
//            $_POST = $row;
//            $result[] = $this->import_data();
            $result[] = curl_post_body(base_url() . 'biz_crm/import_data', $header, http_build_query($row), $cookie);
        }

        return jsonEcho(array('code' => 0, 'msg' => '获取成功', 'data' => $result));
    }
}
