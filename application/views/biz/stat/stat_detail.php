<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link href="/inc/third/layui/css/layui.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
</head>
<body style=" overflow: auto;">
<div id="browse" style="width: 800px; height: 480px; margin-top: 10px;"></div>
<script src="/inc/third/layui/layui.js"></script>
<script>
    layui.extend({
        echarts: '/inc/third/layui/lay/modules/echarts',
        echartsTheme: '/inc/third/layui/lay/modules/echartsTheme',
    }).use(['echarts', 'form'], function () {
        var $ = layui.$
            ,form = layui.form
            ,echarts = layui.echarts;

        var echartsApp2 = [], options2 = [
            //访客浏览器分布
            {
                title : {
                    text: '<?=($data['title']);?>',
                    x: 'center',
                    textStyle: {
                        fontSize: 14
                    }
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient : 'vertical',
                    x : 'right',
                    y : 'center',
                    data:<?=json_encode($data['name']);?>
                },
                series : [{
                    name:'数量 / 占比',
                    type:'pie',
                    radius: [50, 150],
                    center: ['45%', '50%'],
                    roseType: 'area',
                    itemStyle: {
                        borderRadius: 8
                    },
                    data:<?=json_encode($data['data']);?>
                }]
            }
        ]
            ,elemDataView2 = $('#browse')
            , renderDataView2 = function (index) {
            echartsApp2[index] = echarts.init(elemDataView2[index], layui.echartsTheme);
            echartsApp2[index].setOption(options2[index]);
            window.onresize = echartsApp2[index].resize;
        };
        //没找到DOM，终止执行
        if(!elemDataView2[0]) return;
        renderDataView2(0);
    });
</script>
</body>
</html>