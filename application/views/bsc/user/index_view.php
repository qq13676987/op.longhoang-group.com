<style type="text/css">
    .user_group tr>td:first-child{
        width: 80px;
    }
    .user_group tr>td:nth-child(2){
        width: 350px;
    }
    .user_group tr>td:nth-child(3){
        width: 80px;
    }
    #image img{
        width: 100px;
        height: 100px;
        margin-left: 30px;
        margin-top: 10px;
        border: 1px solid black;
    }
    .show_if{
        display: none;
    }

    .ladder_btn{
        width: 23px;
        height: 23px;
    }
    .table_left{
        width: 442px;
        height: 100%;
    }
    .table_right{
        width: 1000px;
        height: 100%;
    }
    .table_body{
        height: 100vh;
        display: flex;
        flex-direction: row;
    }
    .user_role_iframe{
        width:1200px;
        height: 100%;
    }
    /*    ::-webkit-scrollbar { width: 0 !important }
        html{overflow: -moz-scrollbars-none;}*/
</style>
<div id="cc" class="easyui-layout" style="width:100%;height:800px;">
    <div  data-options="region:'west',title:'<?= lang('user group');?>',split:true,tools:'#add_btn'" style="width:230px;">
        <!--<select class="easyui-combobox" id="search_group" style="width: 90%;" prompt="user group" data-options="
            valueField:'code',
            textField:'name',
            onSelect: function(rec){
                var node = $('#tt').tree('find', rec.code);
                $('#tt').tree('collapseAll');
                $('#tt').tree('expandTo', node.target);
            }
        ">
        </select>
        <br/>-->
        <input class="easyui-textbox" id="search_user" style="width: 95%;" prompt="user name">
        <div class="easyui-panel" style="padding:5px">
            <ul id="tt"></ul>
        </div>
        <div id="add_btn">
        </div>
    </div>
    <div class="user_group" data-options="region:'center',title:'<?= lang('user info');?>',tools:'#edit_btn'" style="padding:5px;">
        <form id="user_form" method="post" > 
            <div class="table_body">
                <div class="table_left">
                    <table width="100%" border="0" cellspacing="0">
                        <tr>
                            <td>状态</td>
                            <td>
<!--                                <input type="text" class="easyui-textbox" style="width:120px;" name="title" id="title" readonly>-->
                                <input type="text" class="easyui-textbox" style="width:70px;" name="id" id="id" readonly>
                                <select name="status" id="status" class="easyui-combobox" style="width: 80px">
                                    <option value="0"><?= lang('Normal');?></option>
                                    <option value="1"><?= lang('Disable');?></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>姓名</td>
                            <td> 
                                <input type="text" class="easyui-textbox" style="width:155px;" name="name" id="name" required>
                            <a href='http://wenda.leagueshipping.com/?/question/859' target="_blank" title="点击看级别说明">级别</a>：<input name="m_level" id="m_level" class="easyui-numberspinner" value="5" data-options="min:3,max:15,required:true,prefix:'M'" style="width:80px;"></input>
                            </td>
                        </tr>
                        <tr>
                            <td>职位</td>
                            <td><input type="text" class="easyui-textbox" style="width:300px;" name="job_title" id="job_title" ></td>
                        </tr>
                        <tr>
                            <td>密码</td>
                            <td><input type="text" class="easyui-textbox" style="width:300px;" name="password" id="password" required></td>
                        </tr>
                        <tr>
                            <td>公司</td>
                            <td>
                                <select class="easyui-combobox" style="width:300px;" name="company" id="company" editable="true" data-options="
                            required:true,
                            valueField:'value',
                            textField:'value',
                            url:'/bsc_dict/get_option/hbl_type',
                        ">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>手机</td>
                            <td><input type="text" class="easyui-textbox" required style="width:40px;" name="telephone_area_code" id="telephone_area_code" value="" > - <input type="text" class="easyui-textbox" required style="width:250px;" name="telephone" id="telephone" ></td>
                        </tr>
                        <tr>
                            <td>邮箱</td>
                            <td><input type="text" class="easyui-textbox" style="width:300px;" name="email" id="email"></td>
                        </tr>
                        <tr>
                            <td>qq</td>
                            <td><input type="text" class="easyui-textbox" style="width:300px;" name="qq" id="qq" ></td>
                        </tr>
                        <tr>
                            <td>skype</td>
                            <td><input type="text" class="easyui-textbox" style="width:300px;" name="skype" id="skype" ></td>
                        </tr>
                        <tr>
                            <td><?= lang('Group');?></td>
                            <td>
                                <select id="group" name="group" class="easyui-combobox" style="width:300px;" editable="true" data-options="
                            valueField:'code',
                            textField:'name',
                            url:'/bsc_group/get_option',
                            onLoadSuccess: function(){
                                var data = $(this).combobox('getData');
                                $('#search_group').combobox('loadData', data);
                            },
                            required:true,">
                                </select>
                            </td>
                        </tr>
                        <!--<tr>
                            <td><?= lang('user_role');?></td>
                            <td>
                                <select class="easyui-combobox" style="width:300px;" name="user_role" id="user_role" editable="false" data-options="
                                    multiple:true,
                                    valueField:'value',
                                    textField:'name',
                                    url:'/bsc_dict/get_option/user_role',
                                    required:true,
                                ">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('岗位权限');?></td>
                            <td>
                                <select class="easyui-combotree" style="width:300px;" name="station" id="station" editable="false" data-options="
                                    multiple:true,
                                    url:'/bsc_user_role/get_tree',
                                    onChange:function(newValue, oldValue){
                                        station_select(newValue);
                                    },
                                    required:true,
                                    cascadeCheck: false,
                                ">
                                </select>
                            </td>
                        </tr>-->
                        <tr>
                            <td><?= lang('Group range');?></td>
                            <td>
                                <select class="easyui-combotree default_partner"  style="width:300px;" name="group_range" id="group_range" ids="group_range" editable="true" data-options="
                                        multiple:true,
                                        valueField:'id',
                                        textField:'text',
                                        url:'/bsc_user/get_user_tree1',
                                        value: '',
                                    ">
                                </select>
                            </td>
                        </tr>
                        <tr style="display:none">
                            <td><?= lang('User range');?></td>
                            <td>
                                <select id="user_range" class="easyui-combogrid" name="user_range" style="width:300px;" >
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>直属领导</td>
                            <td>  
                                <select id="leader_id" name="leader_id" class="easyui-combobox" style="width:200px;" editable="true" data-options="
                                    valueField:'id',
                                    textField:'name',
                                    url:'/bsc_user/get_option', 
                                    required:true,">
                                </select>
                                助理
                                <select name="assistant_flag" id="assistant_flag" class="easyui-combobox" style="width: 50px">
                                    <option value="0">否</option>
                                    <option value="1">是</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>主账号2</td>
                            <td>  
                                <select id="pan_id" name="pan_id" class="easyui-combobox" style="width:300px;" editable="true" data-options="
                                    valueField:'id',
                                    textField:'name',
                                    url:'/bsc_user/get_option', 
                                    required:true,">
                                </select>
                            </td>
                        </tr>
                    </table>
                    <br>
                     <a href="javascript:void(0);" class="easyui-linkbutton" id = "save" onclick="save()" style="width:90px">保存个人资料</a>   
                     <a href="javascript:void(0);" class="easyui-linkbutton" id = "set_leader" onclick="set_leader()" style="width:90px">设为部门领导</a>
                    <br>
                    <br>
                    <br>
                    提示：<br>
                    1、岗位角色用于判断该用户是否出现shipment界面里岗位选项里。<br>
                    2、岗位权限用于控制该用户的访问权限。<br><br>
                    3、设为部门领导，表示这个部门所有的人的上级领导都设为他， 他自己除外！ 。<br><br>
                    4、一旦设为助理，这个用户就会复制其直属领导的数据范围权限！
                </div>
            </div>



            <div id="edit_btn">
            </div>
            <div id="image" class="show_if">
            </div>

            <div id="dlg">
                <img id="simg">
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    var read_key = '1read';
    var edit_key = '1edit';
    var url = '/bsc_user/add_data';

    $.extend($.fn.validatebox.defaults.rules, {
        Num: { //验证手机号
            validator: function(value, param){
                return /^\d*$/.test(value);
            },
            message: '请输入正确的手机号码。'
        },
        phoneNum: { //验证手机号
            validator: function(value, param){
                return /^1[3-9]+\d{9}$/.test(value);
            },
            message: '请输入正确的手机号码。'
        },
        telNum:{ //既验证手机号，又验证座机号
            validator: function(value, param){
                return /(^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$)|(^(()|(\d{3}\-))?(1[358]\d{9})$)/.test(value);
            },
            message: '请输入正确的电话号码。'
        }
    });

    function station_select(values){
        //这里根据当前值,改变iframe的url
        // $('#user_role_iframe').attr('src', '/bsc_user_role/index?user_role=' + values.join(','));
    }
    
    function set_leader(){
        var group = $('#group').combobox('getText');
        var group_code = $('#group').combobox('getValue'); 
        var user_id = $('#leader_id').combobox('getValue');
        if(user_id==""){
            alert("未选择领导");
            return;
        }
        var msg = "把 【"+group+"】的所有人的上级领导设置为【"+user_id+"】吗？\n\n请确认！"; 
        if (confirm(msg)==true){  
            $.ajax({
                type: 'GET',
                url: '/bsc_user/update_leader/' + user_id+'/'+group_code,
                dataType: 'json',
                success: function(res){
                    alert(res.msg);  
                }
            });
        }
    }
    
    function save(){
        if($('#user_form').form('validate')){
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    // title:$('#title').textbox('getValue'),
                    status:$('#status').combobox('getValue'),
                    assistant_flag:$('#assistant_flag').combobox('getValue'),
                    leader_id:$('#leader_id').combobox('getValue'),
                    pan_id:$('#pan_id').combobox('getValue'),
                    name:$('#name').textbox('getValue'),
                    m_level:$('#m_level').numberspinner('getValue'),
                    password:$('#password').textbox('getValue'),
                    // menu:$('#menu').combobox('getValues').join(','),
                    group:$('#group').combobox('getValues').join(','),
                    group_range:$('#group_range').combobox('getValues').join(','),
                    user_range:$('#user_range').combogrid('getValues').join(','),
                    telephone:$('#telephone').textbox('getValue'),
                    email:$('#email').textbox('getValue'),
                    // user_role:$('#user_role').combobox('getValues').join(','),
                    company:$('#company').combobox('getValue'),
                    skype:$('#skype').textbox('getValue'),
                    job_title:$('#job_title').textbox('getValue'),
                    qq:$('#qq').textbox('getValue'),
                    telephone_area_code:$('#telephone_area_code').textbox('getValue'),
                    // station:$('#station').combobox('getValues').join(','),
                    // sales_quota: $('#sales_quota').numberbox('getValue'),
                    // points: $('#points').numberbox('getValue'),
                },
                dataType: 'json',
                success: function(res){
                    // $('#tt').tree('reload');
                    alert(res.msg);
                    if(res.code == 0){
                    }else{
                    }
                    //2022-07-05 重新加载树
                    $('#tt').tree('reload');

                }
            });
        }

    }
    $(function () {
        //2022-07-06
        var nodeId;
        $('#tt').tree({
            url:'/bsc_user/getAllCompanyTree?children_closed=true&getUser=true',
            method: 'get',
            animate: true,
            onClick: function(node){
                nodeId = node.id;
                var id_arr = node.id.split('_'); 
                url = '/bsc_user/add_data'; 
                if(id_arr[0] != 'company'){
                    //选中部门的时候 
                    if(id_arr[0].length != 5){
                        $('#set_leader').show();
                    }else{ 
                     //选中个人user的时候 
                        $('#set_leader').hide();
                        $('#save').show();
                    }
                    //刷新输入框的内容
                    $.ajax({
                        type: 'GET',
                        url: '/bsc_user/get_user_one/' + node.id,
                        dataType: 'json',
                        success: function(res){
                            if(res.length == 0){
                                add_click();
                                url = '/bsc_user/add_data';
                                $('#group').combobox('setValue', node.id);
                                // $('#group_range').combobox('setValue', node.id);
                                // $('#user_role_iframe').attr('src', '');
                            }else{
                                url = '/bsc_user/update_data?id=' + res.id; 
                                //选中时直接给iframe改连接 
                                station_select(res.station.split(','));
                                $('#user_form').form('load',res);
                            } 
                        }
                    });
                }else{ 
                    $('#save').hide();
                    $('#set_leader').hide();
                }
                console.log(url);
            },
            formatter:function(node){
                if(node.status == 1){
                    return '<span style="color:red;">' + node.text + '</span>';
                }
                return node.text;
            },
            onLoadSuccess(data){
            if(nodeId!=null){
                var node = $('#tt').tree('find', nodeId);
                $('#tt').tree('expandTo', node.target).tree('select', node.target);
            }
        }
        });
        $('#image').on('click', 'img', function (){
            $('#dlg').dialog({
                title: '预览',
                width: 800,
                height:500,
                resizable:true,
                closed: false,
                cache: false,
                modal: true,
                style: {'text-align': 'center'}
            });
            $("#simg").attr("src",$(this).attr('src'));

        });

        $('#search_user').textbox('textbox').blur(function () {
            var val = $('#search_user').textbox('getValue');
            //2022-08-15 搜索时自动展开树
            if(val == '') $("#tt").tree('collapseAll');
            else $("#tt").tree('expandAll');
            $("#tt").tree("doFilter", val);
        });

        $('#search_user').textbox('textbox').keydown(function (event) {
            if(event.keyCode == 13){
                $('#search_user').textbox('textbox').trigger('blur');
            }
        });

        //实现表格下拉框的分页和筛选--start
        var html = '<div id="reportComboGridTb">';
        html += '<form id="reportComboGridTbform">';
        html += '<div style="padding-left: 5px;display: inline-block;"><label>text:</label><input id="to_search_text" class="easyui-textbox to_search"style="width:96px;"data-options="prompt:\'text\'"/></div>';
        html += '<div style="padding-left: 5px;display: inline-block;"><label>group:</label><input id="to_search_group" class="easyui-combobox to_search"style="width:150px;"data-options="prompt:\'group\',valueField:\'value\',textField:\'value\',url:\'/bsc_dict/get_option/group\',"/></div>';
        html += '<div style="padding-left: 1px; display: inline-block;"><form>';
        html += '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="queryReport"><?= lang('search');?></a>';
        html += '</div>';
        html += '</div>';
        $('body').append(html);
        $.parser.parse('#reportComboGridTb');
        
        $('#user_range').combogrid("reset");
        $('#user_range').combogrid({
            panelWidth: '500px',
            width: 300,
            multiple:true,
            idField: 'id',              //ID字段
            textField: 'name',    //显示的字段
            fitColumns : true,
            striped: true,
            editable: false,
            pagination: true,           //是否分页
            pageList : [ 50,100,500 ],
            pageSize : 500,
            toolbar : '#reportComboGridTb',
            rownumbers: true,           //序号
            collapsible: true,         //是否可折叠的
            method: 'post',
            columns:[[
                {field:'name',title:'name',width:120},
                {field:'group',title:'group',width:120},
            ]],
            emptyMsg : '未找到相应数据!',
            onLoadSuccess : function(data) {
                var opts = $(this).combogrid('grid').datagrid('options');
                var vc = $(this).combogrid('grid').datagrid('getPanel').children('div.datagrid-view');
                vc.children('div.datagrid-empty').remove();
                if (!$(this).combogrid('grid').datagrid('getRows').length) {
                    var d = $('<div class="datagrid-empty"></div>').html(opts.emptyMsg || 'no records').appendTo(vc);
                    d.css({
                        position : 'absolute',
                        left : 0,
                        top : 50,
                        width : '100%',
                        fontSize : '14px',
                        textAlign : 'center'
                    });
                }
            },
            url:'/bsc_user/get_limit_data',
        });
        //点击搜索
        $('#queryReport').click(function(){
            var where = {};
            where['text'] = $('#to_search_text').textbox('getValue');
            where['group'] = $('#to_search_group').combobox('getValue');
            // where['field'] = $('#to_search_field').combo('getValue');
            $('#user_range').combogrid('grid').datagrid('load',{
                where:where,
            });
        });
        //text添加输入值改变
        $('.to_search').textbox('textbox').keydown(function (e) {
            if(e.keyCode == 13){
                $('#queryReport').trigger('click');
            }
        });
        //实现表格下拉框的分页和筛选-----end
    });

    //新增一个角色出来
    function add_ladder(e) {
        var this_tr = $(e).parent('td').parent('tr');
        var last_end_val = this_tr.find('.end');
        if(this_tr.parent('tbody').find('tr').length > 1){
            last_end_val = this_tr.parent('tbody').children('tr').last().find('.end');
        }

        var add_tr = $("<tr/>");
        var start_val = last_end_val.numberbox('getValue');
        if(start_val == '9999999999.99'){
            return;
        }
        if(start_val == ''){
            $.messager.alert('Tips', '最后一条结束金额未填写');
            return;
        }

        var tr_content =   "                                    <td>\n" +
            "                                        " +
            "                                    </td>\n" +
            "                                    <td>\n" +
            "                                        <input class=\"easyui-numberbox start\" value=\"" + start_val + "\" name='start' readonly prompt=\"开始金额\" data-options=\"min:0,precision:2\"> -\n" +
            "                                        <input class=\"easyui-numberbox end\" value=\"\" prompt=\"结束金额\" data-options=\"min:" + start_val + ",precision:2\">\n" +
            "                                    </td>\n" +
            "                                    <td>\n" +
            "                                        <input class=\"easyui-numberbox lr\" value=\"\" prompt=\"提成比例（%）\" data-options=\"min:0,precision:2,max:100,suffix:'%'\">\n" +
            "                                    </td>\n" +
            "                                    <td>\n" +
            "                                        <input type=\"hidden\" class=\"role\" value=\"" + this_tr.find('.role').val() + "\">\n" +
            "                                        <input type=\"hidden\" class=\"id\">\n" +
            "                                        <button type=\"button\" class='ladder_btn' onclick=\"del_ladder(this)\">-</button>\n" +
            "                                        <button type=\"button\" onclick=\"save_commision(this)\">保存</button>\n" +
            "                                    </td>"
        add_tr.append(tr_content);
        this_tr.parent('tbody').append(add_tr);
        $.parser.parse(add_tr);
    }
    function del_ladder(e) {
        var this_tr = $(e).parent('td').parent('tr');
        var id = this_tr.find('.id').val();

        if(id != 0){
            if(this_tr.index() != (this_tr.parent('tbody').children('tr').length - 1)){
                $.messager.alert('Tips', '只能先删除最后一个');
                return;
            }
            $.messager.confirm('确认对话框', '是否确认删除该提成设置？', function (r) {
                if(r){
                    this_tr.remove();
                    $.ajax({
                        type:'GET',
                        url:'/bsc_user/del_commision/' + id,
                        dataType:'json',
                        success:function (res) {
                            $.messager.alert('Tips', res.msg);
                        },
                        error:function () {
                            $.messager.alert('Tips', '发生错误，请联系管理员');
                        }
                    })
                }
            });
        }else{
            this_tr.remove();
        }
        //同时进行删除操作
    }

    var is_submit = false;
    function save_commision(e) {
        var this_tr = $(e).parent('td').parent('tr');
        var start_amount = this_tr.find('.start').numberbox('getValue');
        var end_amount = this_tr.find('.end').numberbox('getValue');
        var commision_rate = this_tr.find('.lr').numberbox('getValue');
        var user_id = $('#id').val();
        var user_role = this_tr.find('.role').val();
        var id = this_tr.find('.id').val();
        if(is_submit){
            return;
        }
        if(start_amount == end_amount){
            $.messager.alert('Tips', '开始金额不能和结束金额相同');
            return;
        }
        if(end_amount == ''){
            $.messager.alert('Tips', '结束金额不能为空');
            return;
        }
        if(commision_rate == ''){
            $.messager.alert('Tips', '提成比例不能为空');
            return;
        }
        var url = '/bsc_user/add_commision';
        if(id != 0 && id != ''){
            url = '/bsc_user/update_commision/' + id;
        }
        is_submit = true;
        ajaxLoading();
        $.ajax({
            type:'POST',
            url:url,
            data:{
                user_id:user_id,
                amount:end_amount,
                commision_rate:commision_rate,
                user_role: user_role,
            },
            dataType:'json',
            success:function (res) {
                ajaxLoadEnd();
                is_submit = false;
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg);
                }
            },
            error:function () {
                ajaxLoadEnd();
                is_submit = false;
                $.messager.alert('Tips', '发生错误，请联系管理员');
            }
        });
    }

    function add_click() {
        $('#id').textbox('clear');
        $('#title').textbox('clear');
        $('#name').textbox('clear');
        $('#password').textbox('clear');
        // $('#station').combobox('clear');
        $('#telephone').textbox('clear');
        $('#email').textbox('clear');
        // $('#user_role').combobox('clear');
        $('#company').combobox('clear');
        $('#m_level').numberspinner('clear');
        $('#group_range').combobox('clear');
        $('#user_range').combobox('clear');
        $('#leader_id').combobox('clear');
    }
    //field 字段名 如menu,  value 变化的值， ischeck 选中或者取消
    function add_image(field, value,ischeck=true) {
        $('.show_if').css('display', 'none');
        if(field != '' && value != ''){
            var check = '';
            if(ischeck){
                check = 'enable';
            }else{
                check = 'disable';
            }
            var str = '<img src="/inc/image/' + field + '/' + value + '_' + check + '.png">';
            $('#image').append(str);
            $('#image').css('display', 'block');
            return true;
        }else{
            return false;
        }

    }
</script>
