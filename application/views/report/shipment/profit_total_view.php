<script type="text/javascript" src="/inc/js/easyui/datagrid-export.js"></script>
<style type="text/css">
    .hide{
        display: none;
    }
    .f{
        width:120px;
    }
    .s{
        width:100px;
    }
    .v{
        width:120px;
    }
    .del_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }
    .breadcrumb > li + li:before {
        color: #CCCCCC;
        content: "/ ";
        padding: 0 5px;
    }
    .breadcrumb > li{
        display: inline-block;
    }
     .add{
        background: url(/inc/js/easyui/themes/icons/edit_add.png) no-repeat center center;
    }
    .remove{
        background: url(/inc/js/easyui/themes/icons/edit_remove.png) no-repeat center center;
    }
</style>
<script type="text/javascript">
    function loaddata(){
        json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        
        $('#tt').datagrid('options').url = '/report/get_profit_total_data';
        $('#tt').datagrid('load', json).datagrid('clearSelections');
        
        var title = $('.groups_field:checked').attr('title');
        $("#now_crumb").text(title);
    }
    var json = {};
    function doSearch(jump = false) {
        var this_val = $('.groups_field:checked').val();
        var serarchs = $('.searchs').length;
        if(this_val == undefined){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('必须选择一个维度');?>');
            return;
        }
        var select_row = $('#tt').datagrid('getSelected');
        if(select_row == null && serarchs > 0){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择一行');?>');
            return;
        }
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 1){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('不能选择多行');?>');
            return;
        }
        if(select_row !== null){
            var s_field_val = "<?= $groups_field;?>";
            if(s_field_val == "") s_field_val = json.groups_field;
            var str = '<input class="searchs" type="hidden" name="searchs[' + (serarchs / 2) + '][]" value="' + s_field_val +'">' +
            '<input class="searchs" type="hidden" name="searchs[' + (serarchs / 2) + '][]" value="' + select_row.name_id + '">';
            
            $('#cx').append(str);
            
            $('#chaxun').window('close');
        
            //带参数跳转到当前页面
            $('#cx_submit').trigger('click');
        }else{
            $('#chaxun').window('close');
            set_config();
            //带参数跳转到当前页面
            $('#cx_submit').trigger('click');
            // loaddata();
        }
    }

    function ajax_get(url,name) {
        return $.ajax({
            type:'POST',
            url:url,
            dataType:'json',
            success:function (res) {
                ajax_data[name] = res;
            },
            error:function () {
                // $.messager.alert('获取数据错误');
            }
        });
    }
    
     /**
     * 返回几页, 和
     */
    function back(count) {
        $('#back').val(count);
        $('#cx_submit').trigger('click');
    }
    
    function get_statistics(){
        var select = $('#tt').datagrid('getSelections');
        var row = {};
        var data = $('#tt').datagrid('getData').rows;
        if(data.length >= 1){
            $.each(data[0], function(index, item){
                if(typeof item == 'number'){
                    row[index] = 0;
                }else{
                    row[index] = '';
                }
            });
        }
        $.each(select, function (index, item) {
            $.each(item, function(i, val){
                if(typeof val == 'number'){
                    var this_val = parseInt(val * 10000);
                    row[i] += this_val; 
                }
            });
        });
        $.each(row, function(index, item){
            if(typeof item == 'number'){
                row[index] = item / 10000;
            }
        });
        
        //ex_AIR_KGS ex_LCL_CBM im_AIR_KGS im_LCL_CBM
        if(row['ex_AIR_good_outers'] != undefined)row['ex_AIR_KGS'] = row['ex_AIR_good_outers'] + '/' + row['ex_AIR_good_weight'] + '/' + row['ex_AIR_good_volume'];
        if(row['ex_LCL_good_outers'] != undefined)row['ex_LCL_CBM'] = row['ex_LCL_good_outers'] + '/' + row['ex_LCL_good_weight'] + '/' + row['ex_LCL_good_volume'];
        if(row['im_AIR_good_outers'] != undefined)row['im_AIR_KGS'] = row['im_AIR_good_outers'] + '/' + row['im_AIR_good_weight'] + '/' + row['im_AIR_good_volume'];
        if(row['im_LCL_good_outers'] != undefined)row['im_LCL_CBM'] = row['im_LCL_good_outers'] + '/' + row['im_LCL_good_weight'] + '/' + row['im_LCL_good_volume'];
        
        $('#statistics_form').form('load', row);
    }
  
    $.fn.datagrid.defaults.view.renderFooter =  function(target, container, frozen){
        var opts = $.data(target, 'datagrid').options;
        var rows = $.data(target, 'datagrid').footer || [];
        var fields = $(target).datagrid('getColumnFields', frozen);
        var table = ['<table class="datagrid-ftable" cellspacing="0" cellpadding="0" border="0"><tbody>'];
         
        for(var i=0; i<rows.length; i++){
            var styleValue = opts.rowStyler ? opts.rowStyler.call(target, i, rows[i]) : '';
            var style = styleValue ? 'style="' + styleValue + '"' : '';
            table.push('<tr class="datagrid-row" datagrid-row-index="' + i + '"' + style + '>');
            table.push(this.renderRow.call(this, target, fields, frozen, i, rows[i]));
            table.push('</tr>');
        }
         
        table.push('</tbody></table>');
        $(container).html(table.join(''));
    };

    /**
     * 该方法直接把旧版本的提前ajax给重新合并了下,但是会出现一个问题,就是刚开始打开页面时,由于js是同时执行的,所以会导致有些格子数据加载几遍,这是个弊端,但是如果那里同步执行的话,用户体验可能会下降,这里之后需要思考如果提升
     */
    function getAjaxData(name, url, target) {
        if(ajax_data[name] != null){//判断是否加载过数据,加载过这里直接返回
            return ajax_data[name];
        }else{//没加载过调用ajax获取后,进行返回
            var t = target;
            ajaxLoading();
            $.ajax({
                type:'POST',
                url:url,
                dataType:'json',
                success:function (res) {
                    ajax_data[name] = res;
                    t.combobox('loadData', res);
                    ajaxLoadEnd();
                },
                error:function () {
                    // $.messager.alert('获取数据错误');
                    ajaxLoadEnd();
                }
            });

            return [];//这里return空的,到时候上面调用load方法手动刷新
        }
    }

    /**
     * 查询 选中字段后改变输入框的事件
     */
    function select_f_change(newVal, oldVal) {
        var index = $('.f').index(this);
        var v_jq = $('.v:eq(' + index + ')');
        if(setting.hasOwnProperty(newVal) === true){
            v_jq.textbox('destroy');
            $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
            $.parser.parse(v_jq.parents('.this_v'));
            var valueField = setting[newVal][1];
            var textField = setting[newVal][2];
            var v_config = {
                data:getAjaxData(setting[newVal][0], setting[newVal][4], $('.v:eq(' + index + ')')),
                valueField:valueField,
                textField:textField,
            };
            if(setting[newVal][3] === 'remote'){
                v_config.data = [{}];
                v_config.data[0][valueField] = '';
                v_config.data[0][textField] = '-';
                v_config.mode = setting[newVal][3];
                v_config.url = setting[newVal][4];
                v_config.onBeforeLoad = function(param){
                    if($(this).combobox('getValue') == '')return false;
                    if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                };
            }
            $('.v:eq(' + index + ')').combobox(v_config);
        }else if ($.inArray(newVal, datebox) !== -1){
            v_jq.textbox('destroy');

            $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datebox\' >');
            $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
        }else if ($.inArray(newVal, datetimebox) !== -1){
            v_jq.textbox('destroy');
            $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datetimebox\' >');
            $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
        }else{
            v_jq.textbox('destroy');
            $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
            $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
        }
    }

    var ajax_data = {};

    var setting = {
        'trans_mode':['trans_mode','name','name', 'local', '/bsc_dict/get_option/trans_mode'],
        'created_by':['trans_mode','id','otherName', 'local', '/bsc_user/get_option'],
        'updated_by':['trans_mode','id','otherName', 'local', '/bsc_user/get_option'],
        'biz_type':['biz_type','name','name', 'local', '/bsc_dict/get_option/biz_type'],
        'status':['status','value', 'name', 'local', '/bsc_dict/get_option/bill_status'],
        'release_type':['release_type','value','value', 'local', '/bsc_dict/get_option/release_type'],
        'trans_origin':['port','port_code','port_name', 'local', '/biz_port/get_option'],
        'trans_destination':['port','port_code','port_name', 'local', '/biz_port/get_option'],
        'trans_carrier':['trans_carrier','client_code','client_name', 'remote', '/biz_client/get_option/carrier?q_field=client_name'],
        'trans_term':['trans_term','value','value', 'local', '/bsc_dict/get_option/trans_term'],
        'INCO_term':['INCO_term','value','value', 'local', '/bsc_dict/get_option/INCO_term'],
        'service_options' : ['service_options', 'value', 'lang', 'local', '/bsc_dict/get_option/service_option'],
        'trans_tool' : ['trans_tool', 'value', 'value', 'local', '/bsc_dict/get_option/trans_tool'],
        'hbl_type' : ['hbl_type', 'value', 'name', 'local', '/bsc_dict/get_hbl_type'],
        'sailing_area':['sailing','sailing_area','sailing', 'local', '/biz_port/get_sailing'],
        <?php foreach ($admin_field as $val){?>
        '<?= $val;?>':['<?= $val?>', 'id', 'otherName', 'local', '/bsc_user/get_data_role/<?= $val;?>'],
        '<?= $val . '_group';?>':['group', 'code', 'name', 'local', '/bsc_user/get_data_role/<?= $val;?>'],
        <?php } ?>
    };
    ///bsc_dict/get_option/service_option
    var datebox = ['booking_ETD', 'truck_date', 'trans_ETD', 'trans_ATD', 'trans_ETA', 'trans_ATA','created_time', 'updated_time', 'dadanwancheng', 'warehouse_in_date', 'report_date',];

    var datetimebox = [];
    
    function init() {
        $('#tt').datagrid({});
        //延迟加载,否则页面请求两次
        setTimeout(loaddata, 100);
    }

    $(function () {
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height() - 67,
            onSelect:function(index ,row){
                get_statistics();
            },
            onUnselect:function(index ,row){
                get_statistics();
            },
            onSelectAll:function(index ,row){
                get_statistics();
            },
            onUnselectAll:function(index ,row){
                get_statistics();
            },
            onLoadSuccess:function(data){
                var ext_data = data.ext_data;
                $(this).datagrid('changeColumnSort', {
                    sortOrder: ext_data.order,
                    sortName: ext_data.sort,
                });
            },
            rowStyler:function(index,row){
                if(row.is_footer){
                    return "font-weight:900;background-color:#F5F5F5;";
                }
            },
        });
        var serarchs = $('.searchs').length / 2;
        var groups_field = $('.groups_field:checked').val();
        if(serarchs <= 0 && groups_field == undefined){
            $('#chaxun').window('open');
        }else{
            loaddata();
        }
        var str = '<li><a href="javascript:window.location.href= \'/report/index\';"><?= lang("report首页");?></a></li>';
        str += '<li><a href="javascript:window.location.href= \'/report/profit_total\'"><?= lang("重新查询");?></a></li>';
        for(var i = 0; i < serarchs; i++){
            var field = $('.searchs:eq(' + (parseInt(i) * 2) +')');
            var value = $('.searchs:eq(' + ((parseInt(i) * 2) + 1) +')');
            var this_field_val = field.val();
            var this_value_val = value.attr('tname');
            var tkey = field.attr('tkey');
            str += '<li><a href="javascript:void(0);" onclick="back(' + (serarchs - i) + ')">' + tkey + ':' + this_value_val + '</a></li>';
        }
        $("#now_crumb").parent('li').before(str);

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
        // ---------------------------------
        $('#cx_table').on('click' , '.add_tr', function () {
            var index = $('.f').length;
            var str = '<tr>\n' +
                '                <td>\n' +
                '                    <?= lang('查询');?>\n'+
                '                </td>\n'+
                '                <td align="right">\n'+
                '                    <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="\n'+
                '                        onChange:select_f_change\n'+
                '                    ">\n'+
                "<?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>" . lang($item) . "</option>";
                    }
                    foreach($admin_field as $val) {
                        echo "<option value='$val'>" . lang($val) . "</option>";
                    }
                ?>" +
                '                    </select>\n'+
                '                    &nbsp;\n'+
                '                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="\n'+
                '                    ">\n'+
                '                        <option value="like"><?= lang("like");?></option>\n'+
                '                        <option value="like前"><?= lang("like前");?></option>\n'+
                '                        <option value="="><?= lang("=");?></option>\n'+
                '                        <option value=">="><?= lang(">=");?></option>\n'+
                '                        <option value="<="><?= lang("<=");?></option>\n'+
                '                        <option value="!="><?= lang("!=");?></option>\n'+
                '                    </select>\n'+
                '                    &nbsp;\n'+
                '                    <div class="this_v">\n'+
                '                        <input name="field[v][]" class="v easyui-textbox">\n'+
                '                    </div>\n'+
                '                    <button class="del_tr" type="button">-</button>\n'+
                '                </td>\n'+
                '            </tr>';
            $('#cx_table tbody').append(str);
            $.parser.parse($('#cx_table tbody tr:last-child'));
            return false;
        });

        //删除按钮
        $('#cx_table').on('click' , '.del_tr', function () {
            var index = $(this).parents('tr').index();

            $(this).parents('tr').remove();
            return false;
        });

        $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
            var ec_value = $(ec_item).combobox('getValue');
            var v_inp = $('.v:eq(' + ec_index + ')');
            var v_val = v_inp.textbox('getValue');
            $(ec_item).combobox('clear').combobox('select', ec_value);
            $('.v:eq(' + ec_index + ')').textbox('setValue', v_val);
        });
        //初始判断count多少,自动调用该方法--end
    });

    function set_config() {
        var f_input = $('.f');
        var count = f_input.length;
        var config = [];
        $.each(f_input, function (index, item) {
            var c_array = [];
            c_array.push($('.f:eq(' + index + ')').combobox('getValue'));
            c_array.push($('.s:eq(' + index + ')').combobox('getValue'));
            config.push(c_array);
        });
        // var config_json = JSON.stringify(config);
        $.ajax({
            type: 'POST',
            url: '/sys_config/save_config_search',
            data:{
                table: 'profit_total',
                count: count,
                config: config,
            },
            dataType:'json',
            success:function (res) {

            },
        });
    }
    
    function export_excel(){
        $('#tt').datagrid('toExcel','<?= lang("货量统计");?>.xls');
    }

    function details(index) {
        //获取参数
        var form_data = {'field[v][]':[], 'field[f][]':[], 'field[s][]':[]};
        var cx_data = $('#cx').serializeArray();

        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(form_data.hasOwnProperty(item.name) === true){
                if(typeof form_data[item.name] == 'string'){
                    form_data[item.name] =  form_data[item.name].split(',');
                    form_data[item.name].push(item.value);
                }else{
                    form_data[item.name].push(item.value);
                }
            }else{
                form_data[item.name] = item.value;
            }
        });
        var get_parameter = {};


        $.each(form_data, function(i, it){
            if(typeof(it) == 'object' && i.indexOf("searchs[") != -1){
                if(it[0] == 'booking_ETD_month'){
                    var y = it[1].split('-')[0];
                    var m = parseInt(it[1].split('-')[1]);
                    //created_time
                    get_parameter['biz_shipment.booking_ETD->='] = it[1] + '-01';
                    get_parameter['biz_shipment.booking_ETD-<='] = it[1] + '-' + getMonthDay(y, m - 1);
                }else if(it[0] == 'created_time_month'){
                    //created_time
                    var y = it[1].split('-')[0];
                    var m = parseInt(it[1].split('-')[1]);
                    get_parameter['biz_shipment.created_time->='] = it[1] + '-01';
                    get_parameter['biz_shipment.created_time-<='] = it[1] + '-' + getMonthDay(y, m - 1);
                }else if(it[0] == 'trans_ATD_month'){
                    //created_time
                    var y = it[1].split('-')[0];
                    var m = parseInt(it[1].split('-')[1]);
                    get_parameter['biz_shipment.trans_ATD->='] = it[1] + '-01';
                    get_parameter['biz_shipment.trans_ATD-<='] = it[1] + '-' + getMonthDay(y, m - 1);
                }else if(it[0] == 'booking_ETD_day'){
                    var y = it[1].split('-')[0];
                    var m = parseInt(it[1].split('-')[1]);
                    //created_time
                    get_parameter['biz_shipment.booking_ETD-='] = it[1];
                }else if(it[0] == 'created_time_day'){
                    //created_time
                    var y = it[1].split('-')[0];
                    var m = parseInt(it[1].split('-')[1]);
                    get_parameter['biz_shipment.created_time-='] = it[1];
                }else if(it[0] == 'trans_ATD_day'){
                    //created_time
                    var y = it[1].split('-')[0];
                    var m = parseInt(it[1].split('-')[1]);
                    get_parameter['biz_shipment.trans_ATD-='] = it[1];
                }else if(it[0] == 'vessel_voyage'){
                    var val = it[1];
                    var val_arr = val.split(' ').filter(e=>e);
                    var voyage = val_arr.splice(val_arr.length - 1);
                    var vessel = val_arr.join(' ');
                    get_parameter['biz_shipment.vessel-like'] = vessel;
                    get_parameter['biz_shipment.voyage-like'] = voyage.join('');
                }else if(it[0] == 'sales_company'){
                    get_parameter['biz_shipment.sales_group-like'] = it[1];
                }else{
                    get_parameter['biz_shipment.' + it[0] + '-='] = it[1];
                }
            }
        });

        //参数1 field
        var dg = $('#tt');
        dg.datagrid('clearSelections');
        dg.datagrid('selectRow', index);
        var row = dg.datagrid('getSelections')[0];
        form_data.groups_field = "<?= $groups_field;?>";
        //参数2 当前点击明细的值 groups_field = name_id值
        if(form_data.groups_field == 'booking_ETD_month'){
            var y = row.name_id.split('-')[0];
            var m = parseInt(row.name_id.split('-')[1]);
            //created_time
            get_parameter['biz_shipment.booking_ETD->='] = row.name_id + '-01';
            get_parameter['biz_shipment.booking_ETD-<='] = row.name_id + '-' + getMonthDay(y, m - 1);
        }else if(form_data.groups_field == 'created_time_month'){
            //created_time
            var y = row.name_id.split('-')[0];
            var m = parseInt(row.name_id.split('-')[1]);
            get_parameter['biz_shipment.created_time->='] = row.name_id + '-01';
            get_parameter['biz_shipment.created_time-<='] = row.name_id + '-' + getMonthDay(y, m - 1);
        }else if(form_data.groups_field == 'trans_ATD_month'){
            //created_time
            var y = row.name_id.split('-')[0];
            var m = parseInt(row.name_id.split('-')[1]);
            get_parameter['biz_shipment.trans_ATD->='] = row.name_id + '-01';
            get_parameter['biz_shipment.trans_ATD-<='] = row.name_id + '-' + getMonthDay(y, m - 1);
        }else if(form_data.groups_field == 'booking_ETD_day'){
            //created_time
            get_parameter['biz_shipment.booking_ETD-='] = row.name_id;
        }else if(form_data.groups_field == 'created_time_day'){
            //created_time
            get_parameter['biz_shipment.created_time-='] = row.name_id;
        }else if(form_data.groups_field == 'trans_ATD_day'){
            //created_time
            get_parameter['biz_shipment.trans_ATD-='] = row.name_id;
        }else if(form_data.groups_field == 'vessel_voyage'){
            var val = row.name_id;
            var val_arr = val.split(' ').filter(e=>e);
            var voyage = val_arr.splice(val_arr.length - 1);
            var vessel = val_arr.join(' ');
            get_parameter['biz_shipment.vessel-like'] = vessel;
            get_parameter['biz_shipment.voyage-like'] = voyage.join('');
        }else if(form_data.groups_field == 'sales_company'){
            get_parameter['biz_shipment.sales_group-like'] = row.name_id;
        }else{
            get_parameter['biz_shipment.' + form_data.groups_field + '-='] = row.name_id;
        }
        $.each(form_data['field[v][]'], function(i,it){
            if(it != ''){
                var this_key = 'biz_shipment.' + form_data['field[f][]'][i] + '-' + form_data['field[s][]'][i];
                get_parameter[this_key] = it;
            }
        });
        //打开新窗口跳转到shipment
        var url = '/biz_shipment/index?auto_click=1';
        $.each(get_parameter, function (i, it) {
            url += "&" + urlencode(i) + '=' + urlencode(it);
        });
        window.open(url);
    }
    
    /**
     * 明细2 该明细是用于 根据job_no 统计的
     */
    function details2(index){
        var cx_data = $('#cx').serializeArray();
        
        var form_data= {};
        
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(form_data.hasOwnProperty(item.name) === true){
                if(typeof form_data[item.name] == 'string'){
                    form_data[item.name] =  form_data[item.name].split(',');
                    form_data[item.name].push(item.value);
                }else{
                    form_data[item.name].push(item.value);
                }
            }else{
                form_data[item.name] = item.value;
            }
        });
        
        //这里和原本相比,需要把之前的groups_field 加入到 groups_field[] 中, 
        //参数1 field
        var dg = $('#tt');
        dg.datagrid('clearSelections');
        dg.datagrid('selectRow', index);
        var row = dg.datagrid('getSelections')[0];
        
        var groups_field = form_data.groups_field;
        var serarchs = $('.searchs').length;
        
        form_data['searchs[' + (serarchs / 2) + '][]'] = [groups_field, row.name_id];
        form_data.groups_field = 'job_no';
        
        //打开新窗口跳转到shipment
        var form = $('<form></form>').attr('action','').attr('method','POST'); // .attr('target', '_blank')

        $.each(form_data, function (index, item) {
            if(typeof item === 'string'){
                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
            }else{
                $.each(item, function(index2, item2){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                })
            }
        });

        form.appendTo('body').submit().remove();
    }

    function buttons_for(value, row, index) {
        var str = '';
        if(row.is_footer) return str;
        str += '<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$(\'#chaxun\').window(\'open\');"><?= lang('查询');?></a>\n';
        str += '<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:details(' + index + ');"><?= lang('明细');?></a>\n';
        str += '<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:details2(' + index + ');"><?= lang('明细2');?></a>\n';
        return str;
    }
    
    function hidden_column(event) {
        event=event?event:window.event;
        event.stopPropagation();
        var is_add = $('.more_column').hasClass('add');

        //这里执行字段的隐藏和显示功能
        if(!is_add){
            $('.more_column').addClass('add');
            $('.more_column').removeClass('remove');
        }else{
            $('.more_column').addClass('remove');
            $('.more_column').removeClass('add');
        }

        //需要隐藏或显示的列名
        var tt = $('#tt');
        var opts = tt.datagrid('options');
        var pager = tt.datagrid('getPager');
        var panel = tt.datagrid('getPanel');

        var hidden_column = [
            <?php foreach($currency_table_fields as $currency_table_field){ ?>
                'sell_<?= $currency_table_field;?>_amount_sum',
                'cost_<?= $currency_table_field;?>_amount_sum',
                '<?= $currency_table_field;?>_gross_profit',
            <?php } ?>
        ];

        $.each(hidden_column, function (i, it) {
            //hidden的字段整理在一起
            if(!is_add){
                tt.datagrid('hideColumn', it);
            }else{
                tt.datagrid('showColumn', it);
            }
        });
    }
</script>
<ul class="breadcrumb">
    <li><a href="javascript:void(0);" id="now_crumb"></a></li>
</ul>
<table id="tt" rownumbers="false" idField="id" toolbar="#tb" singleSelect="false" nowrap="true" showFooter="true" autoRowHeight="false">
    <thead>
    <tr>
        <th title="全选" field="ck" checkbox="true"></th>
        <th data-options="field:'buttons',width:100,sortable:true,formatter:buttons_for"></th>
        <th data-options="field:'name',width:160,sortable:true"><?= lang($groups_field);?></th>
        <?php  
            if($groups_field=='sales' || $groups_field=='operator' || $groups_field=='customer_service'){
                echo "<th data-options=\"field:'sub_company_name',width:80,sortable:true\">" . lang('公司') . "</th>";
                echo "<th data-options=\"field:'group_name',width:100,sortable:true\">" . lang('部门') . "</th>";
            }
        ?>
        <th data-options="field:'gross_profit_rate',width:80,sortable:true"><?= lang('毛利率');?></th>
        <th data-options="field:'count_valid',width:80,sortable:true"><?= lang('有效票数');?></th>
        <th data-options="field:'count',width:80,sortable:true"><?= lang('业务票数');?></th>
        <th data-options="field:'sell_amount_sum',width:80,sortable:true"><?= lang("总应收({currency})", array('currency' => $local_currency));?></th>
        <th data-options="field:'cost_amount_sum',width:80,sortable:true"><?= lang("总应付({currency})", array('currency' => $local_currency));?></th>
        <th data-options="field:'gross_profit_sum',sortable:true"><?= lang("总毛利({currency})", array('currency' => $local_currency));?><a href="javascript:void(0);" class="more_column l-btn-icon add" onClick="hidden_column(event)" style="position: unset;padding-left: 5px;margin-top: 0;"></a></th>
        <?php foreach($currency_table_fields as $currency_table_field){ ?>
        <th data-options="field:'sell_<?= $currency_table_field;?>_amount_sum',width:80,sortable:true,hidden:true"><?= lang("{currency}应收", array('currency' => $currency_table_field));?></th>
        <th data-options="field:'cost_<?= $currency_table_field;?>_amount_sum',width:80,sortable:true,hidden:true"><?= lang("{currency}应付", array('currency' => $currency_table_field));?></th>
        <th data-options="field:'<?= $currency_table_field;?>_gross_profit',width:80,sortable:true,hidden:true"><?= lang("{currency}毛利", array('currency' => $currency_table_field));?></th>
        <?php } ?>
        <th data-options="field:'TEU',width:80,sortable:true"><?= lang('TEU');?></th>
        <th data-options="field:'sell_not_payment_amount',width:100,sortable:true"><?= lang('未到账数');?></th>
        <th data-options="field:'sell_payment_amount',width:80,sortable:true"><?= lang('到账数');?></th>
        <th data-options="field:'sell_payment_amount_rate',width:80,sortable:true"><?= lang('到账率');?></th>
        <th data-options="field:'cost_payment_amount',width:80,sortable:true"><?= lang('付款数');?></th>
        <th data-options="field:'cost_payment_amount_rate',width:80,sortable:true"><?= lang('付款率');?></th>
        <th data-options="field:'after_tax_gross_profit',width:100,sortable:true"><?= lang("去税毛利({currency})", array('currency' => $local_currency));?></th>
        <th data-options="field:'export_TEU',width:80,sortable:true"><?= lang('出口TEU');?></th>
        <th data-options="field:'ex_LCL_CBM',width:150,sortable:true"><?= lang('出拼CBM');?></th>
        <th data-options="field:'special_gross_profit',width:100,sortable:true"><?= lang('专项毛利({currency})', array('currency' => $local_currency));?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0);" onclick="back(1)"> <?= lang('返回上一页');?> </a>
                <a href="javascript:void(0)" id="search" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('查询');?></a>
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#excel_menu',plain:true" iconCls="icon-reload"><?= lang('导出表格');?></a>
            </td>
        </tr>
    </table>
    <div id="excel_menu" style="width:150px;">
        <div data-options="iconCls:'icon-ok'" onclick="export_excel()"><?= lang('导出当前页面');?></div>
    </div>
</div>
<div class="statistics_div">
    <form id="statistics_form">
        <table style="text-align:right;">
            <tr>
                <td><?= lang('业务票数');?>:<input class="easyui-textbox" name="count" readonly style="width:100px;"></td>
                <td><?= lang('退关票数');?>:<input class="easyui-textbox" name="count_cancel" readonly style="width:100px;"></td>
                <td><?= lang('出口TEU');?>:<input class="easyui-textbox" name="export_TEU" readonly style="width:100px;"></td>
                <td><?= lang('出拼CBM');?>:<input class="easyui-textbox" name="ex_LCL_CBM" readonly style="width:100px;"></td>
                <td><?= lang('进口TEU');?>:<input class="easyui-textbox" name="import_TEU" readonly style="width:100px;"></td>
                <td><?= lang('出空KGS');?>:<input class="easyui-textbox" name="ex_LCL_CBM" readonly style="width:100px;"></td>
                <td><?= lang('进空KGS');?>:<input class="easyui-textbox" name="im_AIR_KGS" readonly style="width:100px;"></td>
                <td><?= lang('总毛利');?>:<input class="easyui-textbox" name="gross_profit_sum" readonly style="width:100px;"></td>
            </tr>
            <tr>
                <td><?= lang('TEU');?>:<input class="easyui-textbox" name="TEU" readonly style="width:100px;"></td>
                <td><?= lang("20'’");?>:<input class="easyui-textbox" name="20_size" readonly style="width:100px;"></td>
                <td><?= lang("40'");?>:<input class="easyui-textbox" name="40_size" readonly style="width:100px;"></td>
                <td><?= lang("45'");?>:<input class="easyui-textbox" name="45_size" readonly style="width:100px;"></td>
            </tr>
        </table>
    </form>
</div>
<div id="chaxun" class="easyui-window" title="<?= lang('query');?>" closed="true" style="width:800px;height:380px;padding:5px;">
    <br><br>
    <form id="cx" action="/report/profit_total" method="POST">
        <input type="hidden" name="ids">
        <input type="hidden" name="back" id="back">
        <?php foreach($searchs as $s_key => $search){ ?>
            <input class="searchs" type="hidden" name="searchs[<?= $s_key;?>][]" tkey="<?= lang($search[0]);?>" value="<?= $search[0];?>" tname="<?= $search[2];?>">
            <input class="searchs" type="hidden" name="searchs[<?= $s_key;?>][]" tkey="<?= lang($search[0]);?>" value="<?= $search[1];?>" tname="<?= $search[2];?>">
        <?php } ?>
        <?php if(!empty($fields) && !empty($searchs)){ foreach($fields['f'] as $f_key => $f){ ?>
        <input type="hidden" name="field[f][]" value="<?= $f;?>">
        <input type="hidden" name="field[s][]" value="<?= $fields['s'][$f_key];?>">
        <input type="hidden" name="field[v][]" value="<?= $fields['v'][$f_key];?>">
        <?php }}else{ ?>
        <?php if(!empty($fields)){ ?>
        <table id="cx_table">
        <?php foreach($fields['f'] as $f_key => $this_f){ ?>
            <tr>
                <td>
                    <?= lang('查询');?>
                </td>
                <td align="right">
                    <select name="field[f][]" class="f easyui-combobox" data-options="
                        value:'<?= $this_f;?>',
                        required:true,
                        onChange:select_f_change
                    ">
                        <?php
                            foreach ($f as $rs){
                                $item = $rs[0];
                                echo "<option value='$item'>" . lang($item) . "</option>";
                            }
                            foreach($admin_field as $val) {
                                echo "<option value='{$val}'>" . lang($val) . "</option>";
                                echo "<option value='{$val}_group'>" . lang($val . '_group') . "</option>";
                            }
                        ?>
                    </select>
                    &nbsp;
                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="
                        value:'<?= $fields['s'][$f_key];?>',
                    ">
                        <option value="like"><?= lang("like");?></option>
                        <option value="like前"><?= lang("like前");?></option>
                        <option value="="><?= lang("=");?></option>
                        <option value=">="><?= lang(">=");?></option>
                        <option value="<="><?= lang("<=");?></option>
                        <option value="!="><?= lang("!=");?></option>
                    </select>
                    &nbsp;
                    <div class="this_v">
                        <input name="field[v][]" class="v easyui-textbox" value="<?= $fields['v'][$f_key]?>">
                    </div>
                    <button class="add_tr" type="button">+</button>
                </td>
            </tr>
        <?php } ?>
        </table>
        <?php }else{ ?>
        <table id="cx_table">
            <?php foreach($config_search[1] as $key => $config_search_field){?>
                <tr>
                    <td>
                        <?= lang('查询');?>
                    </td>
                    <td align="left">
                        <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="
                                onChange:select_f_change,
                            ">
                            <?php
                            foreach ($f as $rs){
                                $item = $rs[0];
                                $selected = '';
                                if($item == $config_search_field[0]) $selected = 'selected';
                                echo "<option value='$item' {$selected}>" . lang($item) . "</option>";
                            }
                            ?>
                        </select>
                        &nbsp;

                        <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="
                            ">
                            <option value="like" <?= $config_search_field[1] == 'like' ? 'selected' : '';?>><?= lang("like");?></option>
                            <option value="like前" <?= $config_search_field[1] == 'like前' ? 'selected' : '';?>><?= lang("like前");?></option>
                            <option value="=" <?= $config_search_field[1] == '=' ? 'selected' : '';?>><?= lang("=");?></option>
                            <option value=">=" <?= $config_search_field[1] == '>=' ? 'selected' : '';?>><?= lang(">=");?></option>
                            <option value="<=" <?= $config_search_field[1] == '<=' ? 'selected' : '';?>><?= lang("<=");?></option>
                            <option value="!=" <?= $config_search_field[1] == '!=' ? 'selected' : '';?>><?= lang("!=");?></option>
                        </select>
                        &nbsp;
                        <div class="this_v">
                            <input name="field[v][]" class="v easyui-textbox" value="<?= isset($config_search_field[2]) ? $config_search_field[2] : '';?>">
                        </div>
                        <?php if($key == 0){ ?>
                            <button class="add_tr" type="button">+</button>
                        <?php }else{ ?>
                            <button class="del_tr" type="button">-</button>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <?php } ?>
        <?php } ?>
        <br><br>
        <fieldset>
            <h3><?= lang("统计维度");?>：</h3>
              <table>
               <tr>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="report_date_month" <?php if($groups_field == 'report_date_month') echo 'checked';?> title="<?= lang("按月(report日期)");?>"><?= lang("按月(report日期)");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="trans_ATD_month" <?php if($groups_field == 'trans_ATD_month') echo 'checked';?> title="<?= lang("按月(ATD)");?>"><?= lang("按月(ATD)");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="booking_ETD_month" <?php if($groups_field == 'booking_ETD_month') echo 'checked';?> title="<?= lang("按月(订舱船期)");?>"><?= lang("按月(订舱船期)");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="created_time_month" <?php if($groups_field == 'created_time_month') echo 'checked';?> title="<?= lang("按月(创建日期)");?>"><?= lang("按月(创建)");?>
                    </td>
                   <td>
                       <input class="groups_field" type="radio" name="groups_field" value="trans_ATD_day" <?php if($groups_field == 'trans_ATD_day') echo 'checked';?> title="<?= lang("按日(ATD)");?>"><?= lang("按日(ATD)");?>
                   </td>
                    <td>
                       <input class="groups_field" type="radio" name="groups_field" value="report_date_day" <?php if($groups_field == 'report_date_day') echo 'checked';?> title="<?= lang("按日(report日期)");?>"><?= lang("按日(report日期)");?>
                   </td>
                   <td>
                       <input class="groups_field" type="radio" name="groups_field" value="booking_ETD_day" <?php if($groups_field == 'booking_ETD_day') echo 'checked';?> title="<?= lang("按日(订舱船期)");?>"><?= lang("按日(订舱船期)");?>
                   </td>
                   <td>
                       <input class="groups_field" type="radio" name="groups_field" value="created_time_day" <?php if($groups_field == 'created_time_day') echo 'checked';?> title="<?= lang("按日(创建日期)");?>"><?= lang("按日(创建)");?>
                   </td>
                </tr>
                <tr>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="sales" <?php if($groups_field == 'sales') echo 'checked';?> title="<?= lang("销售");?>"><?= lang("销售");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="sales_group" <?php if($groups_field == 'sales_group') echo 'checked';?> title="<?= lang("销售部");?>"><?= lang("销售部");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="operator" <?php if($groups_field == 'operator') echo 'checked';?> title="<?= lang("操作");?>"><?= lang("操作");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="operator_group" <?php if($groups_field == 'operator_group') echo 'checked';?> title="<?= lang("操作部");?>"><?= lang("操作部");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="customer_service" <?php if($groups_field == 'customer_service') echo 'checked';?> title="<?= lang("客服");?>"><?= lang("客服");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="customer_service_group" <?php if($groups_field == 'customer_service_group') echo 'checked';?> title="<?= lang("客服部");?>"><?= lang("客服部");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="job_no" <?php if($groups_field == 'job_no') echo 'checked';?> title="<?= lang("SHIPMENT 内部编号");?>"><?= lang("SHIPMENT");?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="trans_origin" <?php if($groups_field == 'trans_origin') echo 'checked';?> title="<?= lang("起运港");?>"><?= lang("起运港");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="trans_destination_inner" <?php if($groups_field == 'trans_destination_inner') echo 'checked';?> title="<?= lang("目的港");?>"><?= lang("目的港");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="creditor" <?php if($groups_field == 'creditor') echo 'checked';?> title="<?= lang("订舱代理");?>"><?= lang("订舱代理");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="client_code" <?php if($groups_field == 'client_code') echo 'checked';?> title="<?= lang("委托方");?>"><?= lang("委托方");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="vessel_voyage" <?php if($groups_field == 'vessel_voyage') echo 'checked';?> title="<?= lang("船名航次");?>"><?= lang("船名航次");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="biz_type" <?php if($groups_field == 'biz_type') echo 'checked';?> title="<?= lang("业务类型");?>"><?= lang("业务类型");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="sailing_area" <?php if($groups_field == 'sailing_area') echo 'checked';?> title="<?= lang("航线区域");?>"><?= lang("航线区域");?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="trans_carrier" <?php if($groups_field == 'trans_carrier') echo 'checked';?> title="<?= lang("承运人");?>"><?= lang("承运人");?>
                    </td>
                </tr>
            </table>
        </fieldset>
        <button class="easyui-linkbutton" onclick="doSearch(true);" style="width:200px;height:30px;" type="button"><?= lang('查询');?></button>
        <button id="cx_submit" type="submit" style="display:none;"></button>
    </form>
</div>