<?php


class bsc_menu_relation_model extends CI_Model
{
    private $table = 'bsc_menu_relation';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_one($f = "", $v = "")
    {
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_where_one($where='', $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get($where = "", $sort = "menu_id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function save($data = array())
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id = '', $data = array())
    {
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id = '')
    {
        // delete container
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    /**
     * ????????????
     * @param $menu_relation_id_type
     * @param $menu_relation_id_no
     */
    public function get_menu_relation($menu_relation_id_type = '', $menu_relation_id_no = 0){
        $where = array();
        $where[] = "menu_relation_id_type = '$menu_relation_id_type'";
        $where[] = "menu_relation_id_no = '$menu_relation_id_no'";
        $where = join(' and ', $where);
        $this->db->where($where);

        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    /**
     * ???????user_role ????
     * @param $menu_relation_id_type string
     * @param $menu_relation_id_no int
     * @param $menu_id int
     */
    public function save_menu_relation($menu_relation_id_type = '', $menu_relation_id_no = 0, $menu_id){
        $data = array();
        $data['menu_relation_id_type'] = $menu_relation_id_type;
        $data['menu_relation_id_no'] = $menu_relation_id_no;
        $data['menu_id'] = $menu_id;

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * ???????user_role ????
     * @param $menu_relation_id_type string
     * @param $menu_relation_id_no int
     * @param $menu_id int
     */
    public function del_menu_relation($menu_relation_id_type = '', $menu_relation_id_no = 0, $menu_id){
        $where = array();
        $where[] = "menu_relation_id_type = '$menu_relation_id_type'";
        $where[] = "menu_relation_id_no = '$menu_relation_id_no'";
        $where[] = "menu_id = '$menu_id'";
        $where = join(' and ', $where);

        // delete container
        $this->db->where($where);
        $this->db->delete($this->table);
        return true;
    }

    /**
     * 保存目录与角色的关联(批量)
     */
    public function batch_save($id_type, $id_no, $menu_ids){
        if(empty($id_type) || empty($id_no)){
            return false;
        }
        $menu_ids_array = filter_unique_array(explode(',', $menu_ids));

        //查询原本是否存在关联,不存在关联的进行删除
        //将当前所有关联的menu_id都查出来
        $this->db->select('menu_id');
        $menu_relations = array_column($this->get("menu_relation_id_type = '$id_type' and menu_relation_id_no = '$id_no'"), 'menu_id');

        //传入进来的为主,获取的差集为需要新增的
        $add_datas = array_diff($menu_ids_array, $menu_relations);
        foreach ($add_datas as $menu_id){
            $this->save_menu_relation($id_type, $id_no, $menu_id);
        }

        //数据库中与传入进来的差集表示要删除的
        $delete_datas = array_diff($menu_relations, $menu_ids_array);
        foreach ($delete_datas as $menu_id){
            $this->del_menu_relation($id_type, $id_no, $menu_id);
        }

        return true;
    }

    /**
     * 判断当前是否有该权限
     * @param $menu_id int
     */
    public function checkAuth($menu_id){
        //管理员跳过
        $user_id = get_session('id');
        //判断存储的权限是否有问题
        $cache_key = "role_auth_{$user_id}";

        $cache_data = cache_redis_get($cache_key);

        if(!empty($cache_data)){
            $roleAuth = explode(',', $cache_data);
        }else{
            $roleAuth = self::getRoleAuth();
            //存30分钟
            cache_redis_save(join(',', $roleAuth), $cache_key, 60 * 30);
        }
        return in_array($menu_id, $roleAuth);
    }

    /**
     * 清除指定用户的目录权限
     * @param $user_id
     * @return mixed
     */
    public function clearAuth($user_id){
        //判断存储的权限是否有问题
        $cache_key = "role_auth_{$user_id}";

        return cache_redis_delete($cache_key);
    }

    /**
     * 获取角色权限
     */
    public function getRoleAuth(){
        //目前只获取角色的权限
        $station = get_session('station');
        $userId = get_session('id');
        $station_array = filter_unique_array(explode(',', $station));

        $this->db->select('menu_id');
        $this->db->group_by('menu_id');
        $where = array();


        $where[] = "((menu_relation_id_type = 'bsc_user_role' and menu_relation_id_no in ('" . join("','", $station_array) . "')) " .//当前用户相关角色的目录权限
            "or (menu_relation_id_type = 'bsc_user' and menu_relation_id_no = '{$userId}'))";  //加上当前用户设置的单独权限
        $where_str = join(' and ', $where);


        $rows = self::get($where_str);

        $auths = array_column($rows, 'menu_id');
        return array_unique($auths);
    }

    /**
     * 获取当前访问节点信息，支持扩展参数筛查
     * @param string $id 节点ID
     * @author 橘子俊 <364666827@qq.com>
     * @return array
     */
    public function getInfo($id = 0)
    {
        $map = [];
        $checkField = '';

        if (empty($id)) {
            $directory = $this->router->directory;
            $controller     = $this->router->class;
            $action = $this->router->method;

            $map[] = "url = '" . $directory . $controller . '/' . $action . "'";
        } else {
            $map[] = 'id = $id';
        }

        $map[] = 'display = 1';
        $map[] = 'is_delete = 0';
        $map_str = join(' and ', $map);

        Model('bsc_menu_model');
        $this->db->select('id,name,url,param');
        $rows = $this->bsc_menu_model->get($map_str);
        if (!$rows) {
            return false;
        }

        sort($rows);

        $_param = $_GET;
        if (count($rows) > 1) {
            //如果没有参数,那么默认使用参数为空的那个
            if (!$_param) {
                foreach ($rows as $k => $v) {
                    if ($v['param'] == '') {
                        return $rows[$k];
                    }
                }
            }

            //这里为循环遍历判断当前是目录里的哪一个,
            foreach ($rows as $k => $v) {
                if ($v['param']) {
                    parse_str($v['param'], $param);
                    ksort($param);
                    $paramArr = [];

                    $checkField && $paramArr['field'] = $checkField;

                    foreach ($param as $kk => $vv) {
                        if (isset($_param[$kk])) {
                            $paramArr[$kk] = $_param[$kk];
                        }
                    }

                    $where     = array();
                    $where[]   = "param = '". http_build_query($paramArr) . "'";
                    $where[]   =  "url = '" . $directory . $controller . '/' . $action . "'";
                    $where_str = join(' and ', $where);

                    $this->db->select('id,name,url,param');
                    $res = $this->bsc_menu_model->get_where_one($where_str);
                    if ($res) {
                        return $res;
                    }
                }
            }

            $map[] = "param = ''";
            $map_str = join(' and ', $map);

            $this->db->select('id,name,url,param');
            $res = $this->bsc_menu_model->get_where_one($map_str);
            if ($res) {
                return $res;
            } else {
                return false;
            }

        }

        // 扩展参数判断
        if ($rows[0]['param']) {
            parse_str($rows[0]['param'], $param);
            $checkField && $param['field'] = $checkField;
            foreach ($param as $k => $v) {
                if (!isset($_param[$k]) || $_param[$k] != $v) {
                    return false;
                }
            }
        } else if ($checkField) {// 排除敏感参数
            if (isset($_param[$checkField])) {
                return false;
            }
        }

        return $rows[0];
    }
}