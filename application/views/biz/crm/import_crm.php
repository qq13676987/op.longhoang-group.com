<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title><?= lang('导入CRM客户');?></title>
    <meta name="keywords" content="">
    <meta name="viewport" content="width=1180">
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css"/>
    <script type="text/javascript" src="/inc/third/layer/layer.js"></script>
    <link rel="stylesheet" href="/inc/third/handsontable/handsontable.full.min.css"/>
    <script type="text/javascript" src="/inc/third/handsontable/handsontable.full.min.js"></script>
    <style>
        .search_box {
            border: 1px solid #ddd;
            float: left;
            margin: 10px 0px 0px;
            padding: 15px;
            background-color: #edf2f3;
            width: 99%;
        }
        .selected-td{ background: #ff4c42 !important; }
        .layui-this{font-size: 18px !important; font-weight: bolder}
    </style>
</head>
<div class="layui-tab-content" style="padding: 0px;">
    <div style="margin-bottom: 80px;">
        <div id="handsontable_box" style="width: 99%"></div>
    </div>
    <div id="save_tools" style="display:; width: 100%; text-align: center; border-top: 1px solid #008a6b; padding: 20px 0px;position: fixed; z-index: 999999; bottom: 0px; left: 0px; background-color: white;">
        <button class="layui-btn" id="import_btn" style=" width: 300px;"><?= lang('确认导入CRM客户');?></button>
        <button id="refresh_table" class="layui-btn layui-btn-primary"><?= lang('重置表格');?></button>
    </div>
</div>
<!--创建港口别名-->
<div id="_window" class="table" style="display: none; width: 100%">
    <div id="port_box"></div>
</div>

<!--进度条-->
<div id="progress" style=" display: none;">
    <div style="color: red; height: 35px; line-height: 30px; padding: 10px 25px;"><?= lang('Tips：导入过程中请不要关闭或刷新窗口，否则导入进程会被打断！');?></div>
    <div class="layui-progress layui-progress-big" lay-filter="demo" lay-showPercent="true"
         style="margin:0px 20px;">
        <div class="layui-progress-bar layui-bg-green" lay-percent="0%"></div>
    </div>
</div>
</body>
<script src="/inc/third/layuiadmin/layui/layui.js"></script>
<script src="/inc/js/importExcel.js?time=<?php echo time();?>"></script>
<script type="text/javascript">
    var import_data = false;
    var status = 0;
    layui.config({
        base: '/inc/third/layuiadmin/' //静态资源所在路径
    }).extend({
        index: 'lib/index',
    }).use(['index'], function () {
        //保存表格里的数据
        $('#import_btn').on('click', function () {
            var param = hot.getData();  //表格中的所有数据
            var count_rows = hot.countRows();   //表格的总行数
            var empty_row = hot.countEmptyRows(1);  //空行
            var columns = ['country_code', 'company_name',  'company_name_en', 'client_name', 'web_url', 'company_address', 'export_sailing', 'trans_mode', 'role', 'contact', 'telephone', 'email', 'position'];
            var row = 0;

            //如果没有数据
            if (empty_row >= count_rows) return;
            //开始循环导入
            var loading = layer.msg('<?= lang('导入中，请耐心等待...');?>', {icon:16, time:0, shade:0.5});
            var timer = setInterval(function () {
                //为数据添加键名
                var temp= {}
                $.each(columns, function (index, val) {
                    if (param[row][index] != null && param[row][index] !=  '') {
                        temp[val] = param[row][index];
                    }
                });
                //如果是空行则删除空行
                if ($.isEmptyObject(temp)){
                    hot.alter('remove_row', 0);
                    status = 1;
                }else {
                    $.ajax({
                        type: 'post',
                        url: '/biz_crm/import_crm',
                        data: temp,
                        success: function (data) {
                            if (data.code == 1) {
                                status = 1;
                                hot.alter('remove_row', 0);
                            }else{
                                layer.close(loading);
                                status = 0;
                                layer.alert(data.msg, {icon:5});
                            }
                        },
                        dataType: "json",
                        async: false    //重点，使用同步请求
                    });
                }

                //导入失败则断开，修改后重新执行导入
                if (status == 0){
                    row=0;
                    clearInterval(timer);
                }

                //准备下一行
                row++;
                //如果没有下一行
                if (row >= count_rows){
                    layer.close(loading);
                    layer.alert('<?= lang('所有数据已全部导入。');?>', {icon:1});
                    $('#refresh_table').click();
                    clearInterval(timer);
                }
            }, 1);
        });
    });

    $(function () {
        var columns = [
            {data: 'country_code', type: 'dropdown', className: "htLeft", width: 100},
            {data: 'company_name', type: 'text', className: "htLeft", width: 250},
            {data: 'company_name_en', type: 'text', className: "htLeft", width: 250},
            {data: 'client_name', type: 'text', className: "htLeft", width: 100},
            {data: 'web_url', type: 'text', className: "htLeft", width: 200},
            {data: 'company_address', type: 'text', className: "htLeft", width: 200},
            {data: 'export_sailing', type: 'dropdown', className: "htLeft", width: 100},
            {data: 'trans_mode', type: 'dropdown', className: "htCenter", width: 100},
            {data: 'role', type: 'dropdown', className: "htCenter", width: 150},
            {data: 'contact', type: 'text', className: "htLeft", width: 150},
            {data: 'telephone', type: 'text', className: "htLeft", width: 150},
            {data: 'email', type: 'text', className: "htCenter", width: 150},
            {data: 'position', type: 'dropdown', className: "htCenter", width: 100},
        ];

        var colHeaders = ['<?= lang('国家代码');?>', '<?= lang('客户全称');?>','<?= lang('英文全称');?>', '<?= lang('客户简称');?>', '<?= lang('客户网址');?>','<?= lang('客户地址');?>', '<?= lang('出口航线');?>', '<?= lang('运输模式');?>', '<?= lang('角色');?>', '<?= lang('联系人');?>', '<?= lang('手机号');?>', '<?= lang('邮箱');?>', '<?= lang('职位');?>'];
        var settingForHandsontable = {
            columns: columns, //列配置参数
            data: [{},{},{},{}],//为需要绑定数据集合
            licenseKey: 'non-commercial-and-evaluation',
            colHeaders: colHeaders, // 表头
            rowHeaders: true,
            dropdownMenu: true, //忘了
            fillHandle: true,
            contextMenu: true,//右键显示更多功能,
            filters: true,
            startRows: 0,
            startCols: 0,
            cells: function(row, col, prop) {
                switch (col) {
                    case 0:
                        this.source = <?=json_encode($country_code);?>;
                        break;
                    case 6:
                        this.source = <?=json_encode($export_sailing);?>;
                        break;
                    case 7:
                        this.source = <?=json_encode($trans_mode);?>;
                        break;
                    case 8:
                        this.source = <?=json_encode($client_role);?>;
                        break;
                    case 12:
                        this.source = <?=json_encode($contact_position);?>;
                        break;
                    default:
                }
                return this;
            },
        };
        var container = document.getElementById('handsontable_box');
        hot = new Handsontable(container, settingForHandsontable);

        //刷新表格
        $('#refresh_table').click(function () {
            hot.loadData([{},{},{},{}]);
            hot.loadData([{},{},{},{}]);
        })
    });
</script>