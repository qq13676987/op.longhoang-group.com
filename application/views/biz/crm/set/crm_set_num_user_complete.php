<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<div id="loadingDiv" style="position: absolute; z-index: 1000; top: 0px; left: 0px;
width: 100%; height: 100%; background: white; text-align: center;">
    <h1 style="top: 48%; position: relative;">
        <font color="#15428B">努力加载中···</font>
    </h1>
</div>
<script type="text/javascript">
    function closeLoading() {
        $("#loadingDiv").fadeOut("normal", function () {
            $(this).remove();
        });
    }
    var no;
    $.parser.onComplete = function () {
        if (no) clearTimeout(no);
        no = setTimeout(closeLoading, 1000);
    }
</script>
<style>
    table {
        width: 100%;
        border-collapse: collapse;
    }

    table caption {
        font-size: 2em;
        font-weight: bold;
        margin: 1em 0;
    }

    th, td {
        border: 1px solid #808695;
        text-align: center;
        padding: 10px 0;
    }

    table thead tr {
        background-color: #009688;
        color: #fff;
        font-size: 13px;
        font-weight: bold;
    }

    table tbody tr:nth-child(odd) {
        background-color: #eee;
    }

    table tbody tr:hover {
        background-color: #F0FFFF;
    }

    table tbody tr td:first-child {
        color: #009688;
        font-weight: bold;
        font-size: 13px;
    }

    table tfoot tr td {
        text-align: right;
        padding-right: 20px;
    }

    div::-webkit-scrollbar {
        /*滚动条整体样式*/
        width: 5px; /*高宽分别对应横竖滚动条的尺寸*/
        height: 1px;
    }

    div::-webkit-scrollbar-thumb {
        /*滚动条里面小方块*/

        box-shadow: inset 0 0 5px rgba(0, 0, 0, 0.2);
        background: #95B8E7;
    }

    div::-webkit-scrollbar-track {
        /*滚动条里面轨道*/
        box-shadow: inset 0 0 5px rgba(0, 0, 0, 0.2);
        background: #ededed;
    }
</style>
<body>
<h1>目标完成度</h1>
<div style="padding-top: 10px;padding-bottom: 10px;line-height:30px;height:30px">
    <sapn style="font-size: 1rem;color:#79a1d7;font-weight: bold;vertical-align: middle;">请选择月份</sapn>
    <input name="date_s" id="date_s" value="<?= $date_s ?>" autocomplete="off"
           onclick="javascript:WdatePicker({startDate:'%y-%M',dateFmt:'yyyy-MM'});"
           style="width:150px;border: 1px solid #95B8E7;line-height: 20px;height:20px;border-radius: 5px;outline: none;margin:0px 10px">
    — <input name="date_e" id="date_e" value="<?= $date_e ?>" autocomplete="off"
             onclick="javascript:WdatePicker({startDate:'%y-%M',dateFmt:'yyyy-MM'});"
             style="width:150px;border: 1px solid #95B8E7;line-height: 20px;height:20px;border-radius: 5px;outline: none;margin:0px 10px">
    <button onclick="selected()" style="height:20px;line-height: 16px;vertical-align: middle;">查询</button>
</div>
<div style="height:670px;overflow:scroll;padding:1px;border:1px solid #79a1d7;width:98%;">
    <table>
        <thead>
        <tr>
            <th width="15%" rowspan="2">销售部门</th>
            <th colspan="3" style="background: #5FB878">
                <span style="font-size: 15px"><?= $date_s ?> — <?= $date_e ?></span> 月目标完成度
            </th>
        </tr>
        <tr>
            <th style="background: #5FB878;width:10%">crm客户数</th>
            <th style="background: #5FB878;width:10%">新增合作客户数</th>
            <th style="background: #5FB878;width:10%">订单量</th>
        </tr>
        </thead>
        <tbody>
        <?php if (empty($group)): ?>
            <tr>
                <td colspan="9">请先选择月份暂无数据</td>
            </tr>
        <?php else: ?>
            <?php foreach ($group as $k => $v): ?>
                <tr>
                    <td>
                        <?= $v['name'] ?>
                    </td>
                    <td style="text-align: center;">
                        <div style="display: inline-block;width:40%;">
                            <span style="color:#009688;font-weight: bold">
                                <a href="/biz_crm/crm_set_details/<?=$v['id']?>/1/crm?date_s=<?= $date_s ?>&date_e=<?= $date_e ?>" target="_blank"><?= $rs_crm_month[$v['id']] ?></a>
                            </span>&nbsp;&nbsp;/&nbsp;
                            <span style="color:#1E9FFF;font-weight: bold"><?= $v['crm_num_month'] ?></span>
                        </div>
                        <div style="height:10px;">&nbsp;</div>
                        <div sytle="pedding-top:10px;">
                            <span style="color:#FF5722;font-weight: bold">(<?= $v['crm_month'] ?>)</span>
                        </div>


                    </td>
                    <td style="text-align: center;">
                        <div style="display: inline-block;width:40%;">
                            <span style="color:#009688;font-weight: bold">
                                <a href="/biz_crm/crm_set_details/<?=$v['id']?>/1/client?date_s=<?= $date_s ?>&date_e=<?= $date_e ?>" target="_blank"><?= $rs_new_customer_month[$v['id']] ?></a>
                            </span>&nbsp;&nbsp;/&nbsp;
                            <span style="color:#1E9FFF;font-weight: bold"><?= $v['client_num_month'] ?></span>
                        </div>
                        <div style="height:10px;">&nbsp;</div>
                        <div sytle="pedding-top:10px;">
                            <span style="color:#FF5722;font-weight: bold">(<?= $v['client_month'] ?>)</span>
                        </div>


                    </td>
                    <td style="text-align: center;">
                        <div style="display: inline-block;width:40%;">
                            <span style="color:#009688;font-weight: bold">
                                <a href="/biz_crm/crm_set_details/<?=$v['id']?>/1/shipment?date_s=<?= $date_s ?>&date_e=<?= $date_e ?>" target="_blank"><?= $rs_order_num_month[$v['id']] ?></a>
                            </span>&nbsp;&nbsp;/&nbsp;
                            <span style="color:#1E9FFF;font-weight: bold"><?= $v['shipment_num_month'] ?></span>
                        </div>
                        <div style="height:10px;">&nbsp;</div>
                        <div sytle="pedding-top:10px;">
                            <span style="color:#FF5722;font-weight: bold">(<?= $v['shipment_month'] ?>)</span>
                        </div>

                    </td>

                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
</div>

</body>
<script>
    function load() {
        $("<div class=\"datagrid-mask\"></div>").css({
            display: "block",
            width: "100%",
            height: $(window).height()
        }).appendTo("body");
        $("<div class=\"datagrid-mask-msg\"></div>").html("加载中，请稍候。。。").appendTo("body").css({
            display: "block",
            left: ($(document.body).outerWidth(true) - 190) / 2,
            top: ($(window).height() - 45) / 2
        });
    }

    function selected() {
        var date_s = $('#date_s').val();
        var date_e = $('#date_e').val();
        if (date_s == '') {
            alert('请选择开始月份');
            return;
        }
        if (date_e == '') {
            alert('请选择结束月份');
            return;
        }
        if (date_s > date_e) {
            alert('开始时间不能大于等于结束时间');
            return;
        }
        window.location.href = '/biz_crm/crm_set_num_user_complete?group_code=<?= $group_code ?>&date_s=' + date_s + '&date_e=' + date_e;
        load();
    }
</script>