
<script>
    function errorTip(field, isDiyMessage = false,DiyMessage = 'This field is required'){
        var jq = $('#' + field);
        if(jq.length == 0) return false;
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        var tip_data = {isDiyMessage:isDiyMessage, DiyMessage:DiyMessage};
        if(data.combobox !== undefined){
            return $(jq).combobox('errorTip', tip_data);
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('errorTip', tip_data);
        }else if(data.datebox !== undefined){
            return $(jq).datebox('errorTip', tip_data);
        }else if(data.textbox !== undefined){
            return $(jq).textbox('errorTip', tip_data);
        }
    }
    $(function() {
        <?php foreach($errorTips as $val){ ?>
        errorTip('<?= $val;?>');
        <?php } ?>
        <?php foreach($disabledTips as $val){ ?>
        errorTip('<?= $val;?>', true, '<?= $cause;?>');
        <?php } ?>
    });
</script>