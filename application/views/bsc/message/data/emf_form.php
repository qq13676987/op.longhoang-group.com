<script type="text/javascript">
    var is_submit = false;
    function reset_data(){
        var json = {'is_reset':'1'};
        // GET方式示例，其它方式修改相关参数即可
        var form = $('<form></form>').attr('action','').attr('method','POST');

        $.each(json, function (index, item) {
            if(typeof item === 'string'){
                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
            }else{
                $.each(item, function(index2, item2){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                })
            }
        });

        form.appendTo('body').submit().remove();
    }

    /**
     * 保存数据
     */
    function save_data(){
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $('#fm').form('submit', {
            url: '/bsc_message/set_message_data/consol/<?= $consol_id;?>/E-MANIFEST/EFREIGHT',
            onSubmit: function(param){
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                    is_submit = false;
                    $('.ShippingParty').css('display', 'table-row');
                }
                param = getShippingParties(param);
                return validate;
            },
            success:function(res_json){
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误请联系管理员');
                    return;
                }
                ajaxLoadEnd();
                is_submit = false;
                if(res.code == 0){
                    create_edi();
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.href = "";
                    });
                }else{
                    $.messager.alert('Tips', res.msg);
                }
            }
        });
    }

    function create_edi(action = 'insert'){
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $.ajax({
            type: 'GET',
            url: '/bsc_message/create_message/<?= $consol_id;?>/EFREIGHT/E-MANIFEST/' + action,
            success: function (res_json) {
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误请联系管理员');
                    return;
                }
                ajaxLoadEnd();
                is_submit = false;
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.reload();
                    });
                }else{
                    $.messager.alert('Tips', res.msg);
                }
            },
            error: function () {
                ajaxLoadEnd();
                is_submit = false;
                $.messager.alert('Tips', '发生错误');
            }
        });
    }

    function send_edi(action, receiver, id) {
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $.ajax({
            type: 'GET',
            url: '/bsc_message/send_message/' + id + '/' + action + '/' + receiver,
            success: function (res_json) {
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误请联系管理员');
                    return;
                }
                ajaxLoadEnd();
                is_submit = false;
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.reload();
                    });
                }else{
                    $.messager.alert('Tips', res.msg);
                }
            },
            error: function () {
                ajaxLoadEnd();
                is_submit = true;
                $.messager.alert('Tips', 'error');
            }
        });
    }

    function read_message(id) {
        window.open('/bsc_message/file_read/' + id);
    }

    function ajax_get(url,name) {
        return $.ajax({
            type:'POST',
            url:url,
            dataType:'json',
            success:function (res) {
                ajax_data[name] = res;
            },
            error:function () {
                // $.messager.alert('获取数据错误');
            }
        });
    }

    function create_tool(div_id, id){
        var html = '<div id="' + div_id + '_div">' +
            '<form id="' + div_id + '_form" method="post">' +
            '<div style="padding-left: 5px;display: inline-block;">' +
            '<label><?= lang('company_name');?>:</label><input name="company_name" class="easyui-textbox keydown_search" onkeydown="keydown_search(event, ' + id + ')" style="width:96px;" data-options="prompt:\'text\'"/>' +
            '</div>' +
            '<form>' +
            '<div style="padding-left: 1px; display: inline-block;">' +
            '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" onclick="query_report(\'' + div_id + '\', \'' + id + '\')"><?= lang('search');?></a>' +
            '</div>' +
            '</div>';
        $('body').append(html);
        $.parser.parse('#' + div_id + '_div');
        return div_id;
    }

    /**
     * 根据类名,批量赋值 新增
     */
    function setValueByClass(class_name, val) {
        var inps = $('.' + class_name);

        $.each(inps, function (i, inp) {
            var jq = $(inp);
            setValue(jq, val);
        });

    }
    function setValue(jq, val) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combobox !== undefined){
            return $(jq).combobox('setValue',val);
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('setValue',val);
        }else if(data.datebox !== undefined){
            return $(jq).datebox('setValue',val);
        }else if(data.textbox !== undefined){
            return $(jq).textbox('setValue',val);
        }
    }

    /**
     * 对象转表单name格式
     */
    function obj_form_name(result, obj) {
        var parseParam=function(param, result, key){
            if(param instanceof String||param instanceof Number||param instanceof Boolean){
                result[key] = param;
            }else{
                $.each(param,function(i){
                    var k=key==null?i:key+(param instanceof Array || param instanceof Object ?"["+i+"]":"."+i);

                    result = parseParam(this, result, k);
                });
            }
            return result;
        };

        return parseParam(obj, result);
    }

    //新增
    function company_select(row, e){
        //判断下面是否有对应ID的，没有生成一套新的
        var id = row.id;
        //首先得找到这个div
        if(e === undefined) e = $(this);
        var company_type = e.combogrid('options').company_type;

        //循环将全部值一样的重新赋值
        $.each($('.company_select'), function (i, it) {
            var t  = $(it);

            var this_id = t.combogrid('getValue');
            var this_company_type = t.combogrid('options').company_type;
            var key = t.attr('key');
            //如果当前ID和记录的ID一样,那么开始选中
            if(this_id == id){
                var temp_company_type = company_type;
                if(company_type == 'consignee' && this_company_type == 'notify') temp_company_type = 'notify';
                // 找到了进行赋值
                $('#PartyName_' + temp_company_type + '_' + key).val(row.company_name);
                $('#Address_' + temp_company_type + '_' + key).val(row.company_address);
                $('#CountryValue_' + temp_company_type + '_' + key).textbox('setValue', row.country_name);
                $('#CountryCode_' + temp_company_type + '_' + key).textbox('setValue', row.country);
                $('#City_' + temp_company_type + '_' + key).textbox('setValue', row.city);
                $('#PostalCode_' + temp_company_type + '_' + key).textbox('setValue', row.postalcode);
                // $('#Region_' + temp_company_type + '_' + key).textbox('setValue', row.region);
                $('#SubDivisionCode_' + temp_company_type + '_' + key).textbox('setValue', row.region_code);
                $('#ContactName_' + temp_company_type + '_' + key).textbox('setValue', row.company_contact);
                $('#ContactMail_' + temp_company_type + '_' + key).textbox('setValue', row.company_email);
                $('#Phone_' + temp_company_type + '_' + key).textbox('setValue', row.company_telephone);
            }
        });
    }
    //新增
    function getShippingParties(form_val) {
        var result = {};
        //循环将全部值一样的重新赋值
        $.each($('.company_select'), function (i, it) {
            var t  = $(it);

            var company_type = t.combogrid('options').company_type;

            var key = t.attr('key');
            var id = t.combogrid('getValue');

            // 找到了进行赋值
            if(result[id] === undefined) result[id] = {};
            result[id]['PartyCode'] = id;
            result[id]['PartyName'] = $('#PartyName_' + company_type + '_' + key).val();
            result[id]['PartyType'] = $('#PartyType_' + company_type + '_' + key).val();
            if(result[id]['PartyLocation'] === undefined) result[id]['PartyLocation'] = {};
            result[id]['PartyLocation']['Address'] = $('#Address_' + company_type + '_' + key).val();

            if(result[id]['PartyLocation']['Country'] === undefined) result[id]['PartyLocation']['Country'] = {};
            result[id]['PartyLocation']['Country']['Value'] = $('#CountryValue_' + company_type + '_' + key).textbox('getValue');
            result[id]['PartyLocation']['Country']['code'] = $('#CountryCode_' + company_type + '_' + key).textbox('getValue');
            result[id]['PartyLocation']['City'] = $('#City_' + company_type + '_' + key).textbox('getValue');
            result[id]['PartyLocation']['PostalCode'] = $('#PostalCode_' + company_type + '_' + key).textbox('getValue');
            // result[id]['PartyLocation']['Region'] = $('#Region_' + company_type + '_' + key).textbox('getValue');
            result[id]['PartyLocation']['SubDivisionCode'] = $('#SubDivisionCode_' + company_type + '_' + key).textbox('getValue');

            // if(result[id]['ContactPersons'] === undefined) result[id]['ContactPersons'] = {};
            // result[id]['ContactPersons']['ContactName'] = $('#ContactName_' + company_type + '_' + key).textbox('getValue');
            // result[id]['ContactPersons']['ContactMail'] = $('#ContactMail_' + company_type + '_' + key).textbox('getValue');
            // result[id]['ContactPersons']['Phone'] = $('#Phone_' + company_type + '_' + key).textbox('getValue');
        });
        var result2 = {ShippingParties:result};
        var result3 = obj_form_name(form_val, result2);
        return result3;
    }

    var ajax_data = {};
    $(function () {
        var a1 = ajax_get('/city/get_country','country_code');
        $.when(a1).done(function () {
            $('.country').combobox('loadData', ajax_data.country_code);
        });
        var a2 = ajax_get('/biz_port/get_option?type=<?= $consol['trans_mode'];?>&carrier=<?= $consol['trans_carrier'];?>','port');
        $.when(a2).done(function () {
            //新增
            $('.port-select').combobox('loadData', ajax_data.port);
            // $('.port').combobox('loadData', ajax_data.port);
        });


        var company_config = {
            panelWidth: '600px',
            panelHeight: '300px',
            idField: 'id',              //ID字段
            textField: 'id',    //显示的字段
            // multiple:true,
            // fitColumns : true,
            // striped: true,
            editable: false,
            pagination: false,           //是否分页
            // pageList : [30,50,100],
            // pageSize : 50,
            // rownumbers: true,           //序号
            collapsible: true,         //是否可折叠的
            method: 'post',
            columns:[[
                {field:'id',title:'<?= lang('id');?>',width:80},
                {field:'company_name',title:'<?= lang('country_code');?>',width:200},
                {field:'city',title:'<?= lang('city');?>',width:100},
            ]],
            emptyMsg : '未找到相应数据!',
            onSelect : function (index,row) {
                var options = $(this).datagrid('options');
                var t = $('#' + options.id);
                company_select(row, t);
            },
            onLoadSuccess:function (data) {
                //判断是否是加载
                var t = $(this);
                var val = t.combogrid('getValue');
                //重新选中
                var res = data.rows.filter(item => item.id == val);
                if(res.length > 0){
                    company_select(res[0], t);
                }
            }
        };

        //新增不确定
        var this_inp = $('#ConsolidatorRefCode');
        this_inp.combogrid('reset');
        company_config.company_type = 'consolidator';
        company_config.url = '/biz_company/get_options/consolidator/' + this_inp.attr('client_code');
        company_config.value = this_inp.val();
        company_config.toolbar = '#' + create_tool('ConsolidatorRefCode', 'ConsolidatorRefCode') + '_div';
        this_inp.combogrid(company_config);


        $.each($('.House'), function (i, it) {

               var this_inp = $('#ShipperRefCode' + i);
               this_inp.combogrid('reset');
               company_config.company_type = 'shipper';
               company_config.url = '/biz_company/get_options/shipper/' + this_inp.attr('client_code');
               company_config.value = this_inp.val();
               company_config.toolbar = '#' + create_tool('ShipperRefCode' + i, 'ShipperRefCode' + i) + '_div';
               this_inp.combogrid(company_config);

               var this_inp = $('#ConsigneeRefCode' + i);
               this_inp.combogrid('reset');
               company_config.company_type = 'consignee';
               company_config.url = '/biz_company/get_options/consignee/' + this_inp.attr('client_code');
               company_config.value = this_inp.val();
               company_config.toolbar = '#' + create_tool('ConsigneeRefCode' + i, 'ConsigneeRefCode' + i) + '_div';
               this_inp.combogrid(company_config);

               var this_inp = $('#NotifyRefCode' + i);
               this_inp.combogrid('reset');
               company_config.company_type = 'notify';
               company_config.url = '/biz_company/get_options/notify/' + this_inp.attr('client_code');
               company_config.value = this_inp.val();
               company_config.toolbar = '#' + create_tool('NotifyRefCode' + i, 'NotifyRefCode' + i) + '_div';
               this_inp.combogrid(company_config);

               //新增不确定
               var this_inp = $('#ContainerLoadingPlaceRefCode' + i);
               this_inp.combogrid('reset');
               company_config.company_type = 'containerloadingplace';
               company_config.url = '/biz_company/get_options/containerloadingplace/' + this_inp.attr('client_code');
               company_config.value = this_inp.val();
               company_config.toolbar = '#' + create_tool('ContainerLoadingPlaceRefCode' + i, 'ContainerLoadingPlaceRefCode' + i) + '_div';
               this_inp.combogrid(company_config);


        });
    });

    function query_report(div_id, load_id){
        var where = {};
        var form_data = $('#' + div_id + '_form').serializeArray();
        $.each(form_data, function (index, item) {
            if(where.hasOwnProperty(item.name) === true){
                if(typeof where[item.name] == 'string'){
                    where[item.name] =  where[item.name].split(',');
                    where[item.name].push(item.value);
                }else{
                    where[item.name].push(item.value);
                }
            }else{
                where[item.name] = item.value;
            }
        });
        // where['field'] = $('#to_search_field').combo('getValue');
        $('#' + load_id).combogrid('grid').datagrid('load',where);
    }
    //text添加输入值改变
    function keydown_search(e, click_id){
        if(e.keyCode == 13){
            $('#' + click_id).trigger('click');
        }
    }

    function edit_company(id) {
        var val = $('#' + id).combogrid('getValue');
        if(val == ''){
            $.messager.confirm('Tips', '请先选中任意客户信息');
            return;
        }

        //展开window,进入编辑页面
        var width = 504;
        var height = 600;
        var window_iframe_css = {width: width, height: height};
        $('#window_iframe').css(window_iframe_css).attr('src', '/biz_company/edit_iframe?id=' + val + '&reload_input=' + id);
        $('#window').window({
            title:'修改抬头',
            width:width + 40,
            height:height + 40,
            modal:true
        });
    }

    function edit_company_2(id) {
        var val = $('#' + id).combogrid('getValue');
        if(val == ''){
            $.messager.confirm('Tips', '请先选中任意客户信息');
            return;
        }
        window.open('/biz_company/edit/' + val);
    }

    function add_company(id) {
        var client_code = $('#' + id).attr('client_code');
        var options = $('#' + id).combogrid('options');
        window.open('/biz_company/add/' + options.company_type + '/' + client_code);
    }

    /**
     * 刷新company的数据
     * @param id
     */
    function reload_company(id) {
        $('#' + id).combogrid('grid').datagrid('reload');
    }

    function add_company_consol(id) {
        var options = $('#' + id).combogrid('options');
        var company_alert = {title:'Tips',msg:'请选择需要添加的客户',fn:function (r) {
                window.open('/biz_company/add/' + options.company_type + '/' + r);
            }, input:"<select class='easyui-combobox messager-input' style=\"width:266px;\" data-options=\"valueField:'client_code',textField:'company_name',url:'/biz_client/get_consol_client/<?= $consol_id;?>'\"></select>", input_val:"dlg.find(\".messager-input\").combobox('getValue')"};
        //这里打算让他们选择一个客户代码的下拉框，选择后进入compan页面进行处理
        $.messager.promptext(company_alert);
    }
</script>
<style type="text/css">
    .content{
        width: 98vw;
        height: 100vh;
        display: flex;
        display: -webkit-flex; /* Safari */
        margin: auto;
    }
    .master{
        min-height: 100%;
    }
    .flex{
        display: flex;
        display: -webkit-flex;
    }
    .houses{
        min-height: 100%;
    }

    .title1{
        font-size: 18px;
        font-weight: bold;
    }
    .master-left{
        width: 100%;
        margin-left: 20px;
    }
    .form-item{
        display: flex;
        display: -webkit-flex;
        padding-bottom:10px;
    }
    .form-input{
        width: 60%;
    }
    .form-lable{
        width: 45%;
        text-align: right;
        line-height:18px;
        /*white-space: nowrap;*/
        /*overflow: hidden;*/
        /*text-overflow: ellipsis;*/
    }
    .form-lable-full{
        width: 100%;
        text-align: left;
    }
    .master-left-routerinfo{
        width: 100%;
    }
    .master-left-routerinfo-item{
        width: 100%;
        padding: 1% 0;
    }
    .port{
        display: flex;
        display: -webkit-flex;
    }
    .port-code{
        width: 22%;
        padding-right: 2%;
    }
    .port-name{
        width: 32%;
        padding-right: 2%;
    }
    .port-date{
        width: 32%;
    }
    .master-left-container-item{
        padding-top: 5px;
    }
    .house-row{
        display: flex;
        display: -webkit-flex;
        max-width: 85%;
        padding-bottom: 20px;
    }
    .house-title-left{
        width: 10%;
        min-width: 100px;
    }
    .house-title-right{
        width: 90%;
    }
    .house-content{
        padding-left: 20px;
    }
    .house-row-left{
        width: 30%;
        display: flex;
        display: -webkit-flex;
        align-items: center;
    }
    .house-row-left-label{
        height: 25px;
        line-height: 25px;
        padding-right: 10px;
    }
    .house-row-right{
        width: 70%;
        margin-left: 10px;
    }
    .house-row-right-item{
        display: flex;
        display: -webkit-flex;
        padding-bottom: 10px;
    }
    .house-row-right-item-1{
        min-width: 74px;
        padding-left: 20px;
    }
    .house-row-right-item-2{
        width: 25%;
        min-width: 150px;
    }
    .house-row-right-item-3{
        width: 15%;
        min-width: 100px;
        padding-left: 20px;
    }
    .house-row-right-item-4{
        width: 25%;
        min-width: 150px;
    }
    .house-row-right-item-5{
        width: 25%;
        min-width: 150px;
        padding-left: 5px;
    }
    .weight{
        font-weight: bold;
    }
    .container{
        display: flex;
        display: -webkit-flex;
        flex-direction: column;
        width: 100%;
        min-width: 1000px;
    }
    .container-title{
        border:1px solid #e7e8ee;border-radius:1px;padding:10px;background-color: #f4f5f9;height:20px
        display: flex;
        display: -webkit-flex;
        min-width:1000px
    }
    .container-body{
        border:1px solid #e7e8ee;border-radius:1px;padding:10px;
        display: flex;
        display: -webkit-flex;
        min-width:1000px
    }
    .container-item{
        width: 10%;
        padding-right: 20px;
    }
    .container-title-item{
        width: 10%;
        padding-right: 20px;
    }
    .container-body>.container-item:nth-child(1){
        min-width: 93px;
    }
    .container-title>.container-title-item:nth-child(1){
        min-width: 93px;
    }
    .container-body>.container-item:nth-child(2){
        min-width: 50px;
    }
    .container-title>.container-title-item:nth-child(2){
        min-width: 50px;
    }
    .container-body>.container-item:nth-child(3){
        min-width: 50px;
    }
    .container-title>.container-title-item:nth-child(3){
        min-width: 50px;
    }
    .container-body>.container-item:nth-child(4){
        min-width: 75px;
    }
    .container-title>.container-title-item:nth-child(4){
        min-width: 75px;
    }
    .container-body>.container-item:nth-child(5){
        min-width: 27px;
    }
    .container-title>.container-title-item:nth-child(5){
        min-width: 27px;
    }

    .container-body>.container-item:nth-child(6){
        min-width: 122px;
    }
    .container-title>.container-title-item:nth-child(6){
        min-width: 122px;
    }
    .container-body>.container-item:nth-child(7){
        min-width: 127px;
    }
    .container-title>.container-title-item:nth-child(7){
        min-width: 127px;
    }
    .container-title>.container-title-item:nth-child(8){
        min-width: 48px;
    }
    .container-title>.container-title-item:nth-child(8){
        min-width: 48px;
    }
    .container-title>.container-title-item:nth-child(9){
        min-width: 68px;
    }
    .container-title>.container-title-item:nth-child(9){
        min-width: 68px;
    }
    input:read-only,select:read-only,textarea:read-only
    {
        background-color: #d6d6d6;
        cursor: default;
    }
    label{
        padding-right:10px;
        display: inline-block;
        vertical-align: middle;
    }
</style>
<body>
<button onclick="reset_data()" title="初始化数据">初始化</button>
<button onclick="save_data()" title="保存">保存数据</button>
<?php if(empty($message)){ ?>
    <button onclick="create_edi('<?= $action;?>')" title="生成报文">生成报文</button>
<?php }else{?>
    <button onclick="create_edi('<?= $action;?>')" title="生成报文">生成报文</button>
    <br />
    发送情况:
    <?php if($message['status'] == 0){?>
        未发送
    <?php }else if($message['status'] == 1){
        echo $message['send_time'];?>
        发送成功
    <?php }else if($message['status'] == 2){?>
        发送修改成功
    <?php }?>
    <br />
    <button class="read_message" onclick="read_message(<?= $message['id'];?>)">查看报文</button>
    <button class="send" onclick="send_edi('API', '<?= $message['receiver'];?>', <?= $message['id'];?>)">发送报文</button>
<?php }?>
<br/>
<span style="color:red;font-size:20px;">该页面除第一次生成时，后续进入该页面，都是读取上一次的，如果某些信息修改后需要重新获取，请点击初始化</span>
<form id="fm" method="post">
    <div class="easyui-layout" style="width:100%;min-height:840px;">
        <div data-options="region:'west'" title="Master" style="width:30%;max-width: 450px;" fit="true">
            <div class="easyui-accordion master" data-options="multiple:true,">
                <div title="Master info" data-options="selected:true" style="padding-top: 15px;padding-bottom: 10px">


                    <div class="form-item">
                        <div class="form-lable">
                            <label>FilingType : </label>
                        </div>
                        <div class="form-input">

                            <select class="easyui-combobox" id="filing_type" required name="Master[FilingType]" editable="false"  style="width: 80%;" data-options="
                    value:'<?= $edi_data['Master']['FilingType'];?>',
                ">
                                <option value="I">Import</option>
                                <option value="B">In-Bond / IT  required Bonded Freight Forwarder</option>
                                <option value="F">Freight Remain on Board</option>
                                <option value="S">In-Transit / TandE Transportation and Exportation</option>
                            </select>

                        </div>
                    </div>


                    <div class="form-item">
                        <div class="form-lable">
                            <label>MasterBillNo : </label>
                        </div>
                        <div class="form-input">
                            <input class="easyui-textbox" required name="Master[MasterBillNo]"
                                   value="<?= $edi_data['Master']['MasterBillNo']; ?>" style="width: 80%;">
                        </div>
                    </div>
                    <div class="form-item">
                        <div class="form-lable">
                            <label>CarrierName : </label>
                        </div>
                        <div class="form-input flex">
                            <input class="easyui-textbox" name="Master[CarrierName]" required readonly
                                   value="<?= $edi_data['Master']['CarrierName']; ?>" style="width: 80%;">
                        </div>
                    </div>
                    <div class="form-item">
                        <div class="form-lable">
                            <label>MasterRefNo : </label>
                        </div>
                        <div class="form-input">
                            <input class="easyui-textbox" name="Master[MasterRefNo]" required readonly
                                   value="<?= $edi_data['Master']['MasterRefNo']; ?>" style="width: 80%;">
                        </div>
                    </div>
                    <div class="form-item">
                        <div class="form-lable">
                            <label>OriginAgentCode : </label>
                        </div>
                        <div class="form-input">
                            <input class="easyui-textbox" name="Master[OriginAgentCode]" required readonly
                                   value="<?= $edi_data['Master']['OriginAgentCode']; ?>" style="width: 80%;">
                        </div>
                    </div>
                    <div class="form-item">
                        <div class="form-lable">
                            <label>DestinationAgentCode : </label>
                        </div>
                        <div class="form-input">
                            <input class="easyui-textbox" name="Master[DestinationAgentCode]" required readonly
                                   value="<?= $edi_data['Master']['DestinationAgentCode']; ?>" style="width: 80%;">
                        </div>
                    </div>

                    <div class="form-item">
                        <div class="form-lable">
                            <label>ConsignmentType : </label>
                        </div>
                        <div class="form-input">
                            <input class="easyui-textbox" name="Master[ConsignmentType]" required readonly
                                   value="<?= $edi_data['Master']['ConsignmentType']; ?>" style="width: 80%;">
                        </div>
                    </div>

                    <div class="form-item">
                        <div class="form-lable">
                            <label>ACISubMasterBillNo[code] : </label>
                        </div>
                        <div class="form-input">
                            <input class="easyui-textbox" name="Master[ACISubMasterBillNo][code]" value="<?= $edi_data['Master']['ACISubMasterBillNo']['code'];?>" style="width: 80%;">
                        </div>
                    </div>

                    <div class="form-item">
                        <div class="form-lable">
                            <label>ACISubMasterBillNo[Value] : </label>
                        </div>
                        <div class="form-input">
                            <input class="easyui-textbox" name="Master[ACISubMasterBillNo][Value]" value="<?= $edi_data['Master']['ACISubMasterBillNo']['Value'];?>" style="width: 80%;">
                        </div>
                    </div>

                </div>
                <div title="RouteInformation" data-options="selected:true" style="padding:10px;">
                    <div class="master-left-routerinfo-item form-item" style="padding-bottom: 5px">
                        <div class="form-lable" style="width: 25%;">
                            <label>Vessel:</label>
                        </div>
                        <div class="form-input">
                            <select class="easyui-combogrid" name="Master[RouteInformation][ArrivalVessel][Value]"
                                    style="width: 110%;"
                                    data-options="
                    required:true,
                	idField:'value',
					textField:'value',
					panelWidth:450,
					panelHeight:345,
					pagination:true,
					mode:'remote',
					url:'/bsc_dict/get_option_limit/vessel',
                    columns:[[
                        {field:'name',title:'name',width:200},
                        {field:'ext1',title:'ext1',width:120},
                    ]],
                    value:'<?= $edi_data['Master']['RouteInformation']['ArrivalVessel']['Value']; ?>',
                    onSelect: function(index, row){
					    if(row != undefined){
                            $('#IMO').textbox('setValue', row.ext1);
					    }
					},
                ">
                            </select>
                        </div>
                    </div>
                    <div class="master-left-routerinfo-item form-item" style="padding-bottom: 5px">
                        <div class="form-lable" style="width: 25%;">
                            <label>Voyage:</label>
                        </div>
                        <div class="form-input">
                            <input class="easyui-textbox input" name="Master[RouteInformation][ArrivalVessel][voyage]"
                                   required style="width: 110%;"
                                   value="<?= $edi_data['Master']['RouteInformation']['ArrivalVessel']['voyage']; ?>">
                        </div>
                    </div>
                    <div class="master-left-routerinfo-item form-item" style="padding-bottom: 5px">
                        <div class="form-lable" style="width: 25%;">
                            <label>imo:</label>
                        </div>
                        <div class="form-input">
                            <input class="easyui-textbox input" id="IMO"
                                   name="Master[RouteInformation][ArrivalVessel][IMO]" style="width: 110%;"
                                   required
                                   value="<?= $edi_data['Master']['RouteInformation']['ArrivalVessel']['IMO']; ?>">
                        </div>
                    </div>
                    <div class="master-left-routerinfo-item  form-item">
                        <div class="form-lable form-lable-full">
                            <label>Loading Port:</label>
                        </div>
                    </div>
                    <div class="master-left-routerinfo-item port">
                        <div class="port-code port-item">
                            <select class="easyui-combobox port-select" id="LoadingPortcode" style="width: 100%;"
                                    name="Master[RouteInformation][LoadingPort][code]" required data-options="
                	valueField:'port_code',
					textField:'port_code',
                    value:'<?= $edi_data['Master']['RouteInformation']['LoadingPort']['code']; ?>',
                    onSelect: function(rec){
					    if(rec != undefined){
                            $('#LoadingPortValue').combobox('setValue', rec.port_name);
					    }
					},
                    onHidePanel: function() {
						var valueField = $(this).combobox('options').valueField;
						var val = $(this).combobox('getValue');
						var allData = $(this).combobox('getData');
						var result = true;
						for (var i = 0; i < allData.length; i++) {
							if (val == allData[i][valueField]) {
								result = false;
							}
						}
						if (result) {
							$(this).combobox('clear');
						}
					},
                ">
                            </select>
                        </div>
                        <div class="port-name port-item">
                            <select class="easyui-combobox port-select" id="LoadingPortValue" style="width: 100%;"
                                    name="Master[RouteInformation][LoadingPort][Value]" required data-options="
                	valueField:'port_name',
					textField:'port_name',
                    value:'<?= $edi_data['Master']['RouteInformation']['LoadingPort']['Value']; ?>',
                    onSelect: function(rec){
					    if(rec != undefined){
                            $('#LoadingPortcode').combobox('setValue', rec.port_code);
					    }
					},
                    onHidePanel: function() {
						var valueField = $(this).combobox('options').valueField;
						var val = $(this).combobox('getValue');
						var allData = $(this).combobox('getData');
						var result = true;
						for (var i = 0; i < allData.length; i++) {
							if (val == allData[i][valueField]) {
								result = false;
							}
						}
						if (result) {
							$(this).combobox('clear');
						}
					},
                ">
                            </select>
                        </div>
                        <div class="port-date port-item">
                            <input class="easyui-datebox " name="Master[RouteInformation][LoadingPort][date]"
                                   required style="width: 100%;"
                                   value="<?= $edi_data['Master']['RouteInformation']['LoadingPort']['date']; ?>">
                        </div>
                    </div>
                    <div class="master-left-routerinfo-item">
                        <div class="form-lable form-lable-full">
                            <label>Discharge Port:</label>
                        </div>
                    </div>
                    <div class="master-left-routerinfo-item port">
                        <div class="port-code port-item">
                            <select class="easyui-combobox port-select" id="DischargePortcode" style="width: 100%;"
                                    name="Master[RouteInformation][DischargePort][code]" required data-options="
                	valueField:'port_code',
					textField:'port_code',
                    value:'<?= $edi_data['Master']['RouteInformation']['DischargePort']['code']; ?>',
                    onSelect: function(rec){
					    if(rec != undefined){
                            $('#DischargePortValue').combobox('setValue', rec.port_name);
					    }
					},
                    onHidePanel: function() {
						var valueField = $(this).combobox('options').valueField;
						var val = $(this).combobox('getValue');
						var allData = $(this).combobox('getData');
						var result = true;
						for (var i = 0; i < allData.length; i++) {
							if (val == allData[i][valueField]) {
								result = false;
							}
						}
						if (result) {
							$(this).combobox('clear');
						}
					},
                ">
                            </select>
                        </div>
                        <div class="port-name port-item">
                            <select class="easyui-combobox port-select" id="DischargePortValue" style="width: 100%;"
                                    name="Master[RouteInformation][DischargePort][Value]" required data-options="
                	valueField:'port_name',
					textField:'port_name',
                    value:'<?= $edi_data['Master']['RouteInformation']['DischargePort']['Value']; ?>',
                    onSelect: function(rec){
					    if(rec != undefined){
                            $('#DischargePortcode').combobox('setValue', rec.port_code);
					    }
					},
                    onHidePanel: function() {
						var valueField = $(this).combobox('options').valueField;
						var val = $(this).combobox('getValue');
						var allData = $(this).combobox('getData');
						var result = true;
						for (var i = 0; i < allData.length; i++) {
							if (val == allData[i][valueField]) {
								result = false;
							}
						}
						if (result) {
							$(this).combobox('clear');
						}
					},
                ">
                            </select>
                        </div>
                        <div class="port-date port-item">
                            <input class="easyui-datebox" style="width: 100%;"
                                   name="Master[RouteInformation][DischargePort][date]" required
                                   value="<?= $edi_data['Master']['RouteInformation']['DischargePort']['date']; ?>">
                        </div>
                    </div>

                    <div class="master-left-routerinfo-item">
                        <div class="form-lable form-lable-full">
                            <label>ACIDischargePort:</label>
                        </div>
                    </div>
                    <div class="master-left-routerinfo-item port">
                        <div class="port-code port-item">
                            <select class="easyui-combogrid sub_loc input" name="Master[RouteInformation][ACIDischargePort][subcode]" style="width: 100%;" data-options="
                    required:true,
                	idField:'sub_location_code',
					textField:'sub_location_code',
					panelWidth:450,
					panelHeight:345,
					pagination:true,
					mode:'remote',
					url:'/other/get_aci_sub_loc',
                    columns:[[
                        {field:'sub_location_code',title:'sub_location_code',width:60},
                        {field:'sub_location_name',title:'sub_location_name',width:200},
                        {field:'sub_port_code',title:'sub_port_code',width:120},
                    ]],
                    value:'<?= $edi_data['Master']['RouteInformation']['ACIDischargePort']['subcode'];?>',
                    onSelect:function(index, row){
                        $('#ACIDischargePort_code').textbox('setValue', row.sub_port_code);
                        $('#ACIDischargePort_value').textbox('setValue', row.sub_location_name);
                    },
                ">

                            </select>
                        </div>
                        <div class="port-name port-item">
                            <input class="easyui-textbox input" readonly required id="ACIDischargePort_code" name="Master[RouteInformation][ACIDischargePort][code]" style="width: 100%;" value="<?= $edi_data['Master']['RouteInformation']['ACIDischargePort']['code'];?>">
                        </div>
                        <div class="port-date port-item">
                            <input class="easyui-textbox input" readonly required id="ACIDischargePort_value" name="Master[RouteInformation][ACIDischargePort][Value]" required style="width: 100%;" value="<?= $edi_data['Master']['RouteInformation']['ACIDischargePort']['Value'];?>">
                        </div>
                    </div>

                    <div class="master-left-routerinfo-item">
                        <div class="form-lable form-lable-full">
                            <label>ACIDestinationPort:</label>
                        </div>
                    </div>
                    <div class="master-left-routerinfo-item port">
                        <div class="port-code port-item">
                            <select class="easyui-combogrid sub_loc input" style="width: 100%;" name="Master[RouteInformation][ACIDestinationPort][subcode]" data-options="
                    required:true,
                	idField:'sub_location_code',
					textField:'sub_location_code',
					panelWidth:450,
					panelHeight:345,
					pagination:true,
					mode:'remote',
					url:'/other/get_aci_sub_loc',
                    columns:[[
                        {field:'sub_location_code',title:'sub_location_code',width:60},
                        {field:'sub_location_name',title:'sub_location_name',width:200},
                        {field:'sub_port_code',title:'sub_port_code',width:120},
                    ]],
                    value:'<?= $edi_data['Master']['RouteInformation']['ACIDestinationPort']['subcode'];?>',
                    onSelect:function(index, row){
                        $('#ACIDestinationPort_code').textbox('setValue', row.sub_port_code);
                        $('#ACIDestinationPort_value').textbox('setValue', row.sub_location_name);
                    },
                ">

                            </select>
                        </div>
                        <div class="port-name port-item">
                            <input class="easyui-textbox input" readonly required id="ACIDestinationPort_code" name="Master[RouteInformation][ACIDestinationPort][code]" style="width:100%;" value="<?= $edi_data['Master']['RouteInformation']['ACIDestinationPort']['code'];?>">
                        </div>
                        <div class="port-date port-item">
                            <input style="width:100%;" class="easyui-textbox input" readonly required id="ACIDestinationPort_value" required name="Master[RouteInformation][ACIDestinationPort][Value]" value="<?= $edi_data['Master']['RouteInformation']['ACIDestinationPort']['Value'];?>">
                        </div>
                    </div>


                </div>
                <div title="Master Container" data-options="selected:true" style="padding:10px;min-height:300px">
                    <div class="master-left-container">

                        <?php foreach ($edi_data['Master']['MasterContainers'] as $key => $container) { ?>
                            <div class="master-left-container-item">
                                <?= $container['ContainerNo']; ?> / <?= $container['ContainerType']; ?>
                                / <?= $container['SealNo']; ?>
                                <input type="hidden" name="Master[MasterContainers][<?= $key; ?>][ContainerNo]"
                                       value="<?= $container['ContainerNo']; ?>">
                                <input type="hidden" name="Master[MasterContainers][<?= $key; ?>][ContainerType]"
                                       value="<?= $container['ContainerType']; ?>">
                                <input type="hidden" name="Master[MasterContainers][<?= $key; ?>][SealNo]"
                                       value="<?= $container['SealNo']; ?>">
                                <!--<input type="hidden" name="Master[MasterContainers][<?/*= $key;*/?>][ServiceCode]" value="<?/*= $container['ServiceCode'];*/?>">
                                <input type="hidden" name="Master[MasterContainers][<?/*= $key;*/?>][ShipperOwned]" value="<?/*= $container['ShipperOwned'];*/?>">-->
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>

        <div data-options="region:'center'" title="Houses" style="width:70%;" >
            <div class="easyui-accordion houses" data-options="multiple:true,selected:false">


                <div title="Consolidator" data-options="selected:false" style="padding-top: 20px;padding-bottom: 20px;max-height:98%" fit="true">
                    <div class="house-row house-content " >
                        <div class="house-row-left">
                            <div class="house-row-left-label" style="width:150px">
                                <label for="" class="weight" >Consolidator</label>
                            </div>
                            <input class="easyui-combogrid company_select" id="ConsolidatorRefCode" key="0" name="Master[ConsolidatorRefCode]" client_code="EFREIGHT" style="width: 100%;" value="<?= $edi_data['Master']['ConsolidatorRefCode'];?>">
                            &nbsp;&nbsp;&nbsp;
                            <a href="javascript:void(0);" onclick="edit_company('ConsolidatorRefCode')">Edit</a>/<a href="javascript:void(0);" onclick="add_company('ConsolidatorRefCode')">Add</a>/<a href="javascript:void(0);" onclick="reload_company('ConsolidatorRefCode')">reload</a>
                        </div>
                    </div>

                    <!--新增-->
                    <div class="house-row house-content">
                        <div class="house-row-left" >
                            <div class="house-row-left-label" >
                                <label for="" style="padding-right: 33px">Address</label>
                            </div>
                            <textarea class="input_textarea " readonly id="Address_consolidator_0"  style="height:100%"><?= $ShippingParties[$edi_data['Master']['ConsolidatorRefCode']]['PartyLocation']['Address'];?></textarea>
                        </div>


                        <div class="house-row-right">
                            <div class="house-row-right-item">
                                <div class="house-row-right-item-1">
                                    <label for="">Country</label>
                                </div>
                                <div class="house-row-right-item-2 flex">
                                    <input class="easyui-textbox" readonly id="CountryCode_consolidator_0" value="<?= $ShippingParties[$edi_data['Master']['ConsolidatorRefCode']]['PartyLocation']['Country']['code'];?>" style="width: 40%">
                                    <input class="easyui-textbox" readonly id="CountryValue_consolidator_0" value="<?= $ShippingParties[$edi_data['Master']['ConsolidatorRefCode']]['PartyLocation']['Country']['Value'];?>" style="width: 60%">
                                </div>
                                <div class="house-row-right-item-3">
                                    <label for="">Postal Code</label>
                                </div>
                                <div class="house-row-right-item-4">
                                    <input class="easyui-textbox" readonly id="PostalCode_consolidator_0" value="<?= $ShippingParties[$edi_data['Master']['ConsolidatorRefCode']]['PartyLocation']['PostalCode'];?>" style="width: 100%;">

                                </div>

                            </div>
                            <div class="house-row-right-item">
                                <div class="house-row-right-item-1">
                                    <label for="">City</label>
                                </div>
                                <div class="house-row-right-item-2">
                                    <input type="hidden"  id="PartyName_consolidator_0" value="<?= $ShippingParties[$edi_data['Master']['ConsolidatorRefCode']]['PartyName'];?>">
                                    <input type="hidden" id="PartyType_consolidator_0" value="<?= $ShippingParties[$edi_data['Master']['ConsolidatorRefCode']]['PartyType'];?>">
                                    <input class="easyui-textbox" readonly id="City_consolidator_0" value="<?= $ShippingParties[$edi_data['Master']['ConsolidatorRefCode']]['PartyLocation']['City'];?>" style="width: 100%;">
                                </div>
                                <div class="house-row-right-item-3">
                                    <label for="">SubDivisionCode</label>
                                </div>
                                <div class="house-row-right-item-4">
                                    <select class="easyui-combobox input" readonly id="SubDivisionCode_consolidator_0" style="width: 100%;" data-options="
                                            valueField:'code',
                                            textField:'cityname_en',
                                            value: '<?= $ShippingParties[$edi_data['Master']['ConsolidatorRefCode']]['PartyLocation']['SubDivisionCode'];?>',
                                            <?php if(!empty($ShippingParties[$edi_data['Master']['ConsolidatorRefCode']]['PartyLocation']['Country']['code'])) echo 'url:\'/city/get_province/0/code?country=' . $ShippingParties[$edi_data['Master']['ConsolidatorRefCode']]['PartyLocation']['Country']['code'] . '\',';?>
                                            onLoadSuccess: function(){
                                                var val = $(this).combobox('getValue');
                                                $(this).combobox('clear').combobox('select', val);
                                            }
                                        ">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php foreach ($edi_data['Master']['Houses'] as $key => $House) { ?>
                    <div title="House <?= $key + 1; ?>" class="House" data-options="selected:false" style="padding-top: 20px;padding-bottom: 20px;max-height:98%" fit="true">
                        <div class="house-row house-content">
                            <div class="house-title-right">
                                <span style="padding-right: 22px;">HouseNO :</span>
                                <input class="easyui-textbox input" required readonly style="width: 200px;"
                                       name="Master[Houses][<?= $key; ?>][HouseNo]"
                                       value="<?= $House['HouseNo']; ?>">
                                <!--                                <input type="hidden"  name="Master[Houses][-->
                                <? //= $key;?><!--][OriginalHouseNo]" value="-->
                                <? //= $House['OriginalHouseNo'];?><!--">-->
                            </div>
                        </div>
                        <div class="house-row house-content" style="line-height: 20px">
                            <label for="">PlaceReceipt[code]</label>
                            <select class="easyui-combobox port-select" id="PlaceReceiptcode<?= $key; ?>"
                                    name="Master[Houses][<?= $key; ?>][PlaceReceipt][code]" required style="width: 70%;"
                                    data-options="
                                	valueField:'port_code',
                					textField:'port_code',
                                    value:'<?= $House['PlaceReceipt']['code']; ?>',
                                    onSelect: function(rec){
                					    if(rec != undefined){
                                            $('#PlaceReceiptValue<?= $key; ?>').combobox('setValue', rec.port_name);
                					    }
                					},
                                    onHidePanel: function() {
                						var valueField = $(this).combobox('options').valueField;
                						var val = $(this).combobox('getValue');
                						var allData = $(this).combobox('getData');
                						var result = true;
                						for (var i = 0; i < allData.length; i++) {
                							if (val == allData[i][valueField]) {
                								result = false;
                							}
                						}
                						if (result) {
                							$(this).combobox('clear');
                						}
                					},
                                ">
                            </select>
                            <label for="" style="padding-left: 20px">PlaceReceipt[name]</label>
                            <select class="easyui-combobox port-select" id="PlaceReceiptValue<?= $key; ?>"
                                    name="Master[Houses][<?= $key; ?>][PlaceReceipt][Value]" required
                                    style="width: 70%;"
                                    data-options="
                                	valueField:'port_name',
                					textField:'port_name',
                                    value:'<?= $House['PlaceReceipt']['Value']; ?>',
                                    onSelect: function(rec){
                					    if(rec != undefined){
                                            $('#PlaceReceiptcode<?= $key; ?>').combobox('setValue', rec.port_code);
                					    }
                					},
                                    onHidePanel: function() {
                						var valueField = $(this).combobox('options').valueField;
                						var val = $(this).combobox('getValue');
                						var allData = $(this).combobox('getData');
                						var result = true;
                						for (var i = 0; i < allData.length; i++) {
                							if (val == allData[i][valueField]) {
                								result = false;
                							}
                						}
                						if (result) {
                							$(this).combobox('clear');
                						}
                					},
                                ">
                            </select>
                            <label for="" style="padding-left: 20px">PlaceReceipt[date]</label>
                            <input class="easyui-datebox" required style="width: 70%;"
                                   name="Master[Houses][<?= $key; ?>][PlaceReceipt][date]"
                                   value="<?= $House['PlaceReceipt']['date']; ?>">
                        </div>

                        <div class="house-row house-content" style="line-height: 20px">
                            <label for="">PlaceDelivery[code]</label>
                            <select class="easyui-combobox port-select" id="PlaceDeliverycode<?= $key; ?>"
                                    name="Master[Houses][<?= $key; ?>][PlaceDelivery][code]" required
                                    style="width: 70%;"
                                    data-options="
                                	valueField:'port_code',
                					textField:'port_code',
                                    value:'<?= isset($House['PlaceDelivery']['code']) ? $House['PlaceDelivery']['code'] : ''; ?>',
                                    onSelect: function(rec){
                					    if(rec != undefined){
                                            $('#PlaceDeliveryValue<?= $key; ?>').combobox('setValue', rec.port_name);
                					    }
                					},
                                    onHidePanel: function() {
                						var valueField = $(this).combobox('options').valueField;
                						var val = $(this).combobox('getValue');
                						var allData = $(this).combobox('getData');
                						var result = true;
                						for (var i = 0; i < allData.length; i++) {
                							if (val == allData[i][valueField]) {
                								result = false;
                							}
                						}
                						if (result) {
                							$(this).combobox('clear');
                						}
                					},
                                ">
                            </select>
                            <label for="" style="padding-left: 20px">PlaceDelivery[name]</label>
                            <select class="easyui-combobox port-select" id="PlaceDeliveryValue<?= $key; ?>"
                                    name="Master[Houses][<?= $key; ?>][PlaceDelivery][Value]" required
                                    style="width: 70%;"
                                    data-options="
                                	valueField:'port_name',
                					textField:'port_name',
                                    value:'<?= isset($House['PlaceDelivery']['Value']) ? $House['PlaceDelivery']['Value'] : ''; ?>',
                                    onSelect: function(rec){
                					    if(rec != undefined){
                                            $('#PlaceDeliverycode<?= $key; ?>').combobox('setValue', rec.port_code);
                					    }
                					},
                                    onHidePanel: function() {
                						var valueField = $(this).combobox('options').valueField;
                						var val = $(this).combobox('getValue');
                						var allData = $(this).combobox('getData');
                						var result = true;
                						for (var i = 0; i < allData.length; i++) {
                							if (val == allData[i][valueField]) {
                								result = false;
                							}
                						}
                						if (result) {
                							$(this).combobox('clear');
                						}
                					},
                                ">
                            </select>
                            <label for="" style="padding-left: 20px">PlaceDelivery[date]</label>
                            <input class="easyui-datebox" required style="width: 70%;"
                                   name="Master[Houses][<?= $key; ?>][PlaceDelivery][date]"
                                   value="<?= isset($House['PlaceDelivery']['date']) ? $House['PlaceDelivery']['date'] : ''; ?>">
                        </div>


                        <div class="house-row house-content " >
                            <div class="house-row-left">
                                <div class="house-row-left-label" style="width:150px">
                                    <label for="" class="weight" >Shipper</label>
                                </div>
                                <input class="easyui-combogrid company_select" id="ShipperRefCode<?= $key;?>" key="<?= $key;?>" name="Master[Houses][<?= $key;?>][ShipperRefCode]" client_code="<?= $shipments[$House['HouseNo']]['client_code2'];?>" style="width: 100%;" value="<?= $House['ShipperRefCode'];?>">
                                &nbsp;&nbsp;&nbsp;
                                <a href="javascript:void(0);" onclick="edit_company('ShipperRefCode<?= $key;?>')">Edit</a>/<a href="javascript:void(0);" onclick="reload_company('ShipperRefCode<?= $key;?>')">Reload</a>
                            </div>
                        </div>
                        <div class="house-row house-content">
                            <div class="house-row-left" >
                                <div class="house-row-left-label" >
                                    <label for="" style="padding-right: 33px">Address</label>
                                </div>
                                <textarea class="input_textarea " readonly id="Address_shipper_<?= $key;?>" style="height:100%"><?= $ShippingParties[$House['ShipperRefCode']]['PartyLocation']['Address'];?></textarea>
                            </div>


                            <div class="house-row-right">
                                <div class="house-row-right-item">
                                    <div class="house-row-right-item-1">
                                        <label for="">Country</label>
                                    </div>
                                    <div class="house-row-right-item-2 flex">
                                        <input class="easyui-textbox" readonly id="CountryCode_shipper_<?= $key;?>" value="<?= $ShippingParties[$House['ShipperRefCode']]['PartyLocation']['Country']['code'];?>" style="width: 40%">
                                        <input class="easyui-textbox" readonly id="CountryValue_shipper_<?= $key;?>" value="<?= $ShippingParties[$House['ShipperRefCode']]['PartyLocation']['Country']['Value'];?>" style="width: 60%">
                                    </div>
                                    <div class="house-row-right-item-3">
                                        <label for="">Postal Code</label>
                                    </div>
                                    <div class="house-row-right-item-4">
                                        <input class="easyui-textbox" readonly id="PostalCode_shipper_<?= $key;?>" value="<?= $ShippingParties[$House['ShipperRefCode']]['PartyLocation']['PostalCode'];?>" style="width: 100%;">

                                    </div>

                                </div>
                                <div class="house-row-right-item">
                                    <div class="house-row-right-item-1">
                                        <label for="">City</label>
                                    </div>
                                    <div class="house-row-right-item-2">
                                        <input type="hidden" id="PartyCode_shipper_<?= $key;?>" value="<?= $ShippingParties[$House['ShipperRefCode']]['PartyCode'];?>" style="width: 100%;">
                                        <input type="hidden" id="PartyName_shipper_<?= $key;?>" value="<?= $ShippingParties[$House['ShipperRefCode']]['PartyName'];?>">
                                        <input type="hidden" id="PartyType_shipper_<?= $key;?>" value="<?= $ShippingParties[$House['ShipperRefCode']]['PartyType'];?>" style="width: 100%;">
                                        <input class="easyui-textbox" readonly id="City_shipper_<?= $key;?>" value="<?= $ShippingParties[$House['ShipperRefCode']]['PartyLocation']['City'];?>" style="width: 100%;">

                                    </div>
                                    <div class="house-row-right-item-3">
                                        <label for="">SubDivisionCode</label>
                                    </div>
                                    <div class="house-row-right-item-4">
                                        <select class="easyui-combobox input" readonly id="SubDivisionCode_shipper_<?= $key;?>" style="width: 100%;" data-options="
                                            valueField:'code',
                                            textField:'cityname_en',
                                            value: '<?= $ShippingParties[$House['ShipperRefCode']]['PartyLocation']['SubDivisionCode'];?>',
                                            <?php if(!empty($ShippingParties[$House['ShipperRefCode']]['PartyLocation']['Country']['code'])) echo 'url:\'/city/get_province/0/code?country=' . $ShippingParties[$House['ShipperRefCode']]['PartyLocation']['Country']['code'] . '\',';?>
                                            onLoadSuccess: function(){
                                                var val = $(this).combobox('getValue');
                                                $(this).combobox('clear').combobox('select', val);
                                            }
                                        ">
                                        </select>

                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="house-row house-content ">
                            <div class="house-row-left">
                                <div class="house-row-left-label" style="width:150px">
                                    <label for="" class="weight">Consignee</label>
                                </div>
                                <input class="easyui-combogrid company_select" id="ConsigneeRefCode<?= $key;?>" key="<?= $key;?>" name="Master[Houses][<?= $key;?>][ConsigneeRefCode]" client_code="<?= $shipments[$House['HouseNo']]['client_code2'];?>" style="width: 100%;" value="<?= $House['ConsigneeRefCode'];?>">
                                &nbsp;&nbsp;&nbsp;
                                <a href="javascript:void(0);" onclick="edit_company('ConsigneeRefCode<?= $key;?>')">Edit</a>/<a href="javascript:void(0);" onclick="reload_company('ConsigneeRefCode<?= $key;?>')">Reload</a>
                            </div>
                        </div>


                        <div class="house-row house-content">
                            <div class="house-row-left">
                                <div class="house-row-left-label">
                                    <label for="" style="padding-right: 33px">Address</label>
                                </div>
                                <textarea class="input_textarea" readonly id="Address_consignee_<?= $key;?>" style="height:100%"><?= $ShippingParties[$House['ConsigneeRefCode']]['PartyLocation']['Address'];?></textarea>
                            </div>


                            <div class="house-row-right">
                                <div class="house-row-right-item">
                                    <div class="house-row-right-item-1">
                                        <label for="">Country</label>
                                    </div>
                                    <div class="house-row-right-item-2 flex">
                                        <input class="easyui-textbox" readonly id="CountryCode_consignee_<?= $key;?>" value="<?= $ShippingParties[$House['ConsigneeRefCode']]['PartyLocation']['Country']['code'];?>" style="width: 40%">
                                        <input class="easyui-textbox" readonly id="CountryValue_consignee_<?= $key;?>" value="<?= $ShippingParties[$House['ConsigneeRefCode']]['PartyLocation']['Country']['Value'];?>" style="width: 60%">
                                    </div>
                                    <div class="house-row-right-item-3">
                                        <label for="">Postal Code</label>
                                    </div>
                                    <div class="house-row-right-item-4">
                                        <input class="easyui-textbox" readonly id="PostalCode_consignee_<?= $key;?>" value="<?= $ShippingParties[$House['ConsigneeRefCode']]['PartyLocation']['PostalCode'];?>" style="width: 100%;">

                                    </div>

                                </div>
                                <div class="house-row-right-item">
                                    <div class="house-row-right-item-1">
                                        <label for="">City</label>
                                    </div>
                                    <div class="house-row-right-item-2">
                                        <input type="hidden" id="PartyCode_consignee_<?= $key;?>" value="<?= $ShippingParties[$House['ConsigneeRefCode']]['PartyCode'];?>" style="width: 100%;">
                                        <input type="hidden"  id="PartyName_consignee_<?= $key;?>" value="<?= $ShippingParties[$House['ConsigneeRefCode']]['PartyName'];?>">
                                        <input type="hidden" id="PartyType_consignee_<?= $key;?>" value="<?= $ShippingParties[$House['ConsigneeRefCode']]['PartyType'];?>" style="width: 100%;">
                                        <input class="easyui-textbox" readonly id="City_consignee_<?= $key;?>" value="<?= $ShippingParties[$House['ConsigneeRefCode']]['PartyLocation']['City'];?>" style="width: 100%;">

                                    </div>
                                    <div class="house-row-right-item-3">
                                        <label for="">SubDivisionCode</label>
                                    </div>
                                    <div class="house-row-right-item-4">
                                        <select class="easyui-combobox input" readonly id="SubDivisionCode_consignee_<?= $key;?>" style="width: 100%;" data-options="
                                            valueField:'code',
                                            textField:'cityname_en',
                                            value: '<?= $ShippingParties[$House['ConsigneeRefCode']]['PartyLocation']['SubDivisionCode'];?>',
                                            <?php if(!empty($ShippingParties[$House['ConsigneeRefCode']]['PartyLocation']['Country']['code'])) echo 'url:\'/city/get_province/0/code?country=' . $ShippingParties[$House['ConsigneeRefCode']]['PartyLocation']['Country']['code'] . '\',';?>
                                            onLoadSuccess: function(){
                                                var val = $(this).combobox('getValue');
                                                $(this).combobox('clear').combobox('select', val);
                                            }
                                        ">
                                        </select>

                                    </div>

                                </div>
                            </div>


                        </div>
                        <!-- notify start -->
                        <div class="house-row house-content ">
                            <div class="house-row-left">
                                <div class="house-row-left-label" style="width:150px">
                                    <label for="" class="weight">Notify</label>
                                </div>
                                <input class="easyui-combogrid company_select" id="NotifyRefCode<?= $key;?>" key="<?= $key;?>" name="Master[Houses][<?= $key;?>][NotifyRefCode]" client_code="<?= $shipments[$House['HouseNo']]['client_code2'];?>" style="width: 100%;" value="<?= $House['NotifyRefCode'];?>">
                                &nbsp;&nbsp;&nbsp;
                                <a href="javascript:void(0);" onclick="edit_company('NotifyRefCode<?= $key;?>')">Edit</a>/<a href="javascript:void(0);" onclick="reload_company('NotifyRefCode<?= $key;?>')">Reload</a>
                            </div>
                        </div>
                        <div class="house-row house-content">
                            <div class="house-row-left">
                                <div class="house-row-left-label">
                                    <label for="" style="padding-right: 33px">Address</label>
                                </div>
                                <textarea class="input_textarea" readonly id="Address_notify_<?= $key;?>" style="height: 100%;"><?= $ShippingParties[$House['NotifyRefCode']]['PartyLocation']['Address'];?></textarea>
                            </div>


                            <div class="house-row-right">
                                <div class="house-row-right-item">
                                    <div class="house-row-right-item-1">
                                        <label for="">Country</label>
                                    </div>
                                    <div class="house-row-right-item-2 flex">
                                        <input class="easyui-textbox" readonly id="CountryCode_notify_<?= $key;?>" value="<?= $ShippingParties[$House['NotifyRefCode']]['PartyLocation']['Country']['code'];?>" style="width: 40%">
                                        <input class="easyui-textbox" readonly id="CountryValue_notify_<?= $key;?>" value="<?= $ShippingParties[$House['NotifyRefCode']]['PartyLocation']['Country']['Value'];?>" style="width: 60%">
                                    </div>
                                    <div class="house-row-right-item-3">
                                        <label for="">Postal Code</label>
                                    </div>
                                    <div class="house-row-right-item-4">
                                        <input class="easyui-textbox" readonly id="PostalCode_notify_<?= $key;?>" value="<?= $ShippingParties[$House['NotifyRefCode']]['PartyLocation']['PostalCode'];?>" style="width: 100%;">

                                    </div>

                                </div>
                                <div class="house-row-right-item">
                                    <div class="house-row-right-item-1">
                                        <label for="">City</label>
                                    </div>
                                    <div class="house-row-right-item-2">
                                        <input type="hidden" id="PartyCode_notify_<?= $key;?>" value="<?= $ShippingParties[$House['NotifyRefCode']]['PartyCode'];?>" style="width: 100%;">
                                        <input type="hidden"  id="PartyName_notify_<?= $key;?>" value="<?= $ShippingParties[$House['NotifyRefCode']]['PartyName'];?>">
                                        <input type="hidden" id="PartyType_notify_<?= $key;?>" value="<?= $ShippingParties[$House['NotifyRefCode']]['PartyType'];?>" style="width: 100%;">
                                        <input class="easyui-textbox" readonly id="City_notify_<?= $key;?>" value="<?= $ShippingParties[$House['NotifyRefCode']]['PartyLocation']['City'];?>" style="width: 100%;">

                                    </div>
                                    <div class="house-row-right-item-3">
                                        <label for="">SubDivisionCode</label>
                                    </div>
                                    <div class="house-row-right-item-4">
                                        <select class="easyui-combobox input" readonly id="SubDivisionCode_notify_<?= $key;?>" style="width: 100%;" data-options="
                                            valueField:'code',
                                            textField:'cityname_en',
                                            value: '<?= $ShippingParties[$House['NotifyRefCode']]['PartyLocation']['SubDivisionCode'];?>',
                                            <?php if(!empty($ShippingParties[$House['NotifyRefCode']]['PartyLocation']['Country']['code'])) echo 'url:\'/city/get_province/0/code?country=' . $ShippingParties[$House['NotifyRefCode']]['PartyLocation']['Country']['code'] . '\',';?>
                                            onLoadSuccess: function(data){
                                                var val = $(this).combobox('getValue');
                                                $(this).combobox('clear').combobox('select', val);
                                            }
                                        ">
                                        </select>

                                    </div>

                                </div>
                            </div>


                        </div>
                        <!-- notify end -->
                        <!-- ContainerLoadingPlace start -->
                        <div class="house-row house-content ">
                            <div class="house-row-left">
                                <div class="house-row-left-label" style="width:150px">
                                    <label for="" class="weight">ContainerLoadingPlace</label>
                                </div>
                                <input class="easyui-combogrid company_select" id="ContainerLoadingPlaceRefCode<?= $key;?>" key="<?= $key;?>" name="Master[Houses][<?= $key;?>][ContainerLoadingPlaces][ContainerLoadingPlaceRefCode]" client_code="<?= $shipments[$House['HouseNo']]['client_code2'];?>" style="width: 100%;" value="<?= $House['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode'];?>">
                                &nbsp;&nbsp;&nbsp;
                                <a href="javascript:void(0);" onclick="edit_company('ContainerLoadingPlaceRefCode<?= $key;?>')">Edit</a>/<a href="javascript:void(0);" onclick="add_company('ContainerLoadingPlaceRefCode<?= $key;?>')">Add</a>/<a href="javascript:void(0);" onclick="reload_company('ContainerLoadingPlaceRefCode<?= $key;?>')">Reload</a>
                            </div>
                        </div>
                        <div class="house-row house-content">
                            <div class="house-row-left">
                                <div class="house-row-left-label">
                                    <label for="" style="padding-right: 33px">Address</label>
                                </div>
                                <textarea class="input_textarea" readonly id="Address_containerloadingplace_<?= $key;?>" style="height: 100%;"><?= $ShippingParties[$House['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']]['PartyLocation']['Address'];?></textarea>
                            </div>


                            <div class="house-row-right">
                                <div class="house-row-right-item">
                                    <div class="house-row-right-item-1">
                                        <label for="">Country</label>
                                    </div>
                                    <div class="house-row-right-item-2 flex">
                                        <input class="easyui-textbox" readonly id="CountryCode_containerloadingplace_<?= $key;?>" value="<?= $ShippingParties[$House['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']]['PartyLocation']['Country']['code'];?>" style="width: 40%">
                                        <input class="easyui-textbox" readonly id="CountryValue_containerloadingplace_<?= $key;?>" value="<?= $ShippingParties[$House['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']]['PartyLocation']['Country']['Value'];?>" style="width: 60%">
                                    </div>
                                    <div class="house-row-right-item-3">
                                        <label for="">Postal Code</label>
                                    </div>
                                    <div class="house-row-right-item-4">
                                        <input class="easyui-textbox" readonly id="PostalCode_containerloadingplace_<?= $key;?>" value="<?= $ShippingParties[$House['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']]['PartyLocation']['PostalCode'];?>" style="width: 100%;">

                                    </div>

                                </div>
                                <div class="house-row-right-item">
                                    <div class="house-row-right-item-1">
                                        <label for="">City</label>
                                    </div>
                                    <div class="house-row-right-item-2">
                                        <input type="hidden" id="PartyCode_containerloadingplace_<?= $key;?>" value="<?= $ShippingParties[$House['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']]['PartyCode'];?>" style="width: 100%;">
                                        <input type="hidden"  id="PartyName_containerloadingplace_<?= $key;?>" value="<?= $ShippingParties[$House['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']]['PartyName'];?>">
                                        <input type="hidden" id="PartyType_containerloadingplace_<?= $key;?>" value="<?= $ShippingParties[$House['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']]['PartyType'];?>" style="width: 100%;">
                                        <input class="easyui-textbox" readonly id="City_containerloadingplace_<?= $key;?>" value="<?= $ShippingParties[$House['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']]['PartyLocation']['City'];?>" style="width: 100%;">

                                    </div>
                                    <div class="house-row-right-item-3">
                                        <label for="">SubDivisionCode</label>
                                    </div>
                                    <div class="house-row-right-item-4">
                                        <select class="easyui-combobox input" readonly id="SubDivisionCode_containerloadingplace_<?= $key;?>" style="width: 100%;" data-options="
                                            valueField:'code',
                                            textField:'cityname_en',
                                            value: '<?= $ShippingParties[$House['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']]['PartyLocation']['SubDivisionCode'];?>',
                                            <?php if(!empty($ShippingParties[$House['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']]['PartyLocation']['Country']['code'])) echo 'url:\'/city/get_province/0/code?country=' . $ShippingParties[$House['ContainerLoadingPlaces']['ContainerLoadingPlaceRefCode']]['PartyLocation']['Country']['code'] . '\',';?>
                                            onLoadSuccess: function(data){
                                                var val = $(this).combobox('getValue');
                                                $(this).combobox('clear').combobox('select', val);
                                            }
                                        ">
                                        </select>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ContainerLoadingPlace end -->
                        <div class="house-row house-content">
                            <label for="" style="line-height: 25px">ColoadInSelfFilingIndicator</label>
                            <div style="width: 100px;">
                                <select class="easyui-combobox input coin" editable="false" required name="Master[Houses][<?= $key;?>][ColoadInSelfFilingIndicator]" style="width: 80%;" data-options="
                                    value: '<?= $edi_data['Master']['Houses'][$key]['ColoadInSelfFilingIndicator']; ?>',
                                ">
                                    <option value="false">否</option>
                                    <option value="true">是</option>
                                </select>
                            </div>
                        </div>

                        <div class="house-row house-content">
                            <div class="container">
                                <div class="container-title">
                                    <div class="container-title-item">ContainerRefNo</div>
                                    <div class="container-title-item">Package</div>
                                    <div class="container-title-item">Unit</div>
                                    <div class="container-title-item">GrossWeight</div>
                                    <div class="container-title-item">CBM</div>
                                    <div class="container-title-item">CountryOrigin[code]</div>
                                    <div class="container-title-item">CountryOrigin[Value]</div>
                                    <div class="container-title-item">Marking</div>
                                    <div class="container-title-item">Description</div>
                                </div>
                                <?php if(isset($House['HouseContainers'])) foreach ($House['HouseContainers'] as $hc_k => $HouseContainer){ ?>
                                    <div class="container-body">
                                        <div class="container-item">
                                            <input class="easyui-textbox" required name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][ContainerRefNo]" style="width: 100%;" value="<?= $HouseContainer['ContainerRefNo'];?>">
                                        </div>
                                        <div class="container-item">
                                            <input class="easyui-textbox" required name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][Package]" style="width: 100%;" value="<?= $HouseContainer['Package'];?>">
                                        </div>
                                        <div class="container-item">
                                            <select class="easyui-combobox" name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][Unit]" style="width: 100%;" data-options="
                                        required: true,
                                        valueField:'name',
                                        textField:'name',
                                        value:'<?= $HouseContainer['Unit'];?>',
                                        url: '/bsc_dict/get_option/packing_unit',
                                        onHidePanel: function() {
                                            var valueField = $(this).combobox('options').valueField;
                                            var val = $(this).combobox('getValue');
                                            var allData = $(this).combobox('getData');
                                            var result = true;
                                            for (var i = 0; i < allData.length; i++) {
                                                if (val == allData[i][valueField]) {
                                                    result = false;
                                                }
                                            }
                                            if (result) {
                                                $(this).combobox('clear');
                                            }
                                        },
                                    ">
                                            </select>
                                        </div>
                                        <div class="container-item">
                                            <input class="easyui-textbox input" name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][GrossWeight]" style="width: 100%;" value="<?= $HouseContainer['GrossWeight'];?>">
                                        </div>
                                        <div class="container-item">
                                            <input class="easyui-textbox input" required name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][CBM]" style="width: 100%;" value="<?= $HouseContainer['CBM'];?>">
                                        </div>
                                        <div class="container-item">
                                            <input class="easyui-textbox input" readonly required name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][CountryOrigin][code]" style="width: 100%;" value="<?= $HouseContainer['CountryOrigin']['code'];?>">
                                        </div>
                                        <div class="container-item">
                                            <input class="easyui-textbox input" readonly required name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][CountryOrigin][Value]" style="width: 100%;" value="<?= $HouseContainer['CountryOrigin']['Value'];?>">
                                        </div>
                                        <div class="container-item">
                                            <textarea  class="input_textarea" name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][Marking]" style="width: 100%;"><?= $HouseContainer['Marking'];?></textarea>
                                        </div>
                                        <div class="container-item">
                                            <textarea  class="input_textarea" name="Master[Houses][<?= $key;?>][HouseContainers][<?= $hc_k;?>][Description]" style="width: 100%;"><?= $HouseContainer['Description'];?></textarea>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>


                    </div>
                <?php } ?>
            </div>
        </div>
</form>

<div id="window" style="display:none;">
    <iframe id="window_iframe" frameborder="0">

    </iframe>
</div>
</body>