<?php


class biz_edi_ebooking_consol_model extends CI_Model
{
    protected $table = 'biz_edi_ebooking_consol';
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_one($f = "", $v = "")
    {
    	$this->db->select("biz_edi_ebooking_consol.*,(select id from biz_consol where biz_consol.from_db = biz_edi_ebooking_consol.from_db and biz_consol.from_id_no = biz_edi_ebooking_consol.id_no limit 1) as consol_id");
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_where_one($where='', $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where, null, false);
        $this->db->order_by($sort, $order, false);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function total($where = "")
    {
        if ($where != "") $this->db->where($where, null, false);
        $this->db->from($this->table);
        return $this->db->count_all_results('');
    }

    public function save($data = array())
    {
        $data["created_time"] = date('Y-m-d H:i:s', time());
        $data["created_by"] = $this->session->userdata('id');
        $data["updated_time"] = date('Y-m-d H:i:s', time());
        $data["updated_by"] = $this->session->userdata('id');
        
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id = '', $data = array())
    {
        $data["updated_time"] = date('Y-m-d H:i:s', time());
        $data["updated_by"] = $this->session->userdata('id');
        
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id = '')
    {
        // delete container
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
}
