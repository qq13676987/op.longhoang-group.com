<script type="text/javascript" src="/inc/third/layer/layer.js">
	<script type="text/javascript" src="/inc/js/easyui/datagrid-export.js"></script>
<style type="text/css">
	.f{
		width:115px;
	}
	.s{
		width:60px;
	}
	.v{
		width:250px;
	}
	.del_tr{
		width: 23.8px;
	}
	.this_v{
		display: inline;
	}
	.more_search{
		display: none;
	}
	.all_t{
		color: blue;
		font-weight: 800;
	}
	.datagrid-btable{
		background-color: #FAFAD2;
	}
	.two{
		color:#808080;
	}
	.special_bill{
		color: red;
	}
</style>
<!--这里是查询框相关的代码-->
<script>
	var query_option = {};//这个是存储已加载的数据使用的

	var table_name = 'biz_consol',//需要加载的表名称
			view_name = 'biz_consol_index';//视图名称
	var add_tr_str = "";//这个是用于存储 点击+号新增查询框的值, load_query_box会根据接口进行加载

	/**
	 * 加载查询框
	 */
	function load_query_box(){
		ajaxLoading();
		$.ajax({
			type:'GET',
			url: '/sys_config_title/get_user_query_box?view_name=' + view_name + '&table_name=' + table_name,
			dataType:'json',
			success:function (res) {
				ajaxLoadEnd();
				if(res.code == 0){
					//加载查询框
					$('#cx table').append(res.data.box_html);
					//需要调用下加载器才行
					$.parser.parse($('#cx table tbody .query_box'));

					add_tr_str = res.data.add_tr_str;
					query_option = res.data.query_option;
					var table_columns = [[

					]];//表的表头


					//填充表头信息
					$.each(res.data.table_columns, function (i, it) {
						//读取列配置,然后这里进行合并
						var column_config;
						try {
							column_config = JSON.parse(it.column_config,function(k,v){
								if(v && typeof v === 'string') {
									return v.indexOf('function') > -1 || v.indexOf('FUNCTION') > -1 ? new Function(`return ${v}`)() : v;
								}
								return v;
							});
						}catch (e) {
							column_config = {};
						}
						var this_column_config = {field:it.table_field, title:it.title, width:it.width, sortable: it.sortable};
						//合并一下
						$.extend(this_column_config, column_config);

						if(get_function(it.table_field + '_for') !== undefined) this_column_config.formatter = get_function(it.table_field + '_for');
						if(get_function(it.table_field + '_styler') !== undefined) this_column_config.styler = get_function(it.table_field + '_styler');

						table_columns[0].push(this_column_config);
					});
					table_columns[0].push({field:'shipment_log', title:'<?= lang('shipment_log');?>', width:'50', formatter: function (value, row, index) {return "<a target='_blank' href=\"/sys_log/bind_shipment_log?id=" + row.id +"\"><img width='20' src='/inc/images/1.png'></a>";}});
					table_columns[0].push({field:'profit_percent',title:'<?=lang('利润占比')?>',width:80,align:'left'});

					//渲染表头
					$('#tt').datagrid({
						fitColumns:true,
						width:'auto',
						singleSelect:singleSelect,
						height: $(window).height() - 26,
						onRowContextMenu: function(e, index, row){
							e.preventDefault();
							$(this).datagrid('selectRow', index);
							$(this).datagrid('rowMenu').menu('show', {
								left:e.pageX,
								top:e.pageY
							});
						},
						rowStyler:function(index,row){
							//新加入,如果C3率为100%,显示灰色
							var style = '';
							if(row.trans_ATD != '0000-00-00 00:00:00') style = "color:blue";
							// if(row.normal_lock_c3 == '100%') style += 'background-color:#666666;';
							return style;
						},
						onSelect: function(index,row){
							selectVesselViewRows(row.id);
							get_status_count(row)
							get_statistics();
						},
						onUnselect: function(index,row){
							selectVesselViewRows(row.id, false);
							get_statistics();
						},
						onSelectAll: function(rows){
							$.each(rows, function(i, row){
								selectVesselViewRows(row.id);
							});
							get_statistics();
						},
						onUnselectAll: function(rows){
							$.each(rows, function(i, row){
								selectVesselViewRows(row.id, false);
							});
							get_statistics();
						},

						//添加视图,展开后显示consol
						view: detailview,
						detailFormatter:function(index,row){
							return '<div style="padding:2px"><table class="ddv"></table></div>';
						},
						onExpandRow: function(index,row){
							ajaxLoading()
							//展开子时,如果父选中了,那么自动触发选中
							var ddv = $(this).datagrid('getRowDetail',index).find('table.ddv');
							var json = {};
							var cx_data = $('#cx').serializeArray();
							$.each(cx_data, function (index, item) {
								if(json.hasOwnProperty(item.name) === true){
									if(typeof json[item.name] == 'string'){
										json[item.name] =  json[item.name].split(',');
										json[item.name].push(item.value);
									}else{
										json[item.name].push(item.value);
									}
								}else{
									json[item.name] = item.value;
								}
							});

							var this_checked = false; //获取当前行是否选中
							//获取主视图所有选中的行
							var selectRows = $(this).datagrid('getSelections');
							$.each(selectRows, function(i, it){
								if(it.id == row.id) this_checked = true;
							});
							get_status_count(row)
							ddv.datagrid({
								url:'/biz_branch_consol/get_by_pod_view_data/<?=$client_code?>/<?=$system?>?trans_destination=' + row.trans_destination + '&vessel=' + row.vessel + '&voyage=' + row.voyage + '&trans_carrier=' + row.trans_carrier + '&creditor=' + row.creditor + '&sailing_code=' + row.sailing_code,
								singleSelect:singleSelect,
								rownumbers:true,
								loadMsg:'',
								row_id:row.id,
								checkall:this_checked,
								idField: 'id',
								height:'400px',
								queryParams: json,
								remoteSort: false,
								columns:table_columns,
								onResize:function(){
									$('#tt').datagrid('fixDetailRowHeight',index);
								},
								rowStyler:function(index,row){
									var style = '';
									switch(row.status){
										case 'cancel':
											style += 'color:red;';
											break;
										case 'normal':
											style += 'color:blue;';
											break;
										case 'error':
											style += 'color:green;';
											break;
										case 'pending':
											style += 'color:orange;';
											break;
									}
									return style;
								},
								onSelect:function(index ,row){
									var options = $(this).datagrid('options');
									var p_id = options.row_id;
									var tt = $('#tt');
									var p_index = tt.datagrid('getRowIndex', p_id);
									isSelectAll = true;
									tt.datagrid('selectRow', p_index);
									isSelectAll = false;
									get_statistics();
								},
								onUnselect:function(index ,row){
									var options = $(this).datagrid('options');
									var p_id = options.row_id;
									var tt = $('#tt');
									var p_index = tt.datagrid('getRowIndex', p_id);
									isSelectAll = true;
									tt.datagrid('selectRow', p_index);
									isSelectAll = false;
									get_statistics();
								},
								onLoadSuccess:function(data){
									ajaxLoadEnd()
									var options = $(this).datagrid('options');
									var p_id = options.row_id;
									var checkall = options.checkall;

									if(checkall) selectVesselViewRows(p_id);

									setTimeout(function(){
										$('#tt').datagrid('fixDetailRowHeight',index);
									},0);
								}
							});
							$('#tt').datagrid('fixDetailRowHeight',index);

						}
					});
					$(window).resize(function () {
						$('#tt').datagrid('resize');
					});

					//为了避免初始加载时, 重复加载的问题,这里进行了初始加载ajax数据
					var aj = [];
					$.each(res.data.load_url, function (i,it) {
						aj.push(get_ajax_data(it, i));
					});
					//加载完毕触发下面的下拉框渲染
					//$_GET 在other.js里封装
					var this_url_get = {};
					$.each($_GET, function (i, it) {
						this_url_get[i] = it;
					});
					var auto_click = this_url_get.auto_click;

					$.when.apply($,aj).done(function () {
						//这里进行字段对应输入框的变动
						$.each($('.f.easyui-combobox'), function(ec_index, ec_item){
							var ec_value = $(ec_item).combobox('getValue');
							var v_inp = $('.v:eq(' + ec_index + ')');
							var s_val = $('.s:eq(' + ec_index + ')').combobox('getValue');
							var v_val = v_inp.textbox('getValue');
							//如果自动查看,初始值为空
							if(auto_click === '1') v_val = '';

							$(ec_item).combobox('clear').combobox('select', ec_value);
							//这里比对get数组里的值
							$.each(this_url_get, function (trg_index, trg_item) {
								//切割后,第一个是字段,第二个是符号
								var trg_index_arr = trg_index.split('-');
								//没有第二个参数时,默认用等于
								if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';
								//用一个删一个
								//如果当前的选择框的值,等于get的字段值
								if(ec_value === trg_index_arr[0] && s_val === trg_index_arr[1]){
									v_val = trg_item;//将v改为当前get的
									delete this_url_get[trg_index];
								}
							});
							//没找到就正常处理
							$('.v:eq(' + ec_index + ')').textbox('setValue', v_val);
						});
						//判断是否有自动查询参数
						var no_query_get = ['auto_click', 'view_type'];//这里存不需要塞入查询的一些特殊变量
						$.each(no_query_get, function (i,it) {
							delete this_url_get[it];
						});
						//完全没找到的剩余值,在这里新增一个框进行选择
						$.each(this_url_get, function (trg_index, trg_item) {
							add_tr();//新增一个查询框
							var trg_index_arr = trg_index.split('-');
							//没有第二个参数时,默认用等于
							if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';

							$('#cx table tbody tr:last-child .f').combobox('setValue', trg_index_arr[0]);
							$('#cx table tbody tr:last-child .s').combobox('setValue', trg_index_arr[1]);
							$('#cx table tbody tr:last-child .v').textbox('setValue', trg_item);
						});

						//这里自动执行查询,显示明细
						if(auto_click === '1'){
							doSearch();
						}
					});
				}else{
					$.messager.alert("<?= lang('提示');?>", res.msg);
				}
			},error:function (e) {
				ajaxLoadEnd();
				$.messager.alert("<?= lang('提示');?>", e.responseText);
			}
		});
	}
	function box_info_for(value,row,index){
		var str = '';
		try{
			var box_info_arr = $.parseJSON(value);
		}catch(e){
			var box_info_arr = [{size:'****',num:'*'}];
		}
		var box_info_text = [];
		$.each(box_info_arr, function (i, it) {
			box_info_text.push(it.size + 'x' + it.num);
		});
		str += box_info_text.join(', ');
		if(row.container_info == '港区不一致'){
			str += "<br><span style='color:red'>港区不一致</span>";
		}
		return str;
	}

	/**
	 * 新增一个查询框
	 */
	function add_tr() {
		$('#cx table tbody').append(add_tr_str);
		$.parser.parse($('#cx table tbody tr:last-child'));
		var last_f = $('#cx table tbody tr:last-child .f');
		var last_f_v = last_f.combobox('getValue');
		last_f.combobox('clear').combobox('select', last_f_v);
		return false;
	}

	/**
	 * 删除一个查询
	 */
	function del_tr(e) {
		//删除按钮
		var index = $(e).parents('tr').index();
		$(e).parents('tr').remove();
		return false;
	}

	//执行加载函数
	$(function () {
		load_query_box();
	});
</script>
<script>
	function setValue(jq, val) {
		var data = $.data(jq[0]);
		//这个页面里,只有4个
		//textbox,combobox,datebox,datetimebox
		if(data.combobox !== undefined){
			return $(jq).combobox('setValue',val);
		}else if(data.datetimebox !== undefined){
			return $(jq).datetimebox('setValue',val);
		}else if(data.datebox !== undefined){
			return $(jq).datebox('setValue',val);
		}else if(data.textbox !== undefined){
			return $(jq).textbox('setValue',val);
		}
	}

	function getValue(jq) {
		var data = $.data(jq[0]);
		//这个页面里,只有4个
		//textbox,combobox,datebox,datetimebox
		if(data.combobox !== undefined){
			return $(jq).combobox('getValue');
		}else if(data.datetimebox !== undefined){
			return $(jq).datetimebox('getValue');
		}else if(data.datebox !== undefined){
			return $(jq).datebox('getValue');
		}else if(data.textbox !== undefined){
			return $(jq).textbox('getValue');
		}
	}

	/**
	 * 根据行数据和字段直接添加到查询栏
	 */
	function add_search_field_by_row(row, field) {
		var f_data = $('.f:eq(0)').combobox('getData');
		var f_data_fields = f_data.map(function(value,index) {
			return value['value'];
		});
		//如果是f_data里的,直接判断是否存在按钮,不存在触发且让最后一个选中,然后再赋予值
		if($.inArray(field, f_data_fields) !== -1){
			//存在选中该值的,直接填充
			var f = $('.f');
			var isset = false;
			$.each(f, function (index, item) {
				var this_value = $('.f:eq(' + index + ')').combobox('getValue');
				var this_v_value = $('.v:eq(' + index + ')').textbox('getValue');
				//如果相等,给该f填充值
				if(this_value == field){
					$('.s:eq(' + index + ')').combobox('setValue', 'like');
					if(this_v_value == '')$('.v:eq(' + index + ')').textbox('setValue', row[field]);
					else $('.v:eq(' + index + ')').textbox('setValue', '');
					isset = true;
					return false;
				}
			});
			//如果没找到,新增一个
			if(!isset){
				$('.add_tr:eq(0)').trigger('click');
				$('.f:last').combobox('setValue', field);
				$('.s:last').combobox('setValue', 'like');
				$('.v:last').textbox('setValue', row[field]);
			}
		}else{
			// if(field == 'voyage') $('#voyage').textbox('setValue', row[field]);
		}
		doSearch();
		//不是f_data里的,进行特殊判断
	}

	function buildMenu(target){
		var state = $(target).data('datagrid');
		if (!state.rowMenu){
			state.rowMenu = $('<div></div>').appendTo('body');
			state.rowMenu.menu({
				onClick: function(item){
					var row = $(target).datagrid('getSelected');
					add_search_field_by_row(row, table_name + "." + item.name);
				}
			});
			//委托人、承运人、船名、航次
			var fields = ['creditor', 'trans_carrier', 'vessel', 'voyage'];
			for(var i=0; i<fields.length; i++){
				var field = fields[i];
				var col = $(target).datagrid('getColumnOption', field);
				if(col !== null){
					state.rowMenu.menu('appendItem', {
						text: col.title,
						name: field,
						iconCls: 'icon-ok'
					});
				}else{
					state.rowMenu.menu('appendItem', {
						text: field,
						name: field,
						iconCls: 'icon-ok'
					});
				}
			}
		}
		return state.rowMenu;
	}

	$.extend($.fn.datagrid.methods, {
		rowMenu: function(jq){
			return buildMenu(jq[0]);
		}
	});

	$.fn.pagination.defaults.displayMsg = '<span style="color: red;"><?=lang('总票数')?>:{total}</span> <?=lang('当前')?>:{from}<?=lang('到')?>{to} ';

	function _d(_12,_13,_14){
		var _15=$.data(_12,"datetimebox").options;
		$(_12).combo("setValue",_13);
		if(!_14){
			if(_13){
				var _16=_15.parser.call(_12,_13);
				$(_12).combo("setText",_15.formatter.call(_12,_16));
				$(_12).combo("setValue",_15.formatter.call(_12,_16));
			}else{
				$(_12).combo("setText",_13);
			}
		}
		var _16=_15.parser.call(_12,_13);
		$(_12).datetimebox("calendar").calendar("moveTo",_16);
		$(_12).datetimebox("spinner").timespinner("setValue",_17(_16));
		function _17(_18){
			function _19(_1a){
				return (_1a<10?"0":"")+_1a;
			};
			var tt=[_19(_18.getHours()),_19(_18.getMinutes())];
			if(_15.showSeconds){
				tt.push(_19(_18.getSeconds()));
			}
			return tt.join($(_12).datetimebox("spinner").timespinner("options").separator);
		};
	};

	function _e(_f){
		var _10=$.data(_f,"datetimebox").options;
		var _11=_8(_f);
		_d(_f,_10.formatter.call(_f,_11));
		$(_f).combo("hidePanel");
	};

	function _8(_9){
		var c=$(_9).datetimebox("calendar");
		var t=$(_9).datetimebox("spinner");
		var _a=c.calendar("options").current;
		return new Date(_a.getFullYear(),_a.getMonth(),_a.getDate(),t.timespinner("getHours"),t.timespinner("getMinutes"),t.timespinner("getSeconds"));
	};

	function get_status_count(row){
		if(!row) ruturn
		const { trans_origin,vessel,voyage,trans_carrier,creditor,sailing_code} = row
	}

	/**
	 * 这个特殊获取目前只支持获取ids或box_infos
	 */
	function tsGetSelections() {
		//首先获取选中的船期
		var tt = $('#tt');
		var view_rows = tt.datagrid('getSelections');
		var rows = [],table_rows,index,table_div;

		$.each(view_rows, function (i, it) {
			//获取index
			index = tt.datagrid('getRowIndex', it.id);

			table_div = tt.datagrid('getRowDetail',index).find('table.ddv');
			//如果table_div为空,那么不获取选中了 由于必定有这个div,改为下面try catch了
			if(table_div.length == 0) return true;

			//根据index获取到表格
			try {
				//直接获取数据,如果获取失败,代表未展开,那么直接获取ids
				table_rows = table_div.datagrid('getSelections');

				$.each(table_rows, function (iq, itq) {
					rows.push(itq.id);
				})
			}catch (e) {
				//获取失败,填入ids作为id
				table_rows = it.ids.split(',');

				$.each(table_rows, function (iq, itq) {
					rows.push(itq);
				})
			}
		});
		return rows;
	}

	function get_statistics(){
		var select = tsGetSelections();
		//获取到ID串了,传入后台,自动统计
		//后面改为根据ids请求后端获取统计数据
		$.ajax({
			type: 'POST',
			url: '/biz_branch_consol/get_consol_box_statistics_by_id/<?=$system?>',
			data:{
				ids: select.join(','),
			},
			dataType:'json',
			success:function (res) {
				if(res.code == 0){
					$('#box_info').textbox('setValue', res.data.box_info);
					$('#teu').text(res.data.TEU);
					$('#LCL').text(res.data.LCL);
					$('#AIR').text(res.data.AIR);
					$("#normal_count").text(res.data.status_num.normal)
					$("#cancel_count").text(res.data.status_num.cancel)
					$("#pending_count").text(res.data.status_num.pending)
					$("#error_count").text(res.data.status_num.error)
					$("#combine_into").text(res.data.status_num.combine_into)
				}
			},error:function (e) {
				alert(e.responseText);
			}
		});
		return;
		var box_info_arr = {};
		var teu = 0;
		var LCL = 0;
		var AIR = 0;
		$.each(select, function (index, item) {
			if(item.trans_mode == 'LCL'){
				LCL++;
			}else if(item.trans_mode == 'AIR'){
				AIR++;
			}
			//统计箱型箱量
			try{
				var box_info_json = JSON.parse(item.box_info);
			}catch (e) {
				var box_info_json = {};
			}
			$.each(box_info_json, function (index2, item2) {
				if(box_info_arr[item2.size] === undefined) box_info_arr[item2.size] = 0;
				box_info_arr[item2.size] += parseInt(item2.num);
			});
		});
		var box_info = [];
		$.each(box_info_arr, function (index, item) {
			box_info.push(index + '*' + item);
			if(index.substring(0,1) == '2'){
				teu += 1 * item;
			}else if(index.substring(0,1) == '4'){
				teu += 2 * item;
			}
		});
		box_info = box_info.join(', ');
		$('#box_info').textbox('setValue', box_info);
		$('#teu').text(teu);
		$('#LCL').text(LCL);
		$('#AIR').text(AIR);
	}

	/**
	 * 自动全选子根据传入的值,自动
	 */

	var isSelectAll = false;
	function selectVesselViewRows(value, selected = true){
		if(isSelectAll) return false;
		var tt = $('#tt');

		var index = tt.datagrid('getRowIndex', value);

		var table_div = tt.datagrid('getRowDetail',index).find('table.ddv');
		//如果table_div为空,那么不获取选中了 由于必定有这个div,改为下面try catch了

		//全选
		try{
			if(selected) table_div.datagrid('selectAll');
			else table_div.datagrid('unselectAll');
		}catch(e){

		}
	}

	/**
	 * 获取选中的视图对应的数据
	 */
	function getSelections() {
		//首先获取选中的船期
		var tt = $('#tt');
		var view_rows = tt.datagrid('getSelections');
		var index,table_rows,table_div,
				rows = [];

		//然后循环选中的船期,获取里面被选则的运价
		$.each(view_rows, function (i, it) {
			//获取index
			index = tt.datagrid('getRowIndex', it.id);

			table_div = tt.datagrid('getRowDetail',index).find('table.ddv');
			//如果table_div为空,那么不获取选中了 由于必定有这个div,改为下面try catch了
			if(table_div.length == 0) return true;

			//根据index获取到表格
			try {
				table_rows = table_div.datagrid('getSelections');
			}catch (e) {
				table_rows = [];
			}

			$.each(table_rows, function (iq, itq) {
				rows.push(itq);
			})
		});
		return rows;
	}

	//模板相关的函数--start
	function job_no_for(value, row, index) {
		var str = '';
		str += value;
		str += '<br /><span class="two">' + row.status + '</span>';
		if(row.is_apply_shipment != 0){
			str += '<br />已申请';
		}
		return str;
	}

	function job_no_styler(value, row, index) {
		var style = '';
		if(row.lock_lv == 3) style += 'background-color:#666666';
		return style;
	}

	function pin_for(value,row,index) {
		var str = value;

		return str;
	}
	//模板相关的函数--end

</script>
<script type="text/javascript">
	var is_submit = false;

	function doSearch(){
		var json = {};
		var cx_data = $('#cx').serializeArray();
		$.each(cx_data, function (index, item) {
			if(json.hasOwnProperty(item.name) === true){
				if(typeof json[item.name] == 'string'){
					json[item.name] =  json[item.name].split(',');
					json[item.name].push(item.value);
				}else{
					json[item.name].push(item.value);
				}
			}else{
				json[item.name] = item.value;
			}
		});
		// get_consol_market_data
		$('#tt').datagrid({
			url: '/biz_branch_consol/get_consol_pod_view_data/<?=$client_code?>/<?=$system?>',
			onError: function (index, data) {
				$.messager.alert('error', data.msg);
			},
			queryParams: json,
		}).datagrid('clearSelections');
		set_config();
		$('#chaxun').window('close');
	}

	function resetSearch(){
		$.each($('.v'), function(i,it){
			setValue($(it), '');
		});
	}

	var singleSelect = false;
	function multiple() {
		if(singleSelect){
			singleSelect = false;
		}else{
			singleSelect = true;
		}
		var tt = $('#tt');
		tt.datagrid('options').singleSelect = singleSelect;
		tt.datagrid('reload');
	}
	var more_searchs = false;
	function more_search(){
		if(more_searchs){
			$('.more_search').css('display', 'none');
		}else{
			$('.more_search').css('display', 'table-row');
		}
		more_searchs = !more_searchs;
	}

	$(function(){
		$('#plan_send_time').datetimebox({
			showSeconds:false,
			buttons:[
				{
					text:function(target){return $(target).datetimebox('options').currentText;},
					handler:function(target){
						var options = $(target).datetimebox('options');
						var end_time_target = $('#mail_remind_end_time').html();
						_d(target,options.formatter.call(target,new Date()));
						$(target).datetimebox('hidePanel');
						$('#mail_remind_end_time').datetimebox('setValue', $(target).datetimebox('getValue'));
					}
				},
				{
					text:function(target){return $(target).datetimebox('options').okText;},
					handler:function(target){
						_e(target);
						$('#mail_remind_end_time').datetimebox('setValue', $(target).datetimebox('getValue'));
					}
				},
				{
					text:function(target){return $(target).datetimebox('options').closeText;},
					handler:function(target){
						$(target).datetimebox('hidePanel');
					}
				}
			],
		});

		$('#chaxun').window('open');

		$('#cx').on('keydown', 'input', function (e) {
			if(e.keyCode == 13){
				doSearch();
			}
		});
	});

	var not_all = false;
	function reload_all_data(first_page) {
		var tt = $('#tt');
		// tt.datagrid('getO')
		var options = tt.datagrid('options');
		if(!not_all){
			tt.datagrid('gotoPage', {
				page: first_page,
				callback: function(page){
					tt.datagrid('selectAll');
					var row_data = tt.datagrid('getData');
					var this_total = (page - 1) * options.pageSize + row_data.rows.length;
					if(row_data.rows.length !== 0 && this_total < row_data.total){
						reload_all_data(page + 1);
					}
				}
			})
		}else{
			not_all = false;
		}
	}

	function get_vsl(vessel) {
		$.ajax({
			type:'GET',
			url:'/api/get_vsl?vsl=' + vessel,
			dataType: 'json',  // 请求方式为jsonp
			success:function (res) {
				if(res.DataList !== undefined && res.DataList !== null && res.DataList.length > 0){
					$('#voyage').combobox('loadData', res.DataList);
				}else{
					$('#voyage').combobox('loadData', [{'出口航次':' '}]);
				}
			},
			error:function () {

			}
		});
	}

</script>
<title>市场模块</title>
<table id="tt" rownumbers="false" pagination="true"  idField="id" pagesize="30" toolbar="#tb"  singleSelect="true" nowrap="false">
	<thead>
	<tr>
		<th title="全选" field="ck" checkbox="true"></th>
		<th data-options="field:'booking_ETD',width:'70',sortable:true"><?= lang('订舱船期');?></th>
		<th data-options="field:'trans_ATD',width:'80',sortable:true"><?= lang('ATD');?></th>
		<th data-options="field:'des_ETA',width:'80',sortable:true"><?= lang('ETA');?></th>
		<th data-options="field:'des_ATA',width:'80',sortable:true"><?= lang('ATA');?></th>
		<th data-options="field:'trans_destination',width:'80',sortable:true"><?= lang('POD');?></th>
		<th data-options="field:'vessel',width:'120',sortable:true"><?= lang('船名');?></th>
		<th data-options="field:'voyage',width:'80',sortable:true"><?= lang('航次');?></th>
		<th data-options="field:'trans_carrier_name',width:'80',sortable:true"><?= lang('船公司');?></th>
	</tr>
	</thead>
</table>

<div id="tb" style="padding:3px;">
	<table>
		<tr>
			<td>
				<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>
				<a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm2',plain:true" iconCls="icon-reload"><?= lang('config');?></a>
			</td>
		</tr>
		<tr>
			<td>
				<div class="statistics">
					box_info<input class="easyui-textbox" id="box_info" readonly style="width: 300px;" value="">
					<a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="javascript:reload_all_data(1);"><?= lang('全选');?></a>
					<a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="javascript:$('#tt').datagrid('clearSelections');not_all=true"><?= lang('全不选');?></a>
					<?= lang('总数');?>: <span class="all_t">teu</span>:<span id="teu">0</span> <span class="all_t">LCL</span>:<span id="LCL">0</span> <span class="all_t">AIR</span>:<span id="AIR">0</span>

					<span class="all_t">normal:</span><span id="normal_count">0 teu</span>
					<span class="all_t">cancel:</span><span id="cancel_count">0 teu</span>
					<span class="all_t">pending:</span><span id="pending_count">0 teu</span>
					<span class="all_t">error:</span><span id="error_count">0 teu</span>
					<span class="all_t">combine into:</span><span id="combine_into">0 teu</span>
				</div>
			</td>
		</tr>
	</table>
</div>
<div id="mm2" style="width:150px;">
	<div data-options="iconCls:'icon-ok'" onclick="config()"><?= lang('column');?></div>
	<div data-options="iconCls:'icon-ok'" onclick="javascript:multiple()"><?= lang('multiple');?></div>
</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:600px;height:450px;padding:5px;">
	<br><br>
	<form id="cx">
		<table>
			<tr class="more_search">
				<td>
					<?= lang('freight_affirmed');?>
				</td>
				<td align="right">
					<select readonly name="freight_affirmed[f]" class="easyui-combobox"  required="true" style="width:115px;">
						<option value="freight_affirmed"><?= lang('freight_affirmed');?></option>
					</select>
					&nbsp;

					<select name="freight_affirmed[s]" class="easyui-combobox"  required="true" style="width:100px;">
						<option value="=">=</option>
						<option value=">=">>=</option>
						<option value="<="><=</option>
					</select>
					&nbsp;
					<input name="freight_affirmed[v]" class="easyui-datebox"  style="width:155px;">
				</td>
			</tr>
			<tr class="more_search">
				<td>
					<?= lang('booking_confirmation');?>
				</td>
				<td align="right">
					<select readonly name="booking_confirmation[f]" class="easyui-combobox"  required="true" style="width:115px;">
						<option value="booking_confirmation"><?= lang('booking_confirmation');?></option>
					</select>
					&nbsp;

					<select name="booking_confirmation[s]" class="easyui-combobox"  required="true" style="width:100px;">
						<option value="=">=</option>
						<option value=">=">>=</option>
						<option value="<="><=</option>
					</select>
					&nbsp;
					<input name="booking_confirmation[v]" class="easyui-datebox"  style="width:155px;">
				</td>
			</tr>
			<tr class="more_search">
				<td>
					<?= lang('trans_ETD');?>
				</td>
				<td align="right">
					<select readonly name="trans_ETD[f]" class="easyui-combobox"  required="true" style="width:115px;">
						<option value="trans_ETD"><?= lang('trans_ETD');?></option>
					</select>
					&nbsp;

					<select name="trans_ETD[s]" class="easyui-combobox"  required="true" style="width:100px;">
						<option value="=">=</option>
						<option value=">=">>=</option>
						<option value="<="><=</option>
					</select>
					&nbsp;
					<input name="trans_ETD[v]" class="easyui-datebox"  style="width:155px;">
				</td>
			</tr>
			<tr class="more_search">
				<td>
					<?= lang('trans_ATD');?>
				</td>
				<td align="right">
					<select readonly name="trans_ATD[f]" class="easyui-combobox"  required="true" style="width:115px;">
						<option value="trans_ATD"><?= lang('trans_ATD');?></option>
					</select>
					&nbsp;

					<select name="trans_ATD[s]" class="easyui-combobox"  required="true" style="width:100px;">
						<option value="=">=</option>
						<option value=">=">>=</option>
						<option value="<="><=</option>
					</select>
					&nbsp;
					<input name="trans_ATD[v]" class="easyui-datebox"  style="width:155px;">
				</td>
			</tr>
			<tr class="more_search">
				<td>
					<?= lang('trans_ETA');?>
				</td>
				<td align="right">
					<select readonly name="trans_ETA[f]" class="easyui-combobox"  required="true" style="width:115px;">
						<option value="trans_ETA"><?= lang('trans_ETA');?></option>
					</select>
					&nbsp;

					<select name="trans_ETA[s]" class="easyui-combobox"  required="true" style="width:100px;">
						<option value="=">=</option>
						<option value=">=">>=</option>
						<option value="<="><=</option>
					</select>
					&nbsp;
					<input name="trans_ETA[v]" class="easyui-datebox"  style="width:155px;">
				</td>
			</tr>
			<tr class="more_search">
				<td>
					<?= lang('trans_ATA');?>
				</td>
				<td align="right">
					<select readonly name="trans_ATA[f]" class="easyui-combobox"  required="true" style="width:115px;">
						<option value="trans_ATA"><?= lang('trans_ATA');?></option>
					</select>
					&nbsp;

					<select name="trans_ATA[s]" class="easyui-combobox"  required="true" style="width:100px;">
						<option value="=">=</option>
						<option value=">=">>=</option>
						<option value="<="><=</option>
					</select>
					&nbsp;
					<input name="trans_ATA[v]" class="easyui-datebox"  style="width:155px;">
				</td>
			</tr>
			<tr class="more_search">
				<td>
					<?= lang('closing_date');?>
				</td>
				<td align="right">
					<select readonly name="closing_date[f]" class="easyui-combobox"  required="true" style="width:115px;">
						<option value="closing_date"><?= lang('closing_date');?></option>
					</select>
					&nbsp;

					<select name="closing_date[s]" class="easyui-combobox"  required="true" style="width:100px;">
						<option value="=">=</option>
						<option value=">=">>=</option>
						<option value="<="><=</option>
					</select>
					&nbsp;
					<input name="closing_date[v]" name="" class="easyui-datebox"  style="width:155px;">
				</td>
			</tr>
			<tr class="more_search">
				<td><?= lang('container_no');?></td>
				<td align="right">
					<input name="container_no" class="easyui-textbox"  style="width:245px;">
					<select class="easyui-combobox" name="goods_type" data-options="
                        editable:false,
                        valueField:'value',
                        textField:'valuename',
                        url:'/bsc_dict/get_option/goods_type',editable:false,
                    " style="width:150px;">
					</select>
				</td>
			</tr>
			<tr class="more_search">
				<td><?= lang('shipment no');?></td>
				<td align="right">
					<input name="shipment_no" class="easyui-textbox"  style="width:395px;">
				</td>
			</tr>
			<tr class="more_search">
				<td><?= lang('海运费');?></td>
				<td align="right">
					<select name="is_hyf" class="easyui-combobox" style="width:395px;">
						<option value="0">-</option>
						<option value="1"><?= lang('否');?></option>
						<option value="2"><?= lang('是');?></option>
					</select>
				</td>
			</tr>
			<tr class="more_search">
				<td><?= lang('lock_lv');?></td>
				<td align="left">
					<input name="lock_lv[]" type="checkbox" value="0"><?= lang('未锁');?>
					<input name="lock_lv[]" type="checkbox" value="1"><?= lang('1级');?>
					<input name="lock_lv[]" type="checkbox" value="2"><?= lang('2级');?>
					<input name="lock_lv[]" type="checkbox" value="3"><?= lang('3级');?>
				</td>
			</tr>
			<tr class="more_search">
				<td><?= lang('仓位申请');?></td>
				<td align="left">
					<input type="radio" name="is_apply_shipment" value="0" checked><?= lang('否');?>
					<input type="radio" name="is_apply_shipment" value="1"><?= lang('是');?>
				</td>
			</tr>
		</table>
		<br><br>
		<button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="doSearch();"><?= lang('search'); ?></button>
		<button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="resetSearch();"><?= lang('重置');?></button>
		<a href="javascript:void;" class="easyui-tooltip" data-options="
                content: $('<div></div>'),
                onShow: function(){
                    $(this).tooltip('arrow').css('left', 20);
                    $(this).tooltip('tip').css('left', $(this).offset().left);
                },
                onUpdate: function(cc){
                    cc.panel({
                        width: 500,
                        height: 'auto',
                        border: false,
                        href: '/bsc_help_content/help/query_condition'
                    });
                }
            "><?php //echo lang('help');?></a>
		<a href="javascript:void(0)" id="more_field" onclick="javascript:more_search();"> more</a>
	</form>
</div>
