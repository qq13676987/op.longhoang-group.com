<body >
<div >
<table class="easyui-datagrid" id="tt"  rownumbers="false" pagination="false" idField="id"
        singleSelect="true" nowrap="false"  fitColumns="true" height="1000px" >
    <thead>
    <tr>
        <th data-options="field:'id',width:80,align:'center'">Shipment_id</th>
        <th data-options="field:'job_no',width:80,align:'center'">ShipmentNO</th>
        <th data-options="field:'trans_ATD',width:80,align:'center'">实际离泊</th>
        <th data-options="field:'client_company',width:80,align:'center'">委托方名称</th>
        <th data-options="field:'user_group',width:80,align:'center'">销售部门</th>
        <th data-options="field:'user_name',width:80,align:'center'">销售人员</th>
    </tr>
    </thead>

</table>
</div>
</body>
<script>
    $('#tt').datagrid({
        url:'/biz_crm/crm_set_details_client_get_data/<?=$group?>/<?=$type?>?date_s=<?=$date_s?>&date_e=<?=$date_e?>',
    });
</script>