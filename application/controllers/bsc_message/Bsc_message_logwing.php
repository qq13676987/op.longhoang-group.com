<?php

/**
 * ESL 报文相关
 */
class bsc_message_logwing extends Common_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bsc_message_model');
        $this->model = $this->bsc_message_model;
    }

    public $good_outers_unit_CODE = array(
        'CARRIERS' => 'CRGS',
        'CARTON BOXES' => 'CBOXS',
        'SHRINKWRAPPES' => 'WRAPS',
        'DISCS, DISKS' => 'DSCS',
        'STEEL CASES' => 'STEELS',
        'DOSES' => 'DOSS',
        'STACKS' => 'STAS',
        'TAILS' => 'TALS',
        'TABLETS' => 'TABS',
        'SUITS' => 'SUTS',
        'STRINGS' => 'STRS',
        'STICKS' => 'STKS',
        'SKINS' => 'SKNS',
        'RUNNERS' => 'RUNS',
        'PANCAKES' => 'PCKS',
        'WEBS' => 'WEBS',
        'CUTS' => 'CUTS',
        'CATTIES' => 'CTYS',
        'CASKETS' => 'CSKS',
        'CARTRIDGES' => 'CATS',
        'CARDS' => 'CRDS',
        'CAKES' => 'CAKS',
        'BUSHELS' => 'BUAS',
        'BRITISH GALLONS' => 'BGAS',
        'BOMBS' => 'BOMS',
        'BOARD MEASURE FOOTS' => 'BMFS',
        'BOARD FOOTS' => 'BFTS',
        'BLADES' => 'BLAS',
        'BIRDS' => 'BRDS',
        'BILLETS' => 'BLTS',
        'BEAMS' => 'BEMS',
        'SLABS' => 'SLBS',
        'POLYBAGS' => 'POLS',
        'PANELS' => 'PNLS',
        'FLECON BAGS' => 'FLECONS',
        'NUTS' => 'NUTS',
        'METERS' => 'MTRS',
        'LITERS' => 'LTRS',
        'LINKS' => 'LNKS',
        'HANKS' => 'HANS',
        'FOOTS' => 'FOTS',
        'FIBERITES' => 'FIBS',
        'GALLONS' => 'GLIS',
        'RACK, CLOTHING HANGER' => 'RJ',
        'PALLET' => 'PX',
        'PIPES, IN BUNDLE/BUNCH/TRUSS' => 'PV',
        'UNPACKED UNPACKAGED, SINGLE UNIT' => 'NF',
        'INTERMEDIATE BULK CONTAINER, RIGID PLASTIC' => 'AA',
        'BARE' => 'BRE',
        'TRUSSES' => 'TRU',
        'HEAD' => 'HED',
        'CARCASE' => 'CAR',
        'KILOGRAM' => 'KGM',
        'INCHES' => 'INH',
        'GROSS' => 'GRO',
        'EACH' => 'EAC',
        'DOZENS' => 'DEN',
        'DRY BULK' => 'DBK',
        'COPY' => 'CPY',
        'CARBOY' => 'CBY',
        'CONTAINER BULK CARGO' => 'CBC',
        'KIT' => 'KIT',
        'SHORT/TONS' => 'STN',
        'REAM' => 'REM',
        'PAIRS' => 'PST',
        'P.P.BAGS' => 'PPB',
        'POLY/BAGS' => 'POL',
        'PAPER/DRUMS' => 'P/D',
        'LUG' => 'LUG',
        'LIFTS' => 'LFT',
        'KILO/TON' => 'KTN',
        'VOLUME' => 'VOL',
        'VAN' => 'VAN',
        'TANKS' => 'TNK',
        'METRIC/TON' => 'TNE',
        'STRIP' => 'STP',
        'NOT DEFINED' => 'ZZZ',
        'YARD' => 'YRD',
        'PLYWOOD CASES' => 'WPC',
        'GRAMS' => 'GR',
        'DOZENS' => 'DZ',
        'BULKS' => 'BKS',
        'CONTAINER, OUTER' => 'OU',
        'CONTAINER, METAL' => 'ME',
        'CONTAINER, FLEXIBLE' => '1F',
        'COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN WOODEN BOX' => 'YF',
        'COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN STEEL DRUM' => 'YA',
        'COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN STEEL CRATE BOX' => 'YB',
        'COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN SOLID PLASTIC BOX' => 'YM',
        'COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN PLYWOOD DRUM' => 'YG',
        'COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN PLYWOOD BOX' => 'YH',
        'COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN PLASTIC DRUM' => 'YL',
        'COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN FIBREBOARD BOX' => 'YK',
        'COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN FIBRE DRUM' => 'YJ',
        'COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN ALUMINIUM DRUM' => 'YC',
        'COMPOSITE PACKAGING, PLASTIC RECEPTACLE IN ALUMINIUM CRATE' => 'YD',
        'COMPOSITE PACKAGING, PLASTIC RECEPTACLE' => '6H',
        'COMPOSITE PACKAGING, GLASS RECEPTACLE IN WOODEN BOX' => 'YS',
        'COMPOSITE PACKAGING, GLASS RECEPTACLE IN WICKERWORK HAMPER' => 'YV',
        'COMPOSITE PACKAGING, GLASS RECEPTACLE IN STEEL DRUM' => 'YN',
        'COMPOSITE PACKAGING, GLASS RECEPTACLE IN STEEL CRATE BOX' => 'YP',
        'COMPOSITE PACKAGING, GLASS RECEPTACLE IN SOLID PLASTIC PACK' => 'YZ',
        'COMPOSITE PACKAGING, GLASS RECEPTACLE IN PLYWOOD DRUM' => 'YT',
        'COMPOSITE PACKAGING, GLASS RECEPTACLE IN FIBREBOARD BOX' => 'YX',
        'COMPOSITE PACKAGING, GLASS RECEPTACLE IN FIBRE DRUM' => 'YW',
        'COMPOSITE PACKAGING, GLASS RECEPTACLE IN EXPANDABLE PLASTIC PACK' => 'YY',
        'COMPOSITE PACKAGING, GLASS RECEPTACLE IN ALUMINIUM DRUM' => 'YQ',
        'COMPOSITE PACKAGING, GLASS RECEPTACLE IN ALUMINIUM CRATE' => 'YR',
        'COMPOSITE PACKAGING, GLASS RECEPTACLE' => '6P',
        'CLAMSHELL' => 'AI',
        'CASE, WITH PALLET BASE, WOODEN' => 'EE',
        'CASE, WITH PALLET BASE, PLASTIC' => 'EG',
        'CASE, WITH PALLET BASE, METAL' => 'EH',
        'CASE, WITH PALLET BASE, CARDBOARD' => 'EF',
        'CASE, WITH PALLET BASE' => 'ED',
        'CASE, ISOTHERMIC' => 'EI',
        'CASE, CAR' => '7A',
        'CART, FLATBED' => 'FW',
        'CAN, WITH HANDLE AND SPOUT' => 'CD',
        'CAPSULE' => 'AV',
        'CAGE, ROLL' => 'CW',
        'CAGE, COMMONWEALTH HANDLING EQUIPMENT POOL (CHEP)' => 'DG',
        'BUNDLE, WOODEN' => '8C',
        'BULK, SCRAP METAL' => 'VS',
        'BOX, WOODEN, NATURAL WOOD, WITH SIFT PROOF WALLS' => 'QQ',
        'BOX, STEEL' => '4A',
        'BOX, RECONSTITUTED WOOD' => '4F',
        'BOX, PLYWOOD' => '4D',
        'BOX, PLASTIC, SOLID' => 'QS',
        'BOX, PLASTIC, EXPANDED' => 'QR',
        'BOX, PLASTIC' => '4H',
        'BOX, NATURAL WOOD' => '4C',
        'BOX, FIBREBOARD' => '4G',
        'BOX, COMMONWEALTH HANDLING EQUIPMENT POOL (CHEP), EUROBOX' => 'DH',
        'BOX, ALUMINIUM' => '4B',
        'BELT' => 'B4',
        'BASKET, WITH HANDLE, WOODEN' => 'HB',
        'BASKET, WITH HANDLE, PLASTIC' => 'HA',
        'BASKET, WITH HANDLE, CARDBOARD' => 'HC',
        'BASIN' => 'BM',
        'BARREL, WOODEN, REMOVABLE HEAD' => 'QJ',
        'BARREL, WOODEN, BUNG TYPE' => 'QH',
        'BARREL, WOODEN' => '2C',
        'BALL' => 'AL',
        'BAG, WOVEN PLASTIC, WITHOUT INNER COAT/LINER' => 'XA',
        'BAG, WOVEN PLASTIC, WATER RESISTANT' => 'XC',
        'BAG, WOVEN PLASTIC, SIFT PROOF' => 'XB',
        'BAG, WOVEN PLASTIC' => '5H',
        'BAG, TEXTILE, WITHOUT INNER COAT/LINER' => 'XF',
        'BAG, TEXTILE, WATER RESISTANT' => 'XH',
        'BAG, TEXTILE, SIFT PROOF' => 'XG',
        'BAG, TEXTILE' => '5L',
        'BAG, SUPER BULK' => '43',
        'BAG, POLYBAG' => '44',
        'BAG, PLASTICS FILM' => 'XD',
        'BAG, PLASTIC' => 'EC',
        'BAG, PAPER, MULTI-WALL, WATER RESISTANT' => 'XK',
        'BAG, PAPER, MULTI-WALL' => 'XJ',
        'BAG, LARGE' => 'ZB',
        'BAG, JUMBO' => 'JB',
        'BAG, FLEXIBLE CONTAINER' => 'FX',
        'WOODEN FLOOR' => 'WO',
        'WOODEN PIECE' => 'WE',
        'TOTE BIN' => 'TT',
        'NUMBERS' => 'NO',
        'NIL' => 'NI',
        'MASTER BAG' => 'MA',
        'LOOSE' => 'LS',
        'LENGHT(S)' => 'LE',
        'KRAFT BAG' => 'KR',
        'ITEM' => 'IT',
        'FLEXIBAG' => 'FB',
        'ARTICLES' => 'AR',
        'UNITS' => 'UN',
        'PLYWOOD PALLETS' => 'PW',
        'PIECES' => 'PS',
        'GUNNY BALES' => 'GL',
        'GUNNY BAGS' => 'GG',
        'VAN PACK' => 'VK',
        'BAGS' => 'BG',
        'AMPOULES, PROTECTED' => 'AP',
        'WOODEN PALLET' => 'WP',
        'WOODEN PACKAGE' => 'WK',
        'BULK,LIQUEFIED GAS(AT ABNORMAL TEMPERATURE/PRESSURE' => 'VQ',
        'BUTT' => 'UT',
        'TRAY' => 'TA',
        'SPOOL' => 'SO',
        'SKELETON CASES' => 'SK',
        'SACHETS' => 'SH',
        'SEABULK BAG' => 'SB',
        'RACK' => 'RK',
        'PIPES,IN BUNDLE/BUNCH/TRUSS' => 'PZ',
        'TRAYS' => 'PU',
        'PLASTIC DRUM' => 'PR',
        'PLATES' => 'PG',
        'PACK' => 'PD',
        'BASKET' => 'KB',
        'JUTEBAGS' => 'JT',
        'INGOTS' => 'IN',
        'INTERMEDIATE BULK CONTAINER' => 'IB',
        'FLEXITANK' => 'FT',
        'FILMPACKS' => 'FP',
        'WOODEN PACKAGING' => 'WG',
        'WOODEN WOOD' => 'WW',
        'WOODEN SLING' => 'WS',
        'WOODEN CRATES' => 'WR',
        'WOODEN MATERIAL' => 'WM',
        'AMPOULES, NON-PROTECTED' => 'BS',
        'WOODEN FRAME' => 'WF',
        'WOODEN BUNDLES' => 'WD',
        'WOODEN BOXES' => 'WX',
        'WOODEN CASES' => 'WC',
        'WICKERBOTTLES' => 'WB',
        'PACKETS' => 'PA',
        'CONTAINERS' => 'NT',
        'NESTS' => 'NS',
        'MATCH BOXES' => 'MX',
        'MATS' => 'MT',
        'MULTIWALL SACKS' => 'MS',
        'MILK CRATES' => 'MC',
        'MULTIPLY BAGS' => 'MB',
        'CYLINDERS' => 'CY',
        'CARBOYS,NON-PROTECTE' => 'CO',
        'CAN' => 'CM',
        'CREELS' => 'CE',
        'BOTTLES,NON-PROTECTED, CYL-INDRICA' => 'BO',
        'BOTTLE CRATES, BOTTLERACK' => 'BC',
        'ATOMIZERS' => 'AT',
        'BOARDS,IN BUNDLE/BUNCH/TRUSS' => 'BY',
        'UNPACKED OR UNPACKAGED' => 'NE',
        'WOODEN PACK' => 'WA',
        'BULK,SOLID, GRANULAR PARTICLES(GRAINS)' => 'VR',
        'VACUUMPACKED' => 'VP',
        'BULK, LIQUID,LOOSE' => 'VL',
        'VIALS' => 'VI',
        'BULK,GAS(AT 1031 MBAR AND 15/C)' => 'VG',
        'VATS' => 'VA',
        'TANKS,CYLINDRICAL' => 'TY',
        'TUBES' => 'TU',
        'TRUSS' => 'TS',
        'TRUNKS' => 'TR',
        'TUNS' => 'TO',
        'TINS' => 'TN',
        'TANKS,RECTANGULAR' => 'TK',
        'COLLAPSIBLE TUBES' => 'TD',
        'TEA-CHESTS' => 'TC',
        'TUBS' => 'TB',
        'SHEETS, IN BUNDLE/BUNCH/TRUSS' => 'SZ',
        'SHRINKWRAPPED' => 'SW',
        'SUITCASES' => 'SU',
        'SHEETS' => 'ST',
        'SHEETMETALS' => 'SM',
        'SLIPSHEETS' => 'SL',
        'SEA-CHESTS' => 'SE',
        'SPINDLES' => 'SD',
        'SHALLOW CRATES' => 'SC',
        'SACKS' => 'SA',
        'REDNETS' => 'RT',
        'ROLLS' => 'RO',
        'REELS' => 'RL',
        'RINGS' => 'RG',
        'RODS' => 'RD',
        'PLATES, IN BUNDLE/BUNCH/TRUSS' => 'PY',
        'POTS' => 'PT',
        'POUCHES' => 'PO',
        'PLANKS' => 'PN',
        'PAILS' => 'PL',
        'PIPES' => 'PI',
        'PITCHERS' => 'PH',
        'PALLETS' => 'PE',
        'PARCELS' => 'PC',
        'PAPER BAGS' => 'PB',
        'LIFT VANS' => 'LV',
        'LOTS' => 'LT',
        'LOGS' => 'LG',
        'KEGS' => 'KG',
        'JARS' => 'JR',
        'JUGS' => 'JG',
        'JERRICANS,RECTANGULAR' => 'JC',
        'BULK, SOLID, FINE PARTICLES(POWDERS)' => 'VY',
        'BULK, SOLID, LARGE PARTICLES(NODULES)' => 'VO',
        'TUBES, IN BUNDLE/BUNCH/TRUSS' => 'TZ',
        'SETS' => 'SX',
        'SKIDS' => 'SI',
        'RODS,IN BUNDLE/BUNCH/TRUSS' => 'RZ',
        'LOGS,IN BUNDLE/BUNCH/TRUSS' => 'LZ',
        'JERRICANS,CYLINDRICAL' => 'JY',
        'INGOTS,IN BUNDLE/BUNCH/TRUSS' => 'IZ',
        'IRON DRUMS' => 'ID',
        'PACKAGES' => 'PK',
        'PACKAGE' => 'PK',
        'HAMPERS' => 'HR',
        'HOGSHEADS' => 'HG',
        'GIRDERS' => 'GI',
        'GAS BOTTLES' => 'GB',
        'FRAMES' => 'FR',
        'FOOTLOCKERS' => 'FO',
        'FLASKS' => 'FL',
        'FIRKINS' => 'FI',
        'FRAMED CRATES' => 'FD',
        'FRUIT CRATES' => 'FC',
        'ENVELOPES' => 'EN',
        'DRUMS' => 'DR',
        'CANVAS' => 'CZ',
        'COVERS' => 'CV',
        'CARTONS' => 'CT',
        'CASES' => 'CS',
        'CRATES' => 'CR',
        'CARBOYS,PROTECTED' => 'CP',
        'CONES' => 'CN',
        'COILS' => 'CL',
        'CASKS' => 'CK',
        'COFFINS' => 'CJ',
        'CANISTERS' => 'CI',
        'CHESTS' => 'CH',
        'CAGES' => 'CG',
        'COFFERS' => 'CF',
        'CHURNS' => 'CC',
        'BEER CRATES' => 'CB',
        'CANS,RECTANGULAR' => 'CA',
        'BUTTS' => 'BU',
        'BARS' => 'BR',
        'BOTTLES,PROTECTED CYLINDRICA' => 'BQ',
        'BALES' => 'BL',
        'BASKETS' => 'BK',
        'BUCKETS' => 'BJ',
        'BINS' => 'BI',
        'BUNCHES' => 'BH',
        'BALLONS, NON-PROTECTED' => 'BF',
        'BUNDLES' => 'BE',
        'BOARDS' => 'BD',
        'BOBBINS' => 'BB',
        'BARRELS' => 'BA',
        'AEROSOLS' => 'AE',
        'CUPS' => 'CU',
        'GIRDERS,IN BUNDLE/BUNCH/TRUSS' => 'GZ',
        'DEMIJOHNS,PROTECTED' => 'DP',
        'DEMIJOHNS,NON-PROTECTED' => 'DJ',
        'CANS,CYLINDRICAL' => 'CX',
        'CANS' => 'CQ',
        'BARS,IN BUNDLE/BUNCH/TRUSS' => 'BZ',
        'BLOCKS' => 'BW',
        'BOTTLES,PROTECTED BULBOUS' => 'BV',
        'BALLONS,PROTECTED' => 'BP',
        'BALES,NON-COMPRESSED' => 'BN',
        'BOXES' => 'BX',
        'BOLTS' => 'BT',
        'AMPOULES, NON-PROTECTED' => 'AM',
    );

    public function message_so($consol_id=0){
        $this->load->model('biz_consol_model');
        $this->load->model('biz_shipment_model');
        $this->load->model('biz_port_model');
        $this->load->model('bsc_dict_model');
        $this->load->model('m_model');
        //获取或转换对应的数据--start
        $consol = $this->biz_consol_model->get_one('id', $consol_id);
        $shipments = $this->biz_shipment_model->no_role_get("consol_id =  '" . $consol['id'] . "'");

        foreach($consol as $key=>$value){
            if($key=="box_info") continue;
            $value = str_replace("?","??",$value);
            $value = str_replace("'","?'",$value);
            $value = str_replace(":","?:",$value);
            $value = str_replace("+","?+",$value);
            $consol[$key] = $value;
        }
        foreach($shipments as $key=>$value){
            if($key=="box_info") continue;
            $value = str_replace("?","??",$value);
            $value = str_replace("'","?'",$value);
            $value = str_replace(":","?:",$value);
            $value = str_replace("+","?+",$value);
            $shipments[$key] = $value;
        }


        if(empty($consol)){
            $result['msg'] = lang('consol empty');
            echo json_encode($result);
            return;
        }
        if(empty($shipments)){
            $result['msg'] = lang('shipment empty');
            echo json_encode($result);
            return;
        }
        //获取港口全称--start
        $trans_origin_port = $this->biz_port_model->get_one('port_code', $consol['trans_origin']);
        if(empty($trans_origin_port)){
            $result['msg'] = lang('trans_origin');
            echo json_encode($result);
            return;
        }
        $trans_origin_name = $trans_origin_port['port_name'];

        //DISCHARGE PORT CODE' => '',是POD， 7 单元是POD 全名，TRANSFER PORT CODE是第一中转港代码，
        //PLACE OF DELIVERY CODE是交货地代码，11单元是交货地全名， 参看以下我修改的范例
        $trans_destination_port = $this->biz_port_model->get_one('port_code', $consol['trans_destination']);
        if(empty($trans_destination_port)){
            $result['msg'] = lang('trans_destination');
            echo json_encode($result);
            return;
        }
        $trans_destination_name = $trans_destination_port['port_name'];
        $trans_discharge_port = $this->biz_port_model->get_one('port_code', $consol['trans_discharge']);
        if(empty($trans_discharge_port)){
            $result['msg'] = lang('trans_discharge');
            echo json_encode($result);
            return;
        }
        $trans_discharge_name = $trans_discharge_port['port_name'];
        //获取港口全称--end

        //转换 数量单位 为指定code
        $good_outers = 0;
        $good_weight = 0;
        $good_volume = 0;
        foreach ($shipments as $shipment){
            $good_outers += $shipment['good_outers'];
            $good_weight += $shipment['good_weight'];
            $good_volume += $shipment['good_volume'];
        }
        $good_outers_unit = $shipment['good_outers_unit'];
        //查询字典表获取该code
        $iso_code = $this->bsc_dict_model->get_one('name', $good_outers_unit, 'packing_unit');
        if(!empty($iso_code)){
            $good_outers_unit_CODE = $iso_code['value'];
        }else{
            $result['msg'] = lang('good_outers_unit_code');
            echo json_encode($result);
            return;
        }


        $date = date("ymd");
        $time = date("Hi");
        $job_no = $consol["job_no"];
        $edistr = "UNB+UNOC:2+EH2F:ZZZ+LOGWING:UN+$date:$time+$job_no'\r\n";
        $edistr .="UNH+$job_no+IFTMBF:D:99B:UN'\r\n";
        $edistr .="BGM+335+$job_no+9'\r\n";
        $edistr .="DTM+137:20$date:$time:203'\r\n";

        //2023-04-23 高丽要求改为 截单备注
        $apply_remark=$consol["requirements"]; //订舱备注
        $edistr .="FTX+AAI+++$apply_remark'\r\n";

        //业务编号，这里可以加AP CODE, 合约编号，具体见报文
        $edistr .="RFF+FF:$job_no'\r\n";
        $box_info = $consol["box_info"];
        $box_info = json_decode($box_info,true);
        //print_r($box_info);exit();
        if(empty($box_info)){
            $result['msg'] = "box_info zero";
            echo json_encode($result);
            return;
        }
        $box_new = array_column($box_info,"num");
        $num = array_sum($box_new); //总柜数
        //echo $num; exit();

        $edistr .="CNT+7:$good_weight:KGM'\r\nCNT+11:$good_outers'\r\nCNT+15:$good_volume:MTQ'\r\nCNT+16:$num'\r\n";

        $trans_term=$shipment["trans_term"]; // 对应的代码
        $code_arr = array('HOOK-HOOK'=>'H1',
            'FOR-CY'=>'F5',
            'CY-FOR'=>'C17',
            'CFS-LCL'=>'C16',
            'CY-HOOK'=>'C15',
            'FREE IN-HOOK'=>'F4',
            // 'FREE IN-FREE OUT'=>'F3',
            //2022-05-26 FO = FREE OUT
            'CY-FO'=>'F3',
            'DOOR-RAMP'=>'D10',
            'TACKLE-TACKLE'=>'T4',
            'TACKLE-CY'=>'T1',
            'LINER IN-DOOR'=>'L2',
            'LINER IN-CY'=>'L1',
            'FREE IN-DOOR'=>'F2',
            'FREE IN-CY'=>'F1',
            'DOOR-LINER OUT'=>'D5',
            'DOOR-FREE OUT'=>'D4',
            'DOOR-DOOR'=>'D3',
            'DOOR-CFS'=>'D2',
            'DOOR-CY'=>'D1',
            'CY-FIOS'=>'C14',
            'CY-ICD'=>'C13',
            'CY-RAMP'=>'C10',
            'CY-TACKLE'=>'C9',
            'CY-LINER OUT'=>'C8',
            'CY-FREE OUT'=>'C7',
            'CFS-DOOR'=>'C6',
            'CFS-CFS'=>'C5',
            'CFS-CY'=>'C4',
            'CY-DOOR'=>'C3',
            'CY-CFS'=>'C2',
            'CY-CY'=>'C1',);
        if(!isset($code_arr[$trans_term])){
            $result['msg'] = "运输条款 {$trans_term} 无对应值";
            return jsonEcho($result);
        }

        $trans_term = $code_arr[$trans_term];

        $edistr .="TSR+$trans_term+2'\r\n";

        /*
        提单类型打包方式：
        正本固定按如下打包:
        DOC+706+:26++3'
        DOC+707+:26++3'
        电放请按如下固定打包：
        DOC+706+:26++0'
        DOC+707+:26++1'
        Seaway bill（海运单）：
        DOC+710+:26++1'
        */
        $release_type = $shipment["release_type"];
        if($release_type=="OBL"){
            $edistr .="DOC+706+:26++3'\r\nDOC+707+:26++3'\r\n";
        }
        if($release_type=="TER"){
            $edistr .="DOC+706+:26++0'\r\nDOC+707+:26++1'\r\n";
        }
        if($release_type=="SWB"){
            $edistr .="DOC+710+:26++1'\r\n";
        }

        $payment = $consol["payment"];
        if($payment=="PP") $payment="P";
        if($payment=="CC") $payment="C";
        if($payment=="E") $payment="A";
        $edistr .="CPI+4++$payment'\r\n";

        $trans_origin=$consol["trans_origin"];
        $edistr .="LOC+57+$trans_origin::6:$trans_origin_name'\r\n";

        $vessel = $consol["vessel"];
        $voyage = $consol["voyage"];
        $trans_carrier = $consol["trans_carrier"];
        $code_arr = array('ZYJZXY01'=>'COSU',
            'MCCYSX01'=>'MCCQ',
            'MSJZGH01'=>'MAEU',
            '美国总统轮船'=>'APLU',
            'FGDFLC01'=>'CMDU',
            'RSDZHH01'=>'MSCU',
            'YMHYGF01'=>'YMLU',
            'TPCWYX01'=>'PABV',
            'HBNMHY01'=>'SUDU',
            'DFHWHG01'=>'OOLU',
            'XDSCZG01'=>'HDMU',
            'HBLTCW01'=>'HLCU',
            'DXHYYX02'=>'TLWN',
            'YXZHHY01'=>'ZIMU',
            'HHXYYX01'=>'REGU',
            'YHHYYX01'=>'ASL',
        );

        if(empty($code_arr[$trans_carrier])){
            $result['msg'] = "no carrier code";
            echo json_encode($result);
            return;
        }

        if(empty($voyage)){
            $result['msg'] = "no voyage";
            echo json_encode($result);
            return;
        }

        $trans_carrier = $code_arr[$trans_carrier];
        $edistr .="TDT+20+$voyage+1++$trans_carrier:172+++:::$vessel'\r\n";

        $booking_ETD=$consol["booking_ETD"];
        $booking_ETD = str_replace("-","",$booking_ETD);
        $edistr .="DTM+133:$booking_ETD:102'\r\n";

        //；88=收货地； 9=起运港；8=中转港；11=卸货港；7=目的地
        $trans_discharge = $consol["trans_discharge"];
        $trans_destination = $consol["trans_destination"];
        $edistr .="LOC+88+$trans_origin::6:$trans_origin_name'\r\nLOC+9+$trans_origin::6:$trans_origin_name'
LOC+11+$trans_discharge::6:$trans_discharge_name'\r\nLOC+7+$trans_destination::6:$trans_destination_name'\r\n";

        //CN=收货方	CZ=发货方	NI=通知方	CA=船东	HI=发送方	FW=货代
        $shipper_company = strtoupper($consol["shipper_company"]);
        $shipper_address = strtoupper($consol["shipper_address"]);
        $shipper_address = preg_replace("/\s(?=\s)/","\\1",$shipper_address); //多空格合并成1个
        $shipper_address = str_replace("\n",":",$shipper_address);

        $consignee_company = strtoupper($consol["consignee_company"]);
        $consignee_address = strtoupper($consol["consignee_address"]);
        $consignee_address = preg_replace("/\s(?=\s)/","\\1",$consignee_address); //多空格合并成1个
        $consignee_address = str_replace("\n",":",$consignee_address);

        $notify_company = strtoupper($consol["notify_company"]);
        $notify_address = strtoupper($consol["notify_address"]);
        $notify_address = preg_replace("/\s(?=\s)/","\\1",$notify_address); //多空格合并成1个
        $notify_address = str_replace("\n",":",$notify_address);

        $edistr .="NAD+CZ+++$shipper_company+$shipper_address'
NAD+CN+++$consignee_company+$consignee_address'
NAD+NI+++$notify_company+$notify_address'
NAD+CA+$trans_carrier:160:86'
NAD+HI+LOGW:160:ZZZ++LEAGUE SHIPPING'
NAD+FW+LOGW:160:ZZZ++LEAGUE SHIPPING'\r\n";

        $edistr .="CTA+IC+:JIE'\r\n";
        //获取email
        $this->load->model('bsc_user_model');
        $u_row = $this->bsc_user_model->get_by_id($this->session->userdata("id"));
        $email = $u_row["email"];

        $edistr .="COM+33125949:TE'
COM+63757200:FX'
COM+$email:EM'\r\n";
        $good_outers_unit = "PACKAGES";
        $good_outers_unit_code = "PK";
        $edistr .="GID+1+$good_outers:$good_outers_unit_code::6:$good_outers_unit'\r\n";

        //普通货不需要
        $goods_type = $shipment['goods_type'];

        //获取危险品信息数据
        if($goods_type == 'DR'){
            $this->load->model('biz_shipment_dangergoods_model');
            $dr = $this->biz_shipment_dangergoods_model->get_one("shipment_id", $shipments[0]['id']);
            if(empty($dr)){
                $result['msg'] = lang('缺危险品参数');
                echo json_encode($result);
                return;
            }
        }

        $GDS_goods_type = '';
        if($goods_type == 'DR') $GDS_goods_type = '11';
        if($goods_type == 'RF') $GDS_goods_type = '14';
        if($GDS_goods_type != '') $edistr .= "GDS+$GDS_goods_type'\r\n";//货物类型

        $hscode = $consol["hs_code"];
        $bsc_dict_model = Model('bsc_dict_model');
        $hs_code_dict = $bsc_dict_model->get_by_ext1_eq_one('name', $hscode, 'hs_code', 'TSL');
        if(empty($hs_code_dict)){
            $result['msg'] = "hscode error";
            echo json_encode($result);
            return;
        }

        if(empty($hscode)){
            $result['msg'] = "no hscode";
            echo json_encode($result);
            return;
        }
        // $hscode = substr($hscode,0,6);

        $edistr .="PIA+5+$hscode:HS'\r\n";

        //货物描述，一行一行
        $description = strtoupper($consol["description"]);
        $description = preg_replace("/\s(?=\s)/","\\1",$description); //多空格合并成1个
        $des_arr = explode("\n",$description);
        foreach($des_arr as $des){
            $edistr .="FTX+AAA+++$des'\r\n";
        }

        //重量体积
        $edistr .="MEA+AAE+WT+KGM:$good_weight'
MEA+AAE+AAW+MTQ:$good_volume'\r\n";

        //唛头，一行一行
        $edistr .="PCI++N/M'\r\n";

        if($goods_type == 'DR'){
            $edistr .= "DGS+IMD+{$dr['isdangerous']}+{$dr['UN_no']}'\r\n";//危品信息
            $edistr .= "CTA+HG+:{$dr['e_contact_name']}'\r\n";//危品联系人
            //危品联系方式--start
            $edistr .= "COM+{$dr['e_contact_tel']}:TE'\r\n";
//            $message[] = "COM+危险品联系人邮箱:EM'";
            //危品联系方式--end
        }

        //箱型箱量,多个箱型新增一行
        foreach($box_info as $box){
            $edistr .="EQD+CN++".$box["size"]."+2'\r\nEQN+".$box["num"]."'\r\n";
        }

        $temp = explode("\n",$edistr);
        $totalrow = sizeof($temp)+1;
        $edistr .="UNT+$totalrow+$job_no'
UNZ+1+$job_no'";

        //echo $edistr; exit();
        //保存文件txt
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        $new_dir = $root_path . '/upload/message/LOGWING_EDI/SO/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $consol['id'] . '.txt';
        $file_path = $new_dir . $file_name;
        if(preg_match('/[\x{4e00}-\x{9fa5}]+/u', $edistr, $matches1) === 1){
            echo json_encode(array('code' => 1, 'msg' => '存在中文或中文标点符号,生成失败, 错误内容:' . join(',', $matches1)));
            return;
        }
        file_put_contents($file_path, $edistr);

        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => 'SO',
            'receiver' => 'LOGWING',
        );
        update_message($consol_id, $message_data, 'SO');
        echo json_encode(array('code' => 0, 'msg' => '文件生成成功'));
    }

    private function si_checking($consol, $shipments, $consol_ext){
        return true;
        //requirements description mark_nums
        $consol_check = array('job_no', 'trans_origin', 'trans_origin_name', 'trans_discharge', 'trans_discharge_name',
            'trans_destination', 'trans_destination_name', 'shipper_company', 'shipper_address', 'consignee_company', 'consignee_address', 'notify_company', 'vessel', 'voyage', 'hs_code',  'container_owner', 'box_info', 'trans_carrier', 'des_ETA', 'booking_ETD', 'agent_ref');

        $consol_ext_check = array();
        $shipment_check = array('release_type', 'good_outers_unit', 'goods_type');

        //2023-04-23 高丽提出 des_ETA 不限制必填
        $date_time = array('booking_ETD');
        foreach ($consol_check as $val){
            if(in_array($val, $date_time) && ($consol[$val] == '0000-00-00 00:00:00' || $consol[$val] == '0000-00-00')){
                return $val;
            }
            if(empty($consol[$val])){
                return $val;
            }
        }

        foreach ($shipment_check as $val){
            if(empty($shipments[0][$val])){
                return $val;
            }
        }
        foreach ($consol_ext_check as $val){
            if(empty($consol_ext[$val])){
                return $val;
            }
        }

        foreach ($shipment_check as $val){
            foreach ($shipments as $shipment) {
                if (empty($shipment[$val])) {
                    return $val;
                }
            }
        }
        return true;
    }

    public function message_si($consol_id = 0, $file_function = 9){
        $this->load->model(array('biz_consol_model', 'biz_shipment_model', 'bsc_user_model', 'biz_container_model', 'biz_consol_ext_model'));

        $consol = $this->biz_consol_model->get_one('id', $consol_id);
        if(empty($consol)){
            $result['msg'] = "consol不存在";
            echo json_encode($result);
            return;
        }
        $shipments = $this->biz_shipment_model->no_role_get("consol_id = $consol_id");
        if(empty($shipments)){
            $result['msg'] = "该consol不存在shipment";
            echo json_encode($result);
            return;
        }

        $message_row = $this->model->get_where_one("id_type = 'consol' and id_no = $consol_id and type = 'SI' and receiver = 'LOGWING'");
        if(empty($message_row)){
            $result['msg'] = '请先保存数据后再试';
            echo json_encode($result);
            return;
        }

        $message_data = Model('bsc_message_data_model')->get_where_one("message_id = '{$message_row['id']}'");
        if(empty($message_data)){
            $result['msg'] = '请先保存数据后再试';
            echo json_encode($result);
            return;
        }

        $send_data = json_decode($message_data['send_data'], true);

        $checking = $this->si_checking($consol, $shipments, array());
        if($checking !== true){
            $result['msg'] = lang($checking);
            echo json_encode($result);
            return;
        }

        $containers = $this->biz_container_model->get("consol_id = {$consol_id}");
        if(empty($containers)){
            $result['msg'] = "container 不能为空";
            echo json_encode($result);
            return;
        }
        $des_ETA = date('Ymd', strtotime($consol['des_ETA']));


        foreach($consol as $key=>$value){
            if($key=="box_info") continue;
            $value = str_replace("?","??",$value);
            $value = str_replace("'","?'",$value);
            $value = str_replace(":","?:",$value);
            $value = str_replace("+","?+",$value);
            $consol[$key] = $value;
        }
        foreach($shipments as $key => $shipment){

            foreach ($shipment as $k => $value){
                if($k=="box_info") continue;
                $value = str_replace("?","??",$value);
                $value = str_replace("'","?'",$value);
                $value = str_replace(":","?:",$value);
                $value = str_replace("+","?+",$value);
                $shipments[$key][$k] = $value;
            }
        }
//        foreach($consol_ext as $key=>$value){
//            if($key=="box_info") continue;
//            $value = str_replace("?","??",$value);
//            $value = str_replace("'","?'",$value);
//            $value = str_replace(":","?:",$value);
//            $value = str_replace("+","?+",$value);
//            $consol_ext[$key] = $value;
//        }

        $time = time();
        $message_created_time = date("ymd:Hi", $time);
        $job_no = $consol['job_no'];
        $booking_ETD = $consol['booking_ETD'];
        $description = $consol['description'];
        $apply_remark = $consol['document_remark'];//申请备注
        $trans_origin = $consol['trans_origin'];
        $trans_origin_name = $consol['trans_origin_name'];
        $trans_discharge = $consol['trans_discharge'];
        $trans_discharge_name = $consol['trans_discharge_name'];
        $trans_destination = $consol['trans_destination'];
        $trans_destination_name = $consol['trans_destination_name'];
        $shipper_company = $consol['shipper_company'];
        $shipper_address = $consol['shipper_address'];
        $consignee_company = $consol['consignee_company'];
        $consignee_address = $consol['consignee_address'];
        $notify_company = $consol['notify_company'];
        $notify_address = $consol['notify_address'];
        $vessel = $consol['vessel'];
        $voyage = $consol['voyage'];
        $hs_code = $consol['hs_code'];
        $agent_ref = $consol['agent_ref'];
        $mark_nums = $consol['mark_nums'];
        $container_owner = $consol['container_owner'];
        $box_info = json_decode($consol['box_info'], true);
        $trans_carrier = $consol["trans_carrier"];
        $carrier_ref = $consol['carrier_ref'];
        $release_type = $shipments[0]['release_type'];

        // $AMS_type = $consol_ext['ams_type'];
        $AMS_type = $send_data['ams_type'];

        $good_outers_sum = 0;//consol总件数
        $good_weight_sum = 0;//consol总重量
        $good_volume_sum = 0;//consol总体积
        foreach ($containers as $container){
            $good_outers_sum += $container['packs'];
            $good_weight_sum += $container['weight'];
            $good_volume_sum += $container['volume'];
        }
        $box_num = array_sum(array_column($box_info, 'num'));//订舱总箱数

        $message = array();
        $message[] = "UNB+UNOC:2+EH2F:ZZZ+LOGWING:UN+$message_created_time+$job_no'";//报文的发送方和接收方信息说明
        $message[] = "UNH+$job_no+IFTMIN:D:99B:UN'";//报文格式说明及版本说明
        $message[] = "BGM+335+$job_no+$file_function'";//业务编号（本票订舱数据在乐域的唯一标识）
        $message[] = "DTM+137+" . date('YmdHi', $time) . "+203'";//报文生成时间

        //不能超过2000字符，35字符切
        if(strlen($apply_remark) >= 2000){
            $result['msg'] = lang('截单备注不能超过2000字符');
            echo json_encode($result);
            return;
        }
        $apply_remark_array = explode("\n", $apply_remark);
        $apply_remark_array = cut_array_length($apply_remark_array,999,35);

        foreach ($apply_remark_array as $val){
            $message[] = "FTX+AAI+++$val'";//申请备注
        }
        //RFF
        //FF：业务编号（提单阶段不需要）
        //BN：关联订舱单号，若并票，则用多个 BN 段列出关
        //联订舱单号
        //BM：主提单号
        //SI：提单号
        //BL：组内提单号，若有拆票，则多个提单号用逗号隔
        //开
        //CT：合约号；若没有合约号，则不需要此段
        //AP：船东为 ESL 时的 AP CODE
        //CR：参考号
        //CT2：合约方代码
        //SLS：PIL Sales（业务员）
        //PLS：PILL Sales
        //CSR：PILL 操作
        //DSR：PILL 单证
        //CY：堆场代码（提箱码头）
        //FCL：装箱方式代码（1=外提产装，2=场站装箱）
        //FD：免柜期天数
        //NA：Named Account
        $message[] = "RFF+BM:$agent_ref'";  //主提单号
        $message[] = "RFF+BN:$carrier_ref'";  //订舱单号
        $message[] = "RFF+SI:$agent_ref'";  //提单号
        // $message[] = "RFF+CT:$job_no'";  //合约号；若没有合约号，则不需要此段
        $message[] = "RFF+FF:$job_no'";  //业务编号（提单阶段不需要)

        //CNT
        //类型：7=本票总毛重，11=本票总件数，15=本票总
        //体积，16=本票总总柜数
        //值
        //单位：毛重=KGM，体积=MTQ
        //本票总件毛体描述--start TODO 是否根据container总值来
        $message[] = "CNT+7:$good_weight_sum:KGM'";
        $message[] = "CNT+11:$good_outers_sum'";
        $message[] = "CNT+15:$good_volume_sum:MTQ'";
        $message[] = "CNT+16:$box_num'";
        //本票总件毛体描述--end
        $message[] = "TSR+30+2'";//承运条款

        if($release_type=="OBL"){
            $message[] = "DOC+706+:26++3'";//提单类型
            $message[] = "DOC+707+:26++3'";//提单类型
        }else if($release_type=="TER"){
            $message[] = "DOC+706+:26++0'";//提单类型
            $message[] = "DOC+707+:26++1'";//提单类型
        }else if($release_type=="SWB"){
            $message[] = "DOC+710+:26++1'";//提单类型
        }else{
            $result['msg'] = '暂不支持该放单方式';
            echo json_encode($result);
            return;
        }


        $payment = $consol["payment"];
        if($payment=="PP")$CPI_payment = "P";
        else if($payment=="CC") $CPI_payment = "C";
        else if($payment=="E") $CPI_payment = "A";
        else{
            $result['msg'] = '暂不支持该付款方式';
            echo json_encode($result);
            return;
        }
        $message[] = "CPI+4++$CPI_payment'";//付款方式说明

        //LOC
        //57：付款地标记
        //59：Local 付款地标记
        //73：签单地标记
        //197：订舱接收地
        //付款地，签单地说明--start
        $message[] = "LOC+57+$trans_origin::6+$trans_origin_name'";
        $message[] = "LOC+73+$trans_origin::6+$trans_origin_name'";
        //付款地，签单地说明--end

        //AMS
        //AMS 类型：A=A/nvocc 自主申报 HOUSE 提单；’
        //C=C/无 HOUSE 单或欧线单；
        //M=M/HOUSE 单委托
        //船东申报;
        if(!empty($AMS_type)){
            $message[] = "AMS+++$AMS_type'"; //AMS 类型 给他们自己选择判断
        }


        //TDT
        //10：支线船
        //20：海船
        //30：支线船
        $code_arr = array(
            'ZYJZXY01'=>'COSU',
            'MCCYSX01'=>'MCCQ',
            'MSJZGH01'=>'MAEU',
            '美国总统轮船'=>'APLU',
            'FGDFLC01'=>'CMDU',
            'RSDZHH01'=>'MSCU',
            'YMHYGF01'=>'YMLU',
            'TPCWYX01'=>'PABV',
            'HBNMHY01'=>'SUDU',
            'DFHWHG01'=>'OOLU',
            'XDSCZG01'=>'HDMU',
            'HBLTCW01'=>'HLCU',
            'DXHYYX02'=>'TLWN',
            'YXZHHY01'=>'ZIMU',
            'HHXYYX01'=>'REGU',
            'YHHYYX01'=>'ASL',
        );

        if(!isset($code_arr[$trans_carrier])){
            $result['msg'] = "no carrier code";
            echo json_encode($result);
            return;
        }
        $trans_carrier_code = $code_arr[$trans_carrier];
        $message[] = "TDT+20+$voyage+1++$trans_carrier_code:172+++:::$vessel'";//船名航次说明
        $DTM_booking_ETD = str_replace('-', '', $booking_ETD);
        $message[] = "DTM+133:" . $DTM_booking_ETD . "+102'";//预计达到日期

        //LOC
        //港口类别代码；88=收货地；9=起运港；8=中转港；
        //11=卸货港；7=目的地
        //；88=收货地； 9=起运港；8=中转港；11=卸货港；7=目的地
        //港口信息说明--start
        $message[] = "LOC+88+$trans_origin::6:$trans_origin_name'";
        $message[] = "LOC+9+$trans_origin::6:$trans_origin_name'";
        $message[] = "LOC+11+$trans_discharge::6:$trans_discharge_name'";
        $message[] = "LOC+7+$trans_destination::6:$trans_destination_name'";
        //港口信息说明--end

        // $message[] = "DTM+132:$des_ETA:102'";//预离时间 到港时间 年月日格式 TODO
        //NAD
        //相关方类型 CA=船东；HI=发送方；CZ=发货方；CN=
        //收货方；NI=通知方；N2=第二通知方；SP=实际托
        //运人；PY=付款公司；FW=货代；SA=船代
        //相关方信息--start
        $message[] = "NAD+CA+$trans_carrier_code:160:86++$trans_carrier'";

        $NAD_shipper_address = $shipper_address;
        $NAD_shipper_address = str_replace("\n", ":", $NAD_shipper_address);
        $message[] = "NAD+CZ+++$shipper_company+$NAD_shipper_address'";

        $NAD_consignee_address = $consignee_address;
        $NAD_consignee_address = str_replace("\n", ":", $NAD_consignee_address);
        $message[] = "NAD+CN+++$consignee_company+$NAD_consignee_address'";

        $NAD_notify_address = $notify_address;
        $NAD_notify_address = str_replace("\n", ":", $NAD_notify_address);
        $message[] = "NAD+NI+++$notify_company+$NAD_notify_address'";

        //固定货代 货代取值什么？？？ ODO

        $message[] = "NAD+FW+LOGW::++LEAGUE SHIPPING'";
        //相关方信息--end

        $message[] = "CTA+IC+:JIE'";//相关方联系人 TODO

        //相关方联系方式--start
        $userId = get_session('id');
        $thisUser = $this->bsc_user_model->get_where_one("id = $userId and status = 0");
        if(empty($thisUser)){
            $result['msg'] = '当前账户已禁用或不存在';
            echo json_encode($result);
            return;
        }
        $email = $thisUser['email'];

        $message[] = "COM+33125949:TE'";//TODO
        $message[] = "COM+63757200:FX'";//TODO
        $message[] = "COM+$email:EM'";//TODO
        //相关方联系方式--end


        foreach ($shipments as $key => $shipment){
            //获取shipment的container数据
            $this->load->model('biz_shipment_container_model');
            $shipment_containers = $this->biz_shipment_container_model->join_container_get_by_id($shipment['id'], $consol_id);
            if(empty($shipment_containers)){
                $result['msg'] = $shipment['job_no'] . ' 不存在箱信息';
                echo json_encode($result);
                return;
            }


            $good_outers = array_sum(array_column($shipment_containers, 'packs'));
            $good_outers_unit = $shipment['good_outers_unit'];
            $good_weight = array_sum(array_column($shipment_containers, 'weight'));
            $good_volume = array_sum(array_column($shipment_containers, 'volume'));
            $shipment_hs_code = $shipment['hs_code'];
            $shipment_job_no = $shipment['job_no'];
            $shipment_description = $consol['description'];
            $shipment_mark_nums = $consol['mark_nums'];
            $goods_type = $shipment['goods_type'];
            $GID_no = $key + 1;//货物序号等于当前key+ 1 从1开始，

            //获取危险品信息数据
            if($goods_type == 'DR'){
                $this->load->model('biz_shipment_dangergoods_model');
                $dr = $this->biz_shipment_dangergoods_model->get_one("shipment_id", $shipments[0]['id']);
                if(empty($dr)){
                    $result['msg'] = lang('缺危险品参数');
                    echo json_encode($result);
                    return;
                }
            }


            $good_outers_unit_code_config = $this->good_outers_unit_CODE;
            if(!isset($good_outers_unit_code_config[$good_outers_unit])){
                $result['msg'] = '包装代码未配置';
                echo json_encode($result);
                return;
            }
            $good_outers_unit_code = $good_outers_unit_code_config[$good_outers_unit];
            $message[] = "GID+1+$good_outers:$good_outers_unit_code::6:$good_outers_unit'";//货物件数，包装说明

            //普通货不需要
            $GDS_goods_type = '';
            if($goods_type == 'DR') $GDS_goods_type = '11';
            if($goods_type == 'RF') $GDS_goods_type = '14';
            if($GDS_goods_type != '') $message[] = "GDS+$GDS_goods_type'";//货物类型

            $shipment_hs_code = explode(',', $shipment_hs_code);
            if(sizeof($shipment_hs_code) > 2){
                $result['msg'] = "SHIPMENT:" . $shipment_job_no . ',hs_code 不能超过2种';
                echo json_encode($result);
                return;
            }

            foreach($shipment_hs_code as $val){
                $message[] = "PIA+5+$val:HS'";//商品编码
            }

            //不能超过2000字符，35字符切
            if(strlen($shipment_description) >= 2000){
                $result['msg'] = "SHIPMENT:" . $shipment_job_no .  ',货物描述不能超过2000字符';
                echo json_encode($result);
                return;
            }
            $description_array = explode("\n", $shipment_description);
            $description_array = cut_array_length($description_array,999,35);

            foreach ($description_array as $val){
                $message[] = "FTX+AAA+++$val'";//货名
            }

            //货物信息重量，体积--start
            $message[] = "MEA+AAE+WT+KGM:$good_weight'";//重量
            $message[] = "MEA+AAE+AAW+MTQ:$good_volume'";//体积
            //货物信息重量，体积--end

            //不能超过2000字符，35字符切
            if(strlen($shipment_mark_nums) >= 2000){
                $result['msg'] = "SHIPMENT:" . $shipment_job_no . '唛头不能超过2000字符';
                echo json_encode($result);
                return;
            }
            $mark_nums_array = explode("\n", $shipment_mark_nums);
            $mark_nums_array = cut_array_length($mark_nums_array,999,35);

            foreach ($mark_nums_array as $val){
                $message[] = "PCI++$val'";//唛头
            }

            if($goods_type == 'DR'){
                $message[] = "DGS+IMD+{$dr['isdangerous']}+{$dr['UN_no']}'";//危品信息
                $message[] = "CTA+HG+:{$dr['e_contact_name']}'";//危品联系人
                //危品联系方式--start
                $message[] = "COM+{$dr['e_contact_tel']}:TE'";
                //            $message[] = "COM+危险品联系人邮箱:EM'";
                //危品联系方式--end
            }
            $EQD_SOC = '1';
            // if($container_owner	== 'SOC'){
            //     $EQD_SOC = '1';
            // }else if($container_owner == 'COC'){
            //     $EQD_SOC = '2';
            // }else{
            //     echo json_encode(array('code' => 1, 'msg' => '该货主箱标记不存在'));
            //     return;
            // }
            foreach($shipment_containers as $shipment_container){
                $message[] = "SGP+{$shipment_container['container_no']}+{$shipment_container['packs']}'";//箱号，箱中件数说明
                //箱中当前货物的体积，重量，通风量--start
                $message[] = "MEA+AAE+AAW+MTQ:{$shipment_container['weight']}'";
                $message[] = "MEA+AAE+WT+KGM:{$shipment_container['volume']}'";
            }

            //箱中当前货物的体积，重量，通风量--end
        }
        foreach ($containers as $container){


            $message[] = "EQD+CN+{$container['container_no']}+{$container['container_size']}+$EQD_SOC'";//箱型尺寸

            //若为冻代干箱，则不需要通风量  通风量好像没什么用，统一不要了
            //箱中体积，重量，通风量--start
            $message[] = "MEA+AAE+AAW+MTQ:{$container['volume']}'";
            $message[] = "MEA+AAE+WT+KGM:{$container['weight']}'";
            //箱中体积，重量，通风量--end
            $message[] = "SEL+{$container['seal_no']}+CA'";//铅封号
            $message[] = "EQN+1'";//箱量

            //特殊箱子使用，目前不用
            //OW=超重柜
            //SUB=高代平
            //INA=冻代干
            //OS=超尺寸
            $EQT_type = '';
            if($EQT_type != '') $message[] = "EQT+$EQT_type'";//箱子特殊标记

            //冻代干时无需此段，目前不用
            //温度（-98~998）
            //温度单位：摄氏度=CEL；华氏度=FAH
            $TMP_CEL = '';
            if($TMP_CEL != '') $message[] = "TMP+2+$TMP_CEL:CEL'";//冻柜温度说明

            //若非超尺寸，则不需要此段 目前不用
//        $message[] = "DIM+2+MTR:长:宽:高";//超尺寸，长宽高说明
        }

        $message_length = sizeof($message);
        $message[] = "UNT+$message_length+$job_no'";//报文行数说明
        $message[] = "UNZ+1+$job_no";//报文结尾

        $message = join("\r\n", $message);

        //保存文件txt
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        $new_dir = $root_path . '/upload/message/LOGWING_EDI/SI/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $consol['id'] . '.txt';
        $file_path = $new_dir . $file_name;
        // if(preg_match('/[\x{4e00}-\x{9fa5}]+/u', $message, $matches1) === 1){
        //     echo json_encode(array('code' => 1, 'msg' => '存在中文或中文标点符号,生成失败'));
        //     return;
        // }
        file_put_contents($file_path, $message);

        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => 'SI',
            'receiver' => 'LOGWING',
        );
        update_message($consol_id, $message_data, 'SI');
        echo json_encode(array('code' => 0, 'msg' => '文件生成成功'));
    }

    public function si_set_data($consol_id = 0, $action = ''){

        $is_reset = (int)postValue('is_reset', 0);

        $data = array();
        $data['consol_id'] = $consol_id;
        $data['action'] = $action;

        Model('biz_consol_model');
        $message = $this->model->get_where_one("id_type = 'consol' and id_no = $consol_id and type = 'SI' and receiver = 'LOGWING'");
        $data['consol'] = $this->biz_consol_model->get_by_id($consol_id);

        $data['message'] = $message;
        if(empty($message) || $is_reset){
            $data['edi_data'] = array('ams_type' => 'A');
        }else{
            $message_data = Model('bsc_message_data_model')->get_where_one("message_id = {$message['id']}");
//            var_dump(1111);
//            return;
            if(!empty($message_data)) $data['edi_data'] = json_decode($message_data['send_data'], true);
            else $data['edi_data'] = array('ams_type' => 'A');
        }


        $this->load->view('head');
        $this->load->view('/bsc/message/data/logwing/si_form', $data);
    }

    /**
     * 获取文件
     */
    public function ftp_read_out($id = 0)
    {
        $bsc_message_model = Model('bsc_message_model');
        $this->db->select('id_no,type');
        $message = $bsc_message_model->get_one('id', $id);
        if(empty($message)) return jsonEcho(array('code' => 1, 'msg' => '请生成报文后再试'));
        $biz_consol_model = Model('biz_consol_model');
        $consol = $biz_consol_model->get_by_id($message['id_no']);
        if(empty($consol)) return jsonEcho(array('code' => 1, 'msg' => 'consol不存在'));


        $config = array('host' => 'ftp.booking001.com', 'port' => 21, 'username' => 'FWDEH2F', 'password' => 'T53Q72hT');
        $conn = ftp_connect($config['host'], $config['port']);

        //一旦建立联接，使用ftp_login()发送一个用户名称和用户密码。你可以看到，这个函数ftp_login()使用了 ftp_connect()函数传来的handle，以确定用户名和密码能被提交到正确的服务器。
        ftp_login($conn, $config['username'], $config['password']);

        //被动模式（PASV）的开关，打开或关闭PASV（1表示开）
        ftp_pasv($conn, true);

        //进入目录中用ftp_chdir()函数，它接受一个目录名作为参数。
        $type = $message['type'];
        if($type == 'SO'){
            $ftp_path = '/so/out/';
        }else if($type == 'SI'){
            $ftp_path = '/si/out/';
        }else{
            echo json_encode(array('code' => 1, 'msg' => '参数错误'));
            return;
        }


        $file_path = './upload/message/LOGWING_EDI/' . $type . '_OUT/';

        $list = ftp_nlist($conn, $ftp_path);
        // var_dump($list);
        // return;
        //重排序一下,按照consol编号优先, 然后最后的编号次之进行排序
        uasort($list, function ($x,$y){
            $x_arr = explode('_', $x);
            $y_arr = explode('_', $y);
            if($x_arr[0] < $y_arr[0]){
                return -1;
            }else if($x_arr[0] > $y_arr[0]){
                return 1;
            }else{
                if($x_arr[2] > $y_arr[2]){
                    return -1;
                }else if($x_arr[2] > $y_arr[2]){
                    return 1;
                }else{
                    return 0;
                }
            }
        });
        $this_file_name = '';
        foreach ($list as $file){
            $file_name = explode('_', $file);
            if($consol['job_no'] == $file_name[0] && !file_exists($file_path . $file)){
                $this_file_name = $file;
                $file_path .= $this_file_name;
                ftp_get($conn, $file_path, $ftp_path . $file, FTP_BINARY);
                break;
            }
        }

        //上传文件，ftp_put()函数能很好的胜任，它需要你指定一个本地文件名，上传后的文件名以及传输的类型。比方说：如果你想上传 “abc.txt”这个文件，上传后命名为“xyz.txt”，命令应该是这样：
        ftp_quit($conn);

        if($this_file_name == ''){
            $msg = '未获取到回执文件';
        }else{
            $file_out_content = file_get_contents($file_path);
            $msg = explode("\n", $file_out_content);
            $result_msg = '';
            foreach ($msg as $val){
                if(substr($val, 0, 10) == 'FTX+AAI+++'){
                    $result_msg = $val;
                    break;
                }
            }
            $msg = str_replace('FTX+AAI+++', '', $result_msg);
            $msg = str_replace('<br/>\'', '', $msg);

            //获取成功时, 直接将回执存入
            Model('bsc_message_send_model')->update_last_send($id, array('return_msg' => $msg, 'return_msg_all' => $file_out_content));
        }

        echo json_encode(array('code' => 0, 'msg' => 'success', 'data' => $msg));
    }
}