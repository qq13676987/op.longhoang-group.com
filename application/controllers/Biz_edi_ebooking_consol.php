<?php


class biz_edi_ebooking_consol extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('biz_edi_ebooking_consol_model');
    }

    public $field_edit = array();

    public function index(){
        $data = array();
        // column defination
        $data['status'] = getValue('status', '');
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_edi_ebooking_consol_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $this->field_edit[] = "job_no";
            $data["f"] = array_map(function ($r){
                return array($r, $r, 0);
            }, $this->field_edit);
        }
        // page account
        $rs = $this->sys_config_model->get_one('biz_edi_ebooking_consol_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }
        $data['config_search'] = json_decode(getConfigSearch('biz_edi_ebooking_consol', array('job_no', 'like', ''))['config_text'], true);
        
        $this->load->view('head');
        $this->load->view('biz/edi_ebooking_consol/index_view', $data);
    }

    public function edit($id = 0){
        $shipment_ebooking = $this->biz_edi_ebooking_consol_model->get_one('id', $id);
        if(!empty($shipment_ebooking)){
            $data = $shipment_ebooking;
        }else{
            echo "system error";
            return;
        }

        $this->load->view('head');
        $this->load->view('biz/edi_ebooking_consol/edit_view', $data);
    }

    public function get_data(){
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $fields = postValue('field', array());
        $status = getValue('status', '');
        $post_status = postValue('status', '');

        //-------这个查询条件想修改成通用的 -------------------------------
        $where = array();

        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_edi_ebooking_consol', 'biz_edi_ebooking_consol_index');//获取相关的配置信息
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];


                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') $v = '';
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) $title_data['sql_join'][$title_data['base_data'][$title_f]['sql_join']] = $this_join;

                    $where[] = search_like($f, $s, $v);
                }
            }
        }

        if($status !== '') $where[] = "biz_edi_ebooking_consol.status = $status";
        if($post_status !== '') $where[] = "biz_edi_ebooking_consol.status = $post_status";

        $where = join(' and ', $where);
        //-----------------------------------------------------------------
        $offset = ($page - 1) * $rows;
        $this->db->limit($rows, $offset);
        //获取需要的字段--start
        $selects = array(
            'biz_edi_ebooking_consol.id as id',
            'biz_edi_ebooking_consol.status as status',
        );
        foreach ($title_data['select_field'] as $key => $val){
            if(!in_array($val, $selects)) $selects[] = $val;
        }
        //获取需要的字段--end
        $this->db->select(join(',', $selects), false);
        //这里拼接join数据
        foreach ($title_data['sql_join'] as $j){
            $this->db->join($j[0], $j[1], $j[2]);
        }//将order字段替换为 对应的sql字段
        $sort = $title_data['sql_field']["biz_edi_ebooking_consol.{$sort}"];
        $this->db->is_reset_select(false);//不重置查询
        $rs = $this->biz_edi_ebooking_consol_model->get($where, $sort, $order);

        $this->db->clear_limit();//清理下limit
        $result["total"] = $this->db->count_all_results('');
        $rows = array();
        foreach ($rs as $row) {
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }
    
    /**
     * 拒绝掉该Ebooking
     */
    public function cancel_status(){
        $id = (int)getValue('id', 0);
        $status = (int)postValue('status', 0);

        $data = array();
        $data['status'] = $status;

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_edi_ebooking_consol";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        $this->biz_edi_ebooking_consol_model->update($id, $data);
        stop_notice('biz_edi_ebooking_consol',$id,'biz_edi_ebooking_consol');
        return jsonEcho(array('code' => 0, 'msg' => lang('审核成功')));
    }

    /**
     * 修改状态
     */
    public function update_status(){
        $id = (int)postValue('id', 0);
        $status = (int)postValue('status', 0);
        $return = ['msg'=>'', 'code'=>0];

        if (is_ajax()) {
            $old_row = $this->biz_shipment_ebooking_model->get_one('id', $id);
            if(empty($old_row)) return jsonEcho(array('code' => 1, 'msg' => lang('ebooking不存在')));

            $data = array();
            $data['status'] = $status;
            $query = $this->biz_shipment_ebooking_model->update($id, $data);

            if ($query){
                $return['code'] = 1;
            }else{
                $return['msg'] = 'failure！ error type:mysql';
            }
            //save the operation log
            $log_data = array();
            $log_data["table_name"] = "biz_shipment_ebooking";
            $log_data["key"] = $id;
            $log_data["action"] = "update";
            $log_data["value"] = json_encode($data);
            log_rcd($log_data);
        }else{
            $return['msg'] = 'request not permit！ error type:request type';
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    public function get_shipment($consol_id=0){
		$db = isset($_GET['db'])?$_GET['db']:'';
		if(empty($db)) return "no database";
		$db = $this->load->database($db,true);
		$shipment = $db->where("consol_id = $consol_id")->get('biz_shipment')->result_array();
		$rows = [];
		if(!empty($shipment)){
			foreach ($shipment as $s){
				$s['box_str'] = '';
				$box_info = json_decode($s['box_info'],true);
				if(!empty($box_info)){
					$s['box_str'] = join(',',array_map(function($v){
						return $v['size'].' x '.$v['num'];
					},$box_info));
				}
				array_push($rows,$s);
			}
		}
		return jsonEcho($rows);
	}

	public function save_data($id=0){
    	$data['customer_service_id'] = postValue('customer_service_id',0);
    	$data['operator_id'] = postValue('operator_id',0);
    	$this->db->where('id',$id)->update('biz_edi_ebooking_consol',$data);
		$log_data["table_name"] = "biz_edi_ebooking_consol";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        return jsonEcho(['code'=>1,'msg'=>'SUCCESS']);
	}

	public function create_data($id=0){
    	$old_row = $this->db->where('id',$id)->get('biz_edi_ebooking_consol')->row_array();
    	if(empty($old_row)) return jsonEcho(['code'=>0,'msg'=>'no data']);
    	if($old_row['operator_id'] == 0 || $old_row['customer_service_id'] == 0){
    		return jsonEcho(['code'=>0,'msg'=>'please add operator and customer service for creating shipments.']);
		}
    	$from_db = $this->load->database($old_row['from_db'],true);
    	$from_consol_data = $from_db->where('id',$old_row['id_no'])->get('biz_consol')->row_array();
		if(empty($from_consol_data)){
			return jsonEcho(['code'=>0,'msg'=>'no consol in '.$old_row['from_db'].' system.']);
		}
		$from_shipment_datas = $from_db->select("biz_shipment.*,(select user_id from biz_duty_new where id_type = 'biz_shipment' and id_no = biz_shipment.id and user_role = 'sales') as sales_id")->where('consol_id',$old_row['id_no'])->get('biz_shipment')->result_array();
		if(empty(($from_shipment_datas))){
			return jsonEcho(['code'=>0,'msg'=>'no shipment in '.$old_row['from_db'].' system.']);
		}
		$from_client = $from_db->where('client_code',$from_shipment_datas[0]['client_code'])->get('biz_client')->row_array();
		if(empty(($from_client))){
			return jsonEcho(['code'=>0,'msg'=>'no client in '.$old_row['from_db'].' system.']);
		}
		$client = $this->db->where("client_code != '{$from_client['client_code']}' and company_name = '{$from_client['company_name']}'")->get('biz_client')->row_array();
		if(!empty($client)){
			return jsonEcho(['code'=>0,'msg'=>'failure! duplicate client name in system. contact IT to solve it']);
		}
    	$consol_id = 0;
    	if(!empty($from_consol_data)){
			$data = [];
			$consol_field = ['status','shipper_company','shipper_address','shipper_contact','shipper_telephone','shipper_email','consignee_company','consignee_address','consignee_contact','consignee_telephone','consignee_email','notify_company','notify_address','notify_contact','notify_telephone','notify_email', 'payment','payment_third','carrier_ref','agent_ref','description','mark_nums','description_cn','trans_origin','trans_origin_name','trans_discharge','trans_discharge_name','trans_destination_inner','trans_destination','trans_destination_terminal','trans_destination_name','sailing_code','sailing_area','trans_type','trans_carrier','booking_ETD', 'trans_ETA','trans_ATA','des_ETA','vessel','voyage','voyage_inner','imo_number','feeder','hbl_type','trans_mode','trans_tool','hs_code','box_info','requirements','AGR_no','container_owner','AP_code','AP_remark','customer_booking','carrier_agent','trans_discharge_code','remark','document_remark','goods_type','good_outers','good_outers_unit','good_weight','good_weight_unit','good_volume','good_volume_unit','trans_receipt','trans_receipt_name','trans_term','release_type','hide_flag'];
			$data['from_db'] = $old_row['from_db'];
			$data['from_id_no'] = $old_row['id_no'];
			$data['biz_type'] = $from_consol_data['biz_type'] == 'export'?'import':'export';
			foreach ($consol_field as $c_f){
				$data[$c_f] = isset($from_consol_data[$c_f])?$from_consol_data[$c_f]:'';
			}
			$data['report_date'] = $from_consol_data['des_ATA'];
			$data['trans_ATA'] = $from_consol_data['des_ATA'];
			$data['trans_ETA'] = $from_consol_data['des_ETA'];
			Model('biz_consol_model');
			$consol_id = $this->biz_consol_model->save($data);
			if (!$consol_id) return jsonEcho(['code'=>0,'msg'=>'add failure']);
			$log_data = array();
			$log_data["table_name"] = "biz_consol";
			$log_data["key"] = $consol_id;
			$log_data["action"] = "insert";
			$log_data["value"] = json_encode($data);
			log_rcd($log_data);
			$job_no = consol_job_no_rule($consol_id);
			$data['job_no'] = $job_no;
			$this->biz_consol_model->update($consol_id, array('job_no' => $job_no));
			$from_db->where('id',$old_row['id_no'])->update('biz_consol',['from_db'=>get_system_type(),'from_id_no'=>$consol_id]);
			$duty = array();
			$duty['biz_table'] = 'biz_consol';
			$duty['id_no'] = $consol_id;
			$duty['sales_id'] = 0;
			$duty['sales_group'] = '';
			$duty['market_assistant_id'] = 0;
			$duty['market_assistant_group'] = '';
			$duty['document_id'] = 0;
			$duty['document_group'] = '';
			$duty['booking_id'] = 0;
			$duty['booking_group'] = '';
			$duty['oversea_cus_id'] = 0;
			$duty['oversea_cus_group'] = '';
			$duty['marketing_id'] = 0;
			$duty['marketing_group'] = '';
			$duty['operator_id'] = $old_row['operator_id'];
			$duty['operator_group'] = getUserField($old_row['operator_id'],'group');
			$duty['customer_service_id'] = $old_row['customer_service_id'];
			$duty['customer_service_group'] = getUserField($old_row['customer_service_id'],'group');
			$this->load->model('biz_duty_model');
			$this->biz_duty_model->save($duty);
			$shipment_field = ['status','shipper_company','shipper_address','shipper_contact','shipper_telephone','shipper_email','consignee_company','consignee_address','consignee_contact','consignee_telephone','consignee_email','notify_company','notify_address','notify_contact','notify_telephone','notify_email','trans_mode','trans_tool','trans_term','contract_no','description','description_cn','mark_nums','release_type','release_type_remark','trans_origin','trans_origin_name','trans_discharge','trans_discharge_name','trans_destination_inner','trans_destination','trans_destination_terminal','trans_destination_name','sailing_code','sailing_area','cus_no','trans_carrier','booking_ETD','good_outers','good_outers_unit','good_weight','good_weight_unit','good_volume','good_volume_unit','good_describe','good_commodity','INCO_term','issue_date','expiry_date','hbl_no','hbl_type','tidanqianfa','box_info','hs_code','goods_type','dangergoods_id','requirements','service_options','AGR_no','INCO_option','INCO_address','parent_id','remark','show_fields','loss_flag','invoice_flag','payment_flag','consol_percent','consol_id_apply','pay_buy_flag','vgm_send','SI_outer_link','document_remark','pre_alert','client_code','client_company','client_address','client_contact','client_telephone','client_email'];
			Model('biz_client_model');
			if(!empty($from_shipment_datas)){
				foreach ($from_shipment_datas as $from_shipment_data){
					$shipment_data = [];
					$shipment_data['from_db'] = $old_row['from_db'];
					$shipment_data['from_id_no'] = $from_shipment_data['id'];
					$shipment_data['consol_id'] = $consol_id;
					$shipment_data['biz_type'] = $from_shipment_data['biz_type'] == 'export'?'import':'export';
					foreach ($shipment_field as $s_f){
						$shipment_data[$s_f] = isset($from_shipment_data[$s_f])?$from_shipment_data[$s_f]:'';
					}
					$shipment_data["job_no"] = shipment_job_no_rule($shipment_data['trans_origin'], $shipment_data['trans_mode']);
					Model('biz_shipment_model');
					$shipment_id = $this->biz_shipment_model->save($shipment_data);
					$from_db->where('id',$from_shipment_data['id'])->update('biz_shipment',['from_db'=>get_system_type(),'from_id_no'=>$shipment_id]);
					$log_data = array();
					$log_data["table_name"] = "biz_shipment";
					$log_data["key"] = $shipment_id;
					$log_data["action"] = "insert";
					$log_data["value"] = json_encode($shipment_data);
					log_rcd($log_data);//
					$from_shipment_duty = $from_db->where("id_type = 'biz_shipment' and id_no = {$from_shipment_data['id']}")->get('biz_duty_new')->result_array();
					$duty = array();
					$duty['biz_table'] = 'biz_shipment';
					$duty['id_no'] = $shipment_id;
					foreach ($from_shipment_duty as $s_d){
						$duty[$s_d['user_role'].'_id'] = $s_d['user_id'];
						$duty[$s_d['user_role'].'_group'] = $s_d['user_group'];
					}
					$duty['operator_id'] = $old_row['operator_id'];
					$duty['operator_group'] = getUserField($old_row['operator_id'],'group');
					$duty['customer_service_id'] = $old_row['customer_service_id'];
					$duty['customer_service_group'] = getUserField($old_row['customer_service_id'],'group');
					$duty['sales_id'] = $from_shipment_data['sales_id'];
					$duty['sales_group'] = getUserField($from_shipment_data['sales_id'],'group');
					$this->biz_duty_model->save($duty);
					if(!empty($shipment_data['client_code'])){
						$client = $this->db->where('client_code',$shipment_data['client_code'])->get('biz_client')->row_array();
						if(empty($client)){
							$from_client = $from_db->where('client_code',$shipment_data['client_code'])->get('biz_client')->row_array();
							if(!empty($from_client)){
								$client_data = [];
								foreach ($this->biz_client_model->field_all as $client_f){
									$client_data[$client_f[0]] = $from_client[$client_f[0]];
								}
								unset($client_data['id']);
								$this->biz_client_model->save($client_data);
								$from_client_duty = $from_db->where("client_code = '{$shipment_data['client_code']}' and user_role = 'sales'")->get('biz_client_duty')->row_array();
								if(!empty($from_client_duty)){
									$client_duty_data = $from_client_duty;
									unset($client_duty_data['id']);
									$client_duty_data['created_time'] = date('Y-m-d H:i:s');
									$this->db->insert('biz_client_duty',$client_duty_data);
								}
							}
						}
					}
				}
			}
			$this->db->where('id',$id)->update('biz_edi_ebooking_consol',['status'=>1]);
			$log_data = array();
			$log_data["table_name"] = "biz_edi_ebooking_consol";
			$log_data["key"] = $id;
			$log_data["action"] = "update";
			$log_data["value"] = json_encode(['status'=>1]);
			log_rcd($log_data);
			stop_notice('biz_edi_ebooking_consol',$id,'biz_edi_ebooking_consol');
		}
    	return jsonEcho(['code'=>1,'msg'=>'passed','consol_id'=>$consol_id]);

	}
}
