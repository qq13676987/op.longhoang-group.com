<?php

/**
 * 收件人
 */
class crm_promote_consignee_model extends CI_Model
{
    private $table = 'crm_promote_consignee';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    public function get($where="" ,$sort="id",$order="desc",$like=array()){
        if($where !="")	$this->db->where($where);
        $this->db->like($like);
        $this->db->order_by($sort,$order);
        $query = $this->db->get($this->table);
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function total($where="",$like=array()){
        if($where !="")	$this->db->where($where);
        $this->db->like($like);
        $this->db->from($this->table);
        return  $this->db->count_all_results('');
    }




    public function save($data = array())
    {
        $data["create_time"] = date('y-m-d H:i:s',time());
        $data["update_time"] = date('y-m-d H:i:s',time());
        $data["create_by"] =  $this->session->userdata('id');
        $data["update_by"] =  $this->session->userdata('id');
        $this->db->insert($this->table, $data);

        return $this->db->insert_id();

    }

    public function del($id = '')
    {
        // delete container
        $this->db->where('id', $id);
        return $this->db->delete($this->table);
    }
    public function del_by_parent($id = '')
    {
        // delete container
        $this->db->where('group_id', $id);
        return $this->db->delete($this->table);
    }
    
    public function del_group($group_id = '')
    {
        // delete container
        if(empty($group_id)) return false;
        $this->db->where('group_id', $group_id);
        return $this->db->delete($this->table);
    }

    public function get_row($where=array()){
        if($where != array()) $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_result($where=array()){
        if($where != array()) $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }


}