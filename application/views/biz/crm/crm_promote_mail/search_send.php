<meta charset="UTF-8">
<style>
    .hide{
        display: none;
    }
    .query_total{
        font-size: 20px;
    }
    .query_next{
        color: red;
        font-size: 20px;
    }
</style>
<body class="easyui-layout" data-options="fit: true" style="padding: 0px; ">
    <div>
        <form action="/crm_promote_mail/add_presend_by_search_send" method="POST" id="tab_form">
            <input type="hidden" name="search_type" value="0">
            <div style="padding-top: 150px;" class="form_div tab_form">
                <div style="text-align: center;padding-bottom: 20px">
                    <h1> 1、请选择要发送的标签  </h1>
                    <div class="query_next hide">
                        搜索到<span class="query_total">0</span>个收件人
                        <button type="button query_button" form_id="tab_form">点击生成收件人组</button>
                    </div>
                </div>
                <div style="width: 50%;margin: 0 auto;padding-bottom: 10px">
                    <a class="change_mode change_mode0 hide" href="javascript:void(0);" onclick="query_mode(0)">切换为标签模式</a>
                    <a class="change_mode change_mode1" href="javascript:void(0);" onclick="query_mode(1)">切换为查询模式</a>
                </div>
                <div class="container">
                    <div class="input">
                        <input type="text" name="title" placeholder="" style="text-indent:15px;width:calc(100% - 90px);" id="query" autocomplete="on"></div>
                    <button class="tab_form_button" type="button" onclick="query_total('tab_form')">下一步</button>
                    <a class="clear" onclick="clear_input()">清空</a>
                    <!--export_sailing-->
                    <div class="card_wrap">
                        <a class="tag tag_all" onclick="query_all()" >选取所有</a>
                    </div>
                    <div class="card_wrap">
                        <?php foreach ($export_sailing as $items): ?>
                            <div style="padding-right: 8px;margin-bottom: 8px;">
                                <a class="tag" onclick="query_add('<?=$items; ?>')"><?=$items; ?></a>
                            </div>
                        <?php endforeach; ?>

                    </div>
                    <!--trans_mode-->
                    <div class="card_wrap">
                        <?php foreach ($trans_mode as $items): ?>
                            <div style="padding-right: 8px;margin-bottom: 8px;">
                                <a class="tag" onclick="query_add('<?=$items; ?>')"><?=$items; ?></a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!--product_type-->
                    <div class="card_wrap">
                        <?php foreach ($product_type as $items): ?>
                            <div style="padding-right: 8px;margin-bottom: 8px;">
                                <a class="tag" onclick="query_add('<?=$items; ?>')"><?=$items; ?></a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!--country_code-->
                    <div class="card_wrap">
                        <?php foreach ($country_code as $items): ?>
                            <div style="padding-right: 8px;margin-bottom: 12px;">
                                <a class="tag" onclick="query_add('<?=$items; ?>')"><?=$items; ?></a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <style>
                .card_wrap {
                    width: 100%;
                    display: flex;
                    flex-wrap: wrap;
                    justify-content: flex-start;
                    padding-top: 10px;
                }

                .tag {
                    border-radius: 20px;
                    padding-top: 4px;
                    padding-bottom: 4px;
                    padding-left: 8px;
                    padding-right: 8px;
                    background: #e8eaec;
                    color: #2d8cf0;
                    cursor: pointer;
                }

                .tag:hover {
                    background: #2d8cf0;
                    color: white;
                }

                .container {
                    position: relative;
                    width: 50%;
                    margin: 0 auto;
                }

                .input {
                    width: 100%;
                    height: 38px;
                    border-radius: 20px;
                    background-color: #dcdee2;
                    border: none;
                    font-size: 16px;
                    color: #17233d;
                }

                .container input {
                    width: 100%;
                    height: 38px;
                    border-radius: 20px 0px 0px 20px;
                    background-color: #dcdee2;
                    border: none;
                    font-size: 16px;
                    color: #17233d;
                    padding: 0;
                    outline: none;
                }

                .input:hover {
                    outline: #2d8cf0 solid 2px;
                }

                .clear:hover {
                    outline: #2d8cf0 solid 1px;
                }

                .tab_form_button {
                    position: absolute;
                    top: 0px;
                    right: -5px;
                    width: 90px;
                    background-color: #2d8cf0;
                    border: none;
                    outline: none;
                    cursor: pointer;
                    border-radius: 20px;
                    height: 40px;
                    line-height: 40px;
                    color: #ffffff;
                    font-size: 18px;
                }

                .clear {
                    position: absolute;
                    top: 0px;
                    right: -85px;
                    width: 70px;
                    background-color: #dcdee2;
                    border: none;
                    outline: none;
                    cursor: pointer;
                    border-radius: 20px;
                    height: 40px;
                    line-height: 40px;
                    color: #2d8cf0;
                    font-size: 18px;
                    /*padding-left: 30px;*/
                    text-align: center;
                }

            </style>
        </form>
        <form action="/crm_promote_mail/add_presend_by_search_send" method="POST" id="search_form">
            <input type="hidden" name="search_type" value="1">

            <div class="form_div search_form hide" style="padding-top: 150px;width: 100%">
                <div style="padding-bottom: 20px; margin-left: 500px;">
                    <h1> 1、请设定您要查询的范围  </h1>
                    <div class="query_next hide">
                        搜索到<span class="query_total">0</span>个收件人
                        <button type="button query_button" form_id="search_form">点击生成收件人组</button>
                    </div>
                </div>
                <div style="width: 50%;margin-left: 500px;padding-bottom: 10px">
                    <a class="change_mode change_mode0 hide" href="javascript:void(0);" onclick="query_mode(0)">切换为标签模式</a>
                    <a class="change_mode change_mode1" href="javascript:void(0);" onclick="query_mode(1)">切换为查询模式</a>
                </div>
                <div style="width:50%;margin-left: 500px;">
                    <table>
                        <tr>
                            <td><?= lang('客户简称'); ?> </td>
                            <td>
                                <input class="easyui-textbox" name="field[client_name]" id="client_name" style="width:255px;"/>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('客户地区');?></td>
                            <td>
                                <select class="easyui-combobox" name="field[province]" id="province"  data-options="
                                    valueField:'cityname',
                                    textField:'cityname',
                                    url:'/city/get_province',
                                    onSelect: function(rec){
                                        if(rec != undefined){
                                            $('#city').combobox('reload', '/city/get_city/' + rec.id);
                                        }
                                    }," style="width: 80px;"></select>

                                <select class="easyui-combobox" name="field[city]" id="city" style="width: 80px;" data-options="
                                        valueField:'cityname',
                                        textField:'cityname',
                                        onSelect: function(rec){
                                            if(rec != undefined){
                                                $('#area').combobox('reload', '/city/get_area/' + rec.id);
                                            }
                                        },
                                        onLoadSuccess:function(){
                                            var this_data = $(this).combobox('getData');
                                            var this_val = $(this).combobox('getValue');
                                            var rec = this_data.filter(el => el['cityname'] === this_val);
                                            if(rec.length > 0)rec = rec[0];
                                            $('#area').combobox('reload', '/city/get_area/' + rec.id);
                                        }
                                    "></select>

                                <select class="easyui-combobox" name="field[area]" id="area" style="width: 80px;" data-options="
                                        valueField:'cityname',
                                        textField:'cityname',
                                    "></select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('客户地址'); ?></td>
                            <td>
                                <input class="easyui-textbox" name="field[company_address]" id="company_address" style="width:255px;" />
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('航线区域'); ?></td>
                            <td>
                                <select class="easyui-combobox" name="field[sailing_area]" id="sailing_area" style="width:255px;" data-options="
                                        valueField:'sailing_area',
                                        textField:'sailing',
                                        url:'/biz_port/get_sailing?add_empty=true',
                                    ">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('出口航线');?></td>
                            <td>
                                <select class="easyui-combobox" id="export_sailing1" style="width: 255px;" data-options="
                                        editable:false,
                                        multiple:true,
                                        multivalue:false,
                                        valueField:'value',
                                        textField:'name',
                                        url:'/bsc_dict/get_option/export_sailing',
                                        onChange:function(newValue,oldValue){
                                            $('#export_sailing').val(newValue.join(','));
                                        }
                                    ">
                                </select>
                                <input type="hidden" name="field[export_sailing]" id="export_sailing">
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('运输模式');?></td>
                            <td>
                                <select class="easyui-combobox" id="trans_mode1" style="width: 255px;" data-options="
                                        editable:false,
                                        multiple:true,
                                        multivalue:false,
                                        valueField:'value',
                                        textField:'name',
                                        url:'/bsc_dict/get_option/client_trans_mode',
                                        onChange:function(newValue,oldValue){
                                            $('#trans_mode').val(newValue.join(','));
                                        }
                                    ">
                                </select>
                                <input type="hidden" name="field[trans_mode]" id="trans_mode" >
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('品名大类');?></td>
                            <td>
                                <select class="easyui-combobox" id="product_type1" style="width: 255px;" data-options="
                                        editable:false,
                                        multiple:true,
                                        multivalue:false,
                                        valueField:'value',
                                        textField:'name',
                                        url:'/bsc_dict/get_option/product_type',
                                        onChange:function(newValue,oldValue){
                                            $('#product_type').val(newValue.join(','));
                                        }
                                    ">
                                </select>
                                <input type="hidden" name="field[product_type]"  id="product_type" >
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('品名详情');?></td>
                            <td>
                                <textarea name="field[product_details]" id="product_details" style="height:60px;width:255px;"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('客户来源');?></td>
                            <td>
                                <select class="easyui-combobox" id="client_source1" style="width: 255px;" data-options="
                                        editable:false,
                                        multiple:true,
                                        multivalue:false,
                                        valueField:'value',
                                        textField:'name',
                                        url:'/bsc_dict/get_option/client_source',
                                        onChange:function(newValue,oldValue){
                                            $('#client_source').val(newValue.join(','));
                                        }
                                    ">
                                </select>
                                <input type="hidden" name="field[client_source]" id="client_source">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right"><button type="button" onclick="query_total('search_form')">点击提交到下一步</button></td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </div>

</body>
<script>
    //点击标签
    function query_add(val) {
        var query = $('#query').val();
        if(query==''){
            $('#query').val(val);
        }else{
            $('#query').val(query + ',' + val);
        }

    }

    //切换查询模式
    function query_mode(mode = 0) {
        $('.change_mode').removeClass('hide');
        $('.form_div').addClass('hide');
        query_total_show(0, false);
        if(mode == 0){
            $('.change_mode0').addClass('hide');
            $('.tab_form').removeClass('hide');
        }else if(mode == 1){
            $('.change_mode1').addClass('hide');
            $('.search_form').removeClass('hide');
        }
    }

    //快速选中全部
    function query_all() {
        $('#query').val('all');
        // $.each($('.tag'), function (i, it) {
        //     if($(it).hasClass('tag_all')) return true;
        //     $(it).trigger('click')
        // });
    }

    //点击查一查
    function query_total(form_id) {
        ajaxLoading();
        $('#' + form_id).form('submit', {
            url:'/crm_promote_mail/add_presend_by_search_send?get_total=true',
            success:function (res_json) {
                var res;
                ajaxLoadEnd();
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {
                    $.messager.alert('<?= lang('提示');?>', res_json);
                    return;
                }
                //$.messager.alert('<?//= lang('提示');?>//', res.msg);
                if(res.code == 0){
                    //提示存在多少条,并出现下一步按钮,点击进入总数页面
                    query_total_show(res['data']['total'], true);
                }else{
                }
            }
        });
    }

    //控制展示文本框的
    function query_total_show(total = 0, is_show = false) {
        $('.query_next').addClass('hide');
        $('.query_total').text(0);
        if(is_show){
            $('.query_total').text(total);
            $('.query_next').removeClass('hide');
        }else{

        }
    }

    //点击查一查
    function query(form_id) {
        $('#' + form_id).submit();
    }

    //点击清空
    function clear_input() {
        $('#query').val('');
    }

    $(function () {
        $('#tt').tree({
            onClick: function (node) {
                var title1 = node.text;
                var title2 = node.id;

                if (isNaN(title2)) {
                    var url = "/bsc_user/mail_list?title=" + title2 + '&type=1';
                    var content = '<iframe scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:100%"></iframe>';
                    $('#tabs').tabs('add', {
                        title: title1,
                        content: content,
                        closable: true
                    });

                } else {
                    var url = "/bsc_user/list_details?title=" + title2;
                    var content = '<iframe scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:100%"></iframe>';
                    $('#tabs').tabs('add', {
                        title: title1,
                        content: content,
                        closable: true
                    });
                }
            }
        });

        $('.query_button').on('click', function () {
            var form_id = $(this).attr('form_id');
            query(form_id);
        });
    });
</script>
</html>
