<?php
/**
 * shipment_container新增限制,
 * @param string $container_size
 * @param array $containers
 * @param int|array $shipment_id
 * @param int $id
 * @return bool|string
 */
function cz_shipment_container_limit($container_size, $shipment_id, $id = 0){
    $CI =& get_instance();
    if(is_array($shipment_id)){
        $shipment = $shipment_id;
    }else{
        $shipment = $CI->biz_shipment_model->get_duty_one("id = '$shipment_id'");
    }
    $consol_id = $shipment['consol_id'];
    if($consol_id == 0 && $shipment['parent_id'] == 0){
        return lang('当前shipment未绑定consol');
    }

    if(!empty($shipment)){
        $box_info = array_column(json_decode($shipment['box_info'], true), null, 'size');
        $box_info_size = array_column($box_info, 'size');
        //判断当前箱型是否存在shipment里
        if($shipment['trans_mode'] != 'LCL' && !in_array($container_size, $box_info_size)){
            return lang('箱型箱量超过最大值');
        }
        $containers = array_column($CI->biz_shipment_container_model->get_container_size_count($shipment['id'], 'bsc.id != ' . $id . ' and bc.consol_id = ' . $consol_id), null, 'container_size');

        //加上当前数量后是否超出
        $containers_size = array_column($containers, 'container_size');
        $key = array_search($container_size, $containers_size);
        if($shipment['trans_mode'] != 'LCL' && $key !== false && 1 + $containers[$container_size]['count'] > $box_info[$container_size]['num'])return lang('箱型箱量超过最大值');

        return true;
    }else{
        return false;
    }
}

function cz_consol_container_limit($data, $consol_id, $id = 0){
    $CI =& get_instance();
    $consol = Model('biz_consol_model')->get_duty_one("id = '{$consol_id}'");
    if(!isset($data['container_size'])) return lang('未获取到箱型');
    if(!isset($data['container_no'])) return lang('未获取到箱号');
    if (!empty($consol)) {
        $box_info = json_decode($consol['box_info'], true);
        $box_info_size = array_column($box_info, 'size');
        //判断当前箱型是否存在shipment里
        if ($consol['trans_mode'] != 'LCL' && !in_array($data['container_size'], $box_info_size)) {
            return lang('箱型箱量超过最大值');
        }
        $containers = Model('biz_container_model')->get_container_size_count($consol['id'], 'id != ' . $id, 'GROUP_CONCAT(id)');

        //加上当前数量后是否超出
        $containers_size = array_column($containers, 'container_size');
        $key = array_search($data['container_size'], $containers_size);
        if ($consol['trans_mode'] != 'LCL' && $key !== false && 1 + $containers[$key]['count'] > $box_info[$key]['num']) return lang('箱型箱量超过最大值');
        return true;
    } else {
        return false;
    }
}

/**
 * 将子shipment的container信息进行整合,添加或修改到父shipment里面
 * @param $shipment_id int
 * @param $is_check bool 删除多余的箱数据 新增修改不删，一键获取删除
 */
function cz_child_container_sum($shipment_id, $is_check = false){
    $CI =& get_instance();
    $CI->load->model('biz_shipment_container_model');
    $CI->load->model('biz_container_model');
    
    $CI->db->select('container_no, sum(packs) as packs, sum(weight) as weight, sum(volume) as volume');
    $CI->db->group_by('container_no');
    //$CI->db->force_master();
    $shipment_containers = $CI->biz_shipment_container_model->get("shipment_id in ((select id from biz_shipment where parent_id = $shipment_id))");

    //$CI->db->force_master();
    $parent_shipment_containers = array_column($CI->biz_shipment_container_model->get("shipment_id = $shipment_id"), null, 'container_no');

    $isset_container_no = array();
    foreach ($shipment_containers as $shipment_container){
        $insert_data = array();
        $insert_data['container_no'] = $shipment_container['container_no'];
        $insert_data['packs'] = $shipment_container['packs'];
        $insert_data['weight'] = $shipment_container['weight'];
        $insert_data['volume'] = $shipment_container['volume'];
        $insert_data['shipment_id'] = $shipment_id;

        $this_container_no = isset($parent_shipment_containers[$insert_data['container_no']]) ? $parent_shipment_containers[$insert_data['container_no']] : array();
        $isset_container_no[] = $insert_data['container_no'];
        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_shipment_container";
        //存在修改,不存在新增
        if(!empty($this_container_no)){
            $CI->biz_shipment_container_model->update($this_container_no['id'], $insert_data);
            $id = $this_container_no['id'];
            $log_data["action"] = "update";
        }else{
            $id = $CI->biz_shipment_container_model->save($insert_data);
            $log_data["action"] = "insert";
        }
        $insert_data['id'] = $id;

        $log_data["key"] = $id;
        $log_data["value"] = json_encode($insert_data);
        log_rcd($log_data);
    }

    //开启后,将不存在的箱号全部删除
    if($is_check){
        foreach ($parent_shipment_containers as $row){
            if(!in_array($row['container_no'], $isset_container_no) && !empty($shipment_containers)){
                $CI->biz_shipment_container_model->mdelete($row['id']);

                $log_data = array();
                $log_data["table_name"] = "biz_shipment_container";
                $log_data["key"] = $row['id'];
                $log_data["action"] = "delete";
                $log_data["value"] = json_encode($row);
                log_rcd($log_data);
            }
        }
    }
}

/**
 *  * 将子shipment的container信息进行整合,添加或修改到父shipment里面
 * @param $data
 * @param $shipment_id
 * @param int $consol_id 删除多余的箱数据 新增修改不删，一键获取删除
 * @return bool
 */
function cz_container_handle($data, $shipment_id, $consol_id = 0){
    $CI =& get_instance();
    if($data['id'] > 0){
        //查询是否有对应consol
        if(empty($consol_id)){
            $shipment = $CI->biz_shipment_model->get_duty_one("id = '{$shipment_id}'");
            $consol_id = $shipment['consol_id'];
        }
        $container_size = isset($data['container_size']) ? $data['container_size'] : '';
        $seal_no = isset($data['seal_no']) ? $data['seal_no'] : '';
        $container_tare = isset($data['container_tare']) ? $data['container_tare'] : 0;
        $vgm = isset($data['vgm']) ? $data['vgm'] : 0;
        if(empty($container_size)){
            return false;
        }
        if($consol_id == 0){//未绑定的
            return false;
        }else{
            $CI->load->model('biz_container_model');
            $CI->db->where('consol_id = ' . $consol_id);
            $container = $CI->biz_container_model->get_one('container_no', $data['container_no']);
            if(empty($container)){
                
                $container_save = array(
                    'consol_id' => $consol_id,
                    'container_no' => $data['container_no'],
                    'packs' => $data['packs'],
                    'weight' => $data['weight'],
                    'volume' => $data['volume'],
                    'container_tare' => $container_tare,
                    'vgm' => $vgm,
                    'container_size' => $container_size,
                    'seal_no' => $data['seal_no'],
                );
                $id = $CI->biz_container_model->save($container_save);

                //save the operation log
                $log_data = array();
                $log_data["table_name"] = "biz_container";
                $log_data["key"] = $id;
                $log_data["action"] = "insert";
                $log_data["value"] = json_encode($container_save);
                log_rcd($log_data);
            }else{
                $CI->db->select('SUM(packs) as packs, SUM(weight) as weight, SUM(volume) as volume');
                $CI->db->where("container_no = '{$data['container_no']}'");
               // $CI->db->force_master();
                $sum_container = $CI->biz_shipment_container_model->get_one('shipment_id in', '(select id from biz_shipment where consol_id = ' . $consol_id . ')', false);
                $container_update = array(
                    'packs' => $sum_container['packs'],
                    'weight' => $sum_container['weight'],
                    'volume' => $sum_container['volume'],
                );
                if(empty($container['vgm'])) $container_update['vgm'] = $vgm;
                // if(empty($container['container_size']))
                $container_update['container_size'] = $container_size;
                if(isset($seal_no) && $seal_no != '')$container_update['seal_no'] = $seal_no;
                foreach ($container_update as $ck => $cv){
                    if($container[$ck] == $cv) unset($container_update[$ck]);
                }
                if(!empty($container_update)){
                    $CI->biz_container_model->update($container['id'], $container_update);

                    //save the operation log
                    $log_data = array();
                    $log_data["table_name"] = "biz_container";
                    $log_data["key"] = $container['id'];
                    $log_data["action"] = "update";
                    $log_data["value"] = json_encode($container_update);
                    log_rcd($log_data);
                }
            }
            return true;
        }
    }
}

/**
 * 新增时自动判断子或父进行同步
 * @param array $data
 * @param int $shipment_id
 * @param int $consol_id
 */
function cz_container_add($data = array(), $shipment_id = 0, $consol_id = 0){
    $CI =& get_instance();
    //查询shipment号,查看是否有parent_id
    $CI->load->model('biz_shipment_model');
    $CI->db->select('parent_id');
    $shipment = $CI->biz_shipment_model->get_by_id($shipment_id);
    if($shipment['parent_id'] != 0){
        cz_child_container_sum($shipment['parent_id']);
    }
    cz_container_handle($data, $shipment_id, $consol_id);
}