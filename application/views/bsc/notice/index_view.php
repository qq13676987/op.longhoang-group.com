<style>
    .fs{
        width: 500px;
    }
    .fs_input{
        width: 255px;
    }
</style>
<script type="text/javascript">
    function doSearch(){
        $('#tt').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val() ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val() ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val()
        });
        $('#chaxun').window('close');
    }

    function add(){
        $("#add_notice").window({
            href:'/bsc_notice/add',
            modal:false,
        }).window('open');
    }
    var filter = {};
    $(function () {
        $('#tt').edatagrid({
            url: '/bsc_notice/get_data',
            saveUrl: '/bsc_notice/add_data',
            updateUrl: '/bsc_notice/update_data/',
            destroyUrl: '/bsc_notice/delete_data'
        });
        $('#tt').datagrid({
            modal:true,
            width: 'auto',
        	height: $(window).height(),
           onDblClickRow:function (index,row){
                $('#fm').form('load',row)
                $('#edit_notice').window('open')
           }
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
        $('#fm').on('keydown', '.textbox-text', function (event) {
            var input = $(this).parent('span').siblings();
            keydown(input, event);
        });
        $('#fm').on('blur', '.textbox-text', function (event) {
            var input = $(this).parent('span').siblings();
            if(input.hasClass('easyui-combobox')){
                var valueField = input.combobox('options').valueField;
                var val = input.combobox('getValue');
                var allData = input.combobox('getData');
                var result = true;
                if(Object.keys(filter).length != 0 && filter.hasOwnProperty(val) && filter[val].length >= 1 && filter[val][0]){
                    input.combobox('select', filter[val][0]);
                }else{
                    for (var i = 0; i < allData.length; i++) {
                        if (val == allData[i][valueField]) {
                            result = false;
                        }
                    }
                    if (result) {
                        input.combobox('clear');
                    }
                }
                filter = {};
            }
        })
    });
    function keydown(this_input, event) {
        if(event.keyCode == 9 || event.keyCode == 13){
            var input = this_input;

            if(input.hasClass('easyui-combobox')){
                var valueField = input.combobox('options').valueField;
                var val = input.combobox('getValue');
                var allData = input.combobox('getData');
                var result = true;
                if(Object.keys(filter).length != 0 && filter.hasOwnProperty(val) && filter[val].length >= 1){
                    input.combobox('select', filter[val][0]);
                }else{
                    for (var i = 0; i < allData.length; i++) {
                        if (val == allData[i][valueField]) {
                            result = false;
                        }
                    }
                    if (result) {
                        input.combobox('clear');
                    }
                }
                filter = {};
            }
            if(input.hasClass('vat')){
                save();
                return false;
            }
            if(event.keyCode == 13){
                let formValue = $('#fm').serializeArray();
                $.each(formValue, function(index, item) {
                    if(item.name == input.prop('id')){
                        var inp = $('#' + formValue[(index+1)].name);
                        if(inp.hasClass('easyui-combobox')){
                            inp.combobox('textbox').focus();
                        }else if(inp.hasClass('easyui-textbox')){
                            inp.textbox('textbox').focus();
                        }else if(inp.hasClass('easyui-numberbox')){
                            inp.numberbox('textbox').focus(function () {
                                this.select();
                            }).focus();
                        }
                    }
                });
            }
        }
    }
    function notice_user_for(value,row,index){
        return row.notice_user_name;
    }
    function notice_user_group_for(value,row,index){
        return row.notice_user_group_name;
    }
    function status_for(value,row,index){
        if(value == 0) return 'normal';
        if(value == 1) return 'disable';
    }
    function edit_notice(){
        if(!$('#fm').form('validate')){
            return;
        }
        let data = $('#fm').serializeArray()
        let obj = {}
        data.map(item=>{
            obj[item.name] = item.value
        })
        obj['notice_user'] = $('#add_notice_user').combotree('getValues').join(',')
        obj['notice_user_group'] = $('#add_notice_user_group').combotree('getValues').join(',')
        let url = '/bsc_notice/update_data/';
        $.post(url, obj,
            function(res){
                $.messager.alert('Tips',res.msg)
                if(res.code == 1){
                    $('#tt').datagrid('reload')
                    $('#edit_notice').window('close')
                }
            }, "json");
    }
</script>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th field="id" width="60" sortable="true"><?= lang('id')?></th>
<!--        <th field="type" width="100" editor="text">--><?//= lang('type')?><!--</th>-->
        <th field="start" width="100" ><?= lang('start')?></th>
        <th field="end" width="100"><?= lang('end')?></th>
<!--        <th field="rate" width="100" editor="text">--><?//= lang('rate')?><!--</th>-->
        <th field="txt" width="700" ><?= lang('txt')?></th>
        <th field="href" width="100" ><?= lang('href')?></th>
        <th field="notice_user" width="100" formatter="notice_user_for" ><?= lang('notice_user')?></th>
        <th field="notice_user_group" formatter="notice_user_group_for" width="150" ><?= lang('notice_user_group')?></th>
        <th field="status" width="60"  formatter="status_for"><?= lang('status')?></th>
        <th field="delay_num" width="100"><?= lang('delay_num')?></th>
        <th field="create_time" width="150" sortable="true"><?= lang('create_time')?></th>
        <th field="update_time" width="150" sortable="true"><?= lang('update_time')?></th>
        <th field="table_name" width="150" align="center" ><?= lang('table_name')?></th>
        <th field="id_no" width="100" align="center" ><?= lang('id_no')?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()"><?= lang('add');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#tt').edatagrid('destroyRow')"><?= lang('delete');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#tt').edatagrid('saveRow')"><?= lang('save');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#tt').edatagrid('cancelRow')"><?= lang('cancel');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>

            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                Query 1
            </td>
            <td align="right">
                <select name="f1"  id="f1"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1"  id="s1"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v1" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                Query 2
            </td>
            <td align="right">
                <select name="f2"  id="f2"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2"  id="s2"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v2" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                Query 3
            </td>
            <td align="right">
                <select name="f3"  id="f3"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s3"  id="s3"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v3" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;">Search</a>
</div>
<div id="add_notice" class="easyui-window" title="<?= lang('add');?>" closed="true" style="width:550px;height:355px;padding:5px;">
    <iframe id="add_notice_iframe" style="width:550px;height:355px;padding:5px;">
    
    </iframe>
</div>
<div id="edit_notice" class="easyui-window" title="<?= lang('edit');?>" closed="true" style="width:550px;height:355px;padding:5px;">
    <form id="fm">
        <input type="hidden" name="id">
        <table class="fs_table">
            <tr>
                <td><?= lang('start');?></td>
                <td>
                    <input class="easyui-datetimebox fs_input" name="start">
                </td>
            </tr>
            <tr>
                <td><?= lang('end');?></td>
                <td>
                    <input class="easyui-datetimebox fs_input" name="end">
                </td>
            </tr>
            <tr>
                <td><?= lang('txt');?></td>
                <td>
                    <textarea name="txt" class="fs_input" style="height:80px;"></textarea>
                </td>
            </tr>
            <tr>
                <td><?= lang('notice_user');?></td>
                <td>
                    <select name="notice_user" id="add_notice_user" class="easyui-combotree fs_input" data-options="
                    editable:false,
                    multiple:true,
                    valueField:'id',
                    textField:'text',
                    cascadeCheck: false,
                    url:'/bsc_user/get_user_tree/',
                    onBeforeCheck: function(node,checked){
                        var values = $('#add_notice_user').combotree('getValues');
                        if(checked && node.children != undefined){
                            $.each(node.children, function(index, item){
                                if(item.children == undefined && $.inArray(item.id, values) == -1){
                                    values.push(item.id);
                                }else{
                                    $.each(item.children, function(i, it){
                                        if(it.children == undefined && $.inArray(it.id, values) == -1)values.push(it.id);

                                    })
                                }
                            });
                            $('#notice_user').combotree('setValues', values);
                            return false;
                        }
                    },
                ">

                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('notice_user_group');?></td>
                <td>
                    <select class="easyui-combotree fs_input" name="notice_user_group" id="add_notice_user_group" editable="false" data-options="
                    multiple:true,
                    valueField:'id',
                    textField:'text',
                    url:'/bsc_user/get_user_tree1',

                ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('status');?></td>
                <td>
                    <input type="radio" name="status" value="0" checked><?= lang('normal');?>
                    <input type="radio" name="status" value="1"><?= lang('disable');?>
                </td>
            </tr>
            <tr>
                <td><?= lang('delay_num');?></td>
                <td>
                    <input class="easyui-numberbox fs_input" name="delay_num">
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: right;">
                    <button type="button" onclick="edit_notice()" style="margin-right: 30px;"><?= lang('submit');?></button>
                </td>
            </tr>
        </table>
    </form>
</div>