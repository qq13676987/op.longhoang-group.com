<?php

class biz_bill_invoice extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_bill_invoice_model');
        $this->model = $this->biz_bill_invoice_model;
    }
    
    public function get_invoice($id = 0){
        $data = $this->biz_bill_invoice_model->get_one('id', $id);
        echo json_encode(array('code' => 0, 'msg' => 'success' , 'data' => $data));
    }
}