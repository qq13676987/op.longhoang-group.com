<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>consol-Add</title>

<script type="text/javascript">
    var is_special_char = '';

    function qzb(str){
        str = str.replace(new RegExp('·', "g"), '`');
        str = str.replace(new RegExp('！', "g"), '!');
        str = str.replace(new RegExp('￥', "g"), '$');
        str = str.replace(new RegExp('【', "g"), '[');
        str = str.replace(new RegExp('】', "g"), ']');
        str = str.replace(new RegExp('】', "g"), ']');
        str = str.replace(new RegExp('；', "g"), ';');
        str = str.replace(new RegExp('：', "g"), ':');
        str = str.replace(new RegExp('“', "g"), '"');
        str = str.replace(new RegExp('”', "g"), '"');
        str = str.replace(new RegExp('’', "g"), '\'');
        str = str.replace(new RegExp('‘', "g"), '\'');
        str = str.replace(new RegExp('、', "g"), '');
        str = str.replace(new RegExp('？', "g"), '?');
        str = str.replace(new RegExp('《', "g"), '<');
        str = str.replace(new RegExp('》', "g"), '>');
        str = str.replace(new RegExp('，', "g"), ',');
        str = str.replace(new RegExp('。', "g"), '.');
        str = str.replace(new RegExp(' ', "g"), ' ');
        str = str.replace(new RegExp('　', "g"), ' ');
        str = str.replace(new RegExp('（', "g"), '(');
        str = str.replace(new RegExp('）', "g"), ')');
        return str;
    }

    /**
     * 下拉选择表格展开时,自动让表单内的查询获取焦点
     */
    function combogrid_search_focus(form_id, focus_num = 0){
        // var form = $(form_id);
        setTimeout("easyui_focus($($('" + form_id + "').find('.keydown_search')[" + focus_num + "]));",200);
    }

    /**
     * easyui 获取焦点
     */
    function easyui_focus(jq){
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combogrid !== undefined){
            return $(jq).combogrid('textbox').focus();
        }else if(data.combobox !== undefined){
            return $(jq).combobox('textbox').focus();
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('textbox').focus();
        }else if(data.datebox !== undefined){
            return $(jq).datebox('textbox').focus();
        }else if(data.textbox !== undefined){
            return $(jq).textbox('textbox').focus();
        }
    }

    function changeClient(data){
        var oversea_cus_ids = data['oversea_cus_ids'];
        if(oversea_cus_ids === undefined || oversea_cus_ids == '')oversea_cus_ids = 0;
        $('#oversea_cus_id').combobox('reload', '/bsc_user/get_data_role/oversea_cus?userIds=' + oversea_cus_ids + '&status=0');

        var biz_type = $('#biz_type1').combobox('getValue');
        
        // data.client_code = "leagueshipping";
        $('#shipper_company2').combogrid('grid').datagrid('reload', '/biz_company/get_option/shipper/'+data.client_code + '/' + biz_type);
        $('#consignee_company2').combogrid('grid').datagrid('reload', '/biz_company/get_option/consignee/'+data.client_code + '/' + biz_type);
        $('#notify_company2').combogrid('grid').datagrid('reload', '/biz_company/get_option/notify/'+data.client_code + '/' + biz_type);
    }
    function goEditUrl(url){
        var clientCode = $('#agent_code').val();
        if(!clientCode){
            alert('clientCode is empty');
        }else{
            window.open(url+'/'+clientCode)
        }
    }

    function setValueRT(val){
        if(val == null) val = '';
        return val.replace(new RegExp('T', "g"), ' ');
    }


    function add_box() {
        var length = $('.add_box').length;
        var lang = '';
        var btn = '';
        if(length <= 0){
            lang = "<?= lang('box_info');?>";
            btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
        }else{
            btn = '<a class="easyui-linkbutton" onclick="del_box(this)" iconCls="icon-remove"></a>';
        }
        var str = '<tr class="add_box" >\n' +
            '                            <td>' + lang + '</td>\n'+
            '                            <td>\n'+
            '                                <select class="easyui-combobox box_info_size" name="box_info[size][]" data-options="\n'+
            '                                valueField:\'value\',\n'+
            '                                textField:\'value\',\n'+
            '                                url:\'/bsc_dict/get_option/container_size\',\n'+
            "                                onHidePanel: function() {  \n" +
            "                                   var valueField = $(this).combobox('options').valueField;  \n" +
            "                                   var val = $(this).combobox('getValue');  \n" +
            "                                   var allData = $(this).combobox('getData');  \n" +
            "                                   var result = true;  \n" +
            "                                   for (var i = 0; i < allData.length; i++) {  \n" +
            "                                       if (val == allData[i][valueField]) {  \n" +
            "                                           result = false;  \n" +
            "                                       }  \n" +
            "                                   }  \n" +
            "                                   if (result) {  \n" +
            "                                       $(this).combobox('clear');  \n" +
            "                                   } " +
            "                                }" +
            '                                "></select>\n'+
            '                                X\n'+
            '                                <input class="easyui-numberbox box_info_num" name="box_info[num][]">\n'+
            '                                ' + btn + '\n'+
            '                            </td>\n'+
            '                        </tr>';
        if(length == 0){
            $('#trans_mode').parent('td').parent('tr').after(str);
        }else{
            $('.add_box:eq(' + (length - 1) + ')').after(str);
        }
        $.parser.parse('.add_box:eq(' + length + ')');
    }

    function del_box(e) {
        if(e == undefined){
            $('.add_box').remove();
        }else{
            $(e).parent('td').parent('tr').remove();
        }
    }

    function reload_select(id = '', type='combobox'){
        if(type == 'combobox'){
            var agent_company = $('#agent_company').combogrid('getValue');
            $('#agent_company').combogrid('clear').combogrid('grid').datagrid('selectRecordField', {field:'company_name', value:agent_company});
            $('#vessel').combobox('reload');
        }

        if(id == 'shipper_company2') $('#shipper_company2').combogrid('grid').datagrid('reload');
        if(id == 'consignee_company2') $('#consignee_company2').combogrid('grid').datagrid('reload');
        if(id == 'notify_company2') $('#notify_company2').combogrid('grid').datagrid('reload');
    }
    function add_free_svr() {
        var length = $('.free_svr').length;

        var btn = '';
        var lang = '';
        if(length < 4){
            if(length == 0){
                lang = '<?= lang('free svr');?>';
            }
            if(length < 3){
                btn = '<a class="easyui-linkbutton" onclick="add_free_svr(this)" iconCls="icon-add"></a>';
            }
        }else{
            return;
        }
        var str = '  <tr class="free_svr">\n' +
            '                                <td>' + lang + '</td>\n' +
            '                                <td>\n'+
            '                                    <select editable="false" class="easyui-combobox free_svr_type" name="free_svr[' + length + '][type]" data-options="'+
            '                                        valueField:\'value\',\n'+
            '                                        textField:\'name\',\n'+
            '                                        url: \'/bsc_dict/get_option/free_svr\',\n' +
            '                                        value: \'\',\n'+
            '                                        onSelect: function(rec){\n' +
            '                                            free_svr_type_jc();\n' +
            '                                        },' +
            '                                    ">\n'+
            '                                    </select>\n'+
            '                                    <input class="easyui-numberbox free_svr_days" name="free_svr[' + length + '][days]" ><?= lang('days');?>\n'+
            btn +
            '                                </td>\n'+
            '                            </tr>';

        $('.free_svr:eq(' + (length - 1) + ')').after(str);
        $.parser.parse('.free_svr:eq(' + length + ')');
    }

    function free_svr_type_jc() {
        var free_svr_type = $('.free_svr_type')
        var data = [];
        var jc = false;
        $.each(free_svr_type, function(index,item){
            var val = $(this).combobox('getValue');
            if(val != ''){
                if($.inArray(val, data) === -1){
                    data.push(val);
                }else{
                    jc = true;
                }
            }

        });
        if(jc === true){
            free_svr_cf = true;
        }else{
            free_svr_cf = false
        }
    }

    function checkName(val, type = 0){
        // var reg = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");\
        if(type == 0){
            var reg = new RegExp(/[^\x00-\x7f]/g);
        }else{
            var reg = new RegExp(/[^\x00-\x7f|\u4e00-\u9fa5]/g);
        }
        var rs = "";
        for (var i = 0, l = val.length; i < val.length; i++) {
            if(val.substr(i, 1).match(reg)) rs += val.substr(i, 1);
        }
        return rs;
    }
    
    function trans_carrier_select(rec){
        var trans_mode = $('#trans_mode').val();
        var trans_tool = $('#trans_tool').combobox('getValue');
        $('#trans_origin').combogrid('grid').datagrid('load', "/biz_port/get_option?type=" + trans_tool + "&carrier=" + rec.client_code + '&limit=true');
        $('#trans_discharge').combogrid('grid').datagrid('reload', "/biz_port/get_option?type=" + trans_tool + "&carrier=" + rec.client_code + '&limit=true');

        $('#trans_destination').combogrid('grid').datagrid('reload', "/biz_port/get_discharge?type=" + trans_tool + "&carrier=" + rec.client_code);

        $('#trans_origin').combogrid('clear');
        $('#trans_origin_name').textbox('clear');
        $('#trans_discharge').combogrid('clear');
        $('#trans_discharge_name').textbox('clear');
        $('#trans_destination_inner').textbox('clear');
        $('#trans_destination').combogrid('clear');
        $('#trans_destination_name').textbox('clear');
        $('#trans_destination_terminal').textbox('clear');

        if(rec.client_name == 'TSL'){
            $('#hs_code').combobox('reload', '/bsc_dict/get_option/hs_code?ext1=TSL');
        }else{
            $('#hs_code').combobox('options').url = null
            $('#hs_code').combobox('loadData', {});
        }

        $('#trans_carrier_second').combobox('setValue', rec.client_code);
        // if(rec.client_code == 'JXHY01'){//KML 显示第二承运人
        //     $('.trans_carrier_second').removeClass('hide');
        //
        //     $('#agent_company').combogrid('enable');
        //     $('#agent_address').attr('disabled', false);
        //     $('#agent_contact').attr('disabled', false);
        //     $('#agent_telephone').attr('disabled', false);
        //     $('#agent_code').attr('disabled', false);
        // }else{
        //     $('.trans_carrier_second').addClass('hide');
        //
        //     $('#agent_company').combogrid('disable');
        //     $('#agent_address').attr('disabled', true);
        //     $('#agent_contact').attr('disabled', true);
        //     $('#agent_telephone').attr('disabled', true);
        //     $('#agent_code').attr('disabled', true);
        // }
    }

    function trans_carrier_rule_open(){
        var trans_carrier = $('#trans_carrier').combobox('getValue');
        $('#trans_carrier_rule_iframe').attr('src', '/bsc_help_content/help_carrier/' + trans_carrier + '_CARRIER_REMIND');
        $('#trans_carrier_rule').window('open');
    }

    function biz_type_select(res){
        //当 export_FCL_SEA 的时候，跳转至 专属页面

        var trans_carrier = $('#trans_carrier').combobox('getValue');

        $('#biz_type').val(res.biz_type);
        $('#trans_mode').val(res.trans_mode);

        if(res.trans_mode == 'AIR'){
            $('#trans_tool').combobox('select', 'AIR');
            $('#flight_number').html('<?= lang('flight_number');?>');
            $('#vessel').textbox({ readonly: true });
            document.getElementById('update_vessel').style.display = 'none';
        }else{
            $('#trans_tool').combobox('select', 'SEA');
            $('#flight_number').html('<?= lang('update_voyage');?>');
            $('#vessel').textbox({ readonly: false });
            document.getElementById('update_vessel').style.display = '';
        }
        var trans_tool = $('#trans_tool').combobox('getValue');

        //重新加载
        $('#trans_origin').combogrid('grid').datagrid('load', "/biz_port/get_option?type=" + trans_tool + '&carrier=' + trans_carrier + '&limit=true');
        $('#trans_discharge').combogrid('grid').datagrid('reload', "/biz_port/get_option?type=" + trans_tool + '&carrier=' + trans_carrier + '&limit=true');

        $('#trans_destination').combogrid('grid').datagrid('reload', "/biz_port/get_discharge?type=" + trans_tool + '&carrier=' + trans_carrier);
        
        if(res.trans_mode == 'FCL'){
            $('#creditor').combobox('readonly', true);
            // $('#sailing_code').combobox('readonly', true);
        }else{
            $('#creditor').combobox('readonly', false);
            // $('#sailing_code').combobox('readonly', false);
        }
        
        if(res.trans_mode == 'LCL'){
            $('#LCL_type').parent('td').parent('tr').css('display', '');
        }else{
            $('#LCL_type').parent('td').parent('tr').css('display', 'none');
        }

        //HBL 且LCL时,显示自拼箱按钮
        if($('#agent_company').prop('disabled') !== false &&  res.trans_mode === 'LCL') {
            $('.lcl_zpx').removeClass('hide');
        }else {
            $('.lcl_zpx').addClass('hide');
        }

        if(res.trans_mode == 'FCL' || res.trans_mode == 'LCL'){
            add_box();
        }else{
            del_box();
        }
    }
    
    function trans_tool_select(rec){
        //当 export_FCL_SEA 的时候，跳转至 专属页面
        var new_url = $('#biz_type').val()+"_"+$('#trans_mode').val()+"_"+rec.value;

        if(rec.value == 'AIR'){
            //当运输工具是AIR，以下格子的名称做对应的修改：
            //2021-06-04 16:00 汪庭彬
            $('#vessel').parent('td').siblings('td').text('航班');
            $('#trans_carrier').parent('td').siblings('td').text('航空公司');
            $('#carrier_ref').parent('td').siblings('td').text('主单号');
            $('#booking_ETD').parent('td').siblings('td').text('航班时间');
        }else{
            $('#vessel').parent('td').siblings('td').text('<?= lang('vessel');?>');
            $('#trans_carrier').parent('td').siblings('td').text('<?= lang('trans_carrier');?>');
            $('#carrier_ref').parent('td').siblings('td').text('<?= lang('carrier_ref');?>');
            $('#booking_ETD').parent('td').siblings('td').text('<?= lang('booking_ETD');?>');
        }
    }
    
    //2021-05-27 11:00:00 新需求
    function LCL_type_select(rec){
        //如果选择KML CONSOL，则操作只能选择operator_LCL
        //1、 trans_mode 等于LCL 
        //2、 LCL_type 
        //2.1、等于KML CONSOL时，操作格子只能选择operator_LCL
        //2.2、等于其他的，操作格子只能选择operator
        var trans_mode = $('#trans_mode').val();
        var LCL_type = $('#LCL_type').combobox('getValue');
        var operator_id_input = $('#operator_id');
        var operator_option = operator_id_input.combobox('options');
        if(trans_mode == 'LCL'){
            if(LCL_type == 'KML CONSOL'){
                if(operator_option.url != '/bsc_user/get_data_role/operator_LCL?status=0') operator_id_input.combobox('reload', '/bsc_user/get_data_role/operator_LCL?status=0');
            }else{
                if(operator_option.url != '/bsc_user/get_data_role/operator?status=0') operator_id_input.combobox('reload', '/bsc_user/get_data_role/operator?status=0');
            }
        }
    }
    
    function booking_ETD_select(date){
        $('#quotation_search').combogrid('setValue', '0');
        $('#quotation_id').val('0');
        $('#floor_price').textbox('clear');
    }
    
    function creditor_select(rec) {
        var time = new Date().getTime();
        if(rec.stop_date != '0000-00-00 00:00:00'){
            //判断时间是否大于当前时间,如果大于当前时间,那么提示过期无法选择,请申请延期
            var stop_date_time = new Date(rec.stop_date).getTime();

            //如果当前时间小于停止时间,那么弹窗提示
            // if(time > stop_date_time){
            //     $.messager.alert('Tips', rec.company_name + '已过期无法选择,请到往来单位申请延期');
            //     $('#creditor').combobox('clear');
            //     return false;
            // }else{
            //     return true;
            // }
        }
    }

    function create_company_tool(div_id, id){
        var html = '<div id="' + div_id + '_div">' +
            '<form id="' + div_id + '_form" method="post">' +
            '<div style="padding-left: 5px;display: inline-block;">' +
            '<label><?= lang('company_name');?>:</label><input name="company_name" class="easyui-textbox keydown_search" t_id="' + id + '" style="width:96px;"/>' +
            '</div>' +
            '<form>' +
            '<div style="padding-left: 1px; display: inline-block;">' +
            '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="' + id + '_click" onclick="query_report(\'' + div_id + '\', \'' + id + '\')"><?= lang('search');?></a>' +
            '</div>' +
            '</div>';
        $('body').append(html);
        $.parser.parse('#' + div_id + '_div');
        return div_id;
    }

    function query_report(div_id, load_id){
        var where = {};
        var form_data = $('#' + div_id + '_form').serializeArray();
        $.each(form_data, function (index, item) {
            if(item.value == "") return true;
            if(where.hasOwnProperty(item.name) === true){
                if(typeof where[item.name] == 'string'){
                    where[item.name] =  where[item.name].split(',');
                    where[item.name].push(item.value);
                }else{
                    where[item.name].push(item.value);
                }
            }else{
                where[item.name] = item.value;
            }
        });
        if(where.length == 0){
            $.messager.alert('Tips', '请填写需要搜索的值');
            return;
        }
        $('#' + load_id).combogrid('grid').datagrid('load',where);
    }

    //text添加输入值改变
    function keydown_search(e, click_id){
        if(e.keyCode == 13){
            $('#' + click_id + '_click').trigger('click');
        }
    }

    /**
     * 获取当前控件的指定数据, 默认为获取值
     * @param target
     * @param method
     * @returns {jQuery|*|*|jQuery}
     */
    function getVal(target, method = 'getValue') {
        var data = $.data(target);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combobox !== undefined){
            return $(target).combobox(method);
        }else if(data.datetimebox !== undefined){
            return $(target).datetimebox(method);
        }else if(data.datebox !== undefined){
            return $(target).datebox(method);
        }else if(data.textbox !== undefined){
            return $(target).textbox(method);
        }else{
            if(method == 'getValue') return $(target).val();
            if(method == 'clear') return  $(target).val('');
            else return false;
        }
    }

    var is_message = false;

    $(function(){
        //初始值
        $('#shipper_company').val('<?= es_encode($shipper_company);?>');
        $('#consignee_company').val('<?= es_encode($consignee_company);?>');
        $('#notify_company').val('<?= es_encode($notify_company);?>');
        
        $('#vessel').combobox('reload', '/bsc_dict/get_option/vessel');
        //MBL 离开焦点时,检测MBL是否有输入过
        $('#carrier_ref').textbox('textbox').blur(function (e) {
            var val = $('#carrier_ref').textbox('getValue');
            $.ajax({
                type:'POST',
                url:'/biz_consol/select_value_exist',
                data:{
                    field:'agent_ref',
                    value:val,
                },
                dataType: 'json',
                success: function (res) {
                    if(res.code == 0){
                        $('#carrier_ref_tip').text(res.data.job_no);
                    }else{
                        $('#carrier_ref_tip').text('');
                    }
                }
            });
        });

        $('#agent_ref').textbox('textbox').blur(function (e) {
            var val = $('#agent_ref').textbox('getValue');
            $.ajax({
                type:'POST',
                url:'/biz_consol/select_value_exist',
                data:{
                    field:'agent_ref',
                    value:val,
                },
                dataType: 'json',
                success: function (res) {
                    if(res.code == 0){
                        $('#agent_ref_tip').text(res.data.job_no + '重复');
                    }else{
                        $('#agent_ref_tip').text('');
                    }
                }
            });
        });
        $('#voyage').combobox('textbox').blur(function (e) {
            var this_data = $('#voyage').combobox('getData');
            var this_val = $('#voyage').combobox('textbox').val().toUpperCase();
            var rec = this_data.filter(el => el['leaveVoyNo'] === this_val);
            // if(this_val != '' && rec.length == 0){
            //     $.messager.alert('Tips', '你输入的航次不在港区网站中，可能输入错误，请仔细检查。');
            // }
        });

        $('#customer_booking').click(function () {
            if($(this).prop('checked') === true){//如果勾选 代理清空 disabled 填入客户自订舱
                $('#creditor').combobox('setValue', '客户自订舱').combobox('readonly', true);
                $('input[name="customer_booking"]').val($(this).val());
            }else{
                $('#creditor').combobox('readonly', false).combobox('clear');
                $('input[name="customer_booking"]').val(' ');
            }
        });

        //实现表格下拉框的分页和筛选--start
        $('#agent_company').combogrid("reset");
        $('#agent_company').combogrid({
            panelWidth: '600px',
            panelHeight: '300px',
            idField: 'company_name',              //ID字段
            textField: 'company_name',    //显示的字段
            // multiple:true,
            fitColumns : true,
            striped: true,
            editable: false,
            pagination: false,           //是否分页
            // pageList : [30,50,100],
            // pageSize : 30,
            toolbar : '#agentTb',
            rownumbers: true,           //序号
            collapsible: true,         //是否可折叠的
            value: '<?= $agent_company;?>',
            method: 'post',
            columns:[[
                {field:'country_code',title:'<?= lang('country_code');?>',width:60},
                {field:'company_name',title:'<?= lang('agent_company');?>',width:250},
                {field:'remark',title:'<?= lang('remark');?>',width:250},
            ]],
            emptyMsg : '未找到相应数据!',
            onBeforeSelect:function(index, row){
                // if(row.stop_bool==false){
                //     alert('代理已过期请重新设置');
                //     return false;
                // }
            },
            onSelect : function(index, row){
                $('#agent_address').val(row.company_address);
                $('#agent_contact').val(row.contact_name);
                $('#agent_telephone').val(row.contact_telephone2);
                $('#agent_code').val(row.client_code);
                $('#agent_email').textbox('setValue', row.contact_email);
                changeClient(row);
            },
            onLoadSuccess : function(data) {
                var clientCode = $('#agent_code').val();
                var this_data = data.rows;
                var rec = this_data.filter(el => el['client_code'] === clientCode);
                
                if(clientCode){
                    if(rec.length > 0 ){
                        changeClient(rec[0]);
                    }else{
                        changeClient({client_code:clientCode});
                    }
                }else{
                    changeClient({client_code:clientCode});
                }
                var opts = $(this).combogrid('grid').datagrid('options');
                var vc = $(this).combogrid('grid').datagrid('getPanel').children('div.datagrid-view');
                vc.children('div.datagrid-empty').remove();
                if (!$(this).combogrid('grid').datagrid('getRows').length) {
                    var d = $('<div class="datagrid-empty"></div>').html(opts.emptyMsg || 'no records').appendTo(vc);
                    d.css({
                        position : 'absolute',
                        left : 0,
                        top : 50,
                        width : '100%',
                        fontSize : '14px',
                        textAlign : 'center'
                    });
                }
            },
            queryParams:{
                trans_carrier:$("#trans_carrier").combobox("getValue"),
                trans_destination:$("#trans_destination_inner").textbox("getValue"),
            },
            url:'/biz_client/get_agent?sales_ids=<?= $sales_id;?>',
            // url:'/biz_client/get_consol_agent',
        });
        //点击搜索
        $('#agentQueryReport').click(function(){
            var where = {};
            var form_data = $('#agentTbform').serializeArray();
            $.each(form_data, function (index, item) {
                if(where.hasOwnProperty(item.name) === true){
                    if(typeof where[item.name] == 'string'){
                        where[item.name] =  where[item.name].split(',');
                        where[item.name].push(item.value);
                    }else{
                        where[item.name].push(item.value);
                    }
                }else{
                    where[item.name] = item.value;
                }
            });
            // where['field'] = $('#to_search_field').combo('getValue');
            //2022-07-01 新增
            var trans_carrier = $("#trans_carrier").combobox("getValue");
            var trans_destination = $("#trans_destination_inner").textbox("getValue");
            where['trans_carrier']=trans_carrier;
            where['trans_destination']=trans_destination;
            $('#agent_company').combogrid('grid').datagrid('load',where);
        });
        //text添加输入值改变
        $('.agent_search').textbox('textbox').keydown(function (e) {
            if(e.keyCode == 13){
                $('#agentQueryReport').trigger('click');
            }
        });

        //2022-07-21 自出单
        $('#selfonclick').click(function () {
            $('#agent_company').combogrid('setValue', ' ');
            $('#agent_code').val('OtherAgent');
            $('#pre_alert').val('<?=date('Y-m-d H:i:s', time());?>');
            $('#agent_company').combogrid('hidePanel');
            alert('agent_company为空，代理地址可以填写公司名和地址');
        });
        //实现表格下拉框的分页和筛选-----end
        var company_config = {
            panelWidth: '1000px',
            panelHeight: '300px',
            idField: 'id',              //ID字段
            textField: 'company_name',    //显示的字段
            striped: true,
            editable: false,
            pagination: false,           //是否分页
            toolbar : '#' + create_company_tool('shipper_company2', 'shipper_company2') + '_div',
            collapsible: true,         //是否可折叠的
            method: 'post',
            columns:[[
                {field:'company_name',title:'<?= lang('company_name');?>',width:200},
                {field:'company_address',title:'<?= lang('company_address');?>',width:400},
                {field:'company_contact',title:'<?= lang('company_contact');?>',width:100},
                {field:'company_telephone',title:'<?= lang('company_telephone');?>',width:100},
                {field:'company_email',title:'<?= lang('company_email');?>',width:100},
            ]],
            emptyMsg : '未找到相应数据!',
            mode: 'remote',
            onSelect: function(index, row){
                if(row.company_address == '') row.company_address = ' ';
                if(row.company_contact == '') row.company_contact = ' ';
                if(row.company_telephone == '') row.company_telephone = ' ';
                if(row.company_email == '') row.company_email = ' ';
                $('#shipper_company').val(row.company_name);
                $('#shipper_address').val(row.company_address);
                $('#shipper_contact').val(row.company_contact);
                $('#shipper_telephone').val(row.company_telephone);
                $('#shipper_email').val(row.company_email);
            },
            value:'<?= es_encode($shipper_company);?>',
        };
        $('#shipper_company2').combogrid("reset");
        $('#shipper_company2').combogrid(company_config);

        company_config.toolbar = '#' + create_company_tool('consignee_company2', 'consignee_company2') + '_div';
        company_config.onSelect = function(index, row){
            if(row.company_address == '') row.company_address = ' ';
            if(row.company_contact == '') row.company_contact = ' ';
            if(row.company_telephone == '') row.company_telephone = ' ';
            if(row.company_email == '') row.company_email = ' ';
            $('#consignee_company').val(row.company_name);
            $('#consignee_address').val(row.company_address);
            $('#consignee_contact').val(row.company_contact);
            $('#consignee_telephone').val(row.company_telephone);
            $('#consignee_email').val(row.company_email);
        };
        company_config.value = '<?= es_encode($consignee_company);?>';
        $('#consignee_company2').combogrid("reset");
        $('#consignee_company2').combogrid(company_config);

        company_config.toolbar = '#' + create_company_tool('notify_company2', 'notify_company2') + '_div';
        company_config.onSelect = function(index, row){
            if(row.company_address == '') row.company_address = ' ';
            if(row.company_contact == '') row.company_contact = ' ';
            if(row.company_telephone == '') row.company_telephone = ' ';
            if(row.company_email == '') row.company_email = ' ';
            $('#notify_company').val(row.company_name);
            $('#notify_address').val(row.company_address);
            $('#notify_contact').val(row.company_contact);
            $('#notify_telephone').val(row.company_telephone);
            $('#notify_email').val(row.company_email);
        };
        company_config.value = '<?= es_encode($notify_company);?>';
        $('#notify_company2').combogrid("reset");
        $('#notify_company2').combogrid(company_config);

        $.each($('.keydown_search'), function (i,it) {
            $(it).textbox('textbox').bind('keydown', function(e){keydown_search(e, $(it).attr('t_id'));});
        });
        
        

        $('#f').form({
            url: '/biz_consol/add_data_ajax/',
            onSubmit: function(){
                var vv = $('#carrier_ref_tip').text();
                if(vv !== ''){
                    alert("船公司单号已存在！");
                    return false;
                }
                var vv = $('#agent_ref_tip').text();
                if(vv !== ''){
                    alert("代理单号已存在！");
                    return false;
                }
                $.each($('textarea'), function(index, item){
                    var str = $(this).val();
                    var id = $(this).prop('id');
                    var ids = {
                        agent_address: ['agent_address', '<?= lang('代理地址');?>'],
                        agent_contact: ['agent_contact', '<?= lang('代理联系人');?>'],
                        agent_telephone: ['agent_telephone', '<?= lang('代理电话');?>'],
                        shipper_address: ['shipper_address', '<?= lang('发货人地址');?>'],
                        consignee_address: ['consignee_address', '<?= lang('收货人地址');?>'],
                        notify_address: ['notify_address', '<?= lang('通知人地址');?>'],
                        description: ['description', '<?= lang('英文货物描述');?>'],
                        mark_nums: ['mark_nums', '<?= lang('货物唛头');?>'],
                    };
                    str = qzb(str);
                    $(this).val(str);
                    var newstr = $(this).val();
                    if(ids[id] !== undefined){
                        var special_char = checkName(newstr);
                    }else{
                        var special_char = checkName(newstr, 1);
                    }
                    if(special_char !== ''){
                        $.messager.alert('error', ids[id][1] + '检测到有以下特殊字符:<br/>' + special_char);
                        is_special_char = id;
                        return false;
                    }else{
                        is_special_char = '';
                        $(this).val($(this).val().toUpperCase());
                    }
                });
                if(is_special_char != ''){
                    // alert(is_special_char + '存在特殊字符');
                    return false;
                }
                $('.icon-save').attr('onclick', "");
                return true;
            },
            success:function(res_json){
                try{
                    var res = $.parseJSON(res_json);

                    if(res.code !== 444)alert(res.msg);
                    $('#hs_code_tip').html('');
                    $('.icon-save').attr('onclick', "$('#f').form('submit');");
                    if(res.code == 0){
                        try{opener.$('#tt').edatagrid('reload');}catch (e) {}
                        location.href = '/biz_consol/edit/' + res.data.id;
                    }else if(res.code == 2){//代码tsl hscode错误
                        $('#hs_code_tip').html('<a href="/bsc_dict/read/hs_code/TSL" target="_blank" style="color:red;">hs_code</a>');
                    }else if(res.code == 444){
                        // GET方式示例，其它方式修改相关参数即可
                        var form = $('<form></form>').attr('action','/other/ts_text').attr('method','POST').attr('target', '_blank');
                        var json_data = {};
                        json_data['text'] = res.text;
                        json_data['ts_str[]'] = res.ts_str;
                        json_data['msg'] = res.msg;
                        $.each(json_data, function (index, item) {
                            if(typeof item === 'string'){
                                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                            }else{
                                $.each(item, function(index2, item2){
                                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                                })
                            }
                        });

                        form.appendTo('body').submit().remove();
                    }
                }catch (e) {
                    alert(res_json);
                    $('.icon-save').attr('onclick', "$('#f').form('submit');");
                }
            }
        });
        
        var now = Date.parse(new Date()) / 1000;
        
        //订舱船期范围设置
        $('#booking_ETD').datebox('calendar').calendar({
            //到时候不存在可选择值的，在这里false
            validator: function(date){
                //日期不能超过前后半年
                var star = now - 3600 * 24 * 183;
                var end = now + 3600 * 24 * 183;
                var date_time = Date.parse(date) / 1000;
                if (date_time >= star && date_time <= end) {
                    return true;
                } else {
                    return false;
                }
            }
        });

    });
</script>
<style>
    input:read-only,select:read-only,textarea:read-only
    {
        background-color: #F5F5F5;
        cursor: default;
    }
    input:disabled,select:disabled,textarea:disabled
    {
        background-color: #d6d6d6;
        cursor: default;
    }
    .free_svr_type{
        width: 100px;
    }
    .free_svr_days{
        width: 90px;
    }
    .box_info_num{
        width: 100px;
    }
    .box_info_size{
        width: 100px;
    }
    body{
        background-color: #FAFAD2;
    }
    .hide{
        display: none;
    }
</style>
<body style="margin:10px;">
<table><tr><td>
            <h2> Add </h2></td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td> </td></tr></table>

<div class="easyui-tabs" style="width:100%;height:850px">
    <div title="Basic Registration" style="padding:1px">
        <form id="f" name="f" method="post" action="/biz_consol/add_data/">
            <input type="hidden" name="__token__" value="<?php echo setToken('consol_add');?>">
            <input type="hidden" name="shipment_id" value="<?php echo $shipment_id;?>">
            <input type="hidden" name="gaipei_old_id" value="<?= $gaipei_old_id;?>">
            <div class="easyui-layout" style="width:100%;height:780px;">
                <div data-options="region:'west',tools:'#tt'" title="Basic" style="width:350px;">
                    <div class="easyui-accordion" data-options="multiple:true" style="width:340px;height1:300px;">
                        <div title="agent" style="padding:10px;">
                            <select <?= !$agent_lock ? '' : 'disabled';?> id="agent_company" class="easyui-combogrid" name="agent_company" style="width:255px;">
                            </select>
                            <a href="javascript:void(0);" onclick="goEditUrl('/biz_client/edit')"><?= lang('E');?></a>
                            |
                            <a href="javascript: reload_select('agent');"><?= lang('R');?></a>
                            <a href="#" class="easyui-tooltip" data-options="
                                content: $('<div></div>'),
                                onShow: function(){
                                    $(this).tooltip('arrow').css('left', 20);
                                    $(this).tooltip('tip').css('left', $(this).offset().left);
                                },
                                hideDelay:1000,
                                onUpdate: function(cc){
                                    cc.panel({
                                        width: 300,
                                        height: 'auto',
                                        border: false,
                                        href: '/bsc_help_content/help/consol_agent'
                                    });
                                }
                            "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                            <script>
                                /**
                                 * 自出单填入值
                                 */
                                function lcl_zpx_click() {
                                    $('#agent_company').combogrid('setValue', 'OPEN HOUSEBILL');
                                    $('#oversea_cus_id').combobox('setValue', '0').combobox('reload', '/bsc_user/get_data_role/oversea_cus?userIds=0&status=0');
                                    $('#agent_code').val('OPEN HOUSEBILL');
                                    $('#agent_address').val(' ');
                                    $('#agent_email').textbox('setValue', ' ');
                                    $('#agent_contact').val(' ');
                                    $('#agent_telephone').val(' ');
                                }
                            </script>
                            <br />
                            <input <?= !$agent_lock ? '' : 'disabled';?> name="agent_code" id="agent_code" type="hidden" value="<?php echo $agent_code;?>"></input>
                            <?php if(isset($userList['oversea_cus'])){?>
                                <?php echo lang('oversea_cus'); ?>
                                <br><select <?php echo !$agent_lock  ? '' : 'disabled'; ?> id="oversea_cus_id" class="easyui-combobox"  editable="false" name="oversea_cus_id" style="width:255px;" data-options="
                                valueField:'id',
                                textField:'name',
                                url:'/bsc_user/get_data_role/oversea_cus?userIds=0&status=0',
                                value:'<?php echo $oversea_cus_id;?>',
                                onSelect: function(rec){
                                    $('#oversea_cus_group').val(rec.group);
                                },
                                onHidePanel: function() {
									var valueField = $(this).combobox('options').valueField;
									var val = $(this).combobox('getValue');
									var allData = $(this).combobox('getData');
									var result = true;
									for (var i = 0; i < allData.length; i++) {
										if (val == allData[i][valueField]) {
											result = false;
										}
									}
									if (result) {
										$(this).combobox('clear');
									}
								},
                            ">
                                </select>
                                <input type="hidden" <?php echo !$agent_lock  ? '' : 'disabled'; ?> id="oversea_cus_group" name="oversea_cus_group" style="width:255px;" value="<?php echo $oversea_cus_group;?>">
                                <a href="javascript:void(0);" class="lcl_zpx <?= $trans_mode == 'LCL' ? '' : 'hide';?>" onclick="lcl_zpx_click()"><?= lang('自出单');?></a>
                            <?php } ?>
                            <br><?= lang('代理邮箱');?>
                            <br><input class="easyui-textbox" <?= !$agent_lock ? '' : 'disabled';?> name="agent_email" id="agent_email" value="<?php echo $agent_email;?>" style="width:255px;"/>
                            <br><?= lang('update_agent_address');?>:
                            <br><textarea <?= !$agent_lock ? '' : 'disabled';?> name="agent_address" id="agent_address" style="height:60px;width:300px;"><?php echo $agent_address;?></textarea>
                            <br><?= lang('update_agent_contact');?>:
                            <br><textarea <?= !$agent_lock ? '' : 'disabled';?> name="agent_contact" id="agent_contact" style="height:40px;width:300px;"><?php echo $agent_contact;?></textarea>
                            <br><?= lang('update_agent_telephone');?>:
                            <br><textarea <?= !$agent_lock ? '' : 'disabled';?> name="agent_telephone" id="agent_telephone" style="height:40px;width:300px;"><?php echo $agent_telephone;?></textarea>
                        </div>
                        <div title="Shipper"  style="overflow:auto;padding:10px;">
                            <select id="shipper_company2" class="easyui-combogrid" style="width:255px;"></select> 
                            <input type="hidden" id="shipper_company" name="shipper_company">
                            <a href="javascript: goEditUrl('/biz_company/index/shipper');"><?= lang('E');?></a>
                            /
                            <a href="javascript: reload_select('shipper_company2');"><?= lang('R');?></a>
                            <br><?= lang('update_shipper_address');?>:
                            <br><textarea name="shipper_address" id="shipper_address" style="height:60px;width:300px;"><?php echo $shipper_address;?></textarea>
                            <br><?= lang('update_shipper_contact');?>:
                            <br><textarea name="shipper_contact" id="shipper_contact" style="height:40px;width:300px;"><?php echo $shipper_contact;?></textarea>
                            <br><?= lang('update_shipper_telephone');?>:
                            <br><textarea name="shipper_telephone" id="shipper_telephone" style="height:40px;width:300px;"><?php echo $shipper_telephone;?></textarea>
                            <br><?= lang('update_shipper_email');?>: <input type="checkbox" name="show_shipper_email">
                            <br><textarea name="shipper_email" id="shipper_email" style="height:40px;width:300px;"><?php echo $shipper_email;?></textarea>
                        </div>
                        <div title="Consignee" style="padding:10px;">
                            <select id="consignee_company2" class="easyui-combogrid" style="width:255px;"></select> 
                            <input type="hidden" id="consignee_company" name="consignee_company">
                            <a href="javascript: goEditUrl('/biz_company/index/consignee');"><?= lang('E');?></a>
                            /
                            <a href="javascript: reload_select('consignee_company2');"><?= lang('R');?></a>
                            <br><?= lang('update_consignee_address');?>:
                            <br><textarea name="consignee_address" id="consignee_address" style="height:60px;width:300px;"><?php echo $consignee_address;?></textarea>
                            <br><?= lang('update_consignee_contact');?>:
                            <br><textarea name="consignee_contact" id="consignee_contact" style="height:40px;width:300px;"><?php echo $consignee_contact;?></textarea>
                            <br><?= lang('update_consignee_telephone');?>:
                            <br><textarea name="consignee_telephone" id="consignee_telephone" style="height:40px;width:300px;"><?php echo $consignee_telephone;?></textarea>
                            <br><?= lang('update_consignee_email');?>: <input type="checkbox" name="show_consignee_email">
                            <br><textarea name="consignee_email" id="consignee_email" style="height:40px;width:300px;"><?php echo $consignee_email;?></textarea>
                        </div>
                        <div title="Notify Party" style="padding:10px;">
                            <select id="notify_company2" class="easyui-combogrid" style="width:255px;"></select>
                            <input type="hidden" id="notify_company" name="notify_company">
                            <a href="javascript: goEditUrl('/biz_company/index/notify');"><?= lang('E');?></a>
                            /
                            <a href="javascript: reload_select('notify_company2');"><?= lang('R');?></a>
                            <br><?= lang('update_notify_address');?>:
                            <br><textarea name="notify_address" id="notify_address" style="height:60px;width:300px;"><?php echo $notify_address;?></textarea>
                            <br><?= lang('update_notify_contact');?>:
                            <br><textarea name="notify_contact" id="notify_contact" style="height:40px;width:300px;"><?php echo $notify_contact;?></textarea>
                            <br><?= lang('update_notify_telephone');?>:
                            <br><textarea name="notify_telephone" id="notify_telephone" style="height:40px;width:300px;"><?php echo $notify_telephone;?></textarea>
                            <br><?= lang('update_notify_email');?>: <input type="checkbox" name="show_notify_email">
                            <br><textarea name="notify_email" id="notify_email" style="height:40px;width:300px;"><?php echo $notify_email;?></textarea>
                        </div>
                    </div>
                    <div id="tt">
                        <a href="javascript:void(0)" class="icon-save" onclick="$('#f').form('submit');" title="save" style="margin-right:15px;"></a>
                    </div>
                </div>
                <div data-options="region:'center',title:'Details'">
                    <table style="width: 100%;height:510px;" border="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                                <table class="form_table" border="0" cellspacing="0">
                                    <tr>
                                        <td><?= lang('update_biz_type');?></td>
                                        <td>
                                            <select class="easyui-combobox" id="biz_type1"  style="width:180px;" data-options="
                                                editable:false,
                                                valueField:'name',
                                                textField:'name',
                                                url:'/bsc_dict/get_biz_type',
                                                value: '<?php echo $biz_type . ' ' . $trans_mode;?>',
                                                onSelect: function(res){
                                                    biz_type_select(res);
                                                },
                                            ">
                                            </select>
                                            <input type="hidden" name="trans_mode" id="trans_mode" value="<?= $trans_mode;?>">
                                            <input type="hidden" name="biz_type" id="biz_type" value="<?= $biz_type;?>">
                                            <select  name="trans_tool" id="trans_tool" class="easyui-combobox" style="width:69px;" data-options="
                                                editable:false,
                                                valueField:'value',
                                                textField:'name',
                                                url:'/bsc_dict/get_option/trans_tool',
                                                value:'<?= $trans_tool;?>',
                                                onSelect:function(rec){
                                                    trans_tool_select(rec);
                                                },
                                                 onLoadSuccess: function(){
                                                    var val = $(this).combobox('getValue');
                                                    var rec = {value:val};
                                                    trans_tool_select(rec);
                                                },
                                            ">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr <?php if($trans_mode !== 'LCL') echo "style=\"display: none;\"";?>>
                                        <td><?= lang('LCL_type');?></td>
                                        <td>
                                            <select class="easyui-combobox" name="LCL_type" id="LCL_type" style="width:255px;" data-options="valueField:'value',
                                                textField:'name',
                                                editable: false,
                                                url:'/bsc_dict/get_option/LCL_type',
                                                value:'<?php echo $LCL_type;?>',
                                                onSelect: function(rec){
                                                    LCL_type_select(rec);
                                                },
                                                onLoadSuccess:function(){
                                                    var val = $(this).combobox('getValue');
                                                    if(val == 'KML CONSOL') LCL_type_select();
                                                },
                                            ">
                                            </select>
                                        </td>
                                    </tr>
                                    <?php if(isset($box_info) && !empty($box_info)){foreach($box_info as $key => $row){
                                    $this_btn = '<a class="easyui-linkbutton" onclick="del_box(this)" iconCls="icon-remove"></a>';
                                    $this_lang = '';
                                    if($key == 0){
                                        $this_btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
                                        $this_lang = lang('box_info');
                                    }
                                    ?>
    
                                    <tr class="add_box">
                                        <td><?= $this_lang;?></td>
                                        <td>
                                            <select class="easyui-combobox box_info_size" name="box_info[size][]" data-options="
                                            valueField:'value',
                                            textField:'value',
                                            url:'/bsc_dict/get_option/container_size',
                                            value: '<?= $row['size']?>',
                                            onHidePanel: function() {
                                                var valueField = $(this).combobox('options').valueField;
                                                var val = $(this).combobox('getValue');
                                                var allData = $(this).combobox('getData');
                                                var result = true;
                                                for (var i = 0; i < allData.length; i++) {
                                                    if (val == allData[i][valueField]) {
                                                        result = false;
                                                    }
                                                }
                                                if (result) {
                                                    $(this).combobox('clear');
                                                }
                                            }
                                        "></select>
                                            X
                                            <input class="easyui-numberbox box_info_num"  name="box_info[num][]" value="<?= $row['num'];?>">
                                            <?= $this_btn;?>
                                        </td>
                                    </tr>
                                    <?php }}else{
                                    if($trans_mode == 'FCL'){
                                        $this_btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
                                        $this_lang = lang('box_info');
                                        ?>
                                        <tr class="add_box">
                                            <td><?= $this_lang;?></td>
                                            <td>
                                                <select class="easyui-combobox box_info_size" name="box_info[size][]" data-options="
                                                    valueField:'value',
                                                    textField:'value',
                                                    url:'/bsc_dict/get_option/container_size',
                                                    onHidePanel: function() {
                                                        var valueField = $(this).combobox('options').valueField;
                                                        var val = $(this).combobox('getValue');
                                                        var allData = $(this).combobox('getData');
                                                        var result = true;
                                                        for (var i = 0; i < allData.length; i++) {
                                                            if (val == allData[i][valueField]) {
                                                                result = false;
                                                            }
                                                        }
                                                        if (result) {
                                                            $(this).combobox('clear');
                                                        }
                                                    }
                                                "></select>
                                                X
                                                <input class="easyui-numberbox box_info_num"  name="box_info[num][]" value="">
                                                <?= $this_btn;?>
                                            </td>
                                        </tr>
                                    <?php }}?>
                                     <tr>
                                        <td><?= lang('trans_carrier');?></td>
                                        <td>
                                            <select id="trans_carrier" class="easyui-combobox" editable="true" name="trans_carrier" style="width:235px;" data-options="
                                                    valueField:'client_code',
            								        textField:'client_name',
                                                    url:'/biz_client/get_option/carrier',
                                                    value:'<?php echo $trans_carrier;?>',
                                                    onSelect: function(rec){
                                                        trans_carrier_select(rec);
                                                    },
                                                    onHidePanel: function() {
                                                        var valueField = $(this).combobox('options').valueField;
                                                        var val = $(this).combobox('getValue');
                                                        var allData = $(this).combobox('getData');
                                                        var result = true;
                                                        for (var i = 0; i < allData.length; i++) {
                                                            if (val == allData[i][valueField]) {
                                                                result = false;
                                                            }
                                                        }
                                                        if (result) {
                                                            $(this).combobox('clear');
                                                        }
                                                    },
                                                ">
                                            </select>
                                            <a href="javascript:void(0);" class="easyui-tooltip" onclick="trans_carrier_rule_open()"><?= lang('规则');?></a>
                                        </td>
                                    </tr>

                                    <tr class="trans_carrier_second <?= $trans_carrier == 'JXHY01' ? '' : 'hide';?>">
                                        <td><?= lang('trans_carrier_second');?></td>
                                        <td>
                                            <select id="trans_carrier_second" class="easyui-combobox" editable="true" name="trans_carrier_second" style="width:255px;" data-options="
                                                    valueField:'client_code',
            								        textField:'client_name',
                                                    url:'/biz_client/get_option/carrier',
                                                    value:'<?php echo $trans_carrier;?>',
                                                    onHidePanel: function() {
                                                        var valueField = $(this).combobox('options').valueField;
                                                        var val = $(this).combobox('getValue');
                                                        var allData = $(this).combobox('getData');
                                                        var result = true;
                                                        for (var i = 0; i < allData.length; i++) {
                                                            if (val == allData[i][valueField]) {
                                                                result = false;
                                                            }
                                                        }
                                                        if (result) {
                                                            $(this).combobox('clear');
                                                        }
                                                    },
                                                ">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_trans_origin');?></td>
                                        <td>
                                            <select class="easyui-combogrid" style="width: 80px;" name="trans_origin" id="trans_origin" data-options="
                                                panelWidth: '1000px',
                                                panelHeight: '300px',
                                                idField: 'port_code',              //ID字段
                                                textField: 'port_code',    //显示的字段
                                                url:'/biz_port/get_option?type=<?= $trans_tool;?>&carrier=<?= $trans_carrier;?>&limit=true&old=<?= $trans_origin;?>',
                                                striped: true,
                                                editable: false,
                                                pagination: false,           //是否分页
                                                toolbar : '#trans_origin_div',
                                                collapsible: true,         //是否可折叠的
                                                method: 'get',
                                                columns:[[
                                                    {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                                                    {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                                                    {field:'terminal',title:'<?= lang('码头');?>',width:200},
                                                ]],
                                                mode: 'remote',
                                                value: '<?= $trans_origin;?>',
                                                emptyMsg : '未找到相应数据!',
                                                onSelect: function(index, row){
                                                    if(row !== undefined){
                                                        $('#trans_origin_name').textbox('setValue', row.port_name);
                                                    }
                                                },
                                                onShowPanel:function(){
                                                    var opt = $(this).combogrid('options');
                                                    var toolbar = opt.toolbar;
                                                    combogrid_search_focus(toolbar, 1);
                                                },
                                            ">
                                            </select>
                                            <input class="easyui-textbox" style="width: 170px;" name="trans_origin_name" id="trans_origin_name" value="<?= $trans_origin_name;?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_trans_discharge');?></td>
                                        <td>
                                            <select class="easyui-combogrid" style="width: 80px;" name="trans_discharge" id="trans_discharge" data-options="
                                                panelWidth: '1000px',
                                                panelHeight: '300px',
                                                idField: 'port_code',              //ID字段
                                                textField: 'port_code',    //显示的字段
                                                url:'/biz_port/get_option?type=<?= $trans_tool;?>&carrier=<?= $trans_carrier;?>&limit=true&old=<?= $trans_discharge;?>',
                                                striped: true,
                                                editable: false,
                                                pagination: false,           //是否分页
                                                toolbar : '#trans_discharge_div',
                                                collapsible: true,         //是否可折叠的
                                                method: 'get',
                                                columns:[[
                                                    {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                                                    {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                                                    {field:'terminal',title:'<?= lang('码头');?>',width:200},
                                                ]],
                                                mode: 'remote',
                                                value: '<?= $trans_discharge;?>',
                                                emptyMsg : '未找到相应数据!',
                                                onSelect: function(index, row){
                                                    if(row !== undefined){
                                                        $('#trans_discharge_name').textbox('setValue', row.port_name);
                                                    }
                                                },
                                                onShowPanel:function(){
                                                    var opt = $(this).combogrid('options');
                                                    var toolbar = opt.toolbar;
                                                    combogrid_search_focus(toolbar, 1);
                                                },
                                            ">
                                            </select>
                                            <input class="easyui-textbox" style="width: 170px;" name="trans_discharge_name" id="trans_discharge_name" value="<?= $trans_discharge_name;?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_trans_destination');?></td>
                                        <td>
                                            <input class="easyui-textbox" readonly style="width: 80px;" id="trans_destination_inner" name="trans_destination_inner" value="<?= $trans_destination_inner;?>">
                                            <select class="easyui-combogrid" style="width: 70px;" name="trans_destination" id="trans_destination" data-options="
                                                panelWidth: '1000px',
                                                panelHeight: '300px',
                                                idField: 'port_code',              //ID字段
                                                textField: 'port_code',    //显示的字段
                                                url:'/biz_port/get_discharge?type=<?= $trans_tool;?>&carrier=<?= $trans_carrier;?>&old=<?= $trans_discharge;?>',
                                                striped: true,
                                                editable: false,
                                                pagination: false,           //是否分页
                                                toolbar : '#trans_destination_div',
                                                collapsible: true,         //是否可折叠的
                                                method: 'get',
                                                columns:[[
                                                    {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                                                    {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                                                    {field:'terminal',title:'<?= lang('码头');?>',width:200},
                                                ]],
                                                mode: 'remote',
                                                value: '<?= $trans_destination;?>',
                                                emptyMsg : '未找到相应数据!',
                                                onSelect: function(index, row){
                                                    if(row !== undefined){
                                                        $('#trans_destination_inner').textbox('setValue', row.inner_port_code);
                                                        $('#trans_destination_name').textbox('setValue', row.port_name);
                                                        $('#sailing_area').textbox('setValue', row.sailing_area);
                                                        $('#sailing').textbox('setValue', row.sailing);
                                                        $('#trans_destination_terminal').textbox('setValue', row.terminal);
                                                    }
                                                },
                                                onShowPanel:function(){
                                                    var opt = $(this).combogrid('options');
                                                    var toolbar = opt.toolbar;
                                                    combogrid_search_focus(toolbar, 1);
                                                },
                                            ">
                                            </select>
                                            <input class="easyui-textbox" style="width: 97px;" name="trans_destination_name" id="trans_destination_name" value="<?= $trans_destination_name;?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('trans_destination_terminal');?></td>
                                        <td>
                                            <input class="easyui-textbox" style="width: 255px;" name="trans_destination_terminal" id="trans_destination_terminal" value="<?= $trans_destination_terminal;?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('booking_ETD');?> </td>
                                        <td>
                                            <input class="easyui-datebox" name="booking_ETD" id="booking_ETD" data-options="panelWidth:800,panelHeight:600,"  style="width:255px;" value="<?php echo $booking_ETD;?>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('customer_booking');?></td>
                                        <td>
                                            <select  class="easyui-combobox" editable="false" name="customer_booking" id="customer_booking" style="width:255px;" data-options="
                                                value: '<?= $customer_booking;?>',
                                            ">
                                                <option value="1"><?= lang('customer self');?></option>
                                                <option value="2"><?= lang('contract no');?></option>
                                                <option value="0"><?= lang('our company booking');?></option>
                                            </select>
                                        </td> 
                                    </tr>
                                    <input type="hidden" name="sailing_code" id="sailing_code" value="<?= $sailing_code;?>">
                                     <tr>
                                        <td>
                                            <?= lang('booking_agent');?>
                                        </td>
                                        <td>
                                            <select id="creditor" class="easyui-combobox" name="creditor" style="width:255px;" data-options="
                								valueField:'client_code',
                								textField:'company_name',
                								url:'/biz_client/get_option/booking_agent',
                								value:'<?php echo $creditor;?>',
                								onSelect:function(rec){
                								    creditor_select(rec);
                								},
                								onHidePanel: function() {
                                                    var valueField = $(this).combobox('options').valueField;
                                                    var val = $(this).combobox('getValue');
                                                    var allData = $(this).combobox('getData');
                                                    var result = true;
                                                    for (var i = 0; i < allData.length; i++) {
                                                        if (val == allData[i][valueField]) {
                                                            result = false;
                                                        }
                                                    }
                                                    if (result) {
                                                        $(this).combobox('clear');
                                                    }
                                                }
        								    ">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_status');?></td>
                                        <td>
                                            <select class="easyui-combobox" name="status" id="status" style="width:255px;" data-options="
                                                editable:false,
                                                valueField:'value',
                                                textField:'name',
                                                url:'/bsc_dict/get_option/bill_status',
                                                value:'<?php echo $status;?>'">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('AP code');?></td>
                                        <td>
                                            <input class="easyui-textbox"  name="AP_code" id="AP_code"  style="width:255px;" value="<?= $AP_code;?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('AP remark');?></td>
                                        <td>
                                            <textarea name="AP_remark" id="AP_remark" style="height:60px;width:248px;"><?php echo $AP_remark;?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_carrier_ref');?></td>
                                        <td>
                                            <input class="easyui-textbox"  name="carrier_ref" id="carrier_ref"  style="width:255px;" value="<?php echo $carrier_ref;?>" data-options="events:{keyup: KeyUpUpper}">
                                            <span id="carrier_ref_tip" style="color: red"></span>
                                            <script>
                                                function KeyUpUpper(e) {
                                                    this.value = this.value.toUpperCase();
                                                }
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('放单方式');?></td>
                                        <td>
                                             <select id="release_type"  class="easyui-combobox"  editable="false" name="release_type" style="width:255px;" data-options="
                								valueField:'value',
                								textField:'value',
                								url:'/bsc_dict/get_option/release_type',
                								value:'<?php echo $release_type;?>',
            								">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_payment');?></td>
                                        <td>
                                            <select id="payment" class="easyui-combobox"  editable="false" name="payment" style="width:155px;" data-options="
            								valueField:'value',
            								textField:'lang',
            								url:'/bsc_dict/get_option/payment',
            								onSelect: function(rec){
            									if(rec.value=='E') {
            									    $('#payment_third').textbox('readonly', false);
            									}else{
            									    $('#payment_third').textbox('readonly', true).textbox('clear');
            									}
            								},
            								value:'<?php echo $payment;?>'
            								">
                                            </select>
                                            <input class="easyui-textbox" <?= $payment !== 'E' ? 'readonly' : '';?> name="payment_third" id="payment_third"  style="width:95px;" value="<?php echo $payment_third;?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_agent_ref');?> </td>
                                        <td>
                                            <input class="easyui-textbox"  name="agent_ref" id="agent_ref"  style="width:255px;" value="<?php echo $agent_ref;?>"/>
                                            <span id="agent_ref_tip" style="color: red"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('sailing_area');?></td>
                                        <td>
                                            <input class="easyui-textbox" name="sailing_area" id="sailing_area" style="width:80px;" readonly value="<?= $sailing_area;?>">
                                            <input class="easyui-textbox" id="sailing" style="width:170px;" readonly>
                                        </td>
                                    </tr>
                                    <tr id="update_vessel">
                                        <td>
                                            <?= lang('update_vessel');?>
                                            <a href="/bsc_dict/index/vessel" target="_blank"><?= lang('E');?></a>
                                            |
                                            <a href="javascript: reload_select('vessel');"><?= lang('R');?></a>
                                        </td>
                                        <td>
                                            <select id="vessel" class="easyui-combobox"  editable="true" name="vessel" style="width:255px;" data-options="valueField:'value',
            								textField:'name',
            								mode: 'remote',
                                            onBeforeLoad:function(param){
                                                if($(this).combobox('getValue') == '')return false;
                                                if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                                            },
            								onLoadSuccess:function(){
            								    var data = $(this).combobox('getData');
            								    $('#feeder').combobox('loadData',data);
            								},
            
            								onHidePanel: function() {
                                                var valueField = $(this).combobox('options').valueField;
                                                var val = $(this).combobox('getValue');
                                                var allData = $(this).combobox('getData');
                                                var result = true;
                                                for (var i = 0; i < allData.length; i++) {
                                                    if (val == allData[i][valueField]) {
                                                        result = false;
                                                    }
                                                }
                                                if (result) {
                                                    $(this).combobox('clear');
                                                }
                                            },
            								value:'<?php echo $vessel;?>' ,
            								">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="flight_number"><?= lang('update_voyage');?></td>
                                        <td>
                                            <select id="voyage" class="easyui-combobox"  editable="true" name="voyage" style="width:95px;" data-options="
                                            valueField:'leaveVoyNo',
                                            textField:'leaveVoyNo',
                                            onSelect:function(rec){
                                                $('#trans_ETA').datetimebox('setValue', setValueRT(rec.planArrivalPortTime));
                                                $('#trans_ETD').datetimebox('setValue', setValueRT(rec.planLeavePortTime));
                                                $('#trans_ATA').datetimebox('setValue', setValueRT(rec.factArrivalPortTime));
                                                $('#trans_ATD').datetimebox('setValue', setValueRT(rec.factLeavePortTime));
                                                var tdc_data = [];
                                                if(rec['portOfCall'] !== undefined){
                                                    $.each(rec['portOfCall'].split('+'), function(tdc_index, tdc_item){
                                                        tdc_data.push({'value':tdc_item});
                                                    });
                                                }
                                            },
                                            onLoadSuccess:function(){
                                                var this_data = $(this).combobox('getData');
                                                var this_val = $(this).combobox('getValue');
                                                var rec = this_data.filter(el => el['leaveVoyNo'] === this_val)
                                                var tdc_data = [];
                                                if(rec.length > 0){
                                                    rec = rec[0];
                                                    $.each(rec['portOfCall'].split('+'), function(tdc_index, tdc_item){
                                                        tdc_data.push({'value':tdc_item});
                                                    });
                                                }
                                            },
                                            value:'<?php echo $voyage;?>',
                                            ">
                                            </select>
                                        </td>
                                    </tr>  
                                    <tr>
                                        <td><?= lang('trans_term');?></td>
                                        <td>
                                            <select id="trans_term" class="easyui-combobox"  editable="false" name="trans_term" style="width:255px;" data-options="
                                            value:'<?= $trans_term;?>',
                                            valueField:'value',
                                            textField:'value',
                                            url:'/bsc_dict/get_option/trans_term',
                                            ">
                                            </select>
                                        </td>
                                    </tr>
                              <?php if(isset($free_svr) && !empty($free_svr)){
                                    foreach($free_svr as $key => $row){
                                        $this_btn = '<a class="easyui-linkbutton" onclick="add_free_svr()" iconCls="icon-add"></a>';
                                        $this_lang = '';
                                        if($key < 3){
                                            if($key == 0){
                                                $this_lang = lang('free_svr');
                                            }
                                        }else{
                                            $this_btn = '';
                                        }?>
                                        <tr class="free_svr">
                                            <td><?= $this_lang;?></td>
                                            <td>
                                                <select editable="false" class="easyui-combobox free_svr_type" name="free_svr[<?= $key;?>][type]"  data-options="
                                                            valueField:'value',
                                                            textField:'name',
                                                            url: '/bsc_dict/get_option/free_svr',
                                                            value: '<?= $row['type'];?>',
                                                            onSelect: function(rec){
                                                                free_svr_type_jc();
                                                            },
                                                        ">
                                                </select>
                                                <input class="easyui-numberbox free_svr_days" name="free_svr[<?= $key;?>][days]" value="<?= $row['days'];?>"><?= lang('days');?>
                                                <?= $this_btn;?>
                                            </td>
                                        </tr>
                                    <?php }
                                }else{ ?>
                                    <tr class="free_svr">
                                        <td><?= lang('free svr');?></td>
                                        <td>
                                            <select editable="false" class="easyui-combobox free_svr_type" name="free_svr[0][type]" data-options="
                                                        valueField:'value',
                                                        textField:'name',
                                                        url: '/bsc_dict/get_option/free_svr',
                                                        onSelect: function(rec){
                                                            free_svr_type_jc();
                                                        },
                                                    ">
                                            </select>
                                            <input class="easyui-numberbox free_svr_days" name="free_svr[0][days]"><?= lang('days');?>
                                            <a class="easyui-linkbutton" onclick="add_free_svr()" iconCls="icon-add"></a>
                                        </td>
                                    </tr>
                                <?php }?>
                                    <tr><td colspan="2"><hr></td></tr>
                                    <!--成本--start--> 
                                    <input type="hidden" id="feeder" name="feeder"  value="<?= $feeder;?>">
                                    <input type="hidden" id="feeder_voyage" name="feeder_voyage"  value="<?= $feeder_voyage;?>">
                                    <input type="hidden" id="trans_discharge_code" name="trans_discharge_code"  value="<?= $trans_discharge_code;?>">
                                    <input type="hidden" id="trans_receipt_name" name="trans_receipt_name"  value="<?= $trans_receipt_name;?>">
                                    <input type="hidden" id="freight_affirmed" name="freight_affirmed"  value="<?= $freight_affirmed;?>">
                                    <input type="hidden"id="booking_confirmation" name="booking_confirmation"  value="<?= $booking_confirmation;?>">  
                                    <tr>
                                        <td><?= lang('marketing');?></td> 
                                        <td> 
                                            <select id="marketing_id" class="easyui-combobox"  editable="false" name="marketing_id" style="width:255px;" data-options="
                                                valueField:'id',
                                                textField:'name',
                                                url:'/bsc_user/get_data_role/marketing?status=0',
                                                value:'<?php echo $marketing_id;?>',
                                                onSelect: function(rec){
                                                    $('#marketing_group').val(rec.group);
                                                },
                                                onLoadSuccess:function(){
                                                    var val = $(this).combobox('getValue');
                                                    var data = $(this).combobox('getData');
                                                    var reset = true;
                                                    if(val == 0 && data.length == 2){
                                                        $(this).combobox('setValue', data[1].id);
                                                        $('#marketing_group').val(data[1].group);
                                                    }else{
                                                        $.each(data, function(index,item){
                                                        if(item.id == val){
                                                                reset = false;
                                                            }
                                                        });
                                                        if(reset){
                                                             $(this).combobox('setValue', 0);
                                                             $('#marketing_group').val('');
                                                        }
                                                    }
                                                },
                                            ">
                                            </select> 
                                            <input type="hidden" id="marketing_group" name="marketing_group" style="width:255px;" value="<?php echo $marketing_group;?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('booking');?></td> 
                                        <td> 
                                            <select id="booking_id" class="easyui-combobox"  editable="false" name="booking_id" style="width:255px;" data-options="
                                                valueField:'id',
                                                textField:'name',
                                                url:'/bsc_user/get_data_role/booking?status=0',
                                                value:'<?php echo $booking_id;?>',
                                                onSelect: function(rec){
                                                    $('#booking_group').val(rec.group);
                                                },
                                                onLoadSuccess:function(){
                                                    var val = $(this).combobox('getValue');
                                                    var data = $(this).combobox('getData');
                                                    var reset = true;
                                                    if(val == 0 && data.length == 2){
                                                        $(this).combobox('setValue', data[1].id);
                                                        $('#booking_group').val(data[1].group);
                                                    }else{
                                                        $.each(data, function(index,item){
                                                        if(item.id == val){
                                                                reset = false;
                                                            }
                                                        });
                                                        if(reset){
                                                             $(this).combobox('setValue', 0);
                                                             $('#booking_group').val('');
                                                        }
                                                    }
                                                },
                                            ">
                                            </select> 
                                            <input type="hidden" id="booking_group" name="booking_group" style="width:255px;" value="<?php echo $booking_group;?>">
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('document');?></td> 
                                        <td> 
                                            <select id="document_id" class="easyui-combobox"  editable="false" name="document_id" style="width:255px;" data-options="
                                                valueField:'id',
                                                textField:'name',
                                                url:'/bsc_user/get_data_role/document?status=0',
                                                value:'<?php echo $document_id;?>',
                                                onSelect: function(rec){
                                                    $('#document_group').val(rec.group);
                                                },
                                                onLoadSuccess:function(){
                                                    var val = $(this).combobox('getValue');
                                                    var data = $(this).combobox('getData');
                                                    var reset = true;
                                                    if(val == 0 && data.length == 2){
                                                        $(this).combobox('setValue', data[1].id);
                                                        $('#document_group').val(data[1].group);
                                                    }else{
                                                        $.each(data, function(index,item){
                                                        if(item.id == val){
                                                                reset = false;
                                                            }
                                                        });
                                                        if(reset){
                                                             $(this).combobox('setValue', 0);
                                                             $('#document_group').val('');
                                                        }
                                                    }
                                                },
                                            ">
                                            </select> 
                                            <input type="hidden" id="document_group" name="document_group" style="width:255px;" value="<?php echo $document_group;?>">
                                        
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <!--成本--end-->
                                <?php
                                    $date_zero = array("trans_ETA","trans_ATA","trans_ETD","trans_ATD","closing_date", 'des_ETA');
                                    foreach($date_zero as $dzero){
                                        if($$dzero < "1999-01-01") $$dzero = null;
                                    }
                                ?>
                                <table class="form_table">
                                <tr>
                                    <td><?= lang('update_trans_ETA');?> </td>
                                    <td>
                                        <input class="easyui-datetimebox"  name="trans_ETA" id="trans_ETA"  style="width:255px;" value="<?php echo $trans_ETA;?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= lang('update_trans_ATA');?> </td>
                                    <td>
                                        <input class="easyui-datetimebox"  name="trans_ATA" id="trans_ATA"  style="width:255px;" value="<?php echo $trans_ATA;?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= lang('update_trans_ETD');?> </td>
                                    <td>
                                        <input class="easyui-datetimebox"  name="trans_ETD" id="trans_ETD"  style="width:255px;" value="<?php echo $trans_ETD;?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= lang('update_trans_ATD');?> </td>
                                    <td>
                                        <input class="easyui-datetimebox"  name="trans_ATD" id="trans_ATD"  style="width:255px;" value="<?php echo $trans_ATD;?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= lang('des_ETA');?> </td>
                                    <td>
                                        <input class="easyui-datebox"  name="des_ETA" id="des_ETA"  style="width:255px;" value="<?php echo $des_ETA;?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= lang('closing_date');?> </td>
                                    <td>
                                        <input class="easyui-datetimebox"  name="closing_date" id="closing_date"  style="width:95px;" value="<?php echo $closing_date;?>"/>
                                        ENS/AMS/ACI <input type="checkbox" name="sign[]" <?php if(in_array('ENS/AMS/ACI', explode(',', $sign)))echo 'checked'; ?> value="ENS/AMS/ACI">
                                        ISF <input type="checkbox" name="sign[]" <?php if(in_array('ISF', explode(',', $sign)))echo 'checked'; ?> value="ISF">
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= lang('update_description');?> </td>
                                    <td> 
                                        <textarea name="description" id="description" style="height:60px;width:245px;border:1px red solid;"/><?php echo $description;?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= lang('description_cn');?> </td>
                                    <td>
                                        <textarea name="description_cn" id="description_cn" style="height:60px;width:245px;"/><?php echo $description_cn;?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= lang('update_mark_nums');?> </td>
                                    <td>
                                        <textarea name="mark_nums" id="mark_nums" style="height:60px;width:245px;"/><?php echo $mark_nums;?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= lang('requirements');?></td>
                                    <td>
                                        <textarea name="requirements" id="requirements" style="height:60px;width:248px;"><?php echo $requirements;?></textarea><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= lang('marketing') . lang('remark');?></td>
                                    <td>
                                        <textarea name="remark" id="remark" style="height:60px;width:248px;"><?php echo $remark;?></textarea><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= lang('AGR No.');?></td>
                                    <td>
                                        <input type="text" class="easyui-textbox" name="AGR_no" id="AGR_no" style="width: 255px;" value="<?= $AGR_no;?>">
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td><?= lang('hs_code');?> </td>
                                    <td>
                                        <select class="easyui-combobox"  name="hs_code" id="hs_code"  style="width:255px;"  prompt="<?= lang('Please separate them with commas');?>" data-options="
                                                value:'<?= $hs_code;?>',
                                                valueField:'value',
                								textField:'name',
            									<?php if($trans_carrier == 'DXHYYX02') echo 'url:\'/bsc_dict/get_option/hs_code?ext1=TSL\',';?>
                								mode: 'remote',
                                                onBeforeLoad:function(param){
                                                    if($(this).combobox('getValue') == undefined || $(this).combobox('getValue').length < 3)return false;
                                                    if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                                                },
                								onHidePanel: function() {
                                                    var valueField = $(this).combobox('options').valueField;
                                                    var vals = $(this).combobox('getValues');
                                                    var allData = $(this).combobox('getData');
                                                    if(allData.length == undefined || allData.length == 0) return false;
                                                    $(this).combobox('setValue', '');
                                                    for (var i = 0; i < allData.length; i++) {
                                                        if ($.inArray(allData[i][valueField], vals) !== -1) {
                                                            $(this).combobox('select', allData[i][valueField]);
                                                        }
                                                    }
                                                },
                                            ">
                                        </select>
                                        <span id="hs_code_tip" style="color: red"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= lang('update_warehouse_code');?></td>
                                    <td>
                                        <select id="warehouse_code" class="easyui-combobox"  editable="false" name="warehouse_code" style="width:255px;" data-options="
        								valueField:'client_code',
        								textField:'company_name',
        								url:'/biz_client/get_option/warehouse',
        								value:'<?php echo $warehouse_code;?>'
        								">
                                        </select>
                                    </td>
                                </tr>    
                            </table>
                        </td>
                            <td valign="top">
                                <table class="form_table">
                                    <tr>
                                        <td><?= lang('container_owner');?> </td>
                                        <td>
                                            <select class="easyui-combobox"  name="container_owner" id="container_owner"  style="width:255px;" data-options="
                                                    editable:false,
                                                    value:'<?php echo $container_owner;?>',
                                                    valueField:'value',
                                                    textField:'name',
                                                    url: '/bsc_dict/get_option/container_owner',
                                                    onSelect:function (row){
                                                        //if(row.value=='公司SOC'){
                                                        //    $('#tr_suitcase_point').css('display','table-row');
                                                        //    $('#tr_suitcase_carton_no').css('display','table-row');
                                                        //}else {
                                                        //    $('#tr_suitcase_point').css('display','none');
                                                        //    $('#tr_suitcase_carton_no').css('display','none');
                                                        //}
                                                    }
                                                ">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="tr_suitcase_point" style="<?php if($container_owner!="公司SOC"):?>display:none;<?php endif;?>">
                                        <td>
                                            <?= lang('suitcase_point');?>
                                        </td>
                                        <td>
                                            <select class="easyui-combobox"  name="gs_soc_tixiangdian" id="gs_soc_tixiangdian"  style="width:255px;" data-options="
                                                    editable:false,
                                                    value:'<?php echo isset($gs_soc_tixiangdian)?$gs_soc_tixiangdian:"";?>',
                                                    valueField:'company_name',
                                                    textField:'company_name',
                                                ">
                                            </select>
                                        </td>

                                    </tr>
                                    <tr id="tr_suitcase_carton_no" style="<?php if($container_owner!="公司SOC"):?>display:none;<?php endif;?>">
                                        <td>
                                            <?= lang('suitcase_carton_no');?>
                                        </td>
                                        <td>
                                            <input class="easyui-textbox" name="gs_soc_tixianghao" id="gs_soc_tixianghao" style="width:255px;"
                                                   value="<?php echo isset($gs_soc_tixianghao)?$gs_soc_tixianghao:""; ?>"/>
                                        </td>

                                    </tr>


                                    <tr>
                                        <td><?= lang('货物件数'); ?> </td>
                                        <td>
                                            <input class="easyui-textbox" name="good_outers" id="good_outers" style="width:100px;"
                                                   value="<?php echo $good_outers; ?>"/>
                                            <select name="good_outers_unit" id="good_outers_unit" class="easyui-combobox" style="width:155px;" data-options="
                                                valueField:'name',
                                                textField:'name',
                                                value:'<?= $good_outers_unit;?>',
                                                url: '/bsc_dict/get_option/packing_unit',
                                                onHidePanel: function() {
                                                    var valueField = $(this).combobox('options').valueField;
                                                    var val = $(this).combobox('getValue');
                                                    var allData = $(this).combobox('getData');
                                                    var result = true;
                                                    for (var i = 0; i < allData.length; i++) {
                                                        if (val == allData[i][valueField]) {
                                                            result = false;
                                                        }
                                                    }
                                                    if (result) {
                                                        $(this).combobox('clear');
                                                    }
                                                },
                                                onSelect: function (data) {
                                                    var good_outers = $('#good_outers').val();
                                                    var all = $('#good_outers_unit').combobox('getData');
                                                    var name1 = data.name;
                                                    var name2 = data.name;
                                                    $.each(all,function(index,item){
                                                           if(data.value == item.value && data.name != item.name){
                                                                name2 = item.name;
                                                           }
                                                    })
                                                    var len1 = name1.length;
                                                    var len2 = name2.length;
                                                    if(good_outers > 1){
                                                        if(len2>len1){
                                                            $('#good_outers_unit').combobox('setValue', name2);
                                                        }
                                                    }else{
                                                        if(len2<len1){
                                                            $('#good_outers_unit').combobox('setValue', name2);
                                                        }
                                                    }
                                                }
                                            ">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('货物重量'); ?> </td>
                                        <td>
                                            <input class="easyui-textbox" name="good_weight" id="good_weight" style="width:100px;"
                                                   value="<?php echo $good_weight; ?>"/>
                                            <select readonly name="good_weight_unit" id="good_weight_unit" class="easyui-combobox"style="width:155px;">
                                                <option value="KGS">KGS</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('货物体积'); ?> </td>
                                        <td>
                                            <input class="easyui-textbox" name="good_volume" id="good_volume" style="width:100px;"
                                                   value="<?php echo $good_volume; ?>"/>
                                            <select readonly name="good_volume_unit" id="good_volume_unit" class="easyui-combobox"style="width:155px;">
                                                <option value="CBM">CBM</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('货物类型'); ?></td>
                                        <td>
                                            <select class="easyui-combobox" name="goods_type" id="goods_type" style="width: 255px;" data-options="
                                                valueField:'value',
                                                textField:'valuename',
                                                value:'<?= $goods_type; ?>',
                                                url:'/bsc_dict/get_option/goods_type',
                                            ">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><hr></td>
                                    </tr>
                                    <?php
                                    foreach($userList as $key => $value){
                                        //海外代理单独和agent一起放
                                        if($key == 'oversea_cus')continue;
                                        if($key == 'marketing')continue;
                                        if($key == 'booking')continue;
                                        if($key == 'document')continue;
                                        $group_key = $key . '_group';
                                        ?>
                                        <tr>
                                            <td><?php echo lang($key); ?></td>
                                            <td>
                                                <select id="<?php echo $key;?>_id" class="easyui-combobox"  editable="true" name="<?php echo $key;?>_id" style="width:255px;" data-options="
            									valueField:'id',
            									textField:'name',
            									url:'/bsc_user/get_data_role/<?php echo $key; ?>?status=0',
            									value:'<?php echo $value;?>',
            									onSelect: function(rec){
            									    $('#<?= $group_key;?>').val(rec.group);
            									},
            									onHidePanel: function() {
            										var valueField = $(this).combobox('options').valueField;
            										var val = $(this).combobox('getValue');
            										var allData = $(this).combobox('getData');
            										var result = true;
            										for (var i = 0; i < allData.length; i++) {
            											if (val == allData[i][valueField]) {
            												result = false;
            											}
            										}
            										if (result) {
            											$(this).combobox('clear');
            										}
            								    },
            									">
                                                </select>
                                                <input type="hidden" id="<?php echo $key;?>_group" name="<?php echo $key;?>_group" style="width:255px;" value="<?php echo $$group_key;?>">
                                                <a href='/bsc_user/person_info/<?php echo $value; ?>' target='_blank' title='查看该用户详情'>A</a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </talbe>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="trans_carrier_rule" class="easyui-window" data-options="closed:true,title:'<?= lang('船公司规则');?>',showType:'slide',width: '600px', height:$(window).height(),timeout: 0," style="top:0;left:0;">
    <iframe id="trans_carrier_rule_iframe" scrolling="auto" frameborder="0" style="width:100%;height:99%;"></iframe>
</div>
</body>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">

<div id="trans_origin_div">
    <form id="trans_origin_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_origin_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_origin_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_origin_click" iconCls="icon-search" onclick="query_report('trans_origin', 'trans_origin')"><?= lang('search');?></a>
        </div>
    </form>
</div>
<div id="trans_discharge_div">
    <form id="trans_discharge_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_discharge_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_discharge_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_discharge_click" iconCls="icon-search" onclick="query_report('trans_discharge', 'trans_discharge')"><?= lang('search');?></a>
        </div>
    </form>
</div>
<div id="trans_destination_div">
    <form id="trans_destination_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <label><?= lang('码头');?>:</label><input name="terminal" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_destination_click" iconCls="icon-search" onclick="query_report('trans_destination', 'trans_destination')"><?= lang('search');?></a>
        </div>
    </form>
</div>
<div id="agentTb">
    <form id="agentTbform">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('company_name'); ?>:</label>
            <input name="company_name" class="easyui-textbox agent_search" style="width:96px;"
                   data-options="prompt:'text'"/>
        </div>
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('country_code'); ?>:</label>
            <select name="country_code" class="easyui-textbox agent_search" style="width:96px;"
                    data-options="prompt:'text',valueField:'value',textField:'namevalue', url:'/bsc_dict/get_option/country_code'"></select>
        </div>
        <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'icon-search'"
           id="agentQueryReport"><?= lang('search'); ?></a>
        <!--<a href="javascript:void(0);" class="easyui-linkbutton" id="selfonclick"><?= lang('自出单'); ?></a>-->
    </form>
</div>
