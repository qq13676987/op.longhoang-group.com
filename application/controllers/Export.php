<?php


class export extends Common_Controller
{
    private $file_type = array(
        'doc' => 'word',
        'docx' => 'word',
        'xlsx' => '',
        'xls' => '',
    );
    protected $admin_field = array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document');//, 'finance');
    private $data = array();//存放当前需要用到的数据

    private $model = array();

    private $parameter = array();

    private $attached = array();

    private $sql = array();

    private $main_table = '';
    //用于配置表名
    private $table_array = array(
        'shipment' => 'biz_shipment',
        'wlk_apply' => 'biz_bill_payment_wlk_apply',
        'wlk_apply_relation' => 'biz_bill_payment_wlk_apply_relation',
        'group' => 'bsc_group',
        'consol' => 'biz_consol',
        'client' => 'biz_client',
        'client_account' => 'biz_client_account',
        'overseas_account' => 'bsc_overseas_account',
        'bill' => 'biz_bill',
        'charge' => 'biz_charge',
        'duty' => 'biz_duty_new',
        'user' => 'bsc_user',
        'truck' => 'biz_shipment_truck',
        'container' => 'biz_container',
        'shipment_container' => 'biz_shipment_container',
        'port' => 'biz_port',
        'dict' => 'bsc_dict',
        'invoice' => 'biz_bill_invoice',
        'soc' => 'biz_consol_company_soc',
        'soc_tixinagdian' => 'biz_consol_company_soc_tixiangdian',
        'log' => 'sys_log',
        'kml' => 'biz_kml',
        'si_change_apply' => 'biz_si_change_apply',
    );

    //用于配置特殊关系,一般的1对1可直接 表A.表A字段-表B字段.表B
    //键名为特殊关系名,表A.特殊关系.表B
    //string: id_type = 'shipment'
    //field: id_no = shipment.id
    private $field_group = array(
        '_shipment_si_change_apply' => array(
            array('id_type', '=', 'shipment', 'string'),
            array('id_no', '=', 'id', 'field'),
        ),
        '_shipment_bill' => array(
            array('id_type', '=', 'shipment', 'string'),
            array('id_no', '=', 'id', array('parameter6', 'invoice_id')),
//            array('type', '=', 'sell', 'string'),
            array('charge_code', '!=', 'YJ', 'string'),
            array('client_code', '=', 'client_code', 'parameter'),
            array('invoice_id', 'in', 'invoice_id', 'parameter3'),
            array('confirm_date', '=', 'is_confirm_date', 'parameter5'),
        ),
        '_shipment_bill_sell' => array(
            array('id_type', '=', 'shipment', 'string'),
            array('id_no', '=', 'id', array('parameter6', 'invoice_id')),
            array('client_code', '=', 'client_code', 'parameter'),
            array('charge_code', '!=', 'YJ', 'string'),
            array('type', '=', 'sell', 'string'),
            array('invoice_id', 'in', 'invoice_id', 'parameter3'),
            array('confirm_date', '=', 'is_confirm_date', 'parameter5'),
            array('currency', '=', 'currency', 'parameter3'),
        ),
        '_shipment_bill_cost' => array(
            array('id_type', '=', 'shipment', 'string'),
            array('id_no', '=', 'id', array('parameter6', 'invoice_id')),
            array('client_code', '=', 'client_code', 'parameter'),
            array('charge_code', '!=', 'YJ', 'string'),
            array('type', '=', 'cost', 'string'),
            array('invoice_id', 'in', 'invoice_id', 'parameter3'),
            array('confirm_date', '=', 'is_confirm_date', 'parameter5'),
        ),
        '_consol_bill' => array(
            array('id_type', '=', 'consol', 'string'),
            array('id_no', '=', 'id', 'field'),
            array('charge_code', '!=', 'YJ', 'string'),
            array('invoice_id', '=', 'invoice_id', 'parameter3'),
            array('confirm_date', '=', 'is_confirm_date', 'parameter5'),
        ),
        '_shipment_account' => array(
            array('id', '=', 'account_id', 'parameter2'),
        ),
        '_shipment_truck_par' => array(
            array('shipment_id', '=', 'id', 'field'),
            array('id', '=', 'truck_id', 'parameter4'),
        ),
        '_bill_shipment_wlk' => array(
            array('id', '=', 'id_no', 'field'),
        ),
        '_bill_consol_wlk' => array(
            array('id', '=', 'id_no', 'field'),
        ),
        '_consol_log' => array(
            array('table_name', '=', 'biz_consol', 'string'),
            array('`key`', '=', 'id', 'field'),
        ),
        '_shipment_log' => array(
            array('table_name', '=', 'biz_shipment', 'string'),
            array('`key`', '=', 'id', 'field'),
        ),
        '_consol_soc' => array(
            array('gs_soc_tixianghao', '=', 'gs_soc_tixianghao', 'parameter'),
        ),
        '_shipment_duty_sales' => array(
            array('id_type', '=', 'biz_shipment', 'string'),
            array('id_no', '=', 'id', 'field'),
            array('user_role', '=', 'sales', 'string'),
        ),
        '_shipment_duty_operator' => array(
            array('id_type', '=', 'biz_shipment', 'string'),
            array('id_no', '=', 'id', 'field'),
            array('user_role', '=', 'operator', 'string'),
        ),
        '_shipment_duty_customer_service' => array(
            array('id_type', '=', 'biz_shipment', 'string'),
            array('id_no', '=', 'id', 'field'),
            array('user_role', '=', 'customer_service', 'string'),
        ),
        '_shipment_duty_oversea_cus' => array(
            array('id_type', '=', 'biz_shipment', 'string'),
            array('id_no', '=', 'id', 'field'),
            array('user_role', '=', 'oversea_cus', 'string'),
        ),
        '_consol_duty_operator' => array(
            array('id_type', '=', 'biz_consol', 'string'),
            array('id_no', '=', 'id', 'field'),
            array('user_role', '=', 'operator', 'string'),
        ),
        '_consol_duty_oversea_cus' => array(
            array('id_type', '=', 'biz_consol', 'string'),
            array('id_no', '=', 'id', 'field'),
            array('user_role', '=', 'oversea_cus', 'string'),
        ),
    );

    private $template = array();

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 读取文件
     * @param $file_path
     * @param $type
     * @return bool|\PhpOffice\PhpWord\TemplateProcessor
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     */
    public function read_file($file_path, $type) {
        if($type == 'word'){
//            $phpWord = new \PhpOffice\PhpWord\PhpWord();
            return new \PhpOffice\PhpWord\TemplateProcessor($file_path);
        }

        return false;
    }

    /**
     * 这里进行导出后的一些处理
     */
    private function export_handler_after(){
        $template = $this->template; // 这个就是数据库里的模板的配置信息,
        if($template['data_table'] == 'shipment' && $template['type'] == 'B/L'){
            $is_formal = getValue('is_formal', 'false');
            if($is_formal === 'false') return false;
            //如果是shipment,这里获取下自己的船开标记
            $chuanyiqihang = $this->get_data_field($this->main_table, 'chuanyiqihang');
            $id = $this->get_data_field($this->main_table, 'id');
            $release_type = $this->get_data_field($this->main_table, 'release_type');
            if($chuanyiqihang !== '0'){
                //船开后,下载完成时,将,对应的 签发 勾上
                $update_data =array(
                    'tidanqianfa' => date('Y-m-d H:i:s'),
                );
                $this->model['biz_shipment_model']->update($id, $update_data);

                // record the log
                $log_data = array();
                $log_data["table_name"] = "biz_shipment";
                $log_data["key"] = $id;
                $log_data["action"] = "export_update";
                $log_data["value"] = json_encode($update_data);
                log_rcd($log_data);

                //且将文件上传到upload
                $header = array(
                    "Content-Type:multipart/form-data;charset=UTF-8;",
                );

                //需要将文件存到本地后才能处理

                $temp_path = temp_upload_file($this->export_filename, $this->export_content);


                //这里想尽量走统一上传接口上传, 所以调用了接口远程上传,但是file如何传过去还在研究,
                $type_name = 'B/L telex release';
                if($release_type == 'OBL') $type_name = 'B/L original';
                if($release_type == 'SWB') $type_name = 'B/L Seaway';

                $post_data = array(
                    'biz_table' => 'biz_shipment',
                    'id_no' => $id,
                    'type_name' => $type_name,
                    'file' => curl_file_create(realpath($temp_path)), // 5.3之后就不能用@了
                );

                //上传文件
                curl_post_body_file(base_url() . 'bsc_upload/wb_file_upload?up_location=local&user_id=' . get_session('id'), $header, $post_data);
            }
        }
    }

    /**
     * 导出模板后的一些处理
     */
    private function export_handler_before(){
        $template = $this->template; // 这个就是数据库里的模板的配置信息,
        if($template['data_table'] == 'shipment' && $template['type'] == 'B/L') {
            // //如果是shipment,这里获取下自己的船开标记
            // $chuanyiqihang = $this->get_data_field($this->main_table, 'chuanyiqihang');
            // $tidanqianfa = $this->get_data_field($this->main_table, 'tidanqianfa');
            // $id = $this->get_data_field($this->main_table, 'id');
            // $is_formal = getValue('is_formal', 'false');
            // if($is_formal === 'false') {
            //     $this->document->setValue('uniqid', '');
            //     return false;
            // }

            // //如果已经勾选签单,那么这里直接拒绝返回
            // if($tidanqianfa !== '0'){
            //     echo "<script>alert('提单已签发,请勿重复生成');history.go(-1)</script>";
            //     exit();
            // }

            // $uniqid = '';
            // if ($chuanyiqihang !== '0') {
            //     //这里设置唯一编号
            //     $uniqid = $id . $template['id'] . date('Ymd');
            //     $pdf = isset($_GET['pdf']) ? $_GET['pdf'] : '';
            //     //船开后只能下载PDF
            //     // if($pdf != 1){
            //     //     echo "<script>alert('船开后只能下载PDF');history.go(-1)</script>";
            //     //     exit();
            //     // }
            // }
            // $this->document->setValue('uniqid', $uniqid);
        }
    }

    /**
     * 这个用于切割字段作为数组使用
     * 例子 a=1&b=3&c=4 切割后为 array('a' => 1, 'b' => 3, 'c' => 4);
     * @param string $string
     * @param string $split_s
     * 后面如果想变更=符号的话,可以加个参数
     * @return Array
     */
    static function split_field($string, $split_s = '&amp;'){
        $string_arr = explode($split_s, $string);

        $data = array();

        foreach ($string_arr as $string_val){
            $string_val_arr = explode('=', $string_val);
            if(sizeof($string_val_arr) == 1) array_unshift($string_val_arr, 'value');
            $data[] = $string_val_arr;
        }

        //如果没值,直接返回默认切割的
        return array_column($data, 1, 0);
    }

    /**
     * 导出word文件
     * @param int $dump_type
     * @return bool|void
     * @throws \PhpOffice\PhpWord\Exception\CopyFileException
     * @throws \PhpOffice\PhpWord\Exception\CreateTemporaryFileException
     */
    public function export_word($dump_type = 0){
        ini_set('pcre.backtrack_limit', 999999999);
        $file_name = isset($_GET['file_name']) ? $_GET['file_name'] : '';
        $table = isset($_GET['data_table']) ? $_GET['data_table'] : '';
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $pdf = isset($_GET['pdf']) ? $_GET['pdf'] : '';
        $this->parameter = isset($_POST['parameter']) ? $_POST['parameter'] : array();
        $template_id = getValue('template_id');

        //查询模板对应的数据
        $this->load->model('bsc_template_model');
        $this->model['bsc_template_model'] = $this->bsc_template_model;
        if($file_name){
            $template = $this->bsc_template_model->get_one('file_template',$file_name);
        }else{
            $template = $this->bsc_template_model->get_one('id',$template_id);
            $file_name = $template['file_template'];
        }

        $this->template = $template;
        if(empty($template)){
            echo '模板已更新或不存在';
            return;
        }

        //主表查询
        $file_type = $this->getFileType($file_name);
        $type = $this->file_type[$file_type];
        $gst = get_system_type();
        if(explode('/', $file_name)[1] >= '20230727'){
            $file_path = './upload/' . $gst . $file_name;
        }else{
            $file_path = './upload' . $file_name;
        }


        if(!file_exists($file_path)) return false;

        $document = $this->read_file($file_path,$type);

        $this->document = $document;

        //获取所有匹配的变量
        $Allvariable = $this->document->getVariables();
//        $Allvariable[1] = array_unique($Allvariable[1]);//[0]是带${}的,[1]没有${}

        $Allvariable = array_filter($Allvariable, function ($var){
            if(explode('-', $var)[0] != 'for' && $var[0] != '/' && $var != 'attached' && $var != 'uniqid'){
                return $var;
            }
        });//[0]是带${}的,[1]没有${}
        $backgroup_key = array_search('backgroud', $Allvariable);
        if($backgroup_key !== false){
            $is_backgroup = true;
            unset($Allvariable[$backgroup_key]);
        }else{
            $is_backgroup = false;
        }
        sort($Allvariable);

        $table_array = $this->table_array;
        //获取主model名称
        $main_table = isset($table_array[$table]) ? $table_array[$table] : '';
        $this->main_table = $table;

        if($main_table == '' && $id == ''){
            echo 1;
            return false;
        }
        $model_name = $main_table . '_model';
        $this->load->model($model_name);
        $this->model[$model_name] = $this->$model_name;
        $this->db->limit(1);
        $this->data[$table] = $this->$model_name->no_role_get("id = '$id'");

        //没有主数据
        if(empty($this->data[$table])){
            echo 2;
            return false;
        }

        $this->export_handler_before($template); //导入模板前的一些相关处理

        $result = array();
        //普通的 consol.job_no  获取consol里的job_no，注：consol必须为主表才有值，若传过来的是shipment，但是获取的consol，则为空
        //一对多的 consol.bill.id.[] 代表consol为主的，获取bill里的第一个对应值的ID
        //一对多的 consol.bill.id 代表consol为主的，获取bill里的第一个对应值的ID
        //求总和,个数的 consol.bill.money.sum 获取consol相关的bill表的money字段的和
        $data_array = array(
            'sum' => array(),
            'count' => array(),
            'data' => array(),
        );//需要获取多个数据的
        foreach ($Allvariable as $key => $val){
            $val_arr = explode('.', $val);
            $val_length = sizeof($val_arr);
            //需要获取的字段为最后一个
            $need_field = $val_arr[$val_length - 1];
            unset($val_arr[$val_length - 1]);
            if (sizeof($val_arr) <= 2){//单表获取,只匹配主数据
                //例:  $this->data[$this->main_table][$val_arr[1]] = $this->data['biz_shipment']['id']
                $json = $this->get_special_value($table, $need_field);
                if($json === false){
                    $this_value = $this->get_data_field($table, $need_field);
                }else{
                    $this_value = $json;
                }
            }else {
                $field_name = $val_arr[sizeof($val_arr) - 1];
                unset($val_arr[sizeof($val_arr) - 1]);
                $this_key = join('.', $val_arr);
                //shipment.money.sum
                $need_field_arr = self::split_field($need_field);
                if($need_field_arr['value'] == '[]'){
                    //这里改为,如果循环的时候,[]+后面没有东西,默认就使用get_data 获取,有的话,使用该字段获取
                    if(!isset($need_field_arr['count_field'])){
                        $data = $this->get_data($val_arr);
                        $array_size = sizeof($data);

                    }else{
                        $array_size = $this->get_data_field($this_key, $need_field_arr['count_field'], 0, 1);
                    }


                    //这里的循环值需要变动

                    try{
                        $this->document->cloneRow($this_key . '.' . $field_name . '.' . $need_field, $array_size);
                    }catch (Exception $e){

                    }
                    $this_value = array();
                    for ($i = 0; $i < $array_size; $i++){
                        $this_value[$this_key . '.' . $field_name . '.' . $need_field . '#' . ($i + 1)] = $this->get_data_field($this_key, $field_name, $i,1);
                    }

                }
                // else if ($need_field == 'sum') {

                //     $field_name = 'sum(' . $field_name . ')';

                //     $data_array['sum'][$this_key]['field'][] = $field_name;
                //     $data_array['sum'][$this_key]['file'][] = $val;
                //     $this_value = false;
                // } 
                // else if ($need_field == 'count') {
                //     $field_name = 'count(' . $field_name . ')';

                //     $data_array['count'][$this_key]['field'][] = $field_name;
                //     $data_array['count'][$this_key]['file'][] = $val;

                //     $this_value = false;
                // }
                else if($need_field == 'last'){
                    $data = $this->get_data($val_arr);

                    $data_length = sizeof($data);
                    $this_value = $this->get_data_field(join('.', $val_arr), $field_name, $data_length - 1);
                }
                else {
                    $val_arr[] = $field_name;
                    $val_arr = array_values($val_arr);
                    $json = $this->get_special_value(join('.', $val_arr), $need_field);
                    if($json === false){
                        $this->get_data($val_arr);
                        $this_value = $this->get_data_field(join('.', $val_arr), $need_field);
                    }else{
                        $this_value = $json;
                    }
                }
            }

            if ($this_value !== false){
                if(is_array($this_value)){
                    //var_dump($this_value);
                    foreach ($this_value as $tv_key => $tv_val){
                        $result[] = array($tv_key, '=>', $tv_val);
                        // echo ($tv_key . '=>');
                        // var_dump($tv_val);
                        // echo '<br>';
                        $this->document->setValue($tv_key, $tv_val);
                    }
                }else{
                    $result[] = array($val, '=>', $this_value);
                    // echo ($val . '=>');
                    // var_dump($this_value);
                    // echo '<br>';
                    $this->document->setValue($val, $this_value);
                }
            }
        }

        //处理特殊的值
        //循环sum,count,data
        foreach ($data_array as $key => $val){
            foreach ($val as $k => $v){
                if($key == 'sum'){
                    $data_key = $k . '_' . $key;
                }else if($key == 'count'){
                    $data_key = $k . '_' . $key;
                }else{
                    $data_key = $k;
                }
                array_unique($v['field']);
                $field = join(',', $v['field']);
                $this->get_data(explode('.', $k), $key, $field);
                foreach ($v['file'] as $vfk => $vf){
                    $this_value = $this->get_data_field($data_key, $v['field'][$vfk], 0);
                    //等于空值时,匹配下特殊值,查找特殊值是否存在当前字段
                    if($this_value == NULL){
                        $this_value = $this->get_special_value($data_key, $v['field'][$vfk], 0);
                    }
                    $result[] = array($vf, '=>', $this_value);
                    // echo ($vf . '=>');
                    // var_dump($this_value);
                    // echo '<br>';
                    $this->document->setValue($vf, $this_value);
                }
            }
        }

        //设置背景图
        if($table == 'shipment'){
            if($is_backgroup){
                //新加以签单日期为准,若签单日期为空,则取值ATD
                if(strpos($file_name, '22-HOST-DF')){
                    $key = 'shipment.consol_id-id.consol';
                    $this->get_data(explode('.', $key));
                    $shipment_issue_date = $this->get_data_field('shipment', 'issue_date');

                    if(empty($shipment_issue_date) || $shipment_issue_date == '0000-00-00'){
                        $shipment_issue_date = $this->get_data_field('shipment.consol_id-id.consol', 'trans_ATD');
                    }

                    $release_type = $this->get_data_field('shipment', 'release_type');
                    $company = '';

                    //如果container无值,则只能拉draft单
                    $shipment_containers = $this->get_data(explode('.', 'shipment.id-shipment_id.shipment_container'));
                    if(empty($shipment_issue_date) || $shipment_issue_date == '0000-00-00' || $shipment_issue_date == '0000-00-00 00:00:00' || sizeof($shipment_containers) <= 0){//ATD无值 draft
                        $img_name = 'draft' . $company . '.jpg';
                    }else{
                        //ATD 有值且电放，只能 telex release版本
                        if($release_type == 'TER'){
                            $img_name = 'ter' . $company . '.jpg';
                        }else{//ATD有值且没电放，只能空白背景或者ON BOARD背景.
                            $img_name = 'obl' . $company . '.jpg';
                        }
                    }
                    $this->document->setImageValue('backgroud', array('path' => './inc/image/' . $img_name, 'width' => '791', 'height' => '1104'), \PhpOffice\PhpWord\TemplateProcessor::MAXIMUM_REPLACEMENTS_DEFAULT, 1);
                }else if(strpos($file_name, '23-HOST-DF-jx')){
                    $key = 'shipment.consol_id-id.consol';
                    $this->get_data(explode('.', $key));
                    $shipment_issue_date = $this->get_data_field('shipment', 'issue_date');
                    if(empty($shipment_issue_date) || $shipment_issue_date == '0000-00-00'){
                        $shipment_issue_date = $this->get_data_field('shipment.consol_id-id.consol', 'trans_ATD');
                    }
                    $release_type = $this->get_data_field('shipment', 'release_type');
                    $company = '_jx';

                    //如果container无值,则只能拉draft单
                    $shipment_containers = $this->get_data(explode('.', 'shipment.id-shipment_id.shipment_container'));

                    if(empty($shipment_issue_date) || $shipment_issue_date == '0000-00-00' || $shipment_issue_date == '0000-00-00 00:00:00' || sizeof($shipment_containers) <= 0){//ATD无值 draft
                        $img_name = 'draft' . $company . '.jpg';
                    }else{
                        //ATD 有值且电放，只能 telex release版本
                        if($release_type == 'TER'){
                            $img_name = 'ter' . $company . '.jpg';
                        }else{//ATD 有值且没电放，只能空白背景或者ON BOARD背景.
                            $img_name = 'obl' . $company . '.jpg';
                        }
                    }
                    $this->document->setImageValue('backgroud', array('path' => './inc/image/' . $img_name, 'width' => '791', 'height' => '1104'), \PhpOffice\PhpWord\TemplateProcessor::MAXIMUM_REPLACEMENTS_DEFAULT, 1);
                }
            }
        }else if($table == 'consol' && $is_backgroup){
            if($template['file_name'] == 'KML提单'){
                $trans_ATD = $this->get_data_field('consol', 'trans_ATD');
                $release_type = $this->get_data_field('consol', 'release_type');
                $company = '_jx';

                //如果container无值,则只能拉draft单
                $containers = $this->get_data(explode('.', 'consol.id-consol_id.container'));

                if(empty($trans_ATD) || $trans_ATD == '0000-00-00' || $trans_ATD == '0000-00-00 00:00:00' || sizeof($containers) <= 0){//ATD无值 draft
                    $img_name = 'draft' . $company . '.jpg';
                }else{
                    //ATD 有值且电放，只能 telex release版本
                    if($release_type == 'TER'){
                        $img_name = 'ter' . $company . '.jpg';
                    }else{//ATD 有值且没电放，只能空白背景或者ON BOARD背景.
                        $img_name = 'obl' . $company . '.jpg';
                    }
                }
                $this->document->setImageValue('backgroud', array('path' => './inc/image/' . $img_name, 'width' => '791', 'height' => '1104'), \PhpOffice\PhpWord\TemplateProcessor::MAXIMUM_REPLACEMENTS_DEFAULT, 1);
            }
        }

        //如果存在attached,则进行合并
        $str = '';
        if(!empty($this->attached)){
//            $target_word = \PhpOffice\PhpWord\IOFactory::load($path, "Word2007");
//            $sections = $target_word->getSections();
//            $section = $sections[0];
//            $header = array();
//            $section->addText('ATTACHED', $header);
//            $attached_table = $section->addTable();
//            $attached_table->addRow();
//            foreach ($this->attached as $arow){
//                $attached_table->addCell(800)->addText($arow);
//            }
//            $wordWriter = \PhpOffice\PhpWord\IOFactory::createWriter($target_word, "Word2007");
//            $path = 'upload/temp/target2.docx';
//            $wordWriter->save($path);
            $vessel = '';
            $voyage = '';
            $job_no = $this->get_data_field($this->main_table, 'job_no');
            if($table == 'shipment'){
                $vessel = $this->get_data_field('shipment.consol_id-id.consol', 'vessel');
                $voyage = $this->get_data_field('shipment.consol_id-id.consol', 'voyage');
            }else if($table == 'consol'){
                $vessel = $this->get_data_field($this->main_table, 'vessel');
                $voyage = $this->get_data_field($this->main_table, 'voyage');
            }
            if(!empty($voyage)){
                $voyage = ' V.' . $voyage;
            }

            $str = '<w:p w:rsidR="00B51AC6" w:rsidRDefault="00B51AC6" w:rsidP="00B51AC6"><w:r><w:rPr><w:rFonts w:ascii="Arial" w:eastAsia="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="000000"/><w:sz w:val="36"/></w:rPr><w:lastRenderedPageBreak/><w:t>ATTACH LIST</w:t></w:r></w:p><w:p w:rsidR="00B51AC6" w:rsidRDefault="00B51AC6" w:rsidP="00B51AC6"><w:pPr><w:spacing w:line="0" w:lineRule="atLeast"/><w:jc w:val="left"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:sz w:val="32"/><w:szCs w:val="32"/></w:rPr></w:pPr></w:p>';
            $str .= '<w:p><w:pPr> <w:jc w:val="left"/><w:rPr><w:rFonts w:hint="eastAsia" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:bCs/><w:sz w:val="16"/><w:szCs w:val="16"/><w:lang w:val="en-US" w:eastAsia="zh-CN"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="楷体_GB2312"/><w:b w:val="0"/><w:bCs/><w:sz w:val="16"/><w:szCs w:val="16"/></w:rPr><w:t>VESSEL</w:t></w:r><w:r><w:rPr><w:rFonts w:hint="eastAsia" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:bCs/><w:sz w:val="16"/><w:szCs w:val="16"/><w:lang w:val="en-US" w:eastAsia="zh-CN"/></w:rPr><w:t>:</w:t></w:r><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:bCs/><w:sz w:val="16"/><w:szCs w:val="16"/></w:rPr><w:t>' . $vessel . $voyage . '</w:t></w:r><w:r><w:rPr><w:rFonts w:hint="eastAsia" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:bCs/><w:sz w:val="16"/><w:szCs w:val="16"/><w:lang w:val="en-US" w:eastAsia="zh-CN"/></w:rPr><w:t xml:space="preserve">   </w:t></w:r></w:p><w:p><w:pPr><w:jc w:val="left"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:bCs/><w:sz w:val="16"/><w:szCs w:val="16"/></w:rPr></w:pPr><w:bookmarkStart w:id="1" w:name="_GoBack"/><w:bookmarkEnd w:id="1"/><w:r><w:rPr><w:rFonts w:hint="eastAsia" w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:bCs/><w:sz w:val="16"/><w:szCs w:val="16"/><w:lang w:val="en-US" w:eastAsia="zh-CN"/></w:rPr><w:t>BL NO:</w:t></w:r><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:bCs/><w:sz w:val="16"/><w:szCs w:val="16"/></w:rPr><w:t>' . $job_no . '</w:t></w:r></w:p><w:p><w:pPr><w:jc w:val="left"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b w:val="0"/><w:bCs/><w:sz w:val="16"/><w:szCs w:val="16"/></w:rPr></w:pPr></w:p>';
            //根据字段last_name对数组$data进行降序排列
            $attached_orders = array_column($this->attached,'order');
            array_multisort($attached_orders,SORT_ASC,$this->attached);
            foreach ($this->attached as $akey => $arow) {
                $result[$akey] = $arow['data'];
                $str .= '<w:p w:rsidR="00B51AC6" w:rsidRDefault="00B51AC6" w:rsidP="00B51AC6"><w:pPr><w:spacing w:line="0" w:lineRule="atLeast"/><w:jc w:val="left"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:sz w:val="32"/><w:szCs w:val="32"/></w:rPr></w:pPr><w:r w:rsidRPr="00FD7B89"><w:rPr><w:rFonts w:cstheme="minorHAnsi" w:hint="eastAsia"/><w:sz w:val="32"/><w:szCs w:val="32"/></w:rPr><w:t>' . strtoupper($akey) . '</w:t></w:r></w:p>';
                $str .= '<w:p w:rsidR="00B51AC6" w:rsidRDefault="00B51AC6" w:rsidP="00B51AC6"><w:br /><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi" w:hint="eastAsia"/><w:sz w:val="32"/><w:szCs w:val="32"/></w:rPr><w:t>-------------------------------------------</w:t></w:r>';
                $arow_arr = explode('<w:br />', $arow['data']);
                foreach ($arow_arr as $arow_row) {
                    $str .= '<w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi" w:hint="eastAsia"/><w:sz w:val="16"/><w:szCs w:val="16"/></w:rPr><w:br/><w:t>' . $arow_row . '</w:t></w:r>';
                }
                $str .= ' </w:p>';
            }
        }
        $this->document->setValue('attached', $str);
        //调试看SQL的
        // if(is_admin()){
        // dump($this->sql);
        // return;
        // }

        if($dump_type == 1){
            foreach ($result as $rv){
                echo $rv[0];
                echo $rv[1];
                var_dump($rv[2]);
                echo '<br>';
            }
            dump($this->sql);
            dump($this->data);
            return;
        }

        //3.保存文件
        $date_path = date('Ymd');
        $new_dir = './upload/temp/' . $date_path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $carrier_ref = '';
        $shipper_ref = '';
        if($table == 'shipment'){
            $this->get_data(explode('.', 'shipment.consol_id-id.consol'));
            $carrier_ref = $this->get_data_field('shipment.consol_id-id.consol', 'carrier_ref');
            $shipper_ref = str_replace(',', '', $this->get_data_field($table, 'shipper_ref'));
            $shipper_ref = str_replace('&amp;', '&', $shipper_ref);
            $shipper_ref = $this->replace_name($shipper_ref);
        }else if($table == 'consol'){
            $carrier_ref = $this->replace_name($this->get_data_field($table, 'carrier_ref'));
        }else if($table == 'wlk_apply'){
            $carrier_ref = $this->replace_name($this->get_data_field($table, 'id'));
        }
        //由于有人会输入过长的客户单号和船公司单号,所以这里超过15位自动切
        //2022-05-16 15位改为20位
        if(strlen($shipper_ref) > 20) $shipper_ref = substr($shipper_ref, 0, 20);
        if(strlen($carrier_ref) > 20) $carrier_ref = substr($carrier_ref, 0, 20);
        if($shipper_ref != '')$shipper_ref = ' ' . $shipper_ref;
        $file_name = $carrier_ref . ' '. $this->get_data_field($table, 'job_no') . $shipper_ref . '-' . $template['file_name'] . '.docx';
        //2022-05-26 修复了中文名导致文件下载乱码的问题

        //2022-08-08 临时文件新加时分秒参数,避免下载重复, 后续如果再出问题,改成微秒
        $temp_file_name = $template['id'] . get_session('id') . date('His') . '.docx';
        //2022-08-05 名字暂时不管了,PDF读取会出问题
        // $temp_file_name = $template['id'] . get_session('id') . $file_name;
        $path = 'upload/temp/' . $date_path . '/' . $temp_file_name;

        $file_name = iconv("utf-8","gbk//IGNORE",str_replace(array(','), '', $file_name));

        $this->document->saveAs($path);

        $this->download_log($template['file_name'], $pdf, 'biz_' . $table, $id);
        if($dump_type == 2){//表示输出WORD路径
            echo json_encode(array('code' => 0, 'msg' => '生成文件成功', 'file_path' => $path));
            return;
        }
        $pdf_url = "http://pdf.leagueshipping.com/dubai_leagueshipping_com.php";
        if($dump_type == 3){//表示输出PDF路径
            $file_name = $this->get_data_field($table, 'job_no');
            $export_name = $carrier_ref . ' '. $this->get_data_field($table, 'job_no') . $shipper_ref . '-' . $template['file_name'];
            $pdf_url .= "?name=" . urlencode($file_name) . "&temp_name=" . urlencode($date_path) . "/" . urlencode($temp_file_name) .  "&export_name=" . urlencode($export_name) . "&data_type=json";
            $result_json = curl_get($pdf_url);
            $result_json = str_replace('\ufeff', "", $result_json);
            $result = json_decode($result_json, true);
            echo json_encode(array('code' => 0, 'msg' => '文件生成成功', 'file_path' => $result['filepath'], 'file_name' => $result['filename'], $result_json));
            return;
        }
        if($pdf == 1){
            $file_name = $this->get_data_field($table, 'job_no');
            $export_name = $carrier_ref . ' '. $this->get_data_field($table, 'job_no') . '-' . $template['file_name'];
            $pdf_url .= "?name=" . urlencode($file_name) . "&temp_name=" . urlencode($date_path) . "/" . urlencode($temp_file_name) .  "&export_name=" . urlencode($export_name) . "&data_type=json";
            $result_json = curl_get($pdf_url);
            $result_json = str_replace('\ufeff', "", $result_json);
            $result = json_decode($result_json, true);
            if($result == false){
                echo '导出出错, 请联系管理员';
                return ;
            }

            $file_name = $result['filename'] . '.pdf';

            $file_url = "http://pdf.leagueshipping.com" . $result['filepath'];

//            echo "<script> window.open('http://pdf.leagueshipping.com/$u?name=$file_name&temp_name=$date_path/$temp_file_name.docx&export_name=$export_name');</script>";

            //4.从浏览器下载
            $fp = fopen($file_url,"r");
            //获取文件大小

            $header_array = get_headers($file_url, true);
            $file_size = $header_array['Content-Length'];

            $v_content = '';
            //循环是因为fread有8192的限制,不循环下载的文件不完整
            while (strlen($v_content) < $file_size) {
                $v_content .= fread($fp, $file_size - strlen($v_content));
            }

            $this->export_filename = $file_name;
            $this->export_content = $v_content;

            $this->export_handler_after($template);

            Header("Content-type: application/octet-stream");
            Header("Accept-Ranges: bytes");
            Header("Accept-Length: ".$file_size);
            Header("Content-Disposition: attachment; filename=$file_name");
            echo $v_content;
            fclose($fp);



            return;
            // $pdf_path = 'upload/temp/target.pdf';
            // $this->wordToPDF($path, $pdf_path);
            // // Saving the document as PDF file...
            // header('Content-type: application/pdf');
            // header('Content-Disposition: attachment; filename="' . $file_name . '"');
            // readfile($pdf_path);
            // return;
        }


        $fp = fopen($path,"r");
        $file_size = filesize($path);
        $v_content = '';
        //循环是因为fread有8192的限制,不循环下载的文件不完整
        while (strlen($v_content) < $file_size) {
            $v_content .= fread($fp, $file_size - strlen($v_content));
        }
        $this->export_filename = $temp_file_name;
        $this->export_content = $v_content;

        $this->export_handler_after($template);

        //4.从浏览器下载
        ob_clean();
        ob_start();
        Header("Content-type:application/octet-stream");
        Header("Accept-Ranges:bytes");
        Header("Accept-Length:".$file_size);
        Header("Content-Disposition:attchment; filename=$file_name");
        echo $v_content;
        fclose($fp);
        ob_end_flush();
        // $this->export_handler_after($template);

        // //4.从浏览器下载
        // ob_clean();
        // ob_start();
        // $fp = fopen($path,"r");
        // $file_size = filesize($path);
        // Header("Content-type:application/octet-stream");
        // Header("Accept-Ranges:bytes");
        // Header("Accept-Length:".$file_size);
        // Header("Content-Disposition:attchment; filename=$file_name");
        // $buffer = 1024;
        // $file_count = 0;
        // while (!feof($fp) && $file_count < $file_size){
        //     $file_con = fread($fp,$buffer);
        //     $file_count += $buffer;
        //     echo $file_con;
        // }
        // fclose($fp);
        // ob_end_flush();

        // echo '<script>history.go(-2);</script>';
    }

    public function export_excel(){
        $table = isset($_GET['data_table']) ? $_GET['data_table'] : '';
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $pdf = isset($_GET['pdf']) ? $_GET['pdf'] : '';
        $parameter = isset($_POST['parameter']) ? $_POST['parameter'] : array();
        $template_id = getValue('template_id');

        if(!is_admin()){
            echo "Under Maintenance";
            return;
        }
        $this->template = Model("bsc_template_model")->get_where_one("id = {$template_id}");

        $file_name = $this->template['file_template'];
        $gst = get_system_type();
        if(explode('/', $file_name)[1] >= '20230727'){
            $file_path = './upload/' . $gst . $file_name;
        }else{
            $file_path = './upload' . $file_name;
        }

        if(!file_exists($file_path)) return false;

        $this->load->library('PHPExcel/PHPExcel.php');
        //   'Excel2007',
        //        'Excel5',
        //        'Excel2003XML',
        //        'OOCalc',
        //        'SYLK',
        //        'Gnumeric',
        //        'HTML',
        //        'CSV',
        //根据后缀自动判断使用哪个
        $ext = explode('.', $file_name)[1];
        $inputFileTypeConfig = array(
            'xlsx' => 'Excel2007',
            'xls' => 'Excel5',
            'csv' => 'CSV',
        );
        $inputFileType = $inputFileTypeConfig[$ext];

        // $objPHPExcel = PHPExcel_IOFactory::load($file_path);     //加载excel文件,设置模板

        // $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);  //设置保存版本格式
        // $objActSheet = $objPHPExcel->getActiveSheet();

        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($file_path);//加载模板
        $sheet_index = 0;//sheet 索引

        //首先读取当前模板所有格子的信息, 这里只读取了最后一个sheet  如果后期做多sheet版本 注意!!!! TODO
        $sheetData = $objPHPExcel->getActiveSheet($sheet_index)->toArray(null,false,true,true);
        //调用模板代码引擎, 开始对对应的值进行转换
        $this->load->library('Variable_content');
        $this->Variable_content = new Variable_content($id);
        $this->Variable_content->setParameter($parameter);
        //array(7) {
        //  [1] => array(2) {
        //    ["A"] => NULL
        //    ["B"] => NULL
        //  }
        //}
        foreach ($sheetData as $row => $column){
            $is_foreach = false;
            $for_num = 0;
            foreach ($column as $key => $val){
                if(empty($val)) continue;//如果空的 不用检测
                $variables = $this->Variable_content->getVariables($val, true);
                if(empty($variables)) continue;//如果没有模板字段, 跳过
                foreach ($variables as $variable){
                    $clearVariable = explode('.', $this->Variable_content->cleanVariable($variable));

                    if(array_pop($clearVariable) === '[]'){
                        //首先检测去除[] 后, 数据有多少行
                        $for_num = sizeof($this->Variable_content->getData($clearVariable));
                        $is_foreach = true;
                        break 2;
                    }
                }
            }
            //确认循环
            if($is_foreach){
                //确认循环后, 先把当前行的 全部[] 去掉, 改为字段[数字]
                $new_column = array_map(function ($r){
                    $r_arr = explode('.', $r);
                    //如果最后一个不是[] 就加回去
                    if($end_r_arr = array_pop($r_arr) !== '[]}') {
                        return $r;
                    }else{
                        //是的话, 直接将最后一个 拼上[{count}]
                        $r_arr[sizeof($r_arr) - 1] = $r_arr[sizeof($r_arr) - 1] . '[{count}]}';
                        return join('.', $r_arr);
                    }
                }, $column);
                //先将当前行都替换为新值
                $val_index = 0;//这个是输出第几个内容的
                foreach ($new_column as $key => $val){
                    $this_val = str_replace('{count}', $val_index, $val);
//                    $this_val = $val;
//                    ->getFormattedValue()
                    $objPHPExcel->getActiveSheet()->setCellValue($key . $row, $this_val);
                }
                $val_index++;

                //初始为当前行
                $new_row = $row + 1;
                for ($i = 0; $i < $for_num - 1; $i++){
                    $objPHPExcel->getActiveSheet()->insertNewRowBefore($new_row, 1);//新增一行
                    $objPHPExcel->getActiveSheet()->copyRows($row, $new_row, 1, 1);//新增一行
                    //将新行都替换为新值
                    foreach ($new_column as $key => $val){
                        $this_val = str_replace('{count}', $val_index, $val);
                        if($this_val === NULL) continue;
                        $objPHPExcel->getActiveSheet()->setCellValue($key . $new_row, $this_val);
                    }
                    $new_row++;
                    $val_index++;//用完后++
                }
            }

        };
        //然后重置下查询
        $sheetData = $objPHPExcel->getActiveSheet($sheet_index)->toArray(null,false,true,true);
        foreach ($sheetData as $row => $column){
            //首先, 批量检测 数据里是否存在[] 的, 存在需要进行 循环, 且 重组数组数据
            foreach ($column as $key => $val){
                $this_val = $this->Variable_content->get($val);
                //将值再原路插入回去
                if($this_val === NULL) continue;
                if($this_val == $val) continue;//如果没变化的说明 不需要转化, 也不用管
                $objPHPExcel->getActiveSheet()->setCellValue($key . $row, $this_val);
            }
        }
        //拼接文件名--start
        //获取当前最后一行在哪, 然后在这里开始填充ATTACHED
        $attached = $this->Variable_content->getByVariable('${attached}', 0);
        if(!empty($attached)){
            end($sheetData);
            $last_column = key($sheetData);
            //首先现在 AL显示ATT
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $last_column, "ATTACH LIST")->getStyle('A1')->applyFromArray(array(
                'borders' => array(
                    'allborders' => array( //设置全部边框
                        'style' => \PHPExcel_Style_Border::BORDER_THIN //粗的是thick
                    ),
                ),
            ));
            //根据字段last_name对数组$data进行降序排列
            $attached_orders = array_column($attached,'order');
            array_multisort($attached_orders,SORT_ASC,$attached);
            $last_column++;
            $objPHPExcel->getActiveSheet()->setCellValue("A" . $last_column, '-------------------------------------------');
            $last_column++;
            foreach ($attached as $akey => $arow) {
                //第一列显示名称, 第二列显示值
                $objPHPExcel->getActiveSheet()->setCellValue("A" . $last_column, strtoupper($akey))->getStyle('A1')->applyFromArray(array(
                    'borders' => array(
                        'allborders' => array( //设置全部边框
                            'style' => \PHPExcel_Style_Border::BORDER_THIN //粗的是thick
                        ),
                    ),
                ));
                $last_column++;
                $arow_arr = explode('<w:br />', $arow['data']);
                foreach ($arow_arr as $arow_row){
                    $objPHPExcel->getActiveSheet()->setCellValue("B" . $last_column, strtoupper($arow_row));
                    $last_column++;
                }

            }
        }
        if(is_admin() && getValue('debug') === 'true'){
            $this->Variable_content->getDebugInfo();
            exit();
        }

        $carrier_ref = "";
        $shipper_ref = "";
        if($table == 'shipment'){
            $carrier_ref = $this->Variable_content->get('${shipment.consol_id-id.consol.carrier_ref}');
            $shipper_ref = str_replace(',', '', $this->Variable_content->get('${shipment.shipper_ref}'));
            $shipper_ref = str_replace('&amp;', '&', $shipper_ref);
//            $shipper_ref = $this->replace_name($shipper_ref);
        }else if($table == 'consol'){
            $carrier_ref = $this->Variable_content->get('${consol.carrier_ref}');
//            $carrier_ref = $this->replace_name($this->get_data_field($table, 'carrier_ref'));
        }else if($table == 'wlk_apply'){
//            $carrier_ref = $this->replace_name($this->get_data_field($table, 'id'));
        }
        //由于有人会输入过长的客户单号和船公司单号,所以这里超过15位自动切
        //2022-05-16 15位改为20位
        if(strlen($shipper_ref) > 20) $shipper_ref = substr($shipper_ref, 0, 20);
        if(strlen($carrier_ref) > 20) $carrier_ref = substr($carrier_ref, 0, 20);
        if($shipper_ref != '')$shipper_ref = ' ' . $shipper_ref;
        $file_name = $carrier_ref . ' '. $this->Variable_content->get('${' . $table . 'job_no}') . $shipper_ref . '-' . $this->template['file_name'] . '';
        //2022-05-26 修复了中文名导致文件下载乱码的问题
        $file_name = iconv("utf-8","gbk//IGNORE",$file_name);
        //拼接文件名--end

        //这里是处理公式的地方
        $sheetData = $objPHPExcel->getActiveSheet($sheet_index)->toArray(null,true,true,true);
        //重置下匹配条件 {{ xxxxxx }}
        $this->Variable_content->setVariableRule("/{{(.*?)}}/i");
        $this->Variable_content->setCleanRule("/[{|}]/");
        foreach ($sheetData as $row => $column){
            foreach ($column as $key => $val){
                //开启查找php代码模式, 如果存在, 那么使用
                //检测是否满足 {{ }}格式, 满足的话,
                $variables = $this->Variable_content->getVariables($val);
                foreach ($variables as $variable){
                    //执行php 代码
                    //这里需要设置一个代码白名单 不然会有风险, 目前我先写死判断吧
                    $variableData = eval("return " . $this->Variable_content->cleanVariable($variable));

                    $objPHPExcel->getActiveSheet()->setCellValue($key . $row, $variableData);
                }
            }
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $inputFileType);

        $pdf_url = "http://pdf.leagueshipping.com/dubai_leagueshipping_com.php";
        if($pdf == 1){
            $temp_file_name = $this->template['id'] . get_session('id') . date('His') . '.xlsx';

            //3.保存文件
            $date_path = date('Ymd');
            $new_dir = './upload/temp/' . $date_path . '/';
            if (!file_exists($new_dir)) {
                mkdir($new_dir, 0777, true);
            }
            $path = 'upload/temp/' . $date_path . '/' . $temp_file_name;


            $file_name = iconv("utf-8","gbk//IGNORE",str_replace(array(','), '', $file_name));

            $objWriter->save($path);

            $file_name = $this->Variable_content->get('{{' . $table . '.job_no}}');
            $export_name = $carrier_ref . $this->Variable_content->get('{{' . $table . '.job_no}}') . '-' . $this->template['file_name'];
            $pdf_url .= "?name=" . urlencode($file_name) . "&temp_name=" . urlencode($date_path) . "/" . urlencode($temp_file_name) .  "&export_name=" . urlencode($export_name) . "&data_type=json";
            $result_json = curl_get($pdf_url);
            $result_json = str_replace('\ufeff', "", $result_json);
            $result = json_decode($result_json, true);
            if($result == false){
                echo($result_json);
                echo lang('导出出错, 请联系管理员');
                return ;
            }
            //if(is_admin()){
            //   var_dump($result);
            //   return;
            //}

            $file_name = $result['filename'] . '.pdf';

            $file_url = "http://pdf.leagueshipping.com" . $result['filepath'];

            //4.从浏览器下载
            $fp = fopen($file_url,"r");
            //获取文件大小

            $header_array = get_headers($file_url, true);
            $file_size = $header_array['Content-Length'];

            $v_content = '';
            //循环是因为fread有8192的限制,不循环下载的文件不完整
            while (strlen($v_content) < $file_size) {
                $v_content .= fread($fp, $file_size - strlen($v_content));
            }

            // $this->export_filename = $file_name;
            // $this->export_content = $v_content;

            // $this->export_handler_after($template);

            Header("Content-type: application/octet-stream");
            Header("Accept-Ranges: bytes");
            Header("Accept-Length: ".$file_size);
            Header("Content-Disposition: attachment; filename=$file_name");
            echo $v_content;
            fclose($fp);
            return;
        }

        ob_end_clean();
        header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition:inline;filename=" . $file_name . ".xlsx");//attachment新窗口打印inline本窗口打印
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter->save('php://output');
    }

    /**
     * 去除一些不允许的符号
     */
    private function replace_name($name){
        $name = str_replace('\\', ' ', $name);
        $name = str_replace('/', ' ', $name);
        $name = str_replace(':', ' ', $name);
        $name = str_replace('*', ' ', $name);
        $name = str_replace('?', ' ', $name);
        $name = str_replace('"', ' ', $name);
        $name = str_replace('<', ' ', $name);
        $name = str_replace('>', ' ', $name);
        $name = str_replace('|', ' ', $name);
        $name = str_replace(',', ' ', $name);
        return $name;
    }

    public function download_log($file_path, $pdf = '', $id_type = '', $id_no = 0){
        $download_model = Model('m_model');
        $download_model::$table = 'sys_download_log';

        $file_path_arr = explode('/', $file_path);
        $file = end($file_path_arr);

        $file_name_arr = explode('.', $file);
        $file_name = $file_name_arr[0];

        //如果PDF等于1 那么file_name 的尾号换成1
        if($pdf == 1) $file_type = 'pdf';
        else $file_type = 'docx';

        $add_data = array();
        $add_data['file_path'] = $file_path;
        $add_data['file_name'] = $file_name . '.' . $file_type;
        $add_data['id_type'] = $id_type;
        $add_data['id_no'] = $id_no;
        $add_data['user_id'] = get_session('id');
        $add_data['insert_time'] = date('Y-m-d H:i:s');

        $download_model->save($add_data);
        return true;
    }

    function wordToPDF($file_path, $pdf_path)
    {
        $unoconv = Unoconv\Unoconv::create(array(
            'unoconv.binaries' => '/opt/libreoffice6.4/unoconv-master/unoconv',
        ));
        $unoconv->transcode($file_path, 'pdf', $pdf_path);
    }

    /**
     * 获取model
     * @param $model_name
     * @return mixed
     */
    public function get_model($model_name){
        if(!in_array($model_name, $this->model)){
            $this->load->model($model_name);
            $this->model[$model_name] = $this->$model_name;
        }

        return $this->model[$model_name];
    }

    /**
     * 获取需要用到的数据
     * @param $condition
     * @param string $type
     * @param string $field
     * @return mixed
     */
    public function get_data($condition, $type = 'data', $field = ''){
        //shipment.consol_id.consol.id.bill.money
        //shipment.consol_id.consol.agent_address

        $data_name = join('.', $condition);
        if($type == 'sum'){
            $data_name .= '_sum';
        }else if($type == 'count'){
            $data_name .= '_count';
        }
        if(isset($this->data[$data_name])){
            return $this->data[$data_name];
        }
        $count = sizeof($condition) / 2;

        $field_group = $this->field_group;


        for ($i = 1; $i <= $count; $i++){
            if(get_session('id') == '20215'){
            }
            $table_name = isset($this->table_array[$condition[$i * 2]]) ? $this->table_array[$condition[$i * 2]] : '';
            $model = $this->get_model($table_name . '_model');
            $data_key = join('.', array_slice($condition, 0, 2 * ($i - 1) + 1));
            if(!isset($this->data[$data_key])){//不存在加载数据
                $this->get_data(explode('.', $data_key));
            }
            //是否为特殊条件
            if(array_key_exists($condition[$i * 2 - 1], $field_group)){
                $where = array();
                foreach ($field_group[$condition[$i * 2 - 1]] as $tjkey => $tjval) {
                    //由于 parameter6的加入,这里有可能会变成数组形式, 这里进行处理
                    $join_field = array();
                    if(is_string($tjval[3])) {
                        $join_field[] = $tjval[3];
                    }
                    if(is_array($tjval[3])) $join_field = $tjval[3];

                    //如果没有连接参数,这里不给数据
                    if(empty($join_field)) {
                        $where[] = "0 = 1";
                        continue;
                    }

                    //进行连接类型的判定
                    if ($join_field[0] == 'field'){
                        //biz_bill.id_no = biz_shipment.id
                        $where[] = $tjval[0] . ' ' . $tjval[1] . ' \'' .  $this->get_data_field($data_key, $tjval[2]) . '\'';
                    }else if ($join_field[0] == 'string'){
                        //biz_bill.id_type = 'shipment'
                        $where[] = $tjval[0] . ' ' . $tjval[1] . ' \'' . $tjval[2] . '\'';
                    }else if($join_field[0] == 'parameter'){
                        $tjval[2] = isset($this->parameter[$tjval[2]]) && !empty($this->parameter[$tjval[2]]) ? $this->parameter[$tjval[2]] : $this->get_data_field($data_key, $tjval[2]);

                        $where[] = $tjval[0] . ' ' . $tjval[1] . ' \'' .  $tjval[2] . '\'';
                    }else if($join_field[0] == 'parameter2'){//只获取传入参数的
                        $tjval[2] = isset($this->parameter[$tjval[2]]) ? $this->parameter[$tjval[2]] : '';

                        $where[] = $tjval[0] . ' ' . $tjval[1] . ' \'' .  $tjval[2] . '\'';
                    }else if($join_field[0] == 'parameter3'){//有传入参数取值,没有取
                        if(isset($this->parameter[$tjval[2]])){
                            $tjval[2] = $this->parameter[$tjval[2]];
                            //如果存在空值时,其他值不能一起显示

                            if($tjval[1] == 'in') $where[] = "{$tjval[0]} in ('" . join('\',\'', filter_unique_array(explode(',', $tjval[2]))) . "')" ;
                            else $where[] = $tjval[0] . ' ' . $tjval[1] . ' \'' .  $tjval[2] . '\'';
                        }
                    }else if($join_field[0] == 'parameter4'){//没有传入值就变为默认状态
                        if(!empty($this->parameter[$tjval[2]])){
                            $tjval[2] = $this->parameter[$tjval[2]];
                            $where[] = $tjval[0] . ' ' . $tjval[1] . ' \'' .  $tjval[2] . '\'';
                        }
                    }else if($join_field[0] == 'parameter5'){//没有传入值就变为默认状态
                        if(!empty($this->parameter[$tjval[2]])){
                            $tjval[2] = $this->parameter[$tjval[2]];
                            if($tjval[2] == '1'){
                                $tjval[2] = '\'0000-00-00\'';
                            }else{
                                $tjval[1] = '=';
                                $tjval[2] = "'{$tjval[2]}'";
                            }
                            $where[] = $tjval[0] . ' ' . $tjval[1] . ' ' .  $tjval[2] . '';
                        }
                    }else if($join_field[0] == 'parameter6'){
                        if(empty($join_field[1])) {
                            $where[] = "0 = 1";
                            continue;
                        }
                        //2022-09-20 由于导出时,账单需要根据当前是否有账单ID进行关联,这里又改为这个版本
                        //有参数则取参数的规则,没有不处理
                        if(!empty($this->parameter[$join_field[1]])){
                            continue;
                        }else{
                            //没参数走原本的field
                            $where[] = $tjval[0] . ' ' . $tjval[1] . ' \'' .  $this->get_data_field($data_key, $tjval[2]) . '\'';
                        }
                    }
                }
                $where = join(' and ', $where);
            }else{
                $_field = explode('-', $condition[$i * 2 - 1]);
                if(sizeof($_field) > 1){
                    $where = $_field[1] . ' = \'' . $this->get_data_field($data_key, $_field[0], 0, 1) . '\'';
                }else{
                    $where = $condition[$i * 2 - 1] . ' = \'' . $this->get_data_field($data_key, $condition[$i * 2 - 1], 0, 1) . '\'';
                }
            }

            if($i == (int)$count){//最后一次判断type
                if ($type == 'sum'){

                    $this->db->select($field);
                }else if ($type == 'count'){
                    $this->db->select($field);
                }else if ($type == 'one'){
                    $this->db->limit(1);
                }
                try{
                    if($table_name == 'biz_bill'){
                        $this->data[$data_name] = $model->no_role_get($where, 'id', 'desc', 0, '');
                    }else{
                        $this->data[$data_name] = $model->no_role_get($where);
                    }

                    $this->sql[$data_name] = lastquery();
                }catch (Exception $e){

                }

            }
        }

        return $this->data[$data_name];
    }

    /**
     * 获取json数组对应的描述
     * @param $condition
     * @param $field
     * @param int $num
     * @return bool
     */
    private function get_special_value($condition, $field, $num = 0){
        //键名为字段名, 数组第一个值为连接字符,后面可添加
        $special_field = array(
            'container_no_split','this_local_currency', 'base_amount', 'charge_en', 'ex_rate_dubai', 'net_amount', 'vat_amount', 'grand_total', 'vat_amount_sum', 'grand_total_sum', 'net_amount_sum', 'now1', 'dn_template_title', 'job_invoice', 'shipper_attached', 'consignee_attached', 'notify_attached', 'packs_sum_en', 'bn_sn_info', 'bn_sn_info_attached', 'issue_date', 'mark_nums_attached', 'description_attached',
            'INCO_term_text', 'box_info', 'charge_en', 'vn_container_no_seal_no', 'vn_packs_and_unit', 'vn_payment_amount', 'vn_payment_vnd_amount',
            'vn_payment_amount_sum', 'vn_payment_vnd_amount_sum', 'vn_ex_rate', 'vn_this_local_currency', 'abc', 'trans_ETA_sg_an', 'sg_container_no_seal_no', 'amount_sum_usd', 'sum', 'count', 'avg', 'local_amount_x_vat_THB', 'local_amount_THB', 'unit_price_text', 'amount_text', 'bn_sn_info1', 'th_vat_amount', 'tax_inclusive_amount', 'tax_inclusive_amount_sum',
            'th_dn_div_text', 'hold_without_tax', 'no', 'total_text','shipper_info',
            'consignee_info','notify_info', 'payment_amount_sum', 'vat_amount_sum1', 'vat_amount1', 'ETD_d_M_y', 'ETA_d_M_y', 'booking_ETD_d/m/y', 'dn_th_payment_amount_text', 'dn_th_vat_withhold_text', 'dn_th_vat_amount_text', 'dn_th_thb_total_text', 'vn_arrival_notice_total_text', 'company_client_code'
            , 'container_no_parameter', 'bill_avg_amount_text', 'bill_avg_num'
        );

        $condition_arr = explode('.', $condition);

        if(!in_array($field, $special_field)){
            return false;
        }else{
            if($field == 'container_no_parameter'){
                return isset($this->parameter['container_no']) ? $this->parameter['container_no'] : '';
            }
            //根据传入的 bill_avg_num 和 bill_avg_total 参数自动切割amount
            if($field == 'bill_avg_amount_text'){
                if(isset($this->parameter['bill_avg_num']) && isset($this->parameter['bill_avg_total'])){
                    //这个单价是自动根据cost_sell获取正负的
                    $unit_price_text = $this->get_data_field($condition, 'unit_price_text', $num, true, false);
                    $number = $this->get_data_field($condition, 'bill_avg_num', $num, true, false);
                    $val = round($unit_price_text * $number, 2);
                    $this->set_data_field($val, $condition, 'bill_avg_amount_text', $num);

                    return $val;
                }else{
                    return $this->get_data_field($condition, 'amount_text', $num, true, false);
                }
            }
            if($field == 'bill_avg_num'){
                if(isset($this->parameter['bill_avg_num']) && isset($this->parameter['bill_avg_total'])){
                    $number = $this->get_data_field($condition, 'number', $num, true, false);
                    $bill_avg_num = $this->parameter['bill_avg_num'];
                    $bill_avg_total = $this->parameter['bill_avg_total'];
                    $this_num_rate = $bill_avg_num / $bill_avg_total;
                    $val = round($number * $this_num_rate, 2);
                    $this->set_data_field($val, $condition, 'bill_avg_amount_text', $num);

                    return $val;
                }else{
                    return $this->get_data_field($condition, 'number', $num, true, false);
                }
            }
            if($field == 'company_client_code'){
                return isset($this->parameter['company_client_code']) ? $this->parameter['company_client_code'] : '';
            }
            if($field == 'dn_th_payment_amount_text'){
                if(end($condition_arr) == 'bill'){

                    $this_val = array();
                    $convert_currency = isset($this->parameter['convert_currency']) ? $this->parameter['convert_currency'] : 'THB';
                    $dn_th_vat_withhold_total = $this->get_data_field($condition, 'dn_th_vat_withhold_total', 0, true, false);

                    if($dn_th_vat_withhold_total === ''){
                        $this->get_special_value($condition, 'dn_th_vat_withhold_text', 0);
                        $dn_th_vat_withhold_total = $this->get_data_field($condition, 'dn_th_vat_withhold_total', 0, true, false);
                    }
                    $dn_th_invoice_amount = $this->get_data_field($condition, 'invoice_amount', 0, true, false);
                    if($dn_th_invoice_amount === ''){
                        $this->get_special_value($condition, 'dn_th_vat_amount_text', 0);
                        $dn_th_invoice_amount = $this->get_data_field($condition, 'invoice_amount', 0, true, false);
                    }
                    $payment_amount = round($dn_th_invoice_amount - $dn_th_vat_withhold_total, 2);
                    $this_val[] = "TOTAL PAYMENT:{$convert_currency} " . number_format($payment_amount, 2, '.', '');
                    $payment_amount_arr = explode('.', number_format($payment_amount, 2, '.', ''));
                    $payment_amount_word = "";
                    if(sizeof($payment_amount_arr) > 1){
                        $payment_amount_word .= number_to_word($payment_amount_arr[0]);
                        $payment_amount_word .= " AND " . number_to_word($payment_amount_arr[1]) . " CENT";
                    }else{
                        $payment_amount_word .= number_to_word($payment_amount);
                    }

                    $this_val[] = "AMOUNT IN WORD: {$convert_currency} " . $payment_amount_word;
                    return $this->replace_str(join("\r\n", $this_val), false);
                }
            }
            if($field == 'dn_th_vat_withhold_text'){
                if(end($condition_arr) == 'bill'){
                    $bill = $this->get_data($condition_arr);
                    $convert_currency = isset($this->parameter['convert_currency']) ? $this->parameter['convert_currency'] : 'THB';

                    $withould_amount = array();
                    foreach($bill as $key => $row){
                        $vat_withhold = str_replace('%', '', $row['vat_withhold']);
                        $amount = $this->get_special_value($condition, 'bill_avg_amount_text', $key, true, false);

                        if($row['currency'] === 'THB'){
                            $this_amount = $row['amount'];
                        }else{
                            $this_amount = amount_currency_change($amount, $row['bill_ETD'], $row['currency'], $row['type'], $convert_currency);
                        }
                        if($row['type'] == 'cost'){
                            $this_amount = -$this_amount;
                        }
                        if(!isset($withould_amount[$row['vat_withhold']])) $withould_amount[$row['vat_withhold']] = 0;
                        $withould_amount[$row['vat_withhold']] = round($withould_amount[$row['vat_withhold']] + ($this_amount * $vat_withhold / 100), 2);
                    }
                    $this_val = array();
                    $left_len = strlen("WITHOLDING TAX AAAAAA   ");
                    foreach ($withould_amount as $key => $val){
                        $left_str = str_pad("WITHOLDING TAX {$key}", $left_len, ' ', STR_PAD_RIGHT);
                        $this_val[] = "{$left_str}:{$convert_currency}" . number_format($val, 2, '.', '');
                    }
                    if(!empty($bill)){
                        $this->set_data_field(round(array_sum($withould_amount), 2), $condition, 'dn_th_vat_withhold_total', 0);
                    }
                    return $this->replace_str(join("\r\n", $this_val), false);
                }
            }
            if($field == 'dn_th_vat_amount_text'){
                if(end($condition_arr) == 'bill'){
                    $this_val = array();

                    $bill = $this->get_data($condition_arr);
                    $non_vat_amount_total = 0;//无税金额
                    $vat_amount_total = 0;//有税率的金额总和
                    $vat_amount = array();//税额
                    $convert_currency = isset($this->parameter['convert_currency']) ? $this->parameter['convert_currency'] : 'THB';
                    foreach ($bill as $key => $row){
                        //限制只有THB才触发, 其他的后面需要再说
                        $vat = str_replace('%', '', $row['vat']);
                        $amount = $this->get_special_value($condition, 'bill_avg_amount_text', $key, true, false);
                        if($row['currency'] === $convert_currency){
                            $this_amount = $amount;
                        }else{
                            $this_amount = amount_currency_change($amount, $row['bill_ETD'], $row['currency'], $row['type'], $convert_currency);
                        }
                        if($row['type'] == 'cost'){
                            $this_amount = -$this_amount;
                        }
                        if($row['vat'] == '0%'){
                            $non_vat_amount_total = round($non_vat_amount_total + $this_amount, 2);
                        }else{
                            $vat_amount_total = round($vat_amount_total + $this_amount, 2);

                            if(!isset($vat_amount[$row['vat']])) $vat_amount[$row['vat']] = 0;
                            $vat_amount[$row['vat']] = round($vat_amount[$row['vat']] + ($this_amount * $vat / 100), 2);
                        }
                    }
                    $left_len = strlen("Total Amount Non VAT     ");
                    $left_str = str_pad("TOTAL AMOUNT NON VAT", $left_len, ' ', STR_PAD_RIGHT);
                    $this_val[] = "{$left_str}:{$convert_currency} " . number_format($non_vat_amount_total, 2, '.', '');
                    $left_str = str_pad("TOTAL AMOUNT VAT", $left_len, ' ', STR_PAD_RIGHT);
                    $this_val[] = "{$left_str}:{$convert_currency} " . number_format($vat_amount_total, 2, '.', '');
                    foreach ($vat_amount as $key => $val){
                        $left_str = str_pad("VAT AMOUNT {$key}", $left_len, ' ', STR_PAD_RIGHT);

                        $this_val[] = "{$left_str}: {$convert_currency} " . number_format($val, 2, '.', '');
                    }
                    $invoice_amount = round($non_vat_amount_total + $vat_amount_total + array_sum($vat_amount), 2);
                    if(!empty($bill)){
                        $this->set_data_field($invoice_amount, $condition, 'invoice_amount', 0);
                    }
                    $left_str = str_pad("TOTAL INVOICE", $left_len, ' ', STR_PAD_RIGHT);
                    $this_val[] = "{$left_str}: {$convert_currency} " . number_format($invoice_amount, 2, '.', '');
                    return $this->replace_str(join("\r\n", $this_val), false);
                }
            }
            //2024-03-11 thb的 DN total描述
            if($field == 'dn_th_thb_total_text'){
                if(end($condition_arr) == 'bill'){
                    $this_val = array();
                    $bill = $this->get_data($condition_arr);
                    $convert_currency = isset($this->parameter['convert_currency']) ? $this->parameter['convert_currency'] : 'THB';
                    if(!empty($bill)){
                        $this_currency = $bill[0]['currency'];
                        $ex_rate = round(get_ex_rate($bill[0]['bill_ETD'], $this_currency, 'invoice', $convert_currency), 2);
                        $usd_total = 0;
                        foreach ($bill as $key => $row){
                            //只有USD时候才统计, 且实时获取当前的汇率转换
                            $this_amount = $this->get_special_value($condition, 'bill_avg_amount_text', $key, true, false);
                            if($row['currency'] == $this_currency){

                                if($row['type'] == 'cost'){
                                    $this_amount = -$this_amount;
                                }
                                $usd_total = round($usd_total + $this_amount, 2);
                            }
                        }
                        $left_len = strlen("EXCHANGE RATE ");
                        $thb_usd_total = round($usd_total * $ex_rate, 2);
                        $left_str = str_pad("TOTAL {$this_currency}", $left_len, ' ', STR_PAD_RIGHT);
                        $this_val[] = "{$left_str}: " . number_format($usd_total, 2, '.', '');
                        $left_str = str_pad("EXCHANGE RATE", $left_len, ' ', STR_PAD_RIGHT);
                        $this_val[] = "{$left_str}: " . number_format($ex_rate, 2, '.', '') . "       ";
                        if($this_currency != $convert_currency){
                            $left_str = str_pad("convert {$this_currency} to {$convert_currency}", $left_len, ' ', STR_PAD_RIGHT);
                            $this_val[] = "{$left_str}:" . number_format($thb_usd_total, 2, '.', '');
                        }
                    }
                    return $this->replace_str(join("\r\n", $this_val), false);
                }

            }
            if($field == 'booking_ETD_d/m/y'){
                $date = $this->get_data_field($condition, 'booking_ETD', $num);
                return date('d/m/y', strtotime($date));
            }
            if($field == 'ETA_d_M_y'){
                $date = $this->get_data_field($condition, 'trans_ETA', $num);
                return date('d M y', strtotime($date));
            }
            if($field == 'ETD_d_M_y'){
                $date = $this->get_data_field($condition, 'booking_ETD', $num);
                return date('d M y', strtotime($date));
            }
            //2024-03-27
            if($field == 'vn_arrival_notice_total_text'){
                //费用申领单 total文案 有几个币种显示几个币种
                //CNY: 11111.00           USD: 1222222
                //这里这时候只有cost所以直接+了
                $this_data = $this->get_data($condition_arr);
                $sum_arr = array();

                foreach ($this_data as $row){
                    //由于这里一般管理条件前就限制了cost或者显示 所以不用将amount 在cost 时 转为负数了
                    $this_amount = $row['amount'];
                    $this_currency = $row['currency'];
                    $this_vat = $row['vat'];

                    //使用币种和税率来作为分组
                    $this_key = "{$this_currency}_{$this_vat}";

                    if(!isset($sum_arr[$this_key])) $sum_arr[$this_key] = array('vat' => $this_vat, 'currency' => $this_currency, 'amount' => 0);

                    $sum_arr[$this_key]['amount'] = round($sum_arr[$this_key]['amount'] + $this_amount, 2);
                }
                //现在没有排序的, 后续他们可能会想要USD的放一起?

                $text = array();
                $total_amount = 0;
                $ex_rate = isset($this->parameter['vn_ex_rate']) ? $this->parameter['vn_ex_rate'] : '0.000';
                foreach ($sum_arr as $key => $row){
                    $this_amount = $row['amount'];
                    $this_amount_text = number_format($this_amount, 2, '.', ',');
                    $this_currency = $row['currency'];
                    $this_vat = $row['vat'];
                    $this_vat_amount = round($this_amount * (int)$this_vat / 100, 2);
                    $this_vat_amount_text = number_format($this_vat_amount, 2, '.', ',');
                    $text[] = "{$this_currency}: {$this_amount_text}          VAT:{$this_vat}      VAT AMOUNT ({$this_currency}): {$this_vat_amount_text}";

                    //结算金额等于 总amount + vat_amount 然后全部折算成VND
                    if($this_currency == 'VND'){
                        $total_amount += $this_amount + $this_vat_amount;
                    }else{
                        //如果是其他币种, 那么折算成VND
                        //这里VND都是自己输入汇率的, 所以基本只有俩币种, 两个以上也没法处理了, 先这样
                        $total_amount += round($this_amount * $ex_rate, 2) + round($this_vat_amount * $ex_rate, 2);
                    }
                }
                //Exchange  Rate:  24950.0000
                $text[] = "Exchange  Rate:  {$ex_rate}";

                $total_amount_text = number_format($total_amount, 2, '.', ',');
                $text[] = "Total in VND (incl VAT) : {$total_amount_text}";
                return $this->replace_str(join("\r\n", $text), false);
            }
            if($field == 'total_text'){
                //费用申领单 total文案 有几个币种显示几个币种
                //CNY: 11111.00           USD: 1222222
                //这里这时候只有cost所以直接+了
                $this_data = $this->get_data($condition_arr);
                $sum_arr = array();

                foreach ($this_data as $row){
                    $this_amount = $row['amount'];
                    $this_currency = $row['currency'];
                    // if($row['type'] == 'cost'){
                    //     $this_amount = -$this_amount;
                    // }
                    if(!isset($sum_arr[$this_currency])) $sum_arr[$this_currency] = 0;

                    //开始加金额
                    $sum_arr[$this_currency] += $this_amount;
                }
                $text = "";
                foreach ($sum_arr as $key => $row){
                    $text .= "{$key}: {$row}       ";
                }
                return $text;
            }
            if($field == 'shipper_info'){
                //获取可查看字段
                $show_fields = $this->get_data_field($condition, 'show_fields', $num);
                $return = array();
                $return[] = $this->get_data_field($condition, 'shipper_company', $num);
                $return[] = $this->get_data_field($condition, 'shipper_address', $num);
                if(in_array('SHIPPER_CONTACT', explode(',', $show_fields)))$return[] = $this->get_data_field($condition, 'shipper_contact', $num);
                if(in_array('SHIPPER_TELEPHONE', explode(',', $show_fields)))$return[] = $this->get_data_field($condition, 'shipper_telephone', $num);
                if(in_array('SHIPPER_EMAIL', explode(',', $show_fields)))$return[] = $this->get_data_field($condition, 'shipper_email', $num);
                return join('<w:br />', $return);
            }
            if($field == 'consignee_info'){
                //获取可查看字段
                $show_fields = $this->get_data_field($condition, 'show_fields', $num);
                $return = array();
                $show_fields_array = explode(',', $show_fields);
                $return[] = $this->get_data_field($condition, 'consignee_company', $num);
                $return[] = $this->get_data_field($condition, 'consignee_address', $num);
                if(in_array('CONSIGNEE_CONTACT', $show_fields_array))$return[] = $this->get_data_field($condition, 'consignee_contact', $num);
                if(in_array('CONSIGNEE_TELEPHONE', $show_fields_array))$return[] = $this->get_data_field($condition, 'consignee_telephone', $num);
                if(in_array('CONSIGNEE_EMAIL', $show_fields_array))$return[] = $this->get_data_field($condition, 'consignee_email', $num);
                return join('<w:br />', $return);
            }
            if($field == 'notify_info'){
                //获取可查看字段
                $show_fields = $this->get_data_field($condition, 'show_fields', $num);
                $return = array();
                $return[] = $this->get_data_field($condition, 'notify_company', $num);
                $return[] = $this->get_data_field($condition, 'notify_address', $num);
                if(in_array('NOTIFY_CONTACT', explode(',', $show_fields)))$return[] = $this->get_data_field($condition, 'notify_contact', $num);
                if(in_array('NOTIFY_TELEPHONE', explode(',', $show_fields)))$return[] = $this->get_data_field($condition, 'notify_telephone', $num);
                if(in_array('NOTIFY_EMAIL', explode(',', $show_fields)))$return[] = $this->get_data_field($condition, 'notify_email', $num);
                return join('<w:br />', $return);
            }
            if($field == 'no'){
                return $num + 1;
            }
            if($field == 'th_dn_div_text'){
                $currency = $this->get_data_field($condition, 'currency', $num);
                // $amount = $this->get_data_field($condition . ".amount", 'sum', $num, true, false);
                $vat = $this->get_data_field($condition, 'vat', $num);
                // $vat_amount = round($amount * ((int)$vat / 100), 2);
                //免税3%的金额
                // $hold_without_tax = $this->get_data_field($condition, 'hold_without_tax', $num, true, false);
                // $hold_without_tax_amount = round($amount * ((int)$hold_without_tax / 100), 2);
                //越南收税的金额
                // $th_vat_amount = $this->get_data_field($condition . ".th_vat_amount", 'sum', $num, true, false);
                // $this->set_data_field($th_vat_amount - $hold_without_tax_amount, $condition, 'th_dn_div_amount', 0);

                $bill_data = $this->get_data($condition_arr);
                $bill_amount = array('3%' => 0, '1%' => 0);//获取当前的免税额金额汇总
                $vat_amount_data = array();
                $amount_sum = 0;
                foreach($bill_data as $row){
                    $amount = $row['amount'];
                    if($row['type'] == 'cost'){
                        $amount = -$amount;
                    }
                    $this_vat = $row['vat'];
                    // $thb_amount = amount_currency_change($amount, $row['bill_ETD'], $row['currency'], $row['type'], 'THB');
                    $thb_amount = $amount;
                    $amount_sum += $thb_amount;
                    !isset($bill_amount[$row['vat_withhold']]) && $bill_amount[$row['vat_withhold']] = 0;
                    $bill_amount[$row['vat_withhold']] += $thb_amount * (int)$row['vat_withhold'] / 100;
                    // if($this_vat === '7%'){
                    //     //7%时, 免税额是 3%
                    //     !isset($bill_amount['3%']) && $bill_amount['3%'] = 0;
                    //     $bill_amount['3%'] += $thb_amount * 0.03;
                    // }
                    // //0%时, 免税额是1%
                    // if($this_vat === '0%'){
                    //     !isset($bill_amount['1%']) && $bill_amount['1%'] = 0;
                    //     $bill_amount['1%'] += $thb_amount * 0.01;
                    // }
                    $vat_amount_data[$row['vat']] += $thb_amount * (int)$row['vat'] / 100;
                }


                $text = array();
                // if($vat !== '0%'){

                // }
                $vat_amount = 0;
                if($currency == 'THB'){
                    if(!isset($vat_amount_data['0%'])) $vat_amount_data['0%'] = 0;
                    foreach ($vat_amount_data as $key => $val){
                        $vat_amount += $val;
                        $text[] = "VAT Amount {$key}: {$currency} " . number_format($val, 2, '.', '');
                    }
                    // if(isset($bill_amount['3%'])){
                    //     $vat_amount = $bill_amount['3%'] / 0.03 * 0.07;
                    //     $text[] = "VAT Amount 7%: {$currency} " . number_format($vat_amount, 2, '.', '');
                    // }
                    // if(isset($bill_amount['1%'])){
                    //     $text[] = "VAT Amount 0%: {$currency} " . number_format(0, 2, '.', '');
                    // }
                }

                //invoice 金额等于 amount总额 + 税额
                $invoice_amount = $amount_sum + $vat_amount;
                $text[] = "Total Invoice: " . number_format($invoice_amount, 2, '.', '');
                $text[] = "";
                if($currency == 'THB'){
                    foreach($bill_amount as $key => $val){
                        $text[] = "WITHOLDING TAX {$key}:{$currency} " . number_format($val, 2, '.', '');
                    }
                }
                if(end($text) !== ''){
                    $text[] = "";
                }
                if($currency == 'USD'){
                    $bill_amount = array();
                }
                //payment金额等于 invoice总额 - 免税额
                $payment_amount = $invoice_amount - array_sum($bill_amount);
                $text[] = "Total Payment: " . number_format($payment_amount, 2, '.', '');
                // if($hold_without_tax !== '0%'){
                // $text[] = "Hold Without Tax {$hold_without_tax}:{$currency} " . number_format($hold_without_tax_amount, 2, '.', '');
                // }
                return $this->replace_str(join("\r\n", $text));
            }
            if($field == 'hold_without_tax'){
                if(isset($this->parameter['hold_without_tax']) && !empty($this->parameter['hold_without_tax'])){
                    return $this->parameter['hold_without_tax'];
                }else{
                    return false;
                }
            }
            if($field == 'description_attached'){
                //获取前手动获取下shipper那几个,
                $this->get_data_field($condition, 'shipper_attached', $num, 1);
                $this->get_data_field($condition, 'consignee_attached', $num, 1);
                $this->get_data_field($condition, 'notify_attached', $num, 1);

                $description = $this->get_data_field($condition, 'description', $num);
                $description = explode('<w:br />', $description);
                $new_description = array();
                foreach ($description as $val){
                    //每行超过15字节,换行
                    //按照单词来分
                    //首先根据空格全切
                    $val_arr = explode(' ', trim($val));
                    $this_str = array();
                    foreach ($val_arr as $val2){
                        //当拼接后超过40字节,直接换行
                        if(strlen(join(' ', $this_str)) + strlen($val2) <= 40){
                            $this_str[] = $val2;
                        }else{
                            $new_description[] = join(' ', $this_str);
                            $this_str = array();
                            $this_str[] = $val2;
                        }
                    }
                    $new_description[] = join(' ', $this_str);
                }
                if(sizeof($new_description) > 10){
                    $new_description = join('<w:br />', $new_description);
                    $description = 'SEE ATTACHED';

                    $this->attached['description'] = array('data' => $new_description, 'order' => 3);
                }else{
                    $description = join('<w:br />', $description);
                }
                return $description;
            }
            if($field == 'th_vat_amount'){
                //泰国的 获取当前汇率金额
                $this_amount = $this->get_data_field($condition, 'amount', $num, 0, false);//当前金额
                $vat = $this->get_data_field($condition, 'vat', $num, 0, false);//当前税率, 这个会在外面限制住,
                $str = round($this_amount * ((int)$vat / 100), 2);
                $this->set_data_field($str, $condition, $field, $num);
                return number_format($str, 2, '.', '');
            }
            if($field == 'tax_inclusive_amount'){
                //泰国的 获取当前汇率金额
                $this_amount = $this->get_data_field($condition, 'amount', $num, 0, false);//当前金额
                $vat = $this->get_data_field($condition, 'vat', $num, 0, false);//当前税率, 这个会在外面限制住,
                $str = round($this_amount * (1 + (int)$vat / 100), 2);
                $this->set_data_field($str, $condition, $field, $num);
                return number_format($str, 2, '.', '');
            }
            if($field == 'tax_inclusive_amount_sum'){
                $this_amount_sum = $this->get_data_field($condition . '.amount', 'sum', $num, true, false);//当前金额
                $this->get_data_field($condition, 'th_dn_div_text', $num, true, false);
                $th_dn_div_amount = $this->get_data_field($condition, 'th_dn_div_amount', 0, 0, false);//当前金额
                $str = round($this_amount_sum + $th_dn_div_amount, 2);

                return number_format($str, 2, '.', '');
            }
            if(in_array($field, array('local_amount_x_vat_THB', 'local_amount_THB'))){
                //将金额转换成THB
                $field_arr = explode('_', $field);
                $currency = end($field_arr);//即将转换为的汇率
                $this_amount = $this->get_data_field($condition, 'amount', $num, 0, false);//当前金额
                $this_currency = $this->get_data_field($condition, 'currency', $num, 0, false);//当前汇率
                $bill_ETD = $this->get_data_field($condition, 'bill_ETD', $num, 0, false);//当前汇率
                $type = $this->get_data_field($condition, 'type', $num, 0, false);//当前汇率
                $vat = $this->get_data_field($condition, 'vat', $num, 0, false);//当前税率, 这个会在外面限制住, 保证当前就一种汇率
                if(in_array($field, array('local_amount_x_vat_THB'))){
                    return round(amount_currency_change($this_amount, $bill_ETD, $this_currency, $type, $currency) * (1 + (int)$vat / 100), 2);
                }else{
                    //
                    return amount_currency_change($this_amount, $bill_ETD, $this_currency, $type, $currency);
                }

            }
            if($field == 'unit_price_text' || $field == 'amount_text'){
                //这里是DN那里用到的, cost 转为负数金额
                $field_cofnig = array(
                    'unit_price_text' => 'unit_price',
                    'amount_text' => 'amount',
                );

                //如果是账单那里,就正常获取, 否则返回false
                if(end($condition_arr) == 'bill'){
                    $type = $this->get_data_field($condition, 'type', $num, false, false);
                    $this_val = $this->get_data_field($condition, $field_cofnig[$field], $num, false, false);
                    if($type == 'cost'){
                        $this_val = -$this_val;
                    }
                    return $this_val;

                }else{
                    return false;
                }


            }
            //2023-08-04 总和平均值等, 放到这里来了
            if(in_array($field, array('sum', 'count', 'avg'))){
                //这里的sum等, 是多出来的
                $this_field = array_pop($condition_arr);
                $condition = join('.', $condition_arr);
                //获取当前的数据
                $this_data = $this->get_data($condition_arr);
                //常规的直接获取总和就行了
                $this_val = 0;

                foreach ($this_data as $key => $row){
                    if(!isset($row[$this_field])) {
                        $row[$this_field] = $this->get_data_field($condition, $this_field, $key, 1, false);
                    }
                    if(end($condition_arr) == 'bill'){
                        //如果是bill的, type = cost 时, 金额要变成负数
                        //这里没必要限制字段了, 默认自己配的时候就用的金额字段
                        if($row['type'] == 'cost'){
                            $row[$this_field] = -$row[$this_field];
                        }
                    }

                    //

                    if($field == 'sum'){
                        $this_val += $row[$this_field];
                    }else if($field == 'count'){
                        //数量
                        $this_val += 1;
                    }else if($field == 'avg'){
                        //平均值 这里为了 方便 就写成当前的 除以总数吧
                        $this_val += $row[$this_field] / sizeof($this_data);
                    }
                }
                //先暂时这样,后面有其他的再格式化成别的样子
                return number_format($this_val, 2, '.', '');
            }
            if($field == 'amount_sum_usd'){
                //泰国的 自动将金额汇总为usd
                $bill_data = $this->get_data($condition_arr);
                $this_val = 0;
                foreach ($bill_data as $row){
                    $this_val += amount_currency_change($row['amount'], $row['bill_ETD'], $row['currency'], $row['type'], 'USD');
                }
                return $this_val;
            }
            if($field == 'bn_sn_info1'){
                if ($condition_arr[0] == 'shipment'){
                    $return = array();
                    //container_no seal_no container_size packs weight volume
                    $container_data = $this->get_data($condition_arr);

                    $condition_ct = $condition . '.container_no-container_no.container';
                    $condition_ct_arr = explode('.', $condition_ct);
                    foreach ($container_data as $key => $row){
                        $this_row = array();
                        $this_row[0] = $this->get_data_field($condition, 'container_no', $key);
                        $this->get_data($condition_ct_arr);
                        $this_row[1] = $this->get_data_field($condition_ct, 'container_size');
                        ksort($this_row);
                        $return[] = join('/', $this_row);
                    }
                    return join(';', $return);
                }else{
                    return '';
                }
            }
            //新加坡的,
            if($field == 'sg_container_no_seal_no'){
                //UETU2575412/7484832/20GP
                $container_no = $this->get_data_field($condition, "container_no", $num);
                $seal_no = $this->get_data_field($condition, "seal_no", $num);

                if(empty($this_container)) $this_container = array('container_size' => '', 'seal_no' => '');

                return "{$container_no} / {$this_container['seal_no']}";
            }
            //新加坡的 AN模板用
            if($field == 'trans_ETA_sg_an'){
                $value = $this->get_data_field($condition_arr, 'trans_ETA', $num);
                if($value == '0000-00-00 00:00:00') return '';

                return date('d.m.Y', strtotime($value));
            }
            if($field == 'vn_this_local_currency'){
                //当前本地币种
                $this_local_currency = isset($this->parameter['this_local_currency']) ? $this->parameter['this_local_currency'] : 'VND';
                $this->set_data_field($this_local_currency, $condition, $field, $num);
                return $this_local_currency;
            }

            if($field == 'vn_ex_rate' || $field == 'abc'){
                if(isset($this->parameter['vn_ex_rate']) && !empty($this->parameter['vn_ex_rate'])){
                    $ex_rate = $this->parameter['vn_ex_rate'];
                }else{
                    //获取美元转人民币的汇率
                    if($condition_arr[0] == 'shipment'){
                        $id_type = 'shipment';
                    }else{
                        $id_type = 'consol';
                    }
                    $id_no = $this->get_data_field($condition_arr[0], 'id', 0);
                    $currency = $this->get_data_field($condition, 'vn_this_local_currency', 0, 1);
                    $type = 'sell';//因为这个模板是单独的, 所以随便取一个了
                    $this_currecy = 'USD';//折算为该币种

                    //获取日期
                    $bill_ETD = get_bill_ETD($id_type, $id_no);
                    //获取当前汇率
                    $ex_rate = round(get_ex_rate($bill_ETD, $this_currecy, strtolower($type), $currency), 4);
                }
                $this->set_data_field($ex_rate, $condition, 'vn_ex_rate', $num);
                $this->set_data_field($ex_rate, $condition, $field, $num);
                return $ex_rate;
            }

            if($field == 'vn_payment_vnd_amount'){
                //金额折算成越南币的
                $ex_rate = $this->get_data_field($condition_arr[0], 'vn_ex_rate', 0, 1);
                $amount = $this->get_data_field($condition, 'amount', $num);

                //获取折算金额
                $this_amount = round(strval($amount * 100) * strval($ex_rate * 1000000) / 100000000, 2);
                $this->set_data_field($this_amount, $condition, $field, $num);

                return $this_amount;
            }

            if($field == 'vn_payment_vnd_amount_sum'){
                //求vn_payment_amount之和
                $data = $this->get_data($condition_arr);

                $sum = array_sum(array_column($data, 'vn_payment_vnd_amount'));

                return $sum;
            }

            if($field == 'vn_payment_amount'){
                //加上税率的金额
                $vat = $this->get_data_field($condition, "vat", $num);//税率
                $amount = $this->get_data_field($condition, "amount", $num);//当前总金额

                $vat_num = (int)$vat / 100;
                //税额
                $vat_amount = round($amount * (1 + $vat_num), 2);

                $this->set_data_field($vat_amount, $condition, $field, $num);

                return number_format($vat_amount, 2, '.', '');
            }

            if($field == 'vn_payment_amount_sum'){
                //求vn_payment_amount之和
                $data = $this->get_data($condition_arr);

                $sum = array_sum(array_column($data, 'vn_payment_amount'));

                return number_format($sum, 2, '.', '');
            }

            if($field == 'payment_amount_sum'){
                //求vn_payment_amount之和
                $data = $this->get_data($condition_arr);

                $sum = 0;
                //先按单一币种做
                foreach ($data as $row){
                    $vat = $row['vat'];
                    $amount = $row['amount'];

                    $vat_num = (int)$vat / 100;
                    //税额
                    $vat_amount = round($amount * (1 + $vat_num), 2);

                    $sum += $vat_amount;
                }

                return number_format($sum, 2, '.', '');
            }
            if($field == 'vat_amount_sum1'){
                //求vn_payment_amount之和
                $data = $this->get_data($condition_arr);

                $payment_sum = 0;
                $amount_sum = 0;
                //先按单一币种做
                foreach ($data as $row){
                    $vat = $row['vat'];
                    $amount = $row['amount'];

                    $vat_num = (int)$vat / 100;
                    //税额
                    $vat_amount = round($amount * (1 + $vat_num), 2);

                    $payment_sum += $vat_amount;
                    $amount_sum += $amount;
                }
                $sum = round($payment_sum - $amount_sum, 2);

                return number_format($sum, 2, '.', '');
            }
            if($field == 'vat_amount1'){
                //求vn_payment_amount
                $data = $this->get_data($condition_arr);
                $vat = $this->get_data_field($condition, "vat", $num);
                $amount = $this->get_data_field($condition, "amount", $num);

                $payment_sum = 0;
                $amount_sum = 0;
                //先按单一币种做

                $vat_num = (int)$vat / 100;
                //税额
                $vat_amount = round($amount * (1 + $vat_num), 2);

                $payment_sum += $vat_amount;
                $amount_sum += $amount;
                $sum = round($payment_sum - $amount_sum, 2);

                return number_format($sum, 2, '.', '');
            }

            if($field == 'vn_packs_and_unit'){
                $packs = $this->get_data_field($condition, "packs", $num);

                //因为这里现在只有consol和shipment 直接查询good_outers_unit
                $good_outers_unit = $this->get_data_field($condition_arr[0], 'good_outers_unit', 0);

                return "{$packs} {$good_outers_unit}";
            }

            if($field == 'vn_container_no_seal_no'){
                //UETU2575412/7484832/20GP
                $container_no = $this->get_data_field($condition, "container_no", $num);
                $seal_no = $this->get_data_field($condition, "seal_no", $num);
                $consol_id = $this->get_data_field($condition, "consol_id", $num);

                //获取container的箱型
                $this->db->select('container_size,seal_no');
                $this_container = Model('biz_container_model')->get_where_one("consol_id = '$consol_id' and container_no = '" . $container_no . "'");
                if(empty($this_container)) $this_container = array('container_size' => '', 'seal_no' => '');

                return "{$container_no}/{$this_container['seal_no']}/{$this_container['container_size']}";
            }

            if($field == 'charge_en'){
                $this_charge = $this->get_data_field($condition, 'charge_code', $num);
                $this->load->model('biz_charge_model');
                $charge = $this->biz_charge_model->get_one('charge_code', $this_charge);
                return $charge['charge_name'];
            }
            if($field == 'shipper_attached' || $field == 'consignee_attached' || $field == 'notify_attached'){
                if(isset($this->data[$condition][$num][$field])){
                    return $this->data[$condition][$num][$field];
                }
                $result = array();

                //0 是 字段前缀名, 1 是超出长度就切割, 2是作为att的前缀, 3 是att的排序
                $config_arr = array(
                    'shipper_attached' => array('shipper', 5, '*', 7),
                    'consignee_attached' => array('consignee', 7, '**', 9),
                    'notify_attached' => array('notify', 5, '***', 11),
                );
                $this_config = $config_arr[$field];
                $field_prefix = $this_config[0];

                //company
                $company = $this->get_data_field($condition, "{$field_prefix}_company", $num, 0 , false);
                $result[] = $company;

                //address
                $address = $this->get_data_field($condition, "{$field_prefix}_address", $num, 0, false);
                foreach (explode("\r\n", $address) as $val){
                    $result[] = $val;
                }

                //info
                $info = $this->get_data_field($condition, "{$field_prefix}_info", $num, 1, false);
                foreach (explode("<w:br />", $info) as $val){
                    $result[] = $val;
                }

                //这里对数据进行切割,一行当他50个字
                // $result = cut_array_length($result, 50, 99);

                //切割完成后进行 替换特殊字符
                $result = explode("<w:br />", $this->replace_str(join("\r\n", $result)));
                $result = array_filter($result);

                //如果超过 5行,切割到 attached里
                if(sizeof($result) > $this_config[1]) {
                    $attached = array_slice($result, $this_config[1]);
                    $result = array_slice($result, 0, $this_config[1]);

                    $result[sizeof($result) - 1] .= $this_config[2];//shipper等内容结尾拼上*
                    $attached[0] = $this_config[2] . $attached[0];//attach 内容前排拼上*

                    $description = $this->get_data_field($condition, "description", $num, 0, false);
                    //多余的贴到品名那里取
                    $this->set_data_field($description . "\r\n" . join("\r\n", $attached), $condition, 'description', $num);
//                    $this->attached[$this_config[0]] = array('data' => join('<w:br />', $attached), 'order' => $this_config[3]);
                    $this->set_data_field(join("<w:br />", $result), $condition, $field, $num);
                }


                return join('<w:br />', $result);
            }
            if($field == 'bn_sn_info' || $field == 'bn_sn_info_attached'){//箱号封号等信息
                $result = '';
                if ($condition_arr[0] == 'shipment'){
                    $return = array();
                    //container_no seal_no container_size packs weight volume
                    $container_data = $this->get_data($condition_arr);

                    $condition_ct = $condition . '.container_no-container_no.container';
                    $condition_ct_arr = explode('.', $condition_ct);
                    $this->load->model('biz_container_model');
                    $this->load->model('biz_shipment_model');
                    foreach ($container_data as $key => $row){
                        $this_row = array();
                        $this_row[0] = $this->get_data_field($condition, 'container_no', $key);
                        $this_row[3] = $this->get_data_field($condition, 'packs', $key) . $this->get_data_field($this->main_table, 'good_outers_unit', 0);
                        $this_row[4] = $this->get_data_field($condition, 'weight', $key) . $this->get_data_field($this->main_table, 'good_weight_unit', 0);
                        $this_row[5] = $this->get_data_field($condition, 'volume', $key) . $this->get_data_field($this->main_table, 'good_volume_unit', 0);

                        // $this->get_data($condition_ct_arr);
                        $shipment_id = $this->get_data_field($condition, 'shipment_id', $key);
                        $consol_id = $this->biz_shipment_model->get_one_all('id', $shipment_id)['consol_id'];
                        $this_container = $this->biz_container_model->get_where_one("consol_id = '$consol_id' and container_no = '" . $this_row[0] . "'");
                        $this_row[1] = isset($this_container['seal_no']) ? $this_container['seal_no'] : '';
                        $this_row[2] = isset($this_container['container_size']) ? $this_container['container_size'] : '';
                        ksort($this_row);
                        $return[] = join('/', $this_row);
                    }
                    $result = join('<w:br />', $return);
                }else if($condition_arr[0] == 'consol'){
                    $return = array();
                    //container_no seal_no container_size packs weight volume
                    $container_data = $this->get_data($condition_arr);

//                    $condition_ct = $condition;
//                    $condition_ct_arr = explode('.', $condition_ct);
                    $this->get_data(explode('.', 'consol.id-consol_id.shipment'));
                    foreach ($container_data as $key => $row){
                        $this_row = array();
                        $this_row[0] = $this->get_data_field($condition, 'container_no', $key);
                        $this_row[3] = $this->get_data_field($condition, 'packs', $key) . $this->get_data_field('consol', 'good_outers_unit', 0);
                        $this_row[4] = $this->get_data_field($condition, 'weight', $key) . $this->get_data_field('consol', 'good_weight_unit', 0);
                        $this_row[5] = $this->get_data_field($condition, 'volume', $key) . $this->get_data_field('consol', 'good_volume_unit', 0);
                        $consol_id = $this->get_data_field($condition, 'consol_id', $key);
                        $this_container = $this->biz_container_model->get_where_one("consol_id = '$consol_id' and container_no = '" . $this_row[0] . "'");
                        $this_row[1] = $this_container['seal_no'];
                        $this_row[2] = $this_container['container_size'];
                        ksort($this_row);
                        $return[] = join('/', $this_row);
                    }
                    $result = join('<w:br />', $return);
                }

                return $result;
            }
            if($field == 'issue_date'){
                $result = $this->get_data_field($condition, $field, $num);
                if(empty($result) || $result == '0000-00-00'){
                    $condition = 'shipment.consol_id-id.consol';
                    $this->get_data(explode('.', $condition));
                    $result = $this->get_data_field($condition, 'trans_ATD', $num);
                    if(!empty($result) && $result !== '0000-00-00 00:00:00') $result = date("Y-m-d",strtotime($result));
                }
                return $result;
            }

            if($field == 'mark_nums_attached'){
                $mark_nums = $this->get_data_field($condition, 'mark_nums', $num);
                $mark_nums = explode('<w:br />', $mark_nums);
                $new_mark_nums = array();

                foreach ($mark_nums as $val){
                    //每行超过20字节,换行
                    //按照单词来分
                    //首先根据空格全切
                    $val_arr = explode(' ', trim($val));
                    $this_str = array();
                    foreach ($val_arr as $val2){
                        //当拼接后超过20字节,直接换行
                        if(strlen(join(' ', $this_str)) + strlen($val2) <= 20){
                            $this_str[] = $val2;
                        }else{
                            $new_mark_nums[] = join(' ', $this_str);
                            $this_str = array();
                            $this_str[] = $val2;
                        }
                    }
                    $new_mark_nums[] = join(' ', $this_str);
                }

                if(sizeof($new_mark_nums) > 5){

                    $new_mark_nums = join('<w:br />', $new_mark_nums);
                    $mark_nums = 'SEE ATTACHED';

                    $this->attached['mark_nums'] = array('data' => $new_mark_nums, 'order' => 5);
                }else{
                    $mark_nums = join('<w:br />', $mark_nums);
                }
                return $mark_nums;
            }

            if($field == 'INCO_term_text'){
                //除了FOB 显示FREIGHT COLLECT 其他都显示FREIGHT PREPAID
                $return = '';
                if($condition_arr[0] == 'shipment'){
                    $INCO_term = $this->get_data_field($condition, 'INCO_term', $num);
                    if($INCO_term == 'FOB'){
                        $return = 'FREIGHT COLLECT';
                    }else{
                        $return = 'FREIGHT PREPAID';
                    }
                }else if($condition_arr[0] == 'consol'){
                    $payment = $this->get_data_field($condition, 'payment', $num);
                    if($payment == 'PP'){
                        $return = 'FREIGHT PREPAID';
                    }else if($payment == 'CC'){
                        $return = 'FREIGHT COLLECT';
                    }else if($payment == 'E'){
                        $return = 'PREPAID AT ' . $this->get_data_field($condition, 'payment_third', $num);
                    }
                }

                return $return;
            }

            if($field == 'packs_sum_en'){//用于consol里的
                $data = $this->get_data($condition_arr);
                $packs = 0;
                if ($condition_arr[0] == 'consol'){
                    foreach ($data as $row){
                        $packs += $row['packs'];
                    }
                    if($packs == 0){
                        $shipment_data = $this->get_data(explode('.', 'consol.id-consol_id.shipment'));
                        $good_outers_sum = 0;
                        foreach ($shipment_data as $row){
                            $good_outers_sum += $row['good_outers'];
                        }
                        return number_to_word($good_outers_sum);
                    }
                }else{
                    foreach ($data as $row){
                        $packs += $row['packs'];
                    }
                    if($packs == 0){
                        $shipment_data = $this->get_data(explode('.', 'shipment'));
                        $good_outers_sum = 0;
                        foreach ($shipment_data as $row){
                            $good_outers_sum += $row['good_outers'];
                        }
                        return number_to_word($good_outers_sum);
                    }
                }
                // $packs = $this->get_data_field($condition, 'sum(packs)', 0);
                // var_dump($this->get_data($condition_arr));
                // var_dump($data);
                return number_to_word($packs);
            }

            if($field == 'box_info'){
                $return = array();

                //shipment且为LCL取container的值
                if($condition_arr[0] == 'shipment' && $this->get_data_field($condition, 'trans_mode', $num) == 'LCL'){
                    $this->load->model('biz_container_model');
                    $this->load->model('biz_shipment_model');
                    $shipment_id = $this->get_data_field('shipment', 'id', 0);
                    $consol_id = $this->biz_shipment_model->get_one_all('id', $shipment_id)['consol_id'];
                    $shipment_containers = $this->get_data(explode('.', 'shipment.id-shipment_id.shipment_container'));

                    $box_info = array();
                    foreach ($shipment_containers as $row){
                        $this_container = $this->biz_container_model->get_where_one("consol_id = '$consol_id' and container_no = '" . $row['container_no'] . "'");
                        if(isset($box_info[$this_container['container_size']])){//存在+1,不存在添加
                            $box_info[$this_container['container_size']] += 1;
                        }else{
                            $box_info[$this_container['container_size']] = 1;
                        }
                    }

                    foreach ($box_info as $key => $val){
                        $return[] = $key . 'X' . $val;
                    }
                    $return = join(', ', $return);
                    if(!empty($return)){
                        $return = 'PART OF ' . $return;
                    }
                }else{
                    $value = json_decode($this->get_data_field($condition, $field, $num), true);

                    foreach ($value as $key => $val){
                        $return[] = $val['SIZE'] . 'X' . $val['NUM'];
                    }

                    $return = join(', ', $return);
                }
                return $return;
            }

            if($field == 'job_invoice'){
                //发票号,如果有传入参数,则取该发票号,没传取job_no
                $result = '';
                if(isset($this->parameter['invoice_id']) && $this->parameter['invoice_id'] != 0){
                    $row = Model('biz_bill_invoice_model')->get_one('id', $this->parameter['invoice_id']);
                    return $row['invoice_no'];
                }
                $year = date('Y');
                $id = $this->get_data_field($condition, 'id', $num);
                $num = str_pad($id, 5, '0', STR_PAD_LEFT);
                return "LONG1701" . $year . $num;
            }
            if($field == 'container_no_split'){
                //将箱号拼接起来
                if ($condition_arr[0] == 'shipment'){
                    $result = array();
                    //container_no seal_no container_size packs weight volume
                    $container_data = $this->get_data($condition_arr);

                    foreach ($container_data as $key => $row){
                        $result[] = $row['container_no'];
                    }
                    return join(',', $result);
                }
            }
            if($field == 'now1'){
                return date('M.d,Y');
            }
            if($field == 'dn_template_title'){
                if(isset($this->parameter['dn_template_title'])){
                    $result = $this->parameter['dn_template_title'];
                }else{
                    $result = 'TAX INVOICE';
                }
                return $result;
            }
            if($field == 'this_local_currency'){
                //当前本地币种
                $this_local_currency = isset($this->parameter['this_local_currency']) ? $this->parameter['this_local_currency'] : 'AED';
                $this->set_data_field($this_local_currency, $condition, $field, $num);
                return $this_local_currency;
            }
            if($field == 'base_amount'){
                //折算金额
                $exrate = $this->get_data_field($condition, 'ex_rate_dubai', $num, 1);
                $amount = $this->get_data_field($condition, 'amount', $num);
                //获取折算金额

                $base_amount = round(strval($amount * 100) * strval($exrate * 1000000) / 100000000, 2);
                $this->set_data_field($base_amount, $condition, $field, $num);
                return $base_amount;
            }
            if($field == 'ex_rate_dubai'){
                //迪拜版本的汇率获取
                if($this->get_data_field($condition, $field, $num) !== ''){
                    return $this->get_data_field($condition, $field, $num);
                }

                $bill_ETD = $this->get_data_field($condition, 'bill_ETD', $num);
                $currency = $this->get_data_field($condition, 'currency', $num);
                $type = $this->get_data_field($condition, 'type', $num);
                $this_local_currency = $this->get_data_field($condition, 'this_local_currency', $num, 1);

                $ex_rate_dubai = round(get_ex_rate($bill_ETD, $currency, strtolower($type), $this_local_currency), 4);

                $this->set_data_field($ex_rate_dubai, $condition, $field, $num);
                return $ex_rate_dubai;
            }
            if($field == 'charge_en'){
                $charge = $this->get_data_field($condition, 'charge_code', $num);
                //英文/中文
                $this->db->select('charge_name,charge_name_cn');
                $charge_data = Model('biz_charge_model')->get_one('charge_code', $charge);
                return "{$charge_data['charge_name']}/{$charge_data['charge_name_cn']}";
            }
            if($field == 'vat_amount'){
                $vat = $this->get_data_field($condition, 'vat', $num);
                $vat_num = (int)$vat / 100;
                $base_amount = $this->get_data_field($condition, 'base_amount', $num);
                //税额
                $vat_amount = round($base_amount * $vat_num, 2);

                $this->set_data_field($vat_amount, $condition, $field, $num);
                return $vat_amount;
            }
            if($field == 'vat_amount_sum'){
                $data = $this->get_data($condition_arr);

                $vat_amount_sum = array_sum(array_column($data, 'vat_amount'));
                return $vat_amount_sum;
            }
            if($field == 'grand_total'){
                // $grand_total = $this->get_data_field($condition, 'base_amount', $num);
                $base_amount = $this->get_data_field($condition, 'base_amount', $num, 1);
                $vat_amount = $this->get_data_field($condition, 'vat_amount', $num, 1);

                $grand_total = $base_amount + $vat_amount;

                $this->set_data_field($grand_total, $condition, $field, $num);
                return $grand_total;
            }
            if($field == 'grand_total_sum'){
                $data = $this->get_data($condition_arr);
                $grand_total_sum = array_sum(array_column($data, 'grand_total'));
                return $grand_total_sum;
            }
            if($field == 'net_amount'){
                // $grand_total = $this->get_data_field($condition, 'grand_total', $num, 1);
                // $vat_amount = $this->get_data_field($condition, 'vat_amount', $num, 1);

                // $net_amount = $grand_total - $vat_amount;
                $net_amount = $this->get_data_field($condition, 'base_amount', $num);
                $this->set_data_field($net_amount, $condition, $field, $num);
                return $net_amount;
            }
            if($field == 'net_amount_sum'){
                $data = $this->get_data($condition_arr);
                $net_amount_sum = array_sum(array_column($data, 'net_amount'));
                return $net_amount_sum;
            }
        }
    }

    private function set_data_field($value, $key, $field, $num = 0){
        return $this->data[$key][$num][$field] = $value;
    }
    /**
     * 获取文件类型
     * @param $file_name
     * @return bool|string
     */
    public function getFileType($file_name)
    {
        return substr($file_name, strrpos($file_name, '.') + 1);
    }

    protected function replace_str($str = '', $is_upper = true){
        if($is_upper) $str = strtoupper($str);

        $str = str_replace('&', '&amp;', $str);
        $str = str_replace('<', '&lt;', $str);
        $str = str_replace('>', '&gt;', $str);
        $str = str_replace( "\n", '<w:br />', $str);
        return $str;
    }

    protected function get_data_field($key, $field, $num = 0, $special = 0, $is_replace = true){
        $special_value = $special == 0 ? false : $this->get_special_value($key, $field, $num);
        if($special_value == false){
            if(isset($this->data[$key][$num][$field])){
                $value = $this->data[$key][$num][$field];
                if($is_replace) $value = $this->replace_str($value);
                // $value = strtoupper($this->data[$key][$num][$field]);

                // $value = str_replace('&', '&amp;', $value);
                // $value = str_replace('<', '&lt;', $value);
                // $value = str_replace('>', '&gt;', $value);
                // $value = str_replace( "\n", '<w:br />', $value);

                return $value;
            }else{
                return '';
            }
        }else{
            return $special_value;
        }

    }

    public function export_dword(){
        $file = isset($_GET['file']) ? $_GET['file'] : '';
        if($file == 'shenlingdan'){
            $this->shenlingdan();
        }
        if($file == 'invoice_vp'){
            return $this->invoice_vp();
        }
    }

    public $consol_field_all = array(
        array("job_no", "Job No", "94", "2", '1'),
        array("trans_carrier", "trans_carrier", "77", "9", '1', 'port info'),
        array("carrier_ref", "carrier_ref", "140", "9", '1', 'port info'),
        array("vessel", "vessel", "140", "9", '1', 'port info'),
        array("voyage", "voyage", "60", "9", '1', 'port info'),
        array("booking_ETD", "booking_ETD", "87", "9", '1', 'ship info'),
        array("trans_origin", "trans_origin", "60", "9", '1', 'port info'),
        array("trans_origin_name", "trans_origin_name", "0", "9", '1', 'port info'),
        array("trans_discharge", "trans_discharge", "60", "9", '1', 'port info'),
        array("trans_discharge_name", "trans_discharge_name", "0", "9", '1', 'port info'),
        array("trans_destination", "trans_destination", "60", "9", '1', 'port info'),
        array("trans_destination_name", "trans_destination_name", "0", "9", '1', 'port info'),
        array("creditor", "creditor", "122", "9", '1'),
        array("carrier_agent", "carrier_agent", "0", "9", '1'),
        array("agent_company", "agent Company", "116", "3", '1', 'agent info'),
        array("status", "status", "50", "0", '1'),
        array("biz_type", "biz_type", "60", "0", '1'),//业务类型
        array("created_by", "created_by", "72", "10", '1'),
        array("closing_date", "closing_date", "101", "9", '1'),
        array("trans_ETD", "trans_ETD", "60", "9", '1', 'ship info'),
        array("trans_ATD", "trans_ATD", "60", "9", '1', 'ship info'),
        array("trans_ETA", "trans_ETA", "60", "9", '1', 'ship info'),
        array("trans_ATA", "trans_ATA", "60", "9", '1', 'ship info'),
        array("feeder_voyage", "feeder_voyage", "60", "9", '1', 'port info'),
        array("feeder", "feeder", "60", "9", '1', 'port info'),
        array("trans_type", "trans_type", "60", "9", '1', 'port info'),
        array("sailing_code", "sailing_code", "60", "9", '1', 'port info'),//航线代码
        array("sailing_area", "sailing_area", "60", "9", '1', 'port info'),//航线区域
        array("id", "ID", "0", "1", '1'),
        array("agent_address", "agent_Address", "0", "4", '1', 'agent info'),
        array("agent_contact", "agent_contact", "0", "5", '1', 'agent info'),
        array("agent_telephone", "agent_telephone", "0", "6", '1', 'agent info'),
        array("agent_code", "agent Code", "88", "0", '1', 'agent info'),
        array("shipper_company", "shipper_company", "0", "7", '1', 'booking info'),
        array("shipper_address", "shipper_address", "0", "8", '1', 'booking info'),
        array("shipper_contact", "shipper_contact", "0", "9", '1', 'booking info'),
        array("shipper_telephone", "shipper_telephone", "0", "9", '1', 'booking info'),
        array("shipper_email", "shipper_email", "0", "9", '1', 'booking info'),
        array("consignee_company", "consignee_company", "120", "9", '1', 'booking info'),
        array("consignee_address", "consignee_address", "0", "9", '1', 'booking info'),
        array("consignee_contact", "consignee_contact", "0", "9", '1', 'booking info'),
        array("consignee_telephone", "consignee_contact", "0", "9", '1', 'booking info'),
        array("consignee_email", "consignee_email", "0", "9", '1', 'booking info'),
        array("notify_company", "notify_company", "0", "9", '1', 'booking info'),
        array("notify_address", "notify_address", "0", "9", '1', 'booking info'),
        array("notify_contact", "notify_contact", "0", "9", '1', 'booking info'),
        array("notify_telephone", "notify_contact", "0", "9", '1', 'booking info'),
        array("notify_email", "notify_email", "0", "9", '1', 'booking info'),
        array("container_owner", "container_owner", "80", "9", '1'),
        array("payment", "payment", "80", "9", '1'),
        array("payment_third", "payment_third", "0", "9", '1'),
        array("agent_ref", "agent_ref", "80", "9", '1'),
        array("description", "description", "0", "9", '1'),
        array("description_cn", "description_cn", "0", "9", '1'),
        array("mark_nums", "mark_nums", "0", "9", '1'),
        array("created_time", "created_time", "150", "10", '1'),
        array("updated_by", "updated_by", "150", "10", '1'),
        array("updated_time", "updated_time", "150", "11", '1'),
        array("warehouse_code", "warehouse_code", "0", "0", '1'),
        array("hbl_type", "hbl_type", "0", "0", '1'),
        array("dingcangwancheng", "dingcangwancheng", "0", "0", '1', 'step info'),
        array("xiangyijingang", "xiangyijingang", "0", "0", '1', 'step info'),
        array("matoufangxing", "matoufangxing", "0", "0", '1', 'step info'),
        array("chuanyiqihang", "chuanyiqihang", "0", "0", '1', 'step info'),
        array("trans_mode", "trans_mode", "80", "9", '1', 'booking info2'),
        array("trans_tool", "trans_tool", "80", "9", '1'),
        array("hs_code", "hs_code", "0", "0", '1', 'booking info2'),
        array("box_info", "box_info", "150", "0", '1', 'booking info2'),
        array("free_svr", "free_svr", "0", "0", '1', 'freight rates'),
        array("AGR_no", "AGR_no", "0", "0", '1', 'freight rates'),
        array("requirements", "requirements", "0", "0", '1', 'booking info2'),
        array("floor_price", "floor_price", "0", "0", '1', 'freight rates'),
        array("freight_rate", "freight_rate", "0", "0", '1', 'freight rates'),
        array("freight_affirmed", "freight_affirmed", "0", "0", '1', 'freight rates'),
        array("booking_confirmation", "booking_confirmation", "0", "0", '1', 'freight rates'),
        array("AP_code", "AP_code", "0", "0", '1'),
        array("customer_booking", "customer_booking", "0", "0", '1'),
        array("trans_discharge_code", "trans_discharge_code", "0", "0", '1'),
    );

    /**
     * 申领单 罗雯烜
     * 数据和consol查到的一样,只是
     */
    protected function shenlingdan(){
        $sort = postValue('sort', 'id');
        $order = postValue('order', 'desc');
        $view_name = getValue('view_name', '');

        //-------这个查询条件想修改成通用的 -------------------------------
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $shipment_no = isset($_REQUEST['shipment_no']) ? trim($_REQUEST['shipment_no']) : '';
        $is_hyf = isset($_REQUEST['is_hyf']) ? (int)$_REQUEST['is_hyf'] : '';
        $lock_lv = isset($_REQUEST['lock_lv']) ? $_REQUEST['lock_lv'] : array();
        $is_apply_shipment = isset($_REQUEST['is_apply_shipment']) ? (int)$_REQUEST['is_apply_shipment'] : 0;
        $operator_si = isset($_REQUEST['operator_si']) ? $_REQUEST['operator_si'] : '';
        $document_si = isset($_REQUEST['document_si']) ? $_REQUEST['document_si'] : '';
        //查询条件相关--start
        $where = array();

        $title_data = get_user_title_sql_data('biz_consol', $view_name);//获取相关的配置信息
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];


                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') $v = '';
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) $title_data['sql_join'][$title_data['base_data'][$title_f]['sql_join']] = $this_join;

                    $where[] = search_like($f, $s, $v);
                }
            }
        }

        if ($is_hyf !== 0) {
            if ($is_hyf === 1) {
                $where[] = "biz_consol.sign like '%hyf_qr%'";
            } else if ($is_hyf === 2) {
                $where[] = "biz_consol.sign not like '%hyf_qr%'";
            }
        }
        if ($operator_si !== '') {
            if ($operator_si === '0') $where[] = "biz_consol.operator_si = '0'";
            if ($operator_si === '1') $where[] = "biz_consol.operator_si != '0'";
        }
        if ($document_si !== '') {
            if ($document_si === '0') $where[] = "biz_consol.document_si = '0'";
            if ($document_si === '1') $where[] = "biz_consol.document_si != '0'";
        }
        if ($shipment_no != "") {
            Model('biz_shipment_model');
            $shipment = $this->biz_shipment_model->get_one_all("job_no like '%$shipment_no%'");
            if (empty($shipment)) $shipment['consol_id'] = 0;
            $where[] = "id = {$shipment['consol_id']}";
        }
        //2021-06-03 加入查询是否申请consol
        if ($is_apply_shipment == 1) {
            $where[] = "(select consol_id_apply from biz_shipment where biz_shipment.consol_id_apply = biz_consol.id limit 1) != 0";
        }

        if ($lock_lv !== array()) {
            $lock_lv_where = array();
            if (in_array(0, $lock_lv)) $lock_lv_where[] = 'biz_consol.lock_lv = 0';//C0
            if (in_array(1, $lock_lv)) $lock_lv_where[] = 'biz_consol.lock_lv = 1';//
            if (in_array(2, $lock_lv)) $lock_lv_where[] = "biz_consol.lock_lv = 2";//
            if (in_array(3, $lock_lv)) $lock_lv_where[] = "biz_consol.lock_lv = 3";//C3
            if (!empty($lock_lv_where)) {
                $where[] = '(' . join(' OR ', $lock_lv_where) . ' )';
            }
        };//查询锁

        $where = join(' and ', $where);
        //查询条件相关--end

        $this->db->group_by('biz_consol.vessel,biz_consol.voyage');
        $this->db->select('biz_consol.vessel,biz_consol.voyage, GROUP_CONCAT(biz_consol.carrier_ref) as carrier_refs');
        $rs = Model('biz_consol_model')->get_v3($where, $sort, $order);
        $rows = array();
        foreach ($rs as $row) {

            array_push($rows, $row);
        }

        ini_set('pcre.backtrack_limit', 999999999);

        $file_name = '/word/shenlingdan.docx';
        //查询模板对应的数据
        $file_type = $this->getFileType($file_name);
        $type = $this->file_type[$file_type];

        $file_path = './upload' . $file_name;
        if(!file_exists($file_path)) return false;
        $document = $this->read_file($file_path,$type);

        //${shipment.vessel} ${shipment.voyage} ${shipment.carrier_refs}
        $document->setValue('${shipment.vessel}', $rows[0]['vessel']);
        $document->setValue('${shipment.voyage}', $rows[0]['voyage']);
        $document->setValue('${shipment.carrier_refs}', $rows[0]['carrier_refs']);

        //3.保存文件
        $new_dir = './upload/temp/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = date('Ymd') . '-申领单.docx';
        $path = 'upload/temp/target.docx';
        $document->saveAs($path);

        //4.从浏览器下载
        ob_clean();
        ob_start();
        $fp = fopen($path,"r");
        $file_size = filesize($path);
        Header("Content-type:application/octet-stream");
        Header("Accept-Ranges:bytes");
        Header("Accept-Length:".$file_size);
        Header("Content-Disposition:attchment; filename=$file_name");
        $buffer = 1024;
        $file_count = 0;
        while (!feof($fp) && $file_count < $file_size){
            $file_con = fread($fp,$buffer);
            $file_count += $buffer;
            echo $file_con;
        }
        fclose($fp);
        ob_end_flush();
    }

    public function post_export(){
        //定义post处理页面url
        $get = $_GET;
        $url_p = array();
        $parameter = array();
        foreach ($get as $key => $val){
            if(is_string($val)){
                $url_p[] = $key . '=' . $val;
            }else{
                if(is_array($val) && $key = 'parameter'){
                    $parameter = $val;
                }
            }
        }
        $url_p = '?' . join('&', $url_p);
        $actionUrl = '/export/export_word' . $url_p;
        $queryArr = $parameter;
        //循环拼接表单项
        $formItemString = '' ;

        foreach ( $queryArr  as  $key => $value ){
            $formItemString .= "<input type=\"hidden\" name=\"parameter[$key]\" value=\"$value\">";
        }
        //构造表单并跳转

        $content = "<form style= 'display:none' name= 'submit_form' id= 'submit_form' action='$actionUrl' method= 'post' >
        $formItemString
        </form>
        <script type= \"text/javascript\" >
            document.submit_form.submit();
            setTimeout(function(){
                window.close();
            }, 500);
        </script>";
        echo $content;
    }


    public function invoice_vp(){
        Model('m_model');
        $id = isset($_GET['id']) ? $_GET['id'] : '';
        $this->load->library('Variable_content');
        $this->Variable_content = new Variable_content($id);

        $box_info = json_decode($this->Variable_content->get('${shipment.box_info}'), true);

        //invoice_vp模板根据箱信息, 将账单拆成20份,
        //统计箱子
        $teu = 0;
        //统计一下总teu数 用于计算num分成多少份
        foreach ($box_info as $row){
            $size = substr($row['size'], 0, 2);
            if ($size == '20') {
                $teu += 1 * $row['num'];
            } else if ($size == '40') {
                $teu += 2 * $row['num'];
            }
        }

        $parameter = isset($_POST['parameter']) ? $_POST['parameter'] : array();

        //将参数里的 指定num替换掉
        $parameter['bill_avg_total'] = $teu;//总数

        $shipment_containers = $this->Variable_content->getData("shipment.id-shipment_id.shipment_container");
        $job_no = $this->Variable_content->get('${shipment.job_no}');
        $consol_id = $this->Variable_content->get('${shipment.consol_id}');

        //获取当前目标的code
        $sql = "select * from bsc_template where code = 'INVOICE_VP_CONTAINER'";
        $template = Model('m_model')->query_one($sql);

        $url = base_url() . "export/export_word/2";
        $files = array();//压缩包的全部文件
        foreach ($shipment_containers as $shipment_container){
            $container_no = $shipment_container['container_no'];
            $this_container = Model('biz_container_model')->get_where_one("consol_id = '{$consol_id}' and container_no = '{$container_no}'");
            $container_size = $this_container['container_size'];

            $size = substr($container_size, 0, 2);
            $this_teu = 0;
            if ($size == '20') {
                $this_teu = 1;
            } else if ($size == '40') {
                $this_teu = 2;
            }

            $this_url = $url . "?template_id={$template['id']}&data_table=shipment&id={$id}";
            $parameter['container_no'] = $container_no;
            $parameter['bill_avg_num'] = $this_teu;//    bill_avg_num / bill_avg_total 为当前百分比
            $post = array(
                'parameter' => $parameter
            );
            $headers = [
            ];
            $result_json = curl_post_body($this_url,$headers, http_build_query($post));
            $result = json_decode($result_json, true);

            if(!isset($result['code']) || (isset($result['code']) && $result['code'] != 0)){
                continue;
            }

            //上传文件
            //根据文件路径剪切文件到原本的上传目录，然后保存上传记录到该shipment
//            $pdf_path = "http://pdf.leagueshipping.com" . $result['file_path'];
            $pdf_path = "./" . $result['file_path'];
//            $file_name = "{$job_no}_{$shipment_container['container_no']}.pdf";
            $file_name = "{$job_no}_{$shipment_container['container_no']}.docx";
            $files[] = temp_upload_file($file_name,file_get_contents($pdf_path));
        }

        // 创建一个新的 ZIP 归档文件
        $zip = new ZipArchive();

        $path = date('Ymd');
        $new_dir = dirname(BASEPATH) . '/upload/temp/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $time = time();
        $file_name = "{$job_no}_{$time}_INVOICE_VP.zip";
        $zipname = $new_dir . $file_name;
        $zipname = str_replace('\\', DIRECTORY_SEPARATOR, $zipname);
        $zipname = str_replace('/', DIRECTORY_SEPARATOR, $zipname);

        // 如果 $zip->open() 方法调用成功，它返回一个非零值
        if ($zip->open($zipname, ZipArchive::CREATE) !== TRUE) {
            echo "无法创建 ZIP 文件。";
            return;
        }

        // 遍历文件数组并将每个文件添加到 ZIP 归档中
        foreach ($files as $file) {
            if (!file_exists($file)) {
                continue;
            }
            // 使用文件名（不带路径）作为 ZIP 文件中的条目名称
            $zip->addFile($file, basename($file));
        }

        // 关闭 ZIP 文件以完成操作
        $zip->close();

        // 发送文件到浏览器进行下载
//        header('Content-Type: application/zip');
//        header('Content-Disposition: attachment; filename="'.$zipname.'"');
//        header('Content-Length: ' . filesize($zipname));


        //4.从浏览器下载
        ob_clean();
        ob_start();
        $fp = fopen($zipname,"r");
        $file_size = filesize($zipname);
        Header("Content-type:application/octet-stream");
        Header("Accept-Ranges:bytes");
        Header("Accept-Length:".$file_size);
        Header("Content-Disposition:attchment; filename=$file_name");
        $buffer = 1024;
        $file_count = 0;
        while (!feof($fp) && $file_count < $file_size){
            $file_con = fread($fp,$buffer);
            $file_count += $buffer;
            echo $file_con;
        }
        fclose($fp);
        ob_end_flush();
    }
}
