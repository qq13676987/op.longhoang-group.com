    <script type="text/javascript">
        function doSearch(){
            $('#tt').datagrid('load',{
                name: $('#name').val()
            });
        }
        $(function(){
            $('#tt').edatagrid({
                url: '/bsc_dict/get/<?php echo $catalog;?>',
                saveUrl: '/bsc_dict/add/<?php echo $catalog;?>',
                updateUrl: '/bsc_dict/update/<?php echo $catalog;?>',
                destroyUrl: '/bsc_dict/mdelete',
                onError: function (index, data) {
                    $.messager.alert('error', data.msg);
                }
            });
			$('#tt').datagrid({
				width:'auto',
				height: $(window).height() 
            }); 
			
			$(window).resize(function () {
				$('#tt').datagrid('resize');
			});
        });
            
    </script>

    <table id="tt" style="width:1100px;height:450px"
           rownumbers="false" pagination="true" idField="id"
           pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
        <thead>
        <tr>
            <th field="id" width="60" sortable="true">ID</th>
            <th field="catalog" width="150" align="center" sortable="true">catalog</th>
            <th field="name" width="200" align="center" editor="{type:'validatebox',options:{required:true}}">name</th>
            <th field="value" width="200" editor="{type:'validatebox',options:{required:true}}">value</th>
            <th field="orderby" width="150" align="left" sortable="true"
                editor="{type:'validatebox',options:{required:true}}">orderby
            </th>
            <th field="ext1" width="150" align="left" sortable="true" editor="{type:'validatebox',}">ext1</th>
            <th field="ext2" width="150" align="left" sortable="true" editor="{type:'validatebox',}">ext2</th>
            <th field="ext3" width="150" align="left" sortable="true" editor="{type:'validatebox',}">ext3</th>
            <th field="ext4" width="150" align="left" sortable="true" editor="{type:'validatebox',}">ext4</th>
            <th field="ext5" width="150" align="left" sortable="true" editor="{type:'validatebox',}">ext5</th>
        </tr>
        </thead>
    </table>

   <div id="tb" style="padding:3px;">
        <table><tr>
        <td>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#tt').edatagrid('addRow',0)"><?= lang('add');?></a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#tt').edatagrid('destroyRow')"><?= lang('delete');?></a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#tt').edatagrid('saveRow')"><?= lang('save');?></a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#tt').edatagrid('cancelRow')"><?= lang('cancel');?></a>
        </td>
        <td width='80'> </td>
        <td>
        <span style="font-size:12px;">name</span>
        <input id="name" style="border:1px solid #ccc">
        </td><td>
        <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="doSearch()"><?= lang('search');?></a>
        </td>
        </tr></table>
         
    </div>
