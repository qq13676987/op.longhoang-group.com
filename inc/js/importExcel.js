var import_run = true, err = 0, succe = 0;
var importExcel = {
    init: function (optoin) {
        var _this = this;
        this.optoin = {
            'data':[],
            'importUrl': '',    // 数据导入地址
            'columns': [] // 批定获取Excel的列数
        };

        _this.options = $.extend({}, this.optoin, optoin);
        if (_this.options.importUrl == '') {
            layer.alert('导入地址无效！', {title: '系统消息', icon: 2});
            return false;
        }

        //打开导入进度
        importing = layer.open({
            type: 1
            , title: '线索导入进度'
            , closeBtn: false
            , area: ['500px', '150px']
            , content: $('#progress')
            , success: function (k, layeros) {
                //开始逐条导入
                _this.importData(_this.options.data, 0);
            }
            , cancel: function (index, layero) {
                import_run = false;
                layer.close(index);
            }
        });
    },
    /**
     * @title 发送导入请求
     * @param sendData
     * @returns
     */
    importData: function (sendData, i) {
        var element = layui.element,
            _this = this;
        datalength = sendData.length;
        if (import_run == true) {
            $.ajax({
                type: 'post',
                url: _this.options.importUrl,
                data: sendData[i],
                success: function (data) {
                    //更新进度条
                    var progress = _this.keepTwoDecimal((i+1) / datalength * 100);
                    element.progress('demo', progress + '%');
                    i++;
					if (data.code == 1) {    //如果本次执行成功
						succe++;
					} else {
						err++;
					}
                    if (i < datalength) {
                        //递归直到全部数据导入完成
                        _this.importData(sendData, i);
                    } else {
                        var total = '数据总量：（' + datalength + '）条<br>';
                        var success = '导入成功：（' + succe + '）条<br>';
                        //var faild = '导入失败：（' + err + '）条';
                        layer.alert(total + success, {
                            icon: 1,
                            title: '线索导入结果',
                            closeBtn: false
                        }, function (index, layero) {
                            succe = 0;
                            layer.close(importing);
                            layer.close(index);
                        });
                    }
                },
                dataType: "json"
            });
        }
    },
    /**
     * @title 取两位小数
     * @param num
     * @returns {number}
     */
    keepTwoDecimal: function (num) {
        var result = parseFloat(num);
        result = Math.round(num * 100) / 100;
        return result;
    }
};
