<div id="tab" class="easyui-tabs" style="width:100%;">
	<?php foreach ($tabs as $tab):?>
		<div title="<?=$tab['name']?>" style="padding:1px;width: 100%;height: 99%">
			<iframe scrolling="auto" frameborder="0" id="<?=$tab['name']?>" style="width:99%;height:99%;" ></iframe>
		</div>
	<?php endforeach;?>
</div>

<script>
	$('#tab').tabs({
		height: $(window).height(),
		border: false,
		onSelect: function (title) {
			<?php foreach ($tabs as $tab):?>
			if (title == "<?=$tab['name']?>") {
				if (document.getElementById("<?=$tab['name']?>").src == "") {
					document.getElementById("<?=$tab['name']?>").src = "/biz_branch_consol/index/<?=$client_code?>/<?=$tab['name']?>";
				}
			}
			<?php endforeach;?>
		}

	});
</script>
