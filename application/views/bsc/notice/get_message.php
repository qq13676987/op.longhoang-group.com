<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>消息</title>
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css" media="all">
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <style>
        .demo-carousel{height: 200px; line-height: 200px; text-align: center;}
        .layui-tab-title .layui-this::after {border-bottom:1px solid #f6f6f6 ; border-radius: 0;}
        .layui-tab ｛margin:0px;}
    </style>
</head>
<body  style="background: #f6f6f6 ">
<div class="layui-tab" style="margin: 0px; position: fixed; z-index: 999999; background: #fff; width: 100%">
    <ul class="layui-tab-title" style="background: #fff;">
        <li class="_type layui-this" style="background: #f6f6f6 ;" id="unread">未读</li>
        <li class="_type" id="readed">已读</li>
    </ul>
</div>
<div class="layui-tab-content" >
    <div class="layui-tab-item layui-show" id="message_list" style="margin-top: 40px;"></div>
    <div id="no_more" style="text-align: center; color: #999; line-height: 30px;"></div>
</div>
<script type="text/html" id="message_tpl">
    <div class="layui-card">
        <div class="layui-card-body">
            {{d.txt}}
            <a href="{{d.href}}" target="_blank" style="color: #ff8800; padding: 0px 5px;"> 查看>> </a>
        </div>
        <div class="layui-card-header" style="border-top: 1px solid #efefef;">
            <div style="float: left; color: #999;">发送时间：{{d.create_time}}</div>
            {{# if(d.status==0){ }}
            <div style="float: right;"><a href="javascript:void(0);" onclick="set_read({{d.id}})" style="color: dodgerblue">知道了</a></div>
            {{# } }}
        </div>
    </div>
</script>
<script type="text/javascript" src="/inc/third/layui/layui.js"></script>
<script>
    var laytpl, flow, layer;
    //加载模块
    layui.use(['element', 'flow', 'layer', 'laytpl'], function(){
        flow = layui.flow, laytpl = layui.laytpl, layer = layui.layer;
        $('._type').click(function () {
            $('._type').css('background','#fff');
            $(this).css('background','#f6f6f6 ');
        });

        load_data(0);

        //未读
        $('#unread').click(function () {
            load_data(0);
        });

        //已读
        $('#readed').click(function () {
            load_data(1);
        });
    });

    function load_data(isread) {
        var loading = layer.load(1);
        $('#message_list').html('');
        $.ajax({
            type: 'post',
            url: '/bsc_notice/get_user_noitce_data',
            data: {isread:isread, type:0},
            dataType: 'json',
            success: function (res) {
                layui.each(res.rows, function(index, item){
                    laytpl($('#message_tpl').html()).render(item, function(html){
                        $('#message_list').append(html);
                    });
                });
            }
        });
        layer.close(loading);
        $('#no_more').text('没有更多了');
    }

    function set_read(id) {
        $.ajax({
            url:'/bsc_notice/notice_read?id=' +id,
            type:'get',
            dataType:'json',
            success:function(res) {
                load_data(0);
            }
        });
    }
</script>
</body>
</html>
