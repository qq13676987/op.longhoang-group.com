<!doctype html> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script>
    $(function () {
        $('a').click(function () {
            var url = $(this).prop('href');
            if(url.indexOf('/bsc_template/set_parameter') != -1){
                $('#window').window({
                    title:'window',
                    width:500,
                    height:200,
                    href:url,
                    modal:true
                });
                return false;
            }else{
                return true;
            }

        });
    });
</script>
<style type="text/css">
    *{
        font-size: 14px;
    }
</style>
<span style="color:red;">
</span>
<br><br>
<?php 
    if($bill_type=="LEAGUESHIPPING"){
        $bill_type="J";
    } 
    if($bill_type=="KIMXIN SUPPLY CHAIN"){
        $bill_type="K";
    } 
?>
<table width="500">
<?php if(!empty($row)){
        foreach ($row as $key => $val){ 
            // if($val["sub_company"]!=$bill_type && $val["sub_company"]!="" && $bill_type != 'MBL') continue;
            if($val["id"]==22 ||$val["id"]==37 ) continue;
?>
    <tr>
        <td> <br><?= $val['file_name']; 
        if($val["id"]==22 ||$val["id"]==37 ) { 
        ?>
        <a href="javascript:void;" class="easyui-tooltip" data-options="
                                        content: $('<div></div>'),
                                        onShow: function(){
                                            $(this).tooltip('arrow').css('left', 20);
                                            $(this).tooltip('tip').css('left', $(this).offset().left);
                                        },
                                        onUpdate: function(cc){
                                            cc.panel({
                                                width: 300,
                                                height: 'auto',
                                                border: false,
                                                href: '/bsc_help_content/help/download_bl'
                                            });
                                        }
                                    "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
        <?php
        }
        ?></td>
        <?php 
        $file_template = explode('.', $val['file_template']);
        $export_url = '/export/export_word';
        if(end($file_template) !== 'docx'){
            $export_url = '/export/export_excel';
        }
        if(empty($val['parameter'])){
            $url = $export_url . '?template_id=' . $val['id'] . '&data_table=' . $val['data_table'] . '&id=' . $id;
        }else{
            $url = '/bsc_template/set_parameter?url=' . $export_url . '&template_id=' . $val['id'] . '&data_table=' . $val['data_table'] . '&id=' . $id . '&parameter=' . $val['parameter'];
        }?>
        <?php if(end($file_template) === 'docx'){ ?>
        <td>
            <br> 
            <a href="<?= $url;?>" target='_blank'><?= lang('数据下载');?>(word)</a>
        </td>
        <td>
            <br> 
            <a href="<?= $url;?>&pdf=1" target='_blank'><?= lang('数据下载');?>(pdf)</a>
        </td>
        <?php }else{ ?>
        <td>
            <br> 
            <a href="<?= $url;?>" target='_blank'><?= lang('数据下载');?>(excel)</a>
        </td>
        <td>
            <br> 
            <!--<a href="<?= $url;?>&pdf=1" target='_blank'><?= lang('数据下载');?>(pdf)</a>-->
        </td>
        <?php } ?>
        <?php if(is_admin(1)){ ?>
        <td>
            <br> 
            <a href="/upload<?= "/" . get_system_type() . $val['file_template'];?>?v=1" target='_blank'><?= lang('模板下载');?></a>
        </td>
        <?php } ?>
    </tr>

<?php }}else{
    echo '无';
}?>
</table>
<div id="window">

</div>