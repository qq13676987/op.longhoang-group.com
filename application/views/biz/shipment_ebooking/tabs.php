<div id="tab" class="easyui-tabs" style="width:100%;">
	<div title="Ebooking" style="padding:1px;width: 100%;height: 720px">
		<iframe scrolling="auto" frameborder="0" id="ebooking" style="width:99%;height:99%;" ></iframe>
	</div>
	<div title="pre-alert EDI<span style='color:red'>【<?=$edi_num?>】</span>" style="padding:1px;width: 100%;height: 720px">
		<iframe scrolling="auto" frameborder="0" id="edi" style="width:99%;height:99%;" ></iframe>
	</div>
</div>

<script>
	$('#tab').tabs({
		border: false,
		height:$(window).height(),
		onSelect: function (title,index) {
			if (title == "Ebooking") {
				if (document.getElementById("ebooking").src == "") {
					document.getElementById("ebooking").src = "/biz_shipment_ebooking/index2";
				}
			}
			if (index == 1) {
				if (document.getElementById("edi").src == "") {
					document.getElementById("edi").src = "/biz_edi_ebooking_consol/index";
				}
			}
		}

	});
</script>
