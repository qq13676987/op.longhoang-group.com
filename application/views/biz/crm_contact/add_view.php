<title><?= lang('添加联系人');?></title>
<body style="padding: 10px 20px;">
<div style="width: 100%">
    <form id="myform" name="myform" method="post">
        <input type="hidden" name="crm_id" id="crm_id" value="<?=$data['id'];?>">
        <table>
            <tr style="height: 30px;">
                <td style="width: 60px;"><?= lang('角色');?>：</td>
                <td>
                    <select id="role" name="role" readonly="true" class="easyui-combobox" style="width: 280px; height: 25px;"
                            data-options="">
                    </select>
                </td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('姓名');?>：</td>
                <td><input class="easyui-textbox" type="text" required="required" name="name" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('手机号');?>：</td>
                <td><input class="easyui-textbox" type="text" name="telephone" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('座机号');?>：</td>
                <td><input class="easyui-textbox" type="text" name="telephone2" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('邮箱');?>：</td>
                <td><input class="easyui-textbox" type="text" required="required" name="email" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('地址');?>：</td>
                <td><input class="easyui-textbox" type="text" name="address" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('QQ');?>：</td>
                <td><input class="easyui-textbox" type="text" name="live_chat" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('职位');?>：</td>
                <td>
                    <input class="easyui-combobox" type="text" name="position" value="" style="width: 280px; height: 25px;"
                           data-options="
                           valueField: 'value',
                           textField: 'name',
                           url:'/bsc_dict/get_option/contact_position',"
                    />
                </td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('前台权限');?>：</td>
                <td>
                    <input class="easyui-combobox" type="text" name="menu1" id="menu1" value="" style="width: 280px; height: 25px;"
                           data-options="
                           valueField: 'value',
                           textField: 'value',
                           url:'/biz_client_contact/get_menu',
                           multiple:true,
                            onSelect: function(rec){
                                temp = $('#menu1').combobox('getValues');
                                temp =temp.toString();
                                $('#menu').val(temp);
                            }

                           "/>

                    <input type="hidden" id='menu' name='menu'>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('前台密码');?>：</td>
                <td><input class="easyui-textbox" type="text" name="password" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('航线');?>：</td>
                <td>
                    <input class="easyui-combobox" type="text" name="sailing" value="" style="width: 280px; height: 25px;"
                           data-options="
                           valueField: 'value',
                           textField: 'name',
                           url:'/bsc_dict/get_option/export_sailing',"
                    />
            </tr>
            <tr style="height: 30px;">
                <td><?= lang('备注');?>：</td>
                <td><input class="easyui-textbox" type="text" name="remark" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td></td>
                <td align="center">
                    <input type="hidden" name="is_submit" value="1">
                    <a href="javascript:void(0);" id="save_data" class="easyui-linkbutton" style="width: 280px;height:30px;margin-top: 10px;"><?= lang('保存联系人');?></a>
                </td>
            </tr>
        </table>
    </form>
</div>
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<script>
    $(function () {
        //异步写入数据
        $('#save_data').click(function () {
            var field = {};
            $.each($('#myform').serializeArray(), function (index, item) {
                field[item.name] = item.value;
            });

            $.ajax({
                url: '/biz_client_contact/add_crm/?crm_id=<?=$data['id'];?>',
                type: 'POST',
                data: field,
                dataType: 'json',
                success: function (data) {
                    if (data.code == 1){
                        //刷新父页面的列表控件
                        layer.alert("<?= lang('保存成功');?>", {icon:1,title:'<?= lang('提示');?>'}, function (index) {
                            parent.$('#tt').edatagrid('reload');
                            //关闭当前弹窗
                            parent.$('#_window').window('close');
                        });

                    }else if(data.code == 2){
                        layer.confirm(data.msg+"<?= lang('，点击“继续提交”申请审核。点击“取消”更换其它Email。');?>", {
                            icon:5,
                            title:'<?= lang('提示');?>',
                            btn:["<?= lang('继续提交');?>", "<?= lang('取消');?>"]
                        }, function (index) {
                            apply_crm_contact();
                        });
                    }else{
                        layer.alert('<?= lang('保存失败：');?>'+data.msg, {icon:5});
                    }
                },
                error: function (xhr, status, error) {
                    layer.alert('<?= lang('操作失败!');?>', {icon:5});
                }
            });
        });

        $('#role').combobox({
            required: true,
            multiple: false,
            editable: false,
            valueField: 'value',
            textField: 'value',
            url: '/bsc_dict/get_option/role',
            value: '<?=$data['role']?>',
        })
    })

    function apply_crm_contact() {
        var field = {};
        $.each($('#myform').serializeArray(), function (index, item) {
            field[item.name] = item.value;
        });

        $.ajax({
            url: '/biz_client_contact/apply_crm_contact/?crm_id=<?=$data['id'];?>',
            type: 'POST',
            data: field,
            dataType: 'json',
            success: function (data) {
                if (data.code == 1){
                    //刷新父页面的列表控件
                    layer.alert("<?= lang('保存成功');?>", {icon:1,title:'<?= lang('提示');?>'}, function (index) {
                        parent.$('#tt').edatagrid('reload');
                        //关闭当前弹窗
                        parent.$('#_window').window('close');
                    });
                }else{
                    layer.alert('<?= lang('保存失败：');?>'+data.msg, 'error',{icon:5});
                }
            },
            error: function (xhr, status, error) {
                layer.alert('<?= lang('操作失败!');?>', {icon:5});
            }
        });
    }
</script>
</body>
</html>