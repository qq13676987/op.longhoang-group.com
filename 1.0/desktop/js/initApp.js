$(function () {
	var urlhost = "http://" + window.location.host;
    $('body').app({
        onTaskBlankContextMenu:onTaskBlankContextMenu,
        onAppContextMenu:onAppContextMenu,
        onWallContextMenu:onWallContextMenu,
		onStartMenuClick:onStartMenuClick,
    });
	
	function onStartMenuClick(item){
		var data = $(item.target).data("data");
		$('body').app("createwindow",data);
	}

    var appListMenuData = [
        {
            "text":"Open"
        },
        {
            "text":"Close"
        },
        {
            "text":"Close Other"
        },
        {
            "text":"Close All"
        },
        {
            "text":"Task Manage"
        },
        {
            "text":"Property"
        }
    ];

   	//  var appListMenu = $('body').app('createMenu', {data:appListMenuData});//原先的
    //自己加
    var appListMenu = $('body').app('createMenu', {data:appListMenuData,opt:{onClick:onTaskBlankContextMenuClick}});
    var APPID1;
    //自己加end

    function onTaskBlankContextMenu(e, appid) {
        if (appid) {
           // console.log(appid);
            appListMenu.menu('findItem', 'Task Manage').target.style.display = 'none';
            appListMenu.menu('findItem', 'Property').target.style.display = 'none';
            appListMenu.menu('findItem', 'Open').target.style.display = 'block';
            appListMenu.menu('findItem', 'Close').target.style.display = 'block';
            appListMenu.menu('findItem', 'Close Other').target.style.display = 'block';
            appListMenu.menu('findItem', 'Close All').target.style.display = 'block';
        } else {
            appListMenu.menu('findItem', 'Task Manage').target.style.display = 'block';
            appListMenu.menu('findItem', 'Property').target.style.display = 'block';
            appListMenu.menu('findItem', 'Open').target.style.display = 'none';
            appListMenu.menu('findItem', 'Close').target.style.display = 'none';
            appListMenu.menu('findItem', 'Close Other').target.style.display = 'none';
            appListMenu.menu('findItem', 'Close All').target.style.display = 'none';
        }

        appListMenu.menu('show', {
            left:e.pageX,
            top:e.pageY
        });
		APPID1=appid;//自己加的
        e.preventDefault();
    }
	
	//自己加的
    function onTaskBlankContextMenuClick(item){
		if(item.text == 'Open'){
			$("li[app_id='"+APPID1+"']").dblclick();
			//console.log('这里使原来的显示');
		}
		if(item.text == 'Close'){
			$("div[w_id='"+APPID1+"']").parent().find('.panel-tool-close').click();
			//console.log('这里使原来的关闭');
		}
		if(item.text == 'Close Other'){
			$("div[w_id='"+APPID1+"']").parent().siblings().find('.panel-tool-close').click();
			//console.log('这里关闭其他');
		}
		if(item.text == 'Close All'){
			$('.panel-tool-close').click();
			//console.log('这里关闭所有');
		}
	}
	//自己加的end

    var wallMenuData = [
        {
            "text":"Desktop Config",
            "href": urlhost + "/sys_config/desktop"
        },
        {
            'text':"Set Language",
            "href":"javascript:;",
            "children":[
                {
                    'text':'中文',
                    "href":urlhost + "/lang/setlang?lang=cn",
                    "window": 'no', 
                },
                {
                    'text':'english',
                    "href":urlhost + "/lang/setlang?lang=en",
                    "window": 'no', 
                }]
        },
        {
            "text":"View notification",
            "href": urlhost + "/bsc_notice/user_notice"
        },
        '-',{
            "text":"Refresh",
            "href": "javascript:;",
            "onClick": "javascript:$('body').app('refreshapp')",
        } 
    ];

    var appMenuData = [
        {
            "text":"Open"
        },
        '-',
        {
            "text":"Property"
        }
    ];

    var wallMenu = $('body').app('createMenu', {data:wallMenuData,opt:{onClick:onStartMenuClick}});
    var appMenu = $('body').app('createMenu', {data:appMenuData,opt:{onClick:onAppContextMenuClick}});

	var APPID;
    function onAppContextMenu(e,appid) {
        appMenu.menu('show', {
            left:e.pageX,
            top:e.pageY
        });
		APPID = appid;
    }
	
	function onAppContextMenuClick(item){
		if(item.text == 'Open'){
			$("li[app_id='"+APPID+"']").dblclick();
		}
	}

    function onWallContextMenu(e) {
        wallMenu.menu('show', {
            left:e.pageX,
            top:e.pageY
        });
    }
});
