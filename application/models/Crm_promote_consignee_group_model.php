<?php
class crm_promote_consignee_group_model extends CI_Model{

    private $table = 'crm_promote_consignee_group';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_one($f="",$v=""){
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_all($userid=0){
        $this->db->where("create_by",$userid);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }


    public function get($where="" ,$sort="id",$order="desc",$like=array()){
        if($where !="")	$this->db->where($where);
        $this->db->like($like);
        $this->db->order_by($sort,$order);
        $query = $this->db->get($this->table);

        return $query->result_array();
    }

    public function total($where="",$like=array()){
        if($where !="")	$this->db->where($where);
        $this->db->like($like);
        $this->db->from($this->table);
        return  $this->db->count_all_results('');
    }

    public function get_array($sql=""){
        if($sql!=""){
            $rs = $this->db->query($sql);
            $arr = $rs->result_array();
            return $arr;
        }
    }

    public function get_row($where=array()){
        if($where != array()) $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function del($id = '')
    {
        // delete container
        $this->db->where('id', $id);
        return $this->db->delete($this->table);
    }

    public function add_group_name_rt_id($group_name=""){
        $data = array();
        $data["create_time"] = date('Y-m-d H:i:s',time());
        $data["update_time"] = date('Y-m-d H:i:s',time());
        $data["create_by"] = $this->session->userdata('id');
        $data["update_by"] = $this->session->userdata('id');
        $data["group_name"] = $group_name;

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function add_group_name($group_name=""){
        $data["create_time"] = date('y-m-d H:i:s',time());
        $data["update_time"] = date('y-m-d H:i:s',time());
        $data["create_by"] =  $this->session->userdata('id');
        $data["update_by"] =  $this->session->userdata('id');
        $sql="INSERT INTO crm_promote_consignee_group (`group_name`,`create_time`,`update_time`,`create_by`,`update_by`)
    SELECT  '{$group_name}','{$data["create_time"]}','{$data["update_time"]}','{$data["create_by"]}','{$data["update_by"]}'
    WHERE not exists (select group_name from crm_promote_consignee_group
    where group_name = '{$group_name}')";

        $rs = $this->db->query($sql);
        return $this->db->insert_id();

    }

}