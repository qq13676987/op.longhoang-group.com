    <script type="text/javascript">
		var url;
        function doSearch(){
            $('#tt').datagrid('load',{
                id:$('#id').val(),
                name:$('#name').val(),
            });
			$('#chaxun').window('close');
        }
        $(function(){
			var selectIndex = -1;
            $('#tt').edatagrid({
                url: '/bsc_system_pass/get_data',
                destroyUrl: '/bsc_system_pass/delete_data'
            });
			$('#tt').datagrid({
				width:'auto',
				height: $(window).height(),
				onDblClickRow: function(rowIndex) {
					$(this).datagrid('selectRow', rowIndex); 
					var row = $('#tt').datagrid('getSelected');
					if (row){
						$('#dlg').dialog('open').dialog('center').dialog('setTitle','<?= lang('Credit extension setting');?>');
						$('#fm').form('load',row);
						url = '/bsc_system_pass/update_data/'+row.id;
					}
				},
				onClickRow: function(index, data) {
					if (index == selectIndex) { 
						$(this).datagrid('unselectRow', index); 
						selectIndex = -1;
					}else{
						selectIndex = index;
					}   
				},
				// colorfull rows
				rowStyler:function(index,row){
                    if (row.permit == '1'){
                        return 'color:blue;';
                    }
                    if (row.permit == '0'){
                        return 'color:red;';
                    }                                                                                                           
                }
			}); 
			$(window).resize(function () {
				$('#tt').datagrid('resize');
			});
				
        });
        function add(){  
            $('#dlg').dialog('open').dialog('center').dialog('setTitle','<?= lang('Asdd');?>');
            $('#fm').form('clear');
            url = '/bsc_system_pass/add_data/';
		}
		function del(){
			var row = $('#tt').datagrid('getSelected'); 
			if (row){ 
				if(row.pass=="1"){
					alert("<?= lang('It can\'t be deleted');?>");
				}else{
					$('#tt').edatagrid('destroyRow');
				}
				
			}else{
				alert("<?= lang('Please select a line first');?>");
			}
		}
        function save(){
            $('#fm').form('submit',{
                url: url,
                onSubmit: function(){ 
					return $(this).form('validate');
                },
                success: function(result){ 
                    $('#dlg').dialog('close');   
                    $('#tt').datagrid('reload');    
                }
            });
        }
    </script>
    
<table id="tt"
            rownumbers="false" pagination="true"  idField="id"
            pagesize="30" toolbar="#tb"  singleSelect="true" nowrap="false">
        <thead>
            <tr>
				<?php 
					if(!empty($f)){  
						foreach ($f as $rs){
							
							$field = $rs[0];
							$disp = $rs[1];
							$width = $rs[2];
							
							if ($width =="0") continue;
								
							if($field == "id" || $field == "created_time" ){
								echo "<th field=\"$field\" width=\"$width\" sortable=\"true\">" . lang($disp) . "</th>";
								continue;
							}
							echo "<th field=\"$field\" width=\"$width\">" . lang($disp) . "</th>";
						}
					}
				?>
            </tr>
        </thead>
</table>

   <div id="tb" style="padding:3px;">
        <table>
		<tr>
        <td> 
			<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="del();"><?= lang('delete');?></a>
			<a href="#" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('search');?></a>
        </td> 
        </tr>
		</table>
         
    </div> 
	<div id="chaxun" class="easyui-window" title="<?= lang('query criteria');?>" closed="true" style="width:350px;height:380px;padding:5px;">
		 <table>
		<tr> 
		<td align="right">编号:&nbsp; </td>
		<td>
			<input id="id" class="easyui-textbox" style="width:200px;height:30px;">
		</td>			
		</tr>
		<tr> 
		<td align="right">名字:&nbsp; </td>
		<td>
			<input id="name" class="easyui-textbox" style="width:200px;height:30px;">
		</td>			
		</tr>
		</table>  	
			
		<br><br>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;"><?= lang('search');?></a>
	</div>
	
	<div id="dlg" class="easyui-dialog" style="width:400px" data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons'">
        <form id="fm" method="post" novalidate style="margin:0;padding:20px 50px">
            <h3> </h3>
            <table>
                <tr>
                    <td><?= lang('name');?>:</td>
                    <td>
                        <input name="name" class="easyui-textbox" required="true" style="width:200px;">
                    </td>
                </tr>
                <tr>
                    <td><?= lang('telephone');?>:</td>
                    <td>
                        <input name="telephone" class="easyui-textbox" required="true" style="width:200px;">
                    </td>
                </tr>
                <tr>
                    <td><?= lang('status');?>:</td>
                    <td>
                        <input name="status" class="easyui-numberspinner" required="true" data-options="min:0,max:2"  style="width:200px;">
                        <a title="0是申请状态,1是通过,2是拒绝">help</a>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('remark');?>:</td>
                    <td>
                        <textarea name="remark"></textarea>
                    </td>
                </tr>
            </table>
        </form>
    </div>
	
    <div id="dlg-buttons">
        <a href="javascript:save()" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px"><?= lang('save');?></a>
        <a href="javascript:$('#dlg').dialog('close')" class="easyui-linkbutton" iconCls="icon-cancel" style="width:90px"><?= lang('cancel');?></a>
    </div>
	