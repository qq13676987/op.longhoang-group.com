<?php
class Biz_tag_model extends CI_Model{
    private $table = 'biz_tag';

    public $field_all = array(
        array("id", "id", "94", "2", '1'),
        array("created_by", "created_by", "94", "2", '1'),
        array("created_time", "created_time", "94", "2", '1'),
        array("updated_by", "updated_by", "94", "2", '1'),
        array("updated_time", "updated_time", "94", "2", '1'),
        array("tag_name", "tag_name", "94", "2", '1'),
        array("tag_class", "tag_class", "94", "2", '1'),
        array("id_type", "id_type", "94", "2", '1'),
        array("pass", "pass", "94", "2", '1'),
    );

    public function __construct()
    {
        $this->load->database();
    }

    public function get_one($f="",$v=""){
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }
    public function get_where_one($where = ''){
        if($where !== '') $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }
    public function get($where="" ,$sort="id",$order="desc"){
        if($where !="")	$this->db->where($where);
        $this->db->order_by($sort,$order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function no_role_get($where="" ,$sort="id",$order="desc"){
        if($where !="")	$this->db->where($where);
        $this->db->order_by($sort,$order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
    public function total($where=""){
        if($where !="")	$this->db->where($where);
        $this->db->from($this->table);
        return  $this->db->count_all_results('');
    }

    public function save($data=array()){
        $data["created_by"] = get_session('id');
        $data["created_time"] = date('Y-m-d H:i:s');
        $data["updated_by"] = get_session('id');
        $data["updated_time"] = date('Y-m-d H:i:s');
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id='',$data=array()){
        $data["updated_by"] = get_session('id');
        $data["updated_time"] = date('Y-m-d H:i:s');
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id=''){
        // delete container
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

}