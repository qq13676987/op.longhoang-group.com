<!doctype html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="/inc/css/normal.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/icon.css?v=3">
<link href="/inc/third/layui/css/layui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.easyui.min.js?v=11"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.edatagrid.js?v=5"></script>
<script type="text/javascript" src="/inc/js/easyui/datagrid-detailview.js"></script>
<script type="text/javascript" src="/inc/js/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/inc/js/other.js?v=1"></script>
<script type="text/javascript" src="/inc/js/set_search.js?v=5"></script>
<script type="text/javascript" src="/inc/js/easyui/jquery.easyui.ext.js?v=53"></script>
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<link rel="stylesheet" href="/inc/third/handsontable/handsontable.full.min.css"/>
<script type="text/javascript" src="/inc/third/handsontable/handsontable.full.min.js"></script>
<title><?= lang('CRM');?></title>
<style type="text/css">
    .f {
        width: 115px;
    }

    .s {
        width: 100px;
    }

    .v {
        width: 200px;
    }

    .del_tr {
        width: 23.8px;
    }

    .this_v {
        display: inline;
    }

    .input {
        width: 200px;
    }

    .select {
        width: 210px;
    }

    .textarea {
        width: 200px;
        height: 60px;
    }

    .query_f {
        padding-left: 15px;
        color: #1E9FFF;
    }

    .query_s {
        padding-left: 5px;
        color: #5FB878;
    }

    .query_v {
        padding-left: 5px;
        color: #FF5722;
    }

    /**
    * 修改datagrid表格样式
    **/
    .datagrid-body td {height: 40px !important;}
    .datagrid-header, .datagrid-htable {height: 40px !important; background: #eceff0; border-bottom: none;}
    .datagrid-header-row td{border-width: 0px 1px 1px 0px; border-style: solid; border-color: #ddd;}
    .panel-body-noheader {border-top:0px !important;}
    .tabs {padding-left:0px;}
    .tabs-inner {height: 25px !important;}
    .tabs li.tabs-selected a.tabs-inner {border-bottom: 1px solid #F4F4F4;}
    .tabs-header {background: none;}
    .tabs li a.tabs-inner{padding: 0px 20px;}
    .tabs li.tabs-selected a.tabs-inner{background: #F4F4F4}
    .datagrid-row-over {background: #eceeeb;}
    .datagrid-header td.datagrid-header-over {background:#e5e7e8;color: #000000;cursor: default;}
    .layui-layer-lan .layui-layer-title {background: #325763 !important;}
    #cx table tr td {height: 30px; min-width: 50px;}
    #cx table tr td button {width: 23px;}
    .tips_num {position: absolute; color: white; background: red; top:0px; right:-2px; border-radius: 10px; font-size: 12px; padding: 2px;}
</style>
<!--这里是查询框相关的代码-->
<script>
    var query_option = {};//这个是存储已加载的数据使用的

    var table_name = 'biz_client_crm',//需要加载的表名称
        view_name = 'biz_client_crm_index';//视图名称
    var add_tr_str = "";//这个是用于存储 点击+号新增查询框的值, load_query_box会根据接口进行加载
    var ids_str = '';

    /**
     * 加载查询框
     */
    function load_query_box(){
        ajaxLoading();
        $.ajax({
            type:'GET',
            url: '/sys_config_title/get_user_query_box?view_name=' + view_name + '&table_name=' + table_name,
            dataType:'json',
            success:function (res) {
                ajaxLoadEnd();
                if(res.code == 0){
                    //加载查询框
                    $('#cx table').append(res.data.box_html);
                    //需要调用下加载器才行
                    $.parser.parse($('#cx table tbody .query_box'));

                    add_tr_str = res.data.add_tr_str;
                    query_option = res.data.query_option;
                    var table_columns = [[
                        {field:'ck', checkbox:true},
                        {field:'score', title:'<?= lang('完整度(%)');?>', width:70, align:'center', formatter: get_function('score_for'), styler: get_function('score_styler')},
                        {field:'change', title:'<?= lang('流转日志');?>', width:80, align:'center', formatter: get_function('change_log_for')},
                        //{field:'progress', title:'<?= lang('跟踪进度');?>', width:80, align:'center', formatter: get_function('progress_for')},
                    ]];//表的表头

                    //填充表头信息
                    $.each(res.data.table_columns, function (i, it) {
                        //读取列配置,然后这里进行合并
                        var column_config;
                        try {
                            column_config = JSON.parse(it.column_config,function(k,v){
                                if(v && typeof v === 'string') {
                                    return v.indexOf('function') > -1 || v.indexOf('FUNCTION') > -1 ? new Function(`return ${v}`)() : v;
                                }
                                return v;
                            });
                        }catch (e) {
                            column_config = {};
                        }
                        var this_column_config = {field:it.table_field, title:it.title, width:it.width, sortable: it.sortable, formatter: get_function(it.table_field + '_for'), styler: get_function(it.table_field + '_styler')};
                        //合并一下
                        $.extend(this_column_config, column_config);

                        table_columns[0].push(this_column_config);
                    });

                    //渲染表头
                    $('#tt').datagrid({
                        columns:table_columns,
                        width: 'auto',
                        height: $(window).height() - 55,
                        onDblClickRow: function (index, row) {
                            url = '/biz_crm/edit/' + row.id;
                            window.open(url);
                        },
                        rowStyler: function (index, row) {
                            var apply_status = row.apply_status;
                            if (apply_status == -1) {
                                return 'color: red;';
                            } else if (apply_status == 0) {
                                return 'color: green';
                            } else if (apply_status == 1) {
                                return 'color: #666;';
                            }
                        }
                    });
                    $(window).resize(function () {
                        $('#tt').datagrid('resize');
                    });

                    //为了避免初始加载时, 重复加载的问题,这里进行了初始加载ajax数据
                    var aj = [];
                    $.each(res.data.load_url, function (i,it) {
                        aj.push(get_ajax_data(it, i));
                    });
                    //加载完毕触发下面的下拉框渲染
                    //$_GET 在other.js里封装
                    var this_url_get = {};
                    $.each($_GET, function (i, it) {
                        this_url_get[i] = it;
                    });
                    var auto_click = this_url_get.auto_click;

                    $.when.apply($,aj).done(function () {
                        //这里进行字段对应输入框的变动
                        $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                            var ec_value = $(ec_item).combobox('getValue');
                            var v_inp = $('.v:eq(' + ec_index + ')');
                            var s_val = $('.s:eq(' + ec_index + ')').combobox('getValue');
                            var v_val = v_inp.textbox('getValue');
                            //如果自动查看,初始值为空
                            if(auto_click === '1') v_val = '';

                            $(ec_item).combobox('clear').combobox('select', ec_value);
                            //这里比对get数组里的值
                            $.each(this_url_get, function (trg_index, trg_item) {
                                //切割后,第一个是字段,第二个是符号
                                var trg_index_arr = trg_index.split('-');
                                //没有第二个参数时,默认用等于
                                if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';
                                //用一个删一个
                                //如果当前的选择框的值,等于get的字段值
                                if(ec_value === trg_index_arr[0] && s_val === trg_index_arr[1]){
                                    v_val = trg_item;//将v改为当前get的
                                    delete this_url_get[trg_index];
                                }
                            });
                            //没找到就正常处理
                            $('.v:eq(' + ec_index + ')').textbox('setValue', v_val);
                        });
                        //判断是否有自动查询参数
                        var no_query_get = ['auto_click','token'];//这里存不需要塞入查询的一些特殊变量
                        $.each(no_query_get, function (i,it) {
                            delete this_url_get[it];
                        });
                        //完全没找到的剩余值,在这里新增一个框进行选择
                        $.each(this_url_get, function (trg_index, trg_item) {
                            add_tr();//新增一个查询框
                            var trg_index_arr = trg_index.split('-');
                            //没有第二个参数时,默认用等于
                            if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';

                            $('#cx table tbody tr:last-child .f').combobox('setValue', trg_index_arr[0]);
                            $('#cx table tbody tr:last-child .s').combobox('setValue', trg_index_arr[1]);
                            $('#cx table tbody tr:last-child .v').textbox('setValue', trg_item);
                        });

                        //这里自动执行查询,显示明细
                        if(auto_click === '1'){
                            doSearch();
                        }
                    });
                }else{
                    $.messager.alert("<?= lang('提示');?>", res.msg);
                }
            },error:function (e) {
                ajaxLoadEnd();
                $.messager.alert("<?= lang('提示');?>", e.responseText);
            }
        });
    }

    /**
     * 新增一个查询框
     */
    function add_tr() {
        $('#cx table tbody').append(add_tr_str);
        $.parser.parse($('#cx table tbody tr:last-child'));
        var last_f = $('#cx table tbody tr:last-child .f');
        var last_f_v = last_f.combobox('getValue');
        last_f.combobox('clear').combobox('select', last_f_v);
        return false;
    }

    /**
     * 删除一个查询
     */
    function del_tr(e) {
        //删除按钮
        var index = $(e).parents('tr').index();
        $(e).parents('tr').remove();
        return false;
    }

    //执行加载函数
    $(function () {
        load_query_box();
    });
</script>
<script>
    function setValue(jq, val) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combobox !== undefined){
            return $(jq).combobox('setValue',val);
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('setValue',val);
        }else if(data.datebox !== undefined){
            return $(jq).datebox('setValue',val);
        }else if(data.textbox !== undefined){
            return $(jq).textbox('setValue',val);
        }
    }

    function getValue(jq) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if (data.combobox !== undefined) {
            return $(jq).combobox('getValue');
        } else if (data.datetimebox !== undefined) {
            return $(jq).datetimebox('getValue');
        } else if (data.datebox !== undefined) {
            return $(jq).datebox('getValue');
        } else if (data.textbox !== undefined) {
            return $(jq).textbox('getValue');
        }
    }

    //模板相关
    function change_log_for(value, row, index) {
        var str = '<a href="javascript:void(0);" style="color: #088fff;" onclick="view_log(' + row.id + ')"><?= lang('查看流转');?></a>';
        return str;
    }

    //模板相关
    function clue_num_for(value, row, index) {
        if (value == 0){
            return "<?= lang('暂无线索');?>";
        }else {
            var str = '<a href="javascript:void(0);" style="color: #088fff;" onclick="view_clue(\'' + row.company_search + '\')">(' + value + '<?= lang('条');?>)</a>';
            return str;
        }
    }

    function log_time_for(value, row, index) {
        if (value == null || value == '0000-00-00 00:00:00'){
            return '<a href="javascript:void(0);" style="color: #088fff;" onclick="view_develop(' + row.id + ')"><?= lang('暂无日志');?></div>';
        }else {
            var str = '<a href="javascript:void(0);" style="color: #088fff;" onclick="view_develop(' + row.id + ')">';
            str +='<?= lang('查看日志');?><br>';
            str += '<font color=#999>' + value + '</font></a>';
            return str;
        }
    }

    function progress_for(value, row, index) {
        $color = 'layui-bg-green';
        if (parseInt(value) > 99) $color = 'layui-bg-red';
        var str = '<div class="layui-progress" lay-showPercent="true" lay-showPercent="yes">';
        str += '<div class="layui-progress-bar '+$color+'" style="width: '+value+'%" lay-percent="1/3"></div>';
        str += '</div>';
        return str;
    }

    function company_name_for(value, row){
        var str = value;
        if(row.is_new == 1){
            str = '<span style="color:red;">[&nbsp;New&nbsp;]&nbsp;&nbsp;</span>' + str;
        }
        return str;
    }
    //模板相关--end

    function view_clue(company_search) {
        layer.open({
            type: 2,
            title: '<?= lang('查看线索');?>',
            area: ['1300px','600px'],
            shade: 0.5,
            offset:'100px',
            shadeClose:true,
            content: '/biz_crm/clue_detail/?company_search='+company_search,
            success: function(layero, index) {
                //找到当前弹出层的iframe元素
                var iframe = $(layero).find('iframe');
                var childPageHeight = iframe[0].contentDocument.body.offsetHeight;
                if (childPageHeight > 600){
                    iframe.css('height', 600);
                }else{
                    //对加载后的iframe进行宽高度自适应
                    layer.iframeAuto(index);
                }
            }
        });
    }
</script>
<script type="text/javascript">
    var ids = []
    //selectAll和这个函数差不多,改的时候记得一起改
    function doSearch(url = '') {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if (json.hasOwnProperty(item.name) === true) {
                if (typeof json[item.name] == 'string') {
                    json[item.name] = [json[item.name]];
                    json[item.name].push(item.value);
                } else {
                    json[item.name].push(item.value);
                }
            } else {
                json[item.name] = item.value;
            }
        });

        //清空显示的查询条件
        $('#query').html('');


        var cx_data2 = cx_data
        $.each(cx_data2, function (index, item) {
            if (item.value == 'client.company_name') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('客户全称');?>'};
            }
            if (item.value == 'client.client_name') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('客户简称');?>'};
            }
            if (item.value == 'client.country_code') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('国家代码');?>'};
            }
            if (item.value == 'client.city') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('市');?>'};
            }
            if (item.value == 'contact.email') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('email');?>'};
            }
            if (item.value == 'contact.telephone') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('电话');?>'};
            }
            if (item.value == 'contact.live_chat') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('qq');?>'};
            }
            if (item.value == 'client.company_address') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('客户地址');?>'};
            }
            if (item.value == 'crm.export_sailing') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('出口航线');?>'};
            }
            if (item.value == 'crm.trans_mode') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('运输方式');?>'};
            }
            if (item.value == 'crm.product_type') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('品名大类');?>'};
            }
            if (item.value == 'crm.product_details') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('品名大类描述');?>'};
            }
            if (item.value == 'crm.sales_id') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('当前销售');?>'};
            }
            if (item.value == 'crm.client_source') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('客户来源');?>'};
            }
            if (item.value == 'crm.created_by') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('创建人');?>'};
            }
            if (item.value == 'crm.close_plan_date') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('结束时间');?>'};
            }
            if (item.value == 'crm.created_time') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('创建时间');?>'};
            }
            if (item.value == 'crm.updated_time') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('更新时间');?>'};
            }
            if (item.value == 'crm.apply_remark') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('审核备注');?>'};
            }
            if (item.name == 'field[v][]') {
                if (item.value == '') {
                    cx_data2[index] = {name: 'field[v][]', value: '<?= lang('无查询条件');?>'};
                }
            }
        });
        $.each(cx_data2, function (index, item) {
            if (item.name == 'f_role') {
                if (item.value != '') {
                    $("#query").append('<span class="query_f">' + item.value + '</span>')
                }
            }
            //判断如果name存在,且为string类型
            if (item.name == 'field[name][]') {
                $("#query").append('<span class="query_f">' + item.value + '</span>')
            }
            if (item.name == 'field[s][]') {
                $("#query").append('<span class="query_s">' + item.value + '</span>')
            }
            if (item.name == 'field[v][]') {
                $("#query").append('<span class="query_v">' + item.value + '</span>')
            }

        });

        var opt = $('#tt').datagrid('options');
        if (url == '') {
            opt.url = '/biz_crm/get_apply_data?apply_status=<?= $apply_status;?>';
        } else {
            opt.url = url;
        }

        <?php if(!empty($import)):?>
        opt.url = '/biz_crm/get_apply_data?apply_status=2';
        <?php endif;?>


        $('#tt').datagrid('load', json).datagrid('clearSelections');
        set_config();
        $('#select_ids').val('');
        $('#chaxun').window('close');
    }

    function selectAll() {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if (json.hasOwnProperty(item.name) === true) {
                if (typeof json[item.name] == 'string') {
                    json[item.name] = [json[item.name]];
                    json[item.name].push(item.value);
                } else {
                    json[item.name].push(item.value);
                }
            } else {
                json[item.name] = item.value;
            }
        });

        //清空显示的查询条件
        $('#query').html('');


        var cx_data2 = cx_data;
        $.each(cx_data2, function (index, item) {
            if (item.value == 'client.company_name') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('客户全称');?>'};
            }
            if (item.value == 'client.client_name') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('客户简称');?>'};
            }
            if (item.value == 'client.country_code') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('国家代码');?>'};
            }
            if (item.value == 'client.city') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('市');?>'};
            }
            if (item.value == 'contact.email') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('email');?>'};
            }
            if (item.value == 'contact.telephone') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('电话');?>'};
            }
            if (item.value == 'contact.live_chat') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('qq');?>'};
            }
            if (item.value == 'client.company_address') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('客户地址');?>'};
            }
            if (item.value == 'crm.export_sailing') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('出口航线');?>'};
            }
            if (item.value == 'crm.trans_mode') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('运输方式');?>'};
            }
            if (item.value == 'crm.product_type') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('品名大类');?>'};
            }
            if (item.value == 'crm.product_details') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('品名大类描述');?>'};
            }
            if (item.value == 'crm.sales_id') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('当前销售');?>'};
            }
            if (item.value == 'crm.client_source') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('客户来源');?>'};
            }
            if (item.value == 'crm.created_by') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('创建人');?>'};
            }
            if (item.value == 'crm.close_plan_date') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('结束时间');?>'};
            }
            if (item.value == 'crm.created_time') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('创建时间');?>'};
            }
            if (item.value == 'crm.updated_time') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('更新时间');?>'};
            }
            if (item.value == 'crm.apply_remark') {
                cx_data2[index++] = {name: 'field[name][]', value: '<?= lang('审核备注');?>'};
            }
            if (item.name == 'field[v][]') {
                if (item.value == '') {
                    cx_data2[index] = {name: 'field[v][]', value: '<?= lang('无查询条件');?>'};
                }
            }
        });
        $.each(cx_data2, function (index, item) {
            if (item.name == 'f_role') {
                if (item.value != '') {
                    $("#query").append('<span class="query_f">' + item.value + '</span>')
                }
            }
            //判断如果name存在,且为string类型
            if (item.name == 'field[name][]') {
                $("#query").append('<span class="query_f">' + item.value + '</span>')
            }
            if (item.name == 'field[s][]') {
                $("#query").append('<span class="query_s">' + item.value + '</span>')
            }
            if (item.name == 'field[v][]') {
                $("#query").append('<span class="query_v">' + item.value + '</span>')
            }

        });

        json.getType = 1;
        var opt = $('#tt').datagrid('options');
        $('#select_ids').val('');

        if(!opt.url){
            return $.messager.alert('<?= lang('Tips')?>', '<?= lang('请查询后再试');?>');
        }
        ajaxLoading();

        var title = [];
        $.each(cx_data, function (index, item) {
            if (item.name == 'crm_user') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
            if (item.name == 'crm_user_type') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
            if (item.name == 'f_role') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
            if (item.name == 'field[v][]') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
        });

        $.ajax({
            url: opt.url,
            type:'post',
            data: json,
            success:function (res_json) {
                var res;
                ajaxLoadEnd();
                try {
                    res = $.parseJSON(res_json);
                    if (res.code == 0) {
                        $('#select_ids').val(res.data.ids);

                        $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认不勾全导入?');?>', function (r) {
                            if(r){
                                var select_ids = $('#select_ids').val();
                                var ids = select_ids.split(',');

                                if (ids.length < 1) {
                                    alert('<?= lang('未获取到可选择的客户');?>');
                                }
                                ids = ids.join(',');

                                post('/crm_promote_mail/add_presend_by_search_send_crm', {ids: ids, title: title});
                            }
                        })
                    } else {

                    }
                } catch (e) {
                    $.messager.alert('Tips', res_json);
                }
            }
        });
    }

    function resetSearch(){
        $.each($('.v'), function(i,it){
            setValue($(it), '');
        });
    }


    var cx_data = new Array();
    $(function () {
        //加入黑名单
        $('#add_black_list').click(function () {
            var ids = {};
            var rows = $('#tt').datagrid('getSelections');
            if (rows.length < 1){
                layer.alert('<?= lang('请选择黑名单对象！');?>',{icon:5});
                return;
            }

            $.each(rows, function (index, item) {
                ids[index] = item.id;
            })

            layer.prompt({title: '<?= lang('请输入拉黑原因！');?>', formType: 2, btn:['<?= lang('确定拉黑');?>', '<?= lang('取消');?>']}, function(black_list_cause, index){
                var loading = layer.load(1);
                $.ajax({
                    url: "/biz_crm/add_black_list",
                    type: "POST",
                    data: {ids: ids, black_list_cause:black_list_cause},
                    dataType: "json",
                    success: function (data) {
                        layer.close(loading);
                        layer.close(index);
                        if (data.code == 1) {
                            $('#tt').datagrid('reload');
                            layer.msg('<?= lang('Success.');?>', {icon:1})
                        } else {
                            layer.alert('data.msg', {icon: 5});
                        }
                    },
                    error: function (data) {
                        layer.close(loading);
                        layer.msg('<?= lang('请求失败，错误代码');?>：' + data.status);
                    }
                });
            });
        });

        $('#cx').on('keydown', 'input', function (e) {
            if (e.keyCode == 13) {
                doSearch();
            }
        });

        $('#export_crm').click(function(){
            layer.open({
                type: 2,
                title:'<?= lang('导出CRM客户');?>',
                area: ['93%', '800px'],
                shade:0.3,
                content:'/biz_crm/export_crm/',
                maxmin:true,
                scrollbar:false
            });
        });

        $('#import_crm').click(function(){
            layer.open({
                type: 2,
                title:'<?= lang('导入CRM客户');?>',
                area: ['93%', '800px'],
                shade:0.3,
                content:'/biz_crm/import_crm/',
                maxmin:true,
                scrollbar:false
            });
        });

        //添加汽泡提醒,轮询获取待分配数量,这个版本不需要了
        if ($('#tabs').tabs('exists','<?= lang('待分配');?>') == true) {
            var _this = $('#tabs').find('li').eq(7);
            var html = '<div class="tips_num" id="unassigned_num">0</div>';
            _this.addClass('unassigned').append(html);

            (function poll() {
                $.ajax({
                    url: "/biz_crm/get_unassigned_num",
                    type: "GET",
                    success: function (data) {
                        if (data.code == 200) {
                            $('#unassigned_num').text(data.unassigned_num);
                        }
                    },
                    dataType: "json",
                    complete: setTimeout(function() {poll()}, 10000),
                    timeout: 2000
                })
            })();
        }

        $('#tabs').tabs({
            border: false,
            onSelect: function (title) {
                $('#add_black_list').show();
                $('#batch_deliver').hide();
                $('#new').hide();
                $('#upLevel').hide();
                if (title == '<?= lang('全部');?>') {
                    $('#title_tabs').val();
                    var url = '/biz_crm/get_apply_data';
                    doSearch(url);
                }
                if (title == '<?= lang('线索');?>') {
                    $('#tabs').tabs('select', 0);
                    layer.open({
                        type: 2,
                        title: '<?= lang('线索');?>',
                        area: ['1590px', '90%'],
                        skin: 'layui-layer-lan',
                        shade: 0.5,
                        content: '/biz_crm/clue'
                    });
                }
                if (title == '<?= lang('线索2');?>') {
                    $('#tabs').tabs('select', 0);
                    layer.open({
                        type: 2,
                        title: '<?= lang('线索2');?>',
                        area: ['1590px', '90%'],
                        skin: 'layui-layer-lan',
                        shade: 0.5,
                        content: '/biz_shipment/global_shipment_crm'
                    });
                }
                <?php if (get_session('id') != 20057):?>
                if (title == '<?= lang('待申请');?>') {
                    $('#add').show();
                    $('#batch_deliver').hide();
                    $('#title_tabs').val(1);
                    var url = '/biz_crm/get_apply_data?apply_status=1&workflow=0';
                    doSearch(url);
                }
                <?php endif;?>
                if (title == '<?= lang('申请中');?>') {
                    $('#new').show();
                    $('#add').show();
                    $('#batch_deliver').show();
                    $('#title_tabs').val(1);
                    var url = '/biz_crm/get_apply_data?apply_status=1&workflow=1';
                    doSearch(url);
                }
                if (title == '<?= lang('已通过');?>') {
                    $('#batch_deliver').show();
                    $('#title_tabs').val(2);
                    var url = '/biz_crm/get_apply_data?apply_status=2';
                    doSearch(url);
                }
                if (title == '<?= lang('已拒绝');?>') {
                    $('#batch_deliver').show();
                    $('#title_tabs').val(-1);
                    var url = '/biz_crm/get_apply_data?apply_status=-1';
                    doSearch(url);
                }
                if (title == '<?= lang('转正申请中');?>') {
                    $('#add_black_list').hide();
                    $('#upLevel').show();
                    $('#title_tabs').val(3);
                    var url = '/biz_crm/get_apply_data?apply_status=3';
                    doSearch(url);
                }
                if (title == '<?= lang('转正通过');?>') {
                    $('#add_black_list').hide();
                    $('#title_tabs').val(0);
                    var url = '/biz_crm/get_apply_data?apply_status=0';
                    doSearch(url);
                }
                if (title == '<?= lang('公海');?>') {
                    //window.open('/biz_crm/public_client');
                    $('#tabs').tabs('select',0);
                    layer.open({
                        type: 2,
                        title:'<?= lang('公海客户');?>',
                        skin: 'layui-layer-lan',
                        area: ['95%','90%'],
                        shade:0.5,
                        content:'/biz_crm/public_client'
                    });
                }
                if (title == '<?= lang('CRM黑名单');?>') {
                    $('#tabs').tabs('select',0);
                    layer.open({
                        type: 2,
                        title:'<?= lang('CRM黑名单');?>',
                        skin: 'layui-layer-lan',
                        area: ['1620px','90%'],
                        shade:0.5,
                        content:'/biz_crm/black_list'
                    });
                }
                if (title == '<?= lang('无效邮箱');?>') {
                    window.open('/biz_crm_email/index')
                }


            }
        });
    });

    function userLoadSuccess(node, data) {
        //这里进行循环操作,当找到第一个is_use == true时,选中
        var val = $('#crm_user').val();
        if (val == '') {
            var result = searchIsuse(data);
            $('#user').combotree('setValue', result);
            $('#db_fm').submit();
        }
    }

    /**
     * 查找到最大的第一个可使用的
     * @param data
     */
    function searchIsuse(data) {
        var result = '';
        //循环一遍,查看第一层结构是否有可使用的
        var new_data = [];
        $.each(data, function (i, it) {
            if (it.is_use == true) {
                result = it.id;
                return false;
            }
            new_data.push(it);
        });
        if (result != '') return result;
        //如果没找到,这里循环再递归一下
        $.each(new_data, function (i, it) {
            result = searchIsuse(it.children);
            if (result != '') return result;
        })

        return result;
    }

    var is_submit = false;

    function add() {
        layer.open({
            type: 2,
            title:'<?= lang('添加客户');?>',
            skin: 'layui-layer-lan',
            area: ['1215px', '665px'],
            shade:0.3,
            content:'/biz_crm/add',
            maxHeight:'200',
            success: function(layero, index) {
                //找到当前弹出层的iframe元素
                var iframe = $(layero).find('iframe');
                var childPageHeight = iframe[0].contentDocument.body.offsetHeight;

                if (childPageHeight > 700){
                    iframe.css('height', 700);
                }else{
                    //对加载后的iframe进行宽高度自适应
                    layer.iframeAuto(index);
                }
            }
        });
        //window.open('/biz_client_apply/add?apply_type=1');
    }

    //批量转正审批
    function apply_client() {
        //清空历史
        ids = ids.splice(0, 0);
        var rows = $('#tt').datagrid('getSelections');
        if (rows.length < 1){
            layer.alert('<?= lang('未选择审批对象！');?>',{icon:5});
            return;
        }else{
            $.each(rows, function (index, item) {
                //加入审批
                ids.push(item.id);
            });
        }


        //如果有审批的事务
        if (ids.length > 0){
            var url = '/biz_workflow/batch_approval_page/?id_type=2';
            var index = layer.open({
                type: 2,
                title:'<?= lang('批量审核CRM客户转正');?>',
                area: '600px',
                offset: '200px',
                shade:0.5,
                content:url,
                maxHeight:'200',
                btn: ['<?= lang('保存');?>', '<?= lang('取消');?>'],
                success: function(layero, index) {
                    //找到当前弹出层的iframe元素
                    var iframe = $(layero).find('iframe');
                    var childPageHeight = iframe[0].contentDocument.body.offsetHeight;

                    if (childPageHeight > 600){
                        iframe.css('height', 600);
                    }else{
                        //对加载后的iframe进行宽高度自适应
                        layer.iframeAuto(index);
                    }
                },
                yes: function (e, layero) {
                    var l = window["layui-layer-iframe" + e]
                        , r = "LAY-module-submit"
                        , n = layero.find("iframe").contents().find("#LAY-module-submit");

                    //提交表单
                    l.layui.form.on("submit(" + r + ")", function (f) {
                        var field = f.field;
                        field.ids = ids;
                        field.type = 2;
                        $.ajax({
                            type: 'post',
                            url: '/biz_workflow/batch_approval',
                            data: field,
                            success: function (data) {
                                $('#tt').datagrid('reload');
                                $('#tt').datagrid('unselectAll');
                                ids = [];
                                if (data.code == 1) {
                                    layer.msg('审批成功', {icon: 1, shade: [0.3, '#393D49']},function () {
                                        layer.close(index);//关闭窗口
                                    });
                                } else {
                                    layer.closeAll();//关闭窗口
                                    var index3 = layer.alert(data.msg + '<br><?= lang('点击"确定按钮"查看失败原因！');?>', {icon: 5,offset: '200px',}, function () {
                                        layer.close(index3);
                                        layer.open({
                                            type: 1,
                                            title:'<?= lang('审批结果');?>',
                                            area: ['750px','500px'],
                                            //shade:0.5,
                                            offset: '200px',
                                            content:$('#_window'),
                                            success: function(layero, e){
                                                layer.setTop(layero);
                                                var table_data = data.data;
                                                var columns = [
                                                    {data: 'id',type: 'text',className: "htCenter",width: 100},
                                                    {data: 'title',type: 'text',className: "htLeft",width: 250},
                                                    {data: 'status',type: 'text',className: "htCenter",width: 100,},
                                                    {data: 'msg',type: 'text',className: "htLeft",width: 250,},
                                                ];
                                                var colHeaders = ['<?= lang('id');?>', '<?= lang('备注');?>', '<?= lang('状态');?>', '<?= lang('失败说明');?>'];

                                                $("#handsontable_box").handsontable({
                                                    columns: columns, //列配置参数
                                                    data: table_data,//为需要绑定数据集合
                                                    licenseKey: 'non-commercial-and-evaluation',
                                                    colHeaders: colHeaders, // 表头
                                                    rowHeaders: true,
                                                    dropdownMenu: true, //忘了
                                                    fillHandle: true,
                                                });
                                            }
                                        });
                                    });
                                    submit = false;
                                }
                            },
                            error: function (data) {
                                layer.msg('<?= lang('请求失败，错误代码：');?>' + data.status);
                            },
                            dataType: "json"
                        });
                    });
                    n.trigger("click");
                }
            });
        }
    }

    /**
     * 审核CRM客户
     */
    function apply_crm() {
        //清空历史
        ids = ids.splice(0, 0);
        var rows = $('#tt').datagrid('getSelections');
        if (rows.length < 1){
            layer.alert('<?= lang('未选择审批对象！');?>',{icon:5});
            return;
        }else{
            $.each(rows, function (index, item) {
                //加入审批
                ids.push(item.id);
            });
        }

        //如果有审批的事务
        if (ids.length > 0){
            var url = '/biz_workflow/batch_approval_page/?id_type=1';
            var index = layer.open({
                type: 2,
                title:'<?= lang('批量审核CRM新客户');?>',
                area: '600px',
                offset: '200px',
                shade:0.5,
                content:url,
                maxHeight:'200',
                btn: ['<?= lang('保存');?>', '<?= lang('取消');?>'],
                success: function(layero, index) {
                    //找到当前弹出层的iframe元素
                    var iframe = $(layero).find('iframe');
                    var childPageHeight = iframe[0].contentDocument.body.offsetHeight;

                    if (childPageHeight > 600){
                        iframe.css('height', 600);
                    }else{
                        //对加载后的iframe进行宽高度自适应
                        layer.iframeAuto(index);
                    }
                },
                yes: function (e, layero) {
                    var l = window["layui-layer-iframe" + e]
                        , r = "LAY-module-submit"
                        , n = layero.find("iframe").contents().find("#LAY-module-submit");

                    //提交表单
                    l.layui.form.on("submit(" + r + ")", function (f) {
                        var loading = layer.load(2);
                        var field = f.field;
                        field.ids = ids;
                        field.type = 1;
                        $.ajax({
                            type: 'post',
                            url: '/biz_workflow/batch_approval',
                            data: field,
                            success: function (data) {
                                layer.close(loading);
                                $('#tt').datagrid('reload');
                                $('#tt').datagrid('unselectAll');
                                ids = [];
                                if (data.code == 1) {
                                    layer.msg('<?= lang('审批成功');?>', {icon: 1, shade: [0.3, '#393D49']},function () {
                                        layer.close(index);//关闭窗口
                                    });
                                } else {
                                    layer.closeAll();//关闭窗口
                                    var index3 = layer.alert(data.msg + '<br><?= lang('点击"确定按钮"查看失败原因！');?>', {icon: 5,offset: '200px',}, function () {
                                        layer.close(index3);
                                        layer.open({
                                            type: 1,
                                            title:'<?= lang('审批结果');?>',
                                            area: ['750px','500px'],
                                            //shade:0.5,
                                            offset: '200px',
                                            content:$('#_window'),
                                            success: function(layero, e){
                                                layer.setTop(layero);
                                                var table_data = data.data;
                                                var columns = [
                                                    {data: 'id',type: 'text',className: "htCenter",width: 100},
                                                    {data: 'title',type: 'text',className: "htLeft",width: 250},
                                                    {data: 'status',type: 'text',className: "htCenter",width: 100,},
                                                    {data: 'msg',type: 'text',className: "htLeft",width: 250,},
                                                ];
                                                var colHeaders = ['<?= lang('id');?>', '<?= lang('备注');?>', '<?= lang('状态');?>', '<?= lang('失败说明');?>'];

                                                $("#handsontable_box").handsontable({
                                                    columns: columns, //列配置参数
                                                    data: table_data,//为需要绑定数据集合
                                                    licenseKey: 'non-commercial-and-evaluation',
                                                    colHeaders: colHeaders, // 表头
                                                    rowHeaders: true,
                                                    dropdownMenu: true, //忘了
                                                    fillHandle: true,
                                                });
                                            }
                                        });
                                    });
                                    submit = false;
                                }
                            },
                            error: function (data) {
                                layer.close(loading);
                                layer.msg('<?= lang('请求失败，错误代码：');?>' + data.status);
                            },
                            dataType: "json"
                        });
                    });
                    n.trigger("click");
                }
            });
        }

        /*
        var tt = $('#tt');
        var rows = tt.datagrid('getSelections');
        if (rows.length > 0) {
            var form_data = {};
            var ids = [];
            var msg = '';
            $.each(rows, function (i, it) {
                ids.push(it.id);
                if (it.apply_status == '0') msg = it.id + '已正式,无法修改';
                if (it.apply_status == '3') msg = it.id + '待审核,无法修改';
            });
            if (msg != '') {
                $.messager.alert("提示", msg);
                return;
            }

            var ids_str = ids.join(',');

            form_data.ids = ids_str;
            form_data.apply_status = 1;
            form_data.apply_remark = '';

            $('#apply_crm_form').form('load', form_data);
            $('#apply_crm_div').window('open');
        } else {
            $.messager.alert("Tips", "No Item Selected");
        }*/
    }

    //这段代码旧的审批提交，不走审批事务的流程
    function apply_crm_save() {
        if (is_submit) {
            return;
        }
        $.messager.confirm('<?= lang('确认对话框');?>', '<?= lang('是否确认提交？');?>', function (r) {
            if (r) {
                is_submit = true;
                ajaxLoading();
                var this_window = $('#apply_crm_div');
                var form = $('#apply_crm_form');
                form.form('submit', {
                    url: '/biz_crm/crm_update_data',
                    onSubmit: function () {
                        var validate = $(this).form('validate');
                        if (!validate) {
                            ajaxLoadEnd();
                            is_submit = false;
                        }
                        return validate;
                    },
                    success: function (res_json) {
                        var res;
                        try {
                            res = $.parseJSON(res_json);
                        } catch (e) {
                        }
                        if (res == undefined) {
                            ajaxLoadEnd();
                            is_submit = false;
                            $.messager.alert('Tips', res);
                            return;
                        }
                        this_window.window('close');
                        ajaxLoadEnd();
                        is_submit = false;
                        if (res.code == 0) {
                            $.messager.alert('<?= lang('提示')?>', res.msg);
                            $('#tt').datagrid('reload').datagrid('clearSelections');
                        } else {
                            $.messager.alert('<?= lang('提示')?>', res.msg, 'info', function (r) {
                                this_window.window('open');
                            });
                        }
                    }
                });
            }
        });
    }

    function doSearch_all() {
        var title = $('#title_tabs').val();
        if (title == '') {
            var url = '/biz_crm/get_apply_data';
            doSearch(url);
        } else {
            var url = '/biz_crm/get_apply_data?apply_status=' + title;
            doSearch(url);
        }
    }

    function import_crm() {
        //获取查询条件
        var cx_data = $('#cx').serializeArray();
        var title = [];
        $.each(cx_data, function (index, item) {
            if (item.name == 'crm_user') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
            if (item.name == 'crm_user_type') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
            if (item.name == 'f_role') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
            if (item.name == 'field[v][]') {
                if (item.value != '') {
                    title.push(item.value);
                }
            }
        });

        //获取选中id
        var tt = $('#tt');
        var rows = tt.datagrid('getSelections');
        var ids = [];
        $.each(rows, function (i, it) {
            ids.push(it.id);
        });

        if (ids.length < 1) {
            alert('<?= lang('暂无选中，请选择客户');?>');
            return false;
        }
        post('/crm_promote_mail/add_presend_by_search_send_crm', {ids: ids, title: title});
    }

    //表单模拟post
    function post(URL, PARAMS) {
        var temp_form = document.createElement("form");
        temp_form.action = URL;
        temp_form.target = "_blank";
        temp_form.method = "post";
        temp_form.style.display = "none";
        for (var x in PARAMS) {
            var opt = document.createElement("textarea");
            opt.name = x;
            opt.value = PARAMS[x];
            temp_form.appendChild(opt);
        }
        document.body.appendChild(temp_form);
        temp_form.submit();
    }

    /**
     * duo
     * 删除客户
     */
    function del()
    {
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length>0){
            var ids = [];
            $.each(rows, function (i, it) {
                ids.push(it.id);
            });

            layer.confirm('<?= lang('确认删除选中客户吗？此操作不可逆！');?>', {icon: 3, title:'<?= lang('提示');?>'}, function(index){
                var load = layer.load(1);
                $.ajax({
                    type: 'post',
                    url: '/biz_crm/del_customer',
                    data: {ids:ids},
                    success: function (data) {
                        layer.close(load);
                        layer.close(index);
                        $('#tt').datagrid('reload');
                        $('#tt').datagrid('unselectAll');
                        ids = [];
                        if (data.code == 1) {
                            layer.msg('<?= lang('Success.');?>', {icon: 1, shade: [0.3, '#393D49']});
                        } else {
                            layer.alert(data.msg, {icon: 5});
                        }
                    },
                    error: function (data) {
                        layer.msg('<?= lang('请求失败，错误代码：');?>' + data.status);
                    },
                    dataType: "json"
                });
            });
        }
        else {
            $.messager.alert('<?= lang('提示')?>','<?= lang('请选择删除项');?>');
            return
        }
    }
</script>
<body style="padding-top: 10px;">
<?php if (!empty($import)): ?>
    <div style="display:inline-block">
        <h1> <?= lang('1、请先勾选需要群发的客户');?> </h1>
    </div>
    <div style="display:inline-block;padding-left: 30px">
        <a href="javascript:void(0);" class="easyui-linkbutton" onclick="import_crm()"><?= lang('通过勾选导入邮箱群发');?></a>
        <a href="javascript:void(0);" class="easyui-linkbutton" onclick="selectAll()"><?= lang('直接全部导入群发');?></a>
    </div>
<?php endif; ?>

<!--<span style="color:red; position: absolute; left: 1000px; top:15px;">请每天及时清理“已拒绝”状态的客户，要么直接删除，要么补充资料再次申请，不要呆滞在那里！</span>-->

<div id="tabs" class="easyui-tabs" style="width:100%;height:29px;">
    <div title="<?= lang('全部');?>"></div>
    <?php if (empty($import)): ?>
        <?php if(menu_role('crm_clue')): ?>
        <div title="<?= lang('线索');?>"></div>
        <?php endif; ?>
        <?php if(menu_role('shipment_global_shipment_crm')): ?>
        <div title="<?= lang('线索2');?>"></div>
        <?php endif; ?>
        <?php if (get_session('id') != 20057):?>
            <div title="<?= lang('待申请');?>"></div>
        <?php endif;?>
        <div title="<?= lang('申请中');?>"></div>
        <div title="<?= lang('已通过');?>"></div>
        <div title="<?= lang('已拒绝');?>"></div>
        <!--<div title="<?= lang('转正申请中');?>"></div>-->
        <!--<div title="<?= lang('转正通过');?>"></div>-->
    <?php endif; ?>
    <!--<div title="<?= lang('公海');?>"></div>-->
    <?php
    //这个版本不需要了
    if(1==0):
        //只有田诗雯和销售经理可以查看已分配
        if (get_session('id') == 20001 || getUserField(get_session('id'), 'm_level') == 8):
            //只有销售经理可以查看/操作待分配
            if (get_session('id') != 20001):
                ?>
                <div title="<?= lang('待分配');?>" id="test"></div>
            <?php endif;?>
            <div title="<?= lang('已分配');?>"></div>
        <?php endif;?>
    <?php endif;?>
    <div title="<?= lang('CRM黑名单');?>"></div>
    <!--<div title="<?= lang('无效邮箱');?>"></div>-->

</div>
<table id="tt" style="width:1100px;" rownumbers="false" pagination="true" idField="id" pagesize="30" toolbar="#tb" singleSelect="false" nowrap="true"></table>
<div id="tb" style="padding:5px;">
    <div style="display:flex;display:-webkit-flex;">
        <div style="flex: 1;">
            <!--<a href="javascript:history.go(-1);" class="easyui-linkbutton" iconCls="icon-back" plain="true">返回上一页</a>-->
            <!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
                   onclick="add();">新增</a>-->
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true"
               onclick="javascript:$('#chaxun').window('open');"><?= lang('查询');?></a>
            <a href="javascript:void(0)" id="add" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()"><?= lang('新增客户');?></a>
            <a href="javascript:void(0)" id="import_crm" class="easyui-linkbutton" iconCls="icon-add" plain="true"><?= lang('导入客户');?></a>
            <a href="javascript:void(0)" id="export_crm"><?= lang('导出客户');?></a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
               onclick="del()"><?= lang('删除客户');?></a>
            <?php if(is_admin()): ?>
                <!--<a href="/biz_crm/split_table" target="_blank" class="easyui-linkbutton" iconCls="icon-add" plain="true"><?= lang('导入线索');?></a>-->
            <?php endif; ?>
            <a href="javascript:void(0);" id="batch_deliver" class="easyui-linkbutton" plain="true" onclick="batch_deliver()"><?= lang('批量转交');?></a>

            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true"
               onclick="config()"><?= lang('表头设置');?></a>
            <?php if (get_session('id') == 20001): ?>
                <a href="/toolbox/index" target="_blank"  class="easyui-linkbutton" plain="true"><?= lang('工具箱');?></a>
            <?php endif; ?>
            <div id="query" style="display:inline-block">
            </div>
        </div>
    </div>
</div>

<div id="title_tabs" style="hidden" value=""></div>
<div id="chaxun" class="easyui-window" title="Query" closed="false" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <form id="cx">
        <table>
            <tr>
                <td><?= lang('部门');?></td>
                <td>
                    <select class="easyui-combotree" id="user" style="width:475px;" data-options="
                        valueField:'id',
                        textField:'text',
                        cascadeCheck: false,
                        url:'/bsc_user/get_user_role_tree',
                        value:'<?= $crm_user; ?>',
                        onBeforeSelect: function(node){
                            var inp = $('#user');
                            var roots = inp.combotree('tree').tree('getRoots');
                            var result = true;
                            if(result){
                                if(!node.is_use){
                                    return false;
                                }
                                $('#crm_user').val(node.id);
                                $('#crm_user_type').val(node.type);
                            }
                            return result;
                        },
                        onLoadSuccess:userLoadSuccess,
                    "></select>
                    <input type="hidden" name="crm_user" id="crm_user" value="<?= $crm_user; ?>">
                    <input type="hidden" name="crm_user_type" id="crm_user_type" value="<?= $crm_user_type; ?>">
                </td>
            </tr>
            <tr>
                <td>
                    <?= lang('角色');?>
                </td>
                <td align="right">
                    <!--<select class="easyui-combobox" id="f_role" name="f_role" style="width: 475px"
                            data-options="
                        valueField:'value',
                        textField:'value',
                        url:'/bsc_dict/get_option/role',
                    ">

                    </select>-->
                    <select class="easyui-combobox" id="f_role" name="f_role" style="width: 475px">
                        <option selected="selected" disabled="disabled" style='display: none' value=''></option>
                        <option value="factory">factory</option>
                        <option value="logistics_client">logistics_client</option>
                    </select>
                </td>
            </tr>
        </table>
        <br><br>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="doSearch_all();"><?= lang('查询');?></button>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="resetSearch();"><?= lang('重置');?></button>
        <a href="javascript:void;" class="easyui-tooltip" data-options="
            content: $('<div></div>'),
            onShow: function(){
                $(this).tooltip('arrow').css('left', 20);
                $(this).tooltip('tip').css('left', $(this).offset().left);
            },
            onUpdate: function(cc){
                cc.panel({
                    width: 500,
                    height: 'auto',
                    border: false,
                    href: '/bsc_help_content/help/query_condition'
                });
            }
        "><?= lang('help');?></a>
    </form>
</div>

<!--client_code修改-->
<div id="apply_crm_div" class="easyui-window" style="width:291px;height:165px"
     data-options="title:'window',modal:true,closed:true">
    <form id="apply_crm_form">
        <input type="hidden" name="ids">
        <table>
            <tr>
                <td><?= lang('审核状态');?></td>
                <td>
                    <input type="radio" name="apply_status" id="apply_crm_status_f1" value="-1">
                    <lable for="apply_crm_status_f1"><?= lang('已拒绝');?></lable>
                    <input type="radio" name="apply_status" id="apply_crm_status_1" value="1">
                    <lable for="apply_crm_status_1"><?= lang('申请中');?></lable>
                    <input type="radio" name="apply_status" id="apply_crm_status_2" value="2">
                    <lable for="apply_crm_status_2"><?= lang('已通过');?></lable>
                </td>
            </tr>
            <tr>
                <td><?= lang('审核备注');?></td>
                <td>
                    <textarea class="textarea" name="apply_remark"></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button onclick="apply_crm_save()" type="button" class="easyui-linkbutton"
                            style="width: 180px;"><?= lang('保存'); ?></button>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="batch_deliver_div" style="display: none;">
    <form class="layui-form" lay-filter="myform" action="">
        <div class="layui-form-item" style="margin: 20px 30px 0px 0px;">
            <label class="layui-form-label"><?= lang('销售名单');?></label>
            <div class="layui-input-block">
                <select name="sale_id" id="sale_id" lay-search lay-verify="required" lay-reqText="<?= lang('请选择销售');?>">
                    <option value=""><?= lang('请选择销售');?></option>
                    <?php
                    foreach ($sales as $item) {
                        echo "<option value=\"{$item['id']}\">{$item['name']} / {$item['group_name']}</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <!--<div class="layui-form-item" style="margin: 20px 30px 0px 0px;">
            <label class="layui-form-label">结束日期</label>
            <div class="layui-input-block">
                <input type="text" class="layui-input" id="end_date" lay-verify="required" name="end_date" value="">
            </div>
        </div>-->
        <button id="batch_deliver_btn" style="display: none;" lay-submit lay-filter="*"><?= lang('提交');?></button>
    </form>
</div>
<script type="text/javascript"src="/inc/third/layui/layui.js"></script>
<script>
    layui.use(['form','laydate','element'], function(){
        var form = layui.form;
        var element = layui.element;
        var laydate = layui.laydate;
        //绑定日期控件
        laydate.render({
            elem: '#end_date'
        });

        //空格转换
        form.on('select', function(data){
            //layui的渲染方法可以做到无缝转换，但是不能解决右键时的BUG
            form.render();
            //解决右键BUG就用下面的代码强制转换为空格
            $('body').click(function () {
                $('.layui-form-select .layui-select-title>.layui-input').each(function () {
                    var _this = this;
                    setTimeout(function () {
                        var val = $(_this).val();
                        var new_val = val.replace(/\&nbsp\;/g, ' ')
                        if (new_val !== val) {
                            $(_this).val(new_val)
                        }
                    }, 100)
                })
            })
        });

        //提交表单
        form.on('submit(*)', function(data){
            var loading = layer.load(1);
            var sale_id = $('#sale_id').val();
            var end_date = $('#end_date').val();
            data.field.ids = ids_str;
            $.ajax({
                type: 'post',
                url: '/biz_crm/batch_deliver_client',
                data: {ids: ids_str, sale_id: sale_id, end_date: end_date},
                success: function (data) {
                    layer.close(loading);
                    if (data.code == 1) {
                        layer.close(batch_deliver_window);
                        layer.alert('<?= lang('转交成功');?>', {icon: 1, closeBtn:0}, function (index) {
                            //刷新easyUI的datagrid控件
                            $('#tt').datagrid('reload').datagrid('clearSelections');
                            //清空表单
                            form.val("myform", { "sale_id": "","end_date": ""});
                            layer.close(index);
                        });
                    } else {
                        layer.alert(data.msg, {icon: 5});
                    }
                },
                error: function (data) {
                    layer.close(loading);
                    layer.alert('<?= lang('请求失败，错误代码：');?>' + data.status, {icon: 5});
                },
                dataType: "json"
            });
            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        });
    });

    //批量转交
    function batch_deliver() {
        var tt = $('#tt');
        var rows = tt.datagrid('getSelections');
        if (rows.length > 0) {
            var ids = [];
            $.each(rows, function (i, it) {
                ids.push(it.id);
            });
            ids_str = ids.join(',');
            batch_deliver_window = layer.open({
                id: 'to-sales',
                type: 1,
                title: '<?= lang('批量转交销售');?>',
                area: '330',
                offset: '20%',
                shade: 0.5,
                content: $('#batch_deliver_div'),
                zIndex: 9999,
                btn: ['<?= lang('保存');?>', '<?= lang('取消');?>'],
                success: function (layero, index) {
                    $('#to-sales').css('overflow', 'unset');
                },
                yes: function (e, layero) {
                    $('#batch_deliver_btn').click();
                }
            });
        } else {
            layer.msg("No Item Selected", {icon:2});
        }
    }

    function view_log(id) {
        layer.open({
            type: 2,
            title:'<?= lang('客户流转日志');?>',
            area: '600px',
            offset: '100px',
            shade:0.5,
            shadeClose: true,
            skin: 'layui-layer-lan',
            content:'/biz_crm/crm_log/?crm_id='+id,
            success: function(layero, index) {
                //找到当前弹出层的iframe元素
                var iframe = $(layero).find('iframe');
                var childPageHeight = iframe[0].contentDocument.body.offsetHeight;

                if (childPageHeight > 600){
                    iframe.css('height', 600);
                }else{
                    //对加载后的iframe进行宽高度自适应
                    layer.iframeAuto(index);
                }
            }
        });
    }

    function view_develop(id) {
        layer.open({
            type: 2,
            title:'<?= lang('客户跟进记录');?>',
            area: ['1202px', '700px'],
            offset: '100px',
            shade:0.5,
            skin: 'layui-layer-lan',
            content:'/biz_client_follow_up_log/index/?crm_id='+id,
        });
    }
</script>
</body>
<!--弹窗-->
<div id="_window" class="table" style="display: none; width: 100%">
    <div id="handsontable_box"></div>
</div>
<!--div id="_window" style="display: none; overflow:hidden;">
    <iframe scrolling="auto" id='openIframe' frameborder="0"  src="" style="width:100%;height:100%; overflow: hidden"></iframe>
</div-->
<input type="hidden" id="select_ids">