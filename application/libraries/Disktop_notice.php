<?php
/**
 *  推送桌面消息
 */

class Disktop_notice
{
    private $db;
    public function __construct()
    {
        $this->db = & get_instance()->db;
    }

    /**
     * @title CRM导入新线索触发消息
     */
    public function import_clue($company_search='')
    {
        if (empty($company_search)) return false;
        $end = date('Y-m-d H:i:s', strtotime('+1 day'));
        $where = "company_search = '{$company_search}' or company_search_en = '{$company_search}'";
        //往来单位抬头触发通知
        $client = $this->db->select('id,client_code, company_name')->where($where)->get('biz_client')->row_array();
        if (!empty($client)){
            $duty = $this->db->where(array('client_code'=>$client['client_code'], 'user_role'=>'sales'))->get('biz_client_duty')->result_array();
            foreach ($duty as $item){
                $data = array(
                    'table_name'    => 'biz_client',
                    'id_no'         => $client['id'],
                    'type'          => 0,
                    'code'          => 'client_clue',
                    'start'         => date('Y-m-d H:i:s'),
                    'end'           => $end,
                    'txt'           => "您的合作客户“{$client['company_name']}”有新的出货记录",
                    'href'          => "/biz_client/edit/{$client['id']}/?tab_title=客户信息&child_tab_title=相关线索",
                    'notice_user'   => $item['user_id'],
                    'notice_user_group'   => getUserField($item['user_id'], 'group'),
                    'lock_screen'   => 0,
                    'lock_screen_datetime'   => date('Y-m-d 23:59:59'),
                    'create_time'   => date('Y-m-d H:i:s'),
                    'create_by'     => get_session('id'),
                    'update_time'   => date('Y-m-d H:i:s'),
                    'update_by'     => get_session('id'),
                );

                $this->db->insert('bsc_notice', $data);
            }
        }

        //CRM抬头触发通知
        $crm = $this->db->select('id, company_name, sales_id')->where('apply_status > 0 and sales_id > 0')->where($where)->get('biz_client_crm')->result_array();
        foreach ($crm as $item){
            $data = array(
                'table_name'    => 'biz_client_crm',
                'id_no'         => $item['id'],
                'type'          => 0,
                'code'          => 'crm_clue',
                'start'         => date('Y-m-d H:i:s'),
                'end'           => $end,
                'txt'           => "您的CRM客户“{$item['company_name']}”有新的出货记录",
                'href'          => "/biz_crm/edit/{$item['id']}/?tab_title=相关线索",
                'notice_user'   => $item['sales_id'],
                'notice_user_group'   => getUserField($item['sales_id'], 'group'),
                'lock_screen'   => 0,
                'lock_screen_datetime'   => date('Y-m-d 23:59:59'),
                'create_time'   => date('Y-m-d H:i:s'),
                'create_by'     => get_session('id'),
                'update_time'   => date('Y-m-d H:i:s'),
                'update_by'     => get_session('id'),
            );
            $this->db->insert('bsc_notice', $data);
        }
    }

    /**
     * @title 往来单位添加关联公司触发消息
     */
    public function add_client_relation($client_code)
    {
        if (empty($client_code)) return false;
        $end = date('Y-m-d H:i:s', strtotime('+1 day'));
        $where = "client_code = '{$client_code}'";
        $client = $this->db->select('id,client_code, company_name')->where($where)->get('biz_client')->row_array();
        if (!empty($client)){
            $duty = $this->db->where(array('client_code'=>$client_code, 'user_role'=>'sales'))->get('biz_client_duty')->result_array();
            foreach ($duty as $item){
                $data = array(
                    'table_name'    => 'biz_client',
                    'id_no'         => $client['id'],
                    'type'          => 0,
                    'code'          => 'relation',
                    'start'         => date('Y-m-d H:i:s'),
                    'end'           => $end,
                    'txt'           => "您的合作客户“{$client['company_name']}”有新的关联公司",
                    'href'          => "/biz_client/edit/{$client['id']}/?tab_title=关联公司",
                    'notice_user'   => $item['user_id'],
                    'notice_user_group'   => getUserField($item['user_id'], 'group'),
                    'lock_screen'   => 0,
                    'lock_screen_datetime'   => date('Y-m-d 23:59:59'),
                    'create_time'   => date('Y-m-d H:i:s'),
                    'create_by'     => get_session('id'),
                    'update_time'   => date('Y-m-d H:i:s'),
                    'update_by'     => get_session('id'),
                );

                $this->db->insert('bsc_notice', $data);
            }
        }
    }

    /**
     * 新增销售
     */
    public function add_sales($client_code, $new_sales_id){
        require_once __DIR__ . '/Client.php';
        $Client = new Client();
        //获取抬头下的全部销售
        $client_sales = array_diff(
            array_column($Client->get_client_sales($client_code), 'id'), (array)$new_sales_id
        );

        //通知当前抬头的销售
        $new_sales_name = getUserField($new_sales_id, 'name');
        $end = date('Y-m-d H:i:s', strtotime('+2 day'));
        $client = $this->db->where('client_code', $client_code)->get('biz_client')->row_array();
        foreach ($client_sales as $sales){
            $data = array(
                'table_name'    => 'biz_client',
                'id_no'         => $client['id'],
                'type'          => 0,
                'code'          => 'new_sales',
                'start'         => date('Y-m-d H:i:s'),
                'end'           => $end,
                'txt'           => "您的合作客户“{$client['company_name']}”添加了新销售“{$new_sales_name}”！",
                'href'          => "/biz_client/edit/{$client['id']}",
                'notice_user'   => $sales,
                'notice_user_group'   => getUserField($sales, 'group'),
                'lock_screen'   => 0,
                'lock_screen_datetime'   => date('Y-m-d 23:59:59'),
                'create_time'   => date('Y-m-d H:i:s'),
                'create_by'     => get_session('id'),
                'update_time'   => date('Y-m-d H:i:s'),
                'update_by'     => get_session('id'),
            );

            $this->db->insert('bsc_notice', $data);
        }

        //获取关联公司
        $relation_company = $Client->get_relation_company($client_code);
        //获取关联公司的销售
        $relation_sales_id = [];
        foreach($relation_company as $item){
            $relation_client = $this->db->select('id, company_name')->where('client_code', $item)->get('biz_client')->row_array();
            //去除发过相同消息的销售
            $client_sales = array_diff(array_column($Client->get_client_sales($item), 'id'), $relation_sales_id);
            if (!empty($client_sales)) {
                foreach ($client_sales as $sales) {
                    $data = array(
                        'table_name' => 'biz_client',
                        'id_no' => $relation_client['id'],
                        'type' => 0,
                        'code' => 'new_sales',
                        'start' => date('Y-m-d H:i:s'),
                        'end' => $end,
                        'txt' => "您的合作客户“{$relation_client['company_name']}”的关联公司“{$client['company_name']}”添加了新销售“{$new_sales_name}”！",
                        'href' => "/biz_client/edit/{$relation_client['id']}/?tab_title=关联公司",
                        'notice_user' => $sales,
                        'notice_user_group' => getUserField($sales, 'group'),
                        'lock_screen' => 0,
                        'lock_screen_datetime' => date('Y-m-d 23:59:59'),
                        'create_time' => date('Y-m-d H:i:s'),
                        'create_by' => get_session('id'),
                        'update_time' => date('Y-m-d H:i:s'),
                        'update_by' => get_session('id'),
                    );
                    $this->db->insert('bsc_notice', $data);
                }

                array_push($relation_sales_id, ...$client_sales);
            }
        }
    }

    /**
     * @title
     */
    public function add_user_data_notice($user_id='')
    {
        if (empty($user_id)) return false;
        $end = date('Y-m-d H:i:s', strtotime('+1 day'));
        $data = array(
            'table_name'    => 'bsc_user',
            'id_no'         => $user_id,
            'type'          => 0,
            'code'          => 'user_data',
            'start'         => date('Y-m-d H:i:s'),
            'end'           => $end,
            'txt'           => "您的个人资料需补充（国籍，中文名称，英文名称，邮箱，手机号，头像），双击打开",
            'href'          => "/bsc_user/person",
            'notice_user'   => $user_id,
            'notice_user_group'   => getUserField($user_id, 'group'),
            'lock_screen'   => 1,
            'lock_screen_datetime'   => date('Y-m-d 23:59:59'),
            'create_time'   => date('Y-m-d H:i:s'),
            'create_by'     => get_session('id'),
            'update_time'   => date('Y-m-d H:i:s'),
            'update_by'     => get_session('id'),
        );
        $this->db->insert('bsc_notice', $data);
    }
}