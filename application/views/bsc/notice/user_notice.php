<script type="text/javascript">
    function doSearch(){
        $('#tt').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val() ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val() ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val()
        });
        $('#chaxun').window('close');
    }

    $(function () {
        $('#tt').edatagrid({
            url: '/bsc_notice/get_user_data',
        });
        $('#tt').datagrid({
            width: 'auto',
        	height: $(window).height() 
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });

    function setRowBgColor(index, row) {
        if(row.isread < 0){

        }
        <?php foreach($read_color as $key => $val){?>
        else if(row.isread == <?= $key;?>){
            return '<?= $val;?>';
        }
        <?php }?>
    }
</script>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false" rowStyler="setRowBgColor">
    <thead>
    <tr>
        <th field="id" width="60" sortable="true"><?= lang('id')?></th>
<!--        <th field="table_name" width="150" align="center" >--><?//= lang('table_name')?><!--</th>-->
<!--        <th field="id_no" width="100" align="center" >--><?//= lang('id_no')?><!--</th>-->
<!--        <th field="type" width="100">--><?//= lang('type')?><!--</th>-->
        <th field="start" width="100"><?= lang('start')?></th>
        <th field="end" width="100"><?= lang('end')?></th>
<!--        <th field="rate" width="100"bsc_notice>--><?//= lang('rate')?><!--</th>-->
        <th field="txt" width="100"><?= lang('txt')?></th>
<!--        <th field="status" width="60" formatter="get_status">--><?//= lang('status')?><!--</th>-->
        <th field="create_time" width="150" sortable="true"><?= lang('create_time')?></th>
        <th field="update_time" width="150" sortable="true"><?= lang('update_time')?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');">Query</a>

            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                Query 1
            </td>
            <td align="right">
                <select name="f1"  id="f1"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1"  id="s1"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v1" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                Query 2
            </td>
            <td align="right">
                <select name="f2"  id="f2"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2"  id="s2"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v2" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                Query 3
            </td>
            <td align="right">
                <select name="f3"  id="f3"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s3"  id="s3"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v3" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;">Search</a>
</div>
