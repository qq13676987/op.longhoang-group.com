<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<script type="text/javascript">
    function doSearch() {
        $('#tt').datagrid('load', {
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val(),
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val(),
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val()
        });
        $('#chaxun').window('close');
    }

    function buttons_for(value, row, index) {
        var str = '';
        if (row.is_default == false) {
            str += '<button onclick="set_default(' + row.id + ', \'' + row.client_code + '\')"><?= lang('设置默认');?></button>';
        } else {
            str += "<?= lang('当前默认');?>";
        }
        return str;
    }

    function telephone_for(value, row, index) {
        var str = value;
        if(row.is_bind_wx) str += " <span style='color:blue;'><?= lang('已绑微信');?></span>";
        return str;
    }

    function set_default(id, client_code) {
        $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否设置为当前客户的默认联系人?');?>', function (r) {
            if (r) {
                ajaxLoading();
                $.ajax({
                    url: '/biz_client_contact/set_client_contact',
                    type: 'POST',
                    data: {
                        id: id,
                        client_code: client_code,
                    },
                    success: function (res_json) {
                        var res;
                        try {
                            res = $.parseJson(res_json);
                        } catch (e) {

                        }
                        ajaxLoadEnd();
                        if (res == undefined) {
                            $.messager.alert('Tips', res_json);
                            return;
                        }
                        $.messager.alert('Tips', res.msg);
                        if (res.code == 0) {
                            $('#tt').datagrid('reload');
                        } else {

                        }

                    },
                });
            }
        });
    }

    $.extend($.fn.validatebox.defaults.rules, {
        Num: { //验证手机号
            validator: function (value, param) {
                return /^\d{11}$/.test(value);
            },
            message: '<?= lang('请输入正确的手机号码(纯数字11位)。');?>'
        },
    });

    //打开iframe弹窗，主要用于edatagrid头部工具栏
    function open_window(url, title){
        $('#openIframe')[0].src=url;
        $('#_window').window({
            title:title,
            minimizable:false,
            collapsible:true,
            maximizable:false,
            closable:true,
            border:'thin',
            width:'400',
            height:'490',
            modal:true,
        });
    }

    //审批
    function approve_contact(url){
        $.ajax({
            type: 'post',
            url: url,
            success: function (data) {
                if (data.code == 1){
                    //刷新父页面的列表控件
                    layer.alert("<?= lang('审批完成。');?>", {icon:1,title:'<?= lang('提示');?>'}, function (index) {
                        $('#tt').edatagrid('reload');
                        layer.close(index);
                    });
                }else{
                    layer.alert('<?= lang('审批失败：');?>'+data.msg, 'error',{icon:5});
                }
            },
            error: function (data) {
                layer.alert('<?= lang('操作失败!');?>', {icon:5});
            },
            dataType: "json"
        });
    }

    $(function () {
        $('#tt').edatagrid({
            url: '/biz_client_contact/get_data_crm/?crm_id=<?=$crm_id?>',
            saveUrl: '/biz_client_contact/add_data/',
            updateUrl: '/biz_client_contact/update_data/',
            destroyUrl: '/biz_client_contact/del_crm_contact',
            onDblClickRow: function (index, row) {
                if (row.apply_params != '' && row.approve_user > 0){
                    layer.msg('<?= lang('是否允许使用当前申请的Email?');?>', {
                        icon: 3,
                        title: '<?= lang('审批Email');?>',
                        closeBtn: 1,
                        //shade: 0.5,
                        time: 0 //不自动关闭
                        , btn: ['<?= lang('同意使用');?>', '<?= lang('不同意使用');?>']
                        , yes: function (index) {
                            approve_contact('/biz_client_contact/approve_contact/?status=1&id=' + row.id);
                            layer.close(index);
                        },
                        btn2: function (index) {
                            approve_contact('/biz_client_contact/approve_contact/?status=0&id=' + row.id);
                            layer.close(index);
                        }
                    });
                }else {
                    var url = '/biz_client_contact/edit_crm/?id=' + row.id;
                    var title = '<?= lang('编辑联系人');?>';
                    open_window(url, title);
                }
            },
            onError: function (index, data) {
                if(data.code=='0'){
                    $.messager.confirm('<?= lang('确认');?>',data.msg,function(r){
                        if (r){
                            window.open('/other/force_pass_email?email='+data.email);
                        }
                    });
                }else{
                    $.messager.alert('error', data.msg);
                }
            }
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
            rowStyler: function(index,row){
                var str = '';
                if (row.ok == -1){
                    str += 'color:red;';
                }else if(row.ok == 1){
                    str += 'color:green;';
                }
                return str;
            }
        });

        //表头工具栏“add”事件
        $('#add').click(function () {
            var url = '/biz_client_contact/add_crm/?crm_id=<?=$crm_id?>';
            var title = '<?= lang('添加CRM联系人');?>';
            open_window(url, title);
        });

        //表头工具栏“edit”事件
        $('#edit').click(function () {
            var rows = $('#tt').datagrid('getSelections');
            if (rows.length < 1) {
                $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择编辑对象！');?>');
                return;
            }
            var data = rows[0];
            var url = '/biz_client_contact/edit_crm/?id='+rows[0].id;
            var title = '<?= lang('编辑联系人');?>';
            open_window(url, title);
        });

        //表头工具栏“copy”事件
        $('#copy').click(function () {
            var rows = $('#tt').datagrid('getSelections');
            if (rows.length < 1) {
                $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择复制对象！');?>');
                return;
            }
            var data = rows[0];
            var url = '/biz_client_contact/copy/?id='+rows[0].id;
            var title = '复制联系人';
            open_window(url, title);
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });

</script>
<link rel="stylesheet" type="text/css" href="/inc/third/layui/css/layui.css">
<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th field="id" width="80" sortable="true"><?= lang('ID');?></th>
        <th field="role" width="120"><?= lang('角色');?></th>
        <th field="position" width="100" ><?= lang('职位');?></th>
        <?php if(!empty($role)):?>
            <th field="pre_alert_flag" width="100" ><?= lang('预报接收');?></th>
        <?php endif;?>
        <th field="name" width="100" ><?= lang('姓名');?></th>
        <th field="telephone" width="150" formatter="telephone_for"><?= lang('手机号');?></th>
        <th field="telephone2" width="100" ><?= lang('座机');?></th>
        <th field="email" width="250" ><?= lang('邮箱');?></th>
        <th field="approve_user_name" width="150"><?= lang('审批人');?></th>
        <th field="create_time" width="150"><?= lang('创建时间');?></th>
        <th field="create_by_name" width="150"><?= lang('创建人');?></th>
        <th field="update_time" width="150"><?= lang('修改时间');?></th>
        <th field="update_by_name" width="150"><?= lang('修改人');?></th>
        <!--<th field="menu" width="300" editor="{type:'combobox',-->
        <!--options:{-->
        <!--multiple:true,-->
        <!--valueField:'value',-->
        <!--textField:'value',-->
        <!--url:'/biz_client_contact/client_user_menu_option'-->
        <!--}}">前台权限</th>-->
        <th field="password" width="100"><?= lang('前台密码');?></th>
        <th field="menu" width="190" ><?= lang('前台权限');?></th>
        <!--<th field="sales_id" width="150">默认销售</th>-->
        <!--<th field="user_range" width="150">主子账号</th>-->
        <th field="live_chat" width="100" sortable="true" ><?= lang('QQ');?></th>

        <th field="remark" width="100" ><?= lang('备注');?></th>
        <th field="sailing" width="400" ><?= lang('航线');?></th>
        <th field="address" width="400" ><?= lang('地址');?></th>
        <th field="buttons" width="80" data-options="formatter: buttons_for"></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" id="add" plain="true"><?= lang('新增'); ?></a>
                <!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" id="copy" plain="true"><?= lang('复制'); ?></a>-->
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" id="edit" plain="true"><?= lang('编辑'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
                   onclick="javascript:$('#tt').edatagrid('destroyRow')"><?= lang('删除'); ?></a>
                <!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query'); ?></a>-->
                <a href="/other/force_pass_email" class="easyui-linkbutton" plain="true" target="_blank"><?= lang('手动验证邮箱'); ?></a>
                <span style="color:red;"><?= lang('!!!红色代表验证失败,绿色验证通过,黑色的正在等待验证');?></span>
            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                <?= lang('查询'); ?> 1
            </td>
            <td align="right">                &nbsp;

                <select name="s1" id="s1" class="easyui-combobox" required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v1" class="easyui-textbox" style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('查询'); ?> 2
            </td>
            <td align="right">
                <select name="s2" id="s2" class="easyui-combobox" required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v2" class="easyui-textbox" style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('查询'); ?> 3
            </td>
            <td align="right">

                <select name="s3" id="s3" class="easyui-combobox" required="true" style="width:100px;">
                    <option value="=">=</option>
                    <option value="like">like</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v3" class="easyui-textbox" style="width:120px;">
            </td>
        </tr>
    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()"
       style="width:200px;height:30px;"><?= lang('查询'); ?></a>
</div>

<!--弹窗-->
<div id="_window" style="display: none; overflow:hidden;">
    <iframe scrolling="auto" id='openIframe' frameborder="0"  src="" style="width:100%;height:100%;"></iframe>
</div>