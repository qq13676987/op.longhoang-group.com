<?php

class biz_clue extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_client_model');
        $this->load->model('bsc_user_model');
        $this->load->model('m_model');
    }

    public function index(){
        $this->load->view('/biz/clue/index_view');
        /*if(is_admin()){
            $this->load->view('/biz/clue/index_view');
        }else{
            echo "暂时关闭，开放等通知！";
        }*/
    }

    //国内线索 to 海外
    public function clue_overseas(){
        if (is_ajax()){
            $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
            $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
            $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
            $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
            $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

            $where = array();
            $where[] = 'apply_status > -99';
            $where[] = 'clue_status BETWEEN 1 and 2';
            $where[] = 'clue_name =\'\'';   //CRM线索有两个类型，1类是导入的线索，有线索名称，2类是手动添加的线索，没有线索名称，当前要获取的是第2类

            //这里是新版的查询代码
            $title_data = get_user_title_sql_data('biz_crm_clue', 'biz_crm_clue_view');//获取相关的配置信息

            if(!empty($fields)){
                foreach ($fields['f'] as $key => $field){
                    $f = trim($fields['f'][$key]);
                    $s = $fields['s'][$key];
                    $v = trim($fields['v'][$key]);
                    if($f == '') continue;
                    //TODO 如果是datetime字段,=默认>=且<=
                    $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                    //datebox的用等于时代表搜索当天的
                    if(in_array($f, $date_field) && $s == '='){
                        if(!isset($title_data['sql_field'][$f])) continue;
                        $f = $title_data['sql_field'][$f];
                        if($v != ''){
                            $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                        }
                        continue;
                    }
                    //datebox的用等于时代表搜索小于等于当天最晚的时间
                    if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                        if(!isset($title_data['sql_field'][$f])) continue;
                        $f = $title_data['sql_field'][$f];
                        if($v != ''){
                            $where[] = "{$f} $s '$v 23:59:59'";
                        }
                        continue;
                    }

                    //把f转化为对应的sql字段
                    //不是查询字段直接排除
                    $title_f = $f;
                    if(!isset($title_data['sql_field'][$title_f])) continue;
                    $f = $title_data['sql_field'][$title_f];


                    if($v !== '') {
                        //这里缺了2个
                        if($v == '-') continue;
                        //如果进行了查询拼接,这里将join条件填充好
                        $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                        if(!empty($this_join)) {
                        $this_join0 = explode(' ', $this_join[0]);
                        $title_data['sql_join'][end($this_join0)] = $this_join;
                    }

                        $where[] = search_like($f, $s, $v);
                    }
                }
            }

            $where = join(' and ', $where);
            //where---end
            Model('biz_client_crm_model');
            $offset = ($page - 1) * $rows;
            $this->db->limit($rows, $offset);
            //获取需要的字段--start
            $selects = array(
                'biz_client_crm.id',
                'biz_client_crm.company_name',
                'biz_client_crm.company_search',
                'biz_client_crm.created_by',
                'biz_client_crm.apply_status',
            );
            foreach ($title_data['select_field'] as $key => $val) {
                if (!in_array($val, $selects)) $selects[] = $val;
            }

            //获取需要的字段--end
            $this->db->select(join(',', $selects), false);
            //这里拼接join数据
            foreach ($title_data['sql_join'] as $j) {
                $this->db->join($j[0], $j[1], $j[2]);
            }//将order字段替换为 对应的sql字段
            if (isset($title_data['sql_field']["biz_client_crm.{$sort}"])) {
                $sort = $title_data['sql_field']["biz_client_crm.{$sort}"];
            }
            $this->db->is_reset_select(false);//不重置查询
            $query = $this->db->where($where, null, false)->order_by($sort, $order)->get('biz_client_crm');
            //echo $this->db->last_query();exit;
            $rs = $query->result_array();
            foreach ($rs as $index => $item){
                $rs[$index]['clue_name'] = $item['company_name'];
            }
            $this->db->clear_limit();//清理下limit
            $result["total"] = $this->db->count_all_results('');
            $result['rows'] = $rs;
            return $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
        else
        {
            //总数量
            $total = $this->db->where('clue_name =  \'\' and clue_status between 1 and 3')->count_all_results('biz_client_crm');
            //分析中数量
            $loading = $this->db->where('clue_name =  \'\' and clue_status = 1')->count_all_results('biz_client_crm');
            //可提取数量
            $gain = $this->db->where('clue_name =  \'\' and clue_status = 2')->count_all_results('biz_client_crm');
            //不可提取数量
            $not_gain = $this->db->where('clue_name =  \'\' and clue_status = 3')->count_all_results('biz_client_crm');
            $data['load_proportion'] = $data['gain_proportion'] = $data['not_gain'] = 0;
            $data['total'] = $total;
            if ($total > 0) {
                $data['load_proportion'] = floor(strval(($loading / $total) * 10000)) / 10000 * 100;
                $data['gain_proportion'] = floor(strval(($gain / $total) * 10000)) / 10000 * 100;
                $data['not_gain'] = floor(strval(($not_gain / $total) * 10000)) / 10000 * 100;
            }

            $this->load->view('biz/clue/clue_overseas', $data);
        }
    }

    public function add_clue()
    {
        if (is_ajax()) {
            $return = array('code' => 0, 'msg' => '');
            $_post = $this->input->post();
            //校验提交
            foreach (array('company_name'=>'客户全称', 'client_source'=>'客户来源', 'etd'=>'booking_ETD') as $key => $v){
                if (!isset($_post[$key]) || $_post[$key] == ''){
                    $return['msg'] = $v . "必须填写！";
                    return $this->output->set_content_type('application/json')->set_output(json_encode($return));
                }
            }

            $clue_data = array(
                'clue'=>$_post['company_name'],
                'customer'=>$_post['company_name'],
                'etd' => $_post['etd'],
                'vessel'=>$_post['vessel'],
                'pol'=>$_post['pol'],
                'pod'=>$_post['pod'],
                'export_sailing' => $_post['export_sailing'],
                'trans_mode'=>$_post['trans_mode'],
                'product_type'=>$_post['product_type'],
                'product_details'=>$_post['product_details'],
                'created_by' => get_session('id'),
                'created_time' => date('Y-m-d H:i:s'),
            );

            $crm_data = array(
                'country_code' => $_post['country_code'],
                'company_name' => $_post['company_name'],
                'company_search' => match_chinese($_post['company_name']),
                'export_sailing' => $_post['export_sailing'],
                'trans_mode' => $_post['trans_mode'],
                'product_type' => $_post['product_type'],
                'product_details' => $_post['product_details'],
                'client_source' => $_post['client_source'],
                'clue_status' => 1,   //1表示为分析中
                'apply_status' => -2, //-2表示为线索
                'created_by' => get_session('id'),
                'created_time' => date('Y-m-d H:i:s'),
                'last_follow_time' => '0000-00-00 00:00:00',
                'last_add_contact_time' => '0000-00-00 00:00:00'
            );

            //抬头是否已经存在
            if($this->db->where(array('company_search'=>$crm_data['company_search'], 'client_level'=>-1))->count_all_results('biz_client') > 0){
                $return['msg'] = "添加失败：线索抬头已被加入往来单位黑名单！";
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            //抬头是否已经存在
            if ($this->db->select('id')->where('company_search', $crm_data['company_search'])->count_all_results('biz_client_crm') > 0){
                $return['msg'] = "添加失败：线索抬头已存在！";
                return $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }

            //新增CRM数据
            if ($this->db->insert('biz_client_crm', $crm_data)) {
                $clue_data['crm_id'] = $this->db->insert_id();
                $this->db->insert('biz_client_crm_clue', $clue_data);
                $return['code'] = 1;
            } else {
                $return['msg'] = "Query failed！";
            }

            //返回操作结果
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        } else {
            $this->load->view('head');
            $this->load->view('biz/clue/add_clue');
        }
    }


    public function get_data(){
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "biz_shipment.booking_ETD";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

        $query_date = date('Y-m-d', strtotime("-3 month"));
        $query_week = date('Y-m-d', strtotime("-2 week"));
        $where = array();
        $where[] = "biz_shipment.created_time > '{$query_week}'";
        $where[] = "length(biz_shipment.matoufangxing) >3"; //20230414 crm群要求的
        $where[] = '(select country_code from biz_client where client_code = biz_shipment.client_code) <> (select country_code from biz_client where client_code = biz_shipment.client_code2)';
        $where[] = 'biz_shipment.client_code2 not in(select client_code from biz_shipment where booking_ETD > \''.$query_date.'\')';
        $where[] = 'biz_client.client_level != -1';
        $where[] = '( SELECT count( * ) FROM biz_client_crm WHERE company_search = biz_client.company_search ) < 2';

        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_crm_clue2', 'biz_crm_clue2_view');//获取相关的配置信息

        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];

                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') continue;
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) {
                        $this_join0 = explode(' ', $this_join[0]);
                        $title_data['sql_join'][end($this_join0)] = $this_join;
                    }

                    $where[] = search_like($f, $s, $v);
                }
            }
        }

        $where = join(' and ', $where);
        //where---end
        $offset = ($page - 1) * $rows;
        $this->db->limit($rows, $offset);
        //获取需要的字段--start
        $selects = array(
            'biz_client.company_search' => 'company_search'
        );
        foreach ($title_data['select_field'] as $key => $val) {
            if (!in_array($val, $selects)) $selects[] = $val;
        }

        //获取需要的字段--end
        $this->db->select(join(',', $selects), false);
        //这里拼接join数据
        foreach ($title_data['sql_join'] as $j) {
            $this->db->join($j[0], $j[1], $j[2]);
        }//将order字段替换为 对应的sql字段
        if (isset($title_data['sql_field']["biz_client_crm.{$sort}"])) {
            $sort = $title_data['sql_field']["biz_client_crm.{$sort}"];
        }
        $this->db->is_reset_select(false);//不重置查询
        //echo $this->db->where($where, null, false)->group_by('biz_shipment.client_code2')->order_by($sort, $order)->get_compiled_select('biz_shipment');exit;
        $query = $this->db->where($where, null, false)->group_by('biz_shipment.client_code2')->order_by($sort, $order)->get('biz_shipment');
        $rs = $query->result_array();
        $this->db->clear_limit();//清理下limit
        $result["total"] = $this->db->count_all_results('');
        $result['rows'] = $rs;

        return $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
     * 全部线索
     */
    public function clue_detail(){
        $client_code = $this->input->get('client_code');
        if (empty(trim($client_code))){
            return $this->load->view('/errors/html/error_layui', array('msg'=>'参数错误！', 'icon'=>'layui-icon-404'));
        }

        //创建浏览日志
        $row = $this->db->select('id, company_name')->where("client_code", $client_code)->get('biz_client')->row_array();
        if (!empty($row)) create_browse_log('clue2', $row['id'], $row['company_name']);

        //提取次数
        $this->db->select('(select count(*) from biz_client_crm where company_search = biz_client.company_search) as repeat_num');
        $repeat_num = $this->db->where('client_code', $client_code)->get('biz_client')->row_array()['repeat_num'];

        //联系人
        $contact = $this->db->where('client_code', $client_code)->get('biz_client_contact_list')->result_array();

        //交易记录
        $result = $this->db->select("*")
            ->where('client_code2', $client_code)
            ->order_by('booking_ETD', 'desc')
            ->get('biz_shipment')
            ->result_array();
        if (empty($result)) return $this->load->view('/errors/html/error_layui', array('msg'=>'暂无相关线索！', 'icon'=>'layui-icon-face-cry'));
        $this->load->vars('repeat_num', $repeat_num);
        $this->load->vars('data', $result);
        $this->load->vars('contact', $contact);
        $this->load->view('biz/clue/clue_detail');
    }

    /**
     * @title 提取线索——视图
     */
    public function gaining(){
        $clue_id = (int)$this->input->get('clue_id');
        if ($clue_id == 0){
            return $this->load->view('/errors/html/error_layui', array('msg'=>'参数错误！', 'icon'=>'layui-icon-404'));
        }

        $query_date = date('Y-m-d', strtotime("-3 month"));
        $where = array();
        $where[] = "biz_client.id = {$clue_id}";
        $where[] = "biz_shipment.client_code <> biz_shipment.client_code2";
        $where[] = "biz_shipment.client_code2 not in (select client_code from biz_shipment where booking_ETD > '{$query_date}' ) ";
        $data = $this->db->select('biz_client.*')->where(join(' and ', $where))
            ->join('biz_shipment', 'biz_shipment.client_code2 = biz_client.client_code')
            ->get('biz_client')->row_array();
        if (empty($data)){
            return $this->load->view('/errors/html/error_layui', array('msg'=>'线索不存在或不可提取！', 'icon'=>'layui-icon-404'));
        }

        $this->load->view('head');
        $this->load->view('biz/clue/gaining_view', $data);
    }

    /**
     * @提取线索——写入
     */
    public function gaining_data(){
        $return = array('code'=>0, 'msg'=>'');
        $_post = $this->input->post();
        $clue_id = (int)$this->input->get('clue_id');
        $client = $this->db->select('id')->where('id', $clue_id)->get('biz_client')->row_array();
        if (!$client){
            $return['msg'] = '参数错误！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
        }

        //校验提交
        $validate = $this->validate($_post, 'edit');
        if ($validate !== true){
            $return['msg'] = $validate;
            return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
        }

        if ($this->db->where('company_search', match_chinese($_post['company_name']))->count_all_results('biz_client_crm') >= 3){
            $return['msg'] = '超过可提取上限！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return, 256));
        }

        //创建写入字段
        $data = $this->create_date($_post);
        $data['sales_id'] = (int)$data['sales_id'] == 0 ? get_session('id') : $data['sales_id'];
        $data['company_search'] = match_chinese($data['company_name']);
        $data["approve_user"] = getUserField($data['sales_id'], 'leader_id');
        $data["created_group"] = implode(',', $this->session->userdata('group') );
        $data["created_by"] = $data["updated_by"] = get_session('id');
        $data["created_time"] = $data["updated_time"] = date('Y-m-d H:i:s');
        $data["apply_type"] = 1;
        $data["apply_status"] = 1;  //1表示为申请中
        $data["add_way"] = 'clue2';
        if ($this->db->where(array('company_search'=>$data['company_search'], 'sales_id'=>$data['sales_id']))->count_all_results('biz_client_crm') > 0){
            $return['msg'] = '销售“' . getUserName($data['sales_id']) . '”已提取过该线索，请不要重复提取！';
            return $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }
        //新增CRM数据
        if ($this->db->insert('biz_client_crm', $data)) {
            $insert_id = $this->db->insert_id();
            $return['code'] = 1;
            //创建客户流转日志
            $remark = '从“线索2”中提取，选择销售为:'.getUserName($data['sales_id']);
            create_crm_log($insert_id, 0, get_session('id'), $remark);
        } else {
            $return['msg'] = "Query failed！";
        }

        //返回操作结果
        return $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }

    /**
     * @title 过滤post提交
     * @param array $post
     * @return array
     */
    private function create_date($post = []){
        $field = ['country_code', 'client_name', 'company_name_en', 'company_search', 'company_name', 'company_address', 'created_group',
            'export_sailing', 'trans_mode', 'product_type', 'product_details', 'client_source', 'approve_user', 'role',
            'sales_id','province','city','area','web_url'];
        $data = [];
        foreach ($field as $val){
            $data[$val] = isset($post[$val])?$post[$val]:'';
        }
        return $data;
    }

    /**
     * @title 校验post提交
     * @param array $data
     * @return bool|string
     */
    private function validate($data = [], $event='insert'){
        //校验必填项
        $validate_field = [
            'company_name'=>'客户全称',
            'company_address'=>'客户地址',
            'country_code'=>'国家代码',
            'export_sailing'=>'出口航线',
            'trans_mode'=>'运输模式',
            'sales_id'=>'销售'
        ];
        foreach ($validate_field as $key => $val)
        {
            if (!isset($data[$key]) || $data[$key] == ''){
                return "{$val}必须填写！";
            }
        }
        //校验角色类型
        if ($check_company_role = check_company_role($data['company_name'], $data['role'])) {
            return "{$check_company_role}！";
        }

        //查询该公司是否3个月内有过交易，如果有则不允许添加或编辑
        $company_search = match_chinese($data['company_name']);
        $this->db->where('company_search', $company_search)->get('biz_client')->row_array();
        $this->db->select('biz_client.client_code,biz_client.company_search,biz_shipment.booking_ETD');
        $this->db->where('biz_client.company_search', $company_search);
        $this->db->join('biz_shipment', 'biz_shipment.client_code = biz_client.client_code and status="normal"', 'LEFT');
        $this->db->order_by('biz_shipment.booking_ETD', 'desc')->limit(1);
        $client = $this->db->get('biz_client')->row_array();
        if ($client){
            //如果3个月内有过交易
            if (strtotime('+3 month', strtotime($client['booking_ETD'])) > time()){
                return "“{$data['company_name']}”在3个月内有过交易记录！";
            }
        }
        return true;
    }
}