<?php

class bsc_upload extends Common_Controller
{
    private $allowed_types = array(
        '.jpg','.gif','.jpeg','.png','.pdf','.doc','.docx','.xlsx','.xls','.zip','.rar', '.rtf','.eml'
    );
    public $field_all =  array(
        array('id', 'id', '80', '1', "sortable:true"),
        array('file_name', 'file_name', '400', '2', 'formatter:file_name'),
        array('biz_table', 'biz_table', '0', '3', ''),
        array('type_name', 'type_name', '140', '4', "editor:{type:'combobox',options:{valueField:'value',
                                textField:'value',
                                url:'/bsc_dict/get_option/upload_file_type'}},"),
        array('id_no', 'id_no', '0', '5', ''),
        array('approve', 'approve', '0', '6', ''),
        array('create_time', 'create_time', '250', '20', 'sortable:true'),
        array('create_by', 'create_by', '80', '21', ''),
        array('update_time', 'update_time', '0', '22', ''),
        array('update_by', 'update_by', '0', '23', ''),
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('bsc_upload_model');
    }

    public function upload_files($biz_table = "notype", $id_no = "")
    {
        if($biz_table == 'notype')$biz_table = getValue('biz_table', "notype");
        if($id_no == "")$id_no = getValue('id_no', "");

        //添加类型
        $type = getValue('type', "");
        $data['type']=$type;


        if ($id_no == "") {
            echo "no documents";
            exit;
        }
        $data['biz_table'] = $biz_table;
        $data['id_no'] = $id_no;
        $data['type_name'] = isset($_GET['type_name']) ? $_GET['type_name'] : $data['type'];

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_auto_rate_table');
        if (!empty($rs['config_name'])){
            $data["f"] = json_decode($rs['config_text']);
        }else{
            $data["f"] = $this->field_all;
        }
        $lock_lv = 0;
        //2021-10-14 市场群 小明提出锁上后,上传通道打开, MARS已同意
        if($biz_table == 'biz_shipment'){
            $shipment = Model('biz_shipment_model')->get_duty_one("id = '{$id_no}'");
            // $lock_lv = $shipment['lock_lv'];
        }else if($biz_table == 'biz_consol'){
            $consol = Model('biz_consol_model')->get_duty_one("id = '{$id_no}'");
            // $lock_lv = $consol['lock_lv'];
        }
        $data['lock_lv'] = $lock_lv;
        $data['id'] = $id_no;
        $this->load->view('head');
        $this->load->view('bsc/upload/upload_files',$data);
    }

    public function get_data($biz_table = "notype", $id_no = ""){
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $where = '';
        $type_name = isset($_GET['type_name']) ? urldecode($_GET['type_name']) : '';

        //-------这个查询条件想修改成通用的 -------------------------------
        $where .= "biz_table = '$biz_table' and id_no = '$id_no'";
        if($type_name !== '')$where .= " AND type_name = '$type_name'";

        $f1 = isset($_REQUEST['f1']) ? ($_REQUEST['f1']) : '';
        $s1 = isset($_REQUEST['s1']) ? ($_REQUEST['s1']) : '';
        $v1 = isset($_REQUEST['v1']) ? ($_REQUEST['v1']) : '';
        $f2 = isset($_REQUEST['f2']) ? ($_REQUEST['f2']) : '';
        $s2 = isset($_REQUEST['s2']) ? ($_REQUEST['s2']) : '';
        $v2 = isset($_REQUEST['v2']) ? ($_REQUEST['v2']) : '';
        $f3 = isset($_REQUEST['f3']) ? ($_REQUEST['f3']) : '';
        $s3 = isset($_REQUEST['s3']) ? ($_REQUEST['s3']) : '';
        $v3 = isset($_REQUEST['v3']) ? ($_REQUEST['v3']) : '';
        if($v1!="") $where .= " and $f1 $s1 '$v1'";
        if($v2!="") $where .= " and $f2 $s2 '$v2'";
        if($v3!="") $where .= " and $f3 $s3 '$v3'";
        //-----------------------------------------------------------------
        $this->load->model("bsc_upload_model");
        $offset = ($page-1)*$rows;
        $result["total"] = $this->bsc_upload_model->total($where);
        $this->db->limit($rows, $offset);
        $rs = $this->bsc_upload_model->get($where,$sort,$order);
       
        $rows = array();
        foreach ($rs as $row){
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function delete_data(){

        $id = intval($_REQUEST['id']);
        $this->load->model("bsc_upload_model");
        $olddata = $this->bsc_upload_model->get_one('id', $id);

        $del_file = $this->file_del($olddata['file_name']);

        if($del_file){
            $this->bsc_upload_model->mdelete($id);

            //调用文件处理后对应表upload_so等字段的处理函数
            $this->file_handle($olddata, 0);
            echo json_encode(array('success'=>true));
        }

    }

    public function update_data(){
        $id = $_REQUEST["id"];
        $old_row = $this->bsc_upload_model->get_one('id', $id);

        $field = array('type_name');

        $data = array();
        foreach ($field as $item) {
            if(isset($_POST[$item])){
                $temp = ts_replace(trim($_POST[$item]));
                if(in_array($item, $field) && !empty($temp)) $data[$item] = $temp;
            }

        }

        if(empty($data)){
            echo json_encode(array());
            return;
        }
        $id = $this->bsc_upload_model->update($id, $data);
        $data['id'] = $id;
        // record the log
        $log_data = array();
        $log_data["table_name"] = "bsc_upload";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        echo json_encode($data);
    }

    public function file_del($file_name = ''){
        if ($this->session->userdata('id') == '') {
            redirect('main/login', 'refresh');
        }
        $dirurl = dirname(dirname(dirname(__FILE__)));
        @unlink($dirurl . '/upload/' . $file_name);
        return true;
    }

    /**
     * 文件类型验证
     */
    public function file_type_verify($file){
        $allowed_types = $this->allowed_types;
        $file_type = strtolower(strrchr($file['name'],'.'));

        if (in_array($file_type,$allowed_types)){
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * 进行文件的处理后，对应表的字段处理
     */
    public function file_handle($data,$type = 0){
        $table = array(
            'biz_shipment',
            'biz_consol',
        );

        //往来款审核时,如果上传了付款凭证,这里修改一下审核的数据
        if($data['biz_table'] == 'biz_bill_payment_wlk_apply' && $data['type_name'] == 'payment_voucher'){
            Model('biz_bill_payment_wlk_apply_model');
            $this->biz_bill_payment_wlk_apply_model->update($data['id_no'], array('upload_payment_voucher' => 1));
            return true;
        }

        //判断是否是可修改的表
        if(in_array($data['biz_table'],$table)){
            //查询该shipment的对应type_name是否还有文件
            //type == 0 为删除文件后的处理
            if($type == 0){
                $upload_file = $this->bsc_upload_model->get_one_where("type_name = '" . $data['type_name'] . "' and biz_table = '" . $data['biz_table'] . "' and id_no = '" . $data['id_no'] . "'");

                if(!empty($upload_file)){
                    return false;
                }
            }
            return true;
        }else{

            return false;
        }
    }

    /**
     * 存储文件的函数
     * @param string $type
     * @param $name
     * @param $path
     * @param $file
     * @return bool
     */
    private function setFile($type = 'local', $name, $path, $file){
        if($type == 'local'){// 存储到本地
            if (!file_exists(UPLOAD_FILE)) {
                mkdir(UPLOAD_FILE, 0777, true);
            }
            $new_dir = UPLOAD_FILE . '/' . $path . '/';
            if (!file_exists($new_dir)) {
                mkdir($new_dir, 0777, true);
            }
            $new_path = $new_dir . $name;
            $new_path = str_replace('\\', DIRECTORY_SEPARATOR, $new_path);
            $new_path = str_replace('/', DIRECTORY_SEPARATOR, $new_path);

            if(is_string($file)){
                file_put_contents($new_path, $file);
                return true;
            }else{
                $file['tmp_name'] = iconv("UTF-8","gb2312", $file['tmp_name']);
                //判断tmp_name是否存在,如果是字符串,那么这里直接存
                return move_uploaded_file($file['tmp_name'], $new_path);
            }
        }else if($type == 'ali'){// 存储到阿里云OSS
            $accessKeyId = "LTAIcNVHORoWCXf7";
            $accessKeySecret = "I59vWVf5jhkhgtPGGSs5WAatS7DG9r";
            $endpoint = "http://oss-cn-shanghai.aliyuncs.com";
            $bucket= "opchina";

            require_once  './application/libraries/oss_sdk/autoload.php';

            // 文件名称
            $object = "upload/{$path}/$name";


            if(is_string($file)){
                $content = $file;
            }else{
                $content = file_get_contents($file['tmp_name']);
            }

            $options = array(
                //这个没法直接用的,后续得看看怎么改,只是官方的例子---TODO
//                OSS\OssClient::OSS_HEADERS => array(
//                    'x-oss-object-acl' => 'private',
//                    'x-oss-meta-info' => 'your info'
//                ),
            );

            try{
                $ossClient = new OSS\OssClient($accessKeyId, $accessKeySecret, $endpoint);
                $ossClient->putObject($bucket, $object, $content, $options);
                return true;
            } catch(OSS\OssException $e) {
                return false;
            }
        }
    }
    
    public function wb_file_upload(){
        header("Access-Control-Allow-Origin:*");
        header('Access-Control-Allow-Methods:POST, GET, OPTIONS');
        header('Access-Control-Allow-Credentials:true');
        $this->file_upload();
    }
    
    /**
     * 对外的上传接口
     */
    public function file_upload(){
        $user_id = get_session('id') ? get_session('id') : getValue('user_id', 0);
        $biz_table = isset($_POST["biz_table"]) ? $_POST["biz_table"] : "";
        $id_no = isset($_POST["id_no"]) ? $_POST["id_no"] : "";

        //2022-07-11 判断是否有逗号
        $ids = explode(',',$id_no);
        $id_no=$ids['0'];

        $type_name = isset($_POST["type_name"]) ? $_POST["type_name"] : "";
        $up_location = getValue('up_location', 'local');
        $is_save_data = isset($_POST["is_save_data"]) ? (int)$_POST["is_save_data"] : 1;//2021-11-29 新增只上传图片,不加入数据库的功能
        if($is_save_data && $type_name == ''){
            echo json_encode(array('code' => 1,'msg' => '请选择文件类型'));
            return;
        }
        if($_FILES['file']['error']){
            switch($_FILES['file']['error']){
                case 1:	// UPLOAD_ERR_INI_SIZE
                    $error = 'upload_file_exceeds_limit';
                    break;
                case 2: // UPLOAD_ERR_FORM_SIZE
                    $error = 'upload_file_exceeds_form_limit';
                    break;
                case 3: // UPLOAD_ERR_PARTIAL
                    $error = 'upload_file_partial';
                    break;
                case 4: // UPLOAD_ERR_NO_FILE
                    $error = 'upload_no_file_selected';
                    break;
                case 6: // UPLOAD_ERR_NO_TMP_DIR
                    $error = 'upload_no_temp_directory';
                    break;
                case 7: // UPLOAD_ERR_CANT_WRITE
                    $error = 'upload_unable_to_write_file';
                    break;
                case 8: // UPLOAD_ERR_EXTENSION
                    $error = 'upload_stopped_by_extension';
                    break;
                default :   $error = 'upload_no_file_selected';
                    break;
            }
            echo json_encode(array('code' => 3, 'msg' => $error));
            die;
        }
        //接收的excel信息
        $info = pathinfo($_FILES['file']['name']);

        $file = $_FILES['file'];
        //验证excel文件类型
        $res = $this->file_type_verify($file);
        if(!$res) {
            echo json_encode(array('code' => 1,'msg' => '该文件类型不允许上传'));
        }else{
            //2022-05-11 文件名需要将一些符号过滤掉
            $file['name'] = ts_replace($file['name']);
            
            $new_name = strstr($file['name'], '.', true) . '-' . time() . rand(1000,9999) .  '.' . $info['extension'];

            $path = date('Ymd');

            //存储文件
            $is_set = $this->setFile($up_location, $new_name, $path, $file);
            if(!$is_set) return jsonEcho(array('code' => 1, 'msg' => '上传服务器出现异常,请联系管理员')); //如果存储失败

            if(!$is_save_data){//如果不保存数据,那么直接返回保存的地址
                echo json_encode(array('code' => 0, 'errno' => 0,'msg' => '上传成功', 'data' => array(array('url' => '/upload/' . $path . '/' . $new_name, 'alt' => '', 'href' => ''))));
                return;
            }
            $newdata = array(
                "file_name" => '/' . $path . '/' . $new_name,
                "biz_table" => $biz_table,
                "id_no" => $id_no,
                "approve" => 1,
                'type_name' => $type_name,
            );
            $this->load->model("bsc_upload_model");
            $id = $this->bsc_upload_model->save_auto($newdata, $user_id);

            //2022-07-11判断是否存在多个
            if(count($ids)>1){
                //获取数据
                $datas=$this->db->where('id',$id)->get('bsc_upload')->row_array();
                foreach ($ids as $k => $v){
                    //不能为第一个
                    if($k!='0'){
                        //新增文件
                        $datas['id_no']=$v;
                        unset($datas['id']);
                        $this->bsc_upload_model->save_auto($datas, $user_id);
                        //修改表
                        $data_apply=array('upload_payment_voucher'=>'1');
                        $this->db->where('id',$v)->update('biz_bill_payment_wlk_apply',$data_apply);
                    }
                }
            }


            //根据上传的是否是正本来修改shipment的申领时间
            if($biz_table == 'biz_consol' && in_array($type_name, array('B/L original', 'B/L telex release', 'B/L Seaway'))){
                Model('biz_shipment_model');
                Model('biz_shipment_billoflading_model');
                Model('biz_consol_billoflading_model');


                //发送一封邮件任务
                $this->db->select("id, job_no, cus_no,hbl_type, consol_id, (select operator_id from biz_duty_new where biz_duty_new.biz_table = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id) as operator_id");
                $shipments = array_column($this->biz_shipment_model->get_shipment_by_consol($id_no), null, 'id');
                $shipment_ids_array = array_column($shipments, 'id');
                
                //在MBL的情况下，而且上传的是电放提单，则到shipment勾一下签单勾，非MBL不勾
                foreach($shipment_ids_array as $shipment_id){
                    if($shipments[$shipment_id]['hbl_type']=='MBL' && in_array($type_name, array('B/L telex release', 'B/L Seaway'))){
                        $q_data = array("tidanqianfa" => date("Y-m-d H:i:s"));
                        $this->biz_shipment_model->update($shipment_id,$q_data);
                        // record the log
                        $log_data = array();
                        $log_data["table_name"] = "biz_shipment";
                        $log_data["key"] = $shipment_id;
                        $log_data["action"] = "update";
                        $log_data["value"] = json_encode($q_data);
                        log_rcd($log_data);
                    }
                }
                
                if(!empty($shipments)){
                    $consol_ids = array_column($shipments, 'consol_id');
                    $consol_id = $consol_ids[0];
                    $billoflading = array_column($this->biz_consol_billoflading_model->get("consol_id = {$consol_id}"), null, 'consol_id');
                    $this->load->library('Mail'); 
                    $op_s = array_column($shipments, 'operator_id');
                    $receive ="";
                    $CCs = array();
                    foreach ($op_s as $op){
                        $CCs[] = getUserField($op, 'email');
                        $receive = getUserField($op, 'email');
                    }
                    $CCs = join(',', $CCs);
                    $subject = '提单已拿回 相关SHIPMENT请看内容';
                    $body = '';
                    foreach ($shipment_ids_array as $shipment_id){
                        $this_shipment = isset($shipments[$shipment_id]) ? $shipments[$shipment_id] : array();
                        //发送一条邮件任务给操作
                        if(!empty($this_shipment) && !empty($this_shipment['operator_id'])){
                            $txt = $this_shipment['job_no'] . ' ' . $this_shipment['cus_no'] . " 提单已拿回";
                            $body .= $txt . '<br/>';
                            add_market_mail($this_shipment['operator_id'] , $txt, $txt, '<a href="/biz_consol/edit/' . $id_no . '" target="_blank">点此查看</a>', '操作提醒', '', true);
                        }
                    }
                    if(!empty($shipment_ids_array)){
                        if($receive == $CCs) $CCs = "";
                        add_mail($receive, $subject, $body, $CCs); 
                    }
                    //存在修改,不存在新增
                    $this_data = array();
                    $this_data['get_time'] = date('Y-m-d H:i:s');
                    if(isset($billoflading[$consol_id])){
                        foreach ($this_data as $key => $val){
                            //判断是否相等,相等排除
                            if(isset($billoflading[$key]) && $val == $billoflading[$key]){
                                unset($this_data[$key]);
                            }
                        }
                        $action = 'update';
                        $this_billoflading_id = $billoflading[$consol_id]['id'];

                        $this->biz_consol_billoflading_model->update($this_billoflading_id, $this_data);
                    }else{
                        $this_data['consol_id'] = $consol_id;
                        $this_data['remark'] = '';

                        $action = 'insert';
                        $this_billoflading_id = $this->biz_consol_billoflading_model->save($this_data);
                    }

                    // record the log
                    $log_data = array();
                    $log_data["table_name"] = "biz_consol_billoflading";
                    $log_data["key"] = $this_billoflading_id;
                    $log_data["action"] = $action;
                    $log_data["value"] = json_encode($this_data);
                    log_rcd($log_data);
                }
            }

            //调用文件处理后对应表upload_so等字段的处理函数
            $this->file_handle($newdata, 1);
            return jsonEcho(array('code' => 0,'msg' => '上传成功','data' => array('id' => $id)));

        }
    }

    /**
     * 获取下载链接
     */
    public function get_url($type= 'local', $path = ''){
        if ($type == 'local') {//如果是本地
            return base_url().trim($path, '/');
        }else if($type == 'ali'){
            return "https://opchina.oss-cn-shanghai.aliyuncs.com{$path}";
        }else{
            return $path;
        }
    }
    /**
     * 阿里云OSS判断文件是否存在
     */
    private function aliOssFileExist($object){
        $accessKeyId = "LTAIcNVHORoWCXf7";
        $accessKeySecret = "I59vWVf5jhkhgtPGGSs5WAatS7DG9r";
        $endpoint = "http://oss-cn-shanghai.aliyuncs.com";
        $bucket= "opchina";

        require_once  APPPATH . '/libraries/oss_sdk/autoload.php';

        // 文件名称
        try{
            $ossClient = new OSS\OssClient($accessKeyId, $accessKeySecret, $endpoint);
            return $ossClient->doesObjectExist($bucket, $object);
        } catch(OSS\OssException $e) {
            return false;
        }
    }

    public function get_files($type_name = '', $biz_table = '', $id_no = ''){
        $where = array();

        //-----------where----start--------------
        if($type_name != '') $where[] = "type_name = '$type_name'";
        if($biz_table != '' && $id_no != '') $where[] = "biz_table = '$biz_table' and id_no = $id_no";

        $where = join(' and ', $where);
        //-----------where-----end-------------

        $this->db->select('id,file_name');
        $files = $this->bsc_upload_model->get($where);
        echo json_encode($files);
    }
}

