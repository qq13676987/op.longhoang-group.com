<?php

class biz_auto_rate extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('op_model');
        $this->model = $this->op_model;
        $this->model->set_table('biz_auto_rate');
        $this->db = $this->model->get_db();
    }
    
    public function local_table()
    {
        $data = array();
        //获取参数
        $data['trans_origin'] = $_GET['trans_origin'];
        $data['trans_carrier'] = $_GET['trans_carrier'];
        $data['booking_agent'] = $_GET['booking_agent'];
        $data['sailing_area'] = $_GET['sailing_area'];

        //获取区域中文
        $sailing_area_cn=$this->db->where("sailing_area",$_GET['sailing_area'])->where("length(port_code)=5")->group_by('sailing_area')->get('biz_port')->row_array();
        $data['sailing_area_cn'] =isset($sailing_area_cn['sailing'])?$sailing_area_cn['sailing']:'';

        $title=isset($_GET['title'])?$_GET['title']:'';
        if(empty($title)){
            $data['title']='普货(GC)';
        }else{
            $data['title']=$title;
        }

        $sailing_area = "sailing_area like '%{$_GET['sailing_area']}%'";

        //获取普货的箱型
        $GC = $this->db->where('trans_origin', $_GET['trans_origin'])->where('trans_carrier', $_GET['trans_carrier'])->where('booking_agent', $_GET['booking_agent'])->where($sailing_area)->where('goods_type', 'GC')->where('number_type', '1')->where('length(box_type)>2')->order_by('box_type', 'asc')->get('biz_auto_rate')->result_array();
        $GC = array_column($GC, 'box_type');
        $GC = array_unique($GC);
        $GP20='';
        $GP40='';
        $HQ40='';
        //指定排在前面20GP, 40GP, 40HQ
        foreach ($GC as $k => $v){
            if($v=='20GP'){
                unset($GC[$k]);
                $GP20='1';
            }
            if($v=='40GP'){
                unset($GC[$k]);
                $GP40='1';
            }
            if($v=='40HQ'){
                unset($GC[$k]);
                $HQ40='1';
            }
        }
        if(!empty($HQ40)){
            array_unshift($GC,'40HQ');
        }
        if(!empty($GP40)){
            array_unshift($GC,'40GP');
        }
        if(!empty($GP20)){
            array_unshift($GC,'20GP');
        }
        $data['GC'] = $GC;

        //获取危险品的箱型
        $DR = $this->db->where('trans_origin', $_GET['trans_origin'])->where('trans_carrier', $_GET['trans_carrier'])->where('booking_agent', $_GET['booking_agent'])->where($sailing_area)->where('goods_type', 'DR')->where('number_type', '1')->where('length(box_type)>2')->order_by('box_type', 'asc')->get('biz_auto_rate')->result_array();
        $DR = array_column($DR, 'box_type');
        $DR = array_unique($DR);
        $GP20='';
        $GP40='';
        $HQ40='';
        //指定排在前面20GP, 40GP, 40HQ
        foreach ($DR as $k => $v){
            if($v=='20GP'){
                unset($DR[$k]);
                $GP20='1';
            }
            if($v=='40GP'){
                unset($DR[$k]);
                $GP40='1';
            }
            if($v=='40HQ'){
                unset($DR[$k]);
                $HQ40='1';
            }
        }
        if(!empty($HQ40)){
            array_unshift($DR,'40HQ');
        }
        if(!empty($GP40)){
            array_unshift($DR,'40GP');
        }
        if(!empty($GP20)){
            array_unshift($DR,'20GP');
        }
        $data['DR'] = $DR;

        $this->load->view('head');
        $this->load->view('biz/auto_rate/local_table', $data);
    }
    
    public function local_get_data()
    {

        $result = array();
        /* $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
         $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 1000;*/
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = array();
//        $is_must = isset($_GET['is_must']) ? $_GET['is_must'] : 1;
//        $where[] = "is_must = '$is_must'";
        //获取搜索条件
        $goods_type = isset($_GET['goods_type']) ? $_GET['goods_type'] : '';
        if (!empty($goods_type)) {
            $where[] = "goods_type = '$goods_type'";
        }

        $trans_origin = isset($_GET['trans_origin']) ? $_GET['trans_origin'] : '';
        if (!empty($trans_origin)) {
            $where[] = "trans_origin = '$trans_origin'";
        }

        $trans_carrier = isset($_GET['trans_carrier']) ? $_GET['trans_carrier'] : '';
        if (!empty($trans_carrier)) {
            $where[] = "trans_carrier = '$trans_carrier'";
        }

        //(select client_name from biz_client where biz_client.client_code = biz_auto_rate.trans_carrier)

        $booking_agent = isset($_GET['booking_agent']) ? $_GET['booking_agent'] : '';
        if (!empty($booking_agent)) {
            $where[] = "booking_agent = '$booking_agent'";
        }

        $sailing_area = isset($_GET['sailing_area']) ? $_GET['sailing_area'] : '';
        if (!empty($sailing_area)) {
            $where[] = "(sailing_area like '%$sailing_area%' or sailing_area='')";
        }

        $boxes_all = isset($_GET['boxes']) ? $_GET['boxes'] : ''; 

        $where[] = "is_special = 0";

        $where = join(' and ', $where);

        /*$offset = ($page-1)*$rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);*/
        $this->db->select('biz_auto_rate.*' .
            ',(select company_name from biz_client where biz_client.client_code = biz_auto_rate.client) as client_name' .
            ',(select company_name from biz_client where biz_client.client_code = biz_auto_rate.client_code) as client_code_name' .
            ',(select company_name from biz_client where biz_client.client_code = biz_auto_rate.booking_agent) as booking_agent_name' .
            ',(select client_name from biz_client where biz_client.client_code = biz_auto_rate.trans_carrier) as trans_carrier_name');
        $rs = $this->model->get($where, $sort, $order);
        $arr = array();
        foreach ($rs as $k => $v) { 
            if ($v['currency'] == 'CNY') {
                $v['currency'] = '￥';
            } else if ($v['currency'] == 'USD') {
                $v['currency'] = '$';
            }
            if ( $v['number_type']==2) { 
                $arr[$v['charge'].','.$v['is_must'].','.$v['start_date'].','.$v['client_code']][] = array(
                    'id' => $v['id'],
                    'number_type' => $v['number_type'],
                    'box_type' => '票',
                    'payment' => $v['payment'],
                    'currency' => $v['currency'],
                    'unit_price' => $v['unit_price'],
                ); 
            }else{ 
                if(strlen(($v['box_type']))<3) $v['box_type'] = $boxes_all;
                $boxes = explode(",",$v['box_type']);
                foreach($boxes as $box){
                    $arr[$v['charge'].','.$v['is_must'].','.$v['start_date'].','.$v['client_code']][] = array(
                        'id' => $v['id'],
                        'number_type' => $v['number_type'],
                        'box_type' => $box,
                        'payment' => $v['payment'],
                        'currency' => $v['currency'],
                        'unit_price' => $v['unit_price'],
                    ); 
                }
            }
        }
        $this->load->model("m_model");
        foreach ($arr as $k => $v) {

            $new=array();
            foreach($v as $k1=>$v1){
                $new[$v1['box_type']][]=$v1;
            }

//            var_dump($new);

//            $new = array_column($v, NULL, 'box_type');


            foreach ($new as $k2 => $v2) {

                foreach ($v2 as $k3 => $v3){
//                    var_dump($v3);die;
                    // if($v1['payment']=='') $v1['payment']='PP+CC';
                    $new[$k2][$k3] = "<a onclick='edit(" . $v3['id'] . ")'><span style='color: red;margin-left: 10px;'>" . $v3['currency'] . "</span>&nbsp;<span style='color: red;margin-right: 10px;'>" . $v3['unit_price'] ."</span><span style='color:#009688;font-weight: bold;margin-right: 10px;'>" . $v3['payment'] . "</span></a>";
                }


            }
            $k_array = explode(',',$k);
            $new['must'] = $k_array[1] == "0"?"<span style='color:green;'>可选</span>":"<span style='color:red;'>必选</span>";
            $new['charge'] = $k_array[0];
            $new['start_date'] = $k_array[2];
            $new['client_code'] = $this->db->where('client_code',$k_array[3])->get('biz_client')->row_array()['company_name'];
            $arr[$k] = $new;
        }

        $arr = array_values($arr);

//        var_dump($arr);die;

        $result["rows"] = $arr;
        echo json_encode($result);
    }
}