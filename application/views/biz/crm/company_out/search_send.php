<?php

$province_arr = array(

    'ZJ'=>'浙江省',
    'SH'=>'上海市',
    'GD'=>'广东省',
    'JS'=>'江苏省',
    'AH'=>'安徽省',
    'XZ'=>'西藏自治区',
    'BJ'=>'北京市',
    'SD'=>'山东省',
    'SC'=>'四川省',
    'FJ'=>'福建省',
    'HB'=>'河北省',
    'TJ'=>'天津市',
    'GZ'=>'贵州省',
    'GX'=>'广西壮族自治区',
    'SX'=>'山西省',
    'YN'=>'云南省',
    'XJ'=>'新疆维吾尔自治区',
    'NMG'=>'内蒙古自治区',
    'HLJ'=>'黑龙江省',
    'GS'=>'甘肃省',
    'JL'=>'吉林省',
    'SAX'=>'陕西省',
    'CQ'=>'重庆市',
    'HUB'=>'湖北省',
    'HEN'=>'河南省',
    'HUN'=>'湖南省',
    'JX'=>'江西省',
    'LN'=>'辽宁省',
    'HAIN'=>'海南省',
    'QH'=>'青海省',
    'NX'=>'宁夏回族自治区',
    'HK'=>'香港特别行政区',
    'TW'=>'台湾',
);
$city_arr = array(
    'ZJ'=>array(),
    'SH'=>array(),
    'GD'=>array(),
    'JS'=>array(),
    'AH'=>array(),
    'XZ'=>array(),
    'BJ'=>array(),
    'SD'=>array(),
    'SC'=>array(),
    'FJ'=>array(),
    'HB'=>array(),
    'TJ'=>array(),
    'GZ'=>array(),
    'GX'=>array(),
    'SX'=>array(),
    'YN'=>array(),
    'XJ'=>array(),
    'NMG'=>array(),
    'HLJ'=>array(),
    'GS'=>array(),
    'JL'=>array(),
    'SAX'=>array(),
    'CQ'=>array(),
    'HUB'=>array(),
    'HEN'=>array(),
    'HUN'=>array(),
    'JX'=>array(),
    'LN'=>array(),
    'HAIN'=>array(),
    'QH'=>array(),
    'NX'=>array(),
    'HK'=>array(),
    'TW'=>array(),
);
array_push($city_arr['ZJ'],'嘉兴市');
array_push($city_arr['ZJ'],'宁波市');
array_push($city_arr['ZJ'],'金华市');
array_push($city_arr['ZJ'],'杭州市');
array_push($city_arr['ZJ'],'绍兴市');
array_push($city_arr['ZJ'],'衢州市');
array_push($city_arr['ZJ'],'湖州市');
array_push($city_arr['ZJ'],'温州市');
array_push($city_arr['ZJ'],'丽水市');
array_push($city_arr['ZJ'],'舟山市');
array_push($city_arr['ZJ'],'台州市');
array_push($city_arr['YN'],'西双版纳傣族自治州');
array_push($city_arr['YN'],'红河哈尼族彝族自治州');
array_push($city_arr['YN'],'文山壮族苗族自治州');
array_push($city_arr['YN'],'德宏傣族景颇族自治州');
array_push($city_arr['YN'],'大理白族自治州');
array_push($city_arr['YN'],'怒江傈僳族自治州');
array_push($city_arr['YN'],'迪庆藏族自治州');
array_push($city_arr['YN'],'昆明市');
array_push($city_arr['YN'],'曲靖市');
array_push($city_arr['YN'],'昭通市');
array_push($city_arr['YN'],'保山市');
array_push($city_arr['YN'],'临沧市');
array_push($city_arr['YN'],'楚雄彝族自治州');
array_push($city_arr['YN'],'普洱市');
array_push($city_arr['YN'],'丽江市');
array_push($city_arr['YN'],'玉溪市');
array_push($city_arr['XZ'],'拉萨市');
array_push($city_arr['XZ'],'日喀则市');
array_push($city_arr['XZ'],'那曲市');
array_push($city_arr['XZ'],'阿里地区');
array_push($city_arr['XZ'],'山南市');
array_push($city_arr['XZ'],'林芝市');
array_push($city_arr['XZ'],'昌都市');
array_push($city_arr['XZ'],'-');
array_push($city_arr['SC'],'成都市');
array_push($city_arr['SC'],'乐山市');
array_push($city_arr['SC'],'泸州市');
array_push($city_arr['SC'],'眉山市');
array_push($city_arr['SC'],'绵阳市');
array_push($city_arr['SC'],'自贡市');
array_push($city_arr['SC'],'德阳市');
array_push($city_arr['SC'],'南充市');
array_push($city_arr['SC'],'内江市');
array_push($city_arr['SC'],'攀枝花市');
array_push($city_arr['SC'],'遂宁市');
array_push($city_arr['SC'],'广元市');
array_push($city_arr['SC'],'宜宾市');
array_push($city_arr['SC'],'资阳市');
array_push($city_arr['SC'],'广安市');
array_push($city_arr['SC'],'凉山彝族自治州');
array_push($city_arr['SC'],'达州市');
array_push($city_arr['SC'],'雅安市');
array_push($city_arr['SC'],'巴中市');
array_push($city_arr['SC'],'甘孜藏族自治州');
array_push($city_arr['SC'],'阿坝藏族羌族自治州');
array_push($city_arr['XJ'],'乌鲁木齐市');
array_push($city_arr['XJ'],'昌吉回族自治州');
array_push($city_arr['XJ'],'博尔塔拉蒙古自治州');
array_push($city_arr['XJ'],'阿克苏地区');
array_push($city_arr['XJ'],'自治区直辖县级行政区划');
array_push($city_arr['XJ'],'伊犁哈萨克自治州');
array_push($city_arr['XJ'],'喀什地区');
array_push($city_arr['XJ'],'和田地区');
array_push($city_arr['XJ'],'吐鲁番市');
array_push($city_arr['XJ'],'塔城地区');
array_push($city_arr['XJ'],'克拉玛依市');
array_push($city_arr['XJ'],'巴音郭楞蒙古自治州');
array_push($city_arr['XJ'],'哈密市');
array_push($city_arr['XJ'],'阿勒泰地区');
array_push($city_arr['XJ'],'克孜勒苏柯尔克孜自治州');
array_push($city_arr['TJ'],'天津市');
array_push($city_arr['SH'],'上海市');
array_push($city_arr['SX'],'太原市');
array_push($city_arr['SX'],'长治市');
array_push($city_arr['SX'],'晋中市');
array_push($city_arr['SX'],'临汾市');
array_push($city_arr['SX'],'晋城市');
array_push($city_arr['SX'],'大同市');
array_push($city_arr['SX'],'吕梁市');
array_push($city_arr['SX'],'运城市');
array_push($city_arr['SX'],'朔州市');
array_push($city_arr['SX'],'阳泉市');
array_push($city_arr['SX'],'忻州市');
array_push($city_arr['SX'],'-');
array_push($city_arr['SD'],'莱芜市');
array_push($city_arr['SD'],'德州市');
array_push($city_arr['SD'],'临沂市');
array_push($city_arr['SD'],'潍坊市');
array_push($city_arr['SD'],'日照市');
array_push($city_arr['SD'],'威海市');
array_push($city_arr['SD'],'菏泽市');
array_push($city_arr['SD'],'济宁市');
array_push($city_arr['SD'],'聊城市');
array_push($city_arr['SD'],'滨州市');
array_push($city_arr['SD'],'泰安市');
array_push($city_arr['SD'],'青岛市');
array_push($city_arr['SD'],'烟台市');
array_push($city_arr['SD'],'济南市');
array_push($city_arr['SD'],'东营市');
array_push($city_arr['SD'],'淄博市');
array_push($city_arr['SD'],'枣庄市');
array_push($city_arr['SAX'],'西安市');
array_push($city_arr['SAX'],'宝鸡市');
array_push($city_arr['SAX'],'铜川市');
array_push($city_arr['SAX'],'安康市');
array_push($city_arr['SAX'],'咸阳市');
array_push($city_arr['SAX'],'榆林市');
array_push($city_arr['SAX'],'渭南市');
array_push($city_arr['SAX'],'延安市');
array_push($city_arr['SAX'],'汉中市');
array_push($city_arr['SAX'],'商洛市');
array_push($city_arr['GX'],'防城港市');
array_push($city_arr['GX'],'北海市');
array_push($city_arr['GX'],'柳州市');
array_push($city_arr['GX'],'梧州市');
array_push($city_arr['GX'],'南宁市');
array_push($city_arr['GX'],'来宾市');
array_push($city_arr['GX'],'钦州市');
array_push($city_arr['GX'],'贺州市');
array_push($city_arr['GX'],'百色市');
array_push($city_arr['GX'],'贵港市');
array_push($city_arr['GX'],'桂林市');
array_push($city_arr['GX'],'崇左市');
array_push($city_arr['GX'],'河池市');
array_push($city_arr['GX'],'玉林市');
array_push($city_arr['GS'],'兰州市');
array_push($city_arr['GS'],'天水市');
array_push($city_arr['GS'],'金昌市');
array_push($city_arr['GS'],'张掖市');
array_push($city_arr['GS'],'武威市');
array_push($city_arr['GS'],'平凉市');
array_push($city_arr['GS'],'庆阳市');
array_push($city_arr['GS'],'酒泉市');
array_push($city_arr['GS'],'定西市');
array_push($city_arr['GS'],'陇南市');
array_push($city_arr['GS'],'白银市');
array_push($city_arr['GS'],'甘南藏族自治州');
array_push($city_arr['GS'],'临夏回族自治州');
array_push($city_arr['GS'],'嘉峪关市');
array_push($city_arr['GS'],'-');
array_push($city_arr['GD'],'东莞市');
array_push($city_arr['GD'],'深圳市');
array_push($city_arr['GD'],'广州市');
array_push($city_arr['GD'],'湛江市');
array_push($city_arr['GD'],'茂名市');
array_push($city_arr['GD'],'珠海市');
array_push($city_arr['GD'],'惠州市');
array_push($city_arr['GD'],'河源市');
array_push($city_arr['GD'],'中山市');
array_push($city_arr['GD'],'阳江市');
array_push($city_arr['GD'],'佛山市');
array_push($city_arr['GD'],'梅州市');
array_push($city_arr['GD'],'清远市');
array_push($city_arr['GD'],'江门市');
array_push($city_arr['GD'],'汕尾市');
array_push($city_arr['GD'],'肇庆市');
array_push($city_arr['GD'],'潮州市');
array_push($city_arr['GD'],'韶关市');
array_push($city_arr['GD'],'汕头市');
array_push($city_arr['GD'],'云浮市');
array_push($city_arr['GD'],'揭阳市');
array_push($city_arr['FJ'],'厦门市');
array_push($city_arr['FJ'],'莆田市');
array_push($city_arr['FJ'],'漳州市');
array_push($city_arr['FJ'],'泉州市');
array_push($city_arr['FJ'],'三明市');
array_push($city_arr['FJ'],'福州市');
array_push($city_arr['FJ'],'龙岩市');
array_push($city_arr['FJ'],'宁德市');
array_push($city_arr['FJ'],'南平市');
array_push($city_arr['CQ'],'重庆市');
array_push($city_arr['CQ'],'县');
array_push($city_arr['BJ'],'北京市');
array_push($city_arr['AH'],'合肥市');
array_push($city_arr['AH'],'滁州市');
array_push($city_arr['AH'],'马鞍山市');
array_push($city_arr['AH'],'安庆市');
array_push($city_arr['AH'],'芜湖市');
array_push($city_arr['AH'],'蚌埠市');
array_push($city_arr['AH'],'宿州市');
array_push($city_arr['AH'],'池州市');
array_push($city_arr['AH'],'阜阳市');
array_push($city_arr['AH'],'黄山市');
array_push($city_arr['AH'],'宣城市');
array_push($city_arr['AH'],'六安市');
array_push($city_arr['AH'],'淮南市');
array_push($city_arr['AH'],'亳州市');
array_push($city_arr['AH'],'铜陵市');
array_push($city_arr['AH'],'淮北市');
array_push($city_arr['HUN'],'长沙市');
array_push($city_arr['HUN'],'益阳市');
array_push($city_arr['HUN'],'株洲市');
array_push($city_arr['HUN'],'怀化市');
array_push($city_arr['HUN'],'邵阳市');
array_push($city_arr['HUN'],'娄底市');
array_push($city_arr['HUN'],'永州市');
array_push($city_arr['HUN'],'岳阳市');
array_push($city_arr['HUN'],'常德市');
array_push($city_arr['HUN'],'衡阳市');
array_push($city_arr['HUN'],'湘潭市');
array_push($city_arr['HUN'],'张家界市');
array_push($city_arr['HUN'],'郴州市');
array_push($city_arr['HUN'],'湘西土家族苗族自治州');
array_push($city_arr['HUB'],'武汉市');
array_push($city_arr['HUB'],'襄阳市');
array_push($city_arr['HUB'],'咸宁市');
array_push($city_arr['HUB'],'十堰市');
array_push($city_arr['HUB'],'荆门市');
array_push($city_arr['HUB'],'黄石市');
array_push($city_arr['HUB'],'随州市');
array_push($city_arr['HUB'],'宜昌市');
array_push($city_arr['HUB'],'鄂州市');
array_push($city_arr['HUB'],'荆州市');
array_push($city_arr['HUB'],'省直辖县级行政区划');
array_push($city_arr['HUB'],'孝感市');
array_push($city_arr['HUB'],'黄冈市');
array_push($city_arr['HUB'],'恩施土家族苗族自治州');
array_push($city_arr['HLJ'],'齐齐哈尔市');
array_push($city_arr['HLJ'],'大庆市');
array_push($city_arr['HLJ'],'哈尔滨市');
array_push($city_arr['HLJ'],'牡丹江市');
array_push($city_arr['HLJ'],'佳木斯市');
array_push($city_arr['HLJ'],'鸡西市');
array_push($city_arr['HLJ'],'绥化市');
array_push($city_arr['HLJ'],'伊春市');
array_push($city_arr['HLJ'],'黑河市');
array_push($city_arr['HLJ'],'双鸭山市');
array_push($city_arr['HLJ'],'大兴安岭地区');
array_push($city_arr['HLJ'],'七台河市');
array_push($city_arr['HLJ'],'鹤岗市');
array_push($city_arr['HK'],'-');
array_push($city_arr['GZ'],'六盘水市');
array_push($city_arr['GZ'],'贵阳市');
array_push($city_arr['GZ'],'黔东南苗族侗族自治州');
array_push($city_arr['GZ'],'毕节市');
array_push($city_arr['GZ'],'铜仁市');
array_push($city_arr['GZ'],'遵义市');
array_push($city_arr['GZ'],'黔西南布依族苗族自治州');
array_push($city_arr['GZ'],'黔南布依族苗族自治州');
array_push($city_arr['GZ'],'安顺市');
array_push($city_arr['GZ'],'-');
array_push($city_arr['QH'],'西宁市');
array_push($city_arr['QH'],'海东市');
array_push($city_arr['QH'],'海西蒙古族藏族自治州');
array_push($city_arr['QH'],'果洛藏族自治州');
array_push($city_arr['QH'],'海南藏族自治州');
array_push($city_arr['QH'],'黄南藏族自治州');
array_push($city_arr['QH'],'海北藏族自治州');
array_push($city_arr['QH'],'玉树藏族自治州');
array_push($city_arr['QH'],'-');
array_push($city_arr['NX'],'银川市');
array_push($city_arr['NX'],'中卫市');
array_push($city_arr['NX'],'石嘴山市');
array_push($city_arr['NX'],'吴忠市');
array_push($city_arr['NX'],'固原市');
array_push($city_arr['NX'],'-');
array_push($city_arr['NMG'],'巴彦淖尔市');
array_push($city_arr['NMG'],'乌海市');
array_push($city_arr['NMG'],'通辽市');
array_push($city_arr['NMG'],'赤峰市');
array_push($city_arr['NMG'],'呼和浩特市');
array_push($city_arr['NMG'],'乌兰察布市');
array_push($city_arr['NMG'],'鄂尔多斯市');
array_push($city_arr['NMG'],'锡林郭勒盟');
array_push($city_arr['NMG'],'呼伦贝尔市');
array_push($city_arr['NMG'],'阿拉善盟');
array_push($city_arr['NMG'],'包头市');
array_push($city_arr['NMG'],'兴安盟');
array_push($city_arr['NMG'],'-');
array_push($city_arr['LN'],'大连市');
array_push($city_arr['LN'],'营口市');
array_push($city_arr['LN'],'沈阳市');
array_push($city_arr['LN'],'鞍山市');
array_push($city_arr['LN'],'朝阳市');
array_push($city_arr['LN'],'辽阳市');
array_push($city_arr['LN'],'丹东市');
array_push($city_arr['LN'],'盘锦市');
array_push($city_arr['LN'],'铁岭市');
array_push($city_arr['LN'],'葫芦岛市');
array_push($city_arr['LN'],'锦州市');
array_push($city_arr['LN'],'抚顺市');
array_push($city_arr['LN'],'本溪市');
array_push($city_arr['LN'],'阜新市');
array_push($city_arr['LN'],'-');
array_push($city_arr['JX'],'南昌市');
array_push($city_arr['JX'],'赣州市');
array_push($city_arr['JX'],'吉安市');
array_push($city_arr['JX'],'宜春市');
array_push($city_arr['JX'],'上饶市');
array_push($city_arr['JX'],'新余市');
array_push($city_arr['JX'],'九江市');
array_push($city_arr['JX'],'鹰潭市');
array_push($city_arr['JX'],'萍乡市');
array_push($city_arr['JX'],'抚州市');
array_push($city_arr['JX'],'景德镇市');
array_push($city_arr['JS'],'盐城市');
array_push($city_arr['JS'],'苏州市');
array_push($city_arr['JS'],'南京市');
array_push($city_arr['JS'],'常州市');
array_push($city_arr['JS'],'连云港市');
array_push($city_arr['JS'],'无锡市');
array_push($city_arr['JS'],'南通市');
array_push($city_arr['JS'],'镇江市');
array_push($city_arr['JS'],'徐州市');
array_push($city_arr['JS'],'淮安市');
array_push($city_arr['JS'],'泰州市');
array_push($city_arr['JS'],'扬州市');
array_push($city_arr['JS'],'宿迁市');
array_push($city_arr['JL'],'长春市');
array_push($city_arr['JL'],'延边朝鲜族自治州');
array_push($city_arr['JL'],'吉林市');
array_push($city_arr['JL'],'四平市');
array_push($city_arr['JL'],'松原市');
array_push($city_arr['JL'],'通化市');
array_push($city_arr['JL'],'白城市');
array_push($city_arr['JL'],'辽源市');
array_push($city_arr['JL'],'白山市');
array_push($city_arr['HEN'],'郑州市');
array_push($city_arr['HEN'],'商丘市');
array_push($city_arr['HEN'],'鹤壁市');
array_push($city_arr['HEN'],'南阳市');
array_push($city_arr['HEN'],'洛阳市');
array_push($city_arr['HEN'],'新乡市');
array_push($city_arr['HEN'],'濮阳市');
array_push($city_arr['HEN'],'焦作市');
array_push($city_arr['HEN'],'信阳市');
array_push($city_arr['HEN'],'开封市');
array_push($city_arr['HEN'],'平顶山市');
array_push($city_arr['HEN'],'安阳市');
array_push($city_arr['HEN'],'驻马店市');
array_push($city_arr['HEN'],'漯河市');
array_push($city_arr['HEN'],'省直辖县级行政区划');
array_push($city_arr['HEN'],'许昌市');
array_push($city_arr['HEN'],'三门峡市');
array_push($city_arr['HEN'],'周口市');
array_push($city_arr['HB'],'唐山市');
array_push($city_arr['HB'],'秦皇岛市');
array_push($city_arr['HB'],'石家庄市');
array_push($city_arr['HB'],'邢台市');
array_push($city_arr['HB'],'邯郸市');
array_push($city_arr['HB'],'廊坊市');
array_push($city_arr['HB'],'承德市');
array_push($city_arr['HB'],'保定市');
array_push($city_arr['HB'],'沧州市');
array_push($city_arr['HB'],'衡水市');
array_push($city_arr['HB'],'张家口市');
array_push($city_arr['HAIN'],'海口市');
array_push($city_arr['HAIN'],'三亚市');
array_push($city_arr['HAIN'],'三沙市');
array_push($city_arr['HAIN'],'省直辖县级行政区划');
array_push($city_arr['HAIN'],'儋州市');

array_push($city_arr['TW'],'台北');
array_push($city_arr['TW'],'新北');
array_push($city_arr['TW'],'桃园');
array_push($city_arr['TW'],'台中');
array_push($city_arr['TW'],'台南');
array_push($city_arr['TW'],'基隆');
array_push($city_arr['TW'],'新竹市');
array_push($city_arr['TW'],'嘉义市');
array_push($city_arr['TW'],'新竹县');
array_push($city_arr['TW'],'苗栗');
array_push($city_arr['TW'],'彰化');
array_push($city_arr['TW'],'南投');
array_push($city_arr['TW'],'云林');
array_push($city_arr['TW'],'嘉义县');
array_push($city_arr['TW'],'屏东');
array_push($city_arr['TW'],'台东');
array_push($city_arr['TW'],'花莲');
array_push($city_arr['TW'],'宜兰');
array_push($city_arr['TW'],'澎湖');
array_push($city_arr['TW'],'金门');
array_push($city_arr['TW'],'连江县(马祖)');
?>
<meta charset="UTF-8">
<style>
    .hide{
        display: none;
    }
    .query_total{
        font-size: 20px;
    }
    .query_next{
        color: red;
        font-size: 20px;
    }
</style>
<body class="easyui-layout" data-options="fit: true" style="padding: 0px; ">
<div>
    <form action="/crm_company_out/add_presend_by_search_send" method="POST" id="search_form">
        <input type="hidden" name="search_type" value="1">

        <div class="form_div search_form" style="padding-top: 150px;width: 100%">
            <div style="padding-bottom: 20px; margin-left: 500px;">
                <h1> 1、请设定您要查询的范围  </h1>
            </div>
            <div style="width: 50%;margin-left: 500px;padding-bottom: 10px">
            </div>
            <div style="width:50%;margin-left: 500px;">
                <table>
                    <tr>
                        <td><?= lang('省');?></td>
                        <td>
                            <select class="easyui-combobox" name="field[province_code]" id="province" style="width: 255px;" data-options="onSelect:province_select">
                                <option value="">--请选择省--</option>
                                <?php foreach($province_arr as $key => $province){ ?>
                                    <option value="<?= $key;?>"><?= $province;?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('市');?></td>
                        <td>
                            <select class="easyui-combobox" name="field[city]" id="city" style="width: 255px;">
                                <option value="">--请选择市--</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><?= lang('数据来源'); ?></td>
                        <td>
                            <select class="easyui-combobox" name="field[data_from]" id="data_from" style="width:255px;">
                                <!--<option value="工商">工商</option>-->
                                <option value="参展">参展</option>
                                <option value="外贸平台">外贸平台</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right"><button type="button" onclick="query_total('search_form')">点击提交到下一步</button></td>
                    </tr>
                </table>
                <br/>
                <br/>
                <br/>
                <br/>
                <div class="query_next hide">
                    搜索到<span class="query_total">0</span>个收件人
                    <button type="button query_button" form_id="search_form">点击生成收件人组</button>
                </div>
            </div>
        </div>
    </form>
</div>

</body>
<script type="text/javascript">
    var city = {
        <?php foreach ($city_arr as $key => $city){ ?>
        '<?= $key;?>':[
            <?php foreach($city as $val){ ?>
            '<?= $val;?>',
            <?php } ?>
        ],
        <?php } ?>
    };

    //省选中事件
    function province_select(row) {
        //给市赋予值
        var data = [];

        if(city[row['value']] === undefined) city[row['value']] = [];

        $.each(city[row['value']], function (i, it) {
            data.push({text:it,value:it});
        });

        $('#city').combobox('clear').combobox('loadData', data);
    }

    //点击标签
    function query_add(val) {
        var query = $('#query').val();
        if(query==''){
            $('#query').val(val);
        }else{
            $('#query').val(query + ',' + val);
        }

    }

    //切换查询模式
    function query_mode(mode = 0) {
        $('.change_mode').removeClass('hide');
        $('.form_div').addClass('hide');
        query_total_show(0, false);
        if(mode == 0){
            $('.change_mode0').addClass('hide');
            $('.tab_form').removeClass('hide');
        }else if(mode == 1){
            $('.change_mode1').addClass('hide');
            $('.search_form').removeClass('hide');
        }
    }

    //快速选中全部
    function query_all() {
        $('#query').val('all');
        // $.each($('.tag'), function (i, it) {
        //     if($(it).hasClass('tag_all')) return true;
        //     $(it).trigger('click')
        // });
    }

    //点击查一查
    function query_total(form_id) {
        ajaxLoading();
        $('#' + form_id).form('submit', {
            url:'/crm_company_out/add_presend_by_search_send?get_total=true',
            success:function (res_json) {
                var res;
                ajaxLoadEnd();
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {
                    $.messager.alert('<?= lang('提示');?>', res_json);
                    return;
                }
                //$.messager.alert('<?//= lang('提示');?>//', res.msg);
                if(res.code == 0){
                    //提示存在多少条,并出现下一步按钮,点击进入总数页面
                    query_total_show(res['data']['total'], true);
                }else{
                }
            }
        });
    }

    //控制展示文本框的
    function query_total_show(total = 0, is_show = false) {
        $('.query_next').addClass('hide');
        $('.query_total').text(0);
        if(is_show){
            $('.query_total').text(total);
            $('.query_next').removeClass('hide');
        }else{

        }
    }

    //点击查一查
    function query(form_id) {
        $('#' + form_id).submit();
    }

    //点击清空
    function clear_input() {
        $('#query').val('');
    }

    $(function () {
        $('#tt').tree({
            onClick: function (node) {
                var title1 = node.text;
                var title2 = node.id;

                if (isNaN(title2)) {
                    var url = "/bsc_user/mail_list?title=" + title2 + '&type=1';
                    var content = '<iframe scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:100%"></iframe>';
                    $('#tabs').tabs('add', {
                        title: title1,
                        content: content,
                        closable: true
                    });

                } else {
                    var url = "/bsc_user/list_details?title=" + title2;
                    var content = '<iframe scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:100%"></iframe>';
                    $('#tabs').tabs('add', {
                        title: title1,
                        content: content,
                        closable: true
                    });
                }
            }
        });

        $('.query_button').on('click', function () {
            var form_id = $(this).attr('form_id');
            query(form_id);
        });
    });
</script>
</html>
