<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?= lang('CRM管理');?></title>
    <link rel="stylesheet" type="text/css" href="/inc/third/layui/css/layui.css">
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
            list-style: none;
        }

        body, html {
            height: 100%;
        }

        .curr {
            background: #081c1d;
        }

        .curr a {
            font-size: 14px;
            color: white !important;
        }
        .curr a i {
            font-size: 35px !important;
        }

        .nav {
            height: 100%;
            background: #173640;
            color: #c1c1c1;
        }

        .nav .nav-list {
            width: 100%;
            padding-top: 30px;
        }

        .nav .nav-list .nav-tit {
            padding: 10px 0px;
            width: 100%;
            margin-bottom: 20px;
            font-size: 12px;
            text-align: center;
        }


        .nav .nav-list .nav-tit a {
            text-decoration: none;
            color: #c1c1c1;
        }

        .nav .nav-list .nav-tit a:hover {
            color: #ffffff;
        }

        .nav .nav-list .nav-tit img {
            width: 21px;
            height: 21px;
            vertical-align: middle;
        }

        .nav .nav-list .nav-tit span {
            margin-left: 0px;
        }

        .nav .nav-list .personal-list {
            width: 90px;
            height: 80px;
            margin-left: 30px;
            margin-bottom: 40px;
            font-size: 14px;
            display: none;
            position: relative;
            left: 55px;
            top: -35px;
        }

        .nav .nav-list .personal-list li {
            width: 100%;
            margin-bottom: 15px;
            text-align: center;
            line-height: 25px;
            border-radius: 3px;
        }

        .nav .nav-list .personal-list li a {
            text-decoration: none;
            color: #fff;
        }

        .nav .nav-list .personal-list li.active {
            background: #0e8384;
        }

        .nav .nav-list .personal-list li:hover {
            background: #0e8384;
        }
        .new {position: absolute; right:15px; width: 20px; height: 20px; line-height: 20px; color: white; text-align: center; border-radius: 10px; background: #d30100
        }
    </style>
    <script>
        $(function () {
            var height = document.body.offsetHeight;
            var width = document.body.offsetWidth;
            var right_width = width - 102;
            $('#main').height(height);
            $('#left_nav').height(height).width(100);
            $('#right_nav').height(height).width(right_width);

            $('#personal').on('click', function () {
                $('#personal-child').fadeToggle(300);
            });
            let aLi = $('#personal-child li');
            aLi.on('click', function () {
                $(this).addClass('active').siblings('li').removeClass('active');
            })

            $('.nav-tit a').click(function () {
                $('.nav-tit').removeClass('curr');
                $(this).parent('div').addClass('curr');
            });

        });

        $(window).resize( function  () {           //当浏览器大小变化时
            var height = document.body.offsetHeight;
            var width = document.body.offsetWidth;
            var right_width = width - 102;
            $('#main').height(height);
            $('#left_nav').height(height).width(100);
            $('#right_nav').height(height).width(right_width);
        });
    </script>
</head>
<body style="overflow: auto">
<div id="main" class="easyui-layout" style="width:100%;height:900px; overflow:hidden">
    <div class="left_nav" id="left_nav" style="float: left">
        <div class="nav">
            <div class="nav-list">
                <!--<div class="nav-tit">-->
                <!--    <a href="/biz_stat/index/" target="main">-->
                <!--        <i class="layui-icon layui-icon-chart-screen" style="font-size: 25px; color: #f9feff;"></i><br>-->
                <!--        <span><?= lang('我的简报');?></span>-->
                <!--    </a>-->
                <!--</div>-->
                <div class="nav-tit curr" style="position: relative">
                    <a href="/biz_crm/customer_apply/" target="main">
                        <i class="layui-icon layui-icon-friends" style="font-size: 25px; color: #f9feff;"></i><br>
                        <span><?= lang('CRM客户');?></span>
                    </a>
                </div>
                <div class="nav-tit">
                    <a href="/crm_promote_mail/index_new" target="main">
                        <i class="layui-icon layui-icon-email" style="font-size: 25px; color: #f9feff;"></i><br>
                        <span><?= lang('邮件群发');?></span>
                    </a>
                </div>
                <div class="nav-tit">
                    <a href="/crm_promote_mail/index/" target="main">
                        <i class="layui-icon layui-icon-time" style="font-size: 25px; color: #f9feff;"></i><br>
                        <span><?= lang('群发历史');?></span>
                    </a>
                </div>
                <!--<div class="nav-tit">-->
                <!--    <a href="/crm_promote_mail/index/?group_id=1" target="main">-->
                <!--        <i class="layui-icon layui-icon-template" style="font-size: 25px; color: #f9feff;"></i><br>-->
                <!--        <span>公共模板</span>-->
                <!--    </a>-->
                <!--</div>-->
                <!--<div class="nav-tit">-->
                <!--    <a href="/report/client_order_total_v3/" target="main">-->
                <!--        <i class="layui-icon layui-icon-chart-screen" style="font-size: 25px; color: #f9feff;"></i><br>-->
                <!--        <span>客户分析</span>-->
                <!--    </a>-->
                <!--</div>-->
            </div>
        </div>
    </div>

    <div class="right_nav" id="right_nav" style="padding:0px; float: right">
        <iframe name="main" src="/biz_crm/customer_apply/" scrolling="" frameborder="0" style="width: 100%; height: 100%;"/>
    </div>
</div>

</body>
</html>
