<?php

/**
 * 这个控制器是用来生成一些发给自己公司系统用报文的控制器
 * Class bsc_message_our_company
 */
class bsc_message_our_company extends Common_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->model = Model('bsc_message_model');
    }

    /**
     * 生成Ebooking报文且发送到指定的API接口
     * @param $id_no
     */
    public function ebooking_agent($id_no){
        //代理单是以shipment为主, 一票一票发
        $this->db->select('biz_shipment.*' .
            ',(select client_name from biz_client where biz_client.client_code = biz_shipment.trans_carrier) as trans_carrier_name', false);
        $shipment = Model("biz_shipment_model")->get_where_one("id = '{$id_no}'");
        if(empty($shipment)) return jsonEcho(array('code' => 1, 'msg' => lang('shipment不存在')));
        $shipments = array($shipment);
        
        
        //根据当前consol获取数据信息
        $this->db->select("id,agent_code,agent_company,free_svr,carrier_ref");
        $consol = Model('biz_consol_model')->get_where_one("id = '{$shipment['consol_id']}'");
        if(empty($consol)) return jsonEcho(array('code' => 1, 'msg' => lang('consol不存在')));

        $this->db->select("id,client_code,own_company_flag");
        $agent = Model('biz_client_model')->get_where_one("client_code = '{$consol['agent_code']}'");
        if(empty($agent)) return jsonEcho(array('code' => 1, 'msg' => lang('未获取到代理信息'))); 

        //首先,代理必须是我司分公司
        if($agent['own_company_flag'] !== '1') return jsonEcho(array('code' => 1, 'msg' => lang('该代理不能发送Ebooking')));

        //必须配置了ebooking接口地址
        $ebooking_url = Model('bsc_dict_model')->no_role_get("catalog = '{$agent['client_code']}_api_ebooking_url'");
        if(empty($ebooking_url))  return jsonEcho(array('code' => 1, 'msg' => lang('未配置接口地址')));
        $url = $ebooking_url[0]['value'];

        $datas = array(
            'url' => $url,
            'shipments' => array(),
        );
        $message_datas = array();
        $error_info = array();
        $receiver = 'OUR_COMPANY';
        $type = 'AGENT_EBOOKING';
        
        //发送过去的ebooking数据
        //前面加的一些限制
        //shipment是HBL 且 服务选项勾选目的港服务时, 目的港送货地址必填
        $service_options = array_column(json_decode($shipment['service_options'], true), 0);
        $biz_type = $shipment['biz_type'];
        $INCO_address = $shipment['INCO_address'];
        if($biz_type == 'export'){
            if(in_array('destination_trucking', $service_options)){
                if(empty($INCO_address)){
                    return jsonEcho(array('code' => 1, 'msg' => lang('已勾选目的港送货服务 必须填写目的港送货地址')));
                }
            }
        }else{
            if(in_array('trucking', $service_options)){
                if(empty($INCO_address)){
                    return jsonEcho(array('code' => 1, 'msg' => lang('已勾选 送货服务 必须填写目的港提/送货地址')));
                }
            }
        }
        //代理单应该没问题, 因为有代理肯定是HBL了
        if($shipment['hbl_type'] == 'MBL'){
            return jsonEcho(array('code' => 1, 'msg' => lang("{job_no} MBL不能发送", array('job_no' => $shipment['job_no']))));
        }
        $data = array();

        //关单号和MBL也一起发过去
        $data['cus_no'] = $shipment['cus_no'];
        $data['carrier_ref'] = $consol['carrier_ref'];

        //这里目前先设定为,出口发给对方后, 会变成进口
        $biz_type = $shipment['biz_type'] == 'export' ? 'import' : 'export';

        $data['biz_type'] = $biz_type;
        //由于两个系统客户代码不一定一致,这里使用客户全称作为处理
        //林总说 海外代理的也默认为当前客户代码
        $this_client_code = get_system_config('OUR_MAIN_COMPANY');
        // $data['client_code'] = $shipment['client_code'];
        $data['client_code'] = $this_client_code;
        //二级委托方同理, 传过去,如果那面没这个字段的话,就不用处理了
        //TODO 二级委托方有点问题 国内的二级其实不是必填了, 版本有点差异
        // $data['client_company2'] = $shipment['client_company'];
        $data['client_email'] = $shipment['client_email'];
        $data['client_telephone'] = $shipment['client_telephone'];
        $data['client_contact'] = $shipment['client_contact'];
        $data['trans_mode'] = $shipment['trans_mode'];
        $data['trans_tool'] = $shipment['trans_tool'];
        $data['trans_term'] = $shipment['trans_term'];
        $data['shipper_ref'] = $shipment['job_no'];
        $data['description'] = $shipment['description'];
        $data['description_cn'] = $shipment['description_cn'];
        $data['mark_nums'] = $shipment['mark_nums'];
        $data['release_type'] = $shipment['release_type'];
        $data['release_type_remark'] = $shipment['release_type_remark'];
        $data['trans_origin'] = $shipment['trans_origin'];
        $data['trans_origin_name'] = $shipment['trans_origin_name'];
        $data['trans_discharge'] = $shipment['trans_discharge'];
        $data['trans_discharge_name'] = $shipment['trans_discharge_name'];
        $data['trans_destination_inner'] = $shipment['trans_destination_inner'];
        $data['trans_destination'] = $shipment['trans_destination'];
        $data['trans_destination_terminal'] = $shipment['trans_destination_terminal'];
        $data['trans_destination_name'] = $shipment['trans_destination_name'];
        $data['booking_ETD'] = $shipment['booking_ETD'];
        $data['good_outers'] = $shipment['good_outers'];
        $data['good_outers_unit'] = $shipment['good_outers_unit'];
        $data['good_weight'] = $shipment['good_weight'];
        $data['good_weight_unit'] = $shipment['good_weight_unit'];
        $data['good_volume'] = $shipment['good_volume'];
        $data['good_volume_unit'] = $shipment['good_volume_unit'];
        $data['good_describe'] = $shipment['good_describe'];
        $data['good_commodity'] = $shipment['good_commodity'];
        $data['INCO_term'] = $shipment['INCO_term'];
        // TODO 这里问下林总, 如果是聚瀚,或者其他的 怎么区分呢, 海外的其实是MBL和HBL, 聚翰的是 MBL 和LEAUGE , KIMXIN
        
        $hbl_type = $shipment['hbl_type'] == 'MBL' ? 'MBL' : 'HBL';
        $data['hbl_type'] = $hbl_type;
        $data['box_info'] = $shipment['box_info'];
        $data['goods_type'] = $shipment['goods_type'];
        $data['requirements'] = $shipment['requirements'];
        $data['service_options'] = $shipment['service_options'];
        $data['remark'] = $shipment['remark'];
        $data['INCO_address'] = $shipment['INCO_address'];
        $data['trans_carrier'] = $shipment['trans_carrier_name'];
        //代理改为聚翰
        $data['agent_code'] = $consol['agent_code'];
        $data['free_svr'] = $consol['free_svr'];

        $datas['shipments'][] = $data;
        //如果存在错误信息, 不给提示
        if(!empty($error_info)){
            return jsonEcho(array('code' => 1, 'msg' => "<span style='color:red;font-weight: bold;font-size: 16px;'>" . lang("以下信息为 错误提示") . "</span><br/>" . join('<br/>', $error_info)));
        }
        
         //接下来将报文进行转化
        //保存文件txt
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        //ESL_EDI/日期
        $new_dir = $root_path . "/upload/message/{$receiver}_EDI/{$type}/{$path}/";
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $id_no . '.txt';
        $file_path = $new_dir . $file_name;
        file_put_contents($file_path, json_encode($datas));

        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'receiver' => $receiver,
        );
        $id = update_message_by_shipment($id_no, $message_data, $type);
    
        return jsonEcho(array('code' => 0, 'msg' => lang('生成成功'), 'data' => array('id' => $id)));
    }
    
    /**
     * 生成Ebooking报文且发送到指定的API接口
     * @param $id_no
     */
    public function ebooking_booking_agent($id_no){
        $receiver = 'OUR_COMPANY';
        $type = 'BOOKING_AGENT_EBOOKING';
        
        //根据当前consol获取数据信息
        $this->db->select("*" .
            ",(select company_name from biz_client where biz_client.client_code = biz_consol.agent_code) as agent_company" .
            "", false);
        $consol = Model('biz_consol_model')->get_where_one("id = '{$id_no}'");
        if(empty($consol)) return jsonEcho(array('code' => 1, 'msg' => 'consol不存在'));

        $consol_duty = Model('biz_duty_model')->get_old_by_new("biz_consol", $consol['id'], array('booking'));

        //模拟订舱代理是聚瀚
        // $consol['creditor'] = 'SHJHCW02';
        
        $this->db->select("id,client_code,country_code");
        $agent = Model('biz_client_model')->get_where_one("client_code = '{$consol['creditor']}'");
        if(empty($agent)) return jsonEcho(array('code' => 1, 'msg' => '未获取到订舱代理信息'));

        //必须配置了ebooking接口地址
//        $ebooking_url = Model('bsc_dict_model')->no_role_get("catalog = '{$agent['client_code']}_api_ebooking_url'");
//        if(empty($ebooking_url))  return jsonEcho(array('code' => 1, 'msg' => '未配置'));
//        $url = $ebooking_url[0]['value'];

        $this->db->select('biz_shipment.*' .
            ',(select client_name from biz_client where biz_client.client_code = biz_shipment.trans_carrier) as trans_carrier_name' .
            ',(select company_name from biz_client where biz_client.client_code = biz_shipment.client_code) as client_company' .
            ',(select company_name from biz_client where biz_shipment.client_code2 = biz_client.client_code) as client_company2', false);
        $shipments = Model("biz_shipment_model")->get_shipment_by_consol($id_no);

        $datas = array(
            'shipments' => array(),
        );
        $message_datas = array();
        $error_info = array();
        //2023-07-07 这里和原版的差距为 原版发shipment, 这里作为客户, 只发1票给订舱代理
        $data = array();

        //关单号和MBL也一起发过去
        $data['carrier_ref'] = $consol['carrier_ref'];
        //2023-09-13 讨论的方案为 作为订舱代理时, 业务类型 根据港口判断, 如果港口和当前接收方和发送方有关, 发送后 就讲export 转为import 反之... 
        $this_port = array(substr($consol['trans_origin'], 0, 2), substr($consol['trans_destination'], 0, 2));
        //判断是否与接收或当前国家相关
        
        $this_client_code = get_system_config('OUR_MAIN_COMPANY');
        $this->db->select("country_code");
        $this_client = Model('biz_client_model')->get_where_one("client_code = '{$this_client_code}'");
        if(empty($this_client)) return jsonEcho(array('code' => 1, 'msg' => lang("未获取到当前公司对应的往来单位信息")));
        
        $this_country = array($agent['country_code'], $this_client['country_code']);
        if(!empty(array_intersect($this_country, $this_client))){
            $biz_type = $consol['biz_type'] == 'export' ? 'import' : 'export';
        }else{
            //如果不相关,原样发送
            $biz_type = $consol['biz_type'];
        }
        
        //这里先默认当前只有一个分公司吧
        $data['biz_type'] = $biz_type;
        //客户代码直接写死
        //KML发过去的话, 分为两种情况, 到时候可能要做成选择类型 分为金佰利和穰穰
        //2023-09-07 这里默认为当前的配置我司主公司
        $data['client_code'] = $this_client_code;
        $data['client_code2'] = "";
        //联系信息全部切换为当前票的订舱人员
        $data['client_email'] = getUserField($consol_duty['booking_id'], 'email');
        $data['client_telephone'] = getUserField($consol_duty['booking_id'], 'telephone');
        $data['client_contact'] = getUserField($consol_duty['booking_id'], 'name');
        $data['trans_mode'] = $consol['trans_mode'];
        $data['trans_tool'] = $consol['trans_tool'];
        $data['trans_term'] = $consol['trans_term'];
        $data['shipper_ref'] = $consol['job_no'];
        $data['description'] = $consol['description'];
        $data['description_cn'] = $consol['description_cn'];
        $data['mark_nums'] = $consol['mark_nums'];
        $data['release_type'] = $consol['release_type'];
        $data['release_type_remark'] = "";
        $data['trans_origin'] = $consol['trans_origin'];
        $data['trans_origin_name'] = $consol['trans_origin_name'];
        $data['trans_discharge'] = $consol['trans_discharge'];
        $data['trans_discharge_name'] = $consol['trans_discharge_name'];
        $data['trans_destination_inner'] = $consol['trans_destination_inner'];
        $data['trans_destination'] = $consol['trans_destination'];
        $data['trans_destination_terminal'] = $consol['trans_destination_terminal'];
        $data['trans_destination_name'] = $consol['trans_destination_name'];
        $data['booking_ETD'] = $consol['booking_ETD'];
        $data['good_outers'] = $consol['good_outers'];
        $data['good_outers_unit'] = $consol['good_outers_unit'];
        $data['good_weight'] = $consol['good_weight'];
        $data['good_weight_unit'] = $consol['good_weight_unit'];
        $data['good_volume'] = $consol['good_volume'];
        $data['good_volume_unit'] = $consol['good_volume_unit'];
//        $data['good_describe'] = "";
//        $data['good_commodity'] = "";
        $data['INCO_term'] = $shipments[0]['INCO_term'];

        //订舱代理的统一默认为MBL, 这里就不用管了
        $hbl_type = $shipments[0]['hbl_type'] == 'MBL' ? 'MBL' : 'HBL';
        $data['hbl_type'] = $hbl_type;
        $data['box_info'] = $consol['box_info'];
        $data['goods_type'] = $consol['goods_type'];
        $data['requirements'] = $consol['requirements'];
        $data['service_options'] = $shipments[0]['service_options'];
        $data['remark'] = $consol['remark'];
        $data['INCO_address'] = $shipments[0]['INCO_address'];
        $data['trans_carrier'] = "";
        //代理改为聚翰
        $data['agent_code'] = "";
        $data['free_svr'] = $consol['free_svr'];

        //关单号也是用MBL吗
        $data['cus_no'] = "";

        //生成对应的海运托书, 然后发送过去
        $this_url = base_url() . "export/export_word/3?template_id=60&data_table=consol&id={$consol['id']}";
        $post = array();
        $headers = [
        ];
        $result_json = curl_post_body($this_url,$headers, http_build_query($post));
        $result = json_decode($result_json, true);

        $file_path = "http://pdf.leagueshipping.com" . $result['file_path'];

        $data['files'] = array(
            array(
                'biz_table' => 'biz_shipment',
                'type_name' => "委托书",
                //这里先这样吧, 后续如果有其他地方要作为上传文件的话, 再看看咋改
                'file_path' => $file_path,
                'file_name' => "{$consol['carrier_ref']} {$consol['job_no']} 海运托书.pdf",
            ),
        );
        $datas['shipments'][] = $data;

        //如果存在错误信息, 不给提示
        if(!empty($error_info)){
            return jsonEcho(array('code' => 1, 'msg' => "<span style='color:red;font-weight: bold;font-size: 16px;'>以下信息为 错误提示</span><br/>" . join('<br/>', $error_info)));
        }

        //接下来将报文进行转化
        //保存文件txt
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        //ESL_EDI/日期
        $new_dir = $root_path . "/upload/message/{$receiver}_EDI/{$type}/" . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $consol['id'] . '.txt';
        $file_path = $new_dir . $file_name;
        file_put_contents($file_path, json_encode($datas));

        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => $type,
            'receiver' => $receiver,
        );
        update_message($consol['id'], $message_data, $type);


        return jsonEcho(array('code' => 0, 'msg' => lang('生成成功')));
    }
}