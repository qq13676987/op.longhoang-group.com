<tr>
    <td>
        <select name="box_type[]" class="easyui-combobox" style="width:80px;"
            data-options="
                editable:false,
                valueField:'value',
                textField:'value',
                url:'/bsc_dict/get_option/container_size',
                ">
        </select>
    </td>
    <td>
        <input name="box_number[]" prompt="0" class="easyui-numberbox" precision="0" style="width:50px;">
    </td>
    <td>
        <input name="price[]" prompt="$0.00" class="easyui-numberbox" precision="2" style="width:80px;">
    </td>
    <td>
        <select name="carrier[]" class="easyui-combobox" style="width:80px;"
            data-options="
                required:true,
                valueField:'client_name',
                textField:'client_name',
                value:'',
                url:'/biz_client/get_option/carrier',
                onHidePanel: function() {
                    var valueField = $(this).combobox('options').valueField;
                    var val = $(this).combobox('getValue');
                    var allData = $(this).combobox('getData');
                    var result = true;
                    for (var i = 0; i < allData.length; i++) {
                        if (val == allData[i][valueField]) {
                            result = false;
                        }
                    }
                    if (result) {
                        $(this).combobox('clear');
                    }
                },
            ">
        </select>
    </td>
    <td>
        <button type="button" class="del_tr layui-btn layui-btn-xs">
            <i class="layui-icon layui-icon-subtraction"></i>
        </button>
    </td>
</tr>