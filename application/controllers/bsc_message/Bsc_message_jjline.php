<?php

/**
 * JJLINE 报文相关
 */
class bsc_message_jjline extends Common_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bsc_message_model');
        $this->model = $this->bsc_message_model;
    }

    /**
     * SO报文
     */
    public function message_so($consol_id, $file_function = '9'){
        $time = time();
        $file_sum = 0;
        $file_content = '';
        $consol = Model('biz_consol_model')->get_by_id($consol_id);
        if(empty($consol)) return jsonEcho(array('code' => 1, 'msg' => lang('consol不存在, 生成报文失败')));
        if($consol['status'] !== 'normal') return jsonEcho(array('code' => 1, 'msg' => lang('请将状态改为normal后再试')));

        $shipments = Model('biz_shipment_model')->get_shipment_by_consol($consol_id);
        if(empty($shipments)) return jsonEcho(array('code' => 1, 'msg' => lang('请绑定shipment后再试')));

        $replace_arr = array(array(":", '?:'), array("'", "?'"));

        $message = array();
        //00:BOOKING:BOOKING ORDER FORM:9:::202302140953'
        $message['00'] = array();
        $message['00'][] = '00';//RECORD ID            		记录类型标识	9(2)	00	M
        $message['00'][] = 'BOOKING';//MESSAGE TYPE            		报文类型	X(10)	BOOKING	M
        $message['00'][] = 'BOOKING ORDER FORM';//FILE DESCRIPTION        		文件说明	X(35)	 BOOKING ORDER FORM	C
        $message['00'][] = $file_function;//FILE FUNCTION            		文件功能	9(1)	默认输出9	M
        $message['00'][] = '';//SENDER CODE           		发送方代码	X(13)		C
        $message['00'][] = '';//RECEIVER CODE      		接收方代码	X(13)		C
        $message['00'][] = date('YmdHi', $time);//FILE CREATE TIME     		报文生成时间	9(12)	YYYYMMDDhhmm	M
        $file_sum++;

        //10::CHUN JIN:CN:2305E::::20230120:CNSHA:SHANGHAI:::'
        $message['10'] = array();
        $message['10'][] = '10';//RECORD ID         		记录类型标识	9(2)	10	M
        $message['10'][] = '';//IMO NO		船舶IMO编号	X(9)	国际航行船舶填写IMO号，采用9位数字和大写字母混合码。其前两位表识码为UN，后七位为IMO编号；	C
        $message['10'][] = $consol['vessel'];//VESSEL                         		船名	X(35)	使用标准的英文名称	M
        $message['10'][] = '';//NATIONALITY CODE		舶国籍代码	X(2)		C
        $message['10'][] = $consol['voyage'];//VOYAGE		航次	X(17)		M
        $message['10'][] = '';//TRADE CODE		航线代码	X(10)       O
        $message['10'][] = '';//TRADE                   		航线	X(35)       O
        $message['10'][] = '';//ESTIMATED TIME OF ARRIVAL (ETA)		预计到达日期	9(8)	CCYYMMDD
        $message['10'][] = '';//DEPARTURE  DATE		离港日期（船舶启运日期和时间）	9(8)	YYYYMMDD
        $message['10'][] = $consol['trans_origin'];//DEPART PORT CODE		离港地点代码（船舶离境地海关代码）	X(5)	5位港口ISO代码，如CNSHA\JPOSA\TWKEL等
        $message['10'][] = $consol['trans_origin_name'];//DEPART PORT    		离港地点	X(35)
        $message['10'][] = '';//NEXT CALLING PORT		下一挂港代码	X(5)
        $message['10'][] = '';//NEXT CALLING PORT		下一挂港	X(35)
        $message['10'][] = '';//CONTAINER COUNT   		集装箱箱数	9(10)
        $file_sum++;

        //11:SJJ'
        $trans_carrier_arr = array(
            'SHSJJH01' => 'SJJ',
            'SHHHLC01' => 'HAS',
        );
        if(!isset($trans_carrier_arr[$consol['trans_carrier']])) return jsonEcho(array('code' => 1, 'msg' => lang('承运人 只能选择 JINJIANG-Line 和 HASCO')));
        $message['11'] = array();
        $message['11'][] = '11';//RECORD ID         		记录类型标识	9(2)	11
        $message['11'][] = $trans_carrier_arr[$consol['trans_carrier']];//SHIPPING LINE CODE  		船公司(承运人)代码	X(4)	锦江输出SJJ， 海华输出HAS
//        $message['11'][] = '';//SHIPPING LINE            		船公司(承运人)	X(100)
        $file_sum++;

        $file_content .= message_format($message, true, $replace_arr) . "\r\n";
        $message = array();//转换后清空下

        //12开始就是提单的记录了
        //获取需要发送的shipment
        $send_shipments = array();
        foreach($shipments as $skey => $shipment){
            //检查是否有子shipment,有的话主的不取了,取子的
            $child_shipments = Model('biz_shipment_model')->no_role_get("parent_id = '{$shipment['id']}'");
            if(!empty($child_shipments)){
                foreach ($child_shipments as $child_shipment){
                    //子shipment填充需要发送的数组
                    $send_shipments[] = $child_shipment;
                }
            }else{
                //没有子时使用主的
                $send_shipments[] = $shipment;
            }
        }
        if($consol['payment'] == 'PP'){
            $payment = 'P';
        }else if($consol['payment'] == 'CC'){
            $payment = 'C';
        }else if($consol['payment'] == 'E'){
            //2023-05-06 第三方先按 COLLECT 处理
            $payment = 'C';
        }else{
            $result['msg'] = lang('付款方式暂不支持');
            return jsonEcho(array('code' => 1, 'msg' => lang('payment')));
        }
        // 转换 货主箱标记 container_owner
        if($consol['container_owner'] == 'SOC'){
            $container_owner = 'SOC';
        }else if($consol['container_owner'] == 'COC'){
            $container_owner = '';
        }else{
            return jsonEcho(array('code' => 1, 'msg' => lang('持箱人必须为SOC或COC')));
        }
        foreach ($send_shipments as $send_shipment){
            //12:TEJJCSH300120::::CNSHA:SHANGHAI:CNSHA:SHANGHAI:CY-CY:P:20230120::::::JJJCL40:'
            $message['12'] = array();
            $message['12'][] = '12';//RECORD ID         		记录类型标识	9(2)	12
            $message['12'][] = $send_shipment['cus_no'];//B/L NO.		总提运单号	X(35)	（Master B/L）
            $message['12'][] = '';//PRE. VESSEL CODE		前程运输船名代码	X(9)
            $message['12'][] = '';//PRE. VESSEL		前程运输船名	X(35)
            $message['12'][] = '';//PRE. VOYAGE		前程运输航次	X(6)
            $message['12'][] = $send_shipment['trans_origin'];//PLACE CODE OF RECEIPT		收货地代码（货物托运地或者国家代码）	X(5)
            $message['12'][] = $send_shipment['trans_origin_name'];//PLACE OF RECEIPT		收货地名称	X(70)
            $message['12'][] = $send_shipment['trans_origin'];//LOAD PORT CODE		装货港代码	X(5)	显示5位UN港口代码
            $message['12'][] = $send_shipment['trans_origin_name'];//LOAD PORT		装货港	X(35)
            $message['12'][] = $send_shipment['trans_term'];//TRANSPORTATION ITEMS		运输条款	X(11)	CY/CY  CY/CFS  FI/FO等
            $message['12'][] = $payment;//PREPAID OR COLLECT		付款方式（运费支付方法代码）	X(1)	"C：Collect到付  P：Prepaid only预付  F：Free免费"
            $message['12'][] = date('Ymd', strtotime($send_shipment['booking_ETD']));//Consignment loading date		货物装船日期	9(8)	YYYYMMDD
            $message['12'][] = '';//QUARANTINE CODING		检疫代码	X(1)
            $message['12'][] = '';//DATE OF ISSUE		签发日期	9(8)	 YYYYMMDD
            $message['12'][] = '';//CURRENCY		币种（金额类型代码）	X(3)
            $message['12'][] = '';//HOUSE B/L MARK		仓库提单标识	X(1)	Y/N,不填认为N
            $message['12'][] = '';//B/L SURRENDERED MARK		提单电放标识	X(1)	S代表电放，W代表海运单，Y代表正本，H代表等电
            $message['12'][] = '';//FREIGHT AGREEMENT CODE/Contract No. 		运费协议代码	X(18)
            $message['12'][] = '';//SHIPPER ID		SHIPPER自有系统代码	X(35)
            $message['12'][] = $send_shipment['job_no'];//SHIPPING ORDER		系统订舱号	X(35)
            $message['12'][] = $container_owner;//SOC		是否货主自有箱	X(3)	若是货主自有箱，则值为SOC。若不是，则放空
            $file_sum++;

            //13:JPNGO:NAGOYA:JPNGO:NAGOYA'
            $message['13'] = array();
            $message['13'][] = '13';//RECORD ID         		记录类型标识	9(2)	13
            $message['13'][] = $send_shipment['trans_discharge'];//DISCHARGE PORT CODE		卸货地代码	X(5)	5位标准港口代码
            $message['13'][] = $send_shipment['trans_discharge_name'];//DISCHARGE PORT		卸货港	X(35)
            $message['13'][] = $send_shipment['trans_destination'];//PLACE CODE OF DELIVERY		交货地代码	X(5)	5位标准港口代码
            $message['13'][] = $send_shipment['trans_destination_name'];//PLACE OF DELIVERY		交货地名称	X(70)
            $message['13'][] = $send_shipment['trans_discharge'];//TRANSHIPMENT PORT CODE		中转港代码	X(5)
            $message['13'][] = $send_shipment['trans_discharge_name'];//TRANSHIPMENT PORT		中转港	X(35)
            $message['13'][] = '';//PLACE CODE OF B/L ISSUE		提单签发地代码	X(5)	使用5位UN目的地代码
            $message['13'][] = '';//PLACE OF B/L ISSUE		提单签发地	X(35)
            $message['13'][] = '';//TRANSHIPMENT FLAG		中转标志	X(2)	"10＝国内中转统码  11＝国内水路中转  12＝国内公路中转  13＝国内铁路中转  21＝国际水水中转  30＝内贸本地  31＝内贸中转"
            $file_sum++;

            //15:HDS:::::::::C'
            //15:LSS:::::::::C'
            //15:YAS:::::::::C'
            //15:CIC:::::::::C'
            //15:BAF:::::::::C'
//            $message['15'][] = '15';//RECORD ID            		记录类型标识	 9(2)	15
//            $message['15'][] = '';//FR. CH. CODE        		运费及费用代码	 X(12)	除海运费外的、需要指定预/到付的费用的代码，如BAF,YAS,LSS
//            $message['15'][] = '';//FR. CH. REMARK    		运费及费用说明	 X(26)
//            $message['15'][] = '';//PAYABLE AT (CODE)   		付款地点代码	 X(5)
//            $message['15'][] = '';//PAYABLE AT              		付款地点	X(20)
//            $message['15'][] = '';//QUANTITY                    		数量	9(6) . 999  	 例：567.389
//            $message['15'][] = '';//CURRENCY                   		币种	X(3)	 例：USD
//            $message['15'][] = '';//RATE OF FR. CH.               		费率	9(10). 9999	 例：256.23
//            $message['15'][] = '';//UNIT OF QUANTITY        		数量单位	 X(6)	 例：/CBM（原为4，改为3）
//            $message['15'][] = '';//AMOUNT                     		总计	9(10) . 999
//            $message['15'][] = '';//PREPAID OR COLLECT     		付款方式	 X(1)	P=预付  C=到付  F=免费
//            $file_sum++;

            //16::SHANGHAI  CO.,LTD.::::UNIT E,11A FLOOR,SREA-OCEAN TOWERII,NO.618,EAST YAN?'AN ROAD,SHANGHAI,:200001,CHINA TEL?:+86-21-63922299::::::::CN:63902299'
            $message['16'] = array();
            $message['16'][] = '16';//RECORD ID         		记录类型标识	9(2)	16
            $message['16'][] = '';//SHIPPER CODE		发货人代码	X(128)	填写发货人组织机构代码
            $shipper_company = cut_array_length(array($send_shipment['shipper_company']), 60, 4);
            //占4格
            for($i = 0; $i < 4; $i++){
                $message['16'][] = isset($shipper_company[$i]) ? $shipper_company[$i] : '';//SHIPPER		发货人名称	X(70)	如发货人为自然人，填写发货人具体人名或填写“××公司+发货人具体人名” ；按各个国家地区海关舱单申报规定填写，目前中国海关暂无要求
            }
            $shipper_address = cut_array_length(explode("\r\n", $send_shipment['shipper_address']), 60, 5);
            //占5格
            for($i = 0; $i < 5; $i++){
                $message['16'][] = isset($shipper_address[$i]) ? $shipper_address[$i] : '';//SHIPPER		发货人名称	X(70)	如发货人为自然人，填写发货人具体人名或填写“××公司+发货人具体人名” ；按各个国家地区海关舱单申报规定填写，目前中国海关暂无要求
            }
            $message['16'][] = '';//City name		城市名称	X(35)
            $message['16'][] = '';//Country sub-entity		省份代码	X(5)	2位国家代码（大写字母）+3位地区代码（数字），参照代码表：UN001
            $message['16'][] = '';//Country sub-entity, name		省份名称	X(35)	与省份代码可以选择填写
            $message['16'][] = '';//Postcode identification		邮政编码	X(9)	国内参照代码表：CN002，国外可不填邮政编码
            $message['16'][] = substr($send_shipment['trans_origin'], 0, 2);//Country, coded		国家代码	X(2)	参照代码表：CN001
            $message['16'][] = $send_shipment['shipper_telephone'];//Communication number (TE)		发货人电话	X(50)
            $message['16'][] = $send_shipment['shipper_email'];//Communication number (EM)		发货人EMAIL	X(50)
            $message['16'][] = '';//Communication number (FX)		发货人传真	X(50)
            $message['16'][] = '';//SHIPPER AEO CODE		发货人AEO代码	X(20)	中国海关对有AEO代码的实际收货人会提供通关便利，如客户要求船公司在舱单申报时要申报AEO代码的该项为必填
            $message['16'][] = '';//SHIPPER CODE CUSTOM CN  		发货人代码（中国海关）	X(128)
            $message['16'][] = '';//ATTN		联系人	X(50)
            $file_sum++;

            //17::TRANSPORT CO.,LTD.::::5F YAFGDA BLDG,NO 5-6,HONM5THI1-CHOME,CHUO-KU.OSAKA 541-0053 JAPAN TE:L?:06-1321-2310::::::::JP:63902299:SJJ@SJJ.COM::SJJ'
            $message['17'] = array();
            $message['17'][] = '17';//RECORD ID         		    记录类型标识	9(2)	17
            if($send_shipment['biz_type'] == 'import'){
                return jsonEcho(array('code' => 1, 'msg' => lang('进口时 收货人代码必填')));
            }
            //判断是否TO ORDER 不是的话,地址必填
            if(substr($send_shipment['consignee_company'], 0, 8) !== 'TO ORDER' && empty(trim($send_shipment['consignee_address']))){
                return jsonEcho(array('code' => 1, 'msg' => lang('收货人地址必填')));
            }
            $message['17'][] = '';//CONSIGNEE CODE		收货人代码	X(128)	填写实际收货人组织机构代码；收货人为自然人的填写身份证、护照号等；按各个国家地区海关舱单申报规定填写，目前中国海关出口暂无要求。进口到中国货物必须要填些该内容
            $consignee_company = cut_array_length(array($send_shipment['consignee_company']), 60, 4);
            //占4格
            for($i = 0; $i < 4; $i++){
                $message['17'][] = isset($consignee_company[$i]) ? $consignee_company[$i] : '';//CONSIGNEE		收货人名称	X(70)	填写实际收货人名称；如收货人为自然人，填写实际收货人具体人名或填写“××公司+实际收货人具体人名”；如收货人为“凭指令确定收货人（TO ORDER）”必须填写通知人数据项内容
            }
            $consignee_address = cut_array_length(explode("\r\n", $send_shipment['consignee_address']), 60, 5);
            //占5格
            for($i = 0; $i < 5; $i++){
                $message['17'][] = isset($consignee_address[$i]) ? $consignee_address[$i] : '';//Street and number/P.O.Box		收货人地址（街道,邮箱）	X(70)	收货人不是TO ORDER的必填
            }
            $message['17'][] = '';//City name		城市名称	X(35)
            $message['17'][] = '';//Country sub-entity		省份代码	X(5)	2位国家代码（大写字母）+3位地区代码（数字），参照代码表：UN001
            $message['17'][] = '';//Country sub-entity, name		省份名称	X(35)	与省份代码可以选择填写
            $message['17'][] = '';//Postcode identification		邮政编码	X(9)	国内参照代码表：CN002，国外可不填邮政编码
            $message['17'][] = substr($send_shipment['trans_destination'], 0, 2);//Country,coded		国家代码	X(2)	参照代码表：CN001
            $message['17'][] = $send_shipment['consignee_telephone'];//Communication number (TE)		收货人电话	X(50)
            $message['17'][] = $send_shipment['consignee_email'];//Communication number (EM)		收货人EMAIL	X(50)
            $message['17'][] = '';//Communication number (FX)		收货人传真	X(50)
            $message['17'][] = '';//Department or employee contact name		收货人具体联系人名称	X(35)
            $message['17'][] = '';//CONSIGNEE AEO CODE		收货人AEO代码	X(20)	中国海关对有AEO代码的实际收货人会提供通关便利，如客户要求船公司在舱单申报时要申报AEO代码的该项为必填
            $file_sum++;

            //18::TRANSPORT CO.,LTD.::::5F YA4543N BLDG,NO 5-6,HONMWEHI1-CHOME,CHUO-KU.OSAKA 541-0053 JAPAN TE:L?:06-1233-1360::::::::JP:63902299::::SJJ'
            $message['18'] = array();
            $message['18'][] = '18';//RECORD ID         		记录类型标识	9(2)	18
            $message['18'][] = '';//NOTIFY   CODE		通知人代码	X(128)	填写通知人组织机构代码,按各个国家地区海关舱单申报规定填写，目前中国海关暂无要求
            $notify_company = cut_array_length(array($send_shipment['notify_company']), 60, 4);
            //占4格
            for($i = 0; $i < 4; $i++){
                $message['18'][] = isset($notify_company[$i]) ? $notify_company[$i] : '';//NOTIFY		通知人名称	X(70)	如通知人为自然人，填写通知人具体人名或填写“××公司+通知人具体人名”
            }
            $notify_address = cut_array_length(explode("\r\n", $send_shipment['notify_address']), 60, 5);
            //占5格
            for($i = 0; $i < 5; $i++){
                $message['18'][] = isset($notify_address[$i]) ? $notify_address[$i] : '';//Street and number/P.O.Box		通知人地址（街道,邮箱）	X(70)	如果通知人名称是SAME AS CONSIGNEE，此栏不用填写
            }
            $message['18'][] = '';//City name		城市名称	X(35)
            $message['18'][] = '';//Country sub-entity		省份代码	X(5)
            $message['18'][] = '';//Country sub-entity, name         		省份名称	X(35)	与省份代码可以选择填写
            $message['18'][] = '';//Postcode identification		邮政编码	X(9)	国内参照代码表：CN002，国外可不填邮政编码
            $message['18'][] = substr($send_shipment['trans_destination'], 0, 2);//Country, coded		国家代码	X(2)	参照代码表：CN001
            $message['18'][] = $send_shipment['notify_telephone'];//Communication number (TE)		通知人电话	X(50)	如果通知人名称是SAME AS CONSIGNEE，此栏不用填写
            $message['18'][] = $send_shipment['notify_email'];//Communication number (EM)		通知人EMAIL	X(50)
            $message['18'][] = '';//Communication number (FX)		通知人传真	X(50)
            $message['18'][] = '';//NOTIFY CODE CUSTOM CN		通知人代码（中国海关）	X(128)
            $message['18'][] = '';//ATTN		联系人	X(50)
            $file_sum++;

            //41:1::1633:CT:CARTONS:10839:0:67.82:523690'
            $message['41'] = array();
            $message['41'][] = '41';//RECORD ID         		记录类型标识	9(2)	41	M
            $message['41'][] = 1;//CARGO SEQUENCE NO.		货物序号	X(5)	正常只需要填1	M
            $message['41'][] = '';//CARGO CODE		货类代码	X(8)		C
            //剩下的等箱型填充完补上

            //44:N/M'
            $message['44'][] = '44';//RECORD ID         		记录类型标识	9(2)	44	M
            $mark_nums = cut_array_length(explode("\r\n", $send_shipment['mark_nums']), 30, 9);
            //占9格
            for($i = 0; $i < 9; $i++){
                $message['44'][] = isset($mark_nums[$i]) ? $mark_nums[$i] : '';//MARKS		唛头	X(35)	填写运输包装的标志及代码（唛头）。	 M
            }
            $file_sum++;

            //47:SOCKS'
            $message['47'] = array();
            $message['47'][] = '47';//RECORD ID         		记录类型标识	9(2)	47	M
            $description = cut_array_length(explode("\r\n", $send_shipment['description']), 60, 9);
            //占9格
            for($i = 0; $i < 9; $i++){
                $message['47'][] = isset($description[$i]) ? $description[$i] : '';//CARGO DESCRIPTION 		货物描述	X(70)	填写足以鉴别货物的性质的简明描述。	 M
            }
            $message['47'][] = '';//Free text		货物描述补充信息	X(512)		O
            $file_sum++;

            //51::FSCU5136180:SJJA489245:40GP:F:815:4959:3730:32.82::::::0220386'
            //51::TWCU4045495:SJJA489229:40GP:F:818:5880:3730:35::::::0220686'
            $good_outers = $send_shipment['good_outers'];//0
            $good_weight = $send_shipment['good_weight'];//0
            $good_volume = $send_shipment['good_volume'];//0
            // $sql = "SELECT `biz_shipment_container`.container_no, biz_shipment_container.packs,biz_shipment_container.weight,biz_shipment_container.volume, `biz_container`.`container_size`, `biz_container`.`seal_no`, `biz_container`.`container_tare`
            // FROM `biz_shipment_container`
            // LEFT JOIN `biz_container` ON `biz_container`.`container_no` = `biz_shipment_container`.`container_no` AND `biz_container`.`consol_id` = '{$consol['id']}'
            // WHERE `shipment_id` = '{$send_shipment['id']}'";
            // $temp_rs = Model('m_model')->sqlquery($sql);
            // $shipment_containers = $temp_rs->result_array();
            // $message['51'] = array();
            // foreach ($shipment_containers as $shipment_container) {
            //     $this_array = array();
            //     $this_array[] = '51';//RECORD ID         		记录类型标识	9(2)	51	M
            //     $this_array[] = '';//CARGO SEQUENCE NO.		货物序号	9(3)	此字段无意义	C
            //     $this_array[] = $shipment_container['container_no'];//CTN. NO.		箱号	X(12)	由4个字母即货主编码、6位数字序列号以及1位检测数字组成。填写内容必须由大写字母与数字组成，可以出现一次中划线	M
            //     $this_array[] = $shipment_container['seal_no'];//SEAL NO.     		主铅封号	X(10)	机械封志且封志人为CA	O
            //     $this_array[] = $shipment_container['container_size'];//CTN. SIZE & TYPE		集装箱尺寸类型	X(4)	使用UN95码	M
            //     $this_array[] = 'F';//CTN. STATUS		箱状态（重箱或者空箱标识代码）	X(1)	E/L/F	M
            //     $good_outers += $this_array[] = $shipment_container['packs'];//CTN. NUMBERS OF PACKAGES		箱内货物件数	9(6)		M
            //     $good_weight += $this_array[] = $shipment_container['weight'];//NET WEIGHT		箱内货重	9(5). 999		M
            //     $this_array[] = $shipment_container['container_tare'];//TARE WEIGHT		箱皮重	9(5). 999		C
            //     $good_volume += $this_array[] = $shipment_container['volume'];//CTN. CARGO MEASUREMENT		箱内货物尺码	9(5) . 999		M
            //     $this_array[] = '';//Over Length Front		前超	9(4)	厘米	C
            //     $this_array[] = '';//Over Length Back		后超	9(4)	厘米	C
            //     $this_array[] = '';//Over Width Left		左超	9(4)	厘米	C
            //     $this_array[] = '';//Over Width Right		右超	9(4)	厘米	C
            //     $this_array[] = '';//Over Height		超高	9(4)	厘米	C
            //     $this_array[] = '';//STOWAGE LOCATION		贝位	9(7)		C
            //     $this_array[] = '';//Container supplier type, coded		集装箱来源代码	9(2)	"1=货主自备箱  2=承运人提供箱  3=拼箱人提供箱  4=拆箱人提供箱  5=第三方提供箱"	O
            //     $this_array[] = $container_owner;//SOC		是否货主自有箱	X(3)	若是货主自有箱，则值为SOC。若不是，则放空	O
            //     $message['51'][] = $this_array;
            //     $file_sum++;
            // }

            //获取包装类型代码的 字典表配置--start
            //2023-05-06 先用亿通的 等他们自己来了试试
            $this_dict = Model('bsc_dict_model')->get_one('name', $send_shipment['good_outers_unit'], 'packing_unit_edi', 'JJLINE');
            if(empty($this_dict)){
                $result['msg'] = lang('未配置包装类型代码') . " " . $send_shipment['good_outers_unit'];
                echo json_encode($result);
                return;
            }
            $good_outers_unit_code = $this_dict['value'];
            //获取包装类型代码的 字典表配置--end

            $message['41'][] = $good_outers;//NUMBERS OF PACKAGES		货物件数	9(8)	填写本项货物的包装数量，包装以不再拆解为准；散货时, 件数=“1”	M
            $message['41'][] = '';//PACKAGE TYPE CODE		包装类型代码	X(2)	参照代码表CN005，只能填写数字或字母	M
            $message['41'][] = $good_outers_unit_code;//PACKAGE TYPE 		包装类型	X(35)		C
            $message['41'][] = $good_weight;//CARGO GROSS WEIGHT		货毛重	9(14,4)	单位：千克；最大长度：10位整数，精确到小数点后4位。	M
            $message['41'][] = 0;//CARGO NET WEIGHT		货净重	9(10). 999	单位：千克；整数位最多10位，小数位最多3位	C
            $message['41'][] = $good_volume;//CARGO MEASUREMENT		货尺码	9(5). 999	立方米	M
            $message['41'][] = '';//Tariff code number (Customs)HS CODE		海关税则编号	X(12)	填写6位HS商品编码，对于目的为日本/中国/菲律宾的货物（空箱除外）必填	O
            $file_sum++;

            //52:40:GP:2'
            $box_info = json_decode($send_shipment['box_info'], true);
            foreach ($box_info as $box){
                $size = $box['size'];
                $message['52'] = array();
                $message['52'][] = '52';//RECORD ID 		记录类型标识	9(2)	52	M
                $message['52'][] = substr($size, 0, 2);//CONTAINER SIZE		集装箱尺寸 	9(2)	20、40	M
                $message['52'][] = substr($size, 2, 2);//CONTAINER TYPE		集装箱类型 	X(2)	GP/DC/HC/HT/RF/RH/FR/OT/TK	M
                $message['52'][] = $box['num'];//BOOKING PLAN		预配箱量	9(3)	1-100	M
                $file_sum++;
            }

            //61:'
//            $message['61'] = array();
//            $message['61'][] = '61';//RECORD ID 		记录类型标识	9(2)	61	C
//            $message['61'][] = '';//SHIPPER ON BL		发货人提单显示	X(35)		C
//            $file_sum++;

            //62:'
//            $message['62'] = array();
//            $message['62'][] = '62';//RECORD ID 		记录类型标识	9(2)	62	C
//            $message['62'][] = '';//CNEE ON BL 		收货人提单显示 	X(35)		C
//            $file_sum++;

            //63:'
//            $message['63'] = array();
//            $message['63'][] = '63';//RECORD ID 		记录类型标识	9(2)	63	C
//            $message['63'][] = '';//NOTIFY ON BL		通知人提单显示 	X(35)		C
//            $file_sum++;


            //填充内容到文本里
            $file_content .= message_format($message, true, $replace_arr) . "\r\n";
            $message = array();

            //12:TEJJCSH203::::CNSHA:SHANGHAI:CNSHA:SHANGHAI:CY-CY:P:20230120::::::SHNC:'
            //13:JPNGO:NAGOYA:JPNGO:NAGOYA'
            //15:LSS:::::::::C'
            //15:YAS:::::::::P'
            //15:CIC:::::::::C'
            //15:BAF:::::::::P'
            //16::SHANG LOGISTICS CO.,LTD::::ROOM 803, NO.1236, SICHUAN NORTH ROAD, SHANGHAI, CHINA 200080:TEL?:021:-6334344FAX?:021-6312322:ATTN?:MS.CHEN::::::CN:63902299'
            //17::CO.,LTD.::::169-0075 IGARI BLDG SUWA  3RD FLOOR:1-23-9HHHHHHJOBABA,SHINJUKU-KU,TO:KYO,JAPANTEL?:03-2345-5242FAX?:03-5432-5121:ATTN?:MS SUWA YOKO::::::JP:63902299:SJJ@SJJ.COM::SJJ'
            //18::CO.,LTD.::::169-0235 EREWA BLDG SUWA  5RD FLOOR:1-23-9TAKADANOBABA,SHINJUKU-KU,TO:KYO,JAPANTEL?:03-2345-5242FAX?:03-5432-5121:ATTN?:MS SUWA YOKO::::::JP:63902299::::SJJ'
            //41:1::97:PK:PACKAGES:102500:0:104.76:903290'
            //44:SJ-001PARTS NOPARTS NAM:EQ?'TY(PC)?:C/N?:1-UPINV NO?:SNHC:J-230120MADE IN CHINA'
            //47:TRAG MOTOR'
            //51::TWCU2059740:SJJB499836:20GP:F:20:21090:2250:21.6::::::0090582'
            //51::TWCU2060660:SJJB499835:20GP:F:17:16370:2250:18.36::::::0110382'
            //51::TWCU2065677:SJJB502486:20GP:F:20:22080:2250:21.6::::::0110482'
            //51::TWCU2077106:SJJB502399:20GP:F:20:20740:2250:21.6::::::0110682'
            //51::TWCU2170055:SJJB507604:20GP:F:20:22220:2250:21.6::::::0090482'
            //52:20:GP:5'
            //12:TEJJCSH6607::::CNSHA:SHANGHAI:CNSHA:SHANGHAI:CY-CY:C:20230120:::::S:JJSNYSN:::'
            //13:JPNGO:NAGOYA:JPNGO:NAGOYA'
            //15:OCB:::::::::C'
            //16::SONY (CHINA) LIMITED::::BUILDING 33,NO.82,MOJIROAD, CHINA (SHANGHAI):PILOT AAFE TRADE ZONETE:L?:021-52698654:::::::CN:63902299'
            //17::SONY KOSAI SITE::::201, SAFFDJUKU,KOSAI-SHI,SHIZUOKA-KEN,JAPAN 431-0496,:JAPANCONTACT P:ERSON?:KAMADA,MARI;SATSUKAWA,YUKA:TEL?:081-050-6009-3650  EMAIL?:MARI.AA:M23A@JK.AFDA.COM;HACA.SATSUJJEA@JK.AFDA.COM:::::JP:63902299:SJJ@SJJ.COM::SJJ'
            //18::SOLUTIONS, INC::::30 IIMA,OHYA-CHO,IWA CITY,AICHI ,492-8545 JAPAN:MS.NAOKO.AOZA:KI FAX?:081-050-6009-365 TEL?:081-050-6009-365:::::::JP:63902299::::SJJ'
            //41:1::52:PK:PACKAGES:3477.72:0:84.23:842490'
            //44:N/M'
            //47:CAMER PARTS HS CODE?:80'
            //51::FSCU5134779:SJJB502424:40GP:F:27:1076.27:3730:41.6::::::0180286'
            //51::TWCU4079263:SJJB486947:40GP:F:25:2401.45:3730:42.63::::::0260684'
            //12:TJJA3606::::CNSHA:SHANGHAI:CNSHA:SHANGHAI:CY-CY:C:20230120::::::JJSNYSN:::'
            //13:JPNGO:NAGOYA:JPNGO:NAGOYA'
            //15:OCB:::::::::C'
            //16::SONY (CHINA) LIMITED::::BUILDING 33,NO.82,MOJIROAD, CHINA (SHANGHAI):PILOT AAFE TRADE ZONETE:L?:021-52698654:::::::CN:63902299'
            //17::SONY KOSAI SITE::::201, SAFFDJUKU,KOSAI-SHI,SHIZUOKA-KEN,JAPAN 431-0496:JAPANCONTACT PERSON?:KAMADA,MARI;SATSUKAWA,YUKA:TEL?:081-050-6009-3650  MAIL?:MARI.AA:M23A@JK.AFDA.COM;:HACA.SATSUJJEA@JK.AFDA.COM:::::JP:63902299:SJJ@SJJ.COM::SJJ'
            //18::SOLUTIONS, INC::::30 IIMA,OHYA-CHO,IWA CITY,AICHI ,492-8545 JAPAN:MS.NAOKO.AOZA:KI FAX?:081-050-6009-365 TEL?:081-050-6009-365:::::::JP:63902299::::SJJ'
            //41:1::18:PK:PACKAGES:1963.57:0:26.99:824220'
            //44:N/M'
            //47:PARTS HS CODE?:850'
            //51::TWCU4077002:SJJB486908:40GP:F:18:1963.57:3730:26.99::::::0300384'
            //12:TEJJCSHNGB300095::::CNSHA:SHANGHAI:CNSHA:SHANGHAI:CY-CY:P:20230120::::::JJJCL40'
            //13:JPNGO:NAGOYA:JPNGO:NAGOYA'
            //15:LSS:::::::::P'
            //15:YAS:::::::::P'
            //15:CIC:::::::::C'
            //15:BAF:::::::::P'
            //16::SHANGHAI JINCHANG LOGISTICS CO.,LTD.::::UNIT E,11TH FLOOR,SINO-OCEAN TOWERII,NO.618,EAST YAN?'AN ROAD,SHANGHAI,:200001,CHINA TEL?:+86-21-63902299::::::::CN:63902299'
            //17::LIMITED::::3F,YAESG.,5-9,2-CHOME,YAESU,CHUO-KU,TYO 1PAN TEL?::03-6225420 FAX?:03-256370::::::::JP:63902299:SJJ@SJJ.COM::SJJ'
            //18::LTD.::::TERMINAL BUSI  CENTER OFFICE 1-7,ISOBE-HOME,CHUO-K:U K51-004,JAPAN TEL?:+85268286::::::::JP:63902299::::SJJ'
            //41:1::511:CT:CARTONS:6473:0:43:623140'
            //44:N/M'
            //47:GARMENTS'
            //51::HAHU4035862:SJJB507787:40GP:F:511:6473:3730:43::::::0100604'
            //52:40:GP:1'
        }

        //99:74'
        $message['99'] = array();
        $message['99'][] = '99';
        $message['99'][] = ++$file_sum;//16 记录文件总数
        $file_content .= message_format($message, true);

        //保存文件txt
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        //ESL_EDI/日期
        $new_dir = $root_path . '/upload/message/JJLINE_EDI/SO/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $consol_id . '.txt';
        $file_path = $new_dir . $file_name;
        if(preg_match('/[\x{4e00}-\x{9fa5}]+/u', $file_content, $matches1) === 1){
            echo json_encode(array('code' => 1, 'msg' => lang("存在中文或中文标点符号,生成失败"), $matches1));
            return;
        }
        file_put_contents($file_path, $file_content);

        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => 'SO',
            'receiver' => 'JJLINE',
        );
        update_message($consol_id, $message_data, 'SO');
        echo json_encode(array('code' => 0, 'msg' => lang('文件生成成功')));
    }
}