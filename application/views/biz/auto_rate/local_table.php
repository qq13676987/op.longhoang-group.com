<body>
<?php
$is_must = isset($_GET['is_must']) ? $_GET['is_must'] : 1;
if (!empty($_GET["no_edit"])) echo "<br><span style='color:red;'>如果发现数据有错误，请联系李凌她们调整修改！<br><br></span>";
?>
<div id="tabs" class="easyui-tabs" style="width:100%;">
    <div title="普货(GC)">
        <table id="tt" class="easyui-datagrid" style="width:100%;"
               data-options="url:'/biz_auto_rate/local_get_data?goods_type=GC&trans_origin=<?= $trans_origin ?>&trans_carrier=<?= $trans_carrier ?>&booking_agent=<?= $booking_agent ?>&sailing_area=<?= $sailing_area ?>&boxes=<?php echo implode(',', $GC); ?>',singleSelect:true,toolbar:'#tb'">
            <thead>
            <tr>
                <th data-options="field:'charge',width:100,align:'center'">费用名称</th>
                <th data-options="field:'must',width:100,align:'center'">是否可选</th>
                <th data-options="field:'start_date',width:100,align:'center'">生效日期</th>
                <th data-options="field:'client_code',width:150,align:'center'">付费对象</th>
                <th data-options="field:'票',width:300,align:'center'">票</th>

                <?php foreach ($GC as $item): ?>
                    <th data-options="field:'<?= $item ?>',width:300,align:'center'"><?= $item ?></th>
                <?php endforeach; ?>
            </tr>
            </thead>
        </table>
    </div>
    <div title="危险品(DR)">
        <table id="tt2" class="easyui-datagrid" style="width:100%;"
               data-options="url:'/biz_auto_rate/local_get_data?goods_type=DR&trans_origin=<?= $trans_origin ?>&trans_carrier=<?= $trans_carrier ?>&booking_agent=<?= $booking_agent ?>&sailing_area=<?= $sailing_area ?>&boxes=<?php echo implode(',', $GC); ?>',singleSelect:true,toolbar:'#tb2'">
            <thead>
            <tr>
                <th data-options="field:'charge',width:100,align:'center'">费用名称</th>
                <th data-options="field:'must',width:100,align:'center'">是否可选</th>
                <th data-options="field:'start_date',width:100,align:'center'">生效日期</th>
                <th data-options="field:'client_code',width:150,align:'center'">付费对象</th>
                <th data-options="field:'票',width:300,align:'center'">票</th>

                <?php foreach ($DR as $item): ?>
                    <th data-options="field:'<?= $item ?>',width:300,align:'center'"><?= $item ?></th>
                <?php endforeach; ?>
            </tr>
            </thead>
        </table>
    </div>
</div>


<?php
if (empty($_GET["no_edit"])){
?>
<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
                   onclick="javascript:add()"><?= lang('add'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
                   onclick="javascript:del()">整行<?= lang('delete'); ?></a>
            </td>
            <td width='80'></td>
        </tr>
    </table>
</div>
<div id="tb2" style="padding:3px;">

    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
                   onclick="javascript:add()"><?= lang('add'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
                   onclick="javascript:del2()">整行<?= lang('delete'); ?></a>
            </td>
            <td width='80'></td>
        </tr>
    </table>
    <?php } ?>

</div>

</body>

<div id="dlg" class="easyui-dialog" style="width:500px;height:450px"
     data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons'">
    <form id="fm" method="post" novalidate style="padding:5px 25px">
        <div>
            <div style="display: inline-block">
                <h3>local定额</h3>
            </div>

            <?php
            if (empty($_GET["no_edit"])) {
                ?>
                <div style="display: inline-block; right: 25px; position: absolute;top:45px">
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" onclick="del()"
                       style="width:90px">删除</a>
                </div>
            <?php } ?>
        </div>
        <table border="0" cellspacing="10">
            <div style="border:1px solid red"></div>
            <input name="charge_code" id="charge_code" type="hidden">
            <input name="id" id="id" type="hidden">

            <input name="biz_type" id="biz_type" type="hidden">
            <input name="trans_mode" id="trans_mode" type="hidden">
            <input name="type" id="type" type="hidden">
            <input name="rate_type" id="rate_type" type="hidden">
            <input name="vat" id="vat" type="hidden">
            <input name="booking_agent" id="booking_agent" type="hidden">
            <input name="client_code" id="client_code" type="hidden">
            <input name="trans_origin" id="trans_origin" type="hidden">
            <input name="trans_carrier" id="trans_carrier" type="hidden">
            <tr>
                <td><?= lang('goods_type'); ?></td>
                <td>
                    <select name="goods_type" readonly id="goods_type" class="easyui-combobox" style="width:224px;"
                            data-options="
                    required:true,
                    valueField:'value',
                    textField:'valuename',
                    url:'/bsc_dict/get_option/goods_type',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td>按箱/票</td>
                <td>
                    <select name="number_type" readonly id="number_type" class="easyui-combobox" style="width:224px;"
                            data-options="
                                    required:true,
                                    editable:false,
                                    onSelect: function(rec){
                                        if(rec.value==1){
                                            $('#box_type').combobox({readonly:false});
                                        }else{
                                            $('#box_type').combobox({readonly:true});
                                            $('#box_type').combobox('setValue', '');
                                        }
                                    },
                                 ">
                        <option value=""></option>
                        <option value="1">箱</option>
                        <option value="2">票</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('box_type'); ?></td>
                <td>
                    <select name="box_type" readonly id="box_type" class="easyui-combobox" style="width:224px;"
                            data-options="
                        editable:false,
                        valueField:'value',
                        textField:'value',
                        url:'/bsc_dict/get_option/container_size',
                        ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('charge'); ?>:</td>
                <td>
                    <input name="charge" readonly id="charge" class="easyui-combobox" style="width:224px;"
                           data-options="
                        required:true,
                        valueField:'charge_name_cn',
                        textField:'charge_name_long',
                        url:'/biz_charge/get_option',
                        onSelect: function(rec){
                            $('#charge_code').val(rec.charge_code);
                        },
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">
                </td>
            </tr>
            <tr>
                <td><a href="javascript:void(0);"
                       onclick="select_all('sailing_area', 'sailing_area');"><?= lang('sailing_area'); ?></a></td>
                <td>
                    <select class="easyui-combobox" readonly name="sailing_area" id="sailing_area" style="width:224px;"
                            data-options="
                        editable:false,
                        required:true,
                        multiple:true,
                        valueField:'sailing_area',
                        textField:'sailing',
                        url:'/biz_port/get_sailing',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td>付款方式</td>
                <td>
                    <select name="payment" id="payment" class="easyui-combobox" style="width:224px;"
                            data-options="editable:false,required:false,">
                        <option value="">PP+CC</option>
                        <option value="PP">PP</option>
                        <option value="CC">CC</option>
                    </select>
                </td>
            </tr>


            <tr>
                <td><?= lang('currency'); ?></td>
                <td>
                    <input name="currency" id="currency" class="easyui-combobox" style="width:224px;" data-options="
                        editable:false,
                        required:true,
                        valueField:'name',
                        textField:'name',
                        url:'/biz_bill_currency/get_currency/sell',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">
                </td>
            </tr>
            <tr>
                <td><?= lang('unit_price'); ?></td>
                <td>
                    <input name="unit_price" id="unit_price" class="easyui-numberbox" required="true" precision="2"
                           style="width:224px;"></td>
            </tr>
            <tr>
                <td>可选/必选</td>
                <td>
                    <select name="is_must" id="is_must_edit" class="easyui-combobox" style="width:224px;"
                            data-options="editable:false,required:true,">
                        <option value="0">可选</option>
                        <option value="1">必选</option>
                    </select>
                </td>
            </tr>
        </table>
    </form>
</div>
<?php
if (empty($_GET["no_edit"])) {
    ?>
    <div id="dlg-buttons">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="save()"
           style="width:90px">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
           onclick="javascript:$('#dlg').dialog('close')" style="width:90px">取消</a>
    </div>
    <?php
}
?>
<!--新增页面-->
<div id="dlg_add" class="easyui-dialog" style="width:800px;height:650px"
     data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons-add'">
    <form id="fm_add" method="post" novalidate style="padding:5px 25px">
        <div>
            <div style="display: inline-block">
                <h3>local定额</h3>
            </div>
        </div>
        <table border="0" cellspacing="10">
            <div style="border:1px solid red"></div>
            <input name="charge_code" id="charge_code_add" type="hidden">

            <input name="biz_type" id="biz_type_add" type="hidden">
            <input name="trans_mode" id="trans_mode_add" type="hidden">
            <input name="type" id="type_add" type="hidden">
            <input name="rate_type" id="rate_type_add" type="hidden">
            <input name="vat" id="vat_add" type="hidden">
            <input name="booking_agent" id="booking_agent_add" type="hidden">
            <input name="trans_origin" id="trans_origin_add" type="hidden">
            <input name="trans_carrier" id="trans_carrier_add" type="hidden">
            <tr>
                <td><?= lang('goods_type'); ?></td>
                <td>
                    <select name="goods_type" readonly id="goods_type_add" class="easyui-combobox" style="width:224px;"
                            data-options="
                    required:true,
                    valueField:'value',
                    textField:'valuename',
                    url:'/bsc_dict/get_option/goods_type',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('charge'); ?>:</td>
                <td>
                    <input name="charge" id="charge_add" class="easyui-combobox" style="width:224px;" data-options="
                        required:true,
                        valueField:'charge_name_cn',
                        textField:'charge_name_long',
                        url:'/biz_charge/get_option',
                        onSelect: function(rec){
                            $('#charge_code_add').val(rec.charge_code);
                        },
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">
                </td>
            </tr>

            <tr>
                <td><a href="javascript:void(0);"
                       onclick="select_all('sailing_area_add', 'sailing_area');"><?= lang('sailing_area'); ?></a></td>
                <td>
                    <select class="easyui-combobox" name="sailing_area" id="sailing_area_add" style="width:224px;"
                            data-options="
                        editable:false,
                        required:true,
                        multiple:true,
                        valueField:'sailing_area',
                        textField:'sailing',
                        url:'/biz_port/get_sailing',
                    ">
                    </select>
                </td>

            </tr>
            <tr>
                <td>付款方式</td>
                <td>
                    <select name="payment" id="payment_add" class="easyui-combobox" style="width:224px;"
                            data-options="editable:false,required:false,">
                        <option value="">PP+CC</option>
                        <option value="PP">PP</option>
                        <option value="CC">CC</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('currency'); ?></td>
                <td>
                    <input name="currency" id="currency_add" class="easyui-combobox" style="width:224px;" data-options="
                        editable:false,
                        required:true,
                        valueField:'name',
                        textField:'name',
                        url:'/biz_bill_currency/get_currency/sell',
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">
                </td>
            </tr>
            <tr>
                <td>按箱/票</td>
                <td>
                    <select name="number_type" id="number_type_add" class="easyui-combobox" style="width:224px;"
                            data-options="
                                    required:true,
                                    editable:false,
                                    onSelect: function(rec){
                                        if(rec.value==1){
                                        //选箱
                                            $('#add_but').css('visibility','visible');
                                            $('#table').show();
                                            $('#unit_price2').hide();
                                        }else{
                                        //选票
                                            $('#unit_price2').show();
                                            $('#add_but').css('visibility','hidden');
                                            $('#table').hide();
                                        }
                                    },
                                 ">
                        <option value=""></option>
                        <option value="1">箱</option>
                        <option value="2">票</option>
                    </select>
                </td>
                <td>
                    <div id="unit_price2" hidden>
                        <span style="margin-right: 10px">按票单价</span>
                        <input name="unit_price" id="unit_price_add" class="easyui-numberbox" precision="2"
                               style="width:120px;">
                    </div>
                </td>
                <td>
                    <a id="add_but" style="visibility: hidden" href="javascript:void(0)" class="easyui-linkbutton"
                       iconCls="icon-add" plain="true" onclick="add_table()"></a>
                </td>
            </tr>
            <tr>
                <td>可选/必选</td>
                <td>
                    <select name="is_must" id="is_must" class="easyui-combobox" style="width:224px;"
                            data-options="editable:false,required:true,">
                        <option value="0">可选</option>
                        <option value="1">必选</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('付费对象'); ?></td>
                <td>
                    <select name="client_code"  id="client_code1" class="easyui-combobox" style="width:224px;"
                            data-options="
                    required:true,
                    valueField:'client_code',
                    textField:'company_name',
                    ">
                        <option value="<?= $booking_agent ?>">订舱代理</option>
                    </select>
                </td>
                <td><a href="javascript:void(0);" onclick="reload_client()">其他更多</a></td>
            </tr>
            <tr>
                <td><?= lang('生效日期'); ?></td>
                <td>
                    <input name="start_date"  id="start_date" required class="easyui-datebox" style="width:224px;">

                </td>
            </tr>
            <table id="table" hidden>
                <hr>
                <tr>
                    <th style="text-align: center">
                        <span>箱型</span>
                    </th>
                    <th style="text-align: center">
                        <span>单价</span>
                    </th>
                </tr>
                <tr>
                    <td>
                        <select name="box_type_1" id="box_type_add_1" class="easyui-combobox" style="width:200px;"
                                data-options="
                        editable:false,
                        valueField:'value',
                        textField:'value',
                        url:'/bsc_dict/get_option/container_size',
                        ">
                        </select>
                    </td>
                    <td>
                        <input name="unit_price_1" id="unit_price_add_1" class="easyui-numberbox"
                               precision="2"
                               style="width:200px;">
                    </td>
                </tr>
            </table>
        </table>

<!--        <input type="hidden" name="is_must" id="is_must_add" value="--><?//=$is_must;?><!--">-->
    </form>
</div>
<div id="dlg-buttons-add">
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-ok" onclick="save_add()"
       style="width:90px">保存</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel"
       onclick="javascript:$('#dlg_add').dialog('close')" style="width:90px">取消</a>
</div>
<script>
    var url;
    var filter = {};
    $(function () {
        $("#tabs").tabs('select', '<?=$title?>');
    });

    //航线区域 全选当前输入框
    function select_all(input_id, key = '') {
        var input = $('#' + input_id);
        var data = input.combobox('getData');
        var Values = [];
        $.each(data, function (i, it) {
            if ($.inArray(it[key], Values) === -1) Values.push(it[key]);
        });

        input.combobox('setValues', Values);
    }

    function del() {
        var id = $('#id').val();
        if (id != '') {
            $.messager.confirm('Confirm', '您确定要删除这个费用吗?', function (r) {
                if (r) {
                    $.post('/biz_auto_rate/delete_data?id=' + id, {id: id}, function (result) {
                        if (result.success) {
                            //选择现在的页面
                            var tab = $('#tabs').tabs('getSelected');
                            var title = tab.panel('options').title;

                            window.location.href = "/biz_auto_rate/local_table?trans_origin=<?=$trans_origin?>&trans_carrier=<?=$trans_carrier?>&booking_agent=<?=$booking_agent?>&sailing_area=<?=$sailing_area?>&title=" + title;

                            $('#dlg').dialog('close');
                        } else {
                            $.messager.show({    // show error message
                                title: 'Error',
                                msg: result.errorMsg,
                            });
                        }
                    }, 'json');
                }
            });
        }
    }

    function edit(id) {
        $.ajax({
            type: "POST",
            dataType: "json",//预期服务器返回的数据类型
            url: '/biz_auto_rate/local_get',
            data: {id: id},
            success: function (res) {
                console.log(res);
                $('#dlg').dialog('open').dialog('center').dialog('setTitle', 'Edit');
                $('#fm').form('clear');
                url = '/biz_auto_rate/update_data?id=' + id;

                $('#goods_type').next('span').find('input').css("background-color", "#dddddd");
                $('#charge').next('span').find('input').css("background-color", "#dddddd");
                $('#number_type').next('span').find('input').css("background-color", "#dddddd");
                $('#box_type').next('span').find('input').css("background-color", "#dddddd");
                $('#sailing_area').next('span').find('input').css("background-color", "#dddddd");

                var tab = $('#tabs').tabs('getSelected');
                var title = tab.panel('options').title;
                if (title == '普货(GC)') {
                    $('#goods_type').combobox('setValue', 'GC');
                } else if (title == '危险品(DR)') {
                    $('#goods_type').combobox('setValue', 'DR');
                }

                //给数据赋予固定值
                $('#biz_type').val('export');
                $('#trans_mode').val('FCL');
                $('#type').val('cost');
                $('#rate_type').val('consol');
                $('#vat').val('0%');
                $('#booking_agent').val('<?=$booking_agent?>');
                $('#client_code').val('<?=$booking_agent?>');
                $('#trans_origin').val('<?=$trans_origin?>');
                $('#trans_carrier').val('<?=$trans_carrier?>');
                $('#id').val(id);

                //可变值
                $('#number_type').combobox('setValue', res.number_type);
                $('#box_type').combobox('setValue', res.box_type);
                $('#sailing_area').combobox('setValues', res.sailing_area);
                $('#payment').combobox('setValue', res.payment);
                $('#charge_code').val(res.charge_code);
                $('#charge').combobox('setValue', res.charge);
                $('#currency').combobox('setValue', res.currency);
                $('#unit_price').numberbox('setValue', res.unit_price);
                $('#is_must_edit').combobox('setValue', res.is_must);
            },
            error: function () {
                alert('修改错误请联系管理员');
            }
        });
    }

    function save() {
        ajaxLoading();
        $('#fm').form('submit', {
            url: url,
            onSubmit: function () {
                $('#sailing_area').combobox('setValue', $('#sailing_area').combobox('getValues').join(','));
                var number_type = $('#number_type').combobox('getValue');

                if (number_type == '1') {
                    if (box_type == '') {
                        alert('选择箱后箱型必填');
                        return false;
                    }
                }

                //判断航线是否包含
                var sailing_area_search = '<?=$sailing_area?>';
                var sailing_area = $('#sailing_area').combobox('getValues').join(',');

                if (sailing_area.indexOf(sailing_area_search) == '-1') {
                    alert('必须包含(<?=$sailing_area_cn?>)区域');
                    $('#sailing_area').combobox('setValues', '');
                     ajaxLoadEnd();
                    return false;
                }

                return $(this).form('validate');
            },
            success: function (result) {
                // $('#dlg').addClass('hide');
                var res = $.parseJSON(result);
                console.log(result);


                if (res.code == 0) {
                    /* $('#tt').datagrid('reload');
                     $('#tt2').datagrid('reload');*/


                    //选择现在的页面
                    var tab = $('#tabs').tabs('getSelected');
                    var title = tab.panel('options').title;

                    window.location.href = "/biz_auto_rate/local_table?trans_origin=<?=$trans_origin?>&trans_carrier=<?=$trans_carrier?>&booking_agent=<?=$booking_agent?>&sailing_area=<?=$sailing_area?>&title=" + title;

                    ajaxLoadEnd();
                    $('#dlg').dialog('close');
                } else {
                    ajaxLoadEnd();
                    $.messager.alert('Tips', res.msg);
                }
            }
        });
    }

    function add() {

        $('#dlg_add').dialog('open').dialog('center').dialog('setTitle', 'Add');
        $('#fm_add').form('clear');

        url = '/biz_auto_rate/add_datas';

        $('#goods_type_add').next('span').find('input').css("background-color", "#dddddd");
        $('#payment_add').combobox('setValue', 'PP');

        var tab = $('#tabs').tabs('getSelected');
        var title = tab.panel('options').title;
        if (title == '普货(GC)') {
            $('#goods_type_add').combobox('setValue', 'GC');
        } else if (title == '危险品(DR)') {
            $('#goods_type_add').combobox('setValue', 'DR');
        }


        //给数据赋予固定值
        $('#biz_type_add').val('export');
        $('#trans_mode_add').val('FCL');
        $('#type_add').val('cost');
        $('#rate_type_add').val('consol');
        $('#vat_add').val('0%');
        $('#booking_agent_add').val('<?=$booking_agent?>');
        $('#trans_origin_add').val('<?=$trans_origin?>');
        $('#trans_carrier_add').val('<?=$trans_carrier?>');
        $('#is_must_add').val(<?=$is_must;?>);
    }

    function save_add() {
        $('#fm_add').form('submit', {
            url: url,
            onSubmit: function (param) {
                $('#sailing_area_add').combobox('setValues', $('#sailing_area_add').combobox('getValues').join(','));

                //判断航线是否包含
                var sailing_area_search = '<?=$sailing_area?>';
                var sailing_area = $('#sailing_area_add').combobox('getValues').join(',');

                if (sailing_area.indexOf(sailing_area_search) == '-1') {
                    alert('必须包含(<?=$sailing_area_cn?>)区域');
                    $('#sailing_area_add').combobox('setValues', '');
                    ajaxLoadEnd();
                    return false;
                }

                //判断是否箱还是票
                var number_type = $('#number_type_add').combobox('getValue');
                if (number_type == '1') {
                    var rows_table = document.getElementById("table").rows.length;

                    var arr_table = {};
                    var arr2_table = [];
                    for (var i = 1; i < rows_table; i++) {
                        //为空不允许添加
                        if ($('#box_type_add_' + i).combobox('getValue') == '') {
                            alert('第' + i + '行箱型为空');
                            return false;
                        }
                        if ($('#unit_price_add_' + i).numberbox('getValue') == '') {
                            alert('第' + i + '行单价为空');
                            return false;
                        }

                        arr_table[i] = {
                            box_type: $('#box_type_add_' + i).combobox('getValue'),
                            unit_price: $('#unit_price_add_' + i).numberbox('getValue'),
                        }
                        arr2_table[i] = [$('#box_type_add_' + i).combobox('getValue')];
                    }
                    if (isRepeat(arr2_table)) {
                        alert('存在重复条件');
                        return false;
                    }
                } else if (number_type == '2') {
                    if ($('#unit_price_add').numberbox('getValue') == '') {
                        alert('选票后单价必填');
                        return false;
                    }
                    var arr_table = {};
                }

                arr_table = JSON.stringify(arr_table);
                param.table = arr_table;
                param.sailing_area = sailing_area;
                console.log(sailing_area);

                return $(this).form('validate');
            },
            success: function (result) {
                // $('#dlg').addClass('hide');
                var res = JSON.parse(result);
                if (res.code == 0) {
                    alert(res.msg);
                    /* $('#tt').datagrid('reload');
                     $('#tt2').datagrid('reload');*/


                    //选择现在的页面
                    var tab = $('#tabs').tabs('getSelected');
                    var title = tab.panel('options').title;

                    window.location.href = "/biz_auto_rate/local_table?trans_origin=<?=$trans_origin?>&trans_carrier=<?=$trans_carrier?>&booking_agent=<?=$booking_agent?>&sailing_area=<?=$sailing_area?>&title=" + title + '&is_must=<?=$is_must;?>';


                    ajaxLoadEnd();
                    $('#dlg_add').dialog('close');
                } else {
                    ajaxLoadEnd();
                    $.messager.alert('Tips', res.msg);
                }
            }
        });
    }

    function add_table() {
        var rows = document.getElementById("table").rows.length;
        var str =
            "<tr id=\"tr_" + rows + "\">" +

            "<td>" +
            "<select name=\"box_type_" + rows + "\" id=\"box_type_add_" + rows + "\" class=\"easyui-combobox\" style=\"width:200px;\" data-options=\"editable:false,valueField:\'value\',textField:\'value\',url:\'/bsc_dict/get_option/container_size\'\">" +
            "</select>" +
            "</td>" +

            "<td>" +
            "<input name=\"unit_price_" + rows + "\" id=\"unit_price_add_" + rows + "\" class=\"easyui-numberbox\"  precision=\"2\" style=\"width:200px;\" >" +
            "</td>" +
            "<td>" +
            "<a href=\"javascript:void(0)\" class=\"easyui-linkbutton\" iconCls=\"icon-remove\" plain=\"true\" onclick=\"del_table()\">" +
            "</a>" +
            "</td>" +
            "</tr>";

        $("#table").append(str);

        $.parser.parse($('#tr_' + rows));
    }

    function del_table() {
        var table = document.getElementById("table");
        var len = table.rows.length;
        table.deleteRow(len - 1);
    }

    //判断是否有重复值
    function isRepeat(arr) {
        var hash = {};
        for (var i in arr) {
            if (hash[arr[i]]) {
                return true;
            }
            hash[arr[i]] = true;
        }
        return false;
    }

    //删除整行
    function del() {
        var row = $('#tt').datagrid('getSelected');
        if (row == null) {
            alert('请选中数据');
            return;
        }

        $.messager.confirm('确认删除', '是否删除整行数据', function (r) {
            if (r) {
                $.ajax({
                    type: "POST",
                    dataType: "json",//预期服务器返回的数据类型
                    url: '/biz_auto_rate/del_local_data',
                    data: {
                        'charge': row.charge,
                        'goods_type': 'GC',
                        'trans_origin': '<?= $trans_origin ?>',
                        'trans_carrier': '<?= $trans_carrier ?>',
                        'booking_agent': '<?= $booking_agent ?>',
                        'sailing_area': '<?= $sailing_area ?>',
                        'start_date': row.start_date,
                        'must': row.must,
                    },
                    success: function (res) {
                        alert(res.msg);
                        if (res.code == 0) {
                            //选择现在的页面
                            var tab = $('#tabs').tabs('getSelected');
                            var title = tab.panel('options').title;

                            window.location.href = "/biz_auto_rate/local_table?trans_origin=<?=$trans_origin?>&trans_carrier=<?=$trans_carrier?>&booking_agent=<?=$booking_agent?>&sailing_area=<?=$sailing_area?>&title=" + title;

                        }
                    },
                    error: function () {
                        alert('系统错误,请联系管理员');
                        return;
                    }
                });
            } else {
                return false;
            }
        });


    }



    //删除整行
    function del2() {
        var row = $('#tt2').datagrid('getSelected');
        if (row == null) {
            alert('请选中数据');
            return;
        }

        $.messager.confirm('确认删除', '是否删除整行数据', function (r) {
            if (r) {
                $.ajax({
                    type: "POST",
                    dataType: "json",//预期服务器返回的数据类型
                    url: '/biz_auto_rate/del_local_data',
                    data: {
                        'charge': row.charge,
                        'goods_type': 'DR',
                        'trans_origin': '<?= $trans_origin ?>',
                        'trans_carrier': '<?= $trans_carrier ?>',
                        'booking_agent': '<?= $booking_agent ?>',
                        'sailing_area': '<?= $sailing_area ?>',
                        'start_date': row.start_date,
                        'must': row.must,
                    },
                    success: function (res) {
                        alert(res.msg);
                        if (res.code == 0) {
                            //选择现在的页面
                            var tab = $('#tabs').tabs('getSelected');
                            var title = tab.panel('options').title;

                            window.location.href = "/biz_auto_rate/local_table?trans_origin=<?=$trans_origin?>&trans_carrier=<?=$trans_carrier?>&booking_agent=<?=$booking_agent?>&sailing_area=<?=$sailing_area?>&title=" + title;

                        }
                    },
                    error: function () {
                        alert('系统错误,请联系管理员');
                        return;
                    }
                });
            } else {
                return false;
            }
        });


    }

    function reload_client(){
        $('#client_code1').combobox({url:'/biz_client/get_option/0/1'});

    }


</script>