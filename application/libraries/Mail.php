<?php


use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class Mail
{
    public $mail;
    public static $SMTP_host = '';
    public static $SMTP_username = '';
    public static $SMTP_password = '';
    public static $SMTP_port = 465;//网易云 SSL 465 994 非SSL 25
    public static $is_ssl = true;//网易云 SSL 465 994 非SSL 25

    public static $subject = '';//邮件标题
    public static $body = '';//邮件正文
    public static $altBody = '';//邮件附加信息
    public static $sender = array();//发送人
    public static $receivers = array();//收件人
    public static $reply_by = array();//回复人
    public static $CCs = array();//抄送javascript:;
    public static $BCCs = array();//密送
    public static $attachments = array();//附件信息
    public static $isHTML = true;//是否HTML格式
    public $charset = 'UTF-8';//是否HTML格式

    public function __construct()
    {
        $this->mail = new PHPMailer(true);
    }

    public function send_mail(){
        // Instantiation and passing `true` enables exceptions
        try {
            //Server settings
            // $this->mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output  SMTP::DEBUG_SERVER
            $this->mail->isSMTP();                                            // Send using SMTP
            $this->mail->CharSet = $this->charset; //编码要指定
            $this->mail->Host       = self::$SMTP_host;                    // Set the SMTP server to send through
            $this->mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $this->mail->Username   = self::$SMTP_username;                     // SMTP username
            $this->mail->Password   = self::$SMTP_password;                               // SMTP password
//            $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $this->mail->Port       = self::$SMTP_port;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
            $this->mail->SMTPSecure = self::$is_ssl ? 'ssl' : 'tls';
//            $this->mail->SMTPSecure = 'ssl';

            //Recipients
            if(empty(self::$sender)) self::$sender = array('email' => self::$SMTP_username);
            $sender = self::$sender;
            foreach ($sender as $key => $val){
                $sender[$key] = convert_encoding($sender[$key], 'UTF-8', $this->charset);
            }
            if(isset($sender['name']) && $sender['name'] == ''){
                $this->mail->setFrom($sender['email'], $sender['name']);//发件人
            }else{
                $this->mail->setFrom($sender['email']);
            }
            $receivers = self::$receivers;
            if($receivers !== array() && count($receivers) == count($receivers, 1))$receivers = array($receivers);
            foreach ($receivers as $key => $receiver){
                foreach ($receiver as $k => $v){
                    $receiver[$k] = convert_encoding($receiver[$k], 'UTF-8', $this->charset);
                }
                if(isset($receiver['name']) && $receiver['name'] == ''){
                    $this->mail->addAddress($receiver['email'], $receiver['name']);//收件人
                }else{
                    $this->mail->addAddress($receiver['email']);
                }
            }
            $reply_by = self::$reply_by !== array() ? self::$reply_by : self::$sender;
            foreach ($reply_by as $key => $val){
                $reply_by[$key] = convert_encoding($reply_by[$key], 'UTF-8', $this->charset);
            }
            if(isset($reply_by['name']) && $reply_by['name'] == ''){
                $this->mail->addReplyTo($reply_by['email'], $reply_by['name']);
            }else{
                $this->mail->addReplyTo($reply_by['email']);
            }


            $CCs = self::$CCs;
            if($CCs !== array() && count($CCs) == count($CCs, 1))$CCs = array($CCs);
            foreach ($CCs as $CC){
                foreach ($CC as $k => $v){
                    $CC[$k] = convert_encoding($CC[$k], 'UTF-8', $this->charset);
                }
                if(isset($CC['name']) && $CC['name'] !== ''){
                    $this->mail->addCC($CC['email'], $CC['name']);
                }else{
                    $this->mail->addCC($CC['email']);
                }
            }
            $BCCs = self::$BCCs;
            if($BCCs !== array() && count($BCCs) == count($BCCs, 1))$BCCs = array($BCCs);
            foreach ($BCCs as $BCC){
                foreach ($BCC as $k => $v){
                    $BCC[$k] = convert_encoding($BCC[$k], 'UTF-8', $this->charset);
                }
                if(isset($BCC['name']) && $BCC['name'] !== '') {
                    $this->mail->addBCC($BCC['email'], $BCC['name']);
                }else{
                    $this->mail->addBCC($BCC['email']);
                }
            }

            // Attachments
            $attachments = self::$attachments;
            if($attachments !== array() && count($attachments) == count($attachments, 1))$attachments = array($attachments);
            foreach ($attachments as $key => $attachment){
                foreach ($attachment as $k => $v){
                    $attachment[$k] = convert_encoding($attachment[$k], 'UTF-8', $this->charset);
                }
                if(isset($attachment['name']) && $attachment['name'] !== ''){
                    is_file($attachment['path']) && $this->mail->addAttachment($attachment['path'], $attachment['name']);         // 附件
                }else{
                    is_file($attachment['path']) && $this->mail->addAttachment($attachment['path']);         // 附件
                }
            }

            // Content
            $this->mail->isHTML(self::$isHTML);                                  // Set email format to HTML
            $this->mail->Subject = self::$subject;//标题
            $this->mail->Body    = self::$body;//内容
            $this->mail->AltBody = self::$altBody;//附加信息，可以省略

            $this->mail->Subject = convert_encoding($this->mail->Subject, 'UTF-8', $this->charset);
            $this->mail->Body    = convert_encoding($this->mail->Body, 'UTF-8', $this->charset);
            $this->mail->AltBody = convert_encoding($this->mail->AltBody, 'UTF-8', $this->charset);

            $this->mail->send();
            return 'Message has been sent';
        } catch (Exception $e) {
            return "Message could not be sent. Mailer Error: {$this->mail->ErrorInfo}";
        }
    }

    public function testSmtpConnect($time_out = 5){
        $this->mail->isSMTP();                                            // Send using SMTP
        $this->mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $this->mail->Host       = self::$SMTP_host;                    // Set the SMTP server to send through
        $this->mail->Username   = self::$SMTP_username;                     // SMTP username
        $this->mail->Password   = self::$SMTP_password;                               // SMTP password
        $this->mail->Port       = self::$SMTP_port;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
        $this->mail->SMTPSecure = self::$is_ssl ? 'ssl' : 'tls';
        $this->mail->Timeout = $time_out;
//        var_dump($this->mail->Timeout);
//        return;
        if($smtpConnect = $this->mail->smtpConnect()){
            $this->mail->smtpClose();
        }
        return $smtpConnect;
    }
}