<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title><?php echo $job_no;?>-shipment-Edit</title>

<script type="text/javascript">
    var is_special_char = '';

    function qzb(str){
        str = str.replace(new RegExp('·', "g"), '`');
        str = str.replace(new RegExp('！', "g"), '!');
        str = str.replace(new RegExp('￥', "g"), '$');
        str = str.replace(new RegExp('【', "g"), '[');
        str = str.replace(new RegExp('】', "g"), ']');
        str = str.replace(new RegExp('】', "g"), ']');
        str = str.replace(new RegExp('；', "g"), ';');
        str = str.replace(new RegExp('：', "g"), ':');
        str = str.replace(new RegExp('“', "g"), '"');
        str = str.replace(new RegExp('”', "g"), '"');
        str = str.replace(new RegExp('’', "g"), '\'');
        str = str.replace(new RegExp('‘', "g"), '\'');
        str = str.replace(new RegExp('、', "g"), '');
        str = str.replace(new RegExp('？', "g"), '?');
        str = str.replace(new RegExp('《', "g"), '<');
        str = str.replace(new RegExp('》', "g"), '>');
        str = str.replace(new RegExp('，', "g"), ',');
        str = str.replace(new RegExp('。', "g"), '.');
        str = str.replace(new RegExp(' ', "g"), ' ');
        str = str.replace(new RegExp('　', "g"), ' ');
        str = str.replace(new RegExp('（', "g"), '(');
        str = str.replace(new RegExp('）', "g"), ')');
        return str;
    }

    /**
     * 下拉选择表格展开时,自动让表单内的查询获取焦点
     */
    function combogrid_search_focus(form_id, focus_num = 0){
        // var form = $(form_id);
        setTimeout("easyui_focus($($('" + form_id + "').find('.keydown_search')[" + focus_num + "]));",200);
    }

    /**
     * easyui 获取焦点
     */
    function easyui_focus(jq){
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combogrid !== undefined){
            return $(jq).combogrid('textbox').focus();
        }else if(data.combobox !== undefined){
            return $(jq).combobox('textbox').focus();
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('textbox').focus();
        }else if(data.datebox !== undefined){
            return $(jq).datebox('textbox').focus();
        }else if(data.textbox !== undefined){
            return $(jq).textbox('textbox').focus();
        }
    }

    /**
     * 自动转大写的输入事件
     */
    function toUpperCaseChange(newValue, oldValue){
        $(this).textbox('setValue', newValue.toUpperCase());
    }

    var is_update = false;

    window.onbeforeunload = function (e) {
        // 兼容IE8和Firefox 4之前的版本
        //检测是否有字段变动
        e = e || window.event;
        var dialogText = '您有未保存的内容,是否确认退出';
        if(is_update){
            if (e) {
                e.returnValue = dialogText;
            }
            // Chrome, Safari, Firefox 4+, Opera 12+ , IE 9+
            return dialogText;
        }
    };

    function checkName(val, type = 0){
        // var reg = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");\
        if(type == 0){
            var reg = new RegExp(/[^\x00-\x7f]/g);
        }else{
            var reg = new RegExp(/[^\x00-\x7f|\u4e00-\u9fa5]/g);
        }
        var rs = "";
        for (var i = 0, l = val.length; i < val.length; i++) {
            if(val.substr(i, 1).match(reg)) rs += val.substr(i, 1);
        }
        return rs;
    }

    $.fn.tabs.methods.hideTab = function(jq,which) {
        return jq.each(function () {
            var tab = $(this).tabs("getTab", which);
            if(tab == null){
                return false;
            }
            var opts = tab.panel("options");
            opts.tab.css("display", 'none');
            opts.disabled = true;
        });
    };

    $.fn.tabs.methods.showTab = function(jq,which) {
        return jq.each(function () {
            var tab = $(this).tabs("getTab", which);
            if(tab == null){
                return false;
            }
            var opts = tab.panel("options");
            opts.tab.css("display", "");
            opts.disabled = false;
        });
    };

    function create_client_tool(div_id, id){
        var html = '<div id="' + div_id + '_div">' +
            '<form id="' + div_id + '_form" method="post">' +
            '<div style="padding-left: 5px;display: inline-block;">' +
            '<label><?= lang('client_code');?>:</label><input name="client_code" class="easyui-textbox keydown_search" t_id="' + id + '" style="width:96px;"/>' +
            '<label><?= lang('company_name');?>:</label><input name="company_name" class="easyui-textbox keydown_search" t_id="' + id + '" style="width:96px;"/>' +
            '</div>' +
            '<form>' +
            '<div style="padding-left: 1px; display: inline-block;">' +
            '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="' + id + '_click" onclick="query_report(\'' + div_id + '\', \'' + id + '\')"><?= lang('search');?></a>' +
            '</div>' +
            '</div>';
        $('body').append(html);
        $.parser.parse('#' + div_id + '_div');
        return div_id;
    }

    function create_company_tool(div_id, id){
        var html = '<div id="' + div_id + '_div">' +
            '<form id="' + div_id + '_form" method="post">' +
            '<div style="padding-left: 5px;display: inline-block;">' +
            '<label><?= lang('company_name');?>:</label><input name="company_name" class="easyui-textbox keydown_search" t_id="' + id + '" style="width:96px;"/>' +
            '</div>' +
            '<form>' +
            '<div style="padding-left: 1px; display: inline-block;">' +
            '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="' + id + '_click" onclick="query_report(\'' + div_id + '\', \'' + id + '\')"><?= lang('search');?></a>' +
            '</div>' +
            '</div>';
        $('body').append(html);
        $.parser.parse('#' + div_id + '_div');
        return div_id;
    }

    function query_report(div_id, load_id){
        var where = {};
        var form_data = $('#' + div_id + '_form').serializeArray();
        $.each(form_data, function (index, item) {
            if(item.value == "") return true;
            if(where.hasOwnProperty(item.name) === true){
                if(typeof where[item.name] == 'string'){
                    where[item.name] =  where[item.name].split(',');
                    where[item.name].push(item.value);
                }else{
                    where[item.name].push(item.value);
                }
            }else{
                where[item.name] = item.value;
            }
        });
        if(where.length == 0){
            $.messager.alert('Tips', '请填写需要搜索的值');
            return;
        }
        $('#' + load_id).combogrid('grid').datagrid('load',where);
    }

    //text添加输入值改变
    function keydown_search(e, click_id){
        if(e.keyCode == 13){
            $('#' + click_id + '_click').trigger('click');
        }
    }

    function combogrid_onBeforeLoad(param, id){
        if($('#' + id).combogrid('getValue') != '' && Object.keys(param).length == 0)param.q = $('#' + id).combogrid('getValue');
        if($('#' + id).combogrid('getValue') == '' && Object.keys(param).length == 0) return false;
        else return true;
    }

    var is_load_user = 0;

    $(function(){
    	<?php if($from_db != '' && $from_id_no != 0):?>
		$('body').css('background-color', '#F0FFF0');
		<?php endif;?>
		$('.basic_show').show();
		$('.basic_show').siblings('.accordion-header').css('background','#ffe48d');
        $('body').on('click', '.download_mb', function(r){
            var url = $(this).prop('href');
            if(url.indexOf('/bsc_template/set_parameter') != -1){
                $('#window').window({
                    title:'window',
                    width:500,
                    height:200,
                    href:url,
                    modal:true
                });
                return false;
            }else{
                return true;
            }
        });

        $('#service_option_tab').accordion('select', 'Consol 区域');
        $('#service_option_tab').accordion('select', 'Shipment Tags');
        $('#service_option_tab').accordion('select', 'Client Tags');
        $.each($('.keydown_search'), function (i,it) {
            $(it).textbox('textbox').bind('keydown', function(e){keydown_search(e, $(it).attr('t_id'));});
        });

        $('#tb').tabs({

            border:false,
            onSelect:function(title){
                var body_color = $('body').css('background-color');
                if (title == "Billing"){
                    if(body_color == 'rgb(255, 0, 0)'){
                        $.messager.alert('Tips', '有红色背景报警存在，请先解决');
                        return false;
                    }
                    if (document.getElementById("billing_sub").src==""){
                        document.getElementById("billing_sub").src="/biz_bill/index?id_type=shipment&id_no=<?php echo $id;?>";
                    }
                }
                if (title == "Container"){
                    if (document.getElementById("container_sub").src==""){
                        document.getElementById("container_sub").src="/biz_containershipment/index/<?php echo $id;?>/<?php echo $consol_id;?>?operator_si_check=<?=$operator_si_check?>";
                    }
                }
                if (title == "Download"){
                    if(body_color == 'rgb(255, 0, 0)'){
                        $.messager.alert('Tips', '有红色背景报警存在，请先解决');
                        return false;
                    }
                    if (document.getElementById("Download_sub").src==""){
                        //document.getElementById("Download_sub").src="/api/phpword/list.php?data_table=shipment&id=<?php //echo $id;?>//";
                        document.getElementById("Download_sub").src="/bsc_template/download?data_table=shipment&id=<?php echo $id;?>";
                    }
                }
                /**
                 * 2022-04-18 这里由于提单签发后续会严禁,所以新开一个tab出来使用
                 */
                if (title == "B/L"){
                    if (document.getElementById("b_l").src==""){
                        document.getElementById("b_l").src="/bsc_template/download_b_l?data_table=shipment&id=<?php echo $id;?>";
                    }
                }
                if (title == "Upload files"){
                    if (document.getElementById("upload_files_sub").src==""){
                        document.getElementById("upload_files_sub").src="/bsc_upload/upload_files?biz_table=biz_shipment&id_no=<?php echo $id;?>";
                    }
                }
                if (title == "Truck"){
                    if (document.getElementById("truck_sub").src==""){
                        document.getElementById("truck_sub").src="/biz_shipment_truck/index/<?php echo $id;?>";
                    }
                }
                if (title == "Tracking"){
                    if (document.getElementById("tracking_sub").src==""){
                        document.getElementById("tracking_sub").src="/biz_tracking/index?id_type=biz_shipment&id_no=<?php echo $id;?>";
                    }
                }
                if (title == "Feeder"){
                    if (document.getElementById("feeder_sub").src==""){
                        document.getElementById("feeder_sub").src="/biz_shipment_truck/index_feeder/<?php echo $id;?>";
                    }
                }
                if (title == "EDI"){
                    if (document.getElementById("edi").src==""){
                        document.getElementById("edi").src = "/bsc_message/edi?id_type=shipment&id_no=<?= $id;?>";
                    }
                }
                if(title =='Sub shipment'){
                    if (document.getElementById("sub_shipment").src==""){
                        document.getElementById("sub_shipment").src="/biz_shipment/sub_shipment/<?php echo $id;?>";
                    }
                }
                if(title == 'Log'){
                    if (document.getElementById("log").src==""){
                        document.getElementById("log").src="/sys_log/index?table_name=biz_shipment&key_no=<?= $id?>";
                    }
                }
                if(title == 'Emails'){
                    if (document.getElementById("emails").src==""){
                        document.getElementById("emails").src="/mail_box/template_get_mail?mail_type=shipment&id=<?= $id?>";
                    }
                }
            }

        });

        //shipment错误提示--start
        <?php if(!empty($shipment_check['shipment_msg'])){?>
        $.messager.show({
            title:'<?= lang('warning');?>',
            showType:'slide',
            width: '300px',
            height: '200px',
            timeout: 0,
            msg: '<?= $shipment_check['shipment_msg'];?>',
            style:{
                top: (document.documentElement.scrollTop + (document.documentElement.clientHeight - 300) / 2),
                left: (document.documentElement.scrollLeft + (document.documentElement.clientWidth - 200) / 2),
            }
        });
        $('body').css('background-color', 'red');
        <?php } ?>

        $('.status').change(function () {
            var val = 0;
            var inp = $(this);
            var checked = $(this).prop('checked');
            if(checked) {
                var day1 = new Date();
                var month = '';
                if (day1.getMonth() + 1 < 10) {
                    month = '0' + (day1.getMonth() + 1);
                } else {
                    month = day1.getMonth() + 1;
                }
                var val = day1.getFullYear() + "-" + month + "-" + day1.getDate() + ' ' + day1.getHours() + ':' + day1.getMinutes() + ':' + day1.getSeconds();

            }

            var field = $(this).attr('id');
            var data = {};
            data[field] = val;

            //如果勾的预报的话,提前执行一个 shipment_check_before 提示下
            var submit_type = 0;
            var submit_msg = ''; // 这个目前只有1才会用到
            if(field === 'pre_alert'){
                $.ajax({
                    url: '/biz_shipment/shipment_check_before?id=<?= $id;?>&check_type=pre_alert',
                    type: 'POST',
                    async : false,// 必须使用同步请求
                    dataType:'json',
                    success: function (res) {
                        if(res.code == 0){
                            submit_type = 1;
                            //如果只有一个shipment那么直接提交就行了
                            if(res.data.shipment_count == 1){
                                submit_type = 0;
                            }
                            submit_msg = res.msg;
                        }else{
                            submit_type = -1;
                        }
                    },error:function () {
                        $.messager.alert('Tips', '发生错误');
                    }
                });
            }
            function update_checkbox(data){
                $.ajax({
                    url: '/biz_shipment/update_checkbox/<?php echo $id;?>',
                    type: 'POST',
                    async : false,// 必须使用同步请求
                    data:data,
                    dataType:'json',
                    success: function (res) {
                        if(res.code == 1){
                            $.messager.alert('Tips', res.msg);
                            inp.prop('checked', !checked)
                        }else if(res.code == 2){
                            $.messager.alert('Tips', res.msg);
                            inp.prop('checked', !checked)
                        }else if(res.code == 0){
                            // $.messager.alert('Tips', res.msg); 
                        }else if(res.code == 444){
                            // GET方式示例，其它方式修改相关参数即可
                            var form = $('<form></form>').attr('action','/other/ts_text').attr('method','POST').attr('target', '_blank');
                            var json_data = {};
                            json_data['text'] = res.text;
                            json_data['ts_str[]'] = res.ts_str;
                            json_data['msg'] = res.msg;
                            $.each(json_data, function (index, item) {
                                if(typeof item === 'string'){
                                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                                }else{
                                    $.each(item, function(index2, item2){
                                        form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                                    })
                                }
                            });

                            form.appendTo('body').submit().remove();
                        }
                    },error:function () {
                        $.messager.alert('Tips', '发生错误');
                    }
                });
            }

            //0 是正常的直接提交, 1 是确认窗口提交,-1是直接拒绝掉
            if(submit_type === 0){
                update_checkbox(data);
            }else if(submit_type === 1){
                $.messager.confirm('<?= lang('提示');?>', submit_msg, function (r) {
                    if(r){
                        update_checkbox(data);
                    }else{
                        inp.prop('checked', !checked)
                    }
                })
            }else{
                $.messager.alert('Tips', '<?= lang('无法勾选,未满足条件');?>');
                inp.prop('checked', !checked)
            }

        });

        //实现表格下拉框的分页和筛选--start
        var html = '<div id="consol_id_applyTb">';
        html += '<form id="consol_id_applyTbform">';
        html += '<div style="padding-left: 5px;display: inline-block;">' +
            '<label><?= lang('CONSOL');?>:</label><input name="fields[job_no]" class="easyui-textbox to_search"style="width:96px;"data-options="prompt:\'text\'"/>' +
            '<label><?= lang('trans_carrier');?>:</label><input name="fields[trans_carrier]" class="easyui-textbox to_search"style="width:96px;"data-options="prompt:\'text\'"/>' +
            '<label><?= lang('booking_ETD');?>:</label><input name="fields[booking_ETD_start]" class="easyui-datebox to_search"style="width:96px;"data-options="prompt:\'text\'"/>-<input name="fields[booking_ETD_end]" class="easyui-datebox to_search"style="width:96px;"data-options="prompt:\'text\'"/>' +
            '</div>';
        html += '<div style="padding-left: 1px; display: inline-block;"><form>';
        html += '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="queryReport"><?= lang('search');?></a>';
        html += '</div>';
        html += '</div>';
        $('body').append(html);
        $.parser.parse('#consol_id_applyTb');

        $('#consol_id_apply').combogrid("reset");
        $('#consol_id_apply').combogrid({
            panelWidth: '1000px',
            panelHeight: '300px',
            prompt: 'PENDING CONSOL',
            // multiple:true,
            idField: 'id',              //ID字段
            textField: 'job_no',    //显示的字段
            fitColumns : true,
            striped: true,
            editable: false,
            pagination: true,           //是否分页
            pageList : [30,50,100],
            pageSize : 30,
            toolbar : '#consol_id_applyTb',
            rownumbers: true,           //序号
            collapsible: true,         //是否可折叠的
            method: 'post',
            columns:[[
                {field:'job_no',title:'<?= lang('job_no');?>',width:120},
                {field:'trans_carrier_name',title:'<?= lang('trans_carrier');?>',width:120},
                {field:'booking_ETD',title:'<?= lang('booking_ETD');?>',width:90},
                {field:'box_info',title:'<?= lang('box_info');?>',width:90},
            ]],
            emptyMsg : '未找到相应数据!',
            onLoadSuccess : function(data) {
                var opts = $(this).combogrid('grid').datagrid('options');
                var vc = $(this).combogrid('grid').datagrid('getPanel').children('div.datagrid-view');
                vc.children('div.datagrid-empty').remove();
                if (!$(this).combogrid('grid').datagrid('getRows').length) {
                    var d = $('<div class="datagrid-empty"></div>').html(opts.emptyMsg || 'no records').appendTo(vc);
                    d.css({
                        position : 'absolute',
                        left : 0,
                        top : 50,
                        width : '100%',
                        fontSize : '14px',
                        textAlign : 'center'
                    });
                }
            },
            url:'/biz_consol/get_error_consol/<?= $id;?>',
        });
        //点击搜索
        $('#queryReport').click(function(){
            var where = {};
            var form_data = $('#consol_id_applyTbform').serializeArray();
            $.each(form_data, function (index, item) {
                if(where.hasOwnProperty(item.name) === true){
                    if(typeof where[item.name] == 'string'){
                        where[item.name] =  where[item.name].split(',');
                        where[item.name].push(item.value);
                    }else{
                        where[item.name].push(item.value);
                    }
                }else{
                    where[item.name] = item.value;
                }
            });
            // where['field'] = $('#to_search_field').combo('getValue');
            $('#consol_id_apply').combogrid('grid').datagrid('load',where);
        });
        //text添加输入值改变
        $('.to_search').textbox('textbox').keydown(function (e) {
            if(e.keyCode == 13){
                $('#queryReport').trigger('click');
            }
        });
        //实现表格下拉框的分页和筛选-----end

        //锁了的灰色
        <?php if($lock_lv > 0 ){?>
        $('body').css('background-color', 'gray');
        <?php } ?>

        var a1 = service_option();
        //服务选项事件
        //service_options 选择哪个隐藏哪个
        $('#service_options_div').on('click', '.service_options', function () {
            service_options_check(this);
        });

        $.when(a1).done(function () {
            $.each($('.service_options'), function (index, item) {
                service_options_check(item);
            });
        });
        var warehouse_tab = $("#warehouse_tab").find(".panel-tool");
        $(warehouse_tab[0]).prepend('<a href="javascript:void(0)" class="icon-save" onclick="<?php if(!$r_e) echo "$(\'#f\').form(\'submit\');";?>" title="save" style="margin-right:15px;"></a>');
        var service_option_tab = $("#service_option_tab").find(".panel-tool");
        $(service_option_tab[0]).prepend('<a href="javascript:void(0)" class="icon-save" onclick="<?php if(!$r_e) echo "$(\'#f\').form(\'submit\');";?>" title="save" style="margin-right:15px;"></a>');

        //初始JS
        dangerous_info($('#goods_type').combobox('getValue'));
        INCO_term_info($('#INCO_term').combobox('getValue'));

        $('#f').form({
            url: '/biz_shipment/update_data_ajax/<?php echo $id;?>',
            onChange: function (target) {
                is_update = true;
            },
            onSubmit: function(){
                is_update = false;
                var vv = $('#trans_origin').textbox('getValue');
                if (vv == "") {
                    alert("<?= lang('{field}必填', array('field' => lang('起运港代码')));?>！");
                    return false;
                }
                var vv = $('#biz_type1').combobox('getValue');
                if (vv == " " || vv == "") {
                    alert("<?= lang('{field}必填', array('field' => lang('业务类型')));?>！");
                    return false;
                }
                $.each($('textarea'), function(index, item){
                    var str = $(this).val();
                    var id = $(this).prop('id');
                    var ids = {
                        shipper_address: ['shipper_address', '<?= lang('发货人地址');?>'],
                        consignee_address: ['consignee_address', '<?= lang('收货人地址');?>'],
                        notify_address: ['notify_address', '<?= lang('通知人地址');?>'],
                        description: ['description', '<?= lang('英文货物描述');?>'],
                        mark_nums: ['mark_nums', '<?= lang('货物唛头');?>'],
                    };
                    str = qzb(str);
                    $(this).val(str);
                    var newstr = $(this).val();
                    if(ids[id] !== undefined){
                        var special_char = checkName(newstr);
                    }else{
                        //2023-03-09 由于会输入阿拉伯文等,除了以上地址, 全部取消限制
                        // var special_char = checkName(newstr, 1);
                        var special_char = '';
                    }
                    if(special_char !== ''){
                        console.log(id);
                        $.messager.alert('error', ids[id][1] + '检测到有以下特殊字符:<br/>' + special_char);
                        is_special_char = true;
                        return false;
                    }else{
                        is_special_char = false;
                        $(this).val($(this).val().toUpperCase());
                    }
                });
                if(is_special_char){
                    // alert(is_special_char + '存在特殊字符');
                    return false;
                }
                $('#f').append($('#service_option_tab').clone().css('display', 'none'));
                $('#f').append($('#warehouse_tab').clone().css('display', 'none'));
                var biz_type = $('#biz_type').val();
                if(biz_type == 'export'){
                    $('#f').append($('#POD_service_tab').clone().css('display', 'none'));
                }else{
                    $('#f').append($('#POL_service_tab').clone().css('display', 'none'));
                }
                $('.icon-save').attr('onclick', '');
                return true;
            },
            success:function(res_json){
                $('#f #service_option_tab').remove();
                $('#f #warehouse_tab').remove();
                $('#f #POD_service_tab').remove();
                $('#f #POL_service_tab').remove();
                try{
                    var res = $.parseJSON(res_json);

                    if(res.code !== 444)alert(res.msg);
                    $('#hs_code_tip').html('');
                    $('.icon-save').attr('onclick', "$('#f').form('submit');");
                    if(res.code == 0){
                        try{opener.$('#tt').edatagrid('reload');}catch (e) {}
                        location.href = '/biz_shipment/edit/' + res.data.id;
                    }else if(res.code == 2){//代码tsl hscode错误
                        $('#hs_code_tip').html('<a href="/bsc_dict/read/hs_code/TSL" target="_blank" style="color:red;">hs_code</a>');
                    }else if(res.code == 444){
                        // GET方式示例，其它方式修改相关参数即可
                        var form = $('<form></form>').attr('action','/other/ts_text').attr('method','POST').attr('target', '_blank');
                        var json_data = {};
                        json_data['text'] = res.text;
                        json_data['ts_str[]'] = res.ts_str;
                        json_data['msg'] = res.msg;
                        $.each(json_data, function (index, item) {
                            if(typeof item === 'string'){
                                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                            }else{
                                $.each(item, function(index2, item2){
                                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                                })
                            }
                        });

                        form.appendTo('body').submit().remove();
                    }
                }catch (e) {
                    alert(res_json);
                    $('.icon-save').attr('onclick', "$('#f').form('submit');");
                }
            }
        });

        var now = Date.parse(new Date()) / 1000;

        //订舱船期范围设置
        $('#booking_ETD').datebox('calendar').calendar({
            //到时候不存在可选择值的，在这里false
            validator: function(date){
                //日期不能超过前后半年
                var star = now - 3600 * 24 * 183;
                var end = now + 3600 * 24 * 183;
                var date_time = Date.parse(date) / 1000;
                if (date_time >= star && date_time <= end) {
                    return true;
                } else {
                    return false;
                }
            }
        });

        //2022-06-28 获取选中tab tb
        <?php if(!empty($tab_title)):?>
        $("#tb").tabs('select','<?=$tab_title;?>');
        <?php endif;?>
    });

    function user_info(this_id){
        var val = $('#' + this_id).combobox('getValue');
        window.open('/bsc_user/person_info/' + val);
    }

    var is_submit = false;
    function tb_duty_user(role){
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $.ajax({
            url: '/biz_shipment/tb_duty_user?role=' + role + '&id=<?= $id;?>',
            type: 'GET',
            success: function (res_json) {
                var res;
                try{
                    res = $.parseJSON(res_json);
                }catch(e){

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误,请联系管理员');
                    return;
                }
                is_submit = false;
                ajaxLoadEnd();
                $.messager.alert('Tips', res.msg);
                if(res.code == 0){

                }else{
                }
            },
            error:function(){
                is_submit = false;
                ajaxLoadEnd();
                $.messager.alert('Tips', '发生错误');
            }
        });
    }

    function service_option() {
        return $.ajax({
            url: '/bsc_dict/get_option/service_option',
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                var str = '<table style="float: left;">';
                var service_options_arr = [];
                try {
                    service_options_arr = $.parseJSON($('#service_options').val());
                } catch(e) {
                }
                var service_options = [];
                $.each(service_options_arr, function(i, it){
                    service_options.push(it[0]);
                });
                str += '<tr style="text-align: center"><th>POL</th></tr>';
                $.each(res, function (index, item) {
                    if(item.orderby <= 100){
                        str += '<tr>';
                        var checked = '';
                        if(item.name !== '' && item.value !== ''){
                            if($.inArray(item.value, service_options) !== -1){
                                checked = 'checked';
                            }
                            str += '<td><input <?php echo in_array("service_options",$G_userBscEdit) ? '' : 'disabled'; ?> class="service_options" type="checkbox" name="service_options[' + index + '][]" value="' + item.value + '" style="vertical-align:middle;" ' + checked + '>' + item.lang + '</td>';
                        }
                        str += '</tr>';
                    }
                });
                str += '</table>';
                str += '<table style="float: left;">';
                str += '<tr style="text-align: center"><th>POD</th></tr>';
                $.each(res, function (index, item) {
                    if(item.orderby > 100){
                        str += '<tr>';
                        var checked = '';
                        if(item.name !== '' && item.value !== ''){
                            if($.inArray(item.value, service_options) !== -1){
                                checked = 'checked';
                            }
                            str += '<td><input class="service_options" <?php echo in_array("service_options",$G_userBscEdit) ? '' : 'disabled'; ?> type="checkbox" name="service_options[' + index + '][]" value="' + item.value + '" style="vertical-align:middle;" ' + checked + '>' + item.lang + '</td>';
                        }
                        str += '</tr>';
                    }
                });
                str += '</table>';
                $('#service_options_div').append(str);

            }
        });
    }

    function service_options_check(e){
        //报关服务 custom
        var this_check = $(e)[0].checked;
        var val = $(e).val();
        var method;
        if(this_check){
            method = 'showTab';
        }else{
            method = 'hideTab';
        }
        if(val == 'custom_declaration'){
            $('#tb').tabs(method, 'custom');
        }
        //陆运 truck
        if(val == 'trucking'){
            $('#tb').tabs(method, 'Truck');
        }
        //内装 warehouse
        if(val == 'warehouse'){
            $('#tb').tabs(method, 'Warehouse');
        }
        //订舱 TODO
        if(val == 'booking'){

        }
        if(val == 'feeder'){
            $('#tb').tabs(method, 'Feeder');
        }
        if(val == 'destination_custom'){
            $('#tb').tabs(method, 'Destination Custom');
        }
        if(val == 'destination_trucking'){
            $('#tb').tabs(method, 'Destination Trucking');
        }
        if(val == 'agent_handle'){
            $('#tb').tabs(method, 'Agent Handle');
        }
        var destination_trucking_check = $(".service_options[value='destination_trucking']").prop('checked');
        var trucking_check = $(".service_options[value='trucking']").prop('checked');
        var biz_type = $('#biz_type').val();
        //出口pod truck 触发 目的港送货必填
        if(destination_trucking_check && biz_type == 'export'){
            $('#tb').tabs('showTab', 'POD Service');
        }else{
            $('#tb').tabs('hideTab', 'POD Service');
        }

        // 进口pol truck 触发 目的港提货必填
        if(trucking_check && biz_type == 'import'){
            $('#tb').tabs('showTab', 'POL Service');
        }else{
            $('#tb').tabs('hideTab', 'POL Service');
        }
    }

    function add_consol(){
        location.href="/biz_consol/add/?shipment_id=<?php echo $id;?>";
    }

    function cancel_apply() {
        $.messager.confirm('Tips','是否确认撤销申请？',function(r){
            if (r){
                $.ajax({
                    type:'POST',
                    url:'/biz_shipment/cancel_apply',
                    data:{
                        id:<?= $id; ?>,
                    },
                    success:function (res_json) {
                        var res;
                        try{
                            res = $.parseJSON(res_json);
                        }catch (e) {

                        }
                        if(res == undefined){
                            $.messager.alert('Tips', res_json);
                            return
                        }
                        if(res.code == 0){
                            $.messager.alert('Tips', res.msg, 'info', function () {
                                location.reload();
                            });
                        }else{
                            $.messager.alert('Tips', res.msg);
                        }
                    }
                });
            }
        });
    }


    //预绑定CONSOL
    function apply_consol() {
        $('#apply_consol_div').window('open');
    }

    var form_submit = false;
    function save_apply_consol() {
        if(form_submit){
            return;
        }
        ajaxLoading();
        form_submit = true;
        $('#apply_consol_form').form('submit', {
            url: '/biz_shipment/update_data_table',
            onSubmit: function () {
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                    form_submit = false;
                }
                return validate;
            },
            success: function (res_json) {
                var res;
                try{
                    res = $.parseJSON(res_json);
                }catch(e){

                }
                form_submit = false;
                ajaxLoadEnd();
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误,请联系管理员!');
                    return;
                }
                $.messager.alert('Tips', res.msg);
                if(res.code == 0){
                    $('#apply_time_div').window('close');
                    location.reload();

                }
            }
        });
    }

    function add_box() {
        var length = $('.add_box').length;
        var lang = '';
        var btn = '';
        if(length <= 0){
            lang = "<?= lang('box_info');?>";
            btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
        }else{
            btn = '<a class="easyui-linkbutton" onclick="del_box(this)" iconCls="icon-remove"></a>';
        }
        var str = '<tr class="add_box" >\n' +
            '                            <td>' + lang + '</td>\n'+
            '                            <td>\n'+
            '                                <select <?php echo in_array("box_info",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-combobox box_info_size" name="box_info[size][]" data-options="\n'+
            '                                valueField:\'value\',\n'+
            '                                textField:\'value\',\n'+
            '                                url:\'/bsc_dict/get_option/container_size\',\n'+
            "                                onHidePanel: function() {  \n" +
            "                                   var valueField = $(this).combobox('options').valueField;  \n" +
            "                                   var val = $(this).combobox('getValue');  \n" +
            "                                   var allData = $(this).combobox('getData');  \n" +
            "                                   var result = true;  \n" +
            "                                   for (var i = 0; i < allData.length; i++) {  \n" +
            "                                       if (val == allData[i][valueField]) {  \n" +
            "                                           result = false;  \n" +
            "                                       }  \n" +
            "                                   }  \n" +
            "                                   if (result) {  \n" +
            "                                       $(this).combobox('clear');  \n" +
            "                                   } " +
            "                                }" +
            '                                "></select>\n'+
            '                                X\n'+
            '                                <input <?php echo in_array("box_info",$G_userBscEdit) ? '' : 'disabled'; ?> required class="easyui-numberbox box_info_num" name="box_info[num][]">\n'+
            '                                ' + btn + '\n'+
            '                            </td>\n'+
            '                        </tr>';
        if(length == 0){
            $('#trans_mode').parents('tr').after(str);
        }else{
            $('.add_box:eq(' + (length - 1) + ')').after(str);
        }
        $.parser.parse('.add_box:eq(' + length + ')');
    }

    function del_box(e) {
        if(e == undefined){
            $('.add_box').remove();
        }else{
            $(e).parents('tr').remove();
        }
    }

    function trans_tool_select(rec){
        if(rec.value == 'AIR'){
            //当运输工具是AIR，以下格子的名称做对应的修改：
            //2021-06-04 16:00 汪庭彬
            $('#trans_carrier').parent('td').siblings('td').text('Air Line');
            $('#cus_no').parent('td').siblings('td').text('Sub BL No');
            $('#booking_ETD').parent('td').siblings('td').text('booking_ETD');
        }else{
            $('#trans_carrier').parent('td').siblings('td').text('<?= lang('trans_carrier');?>');
            $('#cus_no').parent('td').siblings('td').text('<?= lang('cus_no');?>');
            $('#booking_ETD').parent('td').siblings('td').text('<?= lang('booking_ETD');?>');
        }
    }

    function trans_carrier_select(rec){
        var trans_mode = $('#trans_mode').val();
        var trans_tool = $('#trans_tool').combobox('getValue');

        //2022-07-14 新增原先的值
        var old1 = $('#trans_origin').combogrid('getValue'); // 获取数据表格对象
        var old2 = $('#trans_discharge').combogrid('getValue'); // 获取数据表格对象
        var old3 = $('#trans_destination').combogrid('getValue'); // 获取数据表格对象


        $('#trans_origin').combogrid('grid').datagrid('load', "/biz_port/get_option?type=" + trans_tool + "&carrier=" + rec.client_code + '&limit=true'+'&old='+old1);
        $('#trans_discharge').combogrid('grid').datagrid('reload', "/biz_port/get_option?type=" + trans_tool + "&carrier=" + rec.client_code + '&limit=true'+'&old='+old2);
        $('#trans_destination').combogrid('grid').datagrid('reload', "/biz_port/get_discharge?type=" + trans_tool + "&carrier=" + rec.client_code+'&old='+old3);

        // $('#trans_origin').combogrid('clear');
        // $('#trans_origin_name').textbox('clear');
        // $('#trans_discharge').combogrid('clear');
        // $('#trans_discharge_name').textbox('clear');
        // $('#trans_destination_inner').textbox('clear');
        // $('#trans_destination').combogrid('clear');
        // $('#trans_destination_name').textbox('clear');
        // $('#trans_destination_terminal').textbox('clear');
    }

    function biz_type_select(res){
        var trans_carrier = $('#trans_carrier').combobox('getValue');
        $('#biz_type').val(res.biz_type);
        $('#trans_mode').val(res.trans_mode);
        if(res.trans_mode == 'AIR'){
            $('#trans_tool').combobox('select', 'AIR');
        }else{
            $('#trans_tool').combobox('select', 'SEA');
        }
        var trans_tool = $('#trans_tool').combobox('getValue');
        //重新加载
        $('#trans_origin').combogrid('grid').datagrid('load', "/biz_port/get_option?type=" + trans_tool + '&carrier=' + trans_carrier + '&limit=true');
        $('#trans_discharge').combogrid('grid').datagrid('reload', "/biz_port/get_option?type=" + trans_tool + '&carrier=' + trans_carrier + '&limit=true');

        $('#trans_destination').combogrid('grid').datagrid('reload', "/biz_port/get_discharge?type=" + trans_tool + '&carrier=' + trans_carrier);
        if(res.trans_mode == 'FCL'){
            add_box();
        }else{
            del_box();
        }
    }

    function add_INCO_term_option() {
        // $('.INCO_term_option').css('display', 'table-row');
    }

    function del_INCO_term_option() {
        // $('input[name="INCO_option[]"]').removeAttr('checked');
        // $('#INCO_address').val('');
        // $('.INCO_term_option').css('display', 'none');
    }

    function dangerous_info(goods_type) {
        //如果不为普通货物，贼显示危险品详细信息
        if(goods_type == 'GC' || goods_type == 'RF'){
            $('.dangerous').css('display','none');
            $('.special_size').css('display','none');
        }else{
            if(goods_type == 'DR'){
                $('#isdangerous').textbox({required: true});
                // $('#ism_spec').textbox({required: true});
                $('#UN_no').textbox({required: true});
                // $('#IMDG_page').textbox({required: true});
                // $('#packing_type').textbox({required: true});
                // $('#flash_point').textbox({required: true});
                // $('#IMDG_code').textbox({required: true});
                // $('#is_pollution_sea').textbox({required: true});
                // $('#ems_no').textbox({required: true});
                // $('#MFAG_NO').textbox({required: true});
                // $('#e_contact_name').textbox({required: true});
                // $('#e_contact_tel').textbox({required: true});

                $('#long').textbox({required: false,value:''});
                $('#wide').textbox({required: false,value:''});
                $('#high').textbox({required: false,value:''});

                $('.dangerous').css('display','table-row');
                $('.special_size').css('display','none');
            }else if(goods_type == 'FR'){
                $('#isdangerous').textbox({required: false,value:''});
                // $('#ism_spec').textbox({required: false,value:''});
                $('#UN_no').textbox({required: false,value:''});
                // $('#IMDG_page').textbox({required: false,value:''});
                // $('#packing_type').textbox({required: false,value:''});
                // $('#flash_point').textbox({required: false,value:''});
                // $('#IMDG_code').textbox({required: false,value:''});
                // $('#is_pollution_sea').textbox({required: false,value:''});
                // $('#ems_no').textbox({required: false,value:''});
                // $('#MFAG_NO').textbox({required: false,value:''});
                // $('#e_contact_name').textbox({required: false,value:''});
                // $('#e_contact_tel').textbox({required: false,value:''});

                $('#long').textbox({required: true});
                $('#wide').textbox({required: true});
                $('#high').textbox({required: true});

                $('.dangerous').css('display','none');
                $('.special_size').css('display','table-row');
            }
        }
    }

    function INCO_term_info(INCO_term) {
        var destination_SO = ['DDU', 'DDP', 'DAP'];
        if($.inArray(INCO_term, destination_SO) !== -1){
            add_INCO_term_option();
        }else{
            del_INCO_term_option();
        }
    }
</script>
<style>
    .hide{
        display: none;
    }
    input:read-only,select:read-only,textarea:read-only
    {
        background-color: #F5F5F5;
        cursor: default;
    }
    input:disabled,select:disabled,textarea:disabled
    {
        background-color: #d6d6d6;
        cursor: default;
    }
    .dangerous{
        display: none;
    }
    .special_size{
        display: none;
    }
    .box_info_num{
        width: 100px;
    }
    .box_info_size{
        width: 100px;
    }
    .container_sum{
        color: red;
    }
</style>

<body style="margin-left:10px;">
<?php
function top_reminder($html=""){
    echo '<div style="background:#e0ecff;border: 1px solid #95B8E7;padding:12px 20px"> 
                <span class="l-btn-left l-btn-icon-left">
                    <span class="l-btn-icon icon-tip" style="top:40%;left: 1px">&nbsp;</span>
                    <span class="l-btn-text">' . lang("Tips") . ': <span style="color:red;font-weight:bold;">   '.$html.' </span>  </span> 
                </span> 
                
            </div>';
}

if(empty($dadanwancheng)){
    // top_reminder(lang('必须先勾选 "打单勾" 才能进一步创建consol和订舱！'));
}

if((empty($consol_id) && empty($consol_id_apply)) && !empty($dadanwancheng)){
    if($parent_id_read==0){
        top_reminder('<a href="javascript:void(0)" onclick="add_consol();"  class="easyui-linkbutton"  style="color:red;font-weight:bold;font-size: 14px;padding:0px 5px">' . lang('创建新consol'). '</a>&nbsp;&nbsp;' . lang('or') . '&nbsp;
                        ');
                        // <a href="javascript:void(0)" onclick="apply_consol();" class="easyui-linkbutton" style="color:red;font-weight:bold;font-size: 14px;padding:0px 5px">申请绑定已存在的consol</a>
    }else{
        top_reminder("<a href=\"javascript:void(0)\" onclick=\"window.open('/biz_shipment/edit/<?=$parent_id_read?>')\" class=\"easyui-linkbutton\" style=\"color:red;font-weight:bold;font-size: 14px;padding:0px 5px\">" . lang('点击跳转父shipment') . "</a>");
    }
}

?>


<div class="easyui-draggable" data-options="" style="position: absolute;right: 38px;top: 10px;z-index: 999;">
    <div id="service_option_tab" class="easyui-accordion" data-options="multiple:true" style="width:300px;">
        <div title="Service Option" id="service_options_div" style="padding:10px;" >
            <input type="hidden" <?php echo in_array("service_options",$G_userBscEdit) ? '' : 'disabled'; ?> name="service_options" id="service_options" value='<?= $service_options;?>'>
        </div>
        <div title="<?= lang('Consol 区域')?>" id="consol_area_div" >
            <table>
                <!-- <tr>
                     <td colspan="2">
                         -----------------consol区域-----------------
                     </td>
                 </tr>-->
                <?php
                $date_zero = array("trans_ETA","trans_ATA","trans_ETD","trans_ATD");
                foreach($date_zero as $dzero){
                    $consol[$dzero] = isset($consol[$dzero]) ? $consol[$dzero] : '';
                    if($consol[$dzero] < "1999-01-01") $consol[$dzero] = null;
                }
                ?>
                <tr>
                    <td><?= lang('订舱代理');?> </td>
                    <td>
                        <input class="easyui-textbox" disabled name="creditor" id="creditor"  style="width:180px;" value="<?php echo  $consol['creditor']  ;?>"/>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('update_trans_ETA');?> </td>
                    <td>
                        <input class="easyui-datetimebox" disabled name="trans_ETA" id="trans_ETA"  style="width:180px;" value="<?php echo  $consol['trans_ETA']  ;?>"/>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('update_trans_ATA');?> </td>
                    <td>
                        <input class="easyui-datetimebox" disabled name="trans_ATA" id="trans_ATA"  style="width:180px;" value="<?php echo  $consol['trans_ATA'] ;?>"/>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('update_trans_ETD');?> </td>
                    <td>
                        <input class="easyui-datetimebox" disabled name="trans_ETD" id="trans_ETD"  style="width:180px;" value="<?php echo  $consol['trans_ETD'] ;?>"/>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('update_trans_ATD');?> </td>
                    <td>
                        <input class="easyui-datetimebox" disabled name="trans_ATD" id="trans_ATD"  style="width:180px;" value="<?php echo  $consol['trans_ATD']  ;?>"/>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('vessel');?></td>
                    <td>
                        <select disabled id="vessel" class="easyui-combobox"  editable="true" name="vessel" style="width:180px;" data-options="
                            valueField:'value',
                            textField:'value',
                            value:'<?php echo isset($consol['vessel']) ? $consol['vessel'] : '';?>'
                            ">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('voyage');?></td>
                    <td>
                        <input disabled class="easyui-textbox"  name="voyage" id="voyage"  style="width:180px;" value="<?php echo isset($consol['voyage']) ? $consol['voyage'] : '';?>"/>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('BOOKING NO');?></td>
                    <td>
                        <?php
                        $carrier_ref = isset($consol['carrier_ref']) ? $consol['carrier_ref'] : '';
                        // if($carrier_ref!="" && strlen($cus_no)<3) echo "<script>setTimeout(\"alert('公司要求关单号必填，麻烦填下shipment关单号！')\",\"3000\");</script>";
                        ?>
                        <input disabled class="easyui-textbox"  name="carrier_ref" id="carrier_ref"  style="width:180px;" value="<?php echo $carrier_ref;?>"/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <?php if($consol_id==0) {?>
                            <?php if($consol_id_apply == 0){?>
                                <?php if($parent_id_read==0):?>
                                    <!--<a href="javascript:void(0)" onclick="apply_consol();" class="easyui-linkbutton" style="margin-right:15px;width:86px;">选择consol</a>-->
                                    <a href="javascript:void(0)" onclick="add_consol();"  class="easyui-linkbutton"  style="margin-right:15px;width:76px;"><?= lang('新增consol')?></a>
                                <?php endif;?>
                            <?php }else{ ?>
                                <a href="javascript:void(0)" style="margin-right:15px;width:116px;">申请中: <?= $consol_apply;?></a>
                                <a href="javascript:void(0)" onclick="cancel_apply()" style="margin-right:15px;width:48px;">撤销申请</a>
                            <?php } ?>

                        <?php }else{ ?>
                            <a href="/biz_consol/edit/<?php echo $consol_id;?>" class="easyui-linkbutton"  title="query consol" style="margin-right:15px;" target="_self"><?= lang('点击进入consol')?></a>
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </div>
        <div title="Shipment Tags" style="padding:10px;" >
            <style type="text/css">
                .topic-tag{
                    display:inline-block
                }
                .topic-tag .text {
                    display: inline-block;
                    height: 16px;
                    line-height: 16px;
                    padding: 2px 5px;
                    background-color: #99cfff;
                    font-size: 12px;
                    color: #fff;
                    border-radius: 4px;
                    cursor: pointer;
                }
                .topic-tag {
                    margin: 0 5px 2px 0;
                }
                .topic-tag .text:hover, .topic-tag .close:hover{
                    background-color: #339dff;
                }
                .this_tag .topic-tag .text, .role_tag .topic-tag .text{
                    border-radius: 4px 0 0 4px;
                    float: left;
                }
                .this_tag .topic-tag .close, .role_tag .topic-tag .close{
                    float: left;
                }
                .topic-tag-apply .text{
                    background-color: #cccccc;
                }
                .topic-tag .close {
                    width: 20px;
                    height: 20px;
                    background-color: #66b7ff;
                    text-align: center;
                    line-height: 20px;
                    color: #fff;
                    font-size: 10px;
                    opacity: 1;
                    filter: alpha(opacity=100);
                    border-radius: 0 4px 4px 0;
                    display: inline-block;
                    text-shadow: 0 1px 0 #fff;
                    cursor: pointer;
                }
                .add_tags_body_input{
                    display: inline-block;
                    vertical-align: middle;
                    width: 200px !important;
                    margin: 0 5px 10px 0;
                    padding: 6px;
                    resize: none;
                    box-shadow: none;
                    height: 22px;
                    font-size: 14px;
                    line-height: 1.42857143;
                    color: #555;
                    background-color: #fff;
                    background-image: none;
                    border: 1px solid #ccc;
                    border-radius: 4px;
                    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                    float: left;
                }
                .add_tags_body_type{
                    display: inline-block;
                    vertical-align: middle;
                    width: 100px !important;
                    margin: 0 5px 10px 0;
                    padding: 6px;
                    resize: none;
                    box-shadow: none;
                    height: 36px;
                    font-size: 14px;
                    line-height: 1.42857143;
                    color: #555;
                    background-image: none;
                    border: 1px solid #ccc;
                    border-radius: 4px;
                    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
                    float: left;
                }
                .add_tags_body_input_btn:hover{
                    background-color: rgb(22,155,213);
                }
                .add_tags_body_input_btn{
                    margin: 0 10px 10px 0;
                    border: none !important;
                    background-color: rgb(94,180,214);
                    color: #fff;
                    min-width: 76px;
                    height: 34px;
                    padding: 0 10px;
                    line-height: 34px;
                    font-size: 14px;
                    display: inline-block;
                    font-weight: 400;
                    text-align: center;
                    white-space: nowrap;
                    vertical-align: middle;
                    cursor: pointer;
                    -webkit-user-select: none;
                    -moz-user-select: none;
                    -ms-user-select: none;
                    user-select: none;
                    background-image: none;
                    border-radius: 4px;
                    box-sizing: border-box;
                }
                .add_tags_body h3{
                    margin-top: 20px;
                    color: #333;
                    font-size: 100%;
                    line-height: 1.7;
                    margin-bottom: 10px;
                }
                .add_tags_body{
                    padding:10px;
                }
            </style>
            <script type="text/javascript">
                //添加标签
                function add_tags() {
                    $('#add_tags_div').window('open');
                }
                //申请标签
                function apply_tag() {
                    var tag_name = $('#tag_name').val().trim();
                    if(tag_name == ''){
                        $.messager.alert('<?= lang('提示');?>', '<?= lang('标签内容不能为空');?>');
                        return false;
                    }
                    var tag_class = $('#tag_class').val();
                    if(tag_class == ''){
                        $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择标签分类');?>');
                        return false;
                    }
                    var post_data = {};
                    post_data.tag_name = tag_name;
                    post_data.tag_class = tag_class;
                    post_data.id_type = $('#tag_id_type').val();
                    post_data.id_no = $('#tag_id_no').val();
                    submit_add_tag(post_data, function (res) {
                        $.messager.alert('<?= lang('提示');?>', res.msg);
                        if(res.code == 0){
                            $('#tag_name').val('');
                            var pass = res.data.is_pass;
                            add_tag_html(post_data.tag_name, tag_class, 'system_tag', pass);
                            add_tag_html(post_data.tag_name, tag_class, 'this_tag', pass);
                        }else{

                        }
                    });
                }

                //点击添加标签
                function click_add_tag(e) {
                    var tag_name = $(e).text();
                    var tag_class = $(e).parents('.topic-bar').attr('tag_class');
                    var post_data = {};
                    post_data.tag_name = tag_name;
                    post_data.tag_class = tag_class;
                    post_data.id_type = $('#tag_id_type').val();
                    post_data.id_no = $('#tag_id_no').val();
                    submit_add_tag(post_data, function (res) {
                        if(res.code == 0){
                            var pass = res.data.is_pass;
                            add_tag_html(post_data.tag_name, tag_class, 'this_tag', pass);
                        }else{
                            $.messager.alert('<?= lang('提示');?>', res.msg);
                        }
                    });
                }

                /**
                 * 添加一个tag标签
                 * @param tag_name 标签名
                 * @param tag_class 标签类
                 * @param tag_type 标签类型, 目前有this_tag 右上角的,system_tag 系统推荐标签
                 * @param pass 是否通过,没通过的是灰色
                 */
                function add_tag_html(tag_name, tag_class, tag_type, pass = 0, is_delete = true) {
                    var span_class = "topic-tag";
                    if(pass == 0) span_class += ' topic-tag-apply';
                    if(tag_type === 'this_tag'){
                        //如果不存在,生成一个
                        if($(".this_tag[tag_class='" + tag_class + "']").length == 0){
                            var topic_bar_html = "<div class=\"topic-bar clearfix this_tag\" tag_class=\"" + tag_class + "\" style=\"margin: 0;\"><h3>" + tag_class + "</h3></div>";
                            $('.this_tag_box').append(topic_bar_html);
                        }

                        var tag_html = "<span class=\"" + span_class + "\">\n" +
                            "                    <a class=\"text\">" + tag_name + "</a>\n";
                        if(is_delete) tag_html += "<a class=\"close\" onclick=\"click_del_tag(this)\">X</a>" +
                            "                </span>";
                        $(".this_tag[tag_class='" + tag_class + "']").append(tag_html);
                    }else if(tag_type === 'system_tag'){
                        //如果不存在,生成一个
                        if($(".system_tag[tag_class='" + tag_class + "']").length == 0){
                            var topic_bar_html = "<div class=\"topic-bar clearfix system_tag\" tag_class=\"" + tag_class + "\" style=\"margin: 0;\"><h3>" + tag_class + "</h3></div>";
                            $('.system_tag_box').append(topic_bar_html);
                        }

                        var tag_html = "<span class=\"" + span_class + "\">\n" +
                            "                    <a class=\"text\" onclick=\"click_add_tag(this)\">" + tag_name + "</a>\n" +
                            "                </span>";
                        $(".system_tag[tag_class='" + tag_class + "']").append(tag_html);
                    }
                }

                //提交方法
                function submit_add_tag(post_data, fn) {
                    ajaxLoading();
                    $.ajax({
                        type: 'POST',
                        url: '/biz_tag/add_tag',
                        data: post_data,
                        success: function (res_json) {
                            ajaxLoadEnd();
                            try {
                                var res = $.parseJSON(res_json);
                                fn(res);
                            }catch (e) {
                                $.messager.alert('<?= lang('提示');?>', res_json);
                            }
                        },
                        error:function (e) {
                            ajaxLoadEnd();
                            $.messager.alert('<?= lang('提示');?>', e.responseText);
                        }
                    });
                }

                //点击删除当前tag
                function click_del_tag(e) {
                    var tag_name = $(e).prev().text();
                    var tag_class = $(e).parents('.topic-bar').attr('tag_class');
                    var post_data = {};
                    post_data.tag_name = tag_name;
                    post_data.tag_class = tag_class;
                    post_data.id_type = $('#tag_id_type').val();
                    post_data.id_no = $('#tag_id_no').val();
                    ajaxLoading();
                    $.ajax({
                        type: 'POST',
                        url: '/biz_tag/del_this_tag',
                        data: post_data,
                        success: function (res_json) {
                            ajaxLoadEnd();
                            try {
                                var res = $.parseJSON(res_json);
                                var topic_tag = $(e).parents('.topic-bar').children('.topic-tag');
                                if(topic_tag.length === 1) $(e).parents('.topic-bar').remove();
                                else $(e).parents('.topic-tag').remove();
                                //判断当前tag下是否还有其他标签, 如果一个都没了,tag_class整个删除
                            }catch (e) {
                                $.messager.alert('<?= lang('提示');?>', res_json);
                            }
                        },
                        error:function (e) {
                            ajaxLoadEnd();
                            $.messager.alert('<?= lang('提示');?>', e.responseText);
                        }
                    });
                }

                //加载当前tag
                function load_tag() {
                    var id_type = $('#tag_id_type').val();
                    var id_no = $('#tag_id_no').val();
                    //分为2部分,1是当前的tag,2是公共模板的tag
                    $.ajax({
                        type: 'GET',
                        url: '/biz_tag/get_tag?id_type=' + id_type + '&id_no=' + id_no,
                        success: function (res_json) {
                            ajaxLoadEnd();
                            try {
                                var res = $.parseJSON(res_json);
                                if(res.code == 0){
                                    //系统推荐的
                                    $.each(res.data.system_tag, function (i, it) {
                                        $.each(it.data, function (i2, it2) {
                                            var is_delete = false;
                                            if(it2.lock === '0') is_delete = true;
                                            add_tag_html(it2.tag_name, it.tag_class, 'system_tag', it2.pass, is_delete);
                                        });
                                    });

                                    //当前的
                                    $.each(res.data.this_tag, function (i, it) {
                                        $.each(it.data, function (i2, it2) {
                                            var is_delete = false;
                                            if(it2.lock === '0') is_delete = true;
                                            add_tag_html(it2.tag_name, it.tag_class, 'this_tag', it2.pass, is_delete);
                                        });
                                    })
                                }else{
                                    $.messager.alert('<?= lang('提示');?>', res_json);
                                }
                            }catch (e) {
                                $.messager.alert('<?= lang('提示');?>', res_json);
                            }
                        },
                        error:function (e) {
                            ajaxLoadEnd();
                            $.messager.alert('<?= lang('提示');?>', e.responseText);
                        }
                    });
                }

                $(function () {
                    load_tag();
                });
            </script>
            <a href="javascript:void(0)" onclick="add_tags()"><?= lang('添加标签'); ?></a>
            <input type="hidden" name="id_type" id="tag_id_type" value="biz_shipment">
            <input type="hidden" name="id_no" id="tag_id_no" value="<?= $id;?>">
            <br>-------------------------------------
            <br><br>
            <div class="this_tag_box">
            </div>
        </div>
        <div title="Client Tags" style="padding:10px;" >
            <script type="text/javascript">
                /**
                 * 添加一个tag标签
                 * @param tag_name 标签名
                 * @param tag_class 标签类
                 * @param tag_type 标签类型, 目前有this_client_tag 右上角的,system_client_tag 系统推荐标签
                 * @param pass 是否通过,没通过的是灰色
                 */
                function add_client_tag_html(tag_name, tag_class, tag_type, pass = 0) {
                    var span_class = "topic-tag";
                    if(pass == 0) span_class += ' topic-tag-apply';

                    if(tag_type === 'this_client_tag'){
                        //如果不存在,生成一个
                        if($(".this_client_tag[tag_class='" + tag_class + "']").length == 0){
                            var topic_bar_html = "<div class=\"topic-bar clearfix this_client_tag\" tag_class=\"" + tag_class + "\" style=\"margin: 0;\"><h3>" + tag_class + "</h3></div>";
                            $('.this_client_tag_box').append(topic_bar_html);
                        }

                        var tag_html = "<span class=\"" + span_class + "\">\n" +
                            "                    <a class=\"text\">" + tag_name + "</a>\n" +
                            "                </span>";
                        $(".this_client_tag[tag_class='" + tag_class + "']").append(tag_html);
                    }else if(tag_type === 'system_client_tag'){
                        //如果不存在,生成一个
                        if($(".system_client_tag[tag_class='" + tag_class + "']").length == 0){
                            var topic_bar_html = "<div class=\"topic-bar clearfix system_client_tag\" tag_class=\"" + tag_class + "\" style=\"margin: 0;\"><h3>" + tag_class + "</h3></div>";
                            $('.system_client_tag_box').append(topic_bar_html);
                        }

                        var tag_html = "<span class=\"" + span_class + "\">\n" +
                            "                </span>";
                        $(".system_client_tag[tag_class='" + tag_class + "']").append(tag_html);
                    }
                }
                //加载当前tag
                function load_client_tag() {
                    var id_type = $('#client_tag_id_type').val();
                    var id_no = $('#client_tag_id_no').val();
                    //分为2部分,1是当前的tag,2是公共模板的tag
                    $.ajax({
                        type: 'GET',
                        url: '/biz_tag/get_tag?id_type=' + id_type + '&id_no=' + id_no,
                        success: function (res_json) {
                            ajaxLoadEnd();
                            try {
                                var res = $.parseJSON(res_json);
                                if(res.code == 0){
                                    console.log();
                                    //系统推荐的
                                    $.each(res.data.system_tag, function (i, it) {
                                        $.each(it.data, function (i2, it2) {
                                            add_client_tag_html(it2.tag_name, it.tag_class, 'system_client_tag', it2.pass);
                                        });
                                    });

                                    //当前的
                                    $.each(res.data.this_tag, function (i, it) {
                                        $.each(it.data, function (i2, it2) {
                                            add_client_tag_html(it2.tag_name, it.tag_class, 'this_client_tag', it2.pass);
                                        });
                                    })
                                }else{
                                    $.messager.alert('<?= lang('提示');?>', res_json);
                                }
                            }catch (e) {
                                $.messager.alert('<?= lang('提示');?>', res_json);
                            }
                        },
                        error:function (e) {
                            ajaxLoadEnd();
                            $.messager.alert('<?= lang('提示');?>', e.responseText);
                        }
                    });
                }

                $(function () {
                    load_client_tag();
                });
            </script>
            <input type="hidden" name="id_type" id="client_tag_id_type" value="biz_client">
            <input type="hidden" name="id_no" id="client_tag_id_no" value="<?= $client_id;?>">
            <div class="this_client_tag_box">
            </div>
        </div>
    </div>
</div>
<!--添加标签-->
<div id="add_tags_div" class="easyui-window" style="width:450px;" data-options="title:'window',modal:true,closed:true,height:'auto',top:200">
    <div class="add_tags_body">
        <h3>自由添加标签</h3>
        <div class="add_tags_body_input_div" style="display: block;">
            <input type="text" class="add_tags_body_input" id="tag_name" autocomplete="off" placeholder="创建或搜索添加新话题...">
            <select class="add_tags_body_type" id="tag_class" style="background-color: #fff;">
                <option value="">选择分类</option>
                <option value="操作类">操作类</option>
                <option value="财务类">财务类</option>
                <option value="其他类">其他类</option>
            </select>
            <a href="javascript:void(0);" onclick="apply_tag()" class="add_tags_body_input_btn">申请添加</a>
        </div>
        <div class="system_tag_box">

        </div>
    </div>
</div>
<table><tr><td>
            <h2> <?= lang('编辑');?> </h2></td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>Job No: <?php echo $job_no;?> &nbsp;&nbsp;&nbsp;&nbsp;  BOOKING NO: <?php echo $consol['carrier_ref'];?> <br>
            <script>
                function reset_over_credit(client_code) {
                    $.ajax({
                        type: 'POST',
                        url: '/other/client_credit',
                        data: {client_code:client_code},
                        dataType: 'json',
                        success: function (res) {
                            $.messager.alert('Tips', res.msg);
                            location.reload();
                        },
                        error: function () {
                            $.messager.alert('Tips', 'error');
                        }
                    });
                }
                function reload_shipment_flag(id) {
                    $.messager.confirm('确认对话框', '是否确认刷新开票标记等,刷新成功后会自动刷新当前页面？', function(r){
                        if (r){
                            $.ajax({
                                type: 'POST',
                                url: '/other/reload_shipment_flag',
                                data: {id:id},
                                dataType: 'json',
                                success: function (res) {
                                    $.messager.alert('Tips', res.msg);
                                    location.reload();
                                },
                                error: function (e) {
                                    $.messager.alert('Tips', e.responseText);
                                }
                            });
                        }
                    });
                }

            </script>
            <?php
			if($invoice_flag==0) echo "<span style='color:green;font-weight:bold;cursor:pointer;' onclick=\"reload_shipment_flag('$id')\"> [No Invoice] </span>";
			if($payment_flag==0) echo "<span style='color:green;font-weight:bold;cursor:pointer;' onclick=\"reload_shipment_flag('$id')\"> [No writeoff] </span>";
			if($loss_flag==1) echo "<span style='color:violet;font-weight:bold;cursor:pointer;' onclick=\"reload_shipment_flag('$id')\"> [Loss] </span>";
            $lock_lv_span = '';
            if($lock_lv != 0){
                $lock_lv_title = '';
				if($lock_lv == 1) $lock_lv_title = 'operator lock';
				if($lock_lv == 2) $lock_lv_title = 'sales lock';
				if($lock_lv == 3) $lock_lv_title = 'finance lock';
				$lock_lv_span =  "[S{$lock_lv} lock]";
            } else{
                $lock_lv_title = '';
				if($consol['lock_lv'] != 0) $lock_lv_title = "carrier cost lock";
				if($consol['lock_lv'] != 0) $lock_lv_span = "[C{$consol['lock_lv']} lock]";
            }
            ?>
            <?php if($lock_lv_span != ''){ ?>
                <a href="javascript:void(0);" class="easyui-tooltip" style="color:violet;font-weight:bold;" data-options="content: function(){
                var str = '<div>';
                <?php foreach ($lock_log as $ll_row){
                    $this_lock_lv_title = '';
                    if($ll_row['op_value'] == 0)$this_lock_lv_title = lang('S0 lock');
					if($ll_row['op_value'] == 1)$this_lock_lv_title = lang('operator lock');
					if($ll_row['op_value'] == 2)$this_lock_lv_title = lang('sales lock');
					if($ll_row['op_value'] == 3)$this_lock_lv_title = lang('finance lock');
                    ?>
                str += '<?= $this_lock_lv_title;?> <?= $ll_row['user_name'];?> <?= $ll_row['op_time_max']?> <br/>';
                <?php } ?>
                <?php foreach ($consol['lock_log'] as $cll_row){
                    $this_lock_lv_title = '';
					if($consol['lock_lv'] == 0) $this_lock_lv_title = "C0 lock";
					if($consol['lock_lv'] != 0) $this_lock_lv_title = "carrier cost lock";
                    ?>
                str += '<?= $this_lock_lv_title;?> <?= $cll_row['user_name'];?> <?= $cll_row['op_time_max']?>';
                <?php } ?>
                if(str == '<div>') str += '<?= $lock_lv_title;?>';
                str += '</div>';
                return str;
            },onShow: function(){$(this).tooltip('arrow').css('left', 20);$(this).tooltip('tip').css('left', $(this).offset().left); },"><?= $lock_lv_span;?></a>
            <?php } ?>
            <script>
                $(function () {
                    $('#openBL').click(function(){
                        $("#tb").tabs('select','B/L');
                    })
                })
            </script>
        </td>
        <td>
            <p style="margin: 0px 50px;">

            </p>
        </td>
        <td> &nbsp;&nbsp;&nbsp;&nbsp; </td>
		<td  style="background-color:#0099cc;" rowspan="2" class="tdcheck">
			<iframe src="/biz_shipment/basic_steps/<?=$id?>" style="width:73vw;height:81px;" frameborder="0"></iframe>
		</td>
    </tr>
	<tr>
		<td></td>
		<td></td>
		<td colspan="3">
			<?php if($from_db != '' && $from_id_no != 0):?>
			<span><?=$biz_type == 'export' ? 'To' : 'From'?> : <?=$from_db?> &emsp; ID: <?=$from_id_no?></span>
			<?php endif;?>
		</td>
	</tr>
</table>

<div  id="tb" class="easyui-tabs" style="width:100%;height:850px">
    <div title="Basic Registration" style="padding:1px">
        <form id="f" name="f" method="post" action="/biz_shipment/update_data/<?php echo $id;?>">
            <div class="easyui-layout" style="width:100%;height:780px;">
                <input type="hidden" <?php echo in_array("job_no",$G_userBscEdit) ? '' : 'disabled'; ?> name="job_no" value="<?php echo $job_no;?>">
				<div data-options="region:'west',tools:'#tt'" title="Basic" style="width:350px;">
					<div class="easyui-accordion" data-options="multiple:true" style="width:340px;height1:300px;">
						<div title="Client" style="padding:10px;" class="basic_show" data-options="collapsible:false">
							<?=lang('Client')?>:<br>
							<?= $client_code; ?>/<?= $client_company; ?><br>
							<?= htmlentities($client_email); ?> <?= $verify_email1 === true?'':($contact_id1 == 0 ? '':"<span style='color: red;cursor: pointer' onclick='verify_email(1,{$contact_id1})'>not verify</span>")?><br>
							<?= htmlentities($client_telephone); ?>
							<br>
							<div class="client_code2_div" style="display: <?=$client_company2 != ''?'block':'none';?>">
								<?= lang('Client B');?>:<br/>
								<?= htmlentities($client_company2); ?><br>
								<?= htmlentities($client2_email); ?><?= $verify_email2 === true ? '' : ($contact_id2 == 0 ? '':"<span style='color: red;cursor: pointer' onclick='verify_email(2,{$contact_id2})'>not verify</span>")?><br>
								<?= htmlentities($client2_telephone);?>
							</div>
						</div>
						<div title="Shipper"  style="overflow:auto;padding:10px;" class="basic_show" data-options="collapsible:false">
							<?= htmlentities($shipper_company); ?>
							<br><br>
							<?php echo nl2br(htmlentities($shipper_address));?>
						</div>
						<div title="Consignee" style="padding:10px;" class="basic_show" data-options="collapsible:false">
							<?= htmlentities($consignee_company);?>
							<br><br>
							<?php echo nl2br(htmlentities($consignee_address));?>
						</div>
						<div title="Notify Party" style="padding:10px;" class="basic_show" data-options="collapsible:false">
							<?= htmlentities($notify_company);?>
							<br><br>
							<?php echo htmlentities($notify_address);?>
						</div>
						<br />
						<span>
                            <div style="width: 300px;display: flex;">
                                <div style="width:100px;">ebk:</div>
                                <div><?= isset($shipment_ebk['updated_time'])?$shipment_ebk['updated_time']:'0000-00-00';?></div>
                            </div>
                            <div style="width: 300px;display: flex;">
                                <div style="width:100px;">cut off:</div>
                                <div><?= isset($consol['closing_date'])?$consol['closing_date']:'0000-00-00';?></div>
                            </div>
                            <div style="width: 300px;display: flex;">
                                <div style="width:100px;">si online:</div>
                                <div><?= isset($si_online['created_time'])?$si_online['created_time']:'0000-00-00';?></div>
                            </div>
                        </span>

					</div>
					<div id="tt">
						<a href="javascript:void(0)"  onclick="edit_basic()" style="margin-right:15px;width: 30px;">Edit</a>
						<a href="javascript:void(0)" class="icon-save" onclick="<?php if(!$r_e) echo "$('#f').form('submit');";?>" title="save" style="margin-right:15px;"></a>
					</div>
				</div>
                <div data-options="region:'center',tools:'#tt2',title:'Details'">
                    <div id="tt2">
                        <?php if($consol_id==0) {?>
                            <?php if($consol_id_apply == 0){?>
                                <!--<a href="javascript:void(0)" onclick="apply_consol();" style="margin-right:15px;width:76px;">选择CONSOL</a>-->
                                <a href="javascript:void(0)" class="icon-add" onclick="add_consol();" title="add consol" style="margin-right:15px;"></a>
                            <?php }else{ ?>
								<a href="javascript:void(0)" style="margin-right:15px;width:116px;">Applying: <?= $consol_apply;?></a>
								<a href="javascript:void(0)" onclick="cancel_apply()" style="margin-right:15px;width:48px;">Cancel apply</a>
							<?php } ?>

                        <?php }else{ ?>
                            <a href="/biz_consol/edit/<?php echo $consol_id;?>" class="icon-tip"  title="query consol" style="margin-right:15px;" target="_self"></a>
                        <?php } ?>
                    </div>
                    <div style="float: left;">
                        <table width="500" border="0" cellspacing="0">
                            <tr>
                                <td><?= lang('update_biz_type');?></td>
                                <td>
                                    <select class="easyui-combobox" id="biz_type1" <?php echo in_array("biz_type",$G_userBscEdit) && in_array('trans_mode', $G_userBscEdit) ? '' : 'disabled'; ?> style="width:180px;" data-options="
                                        editable:false,
                                        valueField:'name',
                                        textField:'name',
                                        url:'/bsc_dict/get_biz_type',
                                        value: '<?php echo $biz_type . ' ' . $trans_mode;?>',
                                        onSelect: function(res){
                                            biz_type_select(res);
                                            var good_volume_val = $('#good_volume').textbox('getValue');
                                        },
                                    ">
                                    </select>
                                    <input type="hidden" name="trans_mode" id="trans_mode" value="<?= $trans_mode;?>">
                                    <input type="hidden" name="biz_type" id="biz_type" value="<?= $biz_type;?>">
                                    <select <?php echo in_array('trans_tool', $G_userBscEdit) ? '' : 'disabled'; ?> name="trans_tool" id="trans_tool" class="easyui-combobox" style="width:75px;" data-options="
                                        editable:false,
                                        valueField:'value',
                                        textField:'name',
                                        url:'/bsc_dict/get_option/trans_tool',
                                        value:'<?= $trans_tool;?>',
                                        onSelect:function(rec){
                                            trans_tool_select(rec);
                                        },
                                        onLoadSuccess: function(){
                                            var val = $(this).combobox('getValue');
                                            var rec = {value:val};
                                            trans_tool_select(rec);
                                        }
                                    ">
                                    </select>
                                </td>
                            </tr>
                            <?php if(isset($box_info) && !empty($box_info)){foreach($box_info as $key => $row){
                                $this_btn = '<a class="easyui-linkbutton" onclick="del_box(this)" iconCls="icon-remove"></a>';
                                $this_lang = '';
                                if($key == 0){
                                    $this_btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
                                    $this_lang = lang('box_info');
                                }
                                ?>

                                <tr class="add_box">
                                    <td><?= $this_lang;?></td>
                                    <td>
                                        <select <?php echo in_array("box_info",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-combobox box_info_size" name="box_info[size][]" data-options="
                                        valueField:'value',
                                        textField:'value',
                                        url:'/bsc_dict/get_option/container_size',
                                        value: '<?= $row['size']?>',
                                        onHidePanel: function() {
                                            var valueField = $(this).combobox('options').valueField;
                                            var val = $(this).combobox('getValue');
                                            var allData = $(this).combobox('getData');
                                            var result = true;
                                            for (var i = 0; i < allData.length; i++) {
                                                if (val == allData[i][valueField]) {
                                                    result = false;
                                                }
                                            }
                                            if (result) {
                                                $(this).combobox('clear');
                                            }
                                        }
                                    "></select>
                                        X
                                        <input <?php echo in_array("box_info",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-numberbox box_info_num"  name="box_info[num][]" value="<?= $row['num'];?>">
                                        <?= $this_btn;?>
                                    </td>
                                </tr>
                            <?php }}else{
                                if($trans_mode == 'FCL'){
                                    $this_btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
                                    $this_lang = lang('box_info');
                                    ?>
                                    <tr class="add_box">
                                        <td><?= $this_lang;?></td>
                                        <td>
                                            <select <?php echo in_array("box_info",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-combobox box_info_size" name="box_info[size][]" data-options="
                                        valueField:'value',
                                        textField:'value',
                                        url:'/bsc_dict/get_option/container_size',
                                        onHidePanel: function() {
                                            var valueField = $(this).combobox('options').valueField;
                                            var val = $(this).combobox('getValue');
                                            var allData = $(this).combobox('getData');
                                            var result = true;
                                            for (var i = 0; i < allData.length; i++) {
                                                if (val == allData[i][valueField]) {
                                                    result = false;
                                                }
                                            }
                                            if (result) {
                                                $(this).combobox('clear');
                                            }
                                        }
                                    "></select>
                                            X
                                            <input <?php echo in_array("box_info",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-numberbox box_info_num"  name="box_info[num][]" value="">
                                            <?= $this_btn;?>
                                        </td>
                                    </tr>
                                <?php }}?>
                            <tr>
                                <td><?= lang('booking_ETD');?> </td>
                                <td>
                                    <input class="easyui-datebox" editable="false" <?php echo in_array('booking_ETD', $G_userBscEdit) ? '' : 'disabled'?> name="booking_ETD" id="booking_ETD"  style="width:235px;" value="<?php echo $booking_ETD;?>"/>
                                    <a href="javascript:void(0);" class="easyui-tooltip" data-options="
                                        content: $('<div></div>'),
                                        onShow: function(){
                                            $(this).tooltip('arrow').css('left', 20);
                                            $(this).tooltip('tip').css('left', $(this).offset().left);
                                        },
                                        onUpdate: function(cc){
                                            cc.panel({
                                                width: 300,
                                                height: 'auto',
                                                border: false,
                                                href: '/bsc_help_content/help/shipment_booking_ETD'
                                            });
                                        }
                                    "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('sailing_code');?></td>
                                <td>
                                    <input class="easyui-textbox" <?php echo in_array('sailing_code', $G_userBscEdit) ? '' : 'disabled'?> name="sailing_code" id="sailing_code" style="width:255px;" value="<?= $sailing_code;?>" data-options="onChange:toUpperCaseChange">
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_status');?></td>
                                <td>
                                    <select class="easyui-combobox" name="status" id="status" <?php echo in_array("status",$G_userBscEdit) ? '' : 'disabled'; ?> style="width:160px;" data-options="
                                        editable:false,
                                        valueField:'value',
                                        textField:'name',
                                        url:'/bsc_dict/get_option/bill_status',
                                        value:'<?php echo $status;?>'">
                                    </select>

                                </td>
                            </tr>

                            <tr>
                                <td><?= lang('update_hbl_no');?></td>
                                <td>
                                    <input class="easyui-textbox" <?php echo in_array("hbl_no",$G_userBscEdit) ? '' : 'disabled'; ?> name="hbl_no" id="hbl_no"  style="width:255px;" value="<?php echo $hbl_no;?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_hbl_type');?></td>
                                <td>
                                    <select id="hbl_type" class="easyui-combobox" <?php echo in_array("hbl_type",$G_userBscEdit) ? '' : 'disabled'; ?> editable="false" name="hbl_type" style="width:255px;" data-options="
                                        valueField:'value',
                                        textField:'name',
                                        url:'/bsc_dict/get_hbl_type',
                                        value: '<?php echo $hbl_type;?>',
                                    ">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('批复号');?></td>
                                <td>
                                    <input <?php echo in_array("AGR_no",$G_userBscEdit) ? '' : 'disabled'; ?> type="text" class="easyui-textbox" name="AGR_no" id="AGR_no" style="width: 255px;" value="<?= $AGR_no;?>">
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_shipper_ref');?> </td>
                                <td>
                                    <input class="easyui-textbox" <?php echo in_array("shipper_ref",$G_userBscEdit) ? '' : 'disabled'; ?> name="shipper_ref" id="shipper_ref"  style="width:255px;" value="<?php echo $shipper_ref;?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_release_type');?> </td>
                                <td>
                                    <select id="release_type" <?php echo in_array("release_type",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-combobox"  editable="false" name="release_type" style="width:100px;" data-options="
        								valueField:'value',
        								textField:'name',
        								url:'/bsc_dict/get_option/release_type',
        								value:'<?php echo $release_type;?>',
        								onSelect: function(rec){
                                            if(rec.value=='TER'){
                                                $('#release_type_remark_box').css('display', '');
                                                $('#release_type_remark').combobox('setValue','<?=$release_type_remark;?>');
                                            } else {
                                                $('#release_type_remark').combobox('setValue','');
                                                $('#release_type_remark_box').css('display', 'none');
                                            }

                                        },
    								">
                                    </select>

                                    <a href="javascript:void(0);" class="easyui-tooltip" data-options="
                                        content: $('<div></div>'),
                                        onShow: function(){
                                            $(this).tooltip('arrow').css('left', 20);
                                            $(this).tooltip('tip').css('left', $(this).offset().left);
                                        },
                                        onUpdate: function(cc){
                                            cc.panel({
                                                width: 300,
                                                height: 'auto',
                                                border: false,
                                                href: '/bsc_help_content/help/release_type'
                                            });
                                        }
                                    "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>

                                    <span id="release_type_remark_box" style="display:none;">
                                        <?php $release_type !== 'TER' && $release_type_remark = '';?>
                                        <select class="easyui-combobox" id="release_type_remark" name="release_type_remark" label="State:" labelPosition="top" style="width:134px;">
                                            <option value="0" <?= $release_type_remark == '0' ? 'selected' : '';?>><?= lang('请选择放单方式');?></option>
                                            <option value="1" <?= $release_type_remark == '1' ? 'selected' : '';?>><?= lang('等通知电放');?></option>
                                            <option value="2" <?= $release_type_remark == '2' ? 'selected' : '';?>><?= lang('直接电放');?></option>
                                        </select>
                                        <script>
                                            $(function () {
                                                var release_type = '<?=$release_type;?>';
                                                if (release_type == 'TER'){
                                                    $('#release_type_remark_box').css('display', '');
                                                    // $('#release_type_remark').combobox('setValue','<?=$release_type_remark;?>');
                                                }else{
                                                    $('#release_type_remark_box').css('display', 'none');
                                                    // $('#release_type_remark').combobox('setValue','');
                                                }
                                            })
                                        </script>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_trans_carrier');?></td>
                                <td>
                                    <select id="trans_carrier" class="easyui-combobox" <?php echo in_array("trans_carrier",$G_userBscEdit) ? '' : 'disabled'; ?>  editable="true" name="trans_carrier" style="width:255px;" data-options="
        								valueField:'client_code',
                                        textField:'client_name',
        								url:'/biz_client/get_option/carrier',
        								value:'<?php echo $trans_carrier;?>',
    								    onSelect:function(rec){
                                            trans_carrier_select(rec);
                                        },
        								onHidePanel: function() {
                                            var valueField = $(this).combobox('options').valueField;
                                            var val = $(this).combobox('getValue');
                                            var allData = $(this).combobox('getData');
                                            var result = true;
                                            for (var i = 0; i < allData.length; i++) {
                                                if (val == allData[i][valueField]) {
                                                    result = false;
                                                }
                                            }
                                            if (result) {
                                                $(this).combobox('clear');
                                            }
                                        },
        							">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_trans_origin');?></td>
                                <td>
                                    <select class="easyui-combogrid" <?php echo in_array("trans_origin",$G_userBscEdit) ? '' : 'disabled'; ?> style="width: 80px;" name="trans_origin" id="trans_origin" data-options="
                                        panelWidth: '1000px',
                                        panelHeight: '300px',
                                        idField: 'port_code',              //ID字段
                                        textField: 'port_code',    //显示的字段
                                        url:'/biz_port/get_option?type=<?= $trans_tool;?>&carrier=<?= $trans_carrier;?>&limit=true&old=<?= $trans_origin;?>',
                                        striped: true,
                                        editable: false,
                                        pagination: false,           //是否分页
                                        toolbar : '#trans_origin_div',
                                        collapsible: true,         //是否可折叠的
                                        method: 'get',
                                        columns:[[
                                            {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                                            {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                                            {field:'terminal',title:'<?= lang('码头');?>',width:200},
                                        ]],
                                        mode: 'remote',
                                        value: '<?= $trans_origin;?>',
                                        emptyMsg : '未找到相应数据!',
                                        onSelect: function(index, row){
                                            if(row !== undefined){
                                                $('#trans_origin_name').textbox('setValue', row.port_name);
                                            }
                                        },
                                        onShowPanel:function(){
                                            var opt = $(this).combogrid('options');
                                            var toolbar = opt.toolbar;
                                            combogrid_search_focus(toolbar, 1);
                                        },
                                    ">
                                    </select>
                                    <input class="easyui-textbox" <?php echo in_array("trans_origin_name",$G_userBscEdit) ? '' : 'disabled'; ?> style="width: 170px;" name="trans_origin_name" id="trans_origin_name" value="<?= $trans_origin_name;?>">
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_trans_discharge');?></td>
                                <td>
                                    <select class="easyui-combogrid" <?php echo in_array("trans_discharge",$G_userBscEdit) ? '' : 'disabled'; ?> style="width: 80px;" name="trans_discharge" id="trans_discharge" data-options="
                                        panelWidth: '1000px',
                                        panelHeight: '300px',
                                        idField: 'port_code',              //ID字段
                                        textField: 'port_code',    //显示的字段
                                        url:'/biz_port/get_option?type=<?= $trans_tool;?>&carrier=<?= $trans_carrier;?>&limit=true&old=<?= $trans_discharge;?>',
                                        striped: true,
                                        editable: false,
                                        pagination: false,           //是否分页
                                        toolbar : '#trans_discharge_div',
                                        collapsible: true,         //是否可折叠的
                                        method: 'get',
                                        columns:[[
                                            {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                                            {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                                            {field:'terminal',title:'<?= lang('码头');?>',width:200},
                                        ]],
                                        mode: 'remote',
                                        value: '<?= $trans_discharge;?>',
                                        emptyMsg : '未找到相应数据!',
                                        onSelect: function(index, row){
                                            if(row !== undefined){
                                                $('#trans_discharge_name').textbox('setValue', row.port_name);
                                            }
                                        },
                                        onShowPanel:function(){
                                            var opt = $(this).combogrid('options');
                                            var toolbar = opt.toolbar;
                                            combogrid_search_focus(toolbar, 1);
                                        },
                                    ">
                                    </select>
                                    <input class="easyui-textbox" <?php echo in_array("trans_discharge_name",$G_userBscEdit) ? '' : 'disabled'; ?> style="width: 170px;" name="trans_discharge_name" id="trans_discharge_name" value="<?= $trans_discharge_name;?>">
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_trans_destination');?></td>
                                <td>
                                    <input class="easyui-textbox" <?php echo in_array("trans_destination_inner",$G_userBscEdit) ? '' : 'disabled'; ?> readonly style="width: 80px;" id="trans_destination_inner" name="trans_destination_inner" value="<?= $trans_destination_inner;?>">
                                    <select class="easyui-combogrid" <?php echo in_array("trans_destination",$G_userBscEdit) ? '' : 'disabled'; ?> style="width: 70px;" name="trans_destination" id="trans_destination" data-options="
                                        panelWidth: '1000px',
                                        panelHeight: '300px',
                                        idField: 'port_code',              //ID字段
                                        textField: 'port_code',    //显示的字段
                                        url:'/biz_port/get_discharge?type=<?= $trans_tool;?>&carrier=<?= $trans_carrier?>&old=<?= $trans_destination;?>',
                                        striped: true,
                                        editable: false,
                                        pagination: false,           //是否分页
                                        toolbar : '#trans_destination_div',
                                        collapsible: true,         //是否可折叠的
                                        method: 'get',
                                        columns:[[
                                            {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                                            {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                                            {field:'terminal',title:'<?= lang('码头');?>',width:200},
                                        ]],
                                        mode: 'remote',
                                        value: '<?= $trans_destination;?>',
                                        emptyMsg : '未找到相应数据!',
                                        onSelect: function(index, row){
                                            if(row !== undefined){
                                                $('#trans_destination_inner').textbox('setValue', row.inner_port_code);
                                                $('#trans_destination_name').textbox('setValue', row.port_name);
                                                $('#sailing_area').textbox('setValue', row.sailing_area);
                                                $('#sailing').textbox('setValue', row.sailing_lang);
                                                $('#trans_destination_terminal').textbox('setValue', row.terminal);
                                            }
                                        },
                                        onShowPanel:function(){
                                            var opt = $(this).combogrid('options');
                                            var toolbar = opt.toolbar;
                                            combogrid_search_focus(toolbar, 1);
                                        },
                                    ">
                                    </select>
                                    <input class="easyui-textbox" <?php echo in_array("trans_destination_name",$G_userBscEdit) ? '' : 'disabled'; ?> style="width: 97px;" name="trans_destination_name" id="trans_destination_name" value="<?= $trans_destination_name;?>">
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('trans_destination_terminal');?></td>
                                <td>
                                    <input class="easyui-textbox" <?php echo in_array("trans_destination_terminal",$G_userBscEdit) ? '' : 'disabled'; ?> style="width: 255px;" name="trans_destination_terminal" id="trans_destination_terminal" value="<?= $trans_destination_terminal;?>">
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('sailing_area');?></td>
                                <td>
                                    <select class="easyui-combobox" name="sailing_area" id="sailing_area" style="width:80px;" data-options="
                                        readonly: true,
                                        valueField:'sailing_area',
								        textField:'sailing_area',
                                        value:'<?= $sailing_area;?>'
                                    "></select>
                                    <select class="easyui-combobox" id="sailing" style="width:170px;" data-options="
                                        readonly: true,
                                        valueField:'sailing_area',
								        textField:'sailing',
								        value:'<?= $sailing_area;?>',
                                    "></select>
                                </td>

                            </tr>
                            <tr>
                                <td><?= lang('关单号');?></td>
                                <td>
                                    <input class="easyui-textbox" <?php echo in_array("cus_no",$G_userBscEdit) ? '' : 'disabled'; ?> id="cus_no" name="cus_no" style="width: 215px;" value="<?= $cus_no;?>" data-options="onChange:toUpperCaseChange">
                                    <a href="javascript:void(0);" class="easyui-tooltip" data-options="
                                        content: $('<div></div>'),
                                        onShow: function(){
                                            $(this).tooltip('arrow').css('left', 20);
                                            $(this).tooltip('tip').css('left', $(this).offset().left);
                                        },
                                        onUpdate: function(cc){
                                            cc.panel({
                                                width: 300,
                                                height: 'auto',
                                                border: false,
                                                href: '/bsc_help_content/help/cus_no'
                                            });
                                        }
                                    "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                                    <?php if($child_shipment > 0){?>
                                        <a href="javascript:void(0)" onclick="open_sub_shipment();"><?= lang('子');?></a>
                                        <script>
                                            function open_sub_shipment(){
                                                document.getElementById("tanchuang").src="/biz_shipment/sub_shipment/<?php echo $id;?>";
                                                $('#gcenter').window('open');
                                            }

                                        </script>
                                    <?php }?>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('trans_term');?></td>
                                <td>
                                    <select id="trans_term" <?php echo in_array("trans_term",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-combobox"  editable="false" name="trans_term" style="width:255px;" data-options="
                            value:'<?= $trans_term;?>',
                            valueField:'value',
                            textField:'value',
                            url:'/bsc_dict/get_option/trans_term',
                            ">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_INCO_term');?></td>
                                <td>
                                    <select id="INCO_term" <?php echo in_array("INCO_term",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-combobox"  editable="false" name="INCO_term" style="width:255px;" data-options="
								valueField:'value',
								textField:'value',
								url:'/bsc_dict/get_option/INCO_term',
                                value: '<?php echo $INCO_term;?>',
                                onSelect: function(rec){
                                    INCO_term_info(rec.value);
                                },
								">
                                    </select>

                                </td>
                            </tr>
                            <!--<tr class="INCO_term_option">-->
                            <!--    <td><?= lang('INCO_option');?></td>-->
                            <!--    <td>-->
                            <!--        <input type='checkbox' <?php echo in_array("INCO_option",$G_userBscEdit) ? '' : 'disabled'; ?> name='INCO_option[]' value='custom' style='vertical-align:middle;' <?php if(in_array('custom',$INCO_option)) echo 'checked';?> ><?= lang('custom');?>-->
                            <!--        <input type='checkbox' <?php echo in_array("INCO_option",$G_userBscEdit) ? '' : 'disabled'; ?> name='INCO_option[]' value='turcking' style='vertical-align:middle;' <?php if(in_array('turcking',$INCO_option)) echo 'checked';?>><?= lang('turcking');?>-->
                            <!--        <input type='checkbox' <?php echo in_array("INCO_option",$G_userBscEdit) ? '' : 'disabled'; ?> name='INCO_option[]' value='warehouse' style='vertical-align:middle;' <?php if(in_array('warehouse',$INCO_option)) echo 'checked';?>><?= lang('warehouse');?>-->
                            <!--    </td>-->
                            <!--</tr>-->
                            <!--<tr class="INCO_term_option">-->
                            <!--    <td><?= lang('INCO_address');?></td>-->
                            <!--    <td>-->
                            <!--        <textarea <?php echo in_array("INCO_address",$G_userBscEdit) ? '' : 'disabled'; ?> name="INCO_address" id="INCO_address" cols="30" style="height: 60px;width: 248px;"><?= $INCO_address;?></textarea>-->
                            <!--    </td>-->
                            <!--</tr>-->
                            <!--						<tr>-->
                            <!--						<td>--><?//= lang('update_issue_date');?><!--</td>-->
                            <!--						<td>-->
                            <!--							<input class="easyui-datebox" --><?php //echo in_array("issue_date",$G_userBscEdit) ? '' : 'disabled'; ?><!--  name="issue_date" id="issue_date"  style="width:255px;" value="--><?php //echo $issue_date;?><!--"/>-->
                            <!--						</td>-->
                            <!--						</tr>-->
                            <!--						<tr>-->
                            <!--						<td>--><?//= lang('update_expiry_date');?><!--</td>-->
                            <!--						<td>-->
                            <!--							<input class="easyui-datebox" --><?php //echo in_array("expiry_date",$G_userBscEdit) ? '' : 'disabled'; ?><!--  name="expiry_date" id="expiry_date"  style="width:255px;" value="--><?php //echo $expiry_date;?><!--"/>-->
                            <!--						</td>-->
                            <!--						</tr>-->
                            <tr>
                                <td><?= lang('requirements');?></td>
                                <td>
                                    <textarea name="requirements" <?php echo in_array("requirements",$G_userBscEdit) ? '' : 'disabled'; ?> id="requirements" style="height:60px;width:248px;"><?php echo $requirements;?></textarea><br>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('issue_date');?></td>
                                <td>
                                    <input class="easyui-datebox" <?php echo in_array("issue_date",$G_userBscEdit) ? '' : 'disabled'; ?>  name="issue_date" id="issue_date"  style="width:234px;" value="<?php echo $issue_date;?>"/>

                                    <a href="javascript:void(0);" class="easyui-tooltip" data-options="
                                        content: $('<div></div>'),
                                        onShow: function(){
                                            $(this).tooltip('arrow').css('left', 20);
                                            $(this).tooltip('tip').css('left', $(this).offset().left);
                                        },
                                        onUpdate: function(cc){
                                            cc.panel({
                                                width: 300,
                                                height: 'auto',
                                                border: false,
                                                href: '/bsc_help_content/help/issue_date'
                                            });
                                        }
                                    "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>


                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    --------------------------------------------------------------------------
                                </td>
                            </tr>
                            <script>
                                //用户选择框的选择事件
                                function userListSelect(rec){
                                    var input = $(this);
                                    var opt = input.combobox('options');
                                    var userListKey = opt.userListKey;
                                    var group_key = userListKey + '_group';
                                    //防止下拉框没值时报错
                                    if(rec === undefined){
                                        return false;
                                    }
                                    $('#' + group_key).val(rec.group);
                                    var userListData = {};

                                    function getUserAjax(url, data_field) {
                                        return $.ajax({
                                            type:'GET',
                                            url:url,
                                            dataType:'json',
                                            async: true,
                                            success:function (res) {
                                                userListData[data_field] = res;
                                            },
                                            error:function () {
                                                // $.messager.alert('获取数据错误');
                                            }
                                        });
                                    }

                                    if(userListKey == 'sales'){
                                        var sales_id = rec.id;

                                        //如果是销售,选择同时还刷新其他的下拉框
                                        var client_code = $('#client_code').textbox('getValue');

                                        //下面加载了 操作和客服的数据
                                        var a1 = getUserAjax('/bsc_user/get_client_user?client_code=' + client_code + '&role=operator&sales_id=' + sales_id, 'operator');
                                        var a2 = getUserAjax('/bsc_user/get_client_user?client_code=' + client_code + '&role=customer_service&sales_id=' + sales_id, 'customer_service');
                                        //2022-07-07 由于agent加入了shipment里,这里需要加一个选择销售时,自动重新加载agent
                                        // $('#agent_company').combogrid('grid').datagrid('reload', '/biz_client/get_agent?sales_ids=' + rec.id);

                                        $.when(a1,a2).done(function () {
                                            //首先给操作框加载数据,然后给客服框
                                            //检测当前这2个框的值,是否存在于加载的数据中,如果不存在,那么
                                            var this_val = $('#operator_id').combobox('getValue');
                                            var this_data = userListData.operator;

                                            var filter_data = this_data.filter(el => el['id'] === this_val);
                                            if(filter_data.length == 0){
                                                $('#operator_id').combobox('select', 0);
                                            }

                                            var this_val = $('#customer_service_id').combobox('getValue');
                                            var this_data = userListData.customer_service;

                                            var filter_data = this_data.filter(el => el['id'] === this_val);
                                            if(filter_data.length == 0){
                                                $('#customer_service_id').combobox('select', 0);
                                            }

                                            $('#operator_id').combobox('loadData', userListData.operator);
                                            $('#customer_service_id').combobox('loadData', userListData.customer_service);
                                        });

                                    }
                                }

                                //用户选择框的加载事件,目前只有 数据只有2条时, 自动选择才会用到 后面可能会并到select事件中
                                function userListOnLoadSuccess(data) {
                                    var input = $(this);
                                    var opt = input.combobox('options');
                                    var userListKey = opt.userListKey;
                                    var group_key = userListKey + '_group';

                                    is_load_user++;
                                    //2021-11-03 12:00防止进入页面时,没有选择客服那些的,自动变成唯一的客服
                                    if(is_load_user <= 3){
                                        return false;
                                    }
                                    var val = $(this).combobox('getValue');
                                    if(val == 0 && data.length == 2){
                                        // if(userListKey == 'sales') {
                                        //     var client_code = $('#client_code').textbox('getValue');
                                        //     reload_user_by_sales(client_code, data[1].id);
                                        // }
                                        $(this).combobox('select', data[1].id);
                                        //2022-07-07由于agent的上限,这里必须改为select触发上面的事件才行
                                        // $('#' + group_key).val(data[1].group);
                                    }
                                }
                            </script>
                            <?php
                            foreach($userList as $key => $value){
                                $group_key = $key . '_group';
                                if($key == 'oversea_cus')continue;
                                ?>
                                <tr>
                                    <td><?php echo lang($key); ?></td>
                                    <td>
                                        <select class="easyui-combobox userList" <?php echo in_array($key,$G_userBscEdit) ? '' : 'disabled'; ?> id="<?php echo $key;?>_id" editable="false" name="<?php echo $key;?>_id" style="width:200px;" data-options="
        									valueField:'id',
        									textField:'name',
        									userListKey: '<?= $key;?>',
        									url:'/bsc_user/get_client_user?client_code=<?= $client_code;?>&role=<?php echo $key; ?>&sales_id=<?= $sales_id;?>',
        									value:'<?php echo $value;?>',
        									onSelect: userListSelect,
        								    onLoadSuccess:userListOnLoadSuccess,
									    ">
                                        </select>

                                        <input type="hidden" <?php echo in_array($key,$G_userBscEdit)  ? '' : 'disabled'; ?> id="<?php echo $key;?>_group" name="<?php echo $key;?>_group" style="width:255px;" value="<?php echo $$group_key;?>">
                                        <a href="javascript:void(0);" onclick="user_info('<?= $key;?>_id')" title='查看该用户详情'><?= getUserName($value);?></a>
                                        <?php if($key == 'operator' || $key == 'customer_service'){ ?>
                                            <a href="javascript:void(0);" onclick="tb_duty_user('<?= $key;?>')" title='同步人员'><?= lang('to Consol');?></a>
                                        <?php } ?>
                                </tr>
                                <?php
                            }
                            ?>
                        </table>
                        <br><br>
                        <?= lang('创建时间');?>： <?php echo $created_time;?> <?= lang('更新时间');?>： <?php echo $updated_time;?>
                    </div>


                    <div style="float: left;width: 500px;">
                        <input type="hidden" <?php echo in_array("dangergoods_id",$G_userBscEdit) ? '' : 'disabled'; ?> id="dangergoods_id" name="dangergoods_id" value="<?= $dangergoods_id;?>">
                        <table width="500" border="0" cellspacing="0">
                            <tr>
                                <td><?= lang('update_description');?> </td>
                                <td>
                                    <textarea <?php echo in_array("description",$G_userBscEdit) ? '' : 'disabled'; ?> name="description" id="description" style="height:60px;width:245px;"/><?php echo $description;?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('description_cn');?> </td>
                                <td>
                                    <textarea <?php echo in_array("description_cn",$G_userBscEdit) ? '' : 'disabled'; ?> name="description_cn" id="description_cn" style="height:60px;width:245px;"/><?php echo $description_cn;?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_mark_nums');?> </td>
                                <td>
                                    <textarea <?php echo in_array("mark_nums",$G_userBscEdit) ? '' : 'disabled'; ?> name="mark_nums" id="mark_nums" style="height:60px;width:245px;"/><?php echo $mark_nums;?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('remark'); ?> </td>
                                <td>
                                    <textarea <?php echo in_array("remark",$G_userBscEdit) ? '' : 'disabled'; ?> name="remark" id="remark" style="height:60px;width:245px;"/><?php echo $remark; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('document') . lang('remark');?></td>
                                <td>
                                    <textarea name="document_remark" <?php echo in_array("document_remark",$G_userBscEdit) ? '' : 'disabled'; ?> id="document_remark" style="height:60px;width:248px;"><?php echo $document_remark;?></textarea>
                                </td>
                            </tr>
                            <!--<tr>
                                <td><?/*= lang('good_describe');*/?> </td>
                                <td>
                                    <textarea <?php /*echo in_array("good_describe",$G_userBscEdit) ? '' : 'disabled'; */?> name="good_describe" id="good_describe" style="height:60px;width:245px;"/><?php /*echo $good_describe;*/?></textarea>
                                </td>
                            </tr>-->
                            <!--<tr>
                                <td><?= lang('good_commodity');?> </td>
                                <td>
                                    <textarea <?php echo in_array("good_commodity",$G_userBscEdit) ? '' : 'disabled'; ?> name="good_commodity" id="good_commodity" style="height:60px;width:245px;"/><?php echo $good_commodity;?></textarea>
                                </td>
                            </tr>-->
                            <tr>
                                <td><?= lang('货物件数');?> </td>
                                <td>
                                    <input class="easyui-textbox" <?php echo in_array("good_outers",$G_userBscEdit) ? '' : 'disabled'; ?> name="good_outers" id="good_outers"  style="width:100px;" value="<?php echo $good_outers;?>"/>
                                    <select name="good_outers_unit" <?php echo in_array("good_outers_unit",$G_userBscEdit) ? '' : 'disabled'; ?> id="good_outers_unit"  class="easyui-combobox" style="width:155px;"data-options="
                                        valueField:'name',
                                        textField:'name',
                                        value:'<?= $good_outers_unit;?>',
                                        url: '/bsc_dict/get_option/packing_unit',
                                        onHidePanel: function() {
                                            var valueField = $(this).combobox('options').valueField;
                                            var val = $(this).combobox('getValue');
                                            var allData = $(this).combobox('getData');
                                            var result = true;
                                            for (var i = 0; i < allData.length; i++) {
                                                if (val == allData[i][valueField]) {
                                                    result = false;
                                                }
                                            }
                                            if (result) {
                                                $(this).combobox('clear');
                                            }
                                        },
                                        onSelect: function (data) {
                                            var good_outers = $('#good_outers').val();
                                            var all = $('#good_outers_unit').combobox('getData');
                                            var name1 = data.name;
                                            var name2 = data.name;
                                            $.each(all,function(index,item){
                                                   if(data.value == item.value && data.name != item.name){
                                                        name2 = item.name;
                                                   }
                                            })
                                            var len1 = name1.length;
                                            var len2 = name2.length;
                                            if(good_outers > 1){
                                                if(len2>len1){
                                                    $('#good_outers_unit').combobox('setValue', name2);
                                                }
                                            }else{
                                                if(len2<len1){
                                                    $('#good_outers_unit').combobox('setValue', name2);
                                                }
                                            }
                                        }
                                    ">
                                    </select>
                                    <span class="container_sum"><?= $container_sum['packs'];?></span>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('货物重量');?> </td>
                                <td>
                                    <input class="easyui-textbox" <?php echo in_array("good_weight",$G_userBscEdit) ? '' : 'disabled'; ?> name="good_weight" id="good_weight"  style="width:100px;" value="<?php echo $good_weight;?>"/>
                                    <select readonly name="good_weight_unit" <?php echo in_array("good_weight_unit",$G_userBscEdit) ? '' : 'disabled'; ?> id="good_weight_unit" class="easyui-combobox" style="width:155px;" data-options="
                                        value:'<?= $good_weight_unit;?>',
                                    ">
                                        <option value="KGS">KGS</option>
                                    </select>
                                    <span class="container_sum"><?= $container_sum['weight'];?></span>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('货物体积');?> </td>
                                <td id="warning">
                                    <input <?php echo in_array("good_volume",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-textbox"  name="good_volume" id="good_volume"  style="width:100px;" value="<?php echo $good_volume;?>"/>
                                    <select readonly <?php echo in_array("good_volume_unit",$G_userBscEdit) ? '' : 'disabled'; ?> name="good_volume_unit"  id="good_volume_unit" class="easyui-combobox" style="width:155px;" data-options="
                                        value:'<?= $good_volume_unit;?>',
                                    ">
                                        <option value="CBM">CBM</option>
                                    </select>
                                    <span class="container_sum"><?= $container_sum['volume'];?></span>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('货物类型');?></td>
                                <td>
                                    <select <?php echo in_array("dangergoods_id",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-combobox" name="goods_type" id="goods_type" style="width: 255px;" data-options="
                                    valueField:'value',
                                    textField:'valuename',
                                    url:'/bsc_dict/get_option/goods_type',
                                    value: '<?= $goods_type;?>',
                                    onSelect:function(rec){
                                        dangerous_info(rec.value);
                                    },
                                ">

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('hs_code');?> </td>
                                <td>
                                    <input <?php echo in_array("hs_code",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-textbox"  name="hs_code" id="hs_code"  style="width:255px;" value="<?php echo $hs_code;?>" prompt="<?= lang('Please separate them with commas');?>"/>
                                    <span id="hs_code_tip" style="color: red"></span>
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('isdangerous');?></td>
                                <td>
                                    <input class="easyui-textbox" id="isdangerous" name="isdangerous" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['isdangerous'];?>">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('ism_spec');?></td>
                                <td>
                                    <input class="easyui-textbox" id="ism_spec" name="ism_spec" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['ism_spec'];?>">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('UN_no');?></td>
                                <td>
                                    <input class="easyui-textbox" id="UN_no" name="UN_no" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['UN_no'];?>">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('IMDG_page');?></td>
                                <td>
                                    <input class="easyui-textbox" id="IMDG_page" name="IMDG_page" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['IMDG_page'];?>">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('packing_type');?></td>
                                <td>
                                    <input class="easyui-textbox" id="packing_type" name="packing_type" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['packing_type'];?>">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('flash_point');?></td>
                                <td>
                                    <input class="easyui-textbox" id="flash_point" name="flash_point" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['flash_point'];?>">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('IMDG_code');?></td>
                                <td>
                                    <input class="easyui-textbox" id="IMDG_code" name="IMDG_code" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['IMDG_code'];?>">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('is_pollution_sea');?></td>
                                <td>
                                    <input class="easyui-textbox" id="is_pollution_sea" name="is_pollution_sea" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['is_pollution_sea'];?>">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('ems_no');?></td>
                                <td>
                                    <input class="easyui-textbox" id="ems_no" name="ems_no" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['ems_no'];?>">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('MFAG_NO');?></td>
                                <td>
                                    <input class="easyui-textbox" id="MFAG_NO" name="MFAG_NO" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['MFAG_NO'];?>">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('e_contact_name');?></td>
                                <td>
                                    <input class="easyui-textbox" id="e_contact_name" name="e_contact_name" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['e_contact_name'];?>">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('e_contact_tel');?></td>
                                <td>
                                    <input class="easyui-textbox" id="e_contact_tel" name="e_contact_tel" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['e_contact_tel'];?>">
                                </td>
                            </tr>
                            <tr class="special_size">
                                <td><?= lang('long');?></td>
                                <td>
                                    <input class="easyui-textbox" id="long" name="long" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['long'];?>">
                                </td>
                            </tr>
                            <tr class="special_size">
                                <td><?= lang('wide');?></td>
                                <td>
                                    <input class="easyui-textbox" id="wide" name="wide" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['wide'];?>">
                                </td>
                            </tr>
                            <tr class="special_size">
                                <td><?= lang('high');?></td>
                                <td>
                                    <input class="easyui-textbox" id="high" name="high" style="width: 255px;" value="<?= empty($dangergoods) ? '' : $dangergoods['high'];?>">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br>

                </div>
            </div>
        </form>
    </div>
    <div title="Billing" style="padding:10px">
        <iframe scrolling="no" frameborder="0" id="billing_sub"  style="width:98%;height: 99%;"></iframe>
    </div>
    <div title="Container" style="padding:10px">
        <iframe scrolling="auto" frameborder="0" id="container_sub" style="width:100%;height:600px;"></iframe>
    </div>
    <div title="Upload files" style="padding:10px">
        <iframe scrolling="auto" frameborder="0" id="upload_files_sub" style="width:100%;height:600px;"></iframe>
    </div>
    <div title="Truck" style="padding:10px">
        <iframe scrolling="auto" frameborder="0" id="truck_sub" style="width:100%;height:600px;"></iframe>
    </div>
    <?php if(menu_role('shipment_tracking')){?>
    <div title="Tracking" style="padding:10px">
        <a href='/api/china_op/<?php echo $cus_no;?>/<?php echo $id;?>' target="_blank">一键从china op系统获取离泊前数据</a>
        <iframe scrolling="auto" frameborder="0" id="tracking_sub" style="width:100%;height:600px;"></iframe>
    </div>
    <?php } ?>
    <div title="Warehouse" style="padding:10px">
        <div id="warehouse_tab" class="easyui-accordion" data-options="multiple:true" style="width:340px;">
            <div title="Warehouse" style="padding:10px;">
                <select <?php echo in_array("warehouse_code",$G_userBscEdit) ? '' : 'disabled'; ?>  id="warehouse_code" class="easyui-combobox"  editable="true" name="warehouse_code" style="width:255px;" data-options="
                        valueField:'client_code',
                        textField:'company_name',
                        url:'/biz_client/get_option/warehouse',
                        value:'<?php echo $warehouse_code;?>',
                        onSelect:function(rec){
                            $('#warehouse_contact').combobox('reload', '/biz_client_contact/get_option/' + rec.client_code).combobox('setValue', rec.contact_name);
							$('#warehouse_telephone').val(rec.contact_telephone);
							$('#warehouse_address').val(rec.company_address);
                        },
                        ">
                </select>
                <br>
                <?= lang('warehouse_no');?>:<br>
                <input class="easyui-textbox" <?php echo in_array("warehouse_no",$G_userBscEdit) ? '' : 'disabled'; ?> name="warehouse_no" id="warehouse_no"  style="width:300px;" value="<?php echo $warehouse_no;?>"><br>
                <?= lang('warehouse_contact');?>:<br>
                <select <?php echo in_array("warehouse_contact",$G_userBscEdit) ? '' : 'disabled'; ?> name="warehouse_contact" id="warehouse_contact" class="easyui-combobox" style="width:300px;" data-options="
                        valueField:'name',
                        textField:'name',
                        url:'',
                        value:'<?= es_encode($warehouse_contact);?>',
                        onSelect:function(rec){
                            $('#warehouse_telephone').val(rec.contact_telephone);
                        }
                    ">

                </select><br>
                <?= lang('warehouse_telephone');?>:<br>
                <textarea <?php echo in_array("warehouse_telephone",$G_userBscEdit) ? '' : 'disabled'; ?> name="warehouse_telephone" id="warehouse_telephone" style="height:40px;width:300px;"><?php echo $warehouse_telephone;?></textarea>
                <br>
                <?= lang('warehouse_address');?>:<br>
                <textarea <?php echo in_array("warehouse_address",$G_userBscEdit) ? '' : 'disabled'; ?> name="warehouse_address" id="warehouse_address" style="height:60px;width:300px;"><?php echo $warehouse_address;?></textarea><br>
                <?= lang('warehouse_request');?> <span style="color:red;">(显示在进仓通知中)</span>:<br>
                <textarea <?php echo in_array("warehouse_request",$G_userBscEdit) ? '' : 'disabled'; ?> name="warehouse_request" id="warehouse_request"  style="height:60px;width:300px;" ><?php echo $warehouse_request;?></textarea><br>
                <?= lang('warehouse_charge');?><span style="color:red;">(仅供内部记录)</span>:<br>
                <textarea <?php echo in_array("warehouse_charge",$G_userBscEdit) ? '' : 'disabled'; ?> name="warehouse_charge" id="warehouse_charge"  style="height:40px;width:300px;" ><?php echo $warehouse_charge;?></textarea><br>
                <?= lang('warehouse_in_date');?>:
                <input class="easyui-datetimebox" <?php echo in_array("warehouse_in_date",$G_userBscEdit) ? '' : 'disabled'; ?> name="warehouse_in_date" id="warehouse_in_date"  style="width:249px;" value="<?php echo $warehouse_in_date;?>"><br>
                <?= lang('warehouse_in_num');?>:
                <input <?php echo in_array("warehouse_in_num",$G_userBscEdit) ? '' : 'disabled'; ?> class="easyui-textbox"  name="warehouse_in_num" id="warehouse_in_num"  style="width:249px;" value="<?php echo $warehouse_in_num;?>"/><br>

            </div>
        </div>
    </div>
    <div title="Download" style="padding:10px">
        <iframe scrolling="auto" frameborder="0" id="Download_sub" style="width:100%;height:700px;"></iframe>
    </div>
    <div title="POD Service" style="padding:10px">
        <div id="POD_service_tab" class="easyui-accordion" data-options="multiple:true" style="width:340px;">
            <div title="POD Trucking" style="padding:10px;">
                <table>
                    <tr>
                        <td><?= lang('目的港提货地址');?></td>
                        <td>
                            <textarea <?php echo in_array("INCO_address",$G_userBscEdit) ? '' : 'disabled'; ?> name="INCO_address" id="INCO_address" cols="30" style="height: 60px;width: 248px;"><?= $INCO_address;?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div title="POL Service" style="padding:10px">
        <div id="POL_service_tab" class="easyui-accordion" data-options="multiple:true" style="width:340px;">
            <div title="POL Trucking" style="padding:10px;">
                <table>
                    <tr>
                        <td><?= lang('目的港送货地址');?></td>
                        <td>
                            <textarea <?php echo in_array("INCO_address",$G_userBscEdit) ? '' : 'disabled'; ?> name="INCO_address" id="INCO_address" cols="30" style="height: 60px;width: 248px;"><?= $INCO_address;?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div title="EDI" style="padding:10px">
        <iframe scrolling="auto" frameborder="0" id="edi" style="width:100%;height:600px;"></iframe>
    </div>
    <?php if($child_shipment > 0){?>
        <div title="Sub shipment" style="padding:10px">
            <iframe scrolling="auto" frameborder="0" id="sub_shipment" style="width:100%;height:600px;"></iframe>
        </div>
    <?php }?>
    <?php if(menu_role('shipment_log')){?>
        <div title="Log" style="padding:10px">
            <iframe scrolling="auto" frameborder="0" id="log" style="width:100%;height:600px;"></iframe>
        </div>
    <?php }?>
</div>

<div id="apply_consol_div" class="easyui-window" title="apply" style="padding:5px;" data-options="closed:true,modal:true,border:'thin'">
    <form id="apply_consol_form" method="post">
        <input type="hidden" name="id" value="<?= $id;?>">
        <table>
            <tr>
                <td><?= lang('consol_id_apply');?></td>
                <td>
                    <select id="consol_id_apply" name="consol_id_apply" class="easyui-combogrid" style="width:255px;">
                    </select>
                    <a href="javascript:void(0);" class="easyui-tooltip" data-options="
                        content: $('<div></div>'),
                        onShow: function(){
                            $(this).tooltip('arrow').css('left', 20);
                            $(this).tooltip('tip').css('left', $(this).offset().left);
                        },
                        onUpdate: function(cc){
                            cc.panel({
                                width: 300,
                                height: 'auto',
                                border: false,
                                href: '/bsc_help_content/help/apply_consol'
                            });
                        }
                    "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                </td>
            </tr>
        </table>
        <div>
            <button type="button" onclick="javascript:save_apply_consol();" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px"><?= lang('submit');?></button>
            <button type="button" onclick="javascript:$('#apply_consol_div').window('close')" class="easyui-linkbutton" iconCls="icon-cancel" style="width:90px"><?= lang('cancel');?></button>
        </div>
    </form>
</div>


<div id="gcenter" class="easyui-window" title="Win"  closed="true" style="width:1020px;height:530px;padding:5px;" data-options="
				iconCls:'icon-sum',right:'1px',top:'110px'">
    <iframe scrolling="auto" frameborder="0" id="tanchuang" style="width:100%;height:100%;"> </iframe>
</div>
<div id="basic_edit_window" class="easyui-window" title="Edit"  closed="true" data-options="top:100,modal:true" style="width:1120px;height:830px;padding:5px;" >
	<iframe scrolling="auto" frameborder="0" id="basic_edit_iframe" style="width:100%;height:100%;"> </iframe>
</div>
<div id="shipment_must_field_window" class="easyui-window" title="shipment_must_field"  closed="true" data-options="modal:true" style="width:1120px;height:430px;padding:5px;" >
	<iframe scrolling="auto" frameborder="0" id="shipment_must_field_iframe" style="width:100%;height:100%;"> </iframe>
</div>
<div id="email_window" closed="true" class="easyui-window" title="email verify" style="width: 1100px;height: 400px;">
	<iframe scrolling="auto" id='email_iframe' frameborder="0"  src="" style="width:100%;height:98%;"></iframe>
</div>
<script>
	function verify_email(type=1,id=0){
		$('#email_iframe').attr('src','/mail_box/send_by_template?template_id=48&id='+id+'&mail_type=biz_client_contact_list')
		$('#email_window').window('open')
	}
	function edit_basic(){
		$('#basic_edit_iframe').attr('src','/biz_shipment/basic_sft/<?=$id?>')
		$('#basic_edit_window').window('open')
	}
</script>
<div id="trans_origin_div">
    <form id="trans_origin_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_origin_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_origin_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_origin_click" iconCls="icon-search" onclick="query_report('trans_origin', 'trans_origin')"><?= lang('search');?></a>
        </div>
    </form>
</div>
<div id="trans_discharge_div">
    <form id="trans_discharge_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_discharge_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_discharge_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_discharge_click" iconCls="icon-search" onclick="query_report('trans_discharge', 'trans_discharge')"><?= lang('search');?></a>
        </div>
    </form>
</div>
<div id="trans_destination_div">
    <form id="trans_destination_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <label><?= lang('码头');?>:</label><input name="terminal" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_destination_click" iconCls="icon-search" onclick="query_report('trans_destination', 'trans_destination')"><?= lang('search');?></a>
        </div>
    </form>
</div>
</body>
