<style>
    .box{
        width: 100%;
        height: 100vh;
    }
    .user_role_tree{
        width: 100%;
        height: 100%;
    }
    .menu_tree{
        width: 15%;
        height:100%;
    }
    .other_info{
        width: 40%;
        height: 100%;
    }
    .read_role_tree{
        width: 15%;
        height:100%;
    }
    .edit_role_tree{
        width: 15%;
        height:100%;
    }
    .user_role_info{
        width: 80%;
        height: 100%;
        display: flex;
        flex-direction: column;
    }
    .user_role_info_top{
        width: 100%;
        height: 32px;
    }
    .user_role_info_bottom{
        width: 100%;
        display: flex;
        flex-direction: row;
    }
    .user_role_info_bottom>div{
        border-right: 1px solid black;
        height: 100%;
    }
    .menu_tree{
        width: 30%;
    }
    .auth_tree{
        width: 15%;
    }
    .read_consol_tree{
        height: 100%;
    }
    .title{
        width:100%;
        height: 30px;
        /*background-color: #6699CC;*/
        text-align: left;
        line-height: 30px;
        font-weight: bold;
        font-size: 15px;
        border: 1px solid;
    }
    .hide{
        display: none;
    }
</style>
<script>

    var is_update = false;

    window.onbeforeunload = function (e) {
        // 兼容IE8和Firefox 4之前的版本
        //检测是否有字段变动
        e = e || window.event;
        var dialogText = '您有未保存的内容,是否确认退出';
        if(is_update){
            if (e) {
                e.returnValue = dialogText;
            }
            // Chrome, Safari, Firefox 4+, Opera 12+ , IE 9+
            return dialogText;
        }
    };
    /**
     * 载入表单的内容(由于不是表单控件,这里变成了纯树,所以数据那些需要重新封装)
     */
    function load_form(){
        is_load = true;
        set_tree_data('menu_tt', '<?= $menu;?>');

        is_load = false;
    }

    function add(type = 0) {
        clear_form();
        $('#user_role').textbox('enable');
        //赋值PID
        if(type === 1){
            $('#pid').val(selected_node.id);
        }

    }

    function clear_form() {
        load_form({});
    }

    var is_load = false;

    /**
     * 保存树的数据
     */
    function set_tree_data(id, data) {
        if(typeof data == 'string'){
            data = data.split(',');
        }
        //首先获取树全部节点
        var inp = $('#' + id);
        var tree_data = inp.tree('getRoots');
        function each_check(array, values){
            //遍历后,如果当前节点的ID存在于data中,那么select
            $.each(array, function (i, it) {
                var node = inp.tree('find', it.id);
                if(it.children != undefined){
                    if($.inArray(it.id, values) !== -1) inp.tree('check', node.target);
                    else inp.tree('uncheck', node.target);
                    each_check(it.children, values);
                }else{

                    if($.inArray(it.id, values) !== -1) {

                        if(it.id == 9) console.log(node);
                        if(id == 'menu_tt')check_node_check_parent(id, node);
                        else inp.tree('check', node.target);
                    }
                    else inp.tree('uncheck', node.target);
                }
            });
        }
        //遍历后,如果当前节点的ID存在于data中,那么select
        each_check(tree_data, data);
    }

    /**
     * 选中节点的时候，把父节点一起选中
     */
    function check_node_check_parent(id, node){
        var inp = $('#' + id);
        if(node == null) return false;
        inp.tree('check', node.target);

        //获取父节点
        var parent = inp.tree('getParent', node.target);
        check_node_check_parent(id, parent);
    }

    /**
     * 选中节点时候把子节点全部选中
     */
    function check_node_check_chilrent(id, node){
        var inp = $('#' + id);
        if(node == null) return false;

        inp.tree('check', node.target);

        if(node.children != undefined){
            $.each(node.children, function(i, it){
                var this_node = inp.tree('find', it.id);
                check_node_check_chilrent(id, this_node);
            });
        }
    }

    /**
     * 如果有子节点，且当前节点取消，那么取消所有子节点
     */
    function uncheck_node_uncheck_chilren(id, node){
        var inp = $('#' + id);
        if(node == null) return false;
        inp.tree('uncheck', node.target);
        if(node.children == undefined) return false;

        $.each(node.children, function(i, it){
            var this_node = inp.tree('find', it.id);
            uncheck_node_uncheck_chilren(id, this_node);
        });
    }

    /**
     * 选中节点时,只选中父节点,不触发全选子节点
     */
    function check_node_only_check_parent(id, node) {
        var inp = $('#' + id);
        if(node == null) return false;
        inp.tree('check', node.target);

        var parent = inp.tree('getParent', node.target);
        //判断是否有父节点
        check_node_only_check_parent(id, parent);
    }

    var check_chilrent = true;//这个是用于不想批量选中子的时候使用
    /**
     * 目录表的选中事件
     */
    function checked_menu(node, checked) {
        if(is_load) return true;

        var inp = $('#menu_tt');
        is_update = true;
        if(checked){
            if(node.children != undefined && check_chilrent){
                check_node_check_chilrent('menu_tt', node);
                return true;
            }
            var parent = inp.tree('getParent', node.target);
            if(parent != null){
                check_chilrent = false;
                check_node_only_check_parent('menu_tt', parent);
                check_chilrent = true;
                return true;
            }
        }else{
            //如果有子节点，且当前节点取消，那么取消所有子节点
            if(node.children != undefined){
                uncheck_node_uncheck_chilren('menu_tt', node);
            }
        }
    }

    //return an array of values that match on a certain key
    function getValues(obj, key) {
        var objects = [];
        for (var i in obj) {
            if (!obj.hasOwnProperty(i)) continue;
            if (typeof obj[i] == 'object') {
                objects = objects.concat(getValues(obj[i], key));
            } else if (i == key) {
                objects.push(obj[i]);
            }
        }
        return objects;
    }


    /**
     * 提交表单的数据
     */
    function submit_form(){
        $('#save_form').form('submit', {
            url: '/bsc_menu/save_data',
            onSubmit: function (param) {
                //需要特殊处理的值
                var menu_val = $('#menu_tt').tree('getChecked');
                var menu = [];
                $.each(menu_val, function (i, it) {
                    menu.push(it.id);
                });
                param.menu = menu.join(',');
                return $(this).form('validate');
            },
            success: function (res_json) {
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                is_update = false;
                if(res === undefined){
                    $.messager.alert('Tips',res_json);
                    return false;
                }
                // $('#tt').tree('reload');
                $.messager.alert('Tips', res.msg);
                if (res.code == 0) {

                } else {
                }
            }
        });
    }

    var selected_node = {};

    $(function () {
        $('#menu_tt').tree({
            url:'/bsc_menu/get_tree',
            dnd:true,
            checkbox:true,
            cascadeCheck: false,
            formatter:function(node){
                var str = '';
                if(node.display == 1){
                    str = node.name;
                }else{
                    str = '<span style="color: #666666;">' + node.name + '</span>';
                }
                if(node.is_menu == 1){
                    str += '<span style="color: red;">*</span>';
                }
                return str;
            },
            onCheck: function(node, checked){
                //勾选父的时候，子全部选中
                checked_menu(node, checked);
            },
            onLoadSuccess:function () {
                load_form();
            }
        });
    });
</script>
<body>
<div id="cc" class="easyui-layout box">
    <div class="user_role_info" data-options="region:'center',title:'<?= lang('用户菜单设置'); ?>',tools:'#edit_btn'">
        <div class="user_role_info_top user_role_form hide">
            <form id="save_form" method="post">
                <input type="hidden" name="id" id="id" value="<?= $id;?>">
                <input type="hidden" name="id_type" id="id_type" value="bsc_user">
                <!--                    <input type="hidden" name="level" id="level">-->
                <input type="hidden" name="pid" id="pid">
                <table>
                    <tr>
                        <td><?= lang('role');?>:</td>
                        <td>
                            <input name="user_role" id="user_role" disabled class="easyui-textbox" style="width: 90px;" data-options="required: true,">
                        </td>
                        <td>
                            <a class="easyui-linkbutton" onclick="submit_form()" style="width:90px"><?= lang('save');?></a>
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="user_role_info_bottom">
            <!--目录树-->
            <div class="menu_tree tree_div">
                <div class="title">
                    <a class="easyui-linkbutton" onclick="submit_form()" style="width:90px"><?= lang('保存');?></a>
                </div>
                <ul id="menu_tt"></ul>
            </div>
        </div>
    </div>
</div>

<!--    右键菜单定义如下：-->
<div id="mm" class="easyui-menu" style="width:120px;">
    <div onclick="add(1)" data-options="iconCls:'icon-add'">新增节点</div>
</div>
</body>