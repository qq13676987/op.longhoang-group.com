<?php
class bsc_template_model extends CI_Model
{
    private $table = 'bsc_template';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_one($f = "", $v = "")
    {
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_where_one($where='', $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
//        return $this->db->last_query();
    }

    //$group为数组则查询多个
    public function get_group($where = '',$group = '', $field = ''){
        if($where != '') $this->db->where($where);
        if($field == ''){
            $field = is_array($group) ? join(',', $group) : $group;
        }
        $this->db->select($field);
        $this->db->group_by($group);
        $query = $this->db->get($this->table);
        return $query->result_array();
//        return $this->db->last_query();
    }

    public function total($where = "")
    {
        if ($where != "") $this->db->where($where);
        $this->db->from($this->table);

        return $this->db->count_all_results('');
    }

    public function save($data = array())
    {
        $data["create_time"] = date('Y-m-d H:i:s', time());
        $data["update_time"] = date('Y-m-d H:i:s', time());
        $data["create_by"] = $this->session->userdata('id');
        $data["update_by"] = $this->session->userdata('id');
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id = '', $data = array())
    {
        $data["update_time"] = date('Y-m-d H:i:s', time());
        $data["update_by"] = $this->session->userdata('id');
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id = '')
    {
        // delete container
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
}