<style>
    .isf_div{
        text-align: center;
        margin: auto;
        width: 800px;
    }
    table{
        border-collapse: collapse;
        border-spacing: 0;
        width: 800px;
    }
    tr td:nth-child(2){
        width: 350px;
    }
    td{
        border: 1px solid black;
    }
</style>
<body>
    <div class="isf_div">
        <h2>IMPORT SECURITY FILING ISF 10+2 Format</h2>
        <table border='0' cellpadding='4' cellspacing='1' >
            <tr>
                <td>1</td>
                <td>
                    Manufacturer or Supplier Name & Address （生产厂家的名字和地址）
                </td>
                <td>
                    <?= $message['manufacturer_name'];?><br />
                    <?= $message['manufacturer_address'];?>
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>
                    Seller/Exporter Name & Address （卖家的名字地址）
                </td>
                <td>
                    <?= $message['seller_name'];?><br />
                    <?= $message['seller_address'];?>
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>
                    Buyer/Importer Name & Address （买家的名字地址）
                </td>
                <td>
                    <?= $message['buyer_name'];?><br />
                    <?= $message['buyer_address'];?>
                </td>
            </tr>
            <tr>
                <td>4</td>
                <td>
                    Ship/Deliver to Name & Address 
        (Name & Address on Delivery Order if different from Importer)（清关后第一个收货点的名字和地址）
                </td>
                <td>
                    <?= $message['ship_name'];?><br />
                    <?= $message['ship_address'];?>
                </td>
            </tr>
            <tr>
                <td>5</td>
                <td>
                    Importer of Record/FTZ applicant identification number, Internal Revenue Service (IRS) number, Employer Identification Number (EIN), Social Security Number (SSN), or CBP assigned number.
        （进口商的登记号，可以是FTZ，IRS，EIN，SSN 或CBP NO）
                </td>
                <td>
                    <?= $message['importer_record'];?>
                </td>
            </tr>
            <tr>
                <td>6</td>
                <td>
                    Consignee numbers. IRS number, EIN, SSN, or CBP assigned number（收货人登记号，同上）
                </td>
                <td>
                    <?= $message['consignee_record'];?>
                </td>
            </tr>
            <tr>
                <td>7</td>
                <td>
                    Country Of Origin (Country of manufacture, production, or growth of the article) (货物原产地）
                </td>
                <td>
                    <?= $message['manufacturer_countryorigin'];?>
                </td>
            </tr>
            <tr>
                <td>8</td>
                <td>
                    HTS number (At 6 digit level) （商品编码HTS CODE前6位）
                </td>
                <td>
                    <?= $message['hts_code'];?>
                </td>
            </tr>
            <tr>
                <td>9</td>
                <td>
                    Container Stuffing Location:
                </td>
                <td>
                    <?= $message['container_stuff_name'];?><br />
                    <?= $message['container_stuff_address'];?>
                </td>
            </tr>
            <tr>
                <td>10</td>
                <td>
                    Consolidator Name & Address:
                </td>
                <td>
                    <?= $message['container_stuff_name'];?><br />
                    <?= $message['container_stuff_address'];?>
                </td>
            </tr>
            <tr>
                <td>11</td>
                <td>
                    Master Bill Of Lading Number:
                </td>
                <td>
                    <?= $message['mbl'];?>
                </td>
            </tr>
            <tr>
                <td>12</td>
                <td>
                    House Bill Of Lading Number:
                </td>
                <td>
                    <?= $message['hbl'];?>
                </td>
            </tr>
            <tr>
                <td>13</td>
                <td>
                    AMS #/SCAC Code
                </td>
                <td>
                    <?= $message['scac_code'];?>
                </td>
            </tr>
        </table>
        <br /> 
        Note: Above information must be provided 72 hours prior loading/unloading at origin Port or Importers are subject to $5000 fine and U.S. Custom Entry hold as result.<br />
    注意：请在开航前72小时提供.否则导致货物不能装船,不能卸下,目的港清关有问题等责任,只能有贵司自行承担并负责相关费用. <br />
        <hr>
        Print Name       /     Signature            /    Date 
    </div>
</body>

