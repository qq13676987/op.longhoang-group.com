<?php
   class Biz_client_contact_model extends CI_Model{
       private $table = 'biz_client_contact_list';

       public function __construct()
       {
           parent::__construct();
           $this->load->database();
       }

       public function get_one($f="",$v=""){
           $this->db->where($f, $v);
           $query = $this->db->get($this->table);
           return $query->row_array();
       }
       
       public function get_where_one($where = ""){
           if($where != '')$this->db->where($where);
           $query = $this->db->get($this->table);
           return $query->row_array();
       }
       
       public function get_option($client_code){
           $this->db->where('client_code', $client_code);
           $query = $this->db->get($this->table);
           return $query->result_array();
       }
       public function get($where="" ,$sort="id",$order="desc"){
           if($where !="")	$this->db->where($where, null, false);
           $this->db->order_by($sort,$order);
           $query = $this->db->get($this->table);
           return $query->result_array();
       }

       public function total($where=""){
           if($where !="")	$this->db->where($where);
           $this->db->from($this->table);
           return  $this->db->count_all_results('');
       }

       public function save($data=array()){
           $data["create_time"] = date('Y-m-d H:i:s', time());
           $data["update_time"] = date('Y-m-d H:i:s', time());
           $data["create_by"] =  $this->session->userdata('id');
           $data["update_by"] =  $this->session->userdata('id');
           $this->db->insert($this->table, $data);
           return $this->db->insert_id();
       }

       public function update($id='',$data=array()){
           $data["update_time"] = date('Y-m-d H:i:s', time());
           $data["update_by"] =  $this->session->userdata('id');
           $this->db->where('id', $id);
           $this->db->update($this->table, $data);
           return $id;
       }

       public function mdelete($id=''){
           // delete container
           $this->db->where('id', $id);
           $this->db->delete($this->table);
       }

        /**
         * 联系人权限,根据当前角色来,如果不是里面的相关人员,那么数据***处理
         */
        public function role($client_code = '', $role = ''){
            if(is_admin()) return true;
            return true;
            
            // //2022-03-17 如果是非活跃的,那么全部人开放
            // $sql = "select is_inactve from biz_client_inactve where client_code = '{$client_code}' and is_inactve = 0";
            // $temp = Model('m_model')->query_one($sql);
            // if(!empty($temp)) return true;
            
            
            $userRange = get_session('user_range');
            $userId = get_session('id');
            if(!isset($this->client)){
                Model('biz_client_model');
                // $this->db->select('id,created_by,client_code,operator_ids,customer_service_ids,sales_ids,marketing_ids,oversea_cus_ids,booking_ids,document_ids,finance_ids');
                $client = Model('biz_client_duty_model')->get_client_data_one($client_code);
                $client['duty'] = array();
                $client['client_code'] = $client_code;
                $this->client = $client;
            }else{
                $client = $this->client;
            }
            if(!isset($this->client['duty'][$role])){
                $this->client['duty'][$role] = Model('biz_client_duty_model')->get("client_code = '{$client['client_code']}' and client_role = '{$role}'");
            }else{
                $this->client['duty'][$role];
            }

            //2022-03-17 新版改为根据用户权限来
            // if(in_array($client['created_by'], $userRange)) return true;

            //根据当前客户的duty进行判断
            //整理出duty里的人员,判断当前是否有权限
            $duty_user_ids = filter_unique_array(array_column($this->client['duty'][$role], 'user_id'));

            //全开放的打开权限
            if(in_array('-1', $duty_user_ids)) return true;

            //没有交集代表无相关人员权限
            if(sizeof(array_intersect($userRange, $duty_user_ids)) > 0) return true;

            //其他任意情况不给查看
            return false;
        }
   }