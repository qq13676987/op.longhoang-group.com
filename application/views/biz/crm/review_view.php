<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="/inc/third/layui/css/layui.css">
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<title>Client_apply-VIEW</title>
<script type="text/javascript">
    $(function () {
        var load = layer.load(1, {offset: ['200px', '850px']});
        $('#search_client').on('load',function () {
            layer.close(load);
        })

        $('#tb').tabs({
            border: false,
            onSelect: function (title) {
                if (title == "联系人") {
                    if (document.getElementById("contact_list").src == "") {
                        document.getElementById("contact_list").src = "/biz_client_contact/index_crm?crm_id=<?= $id;?>";
                    }
                }
                if (title == "每日跟进记录") {
                    if (document.getElementById("follow_up_list").src == "") {
                        document.getElementById("follow_up_list").src = "/biz_client_follow_up_log/index/?crm_id=<?= $id;?>";
                    }
                }
            }
        });
    });
</script>
<style type="text/css">
    input:disabled, select:disabled, textarea:disabled {
        background-color: #d6d6d6;
        cursor: default;
    }
    .form_table {float: left;width: 520px;}
    .form_table tr td:first-child {padding-left: 15px; color: #023e62}
</style>
<body>
<div id="tb" class="easyui-tabs">
    <div title="Basic info" style="padding:1px; overflow: auto">
        <!--基础信息-->
        <div style="padding: 5px 20px 20px 20px; background: #f6f8f5; height: 710px; width: 470px; float: left; border-right:1px solid #eee;">
            <div data-options="region:'west',tools:'#tt'" title="Basic">
                <div style="display: inline-block;vertical-align: top">
                    <div class="layui-tab layui-tab-card" style="width: 470px; min-height: 500px; margin:0px;">
                        <ul class="layui-tab-title">
                            <li class="layui-this">基础资料</li>
                            <li>银行卡资料</li>
                        </ul>
                        <div class="layui-tab-content" style="background: white;">
                            <div class="layui-tab-item layui-show">
                                <table class="form_table" style="padding:10px; width: 450px; overflow:hidden;" border="0"
                                   cellspacing="0">
                                <tr style="height: 35px;">
                                    <td style="width: 80px;"><?= lang('country_code'); ?></td>
                                    <td>
                                        <select class="easyui-combobox" required name="country_code" id="country_code"
                                                style="width:300px; height: 30px;" data-options="
                                    value:'<?= $country_code; ?>',
                                    valueField:'value',
                                    textField:'namevalue',
                                    url:'/bsc_dict/get_option/country_code',
                                    onHidePanel: function() {
										var valueField = $(this).combobox('options').valueField;
										var val = $(this).combobox('getValue');
										var allData = $(this).combobox('getData');
										var result = true;
										for (var i = 0; i < allData.length; i++) {
											if (val == allData[i][valueField]) {
												result = false;
											}
										}
										if (result) {
											$(this).combobox('clear');
										}
								    },
                                ">
                                        </select>
                                    </td>
                                </tr>
                                <tr style="height: 35px;">
                                    <td><?= lang('company_name'); ?> </td>
                                    <td>
                                        <input class="easyui-textbox" readonly name="company_name" id="company_name"
                                               style="width:300px; height: 30px;" value="<?= $company_name; ?>"/>
                                    </td>
                                </tr>
                                <tr style="height: 35px;">
                                    <td><?= lang('client_name'); ?> </td>
                                    <td>
                                        <input class="easyui-textbox" name="client_name" id="client_name"
                                               data-options="required:true" style="width:300px; height: 30px;"
                                               value="<?= $client_name; ?>"/>
                                    </td>
                                </tr>
                                <tr style="height: 35px;">
                                    <td><?= lang('client area'); ?></td>
                                    <td>
                                        <select class="easyui-combobox" name="province" id="province" data-options="
                                valueField:'cityname',
                                textField:'cityname',
                                url:'/city/get_province',
                                value:'<?= $province; ?>',
                                onSelect: function(rec){
                                    if(rec != undefined){
                                        $('#city').combobox('reload', '/city/get_city/' + rec.id);
                                    }
                                }," style="width:100px; height: 30px;"></select>

                                        <select class="easyui-combobox" name="city" id="city"
                                                style="width:98px; height: 30px;"
                                                data-options="
                                    valueField:'cityname',
                                    textField:'cityname',
                                    value:'<?= $city; ?>',
                                    onSelect: function(rec){
                                        if(rec != undefined){
                                            $('#area').combobox('reload', '/city/get_area/' + rec.id);
                                        }
                                    },
                                    onLoadSuccess:function(){
                                        var this_data = $(this).combobox('getData');
                                        var this_val = $(this).combobox('getValue');
                                        var rec = this_data.filter(el => el['cityname'] === this_val);
                                        if(rec.length > 0)rec = rec[0];
                                        $('#area').combobox('reload', '/city/get_area/' + rec.id);
                                    }
                                "></select>

                                        <select class="easyui-combobox" name="area" id="area"
                                                style="width:95px; height: 30px;"
                                                data-options="
                                    valueField:'cityname',
                                    textField:'cityname',
                                    value:'<?= $area; ?>',
                                "></select>
                                    </td>
                                </tr>
                                <tr style="height: 35px;">
                                    <td><?= lang('company_address'); ?></td>
                                    <td>
                                <textarea style="width:295px; height: 80px;" class="textarea b" name="company_address"
                                          id="company_address"><?= $company_address ?></textarea>
                                    </td>
                                </tr>
                                <tr style="height: 35px;">
                                    <td><?= lang('export_sailing'); ?></td>
                                    <td>
                                        <select <?php echo in_array("export_sailing", $G_userBscEdit) ? '' : 'disabled'; ?>
                                                class="easyui-combobox" id="export_sailing1"
                                                style="width:300px; height: 30px;"
                                                data-options="
                                    editable:false,
                                    multiple:true,
                                    multivalue:false,
                                    valueField:'value',
                                    textField:'name',
                                    value:'<?php echo $export_sailing; ?>',
                                    url:'/bsc_dict/get_option/export_sailing',
                                    onChange:function(newValue,oldValue){
                                        $('#export_sailing').val(newValue.join(','));
                                    }
                                ">
                                        </select>
                                        <input type="hidden" name="export_sailing" id="export_sailing"
                                               value="<?php echo $export_sailing; ?>">
                                    </td>
                                </tr>
                                <tr style="height: 35px;">
                                    <td><?= lang('trans_mode'); ?></td>
                                    <td>
                                        <select <?php echo in_array("trans_mode", $G_userBscEdit) ? '' : 'disabled'; ?>
                                                class="easyui-combobox" id="trans_mode1"
                                                style="width:300px; height: 30px;"
                                                data-options="
                                    editable:false,
                                    multiple:true,
                                    multivalue:false,
                                    valueField:'value',
                                    textField:'name',
                                    value:'<?php echo $trans_mode; ?>',
                                    url:'/bsc_dict/get_option/client_trans_mode',
                                    onChange:function(newValue,oldValue){
                                        $('#trans_mode').val(newValue.join(','));
                                    }
                                ">
                                        </select>
                                        <input <?php echo in_array("trans_mode", $G_userBscEdit) ? '' : 'disabled'; ?>
                                                type="hidden" name="trans_mode" id="trans_mode"
                                                value="<?php echo $trans_mode; ?>">
                                    </td>
                                </tr>
                                <tr style="height: 35px;">
                                    <td><?= lang('product_type'); ?></td>
                                    <td>
                                        <select <?php echo in_array("product_type", $G_userBscEdit) ? '' : 'disabled'; ?>
                                                class="easyui-combobox" id="product_type1"
                                                style="width:300px; height: 30px;" data-options="
                                    editable:false,
                                    multiple:true,
                                    multivalue:false,
                                    valueField:'value',
                                    textField:'name',
                                    value:'<?php echo $product_type; ?>',
                                    url:'/bsc_dict/get_option/product_type',
                                    onChange:function(newValue,oldValue){
                                        $('#product_type').val(newValue.join(','));
                                    }
                                ">
                                        </select>
                                        <input <?php echo in_array("product_type", $G_userBscEdit) ? '' : 'disabled'; ?>
                                                type="hidden" name="product_type" id="product_type"
                                                value="<?php echo $product_type; ?>">
                                    </td>
                                </tr>
                                <tr style="height: 35px;">
                                    <td><?= lang('product_details'); ?></td>
                                    <td>
                                <textarea <?php echo in_array("product_details", $G_userBscEdit) ? '' : 'disabled'; ?> name="product_details"
                                                                                                                       id="product_details"
                                                                                                                       style="width:298px; height: 80px;"><?php echo $product_details; ?></textarea>
                                    </td>
                                </tr>
                                <tr style="height: 35px;">
                                    <td><?= lang('client_source'); ?></td>
                                    <td>
                                        <select <?php echo in_array("client_source", $G_userBscEdit) ? '' : 'disabled'; ?>
                                                class="easyui-combobox" id="client_source1"
                                                style="width:300px; height: 30px;" data-options="
                                        required:true,
                                    editable:false,
                                    multiple:true,
                                    multivalue:false,
                                    valueField:'value',
                                    textField:'name',
                                    value: '<?php echo $client_source; ?>',
                                    url:'/bsc_dict/get_option/client_source',
                                    onChange:function(newValue,oldValue){
                                        $('#client_source').val(newValue.join(','));
                                    }
                                ">
                                        </select>
                                        <input type="hidden" name="client_source" value="<?php echo $client_source; ?>"
                                               id="client_source">
                                    </td>
                                </tr>
                                <tr style="height: 35px;">
                                    <td><?= lang('role'); ?> </td>
                                    <td>
                                        <select id="role1"
                                                class="easyui-combobox" <?php echo in_array("role", $G_userBscEdit) ? '' : 'disabled'; ?>
                                                editable="false" name="role" style="width:300px; height: 30px;"
                                                data-options="required:true,
    								valueField:'value',
    								textField:'value',
    								url:'/bsc_dict/get_option/role?ext1=1',
    								value:'<?php echo $role; ?>',
								">
                                        </select>
                                        <a href="javascript:void;" class="easyui-tooltip" data-options="
                                    content: $('<div></div>'),
                                    onShow: function(){
                                        $(this).tooltip('arrow').css('left', 20);
                                        $(this).tooltip('tip').css('left', $(this).offset().left);
                                    },
                                    onUpdate: function(cc){
                                        cc.panel({
                                            width: 500,
                                            height: 'auto',
                                            border: false,
                                            href: '/bsc_help_content/help/role'
                                        });
                                    }
                                "><?= lang('help'); ?></a>
                                    </td>
                                </tr>
                                <tr style="height: 35px;">
                                    <td>审批人</td>
                                    <td>
                                        <select class="easyui-combobox" name="approve_user" readonly="true"
                                                id="approve_user" style="width:300px; height: 30px;" data-options="
                                                required:true,
                                                editable:false,
                                                valueField:'id',
                                                textField:'name',
                                                url:'/bsc_user/get_leader_user',
                                                value:'<?= $approve_user; ?>'
                                            ">
                                        </select>
                                    </td>
                                </tr>
                                <tr style="height: 35px;">
                                    <td><?= lang('sales'); ?></td>
                                    <td>
                                        <select class="easyui-combotree default_partner" id="sales_ids1" readonly="true"
                                                ids="sales_ids" name="sales_id" style="width:300px; height: 30px;"
                                                data-options="
                                    multiple:false,
                                    valueField:'id',
                                    textField:'text',
                                    cascadeCheck: false,
                                    url:'/bsc_user/get_user_tree/sales/true',
                                    value: '<?= $sales_id; ?>',
                                ">
                                        </select>
                                    </td>
                                </tr>
                                <tr style="height: 35px;">
                                    <td><?= lang('created_by'); ?></td>
                                    <td>
                                        <input class="easyui-textbox" style="width:300px; height: 30px;" readonly
                                               value="<?= getUserName($created_by); ?>">
                                    </td>
                                </tr>
                            </table>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="layui-tab-item" style="padding:0px; background: none;">
                                <table border="0" cellspacing="0" class="layui-table" style="padding:20px 20px;">
                                    <tr style="height: 30px;">
                                        <td><?= lang('银行名称(USD)'); ?>： </td>
                                        <td>
                                            <?=$bank_name_usd;?>
                                        </td>
                                    </tr>
                                    <tr style="height: 35px;">

                                        <td><?= lang('bank_account_usd'); ?> ：</td>
                                        <td>
                                            <?=$bank_account_usd;?>
                                        </td>
                                    </tr>
                                    <tr style="height: 35px;">
                                        <td><?= lang('bank_address_usd'); ?> ：</td>
                                        <td>
                                            <?=$bank_address_usd;?>
                                        </td>
                                    </tr>
                                    <tr style="height: 35px;">
                                        <td><?= lang('bank_swift_code_usd'); ?> ：</td>
                                        <td>
                                            <?=$bank_swift_code_usd;?>
                                        </td>
                                    </tr>
                                    <tr style="height: 35px;">
                                        <td><?= lang('银行名称(CNY)'); ?> ：</td>
                                        <td>
                                            <?=$bank_name_cny;?>
                                        </td>
                                    </tr>
                                    <tr style="height: 35px;">
                                        <td><?= lang('bank_account_cny'); ?> ：</td>
                                        <td>
                                            <?=$bank_account_cny;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('bank_address_cny'); ?> ：</td>
                                        <td>
                                            <?=$bank_address_cny;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('taxpayer_name'); ?> ：</td>
                                        <td>
                                            <?=$taxpayer_name;?>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td><?= lang('taxpayer_id'); ?> ：</td>
                                        <td>
                                            <?=$taxpayer_id;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('taxpayer_address'); ?>： </td>
                                        <td>
                                            <?=$taxpayer_address;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('taxpayer_telephone'); ?>： </td>
                                        <td>
                                            <?=$taxpayer_telephone;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('invoice');?><?= lang('email');?>：</td>
                                        <td>
                                            <?=$invoice_email;?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('debit note account');?>：</td>
                                        <td id="title_cn">
                                            <script type="text/javascript">
                                                $(function () {
                                                    $.ajax({
                                                        type: 'post',
                                                        url:'/bsc_dict/get_client_account',
                                                        success: function (data) {
                                                            if (data) {
                                                                var account_id = '<?=$dn_accounts;?>';
                                                                $.each(data, function (index, item) {
                                                                    if (item.id == account_id){
                                                                        $('#title_cn').text(item.title_cn);
                                                                    }
                                                                })
                                                            }
                                                        },
                                                        dataType: "json"
                                                    });
                                                })
                                            </script>
                                            <input type="hidden" id="dn_accounts" name="dn_accounts">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('debit note title'); ?> ：</td>
                                        <td>
                                            <?=$dn_title;?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--查询往来单中同名客户-->
        <div style="padding: 10px 0px 10px 20px; width: 690px; float: left; overflow: hidden">
            <iframe src="/biz_client/client_sales/?company_name=<?= $company_name; ?>" id="search_client" style="width:690px;max-height:250px; min-height: 170px; border:none;"></iframe>
            <!--其他跟进销售-->
            <?php if (!empty($arr)): ?>
                <div style="padding: 20px 20px 20px 20px; border-right: 1px solid #dde5f4;height: 450px; width: auto; min-width: 480px; background:ghostwhite;">
                    <div style="color: red;font-size: 18px">该客户以下销售也在同时跟进:</div>
                    <br>
                    <?php foreach ($arr as $item): ?>
                        <table class="layui-table">
                            <colgroup>
                                <col width="150">
                                <col width="100">
                                <col>
                            </colgroup>
                            <thead>
                            <tr>
                                <th>销售姓名</th>
                                <th>客户联系人</th>
                                <th>客户Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if (!empty($item['client_list'])):
                                foreach ($item['client_list'] as $v):
                                    ?>
                                    <tr>
                                        <td><?= $item['sales_name']; ?></td>
                                        <td><?= $v['name']; ?></td>
                                        <td><?= $v['email']; ?></td>
                                    </tr>
                                <?php
                                endforeach;
                            else: ?>
                                <tr>
                                    <td><?= $item['sales_name']; ?></td>
                                    <td>暂无联系人！</td>
                                    <td></td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <!--底部按钮-->
        <div id="save_tools"
             style="width: 100%; text-align: center; padding: 20px 0px; border-top:1px solid #588c98; position: fixed; z-index: 999; bottom: 0px; background-color:#f6f8f5;">
            <button class="layui-btn" id="next_step" style=" width: 300px; ">下一步</button>
        </div>
    </div>
    <div title="联系人" style="padding:1px">
        <iframe scrolling="auto" frameborder="0" id="contact_list" style="width:100%;height:750px;"></iframe>
    </div>
    <div title="每日跟进记录" style="padding:1px">
        <iframe scrolling="auto" frameborder="0" id="follow_up_list" style="width:100%;height:750px;"></iframe>
    </div>
</div>
<script type="text/javascript" src="/inc/third/layui/layui.js"></script>
<script>
    layui.use('element', function () {
        var index=parent.layer.getFrameIndex(window.name); //获取当前窗口的name
        $('#next_step').click(function () {
            var row = {};
            parent.layer.close(index);
            row.id = '<?=isset($_GET['workflow_id'])?$_GET['workflow_id']:0;?>';
            parent.row_approval_page(row);
        })
    });
</script>
</body>