<script type="text/javascript">
    var is_submit = false;
    /**
     * 保存数据
     */
    function save_data(){
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $('#fm').form('submit', {
            url: '/bsc_message/set_message_data/consol/<?= $consol_id;?>/SI/LOGWING',
            onSubmit: function(param){
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                    is_submit = false;
                }
                return validate;
            },
            success:function(res_json){
                var res;
                try {
                    res = $.parseJSON(res_json);
                }catch (e) {

                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误请联系管理员');
                    return;
                }
                ajaxLoadEnd();
                is_submit = false;
                if(res.code == 0){
                    $.messager.alert('Tips', res.msg, 'info', function () {
                        location.href = "";
                    });
                }else{
                    $.messager.alert('Tips', res.msg);
                }
            }
        });
    }

    function create_edi(action = 'insert'){
        if(is_submit){
            return;
        }
        is_submit = true;
        ajaxLoading();
        $.ajax({
            type: 'POST',
            url: '/bsc_message/create_message/<?= $consol_id;?>/LOGWING/SI/' + action,
            dataType:'json',
            success: function (res) {
                ajaxLoadEnd();
                is_submit = false;
                if(res.code == 0){
                    parent.$.messager.alert('<?= lang('提示');?>', res.msg, 'info', function () {
                        location.reload();
                    });
                }else{
                    parent.$.messager.alert('<?= lang('提示');?>', res.msg);
                }
            },
            error: function (e) {
                ajaxLoadEnd();
                is_submit = false;
                parent.$.messager.alert('<?= lang('提示');?>', e.responseText);
            }
        });
    }
</script>
<body>
<form id="fm" method="post">
    <fieldset>
        <legend><?= lang('CONSOL参数');?></legend>
        <table>
            <tr>
                <td>&nbsp;&nbsp;</td>
                <td><?= lang('ams_type');?></td>
                <td>
                    <select class="easyui-combobox" name="ams_type" id="ams_type" style="width:200px;" data-options="
                        editable:false,
                        value:'<?= $edi_data['ams_type'];?>',
                    ">
                        <option value=""><?= lang('-');?></option>
                        <option value="A"><?= lang('A/nvocc 自主申报 HOUSE 提单');?></option>
                        <option value="C"><?= lang('C/无 HOUSE 单或欧线单');?></option>
                        <option value="M"><?= lang('M/HOUSE 单委托');?></option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <button type="button" onclick="save_data()" title="<?= lang('保存');?>"><?= lang('保存数据');?></button>
                    <button type="button" onclick="create_edi('<?= $action;?>')" title="<?= lang('生成报文');?>"><?= lang('生成报文');?></button>
                </td>
            </tr>
        </table>
    </fieldset>
</body>