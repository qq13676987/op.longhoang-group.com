<?php

class crm_promote_mail_log_model extends CI_Model
{
    private $table = 'crm_promote_mail_log';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function save($data = array())
    {
        $data["created_time"] = date('Y-m-d H:i:s',time());
        $this->db->insert($this->table, $data);

        return $this->db->insert_id();

    }

    public function del($id = '')
    {
        // delete container
        $this->db->where('id', $id);
        return $this->db->delete($this->table);
    }
    public function del_by_parent($id = '')
    {
        // delete container
        $this->db->where('mail_id', $id);
        return $this->db->delete($this->table);
    }

    public function update($id = '', $data = array())
    {
        $data["update_time"] = date('Y-m-d H:i:s', time());
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

}