<?php


class Client
{
    private $db;
    public function __construct()
    {
        $this->db = & get_instance()->db;
    }

    /**
     * @title 是否是活跃客户
     * @param string $client_code   客户代码
     * @return bool
     */
    public function is_active($client_code=[]){
        if (empty($client_code)){
            return false;
        }else{
            $where = "status = 'normal' and date_add(booking_ETD, INTERVAL 3 MONTH) > NOW()";
            //如果3个月内有过交易是活跃客户，否则是非活跃客户
            if ($this->db->where_in('client_code', (array)$client_code)->where($where)->count_all_results('biz_shipment') > 0){
                return true;
            }else{
                return false;
            }
        }
    }

    /**
     * @title 获取某客户的全部销售
     * @param string $client_code   客户代码
     * @return array
     */
    public function get_client_sales($client_code=''){
        if (empty($client_code)) return [];
        $where = array(
            'client_code'   => $client_code,
            'user_role'     => 'sales'
        );
        $sub_query = $this->db->select('user_id')->where($where)->group_by('user_id')->get_compiled_select('biz_client_duty');
        return $this->db->select('id, name, leader_id')->where("id in({$sub_query})", null, false)->get('bsc_user')->result_array();
    }

    /**
     * @title 获取多个用户中级别最高的一个
     * @param array $user_id
     * @return array
     */
    public function get_user_level_max($user_ids = []){
        if (empty($user_ids)) return [];
        $user = $this->db->select('id, name, m_level')->where_in("id", $user_ids)->order_by('m_level', 'desc')->get('bsc_user')->row_array();
        return $user;
    }

    /**
     * @title 获取某抬头全部关联公司的客户代码
     * @param string $client_code   客户代码
     * @return array
     */
    public function get_relation_company($client_code=''){
        if (empty($client_code)) return [];
        $sql = "SELECT client_code_b AS client_code FROM biz_client_relation WHERE client_code_a = '{$client_code}'";
        $sql .= " UNION ALL SELECT client_code_a AS client_code FROM biz_client_relation WHERE client_code_b = '{$client_code}'";
        $rs = $this->db->query($sql)->result_array();
        return array_column($rs, 'client_code');
    }

    /**
     * @title 清除当前客户3个月没成交过的销售
     * @param string $client_code   客户代码
     * @return array
     */
    public function clear_sales($client_code){
        //获取抬头下的全部相关人员
        $client_duty = $this->db->where('client_code', $client_code)
            ->where_in('client_role', ['factory', 'logistics_client'])
            ->get('biz_client_duty')
            ->result_array();
        if (empty($client_duty)) return true;

        //人员按角色归类
        $duty_user = [];
        foreach ($client_duty as $item){
            $duty_user[$item['user_role']][] = $item;
        }

        //获取3个月内下过单的销售
        $where = "id_type = 'biz_shipment' AND user_role = 'sales' ";
        $where .= "AND id_no IN ( select id from biz_shipment where client_code = '{$client_code}' and status = 'normal' and date_add(booking_ETD, INTERVAL 3 MONTH) > NOW() )";
        $trade_of_sales = $this->db->select('user_id')->where($where)->group_by('user_id')->get('biz_duty_new')->result_array();

        //获取3个月内没有下过单的销售
        $duty_sale = isset($duty_user['sales']) ? array_column($duty_user['sales'], 'user_id') : [];
        $no_trade_sales = array_diff($duty_sale, array_column($trade_of_sales, 'user_id'));

        if (!empty($no_trade_sales)) {
            foreach ($duty_user as $key => $rows) {
                if (in_array($key, ['operator', 'customer_service', 'sales_assistant', 'regional_coordinator']) === true) {
                    foreach ($rows as $row) {
                        //先解除操作、客服与销售的关联关系
                        $where = array('duty_id' => $row['id'], 'ext_name' => 'relation_sales');
                        $this->db->where($where)
                            ->where_in('ext_value', $no_trade_sales)
                            ->delete('biz_client_duty_ext');

                        //如果相关人员没有其它关联销售的记录则删除人员
                        if ($this->db->where($where)->count_all_results('biz_client_duty_ext') == 0)
                        {
                            $rs = $this->db->where(array('id' => $row['id'], 'client_code' => $client_code))->delete('biz_client_duty');
                            if ($rs) {
                                $client = Model("biz_client_model")->get_where_one("client_code = '{$client_code}'");
                                // record the log
                                $log_data = array();
                                $log_data["table_name"] = "biz_client_duty";
                                $log_data["key"] = $row['id'];
                                $log_data["master_table_name"] = 'biz_client';
                                $log_data["master_key"] = $client['id'];
                                $log_data["action"] = "delete";
                                $log_data["value"] = json_encode($row);
                                log_rcd($log_data);
                            }
                        }
                    }
                }
            }

            //删除3个月内没有下过单的销售
            foreach ($duty_user['sales'] as $item){
                if (in_array($item['user_id'], $no_trade_sales) === true) {
                    $rs = $this->db->where(array('id' => $item['id'], 'client_code' => $client_code))->delete('biz_client_duty');
                    if ($rs) {
                        $client = Model("biz_client_model")->get_where_one("client_code = '{$client_code}'");
                        // record the log
                        $log_data = array();
                        $log_data["table_name"] = "biz_client_duty";
                        $log_data["key"] = $item['id'];
                        $log_data["master_table_name"] = 'biz_client';
                        $log_data["master_key"] = $client['id'];
                        $log_data["action"] = "delete";
                        $log_data["value"] = json_encode($item);
                        log_rcd($log_data);
                    }
                }
            }
        }

        return true;
    }

    /**
     * @title 是否有3个月没成交过的销售
     * @param string $client_code   客户代码
     * @return array
     */
    public function has_no_trade_sales($client_code){
        //获取抬头下的全部相关人员
        $client_duty = $this->db->where('client_code', $client_code)
            ->where_in('client_role', ['factory', 'logistics_client'])
            ->get('biz_client_duty')
            ->result_array();
        if (empty($client_duty)) return false;

        //人员按角色归类
        $duty_user = [];
        foreach ($client_duty as $item){
            $duty_user[$item['user_role']][] = $item;
        }

        //获取3个月内下过单的销售
        $where = "id_type = 'biz_shipment' AND user_role = 'sales' ";
        $where .= "AND id_no IN ( select id from biz_shipment where client_code = '{$client_code}' and status = 'normal' and date_add(booking_ETD, INTERVAL 3 MONTH) > NOW() )";
        $trade_of_sales = $this->db->select('user_id')->where($where)->group_by('user_id')->get('biz_duty_new')->result_array();

        //获取3个月内没有下过单的销售
        $duty_sale = isset($duty_user['sales']) ? array_column($duty_user['sales'], 'user_id') : [];
        $no_trade_sales = array_diff($duty_sale, array_column($trade_of_sales, 'user_id'));

        if (!empty($no_trade_sales)) {
            $user = $this->db->where_in('id', $no_trade_sales)->get('bsc_user')->result_array();
            if (!empty($user)){
                $user = array_column($user, 'name');
                return "原销售" . join('、', $user) . '因3个月没有成交记录，审核通过时将被删除！';
            }
        }else{
            return false;
        }
    }
}