<script type="text/javascript">
    function doSearch(){
        $('#tt').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val() ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val() ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val(),
            sub_company: $('#sub_company').combobox('getValue'),
            user_role: $('#user_role').combobox('getValue'),
            status:$('#status').val(),
        });
        $('#chaxun').window('close');
    }
    function copy (txt) {
        copytext(txt,function(){
            $.messager.alert('<?= lang('提示');?>',"<?= lang('复制成功！');?>");
        });
    }

    function express_fee(){
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            var row = rows[0];
            if(rows.length > 1){
                $.messager.alert('<?= lang('提示');?>', '<?= lang('只能选择一条');?>');
                return false;
            }
            window.open('/bsc_user/express_fee?id=' + row.id);
        }else{
            $.messager.alert('<?= lang('error')?>','<?= lang('No columns selected')?>');
        }
    }
    
    function late_fee(){
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            var row = rows[0];
            if(rows.length > 1){
                $.messager.alert('<?= lang('提示');?>', '<?= lang('只能选择一条');?>');
                return false;
            }
            window.open('/bsc_user/late_fee?id=' + row.id);
        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('请选择任意一行后再试')?>');
        }
    }
    
    function other_fee(){
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            var row = rows[0];
            if(rows.length > 1){
                $.messager.alert('<?= lang('提示');?>', '<?= lang('只能选择一条');?>');
                return false;
            }
            window.open('/bsc_user/other_fee?id=' + row.id);
        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('请选择任意一行后再试')?>');
        }
    }
    
    function commision_rate(){
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            var row = rows[0];
            if(rows.length > 1){
                $.messager.alert('<?= lang('提示');?>', '<?= lang('只能选择一条');?>');
                return false;
            }
            window.open('/bsc_user/commision_rate?id=' + row.id);
        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('请选择任意一行后再试')?>');
        }
    }

    //2022-06-24 票数奖励
    function commision_piaoshu(){
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            var row = rows[0];
            if(rows.length > 1){
                $.messager.alert('<?= lang('提示');?>', '<?= lang('只能选择一条');?>');
                return false;
            }
            window.open('/bsc_user/commision_piaoshu?id=' + row.id);
        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('请选择任意一行后再试')?>');
        }
    }


    //2022-3-28 新增指标税点
    function sale_targets(){
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            var row = rows[0];
            if(rows.length > 1){
                $.messager.alert('<?= lang('提示');?>', '<?= lang('只能选择一条');?>');
                return false;
            }
            window.open('/bsc_user/sale_targets?id=' + row.id);
        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('请选择任意一行后再试')?>');
        }
    }
    function commision_rate_leader(){
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            var row = rows[0];
            if(rows.length > 1){
                $.messager.alert('<?= lang('提示');?>', '<?= lang('只能选择一条');?>');
                return false;
            }
            window.open('/bsc_user/commision_rate_leader?id=' + row.id);
        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('请选择任意一行后再试')?>');
        }
    }
    
    function email_for(value, row, index) {
        return '<a href="mailto:' + value + '">' + value + '</a>';
    }
    
    function name_for(value, row, index) {
         var str = `<a href='javascript:;' onclick="open_new_tab('${value}')" >${value}</a>`;
        return str;
    }

    function open_new_tab(title){
        // console.log(title)
        parent.parent_open_new_tab(title)

    }
    
    function status_search(status){
        $('#status').val(status);
        doSearch();
    }
    
    $(function () {
        $('#chaxun').on('keydown', 'input', function (e) {
            if(e.keyCode == 13){
                doSearch();
            }
        });
        $('#tt').edatagrid({
            url: '/bsc_user/get_data?title=<?=$title?>&type=<?=$type?>',
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
            onDblClickCell: function (index, field, value) {
                copy(value);
            },
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });

</script>
<style type="text/css">
    input:read-only,select:read-only,textarea:read-only
    {
        background-color: #d6d6d6;
        cursor: default;
    }
</style>
<script>
    function web_log(val, row, index) {
        return '<a style="color:#1E9FFF" href="/biz_client_contact/web_log?email='+row.id+'" target="_blank" ><?= lang('点击查看');?></a>';
    }
    function zoom_in(ele){
        $('.zoom_bg').show();
        $('#zoom_src').attr('src',$(ele).attr('src'))
    }
    function zoom_off(){
        $('.zoom_bg').hide();
        $('#zoom_src').removeAttr('src')
    }
    function del_photo(id=''){
        if(id == '') return;
        $.messager.confirm('<?= lang('确认');?>', '<?= lang('确认删除头像?');?>', function(r){
            if (r){
                $.post("/bsc_user/del_photo/"+id, { },
                    function(res){
                        $('#tt').datagrid('reload')
                    }, "json");
            }
        });
    }
</script>
<div style="width: 100%;height: 100%;background: #ddd;position: absolute;z-index: 999;display: none" onclick="zoom_off()" class="zoom_bg">
    <img src="" id="zoom_src" style="width: 30%;position: relative;left: 35%;top:50px;z-index: 9999" alt="">
</div>
<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
        <tr>
            <th field="name" width="120" formatter="name_for" sortable="true"><?= lang('姓名');?></th>
            <th field="name_en" width="180" formatter="name_for" sortable="true"><?= lang('英文名');?></th>
            <th field="photo" width="80" ><?= lang('头像');?></th>
            <th field="country" width="60"><?= lang('国籍');?></th>
            <th field="sub_company" width="100"><?= lang('分公司');?></th>
            <th field="group" width="210" sortable="true"><?= lang('部门');?></th>
            <th field="email" width="240" formatter="email_for" sortable="true"><?= lang('邮件');?></th>
            <th field="telephone" width="100" sortable="true"><?= lang('电话');?></th>
            <th field="telephone_desk" width="100" sortable="true"><?= lang('座机');?></th>
            <th field="qq" width="100" sortable="true"><?= lang('QQ');?></th>
            <th field="user_role" width="500" sortable="true"><?= lang('角色');?></th>
            <th field="job_description" width="200" sortable="true"><?= lang('工作说明');?></th>
            <!--<th width="100" align="center" data-options="field:'log',formatter:web_log">运价查询日志</th>-->
        </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
<!--                --><?php //if(special_test('express_fee')){ ?><!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="javascript:express_fee();">--><?//= lang('快递费');?><!--</a>--><?php //} ?>
<!--                --><?php //if(special_test('late_fee')){ ?><!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="javascript:late_fee();">--><?//= lang('滞纳金');?><!--</a>--><?php //} ?>
<!--                --><?php //if(is_admin() || in_array('finance', get_session('user_role'))){//财务可见 ?>
<!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="javascript:other_fee();">--><?//= lang('其他扣款');?><!--</a>-->
<!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="javascript:commision_rate();">--><?//= lang('提成设置');?><!--</a>-->
<!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="javascript:sale_targets();">--><?//= lang('指标扣点');?><!--</a>-->
<!--                <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="status_search('');">--><?//= lang('查看全部');?><!--</a>-->
<!--                --><?php //} ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span style="color:red;">
                <?= lang('本页查询条件： {text}', array('text' => $_GET['title']));?>
                </span>
            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <input type="hidden" name="status" id="status" value="0">
    <table> 
        <tr>
            <td>
                <?= lang('分公司');?>
            </td>
            <td align="left">
               <select class="easyui-combobox" name="sub_company" id="sub_company" style="width: 377px;"
                    data-options=" 
                        valueField:'company_code',
                        textField:'company_name',
                        url:'/biz_sub_company/get_option' 
                    ">

                    </select>
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('查询');?> 1
            </td>
            <td align="right">
                <select name="f1"  id="f1"  class="easyui-combobox"  required="true" style="width:150px;"> 
                    <option value="name"><?= lang('name');?></option>
                    <option value="group"><?= lang('group');?></option>
                    <option value="telephone"><?= lang('telephone');?></option>
                    <option value="email"><?= lang('email');?></option>
                    <option value="user_role"><?= lang('user_role');?></option>
                </select>
                &nbsp;

                <select name="s1"  id="s1"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                        <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v1" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('查询');?> 2
            </td>
            <td align="right">
                <select name="f2"  id="f2"  class="easyui-combobox"  required="true" style="width:150px;"> 
                    <option value="name"><?= lang('name');?></option>
                    <option value="group"><?= lang('group');?></option>
                    <option value="telephone"><?= lang('telephone');?></option>
                    <option value="email"><?= lang('email');?></option>
                    <option value="user_role"><?= lang('user_role');?></option>
                </select>
                &nbsp;

                <select name="s2"  id="s2"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                        <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v2" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('查询');?> 3
            </td>
            <td align="right">
                <select name="f3"  id="f3"  class="easyui-combobox"  required="true" style="width:150px;">
                    <option value="name"><?= lang('name');?></option>
                    <option value="group"><?= lang('group');?></option>
                    <option value="telephone"><?= lang('telephone');?></option>
                    <option value="email"><?= lang('email');?></option>
                    <option value="user_role"><?= lang('user_role');?></option>
                </select>
                &nbsp;

                <select name="s3"  id="s3"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                        <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v3" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('用户角色');?>
            </td>
            <td align="right">
                 <select class="easyui-combotree" style="width:395px;" name="user_role" id="user_role" editable="true" data-options="
                    multiple:true,
                    url:'/bsc_user_role/get_role_tree',
                    required:true,
                    cascadeCheck: false,
                ">
                </select>
            </td>
        </tr>
    </table>
     
    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;"><?= lang('查询');?></a>
</div>
