<?php
   class biz_company_model extends CI_Model{
        public function __construct(){
            $this->load->database();
        }
        public function get_by_id($id=0){
                $this->db->where('id', $id);
                $query = $this->db->get('biz_company');
                return $query->row_array();
        } 
        public function get_one($f="",$v=""){
                $this->db->where($f, $v);
                $query = $this->db->get('biz_company');
                return $query->row_array();
        } 
        
        public function get_where_one($where = ""){
            $this->db->where($where);
            $query = $this->db->get('biz_company');
            return $query->row_array();
        }
        
		public function get_option($company_type, $client_code=""){      
                $this->db->where('company_type', $company_type);  
                if($client_code){
                    $this->db->where('client_code', $client_code);  
                }
                $query = $this->db->get('biz_company'); 
                return $query->result_array(); 
        } 
        public function get($where="" ,$sort="id",$order="desc"){ 
			if($where !="")	$this->db->where($where);	
			$this->db->order_by($sort,$order);		
            $query = $this->db->get('biz_company');
            return $query->result_array();
        }
		
		public function total($where=""){  
			if($where !="")	$this->db->where($where);	
            $this->db->from('biz_company');
            return  $this->db->count_all_results('');
        }  	           
        
        public function save($data=array()){
			$data["created_time"] = date('y-m-d H:i:s',time());
			$data["updated_time"] = date('y-m-d H:i:s',time());
			$data["created_by"] =  $this->session->userdata('id');
			$data["updated_by"] =  $this->session->userdata('id');
			$data["created_group"] = implode(',', $this->session->userdata('group'));
            $this->db->insert('biz_company', $data); 
            return $this->db->insert_id();                
        } 
        
        public function update($id='',$data=array()){
				$data["updated_by"] =  $this->session->userdata('id');
                $this->db->where('id', $id);
                $this->db->update('biz_company', $data);   
				return $id;
        }   
        
        public function mdelete($id=''){ 
				// delete company
                $this->db->where('id', $id);
                $this->db->delete('biz_company');                                                
        }         
          
   }