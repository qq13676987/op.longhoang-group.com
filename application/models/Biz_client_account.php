<?php


class Biz_client_account extends Menu_Controller
{
    protected $menu_name = 'client_account';//菜单权限对应的名称
    protected $method_check = array(//需要设置权限的方法
        'index',
        'add_data',
        'update_data',
        'delete_data',
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('biz_client_account_model');
        $this->model = $this->biz_client_account_model;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }


    public $field_edit = array();

    public $field_all = array(
        array('id', 'id', '80', '1'),
//        array('client_code','client_code','100','2'),
        array('bank_title', 'bank_title', '100', '3'),
        array('bank_name', 'bank_name', '100', '3'),
        array('bank_account', 'bank_account', '100', '4'),
        array('bank_address', 'bank_address', '100', '5'),
        array('bank_swift_code', 'bank_swift_code', '100', '6'),
        array('biz_type', 'biz_type', '100', '6'),
        array('create_time', 'create_time', '150', '8'),
//        array('create_by','create_by','150','9'),
        array('update_time', 'update_time', '150', '10'),
//        array('update_by','update_by','150','11'),
    );

    public function index($client_code = 0)
    {
        $data = array();

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('biz_client_account_list_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }
        // page account
        $rs = $this->sys_config_model->get_one('biz_client_account_list_table_row_num');
        if (!empty($rs['config_name'])) {
            $data["n"] = $rs['config_text'];
        } else {
            $data["n"] = "30";
        }

        $data["client_code"] = $client_code;
        $this->load->view('head');
        $this->load->view('biz/client_account/index_view', $data);
    }

    public function get_data($client_code = "")
    {
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = "";
        $where .= 'client_code = CONCAT("' . $client_code . '")';
        $f1 = isset($_REQUEST['f1']) ? $_REQUEST['f1'] : '';
        $s1 = isset($_REQUEST['s1']) ? $_REQUEST['s1'] : '';
        $v1 = isset($_REQUEST['v1']) ? $_REQUEST['v1'] : '';
        $f2 = isset($_REQUEST['f2']) ? $_REQUEST['f2'] : '';
        $s2 = isset($_REQUEST['s2']) ? $_REQUEST['s2'] : '';
        $v2 = isset($_REQUEST['v2']) ? $_REQUEST['v2'] : '';
        $f3 = isset($_REQUEST['f3']) ? $_REQUEST['f3'] : '';
        $s3 = isset($_REQUEST['s3']) ? $_REQUEST['s3'] : '';
        $v3 = isset($_REQUEST['v3']) ? $_REQUEST['v3'] : '';
        if ($v1 != "") $where .= " and $f1 $s1 '$v1'";
        if ($v2 != "") $where .= " and $f2 $s2 '$v2'";
        if ($v3 != "") $where .= " and $f3 $s3 '$v3'";
        //-----------------------------------------------------------------

        $offset = ($page - 1) * $rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $rs = $this->model->get($where, $sort, $order);

        $rows = array();
        foreach ($rs as $row) {

            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data($client_code = "")
    {
        $field = $this->field_edit;
        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($temp != "")
                $data[$item] = $temp;
        }
        $data['client_code'] = $client_code;
        $id = $this->model->save($data);
        $data['id'] = $id;

        echo json_encode($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_client_account_list";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function update_data()
    {
        $id = $_REQUEST["id"];
        $old_row = $this->model->get_one('id', $id);

        $field = $this->field_edit;

        $data = array();
        foreach ($field as $item) {
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            if ($old_row[$item] != $temp)
                $data[$item] = $temp;
        }
        $id = $this->model->update($id, $data);
        $data['id'] = $id;
        echo json_encode($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_client_account_list";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function delete_data()
    {
        $id = intval($_REQUEST['id']);
        $old_row = $this->model->get_one('id', $id);
        $this->model->mdelete($id);
        echo json_encode(array('success' => true));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_client_account_list";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
    }

    public function get_option($client_code = "", $biz_type = '')
    {
        if ($client_code != '') {
            $data = array();

            $empty = array('client_code' => $client_code, 'bank_title' => ' ', 'bank_name' => ' ', 'bank_account' => ' ', 'bank_address' => ' ', 'bank_swift_code' => ' ','biz_type' => $biz_type);
            $data[] = $empty;
            $where = array('client_code' => $client_code);
            if ($biz_type != '') $where['biz_type'] = $biz_type;
            $rs = $this->model->get_option($where);
            foreach ($rs as $row){
                $data[] = $row;
            }
            
            echo json_encode($data);
        }
    }
}