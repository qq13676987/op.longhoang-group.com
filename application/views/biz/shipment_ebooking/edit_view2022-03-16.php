<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title>shipment_ebooking-Edit</title>

<script type="text/javascript">
function qzb(str){
    str = str.replace(new RegExp('·', "g"), '`');
    str = str.replace(new RegExp('！', "g"), '!');
    str = str.replace(new RegExp('￥', "g"), '$');
    str = str.replace(new RegExp('【', "g"), '[');
    str = str.replace(new RegExp('】', "g"), ']');
    str = str.replace(new RegExp('】', "g"), ']');
    str = str.replace(new RegExp('；', "g"), ';');
    str = str.replace(new RegExp('：', "g"), ':');
    str = str.replace(new RegExp('“', "g"), '"');
    str = str.replace(new RegExp('”', "g"), '"');
    str = str.replace(new RegExp('’', "g"), '\'');
    str = str.replace(new RegExp('‘', "g"), '\'');
    str = str.replace(new RegExp('、', "g"), '');
    str = str.replace(new RegExp('？', "g"), '?');
    str = str.replace(new RegExp('《', "g"), '<');
    str = str.replace(new RegExp('》', "g"), '>');
    str = str.replace(new RegExp('，', "g"), ',');
    str = str.replace(new RegExp('。', "g"), '.');
    str = str.replace(new RegExp(' ', "g"), ' ');
    str = str.replace(new RegExp('（', "g"), '(');
    str = str.replace(new RegExp('）', "g"), ')');
    return str;
}

function INCO_term_info(INCO_term) {
    var destination_SO = ['DDU', 'DDP', 'DAP'];
    if($.inArray(INCO_term, destination_SO) !== -1){
        add_INCO_term_option();
    }else{
        del_INCO_term_option();
    }
}

function dangerous_info(goods_type) {
    //如果不为普通货物，贼显示危险品详细信息
    if(goods_type == 'GC' || goods_type == 'RF'){
        $('.dangerous').css('display','none');
        $('.special_size').css('display','none');
    }else{
        if(goods_type == 'DR'){
            $('#isdangerous').textbox({required: true});
            // $('#ism_spec').textbox({required: true});
            $('#UN_no').textbox({required: true});
            // $('#IMDG_page').textbox({required: true});
            // $('#packing_type').textbox({required: true});
            // $('#flash_point').textbox({required: true});
            // $('#IMDG_code').textbox({required: true});
            // $('#is_pollution_sea').textbox({required: true});
            // $('#ems_no').textbox({required: true});
            // $('#MFAG_NO').textbox({required: true});
            // $('#e_contact_name').textbox({required: true});
            // $('#e_contact_tel').textbox({required: true});

            $('#long').textbox({required: false,value:''});
            $('#wide').textbox({required: false,value:''});
            $('#high').textbox({required: false,value:''});

            $('.dangerous').css('display','table-row');
            $('.special_size').css('display','none');
        }else if(goods_type == 'FR'){
            $('#isdangerous').textbox({required: false,value:''});
            // $('#ism_spec').textbox({required: false,value:''});
            $('#UN_no').textbox({required: false,value:''});
            // $('#IMDG_page').textbox({required: false,value:''});
            // $('#packing_type').textbox({required: false,value:''});
            // $('#flash_point').textbox({required: false,value:''});
            // $('#IMDG_code').textbox({required: false,value:''});
            // $('#is_pollution_sea').textbox({required: false,value:''});
            // $('#ems_no').textbox({required: false,value:''});
            // $('#MFAG_NO').textbox({required: false,value:''});
            // $('#e_contact_name').textbox({required: false,value:''});
            // $('#e_contact_tel').textbox({required: false,value:''});

            $('#long').textbox({required: true});
            $('#wide').textbox({required: true});
            $('#high').textbox({required: true});

            $('.dangerous').css('display','none');
            $('.special_size').css('display','table-row');
        }
    }
}

function checkName(val){
    // var reg = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
    var reg = new RegExp(/[^\x00-\x7f|\u4e00-\u9fa5]/g);
    var rs = "";
    for (var i = 0, l = val.length; i < val.length; i++) {
        if(val.substr(i, 1).match(reg)) rs += val.substr(i, 1);
    }
    return rs;
}

$(function(){
    //初始值
    $('#client_company').combobox('setValue', '<?= es_encode($client_company);?>');
    $('#client_contact').combobox('setValue', '<?= es_encode($client_contact);?>');
    $('#shipper_company').combobox('setValue', '<?= es_encode($shipper_company);?>');
    $('#consignee_company').combobox('setValue', '<?= es_encode($consignee_company);?>');
    $('#notify_company').combobox('setValue', '<?= es_encode($notify_company);?>');
    
    $('#client_code').combobox('reload', '/biz_client/get_option/client?other_role=logistics_client,oversea_client&q_field=client_code');
    $('#client_company').combobox('reload', '/biz_client/get_option/client?other_role=logistics_client,oversea_client');
    
    $('#form').form({
        url: "/biz_shipment/ebooking_save/<?php echo $id;?>",
        onSubmit: function(){
            var vv = $('#client_company').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('client_company');?> required！");
                return false;
            }
            var vv = $('#shipper_company').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('shipper_company');?> required！");
                return false;
            }
            var vv = $('#consignee_company').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('consignee_company');?> required！");
                return false;
            }
            var vv = $('#notify_company').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('notify_company');?> required！");
                return false;
            }
            var vv = $('.service_options:checked').length;
            if (vv == 0) {
                alert("<?= lang('service_options');?> required！");
                return false;
            }
        
            var vv = $('#biz_type1').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('biz_type');?> required！");
                return false;
            }
        
            var vv = $('#trans_tool').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('trans_tool');?> required！");
                return false;
            }
            var vv = $('#booking_ETD').datebox('getValue');
            if (vv == "") {
                alert("<?= lang('booking_ETD');?> required！");
                return false;
            }
            var vv = $('#hbl_type').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('hbl_type');?> required！");
                return false;
            }
            var vv = $('#release_type').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('update_release_type');?> required！");
                return false;
            }
            var vv = $('#trans_origin').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('update_trans_origin');?> required！");
                return false;
            }
            var vv = $('#trans_discharge').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('update_trans_discharge');?> required！");
                return false;
            }
            var vv = $('#trans_destination').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('update_trans_destination');?> required！");
                return false;
            }
            
            var vv = $('#trans_term').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('trans_term');?> required！");
                return false;
            }
            var vv = $('#INCO_term').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('update_INCO_term');?> required！");
                return false;
            }
            var vv = $('#sales_id').combobox('getValue');
            if(vv == 0){
                alert("<?= lang('sales');?> required！");
                return false;
            }
            var vv = $('#description').val();
            if (vv == "") {
                alert("<?= lang('description');?> required！");
                return false;
            }
            var vv = $('#goods_type').combobox('getValue');
            if (vv == "") {
                alert("<?= lang('goods_type');?> required！");
                return false;
            }
            var vv = $('#hs_code').val();
            if (vv == "") {
                alert("<?= lang('hs_code');?> required！");
                return false;
            }
            var vv = $('#trans_mode').val();
            if(vv == 'FCL'){
                if($('.box_info_size').length > 0 && ($('.box_info_size').combobox('getValue') == '' || $('.box_info_num').textbox('getValue') == '') ){
                    alert("box_info！");
                    return false;
                }
            }
            $('input[type="checkbox"]').attr('disabled', false);
            $.each($('textarea'), function(index, item){
                var str = $(this).val();
                var id = $(this).prop('id');
                var ids = ['shipper_address', 'consignee_address', 'notify_address', 'description', 'mark_nums'];
                str = qzb(str);
                $(this).val(str);
                var newstr = $(this).val();
                var special_char = checkName(newstr);
                if(special_char !== ''){
                    $.messager.alert('error', id + '检测到有以下特殊字符:<br/>' + special_char);
                    is_special_char = true;
                    return false;
                }else{
                    is_special_char = false;
                    if($.inArray(id, ids) != -1){
                        $(this).val($(this).val().toUpperCase());
                    }
                }
            });
            if(is_special_char){
                return false;
            }
            return true;
        },
        success:function(res_json){
            var res;
            try{
                res = $.parseJSON(res_json);
            }catch (e) {

            }
            if(res === undefined){
                alert('发生错误,请联系管理员');
            }else{
                alert(res.msg);
                if(res.code == 0){
                    try{opener.$('#tt').edatagrid('reload');}catch (e) {}
                    location.href = '/biz_shipment/edit/' + res.data.id;
                }
            }
        }
    });
    
    $('.status').change(function () {
        var val = 0;
        if($(this).prop('checked')) {
            var day1 = new Date();
            var month = '';
            if (day1.getMonth() + 1 < 10) {
                month = '0' + (day1.getMonth() + 1);
            } else {
                month = day1.getMonth() + 1;
            }
            var val = day1.getFullYear() + "-" + month + "-" + day1.getDate() + ' ' + day1.getHours() + ':' + day1.getMinutes() + ':' + day1.getSeconds();
        }

        var field = $(this).attr('id');
        var data = {};
        data[field] = val;
        $.ajax({
            url: '/biz_shipment/data_consol_id/<?php echo $id;?>',
            type: 'POST',
            data:data,
            success: function (res) {

            }
        });
    });
    service_option();

    $('textarea').blur(function () {
        var str = $(this).val();
        if (str.match(/[^\x00-\x7f|\u4e00-\u9fa5]/g)) {
            $(this).val(str.replace(/[^\x00-\x7f|\u4e00-\u9fa5]/g,''));
        }
    });
});

function service_option() {
    $.ajax({
        url: '/bsc_dict/get_option/service_option',
        type: 'GET',
        dataType: 'json',
        success: function (res) {
            var str = '<table style="float: left;">';
            var service_options = [];
            try {
                service_options = $('#service_options').val().split(',');
            } catch(e) {
            }
            str += '<tr style="text-align: center"><th>POL</th></tr>';
            $.each(res, function (index, item) {
                if(item.orderby <= 100){
                    str += '<tr>';
                    var checked = '';
                    if(item.name !== '' && item.value !== ''){
                        if($.inArray(item.value, service_options) !== -1){
                            checked = 'checked';
                        }
                        str += '<td><input class="service_options" disabled type="checkbox" name="service_options[' + index + '][]" value="' + item.value + '" style="vertical-align:middle;" ' + checked + '>' + item.lang + '</td>';
                    }
                    str += '</tr>';
                }
            });
            str += '</table>';
            str += '<table style="float: left;">';
            str += '<tr style="text-align: center"><th>POD</th></tr>';
            $.each(res, function (index, item) {
                if(item.orderby > 100){
                    str += '<tr>';
                    var checked = '';
                    if(item.name !== '' && item.value !== ''){
                        if($.inArray(item.value, service_options) !== -1){
                            checked = 'checked';
                        }
                        str += '<td><input class="service_options" disabled type="checkbox" name="service_options[' + index + '][]" value="' + item.value + '" style="vertical-align:middle;" ' + checked + '>' + item.lang + '</td>';
                    }
                    str += '</tr>';
                }
            });
            str += '</table>';
            $('#service_options_div').append(str);

        }
    });
}
function add_consol(){
	window.open("/biz_consol/add/?shipment_id=<?php echo $id;?>");
}
function changeClient(data){
    $.each($('.userList'), function (index,item) {
        var id = $(this).attr('id').replace('_id','');
        if(id !== 'finance'){
            var key = id + '_ids';
            $(this).combobox('reload', '/bsc_user/get_data_role/' + id + '?userIds=' + data[key]);
        }
    });
    $('#client_contact').combobox('reload', '/biz_client_contact/get_option/' + data.client_code);
    $('#shipper_company').combobox('reload', '/biz_company/get_option/shipper/'+data.client_code);
	$('#consignee_company').combobox('reload', '/biz_company/get_option/consignee/'+data.client_code);
	$('#notify_company').combobox('reload', '/biz_company/get_option/notify/'+data.client_code);
}
function goEditUrl(url){
	var clientCode = $('#client_code').combobox('getValue');
	if(!clientCode){
		alert('clientCode is empty');
	}else{
		window.open(url+'/'+clientCode)
	}
}


function add_box() {
    var length = $('.add_box').length;
    var lang = '';
    var btn = '';
    if(length <= 0){
        lang = "<?= lang('box_info');?>";
        btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
    }else{
        btn = '<a class="easyui-linkbutton" onclick="del_box(this)" iconCls="icon-remove"></a>';
    }
    var str = '<tr class="add_box" >\n' +
        '                            <td>' + lang + '</td>\n'+
        '                            <td>\n'+
        '                                <select class="easyui-combobox box_info_size" name="box_info[size][]" data-options="\n'+
        '                                readonly:true,' +
        '                                required:true,' +
        '                                valueField:\'value\',\n'+
        '                                textField:\'value\',\n'+
        '                                url:\'/bsc_dict/get_option/container_size\',\n'+
        "                                onHidePanel: function() {  \n" +
        "                                   var valueField = $(this).combobox('options').valueField;  \n" +
        "                                   var val = $(this).combobox('getValue');  \n" +
        "                                   var allData = $(this).combobox('getData');  \n" +
        "                                   var result = true;  \n" +
        "                                   for (var i = 0; i < allData.length; i++) {  \n" +
        "                                       if (val == allData[i][valueField]) {  \n" +
        "                                           result = false;  \n" +
        "                                       }  \n" +
        "                                   }  \n" +
        "                                   if (result) {  \n" +
        "                                       $(this).combobox('clear');  \n" +
        "                                   } " +
        "                                }" +
        '                                "></select>\n'+
        '                                X\n'+
        '                                <input readonly required class="easyui-numberbox box_info_num" name="box_info[num][]">\n'+
        '                                ' + btn + '\n'+
        '                            </td>\n'+
        '                        </tr>';
    if(length == 0){
        $('#trans_mode').parents('tr').after(str);
    }else{
        $('.add_box:eq(' + (length - 1) + ')').after(str);
    }
    $.parser.parse('.add_box:eq(' + length + ')');
}

function del_box(e) {
    if(e == undefined){
        $('.add_box').remove();
    }else{
        $(e).parents('tr').remove();
    }
}

function reload_select(id = '', type='combobox'){
    if(type == 'combobox'){
        $('#trans_origin').combobox('reload');
        $('#trans_carrier').combobox('reload');
        $('#shipper_company').combobox('reload');
        $('#consignee_company').combobox('reload');
        $('#notify_company').combobox('reload');
        $('#client_company').combobox('reload');
    }
}

function add_free_svr() {
    var length = $('.free_svr').length;

    var btn = '';
    var lang = '';
    if(length < 4){
        if(length == 0){
            lang = '<?= lang('free svr');?>';
        }
        if(length < 3){
        }
    }else{
        return;
    }
    var str = '  <tr class="free_svr">\n' +
        '                                <td>' + lang + '</td>\n' +
        '                                <td>\n'+
        '                                    <select editable="false" class="easyui-combobox free_svr_type" name="free_svr[' + length + '][type]" data-options="'+
        '                                        valueField:\'value\',\n'+
        '                                        readonly:true,\n'+
        '                                        textField:\'name\',\n'+
        '                                        url: \'/bsc_dict/get_option/free_svr\',\n' +
        '                                        value: \'\',\n'+
        '                                        onSelect: function(rec){\n' +
        '                                            free_svr_type_jc();\n' +
        '                                        },' +
        '                                    ">\n'+
        '                                    </select>\n'+
        '                                    <input readonly class="easyui-numberbox free_svr_days" name="free_svr[' + length + '][days]" ><?= lang('days');?>\n'+
        btn +
        '                                </td>\n'+
        '                            </tr>';

    $('.free_svr:eq(' + (length - 1) + ')').after(str);
    $.parser.parse('.free_svr:eq(' + length + ')');
}

function add_INCO_term_option() {
    $('.INCO_term_option').css('display', 'table-row');
}

function del_INCO_term_option() {
    $('input[name="INCO_option[]"]').removeAttr('checked');
    $('#INCO_address').val('');
    $('.INCO_term_option').css('display', 'none');
}
</script>
<style>
    input:disabled,select:disabled,textarea:disabled
    {
        background-color: #d6d6d6;
        cursor: default;
    }
    input:read-only,select:read-only,textarea:read-only
    {
        background-color: #d6d6d6;
        cursor: default;
    }
    .dangerous{
        display: none;
    }
    .special_size{
        display: none;
    }
    .free_svr_type{
        width: 100px;
    }
    .free_svr_days{
        width: 90px;
    }
    .box_info_num{
        width: 100px;
    }
    .box_info_size{
        width: 100px;
    }
    .container_sum{
        color: red;
    }
    .INCO_term_option{
        display: none;
    }
</style>

<body style="margin-left:10px;">
<table><tr><td>
            <h2> 
            <?php if ($status == 0){?> 
            <button onclick="javascript:$('#form').form('submit');" style="margin-right:15px;"><?= lang('一键录入');?></button>
            <?php } ?>
            </h2></td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><!--<td>Job No: <?php /*echo $job_no;*/?> &nbsp;&nbsp;&nbsp;&nbsp;  HBL NO: <?php /*echo $hbl_no;*/?></td>-->
        <td> &nbsp;&nbsp;&nbsp;&nbsp; </td>
    </tr>
</table>

<div  id="tb" class="easyui-tabs" style="width:100%;height:850px">
		<div title="Basic Registration" style="padding:1px">
		 <form id="form" name="f" method="post" action="/biz_shipment/data/<?php echo $id;?>">
			 <div class="easyui-layout" style="width:100%;height:780px;">
				<div data-options="region:'west',tools:'#tt'" title="Basic" style="width:350px;">
					<div class="easyui-accordion" data-options="multiple:true" style="width:340px;height1:300px;">
                        <div title="Client" style="padding:10px;">
							<select name="client_code" id="client_code" class="easyui-combobox" style="width:90px;" data-options="
							    readonly:true,
                                valueField:'client_code',
								textField:'client_code',
								value: '<?= $client_code; ?>',
								mode: 'remote',
                                onBeforeLoad:function(param){
                                    if($(this).combobox('getValue') == '')return false;
                                    if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                                },
								onSelect: function(rec){
								    var data = $(this).combobox('getData');
								    $('#client_company').combobox('loadData', data);
									$('#client_company').combobox('select', rec.company_name);
								},
                            ">
                            </select>
							<select id="client_company" class="easyui-combobox" editable="true" name="client_company" style="width:175px;" data-options="required:true,
                                    readonly:true,
    								valueField:'company_name', 
    								textField:'company_name',
    								mode: 'remote',
                                    onBeforeLoad:function(param){
                                        if($(this).combobox('getValue') == '')return false;
                                        if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                                    },
    								onSelect: function(rec){
    									$('#client_address').val(rec.company_address);
    									$('#client_contact').combobox('setValue', rec.client_contact);
    									$('#client_telephone').val(rec.contact_telephone2);
    									$('#client_email').val(rec.contact_email);
    									$('#client_code').combobox('setValue',rec.client_code);
    									changeClient(rec);
    								},
    								onLoadSuccess: function(){
    								    var data = $(this).combobox('getData');
    								    $('#client_code').combobox('loadData', data);
    									var clientCode = $('#client_code').combobox('getValue');
    									$.each(data, function(index,item){
    									    if(item.client_code == clientCode){
                                                changeClient(item)
    									    }
    									});
    								},
    								onHidePanel: function() {
    										var valueField = $(this).combobox('options').valueField;
    										var val = $(this).combobox('getValue');
    										var allData = $(this).combobox('getData');
    										var result = true;
    										for (var i = 0; i < allData.length; i++) {
    											if (val == allData[i][valueField]) {
    												result = false;
    											}
    										}
    										if (result) {
    											$(this).combobox('clear');
    										}
    								},
							">
							</select>
                            <a href="/biz_client/index/client" target="_blank"><?= lang('E');?></a>
                            |
                            <a href="javascript: reload_select('client');"><?= lang('R');?></a>
                            <br><?= lang('update_client_contact');?>:<br>
                            <select name="client_contact" id="client_contact" class="easyui-combobox" style="width:300px;" data-options="
                                readonly:true,
                                valueField:'name',
								textField:'name',
								url:'',
								onSelect: function(rec){
									$('#client_telephone').val(rec.telephone);
								},
								onLoadSuccess:function(){
								    var data = $(this).combobox('getData');
								    $('#warehouse_contact').combobox('loadData', data);
								},
                            ">
                            </select>
                            <br><?= lang('update_client_telephone');?>:
                            <br><textarea name="client_telephone" readonly id="client_telephone" style="height:40px;width:300px;"><?php echo $client_telephone;?></textarea>
                            <br><?= lang('update_client_email'); ?>:
                                <br><textarea name="client_email" readonly id="client_email" style="height:60px;width:300px;"><?php echo $client_email; ?></textarea>
                            <br><?= lang('update_client_address');?>:<br>
							<textarea name="client_address" readonly id="client_address" style="height:60px;width:300px;"><?php echo $client_address;?></textarea>
                        </div>
						<div title="Shipper"  style="overflow:auto;padding:10px;">
							<select id="shipper_company" class="easyui-combobox" name="shipper_company" style="width:255px;" data-options="
							    required:true,
							    readonly: true,
								valueField:'company_name', 
								textField:'company_name', 
								url:'',
								onSelect: function(rec){ 
                                    $('#shipper_address').val(rec.company_address);
									$('#shipper_contact').val(rec.company_contact);
									$('#shipper_telephone').val(rec.company_telephone);
                                    $('#shipper_email').val(rec.company_email);
								},
								onHidePanel: function() {
										var valueField = $(this).combobox('options').valueField;
										var val = $(this).combobox('getValue');
										var allData = $(this).combobox('getData');
										var result = true;
										for (var i = 0; i < allData.length; i++) {
											if (val == allData[i][valueField]) {
												result = false;
											}
										}
										if (result) {
											$(this).combobox('clear');
										}
								},
								value:'<?php echo $shipper_company;?>'
								">
							</select>
							<br>
                            <?= lang('shipper_address');?>:
                            <br><textarea readonly name="shipper_address" id="shipper_address" style="height:60px;width:300px;"><?php echo $shipper_address;?></textarea>
                            <br><?= lang('shipper_contact');?>:
                            <br><textarea readonly name="shipper_contact" id="shipper_contact" style="height:40px;width:300px;"><?php echo $shipper_contact;?></textarea>
                            <br><?= lang('shipper_telephone');?>:
                            <br><textarea readonly name="shipper_telephone" id="shipper_telephone" style="height:40px;width:300px;"><?php echo $shipper_telephone;?></textarea>
                            <br><?= lang('shipper_email');?>:
                            <br><textarea readonly name="shipper_email" id="shipper_email" style="height:40px;width:300px;"><?php echo $shipper_email;?></textarea>

                        </div>
						<div title="Consignee" style="padding:10px;">
							 <select id="consignee_company" class="easyui-combobox" name="consignee_company" style="width:255px;" data-options="
							    required:true,
							    readonly: true,
								valueField:'company_name', 
								textField:'company_name', 
								url:'',
								onSelect: function(rec){ 
									$('#consignee_address').val(rec.company_address);
									$('#consignee_contact').val(rec.company_contact);
									$('#consignee_telephone').val(rec.company_telephone);
									$('#consignee_email').val(rec.company_email);
								},
								onHidePanel: function() {
										var valueField = $(this).combobox('options').valueField;
										var val = $(this).combobox('getValue');
										var allData = $(this).combobox('getData');
										var result = true;
										for (var i = 0; i < allData.length; i++) {
											if (val == allData[i][valueField]) {
												result = false;
											}
										}
										if (result) {
											$(this).combobox('clear');
										}
								},
								">
							</select>
							<br><?= lang('consignee_address');?>:
                            <br><textarea readonly name="consignee_address" id="consignee_address" style="height:60px;width:300px;"><?php echo $consignee_address;?></textarea>
                            <br><?= lang('consignee_contact');?>:
                            <br><textarea readonly name="consignee_contact" id="consignee_contact" style="height:40px;width:300px;"><?php echo $consignee_contact;?></textarea>
                            <br><?= lang('consignee_telephone');?>:
                            <br><textarea readonly name="consignee_telephone" id="consignee_telephone" style="height:40px;width:300px;"><?php echo $consignee_telephone;?></textarea>
                            <br><?= lang('consignee_email');?>:
                            <br><textarea readonly name="consignee_email" id="consignee_email" style="height:40px;width:300px;"><?php echo $consignee_email;?></textarea>
                        </div>
						<div title="Notify Party" style="padding:10px;">
							 <select id="notify_company" class="easyui-combobox" name="notify_company" style="width:255px;" data-options="
							    required:true,
							    readonly: true,
								valueField:'company_name', 
								textField:'company_name', 
								url:'',
								onSelect: function(rec){ 
									$('#notify_address').val(rec.company_address);
									$('#notify_contact').val(rec.company_contact);
									$('#notify_telephone').val(rec.company_telephone);
									$('#notify_email').val(rec.company_email);
								},
								onHidePanel: function() {
										var valueField = $(this).combobox('options').valueField;
										var val = $(this).combobox('getValue');
										var allData = $(this).combobox('getData');
										var result = true;
										for (var i = 0; i < allData.length; i++) {
											if (val == allData[i][valueField]) {
												result = false;
											}
										}
										if (result) {
											$(this).combobox('clear');
										}
								},
								">
							</select>
							<br>
                            <?= lang('notify_address');?>:
                            <br><textarea readonly name="notify_address" id="notify_address" style="height:60px;width:300px;"><?php echo $notify_address;?></textarea>
                            <br><?= lang('notify_contact');?>:
                            <br><textarea readonly name="notify_contact" id="notify_contact" style="height:40px;width:300px;"><?php echo $notify_contact;?></textarea>
                            <br><?= lang('notify_telephone');?>:
                            <br><textarea readonly name="notify_telephone" id="notify_telephone" style="height:40px;width:300px;"><?php echo $notify_telephone;?></textarea>
                            <br><?= lang('notify_email');?>:
                            <br><textarea readonly name="notify_email" id="notify_email" style="height:40px;width:300px;"><?php echo $notify_email;?></textarea>
                        </div>
                        <div title="Service Option" id="service_options_div" style="padding:10px;">
                            <input type="hidden" readonly name="service_options" id="service_options" value='<?= $service_options;?>'>
                        </div>

                    </div>
					<div id="tt">
						<!--a href="javascript:void(0)" class="icon-print" onclick="iprint();" title="print" style="margin-right:15px;"></a-->
					</div>
				</div>
				<div data-options="region:'center',tools:'#tt2',title:'Details'">
					<div id="tt2">
					</div>
                    <div style="float: left;">
						<table width="500" border="0" cellspacing="0">
                        <tr>
                            <td><?= lang('biz_type');?></td>
                            <td>
                                <select class="easyui-combobox" id="biz_type1" readonly="readonly" style="width:180px;"
                                        data-options="
                                valueField:'name',
                                textField:'name',
                                url:'/bsc_dict/get_biz_type',
                                value: '<?php echo $biz_type . ' ' . $trans_mode;?>',
                                onSelect: function(res){
                                    $('#biz_type').val(res.biz_type);
                                    $('#trans_mode').val(res.trans_mode);
                                    if(res.trans_mode == 'AIR'){
                                        $('#trans_tool').combobox('setValue', 'AIR');
                                    }else{
                                        $('#trans_tool').combobox('setValue', '');
                                    }
                                    if(res.trans_mode == 'FCL'){
                                        add_box();
                                    }else{
                                        del_box();
                                    }
                                },
                            ">
                                </select>
                                <input type="hidden" name="trans_mode" id="trans_mode" value="<?= $trans_mode;?>">
                                <input type="hidden" name="biz_type" id="biz_type" value="<?= $biz_type;?>">
                                <select name="trans_tool" id="trans_tool" class="easyui-combobox" style="width:75px;" data-options="
                                    required:true,
                                    valueField:'value',
                                    textField:'name',
                                    url:'/bsc_dict/get_option/trans_tool',
                                ">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><?= lang('booking_ETD');?> </td>
                            <td>
                                <input readonly class="easyui-datebox" name="booking_ETD" id="booking_ETD"  style="width:255px;" value="<?php echo $booking_ETD;?>"/>
                            </td>
                        </tr>


						<tr>
    						<td><?= lang('hbl_type');?></td>
    						<td>
    							<select id="hbl_type" class="easyui-combobox" name="hbl_type" style="width:255px;" data-options="
    							    required:true,
    								valueField:'value', 
    								textField:'name',
    								url:'/bsc_dict/get_hbl_type',
    								value: '<?php echo $hbl_type;?>',
    								">
    							</select>
    						</td>
						</tr>
                        <?php if(isset($box_info) && !empty($box_info)){foreach($box_info as $key => $row){
                                $this_lang = '';
                                if($key == 0){
                                    $this_lang = lang('box_info');
                                }
                            ?>

                            <tr class="add_box">
                                <td><?= $this_lang;?></td>
                                <td>
                                    <select class="easyui-combobox box_info_size" name="box_info[size][]" data-options="
                                        readonly:true,
                                        required:true,
                                        valueField:'value',
                                        textField:'value',
                                        url:'/bsc_dict/get_option/container_size',
                                        value: '<?= $row['size']?>',
                                        onHidePanel: function() {
                                            var valueField = $(this).combobox('options').valueField;
                                            var val = $(this).combobox('getValue');
                                            var allData = $(this).combobox('getData');
                                            var result = true;
                                            for (var i = 0; i < allData.length; i++) {
                                                if (val == allData[i][valueField]) {
                                                    result = false;
                                                }
                                            }
                                            if (result) {
                                                $(this).combobox('clear');
                                            }
                                        }
                                    "></select>
                                    X
                                    <input readonly required class="easyui-numberbox box_info_num"  name="box_info[num][]" value="<?= $row['num'];?>">
                                </td>
                            </tr>
                        <?php }}else{ 
                            if($trans_mode == 'FCL'){
                            $this_lang = lang('box_info');
                        ?>
                            <tr class="add_box">
                                <td><?= $this_lang;?></td>
                                <td>
                                    <select readonly class="easyui-combobox box_info_size" name="box_info[size][]" data-options="
                                        required:true,
                                        valueField:'value',
                                        textField:'value',
                                        url:'/bsc_dict/get_option/container_size',
                                        onHidePanel: function() {
                                            var valueField = $(this).combobox('options').valueField;
                                            var val = $(this).combobox('getValue');
                                            var allData = $(this).combobox('getData');
                                            var result = true;
                                            for (var i = 0; i < allData.length; i++) {
                                                if (val == allData[i][valueField]) {
                                                    result = false;
                                                }
                                            }
                                            if (result) {
                                                $(this).combobox('clear');
                                            }
                                        }
                                    "></select>
                                    X
                                    <input readonly required class="easyui-numberbox box_info_num"  name="box_info[num][]" value="">
                                </td>
                            </tr>
                        <?php }}?>
                        <tr>
                            <td><?= lang('booking_ETD');?> </td>
                            <td>
                                <input class="easyui-datebox" readonly="readonly" name="booking_ETD" id="booking_ETD"  style="width:255px;" value="<?php echo $booking_ETD;?>"/>
                            </td>
                        </tr>
						<tr>
						<td><?= lang('shipper_ref');?> </td>
						<td>
							<input readonly class="easyui-textbox" name="shipper_ref" id="shipper_ref"  style="width:255px;" value="<?php echo $shipper_ref;?>"/>
						</td>
						</tr>
						<tr>
						<td><?= lang('release_type');?> </td>
						<td>
							<select id="release_type" class="easyui-combobox" name="release_type" style="width:255px;" data-options="required:true,
								valueField:'value', 
								textField:'value', 
								url:'/bsc_dict/get_option/release_type', 
								value:'<?php echo $release_type;?>'
								">
							</select>
						</td>
						</tr>
                            <tr>
                                <td><?= lang('update_trans_origin');?></td>
                                <td>
                                    <select id="trans_origin" class="easyui-combobox"   name="trans_origin" style="width:80px;" data-options="required:true,
                                    readonly:true,
    								valueField:'port_code',
    								textField:'port_code',
                                    url:'/biz_port/get_option',
    								onSelect: function(rec){
    								    if(rec !== undefined){
                                            $('#trans_origin_name').combobox('setValue', rec.port_name);
    								    }
    								},
    								onLoadSuccess: function(){
    								    var data = $(this).combobox('getData');
                                        $('#trans_origin_name').combobox('loadData', data);
                                        $('#trans_discharge').combobox('loadData', data);
                                        $('#trans_discharge_name').combobox('loadData', data);
                                        $('#trans_destination').combobox('loadData', data);
                                        $('#trans_destination_name').combobox('loadData', data);
                                        $('#sailing_area').combobox('loadData', data);
                                        $('#sailing').combobox('loadData', data);
                                        $(this).combobox('select', '<?php echo $trans_origin;?>');
    								},
    								">
                                    </select>
                                    <select name="trans_origin_name" id="trans_origin_name" class="easyui-combobox"  style="width:170px;" data-options="required:true,
                                    readonly:true,
    								valueField:'port_name',
    								textField:'port_name',
    								onSelect: function(rec){
    								    $('#trans_origin').combobox('setValue', rec.port_code);
    								},
    								">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_trans_discharge');?></td>
                                <td>
                                    <select id="trans_discharge" class="easyui-combobox" name="trans_discharge" style="width:80px;" data-options="required:true,
                                    readonly:true,
    								valueField:'port_code',
    								textField:'port_code',
    								onSelect: function(rec){
                                        if(rec !== undefined){
    								        $('#trans_discharge_name').combobox('setValue', rec.port_name);
    								    }
    								},
									onLoadSuccess: function(){
                                        $(this).combobox('select', '<?php echo $trans_discharge;?>');
    								},
    								">
                                    </select>
                                    <select name="trans_discharge_name" id="trans_discharge_name" class="easyui-combobox"   style="width:170px;" data-options="required:true,
                                    readonly:true,
    								valueField:'port_name',
    								textField:'port_name',
    								onSelect: function(rec){
    								    $('#trans_discharge').combobox('setValue', rec.port_code);
    								},
    								">
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('update_trans_destination');?></td>
                                <td>
                                    <select id="trans_destination" class="easyui-combobox" name="trans_destination" style="width:80px;" data-options="required:true,
                                    readonly:true,
    								valueField:'port_code',
    								textField:'port_code',
    								onSelect: function(rec){
    								    if(rec !== undefined){
    								        $('#trans_destination_name').combobox('setValue', rec.port_name);
    								        $('#sailing_area').textbox('setValue', rec.sailing_area);
    								        $('#sailing').textbox('setValue', rec.sailing);
    								    }
    								},
    								onLoadSuccess: function(){
                                        $(this).combobox('select', '<?php echo $trans_destination;?>');
    								},
    								">
                                    </select>
                                    <select name="trans_destination_name" id="trans_destination_name" class="easyui-combobox" readonly="readonly"  style="width:170px;" data-options="required:true,
                                    readonly:true,
    								valueField:'port_name',
    								textField:'port_name',
    								onSelect: function(rec){
    								    $('#trans_destination').combobox('setValue', rec.port_code);
                                        $('#sailing_area').textbox('setValue', rec.sailing_area);
                                        $('#sailing').textbox('setValue', rec.sailing);
    								},
    								">
                                    </select>
                                </td>
                            </tr>
						<tr>
						<td><?= lang('trans_carrier');?></td>
						<td>
							<select id="trans_carrier" class="easyui-combobox" name="trans_carrier" style="width:255px;" data-options="
							    required:true,
								valueField:'client_code',
                                textField:'client_name',
								url:'/biz_client/get_option/carrier',
								value:'<?php echo $trans_carrier;?>',
								onHidePanel: function() {
                                    var valueField = $(this).combobox('options').valueField;
                                    var val = $(this).combobox('getValue');
                                    var allData = $(this).combobox('getData');
                                    var result = true;
                                    for (var i = 0; i < allData.length; i++) {
                                        if (val == allData[i][valueField]) {
                                            result = false;
                                        }
                                    }
                                    if (result) {
                                        $(this).combobox('clear');
                                    }
                                },
							">
							</select>
						</td>
						</tr>
						<tr>
                            <td><?= lang('trans_term');?></td>
                            <td>
                                <select id="trans_term" class="easyui-combobox" editable="false"  name="trans_term" style="width:255px;" data-options="required:true,
                                value:'<?= $trans_term;?>',
                                valueField:'value',
                                textField:'value',
                                url:'/bsc_dict/get_option/trans_term',
                                ">
                                </select>
                            </td>
                        </tr>
						<tr>
    						<td><?= lang('INCO_term');?></td>
    						<td>
    							<select id="INCO_term" class="easyui-combobox"  editable="false" name="INCO_term" style="width:255px;" data-options="
    							    required:true,
    								valueField:'value', 
    								textField:'value', 
    								url:'/bsc_dict/get_option/INCO_term',
    								onSelect: function(rec){
                                        INCO_term_info(rec.value);
                                    },
                                    onLoadSuccess: function(){
                                        $('#INCO_term').combobox('select', '<?php echo $INCO_term;?>');
                                    }
    								">
    							</select>
    
    						</td>
						</tr>
                        <tr class="INCO_term_option">
                                <td><?= lang('INCO_option');?></td>
                                <td>
                                    <input type='checkbox' name='INCO_option[]' value='custom' style='vertical-align:middle;'><?= lang('custom');?>
                                    <input type='checkbox' name='INCO_option[]' value='turcking' style='vertical-align:middle;'><?= lang('turcking');?>
                                    <input type='checkbox' name='INCO_option[]' value='warehouse' style='vertical-align:middle;'><?= lang('warehouse');?>
                                </td>
                            </tr>
                            <tr class="INCO_term_option">
                                <td><?= lang('INCO_address');?></td>
                                <td>
                                    <textarea name="INCO_address" id="INCO_address" cols="30" style="height: 60px;width: 248px;"></textarea>
                                </td>
                            </tr>
                        <tr>
                            <td><?= lang('requirements');?></td>
                            <td>
                                <textarea readonly name="requirements" id="requirements" style="height:60px;width:248px;"><?php echo $requirements;?></textarea><br>
                            </td>
                        </tr>
                        <tr>
    						<td colspan="2">
                                --------------------------------------------------------------------------
                            </td>
						</tr>
                        <?php
						foreach($userList as $key => $value){
                            $group_key = $key . '_group';
                            ?>
							<tr>
							<td><?php echo lang($key); ?></td>
							<td>
							<select class="easyui-combobox userList" id="<?php echo $key;?>_id" editable="true" name="<?php echo $key;?>_id" style="width:255px;" data-options="
									valueField:'id', 
									textField:'name', 
									url:'/bsc_user/get_data_role/<?php echo $key; ?>',  
									onSelect: function(rec){
									    $('#<?= $key;?>_group').val(rec.group);
									},
									">
                            </select>
                                <input type="hidden" id="<?= $key;?>_group" name="<?= $key;?>_group" style="width:255px;">
                            </tr>
						<?php
						}
						?>	

					</table>
                    </div>
                    <div style="float: left;width: 400px;">
                        <table width="400" border="0" cellspacing="0">
                            <tr>
                                <td><?= lang('description');?> </td>
                                <td>
                                    <textarea readonly name="description" id="description" style="height:60px;width:245px;"/><?php echo $description;?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('description_cn');?> </td>
                                <td>
                                    <textarea readonly name="description_cn" id="description_cn" style="height:60px;width:245px;"/><?php echo $description;?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('mark_nums');?> </td>
                                <td>
                                    <textarea readonly name="mark_nums" id="mark_nums" style="height:60px;width:245px;"/><?php echo $mark_nums;?></textarea>
                                </td>
                            </tr>
                           
                            
                            <tr>
                                <td><?= lang('good_outers');?> </td>
                                <td>
                                    <input readonly class="easyui-textbox" name="good_outers" id="good_outers"  style="width:155px;" value="<?php echo $good_outers;?>"/>
                                    <select name="good_outers_unit" id="good_outers_unit"  class="easyui-combobox" readonly="true"  required="true" style="width:100px;">
                                        <option value="<?php echo $good_outers_unit;?>"><?php echo $good_outers_unit;?></option>
                                        <option value="CTN">CTN</option>
                                        <option value="Kg">Pkg</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('good_weight');?> </td>
                                <td>
                                    <input class="easyui-textbox" readonly name="good_weight" id="good_weight"  style="width:155px;" value="<?php echo $good_weight;?>"/>
                                    <select name="good_weight_unit" readonly="true" id="good_weight_unit"  class="easyui-combobox"  required="true" style="width:100px;">
                                        <option value="<?php echo $good_weight_unit;?>"><?php echo $good_weight_unit;?></option>
                                        <option value="Kg" selected="selected">Kg</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('good_volume');?> </td>
                                <td>
                                    <input readonly class="easyui-textbox"  name="good_volume" id="good_volume"  style="width:155px;" value="<?php echo $good_volume;?>"/>
                                    <select readonly="true" name="good_volume_unit"  id="good_volume_unit"  class="easyui-combobox"  required="true" style="width:100px;">
                                        <option value="<?php echo $good_volume_unit;?>"><?php echo $good_volume_unit;?></option>
                                        <option value="m3" selected="selected">m3</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('goods_type');?></td>
                                <td>
                                    <select class="easyui-combobox" name="goods_type" id="goods_type" style="width: 255px;" data-options="
                                    required: true,
                                    valueField:'value',
                                    textField:'valuename',
                                    url:'/bsc_dict/get_option/goods_type',
                                    value: '<?= $goods_type;?>',
                                    onSelect:function(rec){
                                        dangerous_info(rec.value);
                                    },
                                ">

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td><?= lang('hs_code');?> </td>
                                <td>
                                    <input readonly class="easyui-textbox"  name="hs_code" id="hs_code"  style="width:255px;" value="<?php echo $hs_code;?>" prompt="<?= lang('Please separate them with commas');?>"/>
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('isdangerous');?></td>
                                <td>
                                    <input class="easyui-textbox" id="isdangerous" name="isdangerous" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('ism_spec');?></td>
                                <td>
                                    <input class="easyui-textbox" id="ism_spec" name="ism_spec" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('UN_no');?></td>
                                <td>
                                    <input class="easyui-textbox" id="UN_no" name="UN_no" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('IMDG_page');?></td>
                                <td>
                                    <input class="easyui-textbox" id="IMDG_page" name="IMDG_page" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('packing_type');?></td>
                                <td>
                                    <input class="easyui-textbox" id="packing_type" name="packing_type" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('flash_point');?></td>
                                <td>
                                    <input class="easyui-textbox" id="flash_point" name="flash_point" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('IMDG_code');?></td>
                                <td>
                                    <input class="easyui-textbox" id="IMDG_code" name="IMDG_code" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('is_pollution_sea');?></td>
                                <td>
                                    <input class="easyui-textbox" id="is_pollution_sea" name="is_pollution_sea" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('ems_no');?></td>
                                <td>
                                    <input class="easyui-textbox" id="ems_no" name="ems_no" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('MFAG_NO');?></td>
                                <td>
                                    <input class="easyui-textbox" id="MFAG_NO" name="MFAG_NO" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('e_contact_name');?></td>
                                <td>
                                    <input class="easyui-textbox" id="e_contact_name" name="e_contact_name" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="dangerous">
                                <td><?= lang('e_contact_tel');?></td>
                                <td>
                                    <input class="easyui-textbox" id="e_contact_tel" name="e_contact_tel" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="special_size">
                                <td><?= lang('long');?></td>
                                <td>
                                    <input class="easyui-textbox" id="long" name="long" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="special_size">
                                <td><?= lang('wide');?></td>
                                <td>
                                    <input class="easyui-textbox" id="wide" name="wide" style="width: 255px;">
                                </td>
                            </tr>
                            <tr class="special_size">
                                <td><?= lang('high');?></td>
                                <td>
                                    <input class="easyui-textbox" id="high" name="high" style="width: 255px;">
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
			</div>
			</form>
		</div>
	</div>
</body>