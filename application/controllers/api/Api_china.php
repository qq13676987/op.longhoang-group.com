<?php

class api_china extends Common_Controller
{
    public function __construct()
    {
          parent::__construct();
          Model('m_model');
    }
    
    /**
     * 给其他系统使用的ebooking接口
     */
    public function ebooking(){
        $post_json = file_get_contents("php://input"); 
        $post = json_decode($post_json, true);
        if(empty($post)) return jsonEcho(array('code' => 1, 'msg' => lang('参数不能为空')));
        
        //这里需要一个验证, 如果关单号存在, 且当钱有一个状态未处理完毕的话,直接跳过返回回去
        $shipment_add_datas = array();
        $get_fields = array(
            'biz_type','client_company2','client_company', 'trans_carrier', 'agent_company','client_email','client_telephone','client_contact',
            'trans_mode','trans_tool','trans_term','shipper_ref','description','description_cn','mark_nums','release_type','release_type_remark',
            'trans_origin','trans_origin_name','trans_destination','trans_destination_terminal','trans_destination_name','booking_ETD',
            'good_outers','good_outers_unit','good_weight','good_weight_unit','good_volume','good_volume_unit','good_describe','good_commodity',
            'INCO_term','hbl_type','box_info','goods_type','dangergoods_id','requirements','service_options','remark','operator_id','operator_group',
            'sales_id','sales_group','customer_service_id','customer_service_group','free_svr', 'trans_discharge','trans_discharge_name',
            'trans_destination_inner','carrier_ref','cus_no', 'INCO_address'
        );
        
        $success = 0;
        $fail = 0;
        $error_info = array();
        foreach ($post['shipments'] as $shipment){
            $add_data = array(
                'created_id_type' => 2,//创建人类型, 2代表接口
                'updated_by' => 0,//更新人
                'updated_time' => date('Y-m-d H:i:s'),//更新时间
            );
            
            foreach ($get_fields as $get_field){
                // if(!isset($shipment[$get_field])) $shipment[$get_field] = '';
                if(isset($shipment[$get_field])){
                    $add_data[$get_field] = $shipment[$get_field];
                }
            }
            
            //名称等要根据当时情况,转化为自己的客户代码
            //第二委托方--start
            if(empty($add_data['client_company2'])){
                $fail++;
                $error_info[] = lang("{shipper_ref} 的 {field} 必填", array('shipper_ref' => $shipment['shipper_ref'], 'field' => lang('第二委托方')));
                continue;
            }
            $this->db->select('client_code');
            $temp_row = Model('biz_client_model')->get_where_one("company_name = '{$add_data['client_company2']}'");
            if(empty($temp_row)){
                $fail++;
                $error_info[] = lang("{shipper_ref} 的 未获取到 \"{client_company}\" 对应客户信息", array('shipper_ref' => $shipment['shipper_ref'], 'client_company' => $add_data['client_company2']));
                continue;
            }
            unset($add_data['client_company2']);
            $add_data['client_code2'] = $temp_row['client_code'];
            //第二委托方--end
            
            //委托方--start
            if(empty($add_data['client_company'])){
                $fail++;
                $error_info[] = lang("{shipper_ref} 的 {field} 必填", array('shipper_ref' => $shipment['shipper_ref'], 'field' => '委托方'));
                continue;
            }
            $this->db->select('client_code');
            $temp_row = Model('biz_client_model')->get_where_one("company_name = '{$add_data['client_company']}'");
            if(empty($temp_row)){
                $fail++;
                $error_info[] = lang("{shipper_ref} 的 未获取到 \"{client_company}\" 对应客户信息", array('shipper_ref' => $shipment['shipper_ref'], 'client_company' => $add_data['client_company']));
                continue;
            }
            $add_data['client_code'] = $temp_row['client_code'];
            //委托方--end
            
            //代理--start
            if(empty($add_data['agent_company'])){
                $fail++;
                $error_info[] = lang("{shipper_ref} 的 {field} 必填", array('shipper_ref' => $shipment['shipper_ref'], 'field' => '代理'));
                continue;
            }
            $this->db->select('client_code');
            $temp_row = Model('biz_client_model')->get_where_one("company_name = '{$add_data['agent_company']}'");
            if(empty($temp_row)){
                $fail++;
                $error_info[] = lang("{shipper_ref} 的 未获取到 \"{agent_company}\" 对应代理信息", array('shipper_ref' => $shipment['shipper_ref'], 'agent_company' => $add_data['agent_company']));
                continue;
            }
            unset($add_data['agent_company']);
            $add_data['agent_code'] = $temp_row['client_code'];
            //代理--end
            
            //船公司--start
            if(empty($add_data['trans_carrier'])){
                $fail++;
                $error_info[] = lang("{shipper_ref} 的 {field} 必填", array('shipper_ref' => $shipment['shipper_ref'], 'field' => lang('船公司')));
                continue;
            }
            $this->db->select('client_code');
            $temp_row = Model('biz_client_model')->get_where_one("client_name = '{$add_data['trans_carrier']}'");
            if(empty($temp_row)){
                $fail++;
                $error_info[] = lang("{shipper_ref} 的 未获取到 \"{trans_carrier}\" 对应承运人信息", array('shipper_ref' => $shipment['shipper_ref'], 'trans_carrier' => $add_data['trans_carrier']));
                continue;
            }
            $add_data['trans_carrier'] = $temp_row['client_code'];
            //船公司--end
            
            //查询当前关单号是否存在 通过或者拒绝的不处理
            $this->db->limit(1);
            $ebks = Model('biz_shipment_ebooking_model')->get("shipper_ref = '{$shipment['shipper_ref']}'", 'id', 'desc');
            if(!empty($ebks)){
                $ebk = $ebks[0];
                //如果通过了就不再发了,其他情况都进行修改
                if($ebk['status'] === '1'){
                    $fail++;
                    $error_info[] = lang("{shipper_ref} 的 ebooking已通过,请勿重复发送", array('shipper_ref' => $shipment['shipper_ref']));
                    continue;
                }
                
                //如果存在,把一样的数据剔除
                foreach ($add_data as $key => $val){
                    if(isset($ebk[$key])){
                        if($ebk[$key] == $val) unset($add_data[$key]);
                    }
                }
                
                //如果存在将id塞进去
                $add_data['id'] = $ebk['id'];
            }
            
            $success++;
            $shipment_add_datas[] = $add_data;
        }
        
        
        //接下来开始新增ebooking
        foreach ($shipment_add_datas as $shipment_add_data){
            //检测ID是否存在
            if(isset($shipment_add_data['id'])){
                $id = $shipment_add_data['id'];
                
                $action = "update";
                $this->db->where('id', $id)->update("biz_shipment_ebooking", $shipment_add_data);
            }else{
                $shipment_add_data['created_by'] = 0;//创建人
                $shipment_add_data['created_group'] = '';//创建组
                $shipment_add_data['created_time'] = date('Y-m-d H:i:s');//创建时间
                //2023-03-02 迪拜的简化,改为待审核,下一步直接提交
                // $shipment_add_data['status'] = -2;//默认为待提交状态
                $shipment_add_data['status'] = 0;//默认为待提交状态
                
                $action = "insert";
                $this->db->insert("biz_shipment_ebooking", $shipment_add_data);
                $id = $this->db->insert_id();
            }
            
            if($id){
                $log_data = array();
                $log_data["table_name"] = "biz_shipment_ebooking";
                $log_data["key"] = $id;
                $log_data["action"] = $action;
                $log_data["value"] = json_encode($shipment_add_data);
                log_rcd($log_data);
            }
        }
        
        $error_info_str = "";
        if(!empty($error_info)) $error_info_str = lang('错误信息') . ":<br />" . join('<br />', $error_info);
        
        return jsonEcho(array('code' => 0, 'msg' => lang("接收成功! 更新{success}条, 失败{fail}条", array('success' => $success, 'fail' => $fail)) . "<br/> " . $error_info_str));
    }
}