<?php
   class M_model extends CI_Model{
        
        public static $table;
       
        public function __construct(){
            $this->load->database();
        } 
			
        public function sqlquery($sql=""){
			if($sql!=""){
				return $this->db->query($sql);
			}			
        } 
        public function query_array($sql=""){
			if($sql!=""){
				$rs = $this->db->query($sql);
				$arr = $rs->result_array();
				return $arr;
			}			
        } 
        public function query_one($sql=""){
			if($sql!=""){
				$rs = $this->db->query($sql);
				$row = $rs->row_array();
				return $row;
			}			
        } 
        public function query($sql=""){
			if($sql!=""){
				$reid=$this->db->query($sql);
				return $reid;
			}			
        }


       public function query_id($sql=""){
           if($sql!=""){
               if($this->db->query($sql)) {
                   $reid=$this->db->insert_id();
               }
               else{
                   $reid=0;
               }


               return $reid;
           }
       }

		
		public function getmaxid($tab){
			$this->db->select_max('id');
			$query=$this->db->get($tab);
			return $query->row_array();
		}
		
		public function get($where = "", $sort = "id", $order = "desc")
        {
            if ($where != "") $this->db->where($where);

            $this->db->order_by($sort, $order);

            $query = $this->db->get(self::$table);
            return $query->result_array();
       }
       
        public function total($where='')
        {
            if ($where != '') $this->db->where($where);
            $this->db->from(self::$table);
            return $this->db->count_all_results('');
        }
		
       public function get_one($f="",$v=""){
           $this->db->where($f, $v);
           $query = $this->db->get(self::$table);
           return $query->row_array();
       }

       public function get_one_where($where){
           $this->db->where($where);
           $query = $this->db->get(self::$table);
           return $query->row_array();
       }

       public function save($data=array()){
           $this->db->insert(self::$table, $data);
           return $this->db->insert_id();
       }

       public function update($id='',$data=array()){
           $this->db->where('id', $id);
           $this->db->update(self::$table, $data);
           return $id;
       }
       
       public function mdelete($id = '')
       {
           $this->db->where('id', $id);
           $this->db->delete(self::$table);
       }
       
       public function delete_where($where = '')
       {
           if(empty($where)) return false;
           $this->db->where($where);
           $this->db->delete(self::$table);
       }
   }