<title>Edit</title>
<style type="text/css">
	.input{
		width: 255px;
	}
	.select{
		width: 262px;
	}
	.textarea{
		width: 255px;
		height: 100px;
	}
	.box_info_num{
		width: 100px;
	}
	.box_info_size{
		width: 100px;
	}
	.box{
		min-width: 1178px;
		display: flex;
		/*border: 1px solid black;*/
		margin: 20px;
	}
	.box-left{
		padding-left: 30px;
		margin-right: 30px;
		flex-grow:2;
		max-width: 386px;
	}
	.box-right{
		margin-right: 30px;
		flex-grow:3;
	}
	.box-title{
	}
	.box-title-span{
		font-size: 24px;
		font-weight: bold;
	}
	.box-hr{
		padding: 5px 0;
	}
	.box-left-input{
		padding: 5px 0;
	}
	.box-left-input-label{
		width:81px;
		margin-right: 19px;
		display:inline-block;
		text-align: right;
	}

</style>
<script type="text/javascript">
    function query_report(form_id, combo_id){
        var where = {};
        var form_data = $('#' + form_id).serializeArray();
        $.each(form_data, function (index, item) {
            if(item.value == "") return true;
            if(where.hasOwnProperty(item.name) === true){
                if(typeof where[item.name] == 'string'){
                    where[item.name] =  where[item.name].split(',');
                    where[item.name].push(item.value);
                }else{
                    where[item.name].push(item.value);
                }
            }else{
                where[item.name] = item.value;
            }
        });
        if(where.length == 0){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请填写需要搜索的值');?>');
            return;
        }
        //判断一下当前是 remote 还是local,如果是remode,走下面这个
        var inp = $('#' + combo_id);
        var opt = inp.combogrid('options');
        if(opt.mode == 'remote'){
            inp.combogrid('grid').datagrid('load',where);
        }else{
            //如果是本地的,那么先存一下
            if(opt.old_data == undefined) opt.old_data = inp.combogrid('grid').datagrid('getData'); // 如果没存过,这里存一下
            var rows = [];
            if(JSON.stringify(where) === '{}'){
                inp.combogrid('grid').datagrid('loadData', opt.old_data.rows);
                return;
            }
            $.each(opt.old_data.rows,function(i,obj){
                for(var p in where){
                    var q = where[p].toUpperCase();
                    var v = obj[p].toUpperCase();
                    if (!!v && v.toString().indexOf(q) >= 0){
                        rows.push(obj);
                        break;
                    }
                }
            });
            if(rows.length == 0) {
                inp.combogrid('grid').datagrid('loadData', []);
                return;
            }

            inp.combogrid('grid').datagrid('loadData',rows);
        }
    }

    //text添加输入值改变
    function keydown_search(e, click_id){
        if(e.keyCode == 13){
            $('#' + click_id).trigger('click');
        }
    }

    function cancel(){
        $.messager.confirm('<?= lang('提示');?>', '<?= lang('confirm to reject the Eboooking?');?>', function (r) {
            if(r){
                $.ajax({
                    url: '/biz_edi_ebooking_consol/cancel_status?id=<?= $id;?>',
                    type: 'post',
                    data:{
                        status: -1,
                    },
                    success: function (res_json) {
                        var res;
                        try{
                            res = $.parseJSON(res_json);
                        }catch(e){

                        }
                        if(res == undefined){
                            $.messager.alert('Tips', res);
                            return;
                        }
                        $.messager.alert('Tips', res.msg);
                        if(res.code == 0){
                            location.reload();
                        }else{
                        }
                    },
                });
            }
        });
    }
    /**
     * 下拉选择表格展开时,自动让表单内的查询获取焦点
     */
    function combogrid_search_focus(form_id, focus_num = 0){
        // var form = $(form_id);
        setTimeout("easyui_focus($($('" + form_id + "').find('.keydown_search')[" + focus_num + "]));",200);
    }

    /**
     * easyui 获取焦点
     */
    function easyui_focus(jq){
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combogrid !== undefined){
            return $(jq).combogrid('textbox').focus();
        }else if(data.combobox !== undefined){
            return $(jq).combobox('textbox').focus();
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('textbox').focus();
        }else if(data.datebox !== undefined){
            return $(jq).datebox('textbox').focus();
        }else if(data.textbox !== undefined){
            return $(jq).textbox('textbox').focus();
        }
    }

    $(function () {

        $.each($('.keydown_search'), function (i,it) {
            $(it).textbox('textbox').bind('keydown', function(e){keydown_search(e, $(it).attr('t_id'));});
        });
    });
</script>
<body>
<?php
function top_reminder($html=""){
	echo '<div style="background:#e0ecff;border: 1px solid #95B8E7;padding:12px 20px"> 
                <span class="l-btn-left l-btn-icon-left">
                    <span class="l-btn-icon icon-tip" style="top:40%;left: 1px">&nbsp;</span>
                    <span class="l-btn-text">' . lang("Tips") . ': <span style="color:red;font-weight:bold;">   '.$html.' </span>  </span> 
                </span> 
                
            </div>';
}
top_reminder("This is pre-alert EDI data from oversea system, you can create shipment and consol by one click after you confirm with POL team the data is correct...  Once you created, it cannot be revoked! ");
?>
<div class="box">
	<div class="box-left">
		<form id="fm" method="post">
			<div class="box-title">
				<span class="box-title-span"><?= lang('Consol');?></span>
			</div>
			<div class="box-hr">
				<hr>
			</div>
			<div class="box-left-input">
				<label class="box-left-input-label" for=""><?= lang('From');?>:</label>
				<input class="easyui-textbox input" readonly name="from_db" id="from_db" value="<?= $from_db;?>">
			</div>
			<div class="box-left-input">
				<label class="box-left-input-label" for=""><?= lang('job no');?>:</label>
				<input class="easyui-textbox input" readonly name="job_no" id="job_no" value="<?= $job_no;?>">
			</div>
			<div class="box-left-input">
				<label class="box-left-input-label" for=""><?= lang('status');?>:</label>
				<select name="status" class="easyui-combobox select" readonly data-options="value:<?=$status?>,editable:false" id="status">
					<option value="0"><?=lang('to do')?></option>
					<option value="1"><?=lang('passed')?></option>
					<option value="-1"><?=lang('reject')?></option>
				</select>
			</div>
			<div class="box-left-input">
				<label class="box-left-input-label" for=""><?= lang('ETD');?>:</label>
				<input class="easyui-datetimebox input" data-options="readonly:true" name="ETD" value="<?=$ETD=='0000-00-00 00:00:00'?'':$ETD;?>"/>
			</div>

			<div class="box-left-input">
				<label class="box-left-input-label" for=""><?= lang('ATD');?>:</label>
				<input class="easyui-datetimebox input" data-options="readonly:true" name="ATD" value="<?=$ATD=='0000-00-00 00:00:00'?'':$ATD;?>"/>
			</div>
			<div class="box-left-input">
				<label class="box-left-input-label" for=""><?= lang('ETA');?>:</label>
				<input class="easyui-datetimebox input" data-options="readonly:true" name="ETA" value="<?=$ETA=='0000-00-00 00:00:00'?'':$ETA;?>"/>
			</div>
			<div class="box-left-input">
				<label class="box-left-input-label" for=""><?= lang('POL');?>:</label>
				<select class="easyui-combogrid select" name="POL" id="trans_origin_name" data-options="
                    readonly:true,
                    panelWidth: '1000px',
                    panelHeight: '300px',
                    url:'/biz_port/get_option?carrier=<?= $carrier;?>&limit=true',
                    idField: 'port_code',              //ID字段
                    textField: 'port_name',    //显示的字段
                    striped: true,
                    editable: false,
                    pagination: false,           //是否分页
                    toolbar : '#trans_origin_div',
                    collapsible: true,         //是否可折叠的
                    method: 'get',
                    columns:[[
                        {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                        {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                        {field:'terminal',title:'<?= lang('码头');?>',width:200},
                    ]],
                    mode:'remote',
                    value: '<?= $POL;?>',
                    emptyMsg : '<?= lang('未找到相应数据!');?>',
                    readonly:true,
                    onSelect: function(index, row){
                        if(row !== undefined){
                            $('#trans_origin').textbox('setValue', row.port_code);
                        }
                    },
                    onShowPanel:function(){
                        var opt = $(this).combogrid('options');
                        var toolbar = opt.toolbar;
                        combogrid_search_focus(toolbar);
                    },
                ">
				</select>
			</div>
			<div class="box-left-input">
				<label class="box-left-input-label" for=""><?= lang('POD');?>:</label>
				<select class="easyui-combogrid select" name="trans_destination" id="POD" data-options="
                    readonly:true,
                    panelWidth: '1000px',
                    panelHeight: '300px',
                    idField: 'port_code',              //ID字段
                    textField: 'port_name',    //显示的字段
                    url:'/biz_port/get_discharge?carrier=<?= $carrier?>',
                    striped: true,
                    editable: false,
                    pagination: false,           //是否分页
                    toolbar : '#trans_destination_div',
                    collapsible: true,         //是否可折叠的
                    method: 'get',
                    columns:[[
                        {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                        {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                        {field:'terminal',title:'<?= lang('码头');?>',width:200},
                    ]],
                    mode:'remote',
                    value: '<?= $POD;?>',
                    emptyMsg : '<?= lang('未找到相应数据!');?>',
                    onShowPanel:function(){
                        var opt = $(this).combogrid('options');
                        var toolbar = opt.toolbar;
                        combogrid_search_focus(toolbar, 1);
                    },
                ">
				</select>
			</div>
			<div class="box-left-input">
				<label class="box-left-input-label" for=""><?= lang('vessel');?>:</label>
				<input class="easyui-textbox input" readonly name="vessel" id="vessel" value="<?= $vessel;?>">
			</div>
			<div class="box-left-input">
				<label class="box-left-input-label" for=""><?= lang('voyage');?>:</label>
				<input class="easyui-textbox input" readonly name="voyage" id="voyage" value="<?= $voyage;?>">
			</div>
			<div class="box-left-input">
				<label class="box-left-input-label" for=""><?= lang('carrier');?>:</label>
				<select class="easyui-combobox select" name="carrier" id="carrier" data-options="
                    readonly:true,
                    valueField:'client_code',
				    textField:'client_name',
                    url:'/biz_client/get_option/carrier',
                    value:'<?php echo $carrier;?>',
                    onHidePanel: function() {
                        var valueField = $(this).combobox('options').valueField;
                        var val = $(this).combobox('getValue');
                        var allData = $(this).combobox('getData');
                        var result = true;
                        for (var i = 0; i < allData.length; i++) {
                            if (val == allData[i][valueField]) {
                                result = false;
                            }
                        }
                        if (result) {
                            $(this).combobox('clear');
                        }
                    },
                ">
				</select>
			</div>
			<div class="box-left-input">
				<label class="box-left-input-label" for=""><?= lang('操作');?>:</label>
				<select class="easyui-combogrid select" id="operator_id" name="operator_id" data-options="
                    panelWidth: '1000px',
                    panelHeight: '300px',
                    idField: 'id',              //ID字段
                    textField: 'name',    //显示的字段
                    striped: true,
                    editable: false,
                    pagination: false,           //是否分页
                    toolbar : '#operator_id_div',
                    collapsible: true,         //是否可折叠的
                    method: 'post',
                    columns:[[
                        {field:'id',title:'<?= lang('id');?>',width:60},
                        {field:'name',title:'<?= lang('name');?>',width:80},
                        {field:'group',title:'<?= lang('group');?>',width:100},
                    ]],
                    emptyMsg : '<?= lang('未找到相应数据!');?>',
                    required:true,
                    mode: 'remote',
                    onShowPanel:function(){
                        var opt = $(this).combogrid('options');
                        var toolbar = opt.toolbar;
                        combogrid_search_focus(toolbar);
                    },
                    onSelect: function(){
                    	save()
                    },
                    url:'/bsc_user/get_data_role/operator?status=0',
                    value:'<?= $operator_id;?>',
                ">
				</select>
			</div>
			<div class="box-left-input">
				<label class="box-left-input-label" for=""><?= lang('客服');?>:</label>
				<select class="easyui-combogrid select" id="customer_service_id" name="customer_service_id" data-options="
                    panelWidth: '1000px',
                    panelHeight: '300px',
                    idField: 'id',              //ID字段
                    textField: 'name',    //显示的字段
                    striped: true,
                    editable: false,
                    pagination: false,           //是否分页
                    toolbar : '#customer_service_id_div',
                    collapsible: true,         //是否可折叠的
                    method: 'post',
                    columns:[[
                        {field:'id',title:'<?= lang('id');?>',width:60},
                        {field:'name',title:'<?= lang('name');?>',width:80},
                        {field:'group',title:'<?= lang('group');?>',width:100},
                    ]],
                    emptyMsg : '<?= lang('未找到相应数据!');?>',
                    required:true,
                    mode: 'remote',
                    url:'/bsc_user/get_data_role/customer_service?status=0',
                    value: '<?= $customer_service_id;?>',
                    onSelect: function(){
                    	save()
                    },
                    onShowPanel:function(){
                        var opt = $(this).combogrid('options');
                        var toolbar = opt.toolbar;
                        combogrid_search_focus(toolbar);
                    },
                ">
				</select>
			</div>
			<div class="box-left-input" style="text-align: right;">
				<?php
				// SI状态: 0待操作审核,1已通过,-1已拒绝，-2待销售审核
				if($status == 0): ?>
					<a href="javascript:void(0);" class="easyui-linkbutton" onclick="create_data()"><?= lang('Create LH Shipment and Consol');?></a>
					<a href="javascript:void(0);" class="easyui-linkbutton" onclick="cancel()"><?= lang('拒绝');?></a>
				<?php endif;?>
				<?php if($status == 1): ?>
					<a href="javascript:void(0);" class="easyui-linkbutton" onclick="window.open('/biz_consol/edit/<?=$consol_id?>')"><?= lang('view relative Consol in LH system');?></a>
				<?php endif;?>
				<br>

			</div>
		</form>
	</div>
	<div class="box-right">
		<div class="box-title">
			<span class="box-title-span"><?= lang('Shipment');?></span>
		</div>
		<div class="box-hr">
			<hr>
		</div>
		<div class="box-right-iframe">
			<table class="easyui-datagrid" style="width:910px;height:675px"
				   data-options="url:'/biz_edi_ebooking_consol/get_shipment/<?=$id_no?>/?db=<?= $from_db;?>',fitColumns:true,singleSelect:true,nowrap:false">
				<thead>
				<tr>
					<th data-options="field:'job_no',width:120"><?=lang('Shipment No')?></th>
					<th data-options="field:'trans_mode',width:80"><?=lang('FCL/LCL')?></th>
					<th data-options="field:'box_str',width:180"><?=lang('BOXES')?></th>
					<th data-options="field:'shipper_company',width:180"><?=lang('SHIPPER')?></th>
					<th data-options="field:'consignee_company',width:180"><?=lang('CONSIGNEE')?></th>
					<th data-options="field:'notify_company',width:180"><?=lang('NOTIFY')?></th>
					<th data-options="field:'description',width:180"><?=lang('GOODS DESCRIPTION')?></th>
				</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

<div id="trans_origin_div">
	<form id="trans_origin_form" method="post">
		<div style="padding-left: 5px;display: inline-block;">
			<label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_origin_click"/>
			<label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_origin_click"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" id="trans_origin_click" iconCls="icon-search" onclick="query_report('trans_origin_form', 'trans_origin_name')"><?= lang('查询');?></a>
		</div>
	</form>
</div>

<div id="trans_destination_div">
	<form id="trans_destination_form" method="post">
		<div style="padding-left: 5px;display: inline-block;">
			<label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
			<label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" id="trans_destination_click" iconCls="icon-search" onclick="query_report('trans_destination_form', 'trans_destination')"><?= lang('查询');?></a>
		</div>
	</form>
</div>
<div id="operator_id_div">
	<form id="operator_id_form" method="post">
		<div style="padding-left: 5px;display: inline-block;">
			<label><?= lang('名称');?>:</label><input name="name" class="easyui-textbox keydown_search" style="width:96px;" t_id="operator_id_click"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" id="operator_id_click" iconCls="icon-search" onclick="query_report('operator_id_form', 'operator_id')"><?= lang('查询');?></a>
		</div>
	</form>
</div>
<div id="customer_service_id_div">
	<form id="customer_service_id_form" method="post">
		<div style="padding-left: 5px;display: inline-block;">
			<label><?= lang('名称');?>:</label><input name="name" class="easyui-textbox keydown_search" style="width:96px;" t_id="customer_service_id_click"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" id="customer_service_id_click" iconCls="icon-search" onclick="query_report('customer_service_id_form', 'customer_service_id')"><?= lang('查询');?></a>
		</div>
	</form>
</div>
</body>
<script>
	function save(){
		<?php if($status != 0): ?>
		return;
		<?php endif;?>
		let customer_service_id = $('#customer_service_id').combogrid('getValue');
		let operator_id = $('#operator_id').combogrid('getValue');
		$.post("/biz_edi_ebooking_consol/save_data/<?=$id?>", { customer_service_id,operator_id },
				function(res){
					$.messager.alert('Tips',res.msg)
				}, "json");
	}

	function create_data(){
		ajaxLoading()
		$.post("/biz_edi_ebooking_consol/create_data/<?=$id?>", { },
				function(res){
					ajaxLoadEnd()
					$.messager.alert('Tips',res.msg)
					if(res.code == 1){
						if(res.consol_id != 0){
							location.href = '/biz_consol/edit/'+res.consol_id;
						}else{
							location.reload()
						}
					}
				}, "json");
	}
</script>
