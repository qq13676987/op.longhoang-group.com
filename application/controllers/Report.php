<?php

class report extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_model');
    }
    
    public $shipment_field_all = array(
        array("shipper_ref", "shipper_ref", "105", "9", '1', 'client info'),
        array("id", "ID", "0", "1", '1'),
        array("client_code", "Client Code", "0", "0", '1', 'client info'),
        array("client_company", "Client Company", "130", "3", '1', 'client info'),
        array("trans_origin", "trans_origin", "0", "9", '1', 'booking owner'),
        array("trans_origin_name", "trans_origin_name", "60", "9", '1', 'booking owner'),
        array("trans_discharge", "trans_discharge", "0", "9", '1', 'booking owner'),
        array("trans_discharge_name", "trans_discharge_name", "56", "9", '1', 'booking owner'),
        array("trans_destination", "trans_destination", "0", "9", '1', 'booking owner'),
        array("trans_destination_name", "trans_destination_name", "105", "9", '1', 'booking owner'),
        array("job_no", "Job No", "147", "2", '1'),
        array("booking_ETD", "booking_ETD", "103", "9", '1', 'booking owner'),
        array("trans_carrier", "trans_carrier", "149", "9", '1', 'booking owner'),
        array("biz_type", "biz_type", "47", "0", '1'),
        array("status", "status", "50", "0", '1'),
        array("client_address", "Client_Address", "0", "4", '1', 'client info'),
        array("client_contact", "client_contact", "0", "5", '1', 'client info'),
        array("client_telephone", "client_telephone", "0", "6", '1', 'client info'),
        array("shipper_company", "shipper_company", "120", "7", '1', 'booking info'),
        array("shipper_address", "shipper_address", "0", "8", '1', 'booking info'),
        array("shipper_contact", "shipper_contact", "0", "9", '1', 'booking info'),
        array("shipper_telephone", "shipper_telephone", "0", "9", '1', 'booking info'),
        array("shipper_email", "shipper_email", "0", "9", '1', 'booking info'),
        array("consignee_company", "consignee_company", "120", "9", '1', 'booking info'),
        array("consignee_address", "consignee_address", "0", "9", '1', 'booking info'),
        array("consignee_contact", "consignee_contact", "0", "9", '1', 'booking info'),
        array("consignee_telephone", "consignee_telephone", "0", "9", '1', 'booking info'),
        array("consignee_email", "consignee_email", "0", "9", '1', 'booking info'),
        array("notify_company", "notify_company", "0", "9", '1', 'booking info'),
        array("notify_address", "notify_address", "0", "9", '1', 'booking info'),
        array("notify_contact", "notify_contact", "0", "9", '1', 'booking info'),
        array("notify_telephone", "notify_telephone", "0", "9", '1', 'booking info'),
        array("notify_email", "notify_email", "0", "9", '1', 'booking info'),
        array("trans_mode", "trans_mode", "80", "9", '1'),
        array("trans_tool", "trans_tool", "80", "9", '1'),
        array("description", "description", "0", "9", '1'),
        array("description_cn", "description_cn", "0", "9", '1'),
        array("mark_nums", "mark_nums", "0", "9", '1'),
        array("release_type", "release_type", "60", "9", '1'),
        array("sailing_code", "sailing_code", "60", "9", '1', 'booking owner'),//航线代码
        array("sailing_area", "sailing_area", "60", "9", '1', 'booking owner'),//航线区域
        array("good_outers", "good_outers", "60", "9", '1', 'booking cargo'),
        array("good_outers_unit", "good_outers_unit", "60", "9", '1', 'booking cargo'),
        array("good_weight", "good_weight", "60", "9", '1', 'booking cargo'),
        array("good_weight_unit", "good_weight_unit", "60", "9", '1', 'booking cargo'),
        array("good_volume", "good_volumn", "60", "9", '1', 'booking cargo'),
        array("good_volume_unit", "good_volumn_unit", "60", "9", '1', 'booking cargo'),
        // array("good_describe", "good_describe", "0", "9", '1', 'booking cargo'),
        array("good_commodity", "good_commodity", "0", "9", '1', 'booking cargo'),
        array("INCO_term", "INCO_term", "0", "9", '1', 'booking cargo'),
        array("hbl_no", "hbl_no", "60", "9", '1'),
        array("hbl_type", "hbl_type", "0", "9", '1'),
        array("consol_id", "consol_id", "0", "9", '1'),
        array("warehouse_no", "warehouse_code", "0", "0", '1'),//仓库编号
        array("warehouse_code", "warehouse_code", "0", "0", '1'),//仓库code
        array("warehouse_contact", "warehouse_contact", "0", "0", '1'),//仓库联系人
        array("warehouse_address", "warehouse_address", "0", "0", '1'),//仓库联系人地址
        array("warehouse_telephone", "warehouse_telephone", "0", "0", '1'),//仓库联系人电话
        array("warehouse_request", "warehouse_request", "0", "0", '1'),//进仓要求
        array("warehouse_charge", "warehouse_charge", "0", "0", '1'),//收费标准
        array("warehouse_in_date", "warehouse_in_date", "0", "0", '1'),//入库时间
        array("warehouse_in_num", "warehouse_in_num", "0", "0", '1'),//入库数量
        array("created_by", "created_by", "150", "10", '1'),
        array("created_time", "created_time", "150", "10", '1'),
        array("updated_by", "updated_by", "150", "10", '1'),
        array("updated_time", "updated_time", "150", "11", '1'),
        array("dadanwancheng", "dadanwancheng", "0", "0", '1', 'step info'),
        array("haiguancangdan", "haiguancangdan", "0", "0", '1', 'step info'),
        array("baoguanjieshu", "baoguanjieshu", "0", "0", '1', 'step info'),
        array("xiangyijingang", "xiangyijingang", "0", "0", '1', 'step info'),
        array("haiguanfangxing", "haiguanfangxing", "0", "0", '1', 'step info'),
        array("matoufangxing", "matoufangxing", "0", "0", '1', 'step info'),
        array("peizaifangxing", "peizaifangxing", "0", "0", '1', 'step info'),
        array("chuanyiqihang", "chuanyiqihang", "0", "0", '1', 'step info'),
        array("tidanqueren", "tidanqueren", "0", "0", '1', 'step info'),
        array("tidanqianfa", "tidanqianfa", "0", "0", '1', 'step info'),
        array("yizuofeiyong", "yizuofeiyong", "0", "0", '1', 'step info'),
        array("box_info", "box_info", "150", "0", '1', 'booking owner'),
        array("free_svr", "free_svr", "0", "0", '1', 'booking owner'),
        array("AGR_no", "AGR_no", "0", "0", '1', 'booking owner'),
        array("dangergoods_id", "dangergoods_id", "0", "0", '1', 'booking cargo'),
        array("hs_code", "hs_code", "0", "0", '1', 'booking cargo'),
        array("goods_type", "goods_type", "0", "0", '1', 'booking cargo'),
        array("requirements", "requirements", "0", "0", '1', 'booking cargo'),
        array("service_options", "service_options", "0", "0", '1'),
        array("INCO_option", "INCO_term_option", "0", "0", '1'),
        array("INCO_address", "INCO_term_sp_address", "0", "0", '1'),

        array("cus_no", "cus_no", "0", "2", '1'),
        array("client_email", "client_email", "0", "6", '1', 'client info'),
        array("trans_term", "trans_term", "0", "9", '1'),
        array("remark", "remark", "0", "9", '1'),
//        array("truck_date", "truck_date", "111", "6", '2'),
//        array("truck_company", "truck_company", "0", "6", '2'),
//        array("truck_address", "truck_address", "0", "6", '2'),
//        array("truck_contact", "truck_contact", "0", "6", '2'),
//        array("truck_telephone", "truck_telephone", "0", "6", '2'),
//        array("truck_remark", "truck_remark", "0", "6", '2'),
//        array("trans_ETD", "trans_ETD", "60", "9", '3'),
//        array("trans_ATD", "trans_ATD", "60", "9", '3'),
//        array("trans_ETA", "trans_ETA", "60", "9", '3'),
//        array("trans_ATA", "trans_ATA", "60", "9", '3'),
        array("issue_date", "issue_date", "60", "9", '1'),
//        array("expiry_date", "expiry_date", "60", "9", '3'),
    );

    public $shipment_admin_field = array('operator', 'customer_service', 'sales');
    
    public function index()
    {
        $this->load->view('report/index_view.php');
    }
    
        /**
     * 货量统计表
     */
    public function volume_total()
    {
        header("Cache-control: private");
        setLang('biz_shipment');
        $data = array();
        $back = $data['back'] = (int)postValue('back', 0);
        $data['searchs'] = isset($_POST['searchs']) ? $_POST['searchs'] : array();
        $data['fields'] = isset($_POST['field']) ? $_POST['field'] : array();
        $data['groups_field'] = isset($_POST['groups_field']) ? $_POST['groups_field'] : '';

        //2023-06-19 如果back 存在值时, 那么 代表要返回前面的页面
        $searchs_length = sizeof($data['searchs']);
        if($back > 0 && $searchs_length > 0){
            //变之前让当前的groups 变成删除线的 后面那个
            $data['groups_field'] = $data['searchs'][$searchs_length - $back][0];

            $data['searchs'] = array_slice($data['searchs'], 0, $searchs_length - $back);
        }

        foreach ($data['searchs'] as $key => $s_row) {
            $sql_row = array();
            if ($s_row[0] == 'sales' || $s_row[0] == 'sales_assistant' || $s_row[0] == 'operator' || $s_row[0] == 'customer_service') {
                $sql = "select name from bsc_user where id = {$s_row[1]}";
                $sql_row = $this->m_model->query_one($sql);
            }

            if ($s_row[0] == 'sales_group' || $s_row[0] == 'operator_group' || $s_row[0] == 'customer_service_group') {
                $sql = "select group_name as name from bsc_group where group_code = '{$s_row[1]}'";
                $sql_row = $this->m_model->query_one($sql);
            }

            if ($s_row[0] == 'creditor') {
                $sql = "select company_name as name from biz_client where client_code = '{$s_row[1]}'";
                $sql_row = $this->m_model->query_one($sql);
            }
            if ($s_row[0] == 'trans_carrier') {
                $sql = "select client_name as name from biz_client where client_code = '{$s_row[1]}'";
                $sql_row = $this->m_model->query_one($sql);
            }
            if ($s_row[0] == 'trans_destination') {
                $sql = "select port_name as name from biz_port where port_code = '{$s_row[1]}'";
                $sql_row = $this->m_model->query_one($sql);
            }
            if (empty($sql_row)) $sql_row['name'] = $s_row[1];
            $data['searchs'][$key][] = $sql_row['name'];
        }
        if (empty($data['searchs']) && $data['groups_field'] == 'shipment') $data['groups_field'] = 'month';

        //获取全部退关数据, 在下面分开统计退关信息
        $data['ext_fields'] = array_column(Model('bsc_dict_model')->no_role_get("catalog = 'cancel_msg'"), 'ext2', 'ext1');

        $this->load->view('head');
        $this->load->view('/report/shipment/volume_total_view', $data);
    }

    public function get_volume_total_data()
    {
        Model('m_model');
        $post_group_field = isset($_POST['groups_field']) ? $_POST['groups_field'] : '';
        $searchs = isset($_POST['searchs']) ? $_POST['searchs'] : array();
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $ext_data = array();

        $shipment_table_name = 'biz_shipment';
        $consol_table_name = 'biz_consol';

        $result = array();
        $join_sql = array();
        $join_sql['biz_consol'] = "LEFT JOIN {$consol_table_name} biz_consol ON biz_consol.id = biz_shipment.consol_id";
        $group_field = array();
        if ($post_group_field == 'booking_ETD_month') {//按月
            $group_field[] = "DATE_FORMAT(biz_shipment.booking_ETD,'%Y-%m')";
        } else if ($post_group_field == 'trans_ATD_month') {//按月
            $group_field[] = "DATE_FORMAT(biz_consol.trans_ATD,'%Y-%m')";
            $join_sql['biz_consol'] = "LEFT JOIN {$consol_table_name} biz_consol ON biz_consol.id = biz_shipment.consol_id";
        } else if ($post_group_field == 'created_time_month') {//按月
            $group_field[] = "DATE_FORMAT(biz_shipment.created_time,'%Y-%m-%d')";
        } else if ($post_group_field == 'booking_ETD_day') {//按日
            $group_field[] = "DATE_FORMAT(biz_shipment.booking_ETD,'%Y-%m-%d')";
        } else if ($post_group_field == 'trans_ATD_day') {//按日
            $group_field[] = "DATE_FORMAT(biz_consol.trans_ATD,'%Y-%m-%d')";
            $join_sql['biz_consol'] = "LEFT JOIN {$consol_table_name} biz_consol ON biz_consol.id = biz_shipment.consol_id";
        } else if ($post_group_field == 'created_time_day') {//按日
            $group_field[] = "DATE_FORMAT(biz_shipment.created_time,'%Y-%m-%d')";
        } else if (in_array($post_group_field, array('sales', 'sales_assistant', 'operator', 'customer_service'))) {
            $group_field[] = "biz_duty_new.user_id";
            $join_sql['biz_duty_new'] = "LEFT JOIN biz_duty_new ON biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = '{$post_group_field}'";
        } else if (in_array($post_group_field, array('sales_group','operator_group', 'customer_service_group'))) {
            $group_field[] = "biz_duty_new.user_group";
            $duty_user_role = str_replace('_group', '', $post_group_field);
            $join_sql['biz_duty_new'] = "LEFT JOIN biz_duty_new ON biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = '{$duty_user_role}'";
        } else if(in_array($post_group_field, array('sales_company'))){
            $group_field[] = "substring_index(biz_duty_new.user_group, '-', 1)";
            $duty_user_role = str_replace('_company', '', $post_group_field);
            $join_sql['biz_duty_new'] = "LEFT JOIN biz_duty_new ON biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = '{$duty_user_role}'";
        }
//        else if ($post_group_field == 'sales_company') {//销售分公司
//            $group_field[] = "biz_sub_company.company_code";
//            $join_sql['biz_sub_company'] = "LEFT JOIN biz_sub_company on substring_index(biz_duty_new.sales_group, '-', 1) = biz_sub_company.company_code";
//        }
        else if ($post_group_field == 'creditor') {
            $group_field[] = "biz_consol.creditor";
            $join_sql['biz_consol'] = "LEFT JOIN {$consol_table_name} biz_consol ON biz_consol.id = biz_shipment.consol_id";
        } else if ($post_group_field == 'vessel_voyage') {
            $group_field[] = "biz_consol.vessel";
            $group_field[] = '\' \'';
            $group_field[] = "biz_consol.voyage";
            $join_sql['biz_consol'] = "LEFT JOIN {$consol_table_name} biz_consol ON biz_consol.id = biz_shipment.consol_id";
        } else if ($post_group_field == 'shipment') {
            $group_field[] = "biz_shipment.job_no";
        } else {
            $group_field[] = "biz_shipment.{$post_group_field}";
        }

        if ($group_field == array()) {
            echo json_encode(array('rows' => array(), 'total' => 0));
            return false;
        }
        //where--start


        $where = array();
        $where[] = "biz_shipment.parent_id = 0";

        foreach ($searchs as $key => $search) {
            if ($search[1] !== '') {
                $end_date = date('Y-m-t', strtotime($search[1] . '-01'));
                if ($search[0] == 'booking_ETD_month') $where[] = "biz_shipment.booking_ETD >= '{$search[1]}-01' and biz_shipment.booking_ETD <= '{$end_date}'";
                if ($search[0] == 'trans_ATD_month') {
                    $where[] = "biz_consol.trans_ATD >= '{$search[1]}-01 00:00:00' and biz_consol.trans_ATD <= '{$end_date} 00:00:00'";
                    $join_sql['biz_consol'] = "LEFT JOIN {$consol_table_name} biz_consol ON biz_consol.id = biz_shipment.consol_id";
                }
                if ($search[0] == 'created_time_month') $where[] = "biz_shipment.created_time >= '{$search[1]}-01 00:00:00' and biz_shipment.booking_ETD <= '{$end_date} 00:00:00'";
                if ($search[0] == 'booking_ETD_day') $where[] = "biz_shipment.booking_ETD = '{$search[1]}'";
                if ($search[0] == 'trans_ATD_day') {
                    $where[] = "biz_consol.trans_ATD >= '{$search[1]} 00:00:00' and biz_consol.trans_ATD <= '{$search[1]} 23:59:59'";
                    $join_sql['biz_consol'] = "LEFT JOIN {$consol_table_name} biz_consol ON biz_consol.id = biz_shipment.consol_id";
                }
                if ($search[0] == 'created_time_day') $where[] = "biz_shipment.created_time >= '{$search[1]} 00:00:00' and biz_shipment.created_time <= '{$search[1]} 23:59:59'";

                if (in_array($search[0], array('sales', 'sales_assistant', 'operator', 'customer_service'))) {
                    $where[] = Model('bsc_user_role_model')->get_role_v3("'biz_shipment'", "biz_shipment.id", array($search[0]), "biz_duty_new.user_id = '$search[1]'");
                }
                if (in_array($search[0], array('sales_group', 'operator_group', 'customer_service_group'))) {
                    $duty_user_role = str_replace('_group', '', $search[0]);
                    $where[] = Model('bsc_user_role_model')->get_role_v3("'biz_shipment'", "biz_shipment.id", array($duty_user_role), "biz_duty_new.user_group = '$search[1]'");
                }
                if (in_array($search[0], array('sales_company'))) {
                    $duty_user_role = str_replace('_company', '', $search[0]);
                    $where[] = Model('bsc_user_role_model')->get_role_v3("'biz_shipment'", "biz_shipment.id", array($duty_user_role), "biz_duty_new.user_group like '{$search[1]}%'");
                }
//                if ($search[0] == 'sales_company') {
//                    $where[] = "biz_duty_new.sales_group like '$search[1]%'";
//                }
                if ($search[0] == 'trans_carrier') $where[] = "biz_shipment.trans_carrier = '$search[1]'";
                if ($search[0] == 'trans_destination') $where[] = "biz_shipment.trans_destination = '$search[1]'";
                if ($search[0] == 'trans_destination_inner') $where[] = "biz_shipment.trans_destination_inner = '$search[1]'";
                if ($search[0] == 'trans_origin') $where[] = "biz_shipment.trans_origin = '$search[1]'";
                if ($search[0] == 'sailing_area') $where[] = "biz_shipment.sailing_area = '$search[1]'";
                if ($search[0] == 'creditor') {
                    $where[] = "biz_consol.creditor = '{$search[1]}'";
                    $join_sql['biz_consol'] = "LEFT JOIN {$consol_table_name} biz_consol ON biz_consol.id = biz_shipment.consol_id";
                }
                if ($search[0] == 'biz_type') $where[] = "biz_shipment.biz_type = '{$search[1]}'";
                if ($search[0] == 'client_code') $where[] = "biz_shipment.client_code = '$search[1]'";
                if ($search[0] == 'vessel_voyage') {
                    $where[] = "concat(biz_consol.vessel,' ',biz_consol.voyage) = '{$search[1]}'";
                    $join_sql['biz_consol'] = "LEFT JOIN {$consol_table_name} biz_consol ON biz_consol.id = biz_shipment.consol_id";
                }
            }
        }
        //2023-02-09 如果 是按天那种,这里要加入限制, 最多只能查两个月内的
        $day_group_arr = array('booking_ETD_day', 'trans_ATD_day', 'created_time_day');
        $new_fields = array();

        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_shipment', 'report_volume_total');//获取相关的配置信息
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                $new_fields[] = array('f' => $f, 's' => $s, 'v' => $v);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }
                //服务选项的不等于用not like 处理
                $json_field = array('service_options', "biz_shipment.service_options");
                if(in_array($f, $json_field) && $s == '!='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} not like '%$v%'";
                    }
                    continue;
                }
                if($f == "biz_shipment.cus_no" || $f == 'cus_no'){
                    //2021-07-27 新增关单号可查询子shipment的
                    $this->db->select('parent_id');
                    $child_shipment = $this->biz_shipment_model->get_where_one_all(search_like($f, $s, $v) . ' and parent_id != 0');
                    $cus_no_where = search_like($f, $s, $v);
                    if(!empty($child_shipment)){
                        $cus_no_where = "(" . $cus_no_where . " or biz_shipment.id = " . $child_shipment['parent_id'] . ")";
                    }
                    if($v != '') $where[] = $cus_no_where;
                    continue;
                }
                if($f == "biz_shipment.carrier_ref"){
                    Model('m_model');
                    if(trim($v) === '') continue;
                    //2022-06-14 如果是查的MBL,那么从另一张表里获取
                    $mbl_mores = $this->m_model->query_array("select consol_id from biz_consol_mbl_more where " . search_like('mbl_no', 'like', $v));
                    $where[] = "(biz_consol.id in ('" . join('\',\'', array_column($mbl_mores, 'consol_id')) . "') or " . search_like("biz_consol.carrier_ref", 'like', $v) . ")";
                    continue;
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];

                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') continue;
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) {
                        $this_join0 = explode(' ', $this_join[0]);
                        $title_data['sql_join'][end($this_join0)] = $this_join;
                    }

                    //2022-12-27 加入新逻辑，根据逗号切割传过来的值循环处理
                    //这里的话，如果用的 = ，可以改为 in直接连接
                    $v_arr = explode(',', $v);
                    $this_where = array();
                    foreach ($v_arr as $v_val){
                        $this_where[] = search_like($f, $s, $v_val);
                    }

                    $where[] = '(' . join(' or ', $this_where) . ')';
                    // $where[] = search_like($f, $s, $v);
                }
            }
        }

        //2023-02-09 如果组查询字段存在以下的时, 必须要查询对应的日期,且日期范围为一个月
        if(in_array($post_group_field, $day_group_arr)){
            $check_field = "biz_shipment." . str_replace('_day', '', $post_group_field);
            //将大于和小于抠出来进行处理
            $date_filter = array_filter($new_fields, function ($row) use ($check_field) {
                if($row['f'] == $check_field && in_array($row['s'], array('>=', '<=')) && !empty(trim($row['v']))){
                    return true;
                }else{
                    return false;
                }
            });

            //大于等于和小于等于来去除多余的
            $date_range = array_column($date_filter, 'v', 's');

            if(sizeof($date_range) != 2){
                $result['rows'] = array(array('name' => lang('请设置对应的日期范围, 如 按日(订舱船期)时, 请查询那里输入 两月内的订舱船期')));
                $result['total'] = 1;
                $result['footer'] = array();
                return jsonEcho($result);
            }

            //如果2个日期时, 这时候判断日期范围是否在两月内
            $day_num = abs(round(strtotime($date_range['>=']) - strtotime($date_range['<='])) / 3600 / 24);
            if($day_num > 60){
                $result['rows'] = array(array('name' => lang('日期范围请勿超过2个月')));
                $result['total'] = 1;
                $result['footer'] = array();
                return jsonEcho($result);
            }
        }

        $where[] = Model('bsc_user_role_model')->get_role_v3("'biz_shipment'", "biz_shipment.id", Model('biz_shipment_model')->admin_field);
        $where = join(' AND ', $where);
        if ($where != '') $where = ' AND ' . $where;

        //这里拼接join数据
        foreach ($title_data['sql_join'] as $jk => $j){
            if($jk == 'biz_consol') continue;
            if(isset($join_sql[$j[0]])) continue;

            $join_sql[$j[0]] = "{$j[2]} JOIN {$j[0]} ON {$j[1]}";
        }
        $join_sql = join(' ', $join_sql);
        //where--end
        $name_field = array();
        foreach ($group_field as $f) {
            $name_field[] = $f;
        }

        //2023-05-17 货量统计新加入
        $ext_sql = array();

        //获取全部退关数据, 在下面分开统计退关信息
        $cancel_msg_dict = Model('bsc_dict_model')->no_role_get("catalog = 'cancel_msg'");
        $cancel_msg_dict_arr = array();
        foreach ($cancel_msg_dict as $val){
            !isset($cancel_msg_dict_arr[$val['ext1']]) && $cancel_msg_dict_arr[$val['ext1']] = array(
                'value' => array(),
                'ext1' => $val['ext1'],
                'ext2' => $val['ext2'],
            );
            $cancel_msg_dict_arr[$val['ext1']]['value'][] = $val['value'];
        }
        unset($cancel_msg_dict);

        foreach ($cancel_msg_dict_arr as $val){
            $ext_sql[] = "COUNT(case when (biz_shipment.status='cancel' and biz_consol.cancel_msg in (" . join(',', $val['value']) . ")) then 1 else null end) as {$val['ext1']}_count,";
        }
        $ext_sql = join('', $ext_sql);

        $sql = "SELECT CONCAT(" . join(',', $name_field) . ") as name, " .
            "count(1) as count,count(case when (biz_shipment.status='cancel') then 1 else null end) as count_cancel, " . //退关数
            "(count(case when (biz_shipment.status='cancel') then 1 else null end) / count(1)) as rate_cancel, " . //退关比例
            "GROUP_CONCAT((case when (biz_shipment.biz_type='export') then biz_shipment.box_info else '' end) separator ';') as export_box_info_sum, " .  //出口箱数据总
            "GROUP_CONCAT((case when (biz_shipment.biz_type='import') then biz_shipment.box_info else '' end) separator ';') as import_box_info_sum, " .  //进口箱数据总
            "COUNT((case when (biz_consol.customer_booking='1') then 1 else null end)) as zdc_count, " .  //客户自订舱总数
            "COUNT((case when (biz_consol.customer_booking='1' and biz_shipment.status='cancel') then 1 else null end)) as zdc_cancel, " .  //客户自订舱退关数
            "COUNT((case when (biz_consol.customer_booking!='1') then 1 else null end)) as cgdc_count, " .  //常规订舱总数
            "COUNT((case when (biz_consol.customer_booking!='1' and biz_shipment.status='cancel') then 1 else null end)) as cgdc_cancel, " .  //常规订舱退关数
            $ext_sql .//扩展统计
            "SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_outers else 0 end)) as ex_LCL_good_outers, " .
            "SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_weight else 0 end)) as ex_LCL_good_weight, " .
            "SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_volume else 0 end)) as ex_LCL_good_volume, " .
            "SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_outers else 0 end)) as im_LCL_good_outers, " .
            "SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_weight else 0 end)) as im_LCL_good_weight, " .
            "SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_volume else 0 end)) as im_LCL_good_volume, " .
            "SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_outers else 0 end)) as ex_AIR_good_outers, " .
            "SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_weight else 0 end)) as ex_AIR_good_weight, " .
            "SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_volume else 0 end)) as ex_AIR_good_volume, " .
            "SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_outers else 0 end)) as im_AIR_good_outers, " .
            "SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_weight else 0 end)) as im_AIR_good_weight, " .
            "SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_volume else 0 end)) as im_AIR_good_volume " .
            "FROM {$shipment_table_name} biz_shipment " . $join_sql . " WHERE 1 = 1" . $where . " GROUP BY " . join(',', $group_field) . " limit 5001";
        $rs = $this->m_model->query_array($sql);
        //加入1000条限制,避免卡死
        if (count($rs) > 5000) {
            $result['rows'] = array(array('name' => lang('数据超过5000条,无法显示')));
            $result['total'] = 1;
            $result['footer'] = array();
            return jsonEcho($result);
        }
        // var_dump($rs);
        // return;
        $rows = array();
        //获取name对应的名称填入--start
        $name_datas = array();
        $name_datas_ids = array_filter(array_column($rs, 'name'));
        if (empty($name_datas_ids)) $name_datas_ids[] = 0;

        $name_datas_key_by_id = array();
        $country_code = strtoupper(get_system_type());
        //销售
        $fields_config = array('name' => '', 'count' => 0, 'count_cancel' => 0, 'rate_cancel' => 0, 'zdc_count' => 0, 'zdc_cancel' => 0,'cgdc_count' => 0,
            'cgdc_cancel' => 0, 'export_box_info_sum' => 0, 'import_box_info_sum' => 0, 'ex_LCL_good_outers' => 0, 'ex_LCL_good_weight' => 0,
            'ex_LCL_good_volume' => 0, 'im_LCL_good_outers' => 0, 'im_LCL_good_weight' => 0, 'im_LCL_good_volume' => 0, 'ex_AIR_good_outers' => 0,
            'ex_AIR_good_weight' => 0, 'ex_AIR_good_volume' => 0, 'im_AIR_good_outers' => 0, 'im_AIR_good_weight' => 0, 'im_AIR_good_volume' => 0);
        if ($post_group_field == 'sales' || $post_group_field == 'sales_assistant' || $post_group_field == 'operator' || $post_group_field == 'customer_service') {
            //如果是用户,那么全查,且没有的为0
            //这里获取对应部门的
            $this_group = '';
            $this_group_where = "";

            foreach ($searchs as $key => $search) {
                if ($search[1] !== '') {
                    if ($post_group_field == 'operator' && $search[0] == 'operator_group') $this_group = $search[1];
                    if ($post_group_field == 'sales' && $search[0] == 'sales_group') $this_group = $search[1];
                    if ($post_group_field == 'sales' && $search[0] == 'sales_company') $this_group_where = "and `group` like '%{$search[1]}%'";
                    if ($post_group_field == 'customer_service' && $search[0] == 'customer_service_group') $this_group = $search[1];
                }
            }
            if ($this_group != '') $this_group_where = "and `group` = '$this_group'";

            $name_datas_sql = "select id,name,name_en,(select group_name from bsc_group where bsc_group.group_code = bsc_user.`group`) as group_name from bsc_user where (user_role like '%$post_group_field%' $this_group_where and status = 0 and country_code = '{$country_code}') or id in (" . join(',', $name_datas_ids) . ")";
            $name_datas = $this->m_model->query_array($name_datas_sql);

            $name_datas_key_by_id = array_column($name_datas, null, 'id');

            //将上面不存在的填充到rs里, 全部放入$name_datas_key_by_id,
            foreach ($name_datas as $name_data) {
                if (!in_array($name_data['id'], $name_datas_ids)) {
                    //填充进去一个空的
                    $temp_row = $fields_config;
                    $temp_row['name'] = $name_data['id'];
                    $rs[] = $temp_row;
                }
            }

            unset($name_datas);
        }
        //销售部 操作部 客服部
        if ($post_group_field == 'sales_group' || $post_group_field == 'operator_group' || $post_group_field == 'customer_service_group') {
            //部门也填充不存在的,同用户
            // $name_datas_sql = "select group_code as id,group_name as name from bsc_group where group_code in ('" . join('\',\'', $name_datas_ids) . "')";
            $group_type_config = array('sales_group' => '销售部', 'operator_group' => '操作部', 'customer_service_group' => '客服部');

            $search_company_where = "";
            if($post_group_field == 'sales_group'){
                $search_company = filter_unique_array(array_map(function ($r){
                    if($r[0] == 'sales_company') return $r[1];
                    return array();
                }, $searchs));
                if(!empty($search_company)){
                    $search_company_where = " and group_code like '{$search_company[0]}%'";
                }
            }
            $name_datas_sql = "select group_code as id,group_name as name from bsc_group where (group_type = '{$group_type_config[$post_group_field]}' and status = 0 $search_company_where) or group_code in ('" . join('\',\'', $name_datas_ids) . "') and country_code = '$country_code' ";

            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');
            //将上面不存在的填充到rs里, 全部放入$name_datas_key_by_id,
            foreach ($name_datas as $name_data) {
                if (!in_array($name_data['id'], $name_datas_ids)) {
                    //填充进去一个空的
                    $temp_row = $fields_config;
                    $temp_row['name'] = $name_data['id'];
                    $rs[] = $temp_row;
                }
            }

            unset($name_datas);
        }
        //按天时, 要自动填充吗?
        if ($post_group_field == 'booking_ETD_day' || $post_group_field == 'trans_ATD_day' || $post_group_field == 'created_time_day') {
            //填充的话,取最高和最低日期,填充中间的
            //全部转为时间戳

            $max_time = strtotime($date_range['<=']);
            $min_time = strtotime($date_range['>=']);
            $days = ($max_time - $min_time) / 86400 + 1;
            for ($i = 0; $i < $days; $i++){
                $this_date = date('Y-m-d', $min_time + (86400*$i));
                if (!in_array($this_date, $name_datas_ids)) {
                    //填充进去一个空的
                    $temp_row = $fields_config;
                    $temp_row['name'] = $this_date;
                    $rs[] = $temp_row;
                }
            }
        }
        //销售分公司
        if ($post_group_field == 'sales_company') {
            $name_datas_sql = "select company_code as id,company_name as name from biz_sub_company where company_code in ('" . join('\',\'', $name_datas_ids) . "')";
            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');
            unset($name_datas);
        }
        //订舱代理
        if ($post_group_field == 'creditor' || $post_group_field == 'client_code') {
            $name_datas_sql = "select client_code as id,company_name as name from biz_client where client_code in ('" . join('\',\'', $name_datas_ids) . "')";
            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');
            unset($name_datas);
        }
        //承运人
        if ($post_group_field == 'trans_carrier') {
            $name_datas_sql = "select client_code as id,client_name as name from biz_client where client_code in ('" . join('\',\'', $name_datas_ids) . "')";
            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');
            unset($name_datas);
        }
        //航线区域
        if ($post_group_field == 'sailing_area') {
            $name_datas_sql = "select sailing_area as id,sailing as name from biz_port where sailing_area in ('" . join('\',\'', $name_datas_ids) . "')";
            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');
            unset($name_datas);
        }
        //起运港
        if ($post_group_field == 'trans_origin') {
            $name_datas_sql = "select port_code as id,port_name as name from biz_port where length(port_code)=5 and port_code in ('" . join('\',\'', $name_datas_ids) . "')";
            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');
            //2023-04-23 港口前面统一加入 港口代码显示
            foreach ($name_datas_key_by_id as $key => $val){
                $name_datas_key_by_id[$key]['name'] = "<span style='color:red;'>{$val['id']} | </span>{$val['name']}";
            }

            unset($name_datas);
        }
        //目的港
        if ($post_group_field == 'trans_destination_inner') {
            $name_datas_sql = "select port_code as id,port_name as name from biz_port where port_code in ('" . join('\',\'', $name_datas_ids) . "')";
            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');
            foreach ($name_datas_key_by_id as $key => $val){
                $name_datas_key_by_id[$key]['name'] = "<span style='color:red;'>{$val['id']} | </span>{$val['name']}";
            }
            unset($name_datas);
        }
        //获取name对应的名称填入--end
        $footer = array();
        $i = 1;
        foreach ($rs as $row) {
            $row['id'] = $i++;
            $row['name_id'] = trim($row['name']);
            $row['count'] = (int)$row['count'];
            $row['count_cancel'] = (int)$row['count_cancel'];
            if (isset($name_datas_key_by_id[$row['name_id']])) {
                $row['name'] = $name_datas_key_by_id[$row['name_id']]['name'];
                if(!empty($name_datas_key_by_id[$row['name_id']]['group_name'])){
                    $row['sub_company_name'] = mb_substr($name_datas_key_by_id[$row['name_id']]['group_name'],0,4);
                    $row['group_name'] = mb_substr($name_datas_key_by_id[$row['name_id']]['group_name'],4,strlen($name_datas_key_by_id[$row['name_id']]['group_name']) - 4);
                    $row['name'] = $row['name'] . " | " . $name_datas_key_by_id[$row['name_id']]['name_en'];
                }
            } else {
                if (empty($row['name_id'])) {
                    $row['sub_company_name'] = '无值';
                    $row['group_name'] = '无值';
                }
                $row['name'] = '<span style="color:red;">' . $row['name_id'] . '</span>';
            }

            //2023-06-12 选部门时, 加一个格子, 显示当前部门下的人
            if(in_array($post_group_field, array('sales_group', 'operator_group', 'customer_service_group'))){
                $sql = "select count(*) as c from bsc_user where bsc_user.group = '{$row['name_id']}' and status = 0;";
                $temp_row = Model('m_model')->query_one($sql);
                $row['group_user_num'] = $temp_row['c'];
            }

            //box_info_sum解析所有box_info
            $export_box_info_sum = array_filter(explode(';', $row['export_box_info_sum']));
            $import_box_info_sum = array_filter(explode(';', $row['import_box_info_sum']));
            unset($row['export_box_info_sum']);
            unset($row['import_box_info_sum']);
            //业务类型：FCL， boxinfo里所有20开头的*1 + 40开头*2 + 45开头*2 = 总和

            $row['export_20_size'] = 0;
            $row['export_40_size'] = 0;
            $row['export_45_size'] = 0;
            foreach ($export_box_info_sum as $box_info) {
                $box_info = json_decode($box_info, true);
                if (empty($box_info)) $box_info = array();
                foreach ($box_info as $bi_row) {
                    $size = substr($bi_row['size'], 0, 2);
                    $num = $bi_row['num'];
                    if ($size == '20') {
                        $row['export_20_size'] += $num;
                    } else if ($size == '40') {
                        $row['export_40_size'] += $num;
                    } else if ($size == '45') {
                        $row['export_45_size'] += $num;
                    }
                }
            }
            $row['export_TEU'] = $row['export_20_size'] + $row['export_40_size'] * 2 + $row['export_45_size'] * 2;

            $row['import_20_size'] = 0;
            $row['import_40_size'] = 0;
            $row['import_45_size'] = 0;

            foreach ($import_box_info_sum as $box_info) {
                $box_info = json_decode($box_info, true);
                foreach ($box_info as $bi_row) {
                    $size = substr($bi_row['size'], 0, 2);
                    $num = $bi_row['num'];
                    if ($size == '20') {
                        $row['import_20_size'] += $num;
                    } else if ($size == '40') {
                        $row['import_40_size'] += $num;
                    } else if ($size == '45') {
                        $row['import_45_size'] += $num;
                    }
                }
            }

            $row['import_TEU'] = $row['import_20_size'] + $row['import_40_size'] * 2 + $row['import_45_size'] * 2;

            $row['20_size'] = $row['import_20_size'] + $row['export_20_size'];
            $row['40_size'] = $row['import_40_size'] + $row['export_40_size'];
            $row['45_size'] = $row['import_45_size'] + $row['export_45_size'];

            $row['TEU'] = $row['export_TEU'] + $row['import_TEU'];

            //CBM
            $row['ex_LCL_good_outers'] = ROUND($row['ex_LCL_good_outers'], 4);
            $row['ex_LCL_good_weight'] = ROUND($row['ex_LCL_good_weight'], 4);
            $row['ex_LCL_good_volume'] = ROUND($row['ex_LCL_good_volume'], 4);
            $row['im_LCL_good_outers'] = ROUND($row['im_LCL_good_outers'], 4);
            $row['im_LCL_good_weight'] = ROUND($row['im_LCL_good_weight'], 4);
            $row['im_LCL_good_volume'] = ROUND($row['im_LCL_good_volume'], 4);
            $row['ex_AIR_good_outers'] = ROUND($row['ex_AIR_good_outers'], 4);
            $row['ex_AIR_good_weight'] = ROUND($row['ex_AIR_good_weight'], 4);
            $row['ex_AIR_good_volume'] = ROUND($row['ex_AIR_good_volume'], 4);
            $row['im_AIR_good_outers'] = ROUND($row['im_AIR_good_outers'], 4);
            $row['im_AIR_good_weight'] = ROUND($row['im_AIR_good_weight'], 4);
            $row['im_AIR_good_volume'] = ROUND($row['im_AIR_good_volume'], 4);

            $row['ex_LCL_CBM'] = $row['ex_LCL_good_outers'] . '/' . $row['ex_LCL_good_weight'] . '/' . $row['ex_LCL_good_volume'];
            $row['im_LCL_CBM'] = $row['im_LCL_good_outers'] . '/' . $row['im_LCL_good_weight'] . '/' . $row['im_LCL_good_volume'];

            $row['ex_AIR_KGS'] = $row['ex_AIR_good_outers'] . '/' . $row['ex_AIR_good_weight'] . '/' . $row['ex_AIR_good_volume'];
            $row['im_AIR_KGS'] = $row['im_AIR_good_outers'] . '/' . $row['im_AIR_good_weight'] . '/' . $row['im_AIR_good_volume'];
            $rows[] = $row;

            //总和

            $footer['count'] = isset($footer['count']) ? $footer['count'] + $row['count'] : 0 + $row['count'];
            $footer['count_cancel'] = isset($footer['count_cancel']) ? $footer['count_cancel'] + $row['count_cancel'] : 0 + $row['count_cancel'];
            $footer['export_TEU'] = isset($footer['export_TEU']) ? $footer['export_TEU'] + $row['export_TEU'] : 0 + $row['export_TEU'];
            $footer['import_TEU'] = isset($footer['import_TEU']) ? $footer['import_TEU'] + $row['import_TEU'] : 0 + $row['import_TEU'];
            $footer['TEU'] = isset($footer['TEU']) ? $footer['TEU'] + $row['TEU'] : 0 + $row['TEU'];
            $footer['20_size'] = isset($footer['20_size']) ? $footer['20_size'] + $row['20_size'] : 0 + $row['20_size'];
            $footer['40_size'] = isset($footer['40_size']) ? $footer['40_size'] + $row['40_size'] : 0 + $row['40_size'];
            $footer['45_size'] = isset($footer['45_size']) ? $footer['45_size'] + $row['45_size'] : 0 + $row['45_size'];

            $footer['zdc_count'] = isset($footer['zdc_count']) ? $footer['zdc_count'] + $row['zdc_count'] : 0 + $row['zdc_count'];
            $footer['zdc_cancel'] = isset($footer['zdc_cancel']) ? $footer['zdc_cancel'] + $row['zdc_cancel'] : 0 + $row['zdc_cancel'];
            $footer['cgdc_count'] = isset($footer['cgdc_count']) ? $footer['cgdc_count'] + $row['cgdc_count'] : 0 + $row['cgdc_count'];
            $footer['cgdc_cancel'] = isset($footer['cgdc_cancel']) ? $footer['cgdc_cancel'] + $row['cgdc_cancel'] : 0 + $row['cgdc_cancel'];

            $footer['ex_LCL_good_outers'] = isset($footer['ex_LCL_good_outers']) ? $footer['ex_LCL_good_outers'] + $row['ex_LCL_good_outers'] : 0 + $row['ex_LCL_good_outers'];
            $footer['ex_LCL_good_weight'] = isset($footer['ex_LCL_good_weight']) ? $footer['ex_LCL_good_weight'] + $row['ex_LCL_good_weight'] : 0 + $row['ex_LCL_good_weight'];
            $footer['ex_LCL_good_volume'] = isset($footer['ex_LCL_good_volume']) ? $footer['ex_LCL_good_volume'] + $row['ex_LCL_good_volume'] : 0 + $row['ex_LCL_good_volume'];

            $footer['im_LCL_good_outers'] = isset($footer['im_LCL_good_outers']) ? $footer['im_LCL_good_outers'] + $row['im_LCL_good_outers'] : 0 + $row['im_LCL_good_outers'];
            $footer['im_LCL_good_weight'] = isset($footer['im_LCL_good_weight']) ? $footer['im_LCL_good_weight'] + $row['im_LCL_good_weight'] : 0 + $row['im_LCL_good_weight'];
            $footer['im_LCL_good_volume'] = isset($footer['im_LCL_good_volume']) ? $footer['im_LCL_good_volume'] + $row['im_LCL_good_volume'] : 0 + $row['im_LCL_good_volume'];

            $footer['ex_AIR_good_outers'] = isset($footer['ex_AIR_good_outers']) ? $footer['ex_AIR_good_outers'] + $row['ex_AIR_good_outers'] : 0 + $row['ex_AIR_good_outers'];
            $footer['ex_AIR_good_weight'] = isset($footer['ex_AIR_good_weight']) ? $footer['ex_AIR_good_weight'] + $row['ex_AIR_good_weight'] : 0 + $row['ex_AIR_good_weight'];
            $footer['ex_AIR_good_volume'] = isset($footer['ex_AIR_good_volume']) ? $footer['ex_AIR_good_volume'] + $row['ex_AIR_good_volume'] : 0 + $row['ex_AIR_good_volume'];

            $footer['im_AIR_good_outers'] = isset($footer['im_AIR_good_outers']) ? $footer['im_AIR_good_outers'] + $row['im_AIR_good_outers'] : 0 + $row['im_AIR_good_outers'];
            $footer['im_AIR_good_weight'] = isset($footer['im_AIR_good_weight']) ? $footer['im_AIR_good_weight'] + $row['im_AIR_good_weight'] : 0 + $row['im_AIR_good_weight'];
            $footer['im_AIR_good_volume'] = isset($footer['im_AIR_good_volume']) ? $footer['im_AIR_good_volume'] + $row['im_AIR_good_volume'] : 0 + $row['im_AIR_good_volume'];
        }

        //排序--start
        //2022-08-23 新增一个如果post没有传参,那么这里默认查询
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "name";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $config_sort = Model('sys_config_model')->get_config('report_volume_total_sort');
        $config_order = Model('sys_config_model')->get_config('report_volume_total_order');
        if(isset($_POST['sort'])){
            //存储一下config
            Model('sys_config_model')->save('report_volume_total_sort', $sort, true);
        }else{
            $sort = $config_sort == '' ? $sort : $config_sort;
        }
        if(isset($_POST['order'])){
            //存储一下config
            Model('sys_config_model')->save('report_volume_total_order', $order, true);
        }else{
            $order = $config_order == '' ? $order : $config_order;
        }
        $ext_data['order'] = $order;
        $ext_data['sort'] = $sort;

        $s = array();
        foreach ($rows as $key => $row) {
            if(!isset($row[$sort])) $sort="name";
            $s[$key] = isset($row[$sort]) ? $row[$sort] : $row[$sort];
        }
        if ($order == 'desc') $order = SORT_DESC;
        else $order = SORT_ASC;
        array_multisort($s, $order, $rows);

        $footer['is_footer'] = true;
        $footer['name'] = '总和：';
        isset($footer['rate_cancel']) && $footer['rate_cancel'] = $footer['count'] == 0 ? 0 : ROUND($footer['count_cancel'] / $footer['count'], 4);
        isset($footer['ex_LCL_CBM']) && $footer['ex_LCL_CBM'] = float_number($footer['ex_LCL_good_outers']) . '/' . float_number($footer['ex_LCL_good_weight']) . '/' . float_number($footer['ex_LCL_good_volume']);
        isset($footer['im_LCL_CBM']) && $footer['im_LCL_CBM'] = float_number($footer['im_LCL_good_outers']) . '/' . float_number($footer['im_LCL_good_weight']) . '/' . float_number($footer['im_LCL_good_volume']);

        isset($footer['ex_AIR_KGS']) && $footer['ex_AIR_KGS'] = float_number($footer['ex_AIR_good_outers']) . '/' . float_number($footer['ex_AIR_good_weight']) . '/' . float_number($footer['ex_AIR_good_volume']);
        isset($footer['im_AIR_KGS']) && $footer['im_AIR_KGS'] = float_number($footer['im_AIR_good_outers']) . '/' . float_number($footer['im_AIR_good_weight']) . '/' . float_number($footer['im_AIR_good_volume']);
        $result['rows'] = $rows;
        $result['total'] = sizeof($rows);
        $result['footer'] = array($footer);
        $result['ext_data'] = $ext_data;
        echo json_encode($result);
    }
    
    /**
     * 利润统计表
     */
    public function profit_total()
    {
        header("Cache-control: private");
        $data = array();
        $back = $data['back'] = (int)postValue('back', 0);
        $data['searchs'] = isset($_POST['searchs']) ? $_POST['searchs'] : array();
        $data['fields'] = isset($_POST['field']) ? $_POST['field'] : array();
        $data['groups_field'] = isset($_POST['groups_field']) ? $_POST['groups_field'] : '';
        
        //2023-06-19 如果back 存在值时, 那么 代表要返回前面的页面
        $searchs_length = sizeof($data['searchs']);
        if($back > 0 && $searchs_length > 0){
            //变之前让当前的groups 变成删除线的 后面那个
            $data['groups_field'] = $data['searchs'][$searchs_length - $back][0];

            $data['searchs'] = array_slice($data['searchs'], 0, $searchs_length - $back);
        }
        
        foreach ($data['searchs'] as $key => $s_row) {
            $sql_row = array();
            if ($s_row[0] == 'sales' || $s_row[0] == 'operator' || $s_row[0] == 'customer_service') {
                $sql = "select name from bsc_user where id = '{$s_row[1]}'";
                $sql_row = $this->m_model->query_one($sql);
            }
            if ($s_row[0] == 'sales_group' || $s_row[0] == 'operator_group' || $s_row[0] == 'customer_service_group') {
                $sql = "select group_name as name from bsc_group where group_code = '{$s_row[1]}'";
                $sql_row = $this->m_model->query_one($sql);
            }

            if ($s_row[0] == 'creditor') {
                $sql = "select company_name as name from biz_client where client_code = '{$s_row[1]}'";
                $sql_row = $this->m_model->query_one($sql);
            }
            if ($s_row[0] == 'trans_carrier') {
                $sql = "select client_name as name from biz_client where client_code = '{$s_row[1]}'";
                $sql_row = $this->m_model->query_one($sql);
            }
            if ($s_row[0] == 'trans_destination') {
                $sql = "select port_name as name from biz_port where port_code = '{$s_row[1]}'";
                $sql_row = $this->m_model->query_one($sql);
            }
            if (empty($sql_row)) $sql_row['name'] = $s_row[1];
            $data['searchs'][$key][] = $sql_row['name'];
        }
        if (empty($data['searchs']) && $data['groups_field'] == 'shipment') $data['groups_field'] = 'month';
        $data['admin_field'] = $this->shipment_admin_field;
        
        //2023-05-31 获取当前币种
        $data['currency_table_fields'] = array_column(Model('bsc_dict_model')->get_option("currency"), 'value');
        $data['local_currency'] = get_system_config("CURRENT_CURRENCY");

        $data['config_search'] = json_decode(getConfigSearch('profit_total', array('shipper_ref', 'like', ''))['config_text'], true);

        $data['f'] = $this->shipment_field_all;
        $data['f'][] = array('trans_ATD', 'trans_ATD', 100);
        $data['f'][] = array('report_date','report_date',  100);
        $this->load->view('head');
        // $this->load->view('/report/shipment/2', $data);
        // return;
        $this->load->view('/report/shipment/profit_total_view', $data);
    }

    public function get_profit_total_data()
    {
        Model('m_model');
        $post_group_field = isset($_POST['groups_field']) ? $_POST['groups_field'] : '';
        $searchs = isset($_POST['searchs']) ? $_POST['searchs'] : array();
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();
        $ext_data = array();

        $join_sql = array();
        $group_field = array();
        if ($post_group_field == 'booking_ETD_month') {//按月
            $group_field[] = "DATE_FORMAT(biz_shipment.booking_ETD,'%Y-%m')";
        } else if ($post_group_field == 'trans_ATD_month') {//按月
            $group_field[] = "DATE_FORMAT(biz_consol.trans_ATD,'%Y-%m')";
            $join_sql['biz_consol'] = "LEFT JOIN biz_consol ON biz_consol.id = biz_shipment.consol_id";
        } else if ($post_group_field == 'report_date_month') {//按月
            $group_field[] = "DATE_FORMAT(biz_consol.report_date,'%Y-%m')";
            $join_sql['biz_consol'] = "LEFT JOIN biz_consol ON biz_consol.id = biz_shipment.consol_id";
        } else if ($post_group_field == 'created_time_month') {//按月
            $group_field[] = "DATE_FORMAT(biz_shipment.created_time,'%Y-%m')";
        } else if ($post_group_field == 'booking_ETD_day') {//按月
            $group_field[] = "DATE_FORMAT(biz_shipment.booking_ETD,'%Y-%m-%d')";
        } else if ($post_group_field == 'trans_ATD_day') {//按月
            $group_field[] = "DATE_FORMAT(biz_consol.trans_ATD,'%Y-%m-%d')";
            $join_sql['biz_consol'] = "LEFT JOIN biz_consol ON biz_consol.id = biz_shipment.consol_id";
        } else if ($post_group_field == 'report_date_day') {//按月
            $group_field[] = "DATE_FORMAT(biz_consol.report_date,'%Y-%m-%d')";
            $join_sql['biz_consol'] = "LEFT JOIN biz_consol ON biz_consol.id = biz_shipment.consol_id";
        } else if ($post_group_field == 'created_time_day') {//按月
            $group_field[] = "DATE_FORMAT(biz_shipment.created_time,'%d')";
        } else if (in_array($post_group_field, array('sales', 'operator', 'customer_service'))) {
            $group_field[] = "biz_duty_new.user_id";
            $join_sql['biz_duty_new'] = "LEFT JOIN biz_duty_new ON biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = '{$post_group_field}'";
        } else if (in_array($post_group_field, array('sales_group', 'operator_group', 'customer_service_group'))) {
            $group_field[] = "biz_duty_new.user_group";
            $duty_user_role = str_replace('_group', '', $post_group_field);
            $join_sql['biz_duty_new'] = "LEFT JOIN biz_duty_new ON biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = '{$duty_user_role}'";
        } else if(in_array($post_group_field, array('sales_company'))){
            $group_field[] = "substring_index(biz_duty_new.user_group, '-', 1)";
            $duty_user_role = str_replace('_company', '', $post_group_field);
            $join_sql['biz_duty_new'] = "LEFT JOIN biz_duty_new ON biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = '{$duty_user_role}'";
        } else if ($post_group_field == 'creditor') {
            $group_field[] = 'biz_consol.creditor';
            $join_sql['biz_consol'] = "LEFT JOIN biz_consol ON biz_consol.id = biz_shipment.consol_id";
        } else if ($post_group_field == 'vessel_voyage') {
            $group_field[] = 'biz_consol.vessel';
            $group_field[] = '\' \'';
            $group_field[] = 'biz_consol.voyage';
            $join_sql['biz_consol'] = "LEFT JOIN biz_consol ON biz_consol.id = biz_shipment.consol_id";
        } else if ($post_group_field == 'shipment') {
            $group_field[] = 'biz_shipment.job_no';
        } else {
            $group_field[] = "biz_shipment.{$post_group_field}";
        }

        if ($group_field == '') {
            echo json_encode(array('rows' => array(), 'total' => 0));
            return false;
        }
        //where--start

        $where = array();
        $where[] = 'biz_shipment.parent_id = 0';

        foreach ($searchs as $key => $search) {
            if ($search[1] !== '') {
                if ($search[0] == 'booking_ETD_month') $where[] = "biz_shipment.booking_ETD >= '{$search[1]}-01' and biz_shipment.booking_ETD <= '" . date('Y-m-t', strtotime($search[1])) . "'";
                if ($search[0] == 'trans_ATD_month') {
                    $where[] = "biz_consol.trans_ATD >= '{$search[1]}-01 00:00:00' and biz_consol.trans_ATD <= '" . date('Y-m-t', strtotime($search[1])) . " 00:00:00'";
                    $join_sql['biz_consol'] = "LEFT JOIN biz_consol ON biz_consol.id = biz_shipment.consol_id";
                }
                if ($search[0] == 'report_date_month') {
                    $where[] = "biz_consol.report_date >= '{$search[1]}-01' and biz_consol.report_date <= '" . date('Y-m-t', strtotime($search[1])) . "'";
                    $join_sql['biz_consol'] = "LEFT JOIN biz_consol ON biz_consol.id = biz_shipment.consol_id";
                }
                if ($search[0] == 'created_time_month') $where[] = "biz_shipment.created_time >= '{$search[1]}-01 00:00:00' and biz_shipment.booking_ETD <= '" . date('Y-m-t', strtotime($search[1])) . " 00:00:00'";
                if ($search[0] == 'booking_ETD_day') $where[] = "biz_shipment.booking_ETD = '{$search[1]}'";
                if ($search[0] == 'trans_ATD_day') {
                    $where[] = "biz_consol.trans_ATD >= '{$search[1]} 00:00:00' and biz_consol.trans_ATD <= '{$search[1]} 23:59:59'";
                    $join_sql['biz_consol'] = "LEFT JOIN biz_consol ON biz_consol.id = biz_shipment.consol_id";
                }
                if ($search[0] == 'report_date_day') {
                    $where[] = "biz_consol.report_date >= '{$search[1]}' and biz_consol.report_date <= '{$search[1]}'";
                    $join_sql['biz_consol'] = "LEFT JOIN biz_consol ON biz_consol.id = biz_shipment.consol_id";
                }
                if ($search[0] == 'created_time_day') $where[] = "biz_shipment.created_time >= '{$search[1]} 00:00:00' and biz_shipment.created_time <= '{$search[1]} 23:59:59'";
                if (in_array($search[0], array('sales', 'sales_assistant', 'operator', 'customer_service'))) {
                    $where[] = Model('bsc_user_role_model')->get_field_where('biz_shipment', array($search[0]), "biz_duty_new.user_id = '$search[1]'");
                }
                if (in_array($search[0], array('sales_group', 'operator_group', 'customer_service_group'))) {
                    $duty_user_role = str_replace('_group', '', $search[0]);
                    $where[] = Model('bsc_user_role_model')->get_field_where('biz_shipment', array($duty_user_role), "biz_duty_new.user_group = '$search[1]'");
                }
                if (in_array($search[0], array('sales_company'))) {
                    $duty_user_role = str_replace('_company', '', $search[0]);
                    $where[] = Model('bsc_user_role_model')->get_field_where('biz_shipment', array($duty_user_role), "biz_duty_new.user_group like '{$search[1]}%'");
                }
                if ($search[0] == 'trans_carrier') $where[] = "biz_shipment.trans_carrier = '$search[1]'";
                if ($search[0] == 'trans_destination') $where[] = "biz_shipment.trans_destination = '$search[1]'";
                if ($search[0] == 'trans_origin') $where[] = "biz_shipment.trans_origin = '$search[1]'";
                if ($search[0] == 'creditor') {
                    $where[] = "biz_consol.creditor = '{$search[1]}'";
                    $join_sql['biz_consol'] = "LEFT JOIN biz_consol ON biz_consol.id = biz_shipment.consol_id";
                }
                if ($search[0] == 'biz_type') $where[] = "biz_shipment.biz_type = '{$search[1]}'";
                if ($search[0] == 'client_code') $where[] = "biz_shipment.client_code = '$search[1]'";
            }
        }
        $shipment_field_edit = array_column($this->shipment_field_all, 0);

        //2023-02-09 如果 是按天那种,这里要加入限制, 最多只能查两个月内的
        $day_group_arr = array('booking_ETD_day', 'trans_ATD_day', 'created_time_day');
        $new_fields = array();

        if (!empty($fields)) {
            foreach ($fields['f'] as $key => $field) {
                $f = (trim($fields['f'][$key]));
                $s = ($fields['s'][$key]);
                $v = (trim($fields['v'][$key]));
                $new_fields[] = array('f' => $f, 's' => $s, 'v' => $v);
                if ($f == '') continue;
                $pre = '';
                if (in_array($f, $shipment_field_edit)) {
                    $pre = 'biz_shipment.';
                } else {
                    if (in_array($f, $this->shipment_admin_field)) {
                        $f .= '_id';
                    }
                }
                //TODO 如果是datetime字段,=默认>=且<=
                if ($f == 'trans_ATD' || $f == 'report_date') {
                    $pre = 'biz_consol.';
                    if (!isset($join_sql['biz_consol'])) $join_sql['biz_consol'] = "LEFT JOIN biz_consol ON biz_consol.id = biz_shipment.consol_id";
                }
                $date_field = array('booking_ETD', 'created_time', 'updated_time', 'trans_ATD');
                if (in_array($f, $date_field) && $s == '=') {
                    if ($v != '') {
                        $where[] = "{$pre}{$f} >= '$v 00:00:00' and {$pre}{$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                if (in_array($f, $date_field) && ($s == '<' || $s == '<=')) {
                    if ($v != '') {
                        $where[] = "{$pre}{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }
                if ($v != '') $where[] = search_like($pre . $f, $s, $v);
            }
        }

        //2023-02-09 如果组查询字段存在以下的时, 必须要查询对应的日期,且日期范围为一个月
        if(in_array($post_group_field, $day_group_arr)){
            $check_field = str_replace('_day', '', $post_group_field);
            //将大于和小于抠出来进行处理
            $date_filter = array_filter($new_fields, function ($row) use ($check_field) {
                if($row['f'] == $check_field && in_array($row['s'], array('>=', '<=')) && !empty(trim($row['v']))){
                    return true;
                }else{
                    return false;
                }
            });

            //大于等于和小于等于来去除多余的
            $date_range = array_column($date_filter, 'v', 's');

            if(sizeof($date_range) != 2){
                $result['rows'] = array(array('name' => lang("请设置对应的日期范围, 如 按日(订舱船期)时, 请查询那里输入 两月内的订舱船期")));
                $result['total'] = 1;
                $result['footer'] = array();
                return jsonEcho($result);
            }

            //如果2个日期时, 这时候判断日期范围是否在两月内
            $day_num = abs(round(strtotime($date_range['>=']) - strtotime($date_range['<='])) / 3600 / 24);
            if($day_num > 60){
                $result['rows'] = array(array('name' => lang("日期范围请勿超过2个月")));
                $result['total'] = 1;
                $result['footer'] = array();
                return jsonEcho($result);
            }
        }

        $where[] = Model('bsc_user_role_model')->get_field_where('biz_shipment');
        $where = join(' AND ', $where);
        if ($where != '') $where = ' AND ' . $where;
        $join_sql = join(' ', $join_sql);
        //where--end
        $name_field = array();
        foreach ($group_field as $f) {
            $name_field[] = $f;
            // $name_field[] = "' '";
        }

        $sql = "SELECT CONCAT(" . join(',', $name_field) . ") as name, " .
            "GROUP_CONCAT(biz_shipment.id) as ids, " . //总数
            "count(1) as count,count(case when (biz_shipment.status='cancel') then 1 else null end) as count_cancel, " . //退关数
            "(count(case when (biz_shipment.status='cancel') then 1 else null end) / count(1)) as rate_cancel, " . //退关比例
            "GROUP_CONCAT((case when (biz_shipment.biz_type='export') then biz_shipment.box_info else '' end) separator ';') as export_box_info_sum, " .  //出口箱数据总
            "GROUP_CONCAT((case when (biz_shipment.biz_type='import') then biz_shipment.box_info else '' end) separator ';') as import_box_info_sum, " .  //进口箱数据总
            "SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_outers else 0 end)) as ex_LCL_good_outers, " .
            "SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_weight else 0 end)) as ex_LCL_good_weight, " .
            "SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_volume else 0 end)) as ex_LCL_good_volume, " .
            "SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_outers else 0 end)) as im_LCL_good_outers, " .
            "SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_weight else 0 end)) as im_LCL_good_weight, " .
            "SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'LCL') then biz_shipment.good_volume else 0 end)) as im_LCL_good_volume, " .
            "SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_outers else 0 end)) as ex_AIR_good_outers, " .
            "SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_weight else 0 end)) as ex_AIR_good_weight, " .
            "SUM((case when (biz_shipment.biz_type='export' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_volume else 0 end)) as ex_AIR_good_volume, " .
            "SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_outers else 0 end)) as im_AIR_good_outers, " .
            "SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_weight else 0 end)) as im_AIR_good_weight, " .
            "SUM((case when (biz_shipment.biz_type='import' and biz_shipment.trans_mode = 'AIR') then biz_shipment.good_volume else 0 end)) as im_AIR_good_volume " .
            "FROM biz_shipment " . $join_sql . " WHERE 1 = 1" . $where . " GROUP BY " . join(',', $group_field) . " limit 1001";
        $rs = $this->m_model->query_array($sql);
        $ext_data[] = $sql;

        //加入1000条限制,避免卡死
        if (count($rs) > 1000) {
            $result['rows'] = array(array('name' => lang("数据超过1000条,无法显示")));
            $result['total'] = 1;
            $result['footer'] = array();
            return jsonEcho($result);
        }

        $rows = array();
        //获取name对应的名称填入--start
        $name_datas_ids = array_filter(array_column($rs, 'name'));
        if (empty($name_datas_ids)) $name_datas_ids[] = 0;
        $name_datas_key_by_id = array();
        //销售
        $fields_config = array('name' => '', 'count' => 0, 'ids' => '', 'count_cancel' => 0, 'rate_cancel' => 0, 'export_box_info_sum' => 0, 'import_box_info_sum' => 0, 'ex_LCL_good_outers' => 0, 'ex_LCL_good_weight' => 0, 'ex_LCL_good_volume' => 0, 'im_LCL_good_outers' => 0, 'im_LCL_good_weight' => 0, 'im_LCL_good_volume' => 0, 'ex_AIR_good_outers' => 0, 'ex_AIR_good_weight' => 0, 'ex_AIR_good_volume' => 0, 'im_AIR_good_outers' => 0, 'im_AIR_good_weight' => 0, 'im_AIR_good_volume' => 0, 'sell_CNY_amount' => 0, 'cost_CNY_amount' => 0, 'sell_USD_amount' => 0, 'sell_USD_local_amount' => 0, 'cost_USD_amount' => 0, 'cost_USD_local_amount' => 0, 'sell_special_local_amount' => 0, 'cost_special_local_amount' => 0, 'sell_payment_amount' => 0, 'cost_payment_amount' => 0);
        $country_code = strtoupper(get_system_type());
        
        if ($post_group_field == 'sales' || $post_group_field == 'operator' || $post_group_field == 'customer_service') {
            // $name_datas_sql = "select id,name from bsc_user where id in (" . join(',', $name_datas_ids) . ")";
            $this_group = '';
            $this_group_where = "";
            foreach ($searchs as $key => $search) {
                if ($search[1] !== '') {
                    if ($post_group_field == 'operator' && $search[0] == 'operator_group') $this_group = $search[1];
                    if ($post_group_field == 'sales' && $search[0] == 'sales_group') $this_group = $search[1];
                    if ($post_group_field == 'customer_service' && $search[0] == 'customer_service_group') $this_group = $search[1];
                }
            }
            if ($this_group != '') $this_group_where = "and `group` = '$this_group'";

            $name_datas_sql = "select id, name,(select group_name from bsc_group where bsc_group.group_code = bsc_user.`group`) as group_name from bsc_user where ((user_role like '%$post_group_field%' $this_group_where and status = 0)  and country_code = '{$country_code}') or id in (" . join(',', $name_datas_ids) . ")";
            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');

            //将上面不存在的填充到rs里, 全部放入$name_datas_key_by_id,
            foreach ($name_datas as $name_data) {
                if (!in_array($name_data['id'], $name_datas_ids)) {
                    //填充进去一个空的
                    $temp_row = $fields_config;
                    $temp_row['name'] = $name_data['id'];
                    $rs[] = $temp_row;
                }
            }
            unset($name_datas);
        }
        //销售部 操作部 客服部
        if ($post_group_field == 'sales_group' || $post_group_field == 'operator_group' || $post_group_field == 'customer_service_group') {
            // $name_datas_sql = "select group_code as id,group_name as name from bsc_group where group_code in ('" . join('\',\'', $name_datas_ids) . "')";
            $group_type_config = array('sales_group' => 'SALES', 'operator_group' => 'OPERATOR', 'customer_service_group' => 'CUSTOMERSERVICE');
            $name_datas_sql = "select group_code as id,group_name as name from bsc_group where ((group_type = '{$group_type_config[$post_group_field]}' and status = 0) and country_code = '{$country_code}') or group_code in ('" . join('\',\'', $name_datas_ids) . "')";
            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');

            //将上面不存在的填充到rs里, 全部放入$name_datas_key_by_id,
            foreach ($name_datas as $name_data) {
                if (!in_array($name_data['id'], $name_datas_ids)) {
                    //填充进去一个空的
                    $temp_row = $fields_config;
                    $temp_row['name'] = $name_data['id'];
                    $rs[] = $temp_row;
                }
            }
            unset($name_datas);
        }
        //按天时, 要自动填充吗?
        if ($post_group_field == 'booking_ETD_day' || $post_group_field == 'trans_ATD_day' || $post_group_field == 'created_time_day') {
            //填充的话,取最高和最低日期,填充中间的
            //全部转为时间戳

            $max_time = strtotime($date_range['<=']);
            $min_time = strtotime($date_range['>=']);
            $days = ($max_time - $min_time) / 86400 + 1;
            for ($i = 0; $i < $days; $i++){
                $this_date = date('Y-m-d', $min_time + (86400*$i));
                if (!in_array($this_date, $name_datas_ids)) {
                    //填充进去一个空的
                    $temp_row = $fields_config;
                    $temp_row['name'] = $this_date;
                    $rs[] = $temp_row;
                }
            }
        }
        //订舱代理
        if ($post_group_field == 'creditor' || $post_group_field == 'client_code') {
            $name_datas_sql = "select client_code as id,company_name as name from biz_client where client_code in ('" . join('\',\'', $name_datas_ids) . "')";
            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');
            unset($name_datas);
        }
        //承运人
        if ($post_group_field == 'trans_carrier') {
            $name_datas_sql = "select client_code as id,client_name as name from biz_client where client_code in ('" . join('\',\'', $name_datas_ids) . "')";
            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');
            unset($name_datas);
        }
        //航线区域
        if ($post_group_field == 'sailing_area') {
            $name_datas_sql = "select sailing_area as id,sailing as name from biz_port where sailing_area in ('" . join('\',\'', $name_datas_ids) . "')";
            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');
            unset($name_datas);
        }
        //起运港
        if ($post_group_field == 'trans_origin') {
            $name_datas_sql = "select port_code as id,port_name as name from biz_port where length(port_code)=5 and port_code in ('" . join('\',\'', $name_datas_ids) . "')";
            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');
            foreach ($name_datas_key_by_id as $key => $val){
                $name_datas_key_by_id[$key]['name'] = "<span style='color:red;'>{$val['id']} | </span>{$val['name']}";
            }
            unset($name_datas);
        }
        //目的港
        if ($post_group_field == 'trans_destination_inner') {
            $name_datas_sql = "select port_code as id,port_name as name from biz_port where port_code in ('" . join('\',\'', $name_datas_ids) . "')";
            $name_datas = $this->m_model->query_array($name_datas_sql);
            $name_datas_key_by_id = array_column($name_datas, null, 'id');
            foreach ($name_datas_key_by_id as $key => $val){
                $name_datas_key_by_id[$key]['name'] = "<span style='color:red;'>{$val['id']} | </span>{$val['name']}";
            }
            unset($name_datas);
        }
        //获取name对应的名称填入--end
        $i = 1;
        //方案2 查询相关的所有账单及统计数据

        $rs_ids = filter_unique_array(explode(',', join(',', array_column($rs, 'ids'))));
        if (empty($rs_ids)) $rs_ids[] = 0;

        //2023-05-31 获取当前币种
        $currency_table_fields = Model('bsc_dict_model')->get_option("currency");
        //默认的数组值
        $defalut_bill_rs_field = array(
            'sell_amount_sum' => 0,//总应收
            'cost_amount_sum' => 0,//总应付
            'gross_profit_sum' => 0,//总毛利
            'sell_not_payment_amount' => 0,//未到账数
            'sell_payment_amount' => 0,//到账数
            'cost_payment_amount' => 0,//付款数
            'sell_after_tax_amount' => 0,//去税毛利(RMB)
            'cost_after_tax_amount' => 0,//去税毛利(RMB)
            'after_tax_gross_profit' => 0,//去税毛利(RMB)
            'sell_special_amount' => 0,//专项毛利(RMB)
            'cost_special_amount' => 0,//专项毛利(RMB)
            'special_gross_profit' => 0,//专项毛利(RMB)
            'sell_payment_amount_rate' => 0,//到账率
            'cost_payment_amount_rate' => 0,//付款率
            'gross_profit_rate' => 0,//毛利率
        );
        //币种的都填充下
        foreach ($currency_table_fields as $currency_table_field){
            $defalut_bill_rs_field["sell_{$currency_table_field['value']}_amount_sum"] = 0;
            $defalut_bill_rs_field["cost_{$currency_table_field['value']}_amount_sum"] = 0;
            $defalut_bill_rs_field["{$currency_table_field['value']}_gross_profit"] = 0;
        }
        //由于内存不够,这里分多次查询, 首先 5000个一批吧
        $currency_sql = "";
        foreach ($currency_table_fields as $currency_table_field){
            $currency_sql .= ",SUM((case when (biz_bill.type = 'sell' and biz_bill.currency = '{$currency_table_field['value']}') then amount else 0 end)) as sell_{$currency_table_field['value']}_amount_sum";
            $currency_sql .= ",SUM((case when (biz_bill.type = 'cost' and biz_bill.currency = '{$currency_table_field['value']}') then amount else 0 end)) as cost_{$currency_table_field['value']}_amount_sum";
        }

        $sql = "select id_no" .
            ",sum((case when (biz_bill.type = 'sell') then local_amount else 0 end)) as sell_amount_sum" .
            ",sum((case when (biz_bill.type = 'cost') then local_amount else 0 end)) as cost_amount_sum" .
            ",sum((case when (biz_bill.type = 'sell' and biz_bill.payment_id != 0) then local_amount else 0 end)) as sell_payment_amount" .
            ",sum((case when (biz_bill.type = 'cost' and biz_bill.payment_id != 0) then local_amount else 0 end)) as cost_payment_amount" .
            ",sum((case when (biz_bill.type = 'sell') then amount_no_tax else 0 end)) as sell_after_tax_amount" .
            ",sum((case when (biz_bill.type = 'cost') then amount_no_tax else 0 end)) as cost_after_tax_amount" .
            ",sum((case when (biz_bill.type = 'sell' and biz_bill.is_special = 1) then local_amount else 0 end)) as sell_special_amount" .
            ",sum((case when (biz_bill.type = 'cost' and biz_bill.is_special = 1) then local_amount else 0 end)) as cost_special_amount" . $currency_sql .
            // ",sum(amount) as amount" .
            // ",sum(local_amount) as local_amount " .
            // ",SUM(amount_no_tax) as amount_no_tax" . $currency_sql .
            // ",SUM((case when (biz_bill.is_special = 1) then local_amount else 0 end)) as special_local_amount" .
            // ",SUM((case when (biz_bill.payment_id != 0) then amount else 0 end)) as payment_amount" .
            // ",SUM((case when (biz_bill.payment_id != 0) then local_amount else 0 end)) as payment_local_amount" .
            " from biz_bill where id_type = 'shipment' and id_no in (" . join(',', $rs_ids) . ") group by id_no order by id_no";//,is_special,payment_id
        //将全部shipment的账单查询出来，且与税率分成多条
        $bills_rs = array_column($this->m_model->query_array($sql), null, 'id_no');

        $footer = $defalut_bill_rs_field;
        foreach ($rs as $row) {
            //循环取值后,将值都加到这个数组里
            foreach (explode(',', $row['ids']) as $row_id){
                $this_bill_rs = isset($bills_rs[$row_id]) ? $bills_rs[$row_id] : $defalut_bill_rs_field;
                foreach ($this_bill_rs as $tbr_key => $tbr_val){
                    if($tbr_key == 'id_no') continue;
                    !isset($row[$tbr_key]) && $row[$tbr_key] = 0;
                    $row[$tbr_key] = round($row[$tbr_key] + $tbr_val, 2);
                    $footer[$tbr_key] = round($footer[$tbr_key] + $tbr_val, 2);
                }
                //

            }
            //总毛利
            $row['gross_profit_sum'] = $row['sell_amount_sum'] - $row['cost_amount_sum'];
            //未到账数  sell_not_payment_amount
            $row['sell_not_payment_amount'] = $row['sell_amount_sum'] - $row['sell_payment_amount'];
            //去税毛利
            $row['after_tax_gross_profit'] = $row['sell_after_tax_amount'] - $row['cost_after_tax_amount'];
            //专项毛利 special_gross_profit
            $row['special_gross_profit'] = $row['sell_special_amount'] - $row['cost_special_amount'];

            foreach ($currency_table_fields as $currency_table_field) {
                $row["{$currency_table_field['value']}_gross_profit"] = $row["sell_{$currency_table_field['value']}_amount_sum"] - $row["cost_{$currency_table_field['value']}_amount_sum"];
                $footer["{$currency_table_field['value']}_gross_profit"] += $row["{$currency_table_field['value']}_gross_profit"];
            }

            //到账率 = 到账金额 除以 总金额
            $row['sell_payment_amount_rate'] = $row['sell_payment_amount'] > 0 ? round($row['sell_payment_amount'] / ($row['sell_amount_sum']) * 100, 2) : 0;
            $row['cost_payment_amount_rate'] = $row['cost_payment_amount'] > 0 ? round($row['cost_payment_amount'] / ($row['cost_amount_sum']) * 100, 2) : 0;

            $footer['sell_payment_amount_rate'] = $footer['sell_payment_amount'] > 0 ? round($footer['sell_payment_amount'] / ($footer['sell_amount_sum']) * 100, 2) : 0;
            $footer['cost_payment_amount_rate'] = $footer['cost_payment_amount'] > 0 ? round($footer['cost_payment_amount'] / ($footer['cost_amount_sum']) * 100, 2) : 0;

            $row['id'] = $i++;
            $row['name_id'] = trim($row['name']);
            $row['count'] = (int)$row['count'];
            $row['count_cancel'] = (int)$row['count_cancel'];
            $row['count_valid'] = $row['count'] - $row['count_cancel'];
            if (isset($name_datas_key_by_id[$row['name_id']])) {
                $row['name'] = $name_datas_key_by_id[$row['name_id']]['name'];
                if(!empty($name_datas_key_by_id[$row['name_id']]['group_name'])){
                    $row['sub_company_name'] = mb_substr($name_datas_key_by_id[$row['name_id']]['group_name'],0,4); 
                    $row['group_name'] = mb_substr($name_datas_key_by_id[$row['name_id']]['group_name'],4,strlen($name_datas_key_by_id[$row['name_id']]['group_name']) - 4); 
                } 
            } else {
                if (empty($row['name_id'])) {
                    $row['name_id'] = lang("无值");
                    $row['sub_company_name'] = lang("无值");
                    $row['group_name'] = lang("无值");
                }
                $row['name'] = '<span style="color:red;">' . $row['name_id'] . '</span>';
            }

            //box_info_sum解析所有box_info
            $export_box_info_sum = array_filter(explode(';', $row['export_box_info_sum']));
            $import_box_info_sum = array_filter(explode(';', $row['import_box_info_sum']));
            unset($row['export_box_info_sum']);
            unset($row['import_box_info_sum']);
            //业务类型：FCL， boxinfo里所有20开头的*1 + 40开头*2 + 45开头*2 = 总和

            $row['export_20_size'] = 0;
            $row['export_40_size'] = 0;
            $row['export_45_size'] = 0;
            foreach ($export_box_info_sum as $box_info) {
                $box_info = json_decode($box_info, true);
                foreach ($box_info as $bi_row) {
                    $size = substr($bi_row['size'], 0, 2);
                    $num = $bi_row['num'];
                    if ($size == '20') {
                        $row['export_20_size'] += $num;
                    } else if ($size == '40') {
                        $row['export_40_size'] += $num;
                    } else if ($size == '45') {
                        $row['export_45_size'] += $num;
                    }
                }
            }
            $row['export_TEU'] = $row['export_20_size'] + $row['export_40_size'] * 2 + $row['export_45_size'] * 2;

            $row['import_20_size'] = 0;
            $row['import_40_size'] = 0;
            $row['import_45_size'] = 0;

            foreach ($import_box_info_sum as $box_info) {
                $box_info = json_decode($box_info, true);
                foreach ($box_info as $bi_row) {
                    $size = substr($bi_row['size'], 0, 2);
                    $num = $bi_row['num'];
                    if ($size == '20') {
                        $row['import_20_size'] += $num;
                    } else if ($size == '40') {
                        $row['import_40_size'] += $num;
                    } else if ($size == '45') {
                        $row['import_45_size'] += $num;
                    }
                }
            }

            $row['import_TEU'] = $row['import_20_size'] + $row['import_40_size'] * 2 + $row['import_45_size'] * 2;

            $row['20_size'] = $row['import_20_size'] + $row['export_20_size'];
            $row['40_size'] = $row['import_40_size'] + $row['export_40_size'];
            $row['45_size'] = $row['import_45_size'] + $row['export_45_size'];

            $row['TEU'] = $row['export_TEU'] + $row['import_TEU'];

            //CBM
            $row['ex_LCL_good_outers'] = ROUND($row['ex_LCL_good_outers'], 4);
            $row['ex_LCL_good_weight'] = ROUND($row['ex_LCL_good_weight'], 4);
            $row['ex_LCL_good_volume'] = ROUND($row['ex_LCL_good_volume'], 4);
            $row['im_LCL_good_outers'] = ROUND($row['im_LCL_good_outers'], 4);
            $row['im_LCL_good_weight'] = ROUND($row['im_LCL_good_weight'], 4);
            $row['im_LCL_good_volume'] = ROUND($row['im_LCL_good_volume'], 4);
            $row['ex_AIR_good_outers'] = ROUND($row['ex_AIR_good_outers'], 4);
            $row['ex_AIR_good_weight'] = ROUND($row['ex_AIR_good_weight'], 4);
            $row['ex_AIR_good_volume'] = ROUND($row['ex_AIR_good_volume'], 4);
            $row['im_AIR_good_outers'] = ROUND($row['im_AIR_good_outers'], 4);
            $row['im_AIR_good_weight'] = ROUND($row['im_AIR_good_weight'], 4);
            $row['im_AIR_good_volume'] = ROUND($row['im_AIR_good_volume'], 4);

            $row['ex_LCL_CBM'] = $row['ex_LCL_good_outers'] . '/' . $row['ex_LCL_good_weight'] . '/' . $row['ex_LCL_good_volume'];
            $row['im_LCL_CBM'] = $row['im_LCL_good_outers'] . '/' . $row['im_LCL_good_weight'] . '/' . $row['im_LCL_good_volume'];

            $row['ex_AIR_KGS'] = $row['ex_AIR_good_outers'] . '/' . $row['ex_AIR_good_weight'] . '/' . $row['ex_AIR_good_volume'];
            $row['im_AIR_KGS'] = $row['im_AIR_good_outers'] . '/' . $row['im_AIR_good_weight'] . '/' . $row['im_AIR_good_volume'];
            $rows[] = $row;

            //总和

            $footer['count'] = isset($footer['count']) ? $footer['count'] + $row['count'] : 0 + $row['count'];
            $footer['count_cancel'] = isset($footer['count_cancel']) ? $footer['count_cancel'] + $row['count_cancel'] : 0 + $row['count_cancel'];
            $footer['export_TEU'] = isset($footer['export_TEU']) ? $footer['export_TEU'] + $row['export_TEU'] : 0 + $row['export_TEU'];
            $footer['import_TEU'] = isset($footer['import_TEU']) ? $footer['import_TEU'] + $row['import_TEU'] : 0 + $row['import_TEU'];
            $footer['TEU'] = isset($footer['TEU']) ? $footer['TEU'] + $row['TEU'] : 0 + $row['TEU'];
            $footer['20_size'] = isset($footer['20_size']) ? $footer['20_size'] + $row['20_size'] : 0 + $row['20_size'];
            $footer['40_size'] = isset($footer['40_size']) ? $footer['40_size'] + $row['40_size'] : 0 + $row['40_size'];
            $footer['45_size'] = isset($footer['45_size']) ? $footer['45_size'] + $row['45_size'] : 0 + $row['45_size'];

            $footer['ex_LCL_good_outers'] = isset($footer['ex_LCL_good_outers']) ? $footer['ex_LCL_good_outers'] + $row['ex_LCL_good_outers'] : 0 + $row['ex_LCL_good_outers'];
            $footer['ex_LCL_good_weight'] = isset($footer['ex_LCL_good_weight']) ? $footer['ex_LCL_good_weight'] + $row['ex_LCL_good_weight'] : 0 + $row['ex_LCL_good_weight'];
            $footer['ex_LCL_good_volume'] = isset($footer['ex_LCL_good_volume']) ? $footer['ex_LCL_good_volume'] + $row['ex_LCL_good_volume'] : 0 + $row['ex_LCL_good_volume'];

            $footer['im_LCL_good_outers'] = isset($footer['im_LCL_good_outers']) ? $footer['im_LCL_good_outers'] + $row['im_LCL_good_outers'] : 0 + $row['im_LCL_good_outers'];
            $footer['im_LCL_good_weight'] = isset($footer['im_LCL_good_weight']) ? $footer['im_LCL_good_weight'] + $row['im_LCL_good_weight'] : 0 + $row['im_LCL_good_weight'];
            $footer['im_LCL_good_volume'] = isset($footer['im_LCL_good_volume']) ? $footer['im_LCL_good_volume'] + $row['im_LCL_good_volume'] : 0 + $row['im_LCL_good_volume'];

            $footer['ex_AIR_good_outers'] = isset($footer['ex_AIR_good_outers']) ? $footer['ex_AIR_good_outers'] + $row['ex_AIR_good_outers'] : 0 + $row['ex_AIR_good_outers'];
            $footer['ex_AIR_good_weight'] = isset($footer['ex_AIR_good_weight']) ? $footer['ex_AIR_good_weight'] + $row['ex_AIR_good_weight'] : 0 + $row['ex_AIR_good_weight'];
            $footer['ex_AIR_good_volume'] = isset($footer['ex_AIR_good_volume']) ? $footer['ex_AIR_good_volume'] + $row['ex_AIR_good_volume'] : 0 + $row['ex_AIR_good_volume'];

            $footer['im_AIR_good_outers'] = isset($footer['im_AIR_good_outers']) ? $footer['im_AIR_good_outers'] + $row['im_AIR_good_outers'] : 0 + $row['im_AIR_good_outers'];
            $footer['im_AIR_good_weight'] = isset($footer['im_AIR_good_weight']) ? $footer['im_AIR_good_weight'] + $row['im_AIR_good_weight'] : 0 + $row['im_AIR_good_weight'];
            $footer['im_AIR_good_volume'] = isset($footer['im_AIR_good_volume']) ? $footer['im_AIR_good_volume'] + $row['im_AIR_good_volume'] : 0 + $row['im_AIR_good_volume'];
        }
        //排序--start
        //2022-08-23 新增一个如果post没有传参,那么这里默认查询 如果后面还要加的话,可以封装一下代码
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "name";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';
        $config_sort = Model('sys_config_model')->get_config('report_profit_total_sort');
        $config_order = Model('sys_config_model')->get_config('report_profit_total_order');
        if(isset($_POST['sort'])){
            //存储一下config
            Model('sys_config_model')->save('report_profit_total_sort', $sort, true);
        }else{
            $sort = $config_sort == '' ? $sort : $config_sort;
        }
        if(isset($_POST['order'])){
            //存储一下config
            Model('sys_config_model')->save('report_profit_total_order', $order, true);
        }else{
            $order = $config_order == '' ? $order : $config_order;
        }
        $ext_data['order'] = $order;
        $ext_data['sort'] = $sort;

        $s = array();
        foreach ($rows as $key => $row) {
            if(!isset($row[$sort])) $sort="name";
            $s[$key] = isset($row[$sort]) ? $row[$sort] : $row[$sort];
        }
        if ($order == 'desc') $order = SORT_DESC;
        else $order = SORT_ASC;
        array_multisort($s, $order, $rows);

        // $footer['sell_amount_sum'] = float_number($footer['sell_amount_sum']);
        // $footer['cost_amount_sum'] = float_number($footer['cost_amount_sum']);
        // $footer['USD_gross_profit'] = float_number($footer['USD_gross_profit']);
        // $footer['CNY_gross_profit'] = float_number($footer['CNY_gross_profit']);
        // $footer['gross_profit_sum'] = float_number($footer['gross_profit_sum']);
        isset($footer['TEU']) && $footer['TEU'] = float_number($footer['TEU']);
        // $footer['sell_payment_amount'] = float_number($footer['sell_payment_amount']);
        // $footer['cost_payment_amount'] = float_number($footer['cost_payment_amount']);
        // $footer['after_tax_gross_profit'] = float_number($footer['after_tax_gross_profit']);
        isset($footer['export_TEU']) && $footer['export_TEU'] = float_number($footer['export_TEU']);
        $footer['is_footer'] = true;
        $footer['name'] = lang("总和") . '：';
        isset($footer['rate_cancel']) && $footer['rate_cancel'] = $footer['count'] == 0 ? 0 : ROUND($footer['count_cancel'] / $footer['count'], 4);
        isset($footer['ex_LCL_CBM']) && $footer['ex_LCL_CBM'] = float_number($footer['ex_LCL_good_outers']) . '/' . float_number($footer['ex_LCL_good_weight']) . '/' . float_number($footer['ex_LCL_good_volume']);
        isset($footer['im_LCL_CBM']) && $footer['im_LCL_CBM'] = float_number($footer['im_LCL_good_outers']) . '/' . float_number($footer['im_LCL_good_weight']) . '/' . float_number($footer['im_LCL_good_volume']);

        isset($footer['ex_AIR_KGS']) && $footer['ex_AIR_KGS'] = float_number($footer['ex_AIR_good_outers']) . '/' . float_number($footer['ex_AIR_good_weight']) . '/' . float_number($footer['ex_AIR_good_volume']);
        isset($footer['im_AIR_KGS']) && $footer['im_AIR_KGS'] = float_number($footer['im_AIR_good_outers']) . '/' . float_number($footer['im_AIR_good_weight']) . '/' . float_number($footer['im_AIR_good_volume']);

        echo json_encode(array('rows' => $rows, 'total' => sizeof($rows), 'footer' => array($footer), 'ext_data' => $ext_data));
    }
    
    public function lrb()
    {
        $data = array();
        $is_special = getValue('is_special', 0);
        $sql = "SELECT distinct user_id,(select `name` from bsc_user where id = user_id) as user_name FROM `bsc_user_commision_leader`";
        $team_leader = $this->m_model->query_array($sql);

        //2023-05-31 获取当前币种
        $data['currency_table_fields'] = array_column(Model('bsc_dict_model')->get_option("currency"), 'value');
        $data['local_currency'] = get_system_config("CURRENT_CURRENCY");

        $data['is_special'] = $is_special;
        $data['team_leader'] = $team_leader;
        $this->load->view('head');
        $this->load->view('/report/bill/lrb_view.php', $data);
    }

    public function get_lrb_data()
    {
        ini_set('memory_limit', '1024M');
        $user_role = postValue('user_role', '');
        $tc_type = postValue('tc_type', '');//提成类型, 当为sales 或team 时, 才开启 提成信息的获取
        $sales_leader_tc = postValue('sales_leader_tc', 0);//团队领导
        $user_ids = postValue('user_ids', '');
        $fields = postValue('field', array());

        if(is_array($user_ids)) $user_ids = join(',', $user_ids);
        $user_ids_arr = filter_unique_array(explode(',', $user_ids));

        $is_special = (int)postValue('is_special', 0);
        $lock_lv = postValue('lock_lv', array());
        $ext_data = array();
        // echo '调试中';
        // return;
        $where = array();
        $shipment_table = "biz_shipment";
        $consol_table = "biz_consol";

        //获取权限
        if (!is_admin()) {
            $userId = get_session('id');
            $userRange = join("','", array_filter(explode(',', get_session('group_range'))));
            $user_range = join(',', get_session('user_range'));
            $this_where = array();
            $this_where[] = Model('bsc_user_role_model')->get_role("'biz_shipment'", 'biz_shipment.id');
            // }
            $where[] = '(' . join(' OR ', $this_where) . ')';
        }
        if (!empty($lock_lv)) {
            $consol_lock_lv_where = array();
            $lock_lv_where = array();
            if (in_array('C0', $lock_lv)) $consol_lock_lv_where[] = "biz_consol.lock_lv = 0";
            if (in_array('C3', $lock_lv)) $consol_lock_lv_where[] = "biz_consol.lock_lv = 3";
            if (in_array('S0', $lock_lv)) $lock_lv_where[] = "biz_shipment.lock_lv = 0";
            if (in_array('S1', $lock_lv)) $lock_lv_where[] = "biz_shipment.lock_lv = 1";
            if (in_array('S2', $lock_lv)) $lock_lv_where[] = "biz_shipment.lock_lv = 2";
            if (in_array('S3', $lock_lv)) $lock_lv_where[] = "biz_shipment.lock_lv = 3";
            if (!empty($consol_lock_lv_where)) $where[] = "(" . join(' OR ', $consol_lock_lv_where) . ")";
            if (!empty($lock_lv_where)) $where[] = "(" . join(' OR ', $lock_lv_where) . ")";
        }

        if(!empty($user_role)){
            if(!empty($user_ids_arr)){
                if ($user_role == 'sales') {
                    $where[] = "(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = '{$user_role}') in ('" . join('\',\'', $user_ids_arr) . "')";
                } else {
                    //如果不是销售，那么
                    $where[] = "(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = '{$user_role}') in (" . join(',', $user_ids_arr) . ") " .
                        "and (select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'sales') not in ('" . join('\',\'', $user_ids_arr) . "')";
                }
            }
        }

        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_bill', 'report_lrb');//获取相关的配置信息
        if (!empty($fields)) {
            foreach ($fields['f'] as $key => $field) {
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                $v_arr = array_unique(explode("\r\n", $v));
                if ($f == '') continue;
                $v_arr = array_filter($v_arr, function ($r){
                    $r = trim($r);
                    if($r === "") return false;
                    else return true;
                });

                $f_arr = explode('.', $f);
                if($f_arr[0] == 'consol'){
                    if(!empty($v_arr)){
                        if($f_arr[1] == 'creditor'){
                            $this_where = array();
                            foreach ($v_arr as $vv){
                                $this_where[] = search_like('company_search', $s, match_chinese($vv));
                            }
                            $this_where = join(' or ', $this_where);
                            $clients = Model('biz_client_model')->no_role_get($this_where);
                            $client_codes = join("','", array_column($clients, 'client_code'));
                            $this_where = "biz_consol.creditor in ('$client_codes')";
                        }else{
                            $this_where = array();
                            foreach ($v_arr as $vv){
                                if($f_arr[1] == 'trans_ATD'){
                                    if($s == '>=') $vv = $vv . ' 00:00:00';
                                    if($s == '<=') $vv = $vv . ' 23:59:59';
                                }
                                $this_where[] = search_like("biz_consol.{$f_arr[1]}", $s, $vv);
                            }
                            $this_where = join(' or ', $this_where);
                        }

                        $where[] = $this_where;
                    }
                    continue;
                }else if($f_arr[0] == 'shipment'){
                    if(!empty($v_arr)){
                        $where[] = search_like("biz_shipment.{$f_arr[1]}", $s, $v);
                    }
                }

                if(!isset($title_data['sql_field'][$f])) continue;

                $f_sql_field = $title_data['sql_field'][$f];

                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if($v != ''){
                        $where[] = "{$f_sql_field} >= '{$v} 00:00:00' and {$f_sql_field} <= '{$v} 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if($v != ''){
                        $where[] = "{$f_sql_field} {$s} '{$v} 23:59:59'";
                    }
                    continue;
                }

                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') continue;
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$f]['sql_join']);
                    if(!empty($this_join)) {
                        $this_join0 = explode(' ', $this_join[0]);
                        $title_data['sql_join'][end($this_join0)] = $this_join;
                    }
                    //2022-12-27 加入新逻辑，根据逗号切割传过来的值循环处理
                    //这里的话，如果用的 = ，可以改为 in直接连接
                    $v_arr = explode(',', $v);
                    $this_where = array();
                    foreach ($v_arr as $v_val){
                        $this_where[] = search_like($f_sql_field, $s, $v_val);
                    }

                    $where[] = '(' . join(' or ', $this_where) . ')';
                }
            }
        }
        if (!empty($ids)) $where[] = "biz_bill.id in ($ids)";
        $where[] = "biz_bill.id_type = 'shipment'";
//        $where[] = "biz_bill.id = 0";

        $where = join(' and ', $where);

        //2023-05-31 获取当前币种
        $currency_table_fields = Model('bsc_dict_model')->get_option("currency");
        $currency_sql = "";
        //拼接币种的金额统计 sql
        foreach ($currency_table_fields as $currency_table_field){
            $currency_sql .= ",SUM((case when (biz_bill.type = 'sell' and biz_bill.currency = '{$currency_table_field['value']}') then amount else 0 end)) as {$currency_table_field['value']}_sell_amount";
            $currency_sql .= ",SUM((case when (biz_bill.type = 'cost' and biz_bill.currency = '{$currency_table_field['value']}') then amount else 0 end)) as {$currency_table_field['value']}_cost_amount";
            $currency_sql .= ",SUM((case when (biz_bill.currency = '{$currency_table_field['value']}') then (case when (biz_bill.type = 'sell') then amount else -amount end) else 0 end)) as {$currency_table_field['value']}_profit";
        }

        //这里把当前不计提标记的, 顺便拼这里把
        //,biz_shipment.bujiti_sales,biz_shipment.bujiti_customer_service,biz_shipment.bujiti_operator
        //如果是销售助理, 跟着销售计提的
        $bujiti_fields = array(
            'sales' => 'bujiti_sales',
            'sales_assistant' => 'bujiti_sales',// 由于销售助理是从销售那里抽成的, 所以这里使用的销售不计提作为处理
            'customer_service' => 'bujiti_customer_service',
            'operator' => 'bujiti_operator',
        );
//        if(in_array($user_role, array('sales', 'sales_assistant', 'customer_service', 'operator'))){
        if(isset($bujiti_fields[$user_role])){
            $currency_sql .= ",biz_shipment.{$bujiti_fields[$user_role]} as is_bujiti";
        }else{
            $currency_sql .= ",0 as is_bujiti";
        }

        //获取数据, 相较于原版, 这里提前根据job_no group好
        $this->db->select("biz_bill.id_type, biz_bill.id_no, biz_shipment.job_no,biz_shipment.trans_origin_name,biz_shipment.trans_destination_name,biz_shipment.client_code" .
            ",biz_consol.job_no as consol_no, biz_consol.carrier_ref,biz_consol.trans_ATD,biz_consol.vessel,biz_consol.voyage" .
            ",(select biz_client.company_name from biz_client where biz_client.client_code = biz_shipment.client_code) as client_code_name" .
            ",sum((case when (biz_bill.type = 'sell') then local_amount else -local_amount end)) as gross_profit" .
            ",sum((case when (biz_bill.type = 'sell') then local_amount else 0 end)) as sum_sell_local_amount" .
            ",sum((case when (biz_bill.type = 'sell' and biz_bill.payment_id != 0) then local_amount else 0 end)) as payment_sell_local_amount" .
            ",GROUP_CONCAT(biz_bill.invoice_id) as invoice_ids" .
            ",biz_shipment.box_info" .
            ",(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'sales') as sales_id" .
            // ",(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'sales_assistant') as sales_assistant_id" .
            ",(select biz_duty_new.user_group from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'sales') as sales_group" .
            ",(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'customer_service') as customer_service_id" .
            ",(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = biz_shipment.id and biz_duty_new.user_role = 'operator') as operator_id" .
            $currency_sql .
            "", false);
        $this->db->group_by("biz_shipment.job_no");
        $this->db->join("{$shipment_table} biz_shipment", "biz_bill.id_type = 'shipment' and biz_shipment.id = biz_bill.id_no", 'left');
        $this->db->join("{$consol_table} biz_consol", "biz_shipment.consol_id = biz_consol.id", 'left');
        $rs = Model('biz_bill_model')->get($where, "biz_bill.id", "desc", $is_special, '');

        $expTableData = array();

        //整理用户ID 进行统一名称转换
        $rs_user_ids = filter_unique_array(array_merge(array_column($rs, 'sales_id'), array_column($rs, 'sales_assistant_id'), array_column($rs, 'operator_id'), array_column($rs, 'customer_service_id')));
        $this->db->select('id, name');
        $users = array_column(Model('bsc_user_model')->get("id in ('" . join('\',\'', $rs_user_ids) . "')"), null, 'id');
        unset($userIds);

        //获取组名称
        $userGroupcodes = filter_unique_array(array_column($rs, 'sales_group'));
        $this->db->select('group_code,group_name');
        $groups = array_column(Model('bsc_group_model')->get("group_code in ('" . join('\',\'', $userGroupcodes) . "')"), null, 'group_code');
        unset($userGroupcodes);

        //获取发票信息
        $invoice_ids = filter_unique_array(array_column($rs, 'invoice_ids'));
        $this->db->select('id,invoice_type');
        $invoices = array_column(Model('biz_bill_invoice_model')->get("id in ('" . join('\',\'', $invoice_ids) . "')"), null, 'id');
        unset($invoice_ids);

        //查出发票类型对应的名称
        $invoice_type = array_column(Model('bsc_dict_model')->get_option('invoice_type'), 'name', 'value');
        //首先提前统计出除非特殊外的总金额
        //由于页面上固定了前两个是时间的, 所以这里就简单写成了这样
        $booking_ETD_start = $fields['v'][0];
        $booking_ETD_end = $fields['v'][1];

        $user_id = isset($user_ids_arr[0]) ? $user_ids_arr[0] : 0;
        $special_clients = array();//特殊客户提成
        $assistant_commision = array();//助理提成
        $fts_commision_rate = 0;//非特殊提成
        $points = 0;//税点
        //销售提成时
        $ext_data['tc_text'] = "***";
        $ext_data['tc_amount'] = "***";
        if($tc_type == 'sales'){
            //当 个人提成时, user_ids只有1个
            $sql = "select commision_rate,client_code from bsc_user_client_commision_rate where client_code in ('" . join('\',\'', array_column($rs, 'client_code')) . "') and user_id = {$user_id} and user_role = '{$user_role}'";
            $special_clients = array_column($this->m_model->query_array($sql), null, "client_code");

            //将非特殊的金额统计出来
            $fts_amount = array_sum(array_column(array_filter($rs, function ($r) use($special_clients){
                if(isset($special_clients[$r['client_code']])){
                    return false;
                }else{
                    return true;
                }
            }), 'gross_profit'));

            //获取非特殊的提成比例
            $sql = "select id,amount,user_role,commision_rate from bsc_user_commision_rate where user_id = {$user_id} and user_role = '{$user_role}' and amount >= {$fts_amount} and `datetime` <= '".date("Y-m-01",strtotime($booking_ETD_start))."' order by amount asc, datetime desc";
            $temp_row = Model('m_model')->query_one($sql);
            $fts_commision_rate = !empty($temp_row) ? $temp_row['commision_rate'] : 0;

            //单票操作费
            $s_czf = get_system_config('SHIPMENT_CZF');

            //检测当前是否存在助理格子
            $assistants_ids = array_column($rs, "{$user_role}_assistants_id");
            if(!empty($assistants_ids)){
                //当存在助理信息时, 开始查询助理的相关抽成
                $sql = "SELECT user_id,
                    	( SELECT commision_rate FROM bsc_user_commision_rate b WHERE b.user_id = a.user_id AND datetime <= '".date("Y-m-01",strtotime($booking_ETD_start))."' ORDER BY datetime DESC LIMIT 1 ) AS c 
                    FROM
                    	`bsc_user_commision_rate` a 
                    WHERE
                    	user_role = 'sales_assistant' 
                    	AND user_id IN ('" . join(',', $assistants_ids) . "') 
                    GROUP BY
                    	user_id;";
                $assistant_commision = $this->m_model->query_array($sql);
                $assistant_commision = array_column($assistant_commision,"c","user_id");
            }
        }else if(menu_role('report_lrb_commission_team') && $tc_type == 'team'){
            //团队提成
            //获取总制单毛利
            $amount = array_sum(array_column($rs, 'gross_profit'));

            //获取领导的 提成
            $sql = "select * from bsc_user_commision_leader where user_id = {$sales_leader_tc} and user_role='{$user_role}' and amount >= {$amount} and m_year <= '" . substr($booking_ETD_start, 0, 4) . "' ORDER by m_year desc";
            $temp_row = Model('m_model')->query_one($sql);
            if(!empty($temp_row)){
                $fts_commision_rate = $temp_row['rate'];
                $points = $temp_row['points'] / 100;
            }
        }
        $num = 1;
        foreach ($rs as $key => $row) {
            //给个最前面的序号,没啥用
            $row['num'] = $num++;
            isset($s_czf) && $row['s_czf'] = -$s_czf;//操作费,只用于显示计算, 没什么大用

            //将名称和组名转成对应的中文
            $row['sales_group_name'] = join(',', array_map(function ($r) use($groups){
                if(isset($groups[$r])){
                    return $groups[$r]['group_name'];
                }else{
                    return $r;
                }
            }, explode(',', $row['sales_group'])));
            $row['sales'] = join(',', array_map(function ($r) use($users){
                if(isset($users[$r])){
                    return $users[$r]['name'];
                }else{
                    return $r;
                }
            }, explode(',', $row['sales_id'])));
            $row['customer_service'] = join(',', array_map(function ($r) use($users){
                if(isset($users[$r])){
                    return $users[$r]['name'];
                }else{
                    return $r;
                }
            }, explode(',', $row['customer_service_id'])));
            $row['operator'] = join(',', array_map(function ($r) use($users){
                if(isset($users[$r])){
                    return $users[$r]['name'];
                }else{
                    return $r;
                }
            }, explode(',', $row['operator_id'])));


            //将发票类型转换为中文
            $row['invoice_type_name'] = join(',', filter_unique_array(array_map(function ($r) use($invoices,$invoice_type){
                //如果外面要用就拆出去单独弄吧
                if(isset($invoices[$r])){
                    $this_invoice = $invoices[$r];
                    if(isset($invoice_type[$this_invoice['invoice_type']])){
                        return $invoice_type[$this_invoice['invoice_type']];
                    }else{
                        return $this_invoice['invoice_type'];
                    }
                }else{
                    //不存在发票信息就返回空
                    return "";
                }
            }, explode(',', $row['invoice_ids']))));
//            $row['invoice_type_name'] = join(',', , explode(',', $row['invoice_type'])));
            //箱型箱量转换为文案
            $row['box_info_text'] = array();
            foreach (json_decode($row['box_info'], true) as $box) {
                $row['box_info_text'][] = $box['num'] . '*' . $box['size'];
            }
            $row['box_info_text'] = join(',', $row['box_info_text']);

            if ($row['sum_sell_local_amount'] == 0) $row['return_rate'] = '100%';
            else $row['return_rate'] = round($row['payment_sell_local_amount'] / $row['sum_sell_local_amount'] * 100, 2) . '%';//回款率

            //如果是个人提成
            $row['ts_client'] = false;
            $row['commision_rate'] = "";//提成比例
            $row['commision_amount'] = "";//提成金额
            $row['assistants_commision_rate'] = "";//助理提成比例
            $row['assistants_commision_amount'] = "";//助理提成金额
            if($tc_type == 'sales'){
                //填充当前的提成,
                if(isset($special_clients[$row['client_code']])){
                    $row['ts_client'] = true;
                    //特殊提成
                    $row['commision_rate'] = $special_clients[$row['client_code']]['commision_rate'] . '%';
                    $row['commision_amount'] = round(($row['gross_profit'] - $s_czf) * ((int)$row['commision_rate'] / 100), 2);
                }else{
                    //常规提成
                    $row['commision_rate'] = $fts_commision_rate . '%';
                    $row['commision_amount'] = round(($row['gross_profit'] - $s_czf) * ((int)$fts_commision_rate / 100), 2);
                }
                //如果当前是销售, 且是个人提成的 话, 要显示出 助理 抽了多少
                if(!empty($assistant_commision) && isset($row["{$user_role}_assistant_id"])){
                    //助理的话, 是从销售提成里抽取的
                    $row['assistants_commision_rate'] = $assistant_commision[$row["{$user_role}_assistant_id"]] . '%';
                    $row['assistants_commision_amount'] = round($row['gross_profit'] * ((int)$fts_commision_rate / 100), 2);

                    if((int)$row['commision_rate'] >= (int)$row['assistants_commision_rate']){
                        $row['commision_rate'] = ((int)$row['commision_rate'] - (int)$row['assistants_commision_rate']) . '%';
                        $row['commision_amount'] = round($row['commision_amount'] - $row['assistants_commision_amount'], 2);
                    }else{
                        $row['commision_rate'] = '0%';
                        $row['commision_amount'] = 0;
                    }
                }
            }else if($tc_type == 'team'){
                //团队的直接计算即可
                //常规提成
                $row['commision_rate'] = $fts_commision_rate . '%';
                $row['commision_amount'] = round(($row['gross_profit']) * ((int)$fts_commision_rate / 100), 2);
            }

            //如果不计提且 制单毛利 大于0, 那么 提成为0
            //2022-07-10 利润小于0不算入不计提
            if($row['is_bujiti'] === '1' && $row['gross_profit'] >= 0){
                $row['commision_rate'] = '0%';
                $row['commision_amount'] = 0;
                $row['assistants_commision_rate'] = "0%";
                $row['assistants_commision_amount'] = 0;
            }

            $expTableData[] = $row;
        }
        unset($rs);
        if($tc_type == 'sales'){
            //开始计算金额的信息
            //获取税点
            $sql = "select * from bsc_user_quota_points where user_id = '{$user_id}' and `datetime`<= '".date("Y-m-01", strtotime($booking_ETD_start))."' order by `datetime` desc limit 1";
            $temp_row = Model('m_model')->query_one($sql);
            if(empty($temp_row)) {
                $points = 0;
                $sales_quota = get_system_config('DEDUCTING_TAX_POINT');
            } else {
                $points = $temp_row['points'] / 100;
                $sales_quota = $temp_row['sales_quota'];
            }

            //获取快递费
            $sql = "select id,amount from bsc_user_express_fee where user_id = {$user_id} and date >= '{$booking_ETD_start}' and date <= '{$booking_ETD_end}' and type = 'EXPRESS_FEE'";
            $temp_row = Model('m_model')->query_one($sql);
            $express_fee = !empty($temp_row) ? $temp_row['amount'] : 0;

            //获取滞纳金
            $sql = "select id,amount from bsc_user_express_fee where user_id = {$user_id} and date >= '{$booking_ETD_start}' and date <= '{$booking_ETD_end}' and type = 'LATE_FEE'";
            $temp_row = Model('m_model')->query_one($sql);
            $late_fee = !empty($temp_row) ? $temp_row['amount'] : 0;

            //获取其他费用
            $sql = "select id,amount from bsc_user_express_fee where user_id = {$user_id} and date >= '{$booking_ETD_start}' and date <= '{$booking_ETD_end}' and type = 'OTHER_FEE'";
            $temp_row = Model('m_model')->query_one($sql);
            $other_fee = !empty($temp_row) ? $temp_row['amount'] : 0;

            //看了下公式, 销售和其他不用分开代码, 直接替换就行了
            if(in_array($user_role, array('sales'))){
                $formula = "{总提成金额} * (1 - {税点}) - ({销售指标}+{快递费}+{其他扣款}+{滞纳金})*(1-{税点})*{非特殊提成比例}";//这个是计算公式, 后面填充进内容后 会自动进行
            }else{
                $formula = "{总提成金额} * (1 - {税点}) - ({快递费})*(1-{税点})*{非特殊提成比例}";//这个是计算公式, 后面填充进内容后 会自动进行
            }

            $total_commision_amount = array_sum(array_column($expTableData, 'commision_amount'));//总毛利


            $amount = eval("return ROUND(" . lang($formula, array(
                    '总提成金额' => $total_commision_amount,
                    // '不计提金额' => $bujiti_gross_profit,
                    '销售指标' => $sales_quota,
                    '快递费' => $express_fee,
                    '其他扣款' => $other_fee,
                    '滞纳金' => $late_fee,
                    '税点' => $points,
                    '非特殊提成比例' => $fts_commision_rate / 100,
                )) . ', 2);');

            //以前
            //总金额 = 特殊客户提成+ 非特殊客户提成
            //非特殊提成 = （非特殊总毛利 - 不计提金额 - 销售指标 - 快递费 - 其他扣款 - 滞纳金 - 票数*操作费） * (1 - 税点) * 提成比例
            //上面 =  非特殊总毛利 * (1- 税点) * 提成比例 - (不计提金额 + 销售指标 + 快递费 + 其他扣款 + 滞纳金) * ( 1- 税点) *
            //常规提成比例
            // 特殊提成 = 特殊总提成 * (1 - 税点)

            //新版
            // 总金额 = 总提成金额 * (1 - 税点) - (不计提金额 + 销售指标 + 快递费 + 其他扣款 + 滞纳金) * ( 1- 税点) * 常规提成比例
            //


            $ext_data['tc_amount'] = $amount;
            $ext_data['tc_text'] = lang("个人提成") . lang("：{$formula} = {提成金额}", array(
                '总提成金额' => $total_commision_amount,
                // '不计提金额' => $bujiti_gross_profit,
                '销售指标' => lang('销售指标') . "({$sales_quota})",
                '快递费' => lang('快递费') . "({$express_fee})",
                '其他扣款' => lang('其他扣款') . "({$other_fee})",
                '滞纳金' => lang('滞纳金') . "({$late_fee})",
                // '票数' => "票数({$shipment_count})",
                // '操作费' => "操作费({$s_czf})",
                '税点' => lang('税点') . "({$points})",
                '非特殊提成比例' => $fts_commision_rate / 100,
                '提成金额' => $amount,
            ));
        }else if(menu_role('report_lrb_commission_team') && $tc_type == 'team'){
            $formula = "{毛利} * {提成} * (1 - {税点})";//这个是计算公式, 后面填充进内容后 会自动进行

            $total_commision_amount = array_sum(array_column($expTableData, 'commision_amount'));//总毛利

            $amount = eval("return ROUND(" . lang($formula, array(
                    '毛利' => $total_commision_amount,
                    '税点' => $points,
                    '提成' => $fts_commision_rate / 100,
                )) . ', 2);');

            $ext_data['tc_amount'] = $amount;
            $ext_data['tc_text'] = lang("团队提成") . lang("：{$formula} = {提成金额}", array(
                '毛利' => $total_commision_amount,
                '税点' => "税点({$points})",
                '提成' => $fts_commision_rate / 100,
                '提成金额' => $amount,
            ));
        }

        //排序--start
        //2022-08-23 新增一个如果post没有传参,那么这里默认查询
        $sort = postValue('sort', 'num');
        $order = postValue('order', 'asc');
        $config_sort = Model('sys_config_model')->get_config('report_lrb_sort');
        $config_order = Model('sys_config_model')->get_config('report_lrb_order');
        if(isset($_POST['sort'])){
            //存储一下config
            Model('sys_config_model')->save('report_lrb_sort', $sort, true);
        }else{
            $sort = $config_sort == '' ? $sort : $config_sort;
        }
        if(isset($_POST['order'])){
            //存储一下config
            Model('sys_config_model')->save('report_lrb_order', $order, true);
        }else{
            $order = $config_order == '' ? $order : $config_order;
        }
        $ext_data['order'] = $order;
        $ext_data['sort'] = $sort;

        $s = array();
        foreach ($expTableData as $key => $row) {
            $s[$key] = isset($row[$sort]) ? $row[$sort] : $row[$sort];
        }


        if ($order == 'desc') $order = SORT_DESC;
        else $order = SORT_ASC;
        array_multisort($s, $order, $expTableData);
        unset($s);
        //排序--end

        return jsonEcho(array('code' => 0, 'rows' => $expTableData, 'total' => sizeof($expTableData), 'ext_data' => $ext_data));
    }
    
    public function lrb20230803()
    {
        $is_special = isset($_GET['is_special']) ? $_GET['is_special'] : 0;
        // $sql = "SELECT distinct user_id,(select `name` from bsc_user where id = user_id) as user_name FROM `bsc_user_commision_leader`";
        // $team_leader = $this->m_model->query_array($sql);

        $data = array(); 
        $data['is_special'] = $is_special;
        //2023-05-31 获取当前币种
        $data['currency_table_fields'] = array_column(Model('bsc_dict_model')->get_option("currency"), 'value');
        $data['local_currency'] = get_system_config("CURRENT_CURRENCY");
        // $data['team_leader'] = $team_leader;
        $this->load->view('head');
        $this->load->view('/report/bill/lrb_view.php', $data);
    }

    public function get_lrb_data20230803()
    {
        ini_set('memory_limit', '1024M');
        $users = isset($_REQUEST['users']) ? $_REQUEST['users'] : array();
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

        $title = isset($_REQUEST['title']) ? trim($_REQUEST['title']) : '';
        $currency = isset($_REQUEST['currency']) ? trim($_REQUEST['currency']) : '';
        $type = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
        $groups = isset($_REQUEST['group']) ? $_REQUEST['group'] : array();
        $group_role = isset($_REQUEST['group_role']) ? trim($_REQUEST['group_role']) : '';
        $is_special = isset($_REQUEST['is_special']) ? $_REQUEST['is_special'] : 0;
        $lock_lv = isset($_REQUEST['lock_lv']) ? $_REQUEST['lock_lv'] : array();
        $result = array();
        $ext_data = array();
        $where = array();
        $shipment_table = 'biz_shipment';
        $consol_table = 'biz_consol';

        //获取权限
        $userRole = get_session('user_role');
        $user_role = $users[0]['user_role'];
        if (!is_admin()) {// && !in_array('finance', $userRole)
            $userId = get_session('id');
            $userRange = join("','", array_filter(explode(',', get_session('group_range'))));
            $user_range = join(',', get_session('user_range'));
            $this_where = array();
            $this_where[] = Model('bsc_user_role_model')->get_role("'biz_shipment'", 'bs.id');
            // }
            $where[] = '(' . join(' OR ', $this_where) . ')';

        }
        if (in_array('finance', $userRole)) {
        }
        if (!empty($lock_lv)) {
            $consol_lock_lv_where = array();
            $lock_lv_where = array();
            if (in_array('C0', $lock_lv)) $consol_lock_lv_where[] = "(select lock_lv from biz_consol where biz_consol.id = bs.consol_id) = 0";
            if (in_array('C3', $lock_lv)) $consol_lock_lv_where[] = "(select lock_lv from biz_consol where biz_consol.id = bs.consol_id) = 3";
            if (in_array('S0', $lock_lv)) $lock_lv_where[] = "bs.lock_lv = 0";
            if (in_array('S1', $lock_lv)) $lock_lv_where[] = "bs.lock_lv = 1";
            if (in_array('S2', $lock_lv)) $lock_lv_where[] = "bs.lock_lv = 2";
            if (in_array('S3', $lock_lv)) $lock_lv_where[] = "bs.lock_lv = 3";
            if (!empty($consol_lock_lv_where)) $where[] = "(" . join(' OR ', $consol_lock_lv_where) . ")";
            if (!empty($lock_lv_where)) $where[] = "(" . join(' OR ', $lock_lv_where) . ")";
        }

        $users_where = array();
        $user_role = '';
        foreach ($users as $user) {
            //2022-08-24 已修复由于这个判断导致的如果第一个是-,导致全查的bug
            if (!isset($user['user_ids'])) continue;
            $user_ids = filter_unique_array($user['user_ids']);
            if (empty($user_ids)) continue;
            $user_role = $user['user_role'];
           
            if ($user_role == 'sales') {
                if (!empty($user_ids)) $users_where[] = "(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = bs.id and biz_duty_new.user_role = '{$user_role}') in ('" . join('\',\'', $user_ids) . "')";
            } else {
                if(!empty($user_ids)) $users_where[] = "(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = bs.id and biz_duty_new.user_role = '{$user_role}') in (" . join(',', $user_ids) . ") and (select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = bs.id and biz_duty_new.user_role = 'sales') not in ('" . join('\',\'', $user_ids) . "')";
            }
        }
        if (!empty($users_where)) $where[] = "( " . join(' OR ', $users_where) . " )";
        $groups = join("','", array_filter($groups));
        if ($group_role != "" && !empty($groups)) {
            // $where[] = "(bd.biz_table = 'biz_shipment' and bd." . $group_role . "_group in ('$groups'))";
            $where[] = "(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = bs.id and biz_duty_new.user_role = '{$group_role}') in ('{$groups}')";
        }

        $booking_ETD_start = "";
        $booking_ETD_end = "";
        if (!empty($fields)) {
            foreach ($fields['f'] as $key => $field) {
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if ($f == '') continue;
                if ($f == "shipment.booking_ETD" && $s == ">=") $booking_ETD_start = $v;
                if ($f == "shipment.booking_ETD" && $s == "<=") $booking_ETD_end = $v;
                if ($f == "consol.trans_ATD" && $s == ">=") $booking_ETD_start = $v;
                if ($f == "consol.trans_ATD" && $s == "<=") $booking_ETD_end = $v;
                if ($f == "bill.commision_month" && $s == ">=") $booking_ETD_start = $v;
                if ($f == "bill.commision_month" && $s == "<=") $booking_ETD_end = $v;

                $f_arr = explode('.', $f);
                if ($f_arr[0] == 'bill') {
                    if ($f_arr[1] == 'client_code_name') {
                        $f = '(select company_name from biz_client where client_code = biz_bill.client_code)';
                    } else {
                        $f = 'biz_bill.' . $f_arr[1];
                    }
                    if ($v != "") $where[] = search_like($f, $s, $v);
                } else if ($f_arr[0] == 'shipment') {
                    $biz_shipment_model = Model('biz_shipment_model');
                    if (!empty($v)) {
                        $this_where = array();
                        $this_where[] = search_like($f_arr[1], $s, $v);
                        $this_where = join(' or ', $this_where);
                        $this->db->select('id');
                        $shipments = $biz_shipment_model->no_role_get($this_where);
                        $shipments_ids = join(',', array_column($shipments, 'id'));
                        if (empty($shipments_ids)) $shipments_ids = 0;
                        $where[] = "biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)";
                    }
                } else if ($f_arr[0] == 'consol') {
                    $biz_shipment_model = Model('biz_shipment_model');
                    $biz_consol_model = Model('biz_consol_model');
                    $biz_client_model = Model('biz_client_model');

                    if (!empty($v)) {
                        if ($f_arr[1] == 'creditor') {
                            $this_where = array();
                            $this_where[] = search_like('company_search', $s, match_chinese($v));
                            $this_where = join(' or ', $this_where);
                            $clients = $biz_client_model->no_role_get($this_where);
                            $client_codes = join("','", array_column($clients, 'client_code'));
                            $this_where = "creditor in ('$client_codes')";
                        } else {
                            if ($f_arr[1] == 'trans_ATD') {
                                if ($s == '>=') $v = $v . ' 00:00:00';
                                if ($s == '<=') $v = $v . ' 23:59:59';
                            }

                            $this_where = array();
                            $this_where[] = search_like($f_arr[1], $s, $v);
                            $this_where = join(' or ', $this_where);
                        }

                        $this->db->select('id');
                        $consols = $biz_consol_model->no_role_get($this_where);
                        $consols_ids = join(',', array_column($consols, 'id'));
                        if (empty($consols_ids)) {
                            $consols_ids = 0;
                            $shipments_ids = 0;
                        } else {
                            $this->db->select('id');
                            $shipments = $biz_shipment_model->no_role_get("consol_id in ($consols_ids)");
                            $shipments_ids = join(',', array_column($shipments, 'id'));
                            if (empty($shipments_ids)) {
                                $shipments_ids = 0;
                            }
                        }
                        $where[] = "((biz_bill.id_type = 'shipment' and biz_bill.id_no in (select id from {$shipment_table} where consol_id in ($consols_ids))) or (biz_bill.id_type = 'consol' and biz_bill.id_no in ($shipments_ids)))";
                    }
                } else if ($f_arr[0] == 'shipment_consol') {
                    $biz_shipment_model = Model('biz_shipment_model');
                    $biz_consol_model = Model('biz_consol_model');

                    if (!empty($v)) {
                        $this_where = array();
                        $this_where[] = search_like($f_arr[1], $s, $v);
                        $this_where = join(' or ', $this_where);
                        $this->db->select('id');
                        $consols = $biz_consol_model->no_role_get($this_where);
                        $consols_ids = join(',', array_column($consols, 'id'));
                        if (empty($consols_ids)) {
                            $consols_ids = 0;
                        }
                        $this->db->select('id');
                        $shipments = $biz_shipment_model->no_role_get("consol_id in ($consols_ids)");
                        $shipments_ids = join(',', array_column($shipments, 'id'));
                        if (empty($shipments_ids)) {
                            $shipments_ids = 0;
                        }
                        $where[] = "((biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)) or (biz_bill.id_type = 'consol' and biz_bill.id_no in ($consols_ids)))";
                    }
                } else if ($f_arr[0] == 'shipment_by_consol') {
                    $biz_shipment_model = Model('biz_shipment_model');
                    $biz_consol_model = Model('biz_consol_model');

                    if (!empty($v)) {
                        $this_where = array();
                        $this_where[] = search_like($f_arr[1], $s, $v);
                        $this_where = join(' or ', $this_where);
                        $this->db->select('id,consol_id');
                        $shipments = $biz_shipment_model->no_role_get($this_where);
                        $shipments_ids = join(',', array_column($shipments, 'id'));
                        if (empty($shipments_ids)) {
                            $shipments_ids = 0;
                            $consols_ids = 0;
                        } else {
                            $consol_ids = join(',', array_filter(array_column($shipments, 'consol_id')));
                            $this->db->select('id');
                            if (empty($consol_ids)) $consol_ids = 0;
                            $consols = $biz_consol_model->no_role_get("id in ($consol_ids)");
                            $consols_ids = join(',', array_column($consols, 'id'));
                            if (empty($consols_ids)) $consols_ids = 0;
                        }
                        $where[] = "((biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)) or (biz_bill.id_type = 'consol' and biz_bill.id_no in ($consols_ids)))";
                    }
                } else if ($f_arr[0] == 'duty') {
                    $bsc_user_model = Model('bsc_user_model');
                    if (!empty($v)) {
                        $this->db->select('id');
                        $users = $bsc_user_model->get("name like '%$v%'");
                        if (empty($users)) $users[] = array('id' => -1);
                        $userIds = array_column($users, 'id');
                        // $where[] = "(bd.biz_table = 'biz_shipment' and bd." . $f_arr[1] . "_id in (" . join(',', $userIds) . "))";
                        $where[] = "((select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = bs.id and biz_duty_new.user_role = '{$f_arr[1]}') in (" . join(',', $userIds) . "))";
                    }
                }
            }
        }
        if (!empty($type_where)) $where[] = '( ' . join(' or ', $type_where) . ' ) ';
        if (!empty($ids)) $where[] = "biz_bill.id in ($ids)";
        $where[] = "biz_bill.id_type = 'shipment'";

        $where = join(' and ', $where);
        //-----------------------------------------------------------------
        $this->load->model('biz_bill_model');

        $this->db->select('biz_bill.id_type, biz_bill.id_no,biz_bill.currency,type,amount,local_amount,biz_bill.is_special' .
            ',bs.client_code' .
            ',bs.job_no' .
            ',bs.lock_lv' .
            ',bs.booking_ETD' .
            ',bc.carrier_ref' .
            ',bc.job_no as consol_no' .
            ',bc.trans_ATD' .
            ',bc.vessel' .
            ',bc.voyage' .
            ',bs.trans_origin_name' .
            ',bs.trans_destination_name' .
            ',bs.box_info' .
            ',bs.bujiti_sales' .
            ',bs.bujiti_customer_service' .
            ',bs.bujiti_operator' .
            ',biz_bill.payment_id' .
            //发票类型
            ',(select invoice_type from biz_bill_invoice where biz_bill_invoice.id = biz_bill.invoice_id) as invoice_type' .
            //销售
            // ',bd.sales_id' .
            // ',bd.sales_group' .
            ",(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = bs.id and biz_duty_new.user_role = 'sales') as sales_id" .
            ",(select biz_duty_new.user_group from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = bs.id and biz_duty_new.user_role = 'sales') as sales_group" .
            //操作
            // ',bd.operator_id' .
            ",(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = bs.id and biz_duty_new.user_role = 'operator') as operator_id" .
            //客服
            // ',bd.customer_service_id' .
            ",(select biz_duty_new.user_id from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and biz_duty_new.id_no = bs.id and biz_duty_new.user_role = 'customer_service') as customer_service_id" .
            '', false);
        $this->db->join("{$shipment_table} bs", "biz_bill.id_type = biz_bill.id_type and bs.id = biz_bill.id_no", 'left');
        $this->db->join("{$consol_table} bc", "bs.consol_id = bc.id", 'left');
        $rs = $this->biz_bill_model->get($where, 'biz_bill.id_no', 'desc', $is_special, '');

        $expTableData = array();
        $arrangeData = array();

        //合并数据到一个数组,再进行统计
        $userIds = array_merge(array_column($rs, 'sales_id'), array_column($rs, 'operator_id'), array_column($rs, 'customer_service_id'));
        $userIds = array_filter(array_unique($userIds));
        Model('bsc_user_model');
        $users = array();
        if (!empty($userIds)) {
            $this->db->select('id, name');
            $users = array_column($this->bsc_user_model->get("id in (" . join(',', $userIds) . ")"), 'name', 'id');
        }
        unset($userIds);

        $clientCodes = array_column($rs, 'client_code');
        $clientCodes = array_filter(array_unique($clientCodes));
        Model('biz_client_model');
        $clients = array();
        if (!empty($clientCodes)) {
            $this->db->select('client_code,company_name');
            $clients = array_column($this->biz_client_model->get("client_code in ('" . join('\',\'', $clientCodes) . "')"), 'company_name', 'client_code');
        }

        $userGroupcodes = filter_unique_array(array_column($rs, 'sales_group'));
        Model('bsc_group_model');
        $groups = array();
        if (!empty($userGroupcodes)) {
            $this->db->select('group_code,group_name');
            $groups = array_column($this->bsc_group_model->get("group_code in ('" . join('\',\'', $userGroupcodes) . "')"), 'group_name', 'group_code');
        }
        unset($userGroupcodes);

        foreach ($rs as $key => $row) {
            $row['sales'] = isset($users[$row['sales_id']]) ? $users[$row['sales_id']] : '';
            $row['sales_group_name'] = isset($groups[$row['sales_group']]) ? $groups[$row['sales_group']] : '';
            // unset($row['sales_id']);
            $row['operator'] = isset($users[$row['operator_id']]) ? $users[$row['operator_id']] : '';
            // unset($row['operator_id']);
            $row['customer_service'] = isset($users[$row['customer_service_id']]) ? $users[$row['customer_service_id']] : '';
            // unset($row['customer_service_id']);
            $row['client_code_name'] = isset($clients[$row['client_code']]) ? $clients[$row['client_code']] : '';
            $row['is_bujiti'] = 0;
            //$user_role这个值只要页面没有全部未选导入是不会报错的
            if ($user_role == 'sales') $row['is_bujiti'] = $row['bujiti_sales'];
            if ($user_role == 'customer_service') $row['is_bujiti'] = $row['bujiti_customer_service'];
            if ($user_role == 'operator') $row['is_bujiti'] = $row['bujiti_operator'];
            // unset($row['client_code']);
            // if($row['carrier_ref'] == ''){
            $arrangeData[$row['job_no']][] = $row;
            // }else{
            // $arrangeData[$row['carrier_ref']][] = $row;
            // }
        }
        unset($rs);
        unset($users);
        unset($clients);
        unset($groups);
        $num = 1;
        $total_gross_profit = 0;
        $total_gross_profit_bujiti = 0;
        
        $currency_table_fields = Model('bsc_dict_model')->get_option("currency");
        //默认的数组值
        $defalut_bill_rs_field = array(
            'sell_amount_sum' => 0,//总应收
            'cost_amount_sum' => 0,//总应付
            'gross_profit_sum' => 0,//总毛利
            'sell_not_payment_amount' => 0,//未到账数
            'sell_payment_amount' => 0,//到账数
            'cost_payment_amount' => 0,//付款数
            'sell_special_amount' => 0,//专项毛利(RMB)
            'cost_special_amount' => 0,//专项毛利(RMB)
            'special_gross_profit' => 0,//专项毛利(RMB)
        );
        //币种的都填充下
        foreach ($currency_table_fields as $currency_table_field){
            $defalut_bill_rs_field["sell_{$currency_table_field['value']}_amount_sum"] = 0;
            $defalut_bill_rs_field["cost_{$currency_table_field['value']}_amount_sum"] = 0;
            $defalut_bill_rs_field["{$currency_table_field['value']}_gross_profit"] = 0;
        }

        //查出发票类型对应的名称
        $invoice_type = array_column(Model('bsc_dict_model')->get_option('invoice_type'), 'name', 'value');
        foreach ($arrangeData as $key => $row) {
            $thisData = array();
            $thisData['id_type'] = $row[0]['id_type'];
            $thisData['id_no'] = $row[0]['id_no'];
            $thisData['job_no'] = $row[0]['job_no'];
            $thisData['is_bujiti'] = $row[0]['is_bujiti'];
            $thisData['trans_ATD'] = $row[0]['trans_ATD'];
            $thisData['carrier_ref'] = $row[0]['carrier_ref'];
            $thisData['consol_no'] = $row[0]['consol_no'];
            $thisData['trans_origin_name'] = $row[0]['trans_origin_name'];
            $thisData['trans_destination_name'] = $row[0]['trans_destination_name'];
            $thisData['client_code'] = $row[0]['client_code'];
            $thisData['client_code_name'] = $row[0]['client_code_name'];
            $thisData['vessel'] = $row[0]['vessel'];
            $thisData['voyage'] = $row[0]['voyage'];
            $thisData['sales'] = array();
            // $thisData['sales_group'] = array();
            $thisData['sales_group_name'] = array();
            $thisData['customer_service'] = array();
            $thisData['operator'] = array();
            $thisData['sales_id'] = array();
            $thisData['customer_service_id'] = array();
            $thisData['operator_id'] = array();
            $thisData['num'] = $num++;
            $thisData['invoice_type_name'] = array();
            $box_info_text = json_decode($row[0]['box_info'], true);
            
            //初始化一下金额字段
            $thisData = array_merge($thisData, $defalut_bill_rs_field);
            foreach ($row as $k => $val) {
                $thisData["{$val['type']}_{$val['currency']}_amount_sum"] += $val['amount'];
                if($val['type'] == 'sell'){
                    $thisData["sell_amount_sum"] += $val['local_amount'];//总应收
                    
                    if ($val['is_special'] == 1) $thisData["sell_special_amount"] += $val['local_amount'];//应收专项毛利(RMB)
                    
                    if ($val['payment_id'] > 0) $thisData['sell_payment_amount'] += $val['local_amount'];//已核销应付金额
                }else{
                    $thisData["cost_amount_sum"] += $val['local_amount'];//总应付
                    
                    if ($val['is_special'] == 1) $thisData["cost_special_amount"] += $val['local_amount'];//应付专项毛利(RMB)
                }
              
                if (!in_array($val['sales'], $thisData['sales'])) $thisData['sales'][] = $val['sales'];
                if (!in_array($val['sales_group_name'], $thisData['sales_group_name'])) $thisData['sales_group_name'][] = $val['sales_group_name'];
                if (!in_array($val['operator'], $thisData['operator'])) $thisData['operator'][] = $val['operator'];
                if (!in_array($val['customer_service'], $thisData['customer_service'])) $thisData['customer_service'][] = $val['customer_service'];
                if (!in_array($val['sales_id'], $thisData['sales_id'])) $thisData['sales_id'][] = $val['sales_id'];
                if (!in_array($val['operator_id'], $thisData['operator_id'])) $thisData['operator_id'][] = $val['operator_id'];
                if (!in_array($val['customer_service_id'], $thisData['customer_service_id'])) $thisData['customer_service_id'][] = $val['customer_service_id'];
                isset($invoice_type[$val['invoice_type']]) && $thisData['invoice_type_name'][] = $invoice_type[$val['invoice_type']];

            }
            //加一下币种总额
            foreach ($currency_table_fields as $currency_table_field){
                $thisData["{$currency_table_field['value']}_gross_profit"] = $thisData["sell_{$currency_table_field['value']}_amount_sum"] - $thisData["cost_{$currency_table_field['value']}_amount_sum"];
            }
            $thisData['gross_profit_sum'] = $thisData['sell_amount_sum'] - $thisData['cost_amount_sum'];//总毛利
            $thisData['special_gross_profit'] = $thisData['sell_special_amount'] - $thisData['cost_special_amount'];//总毛利
            
            $thisData['invoice_type_name'] = join(',', filter_unique_array($thisData['invoice_type_name']));

            $thisData['box_info_text'] = array();
            foreach ($box_info_text as $box) {
                $thisData['box_info_text'][] = $box['num'] . '*' . $box['size'];
            }
            $thisData['box_info_text'] = join(',', $thisData['box_info_text']);

            if ($thisData['is_bujiti'] == 1 && $thisData['gross_profit_sum'] >= 0) $total_gross_profit_bujiti += $thisData['gross_profit_sum'];       //标记为不计提的，不算毛利
            $total_gross_profit += $thisData['gross_profit_sum'];
            //专项
            $thisData['sell_special_amount'] = (int)($thisData['sell_special_amount']);
            $thisData['cost_special_amount'] = (int)($thisData['cost_special_amount']);
            $thisData['special_gross_profit'] = $thisData['sell_special_amount'] - $thisData['cost_special_amount'];

            $thisData['sell_amount_sum'] = (int)$thisData['sell_amount_sum'];//总金额
            $thisData['sell_payment_amount'] = (int)$thisData['sell_payment_amount'];//已核销应付金额

            if ($thisData['sell_amount_sum'] === 0) $thisData['return_rate'] = '100%';
            else $thisData['return_rate'] = round($thisData['sell_payment_amount'] / $thisData['sell_amount_sum'], 2) . '%';//回款率

            $thisData['sales'] = join(',', array_filter($thisData['sales']));
            $thisData['customer_service'] = join(',', array_filter($thisData['customer_service']));
            $thisData['operator'] = join(',', array_filter($thisData['operator']));
            $thisData['lock_lv'] = $row[0]['lock_lv'];
            $thisData['bujiti_sales'] = $row[0]['bujiti_sales'];
            $thisData['bujiti_customer_service'] = $row[0]['bujiti_customer_service'];
            $thisData['bujiti_operator'] = $row[0]['bujiti_operator'];

            $expTableData[] = $thisData;
        }

        //排序--start
        //2022-08-23 新增一个如果post没有传参,那么这里默认查询
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "num";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'asc';
        $config_sort = Model('sys_config_model')->get_config('report_lrb_sort');
        $config_order = Model('sys_config_model')->get_config('report_lrb_order');
        if(isset($_POST['sort'])){
            //存储一下config
            Model('sys_config_model')->save('report_lrb_sort', $sort, true);
        }else{
            $sort = $config_sort == '' ? $sort : $config_sort;
        }
        if(isset($_POST['order'])){
            //存储一下config
            Model('sys_config_model')->save('report_lrb_order', $order, true);
        }else{
            $order = $config_order == '' ? $order : $config_order;
        }
        $ext_data['order'] = $order;
        $ext_data['sort'] = $sort;

        $s = array();
        foreach ($expTableData as $key => $row) {
            $s[$key] = isset($row[$sort]) ? $row[$sort] : $row[$sort];
        }


        if ($order == 'desc') $order = SORT_DESC;
        else $order = SORT_ASC;
        array_multisort($s, $order, $expTableData);
        unset($s);

        $this->num = 1;
        $expTableData = array_map(function ($row) {
            $row['num'] = $this->num++;
            return $row;
        }, $expTableData);
        $result['code'] = 0;
        $result['rows'] = $expTableData;
        $result['total'] = sizeof($expTableData);
        $result['ext_data'] = $ext_data;
        echo json_encode($result);
    }

     public function query_lrb_user(){
        $param = $_POST;
        
        // $sql = "select user_id from biz_duty_new where id_type = 'biz_shipment' and user_role = '{$param['user_role']}' and id_no in (select id from biz_shipment where booking_ETD >= '{$param['booking_ETD_start']}' and booking_ETD <= '{$param['booking_ETD_end']}')";
        // $rs = $this->db->query($sql)->result_array();
        
        // $user_ids = array_column($rs, 'user_id');
        
        $where = array();
        $where[] = "FIND_IN_SET('{$param['user_role']}', bsc_user.user_role)";
        //销售全开
        if($param['user_role'] !== 'sales'){
            $host = explode('.', $_SERVER['HTTP_HOST']);
            $where[] = "bsc_user.country_code = '" . strtoupper($host[0]) . "'";
        }
        
        if(empty($where)) $where[] = "1 = 1";
        
        $where_str = join(' and ', $where);
        
        $sql = "select id,`name`,`group`,(select group_name from bsc_group where group_code = bsc_user.`group`) as `group_name` from bsc_user where {$where_str}";
        $rs = $this->db->query($sql)->result_array();
        $data = [];
        $data[] = array('id' => 0, 'user_id' => 0, 'name' => '-', 'group' => '' , 'otherName' => '-');
        if(!empty($rs)){
            foreach ($rs as $row){
                $row['otherName'] = $row['name'].' / '.$row['group_name'];
                $data[] = $row;
            }
        }
        return jsonEcho($data);
    }
}
