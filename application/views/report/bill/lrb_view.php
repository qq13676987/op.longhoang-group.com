<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<script type="text/javascript" src="/inc/js/easyui/datagrid-export.js"></script>
<script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
<link rel="stylesheet" href="/inc/third/layui/css/layui.css">
<style type="text/css">
    .hide{
        display: none;
    }
    .f{
        width:120px;
    }
    .s{
        width:100px;
    }
    .v{
        width:120px;
    }
    .del_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }
    .f2{
        width:135px;
    }
    .s2{
        width:100px;
    }
    .v2{
        width:120px;
    }
    .this_v2{
        display: inline;
    }
    .add{
        background: url(/inc/js/easyui/themes/icons/edit_add.png) no-repeat center center;
    }
    .remove{
        background: url(/inc/js/easyui/themes/icons/edit_remove.png) no-repeat center center;
    }
</style>
<!--这里是查询框相关的代码-->
<script>
    var query_option = {};//这个是存储已加载的数据使用的

    var table_name = 'biz_bill',//需要加载的表名称
        view_name = 'report_lrb';//视图名称
    var add_tr_str = "";//这个是用于存储 点击+号新增查询框的值, load_query_box会根据接口进行加载


    /**
     * 加载查询框
     */
    function load_query_box(){
        ajaxLoading();
        $.ajax({
            type:'GET',
            url: '/sys_config_title/get_user_query_box?view_name=' + view_name + '&table_name=' + table_name,
            dataType:'json',
            success:function (res) {
                ajaxLoadEnd();
                if(res.code == 0){
                    //加载查询框
                    $('#cx .cx_box').append(res.data.box_html);
                    //需要调用下加载器才行
                    $.parser.parse($('#cx table tbody .query_box'));

                    add_tr_str = res.data.add_tr_str;
                    query_option = res.data.query_option;

                    //渲染表头
                    $('#tt').datagrid({
                        width: 'auto',
                        height: $(window).height(),
                        clearSelections:true,
                        onSelect:function(index ,row){
                            get_statistics();
                        },
                        onUnselect:function(index ,row){
                            get_statistics();
                        },
                        onSelectAll:function(index ,row){
                            get_statistics();
                        },
                        onUnselectAll:function(index ,row){
                            get_statistics();
                        },
                        onLoadSuccess:function(data){
                            is_submit = false;
                            ajaxLoadEnd();
                            if(data.code != 0){
                                $.messager.alert('<?= lang('提示');?>', data.msg);
                            }
                            var ext_data = data.ext_data;
                            $(this).datagrid('changeColumnSort', {
                                sortOrder: ext_data.order,
                                sortName: ext_data.sort,
                            });
                        },
                        onLoadError:function(d){
                            is_submit = false;
                            ajaxLoadEnd();
                        },
                        onDblClickRow:function (index, row) {
                            if(row.id_type == 'shipment'){
                                window.open("/biz_shipment/edit/" + row.id_no);
                            }else if(row.id_type == 'consol'){
                                window.open("/biz_consol/edit/" + row.id_no);
                            }
                        },
                        rowStyler:function(index,row){
                            if(row['invoice_date'] != null){
                                // return 'background-color:#A8A8A8;';
                            }else if(row['actual_date'] != null){
                                // return 'background-color:#A8A8A8;';
                            }else if(row['lock_date'] != null){
                                return 'background-color:#A8A8A8;';
                            }else if(row['is_bujiti'] ==1){
                                return 'background-color:#A8A8A8;';
                            }

                        },
                        loadFilter: function(data){
                            $('#my_money').textbox('setValue',data.ext_data.tc_amount);
                            tc_des = data.ext_data.tc_text;
                            return data;
                        },
                    });

                    $(window).resize(function () {
                        $('#tt').datagrid('resize');
                    });

                    //为了避免初始加载时, 重复加载的问题,这里进行了初始加载ajax数据
                    var aj = [];
                    $.each(res.data.load_url, function (i,it) {
                        aj.push(get_ajax_data(it, i));
                    });
                    //加载完毕触发下面的下拉框渲染
                    //$_GET 在other.js里封装
                    var this_url_get = {};
                    $.each($_GET, function (i, it) {
                        this_url_get[i] = it;
                    });
                    var auto_click = this_url_get.auto_click;

                    $.when.apply($,aj).done(function () {
                        //这里进行字段对应输入框的变动
                        $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                            var ec_value = $(ec_item).combobox('getValue');
                            var v_inp = $('.v:eq(' + ec_index + ')');
                            var s_val = $('.s:eq(' + ec_index + ')').combobox('getValue');
                            var v_val = v_inp.textbox('getValue');
                            //如果自动查看,初始值为空
                            if(auto_click === '1') v_val = '';

                            $(ec_item).combobox('clear').combobox('select', ec_value);
                            //这里比对get数组里的值
                            $.each(this_url_get, function (trg_index, trg_item) {
                                //切割后,第一个是字段,第二个是符号
                                var trg_index_arr = trg_index.split('-');
                                //没有第二个参数时,默认用等于
                                if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';
                                //用一个删一个
                                //如果当前的选择框的值,等于get的字段值
                                if(ec_value === trg_index_arr[0] && s_val === trg_index_arr[1]){
                                    v_val = trg_item;//将v改为当前get的
                                    // $('.s:eq(' + ec_index + ')').combobox('setValue', trg_index_arr[1]);//设置S的值
                                    delete this_url_get[trg_index];
                                }
                            });
                            //没找到就正常处理
                            $('.v:eq(' + ec_index + ')').textbox('setValue', v_val);
                        });
                        //判断是否有自动查询参数
                        var no_query_get = ['auto_click','is_special'];//这里存不需要塞入查询的一些特殊变量
                        $.each(no_query_get, function (i,it) {
                            delete this_url_get[it];
                        });
                        //完全没找到的剩余值,在这里新增一个框进行选择
                        $.each(this_url_get, function (trg_index, trg_item) {
                            add_tr();//新增一个查询框
                            var trg_index_arr = trg_index.split('-');
                            //没有第二个参数时,默认用等于
                            if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';

                            $('#cx table tbody tr:last-child .f').combobox('setValue', trg_index_arr[0]);
                            $('#cx table tbody tr:last-child .s').combobox('setValue', trg_index_arr[1]);
                            $('#cx table tbody tr:last-child .v').textbox('setValue', trg_item);
                        });

                        //这里自动执行查询,显示明细
                        if(auto_click === '1'){
                            doSearch();
                        }
                        var serarchs = $('.searchs').length / 2;
                        var groups_field = $('.groups_field:checked').val();
                        if(serarchs <= 0 && groups_field == undefined){
                            $('#chaxun').window('open');
                        }else{
                            loaddata();
                        }
                        var str = '<li><a href="javascript:window.location.href= \'/report/index\';">report首页</a></li>';
                        str += '<li><a href="javascript:window.location.href= \'/report/volume_total\'">重新查询</a></li>';
                        for(var i = 0; i < serarchs; i++){
                            var field = $('.searchs:eq(' + (parseInt(i) * 2) +')');
                            var value = $('.searchs:eq(' + ((parseInt(i) * 2) + 1) +')');
                            var this_field_val = field.val();
                            var this_value_val = value.attr('tname');
                            var tkey = field.attr('tkey');
                            str += '<li><a href="javascript:void(0);" onclick="back(' + (serarchs - i) + ')">' + tkey + ':' + this_value_val + '</a></li>';
                        }
                        $("#now_crumb").parent('li').before(str);
                    });
                }else{
                    $.messager.alert("<?= lang('提示');?>", res.msg);
                }
            },error:function (e) {
                ajaxLoadEnd();
                $.messager.alert("<?= lang('提示');?>", e.responseText);
            }
        });
    }

    /**
     * 新增一个查询框
     */
    function add_tr() {
        $('#cx .cx_box tbody').append(add_tr_str);
        $.parser.parse($('#cx .cx_box tbody tr:last-child'));
        var last_f = $('#cx .cx_box tbody tr:last-child .f');
        var last_f_v = last_f.combobox('getValue');
        last_f.combobox('clear').combobox('select', last_f_v);
        return false;
    }

    /**
     * 删除一个查询
     */
    function del_tr(e) {
        //删除按钮
        var index = $(e).parents('tr').index();
        $(e).parents('tr').remove();
        return false;
    }

    //2022-05-24 MARS 提出 制单毛利加个颜色
    function gross_profit_styler(index,row) {
        var str = '';

        if(row.gross_profit >= 0){
            str += 'color: blue;';
        }else{
            str += 'color: red;';
        }

        return str;
    }

    //执行加载函数
    $(function () {
        $('#chaxun').window('open');

        $('#month_select').focus(function(){
            WdatePicker({
                startDate:'%y-%M',
                dateFmt:'yyyy-MM',
                onpicked:function(){
                    month_select()
                }
            });
        });

        load_query_box();
    });
</script>
<script type="text/javascript">
    var is_submit = false;
    var is_update = false;
    var tc_des = '';
    var showlook=1;
    function doSearch() {
        if(is_submit){
            return;
        }
        if(showlook==0) change_look();
        var booking_ETD_start = $('#booking_ETD_start').datebox('getValue');
        var booking_ETD_end = $('#booking_ETD_end').datebox('getValue');

        if(booking_ETD_start == '' || booking_ETD_end == ''){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择开始和结束日期');?>');
            return;
        }
        //如果有选单个用户，那么这个限制
        var user = $('#user').combobox('getValues');
        if(user.length == 2){
            //做简单版的，第一个有值就行了
            if(new Date(booking_ETD_end).getTime() - new Date(booking_ETD_start).getTime() > 366 * 24 * 3600 * 1000){
                $.messager.alert('<?= lang('提示');?>', '<?= lang('只查询一个人时最多查询1年');?>');
                return;
            }
        }else{
            //新增获取同一年同月份
            if(booking_ETD_start.substring(0,4)!=booking_ETD_end.substring(0,4)){
                $.messager.alert('<?= lang('提示');?>', '<?= lang('开始和结束日期请选择同年');?>');
                return;
            }
            // if(new Date(booking_ETD_end).getTime() - new Date(booking_ETD_start).getTime() > 188 * 24 * 3600 * 1000){
            //     $.messager.alert('Tips', '最多查询180天');
            //     return;
            // }
        }
        $('#hide_date').val($('#booking_ETD_start_f').combobox('getValue'))
        ajaxLoading();
        is_submit = true;
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        $('#tt').datagrid({
            url:'/report/get_lrb_data',
            queryParams: json
        }).datagrid('clearSelections');
        $("#commision").text('***')
        $('#search_booking_ETD_start_f').val($('#booking_ETD_start_f').combobox('getValue'));
        $('#search_booking_ETD_end_f').val($('#booking_ETD_end_f').combobox('getValue'));
        is_update = false;
        set_config();
        $('#chaxun').window('close');
        get_statistics();
        return false;
    }

    function reset(){
        $('.v').textbox('reset');
        return false;
    }

    function tc_lrb_excel(leader) {
        var json = {};
        var dg = $('#tt');
        //获取用户ID
        if(is_update){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('您修改过查询条件，请点击查询后再试');?>');
            return;
        }
        var user = $('#user');
        var user_role = $('#user_role');
        var sales_leader_tc = $('#sales_leader_tc');
        var tc_type = $('#tc_type');
        //将数据与头参数传过去
        var table_fields = dg.datagrid('getColumnFields',true).concat(dg.datagrid('getColumnFields',false));
        json.fields = [];
        for(var i=0; i<table_fields.length; i++){
            var col = dg.datagrid('getColumnOption', table_fields[i]);
            json.fields.push({width:col.boxWidth,field:table_fields[i],title:col.title});
        }
        json.datas = dg.datagrid('getData').rows;
        json.ext_data = dg.datagrid('getData').ext_data;
        json = JSON.stringify(json);
        var form_data = {};
        form_data.datas = json;
        form_data.tc_type = tc_type.combobox('getValue');

        //这里加入限制,booking_ETD_start_f 和 booking_ETD_end_f 必须
        var booking_ETD_start_f = $('#search_booking_ETD_start_f').val();
        var booking_ETD_end_f = $('#search_booking_ETD_end_f').val();
        //这里因为可能会存在,有人选择后,但是不重新查询的问题,所以这里现在更改为,查询后会有一个隐藏域存储当前点击查询后展示的日期字段
        if(booking_ETD_start_f != 'biz_bill.commision_month' || booking_ETD_end_f != 'biz_bill.commision_month'){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请使用计提月份查询后导出');?>');
            return;
        }

        // GET方式示例，其它方式修改相关参数即可
        if(leader==1){//
            var form = $('<form></form>').attr('action','/excel/export_excel?file=tc_lrb_new').attr('target', '_blank').attr('method','POST');
        }
        if(leader==2){
            var form = $('<form></form>').attr('action','/excel/export_excel?file=tc_leader_lrb_new').attr('target', '_blank').attr('method','POST');
        }

        $.each(form_data, function (index, item) {
            if(typeof item === 'string'){
                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
            }else{
                $.each(item, function(index2, item2){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                })
            }
        });
        form.appendTo('body').submit().remove();
    }

    function month_select(){
        var val = $('#month_select').val();
        var date = new Date(val);
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var d = new Date(year, month, 0);
        var end_day = d.getDate();
        $('#booking_ETD_start').datebox('setValue', year + '-' + month + '-01');
        $('#booking_ETD_end').datebox('setValue', year + '-' + month + '-' + end_day);
        $('#month_select').val('');
    }

    function is_special_select(rec){
        if(rec.value == ""){
            window.location.href = "/report/lrb?is_special=";
        }else{
            <?php if($is_special === ''){ ?>
            window.location.href = "/report/lrb";
            <?php } ?>
        }
    }

    function get_statistics(){
        var select = $('#tt').datagrid('getSelections');
        var row = {};
        var data = $('#tt').datagrid('getData').rows;
        if(data.length >= 1){
            $.each(data[0], function(index, item){
                //检测控件是否存在, 存在的话, 进行统计
                if($('#' + index).length > 0) {
                    row[index] = 0;
                }
            });
        }
        $.each(select, function (index, item) {
            $.each(item, function(i, val){
                if($('#' + i).length > 0) {
                    var this_val = parseInt(val * 10000);
                    row[i] += this_val;
                }
            });
        });
        $.each(row, function(index, item){
            if(typeof item == 'number'){
                row[index] = item / 10000;
            }
        });
        row['count_num'] = select.length;
        $('#statistics_form').form('load', row);
    }

    function lock_lv_select(rec){
        //此处为辅助勾选
        //首先清空勾选
        $('input[name="lock_lv[]"]').attr('checked', false);
        $('input[name="consol_lock_lv[]"]').attr('checked', false);

        var val = rec.value;
        if(val == 'NC3'){
            $('#C0').prop('checked', true);
        }else if(val == 'YC3'){
            $('#C3').prop('checked', true);
            $('#S0').prop('checked', true);
            $('#S1').prop('checked', true);
            $('#S2').prop('checked', true);
            $('#S3').prop('checked', true);
        }else if(val == 'NS1'){
            $('#C0').prop('checked', true);
            $('#C3').prop('checked', true);
            $('#S0').prop('checked', true);
        }else if(val == 'YS1'){
            $('#C3').prop('checked', true);
            $('#S1').prop('checked', true);
            $('#S2').prop('checked', true);
            $('#S3').prop('checked', true);
        }else if(val == 'NS2'){
            $('#C0').prop('checked', true);
            $('#C3').prop('checked', true);
            $('#S0').prop('checked', true);
            $('#S1').prop('checked', true);
        }else if(val == 'YS2'){
            $('#C3').prop('checked', true);
            $('#S2').prop('checked', true);
            $('#S3').prop('checked', true);
        }else if(val == 'NS3'){
            $('#C0').prop('checked', true);
            $('#C3').prop('checked', true);
            $('#S1').prop('checked', true);
            $('#S2').prop('checked', true);
        }else if(val == 'YS3'){
            $('#C3').prop('checked', true);
            $('#S3').prop('checked', true);
        }else{
            $.messager.alert('<?= lang('提示');?>', '<?= lang('暂不支持');?>');
        }

    }

    function hidden_column(event, hidden_column) {
        event=event?event:window.event;
        event.stopPropagation();
        var is_add = $(event.target).hasClass('add');

        //这里执行字段的隐藏和显示功能
        if(!is_add){
            $(event.target).addClass('add');
            $(event.target).removeClass('remove');
        }else{
            $(event.target).addClass('remove');
            $(event.target).removeClass('add');
        }

        //需要隐藏或显示的列名
        var tt = $('#tt');
        var opts = tt.datagrid('options');
        var pager = tt.datagrid('getPager');
        var panel = tt.datagrid('getPanel');

        $.each(hidden_column, function (i, it) {
            //hidden的字段整理在一起
            if(!is_add){
                tt.datagrid('hideColumn', it);
            }else{
                tt.datagrid('showColumn', it);
            }
        });
    }

</script>
<table id="tt" rownumbers="false" idField="id_no" toolbar="#tb" singleSelect="false" nowrap="false">
    <thead>
    <tr>
        <th title="全选" field="ck" checkbox="true"></th>
        <th data-options="field:'job_no',width:90,sortable:true"><?= lang('JOBNO');?></th>
        <th data-options="field:'num',width:60,sortable:true"><?= lang('序号');?></th>
        <th data-options="field:'trans_ATD',width:120,sortable:true"><?= lang('开航日');?></th>
        <th data-options="field:'carrier_ref',width:120,sortable:true"><?= lang('提单号(O)');?></th>
        <th data-options="field:'consol_no',width:80,sortable:true"><?= lang('CONSOL');?></th>
        <th data-options="field:'box_info_text',width:200"><?= lang('箱型箱量');?></th>
        <th data-options="field:'trans_origin_name',width:200,sortable:true"><?= lang('起运港');?></th>
        <th data-options="field:'trans_destination_name',width:200,sortable:true"><?= lang('目的港');?></th>
        <th data-options="field:'vessel',width:100,sortable:true"><?= lang('船名');?></th>
        <th data-options="field:'voyage',width:80,sortable:true"><?= lang('航次');?></th>
        <th data-options="field:'client_code_name',width:150,sortable:true"><?= lang('委托方');?></th>
        <th data-options="field:'gross_profit',width:120,sortable:true,styler:gross_profit_styler"><?= lang('制单毛利');?></th>
        <?php foreach($currency_table_fields as $currency_table_field){ ?>
            <th data-options="field:'<?= $currency_table_field;?>_profit',width:80,sortable:true"><?= lang("{currency}毛利", array('currency' => $currency_table_field));?><a href="javascript:void(0);" class="more_column l-btn-icon add" onClick="hidden_column(event, ['<?= $currency_table_field;?>_sell_amount', '<?= $currency_table_field;?>_cost_amount'])" style="position: unset;padding-left: 5px;margin-top: 0;"></a></th>
            <th data-options="field:'<?= $currency_table_field;?>_sell_amount',width:80,sortable:true,hidden:true"><?= lang("{currency}应收", array('currency' => $currency_table_field));?></th>
            <th data-options="field:'<?= $currency_table_field;?>_cost_amount',width:80,sortable:true,hidden:true"><?= lang("{currency}应付", array('currency' => $currency_table_field));?></th>
        <?php } ?>
        <?php if($is_special === ''){ ?>
            <!--专项应收 ，专项应付 ，专项利润。-->
            <th data-options="field:'special_sell_sell_amount',width:120,sortable:true"><?= lang('专项应收');?></th>
            <th data-options="field:'special_cost_sell_amount',width:120,sortable:true"><?= lang('专项应付');?></th>
            <th data-options="field:'special_gross_profit',width:120,sortable:true"><?= lang('专项利润');?></th>
        <?php }?>
        <th data-options="field:'payment_sell_local_amount',width:120,sortable:true"><?= lang('已销账应收');?></th>
        <th data-options="field:'sum_sell_local_amount',width:120,sortable:true"><?= lang('应收总金额');?></th>
        <th data-options="field:'return_rate',width:120,sortable:true"><?= lang('回款率');?></th>
        <th data-options="field:'invoice_type_name',width:80"><?= lang('开票类型');?></th>
        <th data-options="field:'sales',width:80,sortable:true"><?= lang('销售');?></th>
        <th data-options="field:'sales_group_name',width:120,sortable:true"><?= lang('销售组');?></th>
        <!--<th data-options="field:'sales_assistant',width:80,sortable:true"><?= lang('销售助理');?></th>-->
        <th data-options="field:'customer_service',width:80,sortable:true"><?= lang('客服');?></th>
        <th data-options="field:'operator',width:80,sortable:true"><?= lang('操作');?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:window.location.href= '/report/index';"><?= lang('返回上一页');?>  </a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#excel_menu',plain:true" iconCls="icon-reload"><?= lang('export excel');?></a>
                <!--<a href="javascript:;" id="commision-email" class="easyui-linkbutton"  plain="true" onclick="commision_email()"><?= lang('邮件申请提成');?>  </a>-->
            </td>
        </tr>
    </table>
    <div id="excel_menu" style="width:150px;">
        <div data-options="iconCls:'icon-ok'" onclick="$('#tt').datagrid('toExcel','<?= lang('利润表');?>.xls');"><?= lang('导出表格');?></div>
        <?php if(menu_role('report_lrb_commission_personal')){ ?>
            <div data-options="iconCls:'icon-ok'" onclick="tc_lrb_excel(1);"><?= lang('导出提成表格(已隐藏，仅管理员可见)');?></div>
        <?php } ?>
        <?php if(menu_role('report_lrb_commission_team')){ ?>
            <div data-options="iconCls:'icon-ok'" onclick="tc_lrb_excel(2);"><?= lang('导出团队提成(已隐藏，仅管理员可见)');?></div>
        <?php } ?>
    </div>
    <form id="statistics_form" method="post">
        <table>

            <!--            这里要做成已开票金额   未开票金额  已收款金额  未收款金额-->
            <!--            人民币和美金分开-->
            <?php for($i = 0; $i < ceil(sizeof($currency_table_fields) / 2); $i++){ ?>
                <tr>
                    <td><?= $currency_table_fields[$i * 2];?>:</td>
                    <td><?= lang('应收');?><input class="easyui-numberbox" precision="2" id="<?= $currency_table_fields[$i * 2];?>_sell_amount" name="<?= $currency_table_fields[$i * 2];?>_sell_amount" disabled value="0"></td>
                    <td><?= lang('应付');?><input class="easyui-numberbox" precision="2" id="<?= $currency_table_fields[$i * 2];?>_cost_amount" name="<?= $currency_table_fields[$i * 2];?>_cost_amount" disabled value="0"></td>
                    <td><?= lang('利润');?><input class="easyui-numberbox" precision="2" id="<?= $currency_table_fields[$i * 2];?>_profit" name="<?= $currency_table_fields[$i * 2];?>_profit" disabled value="0"></td>
                    <?php if(isset($currency_table_fields[($i * 2) + 1])){ ?>
                        <td><?= $currency_table_fields[($i * 2) + 1];?>:</td>
                        <td><?= lang('应收');?><input class="easyui-numberbox" precision="2" id="<?= $currency_table_fields[($i * 2) + 1];?>_sell_amount" name="<?= $currency_table_fields[($i * 2) + 1];?>_sell_amount" disabled value="0"></td>
                        <td><?= lang('应付');?><input class="easyui-numberbox" precision="2" id="<?= $currency_table_fields[($i * 2) + 1];?>_cost_amount" name="<?= $currency_table_fields[($i * 2) + 1];?>_cost_amount" disabled value="0"></td>
                        <td><?= lang('利润');?><input class="easyui-numberbox" precision="2" id="<?= $currency_table_fields[($i * 2) + 1];?>_profit" name="<?= $currency_table_fields[($i * 2) + 1];?>_profit" disabled value="0"></td>
                    <?php } ?>
                </tr>
            <?php } ?>
            <tr>
                <td></td>
                <td><?= lang('票数');?><input class="easyui-numberbox" precision="0" name="count_num" disabled value="0"></td>
                <td><?= lang('毛利');?><input class="easyui-numberbox" precision="2" id="gross_profit" name="gross_profit" disabled value="0"></td>
                <td><?= lang('提成');?><input class="easyui-textbox"  name="my_money" id="my_money"  value="0" disabled ></td>
                <script>
                    function change_look(){
                        if(showlook==1){
                            $('#tc_description').textbox('setValue',tc_des);
                            showlook =0;
                            $("#kaiguan").html("<?= lang('隐藏');?>");
                        }else{
                            $('#tc_description').textbox('setValue',"***");
                            showlook =1;
                            $("#kaiguan").html("<?= lang('显示');?>");
                        }
                    }

                </script>
                <td colspan="5">
                    <a href="javascript:change_look();"><span id="kaiguan"><?= lang('显示');?></span></a>
                    <input class="easyui-textbox"  name="tc_description" id="tc_description" disabled style="width:720px;" value="***" onclick="change_look()"></td>
                <!--<td><span onclick="show_commision()" style="color: blue;cursor: pointer;width: 26px;float: left"><?= lang('浮动');?></span></td>-->
            </tr>

        </table>
    </form>
</div>
<script>
    function commision_email(){
        let rows = $('#tt').datagrid('getRows');
        let msg = '';
        rows.map((item)=>{
            if(item.return_rate != '100%'){
                msg = "<?= lang('回款未达100%， 不允许申请');?>"
            }
        })
        if(msg != ''){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('回款未达100%， 不允许申请');?>');
            return
        }
        if($('#hide_date').val() != 'bill.commision_month'){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('只有计提月份才能申请');?>');
            return
        }
        let start_time = $('#booking_ETD_start').datebox('getValue');
        let user = $('#user').combobox('getValues');
        let commision_total = $('#commision_total').val() || 0
        let my_money = $('#my_money').textbox('getValue') || 0
        if(user[0] == 0){
            return
        }
        let session_user = "<?=get_session('id')?>"
        if(session_user != user[0]){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('必须是本人申请');?>');
            return
        }
        $("#commision-email-iframe").attr("src",'/mail_box/send_by_template?template_id=26&id='+user[0]+'&mail_type=sys_inner&start_time='+start_time+'&commision_total='+commision_total+'&my_money='+my_money)
        $('#commision-email-window').window('open');

    }
    function show_commision(){
        var start_time = $('#booking_ETD_start').datebox('getValue');
        var end_time = $('#booking_ETD_end').datebox('getValue');
        var user = $('#user').combobox('getValues');
        var user_role = $('#user_role').combobox('getValues');
        if(user[0] == 0){
            $.messager.alert('<?= lang('提示');?>','<?= lang('请正确选择用户');?>')
            return
        }
        let json = {
            start_time,
            end_time,
            user_id: user[0],
            user_role: user_role[0]
        }
        $.ajax({
            type: 'POST',
            url: '/report/count_commision',
            data: json,
            dataType: 'json',
            success: function (res) {
                if(res.code == 1){
                    // $('#commision').html(res.data);
                    if(res.data == '暂无数据'){
                        $.messager.alert('<?= lang('提示');?>','<?= lang('无提成数据');?>')
                        return;
                    }
                    $('#commision_total').val(res.total)
                    let str = '';
                    for (let i = 1; i <= res.data.res1.length; i++) {
                        str += `
                            <tr>
                                <td>新客户指标${i}:</td>
                                <td>${res.data.res1[i-1].calc_str}</td>
                                <td>客户列表:</td>
                                <td>${res.data.res1[i-1].show_list}</td>
                            </tr>
                        `
                    }
                    for (let i = 1; i <= res.data.res2.length; i++) {
                        str += `
                            <tr>
                                <td>teu数指标${i}:</td>
                                <td colspan="3">总计${res.data.res2[i-1].count} 计提:${res.data.res2[i-1].calc_str}</td>
                            </tr>
                        `
                    }
                    for (let i = 1; i <= res.data.res5.length; i++) {
                        str += `
                            <tr>
                                <td>箱数指标${i}:</td>
                                <td colspan="3">总计${res.data.res5[i-1].count} 计提:${res.data.res5[i-1].calc_str}</td>
                            </tr>
                        `
                    }
                    for (let i = 1; i <= res.data.res3.length; i++) {
                        str += `
                            <tr>
                                <td>票数奖励${i}:</td>
                                <td>总票数${res.data.res3[i-1].count} 总金额:${res.data.res3[i-1].amount}</td>
                                <td>票数详情:</td>
                                <td>${res.data.res3[i-1].show_list}</td>
                            </tr>
                        `
                    }
                    $('#show_list').html(str)
                    $('#show_new').window('open');
                }else{
                    $.messager.alert("<?= lang('提示');?>", res.data);
                }
            },
            error:function (e) {
                ajaxLoadEnd();
                $.messager.alert("<?= lang('提示');?>", e.responseText);
            }
        })
    }
</script>
<div id="commision-email-window" class="easyui-window" title="<?= lang('邮件申请提成');?>" closed="true" style="width:1200px;height:537px;padding:5px;">
    <iframe  id="commision-email-iframe" style="width: 100%;height: 100%" src="" frameborder="0"></iframe>
</div>
<div id="chaxun" class="easyui-window" title="<?= lang('query');?>" closed="true" style="width:850px;height:480px;padding:5px;">
    <br>
    <form id="cx" onSubmit="doSearch();">
        <input type="hidden" name="ids">
        <table class="cx_box" cellspacing="5" cellpadding="5" style="min-height: 300px">
            <tr>
                <td>
                    <?= lang('查询');?>
                </td>
                <td align="left">
                    <select name="field[f][]" editable="false" class="f2 easyui-combobox" id="booking_ETD_start_f" required="true" data-options="
                        onSelect:function(rec){
                            $('#booking_ETD_end_f').combobox('setValue', rec.value);
                        }
                    ">
                        <option value="consol.trans_ATD"><?= lang('实际离泊');?></option>
                        <option value="shipment.booking_ETD"><?= lang('订舱船期');?></option>
                        <option value="consol.report_date"><?= lang('report_date');?></option>
                        <option value="biz_bill.commision_month"><?= lang('计提月份');?></option>
                    </select>
                    &nbsp;

                    <select name="field[s][]" readonly class="s2 easyui-combobox"  required="true" data-options="
                    ">
                        <option value=">=">>=</option>
                    </select>
                    &nbsp;
                    <div class="this_v2">
                        <input name="field[v][]" required id="booking_ETD_start" class="v2 easyui-datebox">
                    </div>
                    <input type="text" class="Wdate" id="month_select" style="width:20px;" autocomplete="off"/>
                </td>
            </tr>
            <tr>
                <td>
                    <?= lang('查询');?>
                </td>
                <td align="left">
                    <select name="field[f][]" editable="false" class="f2 easyui-combobox" id="booking_ETD_end_f" required="true" data-options="
                        onSelect:function(rec){
                            $('#booking_ETD_start_f').combobox('setValue', rec.value);
                        }
                    ">
                        <option value="consol.trans_ATD"><?= lang('实际离泊');?></option>
                        <option value="shipment.booking_ETD"><?= lang('订舱船期');?></option>
                        <option value="consol.report_date"><?= lang('report_date');?></option>
                        <option value="biz_bill.commision_month"><?= lang('计提月份');?></option>
                    </select>
                    &nbsp;

                    <select name="field[s][]" readonly class="s2 easyui-combobox"  required="true" data-options="
                    ">
                        <option value="<="><=</option>
                    </select>
                    &nbsp;
                    <div class="this_v2">
                        <input name="field[v][]" required id="booking_ETD_end" class="v2 easyui-datebox">
                    </div>
                </td>
            </tr>
            <tr>
                <td><?=lang('角色')?></td>
                <td>
                    <select class="easyui-combobox" id="user_role" name="user_role" style="width:380px;" data-options="
                            valueField:'user_role',
                            textField:'user_role',
                            url:'/bsc_user_role/get_role/',
                            onSelect:function(res){
                                var booking_ETD_start = $('#booking_ETD_start').datebox('getValue')
                                if(!booking_ETD_start){
                                    alert('<?= lang('请先选择开始时间');?>')
                                    $(this).combobox('clear')
                                    return;
                                }
                                var booking_ETD_end = $('#booking_ETD_end').datebox('getValue')
                                if(!booking_ETD_end){
                                    alert('<?= lang('请先选择结束时间');?>')
                                    $(this).combobox('clear')
                                    return;
                                }
                                var user_role = res.user_role
                                $.post('/report/query_lrb_user', { booking_ETD_start,booking_ETD_end,user_role },
                                    function(res){
                                        $('#user').combobox('loadData',res)
                                    }, 'json');
                            }
                        ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('提成显示');?></td>
                <td align="left">
                    <select class="easyui-combobox" name="tc_type" id="tc_type" style="width:289px;" data-options="
                        onSelect:function(rec){
                            tc_type_select(rec);
                        },
                    ">
                        <option value=""><?= lang('不显示');?></option>
                        <?php if(menu_role('report_lrb_commission_personal')){ ?>
                        <option value='sales'><?= lang('个人提成');?></option>
                        <?php } ?>
                        <?php if(menu_role('report_lrb_commission_team')){ ?>
                        <option value='team'><?= lang('团队提成');?></option>
                        <?php } ?>
                        <!--<option value='sub_company'>分公司提成</option> -->
                    </select>
                    <?= show_tip(lang('个人提成：在 user 这里选择角色和具体的人；<br>团队提成：在团队领导这里选择具体的人;')); ?>
                    &emsp;
                    <span id="sales_leader_tc_tr" style="display: none">
                        <?= lang('团队领导');?>
                        <select class="easyui-combobox" name="sales_leader_tc" id="sales_leader_tc" disabled style="width:289px;" data-options="
                        onSelect:function(rec){
                            sales_leader_tc_select(rec);
                        },
                    ">
                        <option value="">-</option>
                        <?php
                        foreach($team_leader as $tm){
                            echo "<option value='{$tm['user_id']}'>{$tm['user_name']}</option>";
                        }
                        ?>
                    </select>
                    </span>
                </td>
            </tr>
            <tr>
                <!--按销售姓名等-->
                <td><?= lang('姓名');?></td>
                <td align="left">
                    <select class="easyui-combobox" id="user" name="user_ids" style="width:665px;" data-options="
                        valueField:'id',
                        textField:'otherName',
                        multiple:true,
                        onShowPanel:function(){
                            var booking_ETD_start = $('#booking_ETD_start').datebox('getValue')
                            if(!booking_ETD_start){
                                alert('<?= lang('请先选择开始时间');?>')
                                return;
                            }
                            var booking_ETD_end = $('#booking_ETD_end').datebox('getValue')
                            if(!booking_ETD_end){
                                alert('<?= lang('请先选择结束时间');?>')
                                return;
                            }
                            var user_role = $('#user_role').combobox('getValue')
                            if(!user_role){
                                alert('<?= lang('请先选择角色');?>')
                                return;
                            }
                        }
                    ">
                    </select>
                </td>
            </tr>
            <tr style="border-bottom: 1px solid #ccc">
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <script>
                function tc_type_select(rec){
                    if(rec.value=="" || rec.value=="sales") {
                        $('#sales_leader_tc').combobox("disable");
                        $('#sales_leader_tc').combobox("setValue","");
                        if(rec.value=="sales"){
                            $('#user').combobox("options").multiple = false ;
                            $('#user_role').combobox("setValue",'sales');
                            var booking_ETD_start = $('#booking_ETD_start').datebox('getValue')
                            if(!booking_ETD_start){
                                alert('<?= lang('请先选择开始时间');?>')
                                return;
                            }
                            var booking_ETD_end = $('#booking_ETD_end').datebox('getValue')
                            if(!booking_ETD_end){
                                alert('<?= lang('请先选择结束时间');?>')
                                return;
                            }
                            var user_role = $('#user_role').combobox('getValue');
                            if(!user_role){
                                alert('<?= lang('请先选择角色');?>')
                                return;
                            }
                            $.post('/report/query_lrb_user', { booking_ETD_start,booking_ETD_end,user_role },
                                function(res){
                                    $('#user').combobox('loadData',res)
                                }, 'json');
                            $('#sales_leader_tc_tr').hide();
                        }
                    } else{
                        $('#user').combobox("options").multiple = true ;
                        $('#sales_leader_tc').combobox("enable");
                        $('#sales_leader_tc_tr').show();
                    }
                }
            </script>

            <script>
                function sales_leader_tc_select(rec){
                    if(rec.value=="") {
                        $('#user_role').combobox("setValue",'sales');
                        $('#user').combobox("setValue",'');
                        return;
                    }
                    $.getJSON("/bsc_user/get_xiashu/"+rec.value, function(json){
                        var xiashu = '';
                        $.each(json, function(i, field){
                            if(i>0) xiashu+=",";
                            xiashu += field['id'] ;
                        });
                        if(rec.value=='20119' || rec.value=='20117'){  //写死的， 张妍 王俊作为客服领导， 以后再优化
                            $('#user_role').combobox("setValue",'customer_service');
                        }else{
                            $('#user_role').combobox("setValue",'sales');
                        }
                        var booking_ETD_start = $('#booking_ETD_start').datebox('getValue')
                        if(!booking_ETD_start){
                            alert('<?= lang('请先选择开始时间');?>')
                            return;
                        }
                        var booking_ETD_end = $('#booking_ETD_end').datebox('getValue')
                        if(!booking_ETD_end){
                            alert('<?= lang('请先选择结束时间');?>')
                            return;
                        }
                        var user_role = $('#user_role').combobox('getValue');
                        if(!user_role){
                            alert('<?= lang('请先选择角色');?>')
                            return;
                        }
                        $.post('/report/query_lrb_user', { booking_ETD_start,booking_ETD_end,user_role },
                            function(res){
                                $('#user').combobox('loadData',res)
                            }, 'json');
                        $('#user').combobox('setValues', xiashu);
                    });
                }
            </script>
            <?php if(menu_role('billing_special_bill')){ ?>
                <tr>
                    <td><?= lang('是否专项');?></td>
                    <td align="left">
                        <select class="easyui-combobox" name="is_special" id="is_special" style="width:380px;" data-options="
                            onSelect:function(rec){
                                is_special_select(rec);
                            },
                            value: '<?= $is_special;?>',
                        ">
                            <option value="0"><?= lang('非专项');?></option>
                            <option value="1"><?= lang('专项');?></option>
                            <option value=""><?= lang('包含专项');?></option>
                        </select>
                    </td>
                </tr>
            <?php } ?>
            <tr style="display:none;">
                <td>
                    <?= lang('group');?>
                </td>
                <td align="left">
                    <select class="easyui-combobox" name="group_role" style="width:235px;" data-options="
                            valueField:'user_role',
                            textField:'user_role',
                            url:'/bsc_user_role/get_role/',
                            onLoadSuccess:function(){
                                var data = $(this).combobox('getData');
                                $(this).combobox('select', data[0].user_role);
                            }
                        ">
                    </select>
                    &nbsp;
                    <select class="easyui-combobox" id="group" name="group[]" style="width:110px;" data-options="
                        multiple:true,
                        valueField:'code',
                        textField:'name',
                        url:'/bsc_group/get_option/',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('计提标记');?></td>
                <td align="left">
                    <input type="hidden" name="field[f][]" value="bill.commision_flag">
                    <input type="hidden" name="field[s][]" value="=">
                    <div style="display: flex;justify-content: space-between;width: 200px">
                        <span><input type="radio" name="field[v][]" value="" checked><?= lang('全部');?></span>
                        <span><input type="radio" name="field[v][]" value="0"><?= lang('否');?></span>
                        <span><input type="radio" name="field[v][]" value="1"><?= lang('是');?></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td><?= lang('shipment锁');?></td>
                <td align="left">
                    <div style="display: flex">
                        <div style="display: flex;width: 380px;justify-content: space-between;align-items: center">
                            <span><input name="lock_lv[]" id="S0" type="checkbox" value="S0"><?= lang('s未锁');?></span>
                            <span><input name="lock_lv[]" id="S1" type="checkbox" value="S1"><span title="操作锁"> <?= lang('s1级');?></span></span>
                            <span><input name="lock_lv[]" id="S2" type="checkbox" value="S2"><span title="销售锁"> <?= lang('s2级');?> </span></span>
                            <span><input name="lock_lv[]" id="S3" type="checkbox" value="S3"><span title="财务锁"> <?= lang('s3级');?> </span></span>
                            <span><input name="lock_lv[]" id="C0" type="checkbox" value="C0"><?= lang('c未锁');?></span>
                            <span><input name="lock_lv[]" id="C3" type="checkbox" value="C3"><span title="carrier_cost锁"> <?= lang('c3级');?></span></span>
                        </div>
                        <span>
                            <select class="easyui-combobox" editable="false" style="width:280px" data-options="
                                onSelect:function(rec){
                                    lock_lv_select(rec);
                                },
                            ">
                                <option value=""><?= lang('--请选择--');?></option>
                                <option value="NC3"><?= lang('C3未锁');?></option>
                                <option value="YC3"><?= lang('C3已锁');?></option>
                                <option value="NS1"><?= lang('S1未锁');?></option>
                                <option value="YS1"><?= lang('S1已锁');?></option>
                                <option value="NS2"><?= lang('S2未锁');?></option>
                                <option value="YS2"><?= lang('S2已锁');?></option>
                                <option value="NS3"><?= lang('S3未锁');?></option>
                                <option value="YS3"><?= lang('S3已锁');?></option>
                            </select>

                            <a href="javascript:void;" class="easyui-tooltip" data-options="
                                content: $('<div></div>'),
                                onShow: function(){
                                    $(this).tooltip('arrow').css('left', 20);
                                    $(this).tooltip('tip').css('left', $(this).offset().left);
                                },
                                onUpdate: function(cc){
                                    cc.panel({
                                        width: 300,
                                        height: 'auto',
                                        border: false,
                                        href: '/bsc_help_content/help/lock_query_note'
                                    });
                                }
                            "><?php echo lang('帮助');?></a>
                        </span>
                    </div>
                </td>
            </tr>
        </table>
        <br><br>
        <button class="easyui-linkbutton" id="submit" onclick="doSearch();" style="width:200px;height:30px;" type="button"><?= lang('查询');?></button>
        <button class="easyui-linkbutton" id="reset" onclick="reset();" style="width:200px;height:30px;" type="button"><?= lang('操作');?></button>
        <a href="javascript:void(0);" class="easyui-tooltip" data-options="
            content: $('<div></div>'),
            onShow: function(){
                $(this).tooltip('arrow').css('left', 20);
                $(this).tooltip('tip').css('left', $(this).offset().left);
            },
            onUpdate: function(cc){
                cc.panel({
                    width: 500,
                    height: 'auto',
                    border: false,
                    href: '/bsc_help_content/help/query_condition'
                });
            }
        "><?= lang('help');?></a>
    </form>
    <input type="hidden" id="search_booking_ETD_start_f">
    <input type="hidden" id="search_booking_ETD_end_f">
</div>
<input type="hidden" id="hide_date" value="">
<div id="show_new" class="easyui-window" title="<?= lang('浮动业绩指标');?>" closed="true" style="width:700px;height:400px;padding:5px;">
    <table  class="layui-table">
        <tbody id="show_list">

        </tbody>
    </table>
</div>