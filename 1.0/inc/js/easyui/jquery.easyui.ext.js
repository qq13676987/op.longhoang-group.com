/**
 * 该文件用于单独重写esayui 的一些方法使用
 * 汪庭彬
 */
$.fn.datebox.defaults.formatter = function(date) {
    //为加密格式时直接返回
    if(date === '***') return date;
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    return y + '-' + (m < 10 ? '0' + m : m) + '-' + (d < 10 ? '0' + d : d);
};
$.fn.datetimebox.defaults.formatter = function(date){
    //为加密格式时直接返回
    if(date === '***') return date;
    var h=date.getHours();
    var M=date.getMinutes();
    var s=date.getSeconds();
    function _ae0(_ae1){
        return (_ae1<10?"0":"")+_ae1;
    };
    var _ae2=$(this).datetimebox("spinner").timespinner("options").separator;
    var r=$.fn.datebox.defaults.formatter(date)+" "+_ae0(h)+_ae2+_ae0(M);
    if($(this).datetimebox("options").showSeconds){
        r+=_ae2+_ae0(s);
    }
    return r;
};
//2022-05-25 由于金晶想改为数字的,所以这里加入纯数字兼容格式
$.fn.datebox.defaults.parser = function(s) {
    if(!s) return new Date();

    //如果是纯数字,走另一套规则
    var csz = /^[0-9]*$/;
    var ss = [];
    if (csz.test(s)) {
        ss.push(s.substr(0,4));
        ss.push(s.substr(4,2));
        ss.push(s.substr(6,2));
    }else{
        //为加密格式时直接返回
        if(s === '***') return  s;
        ss = s.split('-');
    }

    var y=parseInt(ss[0],10);
    var m=parseInt(ss[1],10);
    var d=parseInt(ss[2],10);
    if(!isNaN(y)&&!isNaN(m)&&!isNaN(d)) return new Date(y, m - 1, d);
    else return new Date();
};
//给datebox的 textbox加一个 绑定的脱离焦点自动选择事件
let old_blur = $.fn.datebox.defaults.events.blur;
$.fn.datebox.defaults.events.blur = function(e){
    old_blur(e);
    var opt_data;
    if($(e.target).parent().prev()[0] == undefined) opt_data = {};
    else opt_data = $.data($(e.target).parent().prev()[0]);
    if(opt_data.datebox !== undefined){
        //如果数值少于8位数,不触发
        var auto_enter = false;
        
        if($($(e.target).parent().prev()[0]).datebox('getValue').length == 8) auto_enter = true;
        
        if(auto_enter) _aa9($(e.target).parent().prev()[0]);
        
        function _aa9(_aaa){
            var _aab=$.data(_aaa,"datebox");
            var opts=_aab.options;
            var _aac=_aab.calendar.calendar("options").current;
            if(_aac){
                _aa8(_aaa,opts.formatter.call(_aaa,_aac));
                $(_aaa).combo("hidePanel");
            }
        };
        function _aa8(_aad,_aae,_aaf){
            var _ab0=$.data(_aad,"datebox");
            var opts=_ab0.options;
            var _ab1=_ab0.calendar;
            _ab1.calendar("moveTo",opts.parser.call(_aad,_aae));
            if(_aaf){
                $(_aad).combo("setValue",_aae);
            }else{
                if(_aae){
                    _aae=opts.formatter.call(_aad,_ab1.calendar("options").current);
                }
                $(_aad).combo("setText",_aae).combo("setValue",_aae);
            }
        }
    }
};