<style type="text/css">
    .fr_table{
        border-collapse: collapse;
    }
    .fr_table td{
        border: 1px solid black;
        padding: 5px;
        text-align: left;
    }
</style>
<span style="color: red;"><?= lang('报文内容如下');?>:</span>
<br />
<br />
<table class="fr_table">
    <tr>
        <td><?= lang('CONSOL');?></td>
        <td>
            <?= $file_content_html['job_no'];?>
        </td>
        <td><?= lang('付款方式');?></td>
        <td>
            <?= $file_content_html['payment'];?>
        </td>
    </tr>
    <tr>
        <td><?= lang('起运港');?></td>
        <td>
            <?= $file_content_html['trans_origin'];?>
        </td>
        <td><?= lang('唛头');?></td>
        <td>
            <?= $file_content_html['mark_nums'];?>
        </td>
    </tr>
    <tr>
        <td><?= lang('中转港');?></td>
        <td>
            <?= $file_content_html['trans_discharge'];?>
        </td>
        <td><?= lang('HS CODE');?></td>
        <td>
            <?= $file_content_html['hs_code'];?>
        </td>
    </tr>
    <tr>
        <td><?= lang('目的港');?></td>
        <td>
            <?= $file_content_html['trans_destination'];?>
        </td>
        <td><?= lang('件毛体');?></td>
        <td>
            <?= lang('件数');?>:<?= $file_content_html['good_outers'];?> <?= $file_content_html['good_outers_unit'];?><br />
            <?= lang('毛重');?>:<?= $file_content_html['good_weight'];?><br />
            <?= lang('体积');?>:<?= $file_content_html['good_volume'];?><br />
        </td>
    </tr>
    <tr>
        <td><?= lang('箱信息');?></td>
        <td>
            <table style="border-collapse: collapse;">
                <?php foreach($file_content_html['container'] as $container){ ?>
                    <tr>
                        <td><?= join(',', $container);?></td>
                    </tr>
                <?php } ?>
            </table>
        </td>
        <td><?= lang('货物描述');?></td>
        <td>
            <?= $file_content_html['description'];?>
        </td>
    </tr>
</table>
<div>
    <h3><?= lang('shipper');?>:</h3>
    <span>
        <?= $file_content_html['shipper'];?>
    </span>
</div>
<div>
    <h3><?= lang('consignee');?>:</h3>
    <span>
        <?= $file_content_html['consignee'];?>
    </span>
</div>
<div>
    <h3><?= lang('notify');?>:</h3>
    <span>
        <?= $file_content_html['notify'];?>
    </span>
</div>