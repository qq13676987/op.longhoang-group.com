<link rel="stylesheet" type="text/css" href="/inc/js/easyui_1_8_6/themes/default/easyui.css?v=1">
<link rel="stylesheet" type="text/css" href="/inc/js/easyui_1_8_6/themes/icon.css?v=1">
<script type="text/javascript" src="/inc/js/easyui_1_8_6/jquery.min.js"></script>
<script type="text/javascript" src="/inc/js/easyui_1_8_6/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/inc/js/easyui_1_8_6/jquery.easyui.min.js"></script>
<script type="text/javascript" src="/inc/js/other.js?v=10"></script>
<script type="text/javascript" src="/inc/js/set_search.js?v=5"></script>
<style type="text/css">
    a{text-decoration:none}
</style>
<script>
    /**
     * 下载报文
     * @param id
     */
    function read_message(id) {
        window.open('/bsc_message/file_read/' + id);
    }

    /**
     * 创建报文
     * @param type
     * @param receiver
     * @param action
     */
    function create_message(type='', receiver='', action = "", match_id = 0, is_diy_data = 0) {
        var id_no = $('#id_no').val();
        if(!is_diy_data){
            //直接生成报文的
            ajaxLoading();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/bsc_message/create_message/' + id_no + '/' + receiver + '/' + type  + '/' + action + '?match_id=' + match_id,
                success: function (res) {
                    ajaxLoadEnd();
                    if(res.code == 0){
                        $.messager.alert('<?= lang('提示');?>', res.msg, 'info', function () {
                            if(res.data !== undefined && res.data.id !== undefined) window.open('/bsc_message/file_read/' + res.data.id);
                            location.reload();
                        });
                    }else if(res.code == 444){
                        // GET方式示例，其它方式修改相关参数即可
                        var form = $('<form></form>').attr('action','/other/ts_text').attr('method','POST').attr('target', '_blank');
                        var json_data = {};
                        json_data['text'] = res.text;
                        json_data['ts_str[]'] = res.ts_str;
                        json_data['msg'] = res.msg;
                        $.each(json_data, function (index, item) {
                            if(typeof item === 'string'){
                                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                            }else{
                                $.each(item, function(index2, item2){
                                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                                })
                            }
                        });

                        form.appendTo('body').submit().remove();
                    }else{
                        $.messager.alert('<?= lang('提示');?>', res.msg);
                    }
                },
                error: function (e) {
                    ajaxLoadEnd();
                    $.messager.alert('<?= lang('提示');?>', e.responseText);
                }
            });
        }else{
            //ams 生成前加一个填写参数的页面
            $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否填写额外参数后提交？');?>', function(r){
                if (r){
                    var url = '/bsc_message/message_form/<?= $id_type;?>/' + id_no + '/' + type + '/' + receiver + '/' + action + '?match_id=' + match_id;
                    if(is_diy_data === 1){
                        //跳转形式
                        window.open(url);
                    }else if(is_diy_data === 2){
                        //小窗口
                        var iframe = $("<iframe>").attr('src', url).css({
                            width: 'auto',
                            height: 'auto',
                            border:0,
                        }).attr('is_load', 'false');
                        iframe.load(function(){
                            IFrameReSize(iframe);
                            IFrameReSizeWidth(iframe);
                            $(this).parent('div').window('resize', {
                                width : $(this).width() + 19,
                                height : $(this).height() + 40
                            });
                            $(this).attr('is_load', 'true');
                        });
                        $("<div>").window({
                            width: 100,
                            height: 100,
                            title:'<?= lang('window');?>',
                            content: iframe,
                            onResize:function (width, height) {
                                var iframe = $(this).children('iframe');
                                if(iframe.attr('is_load') === 'true'){
                                    iframe.css({
                                        width:width - 19,
                                        height:height - 40,
                                    });
                                }
                                
                            }
                        });
                    }
                }else{
                    create_message(type, receiver, action, match_id, 0);
                }
            });
        }

    }

    /**
     * 发送报文
     */
    function send_message(id = 0, action = '', receiver = '') {
        $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认发送报文?');?>', function (r) {
            if(r){
                ajaxLoading();
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/bsc_message/send_message/' + id + '/' + action + '/' + receiver,
                    success: function (res) {
                        ajaxLoadEnd();
                        if(res.code == 0){
                            $.messager.alert('<?= lang('提示');?>', res.msg, 'info', function () {
                                location.reload();
                            });
                        }else{
                            $.messager.alert('<?= lang('提示');?>', res.msg, 'info');
                        }
                    },
                    error: function (e) {
                        ajaxLoadEnd();
                        $.messager.alert('<?= lang('提示');?>', e.responseText);
                    }

                });
            }
        });

    }

    /**
     * 查看发送明细
     * @param message_id
     */
    function send_log(message_id){
        var width = 610, height = 400;
        // var iframe = $("<iframe>").attr('src', '/bsc_message/send_log?id=' + message_id).css({
        //     width: width - 14 ,
        //     height: height - 39,
        // });
        var send_log_table = $("<table>").attr('id', 'send_log');
        $("<div>").window({
            width: width,
            height: height,
            title:'<?= lang('发送明细');?>',
            content: send_log_table,
        });
        send_log_table.datagrid({
            url:'/bsc_message/get_send_log?id=' + message_id,
            width: width - 18 ,
            height: height - 42,
            columns:[[
                {field: 'id', title: "<?= lang('ID');?>", width:60, sortable: true},
                {field: 'status', title: "<?= lang('状态');?>", width:60, sortable: true, formatter:function (value,row,index) {
                    return row.status_name
                }},
                {field: 'return_msg', title: "<?= lang('返回信息');?>", width:120, sortable: true},
                {field: 'send_by', title: "<?= lang('发送人');?>", width:60, sortable: true, formatter:function (value,row,index) {
                    return row.send_by_name;
                }},
                {field: 'send_time', title: "<?= lang('发送时间');?>", width:150, sortable: true},
            ]],
            rownumbers: false,
            pagination: true,
            pageSize: 30,
            singleSelect:true,
            rowStyler: function (index,row) {
                var style = "";
                if(row.status === '0'){
                    style += "color: red";
                }else{
                    style += "color: green";
                }
            }
        });
    }

    /**
     * 获取回执
     * @param id
     * @param action
     * @param receiver
     */
    function get_out(id = 0, action = '', receiver = ''){
        ajaxLoading();
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: '/bsc_message/logwing_ftp_read_out/' + id,
            success: function (res) {
                ajaxLoadEnd();
                if(res.code == 0){
                    $.messager.alert('<?= lang('回执内容');?>', res.msg);
                }else{
                    $.messager.alert('<?= lang('提示');?>', res.msg);
                }
            },
            error: function (e) {
                ajaxLoadEnd();
                $.messager.alert('<?= lang('提示');?>', e.responseText);
            }

        });
    }

    $(function () {
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height() - 26,
        });
        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });
</script>
<script>
    //iframe高度自适应
    function IFrameReSize(e) {

        var pTar = $(e)[0];

        if (pTar) { //ff

            if (pTar.contentDocument && pTar.contentDocument.body.offsetHeight) {

                pTar.height = pTar.contentDocument.body.offsetHeight;

            } //ie

            else if (pTar.Document && pTar.Document.body.scrollHeight) {

                pTar.height = pTar.Document.body.scrollHeight;

            }

        }

    }

    //iframe宽度自适应

    function IFrameReSizeWidth(e) {

        var pTar = $(e)[0];

        if (pTar) { //ff

            if (pTar.contentDocument && pTar.contentDocument.body.offsetWidth) {

                pTar.width = pTar.contentDocument.body.offsetWidth;

            } //ie

            else if (pTar.Document && pTar.Document.body.scrollWidth) {

                pTar.width = pTar.Document.body.scrollWidth;

            }

        }

    }
</script>
<body>
<input type="hidden" id="id_no" value="<?= $id_no;?>">
    <table id="tt" class="easyui-datagrid"  data-options="singleSelect:true,collapsible:true,">
        <thead>
            <tr>
                <th data-options="field:'type',width:80"><?= lang('类型');?></th>
                <th data-options="field:'receiver',width:100"><?= lang('接收方');?></th>
                <th data-options="field:'trans_carrier_name',width:200"><?= lang('船公司');?></th>
                <th data-options="field:'booking_agent_name',width:200"><?= lang('订舱代理');?></th>
                <th data-options="field:'button',width:300,align:'left'"><?= lang('生成报文');?></th>
                <th data-options="field:'send_time',width:200"><?= lang('发送报文');?></th>
                <th data-options="field:'return_msg',width:200,align:'center'"><?= lang('报文回执');?></th>
                <th data-options="field:'send_by_name',width:200,align:'center'"><?= lang('最后1次发送人');?></th>
                <th data-options="field:'send_count',width:200,align:'center'"><?= lang('发送次数');?></th>
            </tr>
        </thead>
        <tbody>
            <?php if(isset($rows)) foreach ($rows as $row){ ?>
            <tr>
                <td><?= $row['message_type_name']; ?></td>
                <td><?= $row['message_receiver_name']; ?></td>
                <td><?= $row['trans_carrier_name']; ?></td>
                <td><?= $row['booking_agent_name']; ?></td>
                <td>
                    <?php if($row['file_path']){ ?>
                    <div style="margin-bottom: 10px;">
                        <a href="/upload/message/<?= $row['message_receiver'];?>_EDI/<?= $row['message_type']; ?><?= $row['file_path'];?>" download="<?= $row['file_name'];?>"><?= lang('下载报文');?></a>
                        &nbsp;
                        &nbsp;
                        <a href="javascript: void(0);" onclick="read_message(<?= $row['message_id']; ?>)"><?= lang('查看报文');?></a>
                    </div>
                    <?php } ?>
                    <div>
                        <?php if($row['insert_code'] !== ''){ ?>
                            <a href="javascript: void(0);" onclick="create_message('<?= $row['message_type']; ?>', '<?= $row['message_receiver']; ?>', 'insert', <?= $row['id'];?>,<?= $row['is_diy_data']; ?>)"><?= lang('生成新增报文');?></a>
                        <?php } ?>
                        &nbsp;
                        &nbsp;
                        &nbsp;
                        <?php if($row['update_code'] !== ''){ ?>
                            <a href="javascript: void(0);" onclick="create_message('<?= $row['message_type']; ?>', '<?= $row['message_receiver']; ?>', 'update', <?= $row['id'];?>, <?= $row['is_diy_data']; ?>)"><?= lang('生成修改报文');?></a>
                        <?php } ?>
                    </div>
                </td>
                <td>
                    <span style="color:orange;"><?= $row['send_time']; ?></span>
                    <br />
                    <?php if($row['message_status'] === '0'){?>
                        <?= lang('未发送');?>
                        <br/>
                    <?php }else if($row['message_status'] === '1'){ ?>
                        <?= lang('发送成功');?>
                        <br/>
                    <?php }else if($row['message_status'] === '2'){?>
                        <?= lang('发送修改成功');?>
                        <br/>
                    <?php }?>
                    <?php if($row['send_type'] !== ''){ ?>
                        <a href="javascript: void(0);" onclick="send_message('<?= $row['message_id'];?>', '<?= $row['send_type'];?>', '<?= $row['message_receiver'];?>')"><?= lang('点击发送报文');?></a>
                        &nbsp;
                        &nbsp;
                        &nbsp;
                    <?php } ?>
                    <?php if($row['message_receiver'] === 'LOGWING'){ ?>
                        <a href="javascript: void(0);" onclick="get_out('<?= $row['message_id'];?>', 'FTP', '<?= $row['message_receiver'];?>')"><?= lang('获取回执');?></a>
                    <?php } ?>
                </td>
                <td><?= $row['return_msg']; ?></td>
                <td><?= $row['send_by_name']; ?></td>
                <td>
                   <a href="javascript:void(0);" onclick="send_log(<?= $row['message_id'];?>)"> <?= $row['send_count']; ?></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</body>