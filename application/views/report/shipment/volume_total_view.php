<script type="text/javascript" src="/inc/js/easyui/datagrid-export.js"></script>
<style type="text/css">
    .hide{
        display: none;
    }
    .f{
        width:120px;
    }
    .s{
        width:100px;
    }
    .v{
        width:120px;
    }
    .del_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }
    .breadcrumb > li + li:before {
        color: #CCCCCC;
        content: "/ ";
        padding: 0 5px;
    }
    .breadcrumb > li{
        display: inline-block;
    }
    .add{
        background: url(/inc/js/easyui/themes/icons/edit_add.png) no-repeat center center;
    }
    .remove{
        background: url(/inc/js/easyui/themes/icons/edit_remove.png) no-repeat center center;
    }
</style>
<!--这里是查询框相关的代码-->
<script>
    var query_option = {};//这个是存储已加载的数据使用的

    var table_name = 'biz_shipment',//需要加载的表名称
        view_name = 'report_volume_total';//视图名称
    var add_tr_str = "";//这个是用于存储 点击+号新增查询框的值, load_query_box会根据接口进行加载


    /**
     * 加载查询框
     */
    function load_query_box(){
        ajaxLoading();
        $.ajax({
            type:'GET',
            url: '/sys_config_title/get_user_query_box?view_name=' + view_name + '&table_name=' + table_name,
            dataType:'json',
            success:function (res) {
                ajaxLoadEnd();
                if(res.code == 0){
                    //加载查询框
                    $('#cx .cx_box').append(res.data.box_html);
                    //需要调用下加载器才行
                    $.parser.parse($('#cx table tbody .query_box'));

                    add_tr_str = res.data.add_tr_str;
                    query_option = res.data.query_option;

                    //渲染表头
                    $('#tt').datagrid({
                        width: 'auto',
                        height: $(window).height() - 67,
                        onSelect:function(index ,row){
                            get_statistics();
                        },
                        onUnselect:function(index ,row){
                            get_statistics();
                        },
                        onSelectAll:function(index ,row){
                            get_statistics();
                        },
                        onUnselectAll:function(index ,row){
                            get_statistics();
                        },
                        onLoadSuccess:function(data){
                            var ext_data = data.ext_data;
                            $(this).datagrid('changeColumnSort', {
                                sortOrder: ext_data.order,
                                sortName: ext_data.sort,
                            });
                        },
                        rowStyler:function(index,row){
                            if(row.is_footer){
                                return "font-weight:900;background-color:#F5F5F5;";
                            }
                        },
                    });

                    $(window).resize(function () {
                        $('#tt').datagrid('resize');
                    });

                    //为了避免初始加载时, 重复加载的问题,这里进行了初始加载ajax数据
                    var aj = [];
                    $.each(res.data.load_url, function (i,it) {
                        aj.push(get_ajax_data(it, i));
                    });
                    //加载完毕触发下面的下拉框渲染
                    //$_GET 在other.js里封装
                    var this_url_get = {};
                    $.each($_GET, function (i, it) {
                        this_url_get[i] = it;
                    });
                    var auto_click = this_url_get.auto_click;

                    $.when.apply($,aj).done(function () {
                        //这里进行字段对应输入框的变动
                        $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                            var ec_value = $(ec_item).combobox('getValue');
                            var v_inp = $('.v:eq(' + ec_index + ')');
                            var s_val = $('.s:eq(' + ec_index + ')').combobox('getValue');
                            var v_val = v_inp.textbox('getValue');
                            //如果自动查看,初始值为空
                            if(auto_click === '1') v_val = '';

                            $(ec_item).combobox('clear').combobox('select', ec_value);
                            //这里比对get数组里的值
                            $.each(this_url_get, function (trg_index, trg_item) {
                                //切割后,第一个是字段,第二个是符号
                                var trg_index_arr = trg_index.split('-');
                                //没有第二个参数时,默认用等于
                                if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';
                                //用一个删一个
                                //如果当前的选择框的值,等于get的字段值
                                if(ec_value === trg_index_arr[0] && s_val === trg_index_arr[1]){
                                    v_val = trg_item;//将v改为当前get的
                                    // $('.s:eq(' + ec_index + ')').combobox('setValue', trg_index_arr[1]);//设置S的值
                                    delete this_url_get[trg_index];
                                }
                            });
                            //没找到就正常处理
                            $('.v:eq(' + ec_index + ')').textbox('setValue', v_val);
                        });
                        //判断是否有自动查询参数
                        var no_query_get = ['auto_click','year', 'year_start', 'year_end'];//这里存不需要塞入查询的一些特殊变量
                        $.each(no_query_get, function (i,it) {
                            delete this_url_get[it];
                        });
                        //完全没找到的剩余值,在这里新增一个框进行选择
                        $.each(this_url_get, function (trg_index, trg_item) {
                            add_tr();//新增一个查询框
                            var trg_index_arr = trg_index.split('-');
                            //没有第二个参数时,默认用等于
                            if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';

                            $('#cx table tbody tr:last-child .f').combobox('setValue', trg_index_arr[0]);
                            $('#cx table tbody tr:last-child .s').combobox('setValue', trg_index_arr[1]);
                            $('#cx table tbody tr:last-child .v').textbox('setValue', trg_item);
                        });

                        //这里自动执行查询,显示明细
                        if(auto_click === '1'){
                            doSearch();
                        }
                        var serarchs = $('.searchs').length / 2;
                        var groups_field = $('.groups_field:checked').val();
                        if(serarchs <= 0 && groups_field == undefined){
                            $('#chaxun').window('open');
                        }else{
                            loaddata();
                        }
                        var str = '<li><a href="javascript:window.location.href= \'/report/index\';"><?= lang('report首页');?></a></li>';
                        str += '<li><a href="javascript:window.location.href= \'/report/volume_total\'"><?= lang('重新查询');?></a></li>';
                        for(var i = 0; i < serarchs; i++){
                            var field = $('.searchs:eq(' + (parseInt(i) * 2) +')');
                            var value = $('.searchs:eq(' + ((parseInt(i) * 2) + 1) +')');
                            var this_field_val = field.val();
                            var this_value_val = value.attr('tname');
                            var tkey = field.attr('tkey');
                            str += '<li><a href="javascript:void(0);" onclick="back(' + (serarchs - i) + ')">' + tkey + ':' + this_value_val + '</a></li>';
                        }
                        $("#now_crumb").parent('li').before(str);
                    });
                }else{
                    $.messager.alert("<?= lang('提示');?>", res.msg);
                }
            },error:function (e) {
                ajaxLoadEnd();
                $.messager.alert("<?= lang('提示');?>", e.responseText);
            }
        });
    }

    /**
     * 新增一个查询框
     */
    function add_tr() {
        $('#cx .cx_box tbody').append(add_tr_str);
        $.parser.parse($('#cx .cx_box tbody tr:last-child'));
        var last_f = $('#cx .cx_box tbody tr:last-child .f');
        var last_f_v = last_f.combobox('getValue');
        last_f.combobox('clear').combobox('select', last_f_v);
        return false;
    }

    /**
     * 删除一个查询
     */
    function del_tr(e) {
        //删除按钮
        var index = $(e).parents('tr').index();
        $(e).parents('tr').remove();
        return false;
    }

    //执行加载函数
    $(function () {
        load_query_box();
    });
</script>
<script type="text/javascript">
    function loaddata(){
        json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });

        $('#search').css('display', 'none');
        $('#tt').datagrid({
            url:'/report/get_volume_total_data',
            queryParams: json
        }).datagrid('clearSelections');

        var title = $('.groups_field:checked').attr('title');
        $("#now_crumb").text(title);
    }
    var json = {};
    function doSearch(jump = false) {
        var this_val = $('.groups_field:checked').val();
        var serarchs = $('.searchs').length;
        if(this_val == undefined){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('必须选择一个维度');?>');
            return;
        }
        var select_row = $('#tt').datagrid('getSelected');
        if(select_row == null && serarchs > 0){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择一行');?>');
            return;
        }
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 1){
            $.messager.alert('<?= lang('提示');?>', '<?= lang('不能选择多行');?>');
            return;
        }
        if(select_row !== null){
            var s_field_val = "<?= $groups_field;?>";
            if(s_field_val == "") s_field_val = json.groups_field;
            var str = '<input class="searchs" type="hidden" name="searchs[' + (serarchs / 2) + '][]" value="' + s_field_val +'">' +
                '<input class="searchs" type="hidden" name="searchs[' + (serarchs / 2) + '][]" value="' + select_row.name_id + '">';

            $('#cx').append(str);

            $('#chaxun').window('close');

            //带参数跳转到当前页面
            $('#cx_submit').trigger('click');
        }else{
            $('#chaxun').window('close');
            set_config();
            //带参数跳转到当前页面
            $('#cx_submit').trigger('click');
            // loaddata();
        }
    }

    /**
     * 返回几页, 和
     */
    function back(count) {
        $('#back').val(count);
        $('#cx_submit').trigger('click');
    }

    function get_statistics(){
        var select = $('#tt').datagrid('getSelections');
        var row = {};
        var data = $('#tt').datagrid('getData').rows;
        if(data.length >= 1){
            $.each(data[0], function(index, item){
                if(typeof item == 'number'){
                    row[index] = 0;
                }else{
                    row[index] = '';
                }
            });
        }
        $.each(select, function (index, item) {
            $.each(item, function(i, val){
                if(typeof val == 'number'){
                    var this_val = parseInt(val * 10000);
                    row[i] += this_val;
                }
            });
        });
        $.each(row, function(index, item){
            if(typeof item == 'number'){
                row[index] = item / 10000;
            }
        });

        //ex_AIR_KGS ex_LCL_CBM im_AIR_KGS im_LCL_CBM
        if(row['ex_AIR_good_outers'] != undefined)row['ex_AIR_KGS'] = row['ex_AIR_good_outers'] + '/' + row['ex_AIR_good_weight'] + '/' + row['ex_AIR_good_volume'];
        if(row['ex_LCL_good_outers'] != undefined)row['ex_LCL_CBM'] = row['ex_LCL_good_outers'] + '/' + row['ex_LCL_good_weight'] + '/' + row['ex_LCL_good_volume'];
        if(row['im_AIR_good_outers'] != undefined)row['im_AIR_KGS'] = row['im_AIR_good_outers'] + '/' + row['im_AIR_good_weight'] + '/' + row['im_AIR_good_volume'];
        if(row['im_LCL_good_outers'] != undefined)row['im_LCL_CBM'] = row['im_LCL_good_outers'] + '/' + row['im_LCL_good_weight'] + '/' + row['im_LCL_good_volume'];

        $('#statistics_form').form('load', row);
    }

    $.fn.datagrid.defaults.view.renderFooter =  function(target, container, frozen){
        var opts = $.data(target, 'datagrid').options;
        var rows = $.data(target, 'datagrid').footer || [];
        var fields = $(target).datagrid('getColumnFields', frozen);
        var table = ['<table class="datagrid-ftable" cellspacing="0" cellpadding="0" border="0"><tbody>'];

        for(var i=0; i<rows.length; i++){
            var styleValue = opts.rowStyler ? opts.rowStyler.call(target, i, rows[i]) : '';
            var style = styleValue ? 'style="' + styleValue + '"' : '';
            table.push('<tr class="datagrid-row" datagrid-row-index="' + i + '"' + style + '>');
            table.push(this.renderRow.call(this, target, fields, frozen, i, rows[i]));
            table.push('</tr>');
        }

        table.push('</tbody></table>');
        $(container).html(table.join(''));
    };

    function hidden_column(event) {
        event=event?event:window.event;
        event.stopPropagation();
        var is_add = $('.more_column').hasClass('add');

        //这里执行字段的隐藏和显示功能
        if(!is_add){
            $('.more_column').addClass('add');
            $('.more_column').removeClass('remove');
        }else{
            $('.more_column').addClass('remove');
            $('.more_column').removeClass('add');
        }

        //需要隐藏或显示的列名
        var tt = $('#tt');
        var opts = tt.datagrid('options');
        var pager = tt.datagrid('getPager');
        var panel = tt.datagrid('getPanel');

        var hidden_column = ['zdc_count', 'zdc_cancel', 'cgdc_count', 'cgdc_cancel', 'customer_reason_count', 'market_reasons_count'];

        $.each(hidden_column, function (i, it) {
            //hidden的字段整理在一起
            if(!is_add){
                tt.datagrid('hideColumn', it);
            }else{
                tt.datagrid('showColumn', it);
            }
        });
    }

    function export_excel(){
        $('#tt').datagrid('toExcel','<?= lang('货量统计');?>.xls');
    }

    function details(index) {
        //获取参数
        var form_data = {'field[v][]':[], 'field[f][]':[], 'field[s][]':[]};
        var cx_data = $('#cx').serializeArray();

        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(form_data.hasOwnProperty(item.name) === true){
                if(typeof form_data[item.name] == 'string'){
                    form_data[item.name] =  form_data[item.name].split(',');
                    form_data[item.name].push(item.value);
                }else{
                    form_data[item.name].push(item.value);
                }
            }else{
                form_data[item.name] = item.value;
            }
        });
        var get_parameter = {};

        $.each(form_data['field[v][]'], function(i,it){
            if(it != ''){
                var this_key = form_data['field[f][]'][i] + '-' + form_data['field[s][]'][i];
                get_parameter[this_key] = it;
            }
        });

        $.each(form_data, function(i, it){
            if(typeof(it) == 'object' && i.indexOf("searchs[") != -1){
                if(it[0] == 'booking_ETD_month'){
                    var y = it[1].split('-')[0];
                    var m = parseInt(it[1].split('-')[1]);
                    //created_time
                    get_parameter['biz_shipment.booking_ETD->='] = it[1] + '-01';
                    get_parameter['biz_shipment.booking_ETD-<='] = it[1] + '-' + getMonthDay(y, m - 1);
                }else if(it[0] == 'created_time_month'){
                    //created_time
                    var y = it[1].split('-')[0];
                    var m = parseInt(it[1].split('-')[1]);
                    get_parameter['biz_shipment.created_time->='] = it[1] + '-01 00:00:00';
                    get_parameter['biz_shipment.created_time-<='] = it[1] + '-' + getMonthDay(y, m - 1) + " 23:59:59";
                }else if(it[0] == 'trans_ATD_month'){
                    //created_time
                    var y = it[1].split('-')[0];
                    var m = parseInt(it[1].split('-')[1]);
                    get_parameter['biz_shipment.trans_ATD->='] = it[1] + '-01 00:00:00';
                    get_parameter['biz_shipment.trans_ATD-<='] = it[1] + '-' + getMonthDay(y, m - 1) + " 23:59:59";
                }else if(it[0] == 'booking_ETD_day'){
                    var y = it[1].split('-')[0];
                    var m = parseInt(it[1].split('-')[1]);
                    //created_time
                    get_parameter['biz_shipment.booking_ETD-='] = it[1];
                }else if(it[0] == 'created_time_day'){
                    //created_time
                    var y = it[1].split('-')[0];
                    var m = parseInt(it[1].split('-')[1]);
                    get_parameter['biz_shipment.created_time-='] = it[1];
                }else if(it[0] == 'trans_ATD_day'){
                    //created_time
                    var y = it[1].split('-')[0];
                    var m = parseInt(it[1].split('-')[1]);
                    get_parameter['biz_shipment.trans_ATD-='] = it[1];
                }else if(it[0] == 'vessel_voyage'){
                    var val = it[1];
                    var val_arr = val.split(' ').filter(e=>e);
                    var voyage = val_arr.splice(val_arr.length - 1);
                    var vessel = val_arr.join(' ');
                    get_parameter['biz_shipment.vessel-like'] = vessel;
                    get_parameter['biz_shipment.voyage-like'] = voyage.join('');
                }else if(it[0] == 'sales_company'){
                    get_parameter['biz_shipment.sales_group-like'] = it[1];
                }else{
                    get_parameter['biz_shipment.' + it[0] + '-='] = it[1];
                }
            }
        });

        //参数1 field
        var dg = $('#tt');
        dg.datagrid('clearSelections');
        dg.datagrid('selectRow', index);
        var row = dg.datagrid('getSelections')[0];
        form_data.groups_field = "<?= $groups_field;?>";
        //参数2 当前点击明细的值 groups_field = name_id值
        if(form_data.groups_field == 'booking_ETD_month'){
            var y = row.name_id.split('-')[0];
            var m = parseInt(row.name_id.split('-')[1]);
            //created_time
            get_parameter['biz_shipment.booking_ETD->='] = row.name_id + '-01';
            get_parameter['biz_shipment.booking_ETD-<='] = row.name_id + '-' + getMonthDay(y, m - 1);
        }else if(form_data.groups_field == 'created_time_month'){
            //created_time
            var y = row.name_id.split('-')[0];
            var m = parseInt(row.name_id.split('-')[1]);
            get_parameter['biz_shipment.created_time->='] = row.name_id + '-01 00:00:00';
            get_parameter['biz_shipment.created_time-<='] = row.name_id + '-' + getMonthDay(y, m - 1) + " 23:59:59";
        }else if(form_data.groups_field == 'trans_ATD_month'){
            //created_time
            var y = row.name_id.split('-')[0];
            var m = parseInt(row.name_id.split('-')[1]);
            get_parameter['biz_shipment.trans_ATD->='] = row.name_id + '-01 00:00:00';
            get_parameter['biz_shipment.trans_ATD-<='] = row.name_id + '-' + getMonthDay(y, m - 1) + " 23:59:59";
        }else if(form_data.groups_field == 'booking_ETD_day'){
            //created_time
            get_parameter['biz_shipment.booking_ETD-='] = row.name_id;
        }else if(form_data.groups_field == 'created_time_day'){
            //created_time
            get_parameter['biz_shipment.created_time-='] = row.name_id;
        }else if(form_data.groups_field == 'trans_ATD_day'){
            //created_time
            get_parameter['biz_shipment.trans_ATD-='] = row.name_id;
        }else if(form_data.groups_field == 'vessel_voyage'){
            var val = row.name_id;
            var val_arr = val.split(' ').filter(e=>e);
            var voyage = val_arr.splice(val_arr.length - 1);
            var vessel = val_arr.join(' ');
            get_parameter['biz_shipment.vessel-like'] = vessel;
            get_parameter['biz_shipment.voyage-like'] = voyage.join('');
        }else if(form_data.groups_field == 'sales_company'){
            get_parameter['biz_shipment.sales_group-like'] = row.name_id;
        }else{
            get_parameter['biz_shipment.' + form_data.groups_field + '-='] = row.name_id;
        }
        //打开新窗口跳转到shipment
        var url = '/biz_shipment/index?auto_click=1';
        $.each(get_parameter, function (i, it) {
            url += "&" + urlencode(i) + '=' + urlencode(it);
        });
        window.open(url);
    }

    /**
     * 明细2 该明细是用于 根据job_no 统计的
     */
    function details2(index){
        var cx_data = $('#cx').serializeArray();

        var form_data= {};

        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(form_data.hasOwnProperty(item.name) === true){
                if(typeof form_data[item.name] == 'string'){
                    form_data[item.name] =  form_data[item.name].split(',');
                    form_data[item.name].push(item.value);
                }else{
                    form_data[item.name].push(item.value);
                }
            }else{
                form_data[item.name] = item.value;
            }
        });

        //这里和原本相比,需要把之前的groups_field 加入到 groups_field[] 中,
        //参数1 field
        var dg = $('#tt');
        dg.datagrid('clearSelections');
        dg.datagrid('selectRow', index);
        var row = dg.datagrid('getSelections')[0];

        var groups_field = form_data.groups_field;
        var serarchs = $('.searchs').length;

        form_data['searchs[' + (serarchs / 2) + '][]'] = [groups_field, row.name_id];
        form_data.groups_field = 'job_no';

        //打开新窗口跳转到shipment
        var form = $('<form></form>').attr('action','').attr('method','POST'); // .attr('target', '_blank')

        $.each(form_data, function (index, item) {
            if(typeof item === 'string'){
                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
            }else{
                $.each(item, function(index2, item2){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                })
            }
        });
        // console.log(form_data);

        form.appendTo('body').submit().remove();
    }

    function buttons_for(value, row, index) {
        var str = '';
        if(row.is_footer) return str;
        str += '<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$(\'#chaxun\').window(\'open\');"><?= lang('query');?></a>\n';
        str += '<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:details(' + index + ');"><?= lang('明细');?></a>\n';
        str += '<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:details2(' + index + ');"><?= lang('明细2');?></a>\n';
        return str;
    }
</script>
<ul class="breadcrumb">
    <li><a href="javascript:void(0);" id="now_crumb"></a></li>
</ul>
<table id="tt" rownumbers="false" idField="id" toolbar="#tb" singleSelect="false" nowrap="true" showFooter="true" autoRowHeight="false">
    <thead>
    <tr>
        <th title="全选" field="ck" checkbox="true"></th>
        <th data-options="field:'buttons',width:100,sortable:true,formatter:buttons_for"></th>
        <th data-options="field:'name',width:160,sortable:true"><?= lang($groups_field);?></th>
        <?php
        if($groups_field=='sales' || $groups_field=='operator' || $groups_field=='customer_service' || $groups_field=='sales_assistant'){
            echo "<th data-options=\"field:'sub_company_name',width:80,sortable:true\">" . lang('公司') . "</th>";
            echo "<th data-options=\"field:'group_name',width:100,sortable:true\">". lang('部门') . "</th>";
        }
        ?>
        <?php
        if($groups_field=='sales_group' || $groups_field=='operator_group' || $groups_field=='customer_service_group'){
            echo "<th data-options=\"field:'group_user_num',width:80,sortable:true\">" . lang('在职人数') . "</th>";
        }
        ?>
        <th data-options="field:'count',width:80,sortable:true"><?= lang('业务票数');?></th>
        <th data-options="field:'count_cancel',width:80,sortable:true"><?= lang('退关票数');?></th>
        <th data-options="field:'rate_cancel',width:80,sortable:true"><?= lang('退关率');?><a href="javascript:void(0);" class="more_column l-btn-icon add" onClick="hidden_column(event)" style="position: unset;padding-left: 5px;margin-top: 0;"></a></th>
        <th data-options="field:'zdc_count',width:80,sortable:true,hidden:true"><?= lang('客户自订舱(总)');?></th>
        <th data-options="field:'zdc_cancel',width:80,sortable:true,hidden:true"><?= lang('客户自订舱(退)');?></th>
        <th data-options="field:'cgdc_count',width:80,sortable:true,hidden:true"><?= lang('常规订舱(总)');?></th>
        <th data-options="field:'cgdc_cancel',width:80,sortable:true,hidden:true"><?= lang('常规订舱(退)');?></th>
        <?php foreach ($ext_fields as $ext_f_k => $ext_field){ ?>
            <th data-options="field:'<?= $ext_f_k;?>_count',width:80,sortable:true,hidden:true"><?= lang($ext_field);?></th>
        <?php } ?>
        <th data-options="field:'export_TEU',width:80,sortable:true"><?= lang('出口TEU');?></th>
        <th data-options="field:'ex_LCL_good_outers',width:80,sortable:true"><?= lang('出拼（件）');?></th>
        <th data-options="field:'ex_LCL_good_weight',width:80,sortable:true"><?= lang('出拼（毛）');?></th>
        <th data-options="field:'ex_LCL_good_volume',width:80,sortable:true"><?= lang('出拼（体）');?></th>
        <th data-options="field:'import_TEU',width:80,sortable:true"><?= lang('进口TEU');?></th>
        <th data-options="field:'im_LCL_good_outers',width:80,sortable:true"><?= lang('进拼（件）');?></th>
        <th data-options="field:'im_LCL_good_weight',width:80,sortable:true"><?= lang('进拼（毛）');?></th>
        <th data-options="field:'im_LCL_good_volume',width:80,sortable:true"><?= lang('进拼（体）');?></th>
        <th data-options="field:'ex_AIR_good_outers',width:80,sortable:true"><?= lang('出空（件）');?></th>
        <th data-options="field:'ex_AIR_good_weight',width:80,sortable:true"><?= lang('出空（毛）');?></th>
        <th data-options="field:'ex_AIR_good_volume',width:80,sortable:true"><?= lang('出空（体）');?></th>
        <th data-options="field:'im_AIR_good_outers',width:80,sortable:true"><?= lang('进空（件）');?></th>
        <th data-options="field:'im_AIR_good_weight',width:80,sortable:true"><?= lang('进空（毛）');?></th>
        <th data-options="field:'im_AIR_good_volume',width:80,sortable:true"><?= lang('进空（体）');?></th>
        <th data-options="field:'TEU',width:80,sortable:true"><?= lang('TEU');?></th>
        <th data-options="field:'20_size',width:80,sortable:true"><?= lang("20'");?></th>
        <th data-options="field:'40_size',width:80,sortable:true"><?= lang("40'");?></th>
        <th data-options="field:'45_size',width:80,sortable:true"><?= lang("40'");?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0);" onclick="back(1)"> <?= lang('返回上一页');?> </a>
                <a href="javascript:void(0)" id="search" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>
                <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#excel_menu',plain:true" iconCls="icon-reload"><?= lang('export excel');?></a>
            </td>
        </tr>
    </table>
    <div id="excel_menu" style="width:150px;">
        <div data-options="iconCls:'icon-ok'" onclick="export_excel()"><?= lang('导出当前页面');?></div>
    </div>
</div>
<div class="statistics_div">
    <form id="statistics_form">
        <table style="text-align:right;">
            <tr>
                <td><?= lang('业务票数');?>:<input class="easyui-textbox" name="count" readonly style="width:100px;"></td>
                <td><?= lang('退关票数');?>:<input class="easyui-textbox" name="count_cancel" readonly style="width:100px;"></td>
                <td><?= lang('出口TEU');?>:<input class="easyui-textbox" name="export_TEU" readonly style="width:100px;"></td>
                <td><?= lang('出拼CBM');?>:<input class="easyui-textbox" name="im_LCL_CBM" readonly style="width:100px;"></td>
                <td><?= lang('进口TEU');?>:<input class="easyui-textbox" name="import_TEU" readonly style="width:100px;"></td>
                <td><?= lang('出空KGS');?>:<input class="easyui-textbox" name="ex_LCL_CBM" readonly style="width:100px;"></td>
                <td><?= lang('进空KGS');?>:<input class="easyui-textbox" name="im_AIR_KGS" readonly style="width:100px;"></td>
            </tr>
            <tr>
                <td><?= lang('TEU');?>:<input class="easyui-textbox" name="TEU" readonly style="width:100px;"></td>
                <td><?= lang("20'");?>:<input class="easyui-textbox" name="20_size" readonly style="width:100px;"></td>
                <td><?= lang("40'");?>:<input class="easyui-textbox" name="40_size" readonly style="width:100px;"></td>
                <td><?= lang("45'");?>:<input class="easyui-textbox" name="45_size" readonly style="width:100px;"></td>
            </tr>
        </table>
    </form>
</div>
<div id="chaxun" class="easyui-window" title="<?= lang('query');?>" closed="true" style="width:800px;height:380px;padding:5px;">
    <br><br>
    <form id="cx" action="/report/volume_total" method="POST">
        <input type="hidden" name="ids">
        <input type="hidden" name="back" id="back">
        <?php foreach($searchs as $s_key => $search){ ?>
            <input class="searchs" type="hidden" name="searchs[<?= $s_key;?>][]" tkey="<?= lang($search[0]);?>" value="<?= $search[0];?>" tname="<?= $search[2];?>">
            <input class="searchs" type="hidden" name="searchs[<?= $s_key;?>][]" tkey="<?= lang($search[0]);?>" value="<?= $search[1];?>" tname="<?= $search[2];?>">
        <?php } ?>
        <?php if(!empty($fields) && !empty($searchs)){ foreach($fields['f'] as $f_key => $f){ ?>
            <input type="hidden" name="field[f][]" value="<?= $f;?>">
            <input type="hidden" name="field[s][]" value="<?= $fields['s'][$f_key];?>">
            <input type="hidden" name="field[v][]" value="<?= $fields['v'][$f_key];?>">
        <?php }}else{ ?>
            <table class="cx_box"></table>
        <?php } ?>
        <br><br>
        <fieldset>
            <h3><?= lang('表头维度');?>：</h3>
            <table>
                <tr>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="trans_ATD_month" <?php if($groups_field == 'trans_ATD_month') echo 'checked';?> title="<?= lang('按月(ATD)');?>"><?= lang('按月(ATD)');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="booking_ETD_month" <?php if($groups_field == 'booking_ETD_month') echo 'checked';?> title="<?= lang('按月(订舱船期)');?>"><?= lang('按月(订舱船期)');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="created_time_month" <?php if($groups_field == 'created_time_month') echo 'checked';?> title="<?= lang('按月(创建日期)');?>"><?= lang('按月(创建)');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="trans_ATD_day" <?php if($groups_field == 'trans_ATD_day') echo 'checked';?> title="<?= lang('按日(ATD)');?>"><?= lang('按日(ATD)');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="booking_ETD_day" <?php if($groups_field == 'booking_ETD_day') echo 'checked';?> title="<?= lang('按日(订舱船期)');?>"><?= lang('按日(订舱船期)');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="created_time_day" <?php if($groups_field == 'created_time_day') echo 'checked';?> title="<?= lang('按日(创建日期)');?>"><?= lang('按日(创建)');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="job_no" <?php if($groups_field == 'job_no') echo 'checked';?> title="<?= lang('SHIPMENT 内部编号');?>"><?= lang('SHIPMENT');?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="sales" <?php if($groups_field == 'sales') echo 'checked';?> title="<?= lang('销售');?>"><?= lang('销售');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="sales_group" <?php if($groups_field == 'sales_group') echo 'checked';?> title="<?= lang('销售部');?>"><?= lang('销售部');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="sales_company" <?php if($groups_field == 'sales_company') echo 'checked';?> title="<?= lang('分公司(销售)');?>"><?= lang('分公司(销售)');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="operator" <?php if($groups_field == 'operator') echo 'checked';?> title="<?= lang('操作');?>"><?= lang('操作');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="operator_group" <?php if($groups_field == 'operator_group') echo 'checked';?> title="<?= lang('操作部');?>"><?= lang('操作部');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="customer_service" <?php if($groups_field == 'customer_service') echo 'checked';?> title="<?= lang('客服');?>"><?= lang('客服');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="customer_service_group" <?php if($groups_field == 'customer_service_group') echo 'checked';?> title="<?= lang('客服部');?>"><?= lang('客服部');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="sales_assistant" <?php if($groups_field == 'sales_assistant') echo 'checked';?> title="<?= lang('销售助理');?>"><?= lang('销售助理');?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="trans_origin" <?php if($groups_field == 'trans_origin') echo 'checked';?> title="<?= lang('起运港');?>"><?= lang('起运港');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="trans_destination_inner" <?php if($groups_field == 'trans_destination_inner') echo 'checked';?> title="<?= lang('目的港');?>"><?= lang('目的港');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="creditor" <?php if($groups_field == 'creditor') echo 'checked';?> title="<?= lang('订舱代理');?>"><?= lang('订舱代理');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="client_code" <?php if($groups_field == 'client_code') echo 'checked';?> title="<?= lang('委托方');?>"><?= lang('委托方');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="vessel_voyage" <?php if($groups_field == 'vessel_voyage') echo 'checked';?> title="<?= lang('船名航次');?>"><?= lang('船名航次');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="biz_type" <?php if($groups_field == 'biz_type') echo 'checked';?> title="<?= lang('业务类型');?>"><?= lang('业务类型');?>
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="sailing_area" <?php if($groups_field == 'sailing_area') echo 'checked';?> title="<?= lang('航线区域');?>"><?= lang('航线区域');?>
                        <input class="groups_field" type="radio" name="groups_field" value="shipment" style="display:none;" <?php if($groups_field == 'shipment') echo 'checked';?> title="<?= lang('shipment');?>">
                    </td>
                    <td>
                        <input class="groups_field" type="radio" name="groups_field" value="trans_carrier" <?php if($groups_field == 'trans_carrier') echo 'checked';?> title="<?= lang('承运人');?>"><?= lang('承运人');?>
                    </td>
                </tr>
            </table>


        </fieldset>
        <button class="easyui-linkbutton" onclick="doSearch(true);" style="width:200px;height:30px;" type="button"><?= lang('search');?></button>
        <button id="cx_submit" type="submit" style="display:none;"></button>
    </form>
</div>