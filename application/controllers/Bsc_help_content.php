<?php


class Bsc_help_content extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('bsc_help_content_model');
        $this->model = $this->bsc_help_content_model;
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($this->field_edit, $v);
        }
    }
    public $field_all = array(
        array("id", "id", "40", "1", 'sortable:true'),
        array("key", "key", "0", "1"),
        array("content", "content", "0", "1"),

    );

    public $lock_filed = array();

    public $field_edit = array();

    public function help($key = ''){
        $data = $this->model->get_one('key', $key);
        if(empty($data)) $data['content'] = '';
        $data['content'] = str_replace("\n", '<br/>', $data['content']);
        echo $data['content'];
    }
    public function help_carrier($key = ''){
        $data = $this->model->get_one('key', $key);
        if(empty($data)) $data['content'] = '';
        $data['content'] = str_replace("\n", '<br/>', $data['content']);
        if($data['content'] != '') echo "<script>parent.$('#trans_carrier_rule').window('open')</script>";
        echo $data['content'];
    }
    
    public function update_by_client()
    {
        $this->load->model('bsc_dict_model');
        $id = $_REQUEST['id'];
        $filed = array(
            "key",
            "content",
        );
        
        $data = array();
        foreach ($filed as $item) {
            if (isset($_REQUEST[$item])) {
                $data[$item] = $_REQUEST[$item];
            }
        }
        $old_row = $this->model->get_one('key', $data['key']);
        if(!empty($old_row))$id = $old_row['id'];
        if($id == ''){
            $id = $this->model->save($data);
            $action = 'insert';
        }else{
            $this->model->update($id, $data);
            $action = 'update';
        }
        
        $data["id"] = $id;
        
        $log_data = array();
        $log_data["table_name"] = "bsc_help_content";
        $log_data["key"] = $id;
        $log_data["action"] = $action;
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
        echo json_encode($data);
    }
}