<script type="text/javascript" src="/inc/js/easyui/datagrid-export.js"></script>
<style type="text/css">
    .f{
        width:115px;
    }
    .s{
        width:100px;
    }
    .v{
        width:120px;
    }
    .del_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }
    #cx tr:nth-child(-n+0){
        display: none;
    }
    .all_t{
        color: blue;
        font-weight: 800;
    }
    .datagrid-btable{
        background-color: #FAFAD2;
    }
</style>
<!--这里是查询框相关的代码-->
<script>
    var query_option = {};//这个是存储已加载的数据使用的

    var table_name = 'biz_consol',//需要加载的表名称
        view_name = 'biz_consol_index';//视图名称
    var add_tr_str = "";//这个是用于存储 点击+号新增查询框的值, load_query_box会根据接口进行加载


    /**
     * 加载查询框
     */
    function load_query_box(){
        ajaxLoading();
        $.ajax({
            type:'GET',
            url: '/sys_config_title/get_user_query_box?view_name=' + view_name + '&table_name=' + table_name,
            dataType:'json',
            success:function (res) {
                ajaxLoadEnd();
                if(res.code == 0){
                    //加载查询框
                    $('#cx table').append(res.data.box_html);
                    //需要调用下加载器才行
                    $.parser.parse($('#cx table tbody .query_box'));

                    add_tr_str = res.data.add_tr_str;
                    query_option = res.data.query_option;
                    var table_columns = [[
                    ]];//表的表头

                    //填充表头信息
                    $.each(res.data.table_columns, function (i, it) {
                        //读取列配置,然后这里进行合并
                        var column_config;
                        try {
                            column_config = JSON.parse(it.column_config,function(k,v){
                                if(v && typeof v === 'string') {
                                    return v.indexOf('function') > -1 || v.indexOf('FUNCTION') > -1 ? new Function(`return ${v}`)() : v;
                                }
                                return v;
                            });
                        }catch (e) {
                            column_config = {};
                        }
                        var this_column_config = {field:it.table_field, title:it.title, width:it.width, sortable: it.sortable, formatter: get_function(it.table_field + '_for'), styler: get_function(it.table_field + '_styler')};
                        //合并一下
                        $.extend(this_column_config, column_config);

                        if(get_function(it.table_field + '_for') !== undefined) this_column_config.formatter = get_function(it.table_field + '_for');
                        if(get_function(it.table_field + '_styler') !== undefined) this_column_config.styler = get_function(it.table_field + '_styler');

                        table_columns[0].push(this_column_config);
                    });

                    //渲染表头
                    $('#tt').datagrid({
                        width:'auto',
                        height: $(window).height() - 26,
                        columns:table_columns,
                        rowStyler:function(index,row){
                            var str = '';
                            switch(row.status){
                                case 'cancel':
                                    return 'color:red;';
                                    break;
                                case 'normal':
                                    return 'color:blue;';
                                case 'error':
                                    return 'color:green;';
                                case 'pending':
                                    return 'color:orange;';
                                    break;
                            }
                        },
                        onError: function (index, data) {
                            $.messager.alert('error', data.msg);
                        },
                        onDblClickRow: function(index, row) {
                            window.open('/biz_consol/edit/'+row.id);
                        },
                        onSelect:function(index ,row){
                            get_statistics();
                        },
                        onUnselect:function(index ,row){
                            get_statistics();
                        },
                        onSelectAll:function(index ,row){
                            get_statistics();
                        },
                        onUnselectAll:function(index ,row){
                            get_statistics();
                        },
                        // onRowContextMenu: function(e, index, row){
                        //     e.preventDefault();
                        //     $(this).datagrid('selectRow', index);
                        //     $(this).datagrid('rowMenu').menu('show', {
                        //         left:e.pageX,
                        //         top:e.pageY
                        //     });
                        // },
                        // detail view,expandrows

                        view: detailview,
                        detailFormatter:function(index,row){
                            return '<div style="padding:2px"><table class="ddv"></table></div>';
                        },
                        onExpandRow: function(index,row){
                            var ddv = $(this).datagrid('getRowDetail',index).find('table.ddv');
                            ddv.datagrid({
                                url:'/biz_shipment/get_by_consol_data/' + row.id,
                                fitColumns:false,
                                singleSelect:true,
                                rownumbers:true,
                                loadMsg:'',
                                height:'auto',
                                columns:[[
                                    {field:'cus_no',title:'<?= lang('cus_no');?>',width:140,align:'left'},
                                    {field:'job_no',title:'<?= lang('job_no');?>',width:120,align:'left'},
                                    {field:'packs',title:'packs',width:60,align:'left'},
                                    {field:'weight',title:'weight',width:60,align:'left'},
                                    {field:'volume',title:'volume',width:60,align:'left'},
                                    {field:'good_outers',title:'<?= lang('good_outers');?>',width:60},
                                    {field:'good_weight',title:'<?= lang('good_weight');?>',width:60},
                                    {field:'good_volume',title:'<?= lang('good_volume');?>',width:60},
                                    {field:'goods_type',title:'<?= lang('goods_type');?>',width:80},
                                    {field:'hs_code',title:'<?= lang('hs_code');?>',width:80},
                                    {field:'trans_term',title:'<?= lang('trans_term');?>',width:80},
                                    {field:'shipper_company',title:'<?= lang('shipper_company');?>',width:200},
                                    {field:'consignee_company',title:'<?= lang('consignee_company');?>',width:200},
                                    {field:'notify_company',title:'<?= lang('notify_company');?>',width:200},
                                    {field:'description',title:'<?= lang('description');?>',width:180},
                                    {field:'hbl_type',title:'<?= lang('hbl_type');?>',width:60},
                                    {field:'sales',title:'<?= lang('sales');?>',width:60},
                                    {field:'operator',title:'<?= lang('operator');?>',width:60},
                                    {field:'customer_service',title:'<?= lang('customer_service');?>',width:60},
                                    {field:'hs_code',title:'<?= lang('HS code');?>',width:80},
                                ]],
                                onResize:function(){
                                    $('#tt').datagrid('fixDetailRowHeight',index);
                                },

                                onLoadSuccess:function(){
                                    setTimeout(function(){
                                        $('#tt').datagrid('fixDetailRowHeight',index);
                                    },0);
                                }
                            });
                            $('#tt').datagrid('fixDetailRowHeight',index);

                        }
                    });
                    $(window).resize(function () {
                        $('#tt').datagrid('resize');
                    });

                    //为了避免初始加载时, 重复加载的问题,这里进行了初始加载ajax数据
                    var aj = [];
                    $.each(res.data.load_url, function (i,it) {
                        aj.push(get_ajax_data(it, i));
                    });
                    //加载完毕触发下面的下拉框渲染
                    //$_GET 在other.js里封装
                    var this_url_get = {};
                    $.each($_GET, function (i, it) {
                        this_url_get[i] = it;
                    });
                    var auto_click = this_url_get.auto_click;

                    $.when.apply($,aj).done(function () {
                        //这里进行字段对应输入框的变动
                        $.each($('.f.easyui-combobox'), function(ec_index, ec_item){
                            var ec_value = $(ec_item).combobox('getValue');
                            var v_inp = $('.v:eq(' + ec_index + ')');
                            var s_val = $('.s:eq(' + ec_index + ')').combobox('getValue');
                            var v_val = v_inp.textbox('getValue');
                            //如果自动查看,初始值为空
                            if(auto_click === '1') v_val = '';

                            $(ec_item).combobox('clear').combobox('select', ec_value);
                            //这里比对get数组里的值
                            $.each(this_url_get, function (trg_index, trg_item) {
                                //切割后,第一个是字段,第二个是符号
                                var trg_index_arr = trg_index.split('-');
                                //没有第二个参数时,默认用等于
                                if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';
                                //用一个删一个
                                //如果当前的选择框的值,等于get的字段值
                                if(ec_value === trg_index_arr[0] && s_val === trg_index_arr[1]){
                                    v_val = trg_item;//将v改为当前get的
                                    delete this_url_get[trg_index];
                                }
                            });
                            //没找到就正常处理
                            $('.v:eq(' + ec_index + ')').textbox('setValue', v_val);
                        });
                        //判断是否有自动查询参数
                        var no_query_get = ['auto_click', 'view_type'];//这里存不需要塞入查询的一些特殊变量
                        $.each(no_query_get, function (i,it) {
                            delete this_url_get[it];
                        });
                        //完全没找到的剩余值,在这里新增一个框进行选择
                        $.each(this_url_get, function (trg_index, trg_item) {
                            add_tr();//新增一个查询框
                            var trg_index_arr = trg_index.split('-');
                            //没有第二个参数时,默认用等于
                            if(trg_index_arr[1] === undefined) trg_index_arr[1] = '=';

                            $('#cx table tbody tr:last-child .f').combobox('setValue', trg_index_arr[0]);
                            $('#cx table tbody tr:last-child .s').combobox('setValue', trg_index_arr[1]);
                            $('#cx table tbody tr:last-child .v').textbox('setValue', trg_item);
                        });

                        //这里自动执行查询,显示明细
                        if(auto_click === '1'){
                            doSearch();
                        }
                    });
                }else{
                    $.messager.alert("<?= lang('提示');?>", res.msg);
                }
            },error:function (e) {
                ajaxLoadEnd();
                $.messager.alert("<?= lang('提示');?>", e.responseText);
            }
        });
    }

    /**
     * 新增一个查询框
     */
    function add_tr() {
        $('#cx table tbody').append(add_tr_str);
        $.parser.parse($('#cx table tbody tr:last-child'));
        var last_f = $('#cx table tbody tr:last-child .f');
        var last_f_v = last_f.combobox('getValue');
        last_f.combobox('clear').combobox('select', last_f_v);
        return false;
    }

    /**
     * 删除一个查询
     */
    function del_tr(e) {
        //删除按钮
        var index = $(e).parents('tr').index();
        $(e).parents('tr').remove();
        return false;
    }

    //执行加载函数
    $(function () {
        load_query_box();
    });
</script>
<!--easyui 相关的一些不重要的函数-->
<script>

    /**
     * 根据行数据和字段直接添加到查询栏
     */
    function add_search_field_by_row(row, field) {
        var f_data = $('.f:eq(0)').combobox('getData');
        var f_data_fields = f_data.map(function(value,index) {
            return value['value'];
        });
        //如果是f_data里的,直接判断是否存在按钮,不存在触发且让最后一个选中,然后再赋予值
        if($.inArray(field, f_data_fields) !== -1){
            //存在选中该值的,直接填充
            var f = $('.f');
            var isset = false;
            $.each(f, function (index, item) {
                var this_value = $('.f:eq(' + index + ')').combobox('getValue');
                var this_v_value = $('.v:eq(' + index + ')').textbox('getValue');
                //如果相等,给该f填充值
                if(this_value == field){
                    $('.s:eq(' + index + ')').combobox('setValue', 'like');
                    if(this_v_value == '')$('.v:eq(' + index + ')').textbox('setValue', row[field]);
                    else $('.v:eq(' + index + ')').textbox('setValue', '');
                    isset = true;
                    return false;
                }
            });
            //如果没找到,新增一个
            if(!isset){
                $('.add_tr:eq(0)').trigger('click');
                $('.f:last').combobox('setValue', field);
                $('.s:last').combobox('setValue', 'like');
                $('.v:last').textbox('setValue', row[field]);
            }
        }else{
            // if(field == 'voyage') $('#voyage').textbox('setValue', row[field]);
        }
        doSearch();
        //不是f_data里的,进行特殊判断
    }

    function buildMenu(target){
        var state = $(target).data('datagrid');
        if (!state.rowMenu){
            state.rowMenu = $('<div></div>').appendTo('body');
            state.rowMenu.menu({
                onClick: function(item){
                    var row = $(target).datagrid('getSelected');
                    add_search_field_by_row(row, table_name + "." + item.name);
                }
            });
            //委托人、承运人、船名、航次
            var fields = ['creditor', 'trans_carrier', 'vessel', 'voyage'];
            for(var i=0; i<fields.length; i++){
                var field = fields[i];
                var col = $(target).datagrid('getColumnOption', field);
                if(col !== null){
                    state.rowMenu.menu('appendItem', {
                        text: col.title,
                        name: field,
                        iconCls: 'icon-ok'
                    });
                }else{
                    state.rowMenu.menu('appendItem', {
                        text: field,
                        name: field,
                        iconCls: 'icon-ok'
                    });
                }
            }
        }
        return state.rowMenu;
    }

    $.extend($.fn.datagrid.methods, {
        rowMenu: function(jq){
            return buildMenu(jq[0]);
        }
    });

    $.fn.pagination.defaults.displayMsg = '<span style="color: red;">总票数:{total}</span> 当前:{from}到{to} ';

    function getValue(jq) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combobox !== undefined){
            return $(jq).combobox('getValue');
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('getValue');
        }else if(data.datebox !== undefined){
            return $(jq).datebox('getValue');
        }else if(data.textbox !== undefined){
            return $(jq).textbox('getValue');
        }
    }

    function setValue(jq, val) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combobox !== undefined){
            return $(jq).combobox('setValue',val);
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('setValue',val);
        }else if(data.datebox !== undefined){
            return $(jq).datebox('setValue',val);
        }else if(data.textbox !== undefined){
            return $(jq).textbox('setValue',val);
        }
    }

    function get_statistics(){
        var select = $('#tt').datagrid('getSelections')
            ,good_outers = 0
            ,good_volume = 0
            ,good_weight = 0
            ,box_info_arr = {}
            ,teu = 0
            ,ex_LCL_good_volume = 0
            ,ex_LCL_good_weight = 0
            ,im_LCL_good_volume = 0
            ,im_LCL_good_weight = 0
            ,ex_AIR_good_volume = 0
            ,ex_AIR_good_weight = 0
            ,im_AIR_good_volume = 0
            ,im_AIR_good_weight = 0;
        $.each(select, function (index, item) {
            var this_good_outers = item.good_outers != null ? parseInt(item.good_outers) : 0;
            var this_good_volume = item.good_volume != null ? parseFloat(item.good_volume) : 0.00;
            var this_good_weight = item.good_weight != null ? parseFloat(item.good_weight) : 0.00;
            good_outers += this_good_outers;
            good_volume += this_good_volume;
            good_weight += this_good_weight;
            if(item.trans_mode == 'LCL'){
                if(item.biz_type == 'export'){
                    ex_LCL_good_weight += this_good_weight;
                    ex_LCL_good_volume += this_good_volume;
                }else if(item.biz_type == 'import'){
                    im_LCL_good_weight += this_good_weight;
                    im_LCL_good_volume += this_good_volume;
                }
            }else if(item.trans_mode == 'AIR'){
                if(item.biz_type == 'export'){
                    ex_AIR_good_weight += this_good_weight;
                    ex_AIR_good_volume += this_good_volume;
                }else if(item.biz_type == 'import'){
                    im_AIR_good_weight += this_good_weight;
                    im_AIR_good_volume += this_good_volume;
                }
            }

            //统计箱型箱量
            try{
                var box_info_json = JSON.parse(item.box_info);
            }catch (e) {
                var box_info_json = {};
            }
            $.each(box_info_json, function (index2, item2) {
                if(box_info_arr[item2.size] === undefined) box_info_arr[item2.size] = 0;
                box_info_arr[item2.size] += parseInt(item2.num);
            });
        });
        var box_info = [];
        $.each(box_info_arr, function (index, item) {
            box_info.push(index + '*' + item);
            if(index.substring(0,1) == '2'){
                teu += 1 * item;
            }else if(index.substring(0,1) == '4'){
                teu +=  2 * item;
            }
        });
        box_info = box_info.join(', ');
        $('#good_outers').textbox('setValue', good_outers);
        $('#good_volume').textbox('setValue', good_volume);
        $('#good_weight').textbox('setValue', good_weight);
        $('#box_info').textbox('setValue', box_info);
        $('#teu').text(teu);
        $('#ex_LCL_good_volume').text(ex_LCL_good_volume.toFixed(2));
        $('#ex_LCL_good_weight').text(ex_LCL_good_weight.toFixed(2));
        $('#im_LCL_good_volume').text(im_LCL_good_volume.toFixed(2));
        $('#im_LCL_good_weight').text(im_LCL_good_weight.toFixed(2));
        $('#ex_AIR_good_volume').text(ex_AIR_good_volume.toFixed(2));
        $('#ex_AIR_good_weight').text(ex_AIR_good_weight.toFixed(2));
        $('#im_AIR_good_volume').text(im_AIR_good_volume.toFixed(2));
        $('#im_AIR_good_weight').text(im_AIR_good_weight.toFixed(2));
    }

    //模板函数等--start
    function apply_shipment_list_select(index, row) {
        var form_data = {};
        form_data.shipment_id = row.id;
        form_data.is_approved = 0;
        form_data.apply_shipment_operator = row.operator_name;
        form_data.apply_shipment_sales = row.sales_name;
        form_data.apply_shipment_customer_service = row.customer_service_name;
        $('#apply_shipment_form').form('load', form_data);
    }

    function job_no_for(value, row, index) {
        var str = value;
        str += '<br /><span style="color:#808080;">' + row.goods_type + '</span>';
        return str;
    }

    function job_no_style(value, row, index) {
        var style = '';
        if(row.document_si != '0') style += 'background-color:#6699CC';
        if(row.lock_lv == '3') style += 'background-color:#666666';
        return style;
    }

    function trans_origin_for(value, row, index) {
        return row.trans_origin + '<br><span style="color:#808080">' + row.trans_origin_name + '</span>';
    }
    function trans_discharge_for(value, row, index) {
        return row.trans_discharge + '<br><span style="color:#808080">' + row.trans_discharge_name + '</span>';
    }
    function trans_destination_for(value, row, index) {
        return row.trans_destination + '<br><span style="color:#808080">' + row.trans_destination_name + '</span>';
    }
    //模板函数等--end
</script>
<script type="text/javascript">
    function doSearch(){
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        $('#tt').datagrid({
            url: '/biz_consol/get_data',
            destroyUrl: '/biz_consol/delete_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            },
            queryParams: json,
        }).datagrid('clearSelections');
        set_config();
		$('#chaxun').window('close');
    }

    var singleSelect = true;
    function multiple() {
        if(singleSelect){
            singleSelect = false;
        }else{
            singleSelect = true;
        }
        $('#tt').datagrid({singleSelect: singleSelect});
    }

    function chuanka1_excel(is_copy = 0) {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        var is_export = true;
        var end_each = false;
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        // GET方式示例，其它方式修改相关参数即可
        var form = $('<form></form>').attr('action','/excel/export_excel?file=chuanka1&is_copy=' + is_copy).attr('method','POST');
        if(is_copy){
            form.attr('target', '_blank');
        }

        $.each(json, function (index, item) {
            if(typeof item === 'string'){
                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
            }else{
                $.each(item, function(index2, item2){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                })
            }
        });

        form.appendTo('body').submit().remove();
    }

    function shenlingdan_word() {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        var is_export = true;
        var end_each = false;
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        //船名航次必须存在
        var vessel_key = $.inArray(table_name + '.vessel', json['field[f][]']);
        var voyage_key = $.inArray(table_name + '.voyage', json['field[f][]']);
        if(vessel_key === -1 && voyage_key === -1){
            $.messager.alert('Tips', '请查询船名航次');
            return;
        }

        //需要值
        if(json['field[v][]'][vessel_key] === '' && json['field[v][]'][voyage_key] === ''){
            $.messager.alert('Tips', '请输入船名航次的值');
            return;
        }
        // GET方式示例，其它方式修改相关参数即可
        var form = $('<form></form>').attr('action','/export/export_dword?file=shenlingdan&view_name=' + view_name).attr('method','POST');

        $.each(json, function (index, item) {
            if(typeof item === 'string'){
                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
            }else{
                $.each(item, function(index2, item2){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                })
            }
        });

        form.appendTo('body').submit().remove();
    }

    //设置船卡模板页面
    function config_chuanka() {
        window.open("/sys_config_title/chuanka_config");
    }

    //新版的自定义表头船卡导出
    //新版的自定义表头船卡导出
    function chuanka_excel(template_name= '') {
        var is_copy = 0;
        $.messager.confirm({
            title:'<?= lang('提示');?>',
            msg:'<?= lang('请选择导出模式?');?>',
            ok:'<?= lang('网页版');?>',
            cancel:'<?= lang('EXCEL');?>',
            fn:function (r) {
                if(r){
                    is_copy = 1;
                }
                var json = {};
                var cx_data = $('#cx').serializeArray();
                $.each(cx_data, function (index, item) {
                    //判断如果name存在,且为string类型
                    if(json.hasOwnProperty(item.name) === true){
                        if(typeof json[item.name] == 'string'){
                            json[item.name] =  json[item.name].split(',');
                            json[item.name].push(item.value);
                        }else{
                            json[item.name].push(item.value);
                        }
                    }else{
                        json[item.name] = item.value;
                    }
                });
                // GET方式示例，其它方式修改相关参数即可
                var form = $('<form></form>').attr('action','/excel/export_excel?file=chuanka_diy&template_name=' + template_name + '&view_name=' + view_name + '&is_copy=' + is_copy).attr('method','POST');
                if(is_copy){
                    form.attr('target', '_blank');
                }

                $.each(json, function (index, item) {
                    if(typeof item === 'string'){
                        form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                    }else{
                        $.each(item, function(index2, item2){
                            form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                        })
                    }
                });

                form.appendTo('body').submit().remove();
            }
        });
    }

    //加载船卡模板
    function load_chuanka() {
        ajaxLoading();
        $.ajax({
            type: 'GET',
            url: '/sys_config/get_chuanka_config',
            dataType:'json',
            success: function (res) {
                var excel_menu = $('#excel_menu');
                $.each(res, function (i, it) {
                    excel_menu.menu('appendItem', {
                        parent: excel_menu.menu('findItem', "船卡").target,  // 设置父菜单元素
                        text: it.template_name,
                        iconCls: 'icon-excel',
                        onclick: function(){
                            chuanka_excel(it.template_name);
                        }
                    });
                })
                ajaxLoadEnd();
            },error:function (e) {
                $.messager.alert('Tips', e.responseText);
            }
        });
    }

    function export_selected(){
        var param = {};
        var tt = $('#tt');
        param.filename = 'CONSOL表格导出.xls';
        param.rows = tt.datagrid('getSelections');


        if(param.rows.length == 0){
            param.worksheet = param.filename;
            param.rows = tt.datagrid('getRows');
        }else{
            param.worksheet = 'CONSOL';
        }

        tt.datagrid('toExcel', param);
    }

    var more_searchs = false;
    function more_search(){
        if(more_searchs){
            $('#cx tr:nth-child(-n+0)').css('display', 'none');
        }else{
            $('#cx tr:nth-child(-n+0)').css('display', 'table-row');
        }
        more_searchs = !more_searchs;
    }

    $(function(){
        $('#chaxun').window('open');
        $('#cx').on('keydown', 'input', function (e) {
            if(e.keyCode == 13){
                doSearch();
            }
        });

        //加载船卡模板进行导出使用
        load_chuanka();
    });

    function resetSearch(){
        $.each($('.v'), function(i,it){
            setValue($(it), '');
        });
    }

    function add(){
        window.open("/biz_consol/add?shipment_id=&page=export_FCL_SEA") ;
	}
	function copy(){
		var row = $('#tt').datagrid('getSelected');
		if (row){
			window.open("/biz_consol/add/" + row.id) ;
		}else{
            $.messager.alert("tips","No Item Selected");
        }
	}

	function del(){
		var row = $('#tt').datagrid('getSelected');
		if (row){
			//if(row.step!="未收款"){
			//	alert("不可以删除");
			//}else{
			//}
            //增加一个手动输入的delete验证
            $.messager.prompt('Tips', '确认删除?请输入delete进行确认', function (r) {
                if(r === 'delete'){
                    $('#tt').edatagrid('destroyRow');
                }
            });
		}else{
			alert("No Item Selected");
		}
	}

    var not_all = false;
    function reload_all_data(first_page) {
        var tt = $('#tt');
        // tt.datagrid('getO')
        var options = tt.datagrid('options');
        if(!not_all){
            tt.datagrid('gotoPage', {
                page: first_page,
                callback: function(page){
                    tt.datagrid('selectAll');
                    var row_data = tt.datagrid('getData');
                    var this_total = (page - 1) * options.pageSize + row_data.rows.length;
                    if(row_data.rows.length !== 0 && this_total < row_data.total){
                        reload_all_data(page + 1);
                    }
                }
            })
        }else{
            not_all = false;
        }
    }

    function check_all_statistics(){
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });

        $.ajax({
            type:'POST',
            url:'/biz_consol/get_data_statistics',
            data:cx_data,
            success:function(rec_json){
                var rec;
                try{
                    rec = $.parseJSON(rec_json);
                    if(rec.code == 0){
                        var data = rec.data;
                        $('#good_outers').textbox('setValue', data.good_outers);
                        $('#good_volume').textbox('setValue', data.good_volume);
                        $('#good_weight').textbox('setValue', data.good_weight);
                        $('#box_info').textbox('setValue', data.box_info);
                        $('#teu').text(data.teu);
                        $('#ex_LCL_good_volume').text(data.ex_LCL_good_volume);
                        $('#ex_LCL_good_weight').text(data.ex_LCL_good_weight);
                        $('#im_LCL_good_volume').text(data.im_LCL_good_volume);
                        $('#im_LCL_good_weight').text(data.im_LCL_good_weight);
                        $('#ex_AIR_good_volume').text(data.ex_AIR_good_volume);
                        $('#ex_AIR_good_weight').text(data.ex_AIR_good_weight);
                        $('#im_AIR_good_volume').text(data.im_AIR_good_volume);
                        $('#im_AIR_good_weight').text(data.im_AIR_good_weight);
                    }else{
                        $.messager.alert('Tips', rec.msg);
                    }
                }catch(e){
                    $.messager.alert('Tips', rec_json);
                }

            }
        });

    }

    function batch_lock(){
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            $.messager.confirm('确认对话框', '是否确认将选择的对应CONSOL锁住？', function(r){
                if (r){
                    // 退出操作;
                    ajaxLoading();
                    var consol_ids = [];
                    $.each(rows, function(index, item){
                        if($.inArray(item.id, consol_ids) == -1)consol_ids.push(item.id);
                    });
                    $.ajax({
                        type: 'POST',
                        url: '/biz_consol/batch_lock',
                        data: {
                            consol_ids: consol_ids.join(','),
                        },
                        success: function (res_json) {
                            var res;
                            try{
                                res = $.parseJSON(res_json);
                            }catch(e){

                            }
                            if(res == undefined){
                                $.messager.alert('Tips', '发生错误,请联系管理员');
                            }else{
                                $.messager.alert('Tips', res.msg);
                                if(res.code == 0){

                                }else{

                                }
                            }
                            ajaxLoadEnd();
                        }
                    });
                }
            });
        }else {
            $.messager.alert('<?= lang('Tips')?>', '<?= lang('No columns selected')?>');
        }
    }

    //审核shipment
    function apply_shipment() {
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            if(rows.length > 1){
                $.messager.alert('Tips', '一次只能申请一行');
                return;
            }
            //（船公司（船公司航线代码港口等筛选后，选中 批量修改） 航线代码港口等筛选后，选中 批量修改）
            var row = rows[0];
            if(row.is_apply_shipment == 0){
                $.messager.alert('Tips', '当前consol不存在已申请的shipment');
                return;
            }
            $('#read_consol').attr('sid', row.id);
            $('#apply_shipment_list').combogrid('clear').combogrid('grid').datagrid('reload', '/biz_shipment/get_apply_shipment/' + row.id);
            var form_data = {};
            form_data.id = row.id;
            $('#apply_shipment_form').form('clear').form('load', form_data);
            $('#apply_shipment_div').window('open');
        }else{
            $.messager.alert('<?= lang('Tips')?>','<?= lang('No columns selected')?>');
        }
    }

    function apply_shipment_save() {
        var update_data = $('#apply_shipment_form').serializeArray();
        $.messager.confirm({title:'确认对话框', width: 400, height: 200, msg:'是否确认提交？', fn: function (r) {
                if(r){
                    if(is_submit){
                        return;
                    }
                    is_submit = true;
                    ajaxLoading();
                    $('#apply_shipment_form').form('submit', {
                        url: '/biz_shipment/apply_shipment',
                        onSubmit: function (param) {
                            var validate = $(this).form('validate');
                            if(!validate){
                                ajaxLoadEnd();
                                is_submit = false;
                            }
                            return validate;
                        },
                        success: function (res_json) {
                            // $('#dlg').addClass('hide');
                            var res;
                            try{
                                res = $.parseJSON(res_json);
                            }catch(e){
                            }
                            if(res == undefined){
                                $.messager.alert('Tips', '发生错误');
                            }else{
                                if(res.code == 0){
                                    $('#tt').datagrid('reload').datagrid('clearSelections');
                                    $('#apply_shipment_div').dialog('close');
                                }
                                $.messager.alert('Tips', res.msg);
                            }
                            is_submit = false;
                            ajaxLoadEnd();
                        },
                    });
                }
            }});

    }
</script>

<table id="tt" rownumbers="false" pagination="true"  idField="id" pagesize="30" toolbar="#tb"  singleSelect="true" nowrap="false">
</table>

   <div id="tb" style="padding:3px;">
        <table>
		<tr>
        <td>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add();"><?= lang('add');?></a>
            <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="copy();"><?= lang('copy');?></a>
			<?php echo $hasDelete ? '<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="del();">' . lang('delete') . '</a>' : ''; ?>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>
			<a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#mm2',plain:true" iconCls="icon-reload"><?= lang('config');?></a>
            <a href="javascript:void(0)" class="easyui-menubutton" data-options="menu:'#excel_menu',plain:true" iconCls="icon-reload"><?= lang('export');?></a>
			<?php if(menu_role('consol_batch_lock3')){ ?>
			<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-lock" plain="true" onclick="batch_lock();" title="carrier_cost锁"><?= lang('consol_lock3');?></a>
			<?php } ?>
            <!--<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="apply_shipment()"><?= lang('仓位申请审核');?></a>-->
            <a href="javascript:void;" class="easyui-tooltip tooltip-f" data-options="
                    content: $('<div></div>'),
                    onShow: function(){
                        $(this).tooltip('arrow').css('left', 20);
                        $(this).tooltip('tip').css('left', $(this).offset().left);
                    },
                    onUpdate: function(cc){
                        cc.panel({
                            width: 300,
                            height: 'auto',
                            border: false,
                            href: '/bsc_help_content/help/consol_index'
                        });
                    }
                " title=""><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
        </td>
        </tr>
            <tr>
                <td>
                    <div class="statistics">
                        <?= lang('box_info');?><input class="easyui-textbox" id="box_info" readonly style="width: 300px;" value="">
                        <?= lang('件数');?><input class="easyui-textbox" id="good_outers" readonly style="width: 50px;" value="0">
                        <?= lang('毛重');?><input class="easyui-textbox" id="good_weight" readonly style="width: 50px;" value="0.00">
                        <?= lang('体积');?><input class="easyui-textbox" id="good_volume" readonly style="width: 50px;" value="0.00">
                        <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="javascript:reload_all_data(1);"><?= lang('全选');?></a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="javascript:check_all_statistics();"><?= lang('快速统计');?></a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="javascript:$('#tt').datagrid('clearSelections');not_all=true"><?= lang('全不选');?></a>
                        <?= lang('总数');?>:
                        <span class="all_t">teu</span>:
                        <span id="teu">0</span>
                        <span class="all_t">ex-LCL</span>:
                        <span id="ex_LCL_good_weight">0</span>KGS/<span id="ex_LCL_good_volume">0</span>CBM
                        <span class="all_t">im-LCL</span>:
                        <span id="im_LCL_good_weight">0</span>KGS/<span id="im_LCL_good_volume">0</span>CBM
                        <span class="all_t">ex-AIR</span>:
                        <span id="ex_AIR_good_weight">0</span>KGS/<span id="ex_AIR_good_volume">0</span>CBM
                        <span class="all_t">im-AIR</span>:
                        <span id="im_AIR_good_weight">0</span>KGS/<span id="im_AIR_good_volume">0</span>CBM
                    </div>
                </td>
            </tr>
		</table>
   </div>
    <div id="excel_menu" style="width:150px;">
        <div data-options="iconCls:'icon-ok'" onclick="config_chuanka()">
            <span><?= lang('船卡');?></span>
            <div class="chuanka_list" style="width:150px;">
                <div onclick="config_chuanka()" iconCls="icon-setting"><b><?= lang('船卡配置');?></b></div>
            </div>
        </div>
<!--        <div data-options="iconCls:'icon-ok'" onclick="shenlingdan_word()">--><?//= lang('申领单');?><!--</div>-->
        <div data-options="iconCls:'icon-ok'" onclick="export_selected()"><?= lang('表格导出');?></div>
    </div>




    <div id="mm2" style="width:150px;">
		<div data-options="iconCls:'icon-ok'" onclick="javascript:config()"><?= lang('column');?></div>
	    <div data-options="iconCls:'icon-ok'" onclick="javascript:multiple()"><?= lang('multiple');?></div>
	</div>

	<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:600px;height:450px;padding:5px;">
		<br><br>
        <form id="cx">
            <table>

                <tr>
                    <td>
                        <?= lang('freight_affirmed');?>
                    </td>
                    <td align="right">
                        <select readonly name="freight_affirmed[f]" class="easyui-combobox"  required="true" style="width:115px;">
                            <option value="freight_affirmed"><?= lang('freight_affirmed');?></option>
                        </select>
                        &nbsp;

                        <select name="freight_affirmed[s]" class="easyui-combobox"  required="true" style="width:100px;">
                            <option value="=">=</option>
                            <option value=">=">>=</option>
                            <option value="<="><=</option>
                        </select>
                        &nbsp;
                        <input name="freight_affirmed[v]" class="easyui-datebox"  style="width:155px;">
                    </td>
                </tr>
                <tr>
                    <td><?= lang('container_no');?></td>
                    <td align="right">
                        <input name="container_no" class="easyui-textbox"  style="width:245px;">
                        <select class="easyui-combobox" name="goods_type" data-options="
                            editable:false,
                            valueField:'value',
                            textField:'valuename',
                            url:'/bsc_dict/get_option/goods_type',
                        " style="width:150px;">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><?= lang('shipment no');?></td>
                    <td align="right">
                        <input name="shipment_no" class="easyui-textbox"  style="width:395px;">

                    </td>
                </tr>
                <!--<tr>-->
                <!--    <td><?= lang('海运费');?></td>-->
                <!--    <td align="right">-->
                <!--        <select name="is_hyf" class="easyui-combobox" style="width:395px;">-->
                <!--            <option value="0">-</option>-->
                <!--            <option value="1">否</option>-->
                <!--            <option value="2">是</option>-->
                <!--        </select>-->
                <!--    </td>-->
                <!--</tr>-->
                <tr>
                    <td><?= lang('lock_lv');?></td>
                    <td align="left">
                        <input name="lock_lv[]" type="checkbox" value="0"><?= lang('未锁');?>
                        <input name="lock_lv[]" type="checkbox" value="3"><?= lang('C3锁');?>
                    </td>
                </tr>
                <!--<tr>-->
                <!--    <td><?= lang('仓位申请');?></td>-->
                <!--    <td align="left">-->
                <!--        <input type="radio" name="is_apply_shipment" value="0" checked>否-->
                <!--        <input type="radio" name="is_apply_shipment" value="1">是-->
                <!--    </td>-->
                <!--</tr>-->
            </table>
            <br><br>
            <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="doSearch();"><?= lang('search'); ?></button>
            <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="resetSearch();">重置</button>

            <a href="javascript:void;" class="easyui-tooltip" data-options="
                content: $('<div></div>'),
                onShow: function(){
                    $(this).tooltip('arrow').css('left', 20);
                    $(this).tooltip('tip').css('left', $(this).offset().left);
                },
                onUpdate: function(cc){
                    cc.panel({
                        width: 500,
                        height: 'auto',
                        border: false,
                        href: '/bsc_help_content/help/query_condition'
                    });
                }
            "><?php //echo lang('help');?></a>
            <!--<a href="javascript:void(0)" id="more_field" onclick="javascript:more_search();"> more</a>-->
        </form>
	</div>

<div id="apply_shipment_div" class="easyui-window" title="审核SHIPMENT" style="padding:5px;" data-options="width:500,closed:true,modal:true,border:'thin'">
    <form id="apply_shipment_form" method="post">
        <input type="hidden" name="id">
        <input type="hidden" name="shipment_id">
        <table>
            <tr>
                <td colspan="99">
                    <input type="radio" name="is_approved" value="1" checked> 通过
                    <input type="radio" name="is_approved" value="0"> 拒绝
                    <br/>
                    <span style="color:red;">注意！！！,现在一个仓位可供多个shipment申请, 必须选择一个shipment进行审核,当通过或拒绝时,其他所有shipment都会做拒绝处理</span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <a href="javascript:void(0);" onclick="read_consol()" sid="" id="read_consol">查看consol</a>
                </td>
                <td>
                    <select class="easyui-combogrid" showPanel id="apply_shipment_list" style="width: 130px;" data-options="panelWidth: '600px',
                        panelHeight: '300px',
                        panelWidth: '700px',
                        idField: 'id',              //ID字段
                        textField: 'job_no',    //显示的字段
                        fitColumns : true,
                        striped: true,
                        editable: false,
                        pagination: false,           //是否分页
                        toolbar : '#apply_shipment_list_div',
                        rownumbers: true,           //序号
                        collapsible: true,         //是否可折叠的
                        method: 'post',
                        mode: 'remote',
                        columns:[[
                            {field:'job_no',title:'<?= lang('SHIPMENT');?>',width:120},
                            {field:'operator_name',title:'<?= lang('operator');?>',width:60},
                            {field:'sales_name',title:'<?= lang('operator');?>',width:60},
                            {field:'customer_service_name',title:'<?= lang('customer_service');?>',width:60},
                            {field:'good_commodity',title:'<?= lang('品名');?>',width:60},
                            {field:'good_weight',title:'<?= lang('货重');?>',width:60},
                            {field:'goods_type',title:'<?= lang('货物类型');?>',width:60},
                            {field:'remark',title:'<?= lang('备注');?>',width:60},
                            {field:'box_info',title:'<?= lang('箱型箱量');?>',width:60},
                        ]],
                        onLoadSuccess:function(res){
                            $('#apply_shipment_list').combo('showPanel');
                            var rows = res.rows;
                            //如果只有1条,那么自动选择
                            if(rows.length == 1){
                                $('#apply_shipment_list').combogrid('grid').datagrid('selectRecordField', {field:'id', value:rows[0]['id']});
                            }
                        },
                        emptyMsg : '未找到相应数据!',
                        onSelect:apply_shipment_list_select,
                    "></select>
                </td>
                <td>
                    <input class="easyui-textbox" disabled name="apply_shipment_operator" style="width:80px;">
                </td>
                <td>
                    <input class="easyui-textbox" disabled name="apply_shipment_sales" style="width:80px;">
                </td>
                <td>
                    <input class="easyui-textbox" disabled name="apply_shipment_customer_service" style="width:80px;">
                </td>
            </tr>
        </table>
        <div>
            <button type="button" onclick="javascript:apply_shipment_save();" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px"><?= lang('submit');?></button>
            <button type="button" onclick="javascript:$('#apply_shipment_div').dialog('close')" class="easyui-linkbutton" iconCls="icon-cancel" style="width:90px"><?= lang('cancel');?></button>
        </div>
    </form>
</div>
