<?php

class biz_client_duty extends Menu_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->model = Model('biz_client_duty_model');
    }

    //维护客户角色权限的页面
    public function index(){
        $data = array();
        $data['client_code'] = getValue('client_code', '');
        $data['client_role'] = getValue('client_role', '');
        $data['is_config_sales_op'] = getValue('is_config_sales_op', 'true');
        $data['crm_id'] = getValue('crm_id', 0);

        $this->load->view("head");
        $this->load->view("/biz/client/duty/index_view", $data);
    }

    /**
     * 新增当前用户的一条用户权限
     */
    public function add_data(){
        $client_code = postValue('client_code', '');
        $client_role = postValue('client_role', '');
        $user_role = postValue('user_role', '');
        $user_id = postValue('user_id', 0);

        if(empty($client_code) || empty($client_role) || empty($user_id)) return jsonEcho(array('code' => 1, 'msg' => '请选择人员'));

//        $client = Model("biz_client_model")->get_where_one("client_code = '{$client_code}'");
//        if(empty($client)){
//            //再查一下apply和crm
//            if($client_code[0] == 'A'){//apply申请
//                $client = Model('biz_client_apply_model')->get_where_one("temp_client_code = '{$client_code}'");
//            }
//
//            if(empty($client)) return jsonEcho(array('code' => 1, 'msg' => '客户不存在'));
//        }

        //已存在不再新增
        $isset_row = $this->model->get_where_one("client_code = '{$client_code}' and client_role = '{$client_role}' and user_role = '{$user_role}' and user_id = '{$user_id}'");
        if(!empty($isset_row)) return jsonEcho(array('code' => 1, 'msg' => '该用户已存在, 请勿重复添加!!'));

        $data = array();
        $data['client_code'] = $client_code;
        $data['client_role'] = $client_role;
        $data['user_role'] = $user_role;
        $data['user_id'] = $user_id;
        $user_group = getUserField($user_id, 'group');
        if($user_id == '-1') $user_group = '-1';
        $data['user_group'] = $user_group;

        $id = $this->model->save($data);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_client_duty";
        $log_data["key"] = $id;
        $log_data["master_table_name"] = 'biz_client';
        $log_data["master_key"] = $client_code;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);

        return jsonEcho(array('code' => 0, 'msg' => '新增成功'));
    }

    /**
     * 删除当前客户的指定用户权限
     */
    public function delete_data(){
        $client_code = postValue('client_code', '');
        $client_role = postValue('client_role', '');
        $user_role = postValue('user_role', '');
        $user_id = postValue('user_id', 0);

        if(empty($client_code) || empty($client_role) || empty($user_id)) return jsonEcho(array('code' => 1, 'msg' => '参数错误'));

        $old_row = $this->model->get_where_one("client_code = '{$client_code}' and client_role = '{$client_role}' and user_role = '{$user_role}' and user_id = '{$user_id}'");
        if(empty($old_row)) return jsonEcho(array('code' => 0, 'msg' => '删除成功'));

//        $client = Model("biz_client_model")->get_where_one("client_code = '{$client_code}'");
//        if(empty($client)){
//            //再查一下apply和crm
//            if($client_code[0] == 'A'){//apply申请
//                $client = Model('biz_client_apply_model')->get_where_one("temp_client_code = '{$client_code}'");
//            }
//
//            if(empty($client)) return jsonEcho(array('code' => 1, 'msg' => '客户不存在'));
//        }

        $this->model->mdelete($old_row['id']);

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_client_duty";
        $log_data["key"] = $old_row['id'];
        $log_data["master_table_name"] = 'biz_client';
        $log_data["master_key"] = $client_code;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);

        return jsonEcho(array('code' => 0, 'msg' => '删除成功'));
    }

    //获取客户的权限数据
    public function get_client_duty(){
        $client_code = getValue('client_code', '');
        $client_role = getValue('client_role', '');

        $this->db->select("biz_client_duty.*, (select name_en from bsc_user where bsc_user.id = biz_client_duty.user_id) as user_name");
        $rs = $this->model->get("client_code = '{$client_code}' and client_role = '{$client_role}'");
        
        //根据原本的规则获取对应的数据
//        $user_role = array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'sales');
        $client_role_config = $this->model->client_role_config;

        $rows = array();
        if(!isset($client_role_config[$client_role])) return jsonEcho(array('code' => 1, 'msg' => '暂不支持获取当前角色配置'));
        $this_config = $client_role_config[$client_role];
        if(empty($this_config[0]) && !is_admin()) return jsonEcho(array('code' => 1, 'msg' => '当前角色' . $client_role . '不需要设置相关人员'));
        //原逻辑,
        //管理员可看到和编辑全部
        $userEditField = array('operator', 'customer_service','marketing', 'oversea_cus', 'booking', 'document', 'sales', 'finance');
        //海外版配置权限关闭
        // $userEditField = array('');

        $sales_ids = filter_unique_array(array_map(function ($r){
            if($r['user_role'] == 'sales') return $r['user_id'];
            return '';
        }, $rs));

        //管理员全开
        //由于加入了权限,这里改为, 如果找不到客户信息,这里就全开放出来
        $client = Model('biz_client_model')->get_where_one("client_code = '{$client_code}'");
        if(!is_admin() && !empty($client)){
            //销售不能配置销售
            if(in_array(get_session('id'), $sales_ids)){
                //当前销售可看到当前的销售, 可配置其他所有角色
                $userEditField = array_diff($userEditField, array('sales'));
            }else{
                //其他人只能看到销售,其他角色都看不到
                //不是销售,全部都只能看
                $userEditField = array();
            }
        }

        foreach ($this_config[0] as $val){
            //首先撑一个壳
            if(!isset($rows[$val])) $rows[$val] = array("user_role" => $val, 'user_role_lang' => lang($val), 'is_edit' => in_array($val, $userEditField), 'data' => array());
        }

        $all_checked = false;
        //往壳里填入对应的有权限的用户数据
        foreach ($rs as $row){
            //如果是全开放的,这里跳过数据
            if($row['user_id'] === '-1') {
                $all_checked = true;
                continue;
            }

            !isset($rows[$row['user_role']]) && $rows[$row['user_role']] = array("user_role" => $row['user_role'], 'user_role_lang' => lang($row['user_role']), 'data' => array());

            $rows[$row['user_role']]['data'][] = $row;
        }
        return jsonEcho(array('code' => 0, 'msg' => 'success', 'data' => $rows, 'all_checked' => $all_checked));
    }
}