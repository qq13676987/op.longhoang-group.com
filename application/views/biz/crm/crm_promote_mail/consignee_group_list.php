
<script type="text/javascript" src="/inc/layer3.1.1/layer.js"></script>
<script type="text/javascript">
    function doSearch(){
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        $('#tt').datagrid('load', json);
    }
    $(function(){
        $('#cx').on('keydown', 'input', function (e) {
            if(e.keyCode == 13){
                doSearch();
            }
        });
    });
    function delConsig(){
        var row = $('#tt').datagrid('getSelected');
        if (row != null){
            layer.confirm('<?= lang('您确定要删除？');?>', {
                btn: ['<?= lang('确定');?>','<?= lang('取消');?>'] //按钮
            }, function(){
                $.getJSON("/crm_promote_mail/delete_consignee_group_data?id="+row.id,function(data){
                    if(data.code==200){
                        layer.msg(data.msg);
                        $('#tt').datagrid('reload');
                    }
                });
            });
        }else{
            $.messager.alert("<?= lang('提示');?>","<?= lang('No Item Selected');?>");
        }
    }
</script>
<div align="center"><h1><?= lang('1、选择或新建收件人组进行群发');?></h1></div>

<table class="easyui-datagrid" id="tt" title="<?= lang('收件人组');?>"
	   data-options="singleSelect:true,url:'/crm_promote_mail/consignee_group_list_get_data',method:'post',pageSize:30" pagination="true" toolbar="#toolbar"  >
	<thead>
	<tr>
		<th data-options="field:'id',width:110"><?= lang('ID');?></th>
		<th data-options="field:'group_name',width:444"><?= lang('收件人组名');?></th>
		<th data-options="field:'num',width:100"><?= lang('email数量');?></th>
		<th data-options="field:'create_by_name',width:111"><?= lang('创建人');?></th>
		<th data-options="field:'create_time',width:222"><?= lang('创建时间');?></th>
		<th data-options="field:'operate',width:222"><?= lang('查看收件人');?></th>

	</tr>
	</thead>
</table>
<div id="toolbar">

	<div>
		<?php //if(menu_role("crm_email_push_group_add")){?>
		<a href="/crm_promote_mail/add_consignees" class="easyui-linkbutton" iconCls="icon-add" plain="true"><?= lang('新增组');?></a>
		<?php //}?>
		<a href="javascript:delConsig();" class="easyui-linkbutton" iconCls="icon-remove" plain="true"><?= lang('删除');?></a>
		<form id='cx' method="post" style="display: inline-block;">
			<input type="text" name="group_name" class="easyui-textbox">
			<a href="javascript:void(0);" class="easyui-linkbutton input" plain="true" onclick="doSearch()"><?= lang('查询');?></a>
		</form>
	</div>

</div>
