<?php


class City extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('city_model');
        $this->model = $this->city_model;
    }
    
    /**
     * 获取国家
     */
    public function get_country (){
        $where = array('type' => 0, 'pid' => 0);
        $data = $this->model->get_option($where);
        
        $rows = array();
        $empty_row = array('id' => 0, 'pid' => 0, 'cityname' => '', 'cityname_en' => '', 'namecode' => '', 'namecode_en' => '', 'code' => '', 'type' => 0);
        $rows[] = $empty_row;
        foreach ($data as $row){
            $row['namecode'] = $row['cityname'] . "({$row['code']})";
            $row['namecode_en'] = $row['cityname_en'] . "({$row['code']})";
            $rows[] = $row;
        }
        
        echo json_encode($rows);
    }

    /**
     * 获取省级
     */
    public function get_province ($country = '中国', $field = "cityname"){
        $country = urldecode($country);
        $country = isset($_GET['country']) ? $_GET['country'] : $country;
        $this->db->select('id');
        $row = $this->model->get_where_one("$field = '$country'");
        
        $where = array('type' => 1, 'pid' => $row['id']);
        $data = $this->model->get_option($where);
        $rows = array();
        $empty_row = array('id' => 0, 'pid' => $row['id'], 'cityname' => '', 'cityname_en' => '', 'code' => '', 'type' => 1);
        $rows[] = $empty_row;
        
        foreach ($data as $row){
            $rows[] = $row;
        }
        
        echo json_encode($rows);
    }

    /**
     * 获取市级
     * @param int $province_id
     */
    public function get_city($province_id = 0){
        if($province_id != 0){
            $where = array('type' => 2, 'pid' => $province_id);
            $data = $this->model->get_option($where);
            echo json_encode($data);
        }

    }

    /**
     * 获取区级
     * @param int $city_id
     */
    public function get_area($city_id = 0){
        if($city_id != 0) {
            $where = array('type' => 3, 'pid' => $city_id);
            $data = $this->model->get_option($where);
            echo json_encode($data);
        }
    }
}