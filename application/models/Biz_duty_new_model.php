<?php

class biz_duty_new_model extends CI_Model
{
    private $table = 'biz_duty_new';
    public $table_new = 'biz_duty_new';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_one($f = "", $v = "")
    {
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_where_one($where='', $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function no_role_get($where="" ,$sort="id",$order="desc"){
        if($where !="")	$this->db->where($where);
        $this->db->order_by($sort,$order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function save($data = array())
    {
        $this->save_new($data);

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = $data['biz_table'];
        $log_data["key"] = $data['id_no'];
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data, 0);
        return true;
    }

    /**
     * �±�Ļ�ȡ����
     */
    public function get_new($where = "", $sort = "id", $order = "desc"){
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table_new);
        return $query->result_array();
    }

    /**
     * ������duty�������,�����ȡ�ָ����ʽ
     */
    public function save_new($data = array()){
        //��������ʱ�� ��������_duty_new�Ĵ���
        $duty_datas = array();
        $duty_data = array(
            'id_type' => $data['biz_table'],
            'id_no' => $data['id_no'],
        );
        foreach ($data as $key => $val){
            $this_duty = $duty_data;
            $key_arr = explode('_', $key);
            //ֻ����2�ֲ��и�
            $last_key = array_pop($key_arr);

            if($last_key === 'id' || $last_key === 'group'){

                //����������������Ϊ��customer_service�Ĵ���

                $this_duty['user_role'] = $user_role = join('_', $key_arr);
                if($last_key === 'id') $this_duty['user_id'] = $val;
                if($last_key === 'group') $this_duty['user_group'] = $val;

                //�����ڴ����µ�, ���ںϲ�һ��
                if(!isset($duty_datas[$user_role])) $duty_datas[$user_role] = $this_duty;
                else $duty_datas[$user_role] = array_merge($duty_datas[$user_role], $this_duty);
            }
        }
        //��������ܵó�ȫ���� ������

        //���Ȳ�ѯȫ�����ݽ��бȽ�
        $duty_new_rows = array_column($this->get_new("id_type = '{$data['biz_table']}' and id_no = '{$data['id_no']}'"), null, 'user_role');

        //�� user_role Ϊ����
        $table = $this->table_new;
//        $table = $this->table;

        foreach ($duty_datas as $duty_data){
            //�����ȫ������Ҫ���������
            //���ȿ���duty���Ƿ���
            if(isset($duty_new_rows[$duty_data['user_role']])){
                //�����������,��ô ȡID�����޸�
                $this->db->where('id', $duty_new_rows[$duty_data['user_role']]['id']);
                $this->db->update($table, $duty_data);
            }else{
                //���û������,��ô ����
                $this->db->insert($table, $duty_data);
                $this->db->insert_id();
            }
        }
    }

    public function update($id = '', $data = array())
    {
//        $this->db->where('id', $id);
//        $this->db->update($this->table, $data);

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_duty_new";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data, 0);
        return $id;
    }

    public function update_by_idno($id = '', $data = array())
    {
//        $old_row = $this->get_where_one("id_no = $id and biz_table = '{$data['biz_table']}'");
//
//        $this->db->where('id_no', $id);
//        $this->db->where('biz_table', $data['biz_table']);
//        $this->db->update($this->table, $data);
//
//
        $data['id_no'] = $id;
        $this->save_new($data);

        //save the operation log
        if(!empty($old_row)){
            $biz_table = $data['biz_table'];
            foreach ($data as $key => $val){
                if(isset($old_row[$key]) && $val == $old_row[$key]){
                    unset($data[$key]);
                }
            }
            if(!empty($data)){
                $log_data = array();
                $log_data["table_name"] = $biz_table;
                $log_data["key"] = $id;
                $log_data["action"] = "duty_update";
                $log_data["value"] = json_encode($data);
                log_rcd($log_data, 0);
            }

        }


        return $id;
    }

    public function mdelete($id_no = '',$id_type = '')
    {
        // delete container
        $this->db->where('id_no', $id_no);
        $this->db->where('id_type', $id_type);
        $this->db->delete($this->table);

        $old_row = $this->get_old_by_new($id_type, $id_no);
        if(!empty($old_row)){
            //save the operation log
            $log_data = array();
            $log_data["table_name"] = $id_type;
            $log_data["key"] = $id_no;
            $log_data["action"] = "delete";
            $log_data["value"] = json_encode($old_row);
            log_rcd($log_data, 0);
        }
    }


    /**
     * ��ȡ�°������ ͬʱת��Ϊ�ɰ�ĸ�ʽ
     * @param $id_type
     * @param $id_no
     */
    public function get_old_by_new($id_type,$id_no){
        $duty_new = $this->get_new("id_type = '{$id_type}' and id_no = '{$id_no}'");
        $data = array();
        foreach ($duty_new as $row){
            $data[$row['user_role'] . '_id'] = $row['user_id'];
            $data[$row['user_role'] . '_group'] = $row['user_group'];
        }
        return $data;
    }
}
