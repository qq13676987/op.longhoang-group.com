<style type="text/css">
    .f{
        width:115px;
    }
    .s{
        width:100px;
    }
    .v{
        width:120px;
    }
    .del_tr{
        width: 23.8px;
    }
    .add_tr{
        width: 23.8px;
    }
    .this_v{
        display: inline;
    }
    #cx tr:nth-child(-n+4){
        display: none;
    }
    .all_t{
        color: blue;
        font-weight: 800;
    }
</style>
<!--easyui其他事件-->
<script type="text/javascript">
    function doSearch(){
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] =  json[item.name].split(',');
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });

        $('#tt').datagrid({
            queryParams: json
        }).datagrid('clearSelections');
        set_config();
        $('#chaxun').window('close');
    }

    function set_config() {
        var f_input = $('.f');
        var count = f_input.length;
        var config = [];
        $.each(f_input, function (index, item) {
            var c_array = [];
            c_array.push($('.f:eq(' + index + ')').combobox('getValue'));
            c_array.push($('.s:eq(' + index + ')').combobox('getValue'));
            config.push(c_array);
        });
        // var config_json = JSON.stringify(config);
        $.ajax({
            type: 'POST',
            url: '/sys_config/save_config_search',
            data:{
                table: 'biz_tag',
                count: count,
                config: config,
            },
            dataType:'json',
            success:function (res) {

            },
        });
    }

    $(function () {
        $('#tt').edatagrid({
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            }
        });
        $('#tt').datagrid({
            url: '/biz_tag/get_data',
            width: 'auto',
            height: $(window).height(),
            onDblClickRow: function (index, row) {
                edit(row);
            },
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            },
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });

</script>
<!--各种点击事件,提交事件-->
<script>
    function approve() {
        var row = $('#tt').datagrid('getSelected');
        if (row){
            $.messager.confirm('确认对话框', '是否确认审核通过？', function(r) {
                if (r) {
                    ajaxLoading();
                    $.ajax({
                        type: 'POST',
                        url: '/biz_tag/approve_tag',
                        data: {id: row.id},
                        success: function (res_json) {
                            ajaxLoadEnd();
                            try {
                                var res = $.parseJSON(res_json);
                                $.messager.alert('<?= lang('提示');?>', res.msg);
                                if (res.code == 0) {
                                    $('#tt').datagrid('reload');
                                } else {

                                }
                            } catch (e) {
                                $.messager.alert('<?= lang('提示');?>', res_json);
                            }
                        },
                        error: function (e) {
                            ajaxLoadEnd();
                            $.messager.alert('<?= lang('提示');?>', e.responseText);
                        }
                    });
                }
            });
        }else{
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择任意一行后再试');?>');
        }
    }

    var is_submit = false;

    function submit_start() {
        is_submit = true;
        ajaxLoading();
    }

    function submit_end() {
        is_submit = false;
        ajaxLoadEnd();
    }

    /**
     * 新增
     */
    function add(){
        $('#add_div').window('open');
    }

    /**
     * 新增保存事件
     */
    function add_save(){
        if(is_submit){
            return;
        }
        $.messager.confirm('确认对话框', '是否确认提交？', function(r) {
            if (r) {
                submit_start();
                var this_window = $('#add_div');
                var form = $('#add_fm');
                form.form('submit', {
                    url: '/biz_tag/add_data',
                    onSubmit: function (param) {
                        var validate = form.form('validate');
                        param.client_code = $('#client_code').val();
                        if (!validate) {
                            submit_end();
                        }
                        return validate;
                    },
                    success: function (res_json) {
                        submit_end();
                        try {
                            var res = $.parseJSON(res_json);
                            $.messager.alert('<?= lang('提示')?>', res.msg);
                            if(res.code == 0){
                                this_window.window('close');
                                $('#tt').datagrid('reload').datagrid('clearSelections');
                            }else{
                            }
                        } catch (e) {
                            $.messager.alert('Tips', res_json);
                        }
                    }
                });
            }
        });
    }

    /**
     * 编辑
     */
    function edit(row){
        if(row.id > 0){
            $('#edit_div').window('open');
            $('#edit_fm').form('load', row);
        }
    }

    /**
     * 新增保存事件
     */
    function edit_save(){
        if(is_submit){
            return;
        }
        $.messager.confirm('确认对话框', '是否确认提交？', function(r) {
            if (r) {
                submit_start();
                var this_window = $('#edit_div');
                var form = $('#edit_fm');
                form.form('submit', {
                    url: '/biz_tag/update_data',
                    onSubmit: function (param) {
                        var validate = form.form('validate');
                        if (!validate) {
                            submit_end();
                        }
                        return validate;
                    },
                    success: function (res_json) {
                        this_window.window('close');
                        submit_end();
                        try {
                            var res = $.parseJSON(res_json);
                            $.messager.alert('<?= lang('提示')?>', res.msg);
                            if(res.code == 0){
                                this_window.window('close');
                                $('#tt').datagrid('reload').datagrid('clearSelections');
                            }else{
                            }
                        } catch (e) {
                            $.messager.alert('Tips', res_json);
                        }
                    }
                });
            }
        });
    }
</script>
<!--easyi模板相关的函数-->
<script>
    /**
     * 查询 选中字段后改变输入框的事件
     */
    function select_f_change(newVal, oldVal) {
        var index = $('.f').index(this);
        var v_jq = $('.v:eq(' + index + ')');
        if(setting.hasOwnProperty(newVal) === true){
            v_jq.textbox('destroy');
            $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
            $.parser.parse(v_jq.parents('.this_v'));
            var valueField = setting[newVal][1];
            var textField = setting[newVal][2];
            var v_config = {
                data:getAjaxData(setting[newVal][0], setting[newVal][4], $('.v:eq(' + index + ')')),
                valueField:valueField,
                textField:textField,
            };
            if(setting[newVal][3] === 'remote'){
                v_config.data = [{}];
                v_config.data[0][valueField] = '';
                v_config.data[0][textField] = '-';
                v_config.mode = setting[newVal][3];
                v_config.url = setting[newVal][4];
                v_config.onBeforeLoad = function(param){
                    if($(this).combobox('getValue') == '')return false;
                    if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                };
            }
            $('.v:eq(' + index + ')').combobox(v_config);
        }else if ($.inArray(newVal, datebox) !== -1){
            v_jq.textbox('destroy');
            $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datebox\' >');
            $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
        }else if ($.inArray(newVal, datetimebox) !== -1){
            v_jq.textbox('destroy');
            $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-datetimebox\' >');
            $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
        }else{
            v_jq.textbox('destroy');
            $(this).siblings('.this_v').append('<input name=\'field[v][]\' class=\'v easyui-textbox\' >');
            $.parser.parse($('.v:eq(' + index + ')').parents('.this_v'));
        }
    }
    function pass_for(value, row ,index){
        var str;
        if(value === '1'){
            str = '<span style="color:green">通过</span>';
        }else{
            str = '申请中';
        }
        return str;
    }
</script>
<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th data-options="field:'id',width:70"><?= lang('ID');?></th>
        <th data-options="field:'tag_name',width:150"><?= lang('标签名称');?></th>
        <th data-options="field:'tag_class',width:100"><?= lang('标签分类');?></th>
        <th data-options="field:'id_type',width:100"><?= lang('类型');?></th>
        <th data-options="field:'pass',width:70,formatter:pass_for"><?= lang('是否通过');?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()"><?= lang('新增');?></a>
<!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#tt').edatagrid('destroyRow')">--><?//= lang('delete');?><!--</a>-->
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('查询');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="approve()"><?= lang('审核通过');?></a>

            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" closed="true" title="Query" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <?php foreach($config_search[1] as $key => $config_search_field){?>
            <tr>
                <td>
                    <?= lang('query');?>
                </td>
                <td align="left">
                    <select name="field[f][]" class="f easyui-combobox"  required="true" data-options="
                                onChange:select_f_change,
                            ">
                        <?php
                        foreach ($f as $rs){
                            $item = $rs[0];
                            $selected = '';
                            if($item == $config_search_field[0]) $selected = 'selected';
                            echo "<option value='$item' {$selected}>" . lang($item) . "</option>";
                        }
                        ?>
                    </select>
                    &nbsp;

                    <select name="field[s][]" class="s easyui-combobox"  required="true" data-options="
                            ">
                        <option value="like" <?= $config_search_field[1] == 'like' ? 'selected' : '';?>>like</option>
                        <option value="=" <?= $config_search_field[1] == '=' ? 'selected' : '';?>>=</option>
                        <option value=">=" <?= $config_search_field[1] == '>=' ? 'selected' : '';?>>>=</option>
                        <option value="<=" <?= $config_search_field[1] == '<=' ? 'selected' : '';?>><=</option>
                        <option value="!=" <?= $config_search_field[1] == '!=' ? 'selected' : '';?>>!=</option>
                    </select>
                    &nbsp;
                    <div class="this_v">
                        <input name="field[v][]" class="v easyui-textbox" value="<?= isset($config_search_field[2]) ? $config_search_field[2] : '';?>">
                    </div>
                    <?php if($key == 0){ ?>
                        <button class="add_tr" type="button">+</button>
                    <?php }else{ ?>
                        <button class="del_tr" type="button">-</button>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;"><?= lang('search');?></a>
</div>
<div id="add_div" class="easyui-window"  title="<?= lang('新增');?>" closed="true" style="width:350px;height:280px;padding:5px;">
    <form id="add_fm" method="post">
        <table>
            <tr>
                <td><?= lang('标签名称');?></td>
                <td>
                    <input class="easyui-textbox" name="tag_name" id="add_tag_name" required style="width: 255px;">
                </td>
            </tr>
            <tr>
                <td><?= lang('标签分类');?></td>
                <td>
                    <select class="easyui-combobox" name="tag_class" id="add_tag_class" style="width: 255px" data-options="
                        required:true,
                        valueField:'value',
                        textField:'name',
                        url:'/bsc_dict/get_option/tag_class',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('应用表格');?></td>
                <td>
                    <select class="easyui-combobox" name="id_type" id="add_id_type" style="width: 255px" data-options="
                        required:true,
                    ">
                        <option value="biz_client">biz_client</option>
                        <option value="biz_shipment">biz_shipment</option>
                        <option value="biz_consol">biz_consol</option>
                    </select>
                    <!--                                            url:'/biz_consol_company_soc_tixiangdian/get_option',
                    -->
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <button type="button" onclick="add_save()" style="width: 100px"><?= lang('submit');?></button>
                </td>
            </tr>
        </table>
    </form>
</div>
<div id="edit_div" class="easyui-window"  title="<?= lang('编辑');?>" closed="true" style="width:350px;height:280px;padding:5px;">
    <form id="edit_fm" method="post">
        <input type="hidden" name="id">
        <table>
            <tr>
                <td><?= lang('标签名称');?></td>
                <td>
                    <input class="easyui-textbox" name="tag_name" id="edit_tag_name" required style="width: 255px;">
                </td>
            </tr>
            <tr>
                <td><?= lang('标签分类');?></td>
                <td>
                    <select class="easyui-combobox" name="tag_class" id="edit_tag_class" style="width: 255px" data-options="
                        required:true,
                        valueField:'value',
                        textField:'name',
                        url:'/bsc_dict/get_option/tag_class',
                    ">
                    </select>
                </td>
            </tr>
            <tr>
                <td><?= lang('应用表格');?></td>
                <td>
                    <select class="easyui-combobox" name="id_type" id="edit_id_type" style="width: 255px" data-options="
                        required:true,
                    ">
                        <option value="biz_client">biz_client</option>
                        <option value="biz_shipment">biz_shipment</option>
                        <option value="biz_consol">biz_consol</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <button type="button" onclick="edit_save()" style="width: 100px"><?= lang('submit');?></button>
                </td>
            </tr>
        </table>
    </form>
</div>