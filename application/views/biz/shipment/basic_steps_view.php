<ul style="list-style: none;background-color:#0099cc;padding-top: 5px;margin:0;padding-left: 5px;">
    <li style="display: flex;align-items: end;width: 99vw;flex-wrap: wrap">
        <?php if($biz_type == 'export'):?>
        <?php echo show_steps('biz_shipment.dadanwancheng','biz_shipment',$id,in_array('dadanwancheng',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_consol.dingcangshenqing','biz_shipment',$id,in_array('dingcangshenqing',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_consol.dingcangwancheng','biz_shipment',$id,in_array('dingcangwancheng',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_consol.yupeiyifang','biz_shipment',$id,in_array('yupeiyifang',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_consol.yifangdan','biz_shipment',$id,in_array('yifangdan',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_shipment.haiguancangdan','biz_shipment',$id,in_array('haiguancangdan',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_shipment.baoguanjieshu','biz_shipment',$id,in_array('baoguanjieshu',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_consol.operator_si','biz_shipment',$id,in_array('operator_si',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_consol.document_si','biz_shipment',$id,in_array('document_si',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_consol.vgm_confirmation','biz_shipment',$id,in_array('vgm_confirmation',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_shipment.cfs_in','biz_shipment',$id,in_array('cfs_in',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_shipment.xiangyijingang','biz_shipment',$id,in_array('xiangyijingang',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_shipment.haiguanfangxing','biz_shipment',$id,in_array('haiguanfangxing',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_shipment.matoufangxing','biz_shipment',$id,in_array('matoufangxing',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_shipment.peizaifangxing','biz_shipment',$id,in_array('peizaifangxing',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_shipment.chuanyiqihang','biz_shipment',$id,in_array('chuanyiqihang',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_shipment.flag_ata','biz_shipment',$id,in_array('flag_ata',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_shipment.tidanqianfa','biz_shipment',$id,in_array('tidanqianfa',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_shipment.pre_alert','biz_shipment',$id,in_array('pre_alert',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_shipment.yizuofeiyong','biz_shipment',$id,in_array('yizuofeiyong',$G_userBscEdit)); ?>
        <?php echo show_steps('biz_shipment.pay_buy_flag','biz_shipment',$id,in_array('pay_buy_flag',$G_userBscEdit)); ?>
        <?php elseif($biz_type == 'import'):?>
            <?php echo show_steps('biz_shipment.dadanwancheng','biz_shipment',$id,in_array('dadanwancheng',$G_userBscEdit)); ?>
            <?php echo show_steps('biz_consol.dingcangshenqing','biz_shipment',$id,in_array('dingcangshenqing',$G_userBscEdit)); ?>
            <?php echo show_steps('biz_consol.dingcangwancheng','biz_shipment',$id,in_array('dingcangwancheng',$G_userBscEdit)); ?>
            <?php echo show_steps('biz_shipment.cfs_in','biz_shipment',$id,in_array('cfs_in',$G_userBscEdit)); ?>
            <?php echo show_steps('biz_consol.document_si','biz_shipment',$id,in_array('document_si',$G_userBscEdit)); ?>
            <?php echo show_steps('biz_shipment.tidanqianfa','biz_shipment',$id,in_array('tidanqianfa',$G_userBscEdit)); ?>
            <?php echo show_steps('biz_shipment.chuanyiqihang','biz_shipment',$id,in_array('chuanyiqihang',$G_userBscEdit)); ?>
            <?php echo show_steps('biz_shipment.flag_ata','biz_shipment',$id,in_array('flag_ata',$G_userBscEdit)); ?>
            <?php echo show_steps('biz_shipment.haiguanfangxing','biz_shipment',$id,in_array('haiguanfangxing',$G_userBscEdit)); ?>
            <?php echo show_steps('biz_shipment.flag_truck','biz_shipment',$id,in_array('flag_truck',$G_userBscEdit)); ?>
            <?php echo show_steps('biz_shipment.yizuofeiyong','biz_shipment',$id,in_array('yizuofeiyong',$G_userBscEdit)); ?>
            <?php echo show_steps('biz_shipment.pay_buy_flag','biz_shipment',$id,in_array('pay_buy_flag',$G_userBscEdit)); ?>
			<?php echo show_steps('biz_shipment.pre_alert','biz_shipment',$id,in_array('pre_alert',$G_userBscEdit)); ?>
        <?php endif;?>
    </li>
</ul>

<script>
    $(function(){
        $('.status').change(function () {
            var val = 0;
            var inp = $(this);
            var checked = $(this).prop('checked');
            if(checked) {
                var day1 = new Date();
                var month = '';
                if (day1.getMonth() + 1 < 10) {
                    month = '0' + (day1.getMonth() + 1);
                } else {
                    month = day1.getMonth() + 1;
                }
                var val = day1.getFullYear() + "-" + month + "-" + day1.getDate() + ' ' + day1.getHours() + ':' + day1.getMinutes() + ':' + day1.getSeconds();

            }

            var field = $(this).attr('id');
            var data = {};
            data[field] = val;
            data['steps_field'] = field;
            data['table'] = 'biz_shipment';
			$.ajax({
                    url: '/biz_shipment/update_checkbox/<?php echo $id;?>',
                    type: 'POST',
                    async : false,// 必须使用同步请求
                    data:data,
                    dataType:'json',
                    success: function (res) {
                        if(res.code == 1){
                            parent.$.messager.alert('Tips', res.msg);
                            inp.prop('checked', !checked)
                        }else if(res.code == 2){
                            parent.$.messager.alert('Tips', res.msg);
                            inp.prop('checked', !checked)
                        }else if(res.code == 0){
							parent.$.messager.alert('Tips', res.msg);
							location.reload()
                        }else if(res.code == 5){
							inp.prop('checked', !checked)
							parent.$('#shipment_must_field_iframe').attr("src",'/biz_shipment/shipment_must_field/<?=$id?>')
							parent.$('#shipment_must_field_window').window('open')
						}
                    },error:function () {
                        parent.$.messager.alert('Tips', '发生错误');
                    }
                });
        });
    })
</script>
