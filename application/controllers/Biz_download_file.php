<?php

/**
 * 下载专区
 */
class biz_download_file extends Menu_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_model');
        $this->load->helper("common");
        $this->model = Model('biz_download_file_model');
    }

    public function catalog()
    {
        //  exit('更新中');
        $data = array();
        $data['pid'] = getValue('pid', 0);
        $data['parentNode'] = $this->model->get_where_one("id = {$data['pid']}");
        if (empty($data['parentNode'])) exit('参数错误');

        //2022-08-22 新增搜索
        $where = array();
        $where[] = "lft > {$data['parentNode']['lft']} and rgt < {$data['parentNode']['rgt']}";
        $search = isset($_GET['search']) ? $_GET['search'] : '';
        $data['search']=$search;
        if (!empty($search)) {
            $where[] = "file_path like '%$search%'";
        }
        $where = join(' and ', $where);

        $this->db->select("biz_download_file.*, (select name from bsc_user where bsc_user.id = biz_download_file.updated_by) as updated_by_name");
        $rs = $this->model->get($where);

        $rows = array();
        foreach ($rs as $row){
            $row['file_path'] = urlencode($row['file_path']);

            $rows[] = $row;
        }

        $data['rows'] = $rows;

        $this->load->view('head');
        $this->load->view('biz/download_file/catalog.php', $data);
    }

    public function upload($file_id = 0)
    {
        $json_data = array(
            "code" => 400,
            "msg" => lang('更新失败'),
        );

        if (!$file_id) $file_id = postValue('file_id', 0);


        $sq = "SELECT * FROM `biz_download_file` WHERE `id` = '{$file_id}'";
        $row_file = $this->m_model->query_one($sq);
        if (empty($row_file)) return jsonEcho(array('code' => 400, 'msg' => lang('参数错误')));
        $parentNodes = $this->model->getSinglePath($row_file['id']);

        $root_path = dirname(BASEPATH);

        $file_names = array_column($parentNodes, 'file_name');

        $this_file_name = array_pop($file_names);
        $upload_path = $root_path . '/upload/download_file/' . join('/', $file_names);

//        $upload_path=$root_path . "/upload/download_file/".$row_file["catalog"]."/";
//        $dir_path = $root_path."/upload/download_file/";

        $old_file_path = $upload_path . '/' . $this_file_name;
        if (!is_dir($upload_path)) return jsonEcho(array('code' => 400, 'msg' => lang('当前目录不是文件夹,请勿添加子节点')));
        if (file_exists($old_file_path)) {
            unlink($old_file_path);
        }
        //移动文件
        $rem = rename($_FILES["file"]["tmp_name"], $upload_path . '/' . $_FILES["file"]["name"]);

        if ($rem) {
            $u_d = array(
                "file_name" => $_FILES["file"]["name"],
                "file_path" => '/' . join('/', $file_names) . '/' . $_FILES["file"]["name"],
            );
            $id = $this->model->update($row_file["id"], $u_d);
            $log = array(
                "table_name" => "biz_download_file",
                "master_table_name" => "biz_download_file",
                "master_key" => $row_file["id"],
                "action" => "update",
                "value" => json_encode($u_d),

            );

            log_rcd($log);
            if ($id) {
                $json_data["code"] = 200;
                $json_data["msg"] = lang('更新成功');
            }
        }
        echo json_encode($json_data, JSON_UNESCAPED_UNICODE);

    }

    /**
     * 添加一个子节点
     * @return bool
     */
    // public function addChildNode($parent_id, $file_name){
    public function addChildNode()
    {
        $parent_id = postValue('parent_id', 0);
        $file_name = postValue('file_name', '');
        $file_path = postValue('file_path', '');

        $file_type = postValue('file_type', 'file');
        //添加一个节点到父节点下
        //查询父节点的信息
        // $this->db->force_master();
        $parentNode = $this->model->get_where_one("id = '{$parent_id}'");
        //如果父节点的信息没有,那么直接取MAX(rgt) ,存储在根节点上
        if (empty($parentNode)) {
            $this->db->select("MAX(rgt) as rgt");
            $maxRgt = $this->model->get_where_one();
            if (empty($maxRgt)) $maxRgt['rgt'] = 0;

            $parentNode = array();
            $parentNode['lft'] = $maxRgt['rgt'];
            $parentNode['rgt'] = $parentNode['lft'] + 1;
            $parentNode['level'] = 0;
        }
//        //原本留来自动插入数据时调试用的,可删除
//        $a = $this->model->get_where_one("lft > {$parentNode['lft']} and rgt < {$parentNode['rgt']} and file_name = '{$file_name}'");
//        if(!empty($a)) return false;
        // $this->db->force_master();
        $parentNodes = $this->model->getSinglePath($parent_id);

        if (empty($parentNodes)) $file_path = '';
        else {
            $file_names = array_column($parentNodes, 'file_name');
            $file_path = '/' . join('/', $file_names);
        }

        //可以直接加上传文件的,代码,但是只有自己人用,就不加了, 点以前的更新文件代码来更新,这里只帮忙创建文件
        $root_path = dirname(BASEPATH);

        $parent_path = $root_path . '/upload/download_file' . $file_path;

        $dir_path = $parent_path . '/' . $file_name;
        if ($file_type == 'dir') {
            if (!is_dir($dir_path)) {
                mkdir($dir_path);
            }
        } else if ($file_type == 'file') {
            if (!is_dir($parent_path)) return jsonEcho(array('code' => 1, 'msg' => lang('当前目录不是文件夹,请勿添加子节点')));
        }

        $thisNode = array();
        $thisNode['file_name'] = $file_name;
        $thisNode['file_path'] = $file_path . '/' . $file_name;
        $thisNode['lft'] = $parentNode['rgt'];//父节点右值为新增节点的左值
        $thisNode['rgt'] = $thisNode['lft'] + 1;//新增节点右值为左值加1
        $thisNode['level'] = $parentNode['level'] + 1;//等级为父节点+1

        //添加完毕后, 所有左值和右值比当前大的都+2
        $this->db->query("update biz_download_file set lft = lft + 2 where lft > {$thisNode['lft']};");
        $this->db->query("update biz_download_file set rgt = rgt + 2 where rgt >= {$thisNode['lft']};");


        $id = $this->model->save($thisNode);

        $log_data = array();
        $log_data["table_name"] = "biz_download_file";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($thisNode);
        log_rcd($log_data);
        return jsonEcho(array('code' => 0, 'msg' => lang('添加节点成功')));
    }

    /**
     * 删除节点
     */
    public function deleteNode()
    {
        $id = postValue('id', 0);

        $thisNode = $this->model->get_where_one("id = '{$id}'");
        if (empty($thisNode)) return jsonEcho(array('code' => 1, 'msg' => lang('参数错误')));

        //是否有子节点, 如果有子节点是不给删除,还是直接全删?
        $childNodes = $this->model->get("lft > '{$thisNode['lft']}' and rgt < '{$thisNode['rgt']}'");
        if (!empty($childNodes)) return jsonEcho(array('code' => 1, 'msg' => lang('存在子节点,请删除全部子节点后再试')));

        $this->model->mdelete($id);

        $log_data = array();
        $log_data["table_name"] = "biz_download_file";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($thisNode);
        log_rcd($log_data);

        //这里可以考虑是否删除文件, 还是不删了,避免被人误删不好恢复

        //删除后, 所有左值和右值比当前大的都+2
        $this->db->query("update biz_download_file set lft = lft - 2 where lft >= {$thisNode['lft']};");
        $this->db->query("update biz_download_file set rgt = rgt - 2 where rgt > {$thisNode['lft']};");

        return jsonEcho(array('code' => 0, 'msg' => lang('删除成功')));
    }

    /**
     * 移动节点
     */
    public function moveNode()
    {
        //分为横向移动和纵向移动
        $parentNodeId = postValue('parentNodeId', 0);//准备移动到的父标签位置
        $id = postValue('id', 0); //当前node id
        $leftNodeId = postValue('leftNodeId', 0);

        $thisNode = $this->model->get_where_one("id = '{$id}'");
        if (empty($thisNode)) return jsonEcho(array('code' => 1, 'msg' => lang('当前节点不存在')));

        $parentNode = $this->model->get_where_one("id = '{$parentNodeId}'");
        if (empty($thisNode)) return jsonEcho(array('code' => 1, 'msg' => lang('父节点不存在')));

        //如果左节点不存在,那么代表移动到当前父节点的最左
        $leftNode = $this->model->get_where_one("id = '{$leftNodeId}'");
        if (empty($leftNode)) {
            $leftNode = array(
                'rgt' => $parentNode['rgt'],
            );
        }

        //如果左右值在范围内,那么代表是横向移动
        $update_data = array();
        $thisNodeLength = $thisNode['rgt'] - $thisNode['lft'] + 1;//当前节点的长度
        if ($parentNode['lft'] < $thisNode['lft'] && $parentNode['rgt'] > $thisNode['rgt']) {
            //横向移动
            //如果移动后的左节点,左值比当前小,那么代表为左移
            if ($leftNode['lft'] < $thisNode['lft']) {
                //左移动的移动距离为 当前节点的左值 减 移动后左节点的右值后 - 1
                //  1 12
                //23 45 67 811
                //          910

                // 1 12
                //25(811) 67(23) 89(45) 1011(67)
                $moveLength = $thisNode['lft'] - $leftNode['rgt'] - 1; //移动距离


                //左移的话,那么当前左值到左节点的右值之间的,统一 加 当前节点的长度
                $this->db->query("update biz_download_file set lft = lft + {$thisNodeLength},rgt = rgt + {$thisNodeLength} where lft > {$leftNode['rgt']} and lft < {$thisNode['lft']};");

                //子节点和当前节点左右值,统一 减少 当前节点的移动距离
                $moveLengthUpdate = $moveLength > 0 ? '-' . $moveLength : '+' . $moveLength;
                $this->db->query("update biz_download_file set lft = lft {$moveLengthUpdate},rgt = rgt {$moveLengthUpdate} where rgt <= {$thisNode['rgt']} and lft >= {$thisNode['lft']};");
            } else {
                // 1 12
                //23 47 89 1011
                //   56

                // 1 12
                //23 45(89) 67(89) 811(47)
                //   56
                //右移动不会存在没有左节点
                //右移动的移动距离为 移动后左节点的左值后 减 当前节点的右值 + 1
                $moveLength = $leftNode['lft'] - $thisNode['rgt'] + 1; //移动距离

                //右移的话,那么当前节点的右值到左节点的右值之间的,统一 减当前节点的长度
                $this->db->query("update biz_download_file set lft = lft - {$thisNodeLength},rgt = rgt - {$thisNodeLength} where rgt > {$thisNode['rgt']} and rgt <= {$leftNode['rgt']};");

                //子节点左右值,统一 减少 当前节点的移动距离
                $moveLengthUpdate = $moveLength < 0 ? '-' . $moveLength : '+' . $moveLength;
                $this->db->query("update biz_download_file set lft = lft {$moveLengthUpdate},rgt = rgt {$moveLengthUpdate} where lft >= {$thisNode['lft']} and rgt <= {$thisNode['rgt']};");

            }

        } else {
            //竖向的后面有时间慢慢研究 TODO
            return jsonEcho(array('code' => 1, 'msg' => lang('暂不支持竖向移动')));
        }

        return jsonEcho(array('code' => 0, 'msg' => lang('移动节点成功')));
    }
}