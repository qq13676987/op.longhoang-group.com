<?php
class sys_config_title_model extends CI_Model{
    private $table = 'sys_config_title_view';
    private $base_table = 'sys_config_title_base';
    private $user_table = 'sys_config_title_user';

    public function __construct(){
        $this->load->database();
        // if ($this->session->userdata('id')==''){
        //     redirect('main/login', 'refresh');
        // }
        // $this->user_id =  $this->session->userdata('id');
    }

    public $user_id = '';

    public function get_one($table_name = ''){
        $sql = "select * from sys_config_title where table_name = '$table_name' order by order_by asc";
        $rs = $this->db->query($sql);
        $rows = $rs->result_array();
        $data = array();
        foreach ($rows as $row) {
            $data[] = array($row['table_field'], $row['field_name'], $row['width'], $row['order_by'], $row['sortable']);
        }
        return $data;
    }

    /**
     * ???????????
     * @param $view_name
     */
    public function get_default_title($view_name){
        //??��???????????????redis????, ??title????????????,??????????,???????????????
        $where = array();

        if(!empty($view_name)) $where[] = "{$this->table}.view_name = '{$view_name}'";
        $where[] = "{$this->table}.is_default = 1";

        $where_str = join(' and ', $where);

        $this->db->where($where_str);
        $this->db->join("{$this->base_table}", "{$this->base_table}.id = {$this->table}.title_base_id");
        $query = $this->db->get($this->table);

        return $query->result_array();
    }

    /**
     * ????table_name  ??????????????????
     * @param string $table_name
     * @param string $view_name
     */
    public function get_user_title($table_name = '', $view_name = ''){
        $userId = get_session('id');

        $where = array();

        $where[] = "{$this->base_table}.table_name = '{$table_name}'";
        if(!empty($view_name)) $where[] = "{$this->table}.view_name = '{$view_name}'";
        $where[] = "{$this->user_table}.user_id = '$userId'";

        $where_str = join(' and ', $where);

        $this->db->where($where_str);
        $this->db->join("{$this->base_table}", "{$this->base_table}.id = {$this->user_table}.base_id");
        $this->db->join("{$this->table}", "{$this->table}.id = {$this->user_table}.title_id");
        $query = $this->db->get($this->user_table);

        return $query->result_array();
    }
    
    public function get_base_title($table_name){
        $where = array();

        $where[] = "{$this->base_table}.table_name = '{$table_name}'";

        $where_str = join(' and ', $where);

        $this->db->where($where_str);
        $query = $this->db->get($this->base_table);

        return $query->result_array();
    }

    /**
     * ???????????
     * @param $view_name
     */
    public function get_view_title($table_name, $view_name){
        //??��???????????????redis????, ??title????????????,??????????,???????????????
        $where = array();

        if(!empty($view_name)) $where[] = "{$this->base_table}.table_name = '{$table_name}'";
        if(!empty($view_name)) $where[] = "{$this->table}.view_name = '{$view_name}'";

        $where_str = join(' and ', $where);

        $this->db->where($where_str);
        $this->db->join("{$this->base_table}", "{$this->base_table}.id = {$this->table}.title_base_id");
        $query = $this->db->get($this->table);

        return $query->result_array();
    }
}
