<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<title><?php echo $job_no;?>-consol-Edit</title>

<script type="text/javascript">
    var free_svr_cf = false;
    var is_special_char = '';
    function qzb(str){
        str = str.replace(new RegExp('·', "g"), '`');
        str = str.replace(new RegExp('！', "g"), '!');
        str = str.replace(new RegExp('￥', "g"), '$');
        str = str.replace(new RegExp('【', "g"), '[');
        str = str.replace(new RegExp('】', "g"), ']');
        str = str.replace(new RegExp('】', "g"), ']');
        str = str.replace(new RegExp('；', "g"), ';');
        str = str.replace(new RegExp('：', "g"), ':');
        str = str.replace(new RegExp('“', "g"), '"');
        str = str.replace(new RegExp('”', "g"), '"');
        str = str.replace(new RegExp('’', "g"), '\'');
        str = str.replace(new RegExp('‘', "g"), '\'');
        str = str.replace(new RegExp('、', "g"), '');
        str = str.replace(new RegExp('？', "g"), '?');
        str = str.replace(new RegExp('《', "g"), '<');
        str = str.replace(new RegExp('》', "g"), '>');
        str = str.replace(new RegExp('，', "g"), ',');
        str = str.replace(new RegExp('。', "g"), '.');
        str = str.replace(new RegExp(' ', "g"), ' ');
        str = str.replace(new RegExp('　', "g"), ' ');
        str = str.replace(new RegExp('（', "g"), '(');
        str = str.replace(new RegExp('）', "g"), ')');
        return str;
    }

    /**
     * 下拉选择表格展开时,自动让表单内的查询获取焦点
     */
    function combogrid_search_focus(form_id, focus_num = 0){
        // var form = $(form_id);
        setTimeout("easyui_focus($($('" + form_id + "').find('.keydown_search')[" + focus_num + "]));",200);
    }

    /**
     * easyui 获取焦点
     */
    function easyui_focus(jq){
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if(data.combogrid !== undefined){
            return $(jq).combogrid('textbox').focus();
        }else if(data.combobox !== undefined){
            return $(jq).combobox('textbox').focus();
        }else if(data.datetimebox !== undefined){
            return $(jq).datetimebox('textbox').focus();
        }else if(data.datebox !== undefined){
            return $(jq).datebox('textbox').focus();
        }else if(data.textbox !== undefined){
            return $(jq).textbox('textbox').focus();
        }
    }

    /**
     * 这个函数是给shipments 里查看箱信息使用
     */
    function container_window(shipment_id) {
        var window_iframe_css = {width: '950px', height: '550px'};
        $('#window_iframe').css(window_iframe_css).attr('src', '/biz_containershipment/index/' + shipment_id + '/<?= $id;?>');
        $('#window').window({
            title:'Container',
            width:1000,
            height:600,
            modal:true
        }).window('open');
    }

    $.fn.datebox.defaults.formatter = function(date){
        var y = date.getFullYear();
        var m = date.getMonth()+1;
        var d = date.getDate();
        if(m < 10) m = '0' + m;
        if(d < 10) d = '0' + d;
        if(y == '1899'){
            y = '0000';
            m = '00';
            d = '00';
        }
        return y+'-'+m+'-'+d;
    }

    /**
     * 新增箱型箱量
     */
    function add_box() {
        var length = $('.add_box').length;
        var lang = '';
        var btn = '';
        if(length <= 0){
            lang = "<?= lang('box_info');?>";
            btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
        }else{
            btn = '<a class="easyui-linkbutton" onclick="del_box(this)" iconCls="icon-remove"></a>';
        }
        var str = '<tr class="add_box" >\n' +
            '                            <td>' + lang + '</td>\n'+
            '                            <td>\n'+
            '                                <select <?php echo in_array("box_info",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-combobox box_info_size" name="box_info[size][]" data-options="\n'+
            '                                valueField:\'value\',\n'+
            '                                textField:\'value\',\n'+
            '                                url:\'/bsc_dict/get_option/container_size\',\n'+
            "                                onHidePanel: function() {  \n" +
            "                                   var valueField = $(this).combobox('options').valueField;  \n" +
            "                                   var val = $(this).combobox('getValue');  \n" +
            "                                   var allData = $(this).combobox('getData');  \n" +
            "                                   var result = true;  \n" +
            "                                   for (var i = 0; i < allData.length; i++) {  \n" +
            "                                       if (val == allData[i][valueField]) {  \n" +
            "                                           result = false;  \n" +
            "                                       }  \n" +
            "                                   }  \n" +
            "                                   if (result) {  \n" +
            "                                       $(this).combobox('clear');  \n" +
            "                                   } " +
            "                                }" +
            '                                "></select>\n'+
            '                                X\n'+
            '                                <input <?php echo in_array("box_info",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-numberbox box_info_num" name="box_info[num][]">\n'+
            '                                ' + btn + '\n'+
            '                            </td>\n'+
            '                        </tr>';
        if(length == 0){
            $('#trans_mode').parent('td').parent('tr').after(str);
        }else{
            $('.add_box:eq(' + (length - 1) + ')').after(str);
        }
        $.parser.parse('.add_box:eq(' + length + ')');
    }

    /**
     * 删除一行箱型箱量
     */
    function del_box(e) {
        if(e == undefined){
            $('.add_box').remove();
        }else{
            $(e).parent('td').parent('tr').remove();
        }
    }

    function reload_select(id = '', type='combobox'){
        if(type == 'combobox'){
            var agent_company = $('#agent_company').combogrid('getValue');
            $('#agent_company').combogrid('clear').combogrid('grid').datagrid('selectRecordField', {field:'company_name', value:agent_company});
            $('#vessel').combobox('reload');

        }
        
        if(id == 'shipper_company2') $('#shipper_company2').combogrid('grid').datagrid('reload');
        if(id == 'consignee_company2') $('#consignee_company2').combogrid('grid').datagrid('reload');
        if(id == 'notify_company2') $('#notify_company2').combogrid('grid').datagrid('reload');
        
    }

    /**
     * 新增免箱期
     */
    function add_free_svr() {
        var length = $('.free_svr').length;

        var btn = '';
        var lang = '';
        if(length < 4){
            if(length == 0){
                lang = '<?= lang('free svr');?>';
            }
            if(length < 3){
                btn = '<a class="easyui-linkbutton" onclick="add_free_svr(this)" iconCls="icon-add"></a>';
            }
        }else{
            return;
        }
        var str = '  <tr class="free_svr">\n' +
            '                                <td>' + lang + '</td>\n' +
            '                                <td>\n'+
            '                                    <select <?php echo in_array("free_svr",$G_userBscEdit) ? '' : 'readonly'; ?> editable="false" class="easyui-combobox free_svr_type" name="free_svr[' + length + '][type]" data-options="'+
            '                                        valueField:\'value\',\n'+
            '                                        textField:\'name\',\n'+
            '                                        url: \'/bsc_dict/get_option/free_svr\',\n' +
            '                                        value: \'\',\n'+
            '                                        onSelect: function(rec){\n' +
            '                                            free_svr_type_jc();\n' +
            '                                        },' +
            '                                    ">\n'+
            '                                    </select>\n'+
            '                                    <input <?php echo in_array("free_svr",$G_userBscEdit) ? '' : 'readonly'; ?>  class="easyui-numberspinner free_svr_days" name="free_svr[' + length + '][days]"  data-options="min:1,max:30"><?= lang('days');?>\n'+
            btn +
            '                                </td>\n'+
            '                            </tr>';

        $('.free_svr:eq(' + (length - 1) + ')').after(str);
        $.parser.parse('.free_svr:eq(' + length + ')');
    }

    /**
     *
     */
    function free_svr_type_jc() {
        var free_svr_type = $('.free_svr_type')
        var data = [];
        var jc = false;
        $.each(free_svr_type, function(index,item){
            var val = $(this).combobox('getValue');
            if(val != ''){
                if($.inArray(val, data) === -1){
                    data.push(val);
                }else{
                    jc = true;
                }
            }

        });
        if(jc === true){
            free_svr_cf = true;
        }else{
            free_svr_cf = false
        }
    }

    var is_update = false;

    window.onbeforeunload = function (e) {
        // 兼容IE8和Firefox 4之前的版本
        //检测是否有字段变动
        e = e || window.event;
        var dialogText = '您有未保存的内容,是否确认退出';
        if(is_update){
            if (e) {
                e.returnValue = dialogText;
            }
            // Chrome, Safari, Firefox 4+, Opera 12+ , IE 9+
            return dialogText;
        }
    };

    function checkName(val, type = 0){
        // var reg = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");\
        if(type == 0){
            var reg = new RegExp(/[^\x00-\x7f]/g);
        }else{
            var reg = new RegExp(/[^\x00-\x7f|\u4e00-\u9fa5]/g);
        }
        var rs = "";
        for (var i = 0, l = val.length; i < val.length; i++) {
            if(val.substr(i, 1).match(reg)) rs += val.substr(i, 1);
        }
        return rs;
    }

    function trans_carrier_select(rec){
        
        var trans_mode = $('#trans_mode').val();
        var trans_tool = $('#trans_tool').combobox('getValue');

        //2022-07-14 新增原先的值
        var old1 = $('#trans_origin').combogrid('getValue'); // 获取数据表格对象
        var old2 = $('#trans_discharge').combogrid('getValue'); // 获取数据表格对象
        var old3 = $('#trans_destination').combogrid('getValue'); // 获取数据表格对象


        $('#trans_origin').combogrid('grid').datagrid('load', "/biz_port/get_option?type=" + trans_tool + "&carrier=" + rec.client_code + '&limit=true'+'&old='+old1);
        $('#trans_discharge').combogrid('grid').datagrid('reload', "/biz_port/get_option?type=" + trans_tool + "&carrier=" + rec.client_code + '&limit=true'+'&old='+old2);

        $('#trans_destination').combogrid('grid').datagrid('reload', "/biz_port/get_discharge?type=" + trans_tool + "&carrier=" + rec.client_code+'&old='+old3);

        if(rec.client_name == 'TSL'){
            $('#hs_code').combobox('reload', '/bsc_dict/get_option/hs_code?ext1=TSL');
        }else{
            $('#hs_code').combobox('options').url = null
            $('#hs_code').combobox('loadData', {});
        }

        $('#trans_carrier_second').combobox('setValue', rec.client_code);
    }

    function trans_carrier_rule_open(){
        var trans_carrier = $('#trans_carrier').combobox('getValue');
        $('#trans_carrier_rule_iframe').attr('src', '/bsc_help_content/help/' + trans_carrier + '_CARRIER_REMIND');
        $('#trans_carrier_rule').window({height:$(window).height()}).window('open');
    }
    
    function creditor_select(rec) {
        var time = new Date().getTime();
        if(rec.stop_date != '0000-00-00 00:00:00'){
            //判断时间是否大于当前时间,如果大于当前时间,那么提示过期无法选择,请申请延期
            var stop_date_time = new Date(rec.stop_date).getTime();

            //如果当前时间小于停止时间,那么弹窗提示
            // if(time > stop_date_time){
            //     $.messager.alert('Tips', rec.company_name + '已过期无法选择,请到往来单位申请延期');
            //     $('#creditor').combobox('clear');
            //     return false;
            // }else{
            //     return true;
            // }
        }
    }

    function set_quotation_fields(field, value){
        var quotation_form_data = {};
        quotation_form_data['fields[' + field + ']'] = value;
        $('#reportComboGridTbform').form('load', quotation_form_data);
        $('#queryReport').trigger('click');
    }

    function get_shipment_sum(){
        $.ajax({
            type:'GET',
            url:'/biz_consol/get_shipment_sum/<?= $id;?>',
            dataType: 'json',  // 请求方式为jsonp
            success:function (res) {
                $.messager.alert('Tips', res.msg);
                if(res.code == 0){
                    $('#good_outers').textbox('setValue', res.data.good_outers);
                    $('#good_outers_unit').textbox('setValue', res.data.good_outers_unit);
                    $('#good_weight').textbox('setValue', res.data.good_weight);
                    $('#good_volume').textbox('setValue', res.data.good_volume);
                    return;
                }
            },
            error:function () {

            }
        });
    }

    function get_shipment_info(field){
        $.ajax({
            type:'GET',
            url:'/biz_consol/get_shipment_info/<?= $id;?>?field=' + field,
            dataType: 'json',  // 请求方式为jsonp
            success:function (res) {
                $.messager.alert('Tips', res.msg);
                if(res.code == 0){
                    var field_arr = field.split(',');
                    $.each(field_arr, function(i, it){
                        try{
                            $('#' + it).textbox('setValue', res.data[it]);
                        }catch(e){
                            $('#' + it).val(res.data[it]);
                        }
                    });
                }
            },
            error:function () {
                $.messager.alert('Tips', '发生错误');
            }
        });
    }

    function create_company_tool(div_id, id){
        var html = '<div id="' + div_id + '_div">' +
            '<form id="' + div_id + '_form" method="post">' +
            '<div style="padding-left: 5px;display: inline-block;">' +
            '<label><?= lang('company_name');?>:</label><input name="company_name" class="easyui-textbox keydown_search" t_id="' + id + '" style="width:96px;"/>' +
            '</div>' +
            '<form>' +
            '<div style="padding-left: 1px; display: inline-block;">' +
            '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="' + id + '_click" onclick="query_report(\'' + div_id + '\', \'' + id + '\')"><?= lang('search');?></a>' +
            '</div>' +
            '</div>';
        $('body').append(html);
        $.parser.parse('#' + div_id + '_div');
        return div_id;
    }

    function query_report(div_id, load_id){
        var where = {};
        var form_data = $('#' + div_id + '_form').serializeArray();
        $.each(form_data, function (index, item) {
            if(item.value == "") return true;
            if(where.hasOwnProperty(item.name) === true){
                if(typeof where[item.name] == 'string'){
                    where[item.name] =  where[item.name].split(',');
                    where[item.name].push(item.value);
                }else{
                    where[item.name].push(item.value);
                }
            }else{
                where[item.name] = item.value;
            }
        });
        if(where.length == 0){
            $.messager.alert('Tips', '请填写需要搜索的值');
            return;
        }
        $('#' + load_id).combogrid('grid').datagrid('load',where);
    }

    //text添加输入值改变
    function keydown_search(e, click_id){
        if(e.keyCode == 13){
            $('#' + click_id + '_click').trigger('click');
        }
    }
    $(function(){
		<?php if($from_db != '' && $from_id_no != 0):?>
		$('body').css('background-color', '#F0FFF0');
		<?php endif;?>
        //初始值
        $('#shipper_company').val('<?= es_encode($shipper_company);?>');
        $('#consignee_company').val('<?= es_encode($consignee_company);?>');
        $('#notify_company').val('<?= es_encode($notify_company);?>');

        $('#vessel').combobox('reload', '/bsc_dict/get_option/vessel');

        $('#tb').tabs({
            border:false,
            onSelect:function(title){
                var body_color = $('body').css('background-color');
                if (title == "Container"){
                    if (document.getElementById("container_sub").src==""){
                        document.getElementById("container_sub").src="/biz_container/index/<?php echo $id;?>";
                    }
                }
                if (title == "Billing"){
                    if(body_color == 'rgb(255, 0, 0)'){
                        $.messager.alert('Tips', '有红色背景报警存在，请先解决');
                        return false;
                    }
                    if (document.getElementById("billing_sub").src==""){
                        document.getElementById("billing_sub").src="/biz_bill/index?id_type=consol&id_no=<?php echo $id;?>";
                    }
                }
                if (title == "Download"){
                    if(body_color == 'rgb(255, 0, 0)'){
                        $.messager.alert('Tips', '有红色背景报警存在，请先解决');
                        return false;
                    }
                    if (document.getElementById("Download_sub").src==""){
                        //document.getElementById("Download_sub").src="/api/phpword/list.php?data_table=consol&id=<?php //echo $id;?>//";
                        document.getElementById("Download_sub").src="/bsc_template/download?data_table=consol&id=<?php echo $id;?>";
                    }
                }
                if (title == "Upload files"){
                    if (document.getElementById("upload_files_sub").src==""){
                        document.getElementById("upload_files_sub").src="/bsc_upload/upload_files?biz_table=biz_consol&id_no=<?php echo $id;?>";
                    }
                }
                if (title == "Tracking"){
                    if (document.getElementById("tracking").src==""){
                        document.getElementById("tracking").src="/wait";
                    }
                }
                if (title == "EDI") {
                    if (document.getElementById("edi").src == "") {
                        document.getElementById("edi").src = "/bsc_message/edi?id_type=consol&id_no=<?= $id;?>";
                    }
                }
                if(title == 'Log'){
                    if (document.getElementById("log").src==""){
                        document.getElementById("log").src="/sys_log/index?table_name=biz_consol&key_no=<?= $id?>";
                    }
                }
                if(title == 'Spmt Log'){
                    if (document.getElementById("spmt_log").src==""){
                        document.getElementById("spmt_log").src="/sys_log/bind_shipment_log?id=<?= $id;?>";
                    }
                }
                if(title == 'Lock Log'){
                    if (document.getElementById("lock_log").src==""){
                        document.getElementById("lock_log").src="/sys_lock_log/consol_log?consol_id=<?= $id;?>";
                    }
                }
                if(title == 'Emails'){
                    if (document.getElementById("emails").src==""){
                        document.getElementById("emails").src="/mail_box/template_get_mail?mail_type=consol&id=<?= $id?>";
                    }
                }
            }

        });

        $('.status').change(function () {
            var val = 0;
            var inp = $(this);
            var checked = $(this).prop('checked');
            if(checked) {
                var day1 = new Date();
                var month = '';
                if(day1.getMonth()+1 < 10){
                    month = '0' + (day1.getMonth()+1);
                }else{
                    month = day1.getMonth()+1;
                }
                var val = day1.getFullYear() + "-" + month + "-" + day1.getDate() + ' ' + day1.getHours() + ':' + day1.getMinutes() + ':' + day1.getSeconds();

            }

            var field = $(this).attr('id');

            //2022-07-22 新增vgm勾选
            if(field == 'vgm_confirmation'){

                //如果实际离泊没填,不让勾
                var trans_ETA = $('#trans_ETA').datetimebox('getValue');

                if(checked){
                    if(trans_ETA == ''){
                        $(this).prop('checked', false);
                        $.messager.alert('Tips', '计划靠泊为空,vgm勾选失败');
                        return false;
                    }else{
                        var a1 = new Date();
                        var a2 = new Date(trans_ETA);
                        //判断是否时间选择错误;
                        if (a1 > a2) {
                            $(this).prop('checked', false);
                            alert("计划靠泊不能小于今天!");
                            return false;
                        }

                        //判断时间差;
                        var total = (a2.getTime() - a1.getTime()) / 1000;//相差的秒数;
                        var endTime = parseInt(total / ( 60 * 60));//计算是否超过24小时;

                        if (endTime > 47) {
                            $(this).prop('checked', false);
                            alert("计划靠泊超过48小时不能发送vgm!");
                            return false;
                        }
                    }
                }

            }
            //船开勾相关逻辑
            if(field == 'chuanyiqihang'){
                //如果实际离泊没填,不让勾
                var trans_ATD = $('#trans_ATD').datetimebox('getValue');

                if(checked){
                    // if(trans_ATD == ''){
                    //     $(this).prop('checked', false);
                    //     $.messager.alert('Tips', '实际离泊为空,船开勾选失败');
                    //     return false;
                    // }
                }
            }

            var data = {};
            data[field] = val;
            $.ajax({
                url: '/biz_consol/update_check_box/<?php echo $id;?>',
                type: 'POST',
                data:data,
                dataType:'json',
                success: function (res) {
                    var ext_data = res.ext_data;
                    if(res.code == 0){
                        //已去除该判断,现在只要勾选操作SI勾,则自动同步SI相关信息shipment信息到consol
                        //这里加入如果扩展里有is_mbl,且为true,那么询问是否同步SHIPMENT,
                        // if(ext_data.is_mbl == true){
                            // $.messager.confirm('Tips', '检测到当前shipment为MBL,是否同步部分shipment信息到consol?', function(r){
                            //     if(r){
                                    
                            //     } 
                            // });
                        var mbl_type = 'MBL';
                        var mbl_ext = '';
                        if(!ext_data.is_mbl){
                            mbl_type = 'HBL';
                            mbl_ext = '(不含收发通)';
                        }
                        if(ext_data.msg != null){
                            $.messager.alert('Tips', ext_data.msg);
                        }
                        if(field == 'operator_si' && val != '0'){

                        }
                        // }
                        if(field == 'dingcangwancheng' && val != '0'){
                            location.reload();
                        }
                        
                    }else{
                        $.messager.alert('Tips', res.msg);
                        inp.prop('checked', !checked)
                    }
                },error:function () {
                    $.messager.alert('Tips', '发生错误');
                }
            });
        });

        $('.top_sign').change(function () {
            var val = [];
            $.each($('input[name="sign[]"]'), function(index, item){
                if($(item).prop('checked')){
                    val.push($(item).val());
                }
            });
            val = val.join(',');

            var data = {};
            if(val == '') val = ' ';
            data['sign'] = val;
            $.ajax({
                url: '/biz_consol/update_data_ajax/<?= $id;?>',
                type: 'POST',
                data:data,
                success: function (res) {

                }
            });
        });

        $('#carrier_ref').textbox('textbox').blur(function (e) {
            var val = $('#carrier_ref').textbox('getValue');
            $.ajax({
                type:'POST',
                url:'/biz_consol/select_value_exist',
                data:{
                    field:'agent_ref',
                    value:val,
                },
                dataType: 'json',
                success: function (res) {
                    if(res.code == 0){
                        $('#carrier_ref_tip').text(res.data.job_no);
                    }else{
                        $('#carrier_ref_tip').text('');
                    }
                }
            });
        });

        $('#agent_ref').textbox('textbox').blur(function (e) {
            var val = $('#agent_ref').textbox('getValue');
            $.ajax({
                type:'POST',
                url:'/biz_consol/select_value_exist',
                data:{
                    field:'agent_ref',
                    value:val,
                },
                dataType: 'json',
                success: function (res) {
                    if(res.code == 0){
                        $('#agent_ref_tip').text(res.data.job_no + '重复');
                    }else{
                        $('#agent_ref_tip').text('');
                    }
                }
            });
        });

        //shipment错误提示--start
        <?php if(!empty($shipment_diff)){?>
        $.messager.show({
            title:'<?= lang('warning');?>',
            showType:'slide',
            width: '300px',
            height: '200px',
            timeout: 0,
            msg: '<?= $shipment_diff;?>',
            style:{
                top:document.body.scrollTop+document.documentElement.scrollTop,
                left:'0',
            }
        });
        $('body').css('background-color', 'red');
        <?php } ?>
        //2021-06-07 锁了的灰色
        <?php if($lock_lv > 0 ){?>
        $('body').css('background-color', 'gray');
        <?php } ?>

        var window_height = $(window).height();
        var window_width = $(window).width();
        $('#tb').tabs('resize', {
            width: 0.99 * window_width,
            height: 0.99 * window_height - 78
        });
        $('#el').layout('resize', {
            width: 0.99 * window_width - 1,
            height: 0.99 * window_height  - 135,
        });

        //实现表格下拉框的分页和筛选--start
        var html = '<div id="agentTb">';
        html += '<form id="agentTbform">';
        html += '<div style="padding-left: 5px;display: inline-block;"><label><?= lang('company_name');?>:</label><input name="company_name" class="easyui-textbox agent_search"style="width:96px;"data-options="prompt:\'text\'"/></div>';
        html += '<div style="padding-left: 5px;display: inline-block;"><label><?= lang('country_code');?>:</label><select name="country_code" class="easyui-textbox agent_search"style="width:96px;"data-options="prompt:\'text\',valueField:\'value\',textField:\'namevalue\', url:\'/bsc_dict/get_option/country_code\'"></select></div>';
        html += '<div style="padding-left: 1px; display: inline-block;"><form>';
        html += '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="agentQueryReport"><?= lang('search');?></a>';
        html += '</div>';
        html += '</div>';
        $('body').append(html);
        $.parser.parse('#agentTb');

        $('#agent_company').combogrid("reset");
        $('#agent_company').combogrid({
            panelWidth: '600px',
            panelHeight: '300px',
            idField: 'company_name',              //ID字段
            textField: 'company_name',    //显示的字段
            striped: true,
            editable: false,
            pagination: false,           //是否分页
            toolbar : '#agentTb',
            rownumbers: true,           //序号
            collapsible: true,         //是否可折叠的
            value: "<?= es_encode($agent_company);?>",
            method: 'post',
            columns:[[
                {field:'country_code',title:'<?= lang('country_code');?>',width:60},
                {field:'company_name',title:'<?= lang('agent_company');?>',width:250},
                {field:'remark',title:'<?= lang('remark');?>',width:250},
            ]],
            emptyMsg : '未找到相应数据!',
            onBeforeSelect:function(index, row){
                // if(row.stop_bool==false){
                //     alert('代理已过期请重新设置');
                //     return false;
                // }
            },
            onSelect : function(index, row){
                if(row.company_address == '') row.company_address = ' ';
                if(row.contact_telephone2 == '') row.contact_telephone2 = ' ';
                if(row.contact_name == '') row.contact_name = ' ';
                $('#agent_address').val(row.company_address);
                $('#agent_contact').val(row.contact_name);
                $('#agent_telephone').val(row.contact_telephone2);
                $('#agent_code').val(row.client_code);
                $('#agent_email').textbox('setValue', row.contact_email);
                changeClient(row);
            },
            onLoadSuccess : function(data) {
                var clientCode = $('#agent_code').val();
                var this_data = data.rows;
                var rec = this_data.filter(el => el['client_code'] === clientCode);
                if(clientCode){
                    if(rec.length > 0 ){
                        changeClient(rec[0]);
                    }else{
                        changeClient({client_code:clientCode});
                    }
                }else{
                    changeClient({client_code:clientCode});
                }
                var opts = $(this).combogrid('grid').datagrid('options');
                var vc = $(this).combogrid('grid').datagrid('getPanel').children('div.datagrid-view');
                vc.children('div.datagrid-empty').remove();
                if (!$(this).combogrid('grid').datagrid('getRows').length) {
                    var d = $('<div class="datagrid-empty"></div>').html(opts.emptyMsg || 'no records').appendTo(vc);
                    d.css({
                        position : 'absolute',
                        left : 0,
                        top : 50,
                        width : '100%',
                        fontSize : '14px',
                        textAlign : 'center'
                    });
                }
            },
            queryParams:{
                trans_carrier:$("#trans_carrier").combobox("getValue"),
                trans_destination:$("#trans_destination_inner").textbox("getValue"),
            },
            url: '/biz_client/get_agent?sales_ids=<?= $sales_id; ?>',
        });
        //点击搜索
        $('#agentQueryReport').click(function(){
            var where = {};
            var form_data = $('#agentTbform').serializeArray();
            $.each(form_data, function (index, item) {
                if(where.hasOwnProperty(item.name) === true){
                    if(typeof where[item.name] == 'string'){
                        where[item.name] =  where[item.name].split(',');
                        where[item.name].push(item.value);
                    }else{
                        where[item.name].push(item.value);
                    }
                }else{
                    where[item.name] = item.value;
                }
            });
            // where['field'] = $('#to_search_field').combo('getValue');
            var trans_carrier = $("#trans_carrier").combobox("getValue");
            var trans_destination = $("#trans_destination_inner").textbox("getValue");
            where['trans_carrier']=trans_carrier;
            where['trans_destination']=trans_destination;
            $('#agent_company').combogrid('grid').datagrid('load',where);
        });
        //text添加输入值改变
        $('.agent_search').textbox('textbox').keydown(function (e) {
            if(e.keyCode == 13){
                $('#agentQueryReport').trigger('click');
            }
        });
        //实现表格下拉框的分页和筛选-----end
        var company_config = {
            panelWidth: '1000px',
            panelHeight: '300px',
            idField: 'id',              //ID字段
            textField: 'company_name',    //显示的字段
            striped: true,
            editable: false,
            pagination: false,           //是否分页
            toolbar : '#' + create_company_tool('shipper_company2', 'shipper_company2') + '_div',
            collapsible: true,         //是否可折叠的
            method: 'post',
            columns:[[
                {field:'company_name',title:'<?= lang('company_name');?>',width:200},
                {field:'company_address',title:'<?= lang('company_address');?>',width:400},
                {field:'company_contact',title:'<?= lang('company_contact');?>',width:100},
                {field:'company_telephone',title:'<?= lang('company_telephone');?>',width:100},
                {field:'company_email',title:'<?= lang('company_email');?>',width:100},
            ]],
            emptyMsg : '未找到相应数据!',
            mode: 'remote',
            onSelect: function(index, row){
                if(row.company_address == '') row.company_address = ' ';
                if(row.company_contact == '') row.company_contact = ' ';
                if(row.company_telephone == '') row.company_telephone = ' ';
                if(row.company_email == '') row.company_email = ' ';
                $('#shipper_company').val(row.company_name);
                $('#shipper_address').val(row.company_address);
                $('#shipper_contact').val(row.company_contact);
                $('#shipper_telephone').val(row.company_telephone);
                $('#shipper_email').val(row.company_email);
            },
            value:'<?= es_encode($shipper_company);?>',
        };
        $('#shipper_company2').combogrid("reset");
        $('#shipper_company2').combogrid(company_config);

        company_config.toolbar = '#' + create_company_tool('consignee_company2', 'consignee_company2') + '_div';
        company_config.onSelect = function(index, row){
            if(row.company_address == '') row.company_address = ' ';
            if(row.company_contact == '') row.company_contact = ' ';
            if(row.company_telephone == '') row.company_telephone = ' ';
            if(row.company_email == '') row.company_email = ' ';
            $('#consignee_company').val(row.company_name);
            $('#consignee_address').val(row.company_address);
            $('#consignee_contact').val(row.company_contact);
            $('#consignee_telephone').val(row.company_telephone);
            $('#consignee_email').val(row.company_email);
        };
        company_config.value = '<?= es_encode($consignee_company);?>';
        $('#consignee_company2').combogrid("reset");
        $('#consignee_company2').combogrid(company_config);

        company_config.toolbar = '#' + create_company_tool('notify_company2', 'notify_company2') + '_div';
        company_config.onSelect = function(index, row){
            if(row.company_address == '') row.company_address = ' ';
            if(row.company_contact == '') row.company_contact = ' ';
            if(row.company_telephone == '') row.company_telephone = ' ';
            if(row.company_email == '') row.company_email = ' ';
            $('#notify_company').val(row.company_name);
            $('#notify_address').val(row.company_address);
            $('#notify_contact').val(row.company_contact);
            $('#notify_telephone').val(row.company_telephone);
            $('#notify_email').val(row.company_email);
        };
        company_config.value = '<?= es_encode($notify_company);?>';
        $('#notify_company2').combogrid("reset");
        $('#notify_company2').combogrid(company_config);

        $.each($('.keydown_search'), function (i,it) {
            $(it).textbox('textbox').bind('keydown', function(e){keydown_search(e, $(it).attr('t_id'));});
        });


        $('#f').form({
            url: "/biz_consol/update_data_ajax/<?php echo $id;?>",
            onChange: function (target) {
                console.log(target);
                if($(target).attr('id') !== 'oversea_cus_id'){
                    is_update = true;
                }
            },
            onSubmit: function(){
                is_update = false;
                var vv = $('#carrier_ref_tip').text();
                if(vv !== ''){
                    alert("船公司单号已存在！");
                    return false;
                }
                var vv = $('#agent_ref_tip').text();
                if(vv !== ''){
                    alert("代理单号已存在！");
                    return false;
                }

                $.each($('textarea'), function(index, item){
                    var str = $(this).val();
                    var id = $(this).prop('id');
                    var ids = {
                        agent_address: ['agent_address', '<?= lang('代理地址');?>'],
                        agent_contact: ['agent_contact', '<?= lang('代理联系人');?>'],
                        agent_telephone: ['agent_telephone', '<?= lang('代理电话');?>'],
                        shipper_address: ['shipper_address', '<?= lang('发货人地址');?>'],
                        consignee_address: ['consignee_address', '<?= lang('收货人地址');?>'],
                        notify_address: ['notify_address', '<?= lang('通知人地址');?>'],
                        description: ['description', '<?= lang('英文货物描述');?>'],
                        mark_nums: ['mark_nums', '<?= lang('货物唛头');?>'],
                    };
                    str = qzb(str);
                    $(this).val(str);
                    var newstr = $(this).val();
                    if(ids[id] !== undefined){
                        var special_char = checkName(newstr);
                    }else{
                        var special_char = checkName(newstr, 1);
                    }

                    if(special_char !== ''){
                        $.messager.alert('error', ids[id][1] + '检测到有以下特殊字符:<br/>' + special_char);
                        is_special_char = id;
                        return false;
                    }else{
                        is_special_char = '';
                        $(this).val($(this).val().toUpperCase());
                    }
                });
                if(is_special_char != ''){
                    // alert(is_special_char + '存在特殊字符');
                    return false;
                }
                $('.icon-save').attr('onclick', "");
                $('#f').append($('#price_flag_msg').clone().css('display', 'none'));
                ajaxLoading();
                return true;
            },
            success:function(res_json){
                ajaxLoadEnd();
                $('#f #price_flag_msg').remove();
                try{
                    var res = $.parseJSON(res_json);
                    if(res.code != 444) alert(res.msg);
                    $('#hs_code_tip').html('');
                    $('.icon-save').attr('onclick', "$('#f').form('submit');");
                    if(res.code == 0){
                        try{opener.$('#tt').edatagrid('reload');}catch (e) {}
                        location.href = '/biz_consol/edit/' + res.data.id;
                    }else if(res.code == 2){//代码tsl hscode错误
                        $('#hs_code_tip').html('<a href="/bsc_dict/read/hs_code/TSL" target="_blank" style="color:red;">hs_code</a>');
                    }else if(res.code == 444){
                        // GET方式示例，其它方式修改相关参数即可
                        var form = $('<form></form>').attr('action','/other/ts_text').attr('method','POST').attr('target', '_blank');
                        var json_data = {};
                        json_data['text'] = res.text;
                        json_data['ts_str[]'] = res.ts_str;
                        json_data['msg'] = res.msg;
                        $.each(json_data, function (index, item) {
                            if(typeof item === 'string'){
                                form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                            }else{
                                $.each(item, function(index2, item2){
                                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                                })
                            }
                        });

                        form.appendTo('body').submit().remove();
                    }
                }catch (e) {
                    alert(res);
                    $('.icon-save').attr('onclick', "$('#f').form('submit');");
                }
            }
        });
        
        var now = Date.parse(new Date()) / 1000;
        
        //订舱船期范围设置
        $('#booking_ETD').datebox('calendar').calendar({
            //到时候不存在可选择值的，在这里false
            validator: function(date){
                //日期不能超过前后半年
                var star = now - 3600 * 24 * 183;
                var end = now + 3600 * 24 * 183;
                var date_time = Date.parse(date) / 1000;
                if (date_time >= star && date_time <= end) {
                    return true;
                } else {
                    return false;
                }
            }
        });

        //2022-06-28 获取选中tab tb
        <?php if(!empty($tab_title)):?>
        $("#tb").tabs('select','<?=$tab_title;?>');
        <?php endif;?>

    })

    function user_info(this_id){
        var val = $('#' + this_id).combobox('getValue');
        window.open('/bsc_user/person_info/' + val);
    }

    function changeClient(data){
        var oversea_cus_ids = data['oversea_cus_ids'];
        if(oversea_cus_ids === undefined || oversea_cus_ids == '')oversea_cus_ids = 0;
        $('#oversea_cus_id').combobox('reload', '/bsc_user/get_data_role/oversea_cus?userIds=' + oversea_cus_ids + '&status=0');

        var biz_type = $('#biz_type').val();
        $('#shipper_company2').combogrid('grid').datagrid('reload', '/biz_company/get_option/shipper/'+data.client_code + '/' + biz_type);
        $('#consignee_company2').combogrid('grid').datagrid('reload', '/biz_company/get_option/consignee/'+data.client_code + '/' + biz_type);
        $('#notify_company2').combogrid('grid').datagrid('reload', '/biz_company/get_option/notify/'+data.client_code + '/' + biz_type);
    }
    function goEditUrl(url){
        var clientCode = $('#agent_code').val();
        if(!clientCode){
            alert('agentCode is empty');
        }else{
            window.open(url+'/'+clientCode)
        }
    }

    function tb_shipment(field){
        $.messager.confirm('确认对话框', '是否确认提交？', function(r) {
            if (r) {
                // 退出操作;
                $.ajax({
                    type:'POST',
                    url:'/biz_consol/tb_shipment_by_consol_field',
                    data:{
                        id:'<?= $id;?>',
                        field:field,
                    },
                    success:function (res_json) {
                        var res;
                        try {
                            res = $.parseJSON(res_json);
                        }catch (e) {

                        }
                        if(res == undefined){
                            $.messager.alert('Tips', res_json);
                        }else{
                            $.messager.alert('Tips', res.msg);
                            if(res.code == 0){
                            }
                        }
                    },
                });
            }
        });
    }
    
    /**
     * 跳转查看被绑定日志
     */
    function read_combine() {
        window.open("/biz_consol/combine_log?id=<?= $id;?>");
    }
    function status_select(rec){
        if(rec.value == 'combine into'){
            $('.change_binding').css('display', 'inline-block');
        }else{
            $('.change_binding').css('display', 'none');
        }
        if(rec.value == 'pending'){
            $('.publish_flag').css('display', 'inline-block');
        }else{
            $('.publish_flag').css('display', 'none');
        }
        if(rec.value == 'cancel'){
            $('.cancel_msg').css('display', 'inline-block');
        }else{
            $('.cancel_msg').css('display', 'none');
        }
    }

    function louzhuang_select(rec) {
        if(rec.value == '漏装' || rec.value == '改配'){
            var remark = $('#remark').val();
            var vessel = $('#vessel').combobox('getValue');
            var voyage = $('#voyage').combobox('getValue');
            var carrier_ref = $('#carrier_ref').textbox('getValue');
            if(remark !== '') remark += ',';
            if(vessel != '' && voyage != ''){
                remark += '旧船名航次:' + vessel + ' V.' + voyage + ' 旧MBL:' + carrier_ref;
                $('#remark').val(remark);
                $('#vessel').combobox('setValue', '');
                $('#voyage').combobox('setValue', '');
                $.messager.alert('Tips', '旧船名已放入备注,请填入新船名');
            }
        }
    } 
    
    function biz_type_select(res){ 
        var trans_carrier = $('#trans_carrier').combobox('getValue');

        $('#biz_type').val(res.biz_type);
        $('#trans_mode').val(res.trans_mode);

        if(res.trans_mode == 'AIR'){
            $('#trans_tool').combobox('select', 'AIR');
            $('#flight_number').html('<?= lang('flight_number');?>');
            $('#vessel').textbox({ readonly: true });
            document.getElementById('update_vessel').style.display = 'none';
        }else{
            $('#trans_tool').combobox('select', 'SEA');
            $('#flight_number').html('<?= lang('update_voyage');?>');
            $('#vessel').textbox({ readonly: false });
            document.getElementById('update_vessel').style.display = '';
        }
        var trans_tool = $('#trans_tool').combobox('getValue');

        //重新加载
        $('#trans_origin').combogrid('grid').datagrid('load', "/biz_port/get_option?type=" + trans_tool + '&carrier=' + trans_carrier + '&limit=true');
        $('#trans_discharge').combogrid('grid').datagrid('reload', "/biz_port/get_option?type=" + trans_tool + '&carrier=' + trans_carrier + '&limit=true');

        $('#trans_destination').combogrid('grid').datagrid('reload', "/biz_port/get_discharge?type=" + trans_tool + '&carrier=' + trans_carrier);
        
        if(res.trans_mode == 'FCL'){
            $('#creditor').combobox('readonly', true);
            // $('#sailing_code').combobox('readonly', true);
        }else{
            $('#creditor').combobox('readonly', false);
            // $('#sailing_code').combobox('readonly', false);
        }
        
        if(res.trans_mode == 'LCL'){
            $('#LCL_type').parent('td').parent('tr').css('display', '');
        }else{
            $('#LCL_type').parent('td').parent('tr').css('display', 'none');
        }

        //HBL 且LCL时,显示自拼箱按钮
        if($('#agent_company').prop('disabled') !== false &&  res.trans_mode === 'LCL') {
            $('.lcl_zpx').removeClass('hide');
        }else {
            $('.lcl_zpx').addClass('hide');
        }

        if(res.trans_mode == 'FCL' || res.trans_mode == 'LCL'){
            add_box();
        }else{
            del_box();
        }
    }

    function cutoff_select(rec){
        var trans_ETA = $('#trans_ETA').datetimebox('getValue');
        if(trans_ETA == ''){
            $.messager.alert('Tips', '系统无ETA时间');
            return;
        }
        var val = rec.value;
        if(val == ''){
            return false;
        }
        var time = new Date(trans_ETA).getTime(); 
        
        
        time -= rec.value * 3600 * 1000;
        var date = new Date(time);
         
        var val = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes();
        $('#carrier_cutoff').datetimebox('setValue', val);
    }
</script>

<style>
    input:read-only,select:read-only,textarea:read-only
    {
        background-color: #F5F5F5;
        cursor: default;
    }
    input:disabled,select:disabled,textarea:disabled
    {
        background-color: #d6d6d6;
        cursor: default;
    }
    .free_svr_type{
        width: 100px;
    }
    .free_svr_days{
        width: 90px;
    }
    .box_info_num{
        width: 100px;
    }
    .box_info_size{
        width: 100px;
    }
    .form_table{
    }
    body{
        background-color: #FAFAD2;
    }
    .change_binding{
        display: none;
    }
    .close_ens_date_div{
        display: inline-block;
    }
    .sum{
        color: red;
    }
    .hide{
        display: none;
    }
</style>

<body style="margin-left:10px;">
<table width="100%">
    <tr>
        <td width="80px;">
            <h2> Edit </h2>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td width="380px;">
            Job No: <?php echo $job_no;?> &nbsp;&nbsp;&nbsp;&nbsp;  BOOKING NO: <?php echo $carrier_ref;?>
            <br />
            <?php
            $lock_lv_span = "";
            $lock_lv_title = '';
            if($shipment_lock_lv == 1)$lock_lv_title = '操作锁';
            if($shipment_lock_lv == 2)$lock_lv_title = '销售锁';
            if($shipment_lock_lv == 3)$lock_lv_title = '财务锁';
            if($shipment_lock_lv != 0){
                $lock_lv_span = "[S{$shipment_lock_lv}锁]";
            }else{
                $lock_lv_title = '';
                if($lock_lv != 0) $lock_lv_title = "carrier cost锁";
                if($lock_lv != 0) $lock_lv_span = "[C{$lock_lv}锁]";
            }
            ?>
            <?php if($lock_lv_span != ''){ ?>
                <a href="javascript:void;" class="easyui-tooltip" style="color:violet;font-weight:bold;" data-options="content: function(){
                var str = '<div>';
                <?php foreach ($lock_log as $ll_row){
                    $this_lock_lv_title = '';
                    if($lock_lv == 0) $this_lock_lv_title = "C0锁";
                    if($lock_lv != 0) $this_lock_lv_title = "carrier cost锁";
                    ?>
                str += '<?= $this_lock_lv_title;?> <?= $ll_row['user_name'];?> <?= $ll_row['op_time_max']?>';
                <?php } ?>
                if(str == '<div>') str += '<?= $lock_lv_title;?>';
                str += '</div>';
                return str;
            },onShow: function(){$(this).tooltip('arrow').css('left', 20);$(this).tooltip('tip').css('left', $(this).offset().left); },"><?= $lock_lv_span;?></a>
            <?php } ?>
        </td>
        <td width="30px;"> &nbsp;&nbsp;&nbsp;&nbsp; </td>
		<td style="background-color:#0099cc;display: flex;align-items: end;" rowspan="2">
			<iframe src="/biz_consol/basic_steps/<?=$id?>" style="width: 100%;height:50px" frameborder="0"></iframe>
			&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
    </tr>
	<tr>
		<td></td>
		<td></td>
		<td colspan="2">
			<?php if($from_db != '' && $from_id_no != 0):?>
				<span><?=$biz_type == 'export' ? 'To' : 'From'?> : <?=$from_db?> &emsp; ID: <?=$from_id_no?></span>
			<?php endif;?>
		</td>
	</tr>
</table>
<div  id="tb" class="easyui-tabs" style="width:100%;height:850px">
    <div title="Basic Registration" style="padding:1px">
        <form id="f" name="f" method="post">
            <div id="el" class="easyui-layout">
                <input type="hidden" name="job_no" <?php echo in_array("job_no",$G_userBscEdit) ? '' : 'readonly'; ?> value="<?php echo $job_no;?>">
                <div data-options="region:'west',tools:'#tt'" title="Basic" style="width:350px;height: 70%;">
                    <div class="easyui-accordion" data-options="multiple:true" style="width:100%;">
                        <div title="agent" style="padding:10px;">
                            <select <?php echo in_array("agent_company",$G_userBscEdit) && !$agent_lock ? '' : 'readonly'; ?> id="agent_company" class="easyui-combogrid" name="agent_company" style="width:255px;">
                            </select>
                            <a href="javascript:void(0);" onclick="goEditUrl('/biz_client/edit')"><?= lang('E');?></a>
                            |
                            <a href="javascript: reload_select('agent');"><?= lang('R');?></a> 
                            
                            <script>
				                /**
                                 * 自出单填入值
                                 */
                                function lcl_zpx_click() {
                                    $('#agent_company').combogrid('setValue', 'OPEN HOUSEBILL');
                                    $('#oversea_cus_id').combobox('setValue', '0').combobox('reload', '/bsc_user/get_data_role/oversea_cus?userIds=0&status=0');
                                    $('#agent_code').val('OPEN HOUSEBILL');
                                    $('#agent_address').val(' ');
                                    $('#agent_email').textbox('setValue', ' ');
                                    $('#agent_contact').val(' ');
                                    $('#agent_telephone').val(' ');
                                }
                            </script>
                            <input type="hidden" <?php echo in_array("agent_code",$G_userBscEdit) && !$agent_lock ? '' : 'readonly'; ?> name="agent_code" id="agent_code" value="<?php echo $agent_code;?>"></input>
                            <br>
                            <?php if(isset($userList['oversea_cus'])){?>
                                <?php echo lang('oversea_cus'); ?>
                                <br><select <?php echo in_array('oversea_cus',$G_userBscEdit) && !$agent_lock  ? '' : 'readonly'; ?> id="oversea_cus_id" class="easyui-combobox"  editable="false" name="oversea_cus_id" style="width:255px;" data-options="
                                valueField:'id',
                                textField:'name',
                                url:'/bsc_user/get_data_role/oversea_cus?userIds=0&status=0',
                                value:'<?php echo $oversea_cus_id;?>',
                                onSelect: function(rec){
                                    $('#oversea_cus_group').val(rec.group);
                                },
                                onLoadSuccess:function(){
                                    var val = $(this).combobox('getValue');
                                    var data = $(this).combobox('getData');
                                    var reset = true;
                                    if(val == 0 && data.length == 2){
                                        $(this).combobox('setValue', data[1].id);
                                        $('#oversea_cus_group').val(data[1].group);
                                    }else{

                                    }
                                },
                            ">
                                </select>
                                <input type="hidden" <?php echo in_array('oversea_cus',$G_userBscEdit) && !$agent_lock  ? '' : 'readonly'; ?> id="oversea_cus_group" name="oversea_cus_group" style="width:255px;" value="<?php echo $oversea_cus_group;?>">
                                <a href="javascript:void(0);" onclick="user_info('oversea_cus_id')" title='查看该用户详情'><?= getUserName($oversea_cus_id);?></a>
                                <!--<a href="javascript:void(0);" class="lcl_zpx <?= $trans_mode == 'LCL' ? '' : 'hide';?>" onclick="lcl_zpx_click()"><?= lang('自出单');?></a>-->

                            <?php } ?>
                            <br><?= lang('代理邮箱');?>
                            <br><input class="easyui-textbox" <?php echo in_array("agent_address",$G_userBscEdit) && !$agent_lock ? '' : 'readonly'; ?> name="agent_email" id="agent_email" value="<?php echo $agent_email;?>" style="width:255px;"/>
                            <br><?= lang('update_agent_address');?>:
                            <br><textarea name="agent_address" <?php echo in_array("agent_address",$G_userBscEdit) && !$agent_lock ? '' : 'readonly'; ?> id="agent_address" style="height:60px;width:300px;"><?php echo $agent_address;?></textarea>
                            <br><?= lang('update_agent_contact');?>:
                            <br><textarea name="agent_contact" <?php echo in_array("agent_contact",$G_userBscEdit) && !$agent_lock ? '' : 'readonly'; ?> id="agent_contact" style="height:40px;width:300px;"><?php echo $agent_contact;?></textarea>
                            <br><?= lang('update_agent_telephone');?>:
                            <br><textarea name="agent_telephone" <?php echo in_array("agent_telephone",$G_userBscEdit) && !$agent_lock ? '' : 'readonly'; ?> id="agent_telephone" style="height:40px;width:300px;"><?php echo $agent_telephone;?></textarea>
                        </div>
                        <div title="Shipper"  style="overflow:auto;padding:10px;">
                            <a href="javascript:void(0);" class="easyui-tooltip" data-options="
                                content: $('<div></div>'),
                                onShow: function(){
                                    $(this).tooltip('arrow').css('left', 20);
                                    $(this).tooltip('tip').css('left', $(this).offset().left);
                                    $(this).tooltip('update', $('#shipper_company').val());
                                },
                                hideDelay:1000,
                            ">
                                <select id="shipper_company2" <?php echo in_array("shipper_company",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-combogrid" style="width:255px;"></select>
                            </a>
                            <input type="hidden" <?php echo in_array("shipper_company",$G_userBscEdit) ? '' : 'readonly'; ?> id="shipper_company" name="shipper_company">
                            <a href="javascript: goEditUrl('/biz_company/index/shipper');"><?= lang('E');?></a>
                            /
                            <a href="javascript: reload_select('shipper_company2');"><?= lang('R');?></a>
                            <br><?= lang('update_shipper_address');?>:
                            <br><textarea <?php echo in_array("shipper_address",$G_userBscEdit) ? '' : 'readonly'; ?> name="shipper_address" id="shipper_address"  style="height:60px;width:300px;"><?php echo $shipper_address;?></textarea>
                            <br><?= lang('update_shipper_contact');?>: <input type="checkbox" name="show_fields[]" <?php if(in_array('shipper_contact', explode(',', $show_fields)))echo 'checked'; ?> value="shipper_contact">
                            <br><textarea <?php echo in_array("shipper_contact",$G_userBscEdit) ? '' : 'readonly'; ?> name="shipper_contact" id="shipper_contact"  style="height:40px;width:300px;"><?php echo $shipper_contact;?></textarea>
                            <br><?= lang('update_shipper_telephone');?>: <input type="checkbox" name="show_fields[]" <?php if(in_array('shipper_telephone', explode(',', $show_fields)))echo 'checked'; ?> value="shipper_telephone">
                            <br><textarea <?php echo in_array("shipper_telephone",$G_userBscEdit) ? '' : 'readonly'; ?> name="shipper_telephone" id="shipper_telephone" style="height:40px;width:300px;"><?php echo $shipper_telephone;?></textarea><br>
                            <?= lang('update_shipper_email');?>: <input type="checkbox" name="show_fields[]" <?php if(in_array('shipper_email', explode(',', $show_fields)))echo 'checked'; ?> value="shipper_email">
                            <br><textarea <?php echo in_array("shipper_email",$G_userBscEdit) ? '' : 'readonly'; ?> name="shipper_email" id="shipper_email" style="height:40px;width:300px;"><?php echo $shipper_email;?></textarea>
                        </div>
                        <div title="Consignee" style="padding:10px;">
                            <a href="javascript:void(0);" class="easyui-tooltip" data-options="
                                content: $('<div></div>'),
                                onShow: function(){
                                    $(this).tooltip('arrow').css('left', 20);
                                    $(this).tooltip('tip').css('left', $(this).offset().left);
                                    $(this).tooltip('update', $('#consignee_company').val());
                                },
                                hideDelay:1000,
                            ">
                                <select <?php echo in_array("consignee_company",$G_userBscEdit) ? '' : 'readonly'; ?> id="consignee_company2" class="easyui-combogrid" style="width:255px;"></select>
                            </a>
                            <input type="hidden" <?php echo in_array("consignee_company",$G_userBscEdit) ? '' : 'readonly'; ?> id="consignee_company" name="consignee_company">
                            <a href="javascript: goEditUrl('/biz_company/index/consignee');"><?= lang('E');?></a>
                            /
                            <a href="javascript: reload_select('consignee_company2');"><?= lang('R');?></a>
                            <br><?= lang('update_consignee_address');?>:
                            <br><textarea <?php echo in_array("consignee_address",$G_userBscEdit) ? '' : 'readonly'; ?>  name="consignee_address" id="consignee_address"  style="height:60px;width:300px;"><?php echo $consignee_address;?></textarea>
                            <br><?= lang('update_consignee_contact');?>: <input type="checkbox" name="show_fields[]" <?php if(in_array('consignee_contact', explode(',', $show_fields)))echo 'checked'; ?> value="consignee_contact">
                            <br><textarea <?php echo in_array("consignee_contact",$G_userBscEdit) ? '' : 'readonly'; ?> name="consignee_contact" id="consignee_contact"  style="height:40px;width:300px;" ><?php echo $consignee_contact;?></textarea>
                            <br><?= lang('update_consignee_telephone');?>: <input type="checkbox" name="show_fields[]" <?php if(in_array('consignee_telephone', explode(',', $show_fields)))echo 'checked'; ?> value="consignee_telephone">
                            <br><textarea <?php echo in_array("consignee_telephone",$G_userBscEdit) ? '' : 'readonly'; ?> name="consignee_telephone" id="consignee_telephone"  style="height:40px;width:300px;"><?php echo $consignee_telephone;?></textarea>
                            <br><?= lang('update_consignee_email');?>: <input type="checkbox" name="show_fields[]" <?php if(in_array('consignee_email', explode(',', $show_fields)))echo 'checked'; ?> value="consignee_email">
                            <br><textarea <?php echo in_array("consignee_email",$G_userBscEdit) ? '' : 'readonly'; ?> name="consignee_email" id="consignee_email"  style="height:40px;width:300px;" ><?php echo $consignee_email;?></textarea>
                        </div>
                        <div title="Notify Party" style="padding:10px;">
                            <a href="javascript:void(0);" class="easyui-tooltip" data-options="
                                content: $('<div></div>'),
                                onShow: function(){
                                    $(this).tooltip('arrow').css('left', 20);
                                    $(this).tooltip('tip').css('left', $(this).offset().left);
                                    $(this).tooltip('update', $('#notify_company').val());
                                },
                                hideDelay:1000,
                            ">
                                <select <?php echo in_array("notify_company",$G_userBscEdit) ? '' : 'readonly'; ?> id="notify_company2" class="easyui-combogrid" style="width:255px;"></select>
                            </a>
                            <input type="hidden" <?php echo in_array("notify_company",$G_userBscEdit) ? '' : 'readonly'; ?> id="notify_company" name="notify_company">
                            <a href="javascript: goEditUrl('/biz_company/index/notify');"><?= lang('E');?></a>
                            /
                            <a href="javascript: reload_select('notify_company2');"><?= lang('R');?></a>
                            <br><?= lang('update_notify_address');?>:
                            <br><textarea <?php echo in_array("notify_address",$G_userBscEdit) ? '' : 'readonly'; ?> name="notify_address" id="notify_address"  style="height:60px;width:300px;" ><?php echo $notify_address;?></textarea>
                            <br><?= lang('update_notify_contact');?>: <input type="checkbox" name="show_fields[]" <?php if(in_array('notify_contact', explode(',', $show_fields)))echo 'checked'; ?> value="notify_contact">
                            <br><textarea <?php echo in_array("notify_contact",$G_userBscEdit) ? '' : 'readonly'; ?> name="notify_contact" id="notify_contact"  style="height:40px;width:300px;" ><?php echo $notify_contact;?></textarea>
                            <br><?= lang('update_notify_telephone');?>: <input type="checkbox" name="show_fields[]" <?php if(in_array('notify_telephone', explode(',', $show_fields)))echo 'checked'; ?> value="notify_telephone">
                            <br><textarea <?php echo in_array("notify_telephone",$G_userBscEdit) ? '' : 'readonly'; ?> name="notify_telephone" id="notify_telephone"  style="height:40px;width:300px;" ><?php echo $notify_telephone;?></textarea>
                            <br><?= lang('update_notify_email');?>: <input type="checkbox" name="show_fields[]" <?php if(in_array('notify_email', explode(',', $show_fields)))echo 'checked'; ?> value="notify_email">
                            <br><textarea <?php echo in_array("notify_email",$G_userBscEdit) ? '' : 'readonly'; ?> name="notify_email" id="notify_email"  style="height:40px;width:300px;" ><?php echo $notify_email;?></textarea>
                        </div>
                    </div>
                    <div id="tt">
                        <a href="javascript:void(0)" class="icon-save" onclick="<?php if(!$r_e && $master_consol == '') echo '$(\'#f\').form(\'submit\')'; else echo "javascript:alert('当前consol无法修改');";?>" title="save" style="margin-right:15px;"></a>
                    </div>
                </div>
                <div data-options="region:'center',title:'Details'" style="width:70%;height: 70%;">
                    <table style="width: 100%;height:510px;" border="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                                <table class="form_table" border="0" cellspacing="0">
                                    <tr>
                                        <td><?= lang('update_biz_type');?></td>
                                        <td>
                                            <select class="easyui-combobox" id="biz_type1" <?php echo in_array("biz_type",$G_userBscEdit) ? '' : 'readonly'; ?> style="width:180px;" data-options="
                                                editable:false,
                                                valueField:'name',
                                                textField:'name',
                                                url:'/bsc_dict/get_biz_type',
                                                value: '<?php echo $biz_type;?> <?php echo $trans_mode;?>',
                                                onSelect: function(res){
                                                    biz_type_select(res);
                                                },
                                            ">
                                            </select>
                                            <input type="hidden" name="biz_type" id="biz_type" value="<?= $biz_type;?>">
                                            <input type="hidden" id="trans_mode" name="trans_mode" value="<?php echo $trans_mode;?>">
                                            <select <?php echo in_array('trans_tool', $G_userBscEdit) ? '' : 'readonly'; ?> name="trans_tool" id="trans_tool" class="easyui-combobox" style="width:69px;" data-options="
                                                editable:false,
                                                valueField:'value',
                                                textField:'name',
                                                url:'/bsc_dict/get_option/trans_tool',
                                                value:'<?= $trans_tool;?>',
                                            ">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr <?php if($trans_mode !== 'LCL') echo "style=\"display: none;\"";?>>
                                        <td><?= lang('LCL_type');?></td>
                                        <td>
                                            <select class="easyui-combobox" name="LCL_type" id="LCL_type" <?php echo in_array("LCL_type",$G_userBscEdit) ? '' : 'readonly'; ?> style="width:255px;" data-options="valueField:'value',
                                                textField:'name',
                                                editable: false,
                                                url:'/bsc_dict/get_option/LCL_type',
                                                value:'<?php echo $LCL_type;?>',
                                            ">
                                            </select>
                                        </td>
                                    </tr>
                                    <?php if(isset($box_info) && !empty($box_info)){foreach($box_info as $key => $row){
                                        $this_btn = '<a class="easyui-linkbutton" onclick="del_box(this)" iconCls="icon-remove"></a>';
                                        $this_lang = '';
                                        if($key == 0){
                                            $this_btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
                                            $this_lang = lang('box_info');
                                        }
                                        ?>

                                        <tr class="add_box">
                                            <td><?= $this_lang;?></td>
                                            <td>
                                                <select <?php echo in_array("box_info",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-combobox box_info_size" name="box_info[size][]" data-options="
                                                valueField:'value',
                                                textField:'value',
                                                url:'/bsc_dict/get_option/container_size',
                                                value: '<?= $row['size']?>',
                                                onHidePanel: function() {
                                                    var valueField = $(this).combobox('options').valueField;
                                                    var val = $(this).combobox('getValue');
                                                    var allData = $(this).combobox('getData');
                                                    var result = true;
                                                    for (var i = 0; i < allData.length; i++) {
                                                        if (val == allData[i][valueField]) {
                                                            result = false;
                                                        }
                                                    }
                                                    if (result) {
                                                        $(this).combobox('clear');
                                                    }
                                                }
                                            "></select>
                                                X
                                                <input <?php echo in_array("box_info",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-numberbox box_info_num"  name="box_info[num][]" value="<?= $row['num'];?>">
                                                <?= $this_btn;?>
                                            </td>
                                        </tr>
                                    <?php }}else{
                                        if($trans_mode == 'FCL'){
                                            $this_btn = '<a class="easyui-linkbutton" onclick="add_box()" iconCls="icon-add"></a>';
                                            $this_lang = lang('box_info');
                                            ?>
                                            <tr class="add_box">
                                                <td><?= $this_lang;?></td>
                                                <td>
                                                    <select <?php echo in_array("box_info",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-combobox box_info_size" name="box_info[size][]" data-options="
                                                    valueField:'value',
                                                    textField:'value',
                                                    url:'/bsc_dict/get_option/container_size',
                                                    onHidePanel: function() {
                                                        var valueField = $(this).combobox('options').valueField;
                                                        var val = $(this).combobox('getValue');
                                                        var allData = $(this).combobox('getData');
                                                        var result = true;
                                                        for (var i = 0; i < allData.length; i++) {
                                                            if (val == allData[i][valueField]) {
                                                                result = false;
                                                            }
                                                        }
                                                        if (result) {
                                                            $(this).combobox('clear');
                                                        }
                                                    }
                                                "></select>
                                                    X
                                                    <input <?php echo in_array("box_info",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-numberbox box_info_num"  name="box_info[num][]" value="">
                                                    <?= $this_btn;?>
                                                </td>
                                            </tr>
                                        <?php }}?>
                                    <tr>
                                        <td><?= lang('trans_carrier');?></td>
                                        <td>
                                            <select id="trans_carrier" class="easyui-combobox" <?php echo in_array("trans_carrier",$G_userBscEdit) ? '' : 'readonly'; ?>  name="trans_carrier" style="width:145px;" data-options="
                                                valueField:'client_code',
            								    textField:'client_name',
                                                url:'/biz_client/get_option/carrier',
                                                value:'<?php echo $trans_carrier;?>',
                                                onSelect: function(rec){
                                                    trans_carrier_select(rec);
                                                },
                                                onHidePanel: function() {
                                                    var valueField = $(this).combobox('options').valueField;
                                                    var val = $(this).combobox('getValue');
                                                    var allData = $(this).combobox('getData');
                                                    var result = true;
                                                    for (var i = 0; i < allData.length; i++) {
                                                        if (val == allData[i][valueField]) {
                                                            result = false;
                                                        }
                                                    }
                                                    if (result) {
                                                        $(this).combobox('clear');
                                                    }
                                                },
                                            ">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="trans_carrier_second <?= $trans_carrier == 'JXHY01' ? '' : 'hide';?>">
                                        <td><?= lang('trans_carrier_second');?></td>
                                        <td>
                                            <select <?php echo in_array("trans_carrier_second",$G_userBscEdit) ? '' : 'readonly'; ?> id="trans_carrier_second" class="easyui-combobox" editable="true" name="trans_carrier_second" style="width:255px;" data-options="
                                                valueField:'client_code',
                                                textField:'client_name',
                                                url:'/biz_client/get_option/carrier',
                                                value:'<?php echo $trans_carrier_second;?>',
                                                onHidePanel: function() {
                                                    var valueField = $(this).combobox('options').valueField;
                                                    var val = $(this).combobox('getValue');
                                                    var allData = $(this).combobox('getData');
                                                    var result = true;
                                                    for (var i = 0; i < allData.length; i++) {
                                                        if (val == allData[i][valueField]) {
                                                            result = false;
                                                        }
                                                    }
                                                    if (result) {
                                                        $(this).combobox('clear');
                                                    }
                                                },
                                            ">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_trans_origin');?></td>
                                        <td>
                                            <select class="easyui-combogrid" <?php echo in_array("trans_origin",$G_userBscEdit) ? '' : 'readonly'; ?> style="width: 80px;" name="trans_origin" id="trans_origin" data-options="
                                                panelWidth: '1000px',
                                                panelHeight: '300px',
                                                idField: 'port_code',              //ID字段
                                                textField: 'port_code',    //显示的字段
                                                url:'/biz_port/get_option?type=<?= $trans_mode;?>&carrier=<?= $trans_carrier;?>&limit=true&old=<?= $trans_origin;?>',
                                                striped: true,
                                                editable: false,
                                                pagination: false,           //是否分页
                                                toolbar : '#trans_origin_div',
                                                collapsible: true,         //是否可折叠的
                                                method: 'get',
                                                columns:[[
                                                    {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                                                    {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                                                    {field:'terminal',title:'<?= lang('码头');?>',width:200},
                                                ]],
                                                mode: 'remote',
                                                value: '<?= $trans_origin;?>',
                                                emptyMsg : '未找到相应数据!',
                                                onSelect: function(index, row){
                                                    if(row !== undefined){
                                                        $('#trans_origin_name').textbox('setValue', row.port_name);
                                                    }
                                                },
                                                onShowPanel:function(){
                                                    var opt = $(this).combogrid('options');
                                                    var toolbar = opt.toolbar;
                                                    combogrid_search_focus(toolbar, 1);
                                                } 
                                            ">
                                            </select>
                                            <input class="easyui-textbox" <?php echo in_array("trans_origin_name",$G_userBscEdit) ? '' : 'readonly'; ?> style="width: 170px;" name="trans_origin_name" id="trans_origin_name" value="<?= $trans_origin_name;?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_trans_discharge');?></td>
                                        <td>
                                            <select class="easyui-combogrid" <?php echo in_array("trans_discharge",$G_userBscEdit) ? '' : 'readonly'; ?> style="width: 80px;" name="trans_discharge" id="trans_discharge" data-options="
                                                panelWidth: '1000px',
                                                panelHeight: '300px',
                                                idField: 'port_code',              //ID字段
                                                textField: 'port_code',    //显示的字段
                                                url:'/biz_port/get_option?type=<?= $trans_mode;?>&carrier=<?= $trans_carrier;?>&limit=true&old=<?= $trans_discharge;?>',
                                                striped: true,
                                                editable: false,
                                                pagination: false,           //是否分页
                                                toolbar : '#trans_discharge_div',
                                                collapsible: true,         //是否可折叠的
                                                method: 'get',
                                                columns:[[
                                                    {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                                                    {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                                                    {field:'terminal',title:'<?= lang('码头');?>',width:200},
                                                ]],
                                                mode: 'remote',
                                                value: '<?= $trans_discharge;?>',
                                                emptyMsg : '未找到相应数据!',
                                                onSelect: function(index, row){
                                                    if(row !== undefined){
                                                        $('#trans_discharge_name').textbox('setValue', row.port_name);
                                                    }
                                                },
                                                onShowPanel:function(){
                                                    var opt = $(this).combogrid('options');
                                                    var toolbar = opt.toolbar;
                                                    combogrid_search_focus(toolbar, 1);
                                                } 
                                            ">
                                            </select>
                                            <input class="easyui-textbox" <?php echo in_array("trans_discharge_name",$G_userBscEdit) ? '' : 'readonly'; ?> style="width: 170px;" name="trans_discharge_name" id="trans_discharge_name" value="<?= $trans_discharge_name;?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_trans_destination');?></td>
                                        <td>
                                            <input class="easyui-textbox" <?php echo in_array("trans_destination_inner",$G_userBscEdit) ? '' : 'readonly'; ?> readonly style="width: 80px;" id="trans_destination_inner" name="trans_destination_inner" value="<?= $trans_destination_inner;?>">
                                            <select class="easyui-combogrid" <?php echo in_array("trans_destination",$G_userBscEdit) ? '' : 'readonly'; ?> style="width: 70px;" name="trans_destination" id="trans_destination" data-options="
                                                panelWidth: '1000px',
                                                panelHeight: '300px',
                                                idField: 'port_code',              //ID字段
                                                textField: 'port_code',    //显示的字段
                                                url:'/biz_port/get_discharge?type=<?= $trans_tool;?>&carrier=<?= $trans_carrier;?>&old=<?= $trans_destination;?>',
                                                striped: true,
                                                editable: false,
                                                pagination: false,           //是否分页
                                                toolbar : '#trans_destination_div',
                                                collapsible: true,         //是否可折叠的
                                                method: 'get',
                                                columns:[[
                                                    {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                                                    {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                                                    {field:'terminal',title:'<?= lang('码头');?>',width:200},
                                                ]],
                                                mode: 'remote',
                                                value: '<?= $trans_destination;?>',
                                                emptyMsg : '未找到相应数据!',
                                                onSelect: function(index, row){
                                                    if(row !== undefined){
                                                        $('#trans_destination_inner').textbox('setValue', row.inner_port_code);
                                                        $('#trans_destination_name').textbox('setValue', row.port_name);
                                                        $('#sailing_area').textbox('setValue', row.sailing_area);
                                                        $('#sailing').textbox('setValue', row.sailing);
                                                        $('#trans_destination_terminal').textbox('setValue', row.terminal);
                                                    }
                                                },
                                                onShowPanel:function(){
                                                    var opt = $(this).combogrid('options');
                                                    var toolbar = opt.toolbar;
                                                    combogrid_search_focus(toolbar, 1);
                                                } 
                                            ">
                                            </select>
                                            <input class="easyui-textbox" <?php echo in_array("trans_destination_name",$G_userBscEdit) ? '' : 'readonly'; ?> style="width: 97px;" name="trans_destination_name" id="trans_destination_name" value="<?= $trans_destination_name;?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('trans_destination_terminal');?></td>
                                        <td>
                                            <input class="easyui-textbox" <?php echo in_array("trans_destination_terminal",$G_userBscEdit) ? '' : 'readonly'; ?> style="width: 255px;" name="trans_destination_terminal" id="trans_destination_terminal" value="<?= $trans_destination_terminal;?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('booking_ETD');?> </td>
                                        <td>
                                            <input class="easyui-datebox" <?php echo in_array('booking_ETD', $G_userBscEdit) ? '' : 'readonly'?> data-options="panelWidth:800,panelHeight:600," name="booking_ETD" id="booking_ETD"  style="width:160px;" value="<?php echo $booking_ETD;?>"/>
                                            <a class="easyui-linkbutton" href="javascript:void(0);" onclick="tb_shipment('booking_ETD')">push to Shipment</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('customer_booking');?></td>
                                        <td>
                                            <select <?php echo in_array("customer_booking",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-combobox" editable="false" name="customer_booking" id="customer_booking" style="width:255px;" data-options="
                                                value: '<?= $customer_booking;?>',
                                            ">
                                                <option value="1"><?= lang('customer self');?></option>
                                                <option value="2"><?= lang('contract no');?></option>
                                                <option value="0"><?= lang('our company booking');?></option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr <?= $booking_error ? "style=\"background-color: red;\" title=\"shipment订舱勾选错误\"" : '';?>  >
                                        <td>
                                            <?= lang('booking_agent');?>
                                        </td>
                                        <td>
                                            <select <?php echo in_array("creditor",$G_userBscEdit) ? '' : 'readonly'; ?> id="creditor" class="easyui-combobox"  name="creditor" style="width:255px;" data-options="
                								valueField:'client_code',
                								textField:'company_name',
                								url:'/biz_client/get_option/booking_agent',
                								value:'<?php echo $creditor;?>',
                								onSelect:function(rec){
                								    creditor_select(rec);
                								},
                								onHidePanel: function() {
                                                    var valueField = $(this).combobox('options').valueField;
                                                    var val = $(this).combobox('getValue');
                                                    var allData = $(this).combobox('getData');
                                                    var result = true;
                                                    for (var i = 0; i < allData.length; i++) {
                                                        if (val == allData[i][valueField]) {
                                                            result = false;
                                                        }
                                                    }
                                                    if (result) {
                                                        $(this).combobox('clear');
                                                    }
                                                }
            								">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_status');?></td>
                                        <td>
                                            <select class="easyui-combobox" name="status" id="status" <?php echo in_array("status",$G_userBscEdit) ? '' : 'readonly'; ?> style="width:100px;" data-options="
                                                editable:false,
                                                valueField:'value',
                                                textField:'name',
                                                url:'/bsc_dict/get_option/consol_status',
                                                value:'<?php echo $status;?>',
                                                onSelect: function(rec){
                                                    status_select(rec);
                                                },
                                                onLoadSuccess:function(){
                                                    var rec = {value:$(this).combobox('getValue')};
                                                    status_select(rec);
                                                }
                                                ">
                                            </select>
                                            <?php if($is_combine){ ?>
                                                <button type="button" onclick="read_combine()">被合并</button>
                                            <?php } ?>
                                            <div class="change_binding">
                                                <input type="text" id="master_consol" class="easyui-textbox" style="width: 100px" <?php echo in_array("master_consol",$G_userBscEdit) && $master_consol == '' ? '' : 'readonly'; ?> value="<?= $master_consol;?>">
                                                <?php if($master_consol == ''){ ?>
                                                    <button type="button" onclick="change_binding()">合并</button>
                                                <?php }else{ ?>
                                                    <button type="button" onclick="unbinding()">撤销</button>
                                                    <a href="javascript:void;" class="easyui-tooltip" data-options="
                                                        content: $('<div></div>'),
                                                        onShow: function(){
                                                            $(this).tooltip('arrow').css('left', 20);
                                                            $(this).tooltip('tip').css('left', $(this).offset().left);
                                                        },
                                                        onUpdate: function(cc){
                                                            cc.panel({
                                                                width: 300,
                                                                height: 'auto',
                                                                border: false,
                                                                href: '/bsc_help_content/help/combine_help'
                                                            });
                                                        }
                                                    "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
                                                <?php } ?>
                                            </div>
                                            <div class="publish_flag">
                                                <select class="easyui-combobox" id="publish_flag" name="publish_flag" style="width:100px" <?php echo in_array("publish_flag",$G_userBscEdit) ? '' : 'readonly'; ?> data-options="
                                                    editable:false,
                                                    value: '<?= $publish_flag;?>',
                                                ">
                                                    <option value="0"><?= lang('不公开');?></option>
                                                    <option value="1"><?= lang('公开');?></option>
                                                </select>
                                            </div>
                                            <div class="cancel_msg">
                                                <!--<button id="gaipei_button" type="button" onclick="gaipei()">改配</button>-->
                                                <script>
                                                    function gaipei(){
                                                        var vv = $('#cancel_msg').combobox('getValue');
                                                        if(vv != 2){
                                                            alert("退关理由请选择改配");
                                                            return false;
                                                        }
                                                        $.messager.confirm('确认','是否确认改配?',function(r) {
                                                            if (r) {
                                                                $.ajax({
                                                                    type: 'POST',
                                                                    url: '/biz_consol/gaipei',
                                                                    data:{
                                                                        id: '<?= $id;?>',
                                                                    },
                                                                    dataType: 'json',
                                                                    success:function (res) {
                                                                        $.messager.alert('Tips', res.msg);
                                                                        if(res.code == 0){ 
                                                                            window.open('/biz_consol/add/<?= $id?>?gaipei_old_id=<?= $id?>');
                                                                        }else{
                                                                        }
                                                                    },
                                                                    error:function (e) {
                                                                        $.messager.alert('Tips', e.responseText);
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                </script>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('AP code');?></td>
                                        <td>
                                            <input class="easyui-textbox" <?php echo in_array("AP_code",$G_userBscEdit) ? '' : 'readonly'; ?> name="AP_code" id="AP_code"  style="width:255px;" value="<?= $AP_code;?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('AP remark');?></td>
                                        <td>
                                            <!-- 这里的输入框改为下拉框，取值根据当前的船公司和shipper来决定 -->
                                            <textarea <?php echo in_array("AP_remark",$G_userBscEdit) ? '' : 'readonly'; ?> name="AP_remark" id="AP_remark" style="height:60px;width:248px;"><?php echo $AP_remark;?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_carrier_ref');?></td>
                                        <td>
                                            <input <?php echo in_array("carrier_ref",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-textbox"  name="carrier_ref" id="carrier_ref"  style="width:227px;" value="<?php echo $carrier_ref;?>"  data-options="events:{keyup: KeyUpUpper}"/>
                                            <span id="carrier_ref_tip" style="color: red"></span>
                                            <script>
                                                function KeyUpUpper(e) {
                                                    this.value = this.value.toUpperCase();
                                                }
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('放单方式');?></td>
                                        <td>
                                             <select <?php echo in_array("release_type",$G_userBscEdit) ? '' : 'readonly'; ?> id="release_type"  class="easyui-combobox"  editable="false" name="release_type" style="width:255px;" data-options="
                								valueField:'value',
                								textField:'value',
                								url:'/bsc_dict/get_option/release_type',
                								value:'<?php echo $release_type;?>',
            								">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_payment');?></td>
                                        <td>
                                            <select <?php echo in_array("payment",$G_userBscEdit) ? '' : 'readonly'; ?> id="payment" class="easyui-combobox"  editable="false" name="payment" style="width:155px;" data-options="
                                                valueField:'value',
                                                textField:'lang',
                                                url:'/bsc_dict/get_option/payment',
                                                onSelect: function(rec){
                                                    if(rec.value=='E') {
                                                        $('#payment_third').textbox('readonly', false);
                                                    }else{
                                                        $('#payment_third').textbox('readonly', true).textbox('clear');
                                                    }
                                                },
                                                value:'<?php echo $payment;?>'
                                            ">
                                            </select>
                                            <input <?php echo in_array("payment_third",$G_userBscEdit) ? '' : 'readonly'; ?> <?= $payment !== 'E' ? 'readonly' : '';?> class="easyui-textbox"  name="payment_third" id="payment_third"  style="width:95px;" value="<?php echo $payment_third;?>" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_agent_ref');?> </td>
                                        <td>
                                            <input <?php echo in_array("agent_ref",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-textbox"  name="agent_ref" id="agent_ref"  style="width:255px;" value="<?php echo $agent_ref;?>"/>
                                            <span id="agent_ref_tip" style="color: red"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('sailing_area');?></td>
                                        <td>
                                            <select class="easyui-combobox" name="sailing_area" id="sailing_area" style="width:80px;" data-options="
                                                readonly: true,
                                                valueField:'sailing_area',
        								        textField:'sailing_area',
                                                value:'<?= $sailing_area;?>'
                                            "></select>
                                            <select class="easyui-combobox" id="sailing" style="width:170px;" data-options="
                                                readonly: true,
                                                valueField:'sailing_area',
        								        textField:'sailing',
        								        value:'<?= $sailing_area;?>',
                                            "></select>
                                        </td>
                                    </tr>
                                    <tr id="update_vessel">
                                        <td>
                                            <?= lang('update_vessel');?>
                                        </td>
                                        <td>
                                            <select <?php echo in_array("vessel",$G_userBscEdit) ? '' : 'readonly'; ?> id="vessel" class="easyui-combobox"  editable="true" name="vessel" style="width:215px;" data-options="
                								valueField:'value',
                								textField:'name',
                								mode: 'remote',
                                                onBeforeLoad:function(param){
                                                    if($(this).combobox('getValue') == '')return false;
                                                    if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                                                },
                								onLoadSuccess:function(){
                								    var data = $(this).combobox('getData');
                								    $('#feeder').combobox('loadData', data);
                								},
                                                onHidePanel: function() {
                                                    var valueField = $(this).combobox('options').valueField;
                                                    var val = $(this).combobox('getValue');
                                                    var allData = $(this).combobox('getData');
                                                    var result = true;
                                                    for (var i = 0; i < allData.length; i++) {
                                                        if (val == allData[i][valueField]) {
                                                            result = false;
                                                        }
                                                    }
                                                    if (result) {
                                                        $(this).combobox('clear');
                                                    }
                                                },
                								value:'<?php echo $vessel;?>',
            								">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="flight_number"><?= lang('update_voyage');?></td>
                                        <td>
                                            <select <?php echo in_array("voyage",$G_userBscEdit) ? '' : 'readonly'; ?> id="voyage" class="easyui-combobox"  editable="true" name="voyage" style="width:90px;" data-options="
                                                valueField:'leaveVoyNo',
                                                textField:'leaveVoyNo',
                                                value:'<?php echo $voyage;?>',
                                            ">
                                            </select>
                                            内
                                            <input <?php echo in_array("voyage_inner",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-textbox"  name="voyage_inner" id="voyage_inner"  style="width:70px;" value="<?php echo $voyage_inner;?>"/>
                                            <select class="easyui-combobox" <?php echo in_array("louzhuang",$G_userBscEdit) ? '' : 'readonly'; ?> editable="false" name="louzhuang" id="louzhuang" style="width: 70px;" data-options="
                                                value: '<?= $louzhuang;?>',
                                                onSelect: function(rec){
                                                    louzhuang_select(rec);
                                                }
                                            ">
                                                <option value="">-</option>
                                                <option value="漏装">漏装</option> 
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('trans_discharge_code');?></td>
                                        <td>
                                            <select <?php echo in_array("trans_discharge_code",$G_userBscEdit) ? '' : 'readonly'; ?> id="trans_discharge_code" class="easyui-combobox"  editable="true" name="trans_discharge_code" style="width:255px;" data-options="valueField:'value',
                                                textField:'name',
                                                value:'<?php echo es_encode($trans_discharge_code);?>',
                                            ">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_feeder');?></td>
                                        <td>
                                            <select <?php echo in_array("feeder",$G_userBscEdit) ? '' : 'readonly'; ?> id="feeder" class="easyui-combobox"  editable="true" name="feeder" style="width:255px;" data-options="
                								valueField:'value',
                								textField:'value',
                								value:'<?php echo $feeder;?>',
            								">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('feeder_voyage');?></td>
                                        <td>
                                            <input <?php echo in_array("feeder_voyage",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-textbox"  name="feeder_voyage" id="feeder_voyage"  style="width:255px;" value="<?php echo $feeder_voyage;?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('trans_receipt');?></td>
                                        <td>
                                            <select <?php echo in_array("trans_receipt",$G_userBscEdit) ? '' : 'readonly'; ?> id="trans_receipt" class="easyui-combogrid"  editable="true" name="trans_receipt" style="width:80px;" data-options="
                                                panelWidth: '1000px',
                                                panelHeight: '300px',
                                                idField: 'port_code',              //ID字段
                                                textField: 'port_code',    //显示的字段
                                                url:'/biz_port/get_option?type=<?= $trans_mode;?>&carrier=<?= $trans_carrier;?>&limit=true&old=<?= $trans_receipt;?>',
                                                striped: true,
                                                editable: false,
                                                pagination: false,           //是否分页
                                                toolbar : '#trans_receipt_div',
                                                collapsible: true,         //是否可折叠的
                                                method: 'get',
                                                columns:[[
                                                    {field:'port_code',title:'<?= lang('港口代码');?>',width:100},
                                                    {field:'port_name',title:'<?= lang('港口名称');?>',width:200},
                                                    {field:'terminal',title:'<?= lang('码头');?>',width:200},
                                                ]],
                                                mode: 'remote',
                                                value: '<?= $trans_receipt;?>',
                                                emptyMsg : '未找到相应数据!',
                                                onSelect: function(index, row){
                                                    if(row !== undefined){
                                                        $('#trans_discharge_name').textbox('setValue', row.port_name);
                                                    }
                                                },
                                                onShowPanel:function(){
                                                    var opt = $(this).combogrid('options');
                                                    var toolbar = opt.toolbar;
                                                    combogrid_search_focus(toolbar, 1);
                                                } 
            								">
                                            </select>
                                            <input class="easyui-textbox" <?php echo in_array("trans_receipt_name",$G_userBscEdit) ? '' : 'readonly'; ?> style="width: 170px;" name="trans_receipt_name" id="trans_receipt_name" value="<?= $trans_receipt_name;?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('trans_term');?></td>
                                        <td>
                                            <select id="trans_term" <?php echo in_array("trans_term",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-combobox"  editable="false" name="trans_term" style="width:255px;" data-options="
                                                value:'<?= $trans_term;?>',
                                                valueField:'value',
                                                textField:'value',
                                                url:'/bsc_dict/get_option/trans_term',
                                            ">
                                            </select>
                                        </td>
                                    </tr>
                                    <?php if(isset($free_svr) && !empty($free_svr)){
                                        foreach($free_svr as $key => $row){
                                            $this_btn = '<a class="easyui-linkbutton" onclick="add_free_svr()" iconCls="icon-add"></a>';
                                            $this_lang = '';
                                            if($key < 3){
                                                if($key == 0){
                                                    $this_lang = lang('free_svr');
                                                }
                                            }else{
                                                $this_btn = '';
                                            }?>
                                            <tr class="free_svr">
                                                <td><?= $this_lang;?></td>
                                                <td>
                                                    <select <?php echo in_array("free_svr",$G_userBscEdit) ? '' : 'readonly'; ?> editable="false" class="easyui-combobox free_svr_type" name="free_svr[<?= $key;?>][type]"  data-options="
                                                    valueField:'value',
                                                    textField:'name',
                                                    url: '/bsc_dict/get_option/free_svr',
                                                    value: '<?= $row['type'];?>',
                                                    onSelect: function(rec){
                                                        free_svr_type_jc();
                                                    },
                                                ">
                                                    </select>
                                                    <input <?php echo in_array("free_svr",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-numberbox free_svr_days" name="free_svr[<?= $key;?>][days]" value="<?= $row['days'];?>"><?= lang('days');?>
                                                    <?= $this_btn;?>
                                                </td>
                                            </tr>
                                        <?php }
                                    }else{ ?>
                                        <tr class="free_svr">
                                            <td><?= lang('free svr');?></td>
                                            <td>
                                                <select <?php echo in_array("free_svr",$G_userBscEdit) ? '' : 'readonly'; ?> editable="false" class="easyui-combobox free_svr_type" name="free_svr[0][type]" data-options="
                                                valueField:'value',
                                                textField:'name',
                                                url: '/bsc_dict/get_option/free_svr',
                                                onSelect: function(rec){
                                                    free_svr_type_jc();
                                                },
                                            ">
                                                </select>
                                                <input <?php echo in_array("free_svr",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-numberspinner free_svr_days" name="free_svr[0][days]" data-options="min:1,max:30"><?= lang('days');?>
                                                <a class="easyui-linkbutton" onclick="add_free_svr()" iconCls="icon-add"></a>
                                            </td>
                                        </tr>
                                    <?php }?>
                                    <!--成本--start-->
                                    <!--<tr><td colspan="2">----------------------市场模块---------------------------</td></tr>-->
                                    <tr style="display: none;">
                                        <td><span id="price_flag_msg_span"></span><?= lang('reason');?></td>
                                        <td>
                                            <input <?php echo in_array("price_flag_msg",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-textbox"  name="price_flag_msg" id="price_flag_msg"  style="width:255px;" value="<?php echo $price_flag_msg;?>"/>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td><?= lang('freight_affirmed');?></td>
                                        <td>
                                            <input <?php echo in_array("freight_affirmed",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-datebox" id="freight_affirmed" name="freight_affirmed" style="width: 135px;" value="<?= $freight_affirmed;?>">
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td><?= lang('booking_confirmation');?></td>
                                        <td>
                                            <input <?php echo in_array("booking_confirmation",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-datebox" id="booking_confirmation" name="booking_confirmation" style="width: 255px;" value="<?= $booking_confirmation;?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('pay_buy_flag');?></td>
                                        <td><?= lang('客户付买');?>
                                            <input <?php echo in_array('pay_buy_flag',$G_userBscEdit)  ? '' : 'readonly'; ?> type="checkbox" class="status" id="pay_buy_flag" <?= $pay_buy_flag == 0 ? '' : 'checked';?> value="1">
                                            <?= lang('订舱口付买');?>
                                            <input type="checkbox" class="status" id="booking_agent_pay_buy_flag"  value="1" disabled> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('marketing');?></td>
                                        <td>
                                            <select <?php echo in_array('marketing',$G_userBscEdit)   ? '' : 'readonly'; ?> id="marketing_id" class="easyui-combobox"  editable="false" name="marketing_id" style="width:255px;" data-options="
                                                valueField:'id',
                                                textField:'name',
                                                url:'/bsc_user/get_data_role/marketing?status=0',
                                                value:'<?php echo $marketing_id;?>',
                                                onSelect: function(rec){
                                                    $('#marketing_group').val(rec.group);
                                                },
                                                onLoadSuccess:function(){
                                                    var val = $(this).combobox('getValue');
                                                    var data = $(this).combobox('getData');
                                                    var reset = true;
                                                    if(val == 0 && data.length == 2){
                                                        $(this).combobox('setValue', data[1].id);
                                                        $('#marketing_group').val(data[1].group);
                                                    }
                                                },
                                            ">
                                            </select>
                                            <input type="hidden" <?php echo in_array('marketing',$G_userBscEdit)  ? '' : 'readonly'; ?> id="marketing_group" name="marketing_group" style="width:255px;" value="<?php echo $marketing_group;?>">
                                            <a href="javascript:void(0);" onclick="user_info('marketing_id')" title='查看该用户详情'><?= getUserName($marketing_id);?></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('booking');?></td>
                                        <td>
                                            <select <?php echo in_array('booking',$G_userBscEdit)   ? '' : 'readonly'; ?> id="booking_id" class="easyui-combobox"  editable="false" name="booking_id" style="width:255px;" data-options="
                                                valueField:'id',
                                                textField:'name',
                                                url:'/bsc_user/get_data_role/booking?status=0',
                                                value:'<?php echo $booking_id;?>',
                                                onSelect: function(rec){
                                                    $('#booking_group').val(rec.group);
                                                },
                                                onLoadSuccess:function(){
                                                    var val = $(this).combobox('getValue');
                                                    var data = $(this).combobox('getData');
                                                    var reset = true;
                                                    if(val == 0 && data.length == 2){
                                                        $(this).combobox('setValue', data[1].id);
                                                        $('#booking_group').val(data[1].group);
                                                    }
                                                },
                                            ">
                                            </select>
                                            <input type="hidden" <?php echo in_array('booking',$G_userBscEdit)  ? '' : 'readonly'; ?> id="booking_group" name="booking_group" style="width:255px;" value="<?php echo $booking_group;?>">
                                            <a href="javascript:void(0);" onclick="user_info('booking_id')" title='查看该用户详情'><?= getUserName($booking_id);?></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('document');?></td>
                                        <td>
                                            <select <?php echo in_array('document',$G_userBscEdit)   ? '' : 'readonly'; ?> id="document_id" class="easyui-combobox"  editable="false" name="document_id" style="width:255px;" data-options="
                                                valueField:'id',
                                                textField:'name',
                                                url:'/bsc_user/get_data_role/document?status=0',
                                                value:'<?php echo $document_id;?>',
                                                onSelect: function(rec){
                                                    $('#document_group').val(rec.group);
                                                },
                                                onLoadSuccess:function(){
                                                    var val = $(this).combobox('getValue');
                                                    var data = $(this).combobox('getData');
                                                    var reset = true;
                                                    if(val == 0 && data.length == 2){
                                                        $(this).combobox('setValue', data[1].id);
                                                        $('#document_group').val(data[1].group);
                                                    }
                                                },
                                            ">
                                            </select>
                                            <input type="hidden" <?php echo in_array('document',$G_userBscEdit)  ? '' : 'readonly'; ?> id="document_group" name="document_group" style="width:255px;" value="<?php echo $document_group;?>">
                                            <a href="javascript:void(0);" onclick="user_info('document_id')" title='查看该用户详情'><?= getUserName($document_id);?></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>freetm<?= lang('AGR No.');?></td>
                                        <td>
                                            <input <?php echo in_array('AGR_no',$G_userBscEdit)  ? '' : 'readonly'; ?> type="text" class="easyui-textbox" name="AGR_no" id="AGR_no" style="width: 255px;" value="<?= $AGR_no;?>">
                                        </td>
                                    </tr>
                                    <!--成本--end-->
                                </table>
                            </td>
                            <td valign="top">
                                <?php
                                $date_zero = array("trans_ETA","trans_ATA","trans_ETD","trans_ATD","closing_date","carrier_cutoff", 'des_ETA', 'cy_open', 'cy_close', 'close_ens_date');
                                foreach($date_zero as $dzero){
                                    if($$dzero < "1999-01-01") $$dzero = null;
                                }
                                ?>
                                <table class="form_table">
                                    <tr>
                                        <td>
                                            <?= lang('update_trans_ETA');?>
                                        </td>
                                        <td>
                                            <input <?php echo in_array("trans_ETA",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-datetimebox"  name="trans_ETA" id="trans_ETA"  style="width:255px;" value="<?php echo $trans_ETA;?>"/>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td><?= lang('update_trans_ATA');?> </td>
                                        <td>
                                            <input <?php echo in_array("trans_ATA",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-datetimebox"  name="trans_ATA" id="trans_ATA"  style="width:255px;" value="<?php echo $trans_ATA;?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_trans_ETD');?> </td>
                                        <td>
                                            <input <?php echo in_array("trans_ETD",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-datetimebox"  name="trans_ETD" id="trans_ETD"  style="width:255px;" value="<?php echo $trans_ETD;?>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('update_trans_ATD');?> </td>
                                        <td>
                                            <input <?php echo in_array("trans_ATD",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-datetimebox"  name="trans_ATD" id="trans_ATD"  style="width:255px;" value="<?php echo $trans_ATD;?>"/>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td><?= lang('closing_date');?> </td>
                                        <td>
                                            <input <?php echo in_array("closing_date",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-datetimebox"  name="closing_date" id="closing_date"  style="width:200px;" value="<?php echo $closing_date;?>"  data-options="showSeconds:false" />
                                        </td>
                                    </tr>   
                                    <tr>
                                        <td><?= lang('report_date');?> </td>
                                        <td>
                                            <input <?php echo in_array("report_date",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-datebox"  name="report_date" id="report_date"  style="width:200px;" value="<?php echo $report_date;?>"/>
                                        </td>
                                    </tr>   
                                    <tr>
                                        <td>
                                            <span onclick="get_shipment_info('description')" style="color:blue;cursor:pointer;" title="点击文字可获取shipment货描"><?= lang('update_description');?></span>
                                        </td>
                                        <td>
                                            <textarea <?php echo in_array("description",$G_userBscEdit) ? '' : 'readonly'; ?> name="description" id="description" style="height:60px;width:245px;"/><?php echo $description;?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('description_cn');?> </td>
                                        <td>
                                            <textarea <?php echo in_array("description_cn",$G_userBscEdit) ? '' : 'readonly'; ?> name="description_cn" id="description_cn" style="height:60px;width:245px;"/><?php echo $description_cn;?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span onclick="get_shipment_info('mark_nums')" style="color:blue;cursor:pointer;"><?= lang('update_mark_nums');?></span>
                                        </td>
                                        <td>
                                            <textarea <?php echo in_array("mark_nums",$G_userBscEdit) ? '' : 'readonly'; ?> name="mark_nums" id="mark_nums" style="height:60px;width:245px;"/><?php echo $mark_nums;?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('requirements');?></td>
                                        <td>
                                            <textarea name="requirements" <?php echo in_array("requirements",$G_userBscEdit) ? '' : 'readonly'; ?> id="requirements" style="height:60px;width:248px;"><?php echo $requirements;?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('marketing') . lang('remark');?></td>
                                        <td>
                                            <textarea name="remark" <?php echo in_array("remark",$G_userBscEdit) ? '' : 'readonly'; ?> id="remark" style="height:60px;width:248px;"><?php echo $remark;?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('document') . lang('remark');?></td>
                                        <td>
                                            <textarea name="document_remark" <?php echo in_array("document_remark",$G_userBscEdit) ? '' : 'readonly'; ?> id="document_remark" style="height:60px;width:248px;"><?php echo $document_remark;?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('hs_code');?> </td>
                                        <td>
                                            <select <?php echo in_array("hs_code",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-combobox"  name="hs_code" id="hs_code"  style="width:255px;"  prompt="<?= lang('Please separate them with commas');?>" data-options="
                                                value:'<?= $hs_code;?>',
                                                valueField:'value',
                								textField:'name',
            									<?php if($trans_carrier == 'DXHYYX02') echo 'url:\'/bsc_dict/get_option/hs_code?ext1=TSL\',';?>
                								mode: 'remote',
                                                onBeforeLoad:function(param){
                                                    if($(this).combobox('getValue') == undefined || $(this).combobox('getValue').length < 3)return false;
                                                    if(Object.keys(param).length == 0)param.q = $(this).combobox('getValue');
                                                },
                								onHidePanel: function() {
                                                    var valueField = $(this).combobox('options').valueField;
                                                    var vals = $(this).combobox('getValues');
                                                    var allData = $(this).combobox('getData');
                                                    if(allData.length == undefined || allData.length == 0) return false;
                                                    $(this).combobox('setValue', '');
                                                    for (var i = 0; i < allData.length; i++) {
                                                        if ($.inArray(allData[i][valueField], vals) !== -1) {
                                                            $(this).combobox('select', allData[i][valueField]);
                                                        }
                                                    }
                                                },
                                            ">
                                            </select>
                                            <span id="hs_code_tip" style="color: red"></span>
                                        </td>
                                    </tr>
                                </table></td>
                            <td valign="top">
                                <table class="form_table">
                                    <tr>
                                        <td><?= lang('container_owner');?> </td>
                                        <td>
                                            <select <?php echo in_array("container_owner",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-combobox"  name="container_owner" id="container_owner"  style="width:255px;" data-options="
                                                editable:false,
                                                value:'<?php echo $container_owner;?>',
                                                valueField:'value',
                                                textField:'name',
                                                url: '/bsc_dict/get_option/container_owner',
                                            ">
                                            </select>
                                        </td>
                                    </tr>


                                    <tr id="tr_suitcase_carton_no" style="display:none;">
                                        <td>
                                            <?= lang('suitcase_carton_no'); ?>
                                        </td>
                                        <td>
                                            <select class="easyui-combogrid" disabled id="gs_soc_tixianghao1" style="width:215px;" data-options="
                                                multiple:true,
                                                editable:false,
                                                panelWidth:450,
                                                panelHeight:350,
                                                idField:'gs_soc_tixianghao',
                                                textField:'gs_soc_tixianghao',
                                                value:'<?php echo isset($gs_soc_tixianghao) ? $gs_soc_tixianghao : ""; ?>',
                                                columns:[[
                                                    {field:'gs_soc_tixianghao',title:'<?= lang('提箱号');?>',width:120},
                                                    {field:'gs_soc_tixiangdian',title:'<?= lang('提箱点');?>',width:120},
                                                    {field:'supplier_client_name',title:'<?= lang('供应商');?>',width:120},
                                                    {field:'box_info',title:'<?= lang('箱型箱量');?>',width:120},
                                                ]],
                                                toolbar:'#gs_soc_tixianghao_div',
                                                onChange:function(newValue, oldValue){
                                                    $('#gs_soc_tixianghao').val(newValue.join(','));
                                                },
                                            ">

                                            </select>
                                            <input type="hidden" <?php echo in_array("gs_soc_tixianghao", $G_userBscEdit) ? '' : 'disabled'; ?>  name="gs_soc_tixianghao" id="gs_soc_tixianghao" value="<?php echo isset($gs_soc_tixianghao) ? $gs_soc_tixianghao : ""; ?>">
                                            <script>
                                                function ts_tool_search(fm_id, load_id) {
                                                    var where = {};
                                                    var form_data = $('#' + fm_id).serializeArray();
                                                    $.each(form_data, function (index, item) {
                                                        if(item.value == "") return true;
                                                        if(where.hasOwnProperty(item.name) === true){
                                                            if(typeof where[item.name] == 'string'){
                                                                where[item.name] =  where[item.name].split(',');
                                                                where[item.name].push(item.value);
                                                            }else{
                                                                where[item.name].push(item.value);
                                                            }
                                                        }else{
                                                            where[item.name] = item.value;
                                                        }
                                                    });
                                                    //提箱号的,默认拼一个参数
                                                    if(load_id == 'gs_soc_tixianghao1') where['ext_value'] = $('#gs_soc_tixianghao').val();
                                                    if(where.length == 0){
                                                        $.messager.alert('Tips', '请填写需要搜索的值');
                                                        return;
                                                    }
                                                    $('#' + load_id).combogrid('grid').datagrid('load',where);
                                                }
                                                function download_tixiangdan() {
                                                    var width = 500;
                                                    var height = 200;
                                                    var window_iframe_css = {width: width, height: height};

                                                    var url = '/bsc_template/set_parameter?url=/export/export_word&template_id=51&data_table=consol&id=<?= $id; ?>&pdf=1&parameter=gs_soc_tixianghao';
                                                    $('#window_iframe').css(window_iframe_css).attr('src', url);
                                                    $('#window').window({
                                                        title:'window',
                                                        width:width,
                                                        height:height,
                                                        href:url,
                                                        modal:true
                                                    }).window('open');
                                                }
                                            </script>
                                            <a href="javascript:void(0);" class="easyui-linkbutton" plain="true" data-options="onClick: function(){download_tixiangdan();}">下载</a>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <span onclick="get_shipment_sum()" style="color:blue;cursor:pointer;"><?= lang('货物件数');?></span>
                                        </td>
                                        <td>
                                            <input class="easyui-textbox" <?php echo in_array("good_outers",$G_userBscEdit) ? '' : 'readonly'; ?> name="good_outers" id="good_outers"  style="width:100px;" value="<?php echo $good_outers;?>"/>
                                            <select name="good_outers_unit" <?php echo in_array("good_outers_unit",$G_userBscEdit) ? '' : 'readonly'; ?> id="good_outers_unit"  class="easyui-combobox" style="width:155px;"data-options="
                                                valueField:'name',
                                                textField:'name',
                                                value:'<?= $good_outers_unit;?>',
                                                url: '/bsc_dict/get_option/packing_unit',
                                                onHidePanel: function() {
                                                    var valueField = $(this).combobox('options').valueField;
                                                    var val = $(this).combobox('getValue');
                                                    var allData = $(this).combobox('getData');
                                                    var result = true;
                                                    for (var i = 0; i < allData.length; i++) {
                                                        if (val == allData[i][valueField]) {
                                                            result = false;
                                                        }
                                                    }
                                                    if (result) {
                                                        $(this).combobox('clear');
                                                    }
                                                },
                                                onSelect: function (data) {
                                                    var good_outers = $('#good_outers').val();
                                                    var all = $('#good_outers_unit').combobox('getData');
                                                    var name1 = data.name;
                                                    var name2 = data.name;
                                                    $.each(all,function(index,item){
                                                           if(data.value == item.value && data.name != item.name){
                                                                name2 = item.name;
                                                           }
                                                    })
                                                    var len1 = name1.length;
                                                    var len2 = name2.length;
                                                    if(good_outers > 1){
                                                        if(len2>len1){
                                                            $('#good_outers_unit').combobox('setValue', name2);
                                                        }
                                                    }else{
                                                        if(len2<len1){
                                                            $('#good_outers_unit').combobox('setValue', name2);
                                                        }
                                                    }
                                                }
                                            ">
                                            </select>
                                            <span class="sum"><?= $shipment_good_outers;?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('货物重量');?> </td>
                                        <td>
                                            <input class="easyui-textbox" <?php echo in_array("good_weight",$G_userBscEdit) ? '' : 'readonly'; ?> name="good_weight" id="good_weight"  style="width:100px;" value="<?php echo $good_weight;?>"/>
                                            <select readonly name="good_weight_unit" <?php echo in_array("good_weight_unit",$G_userBscEdit) ? '' : 'readonly'; ?> id="good_weight_unit" class="easyui-combobox" style="width:155px;" data-options="
                                                value:'<?= $good_weight_unit;?>',
                                            ">
                                                <option value="KGS">KGS</option>
                                            </select>
                                            <span class="sum"><?= $shipment_good_weight;?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('货物体积');?> </td>
                                        <td>
                                            <input <?php echo in_array("good_volume",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-textbox"  name="good_volume" id="good_volume"  style="width:100px;" value="<?php echo $good_volume;?>"/>
                                            <select readonly <?php echo in_array("good_volume_unit",$G_userBscEdit) ? '' : 'readonly'; ?> name="good_volume_unit"  id="good_volume_unit" class="easyui-combobox" style="width:155px;" data-options="
                                                value:'<?= $good_volume_unit;?>',
                                            ">
                                                <option value="CBM">CBM</option>
                                            </select>
                                            <span class="sum"><?= $shipment_good_volume;?></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><?= lang('货物类型'); ?></td>
                                        <td>
                                            <select <?php echo in_array("goods_type",$G_userBscEdit) ? '' : 'readonly'; ?> class="easyui-combobox" name="goods_type" id="goods_type" style="width: 255px;" data-options="
                                                valueField:'value',
                                                textField:'valuename',
                                                value:'<?= $goods_type; ?>',
                                                url:'/bsc_dict/get_option/goods_type',
                                            ">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><hr></td>
                                    </tr>
                                    <?php
                                    foreach($userList as $key => $value){
                                        //海外代理单独和agent一起放
                                        if($key == 'oversea_cus')continue;
                                        if($key == 'marketing')continue;
                                        if($key == 'booking')continue;
                                        if($key == 'document')continue;
                                        $group_key = $key . '_group';
                                        ?>
                                        <tr>
                                            <td><?php echo lang($key); ?></td>
                                            <td>
                                                <select <?php echo in_array($key,$G_userBscEdit) ? '' : 'readonly'; ?> id="<?php echo $key;?>_id" class="easyui-combobox"  editable="false" name="<?php echo $key;?>_id" style="width:255px;" data-options="
                									valueField:'id',
                									textField:'name',
                									url:'/bsc_user/get_data_role/<?php echo $key; ?>?status=0',
                									value:'<?php echo $value;?>',
                									onSelect: function(rec){
                									    $('#<?= $group_key;?>').val(rec.group);
                									},
                									onLoadSuccess:function(){
                                                        var val = $(this).combobox('getValue');
                                                        var data = $(this).combobox('getData');
                                                        var reset = true;
                                                        if(val == 0 && data.length == 2){
                                                            $(this).combobox('setValue', data[1].id);
                                                            $('#<?= $group_key;?>').val(data[1].group);
                                                        }
                                                    }
            									">
                                                </select>
                                                <input type="hidden" <?php echo in_array($key,$G_userBscEdit) ? '' : 'readonly'; ?> id="<?php echo $key;?>_group" name="<?php echo $key;?>_group" style="width:255px;" value="<?php echo $$group_key;?>">
                                                <a href="javascript:void(0);" onclick="user_info('<?= $key;?>_id')" title='查看该用户详情'><?= getUserName($value);?></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <?php if($master_consol == ''){?>

                    <div data-options="region:'south',title:'Shipments',expandMode:'dock',split:'true',onResize(){
                    $('#shipments')[0].contentWindow.resize_table;
                }"  style="height: 200px;">
                        <iframe id="shipments" scrolling="auto" frameborder="0" id="shipments_sub" style="width:100%;height: 100%" src="/biz_shipment/index_by_consol/<?php echo $id;?>"></iframe>
                    </div>
                <?php } ?>
            </div>
        </form>
    </div>
    <?php if($master_consol == ''){?>
        <div title="Billing" style="padding:10px">
            <iframe scrolling="auto" frameborder="0" id="billing_sub" style="width:100%;height:800px;"></iframe>
        </div>
        <div title="Container" style="padding:10px">
            <iframe scrolling="auto" frameborder="0" id="container_sub" style="width:100%;height:600px;"></iframe>
        </div>
		<div title="Download" style="padding:10px">
			<iframe scrolling="auto" frameborder="0" id="Download_sub" style="width:100%;height:700px;"></iframe>
		</div>
        <div title="Upload files" style="padding:10px">
            <iframe scrolling="auto" frameborder="0" id="upload_files_sub" style="width:100%;height:600px;"></iframe>
        </div>
    <?php } ?>
        <div title="EDI" style="padding:10px">
            <iframe scrolling="auto" frameborder="0" id="edi" style="width:100%;height:750px;"></iframe>
        </div>
    <?php if(menu_role('consol_log')){?>
        <div title="Log" style="padding:10px">
            <iframe scrolling="auto" frameborder="0" id="log" style="width:100%;height:600px;"></iframe>
        </div>
    <?php }?>
    <div title="Spmt Log" style="padding:10px">
        <iframe scrolling="auto" frameborder="0" id="spmt_log" style="width:100%;height:600px;"></iframe>
    </div>
    <div title="Lock Log" style="padding:10px">
        <iframe scrolling="auto" frameborder="0" id="lock_log" style="width:100%;height:600px;"></iframe>
    </div>
    <td colspan="2"></td>
</div>

<div id="window" class="easyui-window" data-options="closed:true">
    <iframe id="window_iframe" frameborder="0">

    </iframe>
</div>
<div id="trans_carrier_rule" class="easyui-window" data-options="closed:true,title:'<?= lang('船公司规则');?>',showType:'slide',width: '600px',height: $(window).height(), timeout: 0" style="top:0;left:0;">
    <iframe id="trans_carrier_rule_iframe" scrolling="auto" src="/bsc_help_content/help_carrier/<?= $trans_carrier;?>_CARRIER_REMIND" frameborder="0" style="width:100%;height:99%;"></iframe>
</div>
<div id="trans_origin_div">
    <form id="trans_origin_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_origin_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_origin_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_origin_click" iconCls="icon-search" onclick="query_report('trans_origin', 'trans_origin')"><?= lang('search');?></a>
        </div>
    </form>
</div>
<div id="trans_discharge_div">
    <form id="trans_discharge_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_discharge_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_discharge_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_discharge_click" iconCls="icon-search" onclick="query_report('trans_discharge', 'trans_discharge')"><?= lang('search');?></a>
        </div>
    </form>
</div>
<div id="trans_receipt_div">
    <form id="trans_receipt_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_receipt_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_receipt_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_receipt_click" iconCls="icon-search" onclick="query_report('trans_receipt', 'trans_receipt')"><?= lang('search');?></a>
        </div>
    </form>
</div>
<div id="trans_destination_div">
    <form id="trans_destination_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码');?>:</label><input name="port_code" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <label><?= lang('港口名称');?>:</label><input name="port_name" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <label><?= lang('码头');?>:</label><input name="terminal" class="easyui-textbox keydown_search" style="width:96px;" t_id="trans_destination_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_destination_click" iconCls="icon-search" onclick="query_report('trans_destination', 'trans_destination')"><?= lang('search');?></a>
        </div>
    </form>
</div>
<div id="gs_soc_tixianghao_div">
    <form id="gs_soc_tixianghao_fm" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('提箱点');?>:</label><input class="easyui-textbox" name="gs_soc_tixianghao" style="width:96px;"/>
            <label><?= lang('提箱号');?>:</label><input class="easyui-textbox" name="gs_soc_tixiangdian" style="width:96px;"/>
            <label><?= lang('供应商');?>:</label><input class="easyui-textbox" name="supplier_client_name" style="width:96px;"/>
        </div>
        <div style="padding-left: 5px;display: inline-block;">
            <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-search" onclick="ts_tool_search('gs_soc_tixianghao_fm', 'gs_soc_tixianghao1')"><?= lang('search');?></a>
        </div>
    </form>
</div>
</body>
