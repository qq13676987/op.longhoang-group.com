<style>

</style>
<body>
<table>
    <?php foreach ($rows as $row){ ?>
    <tr>
        <td><?= $row['name'];?>:</td>
        <td>
            <form id="<?= $row['catalog'];?>_form" method="post">
                <input type="hidden" name="id" value="<?= $row['id'];?>">
                <input type="hidden" name="name" value="<?= $row['name'];?>">
                <input type="hidden" name="catalog" value="<?= $row['catalog'];?>">
                <input type="hidden" name="ext1" value="<?= $row['ext1'];?>">
                <input type="text" class="easyui-textbox" name="value" value="<?= $row['value'];?>" required style="width: 255px">
                <a href="javascript:void(0);" class="easyui-linkbutton" data-options="onClick:function(){submit_form('<?= $row['catalog'];?>_form');}"><?= lang('提交');?></a>
            </form>
        </td>
    </tr>
    <?php } ?>
    <script>
        function submit_form(form_id){
            ajaxLoading();
            $('#' + form_id).form('submit', {
                url:"/bsc_dict/update_by_client",
                onSubmit: function(){
                    // do some check
                    // return false to prevent submit;
                    var isValid = $(this).form('validate');
                    if(!isValid){
                        ajaxLoadEnd();
                    }
                    return isValid;    // 返回false终止表单提交
                },
                success:function(res_json){
                    ajaxLoadEnd();
                    try{
                        var res = $.parseJSON(res_json);
                        $.messager.alert('<?= lang('提示');?>', "<?= lang('保存成功');?>");
                    }catch(e){
                        $.messager.alert('<?= lang('提示');?>', res_json);
                    }
                }
            });
        }
        $(function(){

        });
    </script>
</table>

</body>