<title>非业务收支</title>
<script>
    function in_out_for(value,row,index){
        if(value == 'in'){
            return '收入';
        }
        if(value == 'out'){
            return '支出';
        }
    }

    function amount_for(value,row,index){
        if(row.currency == 'USD'){
            return value + "<br>CNY:"+row.local_amount;
        }
        return value;
    }
</script>
        <table id="tt2" style="width:1100px;min-height:650px"
               rownumbers="false"  idField="id"
               toolbar="#tb" singleSelect="true" nowrap="false"  pagination="true" pagesize="30">
            <thead>
            <tr>
                <th field="id" width="60" >id</th>
                <th field="in_out" width="80" formatter="in_out_for">收入/支出</th>
                <th field="charge_name" width="120" >费用名称</th>
                <th field="charge_date" width="180"  >费用日期</th>
                <th field="currency" width="60"  >币种</th>
                <th field="amount" width="120"  formatter="amount_for">金额</th>
                <th field="client_name" width="120"  >收付对象</th>
                <th field="write_off_date" width="180">核销日期</th>
                <th field="created_by_name" width="120" >创建人</th>
                <th field="created_time" width="180">创建时间</th>
                <th field="updated_by_name" width="120">更新人</th>
                <th field="updated_time" width="180">更新时间</th>
            </tr>
            </thead>
        </table>

        <div id="tb" style="padding:3px;">
            <table>
                <tr>
                    <td>
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="add()"><?= lang('add');?></a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="copy_add()"><?= lang('复制新增');?></a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="del()"><?= lang('delete');?></a>
                        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="open_chaxun()"><?= lang('query');?></a>
                    </td>
                    <td width='80'></td>
                </tr>
            </table>

        </div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                <?= lang('query');?> 1
            </td>
            <td align="right">
                <select name="f1"   id="f1"  class="easyui-combobox"  required="true" style="width:150px;" data-options=" onSelect:function(v){
                change_person(v,1)

        }">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$rs[1]'>$rs[0]</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1"  id="s1"  class="easyui-combobox"  required="true" style="width:100px;" >
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;<span class="v1">
                    <input id="v1" class="easyui-textbox "  style="width:120px;">
                </span>



            </td>
        </tr>
        <tr>
            <td>
                <?= lang('query');?> 2
            </td>
            <td align="right">
                <select name="f2"  id="f2"  class="easyui-combobox"  required="true" style="width:150px;" data-options=" onSelect:function(v){
                change_person(v,2)

        }">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$rs[1]'>$rs[0]</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2"  id="s2"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <span class="v2">
                    <input id="v2" class="easyui-textbox"  style="width:120px;">
                </span>

            </td>
        </tr>

        <tr>
            <td>
                <?= lang('query');?> 3
            </td>
            <td align="right">
                <select name="f3"  id="f3"  class="easyui-combobox"  required="true" style="width:150px;"data-options=" onSelect:function(v){
                change_person(v,3)

        }">
                    <?php
                    foreach ($f as $rs){
                        echo "<option value='$rs[1]'>$rs[0]</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s3"  id="s3"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <span class="v3">
                    <input id="v3" class="easyui-textbox"  style="width:120px;">
                </span>

            </td>
        </tr>

    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:120px;height:30px;"><?= lang('search');?></a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:resetdoSearch()" style="width:120px;height:30px;"><?= lang('reset');?></a>
</div>

<div id="add_div" class="easyui-window" style="width:450px;height:345px;padding:5px;" shadow="false" data-options="closed:true,border:'thin'">
    <form id="add_fm" method="post">
        <table>

            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td width="80"><?= lang('收入/支出');?></td>
                <td>
                    <select class="easyui-combobox" name="in_out" style="width:200px;" data-options="editable:false,required:true">
                        <option value="in">收入</option>
                        <option value="out">支出</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td width="80"><?= lang('费用名称');?></td>
                <td>
                    <input name="charge_code" class="easyui-combobox" style="width:200px;" data-options="
                        required:true,
                        valueField:'charge_code',
                        textField:'charge_name_long',
                        url:'/biz_charge/get_option',
                        onSelect: function(rec){

                        },
                        filter:function(q, row){
                            var opts = $(this).combobox('options');
                            if(row[opts.textField].toLowerCase().indexOf(q.toLowerCase())>=0){
                                if(q !== '' && filter[q] == undefined) filter[q] = [];
                                //添加筛选值
                                if(q !== '' && $.inArray(row[opts.textField], filter) == -1)filter[q].push(row[opts.valueField]);
                                return true;
                            }else{
                                return false;
                            }
                        },
                    ">
                </td>
            </tr>
            <tr>
                <td width="80"><?= lang('费用日期');?></td>
                <td>
                    <input class="easyui-datebox input" required name="charge_date" style="width: 200px"  value="">
                </td>
            </tr>
            <tr>
                <td width="80"><?= lang('币种');?></td>
                <td>
                    <select class="easyui-combobox" name="currency" style="width:200px;" data-options="
                        valueField:'name',
                        textField:'name',
                        url:'/biz_bill_currency/get_currency/sell',
                        editable:false,required:true
                    ">
               
                    </select>
                </td>
            </tr>
            <tr>
                <td width="80"><?= lang('金额');?></td>
                <td>
                    <input class="easyui-numberbox input" min="0" required name="amount" style="width: 200px"  value="">
                </td>
            </tr>
            <tr>
                <td width="80"><?= lang('收付对象');?></td>
                <td>
                    <input class="easyui-textbox input" required style="width: 200px" name="client_name" value="">
                </td>
            </tr>
            <tr>
                <td width="80"><?= lang('核销日期');?></td>
                <td>
                    <input class="easyui-datebox input"  name="write_off_date" style="width: 200px"  value="">
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3"><button type="button" onclick="fee_save()" style="width: 100px"><?= lang('save');?></button></td>
            </tr>
        </table>
    </form>
</div>
<input type="hidden" name="id" id="fee_id">
<script type="text/javascript">
    let v1status = '';
    let v2status = '';
    let v3status = '';
    function open_chaxun(){
        let v1 = $('#f1').combobox('getValue')
        let v2 = $('#f2').combobox('getValue')
        let v3 = $('#f3').combobox('getValue')
        change_person({value:v1},1,$(".v1 input[type='hidden']").val())
        change_person({value:v2},2,$(".v2 input[type='hidden']").val())
        change_person({value:v3},3,$(".v3 input[type='hidden']").val())
        $('#chaxun').window('open')
    }
    function change_person(v,index,val=''){
        let ele = 'v'+index
        if(v.value == 'charge_date' || v.value == 'write_off_date' ||  v.value == 'created_time' ||  v.value == 'updated_time'){
            $('.'+ele).html(`<input id='${ele}' class='easyui-datebox'  style='width:120px;'>`)
            $('#'+ele).datebox({value:val})
            if(index == 1){
                v1status = '1'
            }else if(index == 2){
                v2status = '1'
            }else if(index == 3){
                v3status = '1'
            }
        }else if(v.value == 'created_by' ||  v.value == 'updated_by'){
            $('.'+ele).html(`<input id='${ele}' class='easyui-combobox'  style='width:120px;'>`)
            $('#'+ele).combobox({
                value:val,
                valueField: 'id',
                textField: 'otherName',
                url: '/bsc_user/get_data_role',
                editable:false,
            })
            if(index == 1){
                v1status = '2'
            }else if(index == 2){
                v2status = '2'
            }else if(index == 3){
                v3status = '2'
            }
        }else if(v.value == 'currency' ||  v.value == 'in_out'){
            let data = [];
            if(v.value == 'currency'){
                data = [{name:'CNY',value:'CNY'},{name:'USD',value:'USD'}]
            }
            if(v.value == 'in_out'){
                data = [{name:'收入',value:'in'},{name:'支出',value:'out'}]
            }
            $('.'+ele).html(`<input id='${ele}' class='easyui-combobox'  style='width:120px;'>`)
            $('#'+ele).combobox({
                valueField: 'value',
                textField: 'name',
                data: data,
                value:val,
                editable:false,
            })
            if(index == 1){
                v1status = '2'
            }else if(index == 2){
                v2status = '2'
            }else if(index == 3){
                v3status = '2'
            }
        }else if(v.value == 'charge_code'){
            $('.'+ele).html(`<input id='${ele}' class='easyui-combobox'  style='width:120px;'>`)
            $('#'+ele).combobox({
                editable:false,
                valueField:'charge_code',
                textField:'charge_name_long',
                url:'/biz_charge/get_option',
                value:val,
            })
            if(index == 1){
                v1status = '2'
            }else if(index == 2){
                v2status = '2'
            }else if(index == 3){
                v3status = '2'
            }
        }else{
            $('.'+ele).html(`<input id='${ele}' class='easyui-textbox'  style='width:120px;'>`)
            $('#'+ele).textbox({value:val})
            if(index == 1){
                v1status = '3'
            }else if(index == 2){
                v2status = '3'
            }else if(index == 3){
                v3status = '3'
            }
        }
    }
    function resetdoSearch(){
        let v1 = $('#f1').combobox('getValue')
        let v2 = $('#f2').combobox('getValue')
        let v3 = $('#f3').combobox('getValue')
        change_person({value:v1},1)
        change_person({value:v2},2)
        change_person({value:v3},3)
    }
    function doSearch(){
        let v1 = '';
        let v2 = '';
        let v3 = '';
        if(v1status == '1'){
            v1 = $('#v1').datebox('getValue')
        }else if(v1status == '2'){
            v1 = $('#v1').combobox('getValue')
        }else if(v1status == '3'){
            v1 = $('#v1').textbox('getValue')
        }
        if(v2status == '1'){
            v2 = $('#v2').datebox('getValue')
        }else if(v2status == '2'){
            v2 = $('#v2').combobox('getValue')
        }else if(v2status == '3'){
            v2 = $('#v2').textbox('getValue')
        }
        if(v3status == '1'){
            v3 = $('#v3').datebox('getValue')
        }else if(v3status == '2'){
            v3 = $('#v3').combobox('getValue')
        }else if(v3status == '3'){
            v3 = $('#v3').textbox('getValue')
        }
        $('#tt2').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: v1 ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: v2 ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: v3 ,
        });
        $('#chaxun').window('close');
    }
    var type = 'add';

    function edit(row){
        type = 'edit';
        if(row){
            $('#add_fm').form('clear')
            $('#fee_id').val(row.id)
            $('#add_fm').form('load',row)
            $("#add_div").window({title: '编辑'}).window("open")
        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('请选中任意行')?>');
        }
    }
    function copy_add(){
        type = 'add';
        var row = $('#tt2').datagrid('getSelected');
        if(row){
            $('#add_fm').form('clear')
            $('#add_fm').form('load',row)
            $("#add_div").window({title: '添加'}).window("open")
        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('请选中任意行')?>');
        }
    }

    function fee_save(){
        let formData = $('#add_fm').serializeArray();
        let obj = {}
        let msg = ''
        formData.map(item=>{
            if(item.name != 'write_off_date'){
                if(item.value == ''){
                    msg = '请填写必填项'
                }
            }
            obj[item.name] = item.value

        })
        if(msg != ''){
            $.messager.alert('<?= lang('Tips')?>', msg)
            return
        }
        if(type == 'add'){
            $.post("/biz_bill/extra_fee_add", obj,
                function(res){
                    if(res.code == 1){
                        $.messager.alert('<?= lang('Tips')?>', res.msg)
                        $('#tt2').datagrid('reload');
                    }else{
                        $.messager.alert('<?= lang('Tips')?>', res.msg)
                    }
                    $("#add_div").window("close")
                }, "json");
        }else if(type == 'edit'){
            obj['id'] = $('#fee_id').val();
            $.post("/biz_bill/extra_fee_edit", obj,
                function(res){
                    if(res.code == 1){
                        $.messager.alert('<?= lang('Tips')?>', res.msg)
                        $('#tt2').datagrid('reload');
                    }else{
                        $.messager.alert('<?= lang('Tips')?>', res.msg)
                    }
                    $("#add_div").window("close")
                }, "json");
        }

    }

    function add(){
        type = 'add';
        $('#add_fm').form('clear')
        $("#add_div").window({title: '新增'}).window("open")
    }

    function del() {
        var row = $('#tt2').datagrid('getSelected');
        if(row){
            $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认删除？');?>', function(r){
                if (r){
                    $.post("/biz_bill/extra_fee_del", {id: row.id},
                        function(res){
                            $.messager.alert('<?= lang('Tips')?>', res.msg)
                            if(res.code == 1){
                                $('#tt2').datagrid('reload');
                            }
                        }, "json");
                }
            });


        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('请选中任意行')?>');
        }
    }


    $(function () {
        $('#tt2').datagrid({
            url: '/biz_bill/get_extra_fee_data/',
            width: 'auto',
            height: $(window).height(),
            onDblClickRow:function(index,row){
                edit(row)
            },
            rowStyler: function(index,row){
                if (row.in_out == 'in'){
                    return 'color:green;';
                }
                if (row.in_out == 'out'){
                    return 'color:red;';
                }
            }
        });

        $(window).resize(function () {
            $('#tt2').datagrid('resize');
        });

    });

</script>

