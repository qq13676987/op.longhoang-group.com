<?php


class sys_queue_model extends CI_Model
{
    private $table = 'sys_queue';

    //array('任务名称', '方法')
    private $queues = array(
        0 => array('', ''),
        1 => array('CONSOL未处理', 'price_flag_mail'),//consol未处理的任务
        2 => array('CONSOL状态pending', 'price_flag_mail'),//pending的发送
        3 => array('CONSOL未订舱', 'price_flag_mail'),//consol未订舱
        4 => array('CONSOL没有预配', 'price_flag_mail'),//CONSOL没有预配
        5 => array('CONSOL已拒绝', 'price_flag_mail'),//consol已拒绝
        6 => array('CLIENT停用', 'client_stop'),//consol已拒绝
        7 => array('SHIPMENT刷新标记', 'reload_shipment_flag'),//consol已拒绝
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_queue_name($name_id = ''){
        $name = $name_id;
        if(isset($this->queues[$name_id])) $name = $this->queues[$name_id][0];
        return $name;
    }

    public function get_queue_url($name_id = 0){
        $url = '';
        if(isset($this->queues[$name_id])) $url = $this->queues[$name_id][1];
        return $url;
    }

    public function get_one($f = "", $v = "")
    {
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_where_one($where='', $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function total($where = "")
    {
        if ($where != "") $this->db->where($where);
        $this->db->get($this->table);

        return $this->db->count_all_results('');
    }

    public function save($data = array())
    {
        $data["created_time"] = date('Y-m-d H:i:s', time());
        $data["updated_time"] = date('Y-m-d H:i:s', time());
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id = '', $data = array())
    {
        $data["updated_time"] = date('Y-m-d H:i:s', time());
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    /**
     * 根据任务名称,表来源,表主键进行修改, 必须是状态为0未处理的
     * @param string $name
     * @param string $id_type
     * @param string $id_no
     * @param array $data
     * @return mixed
     */
    public function die_queue($name = '', $id_type = '', $id_no = '', $data = array())
    {
        if(!empty($name) && !empty($id_type) && !empty($id_no)){
            $data["updated_time"] = date('Y-m-d H:i:s', time());
            $this->db->where('status', 0);
            $this->db->where('name', $name);
            $this->db->where('id_type', $id_type);
            $this->db->where('id_no', $id_no);
            $this->db->update($this->table, $data);
        }
        return $this->db->affected_rows();
    }

    public function mdelete($id = '')
    {
        // delete container
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

}