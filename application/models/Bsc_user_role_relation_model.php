<?php


class bsc_user_role_relation_model extends CI_Model
{
    private $table = 'bsc_user_role_relation';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get($where = "", $sort = "bsc_user_role_relation.id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    /**
     * 获取
     * @param int $user_id
     */
    public function get_user_role_ids($user_id = 0){
        $rs = $this->get("user_id = $user_id");

        return join(',', array_column($rs, 'user_role_id'));
    }

    /**
     * 保存
     * @param int $user_id
     * @param int $user_role_id
     * @return mixed
     */
    public function save_relation($user_id = 0, $user_role_id = 0){
        $data = array();
        $data['user_id'] = $user_id;
        $data['user_role_id'] = $user_role_id;
        $data['created_by'] = get_session('id');
        $data['created_time'] = date('Y-m-d H:i:s');

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * 删除
     * @param int $user_id
     * @param int $user_role_id
     * @return bool
     */
    public function del_relation($user_id = 0, $user_role_id = 0){
        $where = array();
        $where[] = "user_id = '{$user_id}'";
        $where[] = "user_role_id = '{$user_role_id}'";
        $where = join(' and ', $where);

        // delete container
        $this->db->where($where);
        $this->db->delete($this->table);
        return true;
    }

    /**
     * 保存关联(批量)
     * @param int $user_id
     * @param int $user_role_ids
     * @return bool
     */
    public function batch_save($user_id = 0, $user_role_ids = 0){
        if(empty($user_id)){
            return false;
        }
        $ids_array = filter_unique_array(explode(',', $user_role_ids));

        //查询原本是否存在关联,不存在关联的进行删除
        //将当前所有关联的menu_id都查出来
        $this->db->select('user_role_id');
        $relations = array_column($this->get("user_id = '$user_id'"), 'user_role_id');

        //传入进来的为主,获取的差集为需要新增的
        $add_ids = array_diff($ids_array, $relations);
        foreach ($add_ids as $add_id){
            $this->save_relation($user_id, $add_id);
        }

        //数据库中与传入进来的差集表示要删除的
        $delete_ids = array_diff($relations, $ids_array);
        foreach ($delete_ids as $delete_id){
            $this->del_relation($user_id, $delete_id);
        }

        return true;
    }
}