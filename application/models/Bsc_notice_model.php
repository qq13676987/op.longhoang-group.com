<?php
   class bsc_notice_model extends CI_Model{
       private $table = 'bsc_notice';

       public function __construct()
       {
           parent::__construct();
           $this->load->database();
       }

       public function get_one($f="",$v=""){
           $this->db->where($f, $v);
           $query = $this->db->get($this->table);
           return $query->row_array();
       }
       
       public function get_where_one($where=''){
           if($where == ''){
               return array();
           }
           
           $this->db->where($where);
           $query = $this->db->get($this->table);
           return $query->row_array();
       }

       public function get($where="" ,$sort="id",$order="desc"){
           if($where !="")	$this->db->where($where);
           $this->db->order_by($sort,$order);
           $query = $this->db->get($this->table);
           return $query->result_array();
       }

       public function get_join_read($where="" ,$sort="id",$order="desc",$rows = 30, $offset = 0){
           if($where !="")	$this->db->where($where);
           $userid = $this->session->userdata('id');
           $sql = 'select bsc_notice.*,isread from ' . $this->table . ' left join bsc_notice_user on bsc_notice_user.user_id = ' . $userid . ' and bsc_notice_user.notice_id = '
               . $this->table . '.id WHERE ' . $where . ' ORDER BY ' . $sort . ' ' . $order . ' limit ' . $offset . ',' . $rows;
           $query = $this->db->query($sql);
           return $query->result_array();
       }

       public function total($where=""){
           if($where !="")	$this->db->where($where);
           $this->db->from($this->table);
           return  $this->db->count_all_results('');
       }

       public function save($data=array()){
           $data["create_time"] = date('Y-m-d H:i:s',time());
           $data["update_time"] = date('Y-m-d H:i:s',time());
           $data["create_by"] =  $this->session->userdata('id');
           $data["update_by"] =  $this->session->userdata('id');
           $this->db->insert($this->table, $data);
           return $this->db->insert_id();
       }

       public function update_where($where ,$data=array()){
           $data["update_time"] = date('Y-m-d H:i:s',time());
           $data["update_by"] =  $this->session->userdata('id');
           $this->db->where($where);
           $this->db->update($this->table, $data);
           return true;
       }

       public function update($id='',$data=array()){
           $data["update_time"] = date('Y-m-d H:i:s',time());
           $data["update_by"] =  $this->session->userdata('id');
           $this->db->where('id', $id);
           $this->db->update($this->table, $data);
           return $id;
       }

       public function mdelete($id=''){
           // delete container
           $this->db->where('id', $id);
           $this->db->delete($this->table);
       }

   }