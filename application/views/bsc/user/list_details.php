<title><?= lang('通讯录');?>：<?php echo $name; ?></title>
<style>

    .l {
        font-size: 1rem;
    }

    .select {
        width: 262px;
    }

    .box-left-input-label {
        width: 81px;
        margin-right: 19px;
        display: inline-block;
        text-align: right;
    }

    .box-left-input {
        padding: 5px 0;
    }

</style>
<div style="padding:30px">
    <div style="padding-bottom: 30px;display: flex;align-items: center">
        <span style="font-size:2rem;color: #2d8cf0"><?= $name ?></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <?php 
            $quanxian_id = $id;
            $this->load->model("m_model");
            $leader = $this->m_model->query_one("select id, `name` from bsc_user where id = $leader_id");
            if(empty($leader)) $leader = array("id"=>0,"name"=>"");
            $str ="";
            if($assistant_flag==1){
                $str = lang('{name}  的助理, ', array('name' => $leader["name"]));
                $quanxian_id = $leader["id"];
            }
             
            $group_row = $this->m_model->query_one("select  `group_name` from bsc_group where group_code = '$group'");
        ?>
        <?php if($img != ''):?>
            <img src="<?=$img?>" width="50px" alt="">
        <?php endif;?>
        <span><?= lang('【id: {id}】', array('id' => $id));?></span>
        <span><?= lang('【级别: M{m_level}】', array('m_level' => $m_level));?></span>
        <a href="/main/xmind_by_id/<?= $quanxian_id ?>" target="_blank" style="color:#000;"><?php echo $str; ?>
            <?= lang('【查看权限树状图】');?></a>
        

        <?php if ((is_admin() || in_array('finance', get_session('user_role'))) && !empty($commision_rate_leader)) {//财务可见 ?>
<!--            <button id="set_daiban" style="color:green;"-->
<!--                    onclick="window.open('/bsc_user/commision_rate_leader?id=--><?php //echo $id; ?><!--');">--><?//= lang('【设置团队提成】');?>
<!--            </button>-->
        <?php } ?>



        <?php if (is_admin()) {//财务可见 ?>
            <a href="/main/admin_login/<?php echo $name; ?>" style="color: #000" target="_top"><?= lang('【模拟登陆】');?></a>
        <?php } ?>


        <?php if (is_admin()) { ?>
<!--            <a href="javascript:set_quotation_market();" style="color: #000">--><?//= lang('【设置外部运价审批权限】');?><!--</a>-->
        <?php } ?>

    </div>

<!--    <div style="display:flex;padding-bottom: 20px">-->
<!--        <div style="width:135px">-->
<!--            <label class="l">ID</label>-->
<!--        </div>-->
<!--        <span class="l">--><?//= $id ?><!--</span>-->
<!--    </div>-->
    <div id="tt" class="easyui-tabs" closable="false" style="width:100%;min-height:700px;">
        <div title="<?= lang('基本信息');?>" style="padding:20px;display:none;">
            <div style="display:flex;padding-bottom: 20px" >
                <div style="width:135px">
                    <label class="l"><?= lang('公司');?></label>
                </div>
                <span class="l"><?= $company ?> / <?= !empty($group_row)?$group_row["group_name"]:''; ?></span>
            </div>

            <div style="display:flex;padding-bottom: 20px" >
                <div style="width:135px">
                    <label class="l"><?= lang('英文名');?></label>
                </div>
                <span class="l"><?= $name_en ?></span>
            </div>

<!--            <div style="display:flex;padding-bottom: 20px" >-->
<!--                <div style="width:135px">-->
<!--                    <label class="l">职位</label>-->
<!--                </div>-->
<!--                <span class="l">--><?//= $job_title ?><!--</span>-->
<!--            </div>-->

            <div style="display:flex;padding-bottom: 20px">
                <div style="width:135px">
                    <label class="l"><?= lang('上司');?></label>
                </div>
                <span class="l"><?= $leader["name"]; ?></span>
            </div>

            <div style="display:flex;padding-bottom: 20px">
                <div style="width:135px">
                    <label class="l"><?= lang('角色');?></label>
                </div>
                <span class="l"><?= $user_role ?></span>
            </div>

            <div style="display:flex;padding-bottom: 20px">
                <div style="width:135px">
                    <label class="l"><?= lang('权限');?></label>
                </div>
                <span class="l"><?= $quanxian ?></span>
            </div>

            <div style="display:flex;padding-bottom: 20px">
                <div style="width:135px">
                    <label class="l"><?= lang('手机');?></label>
                </div>
                <span class="l"><?=$telephone_area_code?> - <?= $telephone ?></span>
            </div>

            <div style="display:flex;padding-bottom: 20px">
                <div style="width:135px">
                    <label class="l"><?= lang('座机');?></label>
                </div>
                <span class="l"><?= $telephone_desk ?></span>
            </div>

            <div style="display:flex;padding-bottom: 20px">
                <div style="width:135px">
                    <label class="l"><?= lang('QQ');?></label>
                </div>
                <span class="l"><?= $qq ?></span>
            </div>

            <div style="display:flex;padding-bottom: 20px">
                <div style="width:135px">
                    <label class="l"><?= lang('skype');?></label>
                </div>
                <span class="l"><?= $skype ?></span>
            </div>

            <div style="display:flex;padding-bottom: 20px">
                <div style="width:135px">
                    <label class="l"><?= lang('邮箱');?></label>
                </div>
                <span class="l"><?= $email ?></span>
            </div>
<!--            <div style="display:flex;padding-bottom: 20px">-->
<!--                <div style="width:135px">-->
<!--                    <label class="l">--><?//= lang('入职时间');?><!--</label>-->
<!--                </div>-->
<!--                <span class="l">--><?//= $join_date=='0000-00-00'?'':$join_date ?><!--</span>-->
<!--            </div>-->

<!--            <div style="display:flex;padding-bottom: 20px">-->
<!--                <div style="width:135px">-->
<!--                    <label class="l">群发邮箱</label>-->
<!--                </div>-->
<!--                <span class="l">--><?//= $email_edm ?><!--</span>-->
<!--            </div>-->

            <iframe src="/bsc_user/job_label_tags/<?= $id ?>" name="a" frameborder="0" scrolling="no"
                    style="width:100%;height:500px;">
            </iframe>
        </div>
        <?php if(get_session('id') == $id):?>
        <div title="<?= lang('业绩指标');?>" style="overflow:auto;padding:20px;display:none;">
            <iframe id="commision" style="width: 100%;height: 700px" frameborder="0"></iframe>
        </div>
        <?php endif;?>
    </div>


    <!--<div id="dlg" class="easyui-dialog" style="width:610px;height:81px"-->
    <!-- data-options="closed:true,modal:true,border:'thin'">-->
    <!--    <table id="my_daiban"  class="easyui-datagrid"></table>-->
    <!--</div>-->

</div>

<div id="quotation_market" class="easyui-window" title="<?= lang('设置外部运价审批权限'); ?>" closed="true"
     style="width:730px;height:150px;padding:5px;">
    <div class="box-left-input">
        <label class="box-left-input-label" for=""><?= lang('起运港');?>:</label>
        <select class="easyui-combogrid select" name="trans_origin_name[]" style="width:530px;" id="trans_origin_name"
                data-options="
                    multiple : true,
                    required:false,
                    panelWidth: '1000px',
                    panelHeight: '300px',
                    url:'/biz_port/get_option?type=FCL&limit=true',
                    idField: 'port_code',              //ID字段
                    textField: 'port_code',    //显示的字段
                    striped: true,
                    editable: false,
                    pagination: false,           //是否分页
                    toolbar : '#trans_origin_div',
                    collapsible: true,         //是否可折叠的
                    method: 'get',
                    columns:[[
                        {field:'port_code',title:'<?= lang('港口代码'); ?>',width:100},
                        {field:'port_name',title:'<?= lang('港口名称'); ?>',width:200},
                        {field:'terminal',title:'<?= lang('码头');?>',width:200},
{field:'port_name_cn',title:'<?= lang('备注');?>',width:200},
                        {field:'port_manager_area',title:'<?= lang('口岸划分'); ?>',width:200},
                    ]],
                    mode: 'remote',
                    value: '<?=isset($market['loading_code'])?$market['loading_code']:'';?>',
                    emptyMsg : '<?= lang('未找到相应数据!');?>',
                    required:true,
                    onSelect: function(index, row){
                        if(row !== undefined){
                            $('#trans_origin').textbox('setValue', row.port_code);
                        }
                    },
                    onShowPanel:function(){
                        var opt = $(this).combogrid('options');
                        var toolbar = opt.toolbar;
                        combogrid_search_focus(toolbar);
                    },
                ">
        </select>
    </div>
    <div class="box-left-input">
        <label class="box-left-input-label" for=""><?= lang('船公司');?>:</label>
        <select class="easyui-combobox select" name="trans_carrier[]" style="width:530px;display:hidden;" id="trans_carrier" data-options=" 
                    multiple : true,
                    valueField:'client_code',
				    textField:'client_name',
                    url:'/biz_client/get_option/carrier',
                    value:'<?=isset($market['carrier_code'])?$market['carrier_code']:'';?>',
                    onHidePanel: function() {
                        var valueField = $(this).combobox('options').valueField;
                        var val = $(this).combobox('getValue');
                        var allData = $(this).combobox('getData');
                        var result = true;
                        for (var i = 0; i < allData.length; i++) {
                            if (val == allData[i][valueField]) {
                                result = false;
                            }
                        }
                        if (result) {
                            $(this).combobox('clear');
                        }
                    },
                ">
        </select>
    </div>
    <div class="box-left-input">
        <label class="box-left-input-label" for="">&nbsp;</label>
        <a id="save" href="#" class="easyui-linkbutton"><?= lang('保存');?></a>
    </div>
</div>

<div id="trans_origin_div">
    <form id="trans_origin_form" method="post">
        <div style="padding-left: 5px;display: inline-block;">
            <label><?= lang('港口代码'); ?>:</label><input name="port_code" class="easyui-textbox keydown_search"
                                                       style="width:96px;" t_id="trans_origin_click"/>
            <label><?= lang('港口名称'); ?>:</label><input name="port_name" class="easyui-textbox keydown_search"
                                                       style="width:96px;" t_id="trans_origin_click"/>
            <label><?= lang('口岸划分'); ?>:</label><input name="port_manager_area" class="easyui-textbox keydown_search"
                                                       style="width:96px;" t_id="trans_origin_click"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" id="trans_origin_click" iconCls="icon-search"
               onclick="query_report('trans_origin_form', 'trans_origin_name')"><?= lang('search'); ?></a>
        </div>
    </form>
</div>


<script>

    $('#tt').tabs({
        onSelect: function (title) {
            if (title == "<?= lang('业绩指标');?>") {
                if (document.getElementById("commision").src == "") {
                    document.getElementById("commision").src = "/bsc_user_commision_additional/index?user_id=<?=$id?>";
                }
            }
        }
    });
    //设置代理
    $("#set_daiban").on("click",function(){
        if (confirm('<?= lang('你将授权代班同事可以查看你账户下的所有数据,代班结束请及时撤销代班,请再次确认?');?>')){
           $.ajax({
                type:'POST',
                url:'/bsc_user/set_replacement',
                data:{
                    from_user_id: <?= get_session('id') ?>,
                    to_user_id:<?= $id ?>,
                },
                dataType:'json',
                success:function (res) {
                  alert(res.msg);
                  window.location.reload();
                },
                error:function (e) {
                    $.messager.alert('<?= lang('提示');?>', e.responseText);
                }
            });
        }
    });
    
    $("#end_daiban").on("click",function(){
        var from_user_id = $('#from_user_id').val();
        $.ajax({
            type:'POST',
            url:'/bsc_user/end_replacement',
            data:{
                from_user_id: from_user_id,
            },
            dataType:'json',
            success:function (res) {
              alert(res.msg);
              window.location.reload();
            },
            error:function () {
                $.messager.alert('<?= lang('提示');?>', e.responseText);
            }
        });
    })


    function set_quotation_market() {
        
        <?php
            if(strpos($user_role,'marketing') !== false){  
            }else{
                echo "alert('" . lang('此人不是市场,无法设置运价审批权限') . "'); return;";
            }
        ?> 
        
        $('#quotation_market').window('open');
        //如果没有市场角色，提示无法设置运价审批权限

    }

    function query_report(form_id, combo_id) {
        var where = {};
        var form_data = $('#' + form_id).serializeArray();
        $.each(form_data, function (index, item) {
            if (item.value == "") return true;
            if (where.hasOwnProperty(item.name) === true) {
                if (typeof where[item.name] == 'string') {
                    where[item.name] = where[item.name].split(',');
                    where[item.name].push(item.value);
                } else {
                    where[item.name].push(item.value);
                }
            } else {
                where[item.name] = item.value;
            }
        });
        if (where.length == 0) {
            $.messager.alert('<?= lang('提示');?>', '<?= lang('请填写需要搜索的值');?>');
            return;
        }
        //判断一下当前是 remote 还是local,如果是remode,走下面这个
        var inp = $('#' + combo_id);
        var opt = inp.combogrid('options');
        if (opt.mode == 'remote') {
            inp.combogrid('grid').datagrid('load', where);
        } else {
            //如果是本地的,那么先存一下
            if (opt.old_data == undefined) opt.old_data = inp.combogrid('grid').datagrid('getData'); // 如果没存过,这里存一下
            var rows = [];
            if (JSON.stringify(where) === '{}') {
                inp.combogrid('grid').datagrid('loadData', opt.old_data.rows);
                return;
            }
            $.each(opt.old_data.rows, function (i, obj) {
                for (var p in where) {
                    var q = where[p].toUpperCase();
                    var v = obj[p].toUpperCase();
                    if (!!v && v.toString().indexOf(q) >= 0) {
                        rows.push(obj);
                        break;
                    }
                }
            });
            if (rows.length == 0) {
                inp.combogrid('grid').datagrid('loadData', []);
                return;
            }

            inp.combogrid('grid').datagrid('loadData', rows);
        }
    }

    /**
     * 下拉选择表格展开时,自动让表单内的查询获取焦点
     */
    function combogrid_search_focus(form_id, focus_num = 0) {
        // var form = $(form_id);
        setTimeout("easyui_focus($($('" + form_id + "').find('.keydown_search')[" + focus_num + "]));", 200);
    }

    /**
     * easyui 获取焦点
     */
    function easyui_focus(jq) {
        var data = $.data(jq[0]);
        //这个页面里,只有4个
        //textbox,combobox,datebox,datetimebox
        if (data.combogrid !== undefined) {
            return $(jq).combogrid('textbox').focus();
        } else if (data.combobox !== undefined) {
            return $(jq).combobox('textbox').focus();
        } else if (data.datetimebox !== undefined) {
            return $(jq).datetimebox('textbox').focus();
        } else if (data.datebox !== undefined) {
            return $(jq).datebox('textbox').focus();
        } else if (data.textbox !== undefined) {
            return $(jq).textbox('textbox').focus();
        }
    }

    //保存外部运价设置
    $("#save").on("click", function () {

        //获取船公司和起运港代码
        var trans_origin = $('#trans_origin_name').combobox('getValues');
        var trans_carrier = $('#trans_carrier').combobox('getValues');
        if (trans_origin == '') {
            alert('<?= lang('请选择起运港');?>');
            return;
        }
        // if (trans_carrier == '') {
        //     alert('请选择船公司');
        //     return;
        // }

        $.ajax({
            type: 'POST',
            url: '/bsc_user/set_quotation_market',
            data: {
                user_id: '<?= $id ?>',
                trans_origin: trans_origin,
                trans_carrier: trans_carrier,
            },
            dataType: 'json',
            success: function (res) {
                alert(res.msg);
                window.location.reload();
            },
            error: function () {
                $.messager.alert('<?= lang('提示');?>', e.responseText);
            }
        });
    })

</script>

