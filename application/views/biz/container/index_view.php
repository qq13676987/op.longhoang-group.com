    <script type="text/javascript">
        function doSearch(){
            $('#tt').datagrid('load',{
                container_no: $('#container_no').val()
            });
        }
        $.fn.edatagrid.defaults.onError = function(index, row){
            $.messager.alert('error', row.msg);
        };
        
        var is_define = false;
        
        $(function(){ 
            $('#tt').edatagrid({
                url: '/biz_container/get_data/<?php echo $consol_id;?>/<?php echo $si;?>',
                saveUrl: '/biz_container/add_data/<?php echo $consol_id;?>' ,
                updateUrl: '/biz_container/update_data/' ,
                destroyUrl: '/biz_container/delete_data',
                onBeforeSave:function (index) {
                    var data = $('#tt').datagrid('getData');
                    var ed = $('#tt').datagrid('getEditor', {index:index,field:'container_no'});
                    var container_no = $(ed.target).val().toUpperCase();
                    var id = data.rows[index].id;
                    if(ed == null)return false;
                    $(ed.target).val(container_no);
                    var result = docheck(container_no);
                    var repeat = true;
                    $.each(data.rows, function (index, item) {
                        if(id != item.id && item.container_no == container_no && !item.hasOwnProperty('isNewRecord')){
                            repeat = false;
                        }
                    });
                    if(!repeat){
                        $.messager.alert('Tips', '<?= lang('container_no');?>不能重复');
                        return false;
                    }else{
                        if(!result && !is_define){
                            $.messager.confirm('Tips', '箱号校验出错, 是否确认提交？,', function(r){
                                if (r){
                                    // 退出操作;
                                    is_define = true;
                                    $('#tt').edatagrid('saveRow');
                                }
                            });
                            return false;
                        }
                        is_define = false;
                        return true;
                        // $('#tt').edatagrid('saveRow');
                    }
                }
            });
			$('#tt').datagrid({
				width:'auto',
				height: $(window).height() - 230,
			    <?php if($lock_lv > 0){ ?>
                onDblClickCell:function(index,field) {
                },
                <?php } ?>
            }); 
			
			$(window).resize(function () {
				$('#tt').datagrid('resize');
			});
        });
            
    </script>
    <script language="javascript">
        //去除字符串的空格
        function gf_trim(as_string)
        {
            while(as_string.length > 0 && as_string.indexOf(" ")==0) as_string = as_string.substr(1);
            while(as_string.length > 0 && as_string.lastIndexOf(" ")==(as_string.length-1)) as_string = as_string.substr(0,as_string.length-1);
            return as_string;
        }
        //集装箱箱号验证
        //功能：验证集装箱箱号：
        //参数：
        //   as_cntrno 是否符合国际标准，
        //返回值：True 符合国际标准或强行通过(特殊箱号)
        //举例：gf_chkcntrno( 'TEXU2982987', 0 )
        function chkcntrno(as_cntrno,ai_choice)
        {
            var fi_ki;
            var fi_numsum;
            var fi_nummod;
            var fai_num = new Array(11);
            var fb_errcntrno=false;

            if (as_cntrno==null) return true; //null不进行验证
            if (gf_trim(as_cntrno)=="") return true; //空不进行验证
            as_cntrno = gf_trim(as_cntrno);

            if (as_cntrno.length == 11)   //国际标准为11位，最后一位是校验位，若不是11位肯定不是标准箱
            { for(fi_ki=1;fi_ki<=11;fi_ki++)
                fai_num[fi_ki] = 0;
                for(fi_ki=1;fi_ki<=4;fi_ki++)     //根据国际标准验证法则处理箱号前面的4个英文字母
                {
                    fch_char=as_cntrno.charAt(fi_ki-1).toUpperCase();
                    switch(true)
                    { case (fch_char=="A"):{fai_num[fi_ki] = 10;break;}
                        case (fch_char>="V" && fch_char<="Z"):{fai_num[fi_ki] = fch_char.charCodeAt() - 52;break;}
                        case (fch_char>="L" && fch_char<="U"):{fai_num[fi_ki] = fch_char.charCodeAt() - 53;break;}
                        default:{fai_num[fi_ki] = fch_char.charCodeAt() - 54;break;}
                    }
                }
                for(fi_ki=5;fi_ki<=11;fi_ki++)
                {  fch_char=as_cntrno.charAt(fi_ki-1);
                    fai_num[fi_ki] = parseInt(fch_char); //ctype((mid(as_cntrno, fi_ki, 1)), integer)
                }
                fi_numsum = 0

                for(fi_ki=1;fi_ki<=10;fi_ki++)
                {
                    fi_sqr = 1;
                    for(i=1;i<fi_ki;i++){fi_sqr *=2;}
                    fi_numsum += fai_num[fi_ki] * fi_sqr;
                }

                if (as_cntrno.substr(0,4) == "HLCU") fi_numsum = fi_numsum - 2; //hapaq lloyd的柜号与国际标准相差2
                fi_nummod = fi_numsum % 11;
                if (fi_nummod == 10) fi_nummod = 0;
                if (fi_nummod == fai_num[11]) fb_errcntrno = true;
                return fb_errcntrno;
            }else{
                return fb_errcntrno;
            }
        }

        function docheck(text){
            text = text.replace(/[ ]/g,"");
            text = text.replace(/[\r\n]/g,"");
            return chkcntrno(text ,0);
        }
    </script>
<table id="tt" style="width:1100px;height:450px"
            rownumbers="false" pagination="true"  idField="id"
            pagesize="30" toolbar="#tb"  singleSelect="true" nowrap="false">
        <thead>
            <tr>
                <th field="id" width="60" sortable="true">ID</th>      
                <th field="container_no" width="150" align="center"  editor="text" sortable="true" >container_no</th>
                <th field="seal_no" width="111" align="center"  editor="text">seal_no</th>  
                <th field="container_tare" width="111" align="center"  editor="{
                    type:'numberbox',
                    options:{ 
                        min:0,
                        precision:2
                    }
                }">container_tare</th>  
				<th field="container_size" width="111" align="center" editor="{
                            type:'combobox',
                            options:{ 
                                valueField:'value', 
								textField:'value', 
								url:'/bsc_dict/get_option/container_size', 
                            }
                }">container_size</th> 
                <th field="vgm" width="111" align="center">vgm</th>
				<th field="packs" width="111" align="center">packs</th>
				<th field="weight" width="111" align="center">weight</th>
				<th field="volume" width="111" align="center">volume</th>
            </tr>
        </thead>
</table>

   <div id="tb" style="padding:3px;">
        <table>
            <tr>
                <?php if($lock_lv < 1){?>
                <td>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#tt').edatagrid('addRow',0)">add</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#tt').edatagrid('destroyRow')">delete</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#tt').edatagrid('saveRow');">save</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#tt').edatagrid('cancelRow')">cancel</a>
                </td>
                <td width='80'> </td>
                <?php }?>
                <td>
                    <span style="font-size:12px;">container_no</span>
                    <input id="container_no" style="border:1px solid #ccc">
                </td>
                <td>
                    <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" onclick="doSearch()">search</a>
                </td>
            </tr>
        </table>
         
    </div>
