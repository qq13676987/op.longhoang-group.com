<?php

/**
 * yitong 报文相关
 */
class bsc_message_yitong extends Common_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->model = Model('bsc_message_model');
    }

    public function so_checking($consol, $shipments){
        if(empty($consol)){
            return 'consol empty';
        }
        //没有订舱勾时,不能生成
        if($consol['dingcangwancheng'] == '0' || $consol['dingcangwancheng'] == '0000-00-00 00:00:00') return 'SO必须勾选订舱勾后才能生成';

//        if(empty($shipments)){
//            return 'shipment empty';
//        }
        //校验承运人是否为ESL
        $trans_carrier = array(
            'ESL' => 'XGALQC01',
            'KMTC' => 'GLHYZS01',
            'VOLTA' => 'XJPHL01',
            'ONE' => 'WLCWZG01',
        );
        if(!in_array($consol['trans_carrier'], $trans_carrier)){
            return 'trans_carrier 未设置';
        }

        //船公司单独判断--start
        if($consol['trans_carrier'] === 'XGALQC01'){
            if(empty($consol['agent_ref'])){
                return 'agent_ref';
            }
            if(substr($consol['agent_ref'], 0 , 4) !== 'SHNQ'){
                return 'agent_ref 必须 \'SHNQ\'前缀';
            }
        }
        //船公司单独判断--end




        // if(empty($consol['AGR_no'])){
        // return 'AGR_no';
        // }
//        if(empty($shipments[0]['trans_term'])){
//            return 'trans_term';
//        }
//        if(empty($shipments[0]['release_type'])){
//            return 'release_type';
//        }
//        if(empty($shipments[0]['INCO_term'])){
//            return 'INCO_term';
//        }
//        if(empty($shipments[0]['goods_type'])){
//            return 'goods_type';
//        }
        if(empty($consol['goods_type'])){
            return 'goods_type';
        }
        if(empty($consol['payment'])){
            return 'payment';
        }
        if(empty($consol['trans_term'])){
            return 'trans_term';
        }
        if(empty($consol['release_type'])){
            return 'release_type';
        }
        if(empty($consol['trans_origin'])){
            return 'trans_origin';
        }
        if(empty($consol['vessel'])){
            return 'vessel';
        }
        if(empty($consol['voyage'])){
            return 'voyage';
        }
        if(empty($consol['trans_discharge'])){
            return 'trans_discharge';
        }
        if(empty($consol['payment'])){
            return 'payment';
        }
        if(empty($consol['shipper_company'])){
            return 'shipper_company';
        }
        //company 加地址长度不能超过450
        // $shipper_address_length_max = 450 - 75;
        $shipper_address_length_max = 280;
        if(strlen($consol['shipper_address']) > $shipper_address_length_max){
            return 'SHIPPER 地址长度不能超过' . $shipper_address_length_max . '字符';
        }
        //company 加地址长度不能超过450
        $consignee_address_length_max = 280;
        // $consignee_address_length_max = 450 - 75;
        if(strlen($consol['consignee_address']) > $consignee_address_length_max){
            return 'CONSIGNEE 地址长度不能超过' . $consignee_address_length_max . '字符';
        }
        //company 加地址长度不能超过450
        // $notify_address_length_max = 450 - 75;
        $notify_address_length_max = 280;
        if(strlen($consol['notify_address']) > $notify_address_length_max){
            return 'NOTIFY 地址长度不能超过' . $notify_address_length_max . '字符';
        }
        if(empty($consol['consignee_company'])){
            return 'consignee_company';
        }
        if(empty($consol['hs_code'])){
            return 'hs_code';
        }
        if(strlen($consol['hs_code']) > 10){
            return 'hs_code 长度不能超过10';
        }
        if(empty($consol['mark_nums'])){
            return 'mark_nums';
        }
        if(empty($consol['notify_company'])){
            return 'notify_company';
        }
        if(empty($consol['description'])){
            return 'description';
        }
        if(empty($consol['trans_mode'])){
            return 'trans_mode';
        }
        if(empty($consol['container_owner'])){
            return 'container_owner';
        }
        if(empty($consol['box_info'])){
            return 'box_info';
        }
        if($consol['trans_carrier'] === 'XGALQC01'){
            if(empty($consol['AP_code'])){
                return 'AP_code';
            }
        }
        // if(empty($consol['carrier_ref'])){
        // return 'carrier_ref';
        // }

        return true;
    }

    /**
     * 生成报文 SO
     * @param $consol_id
     */
    public function standard_so($consol_id, $file_function = 9, $carrier="ESL"){

        $this->load->model('biz_consol_model');
        $this->load->model('biz_shipment_model');
        $this->load->model('biz_port_model');
        $this->load->model('biz_container_model');
        $this->load->model('bsc_dict_model');
        $this->load->model('m_model');


        $message = array(
            '00' => array(
                'RECORD ID' => '00',
                'MESSAGE TYPE' => 'IFTMBF',
                'FILE DESCRIPTION' => 'BOOKING',
                'FILE FUNCTION' => '',
                'SENDER CODE' => '',
                'RECEIVER CODE' => $carrier,
                'FILE CREATE TIME' => '',
            ),
            '02' => array(
                'RECORD ID' => '02',
                'BOOKING NO.ID' => '',
                'B\L NO.' => '',
                'DELIVERY TERM' => '',
                'BOOKING PARTY' => '',
                'ISSUE PARTY CODE' => '',
                'ISSUE PARTY' => '',
                'APPLICANT' => '',
                'FOB BK PARTY' => '',
                'B/L TRANSHIP ID' => '',
                'BATCH ID' => '',
                'SHIPMENT DATE' => '',
                'EXPIRY DATE' => '',
                'QUOTATION NO.' => '',
                'CHARGE TYPE' => '',
                'S/C NO.' => '',
                'BOND NO.' => '',
                'SLOT CHARTER ID' => '',
                'EMAIL' => '',
                'NAME' => '',
                'TEL' => '',
                'Commodity Group Code' => '',
                'invoiceTitle' => '',
                'shipmentCondition' => '',
            ),
            '03' => array(
                'RECORD ID' => '03',
                'TYPE OF B/L FORM' => '',
                'PLACE OF B/L ISSUE CODE' => '',
                'PLACE OF B/L ISSUE' => '',
                'DATE OF ISSUE' => '',
                'NUMBERS OF ORIGINAL B/L' => '',
                'PREPAID AT' => '',
                'PAYABLE AT' => '',
            ),
            '11' => array(
                'RECORD ID' => '11',
                'VESSEL CODE' => '',
                'VESSEL' => '',
                'VOYAGE' => '',
                'SHIPPING LINE CODE' => '',
                'SHIPPING LINE' => '',
                'B/L CARRY CODE' => '',
                'B/L CARRY' => '',
                'REQUESTED SHIPMENT DATE' => '',
                'TRADE CODE' => '',
                'TRADE' => '',
                'PRE.VESSEL CODE' => '',
                'PRE.VESSEL' => '',
                'PRE.VOYAGE' => '',
            ),
            '12' => array(
                'RECORD ID' => '12',
                'PLACE CODE OF RECEIPT' => '',
                'PLACE OF RECEIPT' => '',
                'LOAD PORT CODE' => '',
                'LOAD PORT' => '',
                'DISCHARGE PORT CODE' => '',
                'DISCHARGE PORT' => '',
                'TRANSFER PORT CODE' => '',
                'TRANSFER PORT' => '',
                'PLACE OF DELIVERY CODE' => '',
                'PLACE OF DELIVERY' => '',
                'FINAL DESTINATION CODE' => '',
                'FINAL DESTINATION' => '',
                'Depot Code' => '',
                'CTN PORT' => '',
                'CTN PORT NAME' => '',
            ),
            '13' => array(
                'RECORD ID' => '13',
                'OPT.DISCH.PORT CODE' => '',
                'OPT.DISCH.PORT' => '',
                'OPT.PLACE OF DELIVERY CODE' => '',
                'OPT.PLACE OF DELIVERY' => '',
            ),
            '14' => array(
                'RECORD ID' => '14',
                'FR.CLAUSE.CODE' => '',
                'FREIGHT CLAUSE-I-1' => '',
                'FREIGHT CLAUSE-I-2' => '',
            ),
            '15' => array(
                'RECORD ID' => '15',
                'FR.CH.CODE' => '',
                'FR.CH.REMARK' => '',
                'PREPAID OR COLLECT' => '',
                'PAYABLE AT(E)第三' => '',
                'PAYEE CODE' => '',
                'QUANTITY' => '',
                'CURRENCY' => '',
                'RATE OF FR.CH.' => '',
                'UNIT OF QUANTITY' => '',
                'AMOUNT' => '',
            ),
            '17' => array(
                'RECORD ID' => '17',
                'REMARKS-1' => '',
                'REMARKS-2' => '',
                'REMARKS-3' => '',
                'REMARKS-4' => '',
                'REMARKS-5' => '',
                'CURRENCY' => '',
                'CARGO AMOUNT' => '',
                'MODE OF STUFFING' => '',
                'SPEC VANNING' => '',
            ),
            '18' => array(
                'RECORD ID' => '18',
                'REMARKS-1' => '',
                'REMARKS-2' => '',
                'REMARKS-3' => '',
                'REMARKS-4' => '',
                'REMARKS-5' => '',
            ),
            '20' => array(
                'RECORD ID' => '20',
                'SHIPPER CODE' => '',
                'SHIPPER-1' => '',
                'SHIPPER-2' => '',
                'SHIPPER-3' => '',
                'SHIPPER-4' => '',
                'SHIPPER-5' => '',
                'SHIPPER-6' => '',
            ),
            '21' => array(
                'RECORD ID' => '21',
                'CONSIGNEE CODE' => '',
                'CONSIGNEE-1' => '',
                'CONSIGNEE-2' => '',
                'CONSIGNEE-3' => '',
                'CONSIGNEE-4' => '',
                'CONSIGNEE-5' => '',
                'CONSIGNEE-6' => '',
            ),
            '22' => array(
                'RECORD ID' => '22',
                'NOTIFY CODE' => '',
                'NOTIFY-1' => '',
                'NOTIFY-2' => '',
                'NOTIFY-3' => '',
                'NOTIFY-4' => '',
                'NOTIFY-5' => '',
                'NOTIFY-6' => '',
            ),
            '23' => array(
                'RECORD ID' => '23',
                'NOTIFY CODE' => '',
                'ALSONOTIFY-1' => '',
                'ALSONOTIFY-2' => '',
                'ALSONOTIFY-3' => '',
                'ALSONOTIFY-4' => '',
                'ALSONOTIFY-5' => '',
                'ALSONOTIFY-6' => '',
            ),
            '26' => array(
                'RECORD ID' => '26',
                'CP CODE' => '',
                'CP-1' => '',
                'CP-2' => '',
                'CP-3' => '',
                'CP-4' => '',
                'CP-5' => '',
                'CP-6' => '',
            ),
            '41' => array(
                'RECORD-ID' => '41',
                'CARGO SEQUENCE NO.' => '',
                'CARGO CODE' => '',
                'CARGO ID' => '',
                'NUMBERS OF PKGS-1' => '',
                'CODE OF PKGS-1' => '',
                'PACKAGES DES-1' => '',
                'CARGO GROSS WT-1' => '',
                'CARGO MEASUREMENT-1' => '',
                'NUMBERS OF PACKAGES-2' => '',
                'CODE OF PKGS-2' => '',
                'PACKAGES DES.-2' => '',
                'CARGO GROSS WT-2' => '',
                'CARGO MEASUREMENT-2' => '',
                'CARGO GROSS WEIGHT' => '',
                'CARGO NET WEIGHT' => '',
                'QUARANTINE CODING CODE' => '',
                'QUARANTINE CODING' => '',
            ),
            '43' => array(
                'RECORD ID' => '43',
                'CLASS' => '',
                'PAGE' => '',
                'UNDG NO.' => '',
                'LABEL' => '',
                'FLASH POINT' => '',
                'EMS NO.' => '',
                'MFAG NO.' => '',
                'MPT(MARINE POLLUTANT)' => '',
                'EMERGENCY CONTACT' => '',
                'REEFER VENTILATION FLUX' => '',
                'TEMPERATURE ID' => '',
                'TEMPERATURE SETTING' => '',
                'MIN.TEMPERATURE' => '',
                'MAX.TEMPERATURE' => '',
                'OVER LENGTH FRONT' => '',
                'OVER LENGTH BACK' => '',
                'OVER WIDTH LEFT' => '',
                'OVER WIDTH RIGHT' => '',
                'OVER HEIGHT' => '',
                'IMDG Amend No' => '',
                'PACKING_GROUP' => '',
                'Actual size L' => '',
                'Actual size W' => '',
                'Actual size H' => '',
                'VARIANT' => '',
                'TECHNICALNAME' => '',
                'PACKING CODE' => '',
                'OUT PACKAGE' => '',
                'proper shipping name' => '',
                'GROSS WT (OUTER)' => '',
                'NET WT (OUTER)' => '',
                'EMERGENCY CONTACT PERSON' => '',
                'S/L/G' => '',
                'LABEL-2' => '',
                'LABEL-3' => '',
                'LABEL-4' => '',
                'LABEL-5' => '',
            ),
            '44' => array(
                'RECORD ID' => '44',
                'MARKS-1' => '',
                'MARKS-2' => '',
                'MARKS-3' => '',
                'MARKS-4' => '',
                'MARKS-5' => '',
                'MARKS-6' => '',
                'MARKS-7' => '',
                'MARKS-8' => '',
                'MARKS-9' => '',
                'MARKS-10' => '',
            ),
            '47' => array(
                'RECORD ID' => '47',
                'CARGO DESCRIPTION-1' => '',
                'CARGO DESCRIPTION-2' => '',
                'CARGO DESCRIPTION-3' => '',
                'CARGO DESCRIPTION-4' => '',
                'CARGO DESCRIPTION-5' => '',
            ),
            '48' => array(
                'RECORD ID' => '48',
                'CTN.SIZE&TYPE' => '',
                'CTN.NUMBERS' => '',
                'CTN.STATUS' => '',
                'MASTER LCL NO.' => '',
                'MODE OF STUFFING' => '',
                'VANNING DEPOT CODE' => '',
                'VANNING DEPOT' => '',
                'CONTAINER SOC.' => '',
                'CTN.NO.' => '',
                'L or G' => '',
                'OVER WEIGHT' => '',
                'ACTIVE REEFER' => '',
                'PLAN VANNING DATE' => '',
                'OGG' => '',
                'SPECIAL REQUEST' => '',
                'CTN.NUMBERS OF PACKAGES' => '',
                'CARGO WEIGHT' => '',
                'CARGO MEASUREMENT' => '',
            ),
            '51' => array(
                'RECORD-ID' => '51',
                'CTN.NO.' => '',
                'CTN.TYPE&SIZE' => '',
                'SEAL NO.' => '',
                'CTN.NUMBERS OF PACKAGES' => '',
                'CARGO NET WEIGHT' => '',
                'CARGO TARE WEIGHT' => '',
            ),
            '99' => array(
                'RECORD ID' => '99',
                'RECORD TOTAL OF FILE' => '',
            ),
        );

        $result = array('code' => 1, 'msg' => 'error');
        //获取或转换对应的数据--start
        $consol = $this->biz_consol_model->get_one('id', $consol_id);
        foreach($consol as $key => $val){
            $val_json = json_decode($val, true);
            if(is_array($val_json)){
                continue;
            }
            //替换冒号
            $consol[$key] = str_replace(':', '?:', $val);
        }
        $shipments = $this->biz_shipment_model->no_role_get("consol_id =  '" . $consol['id'] . "'");
        foreach($shipments as $skey => $shipment){
            foreach ($shipment as $key => $val){
                $val_json = json_decode($val, true);
                if(is_array($val_json)){
                    continue;
                }
                //替换冒号
                $shipment[$key] = str_replace(':', '?:', $val);
            }
        }
        $checking = $this->so_checking($consol, $shipments);
        if($checking !== true){
            $result['msg'] = lang($checking) . ',请检查';
            echo json_encode($result);
            return false;
        }

        //获取港口全称--start
        $trans_origin_port = $this->biz_port_model->get_one('port_code', $consol['trans_origin']);
        if(empty($trans_origin_port)){
            $result['msg'] = lang('trans_origin');
            echo json_encode($result);
            return false;
        }
        $trans_origin_name = $trans_origin_port['port_name'];

        //DISCHARGE PORT CODE' => '',是POD， 7 单元是POD 全名，TRANSFER PORT CODE是第一中转港代码，
        //PLACE OF DELIVERY CODE是交货地代码，11单元是交货地全名， 参看以下我修改的范例
        $trans_destination_port = $this->biz_port_model->get_one('port_code', $consol['trans_destination']);
        if(empty($trans_destination_port)){
            $result['msg'] = lang('trans_destination');
            echo json_encode($result);
            return false;
        }
        $trans_destination_name = $trans_destination_port['port_name'];
        $trans_discharge_port = $this->biz_port_model->get_one('port_code', $consol['trans_discharge']);
        if(empty($trans_discharge_port)){
            $result['msg'] = lang('trans_discharge');
            echo json_encode($result);
            return false;
        }
        $trans_discharge_name = $trans_discharge_port['port_name'];
        //2021-05-26如果是KMTC,5字代码切为后3位
        $trans_origin = $consol['trans_origin'];
        $trans_discharge = $consol['trans_discharge'];
        $trans_destination = $consol['trans_destination'];
        if($carrier == 'KMTC'){
            //单独查下KMTC_PORT是否有值，有的直接取
            $trans_origin_port = $this->bsc_dict_model->get_one('name', $consol['trans_origin'], 'KMTC_PORT');
            if(!empty($trans_origin_port)) $trans_origin = $trans_origin_port['value'];
            $trans_destination_port = $this->bsc_dict_model->get_one('name', $consol['trans_destination'], 'KMTC_PORT');
            if(!empty($trans_destination_port)) $trans_discharge = $trans_destination_port['value'];
            $trans_discharge_port = $this->bsc_dict_model->get_one('name', $consol['trans_discharge'], 'KMTC_PORT');
            if(!empty($trans_discharge_port)) $trans_destination = $trans_discharge_port['value'];
        }
        //获取港口全称--end

        //转换 付款方式
        if($consol['payment'] == 'PP'){
            $payment = 'P';
            $INCO_term = "FREIGHT PREPAID";
        }else if($consol['payment'] == 'CC'){
            $payment = 'C';
            $INCO_term = 'FREIGHT COLLECT';
        }else if($consol['payment'] == 'E'){
            $payment = 'E';
            if(empty($consol['payment_third'])){
                $result['msg'] = lang('payment_third');
                echo json_encode($result);
                return false;
            }
            $INCO_term = "FREIGHT PREPAID AT {$consol['payment_third']}" ;
        }else{
            $result['msg'] = lang('payment');
            echo json_encode($result);
            return false;
        }
        // 根据贸易方式 转换FREIGHT CLAUSE-I
        //改为根据consol的PP和CC判定
//        if($shipments[0]['INCO_term'] == 'FOB'){
//            $INCO_term = 'FREIGHT COLLECT';
//        }else{
//            $INCO_term = 'FREIGHT PREPAID';
//        }

        //修改匹配的放单方式值
//        if($shipments[0]['release_type'] == 'OBL') {
//            $release_type = 'ORI';
//        }else if($shipments[0]['release_type'] == 'SWB'){
//            $release_type = 'EXP';
//        }else if($shipments[0]['release_type'] == 'TER'){
//            $release_type = 'TER';
//        }else{
//            $result['msg'] = lang('release_type');
//            echo json_encode($result);
//            return false;
//        }
        if($consol['release_type'] == 'OBL') {
            $release_type = 'ORI';
        }else if($consol['release_type'] == 'SWB'){
            $release_type = 'EXP';
        }else if($consol['release_type'] == 'TER'){
            $release_type = 'TER';
        }else{
            $result['msg'] = lang('release_type');
            echo json_encode($result);
            return false;
        }

        //转换 货物类型
        if($consol['goods_type'] == 'GC'){
            $goods_type = 'S';
        }else if($consol['goods_type'] == 'RF'){
            $goods_type = 'R';
        }else if($consol['goods_type'] == 'DR'){
            $goods_type = 'D';
        }else{
            $result['msg'] = lang('goods_type');
            echo json_encode($result);
            return false;
        }

        //转换 数量单位 为指定code
//        $good_outers = 0;
//        $good_weight = 0;
//        $good_volume = 0;
//        foreach ($shipments as $shipment){
//            $good_outers += $shipment['good_outers'];
//            $good_weight += $shipment['good_weight'];
//            $good_volume += $shipment['good_volume'];
//        }
//        $good_outers_unit = $shipment['good_outers_unit'];
        $good_outers = $consol['good_outers'];
        $good_weight = $consol['good_weight'];
        $good_volume = $consol['good_volume'];
        $good_outers_unit = $consol['good_outers_unit'];
        //查询字典表获取该code
        $iso_code = $this->bsc_dict_model->get_one('name', $good_outers_unit, 'packing_unit');
        if(!empty($iso_code)){
            $good_outers_unit_CODE = $iso_code['value'];
        }else{
            $result['msg'] = lang('good_outers_unit_code');
            echo json_encode($result);
            return false;
        }

        // 转换 运输模式
        if($consol['trans_mode'] == 'FCL'){
            $trans_mode = 'F';
        }else if($consol['trans_mode'] == 'LCL'){
            $trans_mode = 'L';
        }else{
            $result['msg'] = lang('trans_mode');
            echo json_encode($result);
            return false;
        }

        // 转换 货主箱标记 container_owner
        if($consol['container_owner'] == 'SOC'){
            $container_owner = 'Y';
        }else if($consol['container_owner'] == 'COC'){
            $container_owner = 'N';
        }else{
            $result['msg'] = lang('container_owner');
            echo json_encode($result);
            return false;
        }
        //获取或转换对应的数据--end

        //开始填入数据
        //00:IFTMBF:BOOKING:9:CNESL587676:ESL:202012151022'
        $message['00']['FILE FUNCTION'] = $file_function;
        if($carrier=="ESL") $message['00']['SENDER CODE'] = 'CNESL592316';
        if($carrier=="KMTC")  $message['00']['SENDER CODE'] = 'TPSKXC00';

        $message['00']['RECEIVER CODE'] = $carrier;//ESL
        $message['00']['FILE CREATE TIME'] = date('YmdHi');//202012151022

        //02:DS201202884:201202884:CY-CY:CNESL587676:::::::::CNESL587676:::::lucas@dsi-log.com:lucas:0532-88257831:::'
        $message['02']['BOOKING NO.ID'] = $consol['job_no'];//DS201202884
        $message['02']['B\L NO.'] = $consol['agent_ref'];//'SHNQ' . date('y') . $consol['id'];//201202884 提单号 船公司单号$consol['carrier_ref']
        $message['02']['DELIVERY TERM'] = $consol['trans_term'];//CY-CY 运输条款
        $message['02']['BOOKING PARTY'] = $message['00']['SENDER CODE'];// CNESL587676  公司专用代买,CNESL + client_code?
//        $message['02']['ISSUE PARTY CODE'] = '';//  发行方代码?  待定
        //ap_code不存在调用sender
        $ap_code = $consol['AP_code'];
        if($ap_code == ''){
            $ap_code = $message['00']['SENDER CODE'];
        }
        $message['02']['QUOTATION NO.'] = $ap_code;//CNESL587676  AP CODE
        // $message['02']['S/C NO.'] = $consol['AGR_no'];//  批复号

        //03:ORI:CNTAO:QINGDAO PORT::03::'
        $message['03']['TYPE OF B/L FORM'] = $release_type;//ORI 放单方式,电放等
        $message['03']['PLACE OF B/L ISSUE CODE'] = $trans_origin;// CNTAO 同起运港code?
        $message['03']['PLACE OF B/L ISSUE'] = $trans_origin_name;//QINGDAO PORT 同起运港名称?
        $message['03']['NUMBERS OF ORIGINAL B/L'] = '03';//03  类似提单里面的THREE?  默认为03

        //11::EMIRATES SANA:02053W::::::::::'
        $message['11']['VESSEL CODE'] = $consol['sailing_code'];// VESSEL CODE 是航线代码
        $message['11']['VESSEL'] = $consol['vessel'];//EMIRATES SANA  船名
        //航次裁剪掉v.
        $consol['voyage'] =  strtr($consol['voyage'], 'V.', '');
        $consol['voyage'] =  strtr($consol['voyage'], 'v.', '');
        $message['11']['VOYAGE'] = $consol['voyage'];//02053W  航次
        // $message['11']['REQUESTED SHIPMENT DATE'] = date('Ymd');//20201230  日期

        //12:CNTAO:QINGDAO PORT:CNTAO:QINGDAO PORT:TZDAR:DAR ES SALAAM:::TZDAR:DAR ES SALAAM:TZDAR:DAR ES SALAAM:::'
        $message['12']['PLACE CODE OF RECEIPT'] =  $trans_origin;//CNTAO  同起运港code?
        $message['12']['PLACE OF RECEIPT'] = $trans_origin_name;//QINGDAO PORT 同起运港名称?
        $message['12']['LOAD PORT CODE'] = $trans_origin;//CNTAO  起运港code
        $message['12']['LOAD PORT'] = $trans_origin_name;//QINGDAO PORT  起运港名称
        $message['12']['DISCHARGE PORT CODE'] = $trans_discharge;//TZDAR  POD
        $message['12']['DISCHARGE PORT'] = $trans_discharge_name;//DAR ES SALAAM POD
        $message['12']['PLACE OF DELIVERY CODE'] = $trans_destination;//TZDAR 交货地代码
        $message['12']['PLACE OF DELIVERY'] = $trans_destination_name;//DAR ES SALAAM 交货地名称
//        $message['12']['FINAL DESTINATION CODE'] = $consol['trans_destination'];//TZDAR 目的地代码
//        $message['12']['FINAL DESTINATION'] = $trans_destination_name;//DAR ES SALAAM 目的地名称

//        $message['13']['OPT.DISCH.PORT CODE'] = '';// ??  选择出口代码
//        $message['13']['OPT.DISCH.PORT'] = '';// ??

        //14:P:FREIGHT PREPAID'
        $message['14']['FR.CLAUSE.CODE'] = $payment;//P 类似文件的付款方式那里?
        $message['14']['FREIGHT CLAUSE-I-1'] = $INCO_term;//FREIGHT PREPAID
//        $message['14']['FREIGHT CLAUSE-I-2'] = '';//FREIGHT PREPAID

        //15:::P:::::::'
        $message['15']['PREPAID OR COLLECT'] = $payment;//P

        $message['17']['REMARKS-1'] = '';//不需要?
        $message['17']['REMARKS-2'] = '';//不需要?
        $message['17']['REMARKS-3'] = '';//不需要?
        $message['17']['REMARKS-4'] = '';//不需要?
        $message['17']['REMARKS-5'] = '';//不需要?

//        $message['18']['REMARKS-1'] = '';//不需要?
//        $message['18']['REMARKS-2'] = '';//不需要?
//        $message['18']['REMARKS-3'] = '';//不需要?
//        $message['18']['REMARKS-4'] = '';//不需要?
//        $message['18']['REMARKS-5'] = '';//不需要?

        //20::SHANDONG WEICHAI IMPORT AND EXPORT CORPORATION,:197A,FUSHOU EAST STREET,HIGH-TECH DEVELOPMENT ZONE,:WEIFANG,SHANDONG,CHINA.:::'
//        $message['20']['SHIPPER CODE'] = '';

        $message['20']['SHIPPER-1'] = $consol['shipper_company'];
        $shipper_address = explode("\n", $consol['shipper_address']);
        $shipper_address = cut_array_length($shipper_address, 75, 6);
        for ($i = 0; $i < sizeof($shipper_address) ; $i++){
            if($i > 4){
                break;
            }
            $message['20']['SHIPPER-' . ($i + 2)] = $shipper_address[$i];
        }

        //21::WAN DA VEHICLE(TZ) CO.LIMITED:PLOT NO.7, BLOCK NO.A KINONDONI,:DAR ES SALAAM,TANZANIA:::'
//        $message['21']['CONSIGNEE CODE'] = '';
        $message['21']['CONSIGNEE-1'] = $consol['consignee_company'];
        $consignee_address = explode("\n", $consol['consignee_address']);
        $consignee_address = cut_array_length($consignee_address, 75, 6);
        for ($i = 0; $i < sizeof($consignee_address) ; $i++){
            if($i > 4){
                break;
            }
            $message['21']['CONSIGNEE-' . ($i + 2)] = $consignee_address[$i];
        }

        //22::SAME AS CONSIGNEE:::::'
//        $message['22']['NOTIFY CODE'] = '';
        $message['22']['NOTIFY-1'] = $consol['notify_company'];//SAME AS CONSIGNEE
        $notify_address = explode("\n", $consol['notify_address']);
        $notify_address = cut_array_length($notify_address, 75, 6);
        for ($i = 0; $i < sizeof($notify_address) ; $i++){
            if($i > 4){
                break;
            }
            $message['22']['NOTIFY-' . ($i + 2)] = $notify_address[$i];
        }

//        $message['23']['NOTIFY CODE'] = '';
//        $message['23']['ALSONOTIFY(1-6)'] = '';//类似NOTIFY级别的

//        $message['26']['CP CODE'] = '';//不需要?
//        $message['26']['CP(1-6)'] = '';//不需要?

        //41:001:840999:S:14:IA:PACKAGES:13517.000:43.540::::::13517.000:::'
        $message['41']['CARGO SEQUENCE NO.'] = '001';//001  货物编号,任意
        $message['41']['CARGO CODE'] = $consol['hs_code'];//840999 hscode

        //S: Normal
        //R:RF
        //D: DG
        //O: OOG
        $message['41']['CARGO ID'] = $goods_type;////S货物类型 S 普货
//        $good_weight_unit = $shipment['good_weight_unit'];
//        $good_volume_unit = $shipment['good_volume_unit'];
        $message['41']['NUMBERS OF PKGS-1'] = $good_outers;//14 数量或者圈数?
        $message['41']['CODE OF PKGS-1'] = $good_outers_unit_CODE;//IA
        $message['41']['PACKAGES DES-1'] = $good_outers_unit;//PACKAGES 货物数量单位?
        $message['41']['CARGO GROSS WT-1'] = $good_weight;//13517.000  货物重量
        $message['41']['CARGO MEASUREMENT-1'] = $good_volume;//43.540  货物体积
        $message['41']['CARGO GROSS WEIGHT'] = $good_weight;//13517.000  货物重量

        //44:N/M:::::::::'
        $mark_nums = explode("\n", $consol['mark_nums']);
        for ($i = 0; $i < sizeof($mark_nums) ; $i++){
            if($i > 9){
                break;
            }
            $message['44']['MARKS-' . ($i + 1)] = $mark_nums[$i];//N/M 货物唛头
        }

        //47:SPARE PARTS FOR DIESEL ENGINE AND GENERATOR SET::::'
        $description = explode("\n", $consol['description']);
        $description = cut_array_length($description, 70, 5);
        for ($i = 0; $i < sizeof($description) ; $i++){
            if($i > 4){
                break;
            }
            $message['47']['CARGO DESCRIPTION-' . ($i + 1)] = $description[$i];//SPARE PARTS FOR DIESEL ENGINE AND GENERATOR SET 货物描述
        }

        //48:45G1:1:F:::::N::::::::::'
        $box_info = json_decode($consol['box_info'], true);
        $message_48 = $message['48'];
        $message['48'] = array();
        foreach ($box_info as $row){
            $this_array = $message_48;
            $size_iso = $this->bsc_dict_model->get_one('value', $row['size'],'container_size');
            $this_array['CTN.SIZE&TYPE'] = $size_iso['name'];//45G1 箱型
            $this_array['CTN.NUMBERS'] = $row['num'];//1 箱量
            $this_array['CTN.STATUS'] = $trans_mode;//F/L 运输模式 trans_mode

            $this_array['CONTAINER SOC.'] = $container_owner;//Y=SOC N=COC
            $message['48'][] = $this_array;
        }

        //51::45G1::14:13517.000:'
//         $containers = $this->biz_container_model->get("consol_id = '" . $consol['id'] . "'");
//         if(empty($containers)){
//             // $result['msg'] = 'consol container';
//             // echo json_encode($result);
//             echo 'consol container';
//             return;
//         }
//         $message_51 = $message['51'];
//         $message['51'] = array();
//         foreach ($containers as $container){
//             $this_array = $message_51;

//             $sql = "SELECT IFNULL(SUM(biz_shipment_container.`packs`), 0) AS packs, IFNULL(SUM(biz_shipment_container.`weight`),0) AS weight, IFNULL(SUM(biz_shipment_container.`volume`),0) AS volume FROM biz_shipment, biz_shipment_container WHERE `biz_shipment_container`.`shipment_id` = `biz_shipment`.id AND `biz_shipment`.`consol_id` = " .
//                 $consol['id'] . " AND `biz_shipment_container`.`container_no` = '" . $container["container_no"] . "'";
//             $temp_rs= $this->m_model->sqlquery($sql);
//             $sum_sp_containers  = $temp_rs->row_array();

//             //循环进行获取
//             $this_array['CTN.NO.'] = $container['container_no'];//箱号
//             $container_size_iso = $this->bsc_dict_model->get_one('value', $container['container_size'],'container_size');
//             $this_array['CTN.TYPE&SIZE'] = $container_size_iso['name'];//45G1 箱型
//             $this_array['SEAL NO.'] = $container['seal_no'];// 箱封号
//             $this_array['CTN.NUMBERS OF PACKAGES'] = $sum_sp_containers['packs'];//14  箱数量或者数量
//             $this_array['CARGO NET WEIGHT'] = $sum_sp_containers['weight'];//13517.000 净重
// //        $message['51']['CARGO TARE WEIGHT'] = '';// 毛重
//             $message['51'][] = $this_array;
//         }

        //99:16'
        $message['99']['RECORD TOTAL OF FILE'] = 1;
        $must = array(17);//若为空,必须存在的

        //清除除了记录ID,全为空的数组
        foreach ($message as $key => $row){
            //如果是二维数组
            if(count($row, 1) > count($row)){
                foreach ($row as $r){
                    if(sizeof(array_filter($r)) <= 1 && !in_array($key, $must)){
                        unset($message[$key]);
                    }
                }
            }else{
                if(sizeof(array_filter($row)) <= 1 && !in_array($key, $must)){
                    unset($message[$key]);
                }
            }
        }
        $message['99']['RECORD TOTAL OF FILE'] += sizeof($message) - 1;//16 记录文件总数
        return $message;
    }

    public function message_manifest($consol_id, $file_function = 9){
        $this->load->model('biz_consol_model');
        $this->load->model('biz_shipment_model');
        $this->load->model('biz_port_model');
        $this->load->model('biz_container_model');
        $this->load->model('bsc_dict_model');
        $this->load->model('bsc_user_model');
        $this->load->model('biz_shipment_container_model');

        $tip_messsage = array();
        //获取到consol信息
        $consol = $this->biz_consol_model->get_one('id', $consol_id);
        //
        $consol_check_field = array('payment', 'trans_origin', 'trans_discharge', 'trans_destination', 'carrier_agent', 'trans_carrier', 'vessel', 'voyage', 'job_no', 'trans_mode', 'booking_ETD');
        foreach ($consol as $key => $val){
            if(!in_array($key, $consol_check_field)) continue;
            if(in_array($key, array('requirements', 'floor_price', 'operator', 'carrier_agent'))){
                $mach = check_banjiao_cn($val);
            }else{
                $mach = check_banjiao($val);
            }
            if(sizeof($mach[0]) > 0){
                echo json_encode(array('code' => 444, 'msg' =>  lang($key) . '存在特殊字符: ' . join('', $mach[0]) . ',生成失败', 'ts_str' => $mach[0], 'text' => $val));
                exit();
            }
        }


        //获取货物信息
        $shipments = $this->biz_shipment_model->no_role_get("consol_id =  '$consol_id'");

        //判断支付方式
        if($consol['payment']=="PP"){
            $consol['paymenttermcode']="P";
            $consol['paymentterm']="FREIGHT PREPAID";
        }else if($consol['payment']=="CC"){
            $consol['paymenttermcode']="C";
            $consol['paymentterm']="FREIGHT COLLECT";

        }else if($consol['payment']=="E"){
            $consol['paymenttermcode']="E";
            $consol['paymentterm']="FREIGHT PAYABLE AT XXXXX";

        }else{
            $result['msg'] = lang('no payment');
            echo json_encode($result);
            return;
        }

        //获取港口全称--start
        $trans_origin_port = $this->biz_port_model->get_one('port_code', $consol['trans_origin']);
        if(empty($trans_origin_port)){
            $result['msg'] = lang('no code: trans_origin');
            echo json_encode($result);
            return;
        }
        $trans_origin_name = $trans_origin_port['port_name'];

        $trans_discharge_port = $this->biz_port_model->get_one('port_code', $consol['trans_discharge']);
        if(empty($trans_origin_port)){
            $result['msg'] = lang('no code: trans_discharge');
            echo json_encode($result);
            return;
        }
        $trans_discharge_name = $trans_discharge_port['port_name'];

        $trans_destination_port = $this->biz_port_model->get_one('port_code', $consol['trans_destination']);
        if(empty($trans_origin_port)){
            $result['msg'] = lang('no code: trans_destination');
            echo json_encode($result);
            return;
        }
        $trans_destination_name = $trans_destination_port['port_name'];


        //获取港口全称--end

        //船代--start
        if(empty($consol['carrier_agent'])){
            $result['msg'] = lang('船代不能为空');
            echo json_encode($result);
            return;
        }
        $this->load->model('bsc_dict_model');
        $carrier_agent = $this->bsc_dict_model->get_one('value', $consol['carrier_agent'], 'carrier_agent');
        if(empty($carrier_agent)){
            $result['msg'] = lang('当前船代不存在对应值');
            echo json_encode($result);
            return;
        }
        $carrier_agent_code = $carrier_agent['ext1'];
        if(empty($carrier_agent_code)){
            $result['msg'] = lang('当前船代不存在对应值');
            echo json_encode($result);
            return;
        }

        //船代--end

        //船司--start
        //2022-07-18 原船司改为取值 实际承运人 $consol['trans_carrier'] => $consol['trans_carrier_second']
        $trans_carrier = $this->bsc_dict_model->get_by_ext1_one('name', $consol['trans_carrier_second'], 'manifest_trans_carrier', 'cangdan');
        if(empty($consol['trans_carrier'])){
            $result['msg'] = lang('承运人不能为空');
            echo json_encode($result);
            return;
        }
        if(empty($trans_carrier)){
            $result['msg'] = lang('当前承运人 ' . $consol['trans_carrier_second'] . ' 不存在对应值');
            echo json_encode($result);
            return;
        }
        $trans_carrier_name = $trans_carrier['value'];
        //船司--end

        if(empty($consol['vessel'])){
            $result['msg'] = lang('船名不能为空');
            echo json_encode($result);
            return;
        }
        if(empty($consol['voyage'])){
            $result['msg'] = lang('航次不能为空');
            echo json_encode($result);
            return;
        }

        /*
        可能要移动到下面的newshipments的循环里去check

        $checking = $this->manifest_yitong_checking($consol, $shipments);
        $result = array('code' => 1, 'msg' => 'error');
        if($checking !== true){
            $result['msg'] = lang($checking);
            echo json_encode($result);
            return;
        }
        */


        $xml = '<?xml version="1.0" encoding="GB2312"?><edi><head>';
        // 拼接head
        $this->load->model('bsc_user_model');
        $u_row = $this->bsc_user_model->get_by_id($this->session->userdata("id"));
        $email = $u_row["email"];
        if(strpos($email,'@') !== false){
        }else{
            $result['msg'] = lang('no email');
            echo json_encode($result);
            return;
        }
        $xml_head = array(
            'sndcom' => 'SHJXGYL',//发送方公司
            'sender' => 'op_depart_02',//发送方部门
            'fromop' => 'YZZ',//发送方操作员简称
            'fromtele' => '021-61072456',//操作员联系电话
            'fromfax' => '',
            'frommail' => $email,//操作员联系邮件地址
            'fromopc' => $u_row["telephone"],// 操作员联系手机
            'fromope' => 'guest',
            'sendtime' =>date('m/d/Y H:i:s',time()),//报文发送时间
            'editype' => 'PREMFT3',//报文类型
            'EdiID'=>'0F9729AB-A847-4E61-897A-7085C850195A',//报文参考号
            'EdiName'=> $consol['job_no'],//报文文件名
            'EdiMode'=> 'HYXML',//报文格式
            'EdiReceiver' => 'NBEPORT'//接收方EDI代码
        );

        $xml .= xml_arr_to_str($xml_head);
        $xml .="</head><body>";

        // 拼接body， 循环多个Billbody
        $new_shipments = array(); // 考虑到儿子的情况，要把儿子的shipment也push进来
        foreach($shipments as $shipment){
            //1.1、爸爸票 提单号取儿子关单号 且箱信息读取所有儿子票的
            //1.2、单身票 直接读取提单号，箱信息为自身
            //查询是否有子票
            $child_shipments = $this->biz_shipment_model->no_role_get("parent_id = '{$shipment['id']}'");
            if(!empty($child_shipments)){
                foreach($child_shipments as $item){
                    array_push($new_shipments,$item);
                }
            }else{
                array_push($new_shipments,$shipment);
            }
        }
        $shipment_check_field = array('release_type', 'job_no', 'cus_no', 'shipper_company', 'shipper_address', 'shipper_telephone', 'shipper_email' , 'consignee_company', 'consignee_address', 'consignee_telephone', 'consignee_contact', 'notify_company', 'notify_address', 'notify_telephone', 'notify_email', 'good_outers_unit', 'trans_discharge', 'consignee_email', 'goods_type', 'description');
        foreach ($new_shipments as $new_shipment){
            foreach ($new_shipment as $key => $val){
                if(!in_array($key, $shipment_check_field)) continue;
                if(in_array($key, array())){
                    $mach = check_banjiao_cn($val);
                }else{
                    $mach = check_banjiao($val);
                }
                if(sizeof($mach[0]) > 0){
                    echo json_encode(array('code' => 444, 'msg' =>  $new_shipment['job_no'] . ' ' . lang($key) . '存在特殊字符: ' . join('', $mach[0]) . ',生成失败', 'ts_str' => $mach[0], 'text' => $val));
                    exit();
                }
            }
        }

        //2022-11-01 徐婕提出 多个shipment时, 只要有一个一致就不提示
        $cus_no_arr = array_column($new_shipments, 'cus_no');
        if(!in_array($consol['carrier_ref'], $cus_no_arr)) $tip_messsage[] = "未找到与consol关单号一致的shipment";

        foreach($new_shipments as $shipment){
            if(trim($consol['shipper_address']) == ''){
                echo json_encode(array('code' => 1, 'msg' => 'SHIPPER 地址必填且不能为空'));
                return;
            }
            $shipment["shipper_address"] = str_replace("\n","",$shipment["shipper_address"]);
            $shipment["consignee_address"] = str_replace("\n","",$shipment["consignee_address"]);
            $shipment["notify_address"] = str_replace("\n","",$shipment["notify_address"]);

            //2022-03-24 原本的限制不能超过170,改为直接170以外的砍掉,徐婕提出
            if(strlen($shipment["shipper_address"]) > 170){
                $shipment["shipper_address"] = substr($shipment["shipper_address"], 0, 170);
                // $result['msg'] = lang('SHIPPER 地址不能超过170个字符');
                // echo json_encode($result);
                // return;
            }

            if(strlen($shipment["consignee_address"]) > 170){
                $shipment["consignee_address"] = substr($shipment["consignee_address"], 0, 170);
                // $result['msg'] = lang('CONSIGNNE 地址不能超过170个字符');
                // echo json_encode($result);
                // return;
            }

            if(strlen($shipment["notify_address"]) > 170){
                $shipment["notify_address"] = substr($shipment["notify_address"], 0, 170);
                // $result['msg'] = lang('NOTIFY 地址不能超过170个字符');
                // echo json_encode($result);
                // return;
            }

            //默认填充电话--start
            $default_phone = '61525450';
            if(trim($shipment['shipper_telephone']) == '') $shipment['shipper_telephone'] = $default_phone;
            if(trim($shipment['consignee_telephone']) == '') $shipment['consignee_telephone'] = $default_phone;
            if(trim($shipment['notify_telephone']) == '') $shipment['notify_telephone'] = $default_phone;
            if(trim($shipment['consignee_contact']) == '') $shipment['consignee_contact'] = 'JIE';
            //默认填充电话--end
            //获取报账单位代码--start
            $this_dict = $this->bsc_dict_model->get_one('name', $shipment['good_outers_unit'], 'packing_unit');
            if(empty($this_dict)){
                $result['msg'] = lang('no code: good_outers_unit');
                echo json_encode($result);
                return;
            }
            $good_outers_unit_code = $this_dict['value'];
            //获取报账单位代码--start
            // 拼接 reference
            if(strlen(trim($shipment['cus_no']))<4) {
                $result['msg'] = "{$shipment['job_no']} 关单号不能空";
                echo json_encode($result);
                return;
            }
            $xml .="<Billbody>";
            $xml .="<reference>";
            $xml_ref = array(
                'enfreight' => $file_function, //报文功能(9：新增 3:订舱 5：修改 1：取消)
                'refid' => '',                  //
                'freid' => '',
                'referenceno' => $shipment['job_no'],//客户端系统托单ID
                'bookingno' =>  $shipment['job_no'],  //操作系统托单ID
                'workno' =>  $shipment['job_no'],//工作编号
                'sale' => '',
                'billofladingno' => $shipment['cus_no']//提单号
            );
            $xml .= xml_arr_to_str($xml_ref);
            $xml .="</reference>";


            $xml .="<detail>";
            $xml .="<baseinfo>";
            // 获取部分特殊值
            $orderman = "Shanghai leagueshipping logistic";
            if(substr($shipment['job_no'],0,1)=='K' ||substr($shipment['job_no'],0,2)=='KM') $orderman = "Shanghai Jinxin supplier chain";
            $base1 = array(
                'orderman' => $orderman,//根据job_no名称
                'source' => '',
                'usdaccountshipper' => '',
                'customshipper' => '',
                'rmbaccountshipper' => '',
                'paymenttermcode' => $consol['paymenttermcode'], //付款方式代码(P/C/E)
                'paymentterm' => $consol['paymentterm'],//付款方式
                'payableat' => '',
                'shippingitem' => 'CY-CY', // 可以直接写死，$shipment['trans_term'],//运费条款
                'billofladingtype' => $shipment['release_type'],//提单类型
            );
            $xml .= xml_arr_to_str($base1);
            preg_match_all('/\d+/', $shipment['shipper_telephone'], $mach);
            $shipper_telephone = join('', $mach[0]);
            $shipper_address = substr($shipment['shipper_address'], 0, 70);

            $xml .="<shipper>";
            $shipper = array(
                'shippercode' => '9999+9999',
                'shippername' => ts_replace($shipment['shipper_company']),
                'shipperaddress' => $shipper_address,
                'shippertele' => $shipper_telephone,
                'shippermail' =>$shipment['shipper_email'],
                'shipperfax' =>'',
                'shippercity' => '',
                'shippersubcode' => '',
                'shippersub' => '',
                'shipperpost' => '',
                'shippercountry' => substr($consol['trans_origin'],0,2),
                'shipperAEO' => '',
            );
            $xml .= xml_arr_to_str($shipper);
            $xml .="</shipper>";
            preg_match_all('/\d+/', $shipment['consignee_telephone'], $mach);
            $consignee_telephone = join('', $mach[0]);
            $xml .="<consignee>";
            if($shipment['consignee_address']=='') $shipment['consignee_address'] = $shipment['consignee_company'];
            $consignee_address = substr($shipment['consignee_address'], 0, 70);

            $consignee = array(
                'consigneecode' => '9999+9999',
                'consigneename' => ts_replace($shipment['consignee_company']),
                'consigneeaddress' => $consignee_address,
                'consigneetele' => $consignee_telephone,
                'consigneemail' => $shipment['consignee_email'],
                'consigneefax' => '',
                'consigneecity' => '',
                'consigneesubcode' => '',
                'consigneesub' => '',
                'consigneepost' => '',
                'consigneecountry' => substr($consol['trans_discharge'],0,2),
                'consigneeperson' => 'leagueshipping',
                'consigneepersontele' => $shipment['consignee_telephone'],
                'consignepersonemail' =>$shipment['consignee_email'],
                'consigneepersonfax' =>'',
                'consigneeAEO' =>'',
            );
            $xml .= xml_arr_to_str($consignee);
            $xml .="</consignee>";

            preg_match_all('/\d+/', $shipment['notify_telephone'], $mach);
            $notify_telephone = join('', $mach[0]);
            $xml .="<notifyparty>";
            if($shipment['notify_address']=='') $shipment['notify_address'] = $shipment['notify_company'];
            $notify_company = ts_replace($shipment['notify_company']);
            $notify_address = substr($shipment['notify_address'], 0, 70);
            $notify_email = $shipment['notify_email'];
            if($shipment['notify_company'] == 'SAME AS CONSIGNEE'){
                $notify_company = ts_replace($shipment['consignee_company']);
                $notify_address = $consignee_address;
                $notify_telephone = $consignee_telephone;
                $notify_email = $shipment['consignee_email'];
            }
            $notifyparty = array(
                'notifycode' => '9999+9999',
                'notifyname' => $notify_company,
                'notifyaddress' => $notify_address,
                'notifytele' => $notify_telephone,
                'notifymail' => $notify_email,
                'notifyfax' => '',
                'notifycity' => '',
                'notifysubcode' => '',
                'notifysub' => '',
                'notifypost' => '',
                'notifycountry' => substr($consol['trans_discharge'],0,2),
                'notifyAEO' => ''
            );
            $xml .= xml_arr_to_str($notifyparty);
            $xml .="</notifyparty>";

            $xml .="<notifyparty2>";
            $notifyparty = array(
                'notifycode' =>  '',
                'notifyname' =>  '',
                'notifyaddress' =>  '',
                'notifytele' =>  '',
                'notifymail' =>  '',
                'notifyfax' => '',
                'notifycity' => '',
                'notifysubcode' => '',
                'notifysub' => '',
                'notifypost' => '',
                'notifycountry' => '',
                'notifyAEO' => ''
            );
            $xml .= xml_arr_to_str($notifyparty);
            $xml .="</notifyparty2>";

            $base2 = array(
                'ordermemo' => '',
                'numberoforiginals' => '3',
                'numberofcopys' => '3',
                'placeofissuecode' => $consol['trans_origin'],  //起运港
                'placeofissue' =>  $trans_origin_port['port_name'],//起运港全称
                'timeofissue' => '', //签单时间
            );
            $xml .= xml_arr_to_str($base2);
            $xml .="</baseinfo>";

            //cargo booking 两个节点都是非必填，直接复制报文
            $xml .="<cargo><APPLICANT/><BOKKINGPARTY/><FOBPARTY/><SCNO/><QUARANTINECODING/><SCACCODE/><QUOTATIONNO/><CHARGETYPE/><NVOCCHBLNO/><realshipper><text/></realshipper><realconsignee><text/></realconsignee><realnotifyparty><text/></realnotifyparty><realnotifyparty2><text/></realnotifyparty2></cargo>";
            $xml .="<booking><shipmentdate/><expirydate/><tranship/><batch/><quotationno/></booking>";

            $xml .="<freight>";
            $flcl =  $consol['trans_mode'];
            if($flcl=="FCL") $flcl = 'F';
            if($flcl=="LCL") $flcl = 'L';
            if($flcl!='F' && $flcl!='L') {
                $result['msg'] = lang('FCL OR LCL ?');
                echo json_encode($result);
                return;
            }
            $xml .="<fl>$flcl</fl><masterlclno/><requestdate/><lastdate/><arrivedate/>";

            $xml .="<port>";
            $port = array(
                'placeofreceipt' => $trans_origin_name, //装货港
                'portofloading' => $trans_origin_name, //装货港
                'portoftranship' => '',
                'portofdischarge' => $trans_discharge_name, //卸货港
                'placeofdelivery' => $trans_destination_name,//目的港
                'finaldestination' => '',
                'co_placeofreceipt' => $consol['trans_origin'],//收货地，默认和装货港代码
                'co_portofloading' => $consol['trans_origin'],//装货港代码
                'co_portoftranship' => '',//中转港代码
                'co_portofdischarge' => $consol['trans_discharge'],//卸货港代码
                'co_placeofdelivery' => $consol['trans_destination'],//目的港代码
                'co_finaldestination' => ''
            );
            $xml .= xml_arr_to_str($port);
            $xml .="</port>";

            $xml .="<vessel>";
            $vessel = array(
                'carrier' => $trans_carrier_name,  //船公司
                'shipagent' => $carrier_agent_code,//$consol['carrier_agent'], //船代
                'co_shipagent' => $carrier_agent_code,//船代代码
                'oceanvessel' => $consol['vessel'],//船名
                'voyno' => $consol['voyage'],//二程航次
                'etd' => $consol['booking_ETD'], //二程开航日期
                'deliverymode' => '',
            );
            $xml .= xml_arr_to_str($vessel);
            $xml .="</vessel>";

            $xml .="<info2nd><carrier/><oceanvessel/><voyno/><etd/><deliverymode/><remarks><text/></remarks></info2nd>";

            $xml .="<container>";

            $containers = $this->biz_shipment_container_model->join_container_get_by_id($shipment['id'], $consol_id);

            if(empty($containers)){
                $result['msg'] = $shipment['job_no'] . ' ' . lang('container 不存在');
                echo json_encode($result);
                return;
            }
            $total_packs = 0;
            $total_weight = 0;
            $total_volume = 0;
            foreach ($containers as $container){
                $total_packs += $container['packs'];
                $total_weight += $container['weight'];
                $total_volume += $container['volume'];
                $xml .="<containers>";
                $this_dict = $this->bsc_dict_model->get_one('value', $container['container_size'], 'container_size');
                if(empty($this_dict)){
                    $result['msg'] = lang('no code: container size');
                    echo json_encode($result);
                    return;
                }
                $container_size_code = $this_dict['name'];
                $temp_arr = array(
                    'containertype' => $container_size_code,//箱型
                    'containerno' => $container['container_no'],    //箱号
                    'sealno'=> $container['seal_no'],  //封号
                    'containernoofpkgs' => $container['packs'],//件数
                    'containergrossweight' => $container['weight'],//毛重
                    'containercbm' => $container['volume'], //体积
                    'containerpackagingcode' => $good_outers_unit_code,//箱货物包装类型代码
                    'containerpackaging' => $shipment['good_outers_unit'], //箱货物包装类型
                    'containermarksandnumbers' =>'',
                );
                $xml .= xml_arr_to_str($temp_arr);
                $xml .="</containers>";
            }
            $xml .="</container>";
            $xml .="</freight>";

            $xml .="<goods>";
            $xml .="<marksandnumbers><text>N/M</text></marksandnumbers>";
            $xml .="<noofpkgs>$total_packs</noofpkgs><packagingcode>$good_outers_unit_code</packagingcode><packaging>".$shipment['good_outers_unit']."</packaging><cargoid>".$shipment['goods_type']."</cargoid><hscode/>";

            $xml .="<description>";
            $description = substr($shipment['description'], 0, 256);
            $descriptions = explode("\n",$description);
            foreach($descriptions as $descp){
                $xml .="<text>";
                $descp = str_replace("&","&amp;",$descp);
                $descp = str_replace("<","&lt;",$descp);
                $descp = str_replace(">","&gt;",$descp);
                $xml .=$descp;
                $xml .="</text>";
            }

            $xml .="</description>";

            $xml .="<cdescription><text/></cdescription><grossweight>$total_weight</grossweight><cbm>$total_volume</cbm><specialgoods><text/></specialgoods>";

            $xml .="</goods>";
            if($shipment['goods_type']=='DR'){
                $this->load->model('biz_shipment_dangergoods_model');
                $dr_row = $this->biz_shipment_dangergoods_model->get_one("shipment_id",$shipment["id"]);
                if(empty($dr_row)){
                    $result['msg'] = lang('缺危险品参数');
                    $result['a'] = $dr_row;
                    $result['b'] = $shipment['id'];
                    echo json_encode($result);
                    return;
                }
                $CLASS = $dr_row["isdangerous"];
                $UNDGNO = $dr_row["UN_no"];
                $xml .="<dr><DANGEROUS><text/></DANGEROUS><CLASS>$CLASS</CLASS><PAGE/><UNDGNO>$UNDGNO</UNDGNO><LABEL><text/></LABEL><FLASHPOINT/><Elinkman/><TEMPERATURE/><TEMPERATUREUNIT/><REEFER><text/></REEFER></dr>";
            }else{
                $xml .="<dr><DANGEROUS><text/></DANGEROUS><CLASS/><PAGE/><UNDGNO/><LABEL><text/></LABEL><FLASHPOINT/><Elinkman/><TEMPERATURE/><TEMPERATUREUNIT/><REEFER><text/></REEFER></dr>";
            }
            $xml .="<loadplan><entryno/><warehouse/><landcarrier/><stuffdate/><stuffmode/><stuffaddress/><stuffmemo/></loadplan>";
            $xml .="<announce><text/></announce><remarks><text/></remarks>";
            $xml .="</detail>";

            $xml .="<goodsdetail>";
            $xml .="<cargosequenceno>1</cargosequenceno><noofpkgs>$total_packs</noofpkgs><packagingcode>$good_outers_unit_code</packagingcode><packaging>".$shipment['good_outers_unit']."</packaging><cargoid>".$shipment['goods_type']."</cargoid><hscode/><grossweight>$total_weight</grossweight><cbm>$total_volume</cbm><undgcode/><class/><currentcode/><origincountrycode/><description>";
            $description = $shipment['description'];
            $descriptions = explode("\n",$description);
            foreach($descriptions as $descp){
                $xml .="<text>";
                $descp = str_replace("&","&amp;",$descp);
                $descp = str_replace("<","&lt;",$descp);
                $descp = str_replace(">","&gt;",$descp);
                $xml .=$descp;
                $xml .="</text>";
            }
            $xml .= "</description><specialgoods/>";

            $xml .= "<marksandnumbers>";
            $mark_nums = $shipment['mark_nums'];
            $mark_nums = explode("\n",$mark_nums);
            foreach($mark_nums as $mncp){
                $xml .="<text>";
                $mncp = str_replace("&","&amp;",$mncp);
                $mncp = str_replace("<","&lt;",$mncp);
                $mncp = str_replace(">","&gt;",$mncp);
                $xml .= $mncp;
                $xml .="</text>";
            }
            $xml .= "</marksandnumbers>";

            $xml .="<container>";
            foreach ($containers as $container){
                $xml .="<containers>";
                $this_dict = $this->bsc_dict_model->get_one('value', $container['container_size'], 'container_size');
                if(empty($this_dict)){
                    $result['msg'] = lang('no code: good_outers_unit');
                    echo json_encode($result);
                    return;
                }
                $container_size_code = $this_dict['name'];
                $temp_arr = array(
                    'containertype' => $container_size_code,//箱型
                    'containerno' => $container['container_no'],    //箱号
                    'sealno'=> $container['seal_no'],  //封号
                    'containernoofpkgs' => $container['packs'],//件数
                    'containergrossweight' => $container['weight'],//毛重
                    'containercbm' => $container['volume'], //体积
                );

                $xml .= xml_arr_to_str($temp_arr);
                $xml .="</containers>";
            }
            $xml .="</container>";

            $xml .="</goodsdetail>";

            $xml .="</Billbody>";
        }

        $xml .="</body></edi>";
        // $xml = iconv('UTF-8', 'GBK//IGNORE', $xml);
        if (preg_match_all("/^([\x81-\xfe][\x40-\xfe])+$/", $xml, $match)) {
            echo json_encode(array('code' => 1, 'msg' => '存在中文:' . join('', $match[0])));
            return;
        }
        $xml = mb_convert_encoding($xml, "UTF-8", "GBK");
        //保存文件xml
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        //ESL_EDI/日期
        $new_dir = $root_path . '/upload/message/yitong_EDI/MANIFEST/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $consol['id'] . '.xml';
        $file_path = $new_dir . $file_name;
        file_put_contents($file_path, $xml);
        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => 'MANIFEST',
            'receiver' => 'yitong',
        );
        $id = update_message($consol_id, $message_data, 'MANIFEST');
        if(!empty($tip_messsage)) $tip_messsage = ",提示!!: " . join(',', $tip_messsage);
        else $tip_messsage = "";
        echo json_encode(array('code' => 0, 'msg' => '文件生成成功' . $tip_messsage, 'data' => array('id' => $id)));

    }

    public function iftcps($consol_id, $file_function = 9){
        $consol = Model('biz_consol_model')->get_where_one("id = '{$consol_id}'");
        foreach($consol as $key => $val){
            $val_json = json_decode($val, true);
            if(is_array($val_json)){
                continue;
            }
            //替换冒号
            $consol[$key] = str_replace(':', ' ', $val);
        }
        $shipments = Model('biz_shipment_model')->no_role_get("consol_id =  '$consol_id'");
        //感觉应该要获取子shipment,再说--TODO
        if(empty($shipments)){
            return jsonEcho(array('code' => 1, 'msg' => '请绑定shipment后再试'));
        }

        //需要发送的shipment信息
        $send_shipments = array();
        foreach($shipments as $skey => $shipment){
            //检查是否有子shipment,有的话主的不取了,取子的
            $child_shipments = $this->biz_shipment_model->no_role_get("parent_id = '{$shipment['id']}'");
            if(!empty($child_shipments)){
                foreach ($child_shipments as $child_shipment){
                    foreach ($child_shipment as $cskey => $csval){
                        $val_json = json_decode($csval, true);
                        if(is_array($val_json)){
                            continue;
                        }
                        //替换冒号
                        // $child_shipment[$cskey] = str_replace(':', ' ', $csval);
                    }
                    //子shipment填充需要发送的数组
                    $send_shipments[] = $child_shipment;
                }
            }else{
                foreach ($shipment as $key => $val){
                    $val_json = json_decode($val, true);
                    if(is_array($val_json)){
                        continue;
                    }
                    //替换冒号
                    // $shipment[$key] = str_replace(':', ' ', $val);
                }
                //没有子时使用主的
                $send_shipments[] = $shipment;
            }

        }
        $shipment = $shipments[0];
//        $child_shipments = $this->biz_shipment_model->no_role_get("parent_id = '{$shipment['id']}'");
//        if(!empty($child_shipments)){
//            return jsonEcho(array('code' => 1, 'msg' => '暂不支持带子shipment的版本,若需要请联系it'));
//        }

        $time = time();

        $message = array();
        $file_sum = 0;
        $file_content = "";

        //00:IFTCPS:BOOKING:9:756252099:717883154:202301061253'
        $message['00'][] = '00';
        $message['00'][] = 'IFTCPS';//固定
        $message['00'][] = 'BOOKING';//固定
        $message['00'][] = $file_function;//当前文件类型 "2=Add 3=Delete 4=Revise 9=Original
        $SENDER_CODE = $message['00'][] = '756252099';//SENDER CODE
        $RECEIVER_CODE = $message['00'][] = '717883154';//RECEIVER CODE
        $message['00'][] = date('YmdHi', $time);//FILE CREATE TIME
        $file_sum++;
        $file_content .= message_format($message, true) . "\r\n";
        unset($message['00']);

        //2023-03-10 带子shipment的话,从这里开始循环数据的
        foreach ($send_shipments as $shipment){
            if(empty($shipment['cus_no'])){
                return jsonEcho(array('code' => 1, 'msg' => "{$shipment['job_no']} 关单号未填写,请填写后再试"));
            }
//02:KSHAFCL1180:177CKXKXS00523:CY-CY:756252099:717883154::::N:N::::::::it_lin@leagueshipping.com'
            //子shipment的有可能是这里进行循环,先不处理了
            $message['02'][] = '02';
            $message['02'][] = $shipment['job_no'];//JOB NO
            $message['02'][] = $shipment['cus_no'];//MBL 舱单好像都是MBL,这里取shipment的关单号吧
            $message['02'][] = $consol['trans_term'];
            $message['02'][] = $SENDER_CODE;
            $message['02'][] = $RECEIVER_CODE;
            $message['02'][] = '';
            $message['02'][] = '';
            $message['02'][] = '';
            $message['02'][] = 'N';//不知道
            $message['02'][] = 'N';//不知道
            $message['02'][] = '';
            $message['02'][] = '';
            $message['02'][] = '';
            $message['02'][] = '';
            $message['02'][] = '';
            $message['02'][] = '';
            $message['02'][] = '';
            $user_id = get_session('id');
            $message['02'][] = getUserField($user_id, 'email');//email传谁的
            $file_sum++;

            //03:ORI:CNSHA:SHANGHAI,CHINA::3'
            $message['03'][] = '03';
            if($consol['release_type'] == 'OBL') {
                $release_type = 'ORI';
            }else if($consol['release_type'] == 'SWB'){
                $release_type = 'EXP';
            }else if($consol['release_type'] == 'TER'){
                $release_type = 'TER';
            }else{
                return jsonEcho(array('code' => 1, 'msg' => '放单方式'));
            }
            $message['03'][] = $release_type;

            //命名一下港口的变量
            $trans_origin = $consol['trans_origin'];
            $trans_origin_name = $consol['trans_origin_name'];
            $trans_discharge = $consol['trans_discharge'];
            $trans_discharge_name = $consol['trans_discharge_name'];
            $trans_destination = $consol['trans_destination'];
            $trans_destination_name = $consol['trans_destination_name'];

            $message['03'][] = $trans_origin;
            $message['03'][] = $trans_origin_name;
            $message['03'][] = '';
            $message['03'][] = '3';//可能是提单份数,先默认为3 有需要再改
            $file_sum++;

            //11:UN9842102:MSC KANOKO:FY252A:MSC:MSC:MSC::20230107'
            $message['11'][] = '11';
            $dict_imo = Model('bsc_dict_model')->get_one_eq('name',  $consol["vessel"], 'vessel');
            if(empty($dict_imo)){
                return jsonEcho(array('code' => 1, 'msg' => '请配置船名的IMO号码'));
            }
            $message['11'][] = 'UN' . $dict_imo["ext1"];//UN + IMO号码
            $message['11'][] = $consol['vessel'];

            $voyage =  strtr($consol['voyage'], 'V.', '');
            $voyage =  strtr($voyage, 'v.', '');
            $message['11'][] = $voyage;

            $this->db->select('id,client_name');
            $trans_carrier = Model('biz_client_model')->get_where_one("client_code = '{$consol['trans_carrier']}'");

            $message['11'][] = $trans_carrier['client_name'];//承运人简写,直接读取吧
            $message['11'][] = $trans_carrier['client_name'];//承运人简写,直接读取吧
            $message['11'][] = $trans_carrier['client_name'];//承运人简写,直接读取吧
            $message['11'][] = '';
            $message['11'][] = date("Ymd",strtotime($consol['booking_ETD']));//--看起来像订舱船期
            $file_sum++;

            //12:CNSHA:SHANGHAI:CNSHA:SHANGHAI:MVMLE:MALE:::MVMLE:MALE'
            $message['12'][] = '12';
            $message['12'][] = $trans_origin;
            $message['12'][] = $trans_origin_name;
            $message['12'][] = $trans_origin;
            $message['12'][] = $trans_origin_name;
            $message['12'][] = $trans_discharge;
            $message['12'][] = $trans_discharge_name;
            $message['12'][] = '';
            $message['12'][] = '';
            $message['12'][] = $trans_destination;
            $message['12'][] = $trans_destination_name;
            $file_sum++;

            //14:P:FREIGHT PREPAID'
            //转换 付款方式
            $message['14'][] = '14';

            if($consol['payment'] == 'PP'){
                $payment = 'P';
                $INCO_term = "FREIGHT PREPAID";
            }else if($consol['payment'] == 'CC'){
                $payment = 'C';
                $INCO_term = 'FREIGHT COLLECT';
            }else if($consol['payment'] == 'E'){
                $payment = 'E';
                if(empty($consol['payment_third'])){
                    $result['msg'] = lang('payment_third');
                    echo json_encode($result);
                    return false;
                }
                $INCO_term = "FREIGHT PREPAID AT {$consol['payment_third']}" ;
            }else{
                $result['msg'] = lang('payment');
                return jsonEcho(array('code' => 1, 'msg' => lang('payment')));
            }
            $message['14'][] = $payment;
            $message['14'][] = $INCO_term;
            $file_sum++;

            //15:::P'
            $message['15'][] = '15';
            $message['15'][] = '';
            $message['15'][] = '';
            $message['15'][] = $payment;
            $file_sum++;

            //20:9999+9999:CENTER INTERNATIONAL GROUP COMPANY LIMITED:NO.10, YONGCHANG DONGSI ROAD, BDA, BEIJING, CHINA  BEIJING ECONOMIC :::::CN:61525450'
            $message['20'][] = '20';

//            $temp_address = str_replace("\r\n", '\r\n', str_replace("'","\'", $shipment['shipper_address']));
//            $shipper = Model('biz_company_model')->get_where_one("client_code = '{$shipment['client_code2']}' and company_type = 'shipper' and company_name = '" . str_replace("'","\'", $shipment['shipper_company']) . "' and company_address = '" . $temp_address . "'");
//            if(empty($shipper)){
//                $shipper = Model('biz_company_model')->get_where_one("client_code = '{$shipment['client_code2']}' and company_type = 'shipper' and company_name = '" . str_replace("'","\'", $shipment['shipper_company']) . "'");
//            }
//            if(empty($shipper)){
//                return jsonEcho(array('code' => 1, 'msg' => "{$shipment['job_no']} SHIPPER 未找到, 请到shipment里点击发货人公司右边'E'进行维护"));
//            }
//        if($shipper['country'] == ''){
//            return jsonEcho(array('code' => 1, 'msg' => "{$shipment['job_no']} 请维护 SHIPPER 国家"));
//        }
            $message['20'][] = '9999+9999';
            $message['20'][] = $shipment['shipper_company'];
            // $shipment['shipper_address'] = substr($shipment['shipper_address'],0,40);
            $shipper_address = explode("\r\n", str_replace(':', '', $shipment['shipper_address']));

            $shipper_address = cut_array_length($shipper_address, 20, 2);
            for ($i = 0; $i < 5; $i++){
                $this_address = isset($shipper_address[$i]) ? $shipper_address[$i] : '';
                $message['20'][] = $this_address;
            }
            //2023-01-12 高丽提出国家代码取值更改为起运港前两位
            $message['20'][] = substr($trans_origin, 0, 2);//国家代码
            $message['20'][] = '61525450';//固定?
            $file_sum++;

            //21:9999+9999:TO THE ORDER OF BANK OF MALDIVES PLC:TO THE ORDER OF BANK OF MALDIVES PLC:::::MV:61525450:::leagueshipping:61525450'
            $message['21'][] = '21';

//            $temp_address = str_replace("\r\n", '\r\n', str_replace("'","\'", $shipment['consignee_address']));
//            $consignee = Model('biz_company_model')->get_where_one("client_code = '{$shipment['client_code2']}' and company_type = 'consignee' and company_name = '" . str_replace("'","\'", $shipment['consignee_company']) . "' and company_address = '" . $temp_address . "'");
//            if(empty($consignee)){
//                $consignee = Model('biz_company_model')->get_where_one("client_code = '{$shipment['client_code2']}' and company_type = 'consignee' and company_name = '" . str_replace("'","\'", $shipment['consignee_company']) . "'");
//            }
//            if(empty($consignee)){
//                return jsonEcho(array('code' => 1, 'msg' => "{$shipment['job_no']} CONSIGNEE未找到, 请到shipment里点击收货人公司右边'E'进行维护", lastquery()));
//            }
//        if($consignee['country'] == ''){
//            return jsonEcho(array('code' => 1, 'msg' => "{$shipment['job_no']} 请维护 CONSIGNEE 国家"));
//        }
            $message['21'][] = '9999+9999';
            $message['21'][] = $shipment['consignee_company'];
            // $shipment['consignee_address'] = substr($shipment['consignee_address'],0,40);
            $consignee_address = explode("\r\n", str_replace(':', '', $shipment['consignee_address']));
            //原本打算5个位置,但是看了下亿通的都是2个,这里改为2个
            $consignee_address = cut_array_length($consignee_address, 20, 2);
            //固定循环5次
            for ($i = 0; $i < 5; $i++){
                $this_address = isset($consignee_address[$i]) ? $consignee_address[$i] : '';
                $message['21'][] = $this_address;
            }
            //2023-01-12 高丽提出国家代码取值更改为目的港前两位
            $message['21'][] = substr($trans_destination, 0, 2);//这个取值consignee对应的国家
            $message['21'][] = '61525450';//同上
            $message['21'][] = '';
            $message['21'][] = '';
            $message['21'][] = 'leagueshipping';//这里应该固定leagueshipping就行了,要改再说
            $message['21'][] = '61525450';//同上
            $file_sum++;

            //22:9999+9999:SAUDI BINLADIN GROUP COMPANY LIMITED:ADDRESS?:HULHULE 22000, REPUBLIC OF MALDIVES. CONTACT?:MALINDU. RAMA:::::MV:61525450'
            //疑问,SAME AS CONSIGNEE 时,跳过还是把c的信息再来一遍
            if($shipment['notify_company'] !== 'SAME AS CONSIGNEE'){
                $message['22'][] = '22';

//                $temp_address = str_replace("\r\n", '\r\n', str_replace("'","\'", $shipment['notify_address']));
//                $notify = $this->biz_company_model->get_where_one("client_code = '{$shipment['client_code2']}' and company_type = 'notify' and company_name =  '" . str_replace("'","\'", $shipment['notify_company']) . "' and company_address = '" . $temp_address . "'");
//                if(empty($notify)){
//                    $notify = $this->biz_company_model->get_where_one("client_code = '{$shipment['client_code2']}' and company_type = 'notify' and company_name =  '" . str_replace("'","\'", $shipment['notify_company']) . "'");
//                }
//                if(empty($notify)){
//                    return jsonEcho(array('code' => 1, 'msg' => "{$shipment['job_no']} notify未找到, 请到shipment里点击通知人公司右边'E'进行维护"));
//                }
//            if($notify['country'] == ''){
//                return jsonEcho(array('code' => 1, 'msg' => "{$shipment['job_no']} 请维护 notify 国家"));
//            }
                $message['22'][] = '9999+9999';
                $message['22'][] = $shipment['notify_company'];
                // $shipment['notify_address'] = substr($shipment['notify_address'],0,40);
                $notify_address = explode("\r\n", str_replace(':', '', $shipment['notify_address']));
                $notify_address = cut_array_length($notify_address, 20, 2);
                //固定循环5次
                for ($i = 0; $i < 5; $i++){
                    $this_address = isset($notify_address[$i]) ? $notify_address[$i] : '';
                    $message['22'][] = $this_address;
                }
                //2023-01-12 高丽提出国家代码取值更改为目的港前两位
                $message['22'][] = substr($trans_destination, 0, 2);//这个取值consignee对应的国家
                $message['22'][] = '61525450';
            }else{
                //如果是,那么把21复制一遍
                $message['22'] = $message['21'];
                $message['22'][0] = '22';
            }
            $file_sum++;

            //41:1::S:1690:PK:PACKAGES:40787.52:450.72::::::40787.52'
            $message['41'][] = '41';

            $message['41'][] = '1';//货物编号,任意 但是以前的是001
            $message['41'][] = '';
            //转换 货物类型
            if($consol['goods_type'] == 'GC'){
                $goods_type = 'S';
            }else if($consol['goods_type'] == 'RF'){
                $goods_type = 'R';
            }else if($consol['goods_type'] == 'DR'){
                $goods_type = 'D';
            }else{
                return jsonEcho(array('code' => 1, 'msg' => lang('goods_type')));
            }
            $message['41']['CARGO ID'] = $goods_type;////S货物类型 S 普货

            //定义下件毛体等数据
            $good_outers = $consol['good_outers'];
            $good_weight = $consol['good_weight'];
            $good_volume = $consol['good_volume'];
            $good_outers_unit = $consol['good_outers_unit'];
            //查询字典表获取该code
            $iso_code = Model('bsc_dict_model')->get_one('name', $good_outers_unit, 'packing_unit');
            if(!empty($iso_code)){
                $good_outers_unit_CODE = $iso_code['value'];
            }else{
                return jsonEcho(array('code' => 1, 'msg' => lang('good_outers_unit_code')));
            }

            //44:N/M'
            //货物唛头 长度10
            $message['44'] = array(
                array('44', 'N/M'),
            );
            //首先按照当前数组长度切割下 1个35
//        $mark_nums = explode("\r\n", $consol['mark_nums']);
//        $mark_nums = cut_array_length($mark_nums, 35, 99);
//        for ($i = 0; $i < sizeof($mark_nums) ; $i++){
//            //唛头长度是10,如果最后一个数组未超过,这里往里面添加,超过后
//            if(sizeof(end($message['44'])) > 11){
//                $message['44'][] = array('44');
//            }
//            $this_end_key = sizeof($message['44']) - 1;
//            //获取当前最后一个数组,长度是否满足
//            $message['44'][$this_end_key][] = $mark_nums[$i];//N/M 货物唛头
//        }
            $file_sum += sizeof($message['44']);

            //47:ROCK WOOL ^SUPPLY OF ROOFING :MATERIALS FOR EXPANSION OF VELANA :INTERNATIONAL AIRPORT PROJECT  :^(NEW TERMINAL BUILDING) AS PER :INVOICE NO 20220220 DATED '
            $message['47'] = array(
                array('47'),
            );
            $description = explode("\r\n", $consol['description']);
            $description = cut_array_length($description, 32, 3);
            for ($i = 0; $i < sizeof($description) ; $i++){
                //唛头长度是10,如果最后一个数组未超过,这里往里面添加,超过后
                if(sizeof(end($message['47'])) > 6){
                    $message['47'][] = array('47');
                }
                $this_end_key = sizeof($message['47']) - 1;
                //获取当前最后一个数组,长度是否满足
                $message['47'][$this_end_key][] = $description[$i];//N/M 货物唛头
            }
            $file_sum += sizeof($message['47']);
            //如果超出可以多来几行47

            //48:45G1:7:F:::::N::::N'
            // 转换 运输模式
            if($consol['trans_mode'] == 'FCL'){
                $trans_mode = 'F';
            }else if($consol['trans_mode'] == 'LCL'){
                $trans_mode = 'L';
            }else{
                return jsonEcho(array('code' => 1, 'msg' => lang('trans_mode')));
            }

            // 转换 货主箱标记 container_owner
            if($consol['container_owner'] == 'SOC'){
                $container_owner = 'Y';
            }else if($consol['container_owner'] == 'COC'){
                $container_owner = 'N';
            }else{
                return jsonEcho(array('code' => 1, 'msg' => lang('container_owner')));
            }

            $box_info = json_decode($consol['box_info'], true);
            $message['48'] = array();
            foreach ($box_info as $key => $row){
                $message['48'][$key] = array();

                $message['48'][$key][] = '48';
                $size_iso = Model('bsc_dict_model')->get_one('value', $row['size'],'container_size');
                $message['48'][$key][] = $size_iso['name'];//箱型
                $message['48'][$key][] = $row['num'];//箱量
                $message['48'][$key][] = $trans_mode;
                $message['48'][$key][] = '';
                $message['48'][$key][] = '';
                $message['48'][$key][] = '';
                $message['48'][$key][] = '';
                $message['48'][$key][] = $container_owner;//Y=SOC N=COC
                $message['48'][$key][] = '';
                $message['48'][$key][] = '';
                $message['48'][$key][] = '';
                $message['48'][$key][] = $container_owner;//Y=SOC N=COC
            }
            $file_sum += sizeof($message['48']);

            //51:DFSU6511974:45G1:FX23332394:250:6012.630::63.000'
            //这里保险起见,直接shipment取统计吧
            // $containers = Model('biz_container_model')->get("consol_id = '" . $consol['id'] . "'");
            // if(empty($containers)){
            //     return jsonEcho(array('code' => 1, 'msg' => lang('请填写container数据')));
            // }
            $sql = "SELECT `biz_shipment_container`.container_no, biz_shipment_container.packs,biz_shipment_container.weight,biz_shipment_container.volume, `biz_container`.`container_size`, `biz_container`.`seal_no`
            FROM `biz_shipment_container`
            LEFT JOIN `biz_container` ON `biz_container`.`container_no` = `biz_shipment_container`.`container_no` AND `biz_container`.`consol_id` = '{$consol['id']}'
            WHERE `shipment_id` = '{$shipment['id']}'";
            $temp_rs = Model('m_model')->sqlquery($sql);
            $shipment_containers = $temp_rs->result_array();
            $message['51'] = array();
            $good_outers = 0;
            $good_weight = 0;
            $good_volume = 0;
            foreach ($shipment_containers as $shipment_container){
                $this_array = array();

                $this_array[] = '51';

                // $sql = "SELECT IFNULL(SUM(biz_shipment_container.`packs`), 0) AS packs, IFNULL(SUM(biz_shipment_container.`weight`),0) AS weight, IFNULL(SUM(biz_shipment_container.`volume`),0) AS volume FROM biz_shipment, biz_shipment_container WHERE `biz_shipment_container`.`shipment_id` = `biz_shipment`.id AND `biz_shipment`.`consol_id` = " .
                //     $consol['id'] . " AND `biz_shipment_container`.`container_no` = '" . $container["container_no"] . "'";
                // $sum_sp_containers  = $temp_rs->row_array();

                $this_array[] = $shipment_container['container_no'];//箱号
                $container_size_iso = Model('bsc_dict_model')->get_one('value', $shipment_container['container_size'],'container_size');
                $this_array[] = $container_size_iso['name'];//45G1 箱型

                $this_array[] = $shipment_container['seal_no'];// 箱封号
                $good_outers += $this_array[] = $shipment_container['packs'];//14  箱数量或者数量
                $good_weight += $this_array[] = $shipment_container['weight'];//13517.000 净重
                $this_array[] = '';
                $good_volume += $this_array[] = $shipment_container['volume'];//volume
                $message['51'][] = $this_array;
            }
            $file_sum += sizeof($message['51']);
            //多个循环

            $message['41'][] = $good_outers;//件数
            $message['41'][] = $good_outers_unit_CODE;//件数单位代码
            $message['41'][] = $good_outers_unit;//件数单位
            $message['41'][] = $good_weight;//重量
            $message['41'][] = $good_volume;//体积
            $message['41'][] = '';
            $message['41'][] = '';
            $message['41'][] = '';
            $message['41'][] = '';
            $message['41'][] = '';
            $message['41'][] = $good_weight;//重量
            $file_sum++;

            $file_content .= message_format($message, true) . "\r\n";
            $message = array();
        }


        //99:25'
        $message['99'] = array();
        $message['99'][] = '99';
        $message['99'][] = ++$file_sum;//16 记录文件总数
        $file_content .= message_format($message, true);
        unset($message['99']);

        //接下来将报文进行转化
        //保存文件txt
        $root_path = dirname(BASEPATH);
        $path = date('Ymd');
        //ESL_EDI/日期
        $new_dir = $root_path . '/upload/message/yitong_EDI/IFTCPS/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $file_name = time() . $consol_id . '.txt';
        $file_path = $new_dir . $file_name;
        if(preg_match('/[\x{4e00}-\x{9fa5}]+/u', $file_content, $matches1) === 1){
            return jsonEcho(array('code' => 1, 'msg' => lang('存在中文或中文标点符号,生成失败: <br/>' . join(' ', $matches1))));
        }
        file_put_contents($file_path, $file_content);

        $message_data = array(
            'file_path' => '/' . $path . '/' . $file_name,
            'type' => 'IFTCPS',
            'receiver' => 'yitong',
        );
        update_message($consol_id, $message_data, 'IFTCPS');
        return jsonEcho(array('code' => 0, 'msg' => lang('文件生成成功')));
    }
}