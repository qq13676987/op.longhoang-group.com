<?php
class bsc_template extends Common_Controller
{
    private $data_table = array(
        'consol',
        'shipment',
    );
    public $field_edit = array();
    //1、数据库字段名 2、页面显示名称 3、宽度， 4、排序  5、 拓展属性
    public $field_all = array(
        array('id', 'id', '80', '1', 'sortable="true"'),
        array('upload_id', 'upload_id', '60', '2', ''),
        array('file_template', 'file_template', '550', '2', 'data-options="formatter:file_template_for"'),
        array('file_name', 'file_name', '190', '3', ''),
        array('data_table', 'data_table', '100', '4', ''),
        array('parameter', 'parameter', '100', '4', ''),
        array('groups', 'groups', '300', '4', "data-options=\"formatter:function(value, row, index){
            if(value.length > 30){
                return value.substring(0, 30) + '...';
            }else{
                return value;
            }
        }\""),
        array('users', 'users', '300', '4', "data-options=\"formatter:function(value, row, index){
            if(value.length > 30){
                return value.substring(0, 30) + '...';
            }else{
                return value;
            }
        }\""),
        array('create_time', 'create_time', '150', '7', 'sortable="true"'),
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('bsc_template_model');
        $this->model = $this->bsc_template_model;

        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "create_by" || $v == "create_time" || $v == "update_by" || $v == "update_time") continue;
            array_push($this->field_edit, $v);
        }
        //根据数据库来进行分组
    }

    public function index()
    {
        $data = array();

        // column defination
        $this->load->model('sys_config_model');
        $rs = $this->sys_config_model->get_one('bsc_template_table');
        if (!empty($rs['config_name'])) {
            $data["f"] = json_decode($rs['config_text']);
        } else {
            $data["f"] = $this->field_all;
        }

        $this->load->view('head');
        $this->load->view('bsc/template/index_view', $data);
    }

    public function get_data(){
        $result = array();
        $page = isset($_POST['page']) ? intval($_POST['page']) : 1;
        $rows = isset($_POST['rows']) ? intval($_POST['rows']) : 30;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "order";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------where -------------------------------
        $where = array();
        $f1 = isset($_REQUEST['f1']) ? ($_REQUEST['f1']) : '';
        $s1 = isset($_REQUEST['s1']) ? ($_REQUEST['s1']) : '';
        $v1 = isset($_REQUEST['v1']) ? ($_REQUEST['v1']) : '';
        $f2 = isset($_REQUEST['f2']) ? ($_REQUEST['f2']) : '';
        $s2 = isset($_REQUEST['s2']) ? ($_REQUEST['s2']) : '';
        $v2 = isset($_REQUEST['v2']) ? ($_REQUEST['v2']) : '';
        $f3 = isset($_REQUEST['f3']) ? ($_REQUEST['f3']) : '';
        $s3 = isset($_REQUEST['s3']) ? ($_REQUEST['s3']) : '';
        $v3 = isset($_REQUEST['v3']) ? ($_REQUEST['v3']) : '';

        if($v1!="") $where[] = $s1 != 'like' ? "$f1 $s1 '$v1'" : "$f1 $s1 '%$v1%'";
        if($v2!="") $where[] = $s2 != 'like' ? "$f2 $s2 '$v2'" : "$f2 $s2 '%$v2%'";
        if($v3!="") $where[] = $s3 != 'like' ? "$f3 $s3 '$v3'" : "$f3 $s3 '%$v3%'";
        $where = join(' and ', $where);
        //-----------------------------------------------------------------
        $offset = ($page-1)*$rows;
        $result["total"] = $this->model->total($where);
        $this->db->limit($rows, $offset);
        $rs = $this->model->get($where,$sort,$order);

        $rows = array();
        foreach ($rs as $row){
            $row['file_template_download'] = "/" . get_system_type() . $row['file_template'];
            array_push($rows, $row);
        }
        $result["rows"] = $rows;
        echo json_encode($result);
    }

    public function add_data(){
        $field = $this->field_edit;
        $data= array();
        foreach ($field as $item){
            $temp = isset($_POST[$item]) ? $_POST[$item] : '';
            $data[$item] = $temp;
        }
        $id = $this->model->save($data);
        $data['id']= $id;

        $this->load->model('bsc_upload_model');
        $upload_row = $this->bsc_upload_model->get_one('id', $data['upload_id']);
        //查询该文件是否绑定id_no,未绑定绑定当前
        if(!empty($upload_row) && $upload_row['id_no'] == ''){
            $this->bsc_upload_model->update($data['upload_id'], array('id_no' => $id));
        }

        echo json_encode( $data );

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_template";
        $log_data["key"] = $id;
        $log_data["action"] = "insert";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function update_data(){
        $id = $_REQUEST["id"];
        $old_row = $this->model->get_one('id', $id);

        $field = $this->field_edit;

        $data= array();
        foreach ($field as $item){
            $temp = isset($_POST[$item]) ? $_POST[$item] : null;
            if($temp !== null){
                if($old_row[$item]!=$temp)
                    $data[$item] = is_array($temp) ? $temp : trim($temp);
            }
        }
        $id = $this->model->update($id,$data);

        $data['id']= $id;
        echo json_encode( $data );

        // record the log
        $log_data = array();
        $log_data["table_name"] = "biz_template";
        $log_data["key"] = $id;
        $log_data["action"] = "update";
        $log_data["value"] = json_encode($data);
        log_rcd($log_data);
    }

    public function delete_data(){

        $id = intval($_REQUEST['id']);
        $old_row = $this->model->get_one('id', $id);
        $this->model->mdelete($id);
        echo json_encode(array('success'=>true));

        //save the operation log
        $log_data = array();
        $log_data["table_name"] = "biz_template";
        $log_data["key"] = $id;
        $log_data["action"] = "delete";
        $log_data["value"] = json_encode($old_row);
        log_rcd($log_data);
    }

    public function get_template(){
        $rs = $this->model->get();
        $data_table = $this->data_table;

        $data = array();
        foreach ($data_table as $dkey => $dval){
            $temp = array();
            foreach($rs as $row){
                if($row['data_table'] == $dval){
                    array_push($temp,array("id" => $row['data_table'] . '.' .$row["id"], "text" => $row["file_name"]));
                }
            }
            array_push($data,array("id" => $dval, "text" => $dval, "children" => $temp));
        }

        echo json_encode($data);
    }

    public function download(){
        $data = array();
        $data['data_table'] = isset($_GET['data_table']) ? $_GET['data_table'] : $this->data_table[0];
        $data['id'] = $_GET['id'];

        //这里如果有晚截标记，但是没输入晚截费，则不让下载。
        $m_model = Model('m_model');

        $data['bill_type'] = '';
        if($data["data_table"]=="shipment"){
            $this->load->model("biz_shipment_model");
            $shipment = $this->biz_shipment_model->get_by_id($data['id']);
            $data["bill_type"] = $shipment["hbl_type"];
        }
        $userId = $this->session->userdata('id');
        $this->load->model('bsc_user_model');
        $userBsc = $this->bsc_user_model->get_by_id($userId);

        $this->load->model('bsc_user_role_model');

        $userRole = $this->bsc_user_role_model->get_user_role(array('template_text'));
        $userBsc = array_merge($userBsc, $userRole);

        $limit = '';
        $where = array();
        $temp_ids = array(0);
        $userBsc_template = explode(",", (isset($userBsc['template_text']) ? $userBsc['template_text'] : '') );
        foreach ($userBsc_template as $ukey => $uval){
            //判断当前data_table是否存在，存在
            if($uval == $data['data_table']){
                $limit = 'all';
                break;
            }else if( strpos($uval, $data['data_table'] . '.') !== false  ){
                $arr_temp =  explode(".", $uval);
                $temp_id = $arr_temp[1];
                $temp_ids[] = $temp_id;
            }
        }
        //2022-04-19 这里改为了只显示没设置类型的模板, 目前只有个B/L 提单不会显示
        //2022-04-21 HOST 聚翰 和 精心的全开放普通版
        $where[] = "(bsc_template.type = '' or id in (37,22))";
        $where[] = "is_delete = 0";
        $where[] = "data_table = '" . $data['data_table'] . "'";
        if(!is_admin()){
            if($limit != 'all'){
                $where[] = 'id in (' . join(',', $temp_ids) . ')';
            }
            $where[] = "(groups = '' or groups like '{$userBsc['group']},%' or groups like '%,{$userBsc['group']}' or groups like '%,{$userBsc['group']},%' or groups = '{$userBsc['group']}' or users like '{$userBsc['id']},%' or users like '%,{$userBsc['id']}' or users like '%,{$userBsc['id']},%' or users = '{$userBsc['id']}')";
        }

        $where = join(' and ', $where);
        $rs = $this->model->get($where, 'order', 'asc');

        $new_rs = array();

        foreach($rs as $r){
            $sql = "select * from bsc_template_match where template_id = ".$r['id'];
            // echo $sql;
            $match = $m_model->query_one($sql);
            if(sizeof($match)==0) {
                $new_rs[] = $r;
                continue;
            }

            $where2 = array();
            if(empty($match["sql"])){
                $where2=$match['id_field']."='".$match['id_value']."'";
            }else{
                $where2=$match['sql'];
            }

            $sql = "select * from {$match['id_type']} where id={$data['id']} and $where2";
            $check_row = $m_model->query_one($sql);
            if(!empty($check_row)) $new_rs[]=$r;
        }


        $data['row'] = $new_rs;
        $this->load->view('head');
        $this->load->view('bsc/template/list',$data);
    }

    public function set_parameter(){
        $url = $_GET['url'];
//        $template_id = getValue('template_id');
//        $file_name = getValue('file_name');
        $data_table = $_GET['data_table'];
        $parameter = $_GET['parameter'];
//        $pdf = isset($_GET['pdf']) ? $_GET['pdf'] : '';
        $id = $_GET['id'];
        //$url .= '?file_name=' . $file_name . '&data_table=' . $data_table . '&id=' . $id . '&pdf=' . $pdf;
        //这里改为直接带全部get参数
        $url .= '?' . http_build_query($_GET);
        if($parameter == ''){
            echo "<script>";
            echo "location.href='$url';";
            echo "</script>";
            return;
        }
        $parameter = explode(',' , $parameter);
        if(in_array('bill_client', $parameter)){
            $bill_client = array();
            $sql = "select client_code, (select company_name_en from biz_client where client_code = biz_bill.client_code) as company_name from biz_bill where id_type = '$data_table' and id_no = '$id' GROUP BY client_code";
            $query = $this->db->query($sql);
            $bill_client = $query->result_array();
            $data['bill_client'] = $bill_client;
        }
        //获取银行配置信息
        if(in_array('client_account', $parameter)){
            $client_account = array();
            if($data_table == 'shipment'){
                $this->load->model('bsc_overseas_account_model');
                $client_account = $this->bsc_overseas_account_model->get("LENGTH(title_cn) <= 12");
            }else if($data_table == 'consol'){

            }
            $data['client_account'] = $client_account;
        }
        //获取DN 的币种
        if(in_array('dn_currency', $parameter)){
            $dn_currency = array();
            $dn_currency = Model('bsc_dict_model')->get_option("currency");
            //2023-03-27 如果有私有库,那么用私有的
            $system_type = get_system_type();
            $this->db->select('value');
            $dn_currency2 = Model('bsc_dict_model')->get_option("currency_{$system_type}");
            if(!empty($dn_currency2)) $dn_currency = $dn_currency2;
            if($data_table == 'shipment'){
                // $dn_currency = array(
                //     array('value' => 'USD'),    
                //     array('value' => 'EUR'),    
                // );
            }else if($data_table == 'consol'){

            }
            $data['dn_currency'] = $dn_currency;
        }
        //获取银行配置信息
        if(in_array('dn_account', $parameter)){
            $client_account = array();
            if($data_table == 'shipment'){
            }else if($data_table == 'consol'){

            }
            $data['client_account'] = $client_account;
        }

        //获取当前账单的所有发票号
        if(in_array('bill_invoice_no', $parameter)){
            $m_model = Model('m_model');
            $invoice_sql = "select invoice_id from biz_bill where id_type = '$data_table' and id_no = $id";
            $query = $m_model->sqlquery($invoice_sql);
            $invoice_ids = $query->result_array();

            $sql = "select id,invoice_no from biz_bill_invoice where id in ('" . join('\',\'', array_column($invoice_ids, 'invoice_id')) . "')";

            $query = $m_model->sqlquery($sql);
            $invoice_nos = $query->result_array();
            //  var_dump($sql);
            $data['bill_invoice_no'] = $invoice_nos;
        }
        if(in_array('truck_one', $parameter)){
            //选择根据哪个truck进行导出
            if($data_table == 'shipment'){
                $where = "shipment_id = '$id'";
            }else if($data_table == 'consol'){
                $where = "id = 0";
            }
            $biz_shipment_truck_model = Model('biz_shipment_truck_model');
            $this->db->select('id,truck_company');
            $trucks = $biz_shipment_truck_model->get($where);
            $data['truck_one'] = $trucks;
        }
        if(in_array('is_confirm_date', $parameter)){
            //选择根据哪个确认日期导出
            if($data_table == 'shipment'){
                $where = "shipment_id = '$id'";
            }else if($data_table == 'consol'){
                $where = "id = 0";
            }
            $sql = "select confirm_date from biz_bill where id_type = '$data_table' and id_no = $id and confirm_date != '0000-00-00' GROUP by confirm_date";
            $m_model = Model('m_model');
            $query = $m_model->sqlquery($sql);
            $confirm_dates = $query->result_array();
            $data['confirm_dates'] = $confirm_dates;
        }
        //提单份数
        if(in_array('B_L_number', $parameter)){
            $sql = "select name,value from bsc_dict where catalog = 'B_L_number' order by orderby";
            $m_model = Model('m_model');
            $query = $m_model->sqlquery($sql);
            $B_L_number = $query->result_array();
            $data['B_L_number'] = $B_L_number;
        }
        //DN发票号
        if(in_array('dn_invoice_no', $parameter)){
            $m_model = Model('m_model');
            $invoice_sql = "select invoice_id from biz_bill where id_type = '$data_table' and id_no = $id";
            $invoice_ids = $m_model->query_array($invoice_sql);

            $sql = "select id,invoice_no from biz_bill_invoice where id in ('" . join('\',\'', array_column($invoice_ids, 'invoice_id')) . "')";
            $query = $m_model->sqlquery($sql);
            $data['dn_invoice_no'] = $query->result_array();
            // array_shift(array('invoice_no' => '', 'id' => 0), $data['dn_invoice_no']);
        }
        //提箱号
        if(in_array('gs_soc_tixianghao', $parameter)){
            $gs_soc_tixianghao_sql = "select gs_soc_tixianghao from biz_consol where id = {$id}";
            $row = Model('m_model')->query_one($gs_soc_tixianghao_sql);
            $data['gs_soc_tixianghao'] = filter_unique_array(explode(',', $row['gs_soc_tixianghao']));
        }
        //免税率
        if(in_array('hold_without_tax', $parameter)){
            $data['hold_without_tax'] = array(
                array('value' => '3%', 'name' => lang("3%")),
                array('value' => '1%', 'name' => lang("1%")),
                array('value' => '0%', 'name' => lang("0%")),
            );
        }
        //当前转换的币种
        if(in_array('this_local_currency', $parameter)){
            $data['this_local_currency'] = Model('bsc_dict_model')->get_option("currency");
        }
        //当前转换的币种
        if(in_array('convert_currency', $parameter)){
            $data['convert_currency'] = Model('bsc_dict_model')->get_option("currency");
        }
        //DN发票号
        if(in_array('third_party_payment', $parameter)){
            $m_model = Model('m_model');
            $sql = "select bsc_dict.name,bsc_dict.value,biz_client.company_name_en from bsc_dict LEFT JOIN biz_client on biz_client.client_code = bsc_dict.value where catalog = 'branch_office' and url = ''";
            $temp_rows = $m_model->query_array($sql);

            $data['third_party_payment'] = $temp_rows;
            // array_shift(array('invoice_no' => '', 'id' => 0), $data['dn_invoice_no']);
        }
        $data['url'] = $url;
        $data['parameter'] = $parameter;
        $this->load->view('bsc/template/set_parameter', $data);
    }
}
