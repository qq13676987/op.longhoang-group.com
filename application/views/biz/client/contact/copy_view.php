<title>复制联系人</title>
<body style="padding: 10px 20px;">
<div style="width: 100%">
    <form id="myform" name="myform" method="post">
        <input type="hidden" name="id" id="id" value="<?=isset($_GET['id'])?(int)$_GET['id']:'0';?>">
        <table>
            <tr style="height: 30px;">
                <td style="width: 60px;">角色：</td>
                <td>
                    <select id="role" name="role" class="easyui-combobox" style="width: 280px; height: 25px;"
                            data-options="
                                    required:true,
                                    multiple:false,
                                    editable:false,
    								valueField:'value',
    								textField:'value',
    								url:'/bsc_dict/get_option/role',
    								value:'',
								">
                    </select>
                </td>
            </tr>
            <tr style="height: 30px;">
                <td>姓名：</td>
                <td><input class="easyui-textbox" type="text" required="required" name="name" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td>手机号：</td>
                <td><input class="easyui-textbox" type="text" required="required" name="telephone" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td>座机号：</td>
                <td><input class="easyui-textbox" type="text" name="telephone2" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td>邮箱：</td>
                <td><input class="easyui-textbox" type="text" required="required" name="email" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td>QQ：</td>
                <td><input class="easyui-textbox" type="text" name="live_chat" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td>职位：</td>
                <td>
                    <input class="easyui-combobox" type="text" name="position" value="" style="width: 280px; height: 25px;"
                           data-options="
                           valueField: 'value',
                           textField: 'name',
                           url:'/bsc_dict/get_option/contact_position',"
                    />
                </td>
            </tr>
            <tr style="height: 30px;">
                <td>前台权限：</td>
                <td>
                    <input class="easyui-combobox" type="text" name="menu" value="" style="width: 280px; height: 25px;"
                           data-options="
                           valueField: 'value',
                           textField: 'value',
                           url:'/biz_client_contact/get_menu',"/>
            </tr>
            <tr style="height: 30px;">
                <td>前台密码：</td>
                <td><input class="easyui-textbox" type="text" name="password" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td>航线：</td>
                <td>
                    <input class="easyui-combobox" type="text" name="sailing" value="" style="width: 280px; height: 25px;"
                           data-options="
                           valueField: 'value',
                           textField: 'name',
                           url:'/bsc_dict/get_option/export_sailing',"
                    />
            </tr>
            <tr style="height: 30px;">
                <td>备注：</td>
                <td><input class="easyui-textbox" type="text" name="remark" value="" style="width: 280px; height: 25px;"/></td>
            </tr>
            <tr style="height: 30px;">
                <td></td>
                <td align="center">
                    <input type="hidden" name="client_code" value="">
                    <input type="hidden" name="is_submit" value="1">
                    <a href="javascript:void(0);" id="save_data" class="easyui-linkbutton" style="width: 280px;height:30px;margin-top: 10px;">保存联系人</a>
                </td>
            </tr>
        </table>
    </form>
</div>
<link type="text/css" href="/inc/css/wangeditor/style.css" rel="stylesheet">
<script type="text/javascript" src="/inc/js/wangeditor/index.js" ></script>
<script type="text/javascript" src="/inc/js/wangeditor/dist/index.js" ></script>
<script>
    $(function () {
        //关闭当前弹窗
        <?php if (empty($client) || empty($data)):?>
        parent.$.messager.alert('系统消息', '客户不存在或编辑对象不存在！');
        setTimeout(function () {
            parent.$('#_window').window('close');
        }, 3000);
        <?php else: ?>

        $('#myform').form('load', <?=json_encode($data);?>);

        //异步写入数据
        $('#save_data').click(function () {
            var field = {};
            $.each($('#myform').serializeArray(), function (index, item) {
                field[item.name] = item.value;
            });

            $.ajax({
                url: '/biz_client_contact/add/?client_code=<?=$client['client_code'];?>',
                type: 'POST',
                data: field,
                dataType: 'json',
                success: function (data) {
                    if (data.code == 1){
                        //刷新父页面的列表控件
                        parent.$('#tt').edatagrid('reload');

                        $.messager.show({
                            title:'系统消息',
                            msg:'保存成功。',
                            timeout:2000,
                            showType:null,
                            style:{
                                right:'',
                                bottom:''
                            }
                        });

                        setTimeout(function () {
                            //关闭当前弹窗
                            parent.$('#_window').window('close');
                        }, 2000);

                    }else{
                        $.messager.alert("系统消息", '保存失败：'+data.msg, 'error');
                    }
                },
                error: function (xhr, status, error) {
                    $.messager.alert("提示", "操作失败"); //xhr.responseText
                }
            });
        });
        <?php endif; ?>
    })
</script>
</body>
</html>