<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Tag-it! Example</title>


    <!-- 这几个CSS文件只是为了让这个示例页面看起来更漂亮。 你可以忽略它们   -->
    <!--    <link href="/inc/jquery_tag/css/master.css" rel="stylesheet" type="text/css">-->
    <!--    <link href="/inc/jquery_tag/css/subpage.css" rel="stylesheet" type="text/css">-->
    <!--    <link href="/inc/jquery_tag/css/examples.css" rel="stylesheet" type="text/css">-->


    <!--*标签-它的基础CSS (jquery.tagit.css)。 -->
    <!--*任何主题的CSS(无论是一个jQuery UI主题，如“flick”，或一个捆绑了标签-它，如在这个例子中的tagit.ui-zendesk.css)-->
    <!-- base CSS和tag . UI -zendesk. CSS主题的范围是Tag-it小部件，所以它们不应该影响你网站的其他任何东西，不像jQuery UI主题。-->
    <link href="/inc/js/jquery_tag/css/jquery.tagit.css" rel="stylesheet" type="text/css">
    <link href="/inc/js/jquery_tag/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">

    <!-- jQuery和jQuery UI是必需的依赖项 -->
    <!-- 虽然我们在这里使用的是jQuery 1.4，但它也用最新的版本进行了测试(截至撰写本文的时候是1.8.3) -->
    <script src="/inc/js/jquery_tag/js/jquery.min.js"></script>
    <script src="/inc/js/jquery_tag/js/jquery-ui.min.js"></script>

    <!-- The real deal -->
    <script src="/inc/js/jquery_tag/js/tag-it.js" type="text/javascript" charset="utf-8"></script>

    <script>
        $(function () {
            var sampleTags = [];
            //获取job_label的值
            $.ajax({
                type: 'GET',
                url: '/bsc_user/job_label_get',
                dataType: 'json',
                success: function (res) {
                    sampleTags = res;
                    $('#singleFieldTags2').tagit({
                        availableTags: sampleTags
                    });
                }
            });


        });
    </script>

</head>
<style>
    ul.tagit {
        height: 338px;
        width: 500px;
        border: 1px solid #95b8e7;
        border-radius: 5px;
    }

    ul.tagit:hover {
        outline: #95b8e7 solid 1px;
    }

    body {
        margin: 0px;
    }

    button {
        border-radius: 5px;
        background-color: #e0ecff;
        color: #515a6e;
        border: 1px solid #95b8e7;
    }


</style>
<body>
<!--<form>-->
<!--    <ul id="myTags">-->
<!--        <li>1</li>-->
<!--    </ul>-->
<!--    <input type="submit" value="Submit">-->
<!--</form>-->
<div style="display:flex;height:74px;width:100%">
    <div style="height:100%;line-height: 74px;width:135px">
        <label style="color:#2d8cf0;font-weight: bold; vertical-align: middle;height:30px;font-size: 20px">职业标签</label>
    </div>
    <input name="tags" id="singleFieldTags2" value="<?=$job_label?>">
    <div style="height:100%;padding-left: 20px;line-height: 74px"> 
        <?php
        if($is_button == 1)
        echo '<button style="height:30px;" onclick="save()">保存设置</button>';
        ?>
        
    </div>
</div>

</body>
</html>
<script>
    //保存标签
    function save(){
      var job_label=$('#singleFieldTags2').val();
        $.ajax({
            type: 'POST',
            url: '/bsc_user/post_job_label',
            data: {
                user_id:<?= $id;?>,
                job_label:job_label,
            },
            dataType: 'json',
            success: function (res) {
               alert(res.msg);
            },
            error: function () {
                alert('error');
            }
        });
    }

</script>

