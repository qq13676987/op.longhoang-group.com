    <script type="text/javascript">
        function doSearch(){
            $('#tt').datagrid('load',{
                container_no: $('#container_no').val()
            });
        }
        
        function container_info_excel() {
            var json = {};
            var cx_data = $('#cx').serializeArray();
            var is_export = true;
            var end_each = false;
            json['shipment_id'] = '<?= $shipment_id;?>';
            json['consol_id'] = '<?= $consol_id?>';
            json['container_no'] = $('#container_no').val();
            
            // GET方式示例，其它方式修改相关参数即可
            var form = $('<form></form>').attr('action','/excel/export_excel?file=container_info').attr('method','POST');
    
            $.each(json, function (index, item) {
                if(typeof item === 'string'){
                    form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                }else{
                    $.each(item, function(index2, item2){
                        form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                    })
                }
            });
    
            form.appendTo('body').submit().remove();
        }
        
        //上传文件操作
        function UploadFile(file_ctrlname, guid_ctrlname, div_files) {
            var value = $("#" + file_ctrlname).filebox('getValue');
            var files = $("#" + file_ctrlname).next().find('input[type=file]')[0].files;
    
            var guid = $("#" + guid_ctrlname).val();
            if (value && files[0]) {
                //构建一个FormData存储复杂对象
                var formData = new FormData();
                formData.append('file', files[0]);
    
                $.ajax({
                    url: '/excel/upload_excel/container_info', //单文件上传
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function (json) {
                        //转义JSON为对象
                        var data;
                        try{
                            data = $.parseJSON(json);
                        }catch(e){
                            
                        }
                        if(data == undefined){
                            $.messager.alert("info", '发生错误');
                        }else{
                            $.messager.alert("info", data.msg);
                            $.each(data.result, function(index, item){
                                item.isNewRecord = true;
                                $('#tt').datagrid('appendRow',item);
                                var data = $('#tt').datagrid('getData');
                                var this_index =  data.rows.length - 1;
                                $('#tt').datagrid('beginEdit', this_index);
                                $('#tt').datagrid('endEdit', this_index);
                            });
                        }
                    },
                    error: function (xhr, status, error) {
                        $.messager.alert("提示", "操作失败"); //xhr.responseText
                    }
                });
            }
        }
        
        function import_excel(){
            $('#import').window('open');
        }
        
        $.fn.edatagrid.defaults.onError = function(index, row){
            $.messager.alert('error', row.msg);
        };
        var is_define = false;
        $(function(){
            $('#tt').edatagrid({
                url: '/biz_containershipment/get_data/<?php echo $shipment_id;?>/<?= $consol_id;?>',
                saveUrl: '/biz_containershipment/add_data/<?php echo $shipment_id;?>/<?= $consol_id;?>' ,
                updateUrl: '/biz_containershipment/update_data/' ,
                destroyUrl: '/biz_containershipment/delete_data',
                onBeforeSave:function (index) {
                    var data = $('#tt').datagrid('getData');
                    var ed = $('#tt').datagrid('getEditor', {index:index,field:'container_no'});
                    var container_no = $(ed.target).combobox('getValue').toUpperCase();
                    var id = data.rows[index].id;
                    if(ed == null)return false;
                    $(ed.target).combobox('setValue',container_no);
                    var result = docheck(container_no);
                    var repeat = true;
                    $.each(data.rows, function (index, item) {
                        if(id != item.id && item.container_no == container_no && !item.hasOwnProperty('isNewRecord')){
                            repeat = false;
                        }
                    });
                    if(!repeat){
                        $.messager.alert('Tips', '<?= lang('container_no');?>不能重复');
                        return false;
                    }else{
                        if(!result && !is_define){
                            $.messager.confirm('Tips', '箱号校验出错, 是否确认提交？,', function(r){
                                if (r){
                                    // 退出操作;
                                    is_define = true;
                                    $('#tt').edatagrid('saveRow');
                                }
                            });
                            return false;
                        }
                        is_define = false;
                        return true;
                        // $('#tt').edatagrid('saveRow');
                    }
                }
            });
			$('#tt').datagrid({
				width:'auto',
				height: $(window).height(),
				<?php if($lock_lv > 0){ ?>
                onDblClickCell:function(index,field) {
                },
                <?php } ?>
            }); 
			
			$(window).resize(function () {
				$('#tt').datagrid('resize');
			});
			
			//添加对话框，上传控件初始化
            $('#file_upload').filebox({
                prompt:'Choose another file...',
                buttonText: 'choose file',  //按钮文本
                buttonAlign: 'right',   //按钮对齐
                //multiple: true,       //是否多文件方式
                //accept:'jpg|jpeg|gif|pdf|doc|docx|xlsx|xls|zip|rar'
                accept: ".xlsx,.xls", //指定文件类型
            });
    
            $('#file_save').click(function () {
                UploadFile("file_upload", "AttachGUID", "div_files");//上传处理
            });
        });
            
    </script>
    <script language="javascript">
        //去除字符串的空格
        function gf_trim(as_string)
        {
            while(as_string.length > 0 && as_string.indexOf(" ")==0) as_string = as_string.substr(1);
            while(as_string.length > 0 && as_string.lastIndexOf(" ")==(as_string.length-1)) as_string = as_string.substr(0,as_string.length-1);
            return as_string;
        }
        //集装箱箱号验证
        //功能：验证集装箱箱号：
        //参数：
        //   as_cntrno 是否符合国际标准，
        //返回值：True 符合国际标准或强行通过(特殊箱号)
        //举例：gf_chkcntrno( 'TEXU2982987', 0 )
        function chkcntrno(as_cntrno,ai_choice)
        {
            var fi_ki;
            var fi_numsum;
            var fi_nummod;
            var fai_num = new Array(11);
            var fb_errcntrno=false;

            if (as_cntrno==null) return true; //null不进行验证
            if (gf_trim(as_cntrno)=="") return true; //空不进行验证
            as_cntrno = gf_trim(as_cntrno);

            if (as_cntrno.length == 11)   //国际标准为11位，最后一位是校验位，若不是11位肯定不是标准箱
            { for(fi_ki=1;fi_ki<=11;fi_ki++)
                fai_num[fi_ki] = 0;
                for(fi_ki=1;fi_ki<=4;fi_ki++)     //根据国际标准验证法则处理箱号前面的4个英文字母
                {
                    fch_char=as_cntrno.charAt(fi_ki-1).toUpperCase();
                    switch(true)
                    { case (fch_char=="A"):{fai_num[fi_ki] = 10;break;}
                        case (fch_char>="V" && fch_char<="Z"):{fai_num[fi_ki] = fch_char.charCodeAt() - 52;break;}
                        case (fch_char>="L" && fch_char<="U"):{fai_num[fi_ki] = fch_char.charCodeAt() - 53;break;}
                        default:{fai_num[fi_ki] = fch_char.charCodeAt() - 54;break;}
                    }
                }
                for(fi_ki=5;fi_ki<=11;fi_ki++)
                {  fch_char=as_cntrno.charAt(fi_ki-1);
                    fai_num[fi_ki] = parseInt(fch_char); //ctype((mid(as_cntrno, fi_ki, 1)), integer)
                }
                fi_numsum = 0

                for(fi_ki=1;fi_ki<=10;fi_ki++)
                {
                    fi_sqr = 1;
                    for(i=1;i<fi_ki;i++){fi_sqr *=2;}
                    fi_numsum += fai_num[fi_ki] * fi_sqr;
                }

                if (as_cntrno.substr(0,4) == "HLCU") fi_numsum = fi_numsum - 2; //hapaq lloyd的柜号与国际标准相差2
                fi_nummod = fi_numsum % 11;
                if (fi_nummod == 10) fi_nummod = 0;
                if (fi_nummod == fai_num[11]) fb_errcntrno = true;
                return fb_errcntrno;
            }else{
                return fb_errcntrno;
            }
        }

        function docheck(text){
            text = text.replace(/[ ]/g,"");
            text = text.replace(/[\r\n]/g,"");
            return chkcntrno(text ,0);
        }
    </script>
<table id="tt" style="width:1100px;height:450px"
            rownumbers="false" pagination="true"  idField="id"
            pagesize="50" toolbar="#tb"  singleSelect="true" nowrap="false">
        <thead>
            <tr>
                <th field="id" width="60" sortable="true">ID</th>       
				<th field="container_no" width="250" align="center" editor="{
                            type:'combobox',
                            options:{
                                required:true,
                                valueField:'container_no',
                                textField:'container_no',
                                url:'/biz_container/get_option/<?php echo $consol_id;?>',
                                onSelect: function(res){
                                    var ed = $('#tt').datagrid('getEditor', {index:0,field:'seal_no'});
                                    $(ed.target).textbox('setValue', res.seal_no);
                                    var ed = $('#tt').datagrid('getEditor', {index:0,field:'container_size'});
                                    $(ed.target).combobox('setValue', res.container_size);
                                }
                            }
                }">container_no</th>
                <th field="seal_no" width="111" align="center"  editor="{type:'textbox',options:{required:true}}" >seal_no</th>
                <th field="container_size" width="111" align="left" editor="{
                            type:'combobox',
                            options:{
                                required:true,
                                valueField:'value',
								textField:'value',
								url:'/bsc_dict/get_option/container_size',
                            }
                }">container_size</th>
                <th field="container_tare" width="111" align="center"  editor="{
                    type:'numberbox',
                    options:{ 
                        min:0,
                        precision:2
                    }
                }">container_tare</th>  
				<th field="en_description" width="200" editor="{type:'textbox'}" >en_description</th>
				<th field="vgm" width="111" editor="{type:'textbox'}" >vgm</th>
                <th field="packs" width="111" editor="{type:'textbox',options:{required:true}}" >packs</th>
				<th field="weight" width="111" editor="{type:'textbox',options:{required:true}}" >weight</th>
				<th field="volume" width="111" editor="{type:'textbox',options:{required:true}}" >volume</th>
            </tr>
        </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <?php if($lock_lv < 1){ ?>
                <td>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#tt').edatagrid('addRow',0)">add</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#tt').edatagrid('destroyRow')">delete</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#tt').edatagrid('saveRow');">save</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#tt').edatagrid('cancelRow')">cancel</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="container_info_excel()">导出EXCEL</a>
                    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:import_excel();"><?= lang('Import');?></a>
                    &nbsp;
                    <?= template_link('shipment', $shipment_id, 'INVOICE_VP_CONTAINER', '', '/export/invoice_vp');?>
                </td>
                <td width='80'> </td>
            <?php } ?>
            <td>
                <span style="font-size:12px;">container_no</span>
                <input id="container_no" style="border:1px solid #ccc">
            </td>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton"  iconCls="icon-search" plain="true" onclick="doSearch()">search</a>
                    <a href="#" class="easyui-tooltip" data-options="
                        content: $('<div></div>'),
                        onShow: function(){
                            $(this).tooltip('arrow').css('left', 20);
                            $(this).tooltip('tip').css('left', $(this).offset().left);
                        },
                        onUpdate: function(cc){
                            cc.panel({
                                width: 300,
                                height: 'auto',
                                border: false,
                                href: '/bsc_help_content/help/shipment_container_index'
                            });
                        }
                    "><img src="../../../inc/image/tipicon.png?v=1" style="width:15px;height:15px;"></a>
            </td>
        </tr>
    </table>
</div>

<div id="import" class="easyui-dialog" style="background-color: #F4F4F4;border:1px solid #95B8E7;margin-top:10px;"
     data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons',title:'<?= lang('Import');?>'">
    <table>
        <tr>
            <td>
                <input class="easyui-filebox" name="file" id="file_upload"
                       data-options="" style="width:400px">
            </td>
        </tr>
    </table>
    <div id="dlg-buttons">
        <a class="easyui-linkbutton" id="file_save" iconCls="icon-ok" style="width:90px"><?= lang('save');?></a>
    </div>
    <div id="div_files"></div>
</div>
