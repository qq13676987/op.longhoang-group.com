<?php

/**
 * 邮件通道的模型
 * Class mail_sender_model
 */
class mail_template_model extends CI_Model{
    private $table = 'mail_template';

    /**
     * 相关的字段
     * 原本的,第一个为字段名, 第二个为页面上使用的名称,第三个为长度, 这里的版本,应该只用的了第一个, 长度的和字段名的话,可能得用林总的新版的表头
     * @var array
     */
    public $field_all = array(
        array("id", "id", "94"),
        array("mail_type", "mail_type", "94"),
        array("mail_title", "mail_title", "94"),
        array("mail_content", "mail_content", "94"),
        array("created_by", "created_by", "94"),
        array("created_time", "created_time", "94"),
        array("updated_by", "updated_by", "94"),
        array("updated_time", "updated_time", "94"),
    );

    public function __construct()
    {
        $this->load->database();
    }

    /**
     * 根据某个字段获取单条数据
     * @param string $f 字段名
     * @param string $v 值
     * @return mixed 返回单条数据, 一维数组
     */
    public function get_one($f="",$v=""){
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    /**
     * 传入where条件 获取单条数据,传入空,返回最老的
     * @param string $where  where 条件 最好字符串
     * @return mixed  返回单条数据, 一维数组
     */
    public function get_where_one($where = ''){
        if($where !== '') $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    /**
     * 获取相关的所有数据,想分页 在调用该方法前使用 $this->db->limit(参数); 详情搜索CI 这里最好别什么都不塞,那么会全部返回的
     * @param string $where
     * @param string $sort 排序字段, 如果join了表格可能得注意加表前缀
     * @param string $order asc 或desc
     * @return mixed 返回多条数据, 二维数组
     */
    public function get($where="" ,$sort="id",$order="desc"){
        if($where !="")	$this->db->where($where);
        $this->db->order_by($sort,$order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    /**
     * 统计总数使用,类似 count
     * @param string $where
     * @return mixed 返回的是具体的数字
     */
    public function total($where=""){
        if($where !="")	$this->db->where($where);
        $this->db->from($this->table);
        return  $this->db->count_all_results('');
    }

    /**
     * 这方法不用管,其他地方可能会用到,放着就好
     * @param string $where
     * @param string $sort
     * @param string $order
     * @return mixed
     */
    public function no_role_get($where="" ,$sort="id",$order="desc"){
        if($where !="")	$this->db->where($where);
        $this->db->order_by($sort,$order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    /**
     * 新增一条数据
     * @param array $data
     * @return mixed 返回新增主键ID
     */
    public function save($data=array()){
        $data["created_by"] = get_session('id');
        $data["created_time"] = date('Y-m-d H:i:s');
        $data["updated_by"] = get_session('id');
        $data["updated_time"] = date('Y-m-d H:i:s');
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * 编辑一条数据, 如果想修改为其他条件,可以 自己加个使用where的, 但是这个最保险
     * @param string $id  主键
     * @param array $data  数据
     * @return string
     */
    public function update($id='',$data=array()){
        $data["updated_by"] = get_session('id');
        $data["updated_time"] = date('Y-m-d H:i:s');
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    /**
     * 删除数据
     * @param string $id
     */
    public function mdelete($id=''){
        // delete container
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }
}