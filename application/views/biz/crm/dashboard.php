<!doctype html> 
<html>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="/inc/css/normal.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/inc/js/easyui/themes/default/easyui.css">
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <script type="text/javascript" src="/inc/js/easyui/jquery.easyui.min.js"></script>
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css" media="all">
    <script type="text/javascript" src="/inc/js/hcharts/highcharts.js?=1"></script>
    <script type="text/javascript" src="/inc/js/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="/inc/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="/inc/js/md5.js"></script>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>我的运价</title>
        <meta name="keywords" content="">
        <meta name="viewport" content="width=1180">
        <!--<link rel="stylesheet" href="/inc/style.css" >-->
        <script type="text/javascript">

            function teu_details(index){
                //获取参数
                var form_data = {'searchs[0][]':[], 'field[v][]':[], 'field[f][]':[], 'field[s][]':[]};
                // var crm_user_type = $.cookie('crm_user_type');
                var crm_user_type = $('#crm_user_type').val();
                // var crm_user = $.cookie('crm_user');
                var crm_user = $('#crm_user').val();
                // var crm_date_first = $.cookie('crm_date_first');
                var crm_date_first = $('#crm_date_first').val();
                // var crm_date_end = $.cookie('crm_date_end');
                var crm_date_end = $('#crm_date_end').val();
                if(crm_user_type == 'user'){
                    form_data['groups_field'] = 'sales';
                    form_data['searchs[0][]'].push('sales');
                    form_data['searchs[0][]'].push(crm_user);

                }else if(crm_user_type == 'group'){
                    form_data['groups_field'] = 'sales_group';

                    form_data['searchs[0][]'].push('sales_group');
                    form_data['searchs[0][]'].push(crm_user);
                }else if(crm_user_type == 'sub_company'){
                    form_data['groups_field'] = 'sales_company';

                    form_data['searchs[0][]'].push('sales_company');
                    form_data['searchs[0][]'].push(crm_user.replace('company_', ''));
                }

                form_data['field[f][]'].push('booking_ETD');
                form_data['field[s][]'].push('>=');
                form_data['field[v][]'].push(crm_date_first);

                form_data['field[f][]'].push('booking_ETD');
                form_data['field[s][]'].push('<=');
                form_data['field[v][]'].push(crm_date_end);

                form_data['field[f][]'].push('status');
                form_data['field[s][]'].push('=');
                form_data['field[v][]'].push('normal');

                //打开新窗口跳转到shipment
                // var form = $('<form></form>').attr('action','/biz_shipment/index?auto_click=1').attr('method','POST').attr('target', '_blank');
                var form = $('<form></form>').attr('action','/report/volume_total').attr('method','POST').attr('target', '_blank');

                $.each(form_data, function (index, item) {
                    if(typeof item === 'string'){
                        form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item));
                    }else{
                        $.each(item, function(index2, item2){
                            form.append($('<input/>').attr('type','hidden').attr('name', index).attr('value', item2));
                        })
                    }
                });

                form.appendTo('body').submit().remove();
            }

            function detail(url){
                if(url.indexOf('?') == -1) url += '?';
                else url += '&';
                var crm_user_type = ($('#crm_user_type').val());
                var crm_user = ($('#crm_user').val());
                var crm_date_first = ($('#crm_date_first').val());
                var crm_date_end = ($('#crm_date_end').val());
                var token = hex_md5(crm_user_type + crm_user + crm_date_first + crm_date_end);
                url += 'crm_user_type=' + crm_user_type + '&crm_user=' + crm_user + '&crm_date_first=' + crm_date_first + '&crm_date_end=' + crm_date_end + '&token=' + token;
                window.open(url);
            }

            function userLoadSuccess(node,data){
                //这里进行循环操作,当找到第一个is_use == true时,选中
                var val = $('#crm_user').val();
                console.log(data);
                if(val == '' && data.length > 0){
                    var result = searchIsuse(data);
                    $('#user').combotree('setValue', result);
                    $('#db_fm').submit();
                }
            }

            /**
             * 查找到最大的第一个可使用的
             * @param data
             */
            function searchIsuse(data){
                var result = '';
                //循环一遍,查看第一层结构是否有可使用的
                var new_data = [];
                $.each(data, function (i, it) {
                    if(it.is_use == true){
                        result = it.id;
                        return false;
                    }
                    new_data.push(it);
                });
                if(result != '') return result;
                //如果没找到,这里循环再递归一下
                $.each(new_data, function (i, it) {
                    result = searchIsuse(it.children);
                    if(result != '') return result;
                })

                return result;
            }

            $(function () {
                var json = {};

                var title = {
                    text: ' '
                };

                var subtitle = {
                    text: '客户数统计'
                };

                var xAxis = {
                    categories: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31]
                };

                var yAxis = {
                    title: {
                        text: '数量'
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                };

                var tooltip = {
                    valueSuffix: ''
                };

                var legend = {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                };

                var series =  [
                    {
                        name: '新增客户',
                        data: [
                            <?= join(',', array_column($NewAddList, 'n'));?>
                        ]
                    },
                    {
                        name: '跟进客户',
                        data: [
                            <?= join(',', array_column($FollowUpList, 'n'));?>
                        ]
                    },
                    {
                        name: '成交客户',
                        data: [
                            <?= join(',', array_column($DealNumberList, 'n'));?>
                        ]
                    }
                ];

                json.title = title;
                json.subtitle = subtitle;
                json.xAxis = xAxis;
                json.yAxis = yAxis;
                json.tooltip = tooltip;
                json.legend = legend;
                json.series = series;
                $('#container').highcharts(json);
            });
        </script>
    </head>
    <body style="margin:10px;">
        <div class="layui-row">
            <div class="layui-col-xs12" style="font-size:12px;">
                总客户数:<a href="javascript:void(0);" onclick="detail('/biz_crm/TotalNumber');" style="color:blue;font-size:22px;"><?php echo $totalNumber;?></a>,
                &nbsp;&nbsp;&nbsp;&nbsp;
                成交客户数:<a href="javascript:void(0);" onclick="detail('/biz_crm/DealMore');" style="color:blue;font-size:22px;" title="shipment成交过的客户数量"><?= $DealMore;?></a>,
                &nbsp;&nbsp;&nbsp;&nbsp;
                当前成交客户数:<a href="javascript:void(0);" onclick="detail('/biz_crm/DealNumber');" style="color:blue;font-size:22px;"><?= $DealNumber;?></a>,
                &nbsp;&nbsp;&nbsp;&nbsp;
                当前客户活跃度:<span style="color:blue;font-size:22px;" title="成交数/当前总客户数"><?= $ClientActivityDegree;?>%</span>,
                &nbsp;&nbsp;&nbsp;&nbsp;
                当前总TEU数:<a href="javascript:void(0);" onclick="teu_details()" style="color:blue;font-size:22px;"><?= $ClientTEU;?></a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                CRM转化率:<a href="javascript:void(0);" style="color:blue;font-size:22px;" title="正式通过/CRM客户里的总客户数"><?= $CrmClientRate;?>%</a>
            </div>
        </div>
        <div class="layui-row" style="margin-top:30px;">
            <div class="layui-col-xs12">
                <div class="grid-demo grid-demo-bg1"> 
                    <form id="db_fm" action="" method="get">
                        <span style="font-weight:bold;font-size:16px;">简报看板</span>   
                        <span style="font-size:12px;margin-left:20px;">
                            <select class="easyui-combotree" id="user" style="width:250px;" data-options="
                                valueField:'id',
                                textField:'text',
                                cascadeCheck: false,
                                url:'/bsc_user/get_user_role_tree',
                                value:'<?= $crm_user;?>',
                                onBeforeSelect: function(node){
                                    var inp = $('#user');
                                    var roots = inp.combotree('tree').tree('getRoots');
                                    var result = true;
                                    if(result){
                                        if(!node.is_use){
                                            return false;
                                        }
                                        $('#crm_user').val(node.id);
                                        $('#crm_user_type').val(node.type);
                                    }
                                    return result;
                                },
                                onLoadSuccess:userLoadSuccess,
                            "></select>
                            <input type="hidden" name="crm_user" id="crm_user" value="<?= $crm_user;?>">
                            <input type="hidden" name="crm_user_type" id="crm_user_type" value="<?= $crm_user_type;?>">
                            /
                            <input type="text" class="easyui-datebox" name="crm_date_first" id="crm_date_first" value="<?= $crm_date_first;?>" style="width:120px;" autocomplete="off"/>
                            -
                            <input type="text" class="easyui-datebox" name="crm_date_end" id="crm_date_end" value="<?= $crm_date_end;?>" style="width:120px;" autocomplete="off"/>
                            <button type="button" onclick="javascript:$('#db_fm').submit();">提交</button>
                        </span>
                    </form>
                </div>
            </div> 
        </div>

    <div class="layui-row"  style="margin-top:50px;margin-left:50px;">
        <div class="layui-col-xs3">
            <div class="grid-demo grid-demo-bg1"> 
                <a href="javascript:void(0);" onclick="detail('/biz_crm/CrmClientNewAdd');" style="color:blue;font-size:22px;"><?php echo $CrmClientNewAdd;?></a> 个  <br> <br>
                <span style="font-size:14px; ">新增CRM客户数</span>
            </div>
        </div>
        <div class="layui-col-xs3">
            <div class="grid-demo grid-demo-bg1"> 
                <a href="javascript:void(0);" onclick="detail('/biz_crm/customer_apply?apply_status=2');" style="color:blue;font-size:22px;"><?= $FollowUp;?></a> 个  <br> <br>
                <span style="font-size:14px; ">跟进客户数</span>
            </div>
        </div>
        <div class="layui-col-xs3">
            <div class="grid-demo grid-demo-bg1"> 
                <span href="/biz_crm/CrmDealOne" target="_blank" style="color:blue;font-size:22px;"><?= $CrmDealOne;?></span> 个  <br> <br>
                <span style="font-size:14px; ">首单CRM客户数</span>
            </div>
        </div>
        <div class="layui-col-xs3">
            <div class="grid-demo grid-demo-bg1"> 
                <span style="color:blue;font-size:22px;"><?= $CrmShipmentCount;?></span> 个  <br> <br>
                <span style="font-size:14px; ">CRM总票数</span>
            </div>
        </div>
    </div>
    <div class="layui-row"  style="margin-top:50px;margin-left:50px;">
        <div class="layui-col-xs3">
            <div class="grid-demo grid-demo-bg1">
                <span style="color:blue;font-size:22px;"><?= $CrmClientTeu;?></span> 个  <br> <br>
                <span style="font-size:14px; ">CRM总TEU数</span>
            </div>
        </div>
        <div class="layui-col-xs3">
            <div class="grid-demo grid-demo-bg1">
                <a href="javascript:void(0);" onclick="detail('/crm_promote_mail/index');" style="color:blue;font-size:22px;"><?= $LastSendMailTime;?></a><br> <br>
                <span style="font-size:14px; ">最近群发时间</span>
            </div>
        </div>
        <div class="layui-col-xs3">
            <div class="grid-demo grid-demo-bg1">
                <span style="color:blue;font-size:22px;"><?= $SendMailCount;?></span><br> <br>
                <span style="font-size:14px; ">邮件群发数量</span>
            </div>
        </div>
    </div>
        <div class="layui-row"  style="margin-top:111px; ">
        <!--<div id="container" style=" height: 400px; margin: 0 auto"></div>-->
    </div>
    </body>
</html>