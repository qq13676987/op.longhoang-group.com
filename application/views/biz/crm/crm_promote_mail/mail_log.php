<title><?= lang('邮件');?></title>
<style>
    * {
        font-size: 14px;

    }

    a {
        text-decoration: none;
    }

    body {
        margin: 12px;
    }

    li {
        list-style-type: none;
        margin: 6px;
    }

    ul {
        padding-left: 10px;
        margin: 0px;
        padding-top: 10px;
        padding-bottom: 10px;
    }

    .head-bg {
        background-color: #F1F5F4;
    }

    .easyui-window h3 {
        margin-top: 8px;
        margin-bottom: 8px;
        margin-left: 10px;
        margin-right: 10px;
    }

    .easyui-window ul {
        padding: 0;

    }

    .span-w {
        width: 16px;
        height: 16px;
        display: inline-block;
        margin-right: 8px;
    }

    ul.item {
        padding: 0;
    }

    ul.item li {
        display: inline-block;
    }

    ul.item li:nth-child(1) {
        width: 235px;
    }

    ul.item li:nth-child(2) {
        width: 366px;
    }


</style>
<body>
<h3><?= lang('主题');?>: <?php echo $mail_c["mail_title"]; ?></h3>
<div class="head-bg">
    <ul>
        <li><?= lang('创建时间');?>: <?php echo $mail_c["create_time"] ?></li>
        <!--  <?php /*if(!empty($mail_c["attachment"])):*/?>
        <li>附件: <a href="<?php /*echo $mail_c["attachment"] */?>" target="_blank">点击查看附件</a></li>
        --><?php /*endif;*/?>
        <?php if ($mail_c["send_group_id"] != 1) { ?>
            <li><?= lang('计划发送时间');?>: <?php echo $mail_c["plan_send_time"] ?></li>
            <li><?= lang('发件人');?>: <?php echo $mail_c["sender_name"] ?></li>
            <?php
            if ($mail_c['status'] == 0)
                echo "<li>" . lang('收件人') . ": <a href='/crm_promote_mail/group_consignee_list?group_id={$mail_c["send_group_id"]}' target='_blank'>" . lang('查看收件人') . "</a> </li>";
            else {
                echo "<li>" . lang('收件人') . ": <a href='#' onclick='owin()'>查看" . sizeof($mail_l) . "个收件人</a> </li>";
            }

            if ($mail_c['status'] == 0)
                echo "<li>  <button onclick='javascript:sendMail({$mail_c["id"]});' style='color:red;font-size:16px;'>  " . lang('点击这里，确认发送') . " </button></li>";
            else {
                echo "<li>" . lang("总数：{total} 已发送:  {send}  已读:  {read} ,全公司还在排队的总数： {paidui} ", array('total' => $total, 'send' => $send, 'read' => $read, 'paidui' => $paidui)) . "</li>";
                if ($total == 0) $percent = 0;
                else
                    $percent = round($send * 100 / $total, 2);
                echo "<a href='#' onclick='owin()'> <div class='easyui-progressbar' data-options='value:$percent' style='width:200px;'></div></a>";
            }
        }
        ?>

    </ul>
</div>
<h3><?= lang('邮件内容');?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='/crm_promote_mail/edit?id=<?php echo $mail_c['id']; ?>'><?= lang('我要修改');?></a>
</h3>
<div><?php echo $mail_c["mail_content"] ?></div>
<div id="win" class="easyui-window" title="<?= lang('收件人');?>" closed="true" style="width:1400px;height:450px;">
    <h3><?= lang('收件人');?></h3>
    <ul>
        <?php foreach ($mail_l as $k => $v): ?>
            <li>
                <ul class="item">
                    <li><span class="icon-ok span-w">&nbsp;</span> <?php echo $v["rcv_name"] ?></li>
                    <li> <?php echo $v["rcv_email"] ?></li>


                    <?php if ($v['status'] == -1): ?>
                        <li style="width:200px"><span style="color:red;"><?= lang('发送失败');?></span></li>
                        <li>
                            <div class='hiddenMemo' style="color:#FF5722;"
                                 title='<?= $v['send_msg']; ?>'> <?= $v['send_msg']; ?></div>
                        </li>
                    <?php elseif ($v['status'] == 1): ?>
                        <li style="width:200px"><?php echo empty($v['open_time']) ? '<span style="color:#1E9FFF;">' . lang('未读') . '</span>' : '<span style="color:#009688;">' . lang('已读') . ' : ' . $v["open_time"] . '</span>'; ?></li>
                        <li>
                            <div class='hiddenMemo' style="color:#009688;" title='<?= $v['send_msg']; ?>'><?= lang('消息已发送');?>
                        </li>
                    <?php else : ?>
                        <li style="width:200px"></li>
                        <li>
                            <div class='hiddenMemo' style="color:#009688;" title='<?= $v['send_msg']; ?>'><?= lang('等待发送');?>
                        </li>
                    <?php endif; ?>


                </ul>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
<style>
    /*单行隐藏显示的样式设定*/
    .hiddenMemo {
        display: inline-block;
        vertical-align: middle;
        width: 300px; /*设置隐藏显示的最大宽度*/
        white-space: nowrap; /* 设置文字在一行显示，不能换行 */
        overflow: hidden; /* 文字长度超出限定宽度，则隐藏超出的内容 */
        text-overflow: ellipsis; /* 规定当文本溢出时，显示省略符号来代表被省略的文本 */
    }

</style>
<script type="text/javascript" src="/inc/layer3.1.1/layer.js"></script>
<script>
    function owin() {
        $('#win').window('open');
    }

    function sendMail(id) {

        layer.confirm('<?= lang('您确定群发当前邮件？');?>', {
            btn: ['<?= lang('确定');?>', '<?= lang('取消');?>'] //按钮
        }, function () {
            layer.closeAll('dialog');
            ajaxLoading('<?= lang('正在发送中,请不要关闭此页面');?>');
            $.getJSON("/crm_promote_mail_auditing/do_it?id=" + id + "&group_id=<?= $mail_c["send_group_id"] ?>&opt=pass&reason=", function (data) {
                ajaxLoadEnd();
                if (data.code == 200) {
                    layer.msg(data.msg);
                    // $('#dg').datagrid('reload');
                    setTimeout("location.reload()", 3000);
                } else {
                    layer.alert(data.msg, {icon:2});
                }

            });

        });
    }

</script>

</body>
</html>