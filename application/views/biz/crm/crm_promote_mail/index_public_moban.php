<div align="center">
    <!--<span style="color:red;font-size:30px;"> 待审核的邮件，请自己到 “查看详情” 中审核发送， 有个“审核发送”按钮。</span><br>-->
</div>
<!--<h3 style="color:red;">本年总发送量: <?= $year_send['send_count'];?>, 已读: <?= $year_send['read_count'];?>. &nbsp;&nbsp;&nbsp;&nbsp; 本月总发送量: <?= $month_send['read_count'];?>, 已读: <?= $month_send['read_count'];?>.</h3>-->
<table id="dg" class="easyui-datagrid" title="<?= lang('邮件群发');?>"
       data-options="singleSelect:true,url:'/crm_promote_mail/get_data?group_id=<?php echo isset($_GET["group_id"])?$_GET["group_id"]:""; ?>',method:'post',pageSize:20" pagination="true" toolbar="#toolbar" fitColumns="true" >
    <thead>
    <tr>
        <th data-options="field:'id',width:40"><?= lang('ID');?></th>
        <th data-options="field:'send_group',width:100, editor:'text'"><?= lang('收件人组');?></th>
        <th data-options="field:'mail_title',width:200"><?= lang('邮件标题');?></th>
        <th data-options="field:'create_name',width:30,align:'center'"><?= lang('创建人');?></th>
        <th data-options="field:'create_time',width:80,align:'center'"><?= lang('创建时间');?></th>
        <th data-options="field:'update_time',width:80,align:'center'"><?= lang('更新时间');?></th>
        <th data-options="field:'operate',width:50,align:'center'"><?= lang('操作');?></th>
    </tr>
    </thead>
</table>

<div id="toolbar">

    <div>
        <?php if(is_admin() || get_session("id") == 20142){ ?>
            <a href="/crm_promote_mail/add_presend/?group_id=1" class="easyui-linkbutton" iconCls="icon-add" plain="true"><?= lang('创建公共模板');?></a>
        <?php }?>

        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#dg').edatagrid('saveRow')"><?= lang('保存');?></a>

        <a href="javascript:delConsig();" class="easyui-linkbutton" iconCls="icon-remove" plain="true"><?= lang('删除');?></a>
        <!--        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');">查询</a>-->

        &nbsp;&nbsp;&nbsp;&nbsp;
        <span style="color:red;"><?= lang('双击表格可以修改“收件人组”的名字');?></span>
    </div>

</div>

<!--查询-->
<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <form id="cx">
        <table>
            <tr>
                <td><?= lang('部门');?></td>
                <td>
                    <select class="easyui-combotree" id="user" style="width:475px;" data-options="
                        valueField:'id',
                        textField:'text',
                        cascadeCheck: false,
                        url:'/bsc_user/get_user_role_tree',
                        value:'<?= $crm_user;?>',
                        onBeforeSelect: function(node){
                            var inp = $('#user');
                            var roots = inp.combotree('tree').tree('getRoots');
                            var result = true;
                            if(result){
                                if(!node.is_use){
                                    return false;
                                }
                                $('#crm_user').val(node.id);
                                $('#crm_user_type').val(node.type);
                            }
                            return result;
                        },
                        onLoadSuccess:userLoadSuccess,
                    "></select>
                    <input type="hidden" name="crm_user" id="crm_user" value="<?= $crm_user;?>">
                    <input type="hidden" name="crm_user_type" id="crm_user_type" value="<?= $crm_user_type;?>">
                </td>
            </tr>
        </table>
        <br><br>
        <button type="button" class="easyui-linkbutton" style="width:200px;height:30px;" onclick="doSearch();"><?= lang('查询');?></button>
        <a href="javascript:void;" class="easyui-tooltip" data-options="
            content: $('<div></div>'),
            onShow: function(){
                $(this).tooltip('arrow').css('left', 20);
                $(this).tooltip('tip').css('left', $(this).offset().left);
            },
            onUpdate: function(cc){
                cc.panel({
                    width: 500,
                    height: 'auto',
                    border: false,
                    href: '/bsc_help_content/help/query_condition'
                });
            }
        "><?= lang('help');?></a>
    </form>
</div>
<script type="text/javascript" src="/inc/layer3.1.1/layer.js"></script>

<script>

    $(function(){

        $('#dg').edatagrid({

            updateUrl: '/crm_promote_mail/upGroupName/',


            onError: function (index, data) {

                $.messager.alert('error', data.msg);

            }

        });

        $('#dg').datagrid({

            width:'auto',

            height: $(window).height()

        });



        $(window).resize(function () {

            $('#dg').datagrid('resize');

        });

    });



</script>


<script>
    /**
     * 设置状态,进行查询
     */
    function status_search(status){
        $('#dg').datagrid({
            queryParams: {
                status: status,
            }
        });
    }

    function doSearch() {
        var json = {};
        var cx_data = $('#cx').serializeArray();
        $.each(cx_data, function (index, item) {
            //判断如果name存在,且为string类型
            if(json.hasOwnProperty(item.name) === true){
                if(typeof json[item.name] == 'string'){
                    json[item.name] = [json[item.name]];
                    json[item.name].push(item.value);
                }else{
                    json[item.name].push(item.value);
                }
            }else{
                json[item.name] = item.value;
            }
        });
        var opt = $('#dg').datagrid('options');
        opt.url = '/crm_promote_mail/get_data';
        $('#dg').datagrid('load', json).datagrid('clearSelections');
        set_config();
        $('#chaxun').window('close');
    }
    function set_config() {
        var f_input = $('.f');
        var count = f_input.length;
        var config = [];
        $.each(f_input, function (index, item) {
            var c_array = [];
            c_array.push($('.f:eq(' + index + ')').combobox('getValue'));
            c_array.push($('.s:eq(' + index + ')').combobox('getValue'));
            config.push(c_array);
        });
        // var config_json = JSON.stringify(config);
        $.ajax({
            type: 'POST',
            url: '/sys_config/save_config_search',
            data:{
                table: 'crm_promote_mail',
                count: count,
                config: config,
            },
            dataType:'json',
            success:function (res) {

            },
        });
    }

    function userLoadSuccess(node,data){
        //这里进行循环操作,当找到第一个is_use == true时,选中
        var val = $('#crm_user').val();
        if(val == ''){
            var result = searchIsuse(data);
            $('#user').combotree('setValue', result);
            $('#db_fm').submit();
        }
    }
    /**
     * 查找到最大的第一个可使用的
     * @param data
     */
    function searchIsuse(data){
        var result = '';
        //循环一遍,查看第一层结构是否有可使用的
        var new_data = [];
        $.each(data, function (i, it) {
            if(it.is_use == true){
                result = it.id;
                return false;
            }
            new_data.push(it);
        });
        if(result != '') return result;
        //如果没找到,这里循环再递归一下
        $.each(new_data, function (i, it) {
            result = searchIsuse(it.children);
            if(result != '') return result;
        })

        return result;
    }


    function sendMail(id){
        layer.confirm('<?= lang('您确定群发当前邮件？');?>', {
            btn: ['<?= lang('确定');?>','<?= lang('取消');?>'] //按钮
        }, function()
        {
            layer.closeAll('dialog');
            $.getJSON("/crm_promote_mail_send/send?id="+id,function(data){

                if(data.code==200){
                    layer.msg(data.msg);
                    $('#dg').datagrid('reload');
                }
                else {
                    layer.msg(data.msg);
                }

            });

        });
    }
    function edit(id) {
        window.open('/crm_promote_mail/edit?id=' + id);
    }

    function send_and_read_for(value, row, index){
        var str = row.send_count + "/" + row.read_count;
        return str;
    }
    function delConsig(){
        var row = $('#dg').datagrid('getSelected');
        if (row != null){
            $.messager.confirm('<?= lang('提示');?>', '<?= lang('您确定要删除？');?>', function(){
                $.getJSON("/crm_promote_mail/delete_data?id="+row.id,function(data){
                    if(data.code==200){
                        $.messager.alert('<?= lang('提示');?>', data.msg);
                        $('#dg').datagrid('reload');
                    } else{
                        $.messager.alert('<?= lang('提示');?>', data.msg);
                    }
                });
            });
        }else{
            $.messager.alert("Tips","No Item Selected");
        }
    }
</script>