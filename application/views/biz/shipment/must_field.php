<style>
	.con{
		width: 95%;
		background: #e0ecff;
		border: 1px solid #95B8E7;
		padding: 12px 20px;
		display: flex;
		flex-wrap: wrap
	}
	.item{
		width: 185px;
		margin-left: 10px;
		border: 1px solid #95B8E7;
		text-align: center;
		background: white;
		height: 30px;
		line-height: 30px;
		font-size: 14px;
	}
	.bg-grey{
		background: #ddd;
	}
</style>
<div class="con">
	<?php foreach ($data as $k=>$v):?>
	<div class="item <?=$v==0?'':'bg-grey'?>">
		<?=lang($k)?>: <span style="color: red"><?=$v==0?'×':'√'?></span>
	</div>
	<?php endforeach;?>
</div>
