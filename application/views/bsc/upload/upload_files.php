<link rel="stylesheet" href="/inc/third/layui/css/layui.css" media="all">
<script src="/inc/third/layui/layui.js" charset="utf-8"></script>
<?php if($lock_lv < 1){?>
<fieldset>
    <legend><?= lang('上传文件'); ?></legend>
    <input type="hidden" value="<?php echo $id_no; ?>" name="id_no" id="id_no">
    <input type="hidden" value="<?php echo $biz_table; ?>" name="biz_table" id="biz_table">
    <table style="height: 199px">
        <tr>
            <td valign="top">
                <table>
                    <tr>
                        <td><?= lang('文件类型'); ?>：</td>
                        <td>
                            <?php if(isset($_GET['type']) && !empty($_GET['type'])):?>
                            <select name="type_name" readonly="true" required id="type_name" class="easyui-combobox" style="width: 400px;"
                                    data-options="valueField:'value',
                                textField:'value',
                                editable:false,
                                url:'/bsc_dict/get_option/upload_file_type'">
                            </select>
                            <?php else:?>
                            <select name="type_name" required id="type_name" class="easyui-combobox" style="width: 400px;"
                                    data-options="valueField:'value',
                                                    textField:'value',
                                                    editable:false,
                                                    url:'/bsc_dict/get_option/upload_file_type?biz_table=<?= $biz_table;?>'">
                            </select>
                            <?php endif;?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <div class="layui-upload-drag" id="file_upload" style="width: 340px;margin-top: 10px;">
                                <i class="layui-icon"></i>
                                <p><?= lang('点击上传,或将文件拖拽到此处'); ?></p>
                                <div class="layui-hide" id="file_upload_view">
                                    <hr>
                                    <span></span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><button id="file_save" style="width: 462px; height: 25px;margin-top: 10px;"><?= lang('submit'); ?></button></td>
                    </tr>
                </table>
            </td>
            <td valign="top" style="padding-left: 20px;">
                <table>
                </table>
            </td>
        </tr>
    </table>
    <div id="div_files"></div>
</fieldset>
<?php }?>
<div class="tb">
    <br/>
    <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#tt').edatagrid('saveRow')"><?= lang('save');?></a>
    <table id="tt"
           rownumbers="false" pagination="true" idField="id"
           toolbar="#tb" singleSelect="true" nowrap="false" showFooter="true" style="height: 500px">
        <thead>
        <tr>
            <?php if (!empty($f)) {
                    foreach ($f as $rs) {

                        $field = $rs[0];
                        $disp = $rs[1];
                        $width = $rs[2];
                        $attr = $rs[4];

                        if ($width == "0") continue;
                        echo "<th data-options=\"field:'$field',width:$width,$attr\">" . lang($disp) . "</th>";
                    }
                }
            ?>

            <th data-options="field:'ID',width:180,align:'center',formatter:formatOper"><?= lang('操作'); ?></th>
        </tr>
        </thead>
    </table>
</div>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
<!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"-->
<!--                   onclick="javascript:$('#tt').edatagrid('destroyRow')">delete</a>-->
            </td>
        </tr>
    </table>

</div>
<script>
    layui.use(['upload', 'element', 'layer'], function() {
        var $ = layui.jquery
            , upload = layui.upload
            , element = layui.element
            , layer = layui.layer;

        //拖拽上传
        upload.render({
            elem: '#file_upload'
            ,accept:'file'
            ,acceptMime: ".gif,.jpeg,.jpg,.png,.pdf,.PDF,.doc,.docx,.xlsx,.xls,.zip,.rar" //指定文件类型
            ,auto: false
            ,multiple:false
            ,data:{
                id_no: function(){
                    return $('#id_no').val();
                },
                biz_table: function(){
                    return $('#biz_table').val();
                },
                type_name: function(){
                    return $('#type_name').combobox('getValue');
                }
            }
            ,url: '/bsc_upload/file_upload' //此处用的是第三方的 http 请求演示，实际使用时改成您自己的上传接口即可。
            ,bindAction: '#file_save'
            ,choose: function(obj){
                //将每次选择的文件追加到文件队列
                var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列


                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function(index, file, result){
                    //   console.log(index); //得到文件索引
                    //   console.log(file); //得到文件对象
                    layui.$('#file_upload_view').removeClass('layui-hide').find('span').text(file.name);
                    //   console.log(result); //得到文件base64编码，比如图片

                    //obj.resetFile(index, file, '123.jpg'); //重命名文件名，layui 2.3.0 开始新增

                    //这里还可以做一些 append 文件列表 DOM 的操作

                    //obj.upload(index, file); //对上传失败的单个文件重新上传，一般在某个事件中使用
                    // delete files[index]; //删除列表中对应的文件，一般在某个事件中使用
                });
            }
            ,done: function(res, index, upload){
                layer.msg(res.msg);
                if(res.code == 0){
                    $('#tt').edatagrid('reload');
                }
                delete this.files[index];
            }

        });
    });
    
    $(function () {

        //判断类型赋值
        <?php if(!empty($type)):?>
        $('#type_name').combobox('enable').combobox({readonly: true});
        $('#type_name').combobox('setValue','<?=$type?>');
        <?php endif;?>


        $('#tt').edatagrid({
            url: '/bsc_upload/get_data/<?php echo $biz_table; ?>/<?php echo $id_no; ?>?type_name=<?= $type_name;?>',
            updateUrl: '/bsc_upload/update_data',
            destroyUrl: '/bsc_upload/delete_data',
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height() - 300
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });

        $('.tb').on('click', '.del', function () {
            var id = $(this).attr('val');
            $.messager.confirm('info', 'Are you sure to delete?', function (r) {
                if(r){
                    $.ajax({
                        type: 'GET',
                        url: '/bsc_upload/delete_data?id=' + id,
                        success: function (res) {
                            $.messager.alert("info", 'success!');
                            $('#tt').edatagrid('reload');
                        }
                    });
                }
            });

        });
    });
    
    function history_file(type_name = ''){
        window.open("/bsc_upload/upload_files/<?php echo $biz_table; ?>/<?php echo $id_no; ?>?type_name=" + type_name);
    }

    function formatOper(val,row,index){
        <?php if($lock_lv < 1){?>
        var oper = '<button class="del" href="javascript:;" val="'+row["id"]+'">删除</button>';
        <?php }else{?>
        var oper = '';
        <?php }?>
        // oper += ' <button type="button" onclick="history_file(\'' + row.type_name + '\')">历史</button>';
        // oper += '&nbsp;&nbsp;<div class="circle" style="color:white;display:inline-block">'+row.sum+'</div>';
        return oper;
    }
    

    function file_name(val,row,index){
        //含有特殊字符的话，处理完再赋值（没有处理空字符串）
        var filename = val;
    	if(filename.indexOf('#') != -1 || filename.indexOf('+') != -1 || filename.indexOf('/') != -1 || filename.indexOf('?') != -1 || filename.indexOf('%') != -1 || filename.indexOf('&') != -1 || filename.indexOf('=') != -1){
    		upload_path = filename.replace(/([\#|\+|\?|\%|\#|\&|\=])/g, function ($1) {
    			return encodeURIComponent($1)
    		})
    	}else{
    		//如果不含有特殊字符直接赋值
    		upload_path = filename;
    	}
    	return '<a href="/download?id_type=<?= $biz_table;?>&id_no=<?= $id_no;?>&file_path=<?= UPLOAD_FILE;?>' + upload_path + '\" target="_blank">' + val + '</a>';
    }

</script>
<style>
    .circle {
        background-color: #E83338;
        width: 16px;
        height: 16px;
        border-radius: 50%;
    }
</style>

