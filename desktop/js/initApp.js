$(function () {
	var urlhost = "http://" + window.location.host;
    $('body').app({
        onTaskBlankContextMenu:onTaskBlankContextMenu,
        onAppContextMenu:onAppContextMenu,
        onWallContextMenu:onWallContextMenu,
		onStartMenuClick:onStartMenuClick,
    });
	
	function onStartMenuClick(item){
		var data = $(item.target).data("data");
		$('body').app("createwindow",data);
	}

    var appListMenuData = [
        {
            "text":"Open"
        },
        {
            "text":"Close"
        },
        {
            "text":"Close Other"
        },
        {
            "text":"Close All"
        },
        {
            "text":"Task Manage"
        },
        {
            "text":"Property"
        }
    ];

   	//  var appListMenu = $('body').app('createMenu', {data:appListMenuData});//原先的
    //自己加
    var appListMenu = $('body').app('createMenu', {data:appListMenuData,opt:{onClick:onTaskBlankContextMenuClick}});
    var APPID1;
    //自己加end

    function onTaskBlankContextMenu(e, appid) {
        if (appid) {
           // console.log(appid);
            appListMenu.menu('findItem', 'Task Manage').target.style.display = 'none';
            appListMenu.menu('findItem', 'Property').target.style.display = 'none';
            appListMenu.menu('findItem', 'Open').target.style.display = 'block';
            appListMenu.menu('findItem', 'Close').target.style.display = 'block';
            appListMenu.menu('findItem', 'Close Other').target.style.display = 'block';
            appListMenu.menu('findItem', 'Close All').target.style.display = 'block';
        } else {
            appListMenu.menu('findItem', 'Task Manage').target.style.display = 'block';
            appListMenu.menu('findItem', 'Property').target.style.display = 'block';
            appListMenu.menu('findItem', 'Open').target.style.display = 'none';
            appListMenu.menu('findItem', 'Close').target.style.display = 'none';
            appListMenu.menu('findItem', 'Close Other').target.style.display = 'none';
            appListMenu.menu('findItem', 'Close All').target.style.display = 'none';
        }

        appListMenu.menu('show', {
            left:e.pageX,
            top:e.pageY
        });
		APPID1=appid;//自己加的
        e.preventDefault();
    }
	
	//自己加的
    function onTaskBlankContextMenuClick(item){
		if(item.text == 'Open'){
			$("li[app_id='"+APPID1+"']").dblclick();
			//console.log('这里使原来的显示');
		}
		if(item.text == 'Close'){
			$("div[w_id='"+APPID1+"']").parent().find('.panel-tool-close').click();
			//console.log('这里使原来的关闭');
		}
		if(item.text == 'Close Other'){
			$("div[w_id='"+APPID1+"']").parent().siblings().find('.panel-tool-close').click();
			//console.log('这里关闭其他');
		}
		if(item.text == 'Close All'){
			$('.panel-tool-close').click();
			//console.log('这里关闭所有');
		}
	}
	//自己加的end

    var wallMenuData = [
        {
            "text":"Desktop Config",
            "href": urlhost + "/sys_config/desktop"
        },
        {
            'text':"Set Language",
            "href":"javascript:;",
            "children":[
                {
                    'text':'中文',
                    "href":urlhost + "/lang/setlang?lang=cn",
                    "window": 'no', 
                },
                {
                    'text':'english',
                    "href":urlhost + "/lang/setlang?lang=en",
                    "window": 'no', 
                }]
        },
        {
            "text":"View notification",
            "href": urlhost + "/bsc_notice/user_notice"
        },
        '-',{
            "text":"Refresh",
            "href": "javascript:;",
            "onClick": "javascript:$('body').app('refreshapp')",
        } 
    ];

    var appMenuData = [
        {
            "text":"Open"
        },
        '-',
        {
            "text":"Property"
        }
    ];

    var wallMenu;
    var appMenu = $('body').app('createMenu', {data:appMenuData,opt:{onClick:onAppContextMenuClick}});

    /**
     * 初始化右键菜单
     */
    function initWallMenu(target) {
        // var jqTarget = $(target);
        // console.log(opts);
        // var opts = $.data('body', 'app').options;

        $.ajax({
            url : '/bsc_menu/contextmenu',
            dataType : "JSON",
            async : false,
            cache : false,
            success : function (resp) {
                wallMenu = $('body').app('createMenu', {data:resp,opt:{onClick:onStartMenuClick}});
            },
            error : function (XMLHttpRequest, textStatus, errorThrown) {
                $.messager.alert("", textStatus || errorThrown, "error");
            }
        });
    }
    initWallMenu();

	var APPID;
    function onAppContextMenu(e,appid) {
        appMenu.menu('show', {
            left:e.pageX,
            top:e.pageY
        });
		APPID = appid;
    }
	
	function onAppContextMenuClick(item){
		if(item.text == 'Open'){
			$("li[app_id='"+APPID+"']").dblclick();
		}
	}

    function onWallContextMenu(e) {
        wallMenu.menu('show', {
            left:e.pageX,
            top:e.pageY
        });
    }
    
    var notice = false;
    //提醒窗口
    function getRemind() {
        //每天提醒1次，提醒每天0点刷新，上线即提示
        // setInterval(function() {
            //只有一条直接提示，超过进入查看更多页面
            // var day_remind = $.cookie('day_remind');
            // if(day_remind == null){//未提示
            //     var date = new Date();
            //     date.setDate( date.getDate()+1);
            //     date.setHours(0);
            //     date.setMinutes(0);
            //     date.setSeconds(0);
            //     $.cookie('day_remind', '1',{ expires: date });
            //     $.messager.show({
            //         title:'提示',
            //         msg:'你有1条待处理提醒，<a href="/aaa">点击查看</a>',
            //         timeout:10000,
            //         showType:'slide'
            //     });
            // }

        // }, 1000);
    
        // $.ajax({
        //     type: 'GET',
        //     url: '/bsc_notice/now_notice_count',
        //     success:function (res_json) {
        //         var res;
        //         try{
        //             res = $.parseJSON(res_json);
        //         }catch(e){
        //
        //         }
        //         if(res !== undefined && res.total > 0 && !notice){
        //             var lang = $.cookie('lang');
        //             if(lang === null){
        //                 lang = 'en';
        //             }
        //             var notice_lang = {
        //                 'en': 'Notice',
        //                 'cn': '通知',
        //             };
        //             notice = true;
        //             var dialogWidth = 600;      // dialog的宽度
        //             var dialogHeight = 500;     // dialog的高度
        //             var topPosition = $(window).height() - dialogHeight - 35;
        //             var leftPosition = $(window).width() - dialogWidth;
        //             var content = '<iframe src="/bsc_notice/main_user_notice" width="100%" height="99%" frameborder="0" scrolling="yes"></iframe>';
        //             $('#notice').dialog({
        //                 title: notice_lang[lang],
        //                 width: dialogWidth,
        //                 height: dialogHeight,
        //                 top:topPosition,
        //                 left:leftPosition,
        //                 content: content,
        //                 onBeforeClose:function(){
        //                     notice = false;
        //                 },
        //             });
        //         }
        //     }
        // });
    }
    // $.extend({
    //     myData:[{},{},{}],//数据
    //     seI_autoFn: null,//定时器
    //     autoFnIndex:0,//定时器执行的次数，可判断执行某些东西
    //     autoFn:function (argument) {//如果有需要，加参数
    //         //判断循环次数，如果有需要
    //         // if ($.autoFnIndex < $.myData.length) {
    //         if($.autoFnIndex === 0)argument();

    //         $.autoFnIndex++;
    //         // if ($.autoFnIndex < 2) {
    //         //
    //         //     //你要执行的方法function()
    //         //     //或者其他$.autoFn()的方法
    //         //     //例如定时更新myData的数据
    //         //
    //         //     $.autoFnIndex++;
    //         // } else {
    //         //     $.autoFnIndex = 0;
    //         // }
    //         //判断循环次数，如果有需要
    //         // if ($.autoFnIndex >= $.myData.length) {
    //         // if ($.autoFnIndex >= 2) {
    //         //     $.autoFnIndex = 0;
    //         // }
    //         //每次清空定时器
    //         clearTimeout($.seI_autoFn);
    //         $.seI_autoFn = null;
    //         //定时器更新时间
    //         var refushTime = 30 * 1000;//30*60*
    //         $.seI_autoFn = setTimeout(function() {
    //             argument();
    //             $.autoFn(argument)  ;
    //         }, refushTime);
    //     }
    // });
    getRemind();
    // $.autoFn();//可不加参数
    $('.app-notice').click(function () {
        if(!notice){
            getRemind();
        }else{
            $('#notice').dialog('close');
        }
    });
    //船东排名点击事件
    $('.app-shipownerRanking').click(function () {
        $('body').app('createwindow', {text:"船东排名", href: urlhost + '/report/month_teu_total?groups_field=trans_carrier', uuid: 'shipownerRanking20220624'})
    });
    //船东排名点击事件
    $('.app-salesTeuTotal').click(function () {
        $('body').app('createwindow', {text:"销售组排名", href: urlhost + '/report/teu_total?group_type=group&role=sales', uuid: 'salesTeuTotal20220613'})
    });

	$('.app-lock_notice').click(function () {
		$('body').app('createwindow', {text:"锁屏任务", href: urlhost + '/bsc_notice/lock_screen', uuid: 'operatorTeuTotal20230320'});
	});
});
