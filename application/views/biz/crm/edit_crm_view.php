<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link type="text/css" rel="stylesheet" href="/inc/third/layui/css/layui.css">
<script type="text/javascript" src="/inc/third/layer/layer.js"></script>
<title><?= lang('CRM-EDIT');?></title>
<script type="text/javascript">
    function checkvalue() {
        var vv = $('#country_code').textbox('getValue');
        if (vv == "") {
            layer.alert("country_code required！", {icon:5});
            return false;
        }
        var vv = $('#client_name').textbox('getValue');
        if (vv == "") {
            layer.alert("client_name required！", {icon:5});
            return false;
        }
        var vv = $('#company_name').textbox('getValue');
        if (vv == "") {
            layer.alert("company_name required！", {icon:5});
            return false;
        }

        var vv = $('#client_name').textbox('getValue');
        if (vv == "") {
            layer.alert("client_name required！", {icon:5});
            return false;
        }

        var web_url = $.trim($('#web_url').textbox('getValue'));
        if (web_url == '') {layer.alert('<?= lang('请填写公司网址！');?>',{icon:5}); return;}

        $('#role').val($('#role1').combobox('getValues'));
        var vv = $('#role1').combobox('getValues');
        if (vv == '') {
            layer.alert("<?= lang('角色必填');?>", {icon:5});
            return false;
        }

        var vv = $('#company_address').val();
        if (vv == "") {
            layer.alert("<?= lang('公司地址必填');?>", {icon:5});
            return false;
        }
        var default_partner_tips = false;
        $.each($('.default_partner'), function (index, item) {
            var id = $(this).attr('ids');
            var val = $(this).combotree('getValues');
            $('#' + id).val(val);
            if ($.inArray(id, default_partner_request) !== -1 && val == '' && id != 'customer_service_ids') {
                default_partner_tips = id;
            }
        });
        if (default_partner_tips) {
            layer.alert(default_partner_tips + " required！", {icon:5});
            return false;
        }
        return true;
    }

    var default_partner_request = [];

    //查找相似度
    function search_similar() {
        var company_name = $('#company_name').textbox('getValue');
        if (company_name === '') {
            $.messager.alert('<?= lang('提示');?>', '<?= lang('先填写全称');?>');
            return;
        }
        ajaxLoading();
        is_search = true;
        $.ajax({
            type: 'GET',
            url: '/biz_client/search?company_name=' + company_name,
            success: function (res_json) {
                ajaxLoadEnd();
                var res;
                try {
                    res = $.parseJSON(res_json);
                } catch (e) {

                }
                if (res == undefined) {
                    $.messager.alert('<?= lang('提示');?>', res);
                } else {
                    if (res.code == 0) {
                        var status = res.data.client_level==0?'<?= lang('停用');?>':'<?= lang('正常');?>';
                        var msg = res.msg + '<br />' + '<?= lang('最接近的为“{company_name}”,相似度为{percent}%，状态：{status}', array('company_name' => "' + res.data.company_name + '", 'percent' => "' + res.data.percent + '", 'status' => "'+status + '"));?>';
                        layer.alert(msg);
                    } else {
                        layer.alert('<?= lang('提示');?>', res.msg);
                    }

                }
            },
        });
    }

    $(function () {
        //2022-06-28 获取选中tab tb
        <?php if(isset($_GET['tab_title']) && !empty($_GET['tab_title'])):?>
        $("#tb").tabs('select', '<?=$_GET['tab_title'];?>');
        <?php endif;?>

        ////实现表格下拉框的分页和筛选--start
        //var html = '<div id="qcc_searchTb">';
        //html += '<form id="qcc_searchform">';
        //html += '<div style="padding-left: 5px;display: inline-block;"><label><?//= lang('公司名称');?>//:</label><input name="keyword" id="qcc_keyword" class="easyui-textbox to_search"style="width:200px;"data-options="prompt:\'text\'"/></div>';
        //
        //html += '<div style="padding-left: 1px; display: inline-block;"><form>';
        //html += '<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:\'icon-search\'" id="qcc_query"><?//= lang('search');?>//</a>';
        //html += '</div>';
        //html += '</div>';
        //$('body').append(html);
        //$.parser.parse('#qcc_searchTb');
        //$('#qcc_search').combogrid("reset");
        //$('#qcc_search').combogrid({
        //    panelWidth: '700px',
        //    panelHeight: '300px',
        //    prompt: '<?//= lang('企查查更精确');?>//',
        //    // multiple:true,
        //    idField: 'name',              //ID字段
        //    textField: 'name',    //显示的字段
        //    fitColumns: true,
        //    striped: true,
        //    editable: false,
        //    pagination: true,           //是否分页
        //    pageList: [20, 50, 100],
        //    pageSize: 20,
        //    toolbar: '#qcc_searchTb',
        //    rownumbers: true,           //序号
        //    collapsible: true,         //是否可折叠的
        //    method: 'get',
        //    columns: [[
        //        {field: 'Name', title: '<?//= lang('Name');?>//', width: 250},
        //        {field: 'CreditCode', title: '<?//= lang('CreditCode');?>//', width: 150},
        //        //{field: 'OperName', title: '<?//= lang('OperName');?>//', width: 150},
        //        //{field: 'No', title: '<?//= lang('No');?>//', width: 150},
        //    ]],
        //    emptyMsg: '未找到相应数据!',
        //    onBeforeLoad: function (param) {
        //        if (Object.keys(param).length < 3) {
        //            return false;
        //        }
        //    },
        //    onSelect: function (index, row) {
        //        var load = layer.load(1);
        //        is_qcc_search = true;
        //        $.ajax({
        //            type: 'post',
        //            url: '/biz_client/get_last_shipment/',
        //            data: {company_name:row.Name},
        //            dataType: 'json',
        //            async: true,
        //            success:function (data) {
        //                layer.close(load);
        //                if (data.code == 0){
        //                    var msg = row.Name + data.msg + '记录，当前处于保护状态，不能使用该抬头！';
        //                    layer.alert(msg, {icon:5});
        //                }else{
        //                    var this_val = row.Name;
        //                    //选中时给下面的赋值
        //                    var input = $('#company_name');
        //                    input.textbox('setValue', this_val);
        //                    //自动查询选择的抬头
        //                    $('#search_client').attr('src', '/biz_crm/clue_analysis/?company_name=' + this_val + '&crm_id=<?//=$id;?>//');
        //                    //选中时给下面的赋值
        //                    var input = $('#taxpayer_id');
        //                    // var this_val = row.No;
        //                    var this_val = row.CreditCode;
        //                    input.textbox('setValue', this_val);
        //                    //选中时给下面的赋值
        //                    var input = $('#taxpayer_name');
        //                    input.textbox('setValue', this_val);
        //                    //自动选择中国
        //                    var input = $('#country_code');
        //                    input.combobox('setValue', 'CN');
        //                }
        //            }
        //        });
        //    },
        //    onLoadSuccess: function (data) {
        //        var opts = $(this).combogrid('grid').datagrid('options');
        //        var vc = $(this).combogrid('grid').datagrid('getPanel').children('div.datagrid-view');
        //        vc.children('div.datagrid-empty').remove();
        //        if (!$(this).combogrid('grid').datagrid('getRows').length) {
        //            var d = $('<div class="datagrid-empty"></div>').html(opts.emptyMsg || 'no records').appendTo(vc);
        //            d.css({
        //                position: 'absolute',
        //                left: 0,
        //                top: 50,
        //                width: '100%',
        //                fontSize: '14px',
        //                textAlign: 'center'
        //            });
        //        }
        //    },
        //    onShowPanel: function () {
        //        //展开面板时
        //        var company_name = $('#company_name').textbox('getValue');
        //        var form_data = {
        //            'keyword': company_name,
        //        };
        //        $('#qcc_searchform').form('load', form_data);
        //        $('#qcc_query').trigger('click');
        //    },
        //    url: '/api_qcc/GetList_1027',
        //});
        ////点击搜索
        //$('#qcc_query').click(function () {
        //    var where = {};
        //    var form_data = $('#qcc_searchform').serializeArray();
        //    $.each(form_data, function (index, item) {
        //        if (where.hasOwnProperty(item.name) === true) {
        //            if (typeof where[item.name] == 'string') {
        //                where[item.name] = where[item.name].split(',');
        //                where[item.name].push(item.value);
        //            } else {
        //                where[item.name].push(item.value);
        //            }
        //        } else {
        //            where[item.name] = item.value;
        //        }
        //    });
        //    $('#qcc_search').combogrid('grid').datagrid('load', where);
        //});
        ////text添加输入值改变
        //$('.to_search').textbox('textbox').keydown(function (e) {
        //    if (e.keyCode == 13) {
        //        $('#qcc_query').trigger('click');
        //    }
        //});

        $('#tb').tabs({
            border: false,
            onSelect: function (title) {
                if (title == "<?= lang('联系人');?>") {
                    if (document.getElementById("contact_list").src == "") {
                        document.getElementById("contact_list").src = "/biz_client_contact/index_crm?crm_id=<?= $id;?>";
                    }
                }
                //if (title == "<?= lang('每日跟进记录');?>") {
                //    if (document.getElementById("follow_up_list").src == "") {
                //        document.getElementById("follow_up_list").src = "/biz_client_follow_up_log/index/?crm_id=<?//= $id;?>//";
                //    }
                //}
            }

        });

        //异步写入数据
        $('#save_data').click(function () {
            if(checkvalue() == true) {
                var load = layer.load(1);
                var field = {};
                $.each($('#myform').serializeArray(), function (index, item) {
                    field[item.name] = item.value;
                });

                $.ajax({
                    url: '/biz_crm/save_data/?id=<?=$id;?>',
                    type: 'POST',
                    data: field,
                    dataType: 'json',
                    success: function (data) {
                        layer.close(load);
                        if (data.code == 1) {
                            layer.alert('<?= lang('保存成功。');?>', {icon:1}, function () {
                                location.reload();
                            });
                        } else {
                            layer.alert('<?= lang('保存失败：');?>' + data.msg, {icon:5});
                        }
                    },
                    error: function (xhr, status, error) {
                        layer.close(load);
                        layer.alert('<?= lang('网络请求失败！');?>', {icon:5});
                    }
                });
            }
        });

        //CRM申请
        $('#crm_approval').click(function () {
            layer.confirm('<?= lang('确认提交给领导审批吗？');?>',{icon:3}, function () {
                var load = layer.load(1);
                $.ajax({
                    url: '/biz_crm/create_crm_approval/?crm_id=<?=$id;?>',
                    type: 'get',
                    dataType: 'json',
                    success: function (data) {
                        layer.close(load);
                        if (data.code == 1) {
                            $('#crm_approval').hide();
                            layer.alert('<?= lang('申请提交成功，请等待领导审批。');?>', {icon:1}, function () {
                                location.reload();
                            });
                        } else {
                            layer.alert('<?= lang('申请失败：');?>' + data.msg, {icon:5});
                        }
                    },
                    error: function (xhr, status, error) {
                        layer.close(load);
                        layer.alert('<?= lang('网络请求失败！');?>', {icon:5});
                    }
                });
            });
        });

        var load = layer.load(1, {offset: ['200px', '850px']});
        $('#search_client').on('load',function () {
            layer.close(load);
        })
    });
</script>
<style type="text/css">
    input:disabled, select:disabled, textarea:disabled {
        background-color: #d6d6d6;
        cursor: default;
    }
    .form_table {float: left;width: 520px;}
    .form_table tr td:first-child {padding-left: 15px; color: #023e62}
</style>
<body>
<!--添加标签-->
<div id="add_tags_div" class="easyui-window" style="width:450px;"
     data-options="title:'<?= lang('CRM添加标签');?>',modal:true,closed:true,height:'auto',top:200">
    <div class="add_tags_body">
        <h3><?= lang('自由添加标签');?></h3>
        <div class="add_tags_body_input_div" style="display: block;">
            <input type="text" class="add_tags_body_input" id="tag_name" autocomplete="off"
                   placeholder="<?= lang('创建或搜索添加新话题...');?>" style="margin-bottom: 0px;">
            <select class="easyui-combobox" id="tag_class" style="background-color: #fff; height: 35px; width: 100px"
                    data-options="
                    valueField:'value',
                    textField:'name',
                    mode:'remote',
                    url:'/bsc_dict/get_option/tag_class',
                     loadFilter:function(data){
                        for (var i = 0; i < data.length; i++) {
                            //判断品牌信息
                            if (data[i].name == '') {
                                data.splice(i,1);
                            }
                        }
                        console.log(data);
                        return data;
                    }
                "
            ></select>
            <a href="javascript:void(0);" onclick="apply_tag()" class="add_tags_body_input_btn" style="margin-bottom: 0px;"><?= lang('申请添加');?></a>
        </div>
        <div class="system_tag_box">

        </div>
    </div>
</div>
<div id="tags_tab" class="easyui-accordion" data-options="multiple:true"
     style="width:300px;position: absolute;right: 38px;top: 10px;z-index: 999;">
    <style type="text/css">
        .topic-tag {
            display: inline-block
        }

        .topic-tag .text {
            display: inline-block;
            height: 16px;
            line-height: 16px;
            padding: 2px 5px;
            background-color: #99cfff;
            font-size: 12px;
            color: #fff;
            border-radius: 4px;
            cursor: pointer;
        }

        .topic-tag {
            margin: 0 5px 2px 0;
        }

        .topic-tag .text:hover, .topic-tag .close:hover {
            background-color: #339dff;
        }

        .this_tag .topic-tag .text, .role_tag .topic-tag .text {
            border-radius: 4px 0 0 4px;
            float: left;
        }

        .this_tag .topic-tag .close, .role_tag .topic-tag .close {
            float: left;
        }

        .topic-tag-apply .text {
            background-color: #cccccc;
        }

        .topic-tag .close {
            width: 20px;
            height: 20px;
            background-color: #66b7ff;
            text-align: center;
            line-height: 20px;
            color: #fff;
            font-size: 10px;
            opacity: 1;
            filter: alpha(opacity=100);
            border-radius: 0 4px 4px 0;
            display: inline-block;
            text-shadow: 0 1px 0 #fff;
            cursor: pointer;
        }

        .add_tags_body_input {
            display: inline-block;
            vertical-align: middle;
            width: 200px !important;
            margin: 0 5px 10px 0;
            padding: 6px;
            resize: none;
            box-shadow: none;
            height: 22px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            float: left;
        }

        .add_tags_body_type {
            display: inline-block;
            vertical-align: middle;
            width: 100px !important;
            margin: 0 5px 10px 0;
            padding: 6px;
            resize: none;
            box-shadow: none;
            height: 36px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
            float: left;
        }

        .add_tags_body_input_btn:hover {
            background-color: rgb(22, 155, 213);
        }

        .add_tags_body_input_btn {
            margin: 0 10px 10px 0;
            border: none !important;
            background-color: rgb(94, 180, 214);
            color: #fff;
            min-width: 76px;
            height: 34px;
            padding: 0 10px;
            line-height: 34px;
            font-size: 14px;
            display: inline-block;
            font-weight: 400;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border-radius: 4px;
            box-sizing: border-box;
        }

        .add_tags_body h3 {
            margin-top: 20px;
            color: #333;
            font-size: 100%;
            line-height: 1.7;
            margin-bottom: 10px;
        }

        .add_tags_body {
            padding: 10px;
        }
    </style>
    <script type="text/javascript">
        //添加标签
        function add_tags() {
            $('#add_tags_div').window('open');
        }

        //申请标签
        function apply_tag() {
            var tag_name = $('#tag_name').val().trim();
            if (tag_name == '') {
                $.messager.alert('<?= lang('提示');?>', '<?= lang('标签内容不能为空');?>');
                return false;
            }
            var tag_class = $('#tag_class').combobox('getValue');
            if (tag_class == '') {
                $.messager.alert('<?= lang('提示');?>', '<?= lang('请选择标签分类');?>');
                return false;
            }
            var post_data = {};
            post_data.tag_name = tag_name;
            post_data.tag_class = tag_class;
            post_data.id_type = 'biz_client_crm';
            post_data.id_no = $('#id').textbox('getValue');
            submit_add_tag(post_data, function (res) {
                $.messager.alert('<?= lang('提示');?>', res.msg);
                if (res.code == 0) {
                    $('#tag_name').val('');
                    var pass = res.data.is_pass;
                    add_tag_html(post_data.tag_name, tag_class, 'system_tag', pass);
                    add_tag_html(post_data.tag_name, tag_class, 'this_tag', pass);
                } else {

                }
            });
        }

        //点击添加标签
        function click_add_tag(e) {
            var tag_name = $(e).text();
            var tag_class = $(e).parents('.topic-bar').attr('tag_class');
            var post_data = {};
            post_data.tag_name = tag_name;
            post_data.tag_class = tag_class;
            post_data.id_type = 'biz_client_crm';
            post_data.id_no = $('#id').textbox('getValue');
            submit_add_tag(post_data, function (res) {
                if (res.code == 0) {
                    var pass = res.data.is_pass;
                    add_tag_html(post_data.tag_name, tag_class, 'this_tag', pass);
                } else {
                    $.messager.alert('<?= lang('提示');?>', res.msg);
                }
            });
        }

        /**
         * 添加一个tag标签
         * @param tag_name 标签名
         * @param tag_class 标签类
         * @param tag_type 标签类型, 目前有this_tag 右上角的,system_tag 系统推荐标签
         * @param pass 是否通过,没通过的是灰色
         */
        function add_tag_html(tag_name, tag_class, tag_type, pass = 0) {
            var span_class = "topic-tag";
            if (pass == 0) span_class += ' topic-tag-apply';

            if (tag_type === 'this_tag') {
                //如果不存在,生成一个
                if ($(".this_tag[tag_class='" + tag_class + "']").length == 0) {
                    var topic_bar_html = "<div class=\"topic-bar clearfix this_tag\" tag_class=\"" + tag_class + "\" style=\"margin: 0;\"><h3>" + tag_class + "</h3></div>";
                    $('.this_tag_box').append(topic_bar_html);
                }

                var tag_html = "<span class=\"" + span_class + "\">\n" +
                    "                    <a class=\"text\">" + tag_name + "</a>\n" +
                    "<a class=\"close\" onclick=\"click_del_tag(this)\">X</a>" +
                    "                </span>";
                $(".this_tag[tag_class='" + tag_class + "']").append(tag_html);
            } else if (tag_type === 'system_tag') {
                //如果不存在,生成一个
                if ($(".system_tag[tag_class='" + tag_class + "']").length == 0) {
                    var topic_bar_html = "<div class=\"topic-bar clearfix system_tag\" tag_class=\"" + tag_class + "\" style=\"margin: 0;\"><h3>" + tag_class + "</h3></div>";
                    $('.system_tag_box').append(topic_bar_html);
                }

                var tag_html = "<span class=\"" + span_class + "\">\n" +
                    "                    <a class=\"text\" onclick=\"click_add_tag(this)\">" + tag_name + "</a>\n" +
                    "                </span>";
                $(".system_tag[tag_class='" + tag_class + "']").append(tag_html);
            } else if (tag_type === 'role_tag') {
                //如果不存在,生成一个
                if ($(".role_tag[tag_class='" + tag_class + "']").length == 0) {
                    var topic_bar_html = "<div class=\"topic-bar clearfix role_tag\" tag_class=\"" + tag_class + "\" style=\"margin: 0;\"></div>";
                    $('.role_tag_box').append(topic_bar_html);
                }

                var tag_html = "<span class=\"" + span_class + "\">\n" +
                    "                    <a class=\"text\" onclick=\"click_role_open('" + tag_name + "')\" role=\"" + tag_name + "\">" + tag_name + "</a>\n" +
                    "<a class=\"close\" onclick=\"click_del_role(this)\" role=\"" + tag_name + "\">X</a>" +
                    "                </span>";
                $(".role_tag[tag_class='" + tag_class + "']").append(tag_html);
            }
        }

        //提交方法
        function submit_add_tag(post_data, fn) {
            ajaxLoading();
            $.ajax({
                type: 'POST',
                url: '/biz_tag/add_tag',
                data: post_data,
                success: function (res_json) {
                    ajaxLoadEnd();
                    try {
                        var res = $.parseJSON(res_json);
                        fn(res);
                    } catch (e) {
                        $.messager.alert('<?= lang('提示');?>', res_json);
                    }
                },
                error: function (e) {
                    ajaxLoadEnd();
                    $.messager.alert('<?= lang('提示');?>', e.responseText);
                }
            });
        }

        //点击删除当前tag
        function click_del_tag(e) {
            var tag_name = $(e).prev().text();
            var tag_class = $(e).parents('.topic-bar').attr('tag_class');
            var post_data = {};
            post_data.tag_name = tag_name;
            post_data.tag_class = tag_class;
            post_data.id_type = 'biz_client_crm';
            post_data.id_no = $('#id').textbox('getValue');
            ajaxLoading();
            $.ajax({
                type: 'POST',
                url: '/biz_tag/del_this_tag',
                data: post_data,
                success: function (res_json) {
                    ajaxLoadEnd();
                    try {
                        var res = $.parseJSON(res_json);
                        var topic_tag = $(e).parents('.topic-bar').children('.topic-tag');
                        if (topic_tag.length === 1) $(e).parents('.topic-bar').remove();
                        else $(e).parents('.topic-tag').remove();
                        //判断当前tag下是否还有其他标签, 如果一个都没了,tag_class整个删除
                    } catch (e) {
                        $.messager.alert('<?= lang('提示');?>', res_json);
                    }
                },
                error: function (e) {
                    ajaxLoadEnd();
                    $.messager.alert('<?= lang('提示');?>', e.responseText);
                }
            });
        }

        //删除role角色
        function click_del_role(e) {
            //去除标签,且去除role里的值即可
            var role = $(e).attr('role');

            $('#role1').combobox('unselect', role);
            $(e).parents('.topic-tag').remove();
        }

        //打开client 角色维护权限窗口
        function click_role_open(role) {
            // var role = $(e).attr('role');
            var client_code = $('#client_code').textbox('getValue');
            $('#window_iframe').css({
                width: 600,
                height: 500,
            }).attr('src', '/biz_client_duty/index?client_code=' + client_code + '&client_role=' + role);
            $('#window').window({
                title: role + '<?= lang('角色权限');?>',
                width: '615',
                height: '539',
                top: 200,
                modal: true
            });
        }

        //加载当前tag
        function load_tag() {
            var id_no = $('#id').textbox('getValue');
            //分为2部分,1是当前的tag,2是公共模板的tag
            $.ajax({
                type: 'GET',
                url: '/biz_tag/get_tag?id_type=biz_client_crm&id_no=' + id_no,
                success: function (res_json) {
                    ajaxLoadEnd();
                    try {
                        var res = $.parseJSON(res_json);
                        if (res.code == 0) {
                            //系统推荐的
                            $.each(res.data.system_tag, function (i, it) {
                                $.each(it.data, function (i2, it2) {
                                    add_tag_html(it2.tag_name, it.tag_class, 'system_tag', it2.pass);
                                });
                            });

                            //当前的
                            $.each(res.data.this_tag, function (i, it) {
                                $.each(it.data, function (i2, it2) {
                                    add_tag_html(it2.tag_name, it.tag_class, 'this_tag', it2.pass);
                                });
                            })
                        } else {
                            $.messager.alert('<?= lang('提示');?>', res_json);
                        }
                    } catch (e) {
                        $.messager.alert('<?= lang('提示');?>', res_json);
                    }
                },
                error: function (e) {
                    ajaxLoadEnd();
                    $.messager.alert('<?= lang('提示');?>', e.responseText);
                }
            });
        }

        $(function () {
            load_tag();
            $('#tag_class').combobox('setValue', '<?= lang('销售类');?>');
        });
    </script>
    <div title="Tags" style="padding:10px;">
        <a href="javascript:void(0)" onclick="add_tags()"><?= lang('添加标签');?></a>
        <br>-------------------------------------
        <br><br>
        <div class="this_tag_box">
        </div>
    </div>
</div>
<?php if ($apply_status == 4):?>
    <div style="position: fixed; bottom: 300px; left: 300px; z-index: 99999;"><img src="/inc/image/blacklist.png" width="200px"></div>
<?php endif;?>
<div id="tb" class="easyui-tabs">
    <div title="Basic info" style="padding:1px; overflow: auto">
        <form id="myform" name="f" method="post" onsubmit="return false;">
            <!--基础信息-->
            <div style="padding: 20px 20px 20px 30px; background: #f6f8f5; height: 800px; width: 450px; float: left; border-right:1px solid #eee;">
                <div data-options="region:'west',tools:'#tt'" title="Basic">
                    <div style="display: inline-block;vertical-align: top">
                        <table class="form_table" style="padding:10px; width: 450px; overflow:hidden;" border="0" cellspacing="0">
                            <?php
                            if (isset($import_name) && $import_name != ''):
                                ?>
                                <tr style="height: 35px;">
                                    <td><?= lang('导入线索'); ?> </td>
                                    <td style="color: #d33d03;" class="easyui-tooltip" title="<?= lang('导入备注：');?><br><?= $remark; ?>">
                                        <?= $import_name; ?>
                                    </td>
                                </tr>
                            <?php
                            endif;
                            ?>
                            <tr id="qcc_tr" style="height: 35px;">
                                <td style="width: 80px;"><?= lang('country_code'); ?></td>
                                <td>
                                    <select class="easyui-combobox" required readonly="" name="country_code" id="country_code"
                                            style="width:300px; height: 30px;" data-options="
                                    value:'<?= $country_code; ?>',
                                    valueField:'value',
                                    textField:'namevalue',
                                    url:'/bsc_dict/get_option/country_code',
                                    onHidePanel: function() {
										var valueField = $(this).combobox('options').valueField;
										var val = $(this).combobox('getValue');
										var allData = $(this).combobox('getData');
										var result = true;
										for (var i = 0; i < allData.length; i++) {
											if (val == allData[i][valueField]) {
												result = false;
											}
										}
										if (result) {
											$(this).combobox('clear');
										}
								    }
                                ">
                                    </select>
                                </td>
                            </tr>
                            <tr style="height: 35px;">
                                <td><?= lang('company_name'); ?> </td>
                                <td>
                                    <input class="easyui-textbox" <?php if ($country_code=='CN') echo 'readonly';?> name="company_name" id="company_name"
                                           style="width:300px; height: 30px;" value="<?= $company_name; ?>"/>
                                </td>
                            </tr>
                            <tr style="height: 35px;" id="company_name_en_tr">
                                <td><?= lang('英文全称'); ?> </td>
                                <td>
                                    <input class="easyui-textbox" name="company_name_en" id="company_name_en"
                                           style="width:300px;height: 30px;" value="<?= $company_name_en; ?>"
                                           data-options="
                                        onChange: function(newValue, oldValue){
                                            $('#company_name_en').textbox('setValue', newValue.replace(/[^\w|\d|' '|'.']/ig,''));
                                        }
                                   "/>
                                </td>
                            </tr>
                            <tr style="height: 35px;">
                                <td><?= lang('client_name'); ?> </td>
                                <td>
                                    <input class="easyui-textbox" name="client_name" id="client_name"
                                           data-options="required:true" style="width:300px; height: 30px;"
                                           value="<?= $client_name; ?>"/>
                                </td>
                            </tr>
                            <tr style="height: 35px;">
                                <td><?= lang('客户网址'); ?> </td>
                                <td>
                                    <input class="easyui-textbox" name="web_url" id="web_url"
                                           data-options="" style="width:300px;height: 30px;"
                                           value="<?= $web_url; ?>"/>
                                </td>
                            </tr>
                            <!--<tr style="height: 35px;">-->
                            <!--    <td><?= lang('client area'); ?></td>-->
                            <!--    <td>-->
                            <!--        <select class="easyui-combobox" name="province" id="province" data-options="-->
                            <!--    valueField:'cityname',-->
                            <!--    textField:'cityname',-->
                            <!--    url:'/city/get_province',-->
                            <!--    value:'<?=$province; ?>',-->
                            <!--    onSelect: function(rec){-->
                            <!--        if(rec != undefined){-->
                            <!--            $('#city').combobox('reload', '/city/get_city/' + rec.id);-->
                            <!--        }-->
                            <!--    }," style="width:100px; height: 30px;"></select>-->

                            <!--        <select class="easyui-combobox" name="city" id="city" style="width:98px; height: 30px;"-->
                            <!--                data-options="-->
                            <!--        valueField:'cityname',-->
                            <!--        textField:'cityname',-->
                            <!--        value:'<?=$city; ?>',-->
                            <!--        onSelect: function(rec){-->
                            <!--            if(rec != undefined){-->
                            <!--                $('#area').combobox('reload', '/city/get_area/' + rec.id);-->
                            <!--            }-->
                            <!--        },-->
                            <!--        onLoadSuccess:function(){-->
                            <!--            var this_data = $(this).combobox('getData');-->
                            <!--            var this_val = $(this).combobox('getValue');-->
                            <!--            var rec = this_data.filter(el => el['cityname'] === this_val);-->
                            <!--            if(rec.length > 0)rec = rec[0];-->
                            <!--            $('#area').combobox('reload', '/city/get_area/' + rec.id);-->
                            <!--        }-->
                            <!--    "></select>-->

                            <!--        <select class="easyui-combobox" name="area" id="area" style="width:95px; height: 30px;"-->
                            <!--                data-options="-->
                            <!--        valueField:'cityname',-->
                            <!--        textField:'cityname',-->
                            <!--        value:'<?= $area; ?>',-->
                            <!--    "></select>-->
                            <!--    </td>-->
                            <!--</tr>-->
                            <tr style="height: 35px;">
                                <td><?= lang('company_address'); ?></td>
                                <td>
                                <textarea style="width:295px; height: 80px;" class="textarea b" name="company_address"
                                          id="company_address"><?= $company_address ?></textarea>
                                </td>
                            </tr>
                            <!--<tr style="height: 35px;">-->
                            <!--    <td><?= lang('export_sailing'); ?></td>-->
                            <!--    <td>-->
                            <!--        <select <?php echo in_array("export_sailing", $G_userBscEdit) ? '' : 'disabled'; ?>-->
                            <!--            class="easyui-combobox" id="export_sailing1" style="width:300px; height: 30px;"-->
                            <!--            data-options="-->
                            <!--        editable:false,-->
                            <!--        multiple:true,-->
                            <!--        multivalue:false,-->
                            <!--        valueField:'value',-->
                            <!--        textField:'name',-->
                            <!--        value:'<?php echo $export_sailing; ?>',-->
                            <!--        url:'/bsc_dict/get_option/export_sailing',-->
                            <!--        onChange:function(newValue,oldValue){-->
                            <!--            $('#export_sailing').val(newValue.join(','));-->
                            <!--        }-->
                            <!--    ">-->
                            <!--        </select>-->
                            <!--        <input type="hidden" name="export_sailing" id="export_sailing"-->
                            <!--               value="<?php echo $export_sailing; ?>">-->
                            <!--    </td>-->
                            <!--</tr>-->
                            <!--<tr style="height: 35px;">-->
                            <!--    <td><?= lang('trans_mode'); ?></td>-->
                            <!--    <td>-->
                            <!--        <select <?php echo in_array("trans_mode", $G_userBscEdit) ? '' : 'disabled'; ?>-->
                            <!--            class="easyui-combobox" id="trans_mode1" style="width:300px; height: 30px;" data-options="-->
                            <!--        editable:false,-->
                            <!--        multiple:true,-->
                            <!--        multivalue:false,-->
                            <!--        valueField:'value',-->
                            <!--        textField:'name',-->
                            <!--        value:'<?php echo $trans_mode; ?>',-->
                            <!--        url:'/bsc_dict/get_option/client_trans_mode',-->
                            <!--        onChange:function(newValue,oldValue){-->
                            <!--            $('#trans_mode').val(newValue.join(','));-->
                            <!--        }-->
                            <!--    ">-->
                            <!--        </select>-->
                            <!--        <input <?php echo in_array("trans_mode", $G_userBscEdit) ? '' : 'disabled'; ?>-->
                            <!--            type="hidden" name="trans_mode" id="trans_mode"-->
                            <!--            value="<?php echo $trans_mode; ?>">-->
                            <!--    </td>-->
                            <!--</tr>-->
                            <!--<tr style="height: 35px;">-->
                            <!--    <td><?= lang('product_type'); ?></td>-->
                            <!--    <td>-->
                            <!--        <select <?php echo in_array("product_type", $G_userBscEdit) ? '' : 'disabled'; ?>-->
                            <!--            class="easyui-combobox" id="product_type1" style="width:300px; height: 30px;" data-options="-->
                            <!--        editable:false,-->
                            <!--        multiple:true,-->
                            <!--        multivalue:false,-->
                            <!--        valueField:'value',-->
                            <!--        textField:'name',-->
                            <!--        value:'<?php echo $product_type; ?>',-->
                            <!--        url:'/bsc_dict/get_option/product_type',-->
                            <!--        onChange:function(newValue,oldValue){-->
                            <!--            $('#product_type').val(newValue.join(','));-->
                            <!--        }-->
                            <!--    ">-->
                            <!--        </select>-->
                            <!--        <input <?php echo in_array("product_type", $G_userBscEdit) ? '' : 'disabled'; ?>-->
                            <!--            type="hidden" name="product_type" id="product_type"-->
                            <!--            value="<?php echo $product_type; ?>">-->
                            <!--    </td>-->
                            <!--</tr>-->
                            <tr style="height: 35px;">
                                <td><?= lang('product_details'); ?></td>
                                <td>
                                <textarea <?php echo in_array("product_details", $G_userBscEdit) ? '' : 'disabled'; ?> name="product_details"
                                                                                                                       id="product_details"
                                                                                                                       style="width:298px; height: 80px;"><?php echo $product_details; ?></textarea>
                                </td>
                            </tr>
                            <!--<tr style="height: 35px;">-->
                            <!--    <td><?= lang('client_source'); ?></td>-->
                            <!--    <td>-->
                            <!--        <select <?php echo in_array("client_source", $G_userBscEdit) ? '' : 'disabled'; ?>-->
                            <!--            class="easyui-combobox" id="client_source" name="client_source" style="width:300px; height: 30px;" data-options="-->
                            <!--        editable:false,-->
                            <!--        multivalue:false,-->
                            <!--        valueField:'value',-->
                            <!--        textField:'name',-->
                            <!--        value: '<?php echo $client_source; ?>',-->
                            <!--        url:'/bsc_dict/get_option/client_source'-->
                            <!--    ">-->
                            <!--        </select>-->
                            <!--    </td>-->
                            <!--</tr>-->
                            <tr style="height: 35px;">
                                <td><?= lang('role'); ?> </td>
                                <td>
                                    <select id="role1"
                                            class="easyui-combobox" <?php echo in_array("role", $G_userBscEdit) ? '' : 'disabled'; ?>
                                            editable="false" name="role" style="width:300px; height: 30px;" data-options="required:true,
    								valueField:'value',
    								textField:'value',
    								url:'/bsc_dict/get_option/role?ext1=1',
    								value:'<?php echo $role; ?>',
								">
                                    </select>
                                    <a href="javascript:void;" class="easyui-tooltip" data-options="
                                    content: $('<div></div>'),
                                    onShow: function(){
                                        $(this).tooltip('arrow').css('left', 20);
                                        $(this).tooltip('tip').css('left', $(this).offset().left);
                                    },
                                    onUpdate: function(cc){
                                        cc.panel({
                                            width: 500,
                                            height: 'auto',
                                            border: false,
                                            href: '/bsc_help_content/help/role'
                                        });
                                    }
                                "><?= lang('help'); ?></a>
                                </td>
                            </tr>
                            <tr style="height: 35px;">
                                <td><?= lang('审批人');?></td>
                                <td>
                                    <select class="easyui-combobox" name="approve_user" readonly="true"
                                            id="approve_user" style="width:300px; height: 30px;" data-options="
                                                required:true,
                                                editable:false,
                                                valueField:'id',
                                                textField:'name',
                                                url:'/bsc_user/get_leader_user',
                                                value:'<?=$approve_user;?>'
                                            ">
                                    </select>
                                </td>
                            </tr>
                            <tr style="height: 35px;">
                                <td><?= lang('sales'); ?></td>
                                <td>
                                    <select class="easyui-combotree default_partner" id="sales_ids1" readonly="true"
                                            ids="sales_ids" name="sales_id" style="width:300px; height: 30px;" data-options="
                                    multiple:false,
                                    valueField:'id',
                                    textField:'text',
                                    cascadeCheck: false,
                                    url:'/bsc_user/get_user_tree/sales/true',
                                    value: '<?= $sales_id; ?>',
                                ">
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <!--查询往来单中同名客户-->
            <div style="padding: 10px 0px 10px 20px; width: 690px; float: left; overflow: hidden">
                <iframe src="/biz_crm/clue_analysis/?company_name=<?=$company_name;?>&crm_id=<?=$id;?>" id="search_client" style="width:690px;height:600px; border:none;"></iframe>
            </div>
            <!--底部按钮-->
            <div id="save_tools"
                 style="width: 100%; text-align: center; padding: 20px 0px; border-top:1px solid #588c98; position: fixed; z-index: 999; bottom: 0px; background-color:#f6f8f5;">

                <?php
                //如果三个月内没有交易过则允许操作
                if ($is_allow) {
                    echo '<button class="layui-btn" id="save_data" style=" width: 300px; ">' . lang('提交保存') . '</button>';
                    if ($apply_status == 1 && (empty($workflow) || $workflow['id_type'] != 1)) {
                        //if(状态==申请中 &&（没有当前记录的审批事务||有事务但事务类型不是资料审核）)
                        // echo '<button class="layui-btn layui-btn-normal" id="crm_approval">申请审核</button>';
                    } elseif ($apply_status == 1 && !empty($workflow) && $workflow['id_type'] == 1) {
                        //if(状态==申请中 &&（没有当前记录的审批事务||有事务但事务类型是资料审核）)
                        echo '<span style="margin-left: 50px; color: #088fff;">' . lang('当前状态：申请审核中...') . '</span>';
                    } elseif ($apply_status == 2 && (empty($workflow) || $workflow['id_type'] != 2)) {
                        //if(状态==资料审核通过 &&（没有当前记录的审批事务||有事务但事务类型不是转正申请）)
                        // echo '<a class="layui-btn layui-btn-normal easyui-tooltip" title="<span style=color:red;>' . lang('提示： 成交单子的客户，由销售申请转正，领导进行审批，进入正式往来单位库！') . '</span>" href="/biz_add_client_affairs/add_client_affairs/?client_type=trade_client&source=crm&id=' . $id . '">' . lang('申请转正') . '</a>';
                    } elseif ($apply_status == 3) {
                        //if(状态==转正申请中)
                        echo '<span style="margin-left: 50px; color: #088fff;">' . lang('当前状态：转正申请中...') . '</span>';
                    } elseif ($apply_status == 0) {
                        //if(状态==已转正)
                        echo '<span style="margin-left: 50px; color: #0c8941;">' . lang('当前状态：已转正') . '</span>';
                    }
                }else{
                    echo '<span style="margin-left: 50px; color: #d30100;">' . lang('Tips:该客户在3个月内产生过交易，正处于保护状态，不能进行任何操作！') . '</span>';
                }
                ?>
                </button>
            </div>
        </form>
        <input value="<?=$id;?>" id="id" name="id" class="easyui-textbox" type="hidden">
    </div>
    <div title="<?= lang('联系人');?>" style="padding:1px">
        <iframe scrolling="auto" frameborder="0" id="contact_list" style="width:100%;height:750px;"></iframe>
    </div>
<!--    <div title="--><?//= lang('每日跟进记录');?><!--" style="padding:1px">-->
<!--        <iframe scrolling="auto" frameborder="0" id="follow_up_list" style="width:100%;height:750px;"></iframe>-->
<!--    </div>-->
    <div title="<?= lang('相关线索');?>" style="padding:1px">
        <iframe scrolling="auto" frameborder="0" src="/biz_crm/clue_detail/?company_search=<?=$company_search;?>&clue_pure_text=<?=$company_search_en;?>" id="contact_list" style="width:100%;height:625px;"></iframe>
    </div>
</div>
</body>