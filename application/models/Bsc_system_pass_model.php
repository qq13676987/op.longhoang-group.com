<?php

class Bsc_system_pass_model extends CI_Model
{
	private $table = 'bsc_system_pass';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->chinadb = $this->db;
	}

	public function get_one($f = "", $v = "")
	{
		$this->chinadb->where($f, $v);
		$query = $this->chinadb->get($this->table);
		return $query->row_array();
	}

	public function get_where_one($where = "")
	{
		if ($where == '') return array();
		$this->chinadb->where($where);
		$query = $this->chinadb->get($this->table);
		return $query->row_array();
	}

	public function get($where = "", $sort = "id", $order = "desc")
	{
		if ($where != "") $this->chinadb->where($where);
		$this->chinadb->order_by($sort, $order);
		$query = $this->chinadb->get($this->table);
		return $query->result_array();
	}

	public function total($where = "")
	{
		if ($where != "") $this->chinadb->where($where);
		$this->chinadb->from($this->table);
		return $this->chinadb->count_all_results('');
	}

	public function save($data = array())
	{
		$this->chinadb = $this->load->database('china',true);
		$data["created_time"] = date('Y-m-d H:i:s', time());
		$this->chinadb->insert($this->table, $data);
		return $this->chinadb->insert_id();
	}

	public function update($id = '', $data = array())
	{
		$this->chinadb = $this->load->database('china',true);
		$this->chinadb->where('id', $id);
		$this->chinadb->update($this->table, $data);
		return $id;
	}

	public function mdelete($id = '')
	{
		$this->chinadb = $this->load->database('china',true);
		// delete container
		$this->chinadb->where('id', $id);
		$this->chinadb->delete($this->table);
	}
}
