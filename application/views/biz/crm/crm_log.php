<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= lang('审批过程');?></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/inc/third/layui/css/layui.css" media="all">
    <script type="text/javascript" src="/inc/js/easyui/jquery.min.js"></script>
    <style>
        .layui-timeline-item::before, hr {
            background-color: #ee8015;
        }
        .layui-timeline-item::before {
            top: 3px;
        }
        .layui-timeline-axis {
            left: -2px;
            top: 3px;
            width: 15px;
            height: 15px;
            line-height: 15px;
            background-color: #ee8015;
            color: #ee8015;
        }
    </style>
</head>
<body>
<?php if ($data):?>
    <div class="layui-card">
        <div class="layui-card-body">
            <ul class="layui-timeline">
                <?php foreach ($data as $key => $val): ?>
                    <li class="layui-timeline-item">
                        <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                        <div class="layui-timeline-content layui-text">
                            <?php if ($key == 0):?>
                            <h3 class="layui-timeline-title" style="color: #d33d03">
                                <?php else: ?>
                                <h3 class="layui-timeline-title" style="color: #4c5052">
                                    <?php endif;?>
                                    <?=date("Y-m-d", $val['create_time']);?>
                                </h3>
                                <p style="margin: 5px 0px; line-height: 30px;">
                                    <?= lang('操作');?>：<span class="layui-badge <?=$key==0?'layui-bg-blue':'layui-badge-rim';?>" style="margin-right: 20px;"><?=$val['operation_name'];?></span>
                                    <?= lang('操作时间');?>：<span class="layui-badge <?=$key==0?'layui-bg-blue':'layui-badge-rim';?>" style="margin-right: 20px;"><?=date('Y-m-d H:i:s', $val['create_time']);?></span>
                                    <?= lang('销售');?>：<span class="layui-badge <?=$key==0?'layui-bg-blue':'layui-badge-rim';?>" style="margin-right: 20px;"><?=$val['sales_name']?$val['sales_name']:lang('无');?></span>
                                </p>
                                <fieldset class="layui-elem-field">
                                    <legend style="font-size: 12px;"><?= lang('操作内容');?></legend>
                                    <div class="layui-field-box" style="color:#ee6204">
                                        <?=$val['remark'];?>
                                    </div>
                                </fieldset>
                        </div>
                    </li>
                <?php
                endforeach;
                ?>
                <li></li>
            </ul>
        </div>
    </div>
<?php else:?>
    <!--无权操作-->
    <div style="margin: 100px auto 20px;width: 95px">
        <i class="layui-icon layui-icon-face-surprised" style="font-size: 30px; color: #1E9FFF;font-size: 100px;"></i>
    </div>
    <div style="margin: 0px auto 150px;width: 200px; text-align: center; color: #303334; font-size: 16px;">
        <span><?= lang('暂无日志内容！');?></span>
    </div>
<?php endif;?>
<script type="text/javascript" src="/inc/third/layui/layui.js"></script>
<script type="text/javascript">
    layui.use(['element']);
</script>
</body>
</html>
