<?php


class excel extends Common_Controller
{
    protected $allowed_types = array(
      '.xlsx','.xls', '.csv'
    );
//    protected $cellName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ');
    protected $cellName = array();
    protected $is_trim = false;

    public function __construct()
    {
        parent::__construct(); 
        $this->load->model('m_model');

        $letter = 'A';
        while ($letter !== 'AAA') {
            $this->cellName[] = $letter++;
        }
    }

    /**
     * 文件类型验证
     */
    private function file_type_verify($file){
        $allowed_types = $this->allowed_types;
        $file_type = strrchr($file['name'],'.');

        if (in_array(strtolower($file_type),$allowed_types)){
            return true;
        }else{
            return false;
        }
    }

    private function data_handle($savePath, $file_name, $type = ''){
        $result = array();
        if($type == 'container_info'){
            $result = $this->container_info_Import($savePath, $file_name);
        }
        return $result;
    }

    /**
     * 箱数据导出 恋恋心情版
     */
    private function container_info_Import($savePath, $file_name)
    {
        //配置字段对应行等 下标: [0]字段名 [1] 字段类型，用于特殊处理，无限制 [2] 用于校验表格字段名是否相等 [3]是否参与新增或修改
        $excel_configs = array(
            array('container_no', 'string', '箱号',1),//箱号
            array('seal_no', 'string', '封号',1),//封号
            array('vgm', 'string', 'VGM',1),//封号
            array('container_size', 'string', '箱型',1),//箱型
            array('trans_mode', 'string', '箱状态',0),//箱状态
            array('cus_no', 'string', '提单号',0),//提单号
            array('packs', 'string', '件数',1),//件数
            array('weight', 'string', '毛重',1),//毛重
            array('volume', 'string', '体积',1),//体积
        );

        $highestColumn = sizeof($excel_configs);

        //单独提取配置里的值
        $adopt_cnname = array_column($excel_configs,2);

        $sheet_data = $this->getExcel($savePath, $file_name, $highestColumn);
        $datas = array();
        foreach ($sheet_data as $sheet){
            //校验中文是否对应
            $br = false;

            foreach ($sheet as $key => $row){

                $i = 0;
                if($br){
                    break;

                }

                if ($key == 0) {
                    foreach ($row as $rkey => $val) {
                        if (strtoupper($val) != $adopt_cnname[$i]) {
                            $br = true;
                            break;
                        }
                        $i++;
                    }
                }else{
                    $data = array();
                    foreach ($excel_configs as $eckey => $excel_config) {
                        $eci = $eckey + 1;
                        if($excel_config[3] == 1){
                            $$excel_config[0] = trim($row[$this->cellName[$eckey]], " ");
                            if ($excel_config[1] == 'date') {
                                $$excel_config[0] = date('Y-m-d', strtotime($$excel_config[0]));
                            } else if ($excel_config[1] == 'remark') {
                                $$excel_config[0] = str_replace("'", '’', $$excel_config[0]);
                            }
                            $data[$excel_config[0]] = $$excel_config[0];
                        }
                    }
                    if(sizeof(array_filter($data)) > 0){
                        $data['container_tare'] = 0;
                        // $data['vgm'] = 0;
                        $datas[] = $data;
                    }
                }
            }
        }
        return $datas;
    }

    /**
     * 上传excel
     * @param string $type
     */
    public function upload_excel($type = ''){
        if (get_session('id') == '') return error(1, lang('请登录后再试'));

        if($_FILES['file']['error']){
            switch($_FILES['file']['error']){
                case 1:	// UPLOAD_ERR_INI_SIZE
                    $error = 'upload_file_exceeds_limit';
                    break;
                case 2: // UPLOAD_ERR_FORM_SIZE
                    $error = 'upload_file_exceeds_form_limit';
                    break;
                case 3: // UPLOAD_ERR_PARTIAL
                    $error = 'upload_file_partial';
                    break;
                case 4: // UPLOAD_ERR_NO_FILE
                    $error = 'upload_no_file_selected';
                    break;
                case 6: // UPLOAD_ERR_NO_TMP_DIR
                    $error = 'upload_no_temp_directory';
                    break;
                case 7: // UPLOAD_ERR_CANT_WRITE
                    $error = 'upload_unable_to_write_file';
                    break;
                case 8: // UPLOAD_ERR_EXTENSION
                    $error = 'upload_stopped_by_extension';
                    break;
                default :   $error = 'upload_no_file_selected';
                    break;
            }
            echo json_encode(array('code' => 3, 'msg' => $error));
            die;
        }
        //接收的excel信息
        $info = pathinfo($_FILES['file']['name']);
        $file = $_FILES['file'];
        //验证excel文件类型
        $res = $this->file_type_verify($file);
        if(!$res) return error(1, lang('该文件类型不允许上传'));

        $new_name = strstr($file['name'], '.', true) . '-' . time() . rand(1000,9999) .  '.' . $info['extension'];
        $root_path = dirname(BASEPATH);

        $path = date('Ymd');
        $new_dir = $root_path . '/upload/temp/' . $path . '/';
        if (!file_exists($new_dir)) {
            mkdir($new_dir, 0777, true);
        }
        $new_path = $new_dir;
        $new_path = str_replace('\\', DIRECTORY_SEPARATOR, $new_path);
        $new_path = str_replace('/', DIRECTORY_SEPARATOR, $new_path);

        $_FILES['file']['tmp_name'] = iconv("UTF-8","gb2312", $_FILES['file']['tmp_name']);
        $res = move_uploaded_file($_FILES['file']['tmp_name'], $new_path . $new_name);
        if (!$res) {
            echo json_encode(array('code' => 2,'msg' => '上传失败', 'result' => array()));
        }else{
            $result = $this->data_handle($new_path, $new_name,$type);
            echo json_encode(array('code' => 0,'msg' => '上传成功', 'result' => $result));
        }
    }

    /**
     * 导出excel
     */
    public function export_excel(){
        $file = isset($_GET['file']) ? $_GET['file'] : '';
        ini_set('memory_limit', '1024M');
        if($file == 'container_info'){
            $this->container_info();
        }else if($file == 'chuanka_diy'){
            $this->chuanka_diy();
        }else if($file == 'tc_lrb_new'){
            //这个是新版的, 不处理数据了, 直接输出下表格
            $this->tc_lrb_new();
        }else if($file == 'tc_leader_lrb_new'){
            $this->tc_leader_lrb_new();
        }else if($file == 'cktz_hw1_v2'){
            $this->cktz_hw(0);
        }else if($file == 'cktz_hw2_v2'){
            $this->cktz_hw(1);
        }
    }
    
    private function tc_lrb_new(){
        $datas_json = postValue('datas', "{}");
        $user_role = isset($_POST['user_role']) ? $_POST['user_role'] : '';
        // if($user_role !== "sales") exit("角色需要选销售");
        $post_datas = json_decode($datas_json, true);

        if(!is_array($post_datas)){
            echo '参数错误';
            return;
        }
        if(isset($post_datas['fields']) && !is_array($post_datas['fields'])){
            echo '参数错误';
            return;
        }
        if(isset($post_datas['datas']) && !is_array($post_datas['datas'])){
            echo '参数错误';
            return;
        }
        $fields = $post_datas['fields'];
        $datas = $post_datas['datas'];
        $ext_data = $post_datas['ext_data'];
        $styler = array();
        $expTableData = array();

        //首先后面统一插入4个固定的列 XX提成比例 XX提成金额 助理提成金额 助理 提成比例
        $fields[] = array('field' => 'is_bujiti', 'width' => '80', 'title' => lang('不计提'));//提成比例
        $fields[] = array('field' => 'ts_client', 'width' => '80', 'title' => lang('特殊客户'));//提成比例
        $fields[] = array('field' => 's_czf', 'width' => '80', 'title' => lang('操作费'));//提成比例
        $fields[] = array('field' => 'commision_rate', 'width' => '80', 'title' => lang('{user_role}提成比例', array('user_role' => lang($user_role))));//提成比例
        $fields[] = array('field' => 'commision_amount', 'width' => '80', 'title' => lang('{user_role}提成金额', array('user_role' => lang($user_role))));//提成金额
        $fields[] = array('field' => 'assistants_commision_rate', 'width' => '80', 'title' => lang('助理提成比例'));//助理提成比例
        $fields[] = array('field' => 'assistants_commision_amount', 'width' => '80', 'title' => lang('助理提成金额'));//助理提成金额

        $c_i = 2;
        //开始填充数据
        //转化表头值对应
        foreach ($fields as $key => $val) {
            //转换下表头格式
            //这个是 easyui 的列名存在时, 给 对应的格子列名赋值
            if(isset($val['title']))$expTableData[$this->cellName[$key] . '1'] = $val['title'];
            if(sizeof($datas) > 0 && isset($datas[0][$val['field']])){
                $c_i = 2;
                //给对应的列赋值
                foreach ($datas as $dkey => $data) {
                    //2022-04-07 不计提的标红
                    if($data['is_bujiti'] === '1'  && $data['gross_profit'] >= 0) $styler[$this->cellName[$key] . $c_i] = array('font' => array('color' => array('rgb' => 'ff3333')));
                    if (isset($data[$val['field']])) {
                        if($val['field'] == 'is_bujiti'){
                            if($data[$val['field']] == '1') $data[$val['field']] = "√";
                            else $data[$val['field']] = "";
                        }
                        if($val['field'] == 'ts_client'){
                            if($data[$val['field']]) $data[$val['field']] = "√";
                            else $data[$val['field']] = "";
                        }
                        $expTableData[$this->cellName[$key] . $c_i] = $data[$val['field']];
                        $c_i++;
                    }
                }
            }
        }

        $c_i++;
        $total_col = 4;//输出在这一列
        $gross_profit_col = 13;//总金额输出在这一列
        //开始输出提成信息
        if(isset($ext_data['tc_text'])){
            $expTableData[$this->cellName[$total_col - 1] . $c_i] = '总提成';
            $expTableData[$this->cellName[$total_col] . $c_i] = $ext_data['tc_text'];
            $c_i++;
        }
        //下一行开始输入

        $this->load->library('PHPExcel/PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $objActSheet = $objPHPExcel->getActiveSheet();
        $end_column = $this->cellName[sizeof($fields) - 1];

        $expTitle = date('Y-m-d') . '提成利润表';

        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allborders' => array( //设置全部边框
                    'style' => \PHPExcel_Style_Border::BORDER_THIN //粗的是thick
                ),
            ),
        );

        foreach ($fields as $key => $field){
            if(isset($field['width'])){
                $objPHPExcel->getActiveSheet()->getColumnDimension($this->cellName[$key])->setWidth(($field['width'] - 5) / 8);
            }
            $objActSheet->setCellValue($this->cellName[$key] . '1', $field['field']);
        }
        // Miscellaneous glyphs, UTF-8

        foreach ($expTableData as $key => $val){
            $objActSheet->setCellValue($key, $val);
        }
        $objPHPExcel->getActiveSheet()->getStyle('A1:' . $end_column . $c_i)->applyFromArray($styleThinBlackBorderOutline);

        //设置样式
        foreach ($styler as $key => $style){
            foreach ($style as $s_key => $val){
                $objPHPExcel->getActiveSheet()->getStyle($key)->applyFromArray($style);
            }
        }
        ob_end_clean();
        header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition:inline;filename=" . $expTitle . ".xlsx");//attachment新窗口打印inline本窗口打印
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }
    
        private function tc_leader_lrb_new(){
        $datas_json = postValue('datas', "{}");
        $user_role = isset($_POST['user_role']) ? $_POST['user_role'] : '';
        // if($user_role !== "sales") exit("角色需要选销售");
        $post_datas = json_decode($datas_json, true);

        if(!is_array($post_datas)){
            echo '参数错误';
            return;
        }
        if(isset($post_datas['fields']) && !is_array($post_datas['fields'])){
            echo '参数错误';
            return;
        }
        if(isset($post_datas['datas']) && !is_array($post_datas['datas'])){
            echo '参数错误';
            return;
        }
        $fields = $post_datas['fields'];
        $datas = $post_datas['datas'];
        $ext_data = $post_datas['ext_data'];
        $styler = array();
        $expTableData = array();

        //首先后面统一插入4个固定的列 XX提成比例 XX提成金额 助理提成金额 助理 提成比例
        $fields[] = array('field' => 'is_bujiti', 'width' => '80', 'title' => lang('不计提'));//提成比例
        $fields[] = array('field' => 'commision_rate', 'width' => '80', 'title' => lang('提成比例', array('user_role' => lang($user_role))));//提成比例
        $fields[] = array('field' => 'commision_amount', 'width' => '80', 'title' => lang('提成金额', array('user_role' => lang($user_role))));//提成金额

        $c_i = 2;
        //开始填充数据
        //转化表头值对应
        foreach ($fields as $key => $val) {
            //转换下表头格式
            //这个是 easyui 的列名存在时, 给 对应的格子列名赋值
            if(isset($val['title']))$expTableData[$this->cellName[$key] . '1'] = $val['title'];
            if(sizeof($datas) > 0 && isset($datas[0][$val['field']])){
                $c_i = 2;
                //给对应的列赋值
                foreach ($datas as $dkey => $data) {
                    //2022-04-07 不计提的标红
                    if($data['is_bujiti'] === '1'  && $data['gross_profit'] >= 0) $styler[$this->cellName[$key] . $c_i] = array('font' => array('color' => array('rgb' => 'ff3333')));
                    if (isset($data[$val['field']])) {
                        if($val['field'] == 'is_bujiti'){
                            if($data[$val['field']] == '1') $data[$val['field']] = "√";
                            else $data[$val['field']] = "";
                        }
                        $expTableData[$this->cellName[$key] . $c_i] = $data[$val['field']];
                        $c_i++;
                    }
                }
            }
        }

        $c_i++;
        $total_col = 4;//输出在这一列
        $gross_profit_col = 13;//总金额输出在这一列
        //开始输出提成信息
        if(isset($ext_data['tc_text'])){
            $expTableData[$this->cellName[$total_col - 1] . $c_i] = '团队提成';
            $expTableData[$this->cellName[$total_col] . $c_i] = $ext_data['tc_text'];
            $c_i++;
        }
        //下一行开始输入

        $this->load->library('PHPExcel/PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $objActSheet = $objPHPExcel->getActiveSheet();
        $end_column = $this->cellName[sizeof($fields) - 1];

        $expTitle = date('Y-m-d') . '团队的提成利润表';

        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allborders' => array( //设置全部边框
                    'style' => \PHPExcel_Style_Border::BORDER_THIN //粗的是thick
                ),
            ),
        );

        foreach ($fields as $key => $field){
            if(isset($field['width'])){
                $objPHPExcel->getActiveSheet()->getColumnDimension($this->cellName[$key])->setWidth(($field['width'] - 5) / 8);
            }
            $objActSheet->setCellValue($this->cellName[$key] . '1', $field['field']);
        }
        // Miscellaneous glyphs, UTF-8

        foreach ($expTableData as $key => $val){
            $objActSheet->setCellValue($key, $val);
        }
        $objPHPExcel->getActiveSheet()->getStyle('A1:' . $end_column . $c_i)->applyFromArray($styleThinBlackBorderOutline);

        //设置样式
        foreach ($styler as $key => $style){
            foreach ($style as $s_key => $val){
                $objPHPExcel->getActiveSheet()->getStyle($key)->applyFromArray($style);
            }
        }
        ob_end_clean();
        header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition:inline;filename=" . $expTitle . ".xlsx");//attachment新窗口打印inline本窗口打印
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }

    /**
     * 船卡1 恋恋心情版
     */
    private function container_info()
    {
        $this->load->library('PHPExcel/PHPExcel.php');

        $root_path = dirname(BASEPATH);

        $template = $root_path . '/upload/excel/container_info.xlsx';

        $objPHPExcel = PHPExcel_IOFactory::load($template);     //加载excel文件,设置模板

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);  //设置保存版本格式
        $objActSheet = $objPHPExcel->getActiveSheet();

        $shipment_id = isset($_POST['shipment_id']) ? intval($_POST['shipment_id']) : 0;
        $consol_id = isset($_POST['consol_id']) ? intval($_POST['consol_id']) : 0;
        $page = isset($_POST['shipment_id']) ? intval($_POST['shipment_id']) : 0;
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "id";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        //-------这个查询条件想修改成通用的 -------------------------------
        $f1 = 'shipment_id';
        $s1 = '=';
        $v1 = "$shipment_id";
        $f2 = 'container_no';
        $s2 = 'like';
        $v2 = isset($_REQUEST['v2']) ? ($_REQUEST['v2']) : '';
        $v2 = "%$v2%";
        $where = "";
        if ($v1 != "") $where = "$f1 $s1 '$v1'";
        if ($v2 != "" && $where != "") $where .= " and $f2 $s2 '$v2'";
        //-----------------------------------------------------------------
        Model('biz_shipment_container_model');
        $result["total"] = $this->biz_shipment_container_model->total($where);
        $this->db->select('*,(select container_size from biz_container where container_no = biz_shipment_container.container_no AND biz_container.consol_id = \'' . $consol_id . '\' limit 1) as container_size,(select seal_no from biz_container where container_no = biz_shipment_container.container_no AND biz_container.consol_id = \'' . $consol_id . '\' limit 1) as seal_no');
        $rs = $this->biz_shipment_container_model->get($where, $sort, $order);

        //获取shipment
        $shipment = Model('biz_shipment_model')->get_duty_one("id = '{$shipment_id}'");
        $trans_mode = '';
        if($shipment['trans_mode'] == 'FCL'){
            $trans_mode = 'F';
        }else if($shipment['trans_mode'] == 'LCL'){
            $trans_mode = 'L';
        }
        //获取每个操作的权限，根据当前用户，进行合并可查看字段

        $datas = array();
        foreach ($rs as $row) {
            array_push($datas, $row);
        }

        $data_setting = array();

        //循环获取箱型--start
        $c_i = 2;
        $this->load->model('bsc_user_model');
        $this->load->model('biz_client_model');
        foreach ($datas as $data){
            $data_setting['A' . $c_i] = $data['container_no'];//箱号
            $data_setting['B' . $c_i] = $data['seal_no'];//封号
            $data_setting['C' . $c_i] = $data['vgm'];//箱型
            $data_setting['D' . $c_i] = $data['container_size'];//箱型
            $data_setting['E' . $c_i] = $trans_mode;//提单号
            $data_setting['F' . $c_i] = $shipment['cus_no'];//提单号
            $data_setting['G' . $c_i] = $data['packs'];//件数
            $data_setting['H' . $c_i] = $data['weight'];//毛重
            $data_setting['I' . $c_i] = $data['volume'];//体积
            $c_i++;
            if($c_i>100)exit("条数太多");
        }
        //循环获取箱型--end

        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allborders' => array( //设置全部边框
                    'style' => \PHPExcel_Style_Border::BORDER_THIN //粗的是thick
                ),
            ),
        );

        $objPHPExcel->getActiveSheet()->getStyle('A2:I' . $c_i)->applyFromArray($styleThinBlackBorderOutline);

        // Miscellaneous glyphs, UTF-8
        foreach ($data_setting as $key => $val){
            $objActSheet->setCellValue($key, $val);
        }

        $expTitle = $shipment['job_no'] . '箱信息';
        ob_end_clean();
        header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition:attachment;filename=" . $expTitle . ".xlsx");//attachment新窗口打印inline本窗口打印
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $objWriter->save('php://output');
    }

    /**
     * 获取数据并以表格的形式保存
     * @param array $filed  二维数组   字段分为length(长度) 与name(字段名)，title（描述）
     * @param array $data
     */
    private function putExcel($expTitle,$expCellName,$expTableData, $style){
        $xlsTitle = iconv('utf-8', 'gb2312', $expTitle);//文件名称

        $cellNum = count($expCellName);
        $dataNum = count($expTableData);

        if($dataNum > 30000){
            echo '<script>alert(\'导出数据不能超过三万条以上\')</script>';
            return;
        }

        $this->load->library('PHPExcel/PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $cellName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');

        $objPHPExcel->getActiveSheet(0)->mergeCells('A1:'.$cellName[$cellNum-1].'1');//合并单元格
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $expTitle.'  导出时间:'.date('Y-m-d H:i:s'));

        $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        for($i=0;$i<$cellNum;$i++){
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i].'2', $expCellName[$i][1]);
        }

        // Miscellaneous glyphs, UTF-8
        $is_total = false;
        $total_arr = array();
        for($i=0;$i<$dataNum;$i++){
            for($j=0;$j<$cellNum;$j++){
                $objPHPExcel->getActiveSheet()->getStyle($cellName[$j])->applyFromArray($style);
                $objPHPExcel->getActiveSheet()->getColumnDimension($cellName[$j])->setWidth($expCellName[$j][2]);
                if($expCellName[$j][3] == 'float'){
                    /*设置G金额字段格式为2位小数点*/
                    $objPHPExcel->getActiveSheet()->getStyle ($cellName[$j])->getNumberFormat()->setFormatCode ("0.00");
                    $is_total = true;
                    $total_arr[$cellName[$j]] = isset($total_arr[$cellName[$j]]) ? $total_arr[$cellName[$j]] + $expTableData[$i][$expCellName[$j][0]] : $expTableData[$i][$expCellName[$j][0]];
                }
                $objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j].($i+3), $expTableData[$i][$expCellName[$j][0]]);
            }
        }
        //合计状态
        if($is_total){
            $objPHPExcel->getActiveSheet(0)->setCellValue('A' . ($i +3), '合计');
            //循环获取->getFormattedValue()
            foreach ($total_arr as $takey => $taval){
                $objPHPExcel->getActiveSheet(0)->setCellValue($takey.($i+3), $taval);
            }
        }
        ob_end_clean();
        header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition:attachment;filename=" . $xlsTitle . ".xlsx");//attachment新窗口打印inline本窗口打印
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }

    protected function get_data_where_v2()
    {
        //-------这个查询条件想修改成通用的 -------------------------------
        $shipment_no = isset($_REQUEST['shipment_no']) ? trim($_REQUEST['shipment_no']) : '';
        $goods_type = isset($_REQUEST['goods_type']) ? trim($_REQUEST['goods_type']) : '';
        $is_hyf = isset($_REQUEST['is_hyf']) ? (int)$_REQUEST['is_hyf'] : '';
        $lock_lv = isset($_REQUEST['lock_lv']) ? $_REQUEST['lock_lv'] : array();
        $container_no = isset($_REQUEST['container_no']) ? trim($_REQUEST['container_no']) : '';
        $is_apply_shipment = isset($_REQUEST['is_apply_shipment']) ? (int)$_REQUEST['is_apply_shipment'] : 0;
        $operator_si = isset($_REQUEST['operator_si']) ? $_REQUEST['operator_si'] : '';
        $document_si = isset($_REQUEST['document_si']) ? $_REQUEST['document_si'] : '';

        $where = array();
        Model('m_model');
        if ($goods_type != '') $where[] = "biz_consol.id in (select consol_id from biz_shipment where biz_shipment.goods_type = '{$goods_type}')";
        if ($is_hyf !== 0) {
            if ($is_hyf === 1) {
                $where[] = "biz_consol.sign like '%hyf_qr%'";
            } else if ($is_hyf === 2) {
                $where[] = "biz_consol.sign not like '%hyf_qr%'";
            }
        }
        if ($operator_si !== '') {
            if ($operator_si === '0') $where[] = "biz_consol.operator_si = '0'";
            if ($operator_si === '1') $where[] = "biz_consol.operator_si != '0'";
        }
        if ($document_si !== '') {
            if ($document_si === '0') $where[] = "biz_consol.document_si = '0'";
            if ($document_si === '1') $where[] = "biz_consol.document_si != '0'";
        }
        if ($shipment_no != "") {
            Model('biz_shipment_model');
            $shipment = $this->biz_shipment_model->get_one_all("job_no like '%$shipment_no%'");
            if (empty($shipment)) $shipment['consol_id'] = 0;
            $where[] = "id = {$shipment['consol_id']}";
        }
        //2021-06-03 加入查询是否申请consol
        if ($is_apply_shipment == 1) {
            $where[] = "(select consol_id_apply from biz_shipment where biz_shipment.consol_id_apply = biz_consol.id limit 1) != 0";
        }

        if ($lock_lv !== array()) {
            $lock_lv_where = array();
            if (in_array(0, $lock_lv)) $lock_lv_where[] = 'biz_consol.lock_lv = 0';//C0
            if (in_array(1, $lock_lv)) $lock_lv_where[] = 'biz_consol.lock_lv = 1';//
            if (in_array(2, $lock_lv)) $lock_lv_where[] = "biz_consol.lock_lv = 2";//
            if (in_array(3, $lock_lv)) $lock_lv_where[] = "biz_consol.lock_lv = 3";//C3
            if (!empty($lock_lv_where)) {
                $where[] = '(' . join(' OR ', $lock_lv_where) . ' )';
            }
        };//查询锁

        //查询箱号
        if ($container_no !== '') {
            Model('biz_container_model');
            $this->db->select('consol_id');
            $containers = $this->biz_container_model->get("container_no like '%$container_no%'");
            if (!empty($containers)) {
                $container_consol_ids = array_column($containers, 'consol_id');
                $where[] = "biz_consol.id in ('" . join("','", $container_consol_ids) . "')";
            } else {
                $where[] = 'biz_consol.id = 0';
            }
        }

        return $where;
    }
    
        // 催款通知海外
    public function cktz_hw($is_special = 0){
        $this->load->library('PHPExcel/PHPExcel.php');

        $root_path = dirname(BASEPATH);

        $template = $root_path . '/upload/excel/cktz_hw.xlsx';

        $objPHPExcel = PHPExcel_IOFactory::load($template);     //加载excel文件,设置模板

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);  //设置保存版本格式
        $objActSheet = $objPHPExcel->getActiveSheet();

        $expTitle = 'SOA-' . date('Y-m-d H:i:s');
        $sort = isset($_POST['sort']) ? strval($_POST['sort']) : "currency";
        $order = isset($_POST['order']) ? strval($_POST['order']) : 'desc';

        $where = array();
        $this->where_v2($where, $sort);
        $where = join(' and ', $where);
        //-----------------------------------------------------------------
        $this->load->model('biz_bill_model');
        $this->db->select('sum(biz_bill.amount) as amount, biz_bill.type, biz_bill.currency, bbi.invoice_no, bbi.invoice_date, biz_bill.client_code' .
            ',IF(id_type = \'shipment\', (select (select carrier_ref from biz_consol where consol_id = id) from biz_shipment where id = biz_bill.id_no), (select carrier_ref from biz_consol where id = biz_bill.id_no)) as carrier_ref' .
            ',IF(id_type = \'shipment\', (select job_no from biz_shipment where id = biz_bill.id_no), (select job_no from biz_consol where id = biz_bill.id_no)) as job_no' .
            ',IF(id_type = \'shipment\', (select trans_origin from biz_shipment where id = biz_bill.id_no), (select trans_origin from biz_consol where id = biz_bill.id_no)) as trans_origin' .
            ',IF(id_type = \'shipment\', (select box_info from biz_shipment where id = biz_bill.id_no), (select box_info from biz_consol where id = biz_bill.id_no)) as box_info' .
            ',IF(id_type = \'shipment\', (select trans_destination from biz_shipment where id = biz_bill.id_no), (select trans_destination from biz_consol where id = biz_bill.id_no)) as trans_destination' .
            ',IF(id_type = \'shipment\', (select hbl_no from biz_shipment where id = biz_bill.id_no), \'\') as hbl_no' .
            ',IF(id_type = \'shipment\', (select trans_ATD from biz_consol where id = (select consol_id from biz_shipment where id = biz_bill.id_no)), (select trans_ATD from biz_consol where id = biz_bill.id_no)) as trans_ATD' .
            ',GROUP_CONCAT(account_no) as account_no' .
            ',biz_bill.id_no,biz_bill.id_type' .
            '', false);
        $this->db->group_by('type,currency, client_code,IF(id_type = \'shipment\', (select job_no from biz_shipment where id = biz_bill.id_no), (select job_no from biz_consol where id = biz_bill.id_no))');
        $this->db->limit(10001);
        $this->db->join('biz_bill_invoice bbi', 'bbi.id = biz_bill.invoice_id', 'left');
        $this->db->join('biz_bill_payment bbp', 'bbp.id = biz_bill.payment_id', 'left');
        $rs = $this->biz_bill_model->join_d_get($where, $sort, $order, $is_special);
        if(sizeof($rs) > 10000){
            echo '<script>alert(\'不能导出超过1W条\')</script>';
            exit();
        }

        //2024-04-14 加入了Volume 格子, 显示实际的箱体积
        $shipment_ids = filter_unique_array(array_column(array_filter($rs, function ($r){
            if($r['id_type'] == 'shipment') return true;
            else return false;
        }), 'id_no'));
        $consol_ids = filter_unique_array(array_column(array_filter($rs, function ($r){
            if($r['id_type'] == 'consol') return true;
            else return false;
        }), 'id_no'));

        if(!empty($shipment_ids)) {
            $sql = "select shipment_id,sum(volume) as volume" .
                " from biz_shipment_container where shipment_id in (" . join(',', $shipment_ids) . ")" .
                " GROUP BY shipment_id";
            unset($shipment_ids);
            $shipment_containers = array_column(Model("m_model")->query_array($sql), null, 'shipment_id');
        }

        if(!empty($consol_ids)){
            $sql = "select (select biz_shipment.consol_id from biz_shipment where biz_shipment.id = biz_shipment_container.shipment_id) as consol_id,sum(volume) as volume" .
                " from biz_shipment_container where (select biz_shipment.consol_id from biz_shipment where biz_shipment.id = biz_shipment_container.shipment_id) in (" . join(',', $consol_ids) . ")" .
                " GROUP BY (select biz_shipment.consol_id from biz_shipment where biz_shipment.id = biz_shipment_container.shipment_id)";
            unset($consol_ids);
            $consol_containers = array_column(Model("m_model")->query_array($sql), null, 'consol_id');
        }


        $bills = array();
        $sum_debit_amount = 0;
        $sum_credit_amount = 0;
        $this->load->model('biz_port_model');
        foreach ($rs as $row) {
            $row['debit'] = '0.00';
            $row['credit'] = '0.00';
            $row['account_no'] = join(',', filter_unique_array(explode(',', $row['account_no'])));
            if($row['type'] == 'cost'){
                $row['credit'] = $row['amount'];
                $sum_credit_amount += $row['amount'];
            }else{
                $row['debit'] = $row['amount'];
                $sum_debit_amount += $row['amount'];
            }
            $port = $this->biz_port_model->get_one('port_code', $row['trans_origin']);
            if(empty($port))$port['port_name'] = '';
            $row['trans_origin'] = $port['port_name'];

            $port = $this->biz_port_model->get_one('port_code', $row['trans_destination']);
            if(empty($port))$port['port_name'] = '';
            $row['trans_destination'] = $port['port_name'];

            $box_info = json_decode($row['box_info'], true);
            $row['box_info_str'] = array();
            foreach ($box_info as $br) {
                $row['box_info_str'][] = $br['size'] . '*' . $br['num'];
            }
            $row['box_info_str'] = join(', ', $row['box_info_str']);

            array_push($bills, $row);
        }

        $data_setting = array(
            'A12' => 'ETA',
            'B12' => 'JOBNO',
            'C12' => 'MB/L',
            'D12' => 'HB/L',
            'E12' => 'Volume',//2023-04-14 海外对账单加入了 体积列
            'F12' => 'Invoice No',
            'G12' => 'INV. DATE',
            'H12' => 'DEP',
            'I12' => 'DEST',
            'J12' => 'CUR',
            'K12' => 'DEBIT',
            'L12' => 'CREDIT',
            'M12' => 'Account No',
        );

        $fields = isset($_POST['field']) ? $_POST['field'] : array();
        if(empty($fields)){
            echo '<script>alert(\'参数不存在\')</script>';
            return;
        }
        //优先使用client_code
        // $client_code_key = array_search('bill.client_code', $fields['f']);

        // if($client_code_key !== false && $fields['v'][$client_code_key] !== ''){
        //     $client_code = $client_code_key !== false ? trim($fields['v'][$client_code_key]) : '';

        //     $this->load->model('biz_client_model');

        //     $client = $this->biz_client_model->get_where_one("client_code like '%$client_code%'");
        // }else{
        //     $client_code_name_key = array_search('bill.client_code_name', $fields['f']);
        //     $company_name = $client_code_name_key !== false ? trim($fields['v'][$client_code_name_key]) : '';

        //     $this->load->model('biz_client_model');

        //     $client = $this->biz_client_model->get_where_one("company_name like '%$company_name%'");
        // }
        $client_code = isset($bills[0]['client_code']) ? $bills[0]['client_code'] : '';
        $client = Model('biz_client_model')->get_where_one("client_code = '$client_code'");


        if(!empty($client)){
            $data_setting['B7'] = $client['company_name'];
            $data_setting['B8'] = $client['company_address'];
        }
        $data_setting['B9'] = date('Y-m-d H:i:s');

        //循环获取箱型--start
        $c_i = 13;
        foreach ($bills as $bill){
            $data_setting['A' . $c_i] = substr($bill['trans_ATD'],0,10);//ETD
            $data_setting['B' . $c_i] = $bill['job_no'];//JOBNO
            $data_setting['C' . $c_i] = $bill['carrier_ref'];//MB/L
            $data_setting['D' . $c_i] = $bill['hbl_no'];//HB/L
            $data_setting['E' . $c_i] = $bill['box_info_str'];//Volume
            $data_setting['F' . $c_i] = $bill['invoice_no'];//Invoice No
            $data_setting['G' . $c_i] = $bill['invoice_date'];//INV. DATE
            $data_setting['H' . $c_i] = $bill['trans_origin'];//DEP
            $data_setting['I' . $c_i] = $bill['trans_destination'];//DEST
            $data_setting['J' . $c_i] = $bill['currency'];//CUR
            $data_setting['K' . $c_i] = $bill['debit'];//DEBIT
            $data_setting['L' . $c_i] = $bill['credit'];//CREDIT
            $data_setting['M' . $c_i] = $bill['account_no'];//CREDIT
            $c_i++;
        }
        //循环获取箱型--end

        $objPHPExcel->getActiveSheet()->mergeCells('A' . $c_i . ':H' . $c_i);
        $objPHPExcel->getActiveSheet()->mergeCells('I' . $c_i . ':J' . $c_i);
        $data_setting['A' . $c_i] = 'Total:';
        $data_setting['I' . $c_i] = 'USD';
        $data_setting['K' . $c_i] = $sum_debit_amount;//DEBIT总金额
        $data_setting['L' . $c_i] = $sum_credit_amount;//CREDIT总金额
        $c_i++;
        $objPHPExcel->getActiveSheet()->mergeCells('A' . $c_i . ':H' . $c_i);
        $objPHPExcel->getActiveSheet()->mergeCells('I' . $c_i . ':L' . $c_i);
        $data_setting['A' . $c_i] = 'Balance:';
        $data_setting['I' . $c_i] = $sum_debit_amount - $sum_credit_amount;

        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allborders' => array( //设置全部边框
                    'style' => \PHPExcel_Style_Border::BORDER_THIN //粗的是thick
                ),
            ),
        );
        $objPHPExcel->getActiveSheet()->getStyle("A12:M12")->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getStyle('A12:M' . $c_i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet()->getStyle( 'A12:M' . $c_i)->applyFromArray($styleThinBlackBorderOutline);
        $objPHPExcel->getActiveSheet()->getStyle('A' . ($c_i - 1) . ':A' . ($c_i))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

        // Miscellaneous glyphs, UTF-8
        foreach ($data_setting as $key => $val){
            $objActSheet->setCellValue($key, $val);
        }
        ob_end_clean();
        header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition:attachment;filename=" . $expTitle . ".xlsx");//attachment新窗口打印inline本窗口打印
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $objWriter->save('php://output');
    }
    
    private function where_v2(&$where, &$sort){
        $title_data = get_user_title_sql_data('biz_bill', 'biz_bill_index');//获取相关的配置信息

        //需要处理的特殊排序字段
        $sort = $title_data['sql_field']["biz_bill.{$sort}"];

        //-------这个查询条件想修改成通用的 -------------------------------
        $user_role = isset($_REQUEST['user_role']) ? trim($_REQUEST['user_role']) : '';
        $user = isset($_REQUEST['user']) ? $_REQUEST['user'] : array();
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

        $currency = isset($_REQUEST['currency']) ? trim($_REQUEST['currency']) : '';
        $type = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
        $groups = isset($_REQUEST['group']) ? $_REQUEST['group'] : array();
        $group_role = isset($_REQUEST['group_role']) ? trim($_REQUEST['group_role']) : '';
        $commision_flag = isset($_REQUEST['commision_flag']) ? trim($_REQUEST['commision_flag']) : '';
        //如果有勾选,那么这里把ID传过来
        $ids = postValue('ids', '');

        Model('biz_bill_invoice_model');
        Model('biz_bill_payment_model');

        $where = array();
        if(!empty($ids))$where[] = "biz_bill.id in ($ids)";
        if ($currency != "") $where[] = "biz_bill.currency = '$currency'";
        if ($commision_flag != "") $where[] = "biz_bill.commision_flag = '$commision_flag'";
        if ($type !== "") {
            if($type == 1) {//未开票未销账
                $where[] = "biz_bill.invoice_id = 0 and biz_bill.payment_id = 0";
            }else if ($type == 2) {//已开票未销账
                $where[] = "biz_bill.invoice_id != 0 and biz_bill.payment_id = 0";
            }else if ($type == 3) {//未销账
                $where[] = "biz_bill.payment_id = 0";
            }else if ($type == 4) {//未开票已销账
                $where[] = "biz_bill.invoice_id = 0 and biz_bill.payment_id != 0";
            }else if ($type == 5) {//待审核
                // $this->db->select('id');
                // $no_lock_pm = $this->biz_bill_payment_model->get("lock_date = '0000-00-00'");
                // if(empty($no_lock_pm)) $no_lock_pm[] = -1;
                // $no_lock_pm_ids = array_column($no_lock_pm, 'id');
                // $where[] = "payment_id in (" . join(',', $no_lock_pm_ids) . ")";
                $where[] = "biz_bill.payment_id in (select id from biz_bill_payment where biz_bill_payment.lock_date = '0000-00-00')";
            }else if ($type == 6) {//审核完成
                // $this->db->select('id');
                // $no_lock_pm = $this->biz_bill_payment_model->get("lock_date != '0000-00-00'");
                // if(empty($no_lock_pm)) $no_lock_pm[] = -1;
                // $no_lock_pm_ids = array_column($no_lock_pm, 'id');
                // $where[] = "payment_id in (" . join(',', $no_lock_pm_ids) . ")";
                $where[] = "biz_bill.payment_id in (select id from biz_bill_payment where biz_bill_payment.lock_date != '0000-00-00')";
            }
        }
        $user = join("','", array_filter($user));
        if ($user_role != "" && !empty($user))
        {
            $where[] = Model('bsc_user_role_model')->get_role_v3('biz_bill.id_type', 'biz_bill.id_no', array(), "biz_duty_new.user_role = '{$user_role}' and biz_duty_new.user_id in ('{$user}')");
            
            // $where[] = "((biz_bill.id_no in (select id_no from biz_duty_new where biz_duty_new.biz_table = 'biz_shipment' and biz_duty_new." . $user_role . "_id in ('$user')) and biz_bill.id_type = 'shipment') " .
            //     "or (biz_bill.id_no in (select id_no from biz_duty_new where biz_duty_new.biz_table = 'biz_consol' and biz_duty_new." . $user_role . "_id in ('$user')) and biz_bill.id_type = 'consol'))";
        }
        $groups = join("','", array_filter($groups));
        if($group_role != "" && !empty($groups)){
            $where[] = Model('bsc_user_role_model')->get_role_v3('biz_bill.id_type', 'biz_bill.id_no', array(), "biz_duty_new.user_role = '{$user_role}' and biz_duty_new.user_group in ('{$groups}')");
            // $where[] = "((biz_bill.id_no in (select id_no from biz_duty_new where biz_duty_new.biz_table = 'biz_shipment' and biz_duty_new." . $group_role . "_group in ('$groups')) and biz_bill.id_type = 'shipment') " .
            //     "or (biz_bill.id_no in (select id_no from biz_duty_new where biz_duty_new.biz_table = 'biz_consol' and biz_duty_new." . $group_role . "_group in ('$groups')) and biz_bill.id_type = 'consol'))";
        }
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                $v_arr = array_unique(explode("\r\n", $v));
                if($f == '') continue;
                $v_arr = array_filter($v_arr, function ($r){
                    $r = trim($r);
                    if($r === "") return false;
                    else return true;
                });
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }
                //这里 job_no 和mbl 单独处理
                $f_arr = explode('.', $f);
                if($f_arr[0] == 'consol'){
                    if(!empty($v_arr)){
                        if($f_arr[1] == 'creditor'){
                            $this_where = array();
                            foreach ($v_arr as $vv){
                                $this_where[] = search_like('company_search', $s, match_chinese($vv));
                            }
                            $this_where = join(' or ', $this_where);
                            $clients = Model('biz_client_model')->no_role_get($this_where);
                            $client_codes = join("','", array_column($clients, 'client_code'));
                            $this_where = "creditor in ('$client_codes')";
                        }else{
                            $this_where = array();
                            foreach ($v_arr as $vv){
                                if($f_arr[1] == 'trans_ATD'){
                                    if($s == '>=') $vv = $vv . ' 00:00:00';
                                    if($s == '<=') $vv = $vv . ' 23:59:59';
                                }
                                $this_where[] = search_like($f_arr[1], $s, $vv);
                            }
                            $this_where = join(' or ', $this_where);
                        }

                        $this->db->select('id');
                        $consols = Model('biz_consol_model')->no_role_get($this_where);
                        $consols_ids = join(',', array_column($consols, 'id'));
                        if(empty($consols_ids)){
                            $consols_ids = 0;
                            $shipments_ids = 0;
                        }else{
                            $this->db->select('id');
                            $shipments = Model('biz_shipment_model')->no_role_get("consol_id in ($consols_ids)");
                            $shipments_ids = join(',', array_column($shipments, 'id'));
                            if(empty($shipments_ids)){
                                $shipments_ids = 0;
                            }
                        }
                        $where[] = "((biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)) or (biz_bill.id_type = 'consol' and biz_bill.id_no in ($consols_ids)))";
                    }
                    continue;
                }else if($f_arr[0] == 'shipment_by_consol'){
                    $biz_shipment_model = Model('biz_shipment_model');
                    $biz_consol_model = Model('biz_consol_model');

                    if(!empty($v_arr)){
                        $this_where = array();
                        foreach ($v_arr as $vv){
                            $this_where[] = search_like($f_arr[1], $s, $vv);
                        }
                        $this_where = join(' or ', $this_where);
                        $this->db->select('id,consol_id');
                        $shipments = $biz_shipment_model->no_role_get($this_where);
                        $shipments_ids = join(',', array_column($shipments, 'id'));
                        if(empty($shipments_ids)){
                            $shipments_ids = 0;
                            $consols_ids = 0;
                        }else{
                            $consol_ids = join(',', array_filter(array_column($shipments, 'consol_id')));
                            $this->db->select('id');
                            if(empty($consol_ids))$consol_ids = 0;
                            $consols = $biz_consol_model->no_role_get("id in ($consol_ids)");
                            $consols_ids = join(',', array_column($consols, 'id'));
                            if(empty($consols_ids))$consols_ids = 0;
                        }
                        $where[] = "((biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)) or (biz_bill.id_type = 'consol' and biz_bill.id_no in ($consols_ids)))";
                    }
                }else if($f_arr[0] == 'shipment'){
                    $biz_shipment_model = Model('biz_shipment_model');
                    if(!empty($v_arr)){
                        $this->db->select('id');
                        $this_where = array();
                        foreach ($v_arr as $vv){
                            $this_where[] = search_like($f_arr[1], $s, $vv);
                        }
                        $this_where = join(' or ', $this_where);
                        // $shipments = $biz_shipment_model->no_role_get($this_where);
                        // $shipments_ids = join(',', array_column($shipments, 'id'));
                        // if(empty($shipments_ids))$shipments_ids = 0;
                        // $where[] = "biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)";
                        $shipment_table = Model('biz_shipment_model')->getTableName();
                        $where[] = "biz_bill.id_type = 'shipment' and biz_bill.id_no in (select id from {$shipment_table} where $this_where)";
                    }
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];


                $this_where = array();
                foreach ($v_arr as $this_v){
                    if($this_v == '-')  continue;
                    $this_where[] = search_like($f, $s, $this_v);
                }
                if(!empty($this_where)) {
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) {
                        $this_join0 = explode(' ', $this_join[0]);
                        $title_data['sql_join'][end($this_join0)] = $this_join;
                    }
                    $where[] = "(" . join(' or ', $this_where) . ")";
                }
            }
        }
        //获取可查看组权限
        $bsc_user_role_model = Model('bsc_user_role_model');
        // $where[] = $bsc_user_role_model->get_bill_role();
        //获取cost和sell查看权限
        $user_role = $bsc_user_role_model->get_user_role(array('read_text'));
        $userRoleRead = explode(',', $user_role['read_text']);//bill.cost,bill.sell
        $type_where = array();
        if(in_array('bill', $userRoleRead) || in_array('bill.cost', $userRoleRead)){
            $type_where[] = "biz_bill.type = 'cost'";
        }
        if(in_array('bill', $userRoleRead) || in_array('bill.sell', $userRoleRead)){
            $type_where[] = "biz_bill.type = 'sell'";
        }
        if(sizeof($type_where) > 1) $type_where = array();
        if(!empty($type_where))$where[] = '(' . join(' or ', $type_where) . ')';

        //这里拼接join数据
        foreach ($title_data['sql_join'] as $j){
            $this->db->join($j[0], $j[1], $j[2], false);
        }
        //获取需要的字段--end
    }

    /**
     * 船卡可配置版
     */
    protected function chuanka_diy(){
        $this->load->library('PHPExcel/PHPExcel.php');

        $root_path = dirname(BASEPATH);

        $template = $root_path . '/upload/excel/chuanka_diy.xlsx';

        $objPHPExcel = PHPExcel_IOFactory::load($template);     //加载excel文件,设置模板

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);  //设置保存版本格式
        $objActSheet = $objPHPExcel->getActiveSheet();

        $template_name = getValue('template_name');
        $view_name = getValue('view_name', 'no_view');
        $is_copy = (int)getValue('is_copy', 0);

        $sort = postValue('sort', 'id');
        $order = postValue('order', 'desc');
        $fields = postValue('field', array());

        $expTitle = date('Y-m-d') . '船卡_' . $template_name;

        $where = $this->get_data_where_v2();

        //这里是新版的查询代码
        $title_data = get_user_title_sql_data('biz_consol', $view_name);//获取相关的配置信息
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                if($f == '') continue;
                //TODO 如果是datetime字段,=默认>=且<=
                $date_field = isset($title_data['editor']['datebox']) ? $title_data['editor']['datebox'] : array();
                //datebox的用等于时代表搜索当天的
                if(in_array($f, $date_field) && $s == '='){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} >= '$v 00:00:00' and {$f} <= '$v 23:59:59'";
                    }
                    continue;
                }
                //datebox的用等于时代表搜索小于等于当天最晚的时间
                if(in_array($f, $date_field) && ($s == '<' || $s == '<=')){
                    if(!isset($title_data['sql_field'][$f])) continue;
                    $f = $title_data['sql_field'][$f];
                    if($v != ''){
                        $where[] = "{$f} $s '$v 23:59:59'";
                    }
                    continue;
                }

                //把f转化为对应的sql字段
                //不是查询字段直接排除
                $title_f = $f;
                if(!isset($title_data['sql_field'][$title_f])) continue;
                $f = $title_data['sql_field'][$title_f];


                if($v !== '') {
                    //这里缺了2个
                    if($v == '-') $v = '';
                    //如果进行了查询拼接,这里将join条件填充好
                    $this_join = join_sql_change_join_ci($title_data['base_data'][$title_f]['sql_join']);
                    if(!empty($this_join)) $title_data['sql_join'][$title_data['base_data'][$title_f]['sql_join']] = $this_join;

                    $where[] = search_like($f, $s, $v);
                }
            }
        }

        $where_str = join(' and ', $where);
        //-----------------------------------------------------------------
        //获取需要的get字段--start

        //读取当前船卡的配置
        $config_prefix = 'chuanka_config_' . get_session('id') . '_' . $template_name;
        $rs = Model('sys_config_model')->get_one($config_prefix);
        if(empty($rs)){
            exit('未找到当前船卡模板');
        }
        $config_data = array_column(json_decode($rs['config_text'], true), null, 'base_id');

        //将base_id都取出来,然后获取表头参数,进行拼接使用
        $base_ids = array_column($config_data, 'base_id');
        $base_rs = array_column(Model('sys_config_title_model')->get_base_title('biz_consol'), null, 'id');
        $selects = array();//select 字段
        $sql_join = array();// join 值
        $data_setting = array();// 当前表格的数据

        foreach ($base_ids as $key => $base_id){
            if(!isset($base_rs[$base_id])) continue;
            $this_row = $base_rs[$base_id];
            $this_cell_key = "{$this->cellName[$key]}2";

            $selects[] = "{$this_row['sql_field']} as {$this_row['table_field']}";
            if(!empty($this_row['sql_join']))$sql_join[$this_row['sql_join']] = join_sql_change_join_ci($this_row['sql_join']);
//            $objPHPExcel->getActiveSheet()->getColumnDimension($this->cellName[$key])->setWidth($config_data[$base_id]['width']);
            $data_setting[$this_cell_key] = lang($this_row['title']);
        }
        $this->db->limit(201);
        $this->db->select(join(',', $selects), false);
        //这里拼接join数据
        foreach ($sql_join as $j){
            $this->db->join($j[0], $j[1], $j[2]);
        }//将order字段替换为 对应的sql字段
        $sort = $title_data['sql_field']["biz_consol.{$sort}"];
        //获取需要的get字段--end
        $rs = Model('biz_consol_model')->get_v3($where_str, $sort, $order);

        //填充数据--start
        $c_i = 3;//从第几行开始
        $userRole = Model('bsc_user_role_model')->get_data_role();
        foreach ($rs as $row) {
            $row = data_role($row, array(), explode(',', $userRole['read_text']), 'biz_consol', 'biz_consol_index');

            foreach ($base_ids as $key => $base_id) {
                if (!isset($base_rs[$base_id])) continue;
                $this_row = $base_rs[$base_id];

                $table_field = $this_row['table_field'];
                $this_val = $row[$table_field];

                if($table_field == 'box_info'){
                    $box_info = json_decode($this_val, true);
                    if(empty($box_info)) $box_info = array();
                    $box_info_text = array();
                    foreach ($box_info as $val){
                        $box_info_text[] = $val['num'] . 'X' . $val['size'];
                    }
                    $this_val = join(', ', $box_info_text);
                }

                if($table_field == 'free_svr'){
                    $box_info = json_decode($this_val, true);
                    if(empty($box_info)) $box_info = array();
                    $box_info_text = array();
                    foreach ($box_info as $val){
                        $box_info_text[] = $val['days'] . '*' . $val['type'];
                    }
                    $this_val = join(',', $box_info_text);
                }

                if(empty($this_val)) $this_val = '';

                $data_setting["{$this->cellName[$key]}{$c_i}"] = $this_val;
            }
            $c_i++;
        }
        //填充数据--end

        //如果为1 这里作为html输出
        if($is_copy){
            $table = "<table border='0' cellpadding='4' cellspacing='1' bgcolor='black' width='1600'>";
            for ($c_i = 0; $c_i <= sizeof($rs); $c_i++){
                //如果是2的都是标题,其他全是
                $table .= "<tr>";
                $this_c_i = $c_i + 2;
                foreach ($this->cellName as $cn){
                    if(!isset($data_setting[$cn . $this_c_i])) continue;
                    if($this_c_i == 2){
                        $table .= "<td bgcolor='white' style='text-align: center'>{$data_setting[$cn . $this_c_i]}</td>";
                    }else{
                        $table .= "<td bgcolor='white'>{$data_setting[$cn . $this_c_i]}</td>";
                    }
                }
                $table .= "</tr>";
            }
            echo $table;
            return;
        }

        $styleThinBlackBorderOutline = array(
            'borders' => array(
                'allborders' => array( //设置全部边框
                    'style' => \PHPExcel_Style_Border::BORDER_THIN //粗的是thick
                ),
            ),
        );

        $objPHPExcel->getActiveSheet()->getStyle('A3:P' . $c_i)->applyFromArray($styleThinBlackBorderOutline);

        // Miscellaneous glyphs, UTF-8
        foreach ($data_setting as $key => $val){
            $objActSheet->setCellValue($key, $val);
        }
        ob_end_clean();
        header('Content-Type:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition:attachment;filename=" . $expTitle . ".xlsx");//attachment新窗口打印inline本窗口打印
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $objWriter->save('php://output');
    }

    /**
     * 获取excel里的数据
     */
    protected function getExcel($savePath, $file_name, $highestColumn, $Encoding = 'UTF-8'){
        $this->load->library('PHPExcel/PHPExcel.php');

        //根据后缀自动判断使用哪个
        $ext = explode('.', $file_name)[1];
        $inputFileTypeConfig = array(
            'xlsx' => 'Excel2007',
            'xls' => 'Excel5',
            'csv' => 'CSV',
        );
        $inputFileType = $inputFileTypeConfig[$ext];
        //获取所有sheet名称
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        if($inputFileType == 'CSV') $objReader->setInputEncoding($Encoding); //加上这一句
        $excel = $objReader->load($savePath . $file_name);
        $sheetnames = $excel->getsheetnames();//卸载表格
        $excel->disconnectWorksheets();

        $data = array();
        foreach ($sheetnames as $sheetname){
            $objReader->setLoadSheetsOnly($sheetname);
            $objPHPExcel = $objReader->load($savePath . $file_name);
            $sheet = $objPHPExcel->getSheet(0);
            //获取行数
            $highestRow = $sheet->getHighestRow();
            //excel中的一条数据
            $excel_data = array();
            for ($i = 1; $i <= $highestRow; $i++) {
                $column = array();
                for ($j = 1; $j <= $highestColumn; $j++) {
                    $j_key = $this->cellName[$j - 1];
                    $this_cell_value = $sheet->getCell($j_key . $i)->getValue();
                    //如果第一个是=，则读取公式
                    if(substr($this_cell_value, 0, 1) == '='){
                        $this_cell_value = $sheet->getCell($j_key . $i)->getFormattedValue();
                    }
                    if($this->is_trim) $this_cell_value = trim($this_cell_value);
                    $column[$j_key] = $this_cell_value;
                }
                array_push($excel_data, $column);
            }
            $data[$sheetname] = $excel_data;
        }
        return $data;
    }
}
