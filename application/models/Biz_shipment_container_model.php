<?php

class biz_shipment_container_model extends CI_Model
{
    private $table = 'biz_shipment_container';
    public function __construct()
    {
        $this->load->database();
    }

    public function get_by_id($id = 0)
    {
        $this->db->where('id', $id);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_where_one($where)
    {
        $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_one($f = "", $v = "", $notsql = true)
    {
        $this->db->where($f, $v, $notsql);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_option($containershipment_type)
    {
        $this->db->where('containershipment_type', $containershipment_type);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
    
    public function no_role_get($where="" ,$sort="id",$order="desc"){
       if($where !="")	$this->db->where($where);
       $this->db->order_by($sort,$order);
       $query = $this->db->get($this->table);
       return $query->result_array();
    }

    public function total($where = "")
    {
        if ($where != "") $this->db->where($where);
        $this->db->from($this->table);
        return $this->db->count_all_results('');
    }

    public function save($data = array())
    {
        $data["created_time"] = date('y-m-d H:i:s', time());
        $data["updated_time"] = date('y-m-d H:i:s', time());
        $data["created_by"] = $this->session->userdata('id');
        $data["updated_by"] = $this->session->userdata('id');
        $data["created_group"] = implode(',', $this->session->userdata('group'));
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function save_admin($data = array())
    {
        $data["created_time"] = date('y-m-d H:i:s', time());
        $data["updated_time"] = date('y-m-d H:i:s', time());
        $data["created_by"] = 0;
        $data["updated_by"] = 0;
        $data["created_group"] = '';
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id = '', $data = array())
    {
        $data["updated_by"] = $this->session->userdata('id');
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }
    
    public function update_admin($id = '', $data = array())
    {
        $data["updated_time"] = date('y-m-d H:i:s', time());
        $data["updated_by"] = 0;
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id = '')
    {
        // delete containershipment
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function join_container_get_by_id($shipment_id, $consol_id)
    {
        $sql = "select bsc.*,container_size,seal_no from " . $this->table . " bsc LEFT JOIN biz_container bc on bsc.container_no = bc.container_no and bc.consol_id = '$consol_id'  where shipment_id = '$shipment_id'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function join_container_get_by_parent_id($shipment_id, $consol_id)
    {
        $sql = "select bsc.*,container_size,seal_no from " . $this->table . " bsc LEFT JOIN biz_container bc on bsc.container_no = bc.container_no and bc.consol_id = '$consol_id'  where shipment_id in (select id from biz_shipment where parent_id = '$shipment_id')";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_container_sum($shipment_id = 0, $field = array()){
        if(!is_array($field)) $field = explode(',', $field);
        foreach ($field as $val){
            $this->db->select_sum($val, $val);
        }
        $this->db->where('shipment_id', $shipment_id);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }
    
    public function get_container_size_count($shipment_id = 0, $where = '',$filed = ''){
        //select bc.container_size, count(*) from biz_shipment_container bsc
        // RIGHT JOIN biz_container bc ON bc.container_no = bsc.container_no where shipment_id = 112 GROUP BY bc.container_size
        if($filed != '') $filed = ',' . $filed;
        $this->db->select('bc.container_size, count(*) as count' . $filed);
        $this->db->join('biz_container bc', 'bc.container_no = bsc.container_no', 'left');
        $this->db->group_by('bc.container_size');
        if($where != '') $this->db->where($where);
        $this->db->where('shipment_id', $shipment_id);
        $query = $this->db->from($this->table . ' bsc');
        return $query->get()->result_array();
    }
    
    public function get_containers_by_consol_id_group_shipment($consol_id = 0){
        $this->db->where("shipment_id in (select id from biz_shipment where biz_shipment.consol_id = $consol_id)");
        $this->db->group_by('shipment_id');
        $query = $this->db->get($this->table);
        return $query->result_array();
    }
}