<script type="text/javascript">
    function doSearch(){
        $('#tt').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val() ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val() ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').val()
        });
        $('#chaxun').window('close');
    }

    $(function () {
        $('#tt').edatagrid({
            url: '/biz_bill_currency/get_data/',
            saveUrl: '/biz_bill_currency/add_data/',
            updateUrl: '/biz_bill_currency/update_data/',
            destroyUrl: '/biz_bill_currency/delete_data'
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height()
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });
    });
    
    //采用jquery easyui loading css效果
    
    function reload_bill_local() {
        $('#reload_bill_local').dialog('open');
        // $.ajax({
        //     type: 'GET',
        //     url: '/biz_bill/reload_bill_local',
        //     data:{
        //     },
        //     dataType: 'json',
        //     success: function (res) {
        //         $.messager.alert('Tips', res.msg);
        //         if(res.code == 0){
        //             $('#tt').datagrid('reload');
        //         }
        //     }
        // });
    }
    
    function reload_bill_local_save() {
        ajaxLoading();
        $('#reload_bill_local').dialog('close');
        $('#reload_bill_local_form').form('submit', {
            url: '/biz_bill/reload_bill_local',
            onSubmit: function () {
                var validate = $(this).form('validate');
                if(!validate){
                    ajaxLoadEnd();
                }
                return validate;
            },
            success: function (result) {
                
                var res;
                try{
                    res = $.parseJSON(result);
                }catch(e){
                    
                }
                if(res == undefined){
                    $.messager.alert('Tips', '发生错误，请联系管理员');
                }else{
                    $.messager.alert('<?= lang('info')?>', res.msg);
                    if(res.code == 0){
                        $('#reload_bill_local_form').form('reset');
                        $('#tt').datagrid('reload');
                    }else{
                        $('#reload_bill_local').dialog('open');
                    }
                }
                ajaxLoadEnd();
            }
        });
    }
</script>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <th field="id" width="60" sortable="true"><?= lang('Id');?></th>
        <th field="currency_from" width="100" align="center" editor="text"><?= lang('currency_from');?></th>
        <th field="currency_to" width="100" align="center" editor="text"><?= lang('currency_to');?></th>
        <th field="sell_value" width="100" data-options="editor:{type:'numberbox', options:{min:0,precision:4}}"><?= lang('sell_value');?></th>
        <th field="cost_value" width="100" data-options="editor:{type:'numberbox', options:{min:0,precision:4}}"><?= lang('cost_value');?></th>
        <th field="invoice_value" width="100" data-options="editor:{type:'numberbox', options:{min:0,precision:4}}"><?= lang('invoice_value');?></th>
        <th field="start_time" width="100" editor="datebox"><?= lang('start_time');?></th>
        <th field="created_by" width="100" sortable="true"><?= lang('created_by');?></th>
        <th field="created_time" width="150" sortable="true"><?= lang('created_time');?></th>
        <th field="updated_by" width="100" sortable="true"><?= lang('updated_by');?></th>
        <th field="updated_time" width="150" sortable="true"><?= lang('updated_time');?></th>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#tt').edatagrid('addRow',0)"><?= lang('add');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#tt').edatagrid('destroyRow')"><?= lang('delete');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#tt').edatagrid('saveRow')"><?= lang('save');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#tt').edatagrid('cancelRow')"><?= lang('cancel');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" onclick="reload_bill_local()"><?= lang('刷新汇率');?></a>
            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                Query 1
            </td>
            <td align="right">
                <select name="f1"  id="f1"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1"  id="s1"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                        <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v1" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                Query 2
            </td>
            <td align="right">
                <select name="f2"  id="f2"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2"  id="s2"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                        <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v2" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                Query 3
            </td>
            <td align="right">
                <select name="f3"  id="f3"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s3"  id="s3"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                        <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v3" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;">Search</a>
</div>
<div id="reload_bill_local" class="easyui-dialog" title="<?= lang('刷新汇率');?>" style="padding:5px;"
     data-options="closed:true,modal:true,border:'thin'">
    <form id="reload_bill_local_form" method="post">
        <table>
            <tr>
                <td><?= lang('start_time');?></td>
                <td><input class="easyui-datebox" name="start_time"></td>
            </tr>
            <tr>
                <td><?= lang('end_time');?></td>
                <td><input class="easyui-datebox" name="end_time"></td>
            </tr>
        </table>
        <a href="javascript:reload_bill_local_save()" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px"><?= lang('提交');?></a>
    </form>
</div>