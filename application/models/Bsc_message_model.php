<?php


class bsc_message_model extends CI_Model
{
    private $table = 'bsc_message';
    public function __construct()
    {
        $this->load->database();
    }

    public function get_one($f = "", $v = "")
    {
        $this->db->where($f, $v);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_one_where($where)
    {
        if ($where != '') $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }
    
    public function get_where_one($where)
    {
        if ($where != '') $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get_by_id_no($biz_table = "", $id_no = '')
    {
        $this->db->where('biz_table', $biz_table);
        $this->db->where('id_no', $id_no);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function save($data = array())
    {
        $data["created_time"] = date('y-m-d H:i:s',time());
        $data["updated_time"] = date('y-m-d H:i:s',time());
        $data["created_by"] =  $this->session->userdata('id');
        $data["updated_by"] =  $this->session->userdata('id');
        $data["created_group"] = implode(',', $this->session->userdata('group') );
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();

    }

    public function update($id = '', $data = array())
    {
        $data["updated_time"] = date('Y-m-d H:i:s', time());
        $data["updated_by"] =  $this->session->userdata('id');
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id = '')
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function get($where = "", $sort = "id", $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function total($where = "")
    {
        if ($where != "") $this->db->where($where);
        $this->db->from($this->table);
        return $this->db->count_all_results('');
    }
    
    public function total1($where = "")
    {
        if ($where != "") $this->db->where($where);
        $this->db->get($this->table);
        exit(lastquery());
        return $this->db->count_all_results('');
    }

}