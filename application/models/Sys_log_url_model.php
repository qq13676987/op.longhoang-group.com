<?php

class Sys_log_url_model extends CI_Model
{
    private $table = 'sys_log_url';

    public function __construct(){
        $this->load->database();
    }

    public function get($where = '', $sort, $order)
    {
        if ($where != '') $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function get_by_id($id = 0)
    {
        if ($id != 0) {
            $this->db->where('id', $id);
            $query = $this->db->get($this->table);
            return $query->row_array();
        }
    }

    public function total($where = '')
    {
        if ($where != '') $this->db->where($where);
        $this->db->from($this->table);
        return $this->db->count_all_results('');
    }

    public function save($data = array())
    {
        $data["created_time"] = date('Y-m-d H:i:s', time());
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
}