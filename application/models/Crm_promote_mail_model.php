<?php

class crm_promote_mail_model extends CI_Model
{
    private $table = 'crm_promote_mail';

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function save($data = array())
    {
        $data["create_time"] = date('y-m-d H:i:s',time());
        $data["update_time"] = date('y-m-d H:i:s',time());
        $data["create_by"] =  $this->session->userdata('id');
        $data["update_by"] =  $this->session->userdata('id');
        $this->db->insert($this->table, $data);
        //echo $this->db->last_query();

        return $this->db->insert_id();

    }


    public function get($where="" ,$sort="id",$order="desc",$like=array()){
        if($where !="")	$this->db->where($where, null,false);
        $this->db->like($like);
        $this->db->order_by($sort,$order);
        $query = $this->db->get($this->table);
        //echo $this->db->last_query();
        return $query->result_array();
    }

    public function total($where="",$like=array()){
        if($where !="")	$this->db->where($where, null,false);
        $this->db->like($like);
        $this->db->from($this->table);
        return  $this->db->count_all_results('');
    }

    public function del($id = '')
    {
        // delete container
        $this->db->where('id', $id);
        return $this->db->delete($this->table);
    }


    public function get_row($where=array()){
        if($where != array()) $this->db->where($where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }


    public function update($id = '', $data = array())
    {
        $data["update_time"] = date('Y-m-d H:i:s', time());
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    /**
     * 返回状态值
     * @return void
     */
    public function status_s($v=0){
        $s=$v;
        if($v==-1){
            $s=lang("已拒绝");
        }
        if($v==0){
            $s=lang("待审核");
        }
        else if($v==1){
            $s=lang("审核通过未发送");
        }
        else if($v==2){
            $s=lang("已发送");
        }
        return $s;

    }

}