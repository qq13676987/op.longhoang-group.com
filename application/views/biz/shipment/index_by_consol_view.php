<script type="text/javascript">
    var url;

    function resize_table() {
        $('#tt').datagrid('resize', {
            width: 'auto',
            height: $(window).height(),
        });
    }

    $(function () {
        //window.parent.$('#el').layout('collapse','south');
        $('#tt').edatagrid({
            url: '/biz_shipment/get_by_consol_data/<?php echo $consol_id;?>',
            updateUrl: '/biz_shipment/update_data_table/',
            onError: function (index, data) {
                $.messager.alert('<?= lang('提示');?>', data.msg);
            }
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height(),
            onDblClickRow: function (rowIndex) {
                $(this).datagrid('selectRow', rowIndex);
                var row = $('#tt').datagrid('getSelected');
                if (row) {
                    url = '/biz_shipment/edit/' + row.id;
                    // 		window.open(url);
                    top.location.href = url;
                }
            },
            onClickRow: function (index, data) {
                update_tt();
            },
            // detail view,expandrows

            view: detailview,
            detailFormatter: function (index, row) {
                return '<div style="padding:2px"><table class="ddv"></table></div>';
            },
            onExpandRow: function (index, row) {
                var ddv = $(this).datagrid('selectRow', index).datagrid('getRowDetail', index).find('table.ddv');
                ddv.edatagrid({
                    url: '/biz_shipment/get_by_consol_data/0/' + row.id,
                    updateUrl: '/biz_shipment/update_data_table/',
                });
                ddv.datagrid({
                    fitColumns: false,
                    rownumbers: true,
                    singleSelect: true,
                    loadMsg: '',
                    height: 'auto',
                    columns: [[
                        <?php
                        if (!empty($f)) {
                            foreach ($f as $k => $rs) {

                                $field = $rs[0];
                                $disp = $rs[1];
                                $width = $rs[2];

                                if ($width == "0") continue;
                                if ($k > 9) break;
                                if ($field == 'cus_no') {
                                    echo "{field:'$field',title:'" . lang($disp) . "',width:$width,align:'left',editor:'text'},";
                                    continue;
                                }
                                echo "{field:'$field',title:'" . lang($disp) . "',width:$width,align:'left'},";
                            }
                        }
                        ?>
                    ]],
                    onResize: function () {
                        $('#tt').datagrid('fixDetailRowHeight', index);
                    },
                    onDblClickRow: function (ri, r) {
                        window.parent.container_window(r.id);
                    },

                    onLoadSuccess: function () {
                        setTimeout(function () {
                            $('#tt').datagrid('fixDetailRowHeight', index);
                        }, 0);
                    },
                });
                $('#tt').datagrid('fixDetailRowHeight', index);

            }
        });
        $(window).resize(function () {
            resize_table();
        });

        $('#multi').change(function () {
            var checked = $(this).prop('checked');
            if (checked) {
                $('#tt').datagrid({
                    singleSelect: false,
                });
                $('#merge').css('display', 'inline-block');
                $('#split').css('display', 'none');
            } else {
                $('#tt').datagrid({
                    singleSelect: true,
                });
                $('#merge').css('display', 'none');
                $('#split').css('display', 'inline-block');
            }
        });

        $('.split_type').click(function () {
            var checked = $(this).prop('checked');
            var val = $(this).val();
            if (checked) {
                if (val == 3) {
                    $('.cus_nos').css('display', 'table-row')
                } else {
                    $('.cus_nos').css('display', 'none')
                }
            }
        });

        //实例化可绑定shipment的下拉表格
        $('#bind_shipment_ids').combogrid({
            panelWidth: 500,
            idField: 'id',
            textField: 'job_no',
            url: '/biz_shipment/get_binding_shipments',
            method: 'post',
            columns: [[
                {field: 'job_no', title: 'job_no', width: 80},
                {field: 'client_company', title: 'client_company', width: 120},
            ]],
            editable: false,
            toolbar: '#bind_shipments_tb',
            fitColumns: true,
        });

        $('#bind_shipments_query').click(function () {
            var where = {};
            var form_data = $('#bind_shipments_tb_form').serializeArray();
            $.each(form_data, function (index, item) {
                if (where.hasOwnProperty(item.name) === true) {
                    if (typeof where[item.name] == 'string') {
                        where[item.name] = where[item.name].split(',');
                        where[item.name].push(item.value);
                    } else {
                        where[item.name].push(item.value);
                    }
                } else {
                    where[item.name] = item.value;
                }
            });
            // where['field'] = $('#to_search_field').combo('getValue');
            $('#bind_shipment_ids').combogrid('grid').datagrid('load', where);
        });
    });

    function keydown_search(e, click_id) {
        // if(e.keyCode == 13){
        $('#' + click_id).trigger('click');
        // }
    }

    function split_shipment() {
        var row = $('#tt').datagrid('getSelected');
        if (row !== null) {
            $('#split_shipment input[name="shipment_id"]').val(row.id);
            $('#split_shipment').dialog('open');
        } else {
            $.messager.alert('<?= lang('error')?>', '<?= lang('No columns selected')?>');
        }
    }

    function save_child() {
        //datagrid获取选中行
        var tt = $('#tt');
        var row = tt.datagrid('getSelected');

        // 获取被选中行的索引 index
        var index = tt.datagrid('getRowIndex', row);
        tt.datagrid('getRowDetail', index).find('table.ddv').edatagrid('saveRow');
    }

    function cancel_child() {
        //datagrid获取选中行
        var tt = $('#tt');
        var row = tt.datagrid('getSelected');

        // 获取被选中行的索引 index
        var index = tt.datagrid('getRowIndex', row);
        tt.datagrid('getRowDetail', index).find('table.ddv').edatagrid('cancelRow');
    }


    function do_split_shipment() {
        var shipment_id = $('#split_shipment input[name="shipment_id"]').val();
        var type = $('#split_shipment input[name="type"]:checked').val();
        var count = $('#split_shipment input[name="count"]').val();
        var cus_nos = $('#split_shipment textarea[name="cus_nos"]').val();
        $.ajax({
            type: 'POST',
            url: '/biz_shipment/new_split_shipment?shipment_id=' + shipment_id + '&type=' + type + '&count=' + count,
            data: {
                cus_nos: cus_nos
            },
            dataType: 'json',
            success: function (res) {
                $.messager.alert('Tips', res.msg);
                if (res.code == 0) {
                    $('#tt').datagrid('reload');
                } else {

                }
            },
            error: function () {
                $.messager.alert('Tips', 'error');
            }
        });
        $('#split_shipment').dialog('close');
    }

    function upload_file() {
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            window.open("/bsc_upload/index/biz_shipment/" + row.id);
        } else {
            $.messager.alert("tips", "No Item Selected");
        }
    }

    function add_shipment() {
        $('#win_add_shipment').dialog('open');
    }

    function do_add_shipment() {
        showmask();
        url = '/biz_shipment/update_data_consol_id/';
        $('#fm_add_shipment').form('submit', {
            url: url,
            onSubmit: function () {
                $('#win_add_shipment').dialog('close');
                return $(this).form('validate');
            },
            success: function (res_json) {
                hidemask();
                var res;
                try {
                    res = $.parseJSON(res_json);
                } catch (e) {

                }
                if (res == undefined) {
                    $.messager.alert('Tips', '发生错误,请联系管理员!');
                } else {
                    if (res.code == 0) {
                        $('#tt').datagrid('reload');
                        $('#id').combogrid('clear');
                        $('#id').combogrid('grid').datagrid('reload');
                    } else {
                        $.messager.alert('Tips', res.msg);
                    }
                }
            }
        });
    }

    function do_del_shipment() {
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            //首先检测父页面是否放单勾已勾
            var yifangdan = window.parent.$('#yifangdan').prop('checked');
            var title = '提示';
            var content = '此票是否已放单? ';
            $.messager.defaults = { ok: "是", cancel: "否",width:300 }; 
            $.messager.confirm(title, content, function (r) {
                // //如果放单勾了,那么r没值代表取消
                // if (yifangdan && !r) {
                //     $.messager.alert('Tips', '请先取消顶部的放单勾');
                //     return false;
                // }
                // //如果放单没勾,那么点确认提示先勾
                // if (!yifangdan && r) {
                //     $.messager.alert('Tips', '请先勾选顶部的放单勾');
                //     return false;
                // }

                showmask();
                $.ajax({
                    type: "POST",
                    url: '/biz_shipment/update_data_consol_id/' + row.id,
                    data: {consol_id: 0},
                    success: function (res_json) {
                        hidemask();
                        var res;
                        try {
                            res = $.parseJSON(res_json);
                        } catch (e) {

                        }
                        if (res == undefined) {
                            $.messager.alert('Tips', '发生错误,请联系管理员!');
                        } else {
                            $.messager.alert('Tips', res.msg);
                            $('#tt').datagrid('reload');
                            $('#id').combogrid('clear');
                            $('#id').combogrid('grid').datagrid('reload');
                        }
                    },
                    error: function (data) {
                        alert("error:" + data.responseText);
                    }

                });
            });
        } else {
            $.messager.alert("tips", "No Item Selected");
        }
    }

    function showmask() {
        $("<div class=\"datagrid-mask\"></div>").css({
            display: "block",
            width: "100%",
            height: $(window).height()
        }).appendTo("body");
        $("<div class=\"datagrid-mask-msg\"></div>").html("please waiting....").appendTo("body").css({
            display: "block",
            left: ($(document.body).outerWidth(true) - 190) / 2,
            top: ($(window).height() - 45) / 2
        });

    }

    function hidemask() {
        $(".datagrid-mask").hide();
        $(".datagrid-mask-msg").hide();
    }

    function add() {
        window.open("/biz_shipment/add?consol_id=<?= $consol_id;?>");
    }

    function config() {
        window.open("/biz_shipment/by_consol_config_title/");
    }

    function split() {
        var checked = $('#multi').prop('checked');
        if (checked) {
            $.messager.alert("tips", "无法多选");
            return;
        }
        var select_row = $('#tt').datagrid('getSelected');
        if (select_row == null || select_row.length < 1) {
            $.messager.alert("tips", "请选择任意一行");
            return;
        }
        $.messager.confirm('tips', '确认拆分shipment?', function (r) {
            if (r) {
                var id = select_row.id;
                $.ajax({
                    type: 'POST',
                    url: '/biz_shipment/split',
                    data: {
                        id: id,
                    },
                    dataType: 'json',
                    success: function (res) {
                        if (res.code == 0) {
                            $('#tt').datagrid('reload');
                        }
                        $.messager.alert("tips", res.msg);
                    },
                    error: function (e) {
                        $.messager.alert("tips", "error");
                    }
                });
            }
        });
    }

    var singleSelect = true;

    function multiple() {
        if (singleSelect) {
            singleSelect = false;
        } else {
            singleSelect = true;
        }
        $('#tt').datagrid({singleSelect: singleSelect});
    }

    function copy() {
        var row = $('#tt').datagrid('getSelected');
        if (row) {
            window.open("/biz_shipment/add/" + row.id + "?consol_id=<?= $consol_id;?>");
        } else {
            $.messager.alert("tips", "No Item Selected");
        }
    }

    function update_tt() {
        var row = $('#tt').datagrid('getSelected');
        if (row != null) {
            var index = $('#tt').datagrid('getRowIndex', row);
            $('#tt').datagrid('beginEdit', index);
        } else {
            $.messager.alert('<?= lang('Tips')?>', '<?= lang('No columns selected')?>');
        }
    }

    function save_tt() {
        var row = $('#tt').datagrid('getSelected');
        if (row != null) {
            var index = $('#tt').datagrid('getRowIndex', row);
            $('#tt').datagrid('endEdit', index).datagrid('clearSelections');
        } else {
            $.messager.alert('<?= lang('Tips')?>', '<?= lang('No columns selected')?>');
        }
    }

    function save_all() {
        save_tt();
        save_child();
    }
    
    /**
     * ebk发送按钮
     */
    function ebooking_send() {
        //根据当前代理 发送EBOOKING信息
        $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认发送ebooking? 确认后将会发送到 代理系统(只支持我司分公司)');?>',  function (r) {
            if(r){
                $.ajax({
                    type:'POST',
                    //$id_no = 0,$carrier = '', $type = '', $file_function = 'insert'
                    url:'/bsc_message/create_message/<?= $consol_id;?>/0/EBOOKING/insert',
                    success:function (res_json) {
                        try {
                            var res = $.parseJSON(res_json);
                            $.messager.alert('<?= lang('提示');?>', res.msg);
                            if(res.code === 0){
                                $('#tt').datagrid('reload');
                            }
                        }catch (e) {
                            $.messager.alert('<?= lang('提示');?>', res_json);
                        }
                    },
                });
            }
        });
    }
</script>
<style type="text/css">
    .cus_nos {
        display: none;
    }
    a{ text-decoration: none;}
</style>
<?php
if(!empty($_GET["hidden"])){
    exit("<br><br><a href='/biz_shipment/index_by_consol/{$consol_id}'>为了页面打开速度，默认隐藏掉了，可点击查看shipments</a>");
}
?>
<table id="tt" rownumbers="false" pagination="true" idField="id" pagesize="<?php echo $n; ?>" toolbar="#tb"
       singleSelect="true" nowrap="true">
    <thead>
    <tr>
        <?php
        if (!empty($f)) {
            foreach ($f as $rs) {

                $field = $rs[0];
                $disp = $rs[1];
                $width = $rs[2];

                if ($width == "0") continue;
                if ($field == "id" || $field == "job_no") {
                    echo "<th field=\"$field\" width=\"$width\" sortable=\"true\">" . lang($disp) . "</th>";
                    continue;
                }
                if ($field == "cus_no") {
                    echo "<th field=\"$field\" width=\"$width\" editor=\"text\">" . lang($disp) . "</th>";
                    continue;
                }
                echo "<th field=\"$field\" width=\"$width\">" . lang($disp) . "</th>";
            }
        }
        ?>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <?php
                $bind_disable = "";
                if ($consol["status"] != "normal") $bind_disable = 'disabled="disabled"';
                ?>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
                   title="绑定关联的SHIPMENT,consol必须得是normal"
                   onclick="add_shipment();" <?php //echo $bind_disable; //暂时屏蔽掉，和error状态陷入死循环了?>><?= lang('bind'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true"
                   title="释放关联的SHIPMENT" onclick="do_del_shipment();"><?= lang('release'); ?></a>
<!--                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"-->
<!--                   title="新增SHIPMENT会自动绑定到当前CONSOL" onclick="add();">--><?//= lang('new add'); ?><!--</a>-->
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true"
                   title="复制选中的SHIPMENT" onclick="copy();"><?= lang('复制'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-reload" plain="true"
                   title="对当前SHIPMENT生成关单号" onclick="split_shipment();"><?= lang('split'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true"
                   title="对修改的分单号内容进行保存" onclick="save_all()"><?= lang('save'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" plain="true"
                   onclick="cancel_child()"><?= lang('cancel'); ?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" plain="true" title="切换多选模式"
                   onclick="multiple()"><?= lang('多选'); ?></a>
               <a href="javascript:void(0);" class="easyui-linkbutton" iconCls="icon-add" plain="true" data-options="onClick:function(){ebooking_send();}"><?= lang('EBK发送'); ?></a>
            </td>
        </tr>
    </table>

</div>
<!--<div id="mm2" style="width:150px;">-->
<!--    <div data-options="iconCls:'icon-ok'" onclick="javascript:config()"><?= lang('column'); ?></div>-->
<!--</div>-->
<div id="win_add_shipment" class="easyui-dialog" title="add shipment" closed="true"
     data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons2'">
    <form id="fm_add_shipment" method="post" novalidate>
        <table style="width:350px;">
            <tr>
                <td align="right">shipment Job No.:&nbsp;</td>
                <td>
                    <!--<select name="id" id="id" class="easyui-combogrid" style="width:250px" data-options="-->
                    <!--		panelWidth: 500,-->
                    <!--		idField: 'id',-->
                    <!--		textField: 'job_no',-->
                    <!--		url: '/biz_shipment/get_by_consol_data/0',-->
                    <!--		method: 'get',-->
                    <!--		columns: [[-->
                    <!--			{field:'job_no',title:'job_no ID',width:80},-->
                    <!--			{field:'client_company',title:'client_company',width:120},-->
                    <!--			{field:'hbl_no',title:'hbl_no',width:80,align:'right'} -->
                    <!--		]],-->
                    <!--		fitColumns: true-->
                    <!--	">-->
                    <!--</select>-->
                    <select name="id" id="bind_shipment_ids" style="width:250px">
                    </select>
                    <input type="hidden" name="consol_id" value="<?php echo $consol_id; ?>">
                </td>
            </tr>
        </table>
    </form>
</div>

<div id="dlg-buttons2">
    <button onclick="javascript:do_add_shipment()" class="easyui-linkbutton" iconCls="icon-ok" style="width:90px"
            id='btn'><?= lang('submit'); ?></button>
    <button onclick="javascript:$('#win_add_shipment').dialog('close')" class="easyui-linkbutton" iconCls="icon-cancel"
            style="width:90px"><?= lang('cancel'); ?></button>
</div>
<div id="split_shipment" class="easyui-dialog" title="split_shipment" closed="true"
     data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons3',width:400">
    <form id="split_shipment" method="post">
        <input type="hidden" name="shipment_id">
        <table>
            <tr>
                <td><?= lang('type') ?></td>
                <td>
                    <input class="split_type" type="radio" name="type" value="1" checked> 数字
                    <input class="split_type" type="radio" name="type" value="2"> 字母
                    <input class="split_type" type="radio" name="type" value="3"> 空白
                </td>
            </tr>
            <tr>
                <td><?= lang('count'); ?></td>
                <td>
                    <input type="text" class="easyui-numberbox" name="count" value="1" data-options="min:1,precision:0"
                           style="width:300px;">
                </td>
            </tr>
            <tr class="cus_nos">
                <td><?= lang('cus_no'); ?></td>
                <td>
                    <textarea name="cus_nos" style="width:300px;height: 100px;"></textarea>
                </td>
            </tr>
        </table>

    </form>
</div>
<div id="dlg-buttons3">
    <button type="button" onclick="javascript:do_split_shipment();" class="easyui-linkbutton" iconCls="icon-ok"
            style="width:90px" id='btn'><?= lang('submit'); ?></button>
    <button type="button" onclick="javascript:$('#split_shipment').dialog('close')" class="easyui-linkbutton"
            iconCls="icon-cancel" style="width:90px"><?= lang('cancel'); ?></button>
</div>

<!--合并shipment修改-->
<div id="merge_shipment_div" class="easyui-window" style="width:500px;height:150px"
     data-options="title:'window',modal:true,closed:true">
    <form id="merge_shipment_form">
        <input type="hidden" name="id">
        <table>
            <tr>
                <td>
                    <input class="easyui-textbox" readonly id="shipment_A_job_nos" style="width: 200px;">
                    <input type="hidden" name="shipment_A_ids" id="shipment_A_ids">
                </td>
                <td> =></td>
                <td>
                    <select class="easyui-combobox" name="shipment_B_id" id="shipment_B_id" style="width: 200px;"
                            data-options="valueField:'id',textField:'job_no',editable:false"></select>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <button onclick="merge_shipment_save()" type="button" class="easyui-linkbutton"
                            style="width: 180px;"><?= lang('save'); ?></button>
                </td>
            </tr>
        </table>
    </form>
</div>

<div id="bind_shipments_tb">
    <form id="bind_shipments_tb_form">
        <div style="padding-left: 5px;display: inline-block;"><label><?= lang('job_no'); ?>:</label><input name="job_no"
                                                                                                           class="tb_search"
                                                                                                           onkeyup="keydown_search(this, 'bind_shipments_query')"
                                                                                                           style="width:96px;"
                                                                                                           data-options="prompt:'text'"/>
        </div>
        <div style="padding-left: 1px; display: inline-block;">
            <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'icon-search'"
               id="bind_shipments_query"><?= lang('search'); ?></a>
        </div>
        <form>
</div>
