<?php

/**
 * 用户邮件查看处理
 * 20220408
 * crm_promote_mail_handle
 */
class Crm_promote_mail_handle extends CI_Controller{
    public function __construct(){

        parent::__construct();
        $this->db = Model('op_model')->get_db();
        $this->load->model('m_model');

    }

    public function index(){

        echo "index";
    }


    /**
     * 邮件查看处理
     * @return void
     */
    public function img($id){

        Header("Content-Type: image/jpeg");

//        $img_p=dirname(__FILE__).DIRECTORY_SEPARATOR."../cache/img.txt";
//        echo file_put_contents($img_p, print_r($_GET,true));
        $date_s=date('Y-m-d H:i:s');
        $id=isset($_GET["id"])?$_GET["id"]:$id;
        $sql_u="UPDATE `crm_promote_mail_log` SET `open_time` = '{$date_s}' WHERE `id` = {$id}";
        $this->db->query($sql_u);
        //print_r($_GET["id"]);


    }
}