<?php

class Bsc_user_role_model extends CI_Model
{
    private $table = 'bsc_user_role';
    public function __construct()
    {
        $this->load->database();
    }

    public function get_by_id($id = 0)
    {
        $this->db->where('id', $id);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    public function get($where = '', $sort = 'id', $order = "desc")
    {
        if ($where != "") $this->db->where($where);
        $this->db->order_by($sort, $order);
        $query = $this->db->get($this->table);
        return $query->result_array();
    }

    public function total($where = "")
    {
        if ($where != "") $this->db->where($where);

        $this->db->from($this->table);
        return $this->db->count_all_results('');
    }

    public function save($data=array()){
        $data["create_time"] = date('Y-m-d H:i:s', time());
        $data["update_time"] = date('Y-m-d H:i:s', time());
        $data["create_by"] =  $this->session->userdata('id');
        $data["update_by"] =  $this->session->userdata('id');
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($id='',$data=array()){
        $data["update_time"] = date('Y-m-d H:i:s', time());
        $data["update_by"] =  $this->session->userdata('id');
        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    public function mdelete($id = '')
    {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
    }

    public function get_one($field, $where)
    {
        $this->db->where($field, $where);
        $query = $this->db->get($this->table);
        return $query->row_array();
    }

    /**
     * 获取用户权限对应可查看的数据
     * @param $data
     * @param $admin_field
     * @return array
     */
    public function get_data_role(){
        //2022-08-29 字段权限转移到 岗位上 http://wenda.leagueshipping.com/?/question/1474
        $station = get_session('station');

        //存储用户字段权限--start
//        $user_role = join("','", $user_role);
//        $userRole = array_column($this->get("user_role in ('$user_role')"), NULL, 'user_role');
        $station_str = join("','", filter_unique_array(explode(',', $station)));
        $userRole = $this->get("id in ('$station_str')");
        //存储用户字段权限--end

        $read_text = array();
        $edit_text = array();
        //情况1 当前可查看的组为当前用户的可查看组
        foreach ($userRole as $row){
            $this_role = $row['read_text'];
            $read_text = array_merge($read_text, explode(',', $this_role));

            $this_role = $row['edit_text'];
            $edit_text = array_merge($edit_text, explode(',', $this_role));
        }

        return array('read_text' => join(',',$read_text), 'edit_text' => join(',',$edit_text));
    }

    /**
     * 使用where语句获取当前用户权限的查询,不需要join duty_new
     * @param $table
     * @param array $user_roles 设置是否具体限制到某个角色
     * @return string
     */
    public function get_field_where($table, $user_roles = array(), $duty_where = ""){
        $userId = get_session('id');
        $userRange = get_session('user_range');
        //2022-11-07 已修复 管理员导致report 详情不统一的问题
        if(empty($user_roles) && empty($duty_where) && is_admin()) {
            return "1 = 1";
        }
        if(!in_array($userId, $userRange)){
            $userRange[] = $userId;
        }

        $userRange = filter_unique_array($userRange);
        $duty_table_name = Model('biz_duty_model')->table_new;

        $where = array();
        $duty_join_where = array();
        $duty_join_where[] = "{$duty_table_name}.user_id in (" . join(',', $userRange) . ")";
        
        if(empty($user_roles) && empty($duty_where) && is_admin()) {
            $duty_join_where = array('1 = 1');
        }
        $duty_join_where_str = '(' . join(' or ', $duty_join_where) . ')';
        if(!empty($user_roles)){
            $duty_join_where_str .= "and {$duty_table_name}.user_role in('" . join("','", $user_roles) . "')";
        }
        if(!empty($duty_where)){
            $duty_join_where_str .= "and {$duty_where}";
        }

//        $where[] = "{$table}.id in (select {$table}.id from {$table} inner join {$duty_table_name} on {$table}.id = {$duty_table_name}.id_no and {$duty_table_name}.id_type = '{$table}' where $duty_join_where_str group by {$table}.id)";
        $where[] = "EXISTS (select 1 from {$duty_table_name} where biz_duty_new.id_type = '{$table}' and biz_duty_new.id_no = {$table}.id and $duty_join_where_str)";

        return '(' . join(' OR ', $where) . ')';
    }

    public function get_user_role($get_field = array()){
        $station = get_session('station');
        $station_arr = filter_unique_array(explode(',', $station));

        $all_role = array(
            'read_text' => array(),
            'edit_text' => array(),
            'delete_text' => array(),
            'template_text' => array(),
            'menu' => array(),
            'special_text' => array(),
            'init_open' => array(),
        );
        if(!is_array($get_field)){
            return $all_role;
        }
        //存储用户字段权限--start
        $station_str = join("','", $station_arr);
        $this->db->select('user_role,' . join(',', $get_field));
        $userRole = $this->get("id in ('$station_str')");
        //存储用户字段权限--end
        foreach ($userRole as $role){
            in_array('read_text', $get_field) && $all_role['read_text'] = array_merge($all_role['read_text'], array_filter(explode(',', $role['read_text'])));
            in_array('edit_text', $get_field) && $all_role['edit_text'] = array_merge($all_role['edit_text'], array_filter(explode(',', $role['edit_text'])));
            in_array('delete_text', $get_field) && $all_role['delete_text'] = array_merge($all_role['delete_text'], array_filter(explode(',', $role['delete_text'])));
            in_array('template_text', $get_field) && $all_role['template_text'] = array_merge($all_role['template_text'], array_filter(explode(',', $role['template_text'])));
            in_array('menu', $get_field) && $all_role['menu'] = array_merge($all_role['menu'], array_filter(explode(',', $role['menu'])));
            in_array('special_text', $get_field) && $all_role['special_text'] = array_merge($all_role['special_text'], array_filter(explode(',', $role['special_text'])));
            in_array('init_open', $get_field) && $all_role['init_open'] = array_merge($all_role['init_open'], array_filter(explode(',', $role['init_open'])));
        }
        $all_role['read_text'] = join(',', array_unique($all_role['read_text']));
        $all_role['edit_text'] = join(',', array_unique($all_role['edit_text']));
        $all_role['delete_text'] = join(',', array_unique($all_role['delete_text']));
        $all_role['template_text'] = join(',', array_unique($all_role['template_text']));
        $all_role['menu'] = join(',', array_unique($all_role['menu']));
        $all_role['special_text'] = join(',', array_unique($all_role['special_text']));
        $all_role['init_open'] = join(',', array_unique($all_role['init_open']));
        return $all_role;
    }

    public function get_role($id_type_field = '', $id_no_field = '', $user_roles = array()){
        $userId = get_session('id');
        $userRange = get_session('user_range');
        //2022-11-07 已修复 管理员导致report 详情不统一的问题
        if(is_admin()) {
            return "1 = 1";
        }
        if(!in_array($userId, $userRange)){
            $userRange[] = $userId;
        }

        $userRange = filter_unique_array($userRange);
        $duty_table_name = Model('biz_duty_model')->table_new;

        $where = array();
        $duty_join_where = array();
        $duty_join_where[] = "{$duty_table_name}.user_id in (" . join(',', $userRange) . ")";

        if(is_admin()) {
            $duty_join_where = array('1 = 1');
        }
        $duty_join_where_str = '(' . join(' or ', $duty_join_where) . ')';
        if(!empty($user_roles)){
            $duty_join_where_str .= "and {$duty_table_name}.user_role in('" . join("','", $user_roles) . "')";
        }

//        $where[] = "{$table}.id in (select {$table}.id from {$table} inner join {$duty_table_name} on {$table}.id = {$duty_table_name}.id_no and {$duty_table_name}.id_type = '{$table}' where $duty_join_where_str group by {$table}.id)";
        $where[] = "EXISTS (select 1 from {$duty_table_name} where {$duty_table_name}.id_type = {$id_type_field} and {$duty_table_name}.id_no = {$id_no_field} and $duty_join_where_str)";

        return '(' . join(' OR ', $where) . ')';
    }
    
    public function get_role_v3($id_type_field = '', $id_no_field = '', $user_roles = array(), $duty_where = ""){
        $userId = get_session('id');
        $userRange = get_session('user_range');
        //2022-11-07 已修复 管理员导致report 详情不统一的问题
        if(empty($user_roles) && empty($duty_where) && is_admin()) {
            return "1 = 1";
        }
        if(!in_array($userId, $userRange)){
            $userRange[] = $userId;
        }

        $userRange = filter_unique_array($userRange);
        $duty_table_name = Model('biz_duty_model')->table_new;

        $where = array();
        $duty_join_where = array();
        $duty_join_where[] = "{$duty_table_name}.user_id in (" . join(',', $userRange) . ")";

        if(empty($user_roles) && empty($duty_where) && is_admin()) {
            $duty_join_where = array('1 = 1');
        }
        $duty_join_where_str = '(' . join(' or ', $duty_join_where) . ')';
        if(!empty($user_roles)){
            $duty_join_where_str .= "and {$duty_table_name}.user_role in('" . join("','", $user_roles) . "')";
        }
        if(!empty($duty_where)){
            $duty_join_where_str .= "and {$duty_where}";
        }

//        $where[] = "{$table}.id in (select {$table}.id from {$table} inner join {$duty_table_name} on {$table}.id = {$duty_table_name}.id_no and {$duty_table_name}.id_type = '{$table}' where $duty_join_where_str group by {$table}.id)";
        $where[] = "EXISTS (select 1 from {$duty_table_name} where biz_duty_new.id_type = {$id_type_field} and biz_duty_new.id_no = {$id_no_field} and $duty_join_where_str)";

        return '(' . join(' OR ', $where) . ')';
    }

}
