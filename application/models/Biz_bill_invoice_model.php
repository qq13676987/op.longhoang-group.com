<?php
   class Biz_bill_invoice_model extends CI_Model{
       private $table = 'biz_bill_invoice';

       public function __construct()
       {
           parent::__construct();
           $this->load->database();
       }

       public function get_one($f="",$v=""){
           $this->db->where($f, $v);
           $query = $this->db->get($this->table);
           return $query->row_array();
       }
       
       public function get_where_one($where = ""){
           if($where != '')$this->db->where($where);
           $query = $this->db->get($this->table);
           return $query->row_array();
       }

       public function get($where="" ,$sort="id",$order="desc"){
           if($where !="")	$this->db->where($where, null, false);
           $this->db->order_by($sort,$order);
           $query = $this->db->get($this->table);
           return $query->result_array();
       }
       
        public function no_role_get($where="" ,$sort="id",$order="desc"){
            if($where !="")	$this->db->where($where);
            $this->db->order_by($sort,$order);
            $query = $this->db->get($this->table);
            return $query->result_array();
        }

       public function total($where=""){
           if($where !="")	$this->db->where($where, null, false);
           $this->db->from($this->table);
           
           return  $this->db->count_all_results('');
       }

       public function save($data=array()){
           $data["created_time"] = date('Y-m-d H:i:s', time());
           $data["updated_time"] = date('Y-m-d H:i:s', time());
           $data["created_by"] =  $this->session->userdata('id');
           $data["updated_by"] =  $this->session->userdata('id');
           $this->db->insert($this->table, $data);
           $id = $this->db->insert_id();
           
           return $id;
       }

       public function update($id='',$data=array(), $is_update_flag = true){
           $data["updated_time"] = date('Y-m-d H:i:s', time());
           $data["updated_by"] =  $this->session->userdata('id');
           $this->db->where('id', $id);
           $this->db->update($this->table, $data);

           if(!$is_update_flag) return $id;
           //更新 invoice_flag
           $sql = "select distinct id_no from biz_bill where invoice_id = $id and id_type='shipment' and type ='sell'";
           $query = $this->db->query($sql);
           $rs = $query->result_array();
           foreach($rs as $shipment){
               $this->calculate_invoice_flag('shipment',$shipment["id_no"]);
           }
           
           return $id;
       }

       public function mdelete($id=''){
           // delete 
           $this->db->where('id', $id);
           $this->db->delete($this->table);
       }
    
        public function calculate_invoice_flag($id_type="shipment",$id_no=0){    
            if($id_type=="conosol") return 1;
            //检测shipment下面是否有bill。 没有bill其实是已开票 
            $sql = "select count(1) as n from biz_bill where id_type='shipment' and type='sell' and id_no = $id_no";
            $query = $this->db->query($sql);
            $n = $query->row_array();
            $n = $n['n'];
            if($n==0){  
                $sql = "update biz_shipment set invoice_flag = 1 where id = $id_no";
                $this->db->query($sql); 
                return 1;
            }  
            
            //检测未开票
            $sql = "select count(1) as n from biz_bill bb left join biz_bill_invoice bbi on bbi.id = bb.invoice_id where (bb.invoice_id = 0 or (bb.invoice_id != 0 and bbi.`lock` < 1)) and bb.id_type='shipment' and bb.type='sell' and bb.id_no = $id_no";
            $query = $this->db->query($sql);
            $n = $query->row_array();
            $n = $n['n'];
            if($n>0){
                //未开票
                $sql = "update biz_shipment set invoice_flag = 0 where id = $id_no";
                $this->db->query($sql); 
                return 1;
            }else{
                //已开票
                $sql = "update biz_shipment set invoice_flag = 1 where id = $id_no";
                $this->db->query($sql); 
                return 1;
                
            } 
        }
        
   }