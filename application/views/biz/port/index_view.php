<script type="text/javascript">
    function doSearch(){
        $('#tt').datagrid('load',{
            f1: $('#f1').combobox('getValue'),
            s1: $('#s1').combobox('getValue'),
            v1: $('#v1').val() ,
            f2: $('#f2').combobox('getValue'),
            s2: $('#s2').combobox('getValue'),
            v2: $('#v2').val() ,
            f3: $('#f3').combobox('getValue'),
            s3: $('#s3').combobox('getValue'),
            v3: $('#v3').combobox('getValue'),
        });
        $('#chaxun').window('close');
    }

    function import_excel(){
        $('#import').window('open');
    }

    function multiple() {
        var tt = $('#tt');
        var singleSelect = tt.datagrid('options').singleSelect;
        if(singleSelect){
            singleSelect = false;
        }else{
            singleSelect = true;
        }
        tt.datagrid('options').singleSelect = singleSelect;
        tt.datagrid('reload');
    }

    function del() {
        var rows = $('#tt').datagrid('getSelections');
        if(rows.length > 0){
            if(rows.length <= 1){
                var row = rows[0];
                var id = row.id;
                $.messager.confirm('<?= lang('提示');?>', '<?= lang('是否确认删除？');?>', function(r){
                    if (r){
                        // 退出操作;
                        $.ajax({
                            type: 'POST',
                            url: '/biz_port/delete_data',
                            data:{
                                id:id,
                            },
                            dataType: 'json',
                            success: function (res) {
                                $.messager.alert('<?= lang('提示');?>', res.msg);
                                if(res.code == 0){
                                    $('#tt').datagrid('reload');
                                }else{
                                }
                            }
                        });
                    }
                });
            }else{
                var ids = [];
                $.each(rows, function (index, item) {
                    ids.push(item.id);
                });
                ids = ids.join(',');
                $.messager.prompt('<?= lang('提示');?>', '<?= lang('请输入delete确认批量删除？');?>', function(r){
                    if (r === 'delete'){
                        // 退出操作;
                        $.ajax({
                            type: 'POST',
                            url: '/biz_port/delete_datas',
                            data:{
                                ids:ids,
                            },
                            dataType: 'json',
                            success: function (res) {
                                $.messager.alert('<?= lang('提示');?>', res.msg);
                                if(res.code == 0){
                                    $('#tt').datagrid('reload');
                                }else{
                                }
                            }
                        });
                    }
                });
            }

        }else{
            $.messager.alert('<?= lang('提示')?>','<?= lang('请选中任意行')?>');
        }
    }

    //上传文件操作
    function UploadFile(file_ctrlname, guid_ctrlname, div_files) {
        var value = $("#" + file_ctrlname).filebox('getValue');
        var files = $("#" + file_ctrlname).next().find('input[type=file]')[0].files;

        var guid = $("#" + guid_ctrlname).val();
        if (value && files[0]) {
            //构建一个FormData存储复杂对象
            var formData = new FormData();
            formData.append('file', files[0]);

            $.ajax({
                url: '/excel/upload_excel/port', //单文件上传
                type: 'POST',
                processData: false,
                contentType: false,
                data: formData,
                success: function (json) {
                    //转义JSON为对象
                    var data = $.parseJSON(json);
                    $.messager.alert("info", data.msg);
                    $('#tt').edatagrid('reload');
                },
                error: function (xhr, status, error) {
                    $.messager.alert("提示", "操作失败"); //xhr.responseText
                }
            });
        }
    }
    $(function () {
        $('#tt').edatagrid({
            url: '/biz_port/get_data/<?= $carrier;?>',
            saveUrl: '/biz_port/add_data/<?= $carrier;?>',
            updateUrl: '/biz_port/update_data',
            destroyUrl: '/biz_port/delete_data',
            onError: function (index, data) {
                $.messager.alert('error', data.msg);
            }
        });
        $('#tt').datagrid({
            width: 'auto',
            height: $(window).height()
        });

        $(window).resize(function () {
            $('#tt').datagrid('resize');
        });

        //添加对话框，上传控件初始化
        $('#file_upload').filebox({
            prompt:'Choose another file...',
            buttonText: 'choose file',  //按钮文本
            buttonAlign: 'right',   //按钮对齐
            //multiple: true,       //是否多文件方式
            //accept:'jpg|jpeg|gif|pdf|doc|docx|xlsx|xls|zip|rar'
            accept: ".gif,.jpeg,.jpg,.png,.pdf,.doc,.docx,.xlsx,.xls,.zip,.rar", //指定文件类型
        });

        $('#file_save').click(function () {
            UploadFile("file_upload", "AttachGUID", "div_files");//上传处理
        });
    });

    function carrier_for(value, row ,index){
        console.log(1);
        return row.carrier_name;
    }
</script>

<table id="tt" style="width:1100px;height:450px"
       rownumbers="false" pagination="true" idField="id"
       pagesize="30" toolbar="#tb" singleSelect="true" nowrap="false">
    <thead>
    <tr>
        <?php if (!empty($f)) {
            foreach ($f as $rs) {

                $field = $rs[0];
                $disp = $rs[1];
                $width = $rs[2];
                $attr = isset($rs[4]) ? $rs[4] : '';

                if ($width == "0") continue;
                if ($carrier != '' && $field == 'carrier'){
                    echo "<th field=\"$field\" width=\"$width\" formatter=\"carrier_for\">" . lang($disp) . "</th>";
                    continue;
                }else if($carrier == '' && $field == 'carrier'){
                    echo "<th field=\"$field\" width=\"$width\" $attr>" . lang($disp) . "</th>";
                    echo "<th field=\"{$field}_name\" width=\"$width\">" . lang($disp . ' name') . "</th>";
                    continue;
                }
                echo "<th field=\"$field\" width=\"$width\" $attr>" . lang($disp) . "</th>";
            }
        }
        ?>
    </tr>
    </thead>
</table>

<div id="tb" style="padding:3px;">
    <table>
        <tr>
            <td>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#tt').edatagrid('addRow',0)"><?= lang('add');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="del()"><?= lang('delete');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#tt').edatagrid('saveRow')"><?= lang('save');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#tt').edatagrid('cancelRow')"><?= lang('cancel');?></a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-search" plain="true" onclick="javascript:$('#chaxun').window('open');"><?= lang('query');?></a>
                <a href="javascript:void(0);" class="easyui-linkbutton" onclick="multiple()">多选</a>
            </td>
            <td width='80'></td>
        </tr>
    </table>

</div>

<div id="chaxun" class="easyui-window" title="Query" closed="true" style="width:650px;height:380px;padding:5px;">
    <br><br>
    <table>
        <tr>
            <td>
                <?= lang('query');?> 1
            </td>
            <td align="right">
                <select name="f1"  id="f1"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s1"  id="s1"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v1" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('query');?> 2
            </td>
            <td align="right">
                <select name="f2"  id="f2"  class="easyui-combobox"  required="true" style="width:150px;">
                    <?php
                    foreach ($f as $rs){
                        $item = $rs[0];
                        echo "<option value='$item'>$item</option>";
                    }
                    ?>
                </select>
                &nbsp;

                <select name="s2"  id="s2"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp;
                <input id="v2" class="easyui-textbox"  style="width:120px;">
            </td>
        </tr>
        <tr>
            <td>
                <?= lang('query');?> 3
            </td>
            <td align="right">
                <select name="f3"  id="f3"  class="easyui-combobox"  required="true" style="width:150px;">
                   <option value='carrier'>承运人代码</option>
                </select>
                &nbsp;

                <select name="s3"  id="s3"  class="easyui-combobox"  required="true" style="width:100px;">
                    <option value="like">like</option>
                    <option value="=">=</option>
                    <option value=">=">>=</option>
                    <option value="<="><=</option>
                </select>
                &nbsp; 
                 <select class="easyui-combobox" id="v3" name="v3" style="width:120px;" data-options="
                            valueField:'client_code',
                            textField:'client_name',
                            url:'/biz_client/get_option/carrier/',
                            onSelect:function(res){ 
                            },
                            onLoadSuccess:function(){ 
                            }
                        ">
                    </select>
            </td>
        </tr>
    </table>

    <br><br>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="javascript:doSearch()" style="width:200px;height:30px;"><?= lang('search');?></a>
</div>

<div id="import" class="easyui-dialog" style="background-color: #F4F4F4;border:1px solid #95B8E7;margin-top:10px;"
     data-options="closed:true,modal:true,border:'thin',buttons:'#dlg-buttons',title:'<?= lang('Import');?>'">
    <table>
        <tr>
            <td>
                <input class="easyui-filebox" name="file" id="file_upload"
                       data-options="" style="width:400px">
            </td>
        </tr>
    </table>
    <div id="dlg-buttons">
        <a class="easyui-linkbutton" id="file_save" iconCls="icon-ok" style="width:90px"><?= lang('save');?></a>
    </div>
    <div id="div_files"></div>
</div>