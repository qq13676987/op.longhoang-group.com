<?php

class Biz_bill_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }
    
    public $field_all = array(
        array("id", "ID", "55", "1"),
        array("type", "type", "40", "2"),
        array("id_type", "id_type", "102", "2"),
        array("id_no", "id_no", "0", "4"),
        array("charge", "charge", "85", "5"),
        array("charge_code", "charge_code", "60", "5"),
        array("client_code", "client", "150", "6"),
        array("currency", "cost", "55", "5"),
        array("unit_price", "unit_price", "70", "6"),
        array("number", "number", "50", "6"),
        array("amount", "amount", "111", "6"),
        array("confirm_date", "confirm_date", "99", "6", ),
        array("remark", "remark", "99", "6"),
        array("vat", "vat", "55", "6"),
        array("account_no", "account_no", "80", "6"),
        array("commision_flag", "commision_flag", "0", "6"),
        array("created_by", "created_by", "60", "10"),
        array("created_time", "created_time", "150", "10"),
        array("updated_by", "updated_by", "60", "10"),
        array("updated_time", "updated_time", "150", "11")
    );
    
    public function get_field_edit(){
        $field_edit = array();
        foreach ($this->field_all as $row) {
            $v = $row[0];
            if ($v == "id" || $v == "created_by" || $v == "created_time" || $v == "updated_by" || $v == "updated_time") continue;
            array_push($field_edit, $v);
        }
        return $field_edit;
    }

    public function get_by_id($id = 0)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('biz_bill');
        return $query->row_array();
    }

    public function get_one($f = "", $v = "")
    {
        $this->db->where($f, $v);
        $query = $this->db->get('biz_bill');
        return $query->row_array();
    }

    public function get_where_one($where = "")
    {
        if ($where != "") $this->db->where($where);
        $query = $this->db->get('biz_bill');
        return $query->row_array();
    }

    public function get_option($bill_type, $is_special = 0)
    {
        $this->db->where('bill_type', $bill_type);
        if($is_special !== '')$this->db->where('is_special', $is_special);
        $query = $this->db->get('biz_bill');
        return $query->result_array();
    }

    public function get($where = "", $sort = "id", $order = "desc", $is_special = 0, $parent_id = true)
    {
        if ($where != "") $this->db->where($where, NULL, FALSE);
        if($is_special !== '')$this->db->where('is_special', $is_special, FALSE);
        $table = 'biz_bill';
        
        if($parent_id !== ''){
            if($parent_id){
                $this->db->where('biz_bill.parent_id', 0, FALSE);
                //判断是否查询invoice相关
                if($where != ""){
                    if(strpos($where, 'id_type') !== false && strpos($where, 'id_no') !== false) $table = "`biz_bill` FORCE INDEX(INDEX_BIZ_BILL_ID_TYPE)";
                }
            }else{
                $this->db->where('biz_bill.parent_id != 0', NULL, FALSE);
            }
        }

        $this->db->order_by($sort, $order, false);

        $query = $this->db->get($table);

        return $query->result_array();
    }
    
    public function total($where = "", $is_special = 0, $parent_id = true)
    {
        if ($where != "") $this->db->where($where, NULL, false);
        if($is_special !== '')$this->db->where('is_special', $is_special, FALSE);
        $table = 'biz_bill';
        if($parent_id !== ''){
            if($parent_id){
                $this->db->where('biz_bill.parent_id', 0, FALSE);
                //判断是否查询invoice相关
                if($where != ""){
                    if(strpos($where, 'id_type') !== false && strpos($where, 'id_no') !== false) $table = "`biz_bill` FORCE INDEX(INDEX_BIZ_BILL_ID_TYPE)";
                }
            }else{
                $this->db->where('biz_bill.parent_id != 0', NULL, false);
            }
        }
        $this->db->from($table);
        return $this->db->count_all_results('');
    }
    
    public function group_get($where = "", $group = "", $sort = "id", $order = "desc", $is_special = 0, $parent_id = true)
    {
        if ($where != "") $this->db->where($where);
        if($is_special !== '')$this->db->where('is_special', $is_special);
        if($parent_id !== ''){
            if($parent_id){
                $this->db->where('biz_bill.parent_id', 0);
            }else{
                $this->db->where('biz_bill.parent_id != 0');
            }
        }
        $this->db->group_by($group);

        $this->db->order_by($sort, $order);

        $query = $this->db->get('biz_bill');
        return $query->result_array();
    }

    public function join_get($where = "", $sort = "id", $order = "desc", $is_special = 0, $parent_id = true){
        $this->db->join('biz_bill_invoice bbi', 'bbi.id = biz_bill.invoice_id', 'left');
        $this->db->join('biz_bill_payment bbp', 'bbp.id = biz_bill.payment_id', 'left');
        $this->db->join('biz_duty_new bd', "bd.id_type = CONCAT('biz_', biz_bill.id_type) and bd.id_no = biz_bill.id_no", 'left');
//          if ($where != "") $this->db->where($where);
//         if($is_special !== '')$this->db->where('is_special', $is_special);
//         $table = 'biz_bill';
        
//         if($parent_id !== ''){
//             if($parent_id){
//                 $this->db->where('biz_bill.parent_id', 0);
//                 //判断是否查询invoice相关
//                 if($where != ""){
//                     if(strpos($where, 'id_type') !== false && strpos($where, 'id_no') !== false) $table = "`biz_bill` FORCE INDEX(INDEX_BIZ_BILL_ID_TYPE)";
//                 }
//             }else{
//                 $this->db->where('biz_bill.parent_id != 0');
//             }
//         }
// //        $rangeGroup = join("','", explode(',', $this->session->userdata('group_range')));
// //        $rangeUser = join(",", explode(',', $this->session->userdata('user_range')));

// //        $this->db->where("( biz_bill.created_group in ('$rangeGroup') or biz_bill.created_by in ('$rangeUser') )");

//         $this->db->order_by($sort, $order);

//         $query = $this->db->get($table);
//         echo lastquery();
//         return $query->result_array();
        $data = $this->get($where, $sort, $order, $is_special, $parent_id);
        return $data;
    }
    
    public function join_get2($where = "", $sort = "id", $order = "desc", $is_special = 0, $parent_id = true){
        $this->db->join('biz_duty_new', "biz_duty_new.id_type = CONCAT('biz_', biz_bill.id_type) and biz_duty_new.id_no = biz_bill.id_no", 'left');

//         if ($where != "") $this->db->where($where);
//         if($is_special !== '')$this->db->where('is_special', $is_special);
//         $table = 'biz_bill';
        
//         if($parent_id !== ''){
//             if($parent_id){
//                 $this->db->where('biz_bill.parent_id', 0);
//                 //判断是否查询invoice相关
//                 if($where != ""){
//                     if(strpos($where, 'id_type') !== false && strpos($where, 'id_no') !== false) $table = "`biz_bill` FORCE INDEX(INDEX_BIZ_BILL_ID_TYPE)";
//                 }
//             }else{
//                 $this->db->where('biz_bill.parent_id != 0');
//             }
//         }
// //        $rangeGroup = join("','", explode(',', $this->session->userdata('group_range')));
// //        $rangeUser = join(",", explode(',', $this->session->userdata('user_range')));

// //        $this->db->where("( biz_bill.created_group in ('$rangeGroup') or biz_bill.created_by in ('$rangeUser') )");

//         $this->db->order_by($sort, $order);

//         $query = $this->db->get($table);
//         echo lastquery();
//         return $query->result_array();
        $data = $this->get($where, $sort, $order, $is_special, $parent_id);
        return $data;
    }

    public function join_total($where = "", $is_special = 0, $parent_id = true){
        $this->db->join('biz_bill_invoice bbi', 'bbi.id = biz_bill.invoice_id', 'left');
        $this->db->join('biz_bill_payment bbp', 'bbp.id = biz_bill.payment_id', 'left');
        
        $this->db->join('biz_duty_new bd', "bd.id_type = CONCAT('biz_', biz_bill.id_type) and bd.id_no = biz_bill.id_no", 'left');
        $data = $this->total($where, $is_special, $parent_id);
        return $data;
    }

    public function join_total2($where = "", $is_special = 0, $parent_id = true){
        $this->db->join('biz_duty_new', "biz_duty_new.id_type = CONCAT('biz_', biz_bill.id_type) and biz_duty_new.id_no = biz_bill.id_no", 'left');
        $data = $this->total($where, $is_special, $parent_id);
        return $data;
    }
    
    public function join_d_get($where = "", $sort = "id", $order = "desc", $is_special = 0, $parent_id = true){
        if(!is_admin() && get_session('admin_login') == 'admin'){
            // $this->db->join('biz_duty_ext bd', "bd.biz_table = CONCAT('biz_', biz_bill.id_type) and bd.id_no = biz_bill.id_no and user_role = 'finance' and user_id = " . get_session('id'), 'RIGHT');
            // $this->db->join('biz_duty_ext_copy1 bde', "bde.biz_table = CONCAT('biz_', biz_bill.id_type) and bde.id_no = biz_bill.id_no and bde.user_role = 'finance'", 'INNER');
            // $this->db->join('biz_duty_ext_user bdeu', "bdeu.d_id = bde.d_id and bdeu.user_id = " . get_session('id'), 'INNER');
        }
//        $this->db->where(Model('bsc_user_role_model')->get_role("CONCAT('biz_', biz_bill.id_type)", 'biz_bill.id_no'), null, false);
        $data = $this->get($where, $sort, $order, $is_special, $parent_id);
        return $data;
    }

    public function join_d_total($where = "", $is_special = 0, $parent_id = true){
        if(!is_admin() && get_session('admin_login') == 'admin'){
            // $this->db->join('biz_duty_ext bd', "bd.biz_table = CONCAT('biz_', biz_bill.id_type) and bd.id_no = biz_bill.id_no and user_role = 'finance' and user_id = " . get_session('id'), 'RIGHT');
            // $this->db->join('biz_duty_ext_copy1 bde', "bde.biz_table = CONCAT('biz_', biz_bill.id_type) and bde.id_no = biz_bill.id_no and bde.user_role = 'finance'", 'INNER');
            // $this->db->join('biz_duty_ext_user bdeu', "bdeu.d_id = bde.d_id and bdeu.user_id = " . get_session('id'), 'INNER');
        }
//        $this->db->where(Model('bsc_user_role_model')->get_role("CONCAT('biz_', biz_bill.id_type)", 'biz_bill.id_no'), null, false);
        $data = $this->total($where, $is_special, $parent_id);
        return $data;
    }

    public function no_role_get($where="" ,$sort="id",$order="desc", $is_special = 0, $parent_id = true){
        if($where !="")	$this->db->where($where);
        if($is_special !== '')$this->db->where('is_special', $is_special);
        if($parent_id !== ''){
            if($parent_id){
                $this->db->where('biz_bill.parent_id', 0);
            }else{
                $this->db->where('biz_bill.parent_id != 0');
            }
        }
        $this->db->order_by($sort,$order);
        $query = $this->db->get('biz_bill');
        return $query->result_array();
    }
    
    public function not_role_get($where="" ,$sort="id",$order="desc"){
        if($where !="")	$this->db->where($where);
        $this->db->order_by($sort,$order);
        $query = $this->db->get('biz_bill');
        return $query->result_array();
    }

    public function save($data = array())
    {
        $data["created_time"] = date('y-m-d H:i:s', time());
        $data["updated_time"] = date('y-m-d H:i:s', time());
        $data["created_by"] = $this->session->userdata('id');
        $data["updated_by"] = $this->session->userdata('id');
        $data["created_group"] = implode(',', $this->session->userdata('group'));
        $this->db->insert('biz_bill', $data);
        $new_id = $this->db->insert_id();
        
        // if(isset($data['id_type']) && $data['id_type'] == 'shipment')$this->add_flag_queue($data["id_no"]);
        // // //检测loss_flag 
        // $this->calculate_loss_flag($data["id_type"],$data["id_no"]); 
        // // //检测invoice_flag
        // $this->calculate_invoice_flag($data["id_type"],$data["id_no"]);  
        return $new_id;
    }

    public function update($id = '', $data = array())
    {
        $data["updated_by"] = $this->session->userdata('id');
        $this->db->where('id', $id);
        $this->db->update('biz_bill', $data);
        
        // //检测loss_flag 
        // if(isset($data['id_type']) && $data['id_type'] == 'shipment'){
            // if(!isset($data['id_no'])) $data = $this->get_by_id($id);
            // $this->add_flag_queue($data["id_no"]);
        // }
        // $this->calculate_loss_flag($data["id_type"],$data["id_no"]);  
        // // //检测invoice_flag
        // $this->calculate_invoice_flag($data["id_type"],$data["id_no"]);  
        
        return $id;
    }

    public function mdelete($id = '')
    {
        // $data = $this->get_by_id($id);
        // delete bill
        $this->db->where('id', $id);
        $this->db->delete('biz_bill');
        
        $sql = "delete from biz_bill where parent_id = $id";
        $this->db->query($sql);
        
        // $this->add_flag_queue($data["id_no"]);
        // // //检测loss_flag 
        // $this->calculate_loss_flag($data["id_type"],$data["id_no"]); 
        // // //检测invoice_flag
        // $this->calculate_invoice_flag($data["id_type"],$data["id_no"]); 
        
    }

    public function mbacth_update($data, $wfield = 'id')
    {
        $af = $this->db->update_batch('biz_bill', $data, $wfield);

        // if($wfield == 'id'){
        //     $ids = array_column($data, 'id');
        //     if(!empty($ids)){
        //         $this->db->select('id,id_type,id_no');
        //         $bills = array_column($this->not_role_get("id in (" . join(',', $ids) . ")"), null, 'id');
        //         foreach($data as $row){
        //             if(!isset($bills[$row['id']])){
        //                 continue;
        //             }
        //             $bill = $bills[$row['id']];
        //             if($bill['id_type'] == 'shipment') $this->add_flag_queue($bill["id_no"]);
        //             if($bill['id_type'] == 'consol'){
        //                 Model('biz_shipment_model');
        //                 $this->db->select('id');
        //                 $shipments = $this->biz_shipment_model->get_shipment_by_consol($bill['id_no']);
        //                 foreach ($shipments as $shipment){
        //                     $this->add_flag_queue($shipment["id"]);
        //                 }
        //             }
        //             // $bill = $this->get_by_id($row['id']);
        //             // $this->calculate_invoice_flag($bill["id_type"],$bill["id_no"]); 
        //             // $this->calculate_loss_flag($bill["id_type"],$bill["id_no"]); 
        //         }
        //     }
        // }
        
        
        
        return $af;
    }

    public function get_all_sum($field = '', $where = '', $is_special = 0, $parent_id = true)
    {
        if(!is_array($field)) $field = explode(',', $field);
        if($is_special !== '')$this->db->where('is_special', $is_special);
        foreach ($field as $val){
            $this->db->select_sum($val, $val);
        }
        if($parent_id !== ''){
            if($parent_id){
                $this->db->where('bs.parent_id', 0);
            }else{
                $this->db->where('bs.parent_id != 0');
            }
        }
        $this->db->where($where);
        $query = $this->db->get('biz_bill bs');
        return $query->row_array();
    }
    
    public function calculate_invoice_flag($id_type="shipment",$id_no=0){    
        if($id_type=="consol") return 1;
        //检测shipment下面是否有bill。 没有bill其实是已开票 
        $sql = "select count(1) as n from biz_bill where id_type='shipment' and type='sell' and id_no = $id_no and parent_id = 0";
        $query = $this->db->query($sql);
        $n = $query->row_array();
        $n = $n['n'];
        if($n==0){  
            $sql = "update biz_shipment set invoice_flag = 1 where id = $id_no";
            $this->db->query($sql); 
            return 1;
        }  
        
        //检测未开票
        $sql = "select count(1) as n from biz_bill bb left join biz_bill_invoice bbi on bbi.id = bb.invoice_id where (bb.invoice_id = 0 or (bb.invoice_id != 0 and bbi.`lock` < 1)) and bb.id_type='shipment' and bb.type='sell' and bb.id_no = $id_no and bb.parent_id = 0";
        $query = $this->db->query($sql);
        $n = $query->row_array();
        $n = $n['n'];
        if($n>0){
            //未开票
            $sql = "update biz_shipment set invoice_flag = 0 where id = $id_no";
            $this->db->query($sql); 
            return 2;
        }else{
            //已开票
            $sql = "update biz_shipment set invoice_flag = 1 where id = $id_no";
            $this->db->query($sql); 
            return 3;
            
        } 
        return 4;
    }
    
    public function calculate_loss_flag($id_type="shipment",$id_no=0){   
        if($id_type=="shipment"){
            //检测shipment的bill  
            $sql = "select IFNULL(sum(if(type='cost',-local_amount,local_amount)),0) as n from biz_bill where id_type='shipment' and id_no = $id_no and parent_id = 0";
            $query = $this->db->query($sql);
            $n = $query->row_array();
            $s1 = $n['n']; 
            
            //检测consol的成本,分摊到shipment
            $sql = "select * from biz_shipment where id = $id_no";
            $query = $this->db->query($sql);
            $row = $query->row_array();
            $consol_id = $row["consol_id"];
            // $consol_percent = $row["consol_percent"];
            $consol_percent = 1;
            // $sql = "select IFNULL(sum(if(type='cost',-local_amount,local_amount)),0) as n from biz_bill where id_type='consol' and id_no = $consol_id and parent_id = 0";
            $sql = "select IFNULL(sum(if(type='cost',-local_amount,local_amount)),0) as n from biz_bill where id_type='shipment' and id_no = $id_no and parent_id != 0";
            $query = $this->db->query($sql);
            $n = $query->row_array();
            $s2 = $n['n']; 
            // $s2 *= $consol_percent;
            
            $s = $s1 + $s2;
            
            if($s<0){ 
                //已亏损
                $sql = "update biz_shipment set loss_flag = 1 where id = $id_no";
                $this->db->query($sql); 
                return 1; 
            }else{
                //未亏损
                $sql = "update biz_shipment set loss_flag = 0 where id = $id_no";
                $this->db->query($sql); 
                return 1;
            } 
        }
        
        if($id_type=="consol"){
            //consol需要更新旗下的所有shipment的亏损情况，先查询shipments
            $sql = "select * from biz_shipment where consol_id = $id_no";
            $query = $this->db->query($sql);
            $rs = $query->result_array();
            foreach($rs as $shipment){
                $id = $shipment["id"];
                $this->calculate_loss_flag("shipment",$id);
            } 
        }
        return 1;
    }

    public function add_flag_queue($id_no){
        return;
        $queue_id = 7;
        Model('sys_queue_model');
        $name = $this->sys_queue_model->get_queue_name($queue_id);
        $sys_queue = $this->sys_queue_model->get_where_one("name = '$name' and id_type = 'biz_shipment' and id_no = '$id_no' and status = 0");
        if(empty($sys_queue))save_queue(7, 'biz_shipment', $id_no);
    }
    
    public function all_where(&$where, &$sort){
        //需要处理的特殊排序字段
        $sort_arrar = array(
            'carrier_ref' => 'IF(id_type = \'shipment\', (select (select carrier_ref from biz_consol where consol_id = id) from biz_shipment where id = biz_bill.id_no and id_type = \'shipment\'), (select carrier_ref from biz_consol where id = biz_bill.id_no and id_type = \'consol\'))',
            'booking_ETD' => 'bill_ETD',
            'job_no' => 'IF(id_type = \'shipment\', (select job_no from biz_shipment where id = biz_bill.id_no and id_type = \'shipment\'), (select job_no from biz_consol where id = biz_bill.id_no and id_type = \'consol\'))',
            'client_code_name' => 'client_code',
            'created_by_name' => '(select name from bsc_user where bsc_user.id = biz_bill.created_by)',
            'updated_by_name' => '(select name from bsc_user where bsc_user.id = biz_bill.updated_by)',
        );
        $sort = isset($sort_arrar[$sort]) ? $sort_arrar[$sort] : $sort;

        //-------这个查询条件想修改成通用的 -------------------------------
        $user_role = isset($_REQUEST['user_role']) ? trim($_REQUEST['user_role']) : '';
        $user = isset($_REQUEST['user']) ? $_REQUEST['user'] : array();
        $fields = isset($_REQUEST['field']) ? $_REQUEST['field'] : array();

        $title = isset($_REQUEST['title']) ? trim($_REQUEST['title']) : '';
        $currency = isset($_REQUEST['currency']) ? trim($_REQUEST['currency']) : '';
        $type = isset($_REQUEST['type']) ? trim($_REQUEST['type']) : '';
        $groups = isset($_REQUEST['group']) ? $_REQUEST['group'] : array();
        $group_role = isset($_REQUEST['group_role']) ? trim($_REQUEST['group_role']) : '';
        $commision_flag = isset($_REQUEST['commision_flag']) ? trim($_REQUEST['commision_flag']) : '';
        
        Model('biz_bill_invoice_model');
        Model('biz_bill_payment_model');

        if ($title != "") $where[] = "type = '$title'";
        if ($currency != "") $where[] = "currency = '$currency'";
        if ($commision_flag != "") $where[] = "commision_flag = '$commision_flag'";
        if ($type !== "") {
            if($type == 1) {//未开票未销账
                $where[] = "biz_bill.invoice_id = 0 and biz_bill.payment_id = 0";
            }else if ($type == 2) {//已开票未销账
                $where[] = "biz_bill.invoice_id != 0 and biz_bill.payment_id = 0";
            }else if ($type == 3) {//未销账
                $where[] = "biz_bill.payment_id = 0";
            }else if ($type == 4) {//未开票已销账
                $where[] = "biz_bill.invoice_id = 0 and biz_bill.payment_id != 0";
            }else if ($type == 5) {//待审核
                $this->db->select('id');
                $no_lock_pm = $this->biz_bill_payment_model->get("lock_date = '0000-00-00'");
                if(empty($no_lock_pm)) $no_lock_pm[] = -1;
                $no_lock_pm_ids = array_column($no_lock_pm, 'id');
                $where[] = "payment_id in (" . join(',', $no_lock_pm_ids) . ")";
            }else if ($type == 6) {//审核完成
                $this->db->select('id');
                $no_lock_pm = $this->biz_bill_payment_model->get("lock_date != '0000-00-00'");
                if(empty($no_lock_pm)) $no_lock_pm[] = -1;
                $no_lock_pm_ids = array_column($no_lock_pm, 'id');
                $where[] = "payment_id in (" . join(',', $no_lock_pm_ids) . ")";
            }
        }
        $user = join("','", array_filter($user));
        if ($user_role != "" && !empty($user))
        {
			$where[] = "((biz_bill.id_no in (select id_no from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' and user_role = '$user_role' and biz_duty_new.user_id in ('$user')) and biz_bill.id_type = 'shipment') " .
				"or (biz_bill.id_no in (select id_no from biz_duty_new where biz_duty_new.id_type = 'biz_consol' and user_role = '$user_role' and biz_duty_new.user_id in ('$user')) and biz_bill.id_type = 'consol'))";
        }
        $groups = join("','", array_filter($groups));
        if($group_role != "" && !empty($groups)){
			$where[] = "((biz_bill.id_no in (select id_no from biz_duty_new where biz_duty_new.id_type = 'biz_shipment' user_role = '$group_role' and biz_duty_new.user_group in ('$groups')) and biz_bill.id_type = 'shipment') " .
				"or (biz_bill.id_no in (select id_no from biz_duty_new where biz_duty_new.id_type = 'biz_consol' user_role = '$group_role' and biz_duty_new.user_group in ('$groups')) and biz_bill.id_type = 'consol'))";
        }
        if(!empty($fields)){
            foreach ($fields['f'] as $key => $field){
                $f = trim($fields['f'][$key]);
                $s = $fields['s'][$key];
                $v = trim($fields['v'][$key]);
                $v = explode("\r\n", $v);
                if($f == '') continue;
                $f_arr = explode('.', $f);
                if($f_arr[0] == 'invoice'){
                    $v = array_filter($v);
                    if(empty($v)) continue;
                    $inv_where = array();
                    foreach ($v as $vv){
                        $where_v = $vv;
                        if($s == 'like') $where_v = $where_v . '%';
                        $inv_where[] = $f_arr[1] . " $s '$where_v'";
                    }
                    if(empty($inv_where)) $inv_where = "id = 0";
                    $inv_where = "(" . join(' or ', $inv_where) . ")";
                    $this->db->select('id');
                    $this_inv = $this->biz_bill_invoice_model->get($inv_where);
                    if(empty($this_inv)) $this_inv[] = -1;
                    $this_inv_ids = array_column($this_inv, 'id');
                    $where[] = "invoice_id in (" . join(',', $this_inv_ids) . ")";
                }else if($f_arr[0] == 'payment'){
                    $pm_where = array();
                    $v = array_filter($v);
                    if(empty($v)) continue;
                    foreach ($v as $vv){
                        if($f_arr[1] == 'payment_user_id'){
                            if($s == 'like')$vv = "'%$vv%'";
                            $pm_where[] = "payment_user_id in (select id from bsc_user where name $s $vv)";
                        }else{
                            $pm_where[] = search_like($f_arr[1], $s, $vv);
                        }
                    }
                    if(empty($pm_where)) $pm_where = "id = 0";
                    $pm_where = "(" . join(' or ', $pm_where) . ")";
                    $this->db->select('id');
                    $this_pm = $this->biz_bill_payment_model->get($pm_where);
                    if(empty($this_pm)) $this_pm[] = -1;
                    $this_pm_ids = array_column($this_pm, 'id');
                    $where[] = "payment_id in (" . join(',', $this_pm_ids) . ")";
                }else if($f_arr[0] == 'bill'){
                    $v = array_filter($v);
                    if(!empty($v)){
                        if($f_arr[1] == 'client_code_name'){
                            Model('biz_client_model');
                            
                            $this->db->select('client_code');
                            $this_where = array();
                            foreach ($v as $vv){
                                $this_where[] = search_like('company_name', $s, $vv);
                            }
                            $this_where = join(' or ', $this_where);
                            
                            $clients = $this->biz_client_model->no_role_get($this_where);
                            
                            $client_codes = join("','", array_column($clients, 'client_code'));
                            if(empty($client_codes))$client_codes = '0';
                            $where[] = "biz_bill.client_code in ('$client_codes')";
                        }else{
                            $this_where = array();
                            foreach ($v as $vv){
                                // if($f_arr[1] == 'account_no' && $s == 'like') {
                                //     $this_where[] = 'biz_bill.' . $f_arr[1] . " like '{$vv}%'";
                                //     continue;
                                // }
                                $this_where[] = search_like('biz_bill.' . $f_arr[1], $s, $vv);
                            }
                            if(!empty($this_where))$where[] = '(' . join(' or ', $this_where) . ')'; 
                        }
                    }
                }else if($f_arr[0] == 'shipment'){
                    $biz_shipment_model = Model('biz_shipment_model');
                    $v = array_filter($v);
                    if(!empty($v)){
                        $this->db->select('id');
                        $this_where = array();
                        foreach ($v as $vv){
                            $this_where[] = search_like($f_arr[1], $s, $vv);
                        }
                        $this_where = join(' or ', $this_where);
                        $shipments = $biz_shipment_model->no_role_get($this_where);
                        $shipments_ids = join(',', array_column($shipments, 'id'));
                        if(empty($shipments_ids))$shipments_ids = 0;
                        $where[] = "biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)";
                    }
                }else if($f_arr[0] == 'consol'){
                    $biz_shipment_model = Model('biz_shipment_model');
                    $biz_consol_model = Model('biz_consol_model');
                    $biz_client_model = Model('biz_client_model');

                    $v = array_filter($v);
                    if(!empty($v)){
                        if($f_arr[1] == 'creditor'){
                            $this_where = array();
                            foreach ($v as $vv){
                                $this_where[] = search_like('company_search', $s, match_chinese($vv));
                            }
                            $this_where = join(' or ', $this_where);
                            $clients = $biz_client_model->no_role_get($this_where);
                            $client_codes = join("','", array_column($clients, 'client_code'));
                            $this_where = "creditor in ('$client_codes')";
                        }else{
                            $this_where = array();
                            foreach ($v as $vv){
                                $this_where[] = search_like($f_arr[1], $s, $vv);
                            }
                            $this_where = join(' or ', $this_where);
                        }
                        
                        $this->db->select('id');
                        $consols = $biz_consol_model->no_role_get($this_where);
                        $consols_ids = join(',', array_column($consols, 'id'));
                        if(empty($consols_ids)){
                            $consols_ids = 0;
                            $shipments_ids = 0;
                        }else{
                            $this->db->select('id');
                            $shipments = $biz_shipment_model->no_role_get("consol_id in ($consols_ids)");
                            $shipments_ids = join(',', array_column($shipments, 'id'));
                            if(empty($shipments_ids)){
                                $shipments_ids = 0;
                            }
                        }
                        $where[] = "((biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)) or (biz_bill.id_type = 'consol' and biz_bill.id_no in ($consols_ids)))";
                    }
                }else if($f_arr[0] == 'shipment_consol'){
                    $biz_shipment_model = Model('biz_shipment_model');
                    $biz_consol_model = Model('biz_consol_model');

                    $v = array_filter($v);
                    if(!empty($v)){
                        $this_where = array();
                        foreach ($v as $vv){
                            $this_where[] = search_like($f_arr[1], $s, $vv);
                        }
                        $this_where = join(' or ', $this_where);
                        $this->db->select('id');
                        $consols = $biz_consol_model->no_role_get($this_where);
                        $consols_ids = join(',', array_column($consols, 'id'));
                        if(empty($consols_ids)){
                            $consols_ids = 0;
                        }
                        if($f_arr[1] == 'carrier_ref') $this_where = str_replace('carrier_ref', 'cus_no', $this_where);
                        $this->db->select('id');
                        $shipments = $biz_shipment_model->no_role_get($this_where);
                        $shipments_ids = join(',', array_column($shipments, 'id'));
                        if(empty($shipments_ids)){
                            $shipments_ids = 0;
                        }
                        $where[] = "((biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)) or (biz_bill.id_type = 'consol' and biz_bill.id_no in ($consols_ids)))";
                    }
                }else if($f_arr[0] == 'shipment_by_consol'){
                    $biz_shipment_model = Model('biz_shipment_model');
                    $biz_consol_model = Model('biz_consol_model');

                    $v = array_filter($v);
                    if(!empty($v)){
                        $this_where = array();
                        foreach ($v as $vv){
                            $this_where[] = search_like($f_arr[1], $s, $vv);
                        }
                        $this_where = join(' or ', $this_where);
                        $this->db->select('id,consol_id');
                        $shipments = $biz_shipment_model->no_role_get($this_where);
                        $shipments_ids = join(',', array_column($shipments, 'id'));
                        if(empty($shipments_ids)){
                            $shipments_ids = 0;
                            $consols_ids = 0;
                        }else{
                            $consol_ids = join(',', array_filter(array_column($shipments, 'consol_id')));
                            $this->db->select('id');
                            if(empty($consol_ids))$consol_ids = 0;
                            $consols = $biz_consol_model->no_role_get("id in ($consol_ids)");
                            $consols_ids = join(',', array_column($consols, 'id'));
                            if(empty($consols_ids))$consols_ids = 0;
                        }
                        $where[] = "((biz_bill.id_type = 'shipment' and biz_bill.id_no in ($shipments_ids)) or (biz_bill.id_type = 'consol' and biz_bill.id_no in ($consols_ids)))";
                    }
                }
            }
        }
        //获取可查看组权限
        $bsc_user_role_model = Model('bsc_user_role_model');
        // $where[] = $bsc_user_role_model->get_bill_role();
        //获取cost和sell查看权限
        $user_role = $bsc_user_role_model->get_user_role(array('read_text'));
        $userRoleRead = explode(',', $user_role['read_text']);//bill.cost,bill.sell
        $type_where = array();
        if(in_array('bill', $userRoleRead) || in_array('bill.cost', $userRoleRead)){
            $type_where[] = "biz_bill.type = 'cost'";
        }
        if(in_array('bill', $userRoleRead) || in_array('bill.sell', $userRoleRead)){
            $type_where[] = "biz_bill.type = 'sell'";
        }
        if(sizeof($type_where) > 1) $type_where = array();
        if(!empty($type_where))$where[] = '(' . join(' or ', $type_where) . ')';
    }
}
